﻿using IEMQSImplementation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ItemCreationWebApi.Controllers
{
    public class MaintainItemAPIController : ApiController
    {
        public readonly IEMQSEntitiesContext db;

        public MaintainItemAPIController()
        {
            if (db == null)
            {
                db = new IEMQSEntitiesContext();
            }
        }

        [HttpPost]
        [Authorize]
        // POST: ICC/api/MaintainItemAPI       
        public HttpResponseMessage Post([System.Web.Mvc.Bind(Include = "MaterialCategory, MaterialGroup, DistinctItem, UOM, ItemCode, LongDescription, CreatedBy")]IList<ICC002Input> item)
        {
            HttpRequestMessage request = new HttpRequestMessage();
            try
            {
                string MDM3Role = clsImplementationEnum.UserRoleName.MDM3.GetStringValue();
                var responseList = new List<ResponseData>();

                //var notSavedItem = new List<ICC002>();
                //var isError = false;
                if (item.Any())
                {
                    var userEmail = (from ath1 in db.ATH001
                                     join com7 in db.COM007 on ath1.Employee equals com7.t_emno
                                     join ath4 in db.ATH004 on ath1.Role equals ath4.Id
                                     where ath4.Role.Equals(MDM3Role)
                                     select com7.t_mail).Distinct().ToList();
                    MAIL001 objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.ICC.ItemCreation).FirstOrDefault();

                    foreach (var temp in item)
                    {
                        try
                        {
                            if (!string.IsNullOrEmpty(temp.ItemCode))
                            {
                                if (!db.ICC002.Any(x => x.ItemCode.Trim() == temp.ItemCode.Trim()))
                                {
                                    ICC002 data = new ICC002();
                                    data.ItemCode = temp.ItemCode;
                                    data.MaterialCategory = temp.MaterialCategory;
                                    data.MaterialGroup = temp.MaterialGroup;
                                    data.DistinctItem = temp.DistinctItem;
                                    data.UOM = temp.UOM;
                                    data.ItemCode = temp.ItemCode;
                                    data.LongDescription = temp.LongDescription;
                                    data.ItemCodeRequestor = temp.CreatedBy;
                                    data.Status = clsImplementationEnum.ICCStatus.Not_Created.GetStringValue();
                                    data.CreatedOn = DateTime.Now;
                                    db.ICC002.Add(data);
                                    db.SaveChanges();

                                    var itemGroupData = db.ICC001.FirstOrDefault(x =>
                                    x.MaterialCategory.Equals(data.MaterialCategory) &&
                                    x.MaterialGroup.Equals(data.MaterialGroup) &&
                                    x.UOM.Equals(data.UOM) &&
                                    x.DistinctItem.Equals(data.DistinctItem));

                                    if (itemGroupData == null)
                                    {
                                        var itemGroup = new ICC001()
                                        {                                            
                                            CreatedOn = DateTime.Now,
                                            DistinctItem = data.DistinctItem,
                                            MaterialCategory = data.MaterialCategory,
                                            MaterialGroup = data.MaterialGroup,
                                            UOM = data.UOM
                                        };
                                        db.ICC001.Add(itemGroup);
                                        db.SaveChanges();
                                    }

                                    if (objTemplateMaster != null)
                                    {
                                        Hashtable _ht = new Hashtable();
                                        EmailSend _objEmail = new EmailSend();
                                        _objEmail.MailToAdd = string.Join(",", userEmail); // Get ALL MDM User Email Ids
                                        _ht["[ItemCode]"] = data.ItemCode;
                                        _ht["[ItemLongDescription]"] = data.LongDescription;
                                        _ht["[MaterialCategory]"] = data.MaterialCategory;
                                        _ht["[MaterialGroup]"] = data.MaterialGroup;
                                        _ht["[DistinctItem]"] = data.DistinctItem;
                                        _ht["[UOM]"] = data.UOM;
                                        _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                                    }

                                    responseList.Add(new ResponseData()
                                    {
                                        result = true,
                                        message = "Record added successfully",
                                        ItemCode = data.ItemCode

                                    });
                                }
                                else
                                {
                                    responseList.Add(new ResponseData()
                                    {
                                        result = false,
                                        message = "Item Code already exists",
                                        ItemCode = temp.ItemCode
                                    });
                                }
                            }
                            else
                            {
                                //isError = true;
                                //notSavedItem.Add(data);

                                responseList.Add(new ResponseData()
                                {
                                    result = false,
                                    message = "Item Code is required",
                                    ItemCode = temp.ItemCode
                                });
                            }
                        }
                        catch (Exception ex)
                        {
                            responseList.Add(new ResponseData()
                            {
                                result = false,
                                message = ex.Message,
                                ItemCode = temp.ItemCode
                            });
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }

                return Request.CreateResponse(HttpStatusCode.OK, responseList);

                //if (isError)
                //{
                //    return Request.CreateResponse(notSavedItem.Select(x => new
                //    {
                //        x.MaterialCategory,
                //        x.MaterialGroup,
                //        x.DistinctItem,
                //        x.UOM,
                //        x.ItemCode,
                //        x.LongDescription,
                //        x.CreatedBy
                //    }));
                //}
                //return Request.CreateResponse(HttpStatusCode.OK, "Data inserted successfully");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpPost]
        [Authorize]
        public string GetTestMessage(string message)
        {
            return message;
        }
    }

    public class ICC002Input
    {
        public string MaterialCategory { get; set; }

        public string MaterialGroup { get; set; }

        public string DistinctItem { get; set; }

        public string UOM { get; set; }

        public string ItemCode { get; set; }

        public string LongDescription { get; set; }

        public string CreatedBy { get; set; }
    }

    public class ResponseData
    {
        public bool result { get; set; }
        public string message { get; set; }
        public string ItemCode { get; set; }
    }
}
