﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQS.PPSCore.Data
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class IEMQSPPSEntities : DbContext
    {
        public IEMQSPPSEntities()
            : base("name=IEMQSPPSEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<PPS001> PPS001 { get; set; }
        public virtual DbSet<PPS101> PPS101 { get; set; }
        public virtual DbSet<PPS151> PPS151 { get; set; }
    
        public virtual ObjectResult<PPS_GET_CHARACTERISTICS_DATA_Result> PPS_GET_CHARACTERISTICS_DATA(Nullable<int> startIndex, Nullable<int> endIndex, string sortingFields, string sqlQuery)
        {
            var startIndexParameter = startIndex.HasValue ?
                new ObjectParameter("StartIndex", startIndex) :
                new ObjectParameter("StartIndex", typeof(int));
    
            var endIndexParameter = endIndex.HasValue ?
                new ObjectParameter("EndIndex", endIndex) :
                new ObjectParameter("EndIndex", typeof(int));
    
            var sortingFieldsParameter = sortingFields != null ?
                new ObjectParameter("SortingFields", sortingFields) :
                new ObjectParameter("SortingFields", typeof(string));
    
            var sqlQueryParameter = sqlQuery != null ?
                new ObjectParameter("SqlQuery", sqlQuery) :
                new ObjectParameter("SqlQuery", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<PPS_GET_CHARACTERISTICS_DATA_Result>("PPS_GET_CHARACTERISTICS_DATA", startIndexParameter, endIndexParameter, sortingFieldsParameter, sqlQueryParameter);
        }
    }
}
