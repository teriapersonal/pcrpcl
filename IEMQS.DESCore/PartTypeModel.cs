﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace IEMQS.DESCore
{
    /// <summary>
    /// DES040 Table Model
    /// Ravi Patel 27-02-2020
    /// </summary>
    public class PartTypeModel
    {
        public Int64? ItemTypeId { get; set; }
        public string ProductForm { get; set; }
        public string ItemType { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }
    }
}
