﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace IEMQS.DESCore
{
   public class PartSheetModel
    {
        public Int64? ItemSheetId { get; set; }

        public Int64? FolderId { get; set; }
        public string Project { get; set; }

        [Remote("CheckSheetNameAlreadyExist", "Part", AdditionalFields = "Project,ItemSheetId", HttpMethod = "POST", ErrorMessage = "Name already exist.")]
        [Required(ErrorMessage ="Sheet name is required.")]
        [StringLength(50,ErrorMessage = "length exceeded the recommended length of 50 characters")]
        public string SheetName { get; set; }
        
        [StringLength(200, ErrorMessage = "length exceeded the recommended length of 200 characters")]
        public string Description { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }
        public string Status { get; set; }
        public string CurrentPsNo { get; set; }
    }
}
