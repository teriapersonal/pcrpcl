﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace IEMQS.DESCore
{
    /// <summary>
    /// DES005 Table Model
    ///         14-11-2019
    /// </summary>
    public class PolicyModel
    {
        public Int64? PolicyId { get; set; }

        [Remote("CheckPolicyNameAlreadyExist", "Parameter", AdditionalFields = "PolicyId", HttpMethod = "POST", ErrorMessage = "Name already exist.")]
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }
        public string Description { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }

        public List<PolicyStepModel> PolicyStepList { get; set; }
    }

    /// <summary>
    /// DES006 Table Model
    ///         14-11-2019
    /// </summary>
    public class PolicyStepModel
    {
        public Int64? PolicyStepId { get; set; }
        public Int64? PolicyId { get; set; }
        public string StepName { get; set; }
        public int? OrderNo { get; set; }
        public string ColorCode { get; set; }
        public int? DesignationLevels { get; set; }
        public bool IsDelete { get; set; }
    }
}
