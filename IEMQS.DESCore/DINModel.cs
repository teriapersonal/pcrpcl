﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace IEMQS.DESCore
{
    public class DINModel
    {
        public Int64? DINId { get; set; }

        [Remote("CheckDINNo", "DIN", AdditionalFields = "DINNoGenratorPrefix,DINId,ProjectNo,IssueNo", HttpMethod = "POST", ErrorMessage = "DIN No already exist.")]
        [StringLength(50, ErrorMessage = "length can't be more than 50")]
        [Required(ErrorMessage = "DIN No is required")]
        public string DINNo { get; set; }
        public string ProjectNo { get; set; }
        public string ContractNo { get; set; }
        public string ProjectDescription { get; set; }
        public string Customer { get; set; }
        public string ASMETag { get; set; }

        [Required(ErrorMessage = "Nuclear Code is required")]
        public string NuclearASME { get; set; }
        public string CustomerForDINReport { get; set; }

        public int? IssueNo { get; set; }

        [Required(ErrorMessage = "Distribution Group is required")]
        public Int64? DistributionGroupId { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }
        public string RoleGroup { get; set; }
        public string DINNoGenratorPrefix { get; set; }

        public string DINAcknowledgeNote { get; set; }
        public Int64? FolderId { get; set; }
        public Int64? BUId { get; set; }

        [Required(ErrorMessage = "Description is required")]
        public string Description { get; set; }


        public string Department { get; set; }

        public Int64? DINAckID { get; set; }

        public string ACKIUrl { get; set; }

        public bool IsLatestIssue { get; set; }

        public string CurrentPsNo { get; set; }

        public List<string> _lstPsNameSplit { get; set; }
        public string DocumentNo { get; set; }
    }

    public class DepartmentName
    {
        public string deptName { get; set; }
        public List<PsNameDeptWise> _lstPsName { get; set; }
    }

    public class PsNameDeptWise
    {
        public string PsName { get; set; }
    }

    public class DINRefDocumentModel : DINModel
    {
        public Int64? DINRefDocumentId { get; set; }

        public string DocumentNo { get; set; }

        public Int64? DocumentId { get; set; }
        public string GeneralRemarks { get; set; }
        public string Status { get; set; }
        public List<DocumentId> DocumentIdList { get; set; }
    }

    public class DocumentId
    {
        public Int64? DocId { get; set; }
    }

    public class DINDistributeDocument
    {
        public Int64? DINDistributeDOCId { get; set; }

        public Int64? DINId { get; set; }

        public Int64? JEPDocumentDetailsId { get; set; }

        public string Department { get; set; }

        public int DeptWiseDOCCopys { get; set; }

        public bool IsElectronicCopys { get; set; }

        public string DepartmentName { get; set; }

        public string DocuemntNo { get; set; }

        public int? DocumentRev { get; set; }

        public string GetFirstPsNumber { get; set; }

        //    public List<DistDepartment> Departments { get; set; }
    }

    public class DistDepartment
    {
        public string DepartmentId { get; set; }
        public string Department { get; set; }

        public Int64? DINGroupDeptId { get; set; }

        public List<string> _lstPsName { get; set; }

    }
}
