﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQS.DESCore
{
    public class DOCLifeCycleModel
    {
        public Int64? DocumentLif { get; set; }
        public Int64? DocumentId { get; set; }
        public string PolicyStep { get; set; }
        public int PolicyOrderID { get; set; }
        public bool IsCompleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }

    }
}
