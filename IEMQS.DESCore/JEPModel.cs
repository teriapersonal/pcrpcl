﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace IEMQS.DESCore
{
    /// <summary>
    /// DES074 Table Model
    /// Sagar Tajpara 03-12-2019
    /// </summary>
    public class JEPModel
    {
        public Int64? JEPId { get; set; }
        public string JEPNumber { get; set; }
        public string Customer { get; set; }
        public string CustomerPONo { get; set; }
        public string Licensor { get; set; }
        public string ContractNo { get; set; }
        public string Project { get; set; }
        public string ProjectDescription { get; set; }
        public string Purchaser { get; set; }
        public bool MilestoneDocument { get; set; }
        public string MRNo { get; set; }
        public string ProjectManager { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }
        public string Status { get; set; }

        public string Owner { get; set; }
        public string Department { get; set; }
        public string RoleGroup { get; set; }
        public long? BUId { get; set; }
        public long? FolderId { get; set; }

        public int? Authorized { get; set; }
        public List<JEPDocumentModel> JEPDocumentList { get; set; }
        public bool IsUpdateChanges { get; set; }
        public string JEPDescription { get; set; }

        public Nullable<bool> IsJEPCheckIn { get; set; }
        public string JEPCheckInBy { get; set; }
        public Nullable<System.DateTime> JEPCheckInOn { get; set; }

        public string JEPCheckInByName { get; set; }
        public bool IsDEENGGUSer { get; set; }
    }

    public class JEPDocumentModel
    {
        public Int64? JEPDocumentDetailsId { get; set; }
        public Int64? JEPId { get; set; }
        public string DocNo { get; set; }
        public string DocDescription { get; set; }
        public int RevisionNo { get; set; }
        public string CustomerDocNo { get; set; }

        public string CustomerDocRevision { get; set; }
        public Int64? DocumentTypeId { get; set; }
        public string Department { get; set; }
        public DateTime? PlannedDate { get; set; }
        public string Weightage { get; set; }
        public Int64? DocumentId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }

        public string Rolename { get; set; }

        public bool MilestoneDocument { get; set; }
    }

    public class JEPProjectwiseDocument
    {
        public string TemplateName { get; set; }

        public Int64? JEPTemplateId { get; set; }
    }

    public class JEPGlobalDoc
    {
        public Int64? JEPId { get; set; }

        public List<JEPGlobalDocument> JEPDocumentList { get; set; }
    }

    public class JEPGlobalDocument
    {
        public Int64? JEPDocListId { get; set; }

    }


    public class CustomerFeedback
    {
        public string CurrentLocationIp { get; set; }
        public string FilePath { get; set; }
        public Int64? JEPId { get; set; }
        public string JEPNumber { get; set; }
        public string Customer { get; set; }
        public string GroupId { get; set; }
        public string CustomerPONo { get; set; }
        public string Licensor { get; set; }
        public string ContractNo { get; set; }
        public string Project { get; set; }
        public string ProjectDescription { get; set; }
        public string Purchaser { get; set; }
        public string MRNo { get; set; }
        public string ProjectManager { get; set; }
        public long? JEPCustomerFeedbackId { get; set; }
        public string JEPCustomerFeedbackType { get; set; }
        [Required(ErrorMessage = "Document Number is Required")]
        public long? JEPDocumentDetailsId { get; set; }
        public string DocumentNo { get; set; }
        public Int64? DocumentId { get; set; }
        public long JEPCustomerFeedbackDocumentId { get; set; }

        [Required(ErrorMessage = "Agency is Required")]
        public long? AgencyId { get; set; }

        [Required(ErrorMessage = "Receipt Date is Required")]
        public System.DateTime? ReceiptDate { get; set; }
        public string ReceiptDateStr { get; set; }

        //[Required(ErrorMessage = "Agency Name is Required")]
        //public string ApprovalCode { get; set; }

        [Required(ErrorMessage = "Approval Code is Required")]
        public Int64? ApprovalId { get; set; }
        [Required(ErrorMessage = "Tranmittal Date is Required")]
        public System.DateTime? RefTransmittalDate { get; set; }
        //[RegularExpression("^[a-zA-Z0-9]*$", ErrorMessage = "Only Alphabets and Numbers allowed.")]
        public string RefTransmittalNo { get; set; }

        public string RefTransmittalDateStr { get; set; }
        public string Remark { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime? CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public System.DateTime? EditedOn { get; set; }
        public string AgencyKeyType { get; set; }
        public string DocNo { get; set; }
        public long? JEPCustFeedbackDocumentId { get; set; }
        public string DocumentTitle { get; set; }
        public string DocumentFormat { get; set; }
        public string DocumentPath { get; set; }
        public List<DocumentMapping> DocMapping { get; set; }
        public int? Authorized { get; set; }
        public string RoleGroup { get; set; }
        public long? FolderId { get; set; }
        public string Department { get; set; }
        public bool IsUpdateChanges { get; set; }
        public List<CustomerFeedback> JEPCustFeedbackList { get; set; }
        public bool IsDetail { get; set; }

        public string ReturnTransmittalNo { get; set; }
        public int RevisionNo { get; set; }
        public string RevisionNostr { get; set; }
        public string ActualSubDate { get; set; }
        public string PlannedSubDate { get; set; }
        public string CustomerDocNo { get; set; }
    }
    public class DocumentMapping
    {
        public Int64? JEPQueryStringValue { get; set; }
        public string CurrentLocationIp { get; set; }
        public string AddToLocationIp { get; set; }
        public Int64? JEPCustomerFeedbackId { get; set; }
        public string GroupId { get; set; }
        public string DocumentTitle { get; set; }
        public string DocumentFormat { get; set; }
        public string DocumentPath { get; set; }
        public Int64? JEPId { get; set; }

        public int FileLocation { get; set; }
    }

    public class Agencies
    {
        [Required(ErrorMessage = "Agency is Required")]
        public Int64? AgencyId { get; set; }
        public string AgencyName { get; set; }
        public string AgencyDescription { get; set; }
        public Int64? BUId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }
    }

    public class ReturnRemark
    {
        public string ReturnNotes { get; set; }
        public Int64 JEPId { get; set; }
    }

    public class JEPBulkUpload
    {
        public Int64 TempDocId { get; set; }
        public string Project { get; set; }
        public Int64 JEPId { get; set; }
        public string JEPNumber { get; set; }
        public string DocNo { get; set; }

        public Int64 DocTypeId { get; set; }
        public int DocRevision { get; set; }
        public string DocDescription { get; set; }
        public string SrcFolder { get; set; }
        public string FileName { get; set; }
        public string FileFormat { get; set; }
        public string DestFolder { get; set; }
        public string CustomerDocNo { get; set; }
        public string CustomerRevNo { get; set; }
        public string Department { get; set; }
        public DateTime PlannedDate { get; set; }
        public string Weightage { get; set; }
        public bool Milestone { get; set; }
        public Int64 PolicyId { get; set; }
        public int Location { get; set; }
        public Int64 FolderId { get; set; }
        public string ClientVersion { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string RoleGroup { get; set; }
        public bool IsLatestVersion { get; set; }

        public string ReturnNotes { get; set; }
        public string VersionGUID { get; set; }
        public int FileLocation { get; set; }
        public string EmailTo { get; set; }
        public int IsUploaded { get; set; }
    }


    public class TempJEPDocumentModel
    {
    
        public string DocumentTypeName { get; set; }
        public string DocNo { get; set; }
        public int RevisionNo { get; set; }
        public string DocDescription { get; set; }
        public string SourceFolder { get; set; }
        public string FileName { get; set; }

        public string DestinationFolder { get; set; }

        public string CustomerDocNo { get; set; }

        public string CustomerDocRevision { get; set; }
        public DateTime? PlannedDate { get; set; }
        public string Department { get; set; }
        public bool MilestoneDocument { get; set; }
        public string ReturnNotes { get; set; }

    }
}

