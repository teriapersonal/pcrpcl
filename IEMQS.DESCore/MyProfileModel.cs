﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQS.DESCore
{
    /// <summary>
    /// Model
    /// Ravi Patel 18-11-2019
    /// </summary>
    public class MyProfileModel
    {
        public string FullName { get; set; }
        public string Emailid { get; set; }
        public string Image { get; set; }
        public string Designation { get; set; }
        public string CompanyName { get; set; }
        public string PhoneNo { get; set; }
        public string WorkNo { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostCode { get; set; }
    }
}
