﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace IEMQS.DESCore
{
    /// <summary>
    /// DES033 Table Model
    /// Ravi Patel 09-12-2019
    /// </summary>

    public class DINGroupModel
    {
        public Int64? DINGroupId { get; set; }

        [Remote("CheckGroupNameAlreadyExist", "DepartmentGroup", AdditionalFields = "DINGroupId", HttpMethod = "POST", ErrorMessage = "Group name already exist.")]
        [Required(ErrorMessage = "Group name is required")]
        public string GroupName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }
        public string Project { get; set; }
        public string CopyGroupName { get; set; }
        public List<DINGroupDepartmentModel> DepartmentGroupList { get; set; }
    }


    /// <summary>
    /// DES034 Table Model
    /// Ravi Patel 09-12-2019
    /// </summary>
    public class DINGroupDepartmentModel
    {
        public Int64? DINGroupDeptId { get; set; }
        public Int64? DINGroupId { get; set; }
        public string DeptId { get; set; }
        public bool IsDelete { get; set; }
        public List<string> PSNoList { get; set; }
        public List<SelectListItem> FunctionList { get; set; }
        public List<SelectListItem> PSNumberList { get; set; }
    }

    /// <summary>
    /// DES035 Table Model
    /// Ravi Patel 09-12-2019
    /// </summary>
    public class GroupPersonDetailsModel
    {
        public Int64? DINGroupUserId { get; set; }
        public Int64? DINGroupDeptId { get; set; }
        public string PSNumber { get; set; }
        //public bool IsDelete { get; set; }
    }
}
