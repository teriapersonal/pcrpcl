﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace IEMQS.DESCore
{
    public class ProjectModel
    {

        public string WarrantyTerms { get; set; }

        public DateTime? WarrantyExpiryDate { get; set; }
        public string JEPNumber { get; set; }

        public string Location { get; set; }
        [Required]
        public string Project { get; set; }
        public string MainProject { get; set; }

        [Required]
        public string Contract { get; set; }
        public string ContractName { get; set; }
        public string ProjectDescription { get; set; }
        public string BUCode { get; set; }
        public string BU { get; set; }
        public string PBU { get; set; }

        public string Customer { get; set; }
        public string CustomerLOINumber { get; set; }

        public string CustomerPONumber { get; set; }

        public string Owner { get; set; }

        public string ProductType { get; set; }
        [Required(ErrorMessage = "Consultant is Required")]
        public string Consultant { get; set; }

        public string Licensor { get; set; }
        public string DesignCode { get; set; }
        public string CodeStamp { get; set; }
        public string RegulatoryRequirement { get; set; }

        [Required(ErrorMessage = "Major Material is Required")]
        public string MajorMaterial { get; set; }
        public string EquipmentDiameter { get; set; }

        [Required(ErrorMessage = "TLtoTL is required")]
        public string TLtoTL { get; set; }
        public string FabricatedWeight { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? LOIDate { get; set; }
        public DateTime? PIMDate { get; set; }

        public DateTime? PODate { get; set; }

        public DateTime? ContractualDeliveryDate { get; set; }
        public string SiteWorkRequire { get; set; }
        public string DeliveryTerms { get; set; }
        public string DeliveryCondition { get; set; }

        [Required(ErrorMessage = "Product End User is Required")]
        public string ProductEndUser { get; set; }
        public DateTime? ZeroDate { get; set; }
        public string EnquiryName { get; set; }
        public string OrderCategory { get; set; }

        public string Status { get; set; }
        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }

        public string CompletedBy { get; set; }
        public DateTime? CompletedOn { get; set; }
        public string InProcessBy { get; set; }
        public DateTime? InProcessOn { get; set; }

        public string Description { get; set; }
        [Required(ErrorMessage = "Equipment No is required")]
        public string EquipmentNo { get; set; }

        [Required(ErrorMessage = "Equipment Name is required")]
        public string EquipmentName { get; set; }

        [Required(ErrorMessage = "ASME Tag is required")]
        public bool? ASMETag { get; set; }

        [Required(ErrorMessage = "Production Center is required")]
        public string ProductionCenter { get; set; }

        [Required(ErrorMessage = "Design Group is required")]
        public string DesignGroup { get; set; }

        public string CustomerForDINReport { get; set; }

        [Required(ErrorMessage = "Drawing Series Number is required")]
        [Display(Name = "Drawing Series Number")]
        [Remote("IsAlreadyExistDrawingSeriesNo", "Project", AdditionalFields = "DrawingSeriesNo", HttpMethod = "POST", ErrorMessage = "Drawing Series Number Already Exist")]
        public string DrawingSeriesNo { get; set; }

        public string ProjectPMG { get; set; }

        [Required(ErrorMessage = "Project Manager is required")]
        public string ProjectPMGPsNo { get; set; }
        public bool ISJEPCreated { get; set; }

        public string CreatedByPsno { get; set; }
        public string Edition { get; set; }
    }

    public class ProjectFolderModel
    {
        public string Contract { get; set; }
        public string ProjectNo { get; set; }
        public string BUCode { get; set; }
        public Int64? BUId { get; set; }
        public Int64? FolderId { get; set; }
        public bool IsInprocess { get; set; }
        public bool IsProjectRight { get; set; }
        public List<Data.SP_DES_GET_PROJECT_FOLDERLIST_Result> FolderList { get; set; }
        public List<CategoryActionModel> ListCategory { get; set; }
        public List<CategoryActionModel> ListAction { get; set; }
        public string RoleGroup { get; set; }
    }

    public class FolderListModel
    {
        public Int64? ProjectFolderId { get; set; }
        public Int64? ParentProjectFolderId { get; set; }

        [Required(ErrorMessage = "Folder Name is required")]
        [Display(Name = "Folder Name")]
        [Remote("IsAlreadyExist", "Project", AdditionalFields = "ParentProjectFolderId,ProjectNo", HttpMethod = "POST", ErrorMessage = "Folder Name Already Exist.")]
        public string FolderName { get; set; }

        [Required(ErrorMessage = "Folder Order No is required")]
        public double FolderOrderNo { get; set; }

        public string ProjectNo { get; set; }
        public bool IsSystemGenerated { get; set; }

        public string Description { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }
        public List<CategoryActionModel> ListCategory { get; set; }
        public List<CategoryActionModel> ListAction { get; set; }
        public string CategoryIds { get; set; }
        public string ActionIds { get; set; }
        public string FunctionIds { get; set; }

        public List<string> ListFunction { get; set; }

        public bool IsViewFolder { get; set; }
        public bool IsWriteFolder { get; set; }
    }

    public class ProjectUserFolderAuthorizeModel
    {
        public long ProjectUserFolderAuthorizeId { get; set; }
        public long? ProjectFolderId { get; set; }
        public string PSNumber { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }
        public string Project { get; set; }
        public string EmailText { get; set; }
        public string RequestStatus { get; set; }
        public string Location { get; set; }

        public string FunctionId { get; set; }

        public long? BUID { get; set; }
    }

    public class MasterMenu
    {
        public int Id { get; set; }
        public string Process { get; set; }
        public string Description { get; set; }
        public string Area { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public int? ParentId { get; set; }
        public int? MainMenuOrderNo { get; set; }
        public int? OrderNo { get; set; }
    }

    public class FindObjectModel
    {
        public string Id { get; set; }

        public Int64 PKId { get; set; }
        public bool IsExist { get; set; }
        public string ProjectNo { get; set; }

        public long JEPId { get; set; }
    }
}
