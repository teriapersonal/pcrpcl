//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQS.DESCore.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class DES077
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DES077()
        {
            this.DES080 = new HashSet<DES080>();
        }
    
        public long AgencyDocListId { get; set; }
        public long AgencyDocId { get; set; }
        public long JEPDocumentDetailsId { get; set; }
        public string ClientDocNo { get; set; }
        public string ClientDocRev { get; set; }
        public Nullable<System.DateTime> PlannedSubmitDate { get; set; }
        public Nullable<System.DateTime> PlannedApprovalDate { get; set; }
        public Nullable<double> Weightage { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DES080> DES080 { get; set; }
        public virtual DES076 DES076 { get; set; }
        public virtual DES075 DES075 { get; set; }
    }
}
