//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQS.DESCore.Data
{
    using System;
    
    public partial class SP_DES_GET_ROCCommentList_Result
    {
        public long ROCCommentsId { get; set; }
        public Nullable<long> ROCId { get; set; }
        public string Status { get; set; }
        public Nullable<double> SrNo { get; set; }
        public Nullable<long> DocumentId { get; set; }
        public string CommentLocation { get; set; }
        public string AgencyComments { get; set; }
        public string AgencyCommentsDate { get; set; }
        public string LandTComment { get; set; }
        public string LandTCommentDate { get; set; }
        public string ClientDocNo { get; set; }
        public string ClientDocRev { get; set; }
        public string AgencyResponse { get; set; }
        public string AgencyResponseDate { get; set; }
        public string CustomerSrNo { get; set; }
        public string CommfromCustDepts { get; set; }
        public string DocumentFormat { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public string EditedOn { get; set; }
        public string DocumentNo { get; set; }
        public string DocumentTitle { get; set; }
        public string DocumentRevision { get; set; }
        public Nullable<int> TableCount { get; set; }
    }
}
