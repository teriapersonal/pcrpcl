//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQS.DESCore.Data
{
    using System;
    
    public partial class SP_DES_GET_DCR_OBJECT_FILE_FOR_TRANSFER_Result
    {
        public long DCRSupportedDocId { get; set; }
        public string DocumentPath { get; set; }
        public string DocumentTitle { get; set; }
        public Nullable<int> FileLocation { get; set; }
    }
}
