//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQS.DESCore.Data
{
    using System;
    
    public partial class SP_DES_GET_GENERATE_TRANSMITTAL_Result
    {
        public long TransmittalId { get; set; }
        public Nullable<long> AgencyId { get; set; }
        public string AgencyName { get; set; }
        public Nullable<long> FolderId { get; set; }
        public string Status { get; set; }
        public string TransmittalNo { get; set; }
        public Nullable<int> Authorized { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public string EditedOn { get; set; }
        public Nullable<int> TableCount { get; set; }
    }
}
