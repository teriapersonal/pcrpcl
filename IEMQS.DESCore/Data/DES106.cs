//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQS.DESCore.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class DES106
    {
        public long Central_IndexStatusId { get; set; }
        public Nullable<long> JEPCustFeedbackDocumentId { get; set; }
        public Nullable<bool> Central { get; set; }
        public Nullable<bool> IsIndex { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<long> EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public Nullable<bool> isDeleted { get; set; }
        public Nullable<long> DCRSupportedDocId { get; set; }
        public Nullable<long> ItemId { get; set; }
        public string Project { get; set; }
        public Nullable<long> ROCCommentedDocId { get; set; }
        public Nullable<long> ROCCommentsAttachId { get; set; }
        public Nullable<long> DocumentMappingId { get; set; }
        public Nullable<long> JEPID { get; set; }
        public Nullable<long> DINId { get; set; }
        public Nullable<long> ROCCommentsId { get; set; }
        public string SolrObjectId { get; set; }
    }
}
