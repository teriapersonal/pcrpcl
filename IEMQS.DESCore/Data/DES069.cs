//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQS.DESCore.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class DES069
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DES069()
        {
            this.DES070 = new HashSet<DES070>();
        }
    
        public long ROCId { get; set; }
        public string EquipmentNo { get; set; }
        public string ROCRefNo { get; set; }
        public string Project { get; set; }
        public Nullable<int> Version { get; set; }
        public Nullable<long> AgencyId { get; set; }
        public string DocNo { get; set; }
        public string DocTitle { get; set; }
        public Nullable<int> DocRev { get; set; }
        public Nullable<System.DateTime> SubmittedDate { get; set; }
        public Nullable<long> DocumentId { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public Nullable<long> FolderId { get; set; }
        public Nullable<bool> IsLatest { get; set; }
        public Nullable<bool> IsROCCheckIn { get; set; }
        public string ROCCheckInBy { get; set; }
        public Nullable<System.DateTime> ROCCheckInOn { get; set; }
    
        public virtual DES027 DES027 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DES070> DES070 { get; set; }
        public virtual DES059 DES059 { get; set; }
        public virtual DES051 DES051 { get; set; }
    }
}
