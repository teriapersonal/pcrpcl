//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQS.DESCore.Data
{
    using System;
    
    public partial class SP_DES_GET_FUNCTION_ROLE_Result
    {
        public long FunctionRoleMappingId { get; set; }
        public string FunctionId { get; set; }
        public Nullable<int> RoleId { get; set; }
        public string t_desc { get; set; }
        public string Rolename { get; set; }
        public Nullable<int> TableCount { get; set; }
    }
}
