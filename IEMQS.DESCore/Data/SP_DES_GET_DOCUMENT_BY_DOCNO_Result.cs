//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQS.DESCore.Data
{
    using System;
    
    public partial class SP_DES_GET_DOCUMENT_BY_DOCNO_Result
    {
        public long DocumentId { get; set; }
        public string Project { get; set; }
        public string DocumnetTypeName { get; set; }
        public string DocumentNo { get; set; }
        public string DocumentTitle { get; set; }
        public Nullable<int> DocumentRevision { get; set; }
        public string ClientDocumentNumber { get; set; }
        public Nullable<bool> MilestoneDocument { get; set; }
        public string PolicyName { get; set; }
        public Nullable<bool> IsJEP { get; set; }
        public Nullable<int> Location { get; set; }
        public string Status { get; set; }
        public Nullable<long> FolderId { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public string EditBy { get; set; }
        public string EditedOn { get; set; }
        public Nullable<int> TableCount { get; set; }
    }
}
