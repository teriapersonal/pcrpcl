//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQS.DESCore.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class DES011
    {
        public long IPRangeId { get; set; }
        public string VlenName { get; set; }
        public string IP_SubnetMask { get; set; }
        public string IPStart { get; set; }
        public string IPEnd { get; set; }
        public Nullable<int> Location { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
    }
}
