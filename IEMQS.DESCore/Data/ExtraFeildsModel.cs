﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQS.DESCore.Data
{
    #region Kishan
    public partial class SP_DES_GET_PART_SHEET_Result
    {
        public string QString { get; set; }

    }
    public partial class SP_DES_GET_ITEM_BOMLIST_Result
    {
        public string QString { get; set; }
        public string BString { get; set; }
    }

    public partial class SP_DES_GET_CHILDITEM_BOM_Result
    {
        public string QString { get; set; }

    }

    public partial class SP_DES_GET_PARENTITEM_BOM_Result
    {
        public string QString { get; set; }

    }
    #endregion

    #region Ravi Patel
    public partial class SP_DES_GET_TRANSMITTAL_EMAIL_CONFIGURATION_Result
    {
        public string QString { get; set; }

    }

    public partial class SP_DES_GET_GLOBAL_JEP_TEMPLATE_Result
    {
        public string QString { get; set; }
        public string JEPTemplateCheckInByName { get; set; }
    }

    public partial class SP_DES_GET_JEP_TEMPLATE_Result
    {
        public string QString { get; set; }
        public string JEPTemplateCheckInByName { get; set; }
    }
    #endregion


    #region Sagar Tajpara | JEP

    public partial class SP_DES_GET_JEPLISTPROJECTWISE_Result
    {
        public string GString { get; set; }
        public string QString { get; set; }

        public string JEPCheckInByName { get; set; }
    }

    #endregion


    #region M M | DOC,DIN

    public partial class SP_DES_GET_DOCUMENT_Result
    {
        public string QString { get; set; }
    }

    public partial class SP_DES_GET_DIN_Result
    {
        public string QString { get; set; }
    }


    public partial class SP_DES_GET_DOCUMENT_BY_DOCNO_Result
    {
        public string QString { get; set; }
    }

    #endregion




    #region Darpan | ROC ,Tranmittal

    public partial class SP_DES_GET_ROC_Result
    {
        public string QString { get; set; }
    }

    public partial class SP_DES_GET_ROCCommentedDOC_Result
    {
        public string QString { get; set; }
    }

    public partial class SP_DES_GET_ROC_BY_ROCID_Result
    {
        public string QString { get; set; }
    }


    public partial class SP_DES_GET_TRANSMITTAL_AGENCY_Result
    {
        public string QString { get; set; }
        public string GString { get; set; }

        public string AgencyCheckInByName { get; set; }
    }

    public partial class SP_DES_GET_GENERATE_TRANSMITTAL_Result
    {
        public string QString { get; set; }
        public string GString { get; set; }
    }
    public partial class SP_DES_GET_PARTLIST_BYPROJECT_Result
    {
        public string QString { get; set; }
        public string BString { get; set; }
    }
    public partial class SP_DES_GET_PARTLIST_BYITEMKEYS_Result
    {
        public string QString { get; set; }
        public string BString { get; set; }
    }


    public partial class SP_DES_GET_RELATEDPARTLIST_BYPROJECT_Result
    {
        public string QString { get; set; }
        public string BString { get; set; }
    }

    public partial class SP_DES_GET_PARTBOMLIST_BYPROJECT_Result
    {
        public string QString { get; set; }
        public string BString { get; set; }
    }




    #endregion

    #region YASH Shah | JEPCustomerFeedback
    public partial class SP_DES_GET_ALL_CUSTOMER_FEEDBACK_LIST_Result
    {
        public string GString { get; set; }
        public string QString { get; set; }
    }

    public partial class SP_DES_GET_DCR_LIST_Result
    {
        public string QString { get; set; }
    }

    public partial class SP_DES_GET_DCR_PENDINGTASK_LIST_Result
    {
        public string QString { get; set; }
    }
    public partial class SP_DES_GET_PENDING_DIN_Result
    {
        public string QString { get; set; }
    }



    
    #endregion

    #region Niki Mehta
    public partial class SP_DES_GET_ALL_OBJECTS_BY_FOLDERID_PROJECT_Result
    {
        public string QString { get; set; }
        public string GString { get; set; }
    }
    public partial class SP_DES_GET_PART_POWERVIEW_Result
    {
        public List<BOMModel> ListBOMModel { get; set; }

        #region added by niki
        public string SignalCode { get; set; }
        public string SelectionCode { get; set; }
        public string ProductClass { get; set; }
        public string ProductLine { get; set; }
        public string ARMDescription { get; set; }
        public string ARMText { get; set; }
        #endregion
    }

    public partial class SP_DES_GENERATE_XML_Result
    {
        public List<BOMModel> ListBOMModel { get; set; }

        #region added by niki
        public string SignalCode { get; set; }
        public string SelectionCode { get; set; }
        public string ProductClass { get; set; }
        public string ProductLine { get; set; }
        public string ARMDescription { get; set; }
        public string ARMText { get; set; }
        #endregion
    }

    #endregion

    public partial class SP_DES_GET_DCR_AFFECTED_DOC_Result
    {
        public string QString { get; set; }
    }

    public partial class SP_DES_GET_DCR_AFFECTED_PART_Result
    {
        public string QString { get; set; }
    }

    public partial class SP_DES_GET_DIN_JepDocumentDetails_Result
    {
        public string QString { get; set; }
    }

    public partial class SP_DES_GET_PART_REVISION_Result
    {
        public string QString { get; set; }
    }
}
