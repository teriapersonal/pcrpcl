//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQS.DESCore.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class DES111
    {
        public long AnnotationId { get; set; }
        public Nullable<long> FileId { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public Nullable<int> PageNo { get; set; }
        public string ID { get; set; }
        public string Type { get; set; }
        public Nullable<double> ParentWidth { get; set; }
        public Nullable<double> ParentHeight { get; set; }
        public Nullable<double> ParentLeft { get; set; }
        public Nullable<double> ParentTop { get; set; }
        public Nullable<double> ScrollTop { get; set; }
        public Nullable<double> ScrollLeft { get; set; }
        public Nullable<double> Left { get; set; }
        public Nullable<double> Top { get; set; }
        public Nullable<double> Width { get; set; }
        public Nullable<double> Height { get; set; }
        public string BackColor { get; set; }
        public string BorderColor { get; set; }
        public Nullable<double> BorderWidth { get; set; }
        public string Title { get; set; }
        public string Note { get; set; }
        public Nullable<double> Opacity { get; set; }
        public Nullable<double> Rotate { get; set; }
        public Nullable<bool> ShowTitle { get; set; }
        public Nullable<bool> ShowBorder { get; set; }
        public Nullable<bool> ShowNote { get; set; }
        public string TextAlign { get; set; }
        public Nullable<bool> CanRotate { get; set; }
        public Nullable<bool> Burn { get; set; }
        public Nullable<bool> Locked { get; set; }
        public Nullable<double> ResizeRatio { get; set; }
        public string TitleColor { get; set; }
        public Nullable<double> TitleFontSize { get; set; }
        public string Author { get; set; }
        public Nullable<bool> LineVertical { get; set; }
        public string ArrowDirection { get; set; }
        public string FreeHandData { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string AttachFileName { get; set; }
        public string AttachFilePath { get; set; }
        public string AnnotationNo { get; set; }
    }
}
