//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQS.DESCore.Data
{
    using System;
    
    public partial class SP_DES_GET_CHILDITEM_BOM_Result
    {
        public long ItemId { get; set; }
        public string Project { get; set; }
        public string ItemKeyType { get; set; }
        public string OrderPolicy { get; set; }
        public string ProductForm { get; set; }
        public string Type { get; set; }
        public string ItemKey { get; set; }
        public string DRGNoDocumentNo { get; set; }
        public string ProcurementDrgDocumentNo { get; set; }
        public string PerentItemKey { get; set; }
        public string PolicyName { get; set; }
        public string Material { get; set; }
        public string ItemGroup { get; set; }
        public string ItemName { get; set; }
        public string String2 { get; set; }
        public string String3 { get; set; }
        public string String4 { get; set; }
        public string ARMSet { get; set; }
        public string ARMRev { get; set; }
        public string SizeCode { get; set; }
        public string CRSYN { get; set; }
        public string Status { get; set; }
        public Nullable<double> ItemWeight { get; set; }
        public Nullable<double> BOMWeight { get; set; }
        public Nullable<double> Thickness { get; set; }
        public Nullable<double> CladPlatePartThickness1 { get; set; }
        public string IsDoubleClad { get; set; }
        public Nullable<double> CladPlatePartThickness2 { get; set; }
        public Nullable<double> CladSpecificGravity2 { get; set; }
        public Nullable<double> JobQty { get; set; }
        public Nullable<double> CommissioningSpareQty { get; set; }
        public Nullable<double> MandatorySpareQty { get; set; }
        public Nullable<double> OperationSpareQty { get; set; }
        public Nullable<double> ExtraQty { get; set; }
        public string Remarks { get; set; }
        public Nullable<double> Quantity { get; set; }
        public Nullable<double> Length { get; set; }
        public Nullable<double> Width { get; set; }
        public Nullable<int> NumberOfPieces { get; set; }
        public string BomReportSize { get; set; }
        public string DrawingBomSize { get; set; }
        public Nullable<int> RevNo { get; set; }
        public Nullable<long> BOMId { get; set; }
        public Nullable<bool> IsFromLN { get; set; }
        public Nullable<bool> IsFromFKMS { get; set; }
        public string UOM { get; set; }
        public Nullable<int> FindNumber { get; set; }
        public string ExtFindNumber { get; set; }
        public string FindNumberDescription { get; set; }
        public string ItemState { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public string EditedOn { get; set; }
        public Nullable<int> TableCount { get; set; }
    }
}
