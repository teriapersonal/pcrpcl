//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IEMQS.DESCore.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class DES057
    {
        public long ProjectFolderCategoryActionId { get; set; }
        public Nullable<long> ProjectFolderId { get; set; }
        public Nullable<long> MappingId { get; set; }
        public Nullable<int> TypeId { get; set; }
    
        public virtual DES054 DES054 { get; set; }
        public virtual DES002 DES002 { get; set; }
    }
}
