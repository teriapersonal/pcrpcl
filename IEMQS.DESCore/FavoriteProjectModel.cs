﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQS.DESCore
{
    /// <summary>
    /// DES052 Table Model
    /// Ravi Patel 20-11-2019
    /// </summary>
    public class FavoriteProjectModel
    {
        public Int64? FavoriteId { get; set; }
        public string Project { get; set; }
        public string PSNumber { get; set; }
        public bool IsFavorite { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }
    }
}
