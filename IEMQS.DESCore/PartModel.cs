﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace IEMQS.DESCore
{
    public class PartModel
    {
        public BOMModel BOMModel { get; set; }

        [Required(ErrorMessage = "Item Id is required")]
        public Int64? ItemId { get; set; }
        public List<SelectListItem> ItemList { get; set; }
        public string Project { get; set; }
        public string Contract { get; set; }

        //[Remote("CheckItemKeyIsvalid", "Part", HttpMethod = "POST", ErrorMessage = "Item Key already exist.")]
        [Required(ErrorMessage = "Item Key Type  is required")]
        public string ItemKeyType { get; set; }

        [Required(ErrorMessage = "Order Policy  is required")]
        public string OrderPolicy { get; set; }

        [Required(ErrorMessage = "Product Form Code  is required")]
        public Int64? ProductFormCodeId { get; set; }
        public string ProductFormCodeName { get; set; }

        [Required(ErrorMessage = "Type is required")]
        public string Type { get; set; }

        public string ItemKey { get; set; }

        public Int64? PolicyId { get; set; }

        [Required(ErrorMessage = "Policy is required")]
        public string Policy { get; set; }
        public bool ReviseWithBOM { get; set; }

        [Required(ErrorMessage = "Material is required")]
        public string Material { get; set; }
        public string MaterialDesc { get; set; }
        public List<SelectListItem> MaterialList { get; set; }

        [Required(ErrorMessage = "Item Group is required")]
        public string ItemGroup { get; set; }
        public string ItemGroupDesc { get; set; }
        public List<SelectListItem> ItemGroupList { get; set; }
        public string UOM { get; set; }


        [Required(ErrorMessage = "Item Description is required")]
        public string ItemName { get; set; }
        public string String2 { get; set; }
        public string String3 { get; set; }
        public string String4 { get; set; }

        public string HdnString2 { get; set; }
        public string HdnString3 { get; set; }
        public string HdnString4 { get; set; }


        public string ARMSet { get; set; }
        public string ARMSetDesc { get; set; }
        public List<SelectListItem> ARMSetList { get; set; }
        public string ARMRev { get; set; }
        public string SizeCode { get; set; }
        public List<SelectListItem> SizeCodeList { get; set; }
        public string CRSYN { get; set; }
        public int? RevNo { get; set; }
        public string Department { get; set; }
        public string RoleGroup { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }
        public Int64? FolderId { get; set; }

        [Required(ErrorMessage = "Thickness is required")]
        public double? Thickness { get; set; }

        [Required(ErrorMessage = "Clad Plate Part Thickness1 is required")]
        public double? CladPlatePartThickness1 { get; set; }

        //New Feild
        [Required(ErrorMessage = "Clad Specific Gravity1 is required")]
        public double? CladSpecificGravity1 { get; set; }

        [Required(ErrorMessage = "Is Double Clad is required")]
        public string IsDoubleClad { get; set; }

        [Required(ErrorMessage = "Clad Plate Part Thickness2 is required")]
        public double? CladPlatePartThickness2 { get; set; }

        [Required(ErrorMessage = "Clad Specific Gravity2 is required")]
        public double? CladSpecificGravity2 { get; set; }
        public double? ItemWeight { get; set; }
        public bool IsFromLN { get; set; }
        public bool IsFromFKMS { get; set; }
        public string ProductForm { get; set; }
        public double? Density { get; set; }
        public double? WeightFactor { get; set; }
        public int? ManufactureItem { get; set; }
        public Int64? ProcurementDrgDocumentId { get; set; }
        public List<SelectListItem> ProcurementDRGNoList { get; set; }
        public string ProcurementDrgDocumentNo { get; set; }
        public string ProductFormBOMReportSize { get; set; }
        public string ProductFormDrawingBOMSize { get; set; }
        public bool DoNotReferExistingItemkey { get; set; }
        public bool DoNotCheckProductFormCode { get; set; }
        public string ActionForPart { get; set; }
        public bool ConfirmRevision { get; set; }

        public string HidenItemGrouop { get; set; }
    }

    public class DDLProject
    {
        public string Project { get; set; }
        public string ProjectDesc { get; set; }
    }

    public class DDLManufactureItem
    {
        public long ItemId { get; set; }
        public string ItemKey { get; set; }
    }

    public class PartCopyModel
    {
        public string Project { get; set; }

        public Int64? ItemSheetId { get; set; }
        public string listItemIds { get; set; }
    }

    public class CopyPartModel
    {
        public string Project { get; set; }
        public string ManufactureItem { get; set; }
        public string ContractProject { get; set; }
        public string ManufactureItemProjectWise { get; set; }
    }

    public class PartGroupModel
    {

        [Required(ErrorMessage = "Project is required")]
        public string Project { get; set; }

        [Required(ErrorMessage = "Manufacture Item is required")]
        public Int64? ManufactureItem { get; set; }
        public Int64? FolderId { get; set; }
        public string Department { get; set; }
        public string PsNo { get; set; }
        public string RoleGroup { get; set; }
        public List<CopyPartGroupDepartmentModel> CopyPartGroupList { get; set; }
    }

    public class CopyPartGroupDepartmentModel
    {
        public string ProjectId { get; set; }
        public Int64? ManufactureItemId { get; set; }
        public List<SelectListItem> ProjectList { get; set; }
        public List<SelectListItem> ManufactureItemList { get; set; }
    }
    public class PartCopy
    {
        public long ItemId { get; set; }
    }

    public class BOMModel
    {
        public DOCModel DOCModel { get; set; }

        public Int64? BOMId { get; set; }
        public Int64? ItemId { get; set; }

        [Required(ErrorMessage = "Parent Item Id is required")]
        public Int64? ParentItemId { get; set; }
        public string ParentItemKey { get; set; }
        public List<SelectListItem> ParentItemList { get; set; }
        public Int64? DRGNoDocumentId { get; set; }
        public List<SelectListItem> DRGNoList { get; set; }
        public string DRGNoDocumentNo { get; set; }

        [Required(ErrorMessage = "Find no is required")]
        public int? FindNumber { get; set; }
        public string ExtFindNumber { get; set; }
        public string FindNumberDescription { get; set; }
        public string ActionForPCRelation { get; set; }

        public double? Weight { get; set; }
        public double? JobQty { get; set; }
        public double? CommissioningSpareQty { get; set; }
        public double? MandatorySpareQty { get; set; }
        public double? OperationSpareQty { get; set; }
        public double? ExtraQty { get; set; }
        public string Remarks { get; set; }

        [Required(ErrorMessage = "Quantity is required")]
        public double? Quantity { get; set; }

        [Required(ErrorMessage = "Length is requird")]
        public double? Length { get; set; }

        [Required(ErrorMessage = "Width is requird")]
        public double? Width { get; set; }

        [Required(ErrorMessage = "Number Of Pieces is requird")]
        public int? NumberOfPieces { get; set; }
        public string BomReportSize { get; set; }
        public string HdnBomReportSize { get; set; }
        public string DrawingBomSize { get; set; }
        public string HdnDrawingBomSize { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }
        public Nullable<int> BOMLevel { get; set; }

        public bool ConfirmRevision { get; set; }

    }

    public class PartColumnModel
    {
        public bool ItemState { get; set; }
        public bool RevNo { get; set; }
        public bool FindNumber { get; set; }
        public bool ReExtFindNumbervNo { get; set; }
        public bool FindNumberDescription { get; set; }
        public bool ARMSet { get; set; }
        public bool ARMRev { get; set; }

        public bool OrderPolicy { get; set; }
        public bool ProductForm { get; set; }
        public bool Material { get; set; }

        public bool ItemGroup { get; set; }
        public bool ItemName { get; set; }
        public bool String2 { get; set; }
        public bool String3 { get; set; }
        public bool String4 { get; set; }
        public bool ProcurementDrgDocumentNo { get; set; }
        public bool ItemWeight { get; set; }
        public bool BOMWeight { get; set; }
        public bool Thickness { get; set; }
        public bool CladPlatePartThickness1 { get; set; }
        public bool IsDoubleClad { get; set; }
        public bool CladPlatePartThickness2 { get; set; }
        public bool CladSpecificGravity2 { get; set; }
        public bool CRSYN { get; set; }
        public bool JobQty { get; set; }
        public bool CommissioningSpareQty { get; set; }
        public bool MandatorySpareQty { get; set; }
        public bool ExtraQty { get; set; }

        public bool Remarks { get; set; }

        public bool Quantity { get; set; }

        public bool Length { get; set; }

        public bool Width { get; set; }

        public bool NumberOfPieces { get; set; }
        public bool BomReportSize { get; set; }
        public bool DrawingBomSize { get; set; }

        public bool Createdbycolumn { get; set; }
        public bool CreatedOncolumn { get; set; }
        public bool editedbycolumn { get; set; }
        public bool EditedOncolumn { get; set; }

        public string Project { get; set; }
    }


    public class PolicySteps : PartColumnModel
    {
        public class ObjectPolicyStepsModel
        {
            public string ColorCode { get; set; }
            public Int64? PartLifeCycleId { get; set; }
            public Int64? ItemId { get; set; }
            public int? PolicyOrderID { get; set; }
            public Int64? PolicyStep { get; set; }

        }
    }

    public class BOMImport
    {
        public int RowNo { get; set; }
        public string ParentItem { get; set; }
        public string ItemId { get; set; }
        public string ActionForPCRelation { get; set; }
        public string DrgNo { get; set; }
        public string FindNo { get; set; }
        public string ExtFindNo { get; set; }
        public string FindNoDesc { get; set; }
        public string JobQty { get; set; }
        public string CommissioningSpareQty { get; set; }
        public string MandatorySpareQty { get; set; }
        public string OperationSpareQty { get; set; }
        public string ExtraQty { get; set; }
        public string QuantityForBOM { get; set; }
        public string Length { get; set; }
        public string Width { get; set; }
        public string NumberOfPieces { get; set; }
        public string Remarks { get; set; }
        public string BOMWeight { get; set; }
        public string BOMReportSize { get; set; }
        public string DrawingBOMSize { get; set; }
        public string ReviseMsg { get; set; }
    }

}
