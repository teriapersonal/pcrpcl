﻿using SolrNet.Attributes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQS.DESCore
{
    public class DocumentIndexModel
    {
        #region Members

        private string documentText;

        #endregion

        [SolrUniqueKey("id")]
        public string ObjectId { get; internal set; }


        [SolrField("Customer")]
        public string Customer { get; internal set; }

        [SolrField("BU")]
        public string BU { get; internal set; }

        [SolrField("Department")]
        public string Department { get; internal set; }

        [SolrField("Type")]
        public string Type { get; internal set; }

        [SolrField("Status")]
        public string Status { get; internal set; }

        [SolrField("Name")]
        public string Name { get; internal set; }

        [SolrField("Revision")]
        public string Revision { get; internal set; }

        [SolrField("Originator")]
        public string Originator { get; internal set; }

        [SolrField("OwnerName")]
        public string OwnerName { get; internal set; }

        [SolrField("filepath")]
        public string MainDocumentPath { get; internal set; }

        [SolrField("Descripion")]
        public string Descripion { get; internal set; }

        [SolrField("FileExtension")]
        public string FileFormat { get; internal set; }

        [SolrField("version")]
        public string version { get; internal set; }

        [SolrField("FileName")]
        public string FileName { get; internal set; }

        [SolrField("SubObject")]
        public string SubObject { get; internal set; }

        [SolrField("ProjectNumber")]
        public string ProjectNumber { get; internal set; }

        public string NumFound { get; set; }

        public string QueryTime { get; set; }
    }
}
