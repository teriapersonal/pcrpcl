﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQS.DESCore
{
    public class ROCModel
    {
        public Int64? ROCId { get; set; }
        public string EquipmentNo { get; set; }
        public string ROCRefNo { get; set; }
        public string Project { get; set; }
        public int? Version { get; set; }
        public Int64? AgencyId { get; set; }

        public string AgencyName { get; set; }
        public string DocNo { get; set; }
        public string DocTitle { get; set; }
        public int? DocRev { get; set; }

        public int? DocRevision { get; set; }
        public DateTime? SubmittedDate { get; set; }
        public Int64? DocumentId { get; set; }
        public string Status { get; set; }
        public double? LastSrNo { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }

        public bool IsCommentChanges { get; set; }

        public bool IsROCCommentsId { get; set; }

        public bool IsAgencyComment { get; set; }
        public bool IsLandTComment { get; set; }
        public bool IsAgencyResponse { get; set; }

        public string CurrentLocationIp { get; set; }

        public string FilePath { get; set; }

        public List<ROCCommentModel> ROCCommentList { get; set; }

        public bool IsView { get; set; }

        public bool IsLatest { get; set; }

        public bool IsRevised { get; set; }

        public Int64? FolderId { get; set; }

        public List<string> newPathAfterRevise { get; set; }

        public int CurrentLocation { get; set; }

        public string CurrentPsNo { get; set; }

        public string JEPClientDocNo { get; set; }
        public string JEPClientDocRev { get; set; }

    }

    public class ROCCommentModel
    {
        public long ROCCommentsId { get; set; }
        public long? ROCId { get; set; }
        public string Status { get; set; }
        public double? SrNo { get; set; }
        public long? DocumentId { get; set; }
        public string CommentLocation { get; set; }
        public string AgencyComments { get; set; }
        public DateTime? AgencyCommentsDate { get; set; }
        public string LandTComment { get; set; }
        public DateTime? LandTCommentDate { get; set; }
        public string ClientDocNo { get; set; }
        public string ClientDocRev { get; set; }
        public string AgencyResponse { get; set; }
        public DateTime? AgencyResponseDate { get; set; }
        public string CustomerSrNo { get; set; }
        public string CommfromCustDepts { get; set; }
        public string DocumentFormat { get; set; }
        public int? DocumentRevision { get; set; }
        public bool IsStatusClosed { get; set; }
    }
    public class DOCRevModel
    {
        public Int64? RevisionId { get; set; }
        public string DocRev { get; set; }
    }

    public class ROCCommentsAttachModel
    {
        public Int64? ROCCommentsAttachId { get; set; }
        public Int64? ROCCommentsId { get; set; }
        public string DocumentPath { get; set; }
        public string DocumentTitle { get; set; }
        public string DocumentFormat { get; set; }
        public bool IsAgencyComment { get; set; }
        public bool IsLandTComment { get; set; }
        public bool IsAgencyResponse { get; set; }
        public string CreatedBy { get; set; }

        public int FileLocation { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }
    }

    public class ROCCommentedDocModel
    {
        public long ROCCommentedDocId { get; set; }
        public long ROCId { get; set; }
        public string DocumentPath { get; set; }
        public string DocumentTitle { get; set; }
        public string DocumentFormat { get; set; }

        public int FileLocation { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }
    }

    public class ROCAttachList
    {
        public List<ROCFileAttachModel> ROCFileAttachList { get; set; }

    }
    public class ROCFileAttachModel
    {
        public long ROCCommentsAttachId { get; set; }
        public string DocumentPath { get; set; }

    }

    public class ROCComment
    {
        public long ROCCommentId { get; set; }
    }


    public class TempROCModel
    {



        public Int64? ROCId { get; set; }
        public string Equipment_No { get; set; }
        public string ROC_Ref_No { get; set; }
        public string Project_No { get; set; }
        public string Doc_No { get; set; }
        public string Reviewing_Agency { get; set; }
        public int? ROC_Version { get; set; }
        public string Doc_Title { get; set; }

        public string Create_Date { get; set; }
        public string Submit_Date { get; set; }

        public List<TempROCCommentModel> ROCCommentList { get; set; }

        public List<TempROCCommentedDocModel> ROCCommentedDOCList { get; set; }

    }

    public class TempROCCommentModel
    {
        public long ROCCommentsId { get; set; }
 
        public string Status { get; set; }
        public double? SR_No { get; set; }
        public int? Rev { get; set; }
        public string Customer_Doc_No { get; set; }

        public string Customer_Doc_Rev { get; set; }
        public string Comment_Location { get; set; }
        public string Reviewing_Agency_Comment_Clarifications { get; set; }
        public string Date { get; set; }
        public string L_T_HEIC_Reply_Resolution { get; set; }
        public string L_T_Reply_Resolution_Date { get; set; }
 
    
        public string Reviewing_Agency_Response { get; set; }
        public string Reviewing_Agency_Response_Date { get; set; }
        public string Customer_Sr_No { get; set; }
        public string Comments_from_Customer_Depts { get; set; }

    }

    public class TempROCCommentedDocModel
    {
        public long ROCCommentedDocId { get; set; }
        public long ROCId { get; set; }
        public string DocumentPath { get; set; }
        public string DocumentTitle { get; set; }
        public string DocumentFormat { get; set; }

        public int FileLocation { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public string EditedOn { get; set; }
    }

}
