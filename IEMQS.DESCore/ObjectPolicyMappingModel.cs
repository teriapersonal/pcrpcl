﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQS.DESCore
{
    /// <summary>
    /// DES007 Table Model
    /// Ravi Patel 13-11-2019
    /// </summary>
    public class ObjectPolicyMappingModel
    {
        public Int64? MappingId { get; set; }
        public Int64? ObjectId { get; set; }
        public Int64? PolicyId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }
        
    }
}
