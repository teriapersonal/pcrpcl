﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace IEMQS.DESCore
{
    public class TempPartModel
    {
        public Int64? FolderId { get; set; }
        public string Department { get; set; }
        public string RoleGroup { get; set; }

        public int? SrNo { get; set; }
        public bool IsSelected { get; set; }
        public Int64? TempItemId { get; set; }

        public Int64? ItemId { get; set; }

        [Required]
        public string Project { get; set; }
        public Int64? ItemSheetId { get; set; }
        public string ItemSheetName { get; set; }
        public string ItemKeyType { get; set; }
        public string OrderPolicy { get; set; }
        public Int64? ProductFormCodeId { get; set; }
        public string Type { get; set; }
        public string ItemKey { get; set; }
        public Int64? PolicyId { get; set; }
        public string Policy { get; set; }

        public bool ReviseWithBOM { get; set; }//new
        public string DRGNo { get; set; }
        public Int64? DRGNoDocumentId { get; set; }//new 
        public Int64? ParentPartId { get; set; }
        public int? FindNumber { get; set; }
        public string ExtFindNumber { get; set; }
        public string FindNumberDescription { get; set; }
        public string ActionForPCRelation { get; set; }
        public string Material { get; set; }
        public string ItemGroup { get; set; }
        public string ItemName { get; set; }
        public string String2 { get; set; }
        public string String3 { get; set; }
        public string String4 { get; set; }

        public string HdnString2 { get; set; }
        public string HdnString3 { get; set; }
        public string HdnString4 { get; set; }
        public string ARMSet { get; set; }
        public Int64? ProcurementDrgDocumentId { get; set; }
        public double? Weight { get; set; }
        public string SizeCode { get; set; }
        public double? Thickness { get; set; }
        public double? CladPlatePartThickness1 { get; set; }
        public double? CladSpecificGravity1 { get; set; }
        public string IsDoubleClad { get; set; }
        public double? CladPlatePartThickness2 { get; set; }
        public double? CladSpecificGravity2 { get; set; }
        public string CRSYN { get; set; }
        public double? JobQty { get; set; }
        public double? CommissioningSpareQty { get; set; }
        public double? MandatorySpareQty { get; set; }
        public double? OperationSpareQty { get; set; }
        public double? ExtraQty { get; set; }
        public string Remarks { get; set; }
        public double? Quantity { get; set; }
        public double? Length { get; set; }
        public double? Width { get; set; }
        public int? NumberOfPieces { get; set; }
        public string BomReportSize { get; set; }
        public string DrawingBomSize { get; set; }
        public int? RevNo { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }

        public bool IsDeleted { get; set; }

        public List<SelectListItem> ItemKeyTypeList { get; set; }
        public List<SelectListItem> OrderPolicyList { get; set; }
        public List<SelectListItem> ItemTypeList { get; set; }
        public List<SelectListItem> ActionforPCRelationList { get; set; }
        public List<SelectListItem> YesOrNoList { get; set; }
        public List<SelectListItem> DRGNoList { get; set; }
        public List<SelectListItem> ManufacturedPartList { get; set; }
        public List<SelectListItem> ProductFormCodeList { get; set; }
        public List<SelectListItem> MaterialList { get; set; }
        public List<SelectListItem> ItemGroupList { get; set; }
        public List<SelectListItem> ARMSetList { get; set; }
        public List<SelectListItem> ProcurementDrawingList { get; set; }
        public List<SelectListItem> SizeCodeList { get; set; }
        public List<TempPartModel> PartList { get; set; }
        public long? BOMId { get; set; }
        public long ParentItemId { get; set; }


        public string UOM { get; set; }
        public string ARMRev { get; set; }
        public double? ItemWeight { get; set; }
        public double? Density { get; set; }
        public double? WeightFactor { get; set; }
        public int? ManufactureItem { get; set; }
        public string HdnBomReportSize { get; set; }
        public string HdnDrawingBomSize { get; set; }

        [Required(ErrorMessage = "Project is Required")]
        public string ddlProject { get; set; }

        [Required(ErrorMessage = "Manufacturing Item is Required")]
        public string ddlmanufacturingitem { get; set; }
        public string ErrorMsg { get; set; }
        public string SuccessMsg { get; set; }
        public string ItemKeyConflict { get; set; }
        public bool MultiFindNoItemKey { get; set; }
    }

    public class PartPolicyModel
    {
        public Int64? PolicyId { get; set; }
        public string ObjectName { get; set; }
        public string PolicyName { get; set; }
    }

    public class PartPowerViewModel
    {
        public long? ItemId { get; set; }
        public string ProjectNo { get; set; }

        public string QString { get; set; }
        public List<PowerViewModel> PowerViewModelList { get; set; }
    }


    public class PowerViewModel
    {
        public long? BOMId { get; set; }
        public long? ItemId { get; set; }
        public long? ParentItemId { get; set; }
        public string ItemKey { get; set; }
        public string Type { get; set; }
        public int? Rev { get; set; }
        public string policy { get; set; }
        public int? FindNumber { get; set; }
        public string ExtFindNumber { get; set; }
        public string ItemName { get; set; }
        public double? Quantity { get; set; }
        public double? Length { get; set; }
        public double? Width { get; set; }
        public int? NumberOfPieces { get; set; }
        public string UOM { get; set; }
        public string Material { get; set; }
        public string State { get; set; }
        public string ItemGroup { get; set; }
        //public int? TableCount { get; set; }


    }

    public class PartTreeViewModel
    {
        public long? ItemId { get; set; }
        public string ProjectNo { get; set; }

        public string QString { get; set; }
        public TreeViewModel TreeViewModelList { get; set; }
    }

    public class TreeViewModel
    {
        public string name { get; set; }
        //public long? id { get; set; }
        public string title { get; set; }
        public List<TreeViewModel> children { get; set; }
    }

    public class GetListOfItmeID
    {
        public List<Int64?> ItemId { get; set; }

        public string BtnText { get; set; }
        public int? PolicyOrderId { get; set; }
    }

    public class PrintPartModel
    {
        public string Item_Key_Type { get; set; }
        public string Order_Policy { get; set; }

        public string Product_Form { get; set; }

        public string Type { get; set; }
        public string Item_Id { get; set; }
        public string Rev_No { get; set; }
        public string Policy { get; set; }
        public Nullable<bool> ReviseWithBOM { get; set; }
        public string Drg_No { get; set; }
        public string Parent_Item { get; set; }
        public int? Find_No { get; set; }
        public string Ext_Find_No { get; set; }
        public string Find_No_Desc { get; set; }
        public string Action_for_PC_Relation { get; set; }
        public string Material { get; set; }
        public string Item_Group { get; set; }
        public string Item_Description { get; set; }
        public string Procurement_Drg { get; set; }
        public string String2 { get; set; }
        public string String3 { get; set; }
        public string String4 { get; set; }
        public string ARM_Set { get; set; }
        public string ARM_Rev { get; set; }
        public string Size_Code { get; set; }
        public double? Thickness { get; set; }
        public double? Clad_PlatePart_Thickness_1 { get; set; }
        public double? Clad_Specific_Gravity_1 { get; set; }
        public string IsDoubleClad { get; set; }
        public double? Clad_PlatePart_Thickness_2 { get; set; }
        public double? Clad_Specific_Gravity_2 { get; set; }
        public double? Item_Weight { get; set; }
        public double? BOM_Weight { get; set; }
        public string CRS { get; set; }
        public double? Job_Qty { get; set; }
        public double? Commissioning_Spare_Qty { get; set; }
        public double? Mandatory_Spare_Qty { get; set; }
        public double? Operation_Spare_Qty { get; set; }
        public double? Extra_Qty { get; set; }
        public double? Quantity { get; set; }
        public double? Length { get; set; }
        public double? Width { get; set; }
        public int? Number_Of_Pieces { get; set; }
        public string BOM_Report_Size { get; set; }
        public string Drawing_BOM_Size { get; set; }
        public double? Specific_Gravity { get; set; }
        public string UOM { get; set; }
        public double? Weight_Factor { get; set; }
        public string Remarks { get; set; }
    }

}
