﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace IEMQS.DESCore
{
    public class TransmittalModel
    {
        public Int64 TransmittalId { get; set; }
        public Int64 AgencyId { get; set; }
        public Int64? FolderId { get; set; }
        public string Status { get; set; }

        public string Project { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }
        public List<TransmittalDocumentModel> TransmittalDocumentList { get; set; }

        public string AgencyName { get; set; }


        public bool IsAgencyCheckIn { get; set; }
        public string AgencyCheckInBy { get; set; }
        public DateTime? AgencyCheckInOn { get; set; }
        public string TransmittalNo { get; set; }
        public string RoleGroup { get; set; }
    }

    public class TransmittalDocumentModel
    {
        public Int64 TransmittalDocListId { get; set; }
        public Int64 TransmittalId { get; set; }
        public Int64? AgencyDocListId { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }
    }

    public class AgencyDocumentModel
    {
        public Int64? AgencyDocId { get; set; }
        public string Project { get; set; }

        [Required(ErrorMessage = "Agency is required")]
        public Int64? AgencyId { get; set; }
        public Int64? FolderId { get; set; }
        public bool IsEmailSentToAgency { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }
        public bool IsCommentChanges { get; set; }

        public string RoleGroup { get; set; }
        public List<AgencyDocumentDetailModel> AgencyDocumentList { get; set; }
    }

    public class AgencyDocumentDetailModel
    {
        public Int64 AgencyDocListId { get; set; }
        public Int64 AgencyDocId { get; set; }
        public Int64 JEPDocumentDetailsId { get; set; }
        public string ClientDocNo { get; set; }
        public string ClientDocRev { get; set; }
        public DateTime? PlannedSubmitDate { get; set; }
        public DateTime? PlannedApprovalDate { get; set; }

        public double? Weightage { get; set; }

        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }

        public Int64? GenDocID { get; set; }
    }

    public class AgencyDocModel
    {
        public Int64? AgencyId { get; set; }
        public string AgencyName { get; set; }
        public string AgencyDesc { get; set; }
        public string DocumentNo { get; set; }
        public string Revision { get; set; }
        public string Description { get; set; }
        public DateTime PlannedSubmitDate { get; set; }
        public string Status { get; set; }

        public string CurrentLocationIp { get; set; }
        public Int64? TransmittalId { get; set; }

        public Int64? CurrentBU  { get; set; }

        public string Project { get; set; }

    }

    public class TransmittalEmail
    {
        public Int64? AgencyId { get; set; }
        public string AgencyName { get; set; }
        public string AgencyTo { get; set; }
        public string AgencyCC { get; set; }
        public string LandTTo { get; set; }
        public string LandTCC { get; set; }
        public string Subject { get; set; }
        public string EmailBody { get; set; }
        public string PONO { get; set; }
        public string Owner { get; set; }
        public string Project { get; set; }
        public string Purchaser { get; set; }
        public string LntProjectNo { get; set; }
        public string TransmittalNo { get; set; }

    }

    public class TransmittalEmailModel
    {
        public Int64 TransmittalEmailHistoryID { get; set; }
        public Int64 TransmittalId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }
    }
}