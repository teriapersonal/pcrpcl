﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQS.DESCore
{
    /// <summary>
    /// DES027 Table Model
    /// Ravi Patel 18-11-2019
    /// </summary>
    public class AgencyModel
    {       
        public Int64? AgencyId { get; set; }
        public string AgencyName { get; set; }
        public string AgencyDescription { get; set; }
        public Int64? BUId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }
        public bool IsAgencyCheckIn { get; set; }
        public string AgencyCheckInBy { get; set; }
        public DateTime? AgencyCheckInOn { get; set; }
    }
}
