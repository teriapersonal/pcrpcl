﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace IEMQS.DESCore
{
    /// <summary>
    /// DES068 Table Model
    /// Ravi Patel 02-12-2019
    /// </summary>
    public class AgencyEmailModel
    {
        public Int64? TransmittalEmailId { get; set; }
        public string Project { get; set; }

        [Remote("CheckAgencyEmailAlreadyExist", "AgencyEmail", AdditionalFields = "TransmittalEmailId,Project", HttpMethod = "POST", ErrorMessage = "Agency is already exist")]
        [Required(ErrorMessage = "Agency is required")]
        public Int64? AgencyId { get; set; }

        [Required(ErrorMessage = "Agency Email is required")]
        [RegularExpression("^(\\s?[^\\s,]+@[^\\s,]+\\.[^\\s,]+\\s?,)*(\\s?[^\\s,]+@[^\\s,]+\\.[^\\s,]+)$", ErrorMessage ="Enter valid Email-Id")]
        public string AgencyTo { get; set; }

        [RegularExpression("^(\\s?[^\\s,]+@[^\\s,]+\\.[^\\s,]+\\s?,)*(\\s?[^\\s,]+@[^\\s,]+\\.[^\\s,]+)$", ErrorMessage = "Enter valid Email-Id")]
        public string AgencyCC { get; set; }

        [Remote("CheckEmailDomain", "AgencyEmail", AdditionalFields = "LandTTo", HttpMethod = "POST")]
        //[RegularExpression("^(\\s?[^\\s,]+@[^\\s,]+\\.[^\\s,]+\\s?,)*(\\s?[^\\s,]+@[^\\s,]+\\.[^\\s,]+)$", ErrorMessage = "Enter valid Email-Id")]
        public string LandTTo { get; set; }

        //[Required(ErrorMessage = "L&amp;T CC is required")]
        //[RegularExpression("^(\\s?[^\\s,]+@[^\\s,]+\\.[^\\s,]+\\s?,)*(\\s?[^\\s,]+@[^\\s,]+\\.[^\\s,]+)$", ErrorMessage = "Enter valid Email-Id")]
        [Remote("CheckEmailDomain", "AgencyEmail", AdditionalFields = "LandTCC", HttpMethod = "POST")]
        public string LandTCC { get; set; }

        [Required(ErrorMessage = "Subject is required")]
        public string Subject { get; set; }

        [AllowHtml]
        public string EmailBody { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }
    }
}
