﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQS.DESCore
{
    public class AdvanceSearchModel
    {
        public string SearchText { get; set; }
        public string CurrentLocationIp { get; set; }

        public string CustomerName { get; set; }

        public long BUId { get; set; }
        public string BU { get; set; }

        public string Department { get; set; }

        public long ObjectId { get; set; }

        public string SubObjectId { get; set; }

        public string ObjectName { get; set; }

        public long PolicyStepId { get; set; }

        public string StepName { get; set; }

        public string Name { get; set; }

        public string Revision { get; set; }

        public string Originator { get; set; }

        public string OwnerName { get; set; }

        public string SearchName { get; set; }

        public string FilterId { get; set; }

        public string Status { get; set; }

        public string FilterName { get; set; }

        public string ProjectNumber { get; set; }

        public string ObjID { get; set; }
    }
}
