﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQS.DESCore
{
    /// <summary>
    /// DES001 Table Model
    /// Kishan Parmar 11-11-2019
    /// </summary>
   public class BUModel
    {
        public Int64? BUId { get; set; }
        public string BU { get; set; }
        public string PBU { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }
        public string BUCode { get; set; }
        public string PBUCode { get; set; }
    }
}
