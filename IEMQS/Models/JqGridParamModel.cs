﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IEMQS.Models
{
    public class JqGridParamModel
    {
        public string sord { get; set; }
        public int page { get; set; }
        public int rows { get; set; }
        public string sidx { get; set; }

        public string searchField { get; set; }
        public string searchOper { get; set; }
        public bool _search { get; set; }
        public string searchString { get; set; }      
        public filters filters { get; set; }   // For Coulumnwise Filters 

    }

    public class filters
    {
        public string groupOp { get; set; }
        public groups groups { get; set; }

        public rules[] rules;

    }
    public class groups
    {
        public string groupOp { get; set; }
        public rules rules { get; set; }        

    }
    public class rules
    {
        public string field { get; set; }
        public string op { get; set; }
        public string data { get; set; }

    }
}