﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;

namespace IEMQS.Models
{
    public class IndexViewModel
    {
        public bool HasPassword { get; set; }
        public IList<UserLoginInfo> Logins { get; set; }
        public string PhoneNumber { get; set; }
        public bool TwoFactor { get; set; }
        public bool BrowserRemembered { get; set; }
    }

    public class ManageLoginsViewModel
    {
        public IList<UserLoginInfo> CurrentLogins { get; set; }
        public IList<AuthenticationDescription> OtherLogins { get; set; }
    }

    public class FactorViewModel
    {
        public string Purpose { get; set; }
    }

    public class SetPasswordViewModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class ChangePasswordViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class AddPhoneNumberViewModel
    {
        [Required]
        [Phone]
        [Display(Name = "Phone Number")]
        public string Number { get; set; }
    }

    public class VerifyPhoneNumberViewModel
    {
        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }

        [Required]
        [Phone]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
    }

    public class ConfigureTwoFactorViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
    }

    public class clsExportParametersModel
    {
        public string dvId { get; set; }
        public string GridType { get; set; }//(Header/Lines/Sublines)
        public string url { get; set; }
    }

    public class DiagramNodes
    {
        public int Id { get; set; }
        public int? ParentId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        public string HeaderId { get; set; }
        public DiagramNodes(int? Id, int? ParentId, string Title, string Description = "", string Url = "", object HeaderId = null)
        {
            this.Id = Id.HasValue? Id.Value: 0;
            this.ParentId = ParentId;
            this.Title = Title;
            this.Description = Description;
            this.HeaderId = HeaderId == null ? "" : HeaderId.ToString();
            this.Url = string.Format(Url, this.HeaderId);
        }

        public DiagramNodes(IEMQSImplementation.SP_ATH_GET_USER_ACCESS_MENU_Result Menu, string Url = "", object HeaderId = null)
        {
            Id = Menu.ProcessId.HasValue ? Menu.ProcessId.Value : 0;
            ParentId = Menu.ParentId;
            Title = Menu.Process;
            Description = Menu.Description;
            this.HeaderId = HeaderId == null ? "" : HeaderId.ToString();
            this.Url = string.Format(Url, this.HeaderId);
        }
    }
}