﻿var _currentScreen = null;
var _isDialogBoxOpened = null;
var _dialog = null;
var _dialogBoxContainer = "dialogContainer";
var _tabContentSelector = '.tab-content';
var _anyCheckboxSelector = '.clsCheckbox:checkbox';
var _zoomWindowSelector = '.zoom-window';
var _checkedCheckboxSelector = _anyCheckboxSelector + ':checked';
var _stockNumberSelector = 'input.stock-number';
var _stockRevisionSelector = 'input.stock-revision';
var _isExecuting = false;
var _dateFormat = 'DD/MM/YYYY';
var _tableName = null; // fill from respective screen
var _tbl = null;
var _columnsForSave = null;
var _columnsForEmail = null;
var _additionalColumnsForAddNewRow = null;
var _groupHeadersForSO = null;
var _customTabs = [];
var _messages = {
    Success: 'Saved successfully'
}

var _last = {
    RowNumber: null,
    Values: null
};

var _mpData = {
    SelectedRow: null,
    Columns: null,
    AllData: null,
    FilteredData: null,
    PreviousStockNumber: null,
    PreviousStockRevision: null,
    DataTable: null,
    TableReference: null,
    Dialog: null
}

var _listYesNo = [
    {
        CatID: 1,
        CatDesc: 'Yes'
    },
    {
        CatID: 2,
        CatDesc: 'No'
    },
];

var _screenTabs = {
    Main: {
        Name: '',
        IsMultiOccurrence: true
    },
    Details: {
        Name: 'Details',
        IsMultiOccurrence: false
    }
}

var _screenSelectors = {
    Root: '#',
    Anchor: 'ul.nav-tabs li a',
    Tabs: 'ul.nav-tabs'
};

var _screenClasses = {
    Active: 'active',
    TabPane: 'tab-pane'
};

var _screenAttributes = {
    Rel: 'data-rel',
    Name: 'data-name'
};
var _defaultTab;

var _messageType = {
    Success: 0,
    Information: 1,
    Error: 2
};

var _screens = {
    GeneratePCR: {
        Name: 'GeneratePCR',
        HasBeforeReWrite: false,
        CurrentTab: null
    },
    MaterialPlanner: {
        Name: 'MaterialPlanner',
        HasBeforeReWrite: true,
        CurrentTab: null
    },
    ReleasePCL: {
        Name: 'ReleasePCL',
        HasBeforeReWrite: false,
        CurrentTab: null
    },
    MaintainReturnDistribution: {
        Name: 'MaintainReturnDistribution',
        HasBeforeReWrite: false,
        CurrentTab: null
    },
    PCLWorkflow: {
        Name: 'PCLWorkflow',
        HasBeforeReWrite: false,
        CurrentTab: null
    }
};

var _stateTypes = {
    Clean: 0,
    Dirty: 1,
    Lookup: 2
}

let _selectedRow = {
    RowNumber: null,
    Columns: {}
};

var _propertyNames = {
    LookUp: 'LookUp',
    LookUpValues: "LookUpValues",
    ViewColumnValues: 'ViewColumnValues',
    DirtyValue: 'DirtyValue',
    CleanValue: 'CleanValue',
    DbColumnName: 'DbColumnName'
};

var _columnType = {
    Textbox: 'Textbox',
    NumberTextbox: 'NumberTextbox',
    Date: 'Date',
    Lookup: 'Lookup'
};

var _columns = {
    Project: {
        Name: 'Project',
        Value: 't_cprj'
    },
    Element: {
        Name: 'Element',
        Value: 't_cspa'
    },
    AreaConsumed: {
        Name: 'AreaConsumed',
        Value: 't_arcs',
        InvalidMessage: 'Area Consumed Cannot Be Negative.',
    },
    BudgetLine: {
        Name: 'BudgetLine',
        Value: 't_sern'
    },
    PartNumber: {
        Name: 'PartNumber',
        Value: 't_prtn'
    },
    ManufacturingItem: {
        Name: 'ManufacturingItem',
        Value: 't_mitm'
    },
    ManufacturingItemPosition: {
        Name: 'ManufacturingItemPosition',
        Value: 't_mpos'
    },
    MaterialPlanner: {
        Name: 'MaterialPlanner',
        Value: 't_mrmk'
    },
    MovementType: {
        Name: 'MovementType',
        Value: 't_mvtp',
        InvalidMessage: 'Movement Type Should be from The List',
        EmptyMessage: 'Movement Type cannot be empty.',
        Type: _columnType.Lookup,
        Lookup: [{ CatID: 1, CatDesc: "Profile Cutting" }, { CatID: 2, CatDesc: "Area Wise Cutting" }, { CatID: 3, CatDesc: "Full Plate" }]
    },
    ChildItem: {
        Name: 'ChildItem',
        Value: 't_sitm'
    },
    Sequence: {
        Name: 'Sequence',
        Value: 't_seqn'
    },
    PCRLineNo: {
        Name: 'PCRLineNo',
        Value: 't_pono'
    },
    PCRLineRevision: {
        Name: 'PCRLineRevision',
        Value: 't_revn'
    },
    PCRLineStatus: {
        Name: 'PCRLineStatus',
        Value: 't_lsta',
        Type: _columnType.Lookup,
        Lookup: null
    },
    PCRPlannerRemark: {
        Name: 'PCRPlannerRemark',
        Value: 't_prmk',
        EmptyMessage: 'Planner remarks cannot be blank'
    },
    PCRInitiator: {
        Name: 'PCRInitiator',
        Value: 't_init',
    },
    ItemRevision: {
        Name: 'ItemRevision',
        Value: 't_revi'
    },
    Thickness: {
        Name: 'Thickness',
        Value: ''
    },
    Thickness02: {
        Name: 'Thickness',
        Value: 't_thic'
    },
    PCRLineId: {
        Name: 'PCRLineId',
        Value: 'PCRLineId'
    },
    Length: {
        Name: 'Length',
        Value: 't_leng',
        InvalidMessage: 'Length Cannot be less than or Equal to Zero',
        EmptyMessage: 'Length Cannot Be Empty.',
        InvalidMessage2: 'Length Cannot be greater than Maximum Original Length',
        DbValidate: true
    },
    Width: {
        Name: 'Width',
        Value: 't_widt',
        InvalidMessage: 'Width of Return Plate must be greater then 0',
        EmptyMessage: 'Width Cannot Be Empty.',
        InvalidMessage2: 'Width of Return Plate cannot be greater Total Width of the Plate',
        DbValidate: true
    },
    LengthModified: {
        Name: 'LengthModified',
        Value: 't_leng_m',
        InvalidMessage: 'Length should not be less than or equal to zero.',
        EmptyMessage: 'Length cannot be empty.',
        Type: _columnType.NumberTextbox
    },
    WidthModified: {
        Name: 'WidthModified',
        Value: 't_widt_m',
        InvalidMessage: 'Width should not be less than or equal to zero.',
        EmptyMessage: 'Width cannot be empty.',
        Type: _columnType.NumberTextbox
    },
    PCRNumber: {
        Name: 'PCRNumber',
        Value: 't_pcrn',
        Type: _columnType.Textbox
    },
    PCLNumber: {
        Name: 'PCLNumber',
        Value: 't_pcln',
        IsHidden: true,
    },
    PCLNumberList: {
        Name: 'PCLNumberList',
        Value: 't_pcln',
        Type: _columnType.Lookup,
        Lookup: null
    },
    LinkToSCR: {
        Name: 'LinkToSCR',
        Value: 't_lscr',
    },
    NoOfPieces: {
        Name: 'NoOfPieces',
        Value: 't_npcs'
    },
    NestedQuantity: {
        Name: 'NestedQuantity',
        Value: 't_nqty',
        InvalidMessage: 'Nested quantity should not be less than zero.',
        Type: _columnType.NumberTextbox
    },
    OriginalItem: {
        Name: 'OriginalItem',
        Value: 't_oitm'
    },
    GrainOrientation: {
        Name: 'GrainOrientation',
        Value: 't_gorn'
    },
    PCRRequirementDate: {
        Name: 'PCRRequirementDate',
        Value: 't_prdt',
        DateMessage: 'PCR Requirement date should Be greater than current date.',
        InvalidMessage: 'PCR Requirement date is not valid date.',
        EmptyMessage: 'PCR Requirement date should not be blank.',
        Type: _columnType.Date
    },
    PCRCreationDate: {
        Name: 'PCRCreationDate',
        Value: 't_pcr_crdt',
        DateMessage: 'PCR Creation date should Be greater than current date.',
        InvalidMessage: 'PCR Creation date is not valid date.',
        EmptyMessage: 'PCR Creation date should not be blank.',
        Type: _columnType.Date
    },
    PCRReleaseDate: {
        Name: 'PCRReleaseDate',
        Value: 't_pcr_rldt',
        DateMessage: 'PCR Release date should Be greater than current date.',
        InvalidMessage: 'PCR Release date is not valid date.',
        EmptyMessage: 'PCR Release date should not be blank.',
        Type: _columnType.Date
    },
    PCLCreationDate: {
        Name: 'PCLCreationDate',
        Value: 't_pcl_crdt',
        DateMessage: 'PCL Creation date should Be greater than current date.',
        InvalidMessage: 'PCL Creation date is not valid date.',
        EmptyMessage: 'PCL Creation date should not be blank.',
        Type: _columnType.Date
    },
    PCLReleaseDate: {
        Name: 'PCLReleaseDate',
        Value: 't_pcl_rldt',
        DateMessage: 'PCL Release date should Be greater than current date.',
        InvalidMessage: 'PCL Release date is not valid date.',
        EmptyMessage: 'PCL Release date should not be blank.',
        Type: _columnType.Date
    },
    RequiredQuantity: {
        Name: 'RequiredQuantity',
        Value: 't_npcs_m',
        InvalidMessage: 'Required quantity should not be less than or equal to zero.',
        EmptyMessage: 'Required Quantity cannot be empty.',
        Type: _columnType.NumberTextbox
    },
    WCDeliver: {
        Name: 'WCDeliver',
        Value: 't_cwoc',
        InvalidMessage: 'Work Center should  be from the list',
        EmptyMessage: 'Work Center cannot be empty.',
        DbValidate: false,
        Type: _columnType.Lookup,
        Lookup: null
    },
    ItemMaterialGrade: {
        Name: 'ItemMaterialGrade',
        Value: 't_dscb'
    },
    PCRType: {
        Name: 'PCRType',
        Value: 't_pcrt',
        InvalidMessage: 'PCRType should  be from the list',
        EmptyMessage: 'PCRType cannot be empty.',
        Type: _columnType.Lookup,
        Lookup: null
    },
    PlannerRemark: {
        Name: 'PlannerRemark',
        Value: 't_prmk',
        EmptyMessage: 'Planner remarks cannot be blank',
        Type: _columnType.Textbox
    },
    CuttingLocation: {
        Name: 'CuttingLocation',
        Value: 't_cloc',
        InvalidMessage: 'Cutting Location should  be from the list',
        EmptyMessage: 'Cutting Location cannot be empty.',
        Type: _columnType.Lookup,
        IsIdValueSame: true,
        Lookup: null
    },
    Location: {
        Name: 'Location',
        Value: 't_loca',
        InvalidMessage: 'Location should  be from the list',
        EmptyMessage: 'Location cannot be empty.',
        Type: _columnType.Lookup,
        Lookup: null
    },
    SubLocation: {
        Name: 'SubLocation',
        Value: 't_sloc',
        EmptyMessage: 'Sub Location cannot be empty.',
        Type: _columnType.Textbox
    },
    ScrapQuantity: {
        Name: 'ScrapQuantity',
        Value: 't_sqty',
    },
    StockNumber: {
        Name: 'StockNumber',
        Value: 't_stkn',
        EmptyMessage: 'Stock Number should not be blank.',
        InvalidMessage: 'Please select stock number from the zoom window',
        //DbValidate: false,
        //IsEditable: false,
        IsValid: false,
        //Type: _columnType.Lookup,
        Lookup: []

    },
    StockRevision: {
        Name: 'StockRevision',
        Value: 't_stkr',
    },
    ShapeType: {
        Name: 'ShapeType',
        Value: 't_shtp',
    },
    ShapeFile: {
        Name: 'ShapeFile',
        Value: 't_shfl',
    },
    UseAlternateStock: {
        Name: 'UseAlternateStock',
        Value: 't_alts',
        InvalidMessage: 'Use Alternate Stock Should be from The List',
        EmptyMessage: 'Use Alternate Stock  cannot be empty.',
        Type: _columnType.Lookup,
        Lookup: _listYesNo
    },
    AlternateStockRemark: {
        Name: 'AlternateStockRemark',
        Value: 't_armk',
        EmptyMessage: 'Alternate Stock Remark cannot be empty.',
    },
    SCRNumber: {
        Name: 'SCRNumber',
        Value: 't_scrn',
    },
    Priority: {
        Name: 'Priority',
        Value: 't_prio',
        InvalidMessage: 'Priority field value should not be greater than 3',
        EmptyMessage: '',
        Type: _columnType.NumberTextbox,
        ValidateIfNotEmpty: true
    },
    ManufacturingItemDesc: {
        Name: 'ManufacturingItemDesc',
        Value: ''
    },
    ChildItemDesc: {
        Name: 'ChildItemDesc',
        Value: ''
    },
    ProjectDesc: {
        Name: 'ProjectDesc',
        Value: ''
    },
    ElementDesc: {
        Name: 'ElementDesc',
        Value: ''
    },
    WCDeliverDesc: {
        Name: 'WCDeliverDesc',
        Value: ''
    },
    MaterialSpecification: {
        Name: 'MaterialSpecification',
        Value: ''
    },
    EmployeeName: {
        Name: 'EmployeeName',
        Value: ''
    },
    ARMCode: {
        Name: 'ARMCode',
        Value: 't_dscd'
    },
    PCRHardCopyNo: {
        Name: 'PCRHardCopyNo',
        Value: 't_hcno'
    },
    CuttingLocationModified: {
        Name: 'CuttingLocationModified',
        Value: 't_cloc',
        InvalidMessage: 'Cutting Location should  be from the list',
        EmptyMessage: 'Cutting Location cannot be empty.',
        Type: _columnType.Lookup,
        IsIdValueSame: true,
        Lookup: null
    },
    AreaOfPieces: {
        Name: 'AreaOfPieces',
        Value: 't_area'
    },
    TotalArea: {
        Name: 'TotalArea',
        Value: 't_area'
    },
    P1: {
        Name: 'P1',
        Value: 't_pr01'
    },
    P2: {
        Name: 'P2',
        Value: 't_pr02'
    },
    P3: {
        Name: 'P3',
        Value: 't_pr03'
    },
    P4: {
        Name: 'P4',
        Value: 't_pr04'
    },
    P5: {
        Name: 'P5',
        Value: 't_pr05'
    },
    P6: {
        Name: 'P6',
        Value: 't_pr06'
    },
    P7: {
        Name: 'P7',
        Value: 't_pr07'
    },
    P8: {
        Name: 'P8',
        Value: 't_pr08'
    },
    P9: {
        Name: 'P9',
        Value: 't_pr09'
    },
    P10: {
        Name: 'P10',
        Value: 't_pr010'
    },
    Revision: {
        Name: 'Revision',
        Value: 't_revn',
        IsHidden: true
    },
    TotalConsumedArea: {
        Name: 'TotalConsumedArea',
        Value: 't_area'
    },
    ReturnArea: {
        Name: 'ReturnArea',
        Value: 't_rtar'
    },
    PCLStatus: {
        Name: 'PCLStatus',
        Value: '',
        Type: _columnType.Lookup,
        Lookup: null
    },
    ReturnRemark: {
        Name: 'ReturnRemark',
        Value: 't_rrmk'
    },
    TotalCuttingLength: {
        Name: 'TotalCuttingLength',
        Value: 't_tclt',
        InvalidMessage: 'Total Cutting Length should not be negative/ zero',
    },
    Planner: {
        Name: 'Planner',
        Value: 't_emno',
        Type: _columnType.Lookup,
        Lookup: null
    },
    PlannerName: {
        Name: 'PlannerName',
        Value: 't_nama'
    },
    Remark: {
        Name: 'Remark',
        Value: 't_mrmk'
    },
    ReturnBalance: {
        Name: 'ReturnBalance',
        Value: 't_rbal',
        InvalidMessage: 'Return Balance(pcs) should not be less than zero',
    },
    AllowedStage1: {
        Name: 'AllowedStage1',
        Value: 't_str1'
    },
    AllowedStage1Name: {
        Name: 'AllowedStage1Name',
        Value: 't_stgi',
        Type: _columnType.Lookup,
        Lookup: _listYesNo
    },
    OddShape: {
        Name: 'OddShape',
        Value: 't_odds',
        InvalidMessage: 'Odd Shape Value should  be from the list',
        EmptyMessage: 'Odd Shape cannot be empty.',
        Type: _columnType.Lookup,
        Lookup: _listYesNo
    },
    Area: {
        Name: 'Area',
        Value: 't_area',
        InvalidMessage: 'Area Cannot be less than or Equal to Zero',
        EmptyMessage: 'Area Cannot Be Empty.',
        InvalidMessage2: 'Area Cannot be greater than Maximum Return Area',
        DbValidate: true
    },
    LoadPlantMachine: {
        Name: 'LoadPlantMachine',
        Value: 't_lpm',
        InvalidMessage: 'Load Plant Machine Value should  be from the list',
        EmptyMessage: 'Load Plant Machine cannot be empty.',
        Type: _columnType.Lookup,
        Lookup: [{ CatID: 1, CatDesc: "Machine 1" }, { CatID: 0, CatDesc: "Machine 2" }]
    },
    //Not Actually a Column Just Used to Display AutoCompleteform
    ScreenName: {
        Name: 'ScreenName',
        Value: '',
        InvalidMessage: 'Screen Name Should Be From the list.',
        Type: _columnType.Lookup,
        Lookup: null
    }
}

var _attributes = {
    ColumnName: 'colname',
    TabIndex: 'tabindex',
    DependentOnColumnName: 'data-DependentOnColumnName',
    Section: 'data-Section',
    Item: 'data-Item',
    ItemId: 'data-ItemId',
    Init: 'data-isInitialized',
    PCRN: 'data-PCRN',
    PrevValue: 'data-PrevValue',
    StockNumber: 'data-StockNumber',
    StockRevision: 'data-StockRevision',
    PCRLineStatus: 'data-PCRLineStatus',
    PCLStatus: 'data-PCLStatus',
    LocationAddress: 'data-Idar'
};

var _classes = {
    EditableItem: 'editable-item',
    InvalidEditableItem: 'invalid-editable-item',
    TopSection: 'top-section',
    TextInputField: 'text-input-field',
    TextInputFieldReadonly: 'text-input-field-readonly',
    TextOutputField: 'text-output-field',
    AutoComplete: 'autocomplete',
    Center: 'center',
    ControlRequired: 'controlrequired',
    DisabledRow: 'disabled-row',
    SelectedRow: 'selected-row',
    InputField: 'inputField',
    Left: 'left',
    Action: 'action',
    Left18: 'left18',
    SelectBox: 'select-box',
    Hide: 'hide',
    NoOfPieces: 'noofpieces',
    UseAlternateStock: 'usealternatestock',
    BudgetLineNumber: 'budgetLineNumber',
    Element: 'element',
    ChildItem: 'childitem',
    Location: 'location',
    Length: 'length',
    Width: 'width',
    Numeric: 'numeric',
    DialogBoxButtons: 'dialog-box-buttons',
    Group1: 'group1',
    Section1: 'section1',
    Section2: 'section2',
    Section3: 'section3',
    Section4: 'section4',
    Section5: 'section5',
    Section6: 'section6',
    Section7: 'section7',
    Section8: 'section8'
};

var _actionType = {
    Load: 'L',
    Reload: 'R',
    Undo: 'U',
    Navigate: 'N',
    FocussedIn: 'F',
    AutoComplete: 'A',
    Dialog: 'D',
    GetData: 'G',
    AddOrEditRows: 'AddOrEditRows',
    BeforeReWrite: 'BeforeReWrite',
    SendAnEmail: 'SendAnEmail',
    ValidateReturnDistributionArea: 'ValidateReturnDistributionArea'
};

var _controllerActions = {
    Action: 'ExecuteAction',
    Controllers: {
        CommonExecute: 'Common',
        MaintainPCR: 'MaintainPCR',
        MaterialPlanner: 'MaterialPlanner',
        ReleasePCL: 'ReleasePCL',
        PCLWorkflow: 'PCLWorkflow'
    }

};

function fnGetSelectedRowsCheckboxes(rowNumber = undefined) {
    var rowSelector = null;
    var checkboxSelector = _checkedCheckboxSelector;
    if (rowNumber === undefined) {
        rowSelector = 'tr';
    } else {
        rowSelector = 'tr[' + _attributes.ItemId + ' = ' + rowNumber + ']'
    }
    var a = fnGetTableSelectorString() + ' ' + rowSelector + ' ' + checkboxSelector;
    console.log('a', a);
    return $(a);
}

function fnGetCurrentRowCheckbox(rowNumber) {
    var rowSelector = 'tr[' + _attributes.ItemId + ' = ' + rowNumber + ']';
    var checkboxSelector = _anyCheckboxSelector;
    var a = fnGetTableSelectorString() + ' ' + rowSelector + ' ' + checkboxSelector;
    console.log('a', a);
    return $(a);
}

function fnSetLocationAddress(row, value) {
    $(row).attr(_attributes.LocationAddress, value);
}

function fnGetControllerActionName(isCommon = false) {
    var returned = null;
    if (isCommon === false) {
        switch (_currentScreen.Name) {
            case _screens.GeneratePCR.Name: {
                returned = _controllerActions.Controllers.MaintainPCR + '/' + _controllerActions.Action;
                break;
            }
            case _screens.MaterialPlanner.Name: {
                returned = _controllerActions.Controllers.MaterialPlanner + '/' + _controllerActions.Action;
                break;
            }
            case _screens.ReleasePCL.Name: {
                returned = _controllerActions.Controllers.ReleasePCL + '/' + _controllerActions.Action;
                break;
            }
            case _screens.MaintainReturnDistribution.Name: {
                returned = _controllerActions.Controllers.ReleasePCL + '/' + _controllerActions.Action;// It is same intentionally
                break;
            }
            case _screens.PCLWorkflow.Name: {
                returned = _controllerActions.Controllers.PCLWorkflow + '/' + _controllerActions.Action;// It is same intentionally
                break;
            }
        }
    } else {
        returned = _controllerActions.Controllers.CommonExecute + '/' + _controllerActions.Action;
    }
    return returned;
}

function fnValidateStockNumber(row, stockNumber, stockRevision, useAlternateStock) {
    var id = $(row).attr(_attributes.ItemId);
    var data = JSON.stringify({ ActionName: _actionType.ValidateStockNumber, Parameters: { Id: id, StockNumber: stockNumber, StockRevision: stockRevision, UseAlternateStock: useAlternateStock } });
    return fnExecuteAction(data, false);
    //return Promise.resolve({ Value: 'Ok' });
}

function fnValidateArea(totalSum, rowValues) {
    //TODO
    let saveDataObj = {
        TotalArea: totalSum,
        Revision: rowValues.Revision
    }
    var data = JSON.stringify({ ActionName: _actionType.ValidateReturnDistributionArea, Parameters: { PCLNumber: _pclNumber, TotalArea: saveDataObj.TotalArea, Revision: saveDataObj.Revision } });

    return fnExecuteAction(data, false);

}

function fnValidateAreaWrapper(totalSum, rowValues, result) {
    return fnValidateArea(totalSum, rowValues)
        .then(
            r => {
                result.Valid = true;
                result.Message = r.Value;
                return Promise.resolve(result);
            },
            e => {
                result.Valid = false;
                result.Message = e.Value;
                return Promise.reject(result);
            }
        );
}

function fnValidateRDColumns(row, column, value, result) {
    let elOdds = fnFindElementByColumnName(row, _columns.OddShape.Name);
    let elArea = fnFindElementByColumnName(row, _columns.Area.Name);
    let elLeng = fnFindElementByColumnName(row, _columns.Length.Name);
    let elWidt = fnFindElementByColumnName(row, _columns.Width.Name);
    var rowValues = fnGetRowValues(row);

    let dataTable1 = _tbl.DataTable();
    let data = dataTable1.rows().data();
    let totalSum = 0.00;

    for (let i = 0; i < data.count(); i++) {
        let tr = $(fnGetTableSelectorString() + ' tr[' + _attributes.ItemId + '=' + data[i].Id + ']');
        let areaVal = Number.parseFloat(fnFindValueByColumnName(tr, _columns.Area.Name));
        totalSum += areaVal;
    }

    switch (column.Name) {
        case _columns.Area.Name: {
            if (fnIsEmptyValue(value) === true && fnIsOddShape(elOdds.val())) {
                result.Valid = false;
                result.Message = column.EmptyMessage;
                return Promise.reject(result);
            }
            else if (value <= 0) {
                result.Valid = false;
                result.Message = column.InvalidMessage;
                return Promise.reject(result);
            }
            else if (value > rowValues.MaximumReturnArea || rowValues.MaximumReturnArea < totalSum) {
                result.Valid = false;
                result.Message = column.InvalidMessage2 + " : " + rowValues.MaximumReturnArea;
                return Promise.reject(result);
            } else {// Area Greater than Max Area Check
                isValid = true;
                elArea.val(value.toFixed(2));

                //DbSide Validations
                return fnValidateAreaWrapper(totalSum, rowValues, result);
            }
            break;
        }
        case _columns.Length.Name: {
            //console.log(elOdds.val(), column.Name);
            if (fnIsEmptyValue(value) === true && (!fnIsOddShape(elOdds.val()))) { // (elOdds.val() == "No")
                result.Valid = false;
                result.Message = column.EmptyMessage;
                return Promise.reject(result);
            }
            else if (value <= 0 && (!fnIsOddShape(elOdds.val()))) { //// (elOdds.val() === "No")
                result.Valid = false;
                //console.log("Hello", fnIsOddShape(elOdds.val()));
                result.Message = column.InvalidMessage;
                return Promise.reject(result);
            }
            else if (value > rowValues.LengthOriginal) {
                result.Valid = false;
                result.Message = column.InvalidMessage2 + " : " + rowValues.LengthOriginal;
                return Promise.reject(result);
            } else {// Area Greater than Max Area Check
                isValid = true;
                if (fnIsPlate(rowValues) === false) {//means a pipe
                    elArea.val((elLeng.val()).toFixed(2));
                }
                else if ((!fnIsOddShape(elOdds.val()))) {// (elOdds.val() === "No")
                    elLeng.val(value.toFixed(2));
                    elArea.val((elLeng.val() * elWidt.val()).toFixed(2));
                }

                //DbSide Validations
                return fnValidateAreaWrapper(totalSum, rowValues, result);
            }
            break;
        }
        case _columns.Width.Name: {
            if (fnIsEmptyValue(value) === true && (!fnIsOddShape(elOdds.val()))) { //(elOdds.val() === "No")
                result.Valid = false;
                result.Message = column.EmptyMessage;
                return Promise.reject(result);
            }
            else if (value <= 0 && fnIsPlate(rowValues) === false && (!fnIsOddShape(elOdds.val()))) { // (elOdds.val() === "No")
                result.Valid = false;
                result.Message = column.InvalidMessage;
                return Promise.reject(result);
            }
            else if (value > rowValues.WidthOriginal) {
                result.Valid = false;
                result.Message = column.InvalidMessage2 + " : " + rowValues.WidthOriginal;
                return Promise.reject(result);
            } else if ((!fnIsOddShape(elOdds.val()))) {// Area Greater than Max Area Check // (elOdds.val() === "No")
                isValid = true;
                elWidt.val(value.toFixed(2));
                elArea.val((elLeng.val() * elWidt.val()).toFixed(2));
            }

            //DbSide Validations
            return fnValidateAreaWrapper(totalSum, rowValues, result);

            break;
        }
    }
}

function fnOddShapeEnableDisable(row, value) {
    let elLeng = fnFindElementByColumnName(row, _columns.Length.Name);
    let elWidt = fnFindElementByColumnName(row, _columns.Width.Name);
    let elArea = fnFindElementByColumnName(row, _columns.Area.Name);
    var rowValues = fnGetRowValues(row);

    if (fnIsOddShape(value)) {
        elLeng.val(0);
        elWidt.val(0);
        fnEnableDisableElement(elLeng, true, true);
        fnEnableDisableElement(elWidt, true, true);
        fnEnableDisableElement(elArea, false, true);
    }
    else {
        //elArea.val(0);
        fnEnableDisableElement(elLeng, false, true);
        fnEnableDisableElement(elWidt, false, true);
        fnEnableDisableElement(elArea, true, true);
        if (fnIsPlate(rowValues) === false) {//means a pipe
            fnEnableDisableElement(elWidt, true, true);
            elWidt.val(0);
        }
    }
}

function fnValidateGridCell(element, columnName, value) {
    var result = {};
    var message = '';
    var isValid = false;
    var row = fnGetClosestRow(element); // getting closest row
    result.Element = element;
    result.ColumnName = columnName;
    result.RowId = $(element.parents('tr')[0]).attr(_attributes.ItemId);
    if (_columns[columnName].DbValidate) {
        switch (columnName) {
            case _columns.Area.Name: {
                value = Number.parseFloat(value);
                if (_currentScreen.Name == _screens.MaintainReturnDistribution.Name) {
                    return fnValidateRDColumns(row, _columns.Area, value, result);
                }
                else {
                    result.Valid = true;
                    result.Message = r.Value;
                    return Promise.resolve(result);
                }
                break;
            }

            case _columns.Length.Name: {
                if (_currentScreen.Name == _screens.MaintainReturnDistribution.Name) {
                    value = Number.parseFloat(value);
                    return fnValidateRDColumns(row, _columns.Length, value, result);
                }
                else {
                    result.Valid = true;
                    result.Message = r.Value;
                    return Promise.resolve(result);
                }
                break;
            }

            case _columns.Width.Name: {
                value = Number.parseFloat(value);
                if (_currentScreen.Name == _screens.MaintainReturnDistribution.Name) {
                    return fnValidateRDColumns(row, _columns.Width, value, result);
                }
                else {
                    result.Valid = true;
                    result.Message = r.Value;
                    return Promise.resolve(result);
                }
                break;
            }

            default: {
                result.Valid = true;
                return Promise.resolve(result);
                break;
            }
        }
    }
    else {
        switch (columnName) {
            case _columns.Location.Name: {
                //var el = fnFindElementByColumnName(row, _columns.WCDeliver.Name);
                if (fnIsEmptyValue(value) === true) {
                    isValid = false;
                    message = _columns.Location.EmptyMessage;
                    //el.prop('disabled', true);
                } else {
                    //el.prop('disabled', false);
                    isValid = fnValidateGridLookUpValue(element);
                    if (isValid === false) {
                        message = _columns.Location.InvalidMessage;
                    }
                }
                break;
            }
            case _columns.SubLocation.Name: {
                if (fnIsEmptyValue(value) === true) {
                    isValid = false;
                    message = _columns.SubLocation.EmptyMessage;
                } else {
                    isValid = true;
                }
                break;
            }
            case _columns.CuttingLocation.Name: {
                if (fnIsEmptyValue(value) === true) {
                    isValid = false;
                    message = _columns.CuttingLocation.EmptyMessage;
                } else {
                    isValid = fnValidateGridLookUpValue(element);
                    if (isValid === false) {
                        message = _columns.CuttingLocation.InvalidMessage;
                    }
                }
                break;
            }
            case _columns.PCRType.Name: {
                if (fnIsEmptyValue(value) === true) {
                    isValid = false;
                    message = _columns.PCRType.EmptyMessage;
                } else {
                    isValid = fnValidateGridLookUpValue(element);
                    if (isValid === false) {
                        message = _columns.PCRType.InvalidMessage;
                    }
                }
                break;
            }
            case _columns.MovementType.Name: {
                if (fnIsEmptyValue(value) === true) {
                    isValid = false;
                    message = _columns.MovementType.EmptyMessage;
                } else {
                    isValid = fnValidateGridLookUpValue(element);
                    if (isValid === false) {
                        message = _columns.MovementType.InvalidMessage;
                    }
                }
                break;
            }
            case _columns.UseAlternateStock.Name: {

                if (fnIsEmptyValue(value) === true) {
                    isValid = false;
                    message = _columns.UseAlternateStock.EmptyMessage;
                }
                else {
                    isValid = fnValidateGridLookUpValue(element);
                    if (isValid === false) {
                        message = _columns.UseAlternateStock.InvalidMessage;
                    }
                }
                break;
            }
            case _columns.StockNumber.Name: {
                //if (fnIsEmptyValue(value) === true) {
                //    isValid = false;
                //    message = _columns.StockNumber.EmptyMessage;
                //}
                //else {
                //isValid = _columns.StockNumber.IsValid;
                //if (isValid !== true) {
                //    message = _columns.StockNumber.InvalidMessage;
                //}


                //}
                //Let it be validated through before rewrite method
                isValid = true;
                break;
            }
            case _columns.OddShape.Name: {
                if (fnIsEmptyValue(value) === true) {
                    isValid = false;
                    message = _columns.OddShape.EmptyMessage;
                }
                else {
                    isValid = fnValidateGridLookUpValue(element);
                    if (isValid === false) {
                        message = _columns.OddShape.InvalidMessage;
                    }
                }
                break;
            }
            case _columns.WCDeliver.Name: {

                if (fnIsEmptyValue(value) === true) {
                    isValid = false;
                    message = _columns.WCDeliver.EmptyMessage;
                } else {
                    isValid = fnValidateGridLookUpValue(element);
                    if (isValid === false) {
                        message = _columns.WCDeliver.InvalidMessage;
                    }
                }
                break;
            }
            case _columns.PCRRequirementDate.Name: {
                if (value === null) {
                    isValid = false;
                    message = _columns.PCRRequirementDate.EmptyMessage;
                } else {
                    isValid = fnValidateDate(value);
                    if (isValid === false) {
                        message = _columns.PCRRequirementDate.InvalidMessage;
                    } else {
                        value = typeof value === 'string' ? moment(value, _dateFormat) : moment(value);
                        value = value.utc();
                        var d = moment(new Date());
                        d = d.utc();
                        if (value.isAfter(d) === false) {
                            isValid = false;
                            message = _columns.PCRRequirementDate.DateMessage;
                        } else {
                            isValid = true;
                        }
                    }
                }
                //isValid = true;
                break;
            }
            case _columns.Priority.Name: {
                if (value !== null && value.length > 0) {
                    if (isNaN(value) === true) {
                        isValid = false;
                        message = _columns.Priority.EmptyMessage;
                    } else {
                        value = Number.parseInt(value);
                        if (value > 3) {
                            isValid = false;
                            message = _columns.Priority.InvalidMessage;
                        } else {
                            isValid = true;
                        }
                    }
                } else {
                    isValid = true;
                }
                break;
            }
            case _columns.LengthModified.Name: {
                if (fnIsEmptyValue(value) === true) {
                    isValid = false;
                    message = _columns.LengthModified.EmptyMessage;
                } else {
                    value = Number.parseFloat(value);
                    if (value < 0) {
                        isValid = false;
                        message = _columns.LengthModified.InvalidMessage;
                    } else {
                        isValid = true;
                    }
                }
                break;
            }
            case _columns.WidthModified.Name: {
                if (fnIsEmptyValue(value) === true) {
                    isValid = false;
                    message = _columns.WidthModified.EmptyMessage;
                } else {
                    value = Number.parseFloat(value);
                    if (value < 0) {
                        isValid = false;
                        message = _columns.WidthModified.InvalidMessage;
                    } else {
                        isValid = true;
                    }
                }
                break;
            }
            case _columns.RequiredQuantity.Name: {
                value = Number.parseFloat(value);
                //console.log(value);
                if (fnIsEmptyValue(value) === true) {
                    isValid = false;
                    message = _columns.RequiredQuantity.EmptyMessage;
                } else {
                    if (value <= 0) {
                        isValid = false;
                        message = _columns.RequiredQuantity.InvalidMessage;
                    } else {
                        isValid = true;
                    }
                }
                break;
            }
            case _columns.NestedQuantity.Name: {
                value = Number.parseFloat(value);
                if (value < 0) {
                    isValid = false;
                    message = _columns.NestedQuantity.InvalidMessage;
                } else {
                    isValid = true;
                }
                break;
            }
            case _columns.AreaConsumed.Name: {
                value = Number.parseFloat(value);
                if (value < 0) {
                    isValid = false;
                    message = _columns.AreaConsumed.InvalidMessage;
                } else {
                    isValid = true;
                }
                break;
            }
            case _columns.PlannerRemark.Name: {
                if (fnIsEmptyValue(value) === true) {
                    isValid = false;
                    message = _columns.PlannerRemark.EmptyMessage;
                } else {
                    isValid = true;
                }
                break;
            }
            case _columns.AlternateStockRemark.Name: {
                let el = fnFindElementByColumnName(row, _columns.UseAlternateStock.Name);
                if (fnIsEmptyValue(value) === true && (fnIsUseAlternateStockSelected(el.val()))) {
                    isValid = false;
                    message = _columns.AlternateStockRemark.EmptyMessage;
                } else {
                    isValid = true;
                }
                break;
            }
            case _columns.ReturnBalance.Name: {
                value = Number.parseFloat(value);
                //console.log(value);
                if (value < 0) {
                    isValid = false;
                    message = _columns.ReturnBalance.InvalidMessage;
                } else {
                    isValid = true;
                }

                break;
            }
            case _columns.TotalCuttingLength.Name: {
                value = Number.parseFloat(value);
                //console.log(value);
                if (value <= 0) {
                    isValid = false;
                    message = _columns.TotalCuttingLength.InvalidMessage;
                } else {
                    isValid = true;
                }

                break;
            }


            default: {
                isValid = true;
                break;
            }
        }
        result.Message = message;
        result.Valid = isValid;
        if (isValid) {
            return Promise.resolve(result);
        } else {
            return Promise.reject(result);
        }
    }
}

function fnIsEmptyValue(value) {
    var returned = false;
    try {
        if (value === null) {
            returned = true;
        } else {
            if (value === undefined) {
                returned = true;
            } else {
                value = value.toString();
                if (value.length === 0) {
                    returned = true;
                }
            }
        }
    } catch (e) {
        returned = true;
    }

    return returned;
}

function fnIsPlate(rowValues) {
    return rowValues.ItemType === 1;
}

function fnIsOddShape(value) {
    return fnIsYesValue(value);
}

function fnIsUseAlternateStockSelected(value) {
    return fnIsYesValue(value);
}

function fnIsYesValue(value) {
    return value === _listYesNo.find(x => x.CatID === 1).CatDesc;
}

function fnGetRowValues(row) {
    /* var dt = _tbl.DataTable();
    var data = dt.row(row).data();
    return data;*/
    return _originalData;
}

function fnFindElementByColumnName(row, columnName, tagName = undefined) {
    tagName = tagName == undefined ? 'input' : tagName;
    return row.find(tagName + '[' + _attributes.ColumnName + '=' + columnName + ']');
}

function fnFindElementByClassName(row, className, tagName) {
    tagName = tagName == undefined ? '' : tagName;
    return $(row).find(tagName + '.' + className);
}

function fnFindValueByColumnName(row, columnName, tagName = undefined) {
    var element = fnFindElementByColumnName(row, columnName, tagName);
    return element.val();

}

function fnFindValueFromData(row, column) {
    var dataTable = _tbl.DataTable();
    var data = dataTable.row(row).data();
    //console.log('from data', data);
    var value = data[column];
    if ($(value).val() !== undefined) {
        value = $(value).val();
    }
    return value;
}

function fnClearState() {
    setTimeout(() => {
        _selectedRow.RowNumber = null;
        _selectedRow.Columns = null;
        _last.RowNumber = null;
        _last.Values = null;
        //console.log('after clearing _selectedRow', _selectedRow);
    }, 100);
}

function fnShouldSave(goingAwayFromRowNumber = undefined) {
    var proceed = fnIsEditedRowFocussedOut(goingAwayFromRowNumber);
    console.log('isRowChanged', goingAwayFromRowNumber, _selectedRow, proceed);
    if (proceed === true) {
        var isDataChanged = fnIsDataChangedInRow();
        console.log('isDataChanged', isDataChanged);
        if (isDataChanged === true) {
            return fnValidateAndSaveRow(_selectedRow.RowNumber)
                .then(
                    s => {
                        return Promise.resolve(s);
                    },
                    e => {
                        return Promise.reject(e);
                    }
                );
        }
    }
    return Promise.resolve(null);
}

function fnRefreshScreen(rowNumber = undefined) {
    switch (_currentScreen.Name) {
        case _screens.GeneratePCR.Name: {
            fnLoadPCRData();
            break;
        }
        case _screens.MaterialPlanner.Name: {
            fnLoadMaterialPlannerData();
            break;
        }
        case _screens.ReleasePCL.Name: {
            fnLoadReleasePCLData();
            break;
        }
        case _screens.MaintainReturnDistribution.Name: {
            fnRefreshMaintainReturnDistribution(_rowNumber);
            break;
        }
    }
}

function fnIsEditableRow(rowNumber) {
    var tr = $(fnGetTableSelectorString() + ' tr[' + _attributes.ItemId + '=' + rowNumber + ']');
    return tr.hasClass(_classes.DisabledRow) === false;
}

function fnGetDataForEmailColumns(cols, rowNumber, otherDataObj) {
    var emailColumnsArray = [];
    var row = $(fnGetTableSelectorString() + ' tr[' + _attributes.ItemId + ' = ' + rowNumber + ']');

    var assign = (r, rN) => {
        var obj = {};
        obj['Id'] = rN;
        for (var cnt = 0; cnt < cols.length; cnt++) {
            var column = _columns[cols[cnt].Name];
            var value = fnFindValueFromData(r, column.Name);
            obj[column.Name] = value;
        }
        return obj;
    }

    //rows that are for assigning Email
    emailColumnsArray.push(assign(row, rowNumber));
    return emailColumnsArray;
}

function fnSendAnEmail(cols, rowNumber, sendEmailForActionName, otherDataObj = undefined) {
    var emailRowsArray = fnGetDataForEmailColumns(cols, rowNumber, otherDataObj);
    var data = JSON.stringify({ ActionName: _actionType.SendAnEmail, Parameters: { Rows: emailRowsArray, Id: rowNumber, SendEmailForActionName: sendEmailForActionName, Remarks: otherDataObj["Remarks"] } });
    return fnExecuteAction(data, true);
}

function fnGetColumnsToValidate(rowNumber = undefined) {
    var cols = [];
    cols = _columnsForSave;
    rowNumber = rowNumber === undefined ? _selectedRow.RowNumber : rowNumber;
    var i = 0;
    if (rowNumber !== null && rowNumber.toString() === i.toString()) {
        if (_additionalColumnsForAddNewRow != null) {
            cols = cols.concat(_additionalColumnsForAddNewRow);
        }
    }
    var validateCols = [];
    for (var cnt = 0; cnt < cols.length; cnt++) {
        if (cols[cnt].IsHidden !== true) {
            validateCols.push(cols[cnt].Name);
        }
    }
    return {
        allCols: cols, validateCols: validateCols
    };
}

function fnIsDataChangedInRow() {
    var returned = false;
    if (_selectedRow.RowNumber !== null && _selectedRow.Columns !== null) {
        if (Object.keys(_selectedRow.Columns).length > 0 && _selectedRow.Columns.constructor === Object) {
            for (var col in _selectedRow.Columns) {
                var areSame = fnCompare(_selectedRow.Columns[col][_propertyNames.CleanValue], _selectedRow.Columns[col][_propertyNames.DirtyValue]);
                if (areSame === false) {
                    returned = true;
                    //console.clear();
                    //console.log('s', _selectedRow);
                    break;
                }
            }
        }
    }
    return returned;
}

function fnCompare(oldValue, newValue) {
    newValue = newValue === null || newValue === undefined ? '' : newValue.toString();
    oldValue = oldValue === null || oldValue === undefined ? '' : oldValue.toString();
    return newValue.toLowerCase() === oldValue.toLowerCase();
}

function fnGetData(controller, action, dataType) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: WebsiteURL + "/" + "/" + controller + "/" + action,
            dataType: dataType,
            type: 'POST',
            beforeSend: (e) => {
                //fnShowMessage('loading', _messageType.Information);
                showLoading(true);
            }
        })
            .done(resolve)
            .fail(reject)
            .always(() => {
                showLoading(false);
            });
    });
}

function fnGetDataForInput(controller, action, dataType, data) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: WebsiteURL + "/" + "/" + controller + "/" + action,
            dataType: dataType,
            data: { input: data },
            type: 'POST',
            beforeSend: (e) => {
                //fnShowMessage('loading', _messageType.Information);
                showLoading(true);
            }
        })
            .done(resolve)
            .fail(reject)
            .always(() => {
                showLoading(false);
            });
    });
}

function fnExecuteAction(data, isCommon = false) {
    console.log('action', data);
    return new Promise((resolve, reject) => {
        $.ajax({
            url: WebsiteURL + '/PCR/' + fnGetControllerActionName(isCommon),
            type: 'POST',
            dataType: 'json',
            data: { input: data },
            beforeSend: (e) => {
                //fnShowMessage('updating', _messageType.Information);
                showLoading(true);
            }
        })
            .done(resolve)
            .fail(reject)
            .always(() => {
                showLoading(false);
            });

    });
}

function fnIsEmptyObject(obj) {
    return Object.keys(obj).length === 0 && obj.constructor === Object
}

function fnSetGridColumnInState(state, uiColumnName, dbColumnName, value, rowNumber, forceClean = false) {
    //console.log('called', rowNumber);
    _selectedRow.RowNumber = rowNumber;
    if (_selectedRow.Columns === null) {
        _selectedRow.Columns = {};
    }
    if (_selectedRow.Columns[uiColumnName] === undefined) {
        _selectedRow.Columns[uiColumnName] = {};
        _selectedRow.Columns[uiColumnName][_propertyNames.DbColumnName] = dbColumnName;
        _selectedRow.Columns[uiColumnName][_propertyNames.DirtyValue] = null;
        _selectedRow.Columns[uiColumnName][_propertyNames.CleanValue] = null;
    }
    if (state === _stateTypes.Clean) {
        if (forceClean == true) {
            _selectedRow.Columns[uiColumnName][_propertyNames.CleanValue] = value;
        } else {
            var prevValue = _selectedRow.Columns[uiColumnName][_propertyNames.CleanValue];
            if (prevValue !== null && prevValue !== undefined) {
                //skip;
                //console.log('skipping');
            } else {
                _selectedRow.Columns[uiColumnName][_propertyNames.DirtyValue] = null;
                _selectedRow.Columns[uiColumnName][_propertyNames.CleanValue] = value;
            }
        }
    } else if (state === _stateTypes.Dirty) {
        _selectedRow.Columns[uiColumnName][_propertyNames.DirtyValue] = value;

    }

}

function fnBeforeSave(cols, rowNumberToSave) {
    //console.log("before calling");
    if (_currentScreen.HasBeforeReWrite === true) {
        var saveDataObj = fnGenerateSaveRowData(cols, rowNumberToSave);
        var data = JSON.stringify({ ActionName: _actionType.BeforeReWrite, Parameters: { Caller: _currentScreen.Name, Rows: saveDataObj.RowsArray } });
        //console.log("calling");
        return fnExecuteAction(data, true);
    }
    else {
        return Promise.resolve({ Key: true });
    }
}

function fnBeforeValidate(rowNumberToSave = undefined) {
    if (fnIsEmptyObject(_selectedRow) === true || _selectedRow.RowNumber === null || _selectedRow.RowNumber === undefined) {
        return Promise.resolve(null);
    }
    return fnValidateAndSaveRow(rowNumberToSave);
}

function fnValidateAndSaveRow(rowNumberToSave = undefined, isPartOfMultiple = undefined) {
    if (_isExecuting === true)
        return Promise.resolve(null);
    var colsObject = fnGetColumnsToValidate(rowNumberToSave);
    var validateCols = colsObject.validateCols;
    var cols = colsObject.allCols;
    var r = _isDialogBoxOpened === true ? null : $(fnGetTableSelectorString() + ' tr[' + _attributes.ItemId + ' = ' + rowNumberToSave + ']');
    return fnValidateRow(validateCols, rowNumberToSave)
        .then(
            result => {
                if (result.IsRedundantCall === true) {
                    return Promise.resolve({
                        IsRedundantCall: true
                    });
                }
                if (_isDialogBoxOpened !== true) {
                    var v = fnPrepareValuesObject(validateCols, r);
                    _last.RowNumber = rowNumberToSave;
                    _last.Values = v;
                }

                fnHandleValidatedRow(result);
                return fnBeforeSave(cols, rowNumberToSave)
                    .then(
                        beforeSaveResult => {
                            if (beforeSaveResult.Key == true) {
                                return fnSaveRow(cols, rowNumberToSave)
                                    .then(
                                        saveResult => {
                                            _isExecuting = false;
                                            if (saveResult.Key === true) {
                                                if (isPartOfMultiple === undefined) {
                                                    fnShowMessage(_messages.Success, _messageType.Success);
                                                }
                                                fnClearState();
                                                return Promise.resolve(saveResult);
                                            } else {
                                                fnShowWarningOrErrorMessage(saveResult);
                                                fnHandleRowBG(r, true);
                                                return Promise.reject(saveResult);
                                            }
                                        },
                                        err => {
                                            _isExecuting = false;
                                            fnShowMessage('Unexpected error ' + JSON.stringify(err), _messageType.Error);
                                            fnHandleRowBG(r, true);
                                            return Promise.reject(null);
                                        }
                                    );
                            }
                            else if (beforeSaveResult.IsInformation === true) {
                                return fnShowConfirmDialog(beforeSaveResult.Value)
                                    .then(
                                        dialogResult => {
                                            if (dialogResult === true) {
                                                return fnSaveRow(cols, rowNumberToSave)
                                                    .then(
                                                        saveResult => {
                                                            _isExecuting = false;
                                                            if (saveResult.Key === true) {
                                                                fnShowMessage(_messages.Success, _messageType.Success);
                                                                fnClearState();
                                                                return Promise.resolve(saveResult);
                                                            } else {
                                                                fnShowWarningOrErrorMessage(saveResult);
                                                                fnHandleRowBG(r, true);
                                                                return Promise.reject(saveResult);
                                                            }
                                                        },
                                                        err => {
                                                            _isExecuting = false;
                                                            fnShowMessage('Unexpected error ' + JSON.stringify(err), _messageType.Error);
                                                            fnHandleRowBG(r, true);
                                                            return Promise.reject(null);
                                                        }
                                                    );
                                            }
                                        }
                                    );
                            }
                            else {
                                fnShowMessage(beforeSaveResult.Value, _messageType.Error);
                                fnHandleRowBG(r, true);
                                return Promise.reject(beforeSaveResult);
                            }
                        },
                        err => {
                            fnShowMessage('Unexpected error ' + JSON.stringify(err), _messageType.Error);
                            fnHandleRowBG(r, true);
                            return Promise.reject(null);
                        }
                    );

            },
            err => {
                if (err !== null) {
                    if (_isDialogBoxOpened !== true) {
                        var v = fnPrepareValuesObject(validateCols, r);
                        _last.RowNumber = rowNumberToSave;
                        _last.Values = v;
                    }
                    fnHandleValidatedRow(err);
                    fnShowInValidMessage(err);
                    fnHandleRowBG(r, true);
                    return Promise.reject(err.filter(x => x.Valid == false));
                }
                return Promise.reject(null);
            }
        );
}

function fnGetRowColumnValue(row, column) {
    var value = null;
    switch (column.Type) {
        case _columnType.Lookup: {
            if (column.IsHidden === true) {
                value = fnFindValueFromData(row, column.Name);
            } else {
                value = fnGetIdValueFromList(column.Name, fnFindValueByColumnName(row, column.Name));
            }

            break;
        }
        default: {
            if (column.IsHidden === true) {
                value = fnFindValueFromData(row, column.Name);
            } else {
                value = fnFindValueByColumnName(row, column.Name);
            }

            break;
        }
    }
    return value;
}

function fnGenerateSaveRowData(cols, rowNumber) {
    rowNumber = typeof (rowNumber) === 'string' ? parseInt(rowNumber) : rowNumber;
    console.log('rr', rowNumber, cols);
    var row = null;
    var rowsArray = [];
    var saveDataObj = {
        RowsArray: [],
        Mode: 0,
        IsDuplicate: false
    }

    var assign = (r, rN) => {
        var obj = {};
        obj['Id'] = rN;

        for (var cnt = 0; cnt < cols.length; cnt++) {
            var column = _columns[cols[cnt].Name];
            var value = fnGetRowColumnValue(r, column);
            //console.log('ca', column, value);
            obj[column.Name] = value;
        }
        return obj;
    }
    switch (rowNumber) {
        case 0: { //Add
            row = $(fnGetTableSelectorString() + ' tr[' + _attributes.ItemId + '=' + rowNumber + ']');
            rowsArray.push(assign(row, rowNumber));
            saveDataObj.Mode = 0;
            saveDataObj.RowsArray = rowsArray;
            break;
        }
        case -1: { //Bulk rows update
            saveDataObj.Mode = 1;
            row = $('div#' + _dialogBoxContainer);
            var rowIds = fnGetRowNumbersFromCheckboxes(fnGetSelectedRowsCheckboxes());
            console.log('roi', rowIds, row);
            for (var cnt = 0; cnt < rowIds.length; cnt++) {
                var item = assign(row, parseInt(rowIds[cnt]));
                console.log('item', item);
                rowsArray.push(item);
            }
            saveDataObj.RowsArray = rowsArray;
        }
        default: { //Single row update/delete
            row = $(fnGetTableSelectorString() + ' tr[' + _attributes.ItemId + '=' + rowNumber + ']');
            rowsArray.push(assign(row, rowNumber));
            saveDataObj.RowsArray = rowsArray;
            saveDataObj.Mode = 1;
            break;
        }
    }
    return saveDataObj;
}

function fnSaveRow(cols, rowNumber, isDuplicate = false, actionMode = undefined) {
    _isExecuting = true;
    var saveDataObj = null;
    saveDataObj = fnGenerateSaveRowData(cols, rowNumber);
    saveDataObj.IsDuplicate = isDuplicate;
    if (saveDataObj.IsDuplicate === true) {
        saveDataObj.Mode = 0;
    }
    if (actionMode !== undefined) {
        saveDataObj.Mode = actionMode;
    }
    var data = JSON.stringify({ ActionName: _actionType.AddOrEditRows, Parameters: { Rows: saveDataObj.RowsArray, Caller: _currentScreen.Name, Mode: saveDataObj.Mode, IsDuplicate: saveDataObj.IsDuplicate } });
    console.log('going to save data', data);
    return fnExecuteAction(data, true);
}

function fnShowConfirmDialog(message) {
    var deferred = $.Deferred();
    bootbox.confirm(message, function (result) {
        deferred.resolve(result);
    });
    return deferred.promise();
}

function fnShowPromptDialog(message) {
    var deferred = $.Deferred();
    bootbox.prompt(message, function (result) {
        deferred.resolve(result);
    });
    return deferred.promise();
}

function fnShowCustomDialog(title, html, parentSelector, elements) {
    var deferred = $.Deferred();
    bootbox.dialog({
        title: title,
        message: html,
        size: 'large',
        buttons: {
            cancel: {
                label: "Cancel",
                className: 'btn-danger',
                callback: function () {
                    //console.log('Custom cancel clicked');
                    deferred.resolve(false);
                }
            },
            ok: {
                label: "OK",
                className: 'btn-info',
                callback: function () {
                    //console.log('Custom OK clicked');
                    //debugger
                    var parent = $(parentSelector);
                    var isValid = true;
                    var msg = '';
                    var values = {};
                    for (var cnt = 0; cnt < elements.length; cnt++) {
                        var obj = elements[cnt];
                        var element = $(parent).find(obj.Selector);
                        var val = element.val();
                        if (val === null || val.length === 0) {
                            isValid = false;
                            msg = msg.length === 0 ? obj.Message : msg + ', ' + obj.Message;
                        } else {
                            values[obj.Name] = val;
                        }
                    }
                    if (isValid === true) {
                        values.IsValid = true;
                        deferred.resolve(values);
                    } else {
                        deferred.reject('Please select ' + msg);
                        //return false;
                    }

                }
            }

        }
    });
    return deferred.promise();
}

function fnIsPCRTypeAModifiedType(value) {
    //Warning Error Prone if enum order changed...
    //Using Find would be same as using string value
    return value === _columns.PCRType.Lookup[2].CatDesc || value === _columns.PCRType.Lookup[3].CatDesc || value === _columns.PCRType.Lookup[4].CatDesc || value === _columns.PCRType.Lookup[5].CatDesc;
}

function fnGetIdValueFromList(columnName, textValue) {
    var column = _columns[columnName];
    if (column.IsIdValueSame !== undefined && column.IsIdValueSame === true) {
        return textValue;
    }
    var list = column.Lookup;
    var idValue = null;
    if (list !== null && list.length) {
        for (var cnt = 0; cnt < list.length; cnt++) {
            if (list[cnt].CatDesc === textValue) {
                idValue = list[cnt].CatID;
                break;
            }
        }
    }
    return idValue;
}

function fnIsEditedRowFocussedOut(goingAwayFromRowNumber) {
    var returned = false;
    if (_isExecuting === true)
        returned = false;
    else {
        var stateRowNumber = _selectedRow.RowNumber;
        console.log('state', stateRowNumber);
        console.log('goingawayfrom', goingAwayFromRowNumber);
        if (stateRowNumber === null || stateRowNumber === undefined) {
            returned = false;
        } else {
            if (goingAwayFromRowNumber === null || goingAwayFromRowNumber === undefined) {
                returned = true;
            } else {
                returned = goingAwayFromRowNumber.toString() === stateRowNumber.toString();
            }
        }
    }
    return returned;
}

function fnGetLookUpValueFromState(entity) {
    var target = _viewColumnStates[entity];
    var value = target !== undefined ? target[_propertyNames.LookUp] : null;
    return value !== undefined ? value : null;
}

function fnGetLookUpValueFromStateAsPromise(entity) {
    return Promise.resolve(fnGetLookUpValueFromState(entity));
}

function fnIsGridColumnValueInLookUp(columnName, value, parentValue = undefined) {
    var lookUp = null;
    var returned = false;
    switch (columnName) {
        case _columns.CuttingLocation.Name: {
            lookUp = fnGetCuttingLocationListForLocation(parentValue);
            break;
        }
        default: {
            lookUp = _columns[columnName].Lookup;
            break;
        }
    }

    if (lookUp !== null) {
        if (value !== null && value.length > 0) {
            for (var c = 0; c < lookUp.length; c++) {
                var v = lookUp[c].CatDesc !== undefined ? lookUp[c].CatDesc.toString() : lookUp[c].toString();
                if (value.toLowerCase() === v.toLowerCase()) {
                    returned = true;
                    break;
                }
            }
        }
    }
    if (returned === false) {
        //console.log(value, lookUp);
    }
    return returned;
}

function fnGetClosestRow(element) {
    element = $(element);
    if (_isDialogBoxOpened === true) {
        return element.parents('div#' + _dialogBoxContainer);
    }
    return element.parents('tr');
}

function fnGetLocation(element) {
    var row = fnGetClosestRow(element);
    var element = fnFindElementByColumnName(row, _columns.Location.Name);
    var location = element.val();
    location = location !== null ? location.toLowerCase() : '';
    var list = _columns.Location.Lookup;
    for (var c = 0; c < list.length; c++) {
        var a = list[c].CatDesc.toLowerCase();
        if (a === location) {
            location = list[c].CatID;
            break;
        }
    }
    return location;
}

function fnValidateGridLookUpValue(element) {
    var colName = element.attr(_attributes.ColumnName);
    var value = element.val();
    var parentValue = colName === _columns.CuttingLocation.Name ? fnGetLocation(element) : undefined;
    var isValid = fnIsGridColumnValueInLookUp(colName, value, parentValue);
    return isValid;
}

function fnValidateDate(value) {
    //console.log('date to be validated', value);
    if (value === null)
        return false;
    value = typeof value === 'string' ? moment(value, _dateFormat) : moment(value);
    return value.isValid();
}

function fnOnHideCalendar(calendar, event) {
    var element = $(calendar).find('input[type=text]');
    var value = event.date !== undefined ? event.date : null;
    var elementValue = element.val();
    value = value != null ? value : elementValue;
    //console.log('fnon', value);    
    fnWrapperToValidate(element, value);
}

function fnCustomize(element, columnName) {
    var row = fnGetClosestRow(element);
    var minValue = '0';
    var maxValue = '999999999';
    var isFound = false;
    var set = false;
    switch (columnName) {
        case _columns.RequiredQuantity.Name: {
            minValue = '1';
            //maxValue = row.find('.' + _classes.NoOfPieces).html();
            isFound = true;
            break;
        }
        case _columns.Priority.Name: {
            isFound = true;
            break;
        }
        case _columns.NestedQuantity.Name: {
            minValue = '0';
            maxValue = row.find('.' + _classes.NoOfPieces).html();
            isFound = true;
            break;
        }

    }
    if (isFound == false) {
        if (element.hasClass(_classes.Numeric)) {
            set = true;
        }
    } else {
        set = true;
    }
    if (set === true) {
        element.autoNumeric('init', { vMin: minValue, vMax: maxValue, aSep: '' });
    }
}

function fnGetFilteredStockNumberList(row) {
    var id = $(row).attr(_attributes.ItemId);
    var data = JSON.stringify({ ActionName: null, Parameters: { Id: id } });
    return fnGetDataForInput(_controllerActions.Controllers.MaterialPlanner, 'GetStockList', 'json', data);
}

function fnGetFilteredWorkLocation(element) {
    var row = fnGetClosestRow(element);
    var value = $(row).attr(_attributes.LocationAddress);
    var arr = _columns.WCDeliver.Lookup;
    var result = arr;
    switch (_currentScreen.Name) {
        case _screens.MaterialPlanner.Name: {
            break;
        }
        case _screens.GeneratePCR.Name: {
            break;
        }
    }
    if (_currentScreen.Name === _screens.MaterialPlanner.Name) {
        result = arr.filter(wcRow => wcRow.RelatedField === value);
    }
    else if (_currentScreen.Name === _screens.GeneratePCR.Name) {
        var location = fnFindValueByColumnName(row, _columns.Location.Name);
        var locationLookUp = _columns.Location.Lookup;
        if (fnIsGridColumnValueInLookUp(_columns.Location.Name, location) === true) {
            var locAddr = locationLookUp.filter(x => x.CatDesc === location)[0].RelatedField;
            result = arr.filter(wcRow => wcRow.RelatedField === locAddr);
        }
        else {
            result = [];
        }

    }
    return result;
}

function fnHandleRowBG(row, add) {
    if (row !== null && _currentScreen.CurrentTab.IsMultiOccurrence === true) {
        var element = $(row).find('td');// + _classes.SelectBox);
        var className = 'invalid-row';
        if (add === true) {
            $(element).addClass(className);
        } else {
            $(element).removeClass(className);
        }
    }
}

function fnOnFocusIn(element, isDirty) {
    element = $(element);
    var row = fnGetClosestRow(element);
    if (_isExecuting == false) {
        var dirty = isDirty !== undefined ? isDirty : false;
        if (dirty === false) {
            fnGrabAndSetGridColumnInState(element, _stateTypes.Clean);
        }
        else {
            console.log('got from focus out');
        }

        var colName = element.attr(_attributes.ColumnName);
        fnCustomize(element, colName);
        fnSetAttr();
        var isAutoComplete = element.hasClass(_classes.AutoComplete);
        if (isAutoComplete === true) {
            var onLocationChange = () => {
                var workCenterElement = fnFindElementByColumnName(row, _columns.WCDeliver.Name);
                workCenterElement.trigger('focusout');
            }

            var onPCRTypeChange = () => {
                var v = element.val();
                var disabled = 'disabled';
                var lengthModifiedElement = fnFindElementByColumnName(row, _columns.LengthModified.Name);
                var widthModifiedElement = fnFindElementByColumnName(row, _columns.WidthModified.Name);
                lengthModifiedElement.off('focusin');
                lengthModifiedElement.off('focusout');
                widthModifiedElement.off('focusin');
                widthModifiedElement.off('focusout');
                if (fnIsPCRTypeAModifiedType(v)) {
                    lengthModifiedElement.removeAttr(disabled);
                    widthModifiedElement.removeAttr(disabled);
                    lengthModifiedElement.on('focusin', function (e) {
                        fnOnFocusIn(this);
                    });
                    widthModifiedElement.on('focusin', function (e) {
                        fnOnFocusIn(this);
                    });
                    lengthModifiedElement.on('focusout', function (e) {
                        fnOnFocusOut(this);
                    });
                    widthModifiedElement.on('focusout', function (e) {
                        fnOnFocusOut(this);
                    });

                } else {
                    //var length = row.find('.' + _classes.Length).html();
                    //var width = row.find('.' + _classes.Width).html();
                    lengthModifiedElement.attr(disabled, disabled);
                    widthModifiedElement.attr(disabled, disabled);
                    lengthModifiedElement.val(lengthModifiedElement.attr(_attributes.PrevValue));
                    widthModifiedElement.val(widthModifiedElement.attr(_attributes.PrevValue));
                    fnGrabAndSetGridColumnInState(lengthModifiedElement, _stateTypes.Clean, true);
                    fnGrabAndSetGridColumnInState(lengthModifiedElement, _stateTypes.Dirty);
                    fnGrabAndSetGridColumnInState(widthModifiedElement, _stateTypes.Clean, true);
                    fnGrabAndSetGridColumnInState(widthModifiedElement, _stateTypes.Dirty);
                }
            }

            var onUseAlternateStockChange = () => {
                if (_currentScreen.Name == _screens.MaterialPlanner.Name) {
                    var value = element.val();
                    let alternateStockRemarkElement = fnFindElementByColumnName(row, _columns.AlternateStockRemark.Name);
                    if (fnIsUseAlternateStockSelected(value) === true) {
                        fnEnableDisableElement(alternateStockRemarkElement, false, true);
                    }
                    else {
                        fnEnableDisableElement(alternateStockRemarkElement, true, true);
                    }
                }
            }

            var onPCLNumberListChange = () => {
                fnRefreshAll(element.val());
            }

            var onScreenNameChange = () => {
                var screen = _columns.ScreenName.Lookup.find(o => o.CatDesc === element.val());
                console.log(screen, _columns.ScreenName.Lookup, element.val());
                if (screen != undefined) { //To Handle invalid ScreenNames
                    fnRefreshPCLWorkflowAll(screen.CatID);
                }
                else {
                    fnShowMessage(_columns.ScreenName.InvalidMessage, _messageType.Error);
                }
            }

            var onOddShapeChange = () => {
                var value = element.val();
                fnOddShapeEnableDisable(row, value);
            }



            switch (colName) {
                case _columns.Location.Name: {
                    FillAutoCompleteWithCallBackNoFocus(element, _columns.Location.Lookup, null, onLocationChange);
                    break;
                }
                case _columns.PCRType.Name: {
                    FillAutoCompleteWithCallBackNoFocus(element, _columns.PCRType.Lookup, null, onPCRTypeChange);
                    break;
                } case _columns.WCDeliver.Name: {
                    FillAutoCompleteWithCallBackNoFocus(element, fnGetFilteredWorkLocation(element));
                    break;
                }
                case _columns.CuttingLocation.Name: {
                    var location = fnGetLocation(element);
                    var list = fnGetCuttingLocationListForLocation(location);
                    FillAutoCompleteWithArrayNoFocus(element, list);
                    break;
                }
                case _columns.MovementType.Name: {
                    FillAutoCompleteWithCallBackNoFocus(element, _columns.MovementType.Lookup);
                    break;
                }
                case _columns.UseAlternateStock.Name: {
                    FillAutoCompleteWithCallBackNoFocus(element, _columns.UseAlternateStock.Lookup, onUseAlternateStockChange);
                    break;
                }
                case _columns.CuttingLocationModified.Name: {
                    FillAutoCompleteWithCallBackNoFocus(element, _columns.CuttingLocationModified.Lookup);
                    break;
                }
                case _columns.PCLNumberList.Name: {
                    FillAutoCompleteWithCallBackNoFocus(element, _columns.PCLNumberList.Lookup, onPCLNumberListChange);
                    break;
                }
                case _columns.AllowedStage1Name.Name: {
                    FillAutoCompleteWithCallBackNoFocus(element, _columns.AllowedStage1Name.Lookup);
                    break;
                }
                case _columns.Planner.Name: {
                    FillAutoCompleteWithCallBackNoFocus(element, _columns.Planner.Lookup);
                    break;
                }
                case _columns.OddShape.Name: {
                    FillAutoCompleteWithCallBackNoFocus(element, _columns.OddShape.Lookup, onOddShapeChange);
                    break;
                }
                case _columns.LoadPlantMachine.Name: {
                    FillAutoCompleteWithCallBackNoFocus(element, _columns.LoadPlantMachine.Lookup);
                    break;
                }
                case _columns.ScreenName.Name: {
                    FillAutoCompleteWithCallBackNoFocus(element, _columns.ScreenName.Lookup, onScreenNameChange);
                    break;
                }
                case _columns.StockNumber.Name: {
                    FillAutoCompleteWithCallBackNoFocus(element, []);
                    element.autocomplete("disable");
                    /*fnGetFilteredStockNumberList(row)
                        .then(d => {
                            
                            if (d && d.length > 0 && d !== '[]') {
                                var lst = d.map(x => {
                                    var a = {};
                                    a.CatID = x.StockNumber;
                                    a.CatDesc = x.StockNumber;
                                    return a;
                                });
                                _columns.StockNumber.LookUp = lst;
                                FillAutoCompleteWithCallBackNoFocus(element, _columns.StockNumber.Lookup);
                                element.data("uiAutocomplete").search(element.val());
                                element.autocomplete("search");
                            }
                           
                        });*/

                    break;
                }
            }
            if (colName !== _columns.StockNumber.Name) {
                element.data("uiAutocomplete").search(element.val());
                element.autocomplete("search");
            }
        }
        else {
            var onAreaConsumedChange = () => {
                if (_currentScreen.Name === _screens.MaterialPlanner.Name) {
                    var status = $(row).attr(_attributes.PCRLineStatus);
                    if (status === _pcrLineStatusEnum.ReleasedtoMaterialPlanner) {
                        var value = parseFloat(element.val());
                        let stockNumberElement = fnFindElementByColumnName(row, _columns.StockNumber.Name);
                        var zoom = stockNumberElement.parent().find('span.glyphicon-search');
                        let useAlternateStockElement = fnFindElementByColumnName(row, _columns.UseAlternateStock.Name);
                        var classes = ['zoom-hidden', 'zoom-display', 'stock-number-only', 'stock-number-with-zoom'];
                        var setDisplay = (v) => {
                            zoom.off('click');
                            if (v === true) {
                                zoom.on('click', function (ee) {
                                    fnOpenZoom(this);
                                    ee.preventDefault();
                                });
                                zoom.removeClass(classes[0]).addClass(classes[1]);
                                stockNumberElement.removeClass(classes[2]).addClass(classes[3]);

                            } else {
                                zoom.removeClass(classes[1]).addClass(classes[0]);
                                stockNumberElement.removeClass(classes[3]).addClass(classes[2]);
                            }
                        }
                        if (value === 0) {
                            isValid = true;
                            setDisplay(true);
                            fnEnableDisableElement(useAlternateStockElement, false, true);
                        }
                        else {
                            isValid = true;
                            setDisplay(null);
                            fnEnableDisableElement(useAlternateStockElement, true, true);
                        }
                    }
                }
            }

            switch (colName) {
                case _columns.StockNumber.Name: {
                    //element.off('change');
                    //element.on('change', onStockNumberChange);
                    break;
                }
                case _columns.AreaConsumed.Name: {
                    element
                        .off('keyup')
                        .on('keyup', onAreaConsumedChange);
                    break;
                }
            }

        }

    }
}

function fnEnableDisableElement(element, shouldDisable, resetEvents = false) {
    element.prop('disabled', shouldDisable);
    if (resetEvents === true) {
        $(element)
            .off('focusout')
            .off('focusin');

        if (shouldDisable === false) {
            $(element)
                .on('focusout', function (e) {
                    fnOnFocusOut(element);
                    e.stopPropagation();
                })
                .on('focusin', function (e) {
                    fnOnFocusIn(element);
                    e.stopPropagation();
                });

        }
    }
}

function fnOnLocationChange(row) {

}


function fnOnFocusOut(element) {
    element = $(element);
    fnGrabAndSetGridColumnInState(element, _stateTypes.Dirty);
    fnWrapperToValidate(element);
}

function fnGetCuttingLocationListForLocation(location) {
    var arr = [];
    var lst = _columns.CuttingLocation.Lookup;
    for (var i = 0; i < lst.length; i++) {
        if (lst[i].CatID === location) {
            arr.push(lst[i].CatDesc)
        }
    }
    return arr;
}

function fnGrabAndSetGridColumnInState(element, state, force = false) {
    element = $(element);
    if (_isDialogBoxOpened !== true) {
        var rowNumber = element.parents('tr').attr(_attributes.ItemId);
        var colName = element.attr(_attributes.ColumnName);
        var value = element.val();
        var col = _columns[colName];
        var uiColumnName = colName;
        var dbColumnName = col.Value;
        fnSetGridColumnInState(state, uiColumnName, dbColumnName, value, rowNumber, force);
    }
}

function fnWrapperToValidate(element, value = undefined) {
    var colName = element.attr(_attributes.ColumnName);
    if (value === undefined) {
        value = element.val();
    }

    fnValidateGridCell(element, colName, value)
        .then(
            validationResult => {
                showLoading(false);
                fnHandleValidatedRowItems(validationResult);
            },
            err => {
                showLoading(false);
                fnHandleValidatedRowItems(err);
                if (err.Valid === false) {
                    var parentValue = colName === _columns.CuttingLocation.Name ? fnGetLocation(element) : undefined;
                    if (parentValue === undefined) {
                        element.trigger('focusin', true);
                    }
                }
            }
        );
}

function fnAddClassIfNotAdded(element, className) {
    if ($(element).hasClass(className) === false) {
        $(element).addClass(className);
    }
}

function fnRemoveClass(element, className) {
    if ($(element).hasClass(className) === true) {
        $(element).removeClass(className);
    }
}

function fnGetTableSelectorString() {
    return 'table#' + _tableName + ' tbody';
}

function fnShouldSkipSinceItIsRedundant(cols, row, rowNumber) {
    if (rowNumber === -1) {//bulk update
        return false;
    }
    if (fnIsEmptyObject(_last) === true) {
        return false;
    }
    if (_last.RowNumber === null && _last.Values === null) {
        return false;
    }
    if (_last.RowNumber !== rowNumber) {
        return false;
    }

    var values = fnPrepareValuesObject(cols, row);
    return fnCompareTwoObjects(values, _last.Values);
}

function fnPrepareValuesObject(cols, row) {
    var values = {};
    for (var cnt = 0; cnt < cols.length; cnt++) {
        var colName = cols[cnt];
        var element = fnFindElementByColumnName(row, colName);
        var value = element.val();
        values[colName] = value;
    }
    return values;
}

function fnCompareTwoObjects(obj1, obj2) {
    var isObjectSame = true;
    for (var prop in obj1) {
        if (obj1.hasOwnProperty(prop)) {
            if (obj1[prop] !== obj2[prop]) {
                isObjectSame = false;
                break;
            }
        }
    }
    return isObjectSame;
}

function fnValidateRow(cols, rowNumber = undefined) {
    rowNumber = rowNumber === undefined ? _selectedRow.RowNumber : rowNumber;
    var row = null;
    if (rowNumber === -1) {//bulk update
        row = $('div#' + _dialogBoxContainer);
    } else {
        row = $(fnGetTableSelectorString() + ' tr[' + _attributes.ItemId + ' = ' + rowNumber + ']');
    }

    if (row.hasClass(_classes.DisabledRow)) {
        return Promise.reject(null);
    }
    var shouldSkip = fnShouldSkipSinceItIsRedundant(cols, row, rowNumber);
    if (shouldSkip === true) {
        console.log('redu');
        return Promise.resolve({
            IsRedundantCall: true,
            Key: true
        });
    }
    var isAtleastOneInValid = false;
    var validationResult = [];
    var promisesList = [];
    for (var cnt = 0; cnt < cols.length; cnt++) {
        var colName = cols[cnt];
        var element = fnFindElementByColumnName(row, colName);
        var value = element.val();
        var promise = fnValidateGridCell(element, colName, value).then(
            result => {
                if (result.Valid === false) {
                    isAtleastOneInValid = true;
                }
                validationResult.push(result);
            },
            err => {
                // in case of errors from dbside variables valid is false
                if (err.Valid === false) {
                    isAtleastOneInValid = true;
                }
                validationResult.push(err);
            }
        );
        promisesList.push(promise);
    }

    return Promise.all(promisesList).then(
        result => {
            if (isAtleastOneInValid === true) {
                return Promise.reject(validationResult);
            }
            return Promise.resolve(validationResult);
        },
        err => {
            return Promise.reject(validationResult);
        }
    );
}

function fnHandleValidatedRow(validationResultArray) {
    var validOnes = validationResultArray.filter(x => x.Valid === true);
    for (var cnt = 0; cnt < validOnes.length; cnt++) {
        fnHandleValidatedRowItems(validOnes[cnt]);
    }
    var invalidOnes = validationResultArray.filter(x => x.Valid === false);
    for (var cnt = 0; cnt < invalidOnes.length; cnt++) {
        fnHandleValidatedRowItems(invalidOnes[cnt], cnt === 0);
    }
}

function fnHandleValidatedRowItems(result, focusThisElement = true) {
    var isValid = result.Valid;
    var element = result.Element;
    if (isValid === false) {
        fnAddClassIfNotAdded(element, _classes.InvalidEditableItem);
        element.attr('style', '');
        element.attr('title', result.Message);
    }
    else {
        fnRemoveClass(element, _classes.InvalidEditableItem);
        //element.attr('style', 'border:none');
        element.attr('title', '');
    }
    if (focusThisElement === true && isValid == false) {
        element.trigger('focusin', true);
    }
}

function fnShowInValidMessage(err) {
    console.log(err);
    if (err === null || err === undefined) {
        return;
    }
    if ($.isArray(err) === false) {
        err.Valid = false;
        err.Message = err.Value;
        err = [err];
    }
    var invalidItems = err.filter(x => x.Valid === false);
    var msg = '';
    var messages = invalidItems.map(x => {
        return x.Message;
    });
    msg = messages.join(', ');
    fnShowMessage(msg, _messageType.Error);
}

function fnCloseDialog() {
    _isDialogBoxOpened = false;
    if (_dialog !== null) {
        _dialog.modal('hide');
        _dialog = null;
    }
}

function fnSaveActionOnDialog(successCallback) {
    fnValidateAndSaveRow(-1)
        .then(
            result => {
                if (result.Key === true) {
                    fnCloseDialog();
                    if (successCallback !== undefined) {
                        successCallback();
                    }
                }
                else {
                    fnShowWarningOrErrorMessage(result);
                }
            },
            err => {

            }
        );
}

function fnGetSingleOccurenceHtml(event, element) {
    var containerObj = {
        Html: null,
        RowNum: null
    }
    var row = $(element).parents('tr[' + _attributes.ItemId + ']');
    let rowNum = $(element).parents('tr').attr(_attributes.ItemId);
    fnEnableDisableElement($(".top-section :input"), true);
    //$(".top-section :input").prop('disabled', true);
    let topSection = $(".top-section");
    topSection
        .addClass("so-top-section");

    var container = $('<div/>');

    container
        .append(topSection);

    var dataTable = _tbl.DataTable();
    console.log("DT:", dataTable);
    var items = [];

    var arrOfSections = [];
    var section = 'section';
    var sectionCnt = 1;
    var proceed = true;
    var obj = {
        SectionId: null,
        Items: []
    };
    while (proceed === true) {
        var cells = row.find('td.' + section + sectionCnt);
        if (cells !== null && cells.length > 0) {
            items = [];
            for (var cnt = 0; cnt < cells.length; cnt++) {
                var cell = cells[cnt];
                var index = dataTable.cell(cell).index().column;
                var headingName = dataTable.column(index).header().innerText + ": ";
                var headingCell = $('<div/>').text(headingName).addClass("col-sm-3");

                var valueCell = $('<div/>').append(cell.innerHTML).addClass("col-sm-9").addClass(cell.className);
                var added = $('<div/>')
                    .append(headingCell)
                    .append(valueCell);
                items.push(added);
            }
            obj = {
                SectionId: sectionCnt,
                Items: items
            };
            arrOfSections.push(obj);
            sectionCnt++;

        } else {
            proceed = false;
        }

    }
    sectionCnt--; // additional count removed for breaking the loop

    for (var sectionsItr = 0; sectionsItr < arrOfSections.length; sectionsItr++) {
        var sectionObj = arrOfSections[sectionsItr];
        var columnsCount = sectionObj.Items.length;
        var numberOfColumnsInRow = 2; // should be consistent across
        var mod = columnsCount % numberOfColumnsInRow;
        var rowsCount = (columnsCount - mod) / numberOfColumnsInRow;
        if (mod > 0) {
            rowsCount += 1;
        }
        let sectionNo = sectionObj.SectionId;
        var groupDiv = $('<div/>')
            .append('<h4>' + _groupHeadersForSO[sectionsItr] + '</h4><hr>')
            .attr('id', section + sectionNo)
            .addClass('so-group');
        console.log(groupDiv);
        for (var rowCount = 0; rowCount < rowsCount; rowCount++) {

            var divRow = $('<div/>')
                .addClass('so-row');
            var colNavigator = rowCount * numberOfColumnsInRow;
            for (var colNumber = 0; colNumber < numberOfColumnsInRow; colNumber++) {
                if (sectionObj.Items.length > colNavigator) {
                    var item = sectionObj.Items[colNavigator];
                    var dateControl = item.find('div.datecontrol.input-group');
                    if (dateControl.length > 0) {
                        fnDefineCalendarEvents(dateControl, null); // adding events
                    } else {
                        var input = item.find('input:not([readonly])');
                        if (input != null) {
                            fnDefineInputEvents(input, null); // adding events
                        }

                    }
                    var divCol = $('<div/>')
                        .addClass('col-sm-6')
                        .html(item);
                    divRow
                        .append(divCol);
                    colNavigator += 1;
                }
            }
            var divCol = $('<div/>')
                .addClass('clear-float')
            divRow
                .append(divCol);
            groupDiv
                .append(divRow);
        }
        container
            .append(groupDiv);
    }

    containerObj.Html = container;
    containerObj.RowNum = rowNum;
    return containerObj;
}

function fnGetBulkUpdateHtml(checkbox, successCallback) {
    var containerId = _dialogBoxContainer;
    var row = checkbox.parents('tr[' + _attributes.ItemId + ']');
    var dataTable = _tbl.DataTable();
    var items = [];
    var cells = row.find('td');
    for (var cnt = 0; cnt < cells.length; cnt++) {
        var cell = cells[cnt];
        var item = $(cell).find('input[' + _attributes.ColumnName + ']');
        if (item !== null && item.length > 0) {
            var colName = item.attr(_attributes.ColumnName);
            if (_columnsForSave.filter(x => x.Name === colName).length > 0) {
                var index = dataTable.cell(cell).index().column;
                var headingName = dataTable.column(index).header().innerText;
                var headingCell = $('<span/>').text(headingName);
                var added = $('<div/>')
                    .append(headingCell)
                    .append(cell.innerHTML);
                items.push(added);
            }
        }
    }

    var columnsCount = _columnsForSave.length;
    var numberOfColumnsInRow = 3;
    var mod = columnsCount % numberOfColumnsInRow;
    var rowsCount = (columnsCount - mod) / numberOfColumnsInRow;
    if (mod > 0) {
        rowsCount += 1;
    }

    var container = $('<div/>')
        .attr('id', containerId);
    for (var rowCount = 0; rowCount < rowsCount; rowCount++) {
        var divRow = $('<div/>')
            .addClass('dialog-row');

        var colNavigator = rowCount * numberOfColumnsInRow;
        for (var colNumber = 0; colNumber < numberOfColumnsInRow; colNumber++) {
            if (items.length > colNavigator) {
                var item = items[colNavigator];
                var dateControl = item.find('div.datecontrol.input-group');
                if (dateControl.length > 0) {
                    fnDefineCalendarEvents(dateControl, null);
                } else {
                    var input = item.find('input:not([readonly])');
                    fnDefineInputEvents(input, null);
                }
                var divCol = $('<div/>')
                    .addClass('col-33')
                    .html(item);
                divRow
                    .append(divCol);
                colNavigator += 1;
            }
        }
        var divCol = $('<div/>')
            .addClass('clear-float')
        divRow
            .append(divCol);
        container
            .append(divRow);
    }

    var btnSave = $('<button style="margin-right:10px;"/>')
        .addClass('btn')
        .addClass('green')
        .text('Save')
        .off('click')
        .on('click', function () {
            fnSaveActionOnDialog(successCallback);
        });
    var btnCancel = $('<button/>')
        .addClass('btn')
        .addClass('red')
        .text('Cancel')
        .off('click')
        .on('click', function () {
            fnCloseDialog();
        });
    var divButtons = $('<div/>')
        .addClass(_classes.DialogBoxButtons);
    var divButtonsInner = $('<div/>')
        .addClass(_classes.DialogBoxButtons)
        .append(btnSave)
        .append(btnCancel);
    divButtons
        .append(divButtonsInner);
    container
        .append(divButtons);
    return container;
}

function fnOpenZoom(zoomElement) {
    var row = fnGetClosestRow(zoomElement);
    var stockNumberElement = $(row).find(_stockNumberSelector);
    var stockRevisionElement = $(row).find(_stockRevisionSelector);
    var previousStockNumber = '';//fnGetRowColumnValue(row, _columns.StockNumber);
    var previousStockRevision = '';//fnGetRowColumnValue(row, _columns.StockRevision);
    var columns = fnGetStockNumberColumns();
    _columns.StockNumber.IsValid = false;//make it false...let it be selected by user
    fnGetFilteredStockNumberList(row)
        .then(
            data => {
                var yesValue = 1;//TODO
                var useAlternateStock = fnGetRowColumnValue(row, _columns.UseAlternateStock);
                useAlternateStock = useAlternateStock === yesValue ? true : false;
                data = fnGetJSONParsedData(data);
                _mpData.AllData = data;
                _mpData.FilteredData = data.filter(x => x.UseAlternateStock === useAlternateStock);
                console.log('fd', _mpData.FilteredData);
                fnGrabAndSetGridColumnInState(stockNumberElement, _stateTypes.Clean);
                fnGrabAndSetGridColumnInState(stockRevisionElement, _stateTypes.Clean);
                _mpData.SelectedRow = row;
                _mpData.Columns = columns;
                _mpData.PreviousStockNumber = previousStockNumber;
                _mpData.PreviousStockRevision = previousStockRevision;

                _columns.StockNumber.Lookup = _mpData.FilteredData.map(x => {
                    return {
                        CatID: x.StockNumber,
                        CatDesc: x.StockNumber
                    };
                });
                _mpData.Dialog = bootbox.dialog({
                    className: 'dialogWidth',
                    message: fnGHtml(useAlternateStock),
                    title: 'Select stock number',
                    buttons: {
                        cancel: {
                            label: 'Cancel',
                            className: 'btn-info',
                            callback: function () {
                                fnCloseStockDialog();
                            }
                        }
                    }
                });
                _mpData.Dialog.init(function () {
                    setTimeout(function () {
                        _mpData.Dialog.find(_zoomWindowSelector).show();
                        fnGenerateStockNumberTable(true);
                    }, 100);
                });
            }
        );
}

function fnGetStockNumberColumns() {
    var columns = [
        {
            title: 'Select',
            data: 'Id'
        },
        {
            title: 'Contract',
            data: 'Contract'
        },
        {
            title: 'Project',
            data: 'Project'
        },
        {
            title: 'Child Item',
            data: 'ChildItem'
        },
        {
            title: 'Stock Number',
            data: 'StockNumber'
        },
        {
            title: 'Stock Revision',
            data: 'StockRevision'
        },
        {
            title: 'Total Area',
            data: 'TotalArea'
        },
        {
            title: 'Area Allocated',
            data: 'AreaAllocated'
        },
        {
            title: 'Free Area',
            data: 'FreeArea'
        }
    ];
    return columns;
}

function fnGHtml(checked) {
    var html = "<div class='zoom-window'>" +
        "<div>Use Alternate Stock: <input type=\"checkbox\" class= \"popup-alts\"" + " ";
    if (checked === true) {
        html += " checked = checked ";
    }

    html += " onchange=\"fnOnUseAlternateStockChange(this);\" /> </div> " +
        "<table id='table-pop-up' class='table table-striped table-bordered table-hover dt-responsive dialog-table' cellspacing = '0' width = '100%' >" +
        "<thead>" +
        "<tr>" +
        "<th class='hide'>Id</th>" +
        "<th class='all'>Contract</th>" +
        "<th class='all'>Project</th>" +
        "<th class='all'>Child Item</th>" +
        "<th class='all'>Stock Number</th>" +
        "<th class='all'>Revision</th>" +
        "<th class='all'>Total Area</th>" +
        "<th class='all'>Area Allocated</th>" +
        "<th class='all'>Free Area</th>" +
        "</tr>" +
        "</thead>" +
        "<tbody></tbody>" +
        "</table> " +
        "</div>";
    return html;
}

function fnOnUseAlternateStockChange(element) {
    element = $(element);
    var v = fnIsChecked(element);
    _mpData.FilteredData = _mpData.AllData.filter(x => x.UseAlternateStock === v);
    fnGenerateStockNumberTable();
}

function fnCloseStockDialog() {
    if (_mpData !== null && _mpData.DataTable !== null) {
        _mpData.DataTable.destroy();
        _mpData.DataTable = null;
    }
    _mpData.TableReference.html('');
    _mpData.Dialog.find(_zoomWindowSelector).hide();
    _mpData.Dialog.modal('hide');
    _mpData.Dialog = null;
}

function fnIsChecked(element) {
    var value = false;
    element = $(element)[0];
    if ($(element).prop('checked') || $(element).is(':checked') || element.checked) {
        value = true;
    }
    return value;
}

function fnBeforeCloseStockDialog(currentElement) {
    var v = _listYesNo.find(x => x.CatID === 2).CatDesc;
    var stockNumber = $(currentElement).attr(_attributes.StockNumber);
    var stockRevision = $(currentElement).attr(_attributes.StockRevision);
    var useAlternateStockElement = $(currentElement).parents('div.zoom-window').find('input.popup-alts');

    if (fnIsChecked(useAlternateStockElement) === true) {
        v = _listYesNo.find(x => x.CatID === 1).CatDesc;
    }
    var success = () => {
        var stockNumberElement = fnFindElementByColumnName(_mpData.SelectedRow, _columns.StockNumber.Name);
        var stockRevisionElement = fnFindElementByColumnName(_mpData.SelectedRow, _columns.StockRevision.Name);
        var useAlternateStockElement = fnFindElementByColumnName(_mpData.SelectedRow, _columns.UseAlternateStock.Name);
        var alternateStockRemarkElement = fnFindElementByColumnName(_mpData.SelectedRow, _columns.AlternateStockRemark.Name);
        var areaConsumedAlement = fnFindElementByColumnName(_mpData.SelectedRow, _columns.AreaConsumed.Name);
        stockNumberElement.val(stockNumber);
        stockRevisionElement.val(stockRevision);
        useAlternateStockElement.val(v);
        if (fnIsUseAlternateStockSelected(v) === true) {
            fnEnableDisableElement(alternateStockRemarkElement, false, true);
        } else {
            fnEnableDisableElement(alternateStockRemarkElement, true, true);
        }
        fnGrabAndSetGridColumnInState(stockNumberElement, _stateTypes.Dirty);
        fnGrabAndSetGridColumnInState(stockRevisionElement, _stateTypes.Dirty);
        fnGrabAndSetGridColumnInState(useAlternateStockElement, _stateTypes.Dirty);
        fnGrabAndSetGridColumnInState(alternateStockRemarkElement, _stateTypes.Dirty);

        if (fnIsEmptyValue(stockNumber) === true) {
            fnEnableDisableElement(areaConsumedAlement, true, true);
            areaConsumedAlement.val(0.0);
            fnGrabAndSetGridColumnInState(areaConsumedAlement, _stateTypes.Dirty);
        }
        else {
            fnEnableDisableElement(areaConsumedAlement, false, true);
        }
        _columns.StockNumber.IsValid = true;
        fnCloseStockDialog();
    }
    var row = _mpData.SelectedRow;
    fnValidateStockNumber(row, stockNumber, stockRevision, _listYesNo.find(x => x.CatDesc === v).CatID)
        .then(result => {
            if (result.Key === true) {
                success();
            } else if (result.IsInformation === true) {
                return fnShowConfirmDialog(result.Value)
                    .then(
                        dialogResult => {
                            if (dialogResult === true) {
                                success();
                            }
                        });
            } else {
                fnShowWarningOrErrorMessage(result);
            }
        });
}

function fnGenerateStockNumberTable() {
    var columns = _mpData.Columns;
    var previousStockNumber = _mpData.PreviousStockNumber;
    var previousStockRevision = _mpData.PreviousStockRevision;
    var options = {};
    $.extend(options, {
        columns: columns,
        columnDefs: [{
            targets: 0,
            render: function (data, type, row, meta) {
                console.log('dataa', row);
                var x =
                    "<label>" + " " +
                    "<input type =\"checkbox\" onchange=\"fnBeforeCloseStockDialog(this);\"" + " " +
                    "class=\"clsCheckbox\"" + " " +
                    _attributes.StockNumber;
                x += " = " + "'" + row.StockNumber + "'" + " ";
                x += _attributes.StockRevision + " = " + "'" + row.StockRevision + "'" + " ";
                x += "/>";
                x += "</label>";
                return x;
            }
        }],
        data: _mpData.FilteredData,
        scrollY: true,
        scrollX: true,
        scrollCollapse: true
    });

    if (_mpData !== null && _mpData.DataTable !== null) {
        _mpData.DataTable.destroy();
        _mpData.DataTable = null;
    }
    _mpData.TableReference = $(_zoomWindowSelector).find('table#table-pop-up');
    _mpData.TableReference
        .empty()
        .removeClass()
        .addClass('display');
    _mpData.DataTable = _mpData.TableReference.DataTable(options);
}


function fnGetRowsFromCheckboxes(checkboxes, excludeDisabled = true) {
    var rows = [];
    for (var cnt = 0; cnt < checkboxes.length; cnt++) {
        var row = $($(checkboxes[cnt]).parents('tr')[0]);
        if (excludeDisabled === true) {
            if (row.hasClass(_classes.DisabledRow) === false) {
                rows.push(row);
            }
        } else {
            rows.push(row);
        }
    }
    console.log('rows', rows);
    return rows;
}

function fnGetRowNumbersFromCheckboxes(checkboxes, excludeDisabled = true) {
    var rowNumbers = [];
    for (var cnt = 0; cnt < checkboxes.length; cnt++) {
        var row = $(checkboxes[cnt]).parents('tr');
        var rowNumber = row.attr(_attributes.ItemId);
        if (excludeDisabled === true) {
            if (row.hasClass(_classes.DisabledRow) === false) {
                rowNumbers.push(rowNumber);
            }
        } else {
            rowNumbers.push(rowNumber);
        }
    }
    console.log('rown', rowNumbers);
    return rowNumbers;
}

function fnSetAttr() {
    var parentSelector = null;
    if (_isDialogBoxOpened === true) {
        parentSelector = 'div#' + _dialogBoxContainer;
    } else {
        parentSelector = fnGetTableSelectorString();
    }
    $(parentSelector + ' .' + _classes.ControlRequired).find('input, select, textarea').attr('required', 'required');
    $(parentSelector + ' input[maxlength], textarea[maxlength]').maxlength({
        alwaysShow: true,
    });
}

function fnHandleLengthModifiedWidthModified(row) {
    var lengthModifiedElement = fnFindElementByColumnName(row, _columns.LengthModified.Name);
    var widthModifiedElement = fnFindElementByColumnName(row, _columns.WidthModified.Name);
    lengthModifiedElement.attr(_attributes.PrevValue, lengthModifiedElement.val());
    widthModifiedElement.attr(_attributes.PrevValue, widthModifiedElement.val());
}

function fnLoadGridData(aoColumns, sUrl, screenServerDataCallBack = null, screenCreateRowCallBack = null) {
    var serverDataCallBack = () => {
        fnTableCallBack(screenServerDataCallBack);
    };

    var rowCallBack = (row, data, dataIndex) => {
        fnRowCallBack(row, data, dataIndex, screenCreateRowCallBack);
    };


    var iFCRight = 1;
    var utilityParams = {
        sUrl: sUrl,
        iFCLeft: 0,
        iFCRight: iFCRight,
        aoColumns: aoColumns
    };
    utilityParams.iResponsiveOnWindowWidth = 800;
    utilityParams.bIsAllCheckbox = true;
    utilityParams.bIsResponsive = false;
    utilityParams.bIsSearchFilter = true;
    utilityParams.bBtnMaximize = true;
    utilityParams.bIsWrapHeader = false;
    utilityParams.bIsWrap = false;
    utilityParams.bServerSide = true;
    utilityParams.bIsLazyLoading = false;
    utilityParams.bIsIgnoreHiddenColumns = true;
    utilityParams.sBtnDivSelector = "#dvExportHeader";
    utilityParams.fnServerDataCallback = serverDataCallBack;
    utilityParams.fnCreateRow = rowCallBack;
    _tbl.dtUtility(utilityParams);
}

function fnTableCallBack(screenServerDataCallBack) {
    if (screenServerDataCallBack !== null) {
        screenServerDataCallBack();
    }
    fnClearState();
    var selector = fnGetTableSelectorString() + ' tr:not(.' + _classes.DisabledRow + ')';
    fnDefineInputEvents(null, selector);
    fnDefineCalendarEvents(null, selector);
    fnDefineAllRowsEvents(selector);
    fnAddScrollers();
    showLoading(false);
}

function fnRowCallBack(row, data, dataIndex, screenCreateRowCallBack) {
    if (screenCreateRowCallBack != null) {
        screenCreateRowCallBack(row, data, dataIndex);
    }
    fnHandleLengthModifiedWidthModified($(row));
}

function fnDefineAllRowsEvents(rowsSelector) {
    var cellsSelector = $(rowsSelector).find('td');
    cellsSelector
        .off('mouseleave')
        .off('mouseout')
        .on('mouseout', function (e) {
            var editableFields = $(this).find('input.' + _classes.InputField);
            if (editableFields !== null && editableFields.length > 0) {
                // do nothing
            } else {
                e.stopPropagation();//dont propagate mouseout to mouseevent to row
            }
        });


    $(rowsSelector).off('mouseleave');
    $(rowsSelector).on('mouseleave', function (e) {
        var r = $(this);
        var x = () => {
            fnOnHoverOutRow(r, e);
        }
        setTimeout(x, 1000);
    });
}
function fnCompareRow() {

}

function fnOnHoverOutRow(row, e) {
    //console.clear();
    //console.log('type', e.type);
    //console.log('originalEvent', e.originalEvent);
    //console.log('target', e.target);
    var rowNumber = $(row).attr(_attributes.ItemId);
    //console.log('rowNumber', row, rowNumber);
    return fnCallSave(rowNumber, e);
}

function fnCallSave(rowNumber, e) {
    //return Promise.resolve(null);
    if (_isExecuting === false) {
        return fnShouldSave(rowNumber)
            .then(
                success => {
                    if (success !== null) {
                        if (success.IsRedundantCall === true) {
                            //do nothing
                        } else {
                            fnRefreshScreen(rowNumber);
                        }
                    }
                    e.preventDefault();

                },
                err => {
                    e.preventDefault();
                });
    }
    return Promise.resolve(null);
}

function fnBindGridData(columns, columnDefs, data, options, screenServerDataCallBack = null, screenCreateRowCallBack = null) {
    $.extend(options, {
        columns: columns,
        columnDefs: columnDefs,
        data: data,
        //scroll: true,
        scrollY: true,
        scrollX: true,
        scrollCollapse: true,

        "createdRow": function (row, data, dataIndex) {
            fnRowCallBack(row, data, dataIndex, screenCreateRowCallBack);
        }
    });
    var dt = _tbl.DataTable();
    dt.destroy();
    _tbl
        .empty()
        .removeClass()
        .addClass('display')
        .DataTable(options);
    fnTableCallBack(screenServerDataCallBack);
}

function fnGetJSONParsedData(input) {
    return JSON.parse(input.toString().replace(/&quot;/g, '"'));
}

function fnParseData(data, colName, row) {
    var returned = data;
    var selectValue;

    switch (colName) {
        case _columns.MovementType.Name: {
            if (_currentScreen.Name == _screens.PCLWorkflow.Name) {
                selectValue = _columns.MovementType.Lookup.find(o => o.CatID === data);
                returned = selectValue.CatDesc;
            }
            break;
        }

        case _columns.WCDeliver.Name: {
            returned = row.WCDeliverName;
            break;
        }

        default: {
            let numericData = parseFloat(data);
            let isDate = (new RegExp('Date\\(.*\\)')).test(data);

            if (!isNaN(numericData)) {
                returned = numericData.toFixed(2);
                returned = returned.toString();
                returned = returned.replace(/\.00$/, '');
            }
            else if (isDate) {
                let dateData = data.replace(/\D/g, "");
                dateData = parseInt(dateData);
                returned = new Date(dateData).toISOString().slice(0, 19).replace('T', ' ');
            }
            break;
        }
    }

    return returned;
}

function fnRenderAutoComplete(row, data, colName, border) {
    var returned = data;
    var selectValue;
    switch (colName) {
        case _columns.WCDeliver.Name: {

            selectValue = _columns.WCDeliver.Lookup.find(o => o.CatDesc === row.WCDeliverName);
            returned = "<input type='text' value=\"" + selectValue.CatDesc + "\"" + " colname = " + colName + " class= 'autocomplete form-control form-control inputField' style='" + border + "'" + "> ";
            break;
        }
        default: {
            selectValue = _columns[colName].Lookup.find(o => o.CatID === data);
            returned = "<input type='text' value=\"" + selectValue.CatDesc + "\"" + " colname = " + colName + " class= 'autocomplete form-control form-control inputField' style='" + border + "'" + "> ";
            break;
        }
    }

    return returned;
}

function fnRenderColumn(data, type, row, meta, x) {
    propName = x.Type;
    colName = x.ColName;
    data = data === null || data === 'null' ? '' : data;
    var returned = data;
    var border = 'border:1px solid #ererer';
    //var border = 'border:none';
    switch (propName) {
        case 1: {
            returned = "<input type= 'text'  maxlength= 15 value=\"" + data + "\"" + " colname = " + colName + " class='form-control form-control inputField' style='" + border + "'" + "> ";
            break;
        }
        case 2: {
            returned = "<input type= 'number'  maxlength= 15 value=\"" + data + "\"" + " colname = " + colName + " class='form-control form-control inputField' style='" + border + "'" + "> ";
            break;
        }
        case 5: {
            returned = fnRenderAutoComplete(row, data, colName, border);
            break;
        }
        case 4: {
            returned = fnActionColumnRender(row.Id, x);
            break;
        }
        default: {//label
            returned = fnParseData(data, colName, row);
            break;
        }
    }

    return returned;
}

function fnActionColumnRender(rowId = undefined, x = undefined) {
    var buttons = '';
    console.log(rowId, x, _currentScreen);
    switch (_currentScreen.Name) {
        case _screens.MaintainReturnDistribution.Name: {
            if (rowId !== undefined) {
                buttons += "<a id=\"DeleteRow" + rowId + "\" name=\"DeleteRow" + rowId + "\" title=\"Delete Row\" class=\"blue action\" onclick=\"fnDeleteRow(event, this, '" + rowId + "')\" ><i class=\"iconspace fa fa-trash\"></i></a>";
            }
            else {
                buttons += "<a id=\"DeleteRow name=\"DeleteRow\" title=\"Delete Row\" class=\"blue action\" onclick=\"fnDeleteRow(event,this)\" ><i class=\"iconspace fa fa-trash\"></i></a>";
            }
            break;
        }

        case _screens.PCLWorkflow.Name: {
            if (x.IsApproveAction) {
                buttons += "<a id=\"ApproveRow" + rowId + "\" name=\"ApproveRow" + rowId + "\" title=\"Approve Row\" class=\"blue action\" onclick=\"fnApproveOrRejectRow(event,'" + rowId + "', 1)\" ><i class=\"iconspace fa fa-check-circle\"></i></a>";
            }

            if (x.IsRejectAction) {
                buttons += "<a id=\"RejectRow" + rowId + "\" name=\"RejectRow" + rowId + "\" title=\"Reject Row\" class=\"blue action\" onclick=\"fnApproveOrRejectRow(event,'" + rowId + "', 0)\" ><i class=\"iconspace fa fa-undo\"></i></a>";
            }
            break;
        }
    }

    return buttons;
}

function fnGetConfigArray(arrAllColumnConfigs) {
    var arr = [];
    var nonSearchable = arrAllColumnConfigs.filter(x => x.NonSearchable === true);
    var nonSortable = arrAllColumnConfigs.filter(x => x.NonSortable === true);
    var invisible = arrAllColumnConfigs.filter(x => x.Invisible === true);
    var targetsNonSe = nonSearchable.map(x => {
        return x.Index;
    });
    var targetsNonSo = nonSortable.map(x => {
        return x.Index;
    });
    var targetsI = invisible.map(x => {
        return x.Index;
    });
    var obj1 = {
        targets: targetsNonSe,
        searchable: false
    };
    var obj2 = {
        targets: targetsNonSo,
        sortable: false
    };
    var obj3 = {
        targets: targetsI,
        visible: false
    };
    arr.push(obj1);
    arr.push(obj2);
    arr.push(obj3);
    return arr;
}

function fnGetMappedConfigs(columnConfigs) {
    return columnConfigs.map(x => {
        var item = {};
        for (var cnt = 0; cnt < x.length; cnt++) {
            item[x[cnt].Key] = x[cnt].Value;
        }
        return item;
    });
}

function fnGetEnumObj(enumArray) {
    var enumObj = {};
    enumArray.forEach(function (item, index) {
        key = item.name.replace(/ /g, '');
        enumObj[key] = item.name;
    });
    return enumObj;
}

function fnGetColumnTypeArray(arrAllColumnConfigs) {
    return arrAllColumnConfigs.map(x => {
        var returned = {};
        returned.targets = x.Index;
        returned.render = (data, type, row, meta) => {
            return fnRenderColumn(data, type, row, meta, x);
        }
        return returned;
    });
}

function fnSplitStringAsObjectArray(input) {
    var arr = input.toString().split(',');
    var objectArr = [];
    for (var cnt = 0; cnt < arr.length; cnt++) {
        objectArr.push(arr[cnt]);
    }
    return objectArr;
}

function fnSplitStringAsIntegerArray(input) {
    var arr = input.toString().split(',');
    var integerArr = [];
    for (var cnt = 0; cnt < arr.length; cnt++) {
        var value = parseInt(arr[cnt]);
        integerArr.push(value);
    }
    return integerArr;
}

function fnDefineInputEvents(inputSelectors, parentSelector) {
    if (parentSelector !== null) {
        inputSelectors = $(parentSelector + ' input.' + _classes.InputField + ':not([disabled])');
    }
    inputSelectors.off('focusout');
    inputSelectors.on('focusout', function (e) {
        var element = $(this);
        fnOnFocusOut(element);
        e.stopPropagation();
    });
    inputSelectors.off('focusin');
    inputSelectors.on('focusin', function (e, isDirty) {
        var element = $(this);
        fnOnFocusIn(element, isDirty);
        e.stopPropagation();
    });
}

function fnDefineCalendarEvents(dateContainerSelectors, parentSelector) {
    if (parentSelector !== null) {
        dateContainerSelectors = $(parentSelector + ' div.datecontrol.input-group');
    }
    dateContainerSelectors.off('mouseleave');
    dateContainerSelectors.off('mouseout');
    dateContainerSelectors.off('click');
    dateContainerSelectors.on('click', dateContainerSelectors, function (e) {
        var element = $(this);
        if ($(this).attr(_attributes.Init) == null || $(this).attr(_attributes.Init) == undefined) {
            var format = "";
            var is_meridian_datetime = false;
            if ($(this).hasClass("form_meridian_datetime")) {
                is_meridian_datetime = true;
                format = "dd/mm/yyyy HH:ii P";
            }
            else {
                format = $(this).attr("data-date-format")
            }
            $(this).datepicker({
                autoclose: true,
                todayBtn: true,
                startDate: new Date()
            });
            $(this).attr(_attributes.Init, true);
            $(this).find(".btn").click();
        }

        e.stopPropagation();
    });
    dateContainerSelectors.each(function () {
        var that = $(this);
        that.datepicker()
            .on('hide', function (e) {
                fnOnHideCalendar(this, e);
                e.stopPropagation();
            });
    });
}

function fnOnCheckUnCheck(event, chk) {
    var row = fnGetClosestRow(chk);
    var cls = '';
    if ($(chk).is(':checked')) {
        row.addClass(cls);
    }
    else {
        row.removeClass(cls);
    }
    event.stopPropagation();
}

function fnShowWarningOrErrorMessage(result) {
    if (result.IsInformation === true) {
        fnShowMessage(result.Value, _messageType.Information);
    } else {
        fnShowMessage(result.Value, _messageType.Error);
    }
}

function fnShowMessage(message, messageType) {
    switch (messageType) {
        case _messageType.Success: {
            DisplayNotiMessage('success', message, 'Success');
            break;
        }
        case _messageType.Information: {
            DisplayNotiMessage('info', message, 'Information');
            break;
        }
        case _messageType.Error:
            DisplayNotiMessage('error', message, 'Error');
            break;
    }
}

function fnScrollHorizontal(which) {
    var selector = 'div.dataTables_scrollBody';
    var element = $(selector);
    if (element.length > 0) {
        element = element[0];
        var width = element.offsetWidth;
        var divider = 4;
        var currentScrollPosition = element.scrollLeft;
        var scrollBy = width / divider;
        var newScrollPosition = 0;
        if (which === -1) { //scroll left
            if (currentScrollPosition < scrollBy) {
                newScrollPosition = 0;
            } else {
                newScrollPosition = currentScrollPosition - scrollBy;
            }
        } else { //scroll right
            newScrollPosition = currentScrollPosition + scrollBy;
        }
        element.scrollLeft = newScrollPosition;
    }
}

function fnAddScrollers() {
    var a = () => {
        var tableRightSelector = 'div.DTFC_RightHeadWrapper';
        var tableRight = $(tableRightSelector);
        return tableRight;
    }
    var b = (r) => {
        var headerRow = r.find('tr.mycustomfilter');
        var header = headerRow.find('th');
        return header;
    }

    var x = (ele) => {
        var div = $('<div/>');
        div.append('<img class = "arrow arrow-left" onclick="fnScrollHorizontal(-1)" />');
        div.append('<img class = "arrow arrow-right" onclick="fnScrollHorizontal(1)" />');
        ele.html('');
        ele.append(div);
    }
    var interval = null;
    var fn = () => {
        var clear = () => {
            if (interval !== null) {
                window.clearInterval(interval);
                interval = null;
            }
        }
        var l = a();
        if (l.length > 0) {
            var p = b(l);
            if (p.length > 0) {
                clear();
                x(p);
            }
        } else {
            clear();
        }
    }
    interval = window.setInterval(() => { fn(); }, 500);
}

function fnHandleMOResult(r) {
    r.RootElement.find('div[' + _screenAttributes.Name + ' = ' + '"' + r.TabName + '"]').html(r.Html);
    showLoading(false);
}

function fnHandleDetailsTabResult(element, r, isDisabled, attributesAndValues, getHtmlFunction) {
    //_tableName = "tblSingleOccurenceDetails";
    var htmlObj = fnGetSingleOccurenceHtml(event, element);
    htmlObj.ActionButtonsHtml = getHtmlFunction(event, htmlObj.RowNum, isDisabled);
    r.RootElement.find('div[' + _screenAttributes.Name + ' = ' + '"' + r.TabName + '"]').html(r.Html);

    if (attributesAndValues !== null && attributesAndValues.length > 0) {
        var firstRow = $(fnGetTableSelectorString() + ' tr:first');
        for (var cnt = 0; cnt < attributesAndValues.length; cnt++) {
            firstRow.attr(attributesAndValues[cnt].Name, attributesAndValues[cnt].Value);
        }
    }
    $("#dvActionButtons").html(htmlObj.ActionButtonsHtml);
    $("#divSingleOccurenceContainer").html(htmlObj.Html);
    showLoading(false);
}

function fnFindElementsByTabName(tabName) {
    var rootElement = $(_screenSelectors.Root);
    var anchor = rootElement.find(_screenSelectors.Anchor + '[' + _screenAttributes.Rel + '= "' + tabName + '"]');
    return {
        Anchor: anchor, Root: rootElement
    };
}

function fnSetScreenEvents(attributeNames, openMainTabFunction, openDetailsTabFunction, getHtmlFunction, customTabs = undefined) {
    var rootElement = $(_screenSelectors.Root);
    var tabHeadingAnchors = rootElement.find(_screenSelectors.Anchor);
    tabHeadingAnchors.each(function (i) {
        var anchor = $(this);
        anchor.off('click');
        anchor.on('click', function (e) {
            var tabObj = fnGetObjOfTabObject(_screenTabs, $(this).attr(_screenAttributes.Rel));
            switch (tabObj.Name) {
                case _screenTabs.Details.Name: {
                    console.clear();
                    if (_currentScreen !== null && _currentScreen.CurrentTab !== null && _currentScreen.CurrentTab.Name === _screenTabs.Details.Name) {
                        fnShowMessage('Already on details tab', _messageType.Information);
                    } else {
                        var selector = _checkedCheckboxSelector;

                        var rowNumbers = fnGetRowNumbersFromCheckboxes($(selector), false);
                        if (rowNumbers === null || rowNumbers.length === 0) {
                            fnShowMessage('No row selected', _messageType.Information);
                        } else {
                            if (rowNumbers.length === 1) {
                                var id = parseInt(rowNumbers[0]);
                                console.log('id', id);
                                var row = fnGetRowsFromCheckboxes($(selector), false)[0];
                                var x = [];
                                for (var a = 0; a < attributeNames.length; a++) {
                                    var attributeName = attributeNames[a];
                                    var attributeValue = $(row).attr(attributeName);
                                    x.push({
                                        Name: attributeName,
                                        Value: attributeValue
                                    });
                                }
                                x.push({
                                    Name: _attributes.ItemId,
                                    Value: id
                                });
                                var element = fnFindElementByClassName(row, 'action', 'a');
                                var isDisabled = fnIsEditableRow(id) === false;
                                fnOpenScreenTab(_screenTabs.Details, openDetailsTabFunction)
                                    .then(r => {
                                        fnHandleDetailsTabResult(element, r, isDisabled, x, getHtmlFunction);
                                    });

                            } else {
                                fnShowMessage('Please select only one row', _messageType.Information);
                            }
                        }
                    }


                    break;
                }
                case _screenTabs.Main.Name: {
                    fnOpenScreenTab(_screenTabs.Main, openMainTabFunction)
                        .then(r => {
                            fnHandleMOResult(r);
                        });

                    break;
                }

                default: {
                    if (customTabs !== undefined && customTabs !== null) {
                        for (let i = 0; i < customTabs.length; i++) {
                            if (_currentScreen !== null && _currentScreen.CurrentTab !== null && _currentScreen.CurrentTab.Name === customTabs[i].Name) {
                                fnShowMessage('Already on ' + customTabs[i].ViewName + ' tab', _messageType.Information);
                            }
                            else if (tabObj.Name === _screenTabs.ReturnBalance.Name) {//custom 
                                fnShowMessage('Please open this tab from Details tab', _messageType.Information);
                            }
                            else {
                                //Assuming All Custom Tabs are MO
                                fnOpenScreenTab(customTabs[i], customTabs[i].Method)
                                    .then(r => {
                                        fnHandleMOResult(r);
                                    });
                            }

                        }
                    }

                }
            }
            e.stopPropagation();
        });
    });
}

function fnCreateNewTab(tabName, tabSelector) {
    console.log(tabName);
    let propTabName = tabName.replace(/ /g, '');
    var tab = {
        Name: propTabName,
        IsMultiOccurrence: true,
        ViewName: tabName
    }
    _screenTabs[tab.Name] = tab;

    console.log($(tabSelector + " li:last-child"));
    $(tabSelector + " li:last-child").after('<li><a data-rel=' + tab.Name + ' data-name=' + tab.Name + ' data-toggle="tab">' + tabName + '</a></li>');
    $(_tabContentSelector).append('<div class="tab-pane" data-name=' + tab.Name + ' id=' + tab.Name + '> ' + tab.Name + '</div>');
    return tab;
}

function fnGetObjOfTabObject(object, value) {
    for (var prop in object) {
        if (object.hasOwnProperty(prop)) {
            if (object[prop].Name === value)
                return object[prop];
        }
    }
}

function fnOpenScreenTab(tab, fn) {
    showLoading(true);
    _currentScreen.CurrentTab = tab;

    var elements = fnFindElementsByTabName(tab.Name);
    var rootElement = elements.Root;
    var anchorElement = elements.Anchor;
    var parentUL = $(anchorElement)
        .parents('ul');
    parentUL
        .find('li.' + _screenClasses.Active)
        .removeClass(_screenClasses.Active);
    $(anchorElement)
        .parent()
        .addClass(_screenClasses.Active);

    rootElement
        .find('div.' + _screenClasses.TabPane).html('');

    rootElement
        .find('div.' + _screenClasses.TabPane + '[' + _screenAttributes.Name + ' = "' + tab.Name + '"]')
        .addClass(_screenClasses.Active);
    return fn().
        then(result => {
            return Promise.resolve({
                RootElement: rootElement,
                Html: result,
                TabName: tab.Name
            });
        });

}

function fnParseServerObject(serverObject) {
    return JSON.parse(JSON.stringify(serverObject));
}