﻿using IEMQS.Areas.WCS.Models;
using IEMQS.Models;
using IEMQS.DESCore;
using IEMQS.DESServices;
using IEMQSImplementation;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.WCS.Controllers
{
    public class FormulaMasterController : clsBase
    {
        clsManager objClsManager = new clsManager();

        [AllowAnonymous]
        [SessionExpireFilter]
        [UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        [SessionExpireFilter]
        [UserPermissions]
        public ActionResult AddHeader(int id = 0)
        {
            WCS005 model = new WCS005();
            List<AutoCompleteModel> list = objClsManager.GetWCSBUList();
            ViewBag.BUList = (from u in list select new { CatID = u.Value, CatDesc = u.Text }).OrderBy(u => u.CatID).ToList();
            model.Location = objClsLoginInfo.Location;
            if (id > 0)
            {
                model = db.WCS005.Where(u => u.HeaderId == id).FirstOrDefault();
                ViewBag.BU = db.COM002.Where(u => u.t_dtyp == 2 && u.t_dimx == model.BU).Select(u => u.t_dimx + " - " + u.t_desc).FirstOrDefault();
            }
            ViewBag.LocationDesc = db.COM002.Where(u => u.t_dtyp == 1 && u.t_dimx == model.Location).Select(u => u.t_dimx + " - " + u.t_desc).FirstOrDefault();
            return View(model);
        }

        [HttpPost]
        public ActionResult getHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                string BU = "";
                List<AutoCompleteModel> BUList = objClsManager.GetWCSBUList();
                foreach (var bus in BUList)
                {
                    BU += "'" + bus.Value + "',";
                }
                BU = BU.TrimEnd(',');

                string Location = objClsLoginInfo.Location;

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "1=1";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = { "BU", "Location", "CreatedBy", "CONVERT(nvarchar(20),CreatedOn,103)" };
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(param.SearchFilter))
                        strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }


                var lstResult = db.SP_WCS_GET_FORMULA_MASTER_HEADER(StartIndex, EndIndex, strSortOrder, strWhere, BU, Location).ToList();

                int? totalRecords = lstResult.Select(i => i.TotalCount).FirstOrDefault();

                var data = (from uc in lstResult
                            select new[]
                            {
                           Convert.ToString(uc.BU),
                           Convert.ToString(uc.Location),
                           Convert.ToString(uc.CreatedBy),
                           Convert.ToDateTime(uc.CreatedOn).ToString("dd/MM/yyyy"),
                           Convert.ToString(uc.HeaderId),
                           Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #region CRUD Header

        [HttpPost]
        public ActionResult SaveHeader(FormCollection fc)
        {
            string BU = fc["BU"].ToString();
            string Location = fc["Location"].ToString();
            int HeaderId = fc["HeaderId"] != "" ? Convert.ToInt32(fc["HeaderId"].ToString()) : 0;

            clsHelper.ResponseMsgWCS objResponseMsg = new clsHelper.ResponseMsgWCS();
            try
            {
                if (HeaderId > 0)
                {
                    if (!db.WCS005.Any(u => u.BU == BU && u.Location == Location && u.HeaderId != HeaderId))
                    {
                        var item = db.WCS005.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                        if (item != null)
                        {
                            item.BU = BU;
                            item.Location = Location;
                            item.EditedBy = objClsLoginInfo.UserName;
                            item.EditedOn = DateTime.Now;
                            db.SaveChanges();
                        }
                        objResponseMsg.HeaderId = HeaderId;
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                    }
                }
                else
                {
                    if (!db.WCS005.Any(u => u.BU == BU && u.Location == Location))
                    {
                        WCS005 objWCS005 = new WCS005();
                        objWCS005.BU = BU;
                        objWCS005.Location = Location;
                        objWCS005.CreatedBy = objClsLoginInfo.UserName;
                        objWCS005.CreatedOn = DateTime.Now;
                        db.WCS005.Add(objWCS005);
                        db.SaveChanges();
                        objResponseMsg.HeaderId = objWCS005.HeaderId;
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region CRUD Lines

        [HttpPost]
        public ActionResult LoadLineDataGridPartial(int HeaderId)
        {
            WCS006 model = new WCS006();
            model.HeaderId = HeaderId;
            return PartialView("_GetLineGridDataPartial", model);
        }

        public ActionResult LoadLineDataTable(JQueryDataTableParamModel param)
        {
            try
            {
                int HeaderId = param.Headerid != "" ? Convert.ToInt32(param.Headerid) : 0;
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = " HeaderId=" + param.Headerid;

                #region Datatable Sorting 

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                string _urlform = string.Empty;
                string indextype = param.Department;

                #endregion

                string[] columnName = { "ProductFormDesc", "FormulaforVolumeCalculation", "FormulaforVolumeCalculationInternalVolume", "FormulaforCGCalculation",
                    "Input1Description", "Input2Description", "Input3Description", "Input4Description", "Input5Description", "Input6Description", "Input7Description",
                    "Input8Description", "Input9Description", "Input10Description"};

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(param.SearchFilter))
                        whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                var lstMCR = db.SP_WCS_GET_FORMULA_MASTER_DETAIL(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstMCR.Select(i => i.TotalCount).FirstOrDefault();

                int newRecordId = 0;
                var newRecord = new[] {
                                    "0",
                                    Helper.GenerateHTMLTextbox(newRecordId,"ProductFormDesc","","",false,"",false,"50"),
                                    Helper.GenerateHTMLTextbox(newRecordId,"FormulaforVolumeCalculation","","",false,"",false,"300"),
                                    Helper.GenerateHTMLTextbox(newRecordId,"FormulaforVolumeCalculationInternalVolume","","",false,"",false,"300"),
                                    Helper.GenerateHTMLTextbox(newRecordId,"FormulaforCGCalculation","","",false,"",false,"300"),
                                    Helper.GenerateHTMLTextbox(newRecordId,"Input1Description","","",false,"",false,"50"),
                                    Helper.GenerateHTMLTextbox(newRecordId,"Input2Description","","",false,"",false,"50"),
                                    Helper.GenerateHTMLTextbox(newRecordId,"Input3Description","","",false,"",false,"50"),
                                    Helper.GenerateHTMLTextbox(newRecordId,"Input4Description","","",false,"",false,"50"),
                                    Helper.GenerateHTMLTextbox(newRecordId,"Input5Description","","",false,"",false,"50"),
                                    Helper.GenerateHTMLTextbox(newRecordId,"Input6Description","","",false,"",false,"50"),
                                    Helper.GenerateHTMLTextbox(newRecordId,"Input7Description","","",false,"",false,"50"),
                                    Helper.GenerateHTMLTextbox(newRecordId,"Input8Description","","",false,"",false,"50"),
                                    Helper.GenerateHTMLTextbox(newRecordId,"Input9Description","","",false,"",false,"50"),
                                    Helper.GenerateHTMLTextbox(newRecordId,"Input10Description","","",false,"",false,"50"),
                                    GenerateGridButton(newRecordId, "Add", "Add Rocord", "fa fa-plus", "SaveRecord(0);" ),
                                };

                //below code for edit of formula master is commented.
                //beacause as discussed with keyur sir on 09-08-2018. user can't change formula.
                //and if we allow user to edit formula, then we should recalculate all weight and cg calculation.

                //Helper.HTMLAutoComplete(h.LineId, "txtProductForm", h.ProductFormDesc, "UpdateData(this, " + h.LineId + ",true)", false, "", "ProductForm") + "" + Helper.GenerateHidden(h.LineId, "ProductForm", h.ProductForm.ToString()),
                //Helper.GenerateHTMLTextbox(h.LineId, "FormulaforVolumeCalculation", h.FormulaforVolumeCalculation, "UpdateData(this, " + h.LineId + ",true);", false, "", false, "300"),
                //Helper.GenerateHTMLTextbox(h.LineId, "FormulaforVolumeCalculationInternalVolume", h.FormulaforVolumeCalculationInternalVolume, "UpdateData(this, " + h.LineId + ",true);", false, "", false, "300"),
                //Helper.GenerateHTMLTextbox(h.LineId, "FormulaforCGCalculation", h.FormulaforCGCalculation, "UpdateData(this, " + h.LineId + ",true);", false, "", false, "300"),
                //Helper.GenerateHTMLTextbox(h.LineId, "Input1Description", h.Input1Description, "UpdateData(this, " + h.LineId + ",true);", false, "", false, "50"),
                //Helper.GenerateHTMLTextbox(h.LineId, "Input2Description", h.Input2Description, "UpdateData(this, " + h.LineId + ",true);", false, "", false, "50"),
                //Helper.GenerateHTMLTextbox(h.LineId, "Input3Description", h.Input3Description, "UpdateData(this, " + h.LineId + ",true);", false, "", false, "50"),
                //Helper.GenerateHTMLTextbox(h.LineId, "Input4Description", h.Input4Description, "UpdateData(this, " + h.LineId + ",false);", false, "", false, "50"),
                //Helper.GenerateHTMLTextbox(h.LineId, "Input5Description", h.Input5Description, "UpdateData(this, " + h.LineId + ",false);", false, "", false, "50"),
                //Helper.GenerateHTMLTextbox(h.LineId, "Input6Description", h.Input6Description, "UpdateData(this, " + h.LineId + ",false);", false, "", false, "50"),
                //Helper.GenerateHTMLTextbox(h.LineId, "Input7Description", h.Input7Description, "UpdateData(this, " + h.LineId + ",false);", false, "", false, "50"),

                var res = (from h in lstMCR
                           select new[] {
                                    Convert.ToString(h.LineId),
                                    h.ProductFormDesc,
                                    h.FormulaforVolumeCalculation,
                                    h.FormulaforVolumeCalculationInternalVolume,
                                    h.FormulaforCGCalculation,
                                    h.Input1Description,
                                    h.Input2Description,
                                    h.Input3Description,
                                    h.Input4Description,
                                    h.Input5Description,
                                    h.Input6Description,
                                    h.Input7Description,
                                    h.Input8Description,
                                    h.Input9Description,
                                    h.Input10Description,
                                    GenerateGridButton(h.LineId, "Attach", "Attach Sketch", "fa fa-image", "AttachSketch("+h.LineId+");" )+" "+GenerateGridButton(h.LineId, "Delete", "Delete Record", "fa fa-trash", "DeleteRecord("+h.LineId+");" ),
                                    }).ToList();

                res.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [NonAction]
        public static string GenerateGridButton(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", bool isDisabled = false)
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            htmlControl = "<a id='" + inputID + "' name='" + inputID + "' title='" + buttonTooltip + "' " + (isDisabled ? "disabled" : "") + "  class='btn btn-outline btn-circle btn-sm blue' " + onClickEvent + " ><i class='" + className + "'></i> </a>";

            return htmlControl;
        }

        public ActionResult GetProductForm(string term, string BU, string Location)
        {
            var objWCS002List = (from a in db.WCS001
                                 join b in db.WCS002 on a.HeaderId equals b.HeaderId
                                 where a.BU == BU && a.Location == Location
                                 select new
                                 {
                                     b.LineId,
                                     b.Material,
                                     b.ProductForm,
                                     b.UnitofMeasurement,
                                     a.ManufacturingCode
                                 }).ToList();

            if (!string.IsNullOrEmpty(term))
            {
                objWCS002List = (from u in objWCS002List where u.ProductForm.Trim().ToLower().Contains(term.Trim().ToLower()) select u).ToList();
            }

            var list1 = (from u in objWCS002List select new { Value = u.LineId, Text = u.ProductForm + "#" + u.Material + "#" + u.UnitofMeasurement + "#" + u.ManufacturingCode }).OrderBy(u => u.Value).ToList();

            return Json(list1, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSketchPartial(int LineId)
        {
            ViewBag.Image = "";
            var Files = (new clsFileUpload()).GetDocuments("WCS006/" + LineId);
            if (Files.Count() > 0)
            {
                ViewBag.Image = Files.FirstOrDefault().URL;
            }
            return PartialView("_GetSketchPartial");
        }

        [HttpPost]
        public ActionResult getLineData(JQueryDataTableParamModel param)
        {
            try
            {
                int HeaderId = param.Headerid != "" ? Convert.ToInt32(param.Headerid) : 0;

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = " HeaderId=" + HeaderId;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = { "ProductForm", "FormulaforVolumeCalculation", "FormulaforVolumeCalculationInternalVolume", "FormulaforCGCalculation",
                        "Input1Description","Input2Description","Input3Description","Input4Description","Input5Description","Input6Description","Input7Description","CreatedBy", "CONVERT(nvarchar(20),CreatedOn,103)" };
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(param.SearchFilter))
                        strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_WCS_GET_FORMULA_MASTER_DETAIL(StartIndex, EndIndex, strSortOrder, strWhere).ToList();

                int? totalRecords = lstResult.Select(i => i.TotalCount).FirstOrDefault();

                var data = (from uc in lstResult
                            select new[]
                            {
                           Convert.ToString(uc.ProductForm),
                           Convert.ToString(uc.FormulaforVolumeCalculation),
                           Convert.ToString(uc.FormulaforVolumeCalculationInternalVolume),
                           Convert.ToString(uc.FormulaforCGCalculation),
                           Convert.ToString(uc.Input1Description),
                           Convert.ToString(uc.Input2Description),
                           Convert.ToString(uc.Input3Description),
                           Convert.ToString(uc.Input4Description),
                           Convert.ToString(uc.Input5Description),
                           Convert.ToString(uc.Input6Description),
                           Convert.ToString(uc.Input7Description),
                           Convert.ToString(uc.CreatedBy),
                           Convert.ToDateTime(uc.CreatedOn).ToString("dd/MM/yyyy"),
                           //"<center>"+
                           //"<a onclick=\"ViewSketch("+uc.LineId+")\" cursor=\"pointer\"><i id=\"btnViewSketch\" name=\"btnView\" style=\"cursor:pointer; margin - left:5px\" Title=\"View Sketch\" class=\"iconspace fa fa-eye\" ></i></a>"+
                           //"<i id=\"btnUpdate\" name=\"btnUpdate\" style=\"cursor:pointer; margin-left:5px\" Title=\"Update Record\" class=\"fa fa-edit\" onClick=\"UpdateRecord("+uc.LineId+")\"></i>"+
                           //"<i id=\"btnDelete\" name=\"btnAction\" style=\"cursor:pointer;margin-left:5px\" Title=\"Delete Record\" class=\"fa fa-trash-o\" onClick=\"DeleteRecord("+uc.LineId+")\"></i>"+
                           //"</center>",
                            Convert.ToString(uc.LineId),
                           Convert.ToString(uc.LineId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveLine(FormCollection fc)
        {
            string ProductFormDesc = Convert.ToString(fc["ProductFormDesc0"]);
            string FormulaforVolumeCalculation = Convert.ToString(fc["FormulaforVolumeCalculation0"]);
            string FormulaforVolumeCalculationInternalVolume = Convert.ToString(fc["FormulaforVolumeCalculationInternalVolume0"]);
            string FormulaforCGCalculation = fc["FormulaforCGCalculation0"].ToString();
            string Input1Description = Convert.ToString(fc["Input1Description0"]);
            string Input2Description = Convert.ToString(fc["Input2Description0"]);
            string Input3Description = Convert.ToString(fc["Input3Description0"]);
            string Input4Description = Convert.ToString(fc["Input4Description0"]);
            string Input5Description = Convert.ToString(fc["Input5Description0"]);
            string Input6Description = Convert.ToString(fc["Input6Description0"]);
            string Input7Description = Convert.ToString(fc["Input7Description0"]);
            string Input8Description = Convert.ToString(fc["Input8Description0"]);
            string Input9Description = Convert.ToString(fc["Input9Description0"]);
            string Input10Description = Convert.ToString(fc["Input10Description0"]);

            int HeaderId = (fc["HeaderId"] != "" && fc["HeaderId"] != null) ? Convert.ToInt32(fc["HeaderId"].ToString()) : 0;

            clsHelper.ResponseMsgWCS objResponseMsg = new clsHelper.ResponseMsgWCS();
            try
            {
                if (!db.WCS006.Any(u => u.HeaderId == HeaderId && u.ProductFormDesc.Trim().ToLower() == ProductFormDesc.Trim().ToLower()))
                {
                    WCS006 objWCS006 = new WCS006();
                    objWCS006.HeaderId = HeaderId;
                    objWCS006.ProductFormDesc = ProductFormDesc;
                    objWCS006.FormulaforVolumeCalculation = FormulaforVolumeCalculation;
                    objWCS006.FormulaforVolumeCalculationInternalVolume = FormulaforVolumeCalculationInternalVolume;
                    objWCS006.FormulaforCGCalculation = FormulaforCGCalculation;
                    objWCS006.Input1Description = Input1Description;
                    objWCS006.Input2Description = Input2Description;
                    objWCS006.Input3Description = Input3Description;
                    objWCS006.Input4Description = Input4Description;
                    objWCS006.Input5Description = Input5Description;
                    objWCS006.Input6Description = Input6Description;
                    objWCS006.Input7Description = Input7Description;
                    objWCS006.Input8Description = Input8Description;
                    objWCS006.Input9Description = Input9Description;
                    objWCS006.Input10Description = Input10Description;
                    objWCS006.CreatedBy = objClsLoginInfo.UserName;
                    objWCS006.CreatedOn = DateTime.Now;
                    db.WCS006.Add(objWCS006);
                    db.SaveChanges();

                    objResponseMsg.LineId = objWCS006.LineId;

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetLineReferecnceInCalculation(int LineId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objWCS006 = db.WCS006.Where(u => u.LineId == LineId).FirstOrDefault();
                var objWCS005 = db.WCS005.Where(u => u.HeaderId == objWCS006.HeaderId).FirstOrDefault();

                var objWCS008List = (from a in db.WCS007
                                     join b in db.WCS008 on a.HeaderId equals b.HeaderId
                                     where a.BU == objWCS005.BU && a.Location == objWCS005.Location && b.ProductForm == objWCS006.ProductForm
                                     select b).ToList();

                List<WCS007> objWCS007List = new List<WCS007>();
                foreach (var item in objWCS008List)
                {
                    var objWCS007 = db.WCS007.Where(u => u.HeaderId == item.HeaderId).FirstOrDefault();
                    objWCS007List.Add(objWCS007);
                }

                if (objWCS007List.Count() > 0)
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = GetHtmlString(objWCS007List);
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No reference found";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        public string GetHtmlString(List<WCS007> list)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("<h4>Weight and CG calculation will be updated for below records. Are you sure want to update? </h4>");
            sb.Append("<br/>");
            sb.Append("<div style=\"max-height:500px;overflow:auto\">");
            sb.Append("<table id=\"tblDetails\" class=\"table table-bordered\">");
            sb.Append("<thead style='background-color:#b1eaef'><tr>");

            sb.Append("<th class=\"min-phone-l\">Sr No</th>" +
                "<th class=\"min-phone-l\">BU</th>" +
            "<th class=\"min-phone-l\">Location</th>" +
            "<th class=\"min-phone-l\">Job No</th>" +
            "<th class=\"min-phone-l\">Document No</th>" +
            "<th class=\"min-phone-l\">Equipment</th>" +
            "<th class=\"min-phone-l\">Equipment No.</th>" +
            "<th class=\"min-phone-l\">Customer</th>");

            sb.Append("</tr></thead>");

            sb.Append("<tbody>");

            int i = 1;
            foreach (var item in list)
            {
                sb.Append("<tr>");

                sb.Append("<td>" + i.ToString() + "</td>");
                sb.Append("<td>" + item.BU + "</td>");
                sb.Append("<td>" + item.Location + "</td>");
                sb.Append("<td>" + item.JobNo + "</td>");
                sb.Append("<td>" + item.DocumentNo + "</td>");
                sb.Append("<td>" + item.Equipment + "</td>");
                sb.Append("<td>" + item.EquipmentNo + "</td>");
                sb.Append("<td>" + item.Customer + "</td>");

                sb.Append("</tr>");

                i++;
            }

            sb.Append("</tbody>");

            sb.Append("</table>");
            sb.Append("</div>");
            return sb.ToString();
        }

        [HttpPost]
        public ActionResult UpdateLineData(string rowId, string columnName, string columnValue)
        {
            clsFormulaCalculator objFormulaCalc = new clsFormulaCalculator();
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int LineId = Convert.ToInt32(rowId);
                var objWCS006 = db.WCS006.Where(u => u.LineId == LineId).FirstOrDefault();

                columnValue = columnValue == null ? null : columnValue.Replace("'", "''");

                if (columnName == "ProductForm")
                {
                    if (columnValue != "")
                    {
                        int columnValue1 = Convert.ToInt32(columnValue);
                        if (!db.WCS006.Any(u => u.HeaderId == objWCS006.HeaderId && u.ProductForm == columnValue1 && u.LineId != LineId))
                        {
                            db.SP_COMMON_TABLE_UPDATE("WCS006", LineId, "LineId", columnName, columnValue, objClsLoginInfo.UserName);
                            objResponseMsg.Key = true;
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                        }
                    }
                    else
                    {
                        db.SP_COMMON_TABLE_UPDATE("WCS006", LineId, "LineId", columnName, null, objClsLoginInfo.UserName);
                        objResponseMsg.Key = true;
                    }
                }
                else
                {
                    db.SP_COMMON_TABLE_UPDATE("WCS006", LineId, "LineId", columnName, columnValue, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();

                    var objWCS005 = db.WCS005.Where(u => u.HeaderId == objWCS006.HeaderId).FirstOrDefault();
                    var objWCS008List = (from a in db.WCS007
                                         join b in db.WCS008 on a.HeaderId equals b.HeaderId
                                         where a.BU == objWCS005.BU && a.Location == objWCS005.Location && b.ProductForm == objWCS006.LineId
                                         select b).ToList();

                    string VolumeCalculationFormula = "";
                    string InnerVolumeCalculationFormula = "";
                    string CGVolumeCalculationFormula = "";

                    if (columnName == "FormulaforVolumeCalculation")//FormulaforVolumeCalculation
                        VolumeCalculationFormula = columnValue;
                    else if (columnName == "FormulaforVolumeCalculationInternalVolume")//FormulaforVolumeCalculationInternalVolume
                        InnerVolumeCalculationFormula = columnValue;
                    else if (columnName == "FormulaforCGCalculation")//FormulaforCGCalculation
                        CGVolumeCalculationFormula = columnValue;
                    else
                    {
                        VolumeCalculationFormula = objWCS006.FormulaforVolumeCalculation;
                        InnerVolumeCalculationFormula = objWCS006.FormulaforVolumeCalculationInternalVolume;
                        CGVolumeCalculationFormula = objWCS006.FormulaforCGCalculation;
                    }

                    string result = "success";
                    foreach (var item in objWCS008List)
                    {
                        decimal VolumeCalculate = 0;
                        decimal InnerVolumeofEquipment = 0;
                        decimal LocationL = 0;
                        decimal LocationX = item.LocationX != null ? Convert.ToDecimal(item.LocationX) : 0;

                        decimal UnitWeightofPart = 0;
                        decimal TotalWeightofPartW = 0;
                        decimal W_X_L = 0;
                        decimal HydroWeight = 0;
                        decimal OperatingWeig = 0;

                        decimal Qty = item.Qty != null ? Convert.ToDecimal(item.Qty) : 0;

                        decimal OperatingFluidDensity = item.OperatingFluidDensity != null ? Convert.ToDecimal(item.OperatingFluidDensity) : 0;
                        decimal DensityofWater = item.DensityofWater != null ? Convert.ToDecimal(item.DensityofWater) : 0;
                        decimal DensityofMaterial = item.DensityofMaterial != null ? Convert.ToDecimal(item.DensityofMaterial) : 0;

                        var objWCS007 = db.WCS007.Where(u => u.HeaderId == item.HeaderId).FirstOrDefault();
                        decimal ValueofPie = objWCS007.ValueofPie != null ? Convert.ToDecimal(objWCS007.ValueofPie) : 0;

                        string VolumeCalculateFormula = string.Empty;
                        string InnerVolumeofEquipmentFormula = string.Empty;
                        string LocationLFormula = string.Empty;

                        result = objFormulaCalc.CalculateWCSFormulas(VolumeCalculationFormula, InnerVolumeCalculationFormula, CGVolumeCalculationFormula
                            , item.Input1, item.Input1Value, item.Input2, item.Input2Value, item.Input3, item.Input3Value, item.Input4, item.Input4Value, item.Input5, item.Input5Value
                            , item.Input6, item.Input6Value, item.Input7, item.Input7Value, item.Input8, item.Input8Value, item.Input9, item.Input9Value, item.Input10, item.Input10Value
                            , ValueofPie, ref VolumeCalculate, ref InnerVolumeofEquipment, ref LocationL
                            , ref VolumeCalculateFormula, ref InnerVolumeofEquipmentFormula, ref LocationLFormula);

                        if (result.ToLower() == "success")
                        {
                            if (columnName == "FormulaforVolumeCalculation")//FormulaforVolumeCalculation
                            {
                                InnerVolumeofEquipment = item.InnerVolumeofEquipment != null ? Convert.ToDecimal(item.InnerVolumeofEquipment) : 0;
                                LocationL = item.LocationL != null ? Convert.ToDecimal(item.LocationL) : 0;
                            }
                            else if (columnName == "FormulaforVolumeCalculationInternalVolume")//FormulaforVolumeCalculationInternalVolume
                            {
                                VolumeCalculate = item.VolumeCalculate != null ? Convert.ToDecimal(item.VolumeCalculate) : 0;
                                LocationL = item.LocationL != null ? Convert.ToDecimal(item.LocationL) : 0;
                            }
                            else if (columnName == "FormulaforCGCalculation")//FormulaforCGCalculation
                            {
                                VolumeCalculate = item.VolumeCalculate != null ? Convert.ToDecimal(item.VolumeCalculate) : 0;
                                InnerVolumeofEquipment = item.InnerVolumeofEquipment != null ? Convert.ToDecimal(item.InnerVolumeofEquipment) : 0;
                            }

                            //calculation
                            UnitWeightofPart = Math.Round(DensityofMaterial * VolumeCalculate, 2);
                            TotalWeightofPartW = Math.Round(UnitWeightofPart * Qty, 2);

                            W_X_L = Math.Round(TotalWeightofPartW * LocationX, 2);
                            HydroWeight = Math.Round(InnerVolumeofEquipment * DensityofWater, 2);
                            OperatingWeig = Math.Round(InnerVolumeofEquipment * OperatingFluidDensity, 2);

                            item.VolumeCalculate = VolumeCalculate;
                            item.UnitWeightofPart = UnitWeightofPart;
                            item.TotalWeightofPartW = TotalWeightofPartW;
                            item.LocationL = LocationL;
                            item.WXL = W_X_L;
                            item.InnerVolumeofEquipment = InnerVolumeofEquipment;
                            item.HydroTestWeight = HydroWeight;
                            item.OperatingWeight = OperatingWeig;
                        }
                        else
                        {
                            break;
                        }
                    }

                    db.SaveChanges();

                    if (result.ToLower() != "success")
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = result;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteLine(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                WCS006 objWCS006 = db.WCS006.Where(x => x.LineId == Id).FirstOrDefault();
                if (objWCS006 != null)
                {
                    if (db.WCS008.Any(x => x.ProductForm == Id))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Line detail used in Weight & CG Calculation. You can't delete it.";
                    }
                    else
                    {
                        db.WCS006.Remove(objWCS006);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please refresh the page and try again.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Copy

        [HttpPost]
        public ActionResult LoadCopyDataPartial()
        {
            List<AutoCompleteModel> list = objClsManager.GetWCSBUList();
            ViewBag.BUList = (from u in list select new { CatID = u.Value, CatDesc = u.Text }).OrderBy(u => u.CatID).ToList();
            ViewBag.Location = objClsLoginInfo.Location;
            ViewBag.LocationDesc = db.COM002.Where(u => u.t_dtyp == 1 && u.t_dimx == objClsLoginInfo.Location).Select(u => u.t_dimx + " - " + u.t_desc).FirstOrDefault();

            return PartialView("_CopyDataPartial");
        }

        [HttpPost]
        public ActionResult CopyData(FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int HeaderId = fc["HeaderId"] != "" ? Convert.ToInt32(fc["HeaderId"].ToString()) : 0;
                string BU = fc["BU_Copy"].ToString().Trim();
                string Location = fc["Location_Copy"].ToString().Trim();

                //copy logic here
                if (!db.WCS005.Any(u => u.BU == BU && u.Location == Location))
                {
                    WCS005 objOldWCS005 = db.WCS005.Where(u => u.HeaderId == HeaderId).FirstOrDefault();

                    //copy header                     
                    WCS005 objWCS005 = new WCS005();
                    objWCS005.BU = BU;
                    objWCS005.Location = Location;

                    objWCS005.CreatedBy = objClsLoginInfo.UserName;
                    objWCS005.CreatedOn = DateTime.Now;
                    db.WCS005.Add(objWCS005);
                    db.SaveChanges();
                    int NewHeaderId = objWCS005.HeaderId;

                    //copy lines
                    List<WCS006> objWCS006List = new List<WCS006>();
                    var list = db.WCS006.Where(u => u.HeaderId == HeaderId).ToList();
                    list.ForEach(u =>
                    {
                        objWCS006List.Add(new WCS006
                        {
                            HeaderId = NewHeaderId,
                            ProductFormDesc = u.ProductFormDesc,
                            FormulaforVolumeCalculation = u.FormulaforVolumeCalculation,
                            FormulaforVolumeCalculationInternalVolume = u.FormulaforVolumeCalculationInternalVolume,
                            FormulaforCGCalculation = u.FormulaforCGCalculation,
                            Input1Description = u.Input1Description,
                            Input2Description = u.Input2Description,
                            Input3Description = u.Input3Description,
                            Input4Description = u.Input4Description,
                            Input5Description = u.Input5Description,
                            Input6Description = u.Input6Description,
                            Input7Description = u.Input7Description,
                            CreatedBy = objClsLoginInfo.UserName,
                            CreatedOn = DateTime.Now
                        });
                    });
                    db.WCS006.AddRange(objWCS006List);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.CopyRecord.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Import Excel

        [HttpPost]
        public ActionResult LoadImportDataPartial()
        {
            return PartialView("_UploadExcelPartial");
        }

        [HttpPost]
        [SessionExpireFilter]
        public ActionResult ImportExcel(HttpPostedFileBase upload, FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();

            if (Path.GetExtension(upload.FileName) == ".xlsx" || Path.GetExtension(upload.FileName) == ".xls")
            {
                int HeaderId = Convert.ToInt32(fc["HeaderId"].ToString());
                ExcelPackage package = new ExcelPackage(upload.InputStream);
                string Location = objClsLoginInfo.Location;

                bool isError = false;
                DataSet ds = GetDataFromExcel(package, HeaderId, out isError);
                if (!isError)
                {
                    try
                    {
                        //header data
                        List<int> lstHeaderId = new List<int>();
                        List<WCS006> lstWCS006 = new List<WCS006>();

                        foreach (DataRow item in ds.Tables[0].Rows)
                        {
                            //if (!string.IsNullOrWhiteSpace(item.Field<string>("BU")) && !string.IsNullOrWhiteSpace(item.Field<string>("Location")))
                            //{
                            //    #region Header 

                            //    WCS005 objWCS005 = new WCS005();
                            //    objWCS005.BU = item.Field<string>("BU");
                            //    objWCS005.Location = item.Field<string>("Location");
                            //    objWCS005.CreatedBy = objClsLoginInfo.UserName;
                            //    objWCS005.CreatedOn = DateTime.Now;
                            //    db.WCS005.Add(objWCS005);
                            //    db.SaveChanges();
                            //    NewHeaderid = objWCS005.HeaderId;

                            //    #endregion
                            //}

                            if (!string.IsNullOrWhiteSpace(item.Field<string>("Product Form")))
                            {
                                #region Lines

                                WCS006 objWCS006 = new WCS006();
                                objWCS006.HeaderId = HeaderId;
                                objWCS006.ProductFormDesc = item.Field<string>("Product Form");
                                objWCS006.FormulaforVolumeCalculation = item.Field<string>("Formula for Volume Calculation");
                                objWCS006.FormulaforVolumeCalculationInternalVolume = item.Field<string>("Formula for Volume Calculation Internal Volume");
                                objWCS006.FormulaforCGCalculation = item.Field<string>("Formula for CG Calculation");
                                objWCS006.Input1Description = item.Field<string>("Input 1 Description");
                                objWCS006.Input2Description = item.Field<string>("Input 2 Description");
                                objWCS006.Input3Description = item.Field<string>("Input 3 Description");
                                objWCS006.Input4Description = item.Field<string>("Input 4 Description");
                                objWCS006.Input5Description = item.Field<string>("Input 5 Description");
                                objWCS006.Input6Description = item.Field<string>("Input 6 Description");
                                objWCS006.Input7Description = item.Field<string>("Input 7 Description");
                                objWCS006.Input8Description = item.Field<string>("Input 8 Description");
                                objWCS006.Input9Description = item.Field<string>("Input 9 Description");
                                objWCS006.Input10Description = item.Field<string>("Input 10 Description");
                                objWCS006.CreatedBy = objClsLoginInfo.UserName;
                                objWCS006.CreatedOn = DateTime.Now;
                                lstWCS006.Add(objWCS006);

                                #endregion
                            }
                        }

                        if (lstWCS006.Count > 0)
                        {
                            db.WCS006.AddRange(lstWCS006);
                            db.SaveChanges();
                        }
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Formula Master imported successfully";
                    }
                    catch (Exception ex)
                    {
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    }
                }
                else
                {
                    objResponseMsg = ErrorExportToExcel(ds);
                }
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Excel file is not valid";
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public DataSet GetDataFromExcel(ExcelPackage package, int HeaderId, out bool isError)
        {
            ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();
            DataTable dtLinesExcel = new DataTable();
            isError = false;
            try
            {
                #region Read Lines data from excel 
                foreach (var firstRowCell in excelWorksheet.Cells[1, 1, 1, excelWorksheet.Dimension.End.Column])
                {
                    dtLinesExcel.Columns.Add(firstRowCell.Text);
                }

                for (var rowNumber = 2; rowNumber <= excelWorksheet.Dimension.End.Row; rowNumber++)
                {
                    var row = excelWorksheet.Cells[rowNumber, 1, rowNumber, excelWorksheet.Dimension.End.Column];
                    var newRow = dtLinesExcel.NewRow();

                    foreach (var cell in row)
                    {
                        newRow[cell.Start.Column - 1] = cell.Text;
                    }

                    dtLinesExcel.Rows.Add(newRow);
                }
                #endregion

                //#region Error columns for Header Data

                //dtLinesExcel.Columns.Add("BUErrorMsg", typeof(string));
                //dtLinesExcel.Columns.Add("LocationErrorMsg", typeof(string));

                //#endregion

                #region Error columns for Line Data

                dtLinesExcel.Columns.Add("ProductFormErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("FormulaforVolumeCalculationErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("FormulaforVolumeCalculationInternalVolumeErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("FormulaforCGCalculationErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("Input1DescriptionErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("Input2DescriptionErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("Input3DescriptionErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("Input4DescriptionErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("Input5DescriptionErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("Input6DescriptionErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("Input7DescriptionErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("Input8DescriptionErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("Input9DescriptionErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("Input10DescriptionErrorMsg", typeof(string));

                #endregion

                #region Validation 

                //List<AutoCompleteModel> lstBUList = objClsManager.GetWCSBUList();

                List<WCS006> lstExistingWCS006 = db.WCS006.Where(x => x.HeaderId == HeaderId).ToList();

                List<WCS006> lstSheetWCS006 = new List<WCS006>();

                string BU1 = string.Empty;
                string Location1 = string.Empty;

                foreach (DataRow item in dtLinesExcel.Rows)
                {
                    #region string variables

                    string errorMessage = string.Empty;

                    //string BU = item.Field<string>("BU");
                    //string Location = item.Field<string>("Location");
                    string ProductForm = item.Field<string>("Product Form");
                    string FormulaforVolumeCalculation = item.Field<string>("Formula for Volume Calculation");
                    string FormulaforVolumeCalculationInternalVolume = item.Field<string>("Formula for Volume Calculation Internal Volume");
                    string FormulaforCGCalculation = item.Field<string>("Formula for CG Calculation");

                    string Input1Description = item.Field<string>("Input 1 Description");
                    string Input2Description = item.Field<string>("Input 2 Description");
                    string Input3Description = item.Field<string>("Input 3 Description");
                    string Input4Description = item.Field<string>("Input 4 Description");
                    string Input5Description = item.Field<string>("Input 5 Description");
                    string Input6Description = item.Field<string>("Input 6 Description");
                    string Input7Description = item.Field<string>("Input 7 Description");
                    string Input8Description = item.Field<string>("Input 8 Description");
                    string Input9Description = item.Field<string>("Input 9 Description");
                    string Input10Description = item.Field<string>("Input 10 Description");

                    #endregion

                    #region Validate Header

                    //if (!string.IsNullOrWhiteSpace(BU) || !string.IsNullOrWhiteSpace(Location))
                    //{
                    //    if (string.IsNullOrWhiteSpace(BU))
                    //    {
                    //        item["BUErrorMsg"] = "BU is Required"; isError = true;
                    //    }
                    //    else
                    //    {
                    //        if (!lstBUList.Any(u => u.Value.Trim().ToLower() == BU.Trim().ToLower()))
                    //        {
                    //            item["BUErrorMsg"] = "BU is not belongs to User"; isError = true;
                    //        }
                    //    }

                    //    if (string.IsNullOrWhiteSpace(Location))
                    //    {
                    //        item["LocationErrorMsg"] = "Location is Required"; isError = true;
                    //    }
                    //    else
                    //    {
                    //        if (objClsLoginInfo.Location.Trim().ToLower() != Location.Trim().ToLower())
                    //        {
                    //            item["LocationErrorMsg"] = "Location is not belongs to User"; isError = true;
                    //        }
                    //    }
                    //}

                    //if (!string.IsNullOrWhiteSpace(BU) && !string.IsNullOrWhiteSpace(Location))
                    //{
                    //    if (lstExistingWCS005.Any(u => u.BU.Trim().ToLower() == BU.Trim().ToLower() && u.Location.Trim().ToLower() == Location.Trim().ToLower()))
                    //    {
                    //        item["LocationErrorMsg"] = "Location is Already Exist"; isError = true;
                    //    }
                    //    else if (lstSheetWCS005.Any(u => u.BU.Trim().ToLower() == BU.Trim().ToLower() && u.Location.Trim().ToLower() == Location.Trim().ToLower()))
                    //    {
                    //        item["LocationErrorMsg"] = "Location is Already Exist in this sheet"; isError = true;
                    //    }
                    //    lstSheetWCS005.Add(new WCS005()
                    //    {
                    //        BU = BU,
                    //        Location = Location
                    //    });
                    //    BU1 = BU;
                    //    Location1 = Location;
                    //    lstSheetWCS006 = new List<WCS006>();
                    //}

                    #endregion

                    #region Validate Lines

                    if (!string.IsNullOrWhiteSpace(ProductForm) || !string.IsNullOrWhiteSpace(FormulaforVolumeCalculation) || !string.IsNullOrWhiteSpace(FormulaforVolumeCalculationInternalVolume) ||
                              !string.IsNullOrWhiteSpace(FormulaforCGCalculation) ||
                              !string.IsNullOrWhiteSpace(Input1Description) || !string.IsNullOrWhiteSpace(Input2Description) ||
                              !string.IsNullOrWhiteSpace(Input3Description))
                    {
                        //ProductForm
                        if (string.IsNullOrWhiteSpace(ProductForm))
                        {
                            item["ProductFormErrorMsg"] = "Product Form is Required"; isError = true;
                        }
                        else
                        {
                            if (ProductForm.Length > 50)
                            {
                                item["ProductFormErrorMsg"] = "Product Form have maximum limit of 50 characters"; isError = true;
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(ProductForm))
                        {
                            if (lstExistingWCS006.Any(u => u.ProductFormDesc.Trim().ToLower() == ProductForm.Trim().ToLower()))
                            {
                                item["ProductFormErrorMsg"] = "Product Form is Already Exist"; isError = true;
                            }
                            else if (lstSheetWCS006.Any(u => u.ProductFormDesc.Trim().ToLower() == ProductForm.Trim().ToLower()))
                            {
                                item["ProductFormErrorMsg"] = "Product Form is Already Exist in this sheet"; isError = true;
                            }

                            lstSheetWCS006.Add(new WCS006()
                            {
                                ProductFormDesc = ProductForm
                            });
                        }

                        //FormulaforVolumeCalculation
                        //if (string.IsNullOrWhiteSpace(FormulaforVolumeCalculation))
                        //{
                        //    item["FormulaforVolumeCalculationErrorMsg"] = "Formula for Volume Calculation is Required"; isError = true;
                        //}
                        //else
                        //{
                        //    if (FormulaforVolumeCalculation.Length > 300)
                        //    {
                        //        item["FormulaforVolumeCalculationErrorMsg"] = "Formula for Volume Calculation have maximum limit of 300 characters"; isError = true;
                        //    }
                        //}

                        ////FormulaforVolumeCalculationInternalVolume
                        //if (string.IsNullOrWhiteSpace(FormulaforVolumeCalculationInternalVolume))
                        //{
                        //    item["FormulaforVolumeCalculationInternalVolumeErrorMsg"] = "Formula for Volume Calculation Internal Volume is Required"; isError = true;
                        //}
                        //else
                        //{
                        //    if (FormulaforVolumeCalculation.Length > 300)
                        //    {
                        //        item["FormulaforVolumeCalculationInternalVolumeErrorMsg"] = "Formula for Volume Calculation Internal Volume have maximum limit of 300 characters"; isError = true;
                        //    }
                        //}

                        ////FormulaforVolumeCalculationInternalVolume
                        //if (string.IsNullOrWhiteSpace(FormulaforCGCalculation))
                        //{
                        //    item["FormulaforCGCalculationErrorMsg"] = "Formula for CG Calculation is Required"; isError = true;
                        //}
                        //else
                        //{
                        //    if (FormulaforCGCalculation.Length > 300)
                        //    {
                        //        item["FormulaforCGCalculationErrorMsg"] = "Formula for CG Calculation have maximum limit of 300 characters"; isError = true;
                        //    }
                        //}

                        //Input1Description
                        //if (string.IsNullOrWhiteSpace(Input1Description))
                        //{
                        //    item["Input1DescriptionErrorMsg"] = "Input 1 Description is Required"; isError = true;
                        //}
                        //else
                        //{
                        //    if (Input1Description.Length > 50)
                        //    {
                        //        item["Input1DescriptionErrorMsg"] = "Input 1 Description have maximum limit of 50 characters"; isError = true;
                        //    }
                        //}

                        ////Input2Description
                        //if (string.IsNullOrWhiteSpace(Input2Description))
                        //{
                        //    item["Input2DescriptionErrorMsg"] = "Input 2 Description is Required"; isError = true;
                        //}
                        //else
                        //{
                        //    if (Input2Description.Length > 50)
                        //    {
                        //        item["Input2DescriptionErrorMsg"] = "Input 2 Description have maximum limit of 50 characters"; isError = true;
                        //    }
                        //}

                        ////Input3Description
                        //if (string.IsNullOrWhiteSpace(Input3Description))
                        //{
                        //    item["Input3DescriptionErrorMsg"] = "Input 3 Description is Required"; isError = true;
                        //}
                        //else
                        //{
                        //    if (Input3Description.Length > 50)
                        //    {
                        //        item["Input3DescriptionErrorMsg"] = "Input 3 Description have maximum limit of 50 characters"; isError = true;
                        //    }
                        //}

                        //FormulaforVolumeCalculation
                        if (!string.IsNullOrWhiteSpace(FormulaforVolumeCalculation))
                        {
                            if (FormulaforVolumeCalculation.Length > 300)
                            {
                                item["FormulaforVolumeCalculationErrorMsg"] = "Formula for Volume Calculation have maximum limit of 300 characters"; isError = true;
                            }
                        }

                        //FormulaforVolumeCalculationInternalVolume
                        if (!string.IsNullOrWhiteSpace(FormulaforVolumeCalculationInternalVolume))
                        {
                            if (FormulaforVolumeCalculationInternalVolume.Length > 300)
                            {
                                item["FormulaforVolumeCalculationInternalVolumeErrorMsg"] = "Formula for Volume Calculation Internal Volume have maximum limit of 300 characters"; isError = true;
                            }
                        }

                        //FormulaforCGCalculation
                        if (!string.IsNullOrWhiteSpace(FormulaforCGCalculation))
                        {
                            if (FormulaforCGCalculation.Length > 300)
                            {
                                item["FormulaforCGCalculationErrorMsg"] = "Formula for CG Calculation have maximum limit of 300 characters"; isError = true;
                            }
                        }

                        //Input1Description
                        if (!string.IsNullOrWhiteSpace(Input1Description))
                        {
                            if (Input1Description.Length > 50)
                            {
                                item["Input1DescriptionErrorMsg"] = "Input 1 Description have maximum limit of 50 characters"; isError = true;
                            }
                        }

                        //Input2Description
                        if (!string.IsNullOrWhiteSpace(Input2Description))
                        {
                            if (Input2Description.Length > 50)
                            {
                                item["Input2DescriptionErrorMsg"] = "Input 2 Description have maximum limit of 50 characters"; isError = true;
                            }
                        }

                        //Input3Description
                        if (!string.IsNullOrWhiteSpace(Input3Description))
                        {
                            if (Input3Description.Length > 50)
                            {
                                item["Input3DescriptionErrorMsg"] = "Input 3 Description have maximum limit of 50 characters"; isError = true;
                            }
                        }

                        //Input4Description
                        if (!string.IsNullOrWhiteSpace(Input4Description))
                        {
                            if (Input4Description.Length > 50)
                            {
                                item["Input4DescriptionErrorMsg"] = "Input 4 Description have maximum limit of 50 characters"; isError = true;
                            }
                        }

                        //Input5Description
                        if (!string.IsNullOrWhiteSpace(Input5Description))
                        {
                            if (Input5Description.Length > 50)
                            {
                                item["Input5DescriptionErrorMsg"] = "Input 5 Description have maximum limit of 50 characters"; isError = true;
                            }
                        }

                        //Input6Description
                        if (!string.IsNullOrWhiteSpace(Input6Description))
                        {
                            if (Input6Description.Length > 50)
                            {
                                item["Input6DescriptionErrorMsg"] = "Input 6 Description have maximum limit of 50 characters"; isError = true;
                            }
                        }

                        //Input7Description
                        if (!string.IsNullOrWhiteSpace(Input7Description))
                        {
                            if (Input7Description.Length > 50)
                            {
                                item["Input7DescriptionErrorMsg"] = "Input 7 Description have maximum limit of 50 characters"; isError = true;
                            }
                        }

                        //Input8Description
                        if (!string.IsNullOrWhiteSpace(Input8Description))
                        {
                            if (Input8Description.Length > 50)
                            {
                                item["Input8DescriptionErrorMsg"] = "Input 8 Description have maximum limit of 50 characters"; isError = true;
                            }
                        }

                        //Input9Description
                        if (!string.IsNullOrWhiteSpace(Input9Description))
                        {
                            if (Input9Description.Length > 50)
                            {
                                item["Input9DescriptionErrorMsg"] = "Input 9 Description have maximum limit of 50 characters"; isError = true;
                            }
                        }

                        //Input10Description
                        if (!string.IsNullOrWhiteSpace(Input10Description))
                        {
                            if (Input10Description.Length > 50)
                            {
                                item["Input10DescriptionErrorMsg"] = "Input 10 Description have maximum limit of 50 characters"; isError = true;
                            }
                        }
                    }

                    #endregion
                }

                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            DataSet ds = new DataSet();
            ds.Tables.Add(dtLinesExcel);
            return ds;
        }

        public clsHelper.ResponceMsgWithFileName ErrorExportToExcel(DataSet ds)
        {
            clsHelper.ResponceMsgWithFileName objResponseMsg = new clsHelper.ResponceMsgWithFileName();
            try
            {
                MemoryStream ms = new MemoryStream();
                using (FileStream fs = System.IO.File.OpenRead(Server.MapPath("~/Resources/WCS/Maintain Formula Master - Error Template.xlsx")))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();

                        int i = 2;
                        foreach (DataRow item in ds.Tables[0].Rows)
                        {
                            #region Generate for Header

                            ///BU

                            //if (!string.IsNullOrWhiteSpace(item.Field<string>(16)))
                            //{
                            //    excelWorksheet.Cells[i, 1].Value = item.Field<string>(0) + " (Note: " + item.Field<string>(16).ToString() + " )";
                            //    excelWorksheet.Cells[i, 1].Style.Font.Color.SetColor(Color.Red); ;
                            //}
                            //else { excelWorksheet.Cells[i, 1].Value = item.Field<string>(0); }

                            /////Location
                            //if (!string.IsNullOrWhiteSpace(item.Field<string>(17)))
                            //{
                            //    excelWorksheet.Cells[i, 2].Value = item.Field<string>(1) + " (Note: " + item.Field<string>(17).ToString() + " )";
                            //    excelWorksheet.Cells[i, 2].Style.Font.Color.SetColor(Color.Red); ;
                            //}
                            //else { excelWorksheet.Cells[i, 2].Value = item.Field<string>(1); }

                            #endregion

                            #region Common Columns for Lines
                            ///lines
                            ///
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(14)))
                            {
                                excelWorksheet.Cells[i, 1].Value = item.Field<string>(0) + " (Note: " + item.Field<string>(14).ToString() + " )";
                                excelWorksheet.Cells[i, 1].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 1].Value = item.Field<string>(0); }

                            if (!string.IsNullOrWhiteSpace(item.Field<string>(15)))
                            {
                                excelWorksheet.Cells[i, 2].Value = item.Field<string>(1) + " (Note: " + item.Field<string>(15).ToString() + " )";
                                excelWorksheet.Cells[i, 2].Style.Font.Color.SetColor(Color.Red);
                            }
                            else { excelWorksheet.Cells[i, 2].Value = item.Field<string>(1); }

                            if (!string.IsNullOrWhiteSpace(item.Field<string>(16)))
                            {
                                excelWorksheet.Cells[i, 3].Value = item.Field<string>(2) + " (Note: " + item.Field<string>(16).ToString() + " )";
                                excelWorksheet.Cells[i, 3].Style.Font.Color.SetColor(Color.Red);
                            }
                            else { excelWorksheet.Cells[i, 3].Value = item.Field<string>(2); }

                            if (!string.IsNullOrWhiteSpace(item.Field<string>(17)))
                            {
                                excelWorksheet.Cells[i, 4].Value = item.Field<string>(3) + " (Note: " + item.Field<string>(17).ToString() + " )";
                                excelWorksheet.Cells[i, 4].Style.Font.Color.SetColor(Color.Red);
                            }
                            else { excelWorksheet.Cells[i, 4].Value = item.Field<string>(3); }

                            if (!string.IsNullOrWhiteSpace(item.Field<string>(18)))
                            {
                                excelWorksheet.Cells[i, 5].Value = item.Field<string>(4) + " (Note: " + item.Field<string>(18).ToString() + " )";
                                excelWorksheet.Cells[i, 5].Style.Font.Color.SetColor(Color.Red);
                            }
                            else { excelWorksheet.Cells[i, 5].Value = item.Field<string>(4); }

                            if (!string.IsNullOrWhiteSpace(item.Field<string>(19)))
                            {
                                excelWorksheet.Cells[i, 6].Value = item.Field<string>(5) + " (Note: " + item.Field<string>(19).ToString() + " )";
                                excelWorksheet.Cells[i, 6].Style.Font.Color.SetColor(Color.Red);
                            }
                            else { excelWorksheet.Cells[i, 6].Value = item.Field<string>(5); }

                            if (!string.IsNullOrWhiteSpace(item.Field<string>(20)))
                            {
                                excelWorksheet.Cells[i, 7].Value = item.Field<string>(6) + " (Note: " + item.Field<string>(20).ToString() + " )";
                                excelWorksheet.Cells[i, 7].Style.Font.Color.SetColor(Color.Red);
                            }
                            else { excelWorksheet.Cells[i, 7].Value = item.Field<string>(6); }

                            if (!string.IsNullOrWhiteSpace(item.Field<string>(21)))
                            {
                                excelWorksheet.Cells[i, 8].Value = item.Field<string>(7) + " (Note: " + item.Field<string>(21).ToString() + " )";
                                excelWorksheet.Cells[i, 8].Style.Font.Color.SetColor(Color.Red);
                            }
                            else { excelWorksheet.Cells[i, 8].Value = item.Field<string>(7); }

                            if (!string.IsNullOrWhiteSpace(item.Field<string>(22)))
                            {
                                excelWorksheet.Cells[i, 9].Value = item.Field<string>(8) + " (Note: " + item.Field<string>(22).ToString() + " )";
                                excelWorksheet.Cells[i, 9].Style.Font.Color.SetColor(Color.Red);
                            }
                            else { excelWorksheet.Cells[i, 9].Value = item.Field<string>(8); }

                            if (!string.IsNullOrWhiteSpace(item.Field<string>(23)))
                            {
                                excelWorksheet.Cells[i, 10].Value = item.Field<string>(9) + " (Note: " + item.Field<string>(23).ToString() + " )";
                                excelWorksheet.Cells[i, 10].Style.Font.Color.SetColor(Color.Red);
                            }
                            else { excelWorksheet.Cells[i, 10].Value = item.Field<string>(9); }

                            if (!string.IsNullOrWhiteSpace(item.Field<string>(24)))
                            {
                                excelWorksheet.Cells[i, 11].Value = item.Field<string>(25) + " (Note: " + item.Field<string>(24).ToString() + " )";
                                excelWorksheet.Cells[i, 11].Style.Font.Color.SetColor(Color.Red);
                            }
                            else { excelWorksheet.Cells[i, 11].Value = item.Field<string>(10); }

                            if (!string.IsNullOrWhiteSpace(item.Field<string>(25)))
                            {
                                excelWorksheet.Cells[i, 12].Value = item.Field<string>(11) + " (Note: " + item.Field<string>(25).ToString() + " )";
                                excelWorksheet.Cells[i, 12].Style.Font.Color.SetColor(Color.Red);
                            }
                            else { excelWorksheet.Cells[i, 12].Value = item.Field<string>(11); }

                            if (!string.IsNullOrWhiteSpace(item.Field<string>(26)))
                            {
                                excelWorksheet.Cells[i, 13].Value = item.Field<string>(12) + " (Note: " + item.Field<string>(26).ToString() + " )";
                                excelWorksheet.Cells[i, 13].Style.Font.Color.SetColor(Color.Red);
                            }
                            else { excelWorksheet.Cells[i, 13].Value = item.Field<string>(12); }

                            if (!string.IsNullOrWhiteSpace(item.Field<string>(27)))
                            {
                                excelWorksheet.Cells[i, 14].Value = item.Field<string>(13) + " (Note: " + item.Field<string>(27).ToString() + " )";
                                excelWorksheet.Cells[i, 14].Style.Font.Color.SetColor(Color.Red);
                            }
                            else { excelWorksheet.Cells[i, 14].Value = item.Field<string>(13); }

                            #endregion

                            i++;
                        }

                        excelPackage.SaveAs(ms);
                    }
                }

                ms.Position = 0;

                string fileName;
                string excelFilePath = Helper.ExcelFilePath("ErrorList", out fileName);

                Byte[] bin = ms.ToArray();
                System.IO.File.WriteAllBytes(excelFilePath, bin);

                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error";
                objResponseMsg.FileName = fileName;
                return objResponseMsg;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }

        #endregion

        #region Export Excel

        public ActionResult GenerateExcelHeader(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string BU = "";
                List<AutoCompleteModel> BUList = objClsManager.GetWCSBUList();
                foreach (var bus in BUList)
                {
                    BU += "'" + bus.Value + "',";
                }
                BU = BU.TrimEnd(',');

                string Location = objClsLoginInfo.Location;

                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_WCS_GET_FORMULA_MASTER_HEADER(1, int.MaxValue, strSortOrder, whereCondition, BU, Location).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }

                    var newlst = (from li in lst
                                  select new
                                  {
                                      BU = Convert.ToString(li.BU),
                                      Location = Convert.ToString(li.Location),
                                      CreatedBy = Convert.ToString(li.CreatedBy),
                                      CreatedOn = Convert.ToDateTime(li.CreatedOn).ToString("dd/MM/yyyy"),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GenerateExcelLines(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                var lst = db.SP_WCS_GET_FORMULA_MASTER_DETAIL(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Data Found";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                var newlst = (from li in lst
                              select new
                              {
                                  ProductForm = Convert.ToString(li.ProductFormDesc),
                                  FormulaforVolumeCalculation = Convert.ToString(li.FormulaforVolumeCalculation),
                                  FormulaforVolumeCalculationInternalVolume = Convert.ToString(li.FormulaforVolumeCalculationInternalVolume),
                                  FormulaforCGCalculation = Convert.ToString(li.FormulaforCGCalculation),
                                  Input1Description = Convert.ToString(li.Input1Description),
                                  Input2Description = Convert.ToString(li.Input2Description),
                                  Input3Description = Convert.ToString(li.Input3Description),
                                  Input4Description = Convert.ToString(li.Input4Description),
                                  Input5Description = Convert.ToString(li.Input5Description),
                                  Input6Description = Convert.ToString(li.Input6Description),
                                  Input7Description = Convert.ToString(li.Input7Description),
                                  Input8Description = Convert.ToString(li.Input8Description),
                                  Input9Description = Convert.ToString(li.Input9Description),
                                  Input10Description = Convert.ToString(li.Input10Description),
                                  //CreatedBy = Convert.ToString(li.CreatedBy),
                                  //CreatedOn = Convert.ToDateTime(li.CreatedOn).ToString("dd/MM/yyyy"),
                              }).ToList();

                strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion
    }
}