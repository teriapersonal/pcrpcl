﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQS.Models;

namespace IEMQS.Areas.Utility.Controllers
{
    public class NumberGroupController : clsBase
    {
        // GET: Utility/NumberGroup
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult LoadNumberGroupData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                //string strWhere = Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();
                string strWhere = string.Empty;
                strWhere += "1=1";
                string[] columnName = { "NumberGroup", "Description", "SeriesLength"};
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);

                //strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName);
                var lstResult = db.SP_GET_NUMBER_GROUP_HEADER
                                (
                               StartIndex,
                               EndIndex,
                               "",
                               strWhere
                                ).ToList();
                int newRecordId = 0;
                var newRecord = new[] {
                                    Helper.GenerateTextbox(newRecordId, "NumberGroup","","",false,"","3"),
                                    Helper.GenerateTextbox(newRecordId, "Description","","",false,"","100"),
                                    Helper.GenerateTextbox(newRecordId, "SeriesLength","","",false,"number",""),
                                    Helper.GenerateCheckbox(newRecordId,"IsActive",false),
                                    GenerateGridButton(newRecordId, "Add", "Add Rocord", "fa fa-plus", "SaveNewRecord();" ),
                                    GenerateHiddenFor(newRecordId, "Id", "0"),
                                };
                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.NumberGroup),
                                Helper.GenerateHTMLTextboxOnChanged(uc.Id,"Description",uc.Description,"UpdateData(this, "+ uc.Id +");",false,"",false,"100"),
                                Helper.GenerateHTMLTextboxOnChanged(uc.Id,"SeriesLength",Convert.ToString(uc.SeriesLength),"UpdateData(this, "+ uc.Id +");",false,"",false),
                                "",
                                "<center><a class='btn btn-xs green' href='/Utility/NumberGroup/NumberGroupLines?id="+Convert.ToInt32(uc.Id)+"'>View<i style='margin-left:5px;' class='fa fa-eye'></i></a><i id='btnDelete' name='btnAction' style='cursor:pointer; ' Title='Delete Record' class='fa fa-trash - o' onClick='DeleteLines(" +uc.Id+ ")'></i></center>",
                                Convert.ToString(uc.Id)
                                

                            }).ToList();
                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SaveNewRecord(FormCollection fc)
        {
            int newRowIndex = 0;
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                GLB003 objGLB003 = new GLB003();
                
                objGLB003.NumberGroup = fc["NumberGroup" + newRowIndex];
                objGLB003.Description = fc["Description" + newRowIndex];
                objGLB003.SeriesLength = Convert.ToInt32(fc["SeriesLength" + newRowIndex]);
                objGLB003.IsActive = Convert.ToBoolean(fc["IsActive" + newRowIndex]);
                objGLB003.CreatedBy = objClsLoginInfo.UserName;
                objGLB003.CreatedOn = DateTime.Now;
                
                db.GLB003.Add(objGLB003);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Record added successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please Try Again";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateData(int Id, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    db.SP_UPDATE_NUMBER_GROUP_HEADER_COLUMN(Id, columnName, columnValue);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Record Updated Successfully" ;
                    
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteLine(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                GLB003 objGLB003 = db.GLB003.Where(x => x.Id == Id ).FirstOrDefault();
                if (objGLB003 != null)
                {
                    db.GLB003.Remove(objGLB003);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details successfully deleted.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details not available for delete.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please Try Again";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public static string GenerateHiddenFor(int rowId, string columnName, string columnValue)
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputValue = columnValue;

            htmlControl = "<input type='hidden' id='" + inputID + "' value='" + inputValue + "' name='" + inputID + "' colname='" + columnName + "'/>";

            return htmlControl;
        }
        [NonAction]
        public static string GenerateGridButton(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";


            htmlControl = "<a id='" + inputID + "' name='" + inputID + "' class='btn btn-outline btn-circle btn-sm blue' " + onClickEvent + " ><i class='" + className + "'></i> " + buttonName + "</a>";

            //htmlControl = "<i  data-modal='' id='" + inputID + "' name='" + inputID + "' style='cursor:Pointer;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";

            return htmlControl;
        }
    }

}