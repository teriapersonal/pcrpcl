﻿using IEMQS.Models;
using IEMQSImplementation;
using IEMQSImplementation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.Utility.Controllers
{
    public class MailTamplateMasterController : clsBase

    {
        [SessionExpireFilter]
        public ActionResult Index(string id = "")
        {
            if (!string.IsNullOrWhiteSpace(id))
            {
                ViewBag.action = id;
            }
             ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("MAIL001");
            return View();
        }

        [SessionExpireFilter]
        public ActionResult LoadDataTable(JQueryDataTableParamModel param)
        {

            //var MailTamplateMastersDetails = db.MAIL001.ToList();
            //IEnumerable<MAIL001> filteredTasks = null;

            //var isTamplateIDSortable = Convert.ToBoolean(Request["bSortable_6"]);
            //var isDescriptionSortable = Convert.ToBoolean(Request["bSortable_5"]);
            //var isSubjectSortable = Convert.ToBoolean(Request["bSortable_4"]);
            //var isBCCEmailSortable = Convert.ToBoolean(Request["bSortable_3"]);
            //var isCCEmailSortable = Convert.ToBoolean(Request["bSortable_2"]);
            ////var isSMTPServerSortable = Convert.ToBoolean(Request["bSortable_1"]);
            //var isTamplateNameSortable = Convert.ToBoolean(Request["bSortable_0"]);
            //var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
            //Func<MAIL001, string> orderingFunction = (tsk =>
            //                        sortColumnIndex == 0 && isTamplateIDSortable ? Convert.ToString(tsk.TemplateId) :
            //                        sortColumnIndex == 1 && isDescriptionSortable ? tsk.Description :
            //                        sortColumnIndex == 2 && isSubjectSortable ? tsk.Subject :
            //                        sortColumnIndex == 3 && isBCCEmailSortable ? tsk.BCCEmail :
            //                        sortColumnIndex == 4 && isCCEmailSortable ? tsk.CCEmail :
            //                        //sortColumnIndex == 5 && isSMTPServerSortable ? tsk.SMTPMaster.SMTPServer :
            //                        sortColumnIndex == 6 && isTamplateNameSortable ? tsk.TamplateName :
            //                        "");
            //var sortDirection = Request["sSortDir_0"]; // asc or desc
            //if (sortDirection == "asc")
            //    filteredTasks = filteredTasks.OrderBy(orderingFunction);
            //else
            //    filteredTasks = filteredTasks.OrderByDescending(orderingFunction);

            //var lstMailTamplateMastersDetails = filteredTasks.Skip(param.iDisplayStart).Take(param.iDisplayLength);
            var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
            int? StartIndex = param.iDisplayStart + 1;
            int? EndIndex = param.iDisplayStart + param.iDisplayLength;
            string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
            string sortDirection = Convert.ToString(Request["sSortDir_0"]);
            string strSortOrder = string.Empty;
            string whereCondition = "1=1";
            if (!string.IsNullOrWhiteSpace(param.sSearch))
            {
                whereCondition += " and (TamplateName like '%" + param.sSearch + "%' or CCEmail like '%" + param.sSearch + "%' or BCCEmail like '%" + param.sSearch + "%' or Subject like '%" + param.sSearch + "%'  or Body like '%" + param.sSearch + "%')";
            }
            else
            {
                whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
            }
            if (!string.IsNullOrWhiteSpace(sortColumnName))
            {
                strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
            }
            var lstTemplate = db.SP_GET_MAILTEMPLATES(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

            int? totalRecords = lstTemplate.Select(i => i.TotalCount).FirstOrDefault();
            var result = from mtd in lstTemplate
                         select new[] {
                mtd.TamplateName,
                mtd.CCEmail,
                mtd.BCCEmail,
                mtd.Subject,
                mtd.Description,
                GetEditButtonStyle(mtd.Body,mtd.TemplateId,  mtd.TamplateName),
                GetActionStyle(mtd.IsActive,mtd.TemplateId),
            };
            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                aaData = result,
                strSortOrder = strSortOrder,
                whereCondition = whereCondition
            }, JsonRequestBehavior.AllowGet);
        }

        public string GetEditButtonStyle(string body, int templateId,string templateName)
        {
            string function = "EditTemplateBody( " + templateId + ",\'" + templateName + "\')";
            if (!string.IsNullOrWhiteSpace(body))
            {  
                return "<center><a class=\"btn btn-xs green\"  Title=\"Edit Template Body\" onClick=\"EditTemplateBody(" + templateId + ",'" + templateName + "')\" >Edit Template Body</a></center>";
            }
            else
            {
                return "<center><a class=\"btn btn-xs blue\"  Title=\"Add Template Body\" onClick=\"EditTemplateBody(" + templateId + ",'" + templateName + "')\" >Add Template Body</a></center>";

                // return "<center><a class='btn btn-xs blue'  href='javascript: void(O); ' Title='Add Template Body' onClick=" + function + ")'>Add Template Body</a></center>";
            }
        }

        public string GetActionStyle(bool? IsActive, int templateId)
        {
            if (IsActive != null && Convert.ToBoolean(IsActive) != false)
            {
                return "<center><i id='btnActiveTemplate' name='btnAction' style='cursor: pointer;'  Title='Deactive Template' class='fa fa-calendar-check-o'  onclick='ActiveTemplate(" + templateId + ")' ></i>&nbsp;&nbsp<i id='btnEditTemplate' name='btnAction' style='cursor: pointer;'  Title='Edit Template' class='fa fa-pencil - square - o'  onclick='EditTemplate(" + templateId + ")' ></i>&nbsp;&nbsp<i id='btnDelete' name='btnAction' style='cursor: pointer; ' Title='Delete Template' class='fa fa-trash - o' onclick='DeleteTemplate(" + templateId + ")'></i></center>";
            }
            else
            {
                return "<center><i id='btnActiveTemplate' name='btnAction' style='cursor: pointer;'  Title='Active Template' class='fa fa-calendar-times-o'  onclick='ActiveTemplate(" + templateId + ")' ></i>&nbsp;&nbsp<i id='btnEditTemplate' name='btnAction' style='cursor: pointer;'  Title='Edit Template' class='fa fa-pencil - square - o'  onclick='EditTemplate(" + templateId + ")' ></i>&nbsp;&nbsp<i id='btnDelete' name='btnAction' style='cursor: pointer; ' Title='Delete Template' class='fa fa-trash - o' onclick='DeleteTemplate(" + templateId + ")'></i></center>";
            }
        }

        [SessionExpireFilter]
        public ActionResult Template()
        {
            MailConfiguration objMailConfiguration = new MailConfiguration();
            return View(objMailConfiguration);
        }

        [SessionExpireFilter]
        [HttpPost]
        public ActionResult TemplateCreatePartial(int? templateId)
        {
            MAIL001 objMAIL001 = new MAIL001();
            objMAIL001.IsActive = false;
            if (templateId != null && templateId > 0)
            {
                objMAIL001 = db.MAIL001.FirstOrDefault(x=>x.TemplateId==templateId);
            }
            return PartialView("TemplateCreatePartial", objMAIL001);
        }
        [SessionExpireFilter]
        [HttpPost]
        public ActionResult DeleteTemplate(int? templateId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();           
         
            if (templateId > 0)
            {
                MAIL001 objMAIL001 = db.MAIL001.FirstOrDefault(x => x.TemplateId == templateId);
                db.MAIL001.Remove(objMAIL001);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.MailMessage.Delete;
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.MailMessage.Error;
            }
            return Json(objResponseMsg);
        }

        [SessionExpireFilter]
        [HttpPost]
        public ActionResult ActiveDeactive(int? templateId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();

            if (templateId > 0)
            {
                MAIL001 objMAIL001 = db.MAIL001.FirstOrDefault(x => x.TemplateId == templateId);

                if (objMAIL001.IsActive == true)
                {
                    objMAIL001.IsActive = false;
                    objResponseMsg.Value = "Template Deactivated suceessfully";
                }
                else
                {
                    objMAIL001.IsActive = true;
                    objResponseMsg.Value = "Template Activated suceessfully";
                }                    
                db.SaveChanges();
                objResponseMsg.Key = true;
               
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.MailMessage.Error;
            }
            return Json(objResponseMsg);
        }




        [SessionExpireFilter]
        [HttpPost]
        public ActionResult TemplateBodyEditPartial(string templateId)
        {
            MailConfiguration mailConfiguration = new MailConfiguration();
            int id = Convert.ToInt32(templateId);
            mailConfiguration.MAIL001 = db.MAIL001.Where(m => m.TemplateId == id).SingleOrDefault();
            if (mailConfiguration != null)
            {
                mailConfiguration.ContentBody = mailConfiguration.MAIL001.Body;
                mailConfiguration.TemplateName = mailConfiguration.MAIL001.TamplateName;
            }
            return PartialView("TemplateBodyEditPartial", mailConfiguration);
        }

        [SessionExpireFilter]
        [HttpPost]
        public ActionResult Save(MAIL001 objMailTamplateMaster)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (ModelState.IsValid)
                {
                    if (objMailTamplateMaster.TemplateId > 0)
                    {
                        MAIL001 mailTamplateMaster = db.MAIL001.Where(x => x.TemplateId == objMailTamplateMaster.TemplateId).FirstOrDefault();
                        mailTamplateMaster.TamplateName = objMailTamplateMaster.TamplateName;
                        mailTamplateMaster.CCEmail = objMailTamplateMaster.CCEmail;
                        mailTamplateMaster.BCCEmail = objMailTamplateMaster.BCCEmail;
                        mailTamplateMaster.Subject = objMailTamplateMaster.Subject;
                        mailTamplateMaster.Description = objMailTamplateMaster.Description;
                        mailTamplateMaster.EditedOn = DateTime.Now; ;
                        mailTamplateMaster.EditedBy = objClsLoginInfo.UserName;
                        mailTamplateMaster.IsActive = objMailTamplateMaster.IsActive;
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.MailMessage.Update;
                    }
                    else
                    {
                        MAIL001 mailTamplateMaster = new MAIL001();
                        mailTamplateMaster.TamplateName = objMailTamplateMaster.TamplateName;
                        mailTamplateMaster.CCEmail = objMailTamplateMaster.CCEmail;
                        mailTamplateMaster.BCCEmail = objMailTamplateMaster.BCCEmail;
                        mailTamplateMaster.Subject = objMailTamplateMaster.Subject;
                        mailTamplateMaster.Description = objMailTamplateMaster.Description;
                        mailTamplateMaster.Body = string.Empty;
                        mailTamplateMaster.CreatedOn = DateTime.Now; ;
                        mailTamplateMaster.CreatedBy = objClsLoginInfo.UserName;
                        mailTamplateMaster.IsActive = objMailTamplateMaster.IsActive;
                        db.MAIL001.Add(mailTamplateMaster);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.MailMessage.Save;
                    }
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.MailMessage.Error;
            }
            return Json(objResponseMsg);
        }

        [SessionExpireFilter]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveTemplate(MailConfiguration MailConfiguration)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            MAIL001 myobj = db.MAIL001.Where(m => m.TemplateId == MailConfiguration.MAIL001.TemplateId).SingleOrDefault();
            try
            {
                if (ModelState.IsValid)
                {
                    if (myobj != null)
                    {
                        myobj.Body = MailConfiguration.ContentBody;
                        db.SaveChanges();
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.MailMessage.Update;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.MailMessage.Error;
            }
            return Json(objResponseMsg);
        }
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "", int? HeaderId = 0, int? latestRv = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_GET_MAILTEMPLATES(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      TemplateName=li.TamplateName,
                                      CCEmail=li.CCEmail,
                                      BCCEmail=li.BCCEmail,
                                      Subject=li.Subject,
                                      Remarks=li.Description,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }



                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
    }
}