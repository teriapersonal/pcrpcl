﻿using IEMQS.Areas.PAM.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.PAM.Controllers
{
    public class MKTController : clsBase
    {

        PAMActionModel Action = new PAMActionModel();

        #region Utility
        public void AddProjectByDefault(SP_PAM_GETProjectDetails_Result projectdet, PAM001 objPAM001, List<PAM002> lstPAM002, List<PAM004> lstPAM004)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                bool Getproject = db.PAM002.Any(m => m.HeaderId == objPAM001.HeaderId && m.Project == projectdet.Project);

                if (Getproject != true)
                {
                    if (!string.IsNullOrWhiteSpace(projectdet.Project))
                    {
                        //var projectdet = db.SP_PAM_GETProjectDetails(project, objPAM001.ContractNo).FirstOrDefault();
                        PAM002 objPam002 = new PAM002()
                        {
                            Project = projectdet.Project,
                            CreatedBy = objClsLoginInfo.UserName,
                            CreatedOn = DateTime.Now,
                            HeaderId = objPAM001.HeaderId,
                            CDD = projectdet.CDD,
                            Equipment = projectdet.Equipment,
                            ZeroDate = projectdet.ZeroDate,
                            LOIDate = objPAM001.LOIDate,
                            PAM = objPAM001.PAM
                        };
                        lstPAM002.Add(objPam002);

                        PAM004 objPAM004 = new PAM004()
                        {
                            PAM = objPAM001.PAM,
                            ActionBy = objClsLoginInfo.UserName,
                            ActionOn = DateTime.Now,
                            Action = "ADD",
                            Comments = projectdet.Project + " Added"
                        };
                        lstPAM004.Add(objPAM004);
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
        }

        public PAM001 CreatePAM(string contractNo)
        {
            PAM001 objPAM001 = null;
            List<PAM002> lstPAM002 = new List<PAM002>();
            List<PAM004> lstPAM004 = new List<PAM004>();

            string maxNoResult = db.Database.SqlQuery<string>("SELECT DBO.FN_GENERATE_MAX_PAM_NO('" + contractNo + "')").FirstOrDefault();
            var maxPAM = maxNoResult.Split('@')[0];
            int maxDocNo = Convert.ToInt32(maxNoResult.Split('@')[1]);
            var contractDet = db.SP_PAM_GETPAMDETAILSCONTRACTWISE(1, 1, "", "ContractNo = '" + contractNo + "' ").FirstOrDefault();
            if (contractDet != null)
            {
                objPAM001 = new PAM001
                {
                    ContractNo = contractDet.ContractNo,
                    PAM = maxPAM,
                    Status = clsImplementationEnum.PAMStatus.Draft.GetStringValue(),
                    LOINo = contractDet.LOINo,
                    LOIDate = contractDet.LOIDate,
                    Customer = contractDet.Customer,
                    CustomerOrderNo = contractDet.CustOrdNo,
                    CustomerOrderDate = contractDet.CustOrdDate,
                    CDD = contractDet.CDD,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                    DocNo = maxDocNo,
                    Prospect = contractDet.Prospect
                };
                db.PAM001.Add(objPAM001);
                db.SaveChanges();

                var Projects = db.SP_PAM_GETPROJECTLIST(objPAM001.HeaderId).ToList();
                var lstProjects = db.SP_PAM_GETProjectDetails("", objPAM001.ContractNo).ToList();
                foreach (var project in Projects)
                {
                    var projectdtl = lstProjects.Where(i => i.Project == project.id).FirstOrDefault();
                    if (projectdtl != null)
                        AddProjectByDefault(projectdtl, objPAM001, lstPAM002, lstPAM004);
                }
                db.PAM002.AddRange(lstPAM002);
                db.PAM004.AddRange(lstPAM004);
                SaveDSS(objPAM001);
            }
            return objPAM001;
        }
        #endregion

        // GET: PAM/MKT
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetPAMGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetPALMGridDataPartial");
        }
        public ActionResult PamRequestPartial(string contractNo, string contractDesc, string customer, string customerDesc)
        {
            PAMRequestViewModel objPAMRequest = new PAMRequestViewModel();
            objPAMRequest.ContractNo = contractNo;
            objPAMRequest.ContractDesc = contractDesc;
            objPAMRequest.Customer = customer;
            objPAMRequest.CustomerDesc = customerDesc;

            return PartialView("_PalmRequestPartial", objPAMRequest);
        }
        public ActionResult loadPAMDataTable(JQueryDataTableParamModel param, string status)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                //whereCondition += " and loidate.t_csbu collate SQL_Latin1_General_CP850_CI_AI in (select * from dbo.fn_split(dbo.GETBUFROMUSER('" + objClsLoginInfo.UserName + "'),',')) ";
                whereCondition += " and t_csbu collate SQL_Latin1_General_CP850_CI_AI in (select * from dbo.fn_split(dbo.GETBUFROMUSER('" + objClsLoginInfo.UserName + "'),',')) ";
                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and status in ('" + clsImplementationEnum.PAMStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.PAMStatus.Returned.GetStringValue() + "','" + clsImplementationEnum.IMBStatus.SentForApproval.GetStringValue() + "')";
                }
                //string[] columnName = {  "pam.status", "cont.t_cono", "cont.t_cono +'-'+ cont.t_desc", "cont.t_ofbp", "cont.t_ofbp +'-' + cust.t_nama", "t_refe", "loidate.t_lfdt", "loidate.t_ccdd", "t_refb", "t_codt", "t_efdt" , "pam.PAM"};
                string[] columnName = { "PAM", "status", "ContractNo", "ConDesc ", "Customer", "CustomerDesc", "LOINo", "LOIDate", "CDD", "CustOrdNo", "CustOrdDate", "Zeordate", "DSS", "DSSStatus" };
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstPam = db.SP_PAM_GETPAMDETAILSCONTRACTWISE(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                var res = from h in lstPam
                          select new[] {
                           h.PAM,
                           Convert.ToString( h.ContractNo),
                           Convert.ToString( h.ConDesc),
                           Convert.ToString( h.Customer),
                           Convert.ToString( h.CustomerDesc),
                           Convert.ToString( h.LOINo),
                           h.LOIDate == null || h.LOIDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) == "01/01/1970"? "NA" : h.LOIDate.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture),
                           //Convert.ToString(Convert.ToDateTime(h.LOIDate).ToString("dd/MM/yyyy") == "01/01/1970" ? "":Convert.ToDateTime(h.LOIDate).ToString("dd/MM/yyyy")),
                           //Convert.ToDateTime(h.LOIDate).ToString("dd/MM/yyyy"),
                           Convert.ToString( h.CustOrdNo),
                           h.CustOrdDate == null || h.CustOrdDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) == "01/01/1970"? "NA" : h.CustOrdDate.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture),
                           //Convert.ToDateTime(h.CustOrdDate).ToString("dd/MM/yyyy"),
                            h.Zeordate == null || h.Zeordate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) == "01/01/1970"? "NA" : h.Zeordate.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture),
                           h.CDD == null || h.CDD.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) == "01/01/1970"? "NA" : h.CDD.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture),
                           Convert.ToString( h.Prospect == 1 ? "Yes":"No"),
                          
                           //Convert.ToDateTime(h.CDD).ToString("dd/MM/yyyy"),
                           Convert.ToString(h.DSS),
                           Convert.ToString(h.DSSStatus),
                           Convert.ToString(h.status),
                           //Convert.ToString( h.HeaderId ),
                           ActionButton( h.HeaderId, h.IsMax.ToString(), h.status, h.PAM, h.Prospect,h.ContractNo, h.ConDesc, h.Customer,h.CustomerDesc, h.PAM,h.CustOrdDate),
                           Convert.ToString( h.IsMax )
                          };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public string ActionButton(int? headerId, string isMax, string status, string PAM, int prospect, string contractNo, string contractDesc, string customer, string customerDesc, string pam, DateTime custOrdDate)
        {
            string buttons = string.Empty;
            string strcustOrdDate = custOrdDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) == "01/01/1970" ? "NA" : custOrdDate.ToString("dd/MM/yyyy");
            var functionCallDelete = "DeletePAM(" + headerId + ")";
            var functionCallAdd = "\"AddPam(" + prospect + ",'" + contractNo + "','" + contractDesc + "','" + customer + "','" + customerDesc + "','" + pam + "','" + strcustOrdDate + "')\"";
            var functionCall = "\"ViewPam(" + headerId + ")\"";
            var functionCall2 = "\"PrintReport(" + headerId + ")\"";
            var functionCallHistory = "\"HistoryPAM('" + PAM + "')\"";
            if ((headerId > 0) && isMax == "True")
            {
                if (status == "Draft")
                {
                    buttons = "<nobr><center>" +
                   "<a href=\"javascript:void(0);\" Title=\"Add PAM\" ><i class=\"disabledicon fa fa-plus\"></i></a>" +
                   "<a href=\"javascript:void(0);\" Title=\"View PAM\" onclick=" + functionCall + "><i class=\"iconspace fa fa-eye\"></i></a>" +
                   "<i href=\"javascript:void(0)\" Title=\"View Timeline\" onclick=ShowTimeline(\"/PAM/MKT/ShowTimeline?HeaderID=" + headerId + "\"); class=\"iconspace fa fa-clock-o\"></i>" +
                   "<i href=\"javascript:void(0);\" Title=\"Print Report\" onclick=" + functionCall2 + " class=\"iconspace fa fa-print\"></i>" +
                   "<i id=\"btnDelete\" name=\"btnAction\" Title=\"Delete PAM\" class=\"iconspace fa fa-trash-o\" onClick=" + functionCallDelete + "></i>" +
                   "<i id=\"btnHistory\" name=\"btnAction\"  Title=\"History PAM\" class=\"iconspace fa fa-history\" onClick=" + functionCallHistory + "></i>" +
                    "</center></nobr>";

                }
                else
                {
                    buttons = "<nobr><center>" +
                    "<a href=\"javascript:void(0);\" Title=\"Add PAM\" onclick=" + functionCallAdd + "><i class=\"iconspace fa fa-plus\"></i></a>" +
                    "<a href=\"javascript:void(0);\" Title=\"View PAM\" onclick=" + functionCall + "><i  class=\"iconspace fa fa-eye\"></i></a>" +
                    "<i href=\"javascript:void(0)\" Title=\"View Timeline\" onclick=ShowTimeline(\"/PAM/MKT/ShowTimeline?HeaderID=" + headerId + "\"); class=\"iconspace fa fa-clock-o\"></i>" +
                    "<i href=\"javascript:void(0);\" Title=\"Print Report\" onclick=" + functionCall2 + " class=\"iconspace fa fa-print\"></i>" +
                    "<i id=\"btnDelete\" name=\"btnAction\" Title=\"Delete PAM\" class=\"disabledicon fa fa-trash-o\"></i>" +
                    "<i id=\"btnHistory\" name=\"btnAction\"  Title=\"History PAM\" class=\"iconspace fa fa-history\" onClick=" + functionCallHistory + "></i>" +
                    "</center></nobr>";
                }

            }
            else if ((headerId > 0) && isMax == "False")
            {
                if (status == "Draft")
                {
                    buttons = "<nobr><center>" +
                        "<a href=\"javascript:void(0);\" Title=\"Add PAM\" ><i class=\"disabledicon fa fa-plus\"></i></a>" +
                        "<a href=\"javascript:void(0);\" Title=\"View PAM\" onclick=" + functionCall + "><i class=\"iconspace fa fa-eye\"></i></a>" +
                        "<i href=\"javascript:void(0)\" Title=\"View Timeline\" onclick=ShowTimeline(\"/PAM/MKT/ShowTimeline?HeaderID=" + headerId + "\"); class=\"iconspace fa fa-clock-o\"></i>" +
                        "<i href=\"javascript:void(0);\" Title=\"Print Report\" onclick=" + functionCall2 + " class=\"iconspace fa fa-print\"></i>" +
                        "<i id=\"btnDelete\" name=\"btnAction\" style=\"cursor:pointer;style=\"margin-right:10px\" Title=\"Delete PAM\" class=\"fa fa-trash-o\" onClick=" + functionCallDelete + "></i>" +
                        "<i id=\"btnHistory\" name=\"btnAction\"  Title=\"History PAM\" class=\"iconspace fa fa-history\" onClick=" + functionCallHistory + "></i>" +
                    "</center></nobr>";
                }
                else
                {
                    buttons = "<nobr><center>" +
                    "<a href=\"javascript:void(0);\" Title=\"Add PAM\" ><i class=\"disabledicon fa fa-plus\"></i></a>" +
                    "<a href=\"javascript:void(0);\" Title=\"View PAM\" onclick=" + functionCall + "><i class=\"iconspace fa fa-eye\"></i></a>" +
                    "<i href=\"javascript:void(0)\" Title=\"View Timeline\" onclick=ShowTimeline(\"/PAM/MKT/ShowTimeline?HeaderID=" + headerId + "\"); class=\"iconspace fa fa-clock-o\"></i>" +
                    "<i href=\"javascript:void(0);\" Title=\"Print Report\" onclick=" + functionCall2 + " class=\"iconspace fa fa-print\"></i>" +
                    "<i id=\"btnDelete\" name=\"btnAction\" Title=\"Delete PAM\" class=\"disabledicon fa fa-trash-o\"></i>" +
                    "<i id=\"btnHistory\" name=\"btnAction\"  Title=\"History PAM\" class=\"iconspace fa fa-history\" onClick=" + functionCallHistory + "></i>" +
                    "</center></nobr>";
                }
            }
            else
            {
                if (status == "Draft")
                {
                    buttons = "<nobr><center>" +
                      "<a href=\"javascript:void(0);\" Title=\"Add PAM\" ><i class=\"disabledicon fa fa-plus\"></i></a>" +
                     "<a href=\"javascript:void(0);\" Title=\"View PAM\" ><i  class=\"disabledicon fa fa-eye\"></i></a>" +
                     "<i href=\"javascript:void(0)\" Title=\"View Timeline\"  class=\"disabledicon fa fa-clock-o\"></i>" +
                      "<i href=\"javascript:void(0);\" Title=\"Print Report\" onclick=" + functionCall2 + " class=\"iconspace fa fa-print\"></i>" +
                      "<i id=\"btnDelete\" name=\"btnAction\"  Title=\"Delete PAM\" class=\"iconspace fa fa-trash-o\" onClick=" + functionCallDelete + "></i>" +
                      "<i id=\"btnHistory\" name=\"btnAction\"  Title=\"History PAM\" class=\"iconspace fa fa-history\" onClick=" + functionCallHistory + "></i>" +
                    "</center></nobr>";
                }
                else
                {
                    buttons = "<nobr><center>" +
                     "<a href=\"javascript:void(0);\" Title=\"Add PAM\" onclick=" + functionCallAdd + "><i class=\"iconspace fa fa-plus\"></i></a>" +
                     "<a href=\"javascript:void(0);\" Title=\"View PAM\" ><i class=\"disabledicon fa fa-eye\"></i></a>" +
                     "<a href=\"javascript:void(0)\" Title=\"View Timeline\"><i  class=\"disabledicon fa fa-clock-o\"></i></a>" +
                     "<a href=\"javascript:void(0);\" Title=\"Print Report\" onclick=" + functionCall2 + "><i class=\"iconspace fa fa-print\"></i></a>" +
                     "<i id=\"btnDelete\" name=\"btnAction\" Title=\"Delete PAM\" class=\"disabledicon fa fa-trash-o\"></i>" +
                     "<i id=\"btnHistory\" name=\"btnAction\"  Title=\"History PAM\" class=\"iconspace fa fa-history\" onClick=" + functionCallHistory + "></i>" +
                     "</center></nobr>";
                }

            }
            return buttons;
        }
        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.TimelineTitle = "PAM History";
            model.Title = "PAM";
            PAM001 objPAM001 = db.PAM001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            model.CreatedBy = Manager.GetUserNameFromPsNo(objPAM001.CreatedBy);
            model.CreatedOn = objPAM001.CreatedOn;
            model.ApprovedBy = Manager.GetUserNameFromPsNo(objPAM001.PMGName);
            model.ApprovedOn = objPAM001.PMGResponseDate;


            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        [HttpPost]
        public async Task<ActionResult> GetRequestStatus(string contractNo, int prospect, string custOrdDate, string pam)
        {
            try
            {
                if (prospect == 1 && string.IsNullOrWhiteSpace(custOrdDate))
                {
                    PAM003 objPAM003 = await db.PAM003.FirstOrDefaultAsync(x => x.ContractNo == contractNo && x.Status != "Approved" && x.Status != "Denied");
                    if (objPAM003 != null)
                    {
                        var res = new
                        {
                            Key = true,
                            BUHead = Manager.GetPsidandDescription(objPAM003.BUHead),
                            RequestedBy = Manager.GetPsidandDescription(objPAM003.RequestedBy),
                            RequestedOn = Convert.ToDateTime(objPAM003.RequestedOn).ToString("dd/MM/yyyy HH:MM"),
                            BUHeadComment = objPAM003.BUHeadComments,
                            Status = objPAM003.Status,
                            Value = ""
                        };
                        return Json(res, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var res = new
                        {
                            Key = true,
                            Status = "NotRequested"
                        };
                        return Json(res, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    clsHelper.ResponseMsgWithStatus objResponseMsg = SavePAM(contractNo);
                    var res = new
                    {
                        Key = objResponseMsg.Key,
                        Status = "PAMCreated",
                        Value = objResponseMsg.Value,
                        Id = objResponseMsg.HeaderId
                    };
                    return Json(res, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetBUHead(string term)
        {
            try
            {
                var items = Manager.GetApprover(clsImplementationEnum.UserRoleName.MKT1.GetStringValue(), objClsLoginInfo.Location, true, objClsLoginInfo.UserName, term);
                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SaveRequest(FormCollection fc)
        {
            PAM003 objPAM003 = new PAM003();
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (fc != null)
                {
                    objPAM003 = new PAM003
                    {
                        ContractNo = fc["ContractNo"].ToString(),
                        RequestedBy = objClsLoginInfo.UserName,
                        RequestedOn = DateTime.Now,
                        Status = clsImplementationEnum.PAMRequestStatus.Requested.GetStringValue(),
                        BUHead = fc["BUHead"].ToString(),
                        POExpectedOn = DateTime.ParseExact(fc["ExpectedDateOfPO"].ToString(), @"d/M/yyyy", CultureInfo.InvariantCulture),
                        Comments = fc["Comments"].ToString()

                    };
                    db.PAM003.Add(objPAM003);
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Request Send Sucessfully";
                    //#region Send Mail
                    Hashtable _ht = new Hashtable();
                    EmailSend _objEmail = new EmailSend();
                    _ht["[BUHead]"] = Manager.GetUserNameFromPsNo(fc["BUHead"].ToString());
                    _ht["[ContractNo]"] = fc["ContractNo"].ToString();
                    _ht["[POExpectedOn]"] = (fc["ExpectedDateOfPO"]);
                    MAIL001 objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.PAM.BUHeadApproval).SingleOrDefault();
                    //_objEmail.MailToAdd = Manager.GetMailIdFromPsNo(objIMB001.ApprovedBy);
                    // _objEmail.MailToAdd = Manager.GetMailIdFromPsNo(fc["BUHead"].ToString());
                    _objEmail.MailToAdd = "abhishek.acharekar@larsentoubro.com";
                    _ht["[Subject]"] = "Approval to initiate PAM for Contract" + "" + fc["ContractNo"].ToString() + "" + "without LOI";
                    _ht["[RequestedBy]"] = Manager.GetUserNameFromPsNo(objClsLoginInfo.UserName);
                    string host = Request.Url.Host;
                    _ht["[BUHeadApprovalLink]"] = WebsiteURL + "/PAM/MKT/PAMRequestApproveView";
                    _objEmail.MailBody = objTemplateMaster.Body;
                    _objEmail.MailCc = Manager.GetMailIdFromPsNo(objClsLoginInfo.UserName.ToString());
                    //_objEmail.mails = objTemplateMaster.Body;
                    //_objEmail.MailBody = objTemplateMaster.Body;
                    _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                    //#endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponseMsgWithStatus SavePAM(string contractNo)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PAM001 objPAM001 = CreatePAM(contractNo);
                if (objPAM001 != null)
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "PAM Saved Succesfully";
                    Action.Add(objPAM001.PAM, "PAM Created With LOI No " + objPAM001.LOINo);
                    objResponseMsg.HeaderId = objPAM001.HeaderId;

                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "PAM Already Created";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return objResponseMsg;
        }
        public ActionResult loadPAMRequestDataTable(JQueryDataTableParamModel param, string status)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "pam3.BUHead = '" + objClsLoginInfo.UserName + "'";
                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and pam3.Status in ('" + clsImplementationEnum.PAMRequestStatus.Requested.GetStringValue() + "')";
                }

                string[] columnName = { "pam3.ContractNo", "cont.t_cono +'-'+ cont.t_desc", "cont.t_ofbp", "cont.t_ofbp +'-' + cust.t_nama", "pam3.RequestedBy", "pam3.Status" };

                //whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                var lstPam = db.SP_PAM_GENERATE_REQUESTS(StartIndex, EndIndex, "", whereCondition).ToList();
                int? totalRecords = lstPam.Select(x => x.TotalCount).FirstOrDefault();
                var res = from h in lstPam
                          select new[] {
                           Convert.ToString(h.ConDesc),
                           Convert.ToString(h.ContractNo),
                           Convert.ToString(h.Customer),
                           Convert.ToString(h.CustomerDesc),
                           Convert.ToDateTime(h.POExpectedOn).ToString("dd/MM/yyyy"),
                           Convert.ToString(h.RequestedBy),
                           Convert.ToString(h.Status),
                             Convert.ToString(h.Comments),
                           Convert.ToString(h.Id),
                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = "",
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]
        public ActionResult PAMRequestApproveView()
        {
            return View();
        }
        public ActionResult GetPAMRequestDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetPAMRequestDataPartial");
        }
        [HttpPost]
        public ActionResult RespondPamRequest(string type, int requestId, string buHeadComments)
        {
            PAM003 objPAM003 = db.PAM003.FirstOrDefault(x => x.Id == requestId);
            if (objPAM003 != null)
            {
                objPAM003.BUHeadComments = buHeadComments;

            }

            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            objResponseMsg.Key = true;
            try
            {
                if (type == "D")
                {
                    objPAM003.Status = clsImplementationEnum.PAMRequestStatus.Denied.GetStringValue();
                    Hashtable _ht = new Hashtable();
                    EmailSend _objEmail = new EmailSend();
                    _ht["[Initiator]"] = Manager.GetUserNameFromPsNo(objPAM003.RequestedBy);
                    _ht["[ContractNo]"] = objPAM003.ContractNo;
                    _ht["[Approver]"] = Manager.GetUserNameFromPsNo(objPAM003.BUHead);
                    _ht["[BUHeadComments]"] = objPAM003.BUHeadComments;
                    MAIL001 objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.PAM.Rejection).SingleOrDefault();
                    //_objEmail.MailToAdd = Manager.GetMailIdFromPsNo(objIMB001.ApprovedBy);
                    _objEmail.MailToAdd = Manager.GetMailIdFromPsNo(objPAM003.RequestedBy);

                    _ht["[Subject]"] = "Rejected PAM without LOI for contract" + "" + objPAM003.ContractNo.ToString() + "";
                    _ht["[BUHead]"] = Manager.GetUserNameFromPsNo(objPAM003.BUHead);
                    _objEmail.MailBody = objTemplateMaster.Body;
                    _objEmail.MailCc = Manager.GetMailIdFromPsNo(objPAM003.BUHead.ToString());
                    //_objEmail.mails = objTemplateMaster.Body;
                    //_objEmail.MailBody = objTemplateMaster.Body;
                    _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                }
                else
                {
                    PAM001 objPAM001 = CreatePAM(objPAM003.ContractNo);
                    if (objPAM001 != null)
                    {
                        Action.Add(objPAM001.PAM, "PAM Created on Approval By BUHead=" + objPAM001.BUHead + "");
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = objPAM001.PAM + " Created Succesfully";
                        objPAM003.Status = clsImplementationEnum.PAMRequestStatus.Approved.GetStringValue();

                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = objPAM001.PAM + " Already Created";
                    }
                }
                // objPAM003.BUHeadComments = buHeadComments;
                //db.SaveChanges();
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public ActionResult AddIndividualPam(int id)
        {
            PAM001 objPAM001 = db.PAM001.FirstOrDefault(x => x.HeaderId == id);

            ViewBag.isInitiator = "false";
            if (objPAM001.CreatedBy == objClsLoginInfo.UserName)
            {
                ViewBag.isInitiator = "true";
            }
            string emp = Session["userid"].ToString();
            //var role = db.ATH001.Where(m => m.Employee == emp).FirstOrDefault();
            //if (role.Role == 40)
            //{
            //    ViewBag.role = "MKT3";
            //}
            if (objClsLoginInfo.GetUserRoleList().Contains(clsImplementationEnum.UserRoleName.MKT3.GetStringValue()))
            {
                ViewBag.role = clsImplementationEnum.UserRoleName.MKT3.GetStringValue();
            }

            string ManagerName = db.COM003.Where(x => x.t_psno == objPAM001.PMGName && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
            if (objPAM001.PMGName != null && objPAM001.PMGName != "")
            {
                //objPAM001.PMGName = objPAM001.PMGName + " - " + ManagerName;
                ViewBag.pmgname = objPAM001.PMGName + " - " + ManagerName;
            }
            else
            {
                objPAM001.PMGName = null;
                ViewBag.pmgname = "";
                //List<ApproverModel> lstApprovers = Manager.GetManagerList(clsImplementationEnum.UserRoleName.PMG2.GetStringValue(), objClsLoginInfo.Location, objClsLoginInfo.UserName, "").ToList();//Manager.GetManagerListWithContractno(clsImplementationEnum.UserRoleName.PMG2.GetStringValue(), objClsLoginInfo.Location, objClsLoginInfo.UserName, term, ContractNo).ToList();
                // Observation No 15358 on 25-06-2018
                var psno = db.Database.SqlQuery<string>(@"SELECT t_crep  FROM " + LNLinkedServer + ".dbo.ttpctm100175 where t_cono = '" + objPAM001.ContractNo + "'").FirstOrDefault();
                if (!string.IsNullOrEmpty(psno))
                {
                    //var projectmanager = lstApprovers.Where(x => x.Code == psno).FirstOrDefault();
                    ViewBag.pmgname = Manager.GetPsidandDescription(psno);
                    objPAM001.PMGName = psno;
                }
            }
            PAM006 objPAM006 = db.PAM006.Where(i => i.HeaderId == id).FirstOrDefault();
            if (objPAM006 == null)
            {
                ViewBag.DisplayDSS = false;
                ViewBag.DSSId = 0;
            }
            else
            {
                ViewBag.DisplayDSS = true;
                ViewBag.DSSId = objPAM006.DSSId;
            }
            string createdby = db.COM003.Where(x => x.t_psno == objPAM001.CreatedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
            if (objPAM001.CreatedBy != null)
            {
                objPAM001.CreatedBy = objPAM001.CreatedBy + " - " + createdby;
            }

            ViewBag.ContractNo = db.Database.SqlQuery<string>(@"SELECT t_cono +'-'+ t_desc FROM  " + LNLinkedServer + ".dbo.ttpctm100175 where t_cono = '" + objPAM001.ContractNo + "'").FirstOrDefault();
            ViewBag.Customer = db.Database.SqlQuery<string>(@"SELECT  t_bpid +'-'+ t_nama   from " + LNLinkedServer + ".dbo.ttccom100175  where t_bpid = '" + objPAM001.Customer + "'").FirstOrDefault();

            //var Prospect = clsImplementationEnum.HandoverReportType.Prospect.GetStringValue();
            //var Firm = clsImplementationEnum.HandoverReportType.Firm.GetStringValue();
            //var objPAM008Prospect = db.PAM008.Where(i => i.HeaderId == objPAM001.HeaderId && i.HandoverType.ToLower() == Prospect.ToLower()).FirstOrDefault();
            //var objPAM008Firm = db.PAM008.Where(i => i.HeaderId == objPAM001.HeaderId && i.HandoverType.ToLower() == Firm.ToLower()).FirstOrDefault();
            //if ((objPAM001.LOINo == null || objPAM001.LOINo == string.Empty) || objPAM008Prospect != null)
            //{
            //    ViewBag.DisplayProspectHandOver = true;
            //}
            //else
            //{
            //    ViewBag.DisplayProspectHandOver = false;
            //}

            //if ((objPAM001.LOINo != null && objPAM001.LOINo != string.Empty))
            //{
            //    ViewBag.DisplayFirmHandOver = true;
            //}
            //else
            //{
            //    ViewBag.DisplayFirmHandOver = false;
            //}

            //Implemented By Ajay Chauhan On 05-06-2018 Observation 15170
            return View(objPAM001);
        }

        public ActionResult loadProjectDataTable(JQueryDataTableParamModel param, int headerId)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "HeaderId = " + headerId + "";
                string[] columnName = { "PAM", "Project", "ProjectDescription", "Equipment" };
                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstPamProject = db.SP_PAM_PROJECT_CONTRACTWISE(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstPamProject.Select(x => x.TotalCount).FirstOrDefault();
                var res = from h in lstPamProject
                          select new[] {
                           Convert.ToString(h.ROW_NO),
                           Convert.ToString(h.Id),
                           Convert.ToString(h.HeaderId),
                           Convert.ToString(h.Project),
                           Convert.ToString(h.Equipment),
                            h.LOIDate == null || h.LOIDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) == "01/01/1970" ? "NA" : h.LOIDate.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture),
                            //Convert.ToString(Convert.ToDateTime(h.LOIDate).ToString("dd/MM/yyyy") == "01/01/1970" ? "":Convert.ToDateTime(h.LOIDate).ToString("dd/MM/yyyy")),
                            h.Zerodate == null || h.Zerodate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) == "01/01/1970"? "NA" : h.Zerodate.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture),
                            //Convert.ToDateTime(h.Zerodate).ToString("dd/MM/yyyy"),
                            h.CDD == null || h.CDD.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) == "01/01/1970"? "NA" : h.CDD.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture),

                           Convert.ToString(h.Id),
                           Convert.ToString(h.PrefixItemIdGen),
                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public async Task<ActionResult> DeleteProject(int lineId, int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            PAM001 objPAM001 = await db.PAM001.FirstOrDefaultAsync(x => x.HeaderId == headerId);
            try
            {
                if (objPAM001.Status != clsImplementationEnum.IMBStatus.SentForApproval.GetStringValue())
                {
                    PAM002 objPAM002 = await db.PAM002.Where(x => x.HeaderId == headerId && x.Id == lineId).FirstOrDefaultAsync();
                    if (objPAM002 != null)
                    {
                        db.PAM002.Remove(objPAM002);
                        await db.SaveChangesAsync();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Project deleted successfully";
                    }
                    if (objResponseMsg.Key == true && objPAM001.Status == clsImplementationEnum.IMBStatus.Approved.GetStringValue())
                    {
                        //objPAM001.re = objPAM001.RevNo + 1;
                    }
                    //if (objPAM001.ApprovedBy != objClsLoginInfo.UserName)
                    //{
                    //    objPAM001.Status = clsImplementationEnum.IMBStatus.Draft.GetStringValue();
                    //}
                    db.SaveChanges();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Document is sent for approval, You can't delete now";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            var response = new
            {
                Key = objResponseMsg.Key,
                Value = objResponseMsg.Value,
                Status = objPAM001.Status
            };
            return Json(response);
        }
        [HttpPost]
        public async Task<ActionResult> DeletePam(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();

            try
            {
                PAM001 objPAM001 = await db.PAM001.FirstOrDefaultAsync(x => x.HeaderId == headerId);
                if (objPAM001 != null)
                {
                    db.PAM001.Remove(objPAM001);
                    await db.SaveChangesAsync();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "PAM deleted successfully";
                }
                db.SaveChanges();

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            var response = new
            {
                Key = objResponseMsg.Key,
                Value = objResponseMsg.Value,

            };
            return Json(response);
        }
        [HttpPost]
        public async Task<ActionResult> AddProject(int headerId, string contract, string customer, string project, DateTime? loiDate)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            PAM001 objPAM001 = await db.PAM001.FirstOrDefaultAsync(x => x.HeaderId == headerId);

            try
            {
                if (!string.IsNullOrEmpty(project))
                {
                    string[] Projects = project.Split(',').ToArray();
                    foreach (var item in Projects)
                    {
                        bool Getproject = db.PAM002.Any(m => m.HeaderId == headerId && m.Project == item);

                        if (Getproject != true)
                        {
                            //if (objPAM001.Status != clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue())
                            //{


                            var projectdet = db.SP_PAM_GETProjectDetails(item, contract).FirstOrDefault();
                            PAM002 objPam002 = new PAM002
                            {
                                Project = item,
                                CreatedBy = objClsLoginInfo.UserName,
                                CreatedOn = DateTime.Now,
                                HeaderId = headerId,
                                CDD = projectdet.CDD,
                                Equipment = projectdet.Equipment,
                                ZeroDate = projectdet.ZeroDate,
                                LOIDate = loiDate,
                                PAM = objPAM001.PAM
                            };
                            db.PAM002.Add(objPam002);

                            PAM004 objPAM004 = new PAM004()
                            {
                                PAM = objPAM001.PAM,
                                ActionBy = objClsLoginInfo.UserName,
                                ActionOn = DateTime.Now,
                                Action = "ADD",
                                Comments = item + " Added"
                            };
                            db.PAM004.Add(objPAM004);

                            await db.SaveChangesAsync();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "Project Saved Successfully";
                            if (objResponseMsg.Key == true && objPAM001.Status == clsImplementationEnum.IMBStatus.Approved.GetStringValue())
                            {
                                //objPAM001.re = objPAM001.RevNo + 1;
                            }
                            //if (objPAM001.ApprovedBy != objClsLoginInfo.UserName)
                            //{
                            //    objPAM001.Status = clsImplementationEnum.IMBStatus.Draft.GetStringValue();
                            //}
                            db.SaveChanges();
                            //}
                        }
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please Select Project(s)";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            var response = new
            {
                Key = objResponseMsg.Key,
                Value = objResponseMsg.Value,
                Status = objPAM001.Status
            };
            return Json(response);
        }
        //[SessionExpireFilter]
        //public ActionResult PojectSelectionPartial(int pamId)
        //{
        //    PAM001 objPAM001 = db.PAM001.FirstOrDefault(x => x.HeaderId == pamId);
        //    return PartialView(objPAM001);
        //}

        public JsonResult GetProjects(int? headerId)
        {
            try
            {
                var lstProjectPAM002 = db.PAM002.Where(x => x.HeaderId == headerId).Select(x => x.Project).ToList();

                var items = db.SP_PAM_GETPROJECTLIST(headerId).ToList();

                items.RemoveAll(x => lstProjectPAM002.Contains(x.id));
                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult loadCheckListDataTable(JQueryDataTableParamModel param, int? headerId, string pam)
        {
            try
            {

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                var lstPam = db.SP_PAM_SECTION_CHECKLIST(headerId, pam, objClsLoginInfo.UserName).ToList();

                var res = from h in lstPam
                          select new[] {
                           Convert.ToString( h.SequenceId),
                           Convert.ToString(h.HeaderId),
                           Convert.ToString(h.SectionId),
                           Convert.ToString(h.ChecklistId),
                           Convert.ToString( h.Checklist),
                           GetChecklistCheckStyle(h.Status,h.IsChecked),
                           GetSectionRemarkStyle(h.Status,h.Remarks),
                           GetAttachment( h.Id,h.Attachment),
                           h.Section,
                           Convert.ToString(h.IsAttachmentMandatory),
                           Convert.ToString(h.Id),
            };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = lstPam.Count,
                    iTotalRecords = lstPam.Count,
                    aaData = res,
                    whereCondition = headerId,
                    strSortOrder = pam,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult loadPaymentTermDataTable(JQueryDataTableParamModel param)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                int HeaderId = Convert.ToInt32(param.Headerid);
                string ContractNo = db.PAM001.Where(x => x.HeaderId == HeaderId).FirstOrDefault().ContractNo;
                string whereCondition = "a.t_cono = '" + ContractNo + "'";
                string[] columnName = { "a.t_dsca", "a.t_pndt", "a.t_perc", "b.t_desc" };
                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                //var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                //string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                //string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                //if (!string.IsNullOrWhiteSpace(sortColumnName))
                //{
                //    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                //}

                var lstPamProject = db.SP_PAM_PAYMENT_TERMS(StartIndex, EndIndex, ContractNo, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstPamProject.Count();
                var res = from h in lstPamProject
                          select new[] {
                           Convert.ToString(h.ROW_NO),
                           Convert.ToString(h.Description),
                           //h.PlannedInvoiceDate == null || h.PlannedInvoiceDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) == "01/01/1970"? "NA" : h.PlannedInvoiceDate.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture),
                           Convert.ToString(h.Percentage),
                           Convert.ToString(h.MilestoneType)
                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string GetAttachment(int id, string attachment)
        {
            //var folderPath = "PAM005/" + id;
            //var existing = (new clsFileUpload()).GetDocuments(folderPath);

            //if (!existing.Any())
            //{
            //    attachment = "False";
            //}
            //else
            //{
            //    attachment = "True";
            //}
            FileUploadController _obj = new FileUploadController();
            attachment = _obj.CheckAnyDocumentsExits("PAM005", id).ToString();
            return attachment;
        }
        public string GetSectionRemarkStyle(string status, string sectionRemarks)
        {

            if (status == clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue())
            {
                //return "<input type='text' name='txtSectionRemark' style='width:100%'  class='form-control input-sm  input-inline sectionTextbox' value='" + sectionRemarks + "' />";
                return "<input type='text' name='txtSectionRemark' style='width:100%' readonly= 'readonly'  class='form-control input-sm  input-inline sectionTextbox' value='" + sectionRemarks + "' />";
            }
            else if (status == clsImplementationEnum.PAMStatus.Approved.GetStringValue())
            {
                //return "<input type='text' name='txtSectionRemark' style='width:100%'  class='form-control input-sm  input-inline sectionTextbox' value='" + sectionRemarks + "' />";
                return "<input type='text' name='txtSectionRemark' style='width:100%' readonly= 'readonly'  class='form-control input-sm  input-inline sectionTextbox' value='" + sectionRemarks + "' />";
            }
            else
            {
                // return "<input type='text' name='txtSectionRemark' style='width:100%' readonly= 'readonly'  class='form-control input-sm  input-inline sectionTextbox' value='" + sectionRemarks + "' />";
                return "<input type='text' name='txtSectionRemark' style='width:100%'  class='form-control input-sm  input-inline sectionTextbox' value='" + sectionRemarks + "' />";
            }
        }

        public string GetChecklistCheckStyle(string status, bool? isCheck)
        {
            if (isCheck == null)
                isCheck = false;
            if (status == clsImplementationEnum.PAMStatus.Approved.GetStringValue())
            {
                return "<input type='checkbox' name='chkChecklistCheck' id='chkChecklistCheck' disabled= 'disabled'  class='make-switch col-md-3' checked='checked'    data-on-text='Yes' data-off-text='No' data-on-color='success' data-off-color='danger' data-size='normal' />";
            }

            else if (status != clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue() && isCheck == true)
            {
                return "<input type='checkbox' name='chkChecklistCheck' id='chkChecklistCheck'  class='make-switch col-md-3' checked='checked'  data-on-text='Yes' data-off-text='No' data-on-color='success' data-off-color='danger' data-size='normal' />";
            }
            else if (status != clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue() && isCheck != true)
            {
                return "<input type='checkbox' name='chkChecklistCheck' id='chkChecklistCheck'   class='make-switch col-md-3'  data-on-text='Yes' data-off-text='No' data-on-color='success' data-off-color='danger' data-size='normal' />";
            }
            else if (isCheck == true)
            {
                return "<input type='checkbox' name='chkChecklistCheck' id='chkChecklistCheck' disabled= 'disabled'  class='make-switch col-md-3' checked='checked'    data-on-text='Yes' data-off-text='No' data-on-color='success' data-off-color='danger' data-size='normal' />";
            }
            else
            {
                return "<input type='checkbox' name='chkChecklistCheck' id='chkChecklistCheck'  class='make-switch col-md-3' disabled= 'disabled'    data-on-text='Yes' data-off-text='No' data-on-color='success' data-off-color='danger' data-size='normal' />";
            }
        }

        public async Task<ActionResult> UpdateSectionRemarks(int id, string changeText)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objPAM005 = await db.PAM005.FirstOrDefaultAsync(x => x.Id == id);
                objPAM005.Remarks = !string.IsNullOrWhiteSpace(changeText) ? changeText : null;
                await db.SaveChangesAsync();
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Remarks Updated Successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public async Task<ActionResult> UpdateChecklistCheck(int id, bool state)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objPAM005 = await db.PAM005.FirstOrDefaultAsync(x => x.Id == id);
                objPAM005.IsChecked = state;
                await db.SaveChangesAsync();
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Check Status Updated Successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ChecklistAttachmentPartial(int? id)
        {
            PAM005 objPAM005 = db.PAM005.Where(x => x.Id == id).FirstOrDefault();
            ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments1("PAM005/" + id));
            return PartialView("_ChecklistAttachmentPartial", objPAM005);
        }

        [HttpPost]
        public ActionResult SavechecklistAttachment(string id/*, bool hasAttachments, Dictionary<string, string> Attach*/)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var folderPath = "PAM005/" + id;
                int intID = Convert.ToInt32(id);
                PAM005 objPAM005 = db.PAM005.Where(x => x.Id == intID).FirstOrDefault();
                objPAM005.FilePresent = true;
                objPAM005.EditedBy = objClsLoginInfo.UserName;
                objPAM005.EditedOn = DateTime.Now;
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = "File Uploaded Successfully";
                //if (hasAttachments == true)
                //{
                //    int intID = Convert.ToInt32(id);
                //    Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                //    PAM005 objPAM005 = db.PAM005.Where(x => x.Id == intID).FirstOrDefault();
                //    objPAM005.FilePresent = true;
                //    objPAM005.EditedBy = objClsLoginInfo.UserName;
                //    objPAM005.EditedOn = DateTime.Now;
                //    db.SaveChanges();
                //    objResponseMsg.Key = true;
                //    objResponseMsg.Value = "File Uploaded Successfully";
                //}
                //else
                //{
                //    Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                //    objResponseMsg.Key = true;
                //    objResponseMsg.Value = "Document Required";
                //}
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveIndividualPAM(FormCollection fc)
        {
            PAM001 objPAM001 = new PAM001();
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (fc != null)
                {
                    int headerId = Convert.ToInt32(fc["HeaderId"]);
                    //clsHelper.ResponseMsg isValid = IsValidatePAM(headerId);
                    //if (!isValid.Key)
                    //{
                    //    return Json(isValid, JsonRequestBehavior.AllowGet);
                    //}
                    string LoginUser = objClsLoginInfo.UserName;
                    if (headerId > 0)
                    {
                        objPAM001 = db.PAM001.Where(i => i.HeaderId == headerId).FirstOrDefault();
                        objPAM001.InitRemarks = fc["InitRemarks"];
                        objPAM001.PMGName = fc["txtpmgname"].Split('-')[0].ToString().Trim(); ;
                        if (objPAM001.Status != clsImplementationEnum.IMBStatus.SentForApproval.GetStringValue())
                        {
                            objPAM001.EditedBy = LoginUser;
                            objPAM001.EditedOn = DateTime.Now;
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = objPAM001.HeaderId.ToString();
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "This Document is sent for approval, You can't edit now";
                        }
                        SaveDSS(objPAM001);
                    }
                    else
                    {
                        db.PAM001.Add(objPAM001);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = objPAM001.HeaderId.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponseMsg IsValidatePAM(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            var lstPAM002 = db.PAM002.Where(x => x.HeaderId == headerId).ToList();
            if (!lstPAM002.Any())
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please add atleast one project";
                return objResponseMsg;
            }
            var lstPAM005 = db.SP_PAM_SECTION_CHECKLIST(headerId, "", "").ToList();
            foreach (var item in lstPAM005)
            {


                //if (item.IsReqired == 1)
                //{
                //    objResponseMsg.Key = false;
                //    objResponseMsg.Value = "Remarks is Required in" + " " + item.Checklist;
                //    return objResponseMsg;
                //}

                //if (item.IsRemarksMandatory == true)
                //{
                //    var remarksexist = db.PAM005.Where(m => m.HeaderId == item.HeaderId && m.ChecklistId == item.ChecklistId && m.PAM == item.PAM).FirstOrDefault();
                //    if (remarksexist != null)
                //    {
                //        if (remarksexist.Remarks == null)
                //        {
                //            objResponseMsg.Key = false;
                //            objResponseMsg.Value = "Remarks is Required in" + " " + item.Checklist;
                //            return objResponseMsg;
                //        }
                //    }

                //}

                if (item.IsAttachmentMandatory == true)
                {
                    //var folderPath = "PAM005/" + item.Id;
                    //var existing = (new clsFileUpload()).GetDocuments(folderPath);

                    //if (!existing.Any())
                    FileUploadController _obj = new FileUploadController();
                    if (!_obj.CheckAnyDocumentsExits("PAM005", item.Id))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Attachment(s) is Required in" + " " + item.Checklist;
                        return objResponseMsg;
                    }
                }
            }
            var Getdss = db.PAM007.Where(M => M.HeaderId == headerId).FirstOrDefault();
            if (Getdss == null)
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please Update Applicable Customer specifications";
                return objResponseMsg;
            }
            objResponseMsg.Key = true;
            return objResponseMsg;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="headerId"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> SendForApprove(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            PAM001 objPAM001 = new PAM001();
            try
            {
                clsHelper.ResponseMsg isValid = IsValidatePAM(headerId);
                if (!isValid.Key)
                {
                    return Json(isValid, JsonRequestBehavior.AllowGet);
                }
                if (headerId > 0)
                {
                    objPAM001 = await db.PAM001.Where(i => i.HeaderId == headerId).FirstOrDefaultAsync();
                    objPAM001.Status = clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue();
                    await db.SaveChangesAsync();
                    //Add entry in pam history
                    PAM004 objPAM004 = new PAM004
                    {
                        PAM = objPAM001.PAM,
                        Action = "SUBMIT",
                        ActionBy = objClsLoginInfo.UserName,
                        ActionOn = DateTime.Now,
                        Comments = "PAM Submitted"
                    };
                    db.PAM004.Add(objPAM004);
                    db.SaveChanges();
                    Hashtable _ht = new Hashtable();
                    EmailSend _objEmail = new EmailSend();
                    _ht["[PMGName]"] = Manager.GetUserNameFromPsNo(objPAM001.PMGName);
                    _ht["[PAM]"] = objPAM001.PAM;
                    // _ht["[Approver]"] = Manager.GetUserNameFromPsNo(objPAM003.BUHead);
                    _ht["[OriginatedBy]"] = Manager.GetUserNameFromPsNo(objPAM001.CreatedBy);
                    MAIL001 objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.PAM.SentforApproval).SingleOrDefault();
                    //_objEmail.MailToAdd = Manager.GetMailIdFromPsNo(objIMB001.ApprovedBy);
                    _objEmail.MailToAdd = Manager.GetMailIdFromPsNo(objPAM001.PMGName);

                    _ht["[Subject]"] = "PAM" + "" + objPAM001.PAM + "" + "sent for approval";
                    string host = Request.Url.Host;
                    _ht["PAMApproveLink"] = WebsiteURL + "/PAM/PMG/ApproveIndividualPam/" + objPAM001.HeaderId;
                    _objEmail.MailBody = objTemplateMaster.Body;
                    _objEmail.MailCc = Manager.GetMailIdFromPsNo(objPAM001.PMGName);
                    //_objEmail.mails = objTemplateMaster.Body;
                    //_objEmail.MailBody = objTemplateMaster.Body;
                    _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);

                    //PAM006 objPAM006 = new PAM006
                    //{
                    //    HeaderId = headerId,
                    //    PAM = objPAM001.PAM,
                    //    DSSNo = objPAM001.ContractNo + "_DSS_001",
                    //    Status = clsImplementationEnum.PAMStatus.Draft.GetStringValue(),
                    //    MKT = objPAM001.CreatedBy,
                    //    MKTComments = objPAM001.InitRemarks,
                    //    PMG = objPAM001.PMGName,
                    //    PMGComments = objPAM001.PMGRemarks,
                    //    CreatedOn = DateTime.Now,
                    //    CreatedBy = objClsLoginInfo.UserName,
                    //    RevNo = 0
                    //};

                    //db.PAM006.Add(objPAM006);
                    //await db.SaveChangesAsync();
                    SaveDSS(objPAM001);
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg);
        }

        public ActionResult GetProjectstatus(string project, int headerId)

        {   //get project and BU by Qms project

            if (!string.IsNullOrWhiteSpace(project))
            {
                BUWiseDropdown objProjectDataModel = new BUWiseDropdown();


                bool Getproject = db.PAM002.Any(m => m.HeaderId == headerId && m.Project == project);

                if (Getproject == true)
                {
                    objProjectDataModel.Status = true;
                }



                return Json(objProjectDataModel, JsonRequestBehavior.AllowGet);



            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "", string ContractNo = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();

            if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
            {
                var lstResult = db.SP_PAM_GETPAMDETAILSCONTRACTWISE(1, int.MaxValue, strSortOrder, whereCondition).Select(x => new { x.PAM, x.status, x.ConDesc, x.CustomerDesc, x.LOINo, x.LOIDate, x.CustOrdNo, x.CustOrdDate, x.CDD }).ToList();

                if (lstResult != null && lstResult.Count > 0)
                {
                    string str = Helper.GenerateExcel(lstResult, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = str;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Data not available.";
                }
            }
            else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
            {
                var lstResult = db.SP_PAM_PROJECT_CONTRACTWISE(1, int.MaxValue, "", whereCondition).Select(x => new { x.ROW_NO, x.Project, x.Equipment, x.LOIDate, x.Zerodate, x.CDD }).ToList();
                if (lstResult != null && lstResult.Count > 0)
                {
                    string str = Helper.GenerateExcel(lstResult, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = str;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Data not available.";
                }
            }
            else if (gridType == clsImplementationEnum.GridType.SUB_LINES.GetStringValue())
            {
                var lstResult = db.SP_PAM_SECTION_CHECKLIST(Convert.ToInt32(whereCondition), strSortOrder, objClsLoginInfo.UserName).Select(x => new { x.SequenceId, x.Checklist, x.IsChecked, x.Remarks, x.Attachment }).ToList();
                if (lstResult != null && lstResult.Count > 0)
                {
                    string str = Helper.GenerateExcel(lstResult, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = str;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Data not available.";
                }
            }
            else if (gridType == clsImplementationEnum.GridType.PaymentTermsLines.GetStringValue())
            {
                var lstResult = db.SP_PAM_PAYMENT_TERMS(1, int.MaxValue, ContractNo, "", whereCondition).Select(x => new { x.Description, x.Percentage, x.MilestoneType }).ToList();
                if (lstResult != null && lstResult.Count > 0)
                {
                    string str = Helper.GenerateExcel(lstResult, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = str;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Data not available.";
                }
            }
            else if (gridType == clsImplementationEnum.GridType.PAM_REQ_HEADER.GetStringValue())
            {
                var lstResult = db.SP_PAM_GENERATE_REQUESTS(1, int.MaxValue, "", whereCondition).Select(x => new { x.ConDesc, x.ContractNo, x.Customer, x.CustomerDesc, x.POExpectedOn, x.RequestedBy, x.Status }).ToList();
                if (lstResult != null && lstResult.Count > 0)
                {
                    string str = Helper.GenerateExcel(lstResult, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = str;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Data not available.";
                }
            }
            else if (gridType == clsImplementationEnum.GridType.SSS_REQ_HEADER.GetStringValue())
            {
                var lstResult = db.SP_PAM_PROJECT_CONTRACTWISE(1, int.MaxValue, strSortOrder, whereCondition).Select(x => new { x.Project, x.Equipment, x.LOIDate, x.Zerodate, x.CDD }).ToList();
                if (lstResult != null && lstResult.Count > 0)
                {
                    string str = Helper.GenerateExcel(lstResult, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = str;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Data not available.";
                }
            }
            else if (gridType == clsImplementationEnum.GridType.HISTORYVIEW.GetStringValue())
            {
                var lstResult = db.SP_PAM_HISTORY(1, int.MaxValue, strSortOrder, whereCondition).Select(x => new { x.PAM, x.Action, x.ActionBy, x.ActionOn, x.Comments }).ToList();
                if (lstResult != null && lstResult.Count > 0)
                {
                    string str = Helper.GenerateExcel(lstResult, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = str;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Data not available.";
                }
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Data not available.";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #region History Details
        [HttpPost]
        public ActionResult GetPAMHistory(string PAMNumber)
        {
            ViewBag.PAM = PAMNumber;
            return PartialView("_PAMHistoryDetailsHtmlPartial");
        }

        [HttpPost]
        public JsonResult LoadPAMHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;

                string sqlQuery = string.Empty;
                sqlQuery += "where PAM='" + param.Headerid + "'";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    sqlQuery += " and (PAM like '%" + param.sSearch + "%' or Action like '%" + param.sSearch + "%' or a.ActionBy +' - '+ c.t_name like '%" + param.sSearch + "%'  or Comments like '%" + param.sSearch + "%')";
                }
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstPAM004 = db.SP_PAM_HISTORY
                                (
                                StartIndex, EndIndex, strSortOrder, sqlQuery
                                ).ToList();

                var data = (from uc in lstPAM004
                            select new[]
                              {
                                Convert.ToString(uc.PAM),
                                Convert.ToString(uc.Action),
                                Convert.ToString(uc.ActionBy),
                                Convert.ToString(uc.ActionOn.Value.ToShortDateString()),
                                Convert.ToString(uc.Comments),
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstPAM004.Count > 0 && lstPAM004.FirstOrDefault().TotalCount > 0 ? lstPAM004.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstPAM004.Count > 0 && lstPAM004.FirstOrDefault().TotalCount > 0 ? lstPAM004.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = sqlQuery
                }, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        public class BUWiseDropdown
        {
            public bool Status { get; set; }

        }

        public void SaveDSS(PAM001 objPAM001)
        {
            string maxNoResult = string.Empty;
            var objPAM006 = db.PAM006.Where(i => i.HeaderId == objPAM001.HeaderId).FirstOrDefault();
            if (objPAM006 == null)
            {
                maxNoResult = db.Database.SqlQuery<string>("SELECT DBO.FN_GENERATE_MAX_DSS_NO('" + objPAM001.ContractNo + "')").FirstOrDefault();
                objPAM006 = new PAM006
                {
                    HeaderId = objPAM001.HeaderId,
                    PAM = objPAM001.PAM,
                    DSSNo = maxNoResult,
                    Status = clsImplementationEnum.PAMStatus.Draft.GetStringValue(),
                    MKT = objPAM001.CreatedBy,
                    MKTComments = objPAM001.InitRemarks,
                    PMG = objPAM001.PMGName,
                    PMGComments = objPAM001.PMGRemarks,
                    CreatedOn = DateTime.Now,
                    CreatedBy = objClsLoginInfo.UserName,
                    RevNo = 0
                };
                db.PAM006.Add(objPAM006);
                db.SaveChanges();
            }
            else
            {
                objPAM006.MKTComments = objPAM001.InitRemarks;
                db.SaveChanges();
            }
        }

        [HttpPost]
        public ActionResult CompletePAM(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objPAM001 = db.PAM001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                objPAM001.Status = clsImplementationEnum.PAMStatus.Completed.GetStringValue();
                objPAM001.EditedBy = objClsLoginInfo.UserName;
                objPAM001.EditedOn = DateTime.Now;

                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = "PAM completed successfully.";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg);
        }
    }
}