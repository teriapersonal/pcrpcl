﻿using IEMQS.Areas.PAM.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.PAM.Controllers
{
    public class PMGController : clsBase
    {


        PAMActionModel Action = new PAMActionModel();
        [AllowAnonymous]
        [SessionExpireFilter]
        [UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetPAMGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetPAMGridDataPartial");
        }

        public ActionResult loadPAMDataTable(JQueryDataTableParamModel param, string status)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                //whereCondition += " and loidate.t_csbu collate SQL_Latin1_General_CP850_CI_AI in (select * from dbo.fn_split(dbo.GETBUFROMUSER('" + objClsLoginInfo.UserName + "'),',')) ";
                whereCondition += " and t_csbu collate SQL_Latin1_General_CP850_CI_AI in (select * from dbo.fn_split(dbo.GETBUFROMUSER('" + objClsLoginInfo.UserName + "'),',')) ";

                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and status in ('" + clsImplementationEnum.IMBStatus.SentForApproval.GetStringValue() + "')";
                }
                else
                {
                    whereCondition += " and status in ('" + clsImplementationEnum.IMBStatus.Approved.GetStringValue() + "','" + clsImplementationEnum.IMBStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.IMBStatus.Returned.GetStringValue() + "')";
                }
                //string[] columnName = { "pam.PAM", "pam.status", "cont.t_cono", "cont.t_cono +'-'+ cont.t_desc", "cont.t_ofbp", "cont.t_ofbp +'-' + cust.t_nama", "t_refe", "loidate.t_lfdt", "loidate.t_ccdd", "t_refb", "t_codt", "t_efdt" };
                string[] columnName = { "PAM", "status", "ContractNo", "ConDesc ", "Customer", "CustomerDesc", "LOINo", "LOIDate", "CDD", "CustOrdNo", "CustOrdDate", "Zeordate", "DSS", "DSSStatus" };

                //  whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstPam = db.SP_PAM_GETPAMDETAILSCONTRACTWISE(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                var res = from h in lstPam
                          select new[] {
                           h.PAM,
                           Convert.ToString(h.HeaderId),
                           Convert.ToString( h.ContractNo),
                           Convert.ToString( h.ConDesc),
                           Convert.ToString( h.Customer),
                           Convert.ToString( h.CustomerDesc),
                           Convert.ToString( h.LOINo),
                           h.LOIDate == null || h.LOIDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) == "01/01/1970"? "NA" : h.LOIDate.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture),
                           Convert.ToString( h.CustOrdNo),
                           Convert.ToDateTime(h.CustOrdDate).ToString("dd/MM/yyyy"),
                           h.Zeordate == null || h.Zeordate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) == "01/01/1970"? "NA" : h.Zeordate.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture),
                           Convert.ToDateTime(h.CDD).ToString("dd/MM/yyyy"),
                           Convert.ToString(h.Prospect == 1 ? "Yes":"No"),
                           Convert.ToString( h.DSS),
                           Convert.ToString( h.DSSStatus),
                           h.status,
                           "" //Action
                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = "",
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.TimelineTitle = "PAM History";
            model.Title = "PAM";
            PAM001 objPAM001 = db.PAM001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            model.CreatedBy = Manager.GetUserNameFromPsNo(objPAM001.CreatedBy);
            model.CreatedOn = objPAM001.CreatedOn;
            model.ApprovedBy = Manager.GetUserNameFromPsNo(objPAM001.PMGName);
            model.ApprovedOn = objPAM001.PMGResponseDate;


            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        [AllowAnonymous]
        [SessionExpireFilter]
        [UserPermissions]
        public ActionResult ApproveIndividualPam(int id)
        {
            PAM001 objPAM001 = db.PAM001.FirstOrDefault(x => x.HeaderId == id);
            string ManagerName = db.COM003.Where(x => x.t_psno == objPAM001.PMGName && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
            if (objPAM001.PMGName != null)
            {
                //  objPAM001.PMGName = objPAM001.PMGName + " - " + ManagerName;
                ViewBag.pmgname = objPAM001.PMGName + " - " + ManagerName;
            }
            else
            {
                objPAM001.PMGName = null;
                ViewBag.pmgname = "";
            }
            PAM006 objPAM006 = db.PAM006.Where(i => i.HeaderId == id).FirstOrDefault();
            if (objPAM006 == null)
            {
                ViewBag.DisplayDSS = false;
                ViewBag.DSSId = 0;
            }
            else
            {
                ViewBag.DisplayDSS = true;
                ViewBag.DSSId = objPAM006.DSSId;
            }
            string createdby = db.COM003.Where(x => x.t_psno == objPAM001.CreatedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
            if (objPAM001.CreatedBy != null)
            {
                objPAM001.CreatedBy = objPAM001.CreatedBy + " - " + createdby;
            }

            ViewBag.ContractNo = db.Database.SqlQuery<string>(@"SELECT t_cono +'-'+ t_desc  FROM " + LNLinkedServer + ".dbo.ttpctm100175 where t_cono = '" + objPAM001.ContractNo + "'").FirstOrDefault();
            ViewBag.Customer = db.Database.SqlQuery<string>(@"SELECT  t_bpid +'-'+ t_nama   from " + LNLinkedServer + ".dbo.ttccom100175  where t_bpid = '" + objPAM001.Customer + "'").FirstOrDefault();

            return View(objPAM001);
        }

        public ActionResult loadProjectDataTable(JQueryDataTableParamModel param, int headerId)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "HeaderId = " + headerId + "";
                string[] columnName = { "PAM", "Project", "ProjectDescription", "Equipment" };
                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                var lstPamProject = db.SP_PAM_PROJECT_CONTRACTWISE(StartIndex, EndIndex, "", whereCondition).ToList();
                int? totalRecords = lstPamProject.Count();
                var res = from h in lstPamProject
                          select new[] {
                           Convert.ToString(h.ROW_NO),
                           Convert.ToString(h.Id),
                           Convert.ToString(h.HeaderId),
                           Convert.ToString(h.Project),
                           Convert.ToString(h.Equipment),
                               h.LOIDate == null || h.LOIDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) == "01/01/1970"? "NA" : h.LOIDate.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture),
                              //Convert.ToString(Convert.ToDateTime(h.LOIDate).ToString("dd/MM/yyyy") == "01/01/1970" ? "":Convert.ToDateTime(h.LOIDate).ToString("dd/MM/yyyy")),
                             Convert.ToDateTime(h.Zerodate).ToString("dd/MM/yyyy"),
                           Convert.ToDateTime(h.CDD).ToString("dd/MM/yyyy"),
                           Convert.ToString(h.Id),
                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = "",
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ChecklistAttachmentPartial(int? id)
        {
            PAM005 objPAM005 = db.PAM005.Where(x => x.Id == id).FirstOrDefault();
            ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments1("PAM005/" + id));
            var status = db.PAM001.Where(m => m.PAM == objPAM005.PAM).FirstOrDefault();
            ViewBag.status = status.Status;

            return PartialView("_ChecklistAttachmentPartial", objPAM005);
        }
        [HttpPost]
        public ActionResult ApproverResponse(FormCollection fc)
        {
            PAM001 objPAM001 = new PAM001();
            PAM004 objPAM004 = null;
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (fc != null)
                {
                    int headerId = Convert.ToInt32(fc["HeaderId"]);
                    string respondType = Convert.ToString(fc["RespondType"]);
                    objPAM001 = db.PAM001.FirstOrDefault(x => x.HeaderId == headerId);
                    string ManagerName = db.COM003.Where(x => x.t_psno == objPAM001.PMGName && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                    if (objPAM001.PMGName != null)
                    {
                        ViewBag.pmgname = objPAM001.PMGName + " - " + ManagerName;
                    }
                    else
                    {
                        objPAM001.PMGName = objClsLoginInfo.UserName;
                        //objPAM001.PMGName = null;
                        ViewBag.pmgname = "";
                    }
                    string createdby = db.COM003.Where(x => x.t_psno == objPAM001.CreatedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();

                    if (respondType == "approve")
                    {
                        objPAM001.Status = clsImplementationEnum.PAMStatus.Approved.GetStringValue();
                        objResponseMsg.Value = "PAM Approved Successfully";
                        Hashtable _ht = new Hashtable();
                        EmailSend _objEmail = new EmailSend();
                        _ht["[OriginatedBy]"] = Manager.GetUserNameFromPsNo(objPAM001.CreatedBy);
                        _ht["[PAM]"] = objPAM001.PAM;
                        string host = Request.Url.Host;

                        _ht["[PAMApproveLink]"] = WebsiteURL + "/PAM/PMG/ApproveIndividualPam/" + objPAM001.HeaderId;
                        _ht["[PMGName]"] = Manager.GetUserNameFromPsNo(objPAM001.PMGName);

                        MAIL001 objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.PAM.Approval).SingleOrDefault();
                        //_objEmail.MailToAdd = Manager.GetMailIdFromPsNo(objIMB001.ApprovedBy);
                        _objEmail.MailToAdd = Manager.GetMailIdFromPsNo(objPAM001.CreatedBy);

                        _ht["[Subject]"] = "PAM" + "" + objPAM001.PAM.ToString() + "" + "Is Approved";

                        _objEmail.MailBody = objTemplateMaster.Body;
                        _objEmail.MailCc = Manager.GetMailIdFromPsNo(objPAM001.PMGName.ToString());
                        //_objEmail.mails = objTemplateMaster.Body;
                        //_objEmail.MailBody = objTemplateMaster.Body;
                        _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);

                        objPAM004 = new PAM004
                        {
                            PAM = objPAM001.PAM,
                            Action = "APPROVE",
                            ActionBy = objClsLoginInfo.UserName,
                            ActionOn = DateTime.Now,
                            Comments = "PAM Approved"
                        };

                    }
                    else
                    {

                        objPAM001.Status = clsImplementationEnum.PAMStatus.Returned.GetStringValue();
                        objResponseMsg.Value = "PAM Returned Successfully";
                        Hashtable _ht = new Hashtable();
                        EmailSend _objEmail = new EmailSend();
                        _ht["[OriginatedBy]"] = Manager.GetUserNameFromPsNo(objPAM001.CreatedBy);
                        _ht["[PAM]"] = objPAM001.PAM;
                        string host = Request.Url.Host;

                        _ht["[PAMApproveLink]"] = WebsiteURL + "/PAM/PMG/ApproveIndividualPam/" + objPAM001.HeaderId;
                        _ht["[PMGName]"] = Manager.GetUserNameFromPsNo(objClsLoginInfo.UserName);
                        _ht["[PMGRemarks]"] = fc["PMGRemarks"].ToString();
                        MAIL001 objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.PAM.PamReturn).SingleOrDefault();
                        //_objEmail.MailToAdd = Manager.GetMailIdFromPsNo(objIMB001.ApprovedBy);
                        _objEmail.MailToAdd = Manager.GetMailIdFromPsNo(objPAM001.CreatedBy);

                        _ht["[Subject]"] = "PAM" + "" + objPAM001.PAM.ToString() + "" + "Is Returned";

                        _objEmail.MailBody = objTemplateMaster.Body;
                        _objEmail.MailCc = Manager.GetMailIdFromPsNo(objPAM001.PMGName.ToString());
                        //_objEmail.mails = objTemplateMaster.Body;
                        //_objEmail.MailBody = objTemplateMaster.Body;
                        _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);

                        objPAM004 = new PAM004
                        {
                            PAM = objPAM001.PAM,
                            Action = "RETURN",
                            ActionBy = objClsLoginInfo.UserName,
                            ActionOn = DateTime.Now,
                            Comments = "PAM Returned"
                        };

                    }
                    objPAM001.PMGRemarks = Convert.ToString(fc["PMGRemarks"]);
                    //objPAM001.PMGName = objClsLoginInfo.UserName;
                    objPAM001.PMGResponseDate = DateTime.Now;
                    db.PAM004.Add(objPAM004);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult loadCheckListDataTable(JQueryDataTableParamModel param, int? headerId, string pam)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                var lstPam = db.SP_PAM_SECTION_CHECKLIST(headerId, pam, objClsLoginInfo.UserName).ToList();

                var res = from h in lstPam
                          select new[] {
                           Convert.ToString( h.SequenceId),
                           Convert.ToString(h.HeaderId),
                           Convert.ToString(h.SectionId),
                           Convert.ToString(h.ChecklistId),
                           Convert.ToString( h.Checklist),
                           GetChecklistCheckStyle(h.Status,h.IsChecked),
                           "<input type='text' name='txtSectionRemark' style='width:100%' readonly= 'readonly'  class='form-control input-sm  input-inline sectionTextbox' value='" + h.Remarks + "' />",
                          // Convert.ToString( h.Attachment),
                           GetAttachment( h.Id,h.Attachment),
                           Convert.ToString(h.Id),
                           Convert.ToString(h.IsAttachmentMandatory),
                           h.Section

                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = lstPam.Count,
                    iTotalRecords = lstPam.Count,
                    aaData = res,
                    whereCondition = headerId,
                    strSortOrder = pam,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public string GetAttachment(int id, string attachment)
        {
            //var folderPath = "PAM005/" + id;
            //var existing = (new clsFileUpload()).GetDocuments(folderPath);

            //if (!existing.Any())
            //{
            //    attachment = "False";
            //}
            //else
            //{
            //    attachment = "True";
            //}
            FileUploadController _obj = new FileUploadController();
            attachment = _obj.CheckAnyDocumentsExits("PAM005", id).ToString();
            return attachment;
        }
        public string GetChecklistCheckStyle(string status, bool? isCheck)
        {
            if (isCheck == null)
                isCheck = false;
            if (isCheck == true)
            {
                return "<input type='checkbox' name='chkChecklistCheck' id='chkChecklistCheck' disabled= 'disabled'  class='make-switch col-md-3' checked='checked'    data-on-text='Yes' data-off-text='No' data-on-color='success' data-off-color='danger' data-size='normal' />";
            }
            else
            {
                return "<input type='checkbox' name='chkChecklistCheck' id='chkChecklistCheck'  class='make-switch col-md-3' disabled= 'disabled'    data-on-text='Yes' data-off-text='No' data-on-color='success' data-off-color='danger' data-size='normal' />";
            }
        }
    }
}