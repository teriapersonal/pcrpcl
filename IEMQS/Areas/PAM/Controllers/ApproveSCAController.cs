﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace IEMQS.Areas.PAM.Controllers
{
    public class ApproveSCAController : clsBase
    {
        #region Utility
        public static string GenerateTextboxFor(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string maxLength = "", bool disabled = false)
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (columnName.ToLower() == "pages")
            {
                className += " numeric";
            }
            htmlControl = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id=" + inputID + " data-lineid='" + rowId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + " maxlength='" + maxLength + "' " + (disabled ? "disabled" : "") + "  />";

            return htmlControl;
        }

        public static string GenerateGridButton(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";


            htmlControl = "<a id='" + inputID + "' name='" + inputID + "' class='btn btn-outline btn-circle btn-sm blue' " + onClickEvent + " ><i class='" + className + "'></i> " + buttonName + "</a>";

            //htmlControl = "<i  data-modal='' id='" + inputID + "' name='" + inputID + "' style='cursor:Pointer;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";

            return htmlControl;
        }

        public string GetChecklistCheckStyle1(string status, bool? isCheck)
        {
            if (isCheck == null)
                isCheck = false;
            if (status == clsImplementationEnum.PAMStatus.Approved.GetStringValue())
            {
                return "<input type='checkbox' name='chkChecklistCheck' id='chkChecklistCheck' disabled= 'disabled'  class='make-switch col-md-3' checked='checked'    data-on-text='Yes' data-off-text='No' data-on-color='success' data-off-color='danger' data-size='normal' />";
            }

            else if (status != clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue() && isCheck == true)
            {
                return "<input type='checkbox' name='chkChecklistCheck' id='chkChecklistCheck'  class='make-switch col-md-3' checked='checked'  data-on-text='Yes' data-off-text='No' data-on-color='success' data-off-color='danger' data-size='normal' />";
            }
            else if (status != clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue() && isCheck != true)
            {
                return "<input type='checkbox' name='chkChecklistCheck' id='chkChecklistCheck'   class='make-switch col-md-3'  data-on-text='Yes' data-off-text='No' data-on-color='success' data-off-color='danger' data-size='normal' />";
            }
            else if (isCheck == true)
            {
                return "<input type='checkbox' name='chkChecklistCheck' id='chkChecklistCheck' disabled= 'disabled'  class='make-switch col-md-3' checked='checked'    data-on-text='Yes' data-off-text='No' data-on-color='success' data-off-color='danger' data-size='normal' />";
            }
            else
            {
                return "<input type='checkbox' name='chkChecklistCheck' id='chkChecklistCheck'  class='make-switch col-md-3' disabled= 'disabled'    data-on-text='Yes' data-off-text='No' data-on-color='success' data-off-color='danger' data-size='normal' />";
            }
        }

        public string GetChecklistCheckStyle(int LineId, string status, bool? isCheck, bool? isDisabled)
        {
            string rtnchecjbox = "";
            if (isCheck == null)
                isCheck = false;
            rtnchecjbox = "<input type='checkbox' style='text-align:center' name='chkChecklistCheck" + LineId.ToString() + "' id='chkChecklistCheck" + LineId.ToString() + "' class='make-switch col-md-3' data-on-text='Yes' data-off-text='No' data-on-color='success' data-off-color='danger' data-size='normal' onchange='UpdateAccepted(this," + LineId.ToString() + ")' ";
            if (isDisabled == true)
            {
                rtnchecjbox += " disabled= 'disabled' ";
            }
            if (isCheck == true)
            {
                rtnchecjbox += "checked='checked' ";
            }
            //if (status.ToLower() == clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue().ToLower())
            //{
            //    if (isCheck == true)
            //    {
            //        rtnchecjbox += " checked='checked' ";
            //    }
            //}
            //else
            //{
            //    rtnchecjbox += " disabled= 'disabled' ";
            //}
            rtnchecjbox += "/>";
            return rtnchecjbox;
        }

        public string GetSectionRemarkStyle(int LineId, string status, string sectionRemarks)
        {
            string rtnremarks = "";
            rtnremarks += "<input type='text' id='txtSectionRemark" + LineId.ToString() + "' name='txtSectionRemark" + LineId.ToString() + "' style='width:100%'  class='form-control input-sm  input-inline sectionTextbox' value='" + sectionRemarks + "' onblur='UpdateRemarks(this," + LineId.ToString() + ")' ";
            if (status.ToLower() != clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue().ToLower())
            {
                rtnremarks += " disabled='disabled' ";
            }
            rtnremarks += " />";
            return rtnremarks;
        }
        #endregion
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetSCAGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetSCAGridDataPartial");
        }

        [HttpPost]
        public JsonResult LoadSCAHeaderDataResult(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and pm16.RequestStatus in('" + clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue() + "') and pm16.PMGPerson in('" + objClsLoginInfo.UserName + "') ";
                }
                else
                {
                    strWhere += "1=1 ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "pm16.Request",
                                                "pm16.RequestStatus",
                                                "(select LTRIM(RTRIM(pm16.ContractNo)) + '-' + LTRIM(RTRIM(cm4.t_desc)) from COM004 cm4 where t_cono = pm16.ContractNo)",
                                                "(select LTRIM(RTRIM(pm16.Customer)) + '-' + LTRIM(RTRIM(cm6.t_nama)) from COM006 cm6 where t_bpid = pm16.Customer)",
                                                "pm16.LOINo",
                                                "pm16.ApprovedBy +' - '+com003a.t_name",
                                                "pm16.CustomerOrderNo",
                                                "pm16.CreatedBy +' - '+com003c.t_name"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_FETCH_SCAHEADER_RESULT
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.Request),
                               Convert.ToString(uc.RequestStatus),
                               Convert.ToString(uc.ContractNo),
                               Convert.ToString(uc.Customer),
                               Convert.ToString(uc.LOINo),
                               Convert.ToString(uc.LOIDate),
                               Convert.ToString(uc.CustomerOrderNo),
                               Convert.ToString(uc.CustomerOrderDate),
                               Convert.ToString(uc.CDD),
                               Convert.ToString(uc.ChangeNote),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn),
                               Convert.ToString(uc.ApprovedBy),
                               Convert.ToString(uc.ApprovedOn),
                               "<center style=\"display:inline-flex;\"><a style=\"View Details\" class='' href='"+WebsiteURL+"/PAM/ApproveSCA/SCADetailsView?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a></center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhere,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = 0,
                    iTotalDisplayRecords = 0,
                    aaData = new string[0]
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]
        public ActionResult SCADetailsView(int HeaderID = 0)
        {
            string currentUser = objClsLoginInfo.UserName;
            PAM016 objPAM016 = db.PAM016.Where(x => x.PMGPerson == currentUser && x.HeaderId == HeaderID).FirstOrDefault();
            if (objPAM016 != null)
            {
                ViewBag.HeaderID = HeaderID;
                return View();
            }
            else
            {
                return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
            }
        }

        [HttpPost]
        public ActionResult LoadSCADetails(int HeaderID)
        {
            PAM016 objPAM016 = db.PAM016.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
            string user = objClsLoginInfo.UserName;
            List<string> lstBUs = (from a in db.ATH001
                                   where a.Employee == user
                                   select a.BU).Distinct().ToList();

            if (lstBUs.Count > 0)
            {
                ViewBag.strBUs = string.Join(",", lstBUs);
            }
            else
            {
                ViewBag.strBUs = "";
            }
            if (objPAM016 != null && objPAM016.HeaderId > 0)
            {
                var objCustomers = (from cm004 in db.COM004
                                    join cm006 in db.COM006 on cm004.t_ofbp equals cm006.t_bpid
                                    where cm004.t_cono.Trim() == objPAM016.ContractNo.Trim()
                                    select new ApproverModel { Code = cm004.t_ofbp, Name = cm004.t_ofbp + "-" + cm006.t_nama }).FirstOrDefault();

                var objContractNos = (from cm004 in db.COM004
                                      join cm008 in db.COM008 on cm004.t_cono equals cm008.t_cono
                                      where cm004.t_cono == objPAM016.ContractNo
                                      orderby cm004.t_cono
                                      select new ApproverModel { Code = cm004.t_cono, Name = cm004.t_cono + "-" + cm004.t_desc }).FirstOrDefault();

                string pmgPerson = db.COM003.Where(x => x.t_psno == objPAM016.PMGPerson && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                ViewBag.pmgPerson = objPAM016.PMGPerson + " - " + pmgPerson;

                ViewBag.ContractDesc = objContractNos.Name;
                ViewBag.CustomersDesc = objCustomers.Name;
            }
            else
            {
                objPAM016 = new PAM016();
                objPAM016.RequestStatus = clsImplementationEnum.ARMStatus.DRAFT.GetStringValue();
                PAM016 pam016 = db.PAM016.OrderByDescending(x => x.HeaderId).FirstOrDefault();
                if (pam016 != null)
                {
                    string strRequestNo = Convert.ToString(pam016.Request.Split('_')[2]);
                    string s = strRequestNo;
                    int number = Convert.ToInt32(s);
                    number += 1;
                    string str = number.ToString("D3");
                    ViewBag.requestNo = str;

                }
                else
                {
                    ViewBag.requestNo = "001";
                }

            }
            return PartialView("_LoadSCADetails", objPAM016);
        }

        [HttpPost]
        public JsonResult LoadSCAHeaderLineDataResult(JQueryDataTableParamModel param, int HeaderId = 0)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                strWhere += "1=1 and pm17.HeaderId = " + HeaderId + "";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {

                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_FETCH_SCAHEADER_LINES_RESULT
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.ProjectNo),
                               Convert.ToString(uc.Equipment),
                               Convert.ToString(uc.LOIDate),
                               Convert.ToString(uc.ZeroDate),
                               Convert.ToString(uc.CDD),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn),
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhere,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = 0,
                    iTotalDisplayRecords = 0,
                    aaData = new string[0]
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult loadSCALineDocDataTable(JQueryDataTableParamModel param, int HeaderId)
        {
            try
            {
                PAM016 objPAM016 = db.PAM016.Where(i => i.HeaderId == HeaderId).FirstOrDefault();

                if (objPAM016 == null)
                {
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = 0,
                        iTotalRecords = 0,
                        aaData = new string[0]
                    }, JsonRequestBehavior.AllowGet);
                }

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                bool isEditable = false;
                string whereCondition = "1=1";
                whereCondition += " and pm18.HeaderId= " + HeaderId.ToString();

                if (objPAM016.RequestStatus.ToLower() == clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue().ToLower())
                {
                    isEditable = true;
                }

                //if (objPAM016.RequestStatus.ToLower() == clsImplementationEnum.PAMStatus.Approved.GetStringValue().ToLower())
                //{
                //    whereCondition += " and IncludeInDIS = 1";
                //}
                List<SelectListItem> pmgApprovelist = new List<SelectListItem>(clsImplementationEnum.PMGApprovalList().Select(x => new SelectListItem { Text = x.ToString(), Value = x.ToString() }));
                // List<SelectListItem> BoolenList = new List<SelectListItem>() { new SelectListItem { Text = "Select", Value = "" }, new SelectListItem { Text = "Yes", Value = "True" }, new SelectListItem { Text = "No", Value = "False" } };
                string[] columnName = { "Description", "DocNo", "DocRev" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstPam = db.SP_FETCH_SCAHEADER_DOC_LINES_RESULT(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from h in lstPam
                           select new[] {
                                    Convert.ToString(h.ROW_NO),
                                    Convert.ToString(h.LineId),
                                    Helper.GenerateHidden(h.LineId, "HeaderId", Convert.ToString(h.HeaderId)),
                                    Convert.ToString(h.Description),
                                   Convert.ToString(h.DocNo),
                                   Convert.ToString(h.DocRev),
                                   Convert.ToString(h.DesignRemarks),
                                    "",
                                    isEditable == true ?Helper.GenerateDropdown(h.LineId, "PMGApproval",new SelectList(pmgApprovelist, "Value", "Text",h.PMGApproval),"","",false,""):Helper.GenerateDropdown(h.LineId, "PMGApproval",new SelectList(pmgApprovelist, "Value", "Text",h.PMGApproval),"","",true,""),
                                    isEditable == true ? GenerateTextboxFor(h.LineId,"PMGRemarks",h.PMGRemarks,"","",false,"","15",false) : GenerateTextboxFor(h.LineId,"PMGRemarks",h.PMGRemarks,"","",true,"","15",false),
                                    }).ToList();

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);



            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]
        public ActionResult SCAAttachmentPartial(int? id, string status = "")
        {
            ViewBag.Status = status;
            PAM018 objPAM018 = db.PAM018.Where(x => x.LineId == id).FirstOrDefault();
            return PartialView("_SCAAttachmentPartial", objPAM018);
        }

        [HttpPost]
        public ActionResult ApproveRequest(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PAM016 objPAM016 = db.PAM016.Where(i => i.HeaderId == HeaderId).FirstOrDefault();
                if (objPAM016 != null)
                {
                    objPAM016.RequestStatus = clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue();
                    objPAM016.ApprovedBy = objClsLoginInfo.UserName;
                    objPAM016.ApprovedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PTTechniqueMeassage.Approve.ToString();
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Request not available.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg);
        }

        [HttpPost]
        public ActionResult UpdateApproverData(string strJson)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (!string.IsNullOrWhiteSpace(strJson))
                {
                    List<ApproverModelEdit> lstApproverModelEdit = new JavaScriptSerializer().Deserialize<List<ApproverModelEdit>>(strJson);

                    int[] arrayLineIds = lstApproverModelEdit.Select(x => x.LineId).ToArray();
                    List<PAM018> lstPAM018 = db.PAM018.Where(x => arrayLineIds.Contains(x.LineId)).ToList();
                    foreach (PAM018 objPAM018 in lstPAM018)
                    {
                        ApproverModelEdit objApproverModelEdit = lstApproverModelEdit.Where(x => x.LineId == objPAM018.LineId && x.HeaderId == objPAM018.HeaderId).FirstOrDefault();
                        objPAM018.PMGApproval = objApproverModelEdit.PMG_approval;
                        objPAM018.PMGRemarks = objApproverModelEdit.PMG_remarks;
                        objPAM018.EditBy = objClsLoginInfo.UserName;
                        objPAM018.EditedOn = DateTime.Now;
                    }
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Request details updated successfully.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Request details are not available.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg);
        }
    }

    public class ApproverModelEdit
    {
        public int LineId { get; set; }
        public int HeaderId { get; set; }
        public string PMG_approval { get; set; }
        public string PMG_remarks { get; set; }

    }
}