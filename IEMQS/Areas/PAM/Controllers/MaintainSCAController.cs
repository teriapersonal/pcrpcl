﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.PAM.Controllers
{
    public class MaintainSCAController : clsBase
    {

        #region Utility
        public static string GenerateTextboxFor(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string maxLength = "", bool disabled = false)
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (columnName.ToLower() == "pages")
            {
                className += " numeric";
            }
            htmlControl = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id=" + inputID + " data-lineid='" + rowId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + " maxlength='" + maxLength + "' " + (disabled ? "disabled" : "") + "  />";

            return htmlControl;
        }

        public static string GenerateGridButton(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";


            htmlControl = "<a id='" + inputID + "' name='" + inputID + "' class='btn btn-outline btn-circle btn-sm blue' " + onClickEvent + " ><i class='" + className + "'></i> " + buttonName + "</a>";

            //htmlControl = "<i  data-modal='' id='" + inputID + "' name='" + inputID + "' style='cursor:Pointer;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";

            return htmlControl;
        }

        public string GetChecklistCheckStyle1(string status, bool? isCheck)
        {
            if (isCheck == null)
                isCheck = false;
            if (status == clsImplementationEnum.PAMStatus.Approved.GetStringValue())
            {
                return "<input type='checkbox' name='chkChecklistCheck' id='chkChecklistCheck' disabled= 'disabled'  class='make-switch col-md-3' checked='checked'    data-on-text='Yes' data-off-text='No' data-on-color='success' data-off-color='danger' data-size='normal' />";
            }

            else if (status != clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue() && isCheck == true)
            {
                return "<input type='checkbox' name='chkChecklistCheck' id='chkChecklistCheck'  class='make-switch col-md-3' checked='checked'  data-on-text='Yes' data-off-text='No' data-on-color='success' data-off-color='danger' data-size='normal' />";
            }
            else if (status != clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue() && isCheck != true)
            {
                return "<input type='checkbox' name='chkChecklistCheck' id='chkChecklistCheck'   class='make-switch col-md-3'  data-on-text='Yes' data-off-text='No' data-on-color='success' data-off-color='danger' data-size='normal' />";
            }
            else if (isCheck == true)
            {
                return "<input type='checkbox' name='chkChecklistCheck' id='chkChecklistCheck' disabled= 'disabled'  class='make-switch col-md-3' checked='checked'    data-on-text='Yes' data-off-text='No' data-on-color='success' data-off-color='danger' data-size='normal' />";
            }
            else
            {
                return "<input type='checkbox' name='chkChecklistCheck' id='chkChecklistCheck'  class='make-switch col-md-3' disabled= 'disabled'    data-on-text='Yes' data-off-text='No' data-on-color='success' data-off-color='danger' data-size='normal' />";
            }
        }

        public string GetChecklistCheckStyle(int LineId, string status, bool? isCheck, bool? isDisabled)
        {
            string rtnchecjbox = "";
            if (isCheck == null)
                isCheck = false;
            rtnchecjbox = "<input type='checkbox' style='text-align:center' name='chkChecklistCheck" + LineId.ToString() + "' id='chkChecklistCheck" + LineId.ToString() + "' class='make-switch col-md-3' data-on-text='Yes' data-off-text='No' data-on-color='success' data-off-color='danger' data-size='normal' onchange='UpdateAccepted(this," + LineId.ToString() + ")' ";
            if (isDisabled == true)
            {
                rtnchecjbox += " disabled= 'disabled' ";
            }
            if (isCheck == true)
            {
                rtnchecjbox += "checked='checked' ";
            }
            //if (status.ToLower() == clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue().ToLower())
            //{
            //    if (isCheck == true)
            //    {
            //        rtnchecjbox += " checked='checked' ";
            //    }
            //}
            //else
            //{
            //    rtnchecjbox += " disabled= 'disabled' ";
            //}
            rtnchecjbox += "/>";
            return rtnchecjbox;
        }

        public string GetSectionRemarkStyle(int LineId, string status, string sectionRemarks)
        {
            string rtnremarks = "";
            rtnremarks += "<input type='text' id='txtSectionRemark" + LineId.ToString() + "' name='txtSectionRemark" + LineId.ToString() + "' style='width:100%'  class='form-control input-sm  input-inline sectionTextbox' value='" + sectionRemarks + "' onblur='UpdateRemarks(this," + LineId.ToString() + ")' ";
            if (status.ToLower() != clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue().ToLower())
            {
                rtnremarks += " disabled='disabled' ";
            }
            rtnremarks += " />";
            return rtnremarks;
        }
        #endregion

        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetSCAGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetSCAGridDataPartial");
        }

        [HttpPost]
        public JsonResult LoadSCAHeaderDataResult(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and pm16.RequestStatus in('" + clsImplementationEnum.ARMStatus.DRAFT.GetStringValue() + "') ";
                }
                else
                {
                    strWhere += "1=1 ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "pm16.Request",
                                                "pm16.RequestStatus",
                                                "(select LTRIM(RTRIM(pm16.ContractNo)) + '-' + LTRIM(RTRIM(cm4.t_desc)) from COM004 cm4 where t_cono = pm16.ContractNo)",
                                                "(select LTRIM(RTRIM(pm16.Customer)) + '-' + LTRIM(RTRIM(cm6.t_nama)) from COM006 cm6 where t_bpid = pm16.Customer)",
                                                "pm16.LOINo",
                                                "pm16.ApprovedBy +' - '+com003a.t_name",
                                                "pm16.CustomerOrderNo",
                                                "pm16.CreatedBy +' - '+com003c.t_name"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_FETCH_SCAHEADER_RESULT
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.Request),
                               Convert.ToString(uc.RequestStatus),
                               Convert.ToString(uc.ContractNo),
                               Convert.ToString(uc.Customer),
                               Convert.ToString(uc.LOINo),
                               Convert.ToString(uc.LOIDate),
                               Convert.ToString(uc.CustomerOrderNo),
                               Convert.ToString(uc.CustomerOrderDate),
                               Convert.ToString(uc.CDD),
                               Convert.ToString(uc.ChangeNote),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn),
                               Convert.ToString(uc.ApprovedBy),
                               Convert.ToString(uc.ApprovedOn),
                               "<center style=\"display:inline-flex;\"><a style=\"View Details\" class='' href='"+WebsiteURL+"/PAM/MaintainSCA/SCADetailsView?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a></center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhere,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = 0,
                    iTotalDisplayRecords = 0,
                    aaData = new string[0]
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]
        public ActionResult SCADetailsView(int HeaderID = 0)
        {
            ViewBag.HeaderID = HeaderID;
            return View();
        }

        [HttpPost]
        public ActionResult LoadSCADetails(int HeaderID)
        {
            PAM016 objPAM016 = db.PAM016.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
            string user = objClsLoginInfo.UserName;
            List<string> lstBUs = (from a in db.ATH001
                                   where a.Employee == user
                                   select a.BU).Distinct().ToList();

            if (lstBUs.Count > 0)
            {
                ViewBag.strBUs = string.Join(",", lstBUs);
            }
            else
            {
                ViewBag.strBUs = "";
            }
            if (objPAM016 != null && objPAM016.HeaderId > 0)
            {
                var objCustomers = (from cm004 in db.COM004
                                    join cm006 in db.COM006 on cm004.t_ofbp equals cm006.t_bpid
                                    where cm004.t_cono.Trim() == objPAM016.ContractNo.Trim()
                                    select new ApproverModel { Code = cm004.t_ofbp, Name = cm004.t_ofbp + "-" + cm006.t_nama }).FirstOrDefault();

                var objContractNos = (from cm004 in db.COM004
                                      join cm008 in db.COM008 on cm004.t_cono equals cm008.t_cono
                                      where cm004.t_cono == objPAM016.ContractNo
                                      orderby cm004.t_cono
                                      select new ApproverModel { Code = cm004.t_cono, Name = cm004.t_cono + "-" + cm004.t_desc }).FirstOrDefault();


                ViewBag.ContractDesc = objContractNos.Name;
                ViewBag.CustomersDesc = objCustomers.Name;
            }
            else
            {
                objPAM016 = new PAM016();
                objPAM016.RequestStatus = clsImplementationEnum.ARMStatus.DRAFT.GetStringValue();
                PAM016 pam016 = db.PAM016.OrderByDescending(x => x.HeaderId).FirstOrDefault();
                if (pam016 != null)
                {
                    string strRequestNo = Convert.ToString(pam016.Request.Split('_')[2]);
                    string s = strRequestNo;
                    int number = Convert.ToInt32(s);
                    number += 1;
                    string str = number.ToString("D3");
                    ViewBag.requestNo = str;

                }
                else
                {
                    ViewBag.requestNo = "001";
                }

            }
            var listEmployee = Manager.GetApprover(clsImplementationEnum.UserRoleName.PMG3.GetStringValue(), objClsLoginInfo.Location, true, objClsLoginInfo.UserName, "");
            ViewBag.lstToEmployee = listEmployee.Select(i => new BULocWiseCategoryModel { CatID = i.id, CatDesc = i.text }).ToList();
            if (objPAM016.PMGPerson != null && objPAM016.PMGPerson != string.Empty)
            {
                ViewBag.PMGPersonName = objPAM016.PMGPerson + "-" + (db.COM003.FirstOrDefault(i => i.t_psno == objPAM016.PMGPerson && i.t_actv == 1).t_name);
            }
            return PartialView("_LoadSCADetails", objPAM016);
        }

        [HttpPost]
        public ActionResult SaveHeader(PAM016 pam016)
        {
            ResponceMsgWithData objResponseMsg = new ResponceMsgWithData();

            try
            {
                PAM016 objPAM016 = db.PAM016.Where(x => x.HeaderId == pam016.HeaderId).FirstOrDefault();
                if (objPAM016 != null)
                {
                    if (objPAM016.RequestStatus == clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue())
                    {
                        objPAM016.PMGPerson = pam016.PMGPerson;
                    }
                    else
                    {
                        objPAM016.ChangeNote = pam016.ChangeNote;
                        objPAM016.PMGPerson = pam016.PMGPerson;
                    }
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objPAM016.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Request updated successfully.";
                }
                else
                {
                    var contractDetails = db.Database.SqlQuery<ERPLnResult>(@"SELECT t_refe as LOINo,loidate.t_lfdt as LOIDate,loidate.t_ccdd as CDD,t_refb as CustOrdNo,t_codt as CustOrdDate,t_efdt as Zeordate from " + LNLinkedServer + ".dbo.ttpctm100175 cont join " + LNLinkedServer + ".dbo.tltctm100175 loidate on loidate.t_cono = cont.t_cono where cont.t_cono = '" + pam016.ContractNo + "'").FirstOrDefault();

                    objPAM016 = new PAM016();
                    objPAM016.Request = pam016.Request;
                    objPAM016.RequestStatus = clsImplementationEnum.ARMStatus.DRAFT.GetStringValue();
                    objPAM016.ContractNo = pam016.ContractNo;
                    objPAM016.Customer = pam016.Customer;
                    objPAM016.LOINo = (!string.IsNullOrWhiteSpace(contractDetails.LOINo) ? contractDetails.LOINo : null);
                    objPAM016.LOIDate = contractDetails.LOIDate;
                    objPAM016.CustomerOrderNo = contractDetails.CustOrdNo;
                    objPAM016.CustomerOrderDate = contractDetails.CustOrdDate;
                    objPAM016.CDD = contractDetails.CDD;
                    objPAM016.ChangeNote = pam016.ChangeNote;
                    objPAM016.PMGPerson = pam016.PMGPerson;
                    objPAM016.CreatedBy = objClsLoginInfo.UserName;
                    objPAM016.CreatedOn = DateTime.Now;
                    db.PAM016.Add(objPAM016);
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objPAM016.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Request generate successfully.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.ErplnErrorMessage.ToString();
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetProjects(int? headerId)
        {
            try
            {
                var items = db.Database.SqlQuery<ProjectModel>(@"select t_sprj as id,t_sprj+'-'+t_desc as text from " + LNLinkedServer + ".dbo.tltctm108175 where t_sprj<>'' and t_sprj collate SQL_Latin1_General_CP850_CI_AI not in(select pm17.ProjectNo from pam017 pm17 where pm17.Headerid = " + headerId + " ) and t_cono collate SQL_Latin1_General_CP850_CI_AI = (select ContractNo from pam016 where HeaderId=" + headerId + ")").ToList();
                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadSCAHeaderLineDataResult(JQueryDataTableParamModel param, int HeaderId = 0)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                strWhere += "1=1 and pm17.HeaderId = " + HeaderId + "";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {

                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_FETCH_SCAHEADER_LINES_RESULT
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.ProjectNo),
                               Convert.ToString(uc.Equipment),
                               Convert.ToString(uc.LOIDate),
                               Convert.ToString(uc.ZeroDate),
                               Convert.ToString(uc.CDD),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn),
                               "<center style=\"display:inline-flex;\"><i style='cursor:pointer;' title=\"Delete Project\" onclick=\"DeleteProject("+uc.LineId+")\" class='fa fa-trash'></i></center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhere,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = 0,
                    iTotalDisplayRecords = 0,
                    aaData = new string[0]
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AddProjects(int headerId, string project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PAM016 objPAM016 = db.PAM016.Where(x => x.HeaderId == headerId).FirstOrDefault();
                if (objPAM016 != null)
                {
                    string[] arrayProject = project.Split(',').ToArray();
                    List<PAM017> lstPAM017 = new List<PAM017>();
                    var lstProjects = db.SP_PAM_GETProjectDetails("", objPAM016.ContractNo).ToList();
                    for (int i = 0; i < arrayProject.Length; i++)
                    {
                        var projectdet = lstProjects.Where(x=>x.Project== arrayProject[i]).FirstOrDefault();
                        //var projectdet = db.SP_PAM_GETProjectDetails(Convert.ToString(arrayProject[i]), objPAM016.ContractNo).FirstOrDefault();
                        PAM017 objPAM017 = new PAM017();
                        objPAM017.HeaderId = objPAM016.HeaderId;
                        objPAM017.Request = objPAM016.Request;
                        objPAM017.ProjectNo = Convert.ToString(arrayProject[i]);
                        objPAM017.LOIDate = objPAM016.LOIDate;
                        //objPAM017.CDD = objPAM016.CDD;
                        objPAM017.CDD = projectdet.CDD;
                        objPAM017.ZeroDate = projectdet.ZeroDate;
                        objPAM017.Equipment = projectdet.Equipment;
                        objPAM017.CreatedBy = objClsLoginInfo.UserName;
                        objPAM017.CreatedOn = DateTime.Now;
                        lstPAM017.Add(objPAM017);
                    }
                    if (lstPAM017 != null && lstPAM017.Count > 0)
                    {
                        db.PAM017.AddRange(lstPAM017);
                        db.SaveChanges();
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Projects added successfully.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Request not available.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Something went wrong please try agin after sometime.";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProject(int LineID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PAM017 objPAM017 = db.PAM017.Where(x => x.LineId == LineID).FirstOrDefault();
                if (objPAM017 != null)
                {
                    db.PAM017.Remove(objPAM017);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Project deleted successfully.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Project not available.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Something went wrong please try agin after sometime.";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult loadSCALineDocDataTable(JQueryDataTableParamModel param, int HeaderId)
        {
            try
            {
                PAM016 objPAM016 = db.PAM016.Where(i => i.HeaderId == HeaderId).FirstOrDefault();

                if (objPAM016 == null)
                {
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = 0,
                        iTotalRecords = 0,
                        aaData = new string[0]
                    }, JsonRequestBehavior.AllowGet);
                }

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                bool isEditable = true;
                string whereCondition = "1=1";
                whereCondition += " and pm18.HeaderId= " + HeaderId.ToString();

                if (objPAM016.RequestStatus.ToLower() == clsImplementationEnum.PAMStatus.Approved.GetStringValue().ToLower() || objPAM016.RequestStatus.ToLower() == clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue().ToLower())
                {
                    isEditable = false;
                }

                //if (objPAM016.RequestStatus.ToLower() == clsImplementationEnum.PAMStatus.Approved.GetStringValue().ToLower())
                //{
                //    whereCondition += " and IncludeInDIS = 1";
                //}

                string[] columnName = { "Description", "DocNo", "DocRev" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstPam = db.SP_FETCH_SCAHEADER_DOC_LINES_RESULT(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                int newRecordId = 0;
                var newRecord = new[] {
                                    "0",
                                    "0",
                                    Helper.GenerateHidden(newRecordId, "HeaderId", Convert.ToString(HeaderId)),
                                    isEditable == true ? GenerateTextboxFor(newRecordId,"Description","","","",false,"","15",false) : GenerateTextboxFor(newRecordId,"Description","","","",false,"","15",true),
                                    isEditable == true ? GenerateTextboxFor(newRecordId,"DocNo","","","",false,"","15",false) : GenerateTextboxFor(newRecordId,"DocNo","","","",false,"","15",true),
                                    isEditable == true ? GenerateTextboxFor(newRecordId,"DocRev","R1","","",false,"","15",false) : GenerateTextboxFor(newRecordId,"DocRev","R1","","",false,"","15",true),
                                    isEditable == true ? GenerateTextboxFor(newRecordId,"DesignRemarks","","","",false,"","15",false) : GenerateTextboxFor(newRecordId,"DesignRemarks","","","",false,"","15",true),
                                    "",
                                    isEditable == true ? GenerateTextboxFor(newRecordId,"PMGApproval","","","",true,"","20",false) : GenerateTextboxFor(newRecordId,"PMGRemarks","","","",true,"","20",true),
                                    isEditable == true ? GenerateTextboxFor(newRecordId,"PMGRemarks","Pending","","",true,"","15",false) : GenerateTextboxFor(newRecordId,"PMGRemarks","","","",true,"","15",true),
                                    isEditable == true ? GenerateGridButton(newRecordId, "Add", "Add Rocord", "fa fa-plus", "SaveLineDocRecord(this,0);" ) : GenerateTextboxFor(newRecordId,"ReturnRemarks","","","",false,"","500",true),
                                };

                var res = (from h in lstPam
                           select new[] {
                                    Convert.ToString(h.ROW_NO),
                                    Convert.ToString(h.LineId),
                                    Helper.GenerateHidden(h.LineId, "HeaderId", Convert.ToString(h.HeaderId)),
                                    isEditable == true ? GenerateTextboxFor(h.LineId,"Description",h.Description,"","",false,"","15",false) : GenerateTextboxFor(h.LineId,"Description",h.Description,"","",false,"","15",true),
                                    isEditable == true ? GenerateTextboxFor(h.LineId,"DocNo",h.DocNo,"","",false,"","15",false) : GenerateTextboxFor(h.LineId,"DocNo",h.DocNo,"","",false,"","15",true),
                                    isEditable == true ? GenerateTextboxFor(h.LineId,"DocRev",h.DocRev,"","",false,"","15",false) : GenerateTextboxFor(h.LineId,"DocRev",h.DocRev,"","",false,"","15",true),
                                    isEditable == true ? GenerateTextboxFor(h.LineId,"DesignRemarks",h.DesignRemarks,"","",false,"","15",false) : GenerateTextboxFor(h.LineId,"DesignRemarks",h.DesignRemarks,"","",false,"","15",true),
                                    "",
                                    isEditable == true ? GenerateTextboxFor(h.LineId,"PMGApproval",h.PMGApproval,"","",true,"","20",false) : GenerateTextboxFor(h.LineId,"PMGApproval",h.PMGApproval,"","",true,"","20",true),
                                    isEditable == true ? GenerateTextboxFor(h.LineId,"PMGRemarks",h.PMGRemarks,"","",true,"","15",false) : GenerateTextboxFor(h.LineId,"PMGRemarks",h.PMGRemarks,"","",true,"","15",true),
                                    isEditable == true ? "<center><i style='cursor:pointer;padding-right:5px;' title=\"Edit Line\" onclick=\"SaveLineDocRecord(this,"+h.LineId+")\" class='fa fa-edit'></i><i style='cursor:pointer;' title=\"Delete Line\" onclick=\"DeleteSCALine("+h.LineId+")\" class='fa fa-trash'></i></center>" : "",
                                    }).ToList();

                res.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);



            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SaveDocLines(FormCollection fc, int Id = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int LineId = Id;
                int HeaderId = Convert.ToInt32(fc["HeaderId" + Id]);
                PAM016 objPAM016 = db.PAM016.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                PAM018 objPAM018 = null;
                if (LineId > 0)
                {
                    objPAM018 = db.PAM018.Where(x => x.LineId == LineId).FirstOrDefault();
                }
                else
                {
                    objPAM018 = new PAM018();
                    objPAM018.PMGApproval = clsImplementationEnum.PMGApproval.PENDING.GetStringValue();
                }
                objPAM018.HeaderId = objPAM016.HeaderId;
                objPAM018.Request = objPAM016.Request;
                objPAM018.Description = fc["Description" + LineId];
                objPAM018.DocNo = fc["DocNo" + LineId];
                objPAM018.DocRev = fc["DocRev" + LineId];
                objPAM018.DesignRemarks = fc["DesignRemarks" + LineId];
                //objPAM012.IncludeInDIS = fc["IncludeInDIS" + LineId];
                objPAM018.PMGRemarks = fc["PMGRemarks" + LineId];
                if (LineId > 0)
                {
                    objPAM018.EditBy = objClsLoginInfo.UserName;
                    objPAM018.EditedOn = DateTime.Now;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                }
                else
                {
                    objPAM018.CreatedBy = objClsLoginInfo.UserName;
                    objPAM018.CreatedOn = DateTime.Now;
                    db.PAM018.Add(objPAM018);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
                }
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public ActionResult SCAAttachmentPartial(int? id, string status = "")
        {
            ViewBag.Status = status;
            PAM018 objPAM018 = db.PAM018.Where(x => x.LineId == id).FirstOrDefault();
            return PartialView("_SCAAttachmentPartial", objPAM018);
        }

        [HttpPost]
        public ActionResult SaveSCAAttachment(string id, bool hasAttachments, Dictionary<string, string> Attach)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var folderPath = "PAM018/" + id;
                Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                objResponseMsg.Key = true;
                objResponseMsg.Value = "File Uploaded Successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SubmitHeader(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            bool IsFileAttach = true;
            try
            {
                PAM016 objPAM016 = db.PAM016.Where(i => i.HeaderId == HeaderId).FirstOrDefault();
                if (objPAM016 != null)
                {
                    foreach (var item in objPAM016.PAM018)
                    {
                        //var folderPath = "PAM018/" + item.LineId;
                        //var existing = (new clsFileUpload()).GetDocuments(folderPath);
                        FileUploadController _obj = new FileUploadController();
                        if (!_obj.CheckAnyDocumentsExits("PAM018", item.LineId))
                        {
                            IsFileAttach = false;
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "All line must have attachment!";
                            break;
                        }
                    }
                    if (IsFileAttach)
                    {
                        objPAM016.RequestStatus = clsImplementationEnum.NDETechniqueStatus.SentForApproval.GetStringValue();
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.PTTechniqueMeassage.SentForApproval.ToString();
                    }
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Request not available.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg);
        }

        [HttpPost]
        public ActionResult DeleteSCALine(int LineID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PAM018 objPAM018 = db.PAM018.Where(x => x.LineId == LineID).FirstOrDefault();
                if (objPAM018 != null)
                {
                    db.PAM018.Remove(objPAM018);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Line deleted successfully.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Line not available.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Something went wrong please try agin after sometime.";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();

            if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
            {
                List<clsSCAHeaderExport> lstclsSCAHeaderExport = new List<clsSCAHeaderExport>();
                var lstResult = db.SP_FETCH_SCAHEADER_RESULT(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                if (lstResult != null && lstResult.Count > 0)
                {
                    lstclsSCAHeaderExport.AddRange(lstResult.Select(x => new clsSCAHeaderExport
                    {
                        ROW_NO = x.ROW_NO,
                        Request = x.Request,
                        RequestStatus = x.RequestStatus,
                        ContractNo = x.ContractNo,
                        Customer = x.Customer,
                        LOINo = x.LOINo,
                        LOIDate = x.LOIDate,
                        CustomerOrderNo = x.CustomerOrderNo,
                        CustomerOrderDate = x.CustomerOrderDate,
                        CDD = x.CDD,
                        ChangeNote = x.ChangeNote,
                        CreatedBy = x.CreatedBy,
                        CreatedOn = x.CreatedOn,
                        ApprovedBy = x.ApprovedBy,
                        ApprovedOn = x.ApprovedOn
                    }));
                    string str = Helper.GenerateExcel(lstclsSCAHeaderExport, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = str;
                }
               
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Data not available.";
                }
            }
            else if(gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
            {
                var lstResult = db.SP_FETCH_SCAHEADER_LINES_RESULT
                                (
                                1, int.MaxValue, strSortOrder, whereCondition
                                ).Select(x=>new { x.ProjectNo,x.Equipment,x.LOIDate,x.CDD,x.CreatedBy,x.CreatedOn})
                                .ToList();
                if(lstResult != null && lstResult.Count > 0)
                {
                    string str = Helper.GenerateExcel(lstResult, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = str;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Data not available.";
                }
            }
            else if(gridType == clsImplementationEnum.GridType.SUB_LINES.GetStringValue())
            {
                var lstResult = db.SP_FETCH_SCAHEADER_DOC_LINES_RESULT(1, int.MaxValue, strSortOrder, whereCondition).Select(x=>new {x.ROW_NO,x.Description,x.DocNo,x.DocRev,x.DesignRemarks,x.PMGApproval,x.PMGRemarks}).ToList();
                if (lstResult != null && lstResult.Count > 0)
                {
                    string str = Helper.GenerateExcel(lstResult, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = str;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Data not available.";
                }
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Data not available.";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
    }
    public class ERPLnResult
    {
        public string LOINo { get; set; }
        public DateTime? LOIDate { get; set; }
        public DateTime? CDD { get; set; }
        public string CustOrdNo { get; set; }
        public DateTime? CustOrdDate { get; set; }
        public DateTime? Zeordate { get; set; }
    }

    public class ProjectModel
    {
        public string id { get; set; }
        public string text { get; set; }
    }

    public class ResponceMsgWithData : clsHelper.ResponseMsg
    {
        public int HeaderID;
        public int Revision;
        public string Status;
    }

    public class clsSCAHeaderExport
    {
        public Nullable<long> ROW_NO { get; set; }
        public string Request { get; set; }
        public string RequestStatus { get; set; }
        public string ContractNo { get; set; }
        public string Customer { get; set; }
        public string LOINo { get; set; }
        public Nullable<System.DateTime> LOIDate { get; set; }
        public string CustomerOrderNo { get; set; }
        public Nullable<System.DateTime> CustomerOrderDate { get; set; }
        public Nullable<System.DateTime> CDD { get; set; }
        public bool ChangeNote { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string ApprovedBy { get; set; }
        public Nullable<System.DateTime> ApprovedOn { get; set; }
    }
}