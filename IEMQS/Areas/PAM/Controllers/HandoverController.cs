﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.PAM.Models;
using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.PAM.Controllers
{
    public class HandoverController : clsBase
    {
        // GET: PAM/Handover
        public ActionResult Index(int id, string type)
        {
            return RedirectToAction("HandoverReport", "Handover", new { HeaderId = id, type = type });
        }

        [SessionExpireFilter]
        public ActionResult HandoverReport(int? HeaderId, string type)
        {
            HandOverReportModel model = new HandOverReportModel();
            string HandoverType = string.Empty;
            try
            {
                var objPAM001 = db.PAM001.Where(i => i.HeaderId == HeaderId).FirstOrDefault();
                var objPAM002ZeroDate = db.PAM002.Where(P => P.HeaderId == HeaderId).ToList().Select(p => p.ZeroDate).Distinct().ToList().Select(i=>i.HasValue?i.Value.ToString("dd/MM/yyyy"):"").ToList();
                var remove = objPAM002ZeroDate.ToList<string>();
                remove.RemoveAll(i => string.IsNullOrEmpty(i));
                var Zerodates = String.Join(", ", remove.ToArray());
                var objPAM002CDD = db.PAM002.Where(P => P.HeaderId == HeaderId).ToList().Select(p => p.CDD).Distinct().ToList().Select(i => i.HasValue ? i.Value.ToString("dd/MM/yyyy") : "").ToList();
                var removeCDD = objPAM002CDD.ToList<string>();
                removeCDD.RemoveAll(i => string.IsNullOrEmpty(i));
                var CDDDates = String.Join(", ", removeCDD.ToArray());
                var userRole = objClsLoginInfo.GetUserRole();
                if (type == "1")
                {
                    HandoverType = clsImplementationEnum.HandoverReportType.Prospect.GetStringValue();
                }
                else if (type == "2")
                {
                    HandoverType = clsImplementationEnum.HandoverReportType.Firm.GetStringValue();
                }

                var objPAM008 = db.PAM008.Where(i => i.HeaderId == HeaderId && i.HandoverType == HandoverType).FirstOrDefault();
                if (objPAM008 != null)
                {
                    model.Id = objPAM008.Id;
                }
                else
                {
                    model.Id = 0;
                }
                //model.Customer = objPAM001.Customer;
                model.Customer= db.Database.SqlQuery<string>(@"SELECT  t_bpid +'-'+ t_nama   from " + LNLinkedServer + ".dbo.ttccom100175  where t_bpid = '" + objPAM001.Customer + "'").FirstOrDefault();
                model.ContractNo = objPAM001.ContractNo;
                model.ContractDescription = db.Database.SqlQuery<string>(@"SELECT t_desc FROM " + LNLinkedServer + ".dbo.ttpctm100175 where t_cono = '" + objPAM001.ContractNo + "'").FirstOrDefault();
                model.ZeroDate = Zerodates;
                model.CDD = CDDDates;
                model.InquiryNo = db.COM005.Where(i => i.t_cono == model.ContractNo).FirstOrDefault().t_enqr;
                model.CreatedBy = Manager.GetUserNameFromPsNo(objPAM001.CreatedBy);
                model.PMGResponseDate = objPAM001.PMGResponseDate != null ? objPAM001.PMGResponseDate.Value.ToString("dd/MM/yyyy") : string.Empty;
                model.PMGName = objPAM001.PMGName;
                model.CreatedOn = objPAM001.CreatedOn != null ? objPAM001.CreatedOn.Value.ToString("dd/MM/yyyy") : string.Empty;
                model.PAMNo = objPAM001.PAM;
                model.HeaderId = HeaderId.ToString();
                model.DSSId = db.PAM006.Where(i => i.HeaderId == HeaderId).FirstOrDefault().DSSId;
                //ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments1("PAM001/" + HeaderId.ToString()));

                var objPAM006 = db.PAM006.Where(i => i.HeaderId == HeaderId).FirstOrDefault();

                if (userRole.ToUpper().Contains(clsImplementationEnum.UserRoleName.MKT3.GetStringValue()) || userRole.ToLower().Contains(clsImplementationEnum.UserRoleName.MKT2.GetStringValue()))
                {
                    if (objPAM006 != null && objPAM006.Status.ToLower() == clsImplementationEnum.PAMStatus.Draft.GetStringValue().ToLower())
                    {
                        model.IsNonEditable = false;
                    }
                    else
                    {
                        model.IsNonEditable = true;
                    }
                }
                else
                {
                    model.IsNonEditable = true;
                }

                if ((objPAM001.LOINo == null || objPAM001.LOINo == string.Empty))
                {
                    model.HandoverType = clsImplementationEnum.HandoverReportType.Prospect.GetStringValue();
                    ViewBag.Title = "Handover Report – Prospect for MKT";
                    model.PageName = "Handover Report – Prospect for MKT";
                    model.Subject = "Handover of Applicable documents of Prospect Order";
                    if (objPAM008 != null)
                    {
                        model.Description = objPAM008.Description;
                    }
                    else
                    {
                        model.Description = "We are pleased to inform that we are expecting an order from " + model.Customer + " in near future for " + model.ContractDescription + ". In order to kick start design related activities in advance, we would like to handover the applicable / related project documents for your advance Information. Please note that, documents may get changed once we receive the firm purchase order which shall be communicated separately.";
                    }
                }
                else
                {
                    if (HandoverType == clsImplementationEnum.HandoverReportType.Prospect.GetStringValue())
                    {
                        model.HandoverType = clsImplementationEnum.HandoverReportType.Prospect.GetStringValue();
                        ViewBag.Title = "Handover Report – Prospect for MKT";
                        model.PageName = "Handover Report – Prospect for MKT";
                        model.Subject = "Handover of Applicable documents of Prospect Order";
                    }
                    else
                    {
                        model.HandoverType = clsImplementationEnum.HandoverReportType.Firm.GetStringValue();
                        ViewBag.Title = "Handover Report – Firm for MKT";
                        model.PageName = "Handover Report – Firm for MKT";
                        model.Subject = "Handover of Applicable documents of Firm Order";
                        model.LOINo = objPAM001.LOINo;
                        model.LOIDate = objPAM001.LOIDate != null ? objPAM001.LOIDate.Value.ToString("dd/MM/yyyy") : string.Empty;
                    }

                    if (objPAM008 != null)
                    {
                        model.Description = objPAM008.Description;
                    }
                    else
                    {
                        model.Description = "We are pleased to inform that we have received the firm order from " + model.Customer + " through LOI/PO No. " + model.LOINo + " dtd. " + model.LOIDate + " for " + model.ContractNo + ". We would like to handover the applicable / related project documents.";
                    }
                }

                return View(model);

            }
            catch (Exception)
            {

                throw;
            }
        }


        [SessionExpireFilter]
        public ActionResult PrintHandover(int? id)
        {
            HandOverReportModel model = new HandOverReportModel();
            try
            {
                var objPAM008 = db.PAM008.Where(i => i.Id == id).FirstOrDefault();
                var objPAM001 = db.PAM001.Where(i => i.HeaderId == objPAM008.HeaderId).FirstOrDefault();
                var userRole = objClsLoginInfo.GetUserRole();
                if ((objClsLoginInfo.GetUserRoleList().Contains(clsImplementationEnum.UserRoleName.ENGG3.GetStringValue()))|| (objClsLoginInfo.GetUserRoleList().Contains(clsImplementationEnum.UserRoleName.ENGG2.GetStringValue())))
                {
                    model.IsNonEditable = true;
                }

                model.Customer = objPAM001.Customer;
                model.ContractNo = objPAM001.ContractNo;
                model.ContractDescription = db.Database.SqlQuery<string>(@"SELECT t_desc FROM " + LNLinkedServer + ".dbo.ttpctm100175 where t_cono = '" + objPAM001.ContractNo + "'").FirstOrDefault();
                model.InquiryNo = db.COM005.Where(i => i.t_cono == model.ContractNo).FirstOrDefault().t_enqr;
                model.PMGResponseDate = objPAM001.PMGResponseDate != null ? objPAM001.PMGResponseDate.Value.ToString("dd/MM/yyyy") : string.Empty;
                model.PMGName = objPAM001.PMGName;
                model.CreatedOn = objPAM001.CreatedOn != null ? objPAM001.CreatedOn.Value.ToString("dd/MM/yyyy") : string.Empty;
                model.PAMNo = objPAM001.PAM;
                model.HeaderId = objPAM008.HeaderId.ToString();
                model.HandoverType = objPAM008.HandoverType;
                //ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments1("PAM001/" + HeaderId.ToString()));
                if (objPAM008.HandoverType == clsImplementationEnum.HandoverReportType.Prospect.GetStringValue())
                {
                    ViewBag.Title = "Handover Report – Prospect for Print";
                    model.PageName = "Handover Report – Prospect for Print";
                    model.Subject = "Handover of Applicable documents of Prospect Order";
                    model.Description = "We are pleased to inform that we are expecting an order from " + model.Customer + " in near future for " + model.ContractDescription + ". In order to kick start design related activities in advance, we would like to handover the applicable / related project documents for your advance Information. Please note that, documents may get changed once we receive the firm purchase order which shall be communicated separately.";
                    model.CreatedBy = Manager.GetUserNameFromPsNo(objPAM001.CreatedBy);
                }
                else
                {
                    ViewBag.Title = "Handover Report – Firm for Print";
                    model.PageName = "Handover Report – Firm for Print";
                    model.Subject = "Handover of Applicable documents of Firm Order";
                    model.LOINo = objPAM001.LOINo;
                    model.LOIDate = objPAM001.LOIDate != null ? objPAM001.LOIDate.Value.ToString("dd/MM/yyyy") : string.Empty;
                    model.CreatedBy = Manager.GetUserNameFromPsNo(objPAM001.CreatedBy);
                }

                return View(model);

            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult loadProjectDataTable(JQueryDataTableParamModel param, int headerId)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "HeaderId = " + headerId + "";
                string[] columnName = { "PAM", "Project", "ProjectDescription" };
                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                var lstPamProject = db.SP_PAM_PROJECT_CONTRACTWISE(StartIndex, EndIndex, "", whereCondition).ToList();
                int? totalRecords = lstPamProject.Count();
                var res = from h in lstPamProject
                          select new[] {
                           Convert.ToString(h.Project),
                           Convert.ToString(h.ProjectDescription),
                           Convert.ToDateTime(h.LOIDate).ToString("dd/MM/yyyy"),
                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        //public ActionResult Save(PAM008 pam008, string ContractNo, bool hasAttachments, Dictionary<string, string> Attach)
        public ActionResult Save(PAM008 pam008, string ContractNo)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsgWithStatus = new clsHelper.ResponseMsgWithStatus();
            PAM008 objPAM008 = new PAM008();
            try
            {
                if (pam008.Id > 0)
                {
                    objPAM008 = db.PAM008.Where(i => i.Id == pam008.Id).FirstOrDefault();
                    //For Edit Mode
                    objPAM008.Description = pam008.Description;
                    objPAM008.EditedBy = objClsLoginInfo.UserName;
                    objPAM008.EditedOn = DateTime.Now;
                    db.SaveChanges();

                    var folderPath = "PAM008/" + objPAM008.Id;
                    //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                    objResponseMsgWithStatus.HeaderId = objPAM008.Id;
                    objResponseMsgWithStatus.Key = true;
                    objResponseMsgWithStatus.Value = "Updated successfully";
                }
                else
                {
                    //Add New Record
                    objPAM008.CreatedBy = objClsLoginInfo.UserName;
                    objPAM008.CreatedOn = DateTime.Now;
                    objPAM008.Description = pam008.Description;
                    objPAM008.HandoverType = pam008.HandoverType;
                    objPAM008.HeaderId = pam008.HeaderId;
                    objPAM008.HandoverReport = MakeHandoverReport(ContractNo);

                    db.PAM008.Add(objPAM008);
                    db.SaveChanges();
                    var folderPath = "PAM008/" + objPAM008.Id;
                    //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                    objResponseMsgWithStatus.HeaderId = objPAM008.Id;
                    objResponseMsgWithStatus.Key = true;
                    objResponseMsgWithStatus.Value = "Save successfully";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsgWithStatus.Key = false;
                objResponseMsgWithStatus.Value = ex.Message;
            }

            return Json(objResponseMsgWithStatus);
        }


        public string MakeHandoverReport(string ContractNo)
        {
            var HandoverReport = string.Empty;
            var mtNumber = "1";
            var objPAM008 = db.PAM008.Where(i => i.HandoverReport.Contains(ContractNo + "/HR")).ToList();
            if (objPAM008.Count > 0)
            {
                int MaxNo = 0;
                foreach (var item in objPAM008)
                {
                    string SplitId = item.HandoverReport.Split('/').Last();
                    if (MaxNo < Convert.ToInt32(SplitId))
                    {
                        MaxNo = Convert.ToInt32(SplitId);
                    }
                }
                mtNumber = (MaxNo + 1).ToString();
            }
            HandoverReport = ContractNo + "/HR/" + mtNumber.PadLeft(3, '0');
            return HandoverReport;
        }

        public class HandOverReportModel
        {
            public string PAMNo { get; set; }
            public string Date { get; set; }
            public string ZeroDate { get; set; }
            public string CDD { get; set; }
            public string Customer { get; set; }
            public string ContractNo { get; set; }
            public string ContractDescription { get; set; }
            public string InquiryNo { get; set; }
            public string LOINo { get; set; }
            public string LOIDate { get; set; }
            public string CreatedBy { get; set; }
            public string CreatedOn { get; set; }
            public string HandoverReport { get; set; }
            public string HandoverType { get; set; }
            public string Description { get; set; }
            public int DSSId { get; set; }
            public string PMGName { get; set; }
            public string PMGResponseDate { get; set; }
            public string PageName { get; set; }
            public string HeaderId { get; set; }
            public string Subject { get; set; }
            public bool IsNonEditable { get; set; }
            public int Id { get; set; }
        }
    }
}