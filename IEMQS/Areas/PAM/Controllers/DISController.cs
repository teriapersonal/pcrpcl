﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.PAM.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
namespace IEMQS.Areas.PAM.Controllers
{
    public class DISController : clsBase
    {
        #region Utility
        public static string GenerateTextboxFor(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string maxLength = "", bool disabled = false)
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (columnName.ToLower() == "pages")
            {
                className += " numeric";
            }
            htmlControl = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id=" + inputID + " data-lineid='" + rowId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + " maxlength='" + maxLength + "' " + (disabled ? "disabled" : "") + "  />";

            return htmlControl;
        }

        public static string GenerateTextAreaFor(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string maxLength = "", bool disabled = false)
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (columnName.ToLower() == "pages")
            {
                className += " numeric";
            }
            htmlControl = "<textarea " + (isReadOnly ? "readonly='readonly'" : "") + " id=" + inputID + " data-lineid='" + rowId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + " maxlength='" + maxLength + "' " + (disabled ? "disabled" : "") + "' >" + inputValue + "</textarea>";

            return htmlControl;
        }
        public static string GenerateGridButton(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";


            htmlControl = "<a id='" + inputID + "' name='" + inputID + "' class='btn btn-outline btn-circle btn-sm blue' " + onClickEvent + " ><i class='" + className + "'></i> " + buttonName + "</a>";

            //htmlControl = "<i  data-modal='' id='" + inputID + "' name='" + inputID + "' style='cursor:Pointer;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";

            return htmlControl;
        }
        public string GetChecklistCheckStyle(int LineId, string status, bool? isCheck, bool? isDisabled)
        {
            string rtnchecjbox = "";
            if (isCheck == null)
                isCheck = false;
            rtnchecjbox = "<input type='checkbox' style='text-align:center' name='chkChecklistCheck" + LineId.ToString() + "' id='chkChecklistCheck" + LineId.ToString() + "' class='make-switch col-md-3' data-on-text='Yes' data-off-text='No' data-on-color='success' data-off-color='danger' data-size='normal' onchange='UpdateAccepted(this," + LineId.ToString() + ")' ";
            if (isDisabled == true)
            {
                rtnchecjbox += " disabled= 'disabled' ";
            }
            if (isCheck == true)
            {
                rtnchecjbox += "checked='checked' ";
            }
            //if (status.ToLower() == clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue().ToLower())
            //{
            //    if (isCheck == true)
            //    {
            //        rtnchecjbox += " checked='checked' ";
            //    }
            //}
            //else
            //{
            //    rtnchecjbox += " disabled= 'disabled' ";
            //}
            rtnchecjbox += "/>";
            return rtnchecjbox;
        }
        public string GetSectionRemarkStyle(int LineId, string status, string sectionRemarks)
        {
            string rtnremarks = "";
            rtnremarks += "<input type='text' id='txtSectionRemark" + LineId.ToString() + "' name='txtSectionRemark" + LineId.ToString() + "' style='width:100%'  class='form-control input-sm  input-inline sectionTextbox' value='" + sectionRemarks + "' onblur='UpdateRemarks(this," + LineId.ToString() + ")' ";
            if (status.ToLower() != clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue().ToLower())
            {
                rtnremarks += " disabled='disabled' ";
            }
            rtnremarks += " />";
            return rtnremarks;
        }
        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (!string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
            {
                if (string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue()))
                {
                    htmlControl = "";// "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }
                else
                {
                    htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }
            }

            return htmlControl;
        }
        #endregion

        // GET: PAM/DIS
        #region DIS Maintain
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public bool CheckDISStatus(int HeaderId)
        {
            PAM011 objPAM011 = db.PAM011.Where(x => x.PAMHeaderId == HeaderId).FirstOrDefault();
            if (objPAM011 == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        [SessionExpireFilter]
        public ActionResult GetDISGridDataPartial(string status, string ForApprove)
        {
            ViewBag.Status = status;
            ViewBag.ForApprove = ForApprove;
            return PartialView("_GetDISGridDataPartial");
        }

        public ActionResult loadDISDataTable(JQueryDataTableParamModel param, string status, string ForApprove)
        {
            try
            {

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                //whereCondition += " and loidate.t_csbu collate SQL_Latin1_General_CP850_CI_AI in (select * from dbo.fn_split(dbo.GETBUFROMUSER('" + objClsLoginInfo.UserName + "'),',')) ";
                if (Convert.ToBoolean(ForApprove))
                {
                    if (status.ToUpper() == "PENDING")
                    {
                        whereCondition += " and status in ('" + clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue() + "')";
                    }
                    else
                    {
                        whereCondition += " and status in ('" + clsImplementationEnum.PAMStatus.Approved.GetStringValue() + "','" + clsImplementationEnum.PAMStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.IMBStatus.Returned.GetStringValue() + "','" + clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue() + "')";
                    }
                }
                else
                {
                    if (status.ToUpper() == "PENDING")
                    {
                        whereCondition += " and status in ('" + clsImplementationEnum.PAMStatus.Returned.GetStringValue() + "','" + clsImplementationEnum.PAMStatus.Draft.GetStringValue() + "')";
                    }
                    else
                    {
                        whereCondition += " and status in ('" + clsImplementationEnum.PAMStatus.Approved.GetStringValue() + "','" + clsImplementationEnum.PAMStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.IMBStatus.Returned.GetStringValue() + "','" + clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue() + "')";
                    }
                }
                string[] columnName = { "PAM", "DISNo", "IssueNo", "Status", "DesignCode", "ASME", "Remarks" };

                // whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstPam = db.SP_DIS_GETDISDETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from h in lstPam
                           select new[] {
                               Convert.ToString(h.PAM),
                               Convert.ToString(h.DISNo),
                               Convert.ToString(h.IssueNo),
                               Convert.ToString(h.Status),
                               Convert.ToString(h.Remarks),
                               "", //Action
                               Convert.ToString(h.HeaderId),
                               Convert.ToString(h.PAMHeaderId),

                    }).ToList();
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [SessionExpireFilter]
        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.TimelineTitle = "DIS History";
            model.Title = "DIS";
            PAM011 objPAM011 = db.PAM011.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            model.CreatedBy = Manager.GetUserNameFromPsNo(objPAM011.CreatedBy);
            model.CreatedOn = objPAM011.CreatedOn;
            model.ReturnedBy = Manager.GetUserNameFromPsNo(objPAM011.ReturnedBy);
            model.ReturnedOn = objPAM011.ReturnedOn;
            model.ApprovedBy = Manager.GetUserNameFromPsNo(objPAM011.ApprovedBy);
            model.ApprovedOn = objPAM011.ApprovedOn;


            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }

        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult DISHeaderDetails(int id)
        {
            PAM011 objPAM011 = db.PAM011.Where(x => x.HeaderId == id).FirstOrDefault();

            PAM001 objPAM001 = db.PAM001.FirstOrDefault(x => x.HeaderId == objPAM011.PAMHeaderId);

            var BU = db.COM008.Where(x => x.t_cono == objPAM001.ContractNo).Select(x => x.t_csbu).FirstOrDefault();
            BU = BU != null ? BU : "";
            ViewBag.BU = BU;
            List<GLB002> objGLB002 = Manager.GetSubCatagories("DesignCode", BU, objClsLoginInfo.Location).ToList();
            if (objGLB002.Count > 0)
            {
                ViewBag.lstDesignCode = objGLB002.Select(i => new { Code = i.Code, Description = i.Description }).Distinct().Select(x => new BULocWiseCategoryModel { CatDesc = (x.Code + "-" + x.Description), CatID = x.Code }).ToList();
                var lstDesignCode = objGLB002.Where(x => x.Code == objPAM011.DesignCode).FirstOrDefault();
                if (lstDesignCode != null)
                {
                    ViewBag.DesignCode = lstDesignCode.Code;
                }
            }
            return View("CreateDis", objPAM011);
        }
        public JsonResult GetDesignCode(string search, string bu)
        {
            try
            {
                List<GLB002> objGLB002 = Manager.GetSubCatagories("DesignCode", bu, objClsLoginInfo.Location).ToList();
                var item = objGLB002.Select(i => new { Code = i.Code, Description = i.Description }).Distinct()
                           .Select(x => new { text = (x.Code + "-" + x.Description), id = x.Code }).ToList();

                return Json(item, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        [SessionExpireFilter]
        public ActionResult CreateDis(int DSSId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            PAM011 objPAM011 = null;
            try
            {
                PAM006 objPAM006 = db.PAM006.Where(x => x.DSSId == DSSId).FirstOrDefault();
                objPAM011 = db.PAM011.Where(x => x.PAMHeaderId == objPAM006.HeaderId).FirstOrDefault();

                PAM001 objPAM001 = db.PAM001.FirstOrDefault(x => x.HeaderId == objPAM006.HeaderId);

                var BU = db.COM008.Where(x => x.t_cono == objPAM001.ContractNo).Select(x => x.t_csbu).FirstOrDefault();
                BU = BU != null ? BU : "";
                List<GLB002> objGLB002 = Manager.GetSubCatagories("DesignCode", BU, objClsLoginInfo.Location).ToList();

                if (objGLB002.Count > 0)
                    ViewBag.lstDesignCode = objGLB002.Select(i => new { Code = i.Code, Description = i.Description }).Distinct().Select(x => new BULocWiseCategoryModel { CatDesc = (x.Code + "-" + x.Description), CatID = x.Code }).ToList();


                if (objPAM011 != null)
                {
                    ViewBag.BU = BU;
                    if (objGLB002.Count > 0)
                        objPAM011.DesignCode = objGLB002.Where(x => x.Code == objPAM011.DesignCode).Select(x => new { designcode = x.Code + "-" + x.Description }).FirstOrDefault().designcode;


                    return View(objPAM011);
                }
                else
                {
                    objPAM011 = new PAM011();
                }

                objPAM011.PAMHeaderId = objPAM006.HeaderId;
                objPAM011.PAM = objPAM006.PAM;
                objPAM011.DISNo = objPAM006.DSSNo.Replace("DSS", "DIS");
                objPAM011.IssueNo = 1;
                objPAM011.Status = clsImplementationEnum.PAMStatus.Draft.GetStringValue();
                objPAM011.CreatedBy = objClsLoginInfo.UserName;
                objPAM011.CreatedOn = DateTime.Now;
                objPAM011.ASME = false;
                db.PAM011.Add(objPAM011);
                db.SaveChanges();

                if (objPAM006.PAM007.Count > 0)
                {
                    string ContractNo = db.PAM001.Where(x => x.HeaderId == objPAM011.PAMHeaderId).Select(x => x.ContractNo).FirstOrDefault();
                    var sourcePath = "PAM007/ContractWiseDocuments/" + ContractNo;
                    //var existing = (new clsFileUpload()).GetDocuments(sourcePath);
                    FileUploadController _objFUC = new FileUploadController();
                    var existing = _objFUC.GetAllDocumentsByTableNameTableId("PAM007/" + ContractNo, 1);

                    foreach (var item in objPAM006.PAM007)
                    {
                        int n = 0;
                        bool isNumeric = int.TryParse(item.Pages, out n);
                        if (!isNumeric)
                            item.Pages = string.Empty;


                        PAM012 objPAM012 = new PAM012
                        {
                            HeaderId = objPAM011.HeaderId,
                            DISNo = objPAM011.DISNo,
                            DISIssueNo = objPAM011.IssueNo,
                            Description = item.Description,
                            DocNo = item.DocNo,
                            DocRev = item.DocRev,
                            Pages = item.Pages,
                            IncludeInDIS = item.IsAccepted == true ? true : false,
                            CreatedBy = objClsLoginInfo.UserName,
                            CreatedOn = DateTime.Now
                        };
                        db.PAM012.Add(objPAM012);
                        db.SaveChanges();

                        var destinationPath = "PAM012/" + objPAM012.LineId;
                        var objIsFileExist = existing.Where(k => Path.GetFileNameWithoutExtension(k.Document_name) == item.DocNo).FirstOrDefault();
                        if (objIsFileExist != null)
                        {
                            //clsUpload.CopyFolderContents(folderPath, destinationPath);
                            //(new clsFileUpload()).CopyFolderContentsAsync(sourcePath, destinationPath, objIsFileExist.Name);

                            _objFUC.CopyDataOnFCSServerAsync(sourcePath, destinationPath, "PAM007/"+ ContractNo, 1, "PAM012", objPAM012.LineId, IEMQS.DESServices.CommonService.GetUseIPConfig, IEMQS.DESServices.CommonService.objClsLoginInfo.UserName, objIsFileExist.DocumentMappingId,true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return View(objPAM011);
        }
        [SessionExpireFilter]
        public ActionResult GetDISHeaderDetails(int DisId, int PAMHeaderId)
        {
            DisHeaderDtl header = new DisHeaderDtl();

            header.HeaderId = DisId.ToString();
            header.PAMHeaderId = PAMHeaderId.ToString();

            PAM001 objPAM001 = db.PAM001.FirstOrDefault(x => x.HeaderId == PAMHeaderId);

            header.Contract = db.Database.SqlQuery<string>(@"SELECT t_cono +'-'+ t_desc FROM " + LNLinkedServer + ".dbo.ttpctm100175 where t_cono = '" + objPAM001.ContractNo + "'").FirstOrDefault();
            header.Customer = db.Database.SqlQuery<string>(@"SELECT  t_bpid +'-'+ t_nama   from " + LNLinkedServer + ".dbo.ttccom100175  where t_bpid = '" + objPAM001.Customer + "'").FirstOrDefault();
            header.ContractNo = objPAM001.ContractNo;

            PAM011 objPAM011 = db.PAM011.FirstOrDefault(x => x.HeaderId == DisId);
            header.Status = objPAM011.Status;
            header.DISNo = objPAM011.DISNo;
            header.RevNo = objPAM011.IssueNo.ToString();
            header.CreatedOn = Convert.ToString(objPAM011.CreatedOn.Value != null ? objPAM011.CreatedOn.Value.ToString("dd/MM/yyyy") : string.Empty);

            return PartialView("~/Areas/PAM/Views/Shared/_DISHeaderDetails.cshtml", header);
        }

        public ActionResult GetDISLinesDetails(int DisId, string ForApprove, string Status)
        {
            ViewBag.DisId = DisId.ToString();
            ViewBag.Status = Status;
            if (ForApprove == null)
            {
                ViewBag.ForApprove = "false";
            }
            else
            {
                ViewBag.ForApprove = ForApprove;
            }
            return PartialView("~/Areas/PAM/Views/DIS/_GetDISLinesGridDataPartial.cshtml");
        }

        public ActionResult loadDISLineDataTable(JQueryDataTableParamModel param, int DisId, string ForApprove)
        {
            try
            {

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                ViewBag.ForApprove = ForApprove;
                PAM011 objPAM011 = db.PAM011.Where(i => i.HeaderId == DisId).FirstOrDefault();
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                bool isEditable = true;
                string whereCondition = "1=1";
                whereCondition += " and HeaderId= " + DisId.ToString();

                if (objPAM011.Status.ToLower() == clsImplementationEnum.PAMStatus.Approved.GetStringValue().ToLower() || objPAM011.Status.ToLower() == clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue().ToLower())
                {
                    isEditable = false;
                }

                if (objPAM011.Status.ToLower() == clsImplementationEnum.PAMStatus.Approved.GetStringValue().ToLower())
                {
                    //whereCondition += " and IncludeInDIS = 1"; //change on 08-1-2018 observation ID 14285
                }

                string[] columnName = { "Description", "DocNo", "DocRev", "Pages" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstPam = db.SP_DIS_GETDISLINEDETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                FileUploadController _objFUC = new FileUploadController();
                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();
                if (ForApprove == null || ForApprove.ToLower() == "false")
                {
                    int newRecordId = 0;
                    var newRecord = new[] {
                                    "0",
                                    "0",
                                    Helper.GenerateHidden(newRecordId, "HeaderId", Convert.ToString(DisId)),
                                    isEditable == true ? GenerateTextAreaFor(newRecordId,"Description","","","",false,"","200",false) : GenerateTextboxFor(newRecordId,"Description","","","",false,"","200",true),
                                    isEditable == true ? GenerateTextboxFor(newRecordId,"DocNo","","","",false,"","30",false) : GenerateTextboxFor(newRecordId,"DocNo","","","",false,"","30",true),
                                    isEditable == true ? GenerateTextboxFor(newRecordId,"DocRev","R0","","",false,"","15",false) : GenerateTextboxFor(newRecordId,"DocRev","R0","","",false,"","5",true),
                                    isEditable == true ? GenerateTextboxFor(newRecordId,"Pages","","","",false,"","500",false) : GenerateTextboxFor(newRecordId,"Pages","","","",false,"","500",true),
                                    "",
                                    isEditable == true ? GetChecklistCheckStyle(newRecordId,objPAM011.Status,true, false) : GetChecklistCheckStyle(newRecordId,objPAM011.Status,true, true),
                                    isEditable == true ? GenerateTextboxFor(newRecordId,"ReturnRemarks","","","",false,"","500",false) : GenerateTextboxFor(newRecordId,"ReturnRemarks","","","",false,"","500",true),
                                    isEditable == true ? GenerateGridButton(newRecordId, "Add", "Add Rocord", "fa fa-plus", "SaveLineRecord(0);" ) : "",
                                    "false"
                                };

                    var res = (from h in lstPam
                               select new[] {
                                    Convert.ToString(h.ROW_NO),
                                    Convert.ToString(h.LineId),
                                    Helper.GenerateHidden(h.LineId, "HeaderId", Convert.ToString(h.HeaderId)),
                                    isEditable == true ? GenerateTextAreaFor(h.LineId,"Description",h.Description,"","",false,"","200",false) : GenerateTextboxFor(h.LineId,"Description",h.Description,"","",false,"","200",true),
                                    isEditable == true ? GenerateTextboxFor(h.LineId,"DocNo",h.DocNo,"","",false,"","30",false) : GenerateTextboxFor(h.LineId,"DocNo",h.DocNo,"","",false,"","30",true),
                                    isEditable == true ? GenerateTextboxFor(h.LineId,"DocRev",h.DocRev,"","",false,"","15",false) : GenerateTextboxFor(h.LineId,"DocRev",h.DocRev,"","",false,"","5",true),
                                    isEditable == true ? GenerateTextboxFor(h.LineId,"Pages",h.Pages,"","",false,"","500",false) : GenerateTextboxFor(h.LineId,"Pages",h.Pages,"","",false,"","500",true),
                                    "",
                                    isEditable == true ? GetChecklistCheckStyle(h.LineId,objPAM011.Status,h.IncludeInDIS, false) : GetChecklistCheckStyle(h.LineId,objPAM011.Status,h.IncludeInDIS, true),
                                    isEditable == true ? GenerateTextboxFor(h.LineId,"ReturnRemarks",h.ReturnRemarks,"","",false,"","500",false) : GenerateTextboxFor(h.LineId,"ReturnRemarks",h.ReturnRemarks,"","",false,"","500",true),
                                    isEditable == true ? HTMLActionString(h.LineId,"","Edit","Edit Record","fa fa-pencil-square-o","SaveLineRecord("+h.LineId+");") : "",
                                    _objFUC.CheckAnyDocumentsExits("PAM012",h.LineId).ToString()
                                    }).ToList();
                    //Helper.CheckAttachmentUpload("PAM012/"+h.LineId).ToString()
                    res.Insert(0, newRecord);
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                        whereCondition = whereCondition,
                        strSortOrder = strSortOrder,
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var res = (from h in lstPam
                               select new[] {
                                    Convert.ToString(h.ROW_NO),
                                    Convert.ToString(h.LineId),
                                    Helper.GenerateHidden(h.LineId, "HeaderId", Convert.ToString(h.HeaderId)),
                                    GenerateTextAreaFor(h.LineId,"Description",h.Description,"","",false,"","200",true),
                                    GenerateTextboxFor(h.LineId,"DocNo",h.DocNo,"","",false,"","30",true),
                                    GenerateTextboxFor(h.LineId,"DocRev",h.DocRev,"","",false,"","15",true),
                                    GenerateTextboxFor(h.LineId,"Pages",h.Pages,"","",false,"","500",true),
                                    "",
                                    GetChecklistCheckStyle(h.LineId,objPAM011.Status,h.IncludeInDIS, true),
                                    GenerateTextboxFor(h.LineId,"ReturnRemarks",h.ReturnRemarks,"","",false,"","500", true),
                                    _objFUC.CheckAnyDocumentsExits("PAM012",h.LineId).ToString()

                    }).ToList();
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                        whereCondition = whereCondition,
                        strSortOrder = strSortOrder,
                    }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveHeader(FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var HeaderId = Convert.ToInt32(fc["HeaderId"]);
                PAM011 objPAM011 = db.PAM011.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                objPAM011.DesignCode = (fc["DesignCode"]).Split('-')[0];
                objPAM011.ASME = Convert.ToBoolean(fc["ASME"]);
                objPAM011.Remarks = fc["Remarks"];
                objPAM011.ReasonOfIssue = fc["ReasonOfIssue"];

                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SaveLines(FormCollection fc, int Id = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int LineId = Id;
                int HeaderId = Convert.ToInt32(fc["HeaderId" + Id]);
                PAM011 objPAM011 = db.PAM011.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                PAM012 objPAM012 = null;
                if (LineId > 0)
                {
                    objPAM012 = db.PAM012.Where(x => x.LineId == LineId).FirstOrDefault();
                }
                else
                {
                    objPAM012 = new PAM012();
                    objPAM012.IncludeInDIS = true;
                }
                objPAM012.HeaderId = objPAM011.HeaderId;
                objPAM012.DISNo = objPAM011.DISNo;
                objPAM012.DISIssueNo = objPAM011.IssueNo;
                objPAM012.Description = fc["Description" + LineId];
                objPAM012.DocNo = fc["DocNo" + LineId];
                objPAM012.DocRev = fc["DocRev" + LineId];
                objPAM012.Pages = fc["Pages" + LineId];

                int n = 0;
                bool isNumeric = int.TryParse(objPAM012.Pages, out n);
                if (!isNumeric)
                    objPAM012.Pages = string.Empty;

                //objPAM012.IncludeInDIS = fc["IncludeInDIS" + LineId];
                objPAM012.ReturnRemarks = fc["ReturnRemarks" + LineId];
                if (LineId > 0)
                {
                    objPAM012.EditBy = objClsLoginInfo.UserName;
                    objPAM012.EditedOn = DateTime.Now;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                }
                else
                {
                    objPAM012.CreatedBy = objClsLoginInfo.UserName;
                    objPAM012.CreatedOn = DateTime.Now;
                    db.PAM012.Add(objPAM012);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                }
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateAccepted(int LineId, bool IncludeInDIS)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PAM012 objPAM012 = db.PAM012.Where(x => x.LineId == LineId).FirstOrDefault();
                objPAM012.IncludeInDIS = IncludeInDIS;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg);
        }
        [SessionExpireFilter]
        public ActionResult DISAttachmentPartial(int? id, string status = "", string ForApprover = "")
        {
            ViewBag.Status = status;
            ViewBag.ForApprover = ForApprover;
            PAM012 objPAM012 = db.PAM012.Where(x => x.LineId == id).FirstOrDefault();
            return PartialView("_DISAttachmentPartial", objPAM012);
        }

        [HttpPost]
        public ActionResult SaveDISAttachment(string id, bool hasAttachments, Dictionary<string, string> Attach)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var folderPath = "PAM012/" + id;
                Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                objResponseMsg.Key = true;
                objResponseMsg.Value = "File Uploaded Successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreateRevision(int DisId = 0, string reason = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (DisId > 0)
                {
                    PAM011 objPAM011 = db.PAM011.Where(x => x.HeaderId == DisId).FirstOrDefault();
                    objPAM011.IssueNo += 1;
                    objPAM011.ReasonOfIssue = reason;
                    objPAM011.Status = clsImplementationEnum.PAMStatus.Draft.GetStringValue();
                    objPAM011.CreatedBy = objClsLoginInfo.UserName;
                    objPAM011.ReturnRemarks = string.Empty;
                    objPAM011.CreatedOn = DateTime.Now;
                    objPAM011.ApprovedBy = null;
                    objPAM011.ApprovedOn = null;
                    objPAM011.ReturnedBy = null;
                    objPAM011.ReturnedOn = null;

                    List<PAM012> objPAM012 = db.PAM012.Where(x => x.HeaderId == objPAM011.HeaderId).ToList();

                    //Change By Ajay Chauhan On 8-1-2018 Observation ID 14285
                    //IEnumerable<PAM012> resultObj = objPAM012.Where(x => !x.IncludeInDIS);
                    //db.PAM012.RemoveRange(resultObj);

                    objPAM012.Select(x => { x.DISIssueNo = objPAM011.IssueNo; return x; }).ToList();
                    objPAM011.IsSINCreated = false;
                    db.SaveChanges();


                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revision.ToString();
                    objResponseMsg.HeaderId = objPAM011.HeaderId;
                    objResponseMsg.Status = objPAM011.Status;
                    objResponseMsg.RevNo = Convert.ToString(objPAM011.IssueNo);
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        #region History
        [HttpPost]
        public ActionResult GetHistoryDataPartial(int HeaderID, string isApprover = "false")
        {
            ViewBag.HeaderID = HeaderID;
            ViewBag.approver = isApprover;
            return PartialView("_GetHistoryDataPartial");
        }

        [HttpPost]
        public JsonResult LoadDISHeaderHistoryData(JQueryDataTableParamModel param, string isApprover = "false")
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();

                string[] columnName = { "PAM", "DISNo", "IssueNo", "Status", "DesignCode", "ASME", "Remarks" };

                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_DIS_GETHEADER_HISTORY
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.Id),
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.PAM),
                               Convert.ToString(uc.DISNo),
                               Convert.ToString(uc.IssueNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn !=null?(Convert.ToDateTime(uc.CreatedOn)).ToString("dd/MM/yyyy"):""),
                               "<center><a title='View' href='"+WebsiteURL+"/PAM/DIS/GetDISHistoryDetails?ID="+Convert.ToInt32(uc.Id)+"&approver="+isApprover+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a>&nbsp;&nbsp;<i style='margin-left:5px;cursor:pointer;' title='Print Report' class='fa fa-print' onClick='PrintReport(" + Convert.ToInt32(uc.Id) + ")'></i></center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]
        public ActionResult GetDISHeaderHistoryDetails(int Id, int PAMHeaderId)
        {
            DisHeaderDtl header = new DisHeaderDtl();

            header.HeaderId = Id.ToString();
            header.PAMHeaderId = PAMHeaderId.ToString();

            PAM001 objPAM001 = db.PAM001.FirstOrDefault(x => x.HeaderId == PAMHeaderId);

            header.Contract = db.Database.SqlQuery<string>(@"SELECT t_cono +'-'+ t_desc FROM " + LNLinkedServer + ".dbo.ttpctm100175 where t_cono = '" + objPAM001.ContractNo + "'").FirstOrDefault();
            header.Customer = db.Database.SqlQuery<string>(@"SELECT  t_bpid +'-'+ t_nama   from " + LNLinkedServer + ".dbo.ttccom100175  where t_bpid = '" + objPAM001.Customer + "'").FirstOrDefault();
            header.ContractNo = objPAM001.ContractNo;

            PAM011_Log objPAM011_Log = db.PAM011_Log.FirstOrDefault(x => x.Id == Id);
            header.Status = objPAM011_Log.Status;
            header.DISNo = objPAM011_Log.DISNo;
            header.RevNo = objPAM011_Log.IssueNo.ToString();
            header.CreatedOn = Convert.ToString(objPAM011_Log.CreatedOn.Value != null ? objPAM011_Log.CreatedOn.Value.ToString("dd/MM/yyyy") : string.Empty);

            return PartialView("~/Areas/PAM/Views/Shared/_DISHeaderHistoryDetails.cshtml", header);
        }

        [SessionExpireFilter]
        public ActionResult GetDISHistoryDetails(int ID = 0, string approver = "false")
        {
            ViewBag.approver = approver;
            PAM011_Log objPAM011_Log = db.PAM011_Log.Where(x => x.Id == ID).FirstOrDefault();

            PAM001 objPAM001 = db.PAM001.FirstOrDefault(x => x.HeaderId == objPAM011_Log.PAMHeaderId);

            var BU = db.COM008.Where(x => x.t_cono == objPAM001.ContractNo).Select(x => x.t_csbu).FirstOrDefault();
            List<GLB002> objGLB002 = Manager.GetSubCatagories("DesignCode", BU, objClsLoginInfo.Location).ToList();
            if (objGLB002.Count > 0)
            {
                var lstDesignCode = objGLB002.Where(x => x.Code == objPAM011_Log.DesignCode).FirstOrDefault();
                if (lstDesignCode != null)
                {
                    ViewBag.DesignCode = lstDesignCode.Code + "-" + lstDesignCode.Description;
                }
            }
            return View(objPAM011_Log);
        }

        public ActionResult GetDISLinesHistoryDetails(int RefId, string ForApprove, string Status)
        {
            ViewBag.RefId = RefId.ToString();
            ViewBag.Status = Status;
            if (ForApprove == null)
            {
                ViewBag.ForApprove = "false";
            }
            else
            {
                ViewBag.ForApprove = ForApprove;
            }
            return PartialView("~/Areas/PAM/Views/DIS/_GetDISLinesHistoryGridDataPartial.cshtml");
        }

        public ActionResult loadDISLineHistoryDataTable(JQueryDataTableParamModel param, int RefId, string ForApprove)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                ViewBag.ForApprove = ForApprove;

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                whereCondition += " and RefId= " + RefId.ToString();

                string[] columnName = { "Description", "DocNo", "DocRev", "Pages" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstPam = db.SP_GET_DIS_LINES_HISTORY(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();
                if (ForApprove == null || ForApprove.ToLower() == "false")
                {
                    var res = (from h in lstPam
                               select new[] {
                                    Convert.ToString(h.ROW_NO),
                                    Convert.ToString(h.LineId),
                                    Helper.GenerateHidden(h.LineId, "HeaderId", Convert.ToString(h.HeaderId)),
                                    GenerateTextAreaFor(h.LineId,"Description",h.Description,"","",false,"","200",true),
                                    GenerateTextboxFor(h.LineId,"DocNo",h.DocNo,"","",false,"","30",true),
                                    GenerateTextboxFor(h.LineId,"DocRev",h.DocRev,"","",false,"","5",true),
                                    GenerateTextboxFor(h.LineId,"Pages",h.Pages,"","",false,"","500",true),
                                    "",
                                    GetChecklistCheckStyle(h.LineId,"",h.IncludeInDIS, true),
                                    GenerateTextboxFor(h.LineId,"ReturnRemarks",h.ReturnRemarks,"","",false,"","500",true),
                                    }).ToList();

                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                        whereCondition = whereCondition,
                        strSortOrder = strSortOrder,
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var res = (from h in lstPam
                               select new[] {
                                   Convert.ToString(h.ROW_NO),
                                    Convert.ToString(h.LineId),
                                    Helper.GenerateHidden(h.LineId, "HeaderId", Convert.ToString(h.HeaderId)),
                                    GenerateTextAreaFor(h.LineId,"Description",h.Description,"","",false,"","200",true),
                                    GenerateTextboxFor(h.LineId,"DocNo",h.DocNo,"","",false,"","30",true),
                                    GenerateTextboxFor(h.LineId,"DocRev",h.DocRev,"","",false,"","5",true),
                                    GenerateTextboxFor(h.LineId,"Pages",h.Pages,"","",false,"","500",true),
                                    "",
                                    GetChecklistCheckStyle(h.LineId,"",h.IncludeInDIS, true),
                                    GenerateTextboxFor(h.LineId,"ReturnRemarks",h.ReturnRemarks,"","",false,"","500", true),

                    }).ToList();
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                        whereCondition = whereCondition,
                        strSortOrder = strSortOrder,
                    }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #endregion

        #region DIS Approver
        public ActionResult SendForApproval(int DisId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            bool IsFileAttach = true;
            PAM011 objPAM011 = new PAM011();
            try
            {
                if (DisId > 0)
                {
                    objPAM011 = db.PAM011.Where(i => i.HeaderId == DisId).FirstOrDefault();
                    if (objPAM011 != null)
                    {
                        foreach (var item in objPAM011.PAM012)
                        {
                            //var folderPath = "PAM012/" + item.LineId;

                            //var existing = (new clsFileUpload()).GetDocuments(folderPath);

                            FileUploadController _obj = new FileUploadController();
                            if (!_obj.CheckAnyDocumentsExits("PAM012", item.LineId))
                            {
                                IsFileAttach = false;
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = "All line must have attachment!";
                                break;
                            }
                        }
                        if (IsFileAttach)
                        {
                            objPAM011.Status = clsImplementationEnum.NDETechniqueStatus.SentForApproval.GetStringValue();
                            objPAM011.ReturnRemarks = null;
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.PTTechniqueMeassage.SentForApproval.ToString();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg);
        }


        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult ApproveDIS()
        {
            return View();
        }

        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult ApproveDISDetails(int id)
        {
            PAM011 objPAM011 = db.PAM011.FirstOrDefault(x => x.HeaderId == id);

            PAM001 objPAM001 = db.PAM001.FirstOrDefault(x => x.HeaderId == objPAM011.PAMHeaderId);

            var BU = db.COM008.Where(x => x.t_cono == objPAM001.ContractNo).Select(x => x.t_csbu).FirstOrDefault();
            ViewBag.BU = BU;
            List<GLB002> objGLB002 = Manager.GetSubCatagories("DesignCode", BU, objClsLoginInfo.Location).ToList();
            var designCode = objGLB002.Where(x => x.Code == objPAM011.DesignCode).FirstOrDefault();
            if (designCode != null)
            {
                objPAM011.DesignCode = designCode.Code;
            }
            return View(objPAM011);
        }

        [SessionExpireFilter, HttpPost]
        public ActionResult ApproveDIS(int DisId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objPAM011 = db.PAM011.Where(i => i.HeaderId == DisId).FirstOrDefault();
                db.SP_DIS_APPROVE(DisId, objClsLoginInfo.UserName);
                objResponseMsg.Key = true;
                objResponseMsg.Value = "DIS Approved Successfully.";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter, HttpPost]
        public ActionResult ReturnDIS(int DisId, string returnRemarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objPAM011 = db.PAM011.Where(i => i.HeaderId == DisId).FirstOrDefault();
                objPAM011.Status = clsImplementationEnum.PAMStatus.Returned.GetStringValue();
                objPAM011.ReturnRemarks = returnRemarks;
                objPAM011.ReturnedBy = objClsLoginInfo.UserName;
                objPAM011.ReturnedOn = DateTime.Now;
                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = "DIS Return Successfully.";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();

            if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
            {
                var lstResult = db.SP_DIS_GETDISDETAILS(1, int.MaxValue, strSortOrder, whereCondition)
                             .Select(x => new
                             {
                                 x.PAM,
                                 x.DISNo,
                                 x.IssueNo,
                                 x.Status,
                                 x.Remarks
                             }).ToList();

                if (lstResult != null && lstResult.Count > 0)
                {
                    string str = Helper.GenerateExcel(lstResult, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = str;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Data not available.";
                }
            }
            else if (gridType == clsImplementationEnum.GridType.SUB_LINES.GetStringValue())
            {
                var lstResult = db.SP_DIS_GETDISLINEDETAILS(1, int.MaxValue, strSortOrder, whereCondition)
                                 .Select(x => new
                                 {
                                     x.Description,
                                     x.DocNo,
                                     x.DocRev,
                                     x.Pages,
                                     x.IncludeInDIS,
                                     x.ReturnRemarks
                                 }).ToList();
                if (lstResult != null && lstResult.Count > 0)
                {
                    string str = Helper.GenerateExcel(lstResult, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = str;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Data not available.";
                }
            }
            else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
            {
                var lstResult = db.SP_DIS_GETHEADER_HISTORY(1, int.MaxValue, strSortOrder, whereCondition)
                                 .Select(x => new
                                 {
                                     x.PAM,
                                     x.DISNo,
                                     x.IssueNo,
                                     x.Status,
                                     x.Remarks
                                 }).ToList();
                if (lstResult != null && lstResult.Count > 0)
                {
                    string str = Helper.GenerateExcel(lstResult, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = str;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Data not available.";
                }
            }
            else if (gridType == clsImplementationEnum.GridType.HISTORYLINES.GetStringValue())
            {
                var lstResult = db.SP_GET_DIS_LINES_HISTORY(1, int.MaxValue, strSortOrder, whereCondition)
                                 .Select(x => new
                                 {
                                     x.Description,
                                     x.DocNo,
                                     x.DocRev,
                                     x.Pages,
                                     x.IncludeInDIS,
                                     x.ReturnRemarks
                                 }).ToList();
                if (lstResult != null && lstResult.Count > 0)
                {
                    string str = Helper.GenerateExcel(lstResult, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = str;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Data not available.";
                }
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult BulkCopyAttachmentUploadForDIS(int HeaderId = 0, int LineId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objPAM011 = (from p6 in db.PAM011
                                 join p7 in db.PAM012 on p6.HeaderId equals p7.HeaderId
                                 join p1 in db.PAM001 on p6.PAMHeaderId equals p1.HeaderId
                                 where p7.HeaderId == (HeaderId == 0 ? p7.HeaderId : HeaderId)
                                 && p7.LineId == (LineId == 0 ? p7.LineId : LineId)
                                 select new { LineId = p7.LineId, ContractNo = p1.ContractNo, DocNo = p7.DocNo }
                                  ).ToList();

                var listContract = objPAM011.Select(i => i.ContractNo).Distinct().ToList();

                foreach (var cont in listContract)
                {
                    var sourcePath = "PAM007/ContractWiseDocuments/" + cont;

                    FileUploadController _objFUC = new FileUploadController();

                    //var existing = (new clsFileUpload()).GetDocuments(sourcePath);
                    var existing = _objFUC.GetAllDocumentsByTableNameTableId("PAM007/" + cont, 1);

                    var listPAM012 = objPAM011.Where(i => i.ContractNo == cont).ToList();
                    foreach (var doc in listPAM012)
                    {
                        //var existingLines = (new clsFileUpload()).GetDocuments("PAM012/"+doc.LineId);
                        var existingLines = _objFUC.GetAllDocumentsByTableNameTableId("PAM012", doc.LineId);
                        if (!existingLines.Any())
                        {
                            var objIsFileExist = existing.Where(k => Path.GetFileNameWithoutExtension(k.Document_name) == doc.DocNo).FirstOrDefault();
                            if (objIsFileExist != null)
                            {
                                //clsUpload.CopyFolderContents(folderPath, destinationPath);
                                //(new clsFileUpload()).CopyFolderContentsAsync(sourcePath, "PAM012/" + doc.LineId, objIsFileExist.Name);
                                string topath = "PAM012/" + doc.LineId;

                                _objFUC.CopyDataOnFCSServerAsync(sourcePath, topath, "PAM007/" + cont, 1, "PAM012", doc.LineId, IEMQS.DESServices.CommonService.GetUseIPConfig, IEMQS.DESServices.CommonService.objClsLoginInfo.UserName,0,true);
                            }
                        }
                    }
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
    }

    public class DisHeaderDtl
    {
        public string Contract { get; set; }
        public string Customer { get; set; }
        public string ContractNo { get; set; }
        public string DISNo { get; set; }
        public string RevNo { get; set; }
        public string CreatedOn { get; set; }
        public string Status { get; set; }
        public string HeaderId { get; set; }
        public string PAMHeaderId { get; set; }
    }
}