﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.PAM.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace IEMQS.Areas.PAM.Controllers
{
    public class SINController : clsBase
    {
        #region Utility
        public static List<SelectItemList> GetDeptWiseEmployeeList(string dept)
        {
            IEMQSEntitiesContext db = new IEMQSEntitiesContext();

            var employeslist = db.COM003.Where(x => x.t_depc == dept && x.t_actv == 1).ToList();
            if (employeslist.Count > 0)
            {

                var lstEmployee = employeslist.Select(x => new SelectItemList { text = (x.t_psno + "-" + x.t_name), id = x.t_psno }).ToList();
                return lstEmployee;
            }
            return null;
        }
        public string GetHeaderIds()
        {
            string username = objClsLoginInfo.UserName;
            string HeaderIds = string.Empty;
            List<int> data = new List<int>();

            var lstDepartment = db.PAM014.ToList();
            foreach (var item in lstDepartment)
            {
                if (!string.IsNullOrEmpty(item.RecdBy))
                {
                    var result = item.RecdBy.Split(',').Contains(username);
                    if (result)
                    {
                        data.Add(item.HeaderId);
                    }
                }
            }
            data = data.Distinct().ToList();

            HeaderIds = string.Join(",", data);
            return HeaderIds;
        }
        public string GetDepartmentName(string deptId)
        {
            var lstDepartment = db.COM002.Where(x => x.t_dtyp == 3 && x.t_dimx == deptId).FirstOrDefault();
            if (lstDepartment != null)
            {
                return lstDepartment.t_dimx + "-" + lstDepartment.t_desc;
            }
            return null;
        }
        public string GetEmployeeByPsNo(string psno)
        {
            var lstemps = db.COM003.Where(x => x.t_psno == psno && x.t_actv == 1).FirstOrDefault();
            if (lstemps != null)
            {
                return lstemps.t_psno + "-" + lstemps.t_name;
            }
            return null;
        }
        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (string.Equals(status, clsImplementationEnum.PAMStatus.Issued.GetStringValue()))
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;margin-left:5px;opacity:0.5;' Title='" + buttonTooltip + "' class='" + className + "'" + " ></i>";
            }
            else
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            }

            return htmlControl;
        }
        public string GenerateAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false)
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = columnName == "SeamDescription" ? "autocomplete form-control clsseamdesc" : "autocomplete form-control";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick=" + onClickMethod + "" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' data-lineid='" + rowId + "' hdElement='" + hdElement + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + " " + (disabled ? "disabled" : "") + " />";

            return strAutoComplete;
        }
        public static string GenerateTextboxFor(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string maxLength = "", bool disabled = false)
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            htmlControl = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id=" + inputID + " data-lineid='" + rowId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + " maxlength='" + maxLength + "' " + (disabled ? "disabled" : "") + "  />";

            return htmlControl;
        }
        public static string GenerateGridButton(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";


            htmlControl = "<a id='" + inputID + "' name='" + inputID + "' class='btn btn-outline btn-circle btn-sm blue' " + onClickEvent + " ><i class='" + className + "'></i> " + buttonName + "</a>";

            //htmlControl = "<i  data-modal='' id='" + inputID + "' name='" + inputID + "' style='cursor:Pointer;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";

            return htmlControl;
        }
        public string GetChecklistCheckStyle(int LineId, string status, bool? isCheck, bool? isDisabled, string onChangeMethod = "")
        {
            string rtnchecjbox = "";
            string onChangeEvent = !string.IsNullOrEmpty(onChangeMethod) ? "onchange='" + onChangeMethod + "'" : "";
            if (isCheck == null)
                isCheck = false;
            rtnchecjbox = "<input type='checkbox' style='text-align:center' name='chkChecklistCheck" + LineId.ToString() + "' id='chkChecklistCheck" + LineId.ToString() + "' class='make-switch col-md-3' data-on-text='Yes' data-off-text='No' data-on-color='success' data-off-color='danger' data-size='normal' " + onChangeEvent + " ";
            if (isDisabled == true)
            {
                rtnchecjbox += " disabled= 'disabled' ";
            }
            if (isCheck == true)
            {
                rtnchecjbox += " checked='checked' ";
            }
            //if (status.ToLower() == clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue().ToLower())
            //{
            //    if (isCheck == true)
            //    {
            //        rtnchecjbox += " checked='checked' ";
            //    }
            //}
            //else
            //{
            //    rtnchecjbox += " disabled= 'disabled' ";
            //}
            rtnchecjbox += "/>";
            return rtnchecjbox;
        }
        public string GetSectionRemarkStyle(int LineId, string status, string sectionRemarks)
        {
            string rtnremarks = "";
            rtnremarks += "<input type='text' id='txtSectionRemark" + LineId.ToString() + "' name='txtSectionRemark" + LineId.ToString() + "' style='width:100%'  class='form-control input-sm  input-inline sectionTextbox' value='" + sectionRemarks + "' onblur='UpdateRemarks(this," + LineId.ToString() + ")' ";
            if (status.ToLower() != clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue().ToLower())
            {
                rtnremarks += " disabled='disabled' ";
            }
            rtnremarks += " />";
            return rtnremarks;
        }
        public static string MultiSelectDropdown(List<SelectItemList> list, int rowID, string selectedValue, string selectedDept, bool Disabled = false, string OnChangeEvent = "", string OnBlurEvent = "", string ColumnName = "")
        {
            string multipleSelect = string.Empty;
            string[] arrayVal = { };

            try
            {
                if (rowID > 0 && selectedValue != "" && selectedDept != "")
                {
                    list = GetDeptWiseEmployeeList(selectedDept);
                }

                if (!string.IsNullOrWhiteSpace(selectedValue))
                {
                    arrayVal = selectedValue.Split(',');
                }
                multipleSelect += "<select data-name='ddlmultiple' name='ddlmultiple" + rowID + "' id='ddlmultiple" + rowID + "' data-lineid='" + rowID + "' multiple='multiple'  style='width: 100 % ' colname='" + ColumnName + "' class='form-control multiselect-drodown personMultiselect' " + (Disabled ? "disabled " : "") + (OnChangeEvent != string.Empty ? "onchange=" + OnChangeEvent + "" : "") + (OnBlurEvent != string.Empty ? " onblur='" + OnBlurEvent + "'" : "") + " >";
                if (list != null && list.Count > 0)
                {
                    foreach (var item in list)
                    {
                        multipleSelect += "<option value='" + item.id + "' " + ((arrayVal.Contains(item.id)) ? " selected" : "") + " >" + item.text + "</option>";
                    }
                }
                multipleSelect += "</select>";
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return multipleSelect;
        }
        #endregion

        #region SIN Header
        // GET: PAM/SIN
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult Index(string Project)
        {
            ViewBag.IsDisplayOnly = false;
            ViewBag.chkProject = Project;
            return View();
        }

        [HttpPost]
        public bool CheckSINStatus(int PAMHeaderId = 0)
        {
            if (PAMHeaderId > 0)
            {
                PAM013 objPAM013 = db.PAM013.Where(x => x.PAMHeaderId == PAMHeaderId).FirstOrDefault();
                if (objPAM013 == null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }

        [SessionExpireFilter]
        public ActionResult GetSINGridDataPartial(string status, string ForApprove, bool IsDisplayOnly = false)
        {
            ViewBag.IsDisplayOnly = IsDisplayOnly;
            ViewBag.Status = status;
            ViewBag.ForApprove = ForApprove;
            return PartialView("_GetSINGridDataPartial");
        }
        public ActionResult loadSINDataTable(JQueryDataTableParamModel param, string status, string ForApprove,string Project)
        {
            try
            {
                if(Project !=null && Project!="")
                {
                    var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                    var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                    string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                    string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                    int? StartIndex = param.iDisplayStart + 1;
                    int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                    string whereCondition = "1=1";
                    //whereCondition += " and loidate.t_csbu collate SQL_Latin1_General_CP850_CI_AI in (select * from dbo.fn_split(dbo.GETBUFROMUSER('" + objClsLoginInfo.UserName + "'),',')) ";
                    if (Convert.ToBoolean(ForApprove))
                    {
                        var HeaderIds = GetHeaderIds();
                        //var HeaderIds = db.PAM014.Where(x => x.RecdBy == objClsLoginInfo.UserName).Select(x => x.HeaderId).ToList();
                        if (HeaderIds != "" && HeaderIds.Length > 0)
                        {
                            //var Ids = string.Join(",", HeaderIds);
                            whereCondition += " and PAM002.HeaderId in (" + HeaderIds + ")";
                        }
                        else
                        {
                            return Json(new
                            {
                                sEcho = param.sEcho,
                                iTotalDisplayRecords = 0,
                                iTotalRecords = 0,
                                aaData = new string[0],
                                whereCondition = whereCondition,
                                strSortOrder = "",
                            }, JsonRequestBehavior.AllowGet);
                        }

                        if (status.ToUpper() == "PENDING")
                        {
                            whereCondition += " and status in ('" + clsImplementationEnum.PAMStatus.Issued.GetStringValue() + "')";
                        }
                        else
                        {
                            whereCondition += " and status in ('" + clsImplementationEnum.PAMStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.PAMStatus.Issued.GetStringValue() + "')";
                        }
                    }
                    else
                    {
                        if (status.ToUpper() == "PENDING")
                        {
                            whereCondition += " and status in ('" + clsImplementationEnum.PAMStatus.Draft.GetStringValue() + "')";
                        }
                        else
                        {
                            whereCondition += " and status in ('" + clsImplementationEnum.PAMStatus.Issued.GetStringValue() + "','" + clsImplementationEnum.PAMStatus.Draft.GetStringValue() + "')";
                        }
                    }
                    string[] columnName = { "PAMHeaderId", "SINNo", "Shop", "ProdGroup", "dbo.GET_USERNAME_BY_PSNO(PreparedBy)", "ReasonofNextIssue", "IssueNo", "Status" };

                    // whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                    if (!string.IsNullOrEmpty(param.sSearch))
                    {
                        whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                    }
                    else
                    {
                        whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                    }

                    string strSortOrder = string.Empty;
                    if (!string.IsNullOrWhiteSpace(sortColumnName))
                    {
                        strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                    }
                    whereCondition += " and PAM002.Project='" + Project.ToString() + "'";

                    var lstPam = db.SP_SIN_GETSINDETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                    lstPam = lstPam.Where(x => x.Project.Contains(Project)).ToList();
                    int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                    var res = (from h in lstPam
                               select new[] {
                               Convert.ToString(h.HeaderId),
                               Convert.ToString(h.PAMHeaderId),
                               Convert.ToString(h.SINNo),
                               Convert.ToString(h.Shop),
                               Convert.ToString(h.ProdGroup),
                               Convert.ToString(h.PreparedBy),
                               Convert.ToString(h.ReasonofNextIssue),
                               Convert.ToString(h.IssueNo),
                               Convert.ToString(h.Status),
                               "" //Action
                    }).ToList();
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                        whereCondition = whereCondition,
                        strSortOrder = strSortOrder,
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                    var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                    string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                    string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                    int? StartIndex = param.iDisplayStart + 1;
                    int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                    string whereCondition = "1=1";
                    //whereCondition += " and loidate.t_csbu collate SQL_Latin1_General_CP850_CI_AI in (select * from dbo.fn_split(dbo.GETBUFROMUSER('" + objClsLoginInfo.UserName + "'),',')) ";
                    if (Convert.ToBoolean(ForApprove))
                    {
                        var HeaderIds = GetHeaderIds();
                        //var HeaderIds = db.PAM014.Where(x => x.RecdBy == objClsLoginInfo.UserName).Select(x => x.HeaderId).ToList();
                        if (HeaderIds != "" && HeaderIds.Length > 0)
                        {
                            //var Ids = string.Join(",", HeaderIds);
                            whereCondition += " and PAM002.HeaderId in (" + HeaderIds + ")";
                        }
                        else
                        {
                            return Json(new
                            {
                                sEcho = param.sEcho,
                                iTotalDisplayRecords = 0,
                                iTotalRecords = 0,
                                aaData = new string[0],
                                whereCondition = whereCondition,
                                strSortOrder = "",
                            }, JsonRequestBehavior.AllowGet);
                        }

                        if (status.ToUpper() == "PENDING")
                        {
                            whereCondition += " and status in ('" + clsImplementationEnum.PAMStatus.Issued.GetStringValue() + "')";
                        }
                        else
                        {
                            whereCondition += " and status in ('" + clsImplementationEnum.PAMStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.PAMStatus.Issued.GetStringValue() + "')";
                        }
                    }
                    else
                    {
                        if (status.ToUpper() == "PENDING")
                        {
                            whereCondition += " and status in ('" + clsImplementationEnum.PAMStatus.Draft.GetStringValue() + "')";
                        }
                        else
                        {
                            whereCondition += " and status in ('" + clsImplementationEnum.PAMStatus.Issued.GetStringValue() + "','" + clsImplementationEnum.PAMStatus.Draft.GetStringValue() + "')";
                        }
                    }
                    string[] columnName = { "PAMHeaderId", "SINNo", "Shop", "ProdGroup", "dbo.GET_USERNAME_BY_PSNO(PreparedBy)", "ReasonofNextIssue", "IssueNo", "Status" };

                    // whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                    if (!string.IsNullOrEmpty(param.sSearch))
                    {
                        whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                    }
                    else
                    {
                        whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                    }

                    string strSortOrder = string.Empty;
                    if (!string.IsNullOrWhiteSpace(sortColumnName))
                    {
                        strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                    }

                    var lstPam = db.SP_SIN_GETSINDETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                    int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                    var res = (from h in lstPam
                               select new[] {
                               Convert.ToString(h.HeaderId),
                               Convert.ToString(h.PAMHeaderId),
                               Convert.ToString(h.SINNo),
                               Convert.ToString(h.Shop),
                               Convert.ToString(h.ProdGroup),
                               Convert.ToString(h.PreparedBy),
                               Convert.ToString(h.ReasonofNextIssue),
                               Convert.ToString(h.IssueNo),
                               Convert.ToString(h.Status),
                               "" //Action
                    }).ToList();
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                        whereCondition = whereCondition,
                        strSortOrder = strSortOrder,
                    }, JsonRequestBehavior.AllowGet);
                }
                
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult SINHeaderDetails(int id,string Project)
        {
            ViewBag.IsDisplayOnly = false;
            PAM013 objPAM013 = db.PAM013.Where(x => x.HeaderId == id).FirstOrDefault();
            var lstDepartment = db.COM002.Where(x => x.t_dtyp == 3).Select(x => new BULocWiseCategoryModel { CatDesc = (x.t_dimx + "-" + x.t_desc), CatID = x.t_dimx }).ToList();
            ViewBag.lstShop = lstDepartment;
            ViewBag.Shop = GetDepartmentName(objPAM013.Shop);
            ViewBag.chkProject = Project;

            return View("CreateSIN", objPAM013);
        }
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult SINHeaderHistroyDetails(int id)
        {
            PAM013_Log objPAM013_Log = db.PAM013_Log.Where(x => x.HeaderId == id).FirstOrDefault();
            var lstDepartment = db.COM002.Where(x => x.t_dtyp == 3).Select(x => new BULocWiseCategoryModel { CatDesc = (x.t_dimx + "-" + x.t_desc), CatID = x.t_dimx }).ToList();
            ViewBag.lstShop = lstDepartment;
            ViewBag.Shop = GetDepartmentName(objPAM013_Log.Shop);

            return View("CreateHistroySIN", objPAM013_Log);
        }
        [SessionExpireFilter]
        public ActionResult CreateSIN(int DISId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            PAM013 objPAM013 = null;
            try
            {
                PAM011 objPAM011 = db.PAM011.Where(x => x.HeaderId == DISId).FirstOrDefault();
                objPAM013 = db.PAM013.Where(x => x.PAMHeaderId == objPAM011.PAMHeaderId).FirstOrDefault();

                var lstDepartment = db.COM002.Where(x => x.t_dtyp == 3).Select(x => new BULocWiseCategoryModel { CatDesc = (x.t_dimx + "-" + x.t_desc), CatID = x.t_dimx }).ToList();
                ViewBag.lstShop = lstDepartment;

                if (objPAM013 != null)
                {
                    if (objPAM013.Status.ToLower() == clsImplementationEnum.PAMStatus.Draft.GetStringValue().ToLower() && !objPAM011.IsSINCreated)
                    {
                        objPAM011.IsSINCreated = true;
                        if (objPAM013.PAM015.Count > 0)
                        {
                            //delete existing line from PAM015 if SIN is in draft state
                            db.PAM015.RemoveRange(objPAM013.PAM015);
                            db.SaveChanges();

                            //add all DIS lines from PAM012 to PAM015
                            if (objPAM011.PAM012.Count > 0)
                            {
                                List<PAM012> newObjPAM012 = objPAM011.PAM012.Where(x => x.IncludeInDIS).ToList();
                                foreach (var item in newObjPAM012)
                                {
                                    PAM015 newObjPAM015 = new PAM015
                                    {
                                        HeaderId = objPAM013.HeaderId,
                                        DocDescription = item.Description,
                                        DocNo = item.DocNo,
                                        IncludeInSIN = item.IncludeInDIS,
                                        SINNo = objPAM013.SINNo,
                                        DocRev = item.DocRev,
                                        Notes = item.ReturnRemarks,
                                        CreatedBy = objClsLoginInfo.UserName,
                                        CreatedOn = DateTime.Now
                                    };

                                    db.PAM015.Add(newObjPAM015);
                                    db.SaveChanges();

                                    var folderPath = "PAM012/" + item.LineId;
                                    var destinationPath = "PAM015/" + newObjPAM015.LineId;
                                    //var existing = (new clsFileUpload()).GetDocuments(folderPath);
                                    FileUploadController _objFUC = new FileUploadController();
                                    if (_objFUC.CheckAnyDocumentsExits("PAM012",item.LineId))
                                    {
                                        //clsUpload.CopyFolderContents(folderPath, destinationPath);
                                        //(new clsFileUpload()).CopyFolderContentsAsync(folderPath, destinationPath);                                        
                                        _objFUC.CopyDataOnFCSServerAsync(folderPath, destinationPath, "PAM012", item.LineId, "PAM015", newObjPAM015.LineId, IEMQS.DESServices.CommonService.GetUseIPConfig, IEMQS.DESServices.CommonService.objClsLoginInfo.UserName,0,true);
                                    }
                                }
                            }
                        }
                    }
                    else if (objPAM013.Status.ToLower() == clsImplementationEnum.PAMStatus.Issued.GetStringValue().ToLower() && !objPAM011.IsSINCreated)
                    {
                        objPAM011.IsSINCreated = true;
                        //create new revision of SIN and copied all latest DIS lines into SIN lines PAM012 to PAM015
                        objPAM013.IssueNo += 1;
                        objPAM013.Status = clsImplementationEnum.PAMStatus.Draft.GetStringValue();
                        objPAM013.ModifiedBy = objClsLoginInfo.UserName;
                        objPAM013.ModifiedOn = DateTime.Now;

                        //delete old lines from PAM015 and add new IDS lines from PAM012
                        if (objPAM013.PAM015.Count > 0)
                        {
                            db.PAM015.RemoveRange(objPAM013.PAM015);
                            db.SaveChanges();

                            //add all DIS lines from PAM012 to PAM015
                            if (objPAM011.PAM012.Count > 0)
                            {
                                List<PAM012> newObjPAM012 = objPAM011.PAM012.Where(x => x.IncludeInDIS).ToList();
                                foreach (var item in newObjPAM012)
                                {
                                    PAM015 newObjPAM015 = new PAM015
                                    {
                                        HeaderId = objPAM013.HeaderId,
                                        DocDescription = item.Description,
                                        DocNo = item.DocNo,
                                        IncludeInSIN = item.IncludeInDIS,
                                        SINNo = objPAM013.SINNo,
                                        DocRev = item.DocRev,
                                        Notes=item.ReturnRemarks,
                                        CreatedBy = objClsLoginInfo.UserName,
                                        CreatedOn = DateTime.Now
                                    };

                                    db.PAM015.Add(newObjPAM015);
                                    db.SaveChanges();

                                    var folderPath = "PAM012/" + item.LineId;
                                    var destinationPath = "PAM015/" + newObjPAM015.LineId;
                                    //var existing = (new clsFileUpload()).GetDocuments(folderPath);

                                    FileUploadController _objFUC = new FileUploadController();
                                    if (_objFUC.CheckAnyDocumentsExits("PAM012", item.LineId))
                                    {
                                        //clsUpload.CopyFolderContents(folderPath, destinationPath);
                                        //(new clsFileUpload()).CopyFolderContentsAsync(folderPath, destinationPath);
                                        _objFUC.CopyDataOnFCSServerAsync(folderPath, destinationPath, "PAM012", item.LineId, "PAM015", newObjPAM015.LineId, IEMQS.DESServices.CommonService.GetUseIPConfig, IEMQS.DESServices.CommonService.objClsLoginInfo.UserName,0,true);
                                    }
                                }
                            }
                        }
                    }

                    var lstShops = lstDepartment.Where(x => x.CatID == objPAM013.Shop).FirstOrDefault();

                    if (lstShops != null)
                    {
                        ViewBag.Shop = lstShops.CatDesc;
                    }
                    return View(objPAM013);
                }
                else
                {
                    objPAM013 = new PAM013();
                }

                objPAM013.PAMHeaderId = objPAM011.PAMHeaderId;
                objPAM013.DISId = objPAM011.HeaderId;
                objPAM013.DISNo = objPAM011.DISNo;
                objPAM013.PAM = objPAM011.PAM;
                objPAM013.Status = clsImplementationEnum.PAMStatus.Draft.GetStringValue();
                objPAM013.SINNo = objPAM011.DISNo.Replace("DIS", "SIN");
                objPAM013.PreparedBy = objClsLoginInfo.UserName;
                objPAM013.IssueNo = objPAM011.IssueNo;
                objPAM013.CreatedBy = objClsLoginInfo.UserName;
                objPAM013.CreatedOn = DateTime.Now;
                db.PAM013.Add(objPAM013);
                db.SaveChanges();

                if (objPAM011.PAM012.Count > 0)
                {
                    List<PAM012> newObjPAM012 = objPAM011.PAM012.Where(x => x.IncludeInDIS).ToList();
                    foreach (var item in newObjPAM012)
                    {
                        PAM015 objPAM015 = new PAM015
                        {
                            HeaderId = objPAM013.HeaderId,
                            DocDescription = item.Description,
                            DocNo = item.DocNo,
                            IncludeInSIN = item.IncludeInDIS,
                            SINNo = objPAM013.SINNo,
                            DocRev = item.DocRev,
                            Notes = item.ReturnRemarks,
                            CreatedBy = objClsLoginInfo.UserName,
                            CreatedOn = DateTime.Now
                        };

                        db.PAM015.Add(objPAM015);
                        db.SaveChanges();

                        var folderPath = "PAM012/" + item.LineId;
                        var destinationPath = "PAM015/" + objPAM015.LineId;
                        //var existing = (new clsFileUpload()).GetDocuments(folderPath);

                        FileUploadController _objFUC = new FileUploadController();
                        if (_objFUC.CheckAnyDocumentsExits("PAM012", item.LineId))
                        {
                            //clsUpload.CopyFolderContents(folderPath, destinationPath);
                            //(new clsFileUpload()).CopyFolderContentsAsync(folderPath, destinationPath);
                            _objFUC.CopyDataOnFCSServerAsync(folderPath, destinationPath, "PAM012", item.LineId, "PAM015", objPAM015.LineId, IEMQS.DESServices.CommonService.GetUseIPConfig, IEMQS.DESServices.CommonService.objClsLoginInfo.UserName,0,true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return View(objPAM013);
        }

        [SessionExpireFilter]
        public ActionResult GetSINHeaderDetails(int SinId, int PAMHeaderId)
        {
            SINHeaderDtl header = new SINHeaderDtl();

            header.HeaderId = SinId.ToString();
            header.PAMHeaderId = PAMHeaderId.ToString();

            PAM001 objPAM001 = db.PAM001.FirstOrDefault(x => x.HeaderId == PAMHeaderId);

            header.Contract = db.Database.SqlQuery<string>(@"SELECT t_cono +'-'+ t_desc FROM    " + LNLinkedServer + ".dbo.ttpctm100175 where t_cono = '" + objPAM001.ContractNo + "'").FirstOrDefault();
            header.Customer = db.Database.SqlQuery<string>(@"SELECT  t_bpid +'-'+ t_nama   from " + LNLinkedServer + ".dbo.ttccom100175  where t_bpid = '" + objPAM001.Customer + "'").FirstOrDefault();
            header.ContractNo = objPAM001.ContractNo;

            PAM013 objPAM013 = db.PAM013.FirstOrDefault(x => x.HeaderId == SinId);
            header.Status = objPAM013.Status;
            header.SINNo = objPAM013.SINNo;
            header.RevNo = objPAM013.IssueNo.ToString();
            header.CreatedOn = objPAM013.CreatedOn.ToString();
            header.CreatedBy = Manager.GetPsidandDescription(objPAM013.CreatedBy);

            return PartialView("~/Areas/PAM/Views/Shared/_SINHeaderDetails.cshtml", header);
        }

        [HttpPost]
        public ActionResult SaveHeaderValue(int headerId = 0, string shop = "", string productGroup = "", string resonIssue = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (headerId > 0)
                {
                    PAM013 objPAM013 = db.PAM013.Where(x => x.HeaderId == headerId).FirstOrDefault();
                    objPAM013.Shop = shop;
                    objPAM013.ProdGroup = productGroup;
                    if (resonIssue != "")
                    {
                        objPAM013.ReasonofNextIssue = resonIssue;
                    }
                    objPAM013.ModifiedBy = objClsLoginInfo.UserName;
                    objPAM013.ModifiedOn = DateTime.Now;
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region SIN Department
        [SessionExpireFilter]
        public ActionResult GetSINDepartment(int SinId, string ForApprove, string Status, bool IsDisplayOnly = false)
        {
            ViewBag.IsDisplayOnly = IsDisplayOnly;
            ViewBag.SinId = SinId.ToString();
            ViewBag.Status = Status;
            ViewBag.Acknowledge = clsImplementationEnum.GetDeptAcknowledgements().Select(x => new BULocWiseCategoryModel { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();
            ViewBag.lstDepartment = db.COM002.Where(x => x.t_dtyp == 3).Select(x => new BULocWiseCategoryModel { CatDesc = (x.t_dimx + "-" + x.t_desc), CatID = x.t_dimx }).ToList();
            if (ForApprove == null)
            {
                ViewBag.ForApprove = "false";
            }
            else
            {
                ViewBag.ForApprove = ForApprove;
            }
            return PartialView("~/Areas/PAM/Views/SIN/_GetSINDepartmentGridDataPartial.cshtml");
        }
        public ActionResult loadSINDeptDataTable(JQueryDataTableParamModel param, int SINId, string ForApprove)
        {
            try
            {
                PAM013 objPAM013 = db.PAM013.Where(i => i.HeaderId == SINId).FirstOrDefault();
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                List<SelectItemList> items = new List<SelectItemList>();
                var lstdept = db.COM002.Where(x => x.t_dtyp == 3).ToList();
                var lstEmp = db.COM003.ToList();

                var IsDisplayOnly = !string.IsNullOrEmpty(Request["IsDisplayOnly"]) ? Convert.ToBoolean(Request["IsDisplayOnly"].ToString()) : false;
                bool IsEditable = true;
                if (objPAM013.Status.ToLower() == clsImplementationEnum.PAMStatus.Issued.GetStringValue().ToLower())
                {
                    IsEditable = false;
                }
                if (IsDisplayOnly)
                {
                    IsEditable = false;
                }
                
                if (ForApprove == null || ForApprove.ToLower() == "false")
                {
                    string whereCondition = "1=1";
                    whereCondition += " and HeaderId= " + SINId.ToString();

                    string[] columnName = { "Department", "RecdBy", "Remarks" };

                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                    var lstPam = db.SP_SIN_GETSINDEPARMENTDETAILS(StartIndex, EndIndex, "", whereCondition).ToList();
                    int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                    int newRecordId = 0;
                    var newRecord = new[] {
                                    "",
                                    "",
                                    Helper.GenerateHidden(newRecordId, "HeaderId", Convert.ToString(SINId)),
                                    IsEditable ? GenerateAutoComplete(newRecordId, "Department","","UpdateData(this, "+ newRecordId +");", "", false,"","Department")+""+Helper.GenerateHidden(newRecordId, "ddlDepartment", Convert.ToString(newRecordId)) : GenerateAutoComplete(newRecordId, "Department","","", "", false,"","Department", true)+""+Helper.GenerateHidden(newRecordId, "ddlDepartment", Convert.ToString(newRecordId)),
                                    //IsEditable ? GenerateAutoComplete(newRecordId, "RecdBy","","", "", false,"","RecdBy")+""+Helper.GenerateHidden(newRecordId, "ddlRecdBy", Convert.ToString(newRecordId)) : GenerateAutoComplete(newRecordId, "RecdBy","","", "", false,"","RecdBy", true)+""+Helper.GenerateHidden(newRecordId, "ddlRecdBy", Convert.ToString(newRecordId)),
                                    IsEditable ? MultiSelectDropdown(items,newRecordId,"","",false,"","","RecdBy") : MultiSelectDropdown(items,newRecordId,"","",true,"","","RecdBy"),
                                    "",
                                    "",
                                    IsEditable ? GenerateGridButton(newRecordId, "Save", "Add Rocord", "fa fa-save", "SaveDeptRecord(0);" ) : "",
                                    "",
                                };

                    var res = (from h in lstPam
                               select new[] {
                                    Convert.ToString(h.ROW_NO),
                                    Convert.ToString(h.LineId),
                                    Helper.GenerateHidden(h.LineId, "HeaderId", Convert.ToString(h.HeaderId)),
                                    IsEditable ? GenerateAutoComplete(h.LineId, "Department",h.Department,"UpdateData(this, "+ h.LineId +");", "", false,"","Department")+""+Helper.GenerateHidden(h.LineId, "ddlDepartment",h.Department.Split('-')[0]) : GenerateAutoComplete(h.LineId, "Department",h.Department,"", "", false,"","Department", true)+""+Helper.GenerateHidden(h.LineId, "ddlDepartment",h.Department.Split('-')[0]),
                                    //IsEditable ? GenerateAutoComplete(h.LineId, "RecdBy",h.RecdBy,"", "", false,"","RecdBy")+""+Helper.GenerateHidden(h.LineId, "ddlRecdBy", Convert.ToString(h.RecdBy)) : GenerateAutoComplete(h.LineId, "RecdBy",h.RecdBy,"", "", false,"","RecdBy", true)+""+Helper.GenerateHidden(h.LineId, "ddlRecdBy", Convert.ToString(h.RecdBy)),
                                    IsEditable ? MultiSelectDropdown(items,h.LineId,h.RecdBy,h.Department.Split('-')[0],false,"","","RecdBy") : MultiSelectDropdown(items,h.LineId,h.RecdBy,h.Department.Split('-')[0],true,"","","RecdBy"),
                                    GenerateAutoComplete(h.LineId, "Acknowledged",h.Acknowledged,"", "", false,"","Acknowledged", true)+""+Helper.GenerateHidden(h.LineId, "ddlAcknowledged",h.Acknowledged),
                                    GenerateTextboxFor(h.LineId,"Remarks",h.Remarks,"","",false,"","30",true),
                                    IsEditable ? HTMLActionString(h.LineId,"","Delete","Delete Record","fa fa-trash-o","DeleteDepartmentLine("+ h.HeaderId+ "," + h.LineId +"); ") + " " + HTMLActionString(h.LineId,"","Edit","Edit Record","fa fa-pencil-square-o","SaveDeptRecord("+h.LineId+", false);") : "",
                                    }).ToList();

                    res.Insert(0, newRecord);
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string whereCondition = "1=1";
                    whereCondition += " and HeaderId= " + SINId.ToString() + " and RecdBy like '%" + Convert.ToString(objClsLoginInfo.UserName) + "%'";

                    string[] columnName = { "Department", "RecdBy", "Remarks" };

                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                    var lstPam = db.SP_SIN_GETSINDEPARMENTDETAILS(StartIndex, EndIndex, "", whereCondition).ToList();
                    int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();
                    var res = (from h in lstPam
                               select new[] {
                                    Convert.ToString(h.ROW_NO),
                                    Convert.ToString(h.LineId),
                                    Helper.GenerateHidden(h.LineId, "HeaderId", Convert.ToString(h.HeaderId)),
                                    GenerateAutoComplete(h.LineId, "Department",h.Department,"", "", false,"","Department",true)+""+Helper.GenerateHidden(h.LineId, "ddlDepartment",h.Department.Split('-')[0]),
                                    //GenerateAutoComplete(h.LineId, "RecdBy",h.RecdBy,"", "", false,"","RecdBy",true)+""+Helper.GenerateHidden(h.LineId, "ddlRecdBy", Convert.ToString(h.RecdBy)),
                                    MultiSelectDropdown(items,h.LineId,h.RecdBy,h.Department.Split('-')[0],true,"","","RecdBy"),
                                    GenerateAutoComplete(h.LineId, "Acknowledged",h.Acknowledged,"", "", false,"","Acknowledged")+""+Helper.GenerateHidden(h.LineId, "ddlAcknowledged",h.Acknowledged),
                                    GenerateTextboxFor(h.LineId,"Remarks",h.Remarks,"","",false,"","500",false),
                                    HTMLActionString(h.LineId,"","Edit","Edit Record","fa fa-pencil-square-o","SaveDeptRecord("+h.LineId+", true);"),
                    }).ToList();
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                    }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetPersonDeptWise(string param)
        {
            //var employeslist = db.COM003.Where(x => x.t_depc == department).ToList();
            //if (employeslist.Count > 0)
            //{
            //    var lstEmployee = employeslist.Select(x => new BULocWiseCategoryModel { CatDesc = (x.t_psno + "-" + x.t_name), CatID = x.t_psno }).ToList();
            //    return Json(new { lstPerson = lstEmployee } , JsonRequestBehavior.AllowGet);
            //}
            var employeslist = db.COM003.Where(x => x.t_depc == param && x.t_actv == 1).ToList();
            if (employeslist.Count > 0)
            {
                var lstEmployee = employeslist.Select(x => new ddlValue { text = (x.t_psno + "-" + x.t_name), id = x.t_psno }).ToList();
                return Json(lstEmployee, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SaveSINDepartment(FormCollection fc, int Id = 0, string isApprover = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int HeaderId = Convert.ToInt32(fc["HeaderId" + Id]);
                PAM014 objPAM014 = null;

                if (Id > 0)
                {
                    objPAM014 = db.PAM014.Where(x => x.LineId == Id).FirstOrDefault();
                    string department = fc["ddlDepartment" + Id];
                    var lstdept = db.PAM014.Where(x => x.HeaderId == HeaderId && x.Department == department && x.LineId != Id).ToList();
                    if (lstdept.Count > 0)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "ExistsDept";
                    }
                    else
                    {
                        if (isApprover.ToLower() == "false")
                        {
                            objPAM014.Department = fc["ddlDepartment" + Id];
                            objPAM014.RecdBy = fc["ddlmultiple" + Id];
                        }
                        else
                        {
                            objPAM014.Acknowledged = fc["ddlAcknowledged" + Id];
                            objPAM014.AcknowledgedOn = DateTime.Now;
                            objPAM014.Remarks = fc["Remarks" + Id];

                            //entry reflact in department log table
                            PAM014_Log objPAM014_Log = db.PAM014_Log.Where(x => x.LineId == objPAM014.LineId).FirstOrDefault();
                            objPAM014_Log.Acknowledged = objPAM014.Acknowledged;
                            objPAM014_Log.Remarks = objPAM014.Remarks;
                            objPAM014_Log.EditBy = objClsLoginInfo.UserName;
                            objPAM014_Log.EditedOn = DateTime.Now;
                            db.SaveChanges();
                        }
                        objPAM014.EditBy = objClsLoginInfo.UserName;
                        objPAM014.EditedOn = DateTime.Now;
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                        db.SaveChanges();
                    }
                }
                else
                {
                    string department = fc["ddlDepartment" + Id];
                    var lstdept = db.PAM014.Where(x => x.HeaderId == HeaderId && x.Department == department && x.LineId != Id).ToList();
                    if (lstdept.Count > 0)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "ExistsDept";
                    }
                    else
                    {
                        objPAM014 = new PAM014();
                        objPAM014.HeaderId = HeaderId;
                        objPAM014.Department = fc["ddlDepartment" + Id];
                        objPAM014.RecdBy = fc["ddlmultiple" + Id];
                        objPAM014.CreatedBy = objClsLoginInfo.UserName;
                        objPAM014.CreatedOn = DateTime.Now;
                        db.PAM014.Add(objPAM014);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult UpdateDept(int LineId, string Acknowledged)
        //{
        //    clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
        //    try
        //    {
        //        PAM014 objPAM014 = db.PAM014.Where(x => x.LineId == LineId).FirstOrDefault();
        //        objPAM014.Acknowledged = Acknowledged;
        //        objPAM014.EditBy = objClsLoginInfo.UserName;
        //        objPAM014.EditedOn = DateTime.Now;
        //        db.SaveChanges();

        //        objResponseMsg.Key = true;
        //        objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        objResponseMsg.Key = false;
        //        objResponseMsg.Value = ex.Message;
        //    }

        //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //}
        [HttpPost]
        public ActionResult DeleteDepartmentLines(int LineID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PAM014 objPAM014 = db.PAM014.Where(x => x.LineId == LineID).FirstOrDefault();

                if (objPAM014 != null)
                {
                    db.PAM014.Remove(objPAM014);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details successfully deleted.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region SIN Lines
        [SessionExpireFilter]
        public ActionResult GetSINLinesDetails(int SinId, string ForApprove, bool IsDisplayOnly = false)
        {
            ViewBag.IsDisplayOnly = IsDisplayOnly;
            ViewBag.SinId = SinId.ToString();
            if (ForApprove == null)
            {
                ViewBag.ForApprove = "false";
            }
            else
            {
                ViewBag.ForApprove = ForApprove;
            }
            return PartialView("~/Areas/PAM/Views/SIN/_GetSINLinesGridDataPartial.cshtml");
        }
        public ActionResult loadSINLineDataTable(JQueryDataTableParamModel param, int SINId, string ForApprove)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                var IsDisplayOnly = !string.IsNullOrEmpty(Request["IsDisplayOnly"]) ? Convert.ToBoolean(Request["IsDisplayOnly"].ToString()) : false;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                PAM013 objPAM013 = db.PAM013.Where(i => i.HeaderId == SINId).FirstOrDefault();
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                bool IsEditable = true;
                string whereCondition = "1=1";
                whereCondition += " and HeaderId= " + SINId.ToString();

                if (objPAM013.Status.ToLower() == clsImplementationEnum.PAMStatus.Issued.GetStringValue().ToLower())
                {
                    IsEditable = false;
                    whereCondition += " and IncludeInSIN = 1";
                }
                if (IsDisplayOnly)
                {
                    IsEditable = false;
                }
                string[] columnName = { "DocDescription", "DocNo", "DocRev", "Notes" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstPam = db.SP_SIN_GETSINLINEDETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();
                if (ForApprove == null || ForApprove.ToLower() == "false")
                {
                    var res = (from h in lstPam
                               select new[] {
                                    Convert.ToString(h.ROW_NO),
                                    Convert.ToString(h.LineId),
                                    Helper.GenerateHidden(h.LineId, "HeaderId", Convert.ToString(h.HeaderId)),
                                    GenerateTextboxFor(h.LineId,"DocDescription",h.DocDescription,"","",false,"","14",true),
                                    GenerateTextboxFor(h.LineId,"DocNo",h.DocNo,"","",false,"","30",true),
                                    GenerateTextboxFor(h.LineId,"DocRev",h.DocRev,"","",false,"","5",true),
                                    IsEditable ? GenerateTextboxFor(h.LineId,"Notes",h.Notes,"","",false,"","500",false) : GenerateTextboxFor(h.LineId,"Notes",h.Notes,"","",false,"","500",true),
                                    "",
                                    IsEditable ? GetChecklistCheckStyle(h.LineId,objPAM013.Status,h.IncludeInSIN, false, "UpdateAccepted(this," + Convert.ToString(h.LineId) + ")") : GetChecklistCheckStyle(h.LineId,objPAM013.Status,h.IncludeInSIN, true, "UpdateAccepted(this," + Convert.ToString(h.LineId) + ")"),
                                    IsEditable ? HTMLActionString(h.LineId,"","Edit","Edit Record","fa fa-pencil-square-o","SaveLineRecord("+h.LineId+");") : "<i id=\"\" name=\"\" style='cursor: pointer;margin-left:5px;opacity:0.5;' Title=\"\" class=\"fa fa-pencil-square-o\" ></i>",
                                    }).ToList();

                    //res.Insert(0, newRecord);
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                        whereCondition = whereCondition,
                        strSortOrder = "",
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var res = (from h in lstPam
                               select new[] {
                                    Convert.ToString(h.ROW_NO),
                                    Convert.ToString(h.LineId),
                                    Helper.GenerateHidden(h.LineId, "HeaderId", Convert.ToString(h.HeaderId)),
                                    GenerateTextboxFor(h.LineId,"DocDescription",h.DocDescription,"","",false,"","14",true),
                                    GenerateTextboxFor(h.LineId,"DocNo",h.DocNo,"","",false,"","30",true),
                                    GenerateTextboxFor(h.LineId,"DocRev",h.DocRev,"","",false,"","5",true),
                                    GenerateTextboxFor(h.LineId,"Notes",h.Notes,"","",false,"","500",true),
                                    "",
                                    GetChecklistCheckStyle(h.LineId,objPAM013.Status,h.IncludeInSIN, true),
                    }).ToList();
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                        whereCondition = whereCondition,
                        strSortOrder = "",
                    }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveLines(int lineId, string notes)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (lineId > 0)
                {
                    PAM015 objPAM015 = db.PAM015.Where(x => x.LineId == lineId).FirstOrDefault();
                    objPAM015.Notes = notes;
                    objPAM015.EditBy = objClsLoginInfo.UserName;
                    objPAM015.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateAccepted(int LineId, bool IncludeInSIN)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PAM015 objPAM015 = db.PAM015.Where(x => x.LineId == LineId).FirstOrDefault();
                objPAM015.IncludeInSIN = IncludeInSIN;
                objPAM015.EditBy = objClsLoginInfo.UserName;
                objPAM015.EditedOn = DateTime.Now;
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SINAttachmentPartial(int? id)
        {
            PAM015 objPAM015 = db.PAM015.Where(x => x.LineId == id).FirstOrDefault();
            return PartialView("_SINAttachmentPartial", objPAM015);
        }

        [HttpPost]
        public ActionResult ContractIssue(int SINId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (SINId > 0)
                {
                    PAM013 objPAM013 = db.PAM013.Where(x => x.HeaderId == SINId).FirstOrDefault();
                    objPAM013.Status = clsImplementationEnum.PAMStatus.Issued.GetStringValue();
                    objPAM013.ModifiedBy = objClsLoginInfo.UserName;
                    objPAM013.ModifiedOn = DateTime.Now;
                    db.SaveChanges();

                    db.SP_SIN_ISSUED(objPAM013.HeaderId, objPAM013.ModifiedBy);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Issued.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region SIN Issuer
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult SINIssuer()
        {
            return View();
        }


        /// Histroy addred by deepak tiwari (28/11/2017)
        #region SIN Header Histroy

        public ActionResult GetHistoryView(string ForApprove, int headerid = 0)
        {
            PAM013_Log objPAM013_Log = new PAM013_Log();
            ViewBag.ForApprove = ForApprove;
            ViewBag.headerid = headerid;
            return PartialView("getHistorysinPartial", objPAM013_Log);
        }

        public ActionResult loadSINHistroyDataTable(JQueryDataTableParamModel param, string headerid)
        {
            try
            {

                //string ForApprove
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1 and headerid=" + Convert.ToInt32(headerid) + "";
                //whereCondition += " and loidate.t_csbu collate SQL_Latin1_General_CP850_CI_AI in (select * from dbo.fn_split(dbo.GETBUFROMUSER('" + objClsLoginInfo.UserName + "'),',')) ";

                string[] columnName = { "PAMHeaderId", "SINNo", "Shop", "ProdGroup", "dbo.GET_USERNAME_BY_PSNO(PreparedBy)", "ReasonofNextIssue", "IssueNo", "Status" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstPam = db.SP_SIN_GETHistroySINDETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from h in lstPam
                           select new[] {
                               Convert.ToString(h.id),
                               Convert.ToString(h.HeaderId),
                               Convert.ToString(h.PAMHeaderId),
                               Convert.ToString(h.SINNo),
                               Convert.ToString(h.Shop),
                               Convert.ToString(h.ProdGroup),
                               Convert.ToString(h.PreparedBy),
                               Convert.ToString(h.ReasonofNextIssue),
                               Convert.ToString(h.IssueNo),
                               Convert.ToString(h.Status),
                               "" //Action
                    }).ToList();
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult SINHistroyHeaderDetails(int id)
        {
            PAM013_Log objPAM013_Log = db.PAM013_Log.Where(x => x.Id == id).FirstOrDefault();
            var lstDepartment = db.COM002.Where(x => x.t_dtyp == 3).Select(x => new BULocWiseCategoryModel { CatDesc = (x.t_dimx + "-" + x.t_desc), CatID = x.t_dimx }).ToList();
            ViewBag.lstShop = lstDepartment;
            ViewBag.Shop = GetDepartmentName(objPAM013_Log.Shop);

            return View("CreateHistroySIN", objPAM013_Log);
        }
        public ActionResult GetSINHeaderHistroyDetails(int SinId, int PAMHeaderId)
        {
            SINHeaderDtl header = new SINHeaderDtl();

            header.HeaderId = SinId.ToString();
            header.PAMHeaderId = PAMHeaderId.ToString();

            PAM001 objPAM001 = db.PAM001.FirstOrDefault(x => x.HeaderId == PAMHeaderId);

            header.Contract = db.Database.SqlQuery<string>(@"SELECT t_cono +'-'+ t_desc FROM    " + LNLinkedServer + ".dbo.ttpctm100175 where t_cono = '" + objPAM001.ContractNo + "'").FirstOrDefault();
            header.Customer = db.Database.SqlQuery<string>(@"SELECT  t_bpid +'-'+ t_nama   from " + LNLinkedServer + ".dbo.ttccom100175  where t_bpid = '" + objPAM001.Customer + "'").FirstOrDefault();
            header.ContractNo = objPAM001.ContractNo;

            PAM013_Log objPAM013 = db.PAM013_Log.FirstOrDefault(x => x.HeaderId == SinId);
            header.Status = objPAM013.Status;
            header.SINNo = objPAM013.SINNo;
            header.RevNo = objPAM013.IssueNo.ToString();
            header.CreatedOn = objPAM013.CreatedOn.ToString();
            header.CreatedBy = Manager.GetPsidandDescription(objPAM013.CreatedBy);

            return PartialView("~/Areas/PAM/Views/Shared/_SINHeaderDetails.cshtml", header);
        }
        public ActionResult GetSINHistroyDepartment(int SinId, string ForApprove, string Status)
        {
            ViewBag.SinId = SinId.ToString();
            ViewBag.Status = Status;
            ViewBag.Acknowledge = clsImplementationEnum.GetDeptAcknowledgements().Select(x => new BULocWiseCategoryModel { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();
            ViewBag.lstDepartment = db.COM002.Where(x => x.t_dtyp == 3).Select(x => new BULocWiseCategoryModel { CatDesc = (x.t_dimx + "-" + x.t_desc), CatID = x.t_dimx }).ToList();
            if (ForApprove == null)
            {
                ViewBag.ForApprove = "false";
            }
            else
            {
                ViewBag.ForApprove = ForApprove;
            }
            return PartialView("~/Areas/PAM/Views/SIN/_GetHistroySINDepartmentGridDataPartial.cshtml");
        }
        public ActionResult loadSINHistroyDeptDataTable(JQueryDataTableParamModel param, int SINId, string ForApprove)
        {
            try
            {
                PAM013_Log objPAM013 = db.PAM013_Log.Where(i => i.HeaderId == SINId).FirstOrDefault();
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                List<SelectItemList> items = new List<SelectItemList>();
                var lstdept = db.COM002.Where(x => x.t_dtyp == 3).ToList();
                var lstEmp = db.COM003.ToList();

                

                string whereCondition = "1=1";
                whereCondition += " and HeaderId= " + SINId.ToString() + " and RecdBy like '%" + Convert.ToString(objClsLoginInfo.UserName) + "%'";

                string[] columnName = { "Department", "RecdBy", "Remarks" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                var lstPam = db.SP_SIN_GETSINDEPARMENTHistroyDETAILS(StartIndex, EndIndex, "", whereCondition).ToList();
                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();
                var res = (from h in lstPam
                           select new[] {
                                    Convert.ToString(h.ROW_NO),
                                    Convert.ToString(h.LineId),
                                    Helper.GenerateHidden(h.LineId, "HeaderId", Convert.ToString(h.HeaderId)),
                                    GenerateAutoComplete(h.LineId, "Department",h.Department,"", "", false,"","Department",true)+""+Helper.GenerateHidden(h.LineId, "ddlDepartment",h.Department.Split('-')[0]),
                                    //GenerateAutoComplete(h.LineId, "RecdBy",h.RecdBy,"", "", false,"","RecdBy",true)+""+Helper.GenerateHidden(h.LineId, "ddlRecdBy", Convert.ToString(h.RecdBy)),
                                    MultiSelectDropdown(items,h.LineId,h.RecdBy,h.Department.Split('-')[0],true,"","","RecdBy"),
                                    GenerateAutoComplete(h.LineId, "Acknowledged",h.Acknowledged,"", "", false,"","Acknowledged")+""+Helper.GenerateHidden(h.LineId, "ddlAcknowledged",h.Acknowledged),
                                    GenerateTextboxFor(h.LineId,"Remarks",h.Remarks,"","",false,"","500",false),
                                    HTMLActionString(h.LineId,"","Edit","Edit Record","fa fa-pencil-square-o","SaveDeptRecord("+h.LineId+", true);"),
                    }).ToList();
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                }, JsonRequestBehavior.AllowGet);
                // }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetSINLinesHistroyDetails(int SinId, string ForApprove)
        {
            ViewBag.SinId = SinId.ToString();
            if (ForApprove == null)
            {
                ViewBag.ForApprove = "false";
            }
            else
            {
                ViewBag.ForApprove = ForApprove;
            }
            return PartialView("~/Areas/PAM/Views/SIN/_GetSINLinesHistroyGridDataPartial.cshtml");
        }
        public ActionResult loadSINLineHistroyDataTable(JQueryDataTableParamModel param, int SINId, string ForApprove)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                PAM013_Log objPAM013 = db.PAM013_Log.Where(i => i.HeaderId == SINId).FirstOrDefault();
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                bool IsEditable = false;
                string whereCondition = "1=1";
                whereCondition += " and HeaderId= " + SINId.ToString();

                //if (objPAM013.Status.ToLower() == clsImplementationEnum.PAMStatus.Issued.GetStringValue().ToLower())
                //{
                //    IsEditable = false;
                //    whereCondition += " and IncludeInSIN = 1";
                //}
                string[] columnName = { "DocDescription", "DocNo", "DocRev", "Notes" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstPam = db.SP_SIN_GETSINLINEHeaderDETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();
                if (ForApprove == null || ForApprove.ToLower() == "false")
                {
                    var res = (from h in lstPam
                               select new[] {
                                    Convert.ToString(h.ROW_NO),
                                    Convert.ToString(h.LineId),
                                    Helper.GenerateHidden(h.LineId, "HeaderId", Convert.ToString(h.HeaderId)),
                                    GenerateTextboxFor(h.LineId,"DocDescription",h.DocDescription,"","",false,"","14",true),
                                    GenerateTextboxFor(h.LineId,"DocNo",h.DocNo,"","",false,"","30",true),
                                    GenerateTextboxFor(h.LineId,"DocRev",h.DocRev,"","",false,"","5",true),
                                    IsEditable ? GenerateTextboxFor(h.LineId,"Notes",h.Notes,"","",false,"","500",false) : GenerateTextboxFor(h.LineId,"Notes",h.Notes,"","",false,"","500",true),
                                    "",
                                    IsEditable ? GetChecklistCheckStyle(h.LineId,objPAM013.Status,h.IncludeInSIN, false, "UpdateAccepted(this," + Convert.ToString(h.LineId) + ")") : GetChecklistCheckStyle(h.LineId,objPAM013.Status,h.IncludeInSIN, true, "UpdateAccepted(this," + Convert.ToString(h.LineId) + ")"),
                                    IsEditable ? HTMLActionString(h.LineId,"","Edit","Edit Record","fa fa-pencil-square-o","SaveLineRecord("+h.LineId+");") : "<i id=\"\" name=\"\" style='cursor: pointer;margin-left:5px;opacity:0.5;' Title=\"\" class=\"fa fa-pencil-square-o\" ></i>",
                                    }).ToList();

                    //res.Insert(0, newRecord);
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                        whereCondition = whereCondition,
                        strSortOrder = "",
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var res = (from h in lstPam
                               select new[] {
                                    Convert.ToString(h.ROW_NO),
                                    Convert.ToString(h.LineId),
                                    Helper.GenerateHidden(h.LineId, "HeaderId", Convert.ToString(h.HeaderId)),
                                    GenerateTextboxFor(h.LineId,"DocDescription",h.DocDescription,"","",false,"","14",true),
                                    GenerateTextboxFor(h.LineId,"DocNo",h.DocNo,"","",false,"","30",true),
                                    GenerateTextboxFor(h.LineId,"DocRev",h.DocRev,"","",false,"","5",true),
                                    GenerateTextboxFor(h.LineId,"Notes",h.Notes,"","",false,"","500",true),
                                    "",
                                    GetChecklistCheckStyle(h.LineId,objPAM013.Status,h.IncludeInSIN, true),
                    }).ToList();
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                        whereCondition = whereCondition,
                        strSortOrder = "",
                    }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion


        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult SINIssuerDetails(int Id)
        {
            ViewBag.IsDisplayOnly = false;
            PAM013 objPAM013 = db.PAM013.FirstOrDefault(x => x.HeaderId == Id);
            var lstDepartment = db.COM002.Where(x => x.t_dtyp == 3).Select(x => new BULocWiseCategoryModel { CatDesc = (x.t_dimx + "-" + x.t_desc), CatID = x.t_dimx }).ToList();
            ViewBag.lstShop = lstDepartment;
            objPAM013.Shop = GetDepartmentName(objPAM013.Shop);
            return View(objPAM013);
        }
        #endregion

        [SessionExpireFilter]
        [UserPermissions]
        public ActionResult DisplaySIN()
        {
            ViewBag.IsDisplayOnly = true;
            return View("Index");
        }

        public ActionResult SINDisplayHeader(int id)
        {
            ViewBag.IsDisplayOnly = true;
            PAM013 objPAM013 = db.PAM013.Where(x => x.HeaderId == id).FirstOrDefault();
            var lstDepartment = db.COM002.Where(x => x.t_dtyp == 3).Select(x => new BULocWiseCategoryModel { CatDesc = (x.t_dimx + "-" + x.t_desc), CatID = x.t_dimx }).ToList();
            ViewBag.lstShop = lstDepartment;
            ViewBag.Shop = GetDepartmentName(objPAM013.Shop);

            return View("CreateSIN", objPAM013);
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();

            if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
            {

                var lstResult = db.SP_SIN_GETSINDETAILS(1, int.MaxValue, strSortOrder, whereCondition)
                                .Select(x => new
                                {
                                    x.SINNo,
                                    x.Shop,
                                    x.ProdGroup,
                                    x.PreparedBy,
                                    x.ReasonofNextIssue,
                                    x.IssueNo,
                                    x.Status
                                }).ToList();

                if (lstResult != null && lstResult.Count > 0)
                {
                    string str = Helper.GenerateExcel(lstResult, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = str;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Data not available.";
                }
            }
            else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
            {

            }
            else if (gridType == clsImplementationEnum.GridType.SUB_LINES.GetStringValue())
            {

                var lstResult = db.SP_SIN_GETSINLINEDETAILS(1, int.MaxValue, strSortOrder, whereCondition)
                                 .Select(x => new
                                 {
                                     x.DocDescription,
                                     x.DocNo,
                                     x.DocRev,
                                     x.Notes,
                                     x.IncludeInSIN,
                                 }).ToList();
                if (lstResult != null && lstResult.Count > 0)
                {
                    string str = Helper.GenerateExcel(lstResult, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = str;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Data not available.";
                }
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
    }



    public class SINHeaderDtl
    {
        public string Contract { get; set; }
        public string Customer { get; set; }
        public string ContractNo { get; set; }
        public string SINNo { get; set; }
        public string RevNo { get; set; }
        public string CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public string Status { get; set; }
        public string HeaderId { get; set; }
        public string PAMHeaderId { get; set; }
    }
}