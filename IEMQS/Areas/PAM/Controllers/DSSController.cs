﻿using IEMQS.Areas.PAM.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Areas.Utility.Models;
using IEMQS.DESCore;
using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.PAM.Controllers
{
    public class DSSController : clsBase
    {

        #region Utility
        public static string GenerateTextboxFor(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string maxLength = "", bool disabled = false)
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (columnName.ToLower() == "pages")
            {
                className += " numeric";
            }
            htmlControl = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id=" + inputID + " data-lineid='" + rowId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + " maxlength='" + maxLength + "' " + (disabled ? "disabled" : "") + "  />";

            return htmlControl;
        }

        public static string GenerateGridButton(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";


            htmlControl = "<a id='" + inputID + "' name='" + inputID + "' class='btn btn-outline btn-circle btn-sm blue' " + onClickEvent + " ><i class='" + className + "'></i> " + buttonName + "</a>";

            //htmlControl = "<i  data-modal='' id='" + inputID + "' name='" + inputID + "' style='cursor:Pointer;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";

            return htmlControl;
        }

        public string GetChecklistCheckStyle1(string status, bool? isCheck)
        {
            if (isCheck == null)
                isCheck = false;
            if (status == clsImplementationEnum.PAMStatus.Approved.GetStringValue())
            {
                return "<input type='checkbox' name='chkChecklistCheck' id='chkChecklistCheck' disabled= 'disabled'  class='make-switch col-md-3' checked='checked'    data-on-text='Yes' data-off-text='No' data-on-color='success' data-off-color='danger' data-size='normal' />";
            }

            else if (status != clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue() && isCheck == true)
            {
                return "<input type='checkbox' name='chkChecklistCheck' id='chkChecklistCheck'  class='make-switch col-md-3' checked='checked'  data-on-text='Yes' data-off-text='No' data-on-color='success' data-off-color='danger' data-size='normal' />";
            }
            else if (status != clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue() && isCheck != true)
            {
                return "<input type='checkbox' name='chkChecklistCheck' id='chkChecklistCheck'   class='make-switch col-md-3'  data-on-text='Yes' data-off-text='No' data-on-color='success' data-off-color='danger' data-size='normal' />";
            }
            else if (isCheck == true)
            {
                return "<input type='checkbox' name='chkChecklistCheck' id='chkChecklistCheck' disabled= 'disabled'  class='make-switch col-md-3' checked='checked'    data-on-text='Yes' data-off-text='No' data-on-color='success' data-off-color='danger' data-size='normal' />";
            }
            else
            {
                return "<input type='checkbox' name='chkChecklistCheck' id='chkChecklistCheck'  class='make-switch col-md-3' disabled= 'disabled'    data-on-text='Yes' data-off-text='No' data-on-color='success' data-off-color='danger' data-size='normal' />";
            }
        }

        public string GetChecklistCheckStyle(int LineId, string status, bool? isCheck, bool? isDisabled)
        {
            string rtnchecjbox = "";
            if (isCheck == null)
                isCheck = false;
            rtnchecjbox = "<input type='checkbox' style='text-align:center' name='chkChecklistCheck" + LineId.ToString() + "' id='chkChecklistCheck" + LineId.ToString() + "' class='make-switch col-md-3' data-on-text='Yes' data-off-text='No' data-on-color='success' data-off-color='danger' data-size='normal' onchange='UpdateAccepted(this," + LineId.ToString() + ")' ";
            if (isDisabled == true)
            {
                rtnchecjbox += " disabled= 'disabled' ";
            }
            if (isCheck == true)
            {
                rtnchecjbox += "checked='checked' ";
            }
            //if (status.ToLower() == clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue().ToLower())
            //{
            //    if (isCheck == true)
            //    {
            //        rtnchecjbox += " checked='checked' ";
            //    }
            //}
            //else
            //{
            //    rtnchecjbox += " disabled= 'disabled' ";
            //}
            rtnchecjbox += "/>";
            return rtnchecjbox;
        }


        #endregion

        string check = string.Empty;
        int flag = 0;
        PAMActionModel Action = new PAMActionModel();

        // GET: PAM/DSS
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetDSSGridDataPartial(string status, string ForApprove)
        {
            ViewBag.Status = status;
            ViewBag.ForApprove = ForApprove;
            return PartialView("_GetDSSGridDataPartial");
        }

        public ActionResult loadDSSDataTable(JQueryDataTableParamModel param, string status, string ForApprove)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                //whereCondition += " and loidate.t_csbu collate SQL_Latin1_General_CP850_CI_AI in (select * from dbo.fn_split(dbo.GETBUFROMUSER('" + objClsLoginInfo.UserName + "'),',')) ";
                if (Convert.ToBoolean(ForApprove))
                {
                    if (status.ToUpper() == "PENDING")
                    {
                        whereCondition += " and status in ('" + clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue() + "')";
                    }
                    else
                    {
                        whereCondition += " and status in ('" + clsImplementationEnum.PAMStatus.Approved.GetStringValue() + "','" + clsImplementationEnum.PAMStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.IMBStatus.Returned.GetStringValue() + "','" + clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue() + "')";
                    }
                }
                else
                {
                    if (status.ToUpper() == "PENDING")
                    {
                        whereCondition += " and status in ('" + clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue() + "','" + clsImplementationEnum.PAMStatus.Draft.GetStringValue() + "')";
                    }
                    else
                    {
                        whereCondition += " and status in ('" + clsImplementationEnum.PAMStatus.Approved.GetStringValue() + "','" + clsImplementationEnum.PAMStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.IMBStatus.Returned.GetStringValue() + "','" + clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue() + "')";
                    }
                }
                string[] columnName = { "PAM", "DSSNo", "REvNo", "Status", "MKT" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                var lstPam = db.SP_DSS_GETDSSDETAILS(StartIndex, EndIndex, "", whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                var res = from h in lstPam
                          select new[] {
                              h.DssId.ToString(),
                               h.HeaderId.ToString(),
                               h.PAM.ToString(),
                               h.DSSNo.ToString(),
                               "R" + h.REvNo.ToString(),
                               h.Status.ToString(),
                               "" //Action
                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = "",
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ShowTimeline(int DssId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.TimelineTitle = "DSS History";
            model.Title = "DSS";
            PAM006 objPAM006 = db.PAM006.Where(x => x.DSSId == DssId).FirstOrDefault();
            model.CreatedBy = Manager.GetUserNameFromPsNo(objPAM006.CreatedBy);
            model.CreatedOn = objPAM006.CreatedOn;

            model.ApprovedBy = Manager.GetUserNameFromPsNo(objPAM006.ApprovedBy);
            model.ApprovedOn = objPAM006.ApprovedOn;


            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }

        public ActionResult GetDSSHeaderDetails(int DssId, int HeaderId)
        {
            DssHeaderDtl header = new DssHeaderDtl();

            header.DssId = DssId.ToString();
            header.HeaderId = HeaderId.ToString();

            PAM001 objPAM001 = db.PAM001.FirstOrDefault(x => x.HeaderId == HeaderId);

            header.Contract = db.Database.SqlQuery<string>(@"SELECT t_cono +'-'+ t_desc FROM " + LNLinkedServer + ".dbo.ttpctm100175 where t_cono = '" + objPAM001.ContractNo + "'").FirstOrDefault();
            header.Customer = db.Database.SqlQuery<string>(@"SELECT  t_bpid +'-'+ t_nama   from " + LNLinkedServer + ".dbo.ttccom100175  where t_bpid = '" + objPAM001.Customer + "'").FirstOrDefault();
            header.ContractNo = objPAM001.ContractNo;

            PAM006 objPAM006 = db.PAM006.FirstOrDefault(x => x.DSSId == DssId);
            header.Status = objPAM006.Status;
            header.DSSNo = objPAM006.DSSNo;
            header.RevNo = "R" + objPAM006.RevNo.ToString();
            header.CreatedOn = objPAM006.CreatedOn.ToString();
            if (objPAM006.ENGComments == null)
            {
                header.Comments = "";
                ViewBag.comments = "";

            }
            else
            {
                header.Comments = objPAM006.ENGComments.ToString();
                ViewBag.comments = objPAM006.ENGComments.ToString();
            }
            if (objPAM006.MKTComments == null)
            {
                header.MKTComments = "";
                ViewBag.MKTComments = "";
            }
            else
            {
                header.MKTComments = objPAM006.MKTComments.ToString();
                ViewBag.MKTComments = objPAM006.MKTComments.ToString();
            }
            var status = db.PAM007.Where(m => m.DSSId == DssId).FirstOrDefault();
            if (status == null)
            {
                ViewBag.status = "false";
            }
            else
            {
                ViewBag.status = "true";
            }

            return PartialView("~/Areas/PAM/Views/Shared/_DSSHeaderDetails.cshtml", header);
        }

        public ActionResult GetLineDetailsbyId(int DssId, int LineId)
        {
            PAM006 objPAM006 = db.PAM006.Where(i => i.DSSId == DssId).FirstOrDefault();
            PAM007 objPAM007 = new PAM007();
            if (LineId == 0)
            {
                objPAM007.DSSId = DssId;
                objPAM007.HeaderId = objPAM006.HeaderId;
                objPAM007.DSSNo = objPAM006.DSSNo;
            }
            else
            {
                objPAM007 = db.PAM007.Where(i => i.LineId == LineId).FirstOrDefault();
            }
            ViewBag.Status = objPAM006.Status;
            return PartialView("_AddDSSLines", objPAM007);
        }

        [HttpPost]
        public ActionResult SaveLine(PAM007 pam007, bool hasAttachments, Dictionary<string, string> Attach)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsgWithStatus = new clsHelper.ResponseMsgWithStatus();
            PAM007 objPAM007 = new PAM007();
            try
            {
                if (hasAttachments)
                {
                    PAM006 objPAM006 = db.PAM006.Where(i => i.DSSId == pam007.DSSId).FirstOrDefault();
                    if (pam007.LineId > 0)
                    {
                        objPAM007 = db.PAM007.Where(i => i.LineId == pam007.LineId).FirstOrDefault();
                        //For Edit Mode
                        objPAM007.Description = pam007.Description;
                        objPAM007.DocNo = pam007.DocNo;
                        objPAM007.DocRev = pam007.DocRev;
                        objPAM007.DSSId = pam007.DSSId;

                        int n = 0;
                        bool isNumeric = int.TryParse(pam007.Pages, out n);
                        if (!isNumeric)
                            objPAM007.Pages = string.Empty;
                        else
                            objPAM007.Pages = pam007.Pages;

                        objPAM007.EditedBy = objClsLoginInfo.UserName;
                        objPAM007.EditedOn = DateTime.Now;
                        if (string.Equals(objPAM006.Status, clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue()))
                        {
                            objPAM006.RevNo = objPAM006.RevNo + 1;
                            objPAM006.Status = clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue();
                            foreach (var item in objPAM006.PAM007)
                            {
                                item.Remarks = null;
                                item.IsAccepted = false;
                            }
                        }
                        //db.PAM007.Add(objPAM007);
                        db.SaveChanges();

                        var folderPath = "PAM007/" + objPAM007.LineId;
                        Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                        objResponseMsgWithStatus.Key = true;
                        objResponseMsgWithStatus.Value = "Line Updated successfully";
                    }
                    else
                    {
                        //Add New Record
                        objPAM007.CreatedBy = objClsLoginInfo.UserName;
                        objPAM007.CreatedOn = DateTime.Now;
                        objPAM007.Description = pam007.Description;
                        objPAM007.DocNo = pam007.DocNo;
                        objPAM007.DocRev = pam007.DocRev;
                        objPAM007.DSSId = pam007.DSSId;
                        objPAM007.DSSNo = pam007.DSSNo;
                        objPAM007.HeaderId = pam007.HeaderId;

                        int n = 0;
                        bool isNumeric = int.TryParse(pam007.Pages, out n);
                        if (!isNumeric)
                            objPAM007.Pages = string.Empty;
                        else
                            objPAM007.Pages = pam007.Pages;
                        
                        if (string.Equals(objPAM006.Status, clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue()))
                        {
                            objPAM006.RevNo = objPAM006.RevNo + 1;
                            objPAM006.Status = clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue();
                            db.SaveChanges();
                        }

                        db.PAM007.Add(objPAM007);
                        db.SaveChanges();
                        var folderPath = "PAM007/" + objPAM007.LineId;
                        Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                        objResponseMsgWithStatus.Key = true;
                        objResponseMsgWithStatus.Value = "Line Added successfully";
                    }
                }
                else
                {
                    objResponseMsgWithStatus.Key = false;
                    objResponseMsgWithStatus.Value = "DocumentRequired";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsgWithStatus.Key = false;
                objResponseMsgWithStatus.Value = ex.Message;
            }

            return Json(objResponseMsgWithStatus);
        }

        public ActionResult SaveDocLines(FormCollection fc, int Id = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int LineId = Id;
                int DSSId = Convert.ToInt32(fc["DSSId" + Id]);
                PAM006 objPAM006 = db.PAM006.Where(x => x.DSSId == DSSId).FirstOrDefault();
                PAM007 objPAM007 = null;
                if (LineId > 0)
                {
                    objPAM007 = db.PAM007.Where(x => x.LineId == LineId).FirstOrDefault();
                }
                else
                {
                    objPAM007 = new PAM007();
                }
                objPAM007.HeaderId = objPAM006.HeaderId;
                objPAM007.DSSId = DSSId;
                objPAM007.Description = fc["Description" + LineId];
                objPAM007.DocNo = fc["DocNo" + LineId];
                objPAM007.DocRev = fc["DocRev" + LineId];

                int n = 0;
                bool isNumeric = int.TryParse(Convert.ToString(fc["Pages" + LineId]), out n);
                if (!isNumeric)
                    objPAM007.Pages = string.Empty;
                else
                    objPAM007.Pages = fc["Pages" + LineId];
               
                objPAM007.Remarks = fc["Remarks" + LineId];
                objPAM007.DSSNo = objPAM006.DSSNo;
                if (LineId > 0)
                {
                    objPAM007.EditedBy = objClsLoginInfo.UserName;
                    objPAM007.EditedOn = DateTime.Now;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                }
                else
                {
                    objPAM007.CreatedBy = objClsLoginInfo.UserName;
                    objPAM007.CreatedOn = DateTime.Now;
                    db.PAM007.Add(objPAM007);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
                }
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public ActionResult AddDSS(int id)
        {
            PAM006 objPAM006 = db.PAM006.FirstOrDefault(x => x.DSSId == id);

            var role = (from a in db.ATH001
                        join b in db.ATH004 on a.Role equals b.Id
                        where a.Employee.Equals(objClsLoginInfo.UserName.Trim(), StringComparison.OrdinalIgnoreCase)
                        select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();
            if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.MKT3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
            {
                ViewBag.Role = "mkt";
            }

            ViewBag.headerid = objPAM006.HeaderId;
            var person = db.SP_PAM_DSS_PersonDtl(id).FirstOrDefault();
            ViewBag.Marketing = person.MKTName;
            ViewBag.ESTName = person.ESTName;
            ViewBag.PMGName = person.PMGName;
            // ViewBag.DesignLead = person.DesignerName;
            ViewBag.DesignLead = Manager.GetUserNameFromPsNo(objPAM006.ApprovedBy);

            ViewBag.ESTDate = person.ESTDate;
            ViewBag.MKTDate = person.MKTDate;
            ViewBag.PMGDate = person.PMGDate;
            // ViewBag.DesignLead = person.DesignerName;
            ViewBag.DesignerDate = person.DesignerDate;

            #region Prospect & Firm Handover Button
            PAM001 objPAM001 = db.PAM001.FirstOrDefault(x => x.HeaderId == objPAM006.HeaderId);
            var Prospect = clsImplementationEnum.HandoverReportType.Prospect.GetStringValue();
            var Firm = clsImplementationEnum.HandoverReportType.Firm.GetStringValue();
            var objPAM008Prospect = db.PAM008.Where(i => i.HeaderId == objPAM001.HeaderId && i.HandoverType.ToLower() == Prospect.ToLower()).FirstOrDefault();
            if (((objPAM001.LOINo == null || objPAM001.LOINo == string.Empty) || objPAM008Prospect != null) &&
                (objPAM001.Status.ToLower() == clsImplementationEnum.NDETechniqueStatus.SentForApproval.GetStringValue().ToLower() ||
                objPAM001.Status.ToLower() == clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue().ToLower()) &&
                (objPAM006.Status.ToLower() == clsImplementationEnum.NDETechniqueStatus.SentForApproval.GetStringValue().ToLower() ||
                objPAM006.Status.ToLower() == clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue().ToLower()))

            {
                ViewBag.DisplayProspectHandOver = true;
            }
            else
            {
                ViewBag.DisplayProspectHandOver = false;
            }

            if ((objPAM001.LOINo != null && objPAM001.LOINo != string.Empty) &&
                (objPAM001.Status.ToLower() == clsImplementationEnum.NDETechniqueStatus.SentForApproval.GetStringValue().ToLower() ||
                objPAM001.Status.ToLower() == clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue().ToLower()))
            {
                ViewBag.DisplayFirmHandOver = true;
            }
            else
            {
                ViewBag.DisplayFirmHandOver = false;
            }
            #endregion

            return View(objPAM006);
        }

        public ActionResult DSSAttachmentPartial(int? id, string status = "")
        {
            ViewBag.Status = status;
            PAM007 objPAM005 = db.PAM007.Where(x => x.LineId == id).FirstOrDefault();
            return PartialView("_DSSAttachmentPartial", objPAM005);
        }

        [HttpPost]
        public ActionResult SaveDSSAttachment(string id, bool hasAttachments, Dictionary<string, string> Attach)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var folderPath = "PAM007/" + id;
                Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                objResponseMsg.Key = true;
                objResponseMsg.Value = "File Uploaded Successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDSSLinesDetails(int DssId, string ForApprove)
        {
            ViewBag.DssId = DssId.ToString();
            int PAMHeaderId = db.PAM006.Where(x => x.DSSId == DssId).Select(x => x.HeaderId).FirstOrDefault();
            ViewBag.HeaderId = PAMHeaderId;
            ViewBag.ContractNo = db.PAM001.Where(x => x.HeaderId == PAMHeaderId).Select(x => x.ContractNo).FirstOrDefault();

            if (ForApprove == null)
            {
                ViewBag.ForApprove = "false";
            }
            else
            {
                ViewBag.ForApprove = ForApprove;
            }
            return PartialView("_GetDSSLinesGridDataPartial");
        }


        public ActionResult loadDSSLineDataTable(JQueryDataTableParamModel param, int DssId, string ForApprove)
        {
            try
            {
                PAM006 objPAM006 = db.PAM006.Where(i => i.DSSId == DssId).FirstOrDefault();
                var Contract = objPAM006.PAM.Split('-').First();
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                bool isEditable = true;
                string whereCondition = "1=1";
                whereCondition += " and DssId= " + DssId.ToString();
                if (objPAM006.Status.ToLower() == clsImplementationEnum.PAMStatus.Approved.GetStringValue().ToLower() || objPAM006.Status.ToLower() == clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue().ToLower())
                {
                    isEditable = false;
                }
                string[] columnName = { "Description", "DocNo", "DocRev", "Pages" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstPam = db.SP_DSS_GETDSSLINEDETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();
                if (ForApprove == null || ForApprove.ToLower() == "false")
                {

                    int newRecordId = 0;
                    var newRecord = new[] {
                                    "0",
                                    "",
                                    Helper.GenerateHidden(newRecordId, "DssId", Convert.ToString(DssId)),
                                    Helper.GenerateHidden(newRecordId, "HeaderId", Convert.ToString(objPAM006.HeaderId)),
                                    isEditable == true ? GenerateTextboxFor(newRecordId,"Description","","","",false,"","200",false) : GenerateTextboxFor(newRecordId,"Description","","","",false,"","15",true),
                                    isEditable == true ? GenerateTextboxFor(newRecordId,"DocNo","","","",false,"","200",false) : GenerateTextboxFor(newRecordId,"DocNo","","","",false,"","15",true),
                                    isEditable == true ? GenerateTextboxFor(newRecordId,"DocRev","R0","","",false,"","15",false) : GenerateTextboxFor(newRecordId,"DocRev","R1","","",false,"","15",true),
                                    isEditable == true ? GenerateTextboxFor(newRecordId,"Pages","","","",false,"","15",false) : GenerateTextboxFor(newRecordId,"Pages","","","",false,"","15",true),
                                    "",
                                    isEditable == true ? GenerateTextboxFor(newRecordId,"Remarks",string.Empty,"","",false,"","500",false) : GenerateTextboxFor(newRecordId,"Remarks",string.Empty,"","",false,"","500",true),//Observation 15939 on 31-07-2018
                                    isEditable == true ? GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveLineDocRecord(this,0);" ) : GenerateTextboxFor(newRecordId,"ReturnRemarks","","","",false,"","500",true),
                                    "false"
                                };

                    //var existing = (new clsFileUpload()).GetDocuments("PAM007/ContractWiseDocuments/" + Contract);

                    var res = (from h in lstPam
                               select new[] {
                                    Convert.ToString(h.LineId),
                                    !string.IsNullOrEmpty(h.DocNo)?Convert.ToString(h.DocNo.Split('.').FirstOrDefault()):"",
                                    Helper.GenerateHidden(h.LineId, "DssId", Convert.ToString(h.DSSId)),
                                    Helper.GenerateHidden(h.LineId, "HeaderId", Convert.ToString(objPAM006.HeaderId)),
                                    isEditable == true ? GenerateTextboxFor(h.LineId,"Description",h.Description,"","",false,"","200",false) : GenerateTextboxFor(h.LineId,"Description",h.Description,"","",false,"","200",true),
                                    isEditable == false ? GenerateTextboxFor(h.LineId,"DocNo",h.DocNo,"","",false,"","200",false) : GenerateTextboxFor(h.LineId,"DocNo",h.DocNo,"","",false,"","200",true),
                                    isEditable == true ? GenerateTextboxFor(h.LineId,"DocRev",h.DocRev,"","",false,"","15",false) : GenerateTextboxFor(h.LineId,"DocRev",h.DocRev,"","",false,"","15",true),
                                    isEditable == true ? GenerateTextboxFor(h.LineId,"Pages",h.Pages,"","",false,"","15",false) : GenerateTextboxFor(h.LineId,"Pages",h.Pages,"","",false,"","15",true),
                                    "",
                                    isEditable == true ? GenerateTextboxFor(h.LineId,"Remarks",h.Remarks,"","",false,"","500",false) : GenerateTextboxFor(h.LineId,"Remarks",h.Remarks,"","",false,"","500",true),//Observation 15939 on 31-07-2018
                                    isEditable == true ? "<center><i style='cursor:pointer;' title=\"Edit Line\" onclick=\"SaveLineDocRecord(this,"+h.LineId+")\" class='fa fa-edit iconspace'></i><i style='cursor:pointer;' title=\"Delete Line\" onclick=\"DeleteLines("+h.LineId+")\" class='fa fa-trash iconspace'></i></center>" : "",
                                    //existing.Any(k => k.NameWithoutExt == h.DocNo).ToString()
                                    (h.Attachments.ToLower()=="yes").ToString()
                                    }).ToList();

                    res.Insert(0, newRecord);

                    //var res = from h in lstPam
                    //          select new[] {
                    //          h.LineId.ToString(),
                    //          (++srno).ToString(),
                    //          h.DSSId.ToString(),
                    //          h.Description.ToString(),
                    //           h.DocNo.ToString(),
                    //           h.DocRev.ToString(),
                    //           h.Pages.ToString(),
                    //           "" //Action
                    //};
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                        whereCondition = whereCondition,
                        strSortOrder = strSortOrder,
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    //var existing = (new clsFileUpload()).GetDocuments("PAM007/ContractWiseDocuments/" + Contract);

                    var res = (from h in lstPam
                               select new[] {
                                    Convert.ToString(h.LineId),
                                    Convert.ToString(h.ROW_NO),
                                    Helper.GenerateHidden(h.LineId, "DssId", Convert.ToString(h.DSSId)),
                                    Helper.GenerateHidden(h.LineId, "HeaderId", Convert.ToString(objPAM006.HeaderId)),
                                    isEditable == true ? GenerateTextboxFor(h.LineId,"Description",h.Description,"","",false,"","200",false) : GenerateTextboxFor(h.LineId,"Description",h.Description,"","",false,"","200",true),
                                    isEditable == true ? GenerateTextboxFor(h.LineId,"DocNo",h.DocNo,"","",false,"","200",false) : GenerateTextboxFor(h.LineId,"DocNo",h.DocNo,"","",false,"","200",true),
                                    isEditable == true ? GenerateTextboxFor(h.LineId,"DocRev",h.DocRev,"","",false,"","15",false) : GenerateTextboxFor(h.LineId,"DocRev",h.DocRev,"","",false,"","15",true),
                                    isEditable == true ? GenerateTextboxFor(h.LineId,"Pages",h.Pages,"","",false,"","15",false) : GenerateTextboxFor(h.LineId,"Pages",h.Pages,"","",false,"","15",true),
                                    "",
                                     GetChecklistCheckStyle(h.LineId,objPAM006.Status,h.IsAccepted), //Yes/No
                                    GetSectionRemarkStyle(h.LineId,objPAM006.Status,h.Remarks), //Remarks
                                    isEditable == true ? "<center><i style='cursor:pointer;' title=\"Edit Line\" onclick=\"SaveLineDocRecord(this,"+h.LineId+")\" class='fa fa-edit iconspace'></i><i style='cursor:pointer;' title=\"Delete Line\" onclick=\"DeleteLines("+h.LineId+")\" class='fa fa-trash iconspace'></i></center>" : "",
                                    //existing.Any(k => k.NameWithoutExt == h.DocNo).ToString()
                                    (h.Attachments.ToLower()=="yes").ToString()
                                    }).ToList();
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                        whereCondition = whereCondition,
                        strSortOrder = strSortOrder,
                    }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #region Delete single or Multiple

        public ActionResult DeleteDSSLines(string strLineIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrayLineIds = strLineIds.Split(',').ToArray();
                for (int i = 0; i < arrayLineIds.Length; i++)
                {
                    int LineId = Convert.ToInt32(arrayLineIds[i]);
                    objResponseMsg = DeleteDSSLine(LineId);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg);
        }

        public clsHelper.ResponseMsg DeleteDSSLine(int LineId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (LineId > 0)
                {
                    PAM007 objPAM007 = db.PAM007.Where(x => x.LineId == LineId).FirstOrDefault();
                    if (objPAM007 != null)
                    {

                        string ContractNo = db.PAM001.Where(x => x.HeaderId == objPAM007.HeaderId).Select(x => x.ContractNo).FirstOrDefault();
                        string sourcePath = "PAM007/ContractWiseDocuments/" + ContractNo;

                        FileUploadController _objFUC = new FileUploadController();
                        string Table_Name = "PAM007/" + ContractNo;
                        var data = _objFUC.GetAllDocumentsByTableNameTableId(Table_Name, 1).FirstOrDefault(f=> Path.GetFileNameWithoutExtension(f.Document_name) == objPAM007.DocNo);
                        if(data != null)
                        {
                            _objFUC.DeleteFileOnFCSServerAsync(data.DocumentMappingId, IEMQS.DESServices.CommonService.GetUseIPConfig);
                        }

                        //var lstDocuments = (new clsFileUpload()).GetDocuments(sourcePath, false, "", objPAM007.DocNo, false);
                        //if (lstDocuments != null && lstDocuments.Count() > 0)
                        //{
                        //    foreach (var item in lstDocuments)
                        //    {
                        //        (new clsFileUpload()).DeleteFileAsync(sourcePath, item.Name);
                        //    }
                        //}

                        //var existing = lstDocuments.Select(x => new { DocName = x.Name, DocExtention = x.Name.Split('.').Last() }).ToList().OrderBy(x => x.DocName);
                        //string documentName = objPAM007.DocNo;
                        //if (existing.Any(x => x.DocName == documentName + "." + x.DocExtention))
                        //{
                        //    // fileName = documentName + "." + existing.Where(x => x.DocName == documentName).Select(x => x.DocExtention).FirstOrDefault();
                        //    string fileName = existing.Where(x => x.DocName == documentName + "." + x.DocExtention).Select(x => x.DocName).FirstOrDefault();
                        //    (new clsFileUpload()).DeleteFileAsync(sourcePath, fileName);
                        //}

                        //string deletefolderPath = "PAM007/" + objPAM007.LineId;
                        //var lstTVL011RefDoc = (new clsFileUpload()).GetDocuments(deletefolderPath, false, "", objPAM007.DocNo, false);
                        //foreach (var filename in lstTVL011RefDoc)
                        //{
                        //    (new clsFileUpload()).DeleteFileAsync(deletefolderPath, filename.Name);
                        //}

                        db.PAM007.Remove(objPAM007);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Line deleted successfully";
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Line is not available for delete";
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Line is not available for delete";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return objResponseMsg;
        }

        #endregion

        #region ForApproval

        public ActionResult SendForApproval(int DssId, string Comments)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            PAM006 objPAM006 = new PAM006();
            try
            {
                if (DssId > 0)
                {
                    objPAM006 = db.PAM006.Where(i => i.DSSId == DssId).FirstOrDefault();
                    if (objPAM006 != null)
                    {
                        if (objPAM006.PAM007.Count > 0)
                        {
                            objPAM006.MKTComments = Comments;
                            objPAM006.Status = clsImplementationEnum.NDETechniqueStatus.SentForApproval.GetStringValue();
                            db.SaveChanges();

                            ////remove files under this contract folder
                            //string ContractNo = db.PAM001.Where(x => x.HeaderId == objPAM006.HeaderId).Select(x => x.ContractNo).FirstOrDefault();
                            //string deletefolderPath = "PAM007/ContractWiseDocuments/" + ContractNo;
                            //var lstPAM007Doc = (new clsFileUpload()).GetDocuments(deletefolderPath, true).Select(x => x.Name).ToList();
                            //foreach (var filename in lstPAM007Doc)
                            //{
                            //    (new clsFileUpload()).DeleteFileAsync(deletefolderPath, filename.ToString());
                            //}

                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.PTTechniqueMeassage.SentForApproval.ToString();
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Please Add Atleast One Line For Send For Approval";
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg);
        }

        [SessionExpireFilter]
        public ActionResult ApproveDSS()
        {
            return View();
        }

        [SessionExpireFilter]
        public ActionResult ApproveDSSDetails(int id)
        {
            PAM006 objPAM006 = db.PAM006.FirstOrDefault(x => x.DSSId == id);

            var person = db.SP_PAM_DSS_PersonDtl(id).FirstOrDefault();
            ViewBag.Marketing = person.MKTName;
            ViewBag.ESTName = person.ESTName;
            ViewBag.PMGName = person.PMGName;
            ViewBag.DesignLead = person.DesignerName;

            ViewBag.ESTDate = person.ESTDate;
            ViewBag.MKTDate = person.MKTDate;
            ViewBag.PMGDate = person.PMGDate;
            // ViewBag.DesignLead = person.DesignerName;
            ViewBag.DesignerDate = person.DesignerDate;

            //check is DIS create or not
            PAM011 objPAM011 = db.PAM011.Where(x => x.PAMHeaderId == objPAM006.HeaderId).FirstOrDefault();
            if (objPAM011 != null)
            {
                ViewBag.IsDISCreated = "true";
            }
            else
            {
                ViewBag.IsDISCreated = "false";
            }
            return View(objPAM006);
        }

        public string GetSectionRemarkStyle(int LineId, string status, string sectionRemarks)
        {
            string rtnremarks = "";
            if (check == "True")
            {
                rtnremarks += "<input type='text'  id='txtSectionRemark" + LineId.ToString() + "' name='txtSectionRemark" + LineId.ToString() + "' style='width:100%'  class='form-control input-sm  input-inline sectionTextbox' value='" + sectionRemarks + "' onblur='UpdateRemarks(this," + LineId.ToString() + ")' readonly='readonly' maxlength='500' ";
            }
            else
            {
                rtnremarks += "<input type='text'  id='txtSectionRemark" + LineId.ToString() + "' name='txtSectionRemark" + LineId.ToString() + "' style='width:100%'  class='form-control input-sm  input-inline sectionTextbox' value='" + sectionRemarks + "' onblur='UpdateRemarks(this," + LineId.ToString() + ")' maxlength='500' ";
            }
            if (status.ToLower() != clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue().ToLower())
            {
                rtnremarks += " disabled='disabled' ";
            }
            rtnremarks += " />";
            return rtnremarks;
        }

        public string GetChecklistCheckStyle(int LineId, string status, bool? isCheck)
        {
            //Session["ischeck"] = isCheck;
            check = Convert.ToString(isCheck);
            string rtnchecjbox = "";
            // string rtnremarks = "";
            if (isCheck == null)
                isCheck = false;
            rtnchecjbox = "<input type='checkbox' name='chkChecklistCheck" + LineId.ToString() + "' id='chkChecklistCheck" + LineId.ToString() + "' class='make-switch col-md-3' data-on-text='Yes' data-off-text='No' data-on-color='success' data-off-color='danger' data-size='normal' onchange='UpdateAccepted(this," + LineId.ToString() + ")' ";
            if (isCheck == true)
            {
                rtnchecjbox += " checked='checked' ";

            }
            if (status.ToLower() != clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue().ToLower())
            {
                rtnchecjbox += " disabled= 'disabled' ";
            }
            rtnchecjbox += "/>";
            return rtnchecjbox;
        }

        public ActionResult UpdateAccepted(int LineId, bool IsAccepted)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PAM007 objPAM007 = db.PAM007.Where(x => x.LineId == LineId).FirstOrDefault();
                objPAM007.IsAccepted = IsAccepted;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg);
        }

        public ActionResult UpdateRemarks(int LineId, string Remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PAM007 objPAM007 = db.PAM007.Where(x => x.LineId == LineId).FirstOrDefault();
                objPAM007.Remarks = Remarks;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg);
        }

        [SessionExpireFilter, HttpPost]
        public ActionResult ApproveDSSDetailById(int DssId, string Comments)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objPAM007 = db.PAM007.Where(i => i.DSSId == DssId && (i.IsAccepted == null || i.IsAccepted == false)).ToList();
                var objpam006 = db.PAM006.Where(m => m.DSSId == DssId).FirstOrDefault();
                if (objPAM007.Count > 0)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "You must accept all line details before approval";
                }
                else
                {
                    db.SP_DSS_APPROVE(DssId, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objpam006.ENGComments = Comments;
                    db.SaveChanges();
                    objResponseMsg.Value = "DSS Approved Successfully.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter, HttpPost]
        public ActionResult ReturnDSSDetailById(int DssId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            PAM006 objNDE010 = new PAM006();
            try
            {
                if (DssId > 0)
                {

                    var objPAM007 = db.PAM007.Where(i => i.DSSId == DssId && i.Remarks != null && i.Remarks.Trim() != string.Empty).ToList();
                    if (objPAM007.Count > 0)
                    {
                        objNDE010 = db.PAM006.Where(i => i.DSSId == DssId).FirstOrDefault();
                        objNDE010.Status = clsImplementationEnum.NDETechniqueStatus.Returned.GetStringValue();

                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.PTTechniqueMeassage.Return.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Please Enter Return Remarks.";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg);
        }
        #endregion

        //Developed By Ajay Chauhan On 05-06-2018 Observation ID 
        #region Import Excell
        public ActionResult ReadExcel()
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            int DSSId = Convert.ToInt32(Request.Form["DSSId"]);
            if (Request.Files.Count == 0)
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please select a file first!";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            HttpPostedFileBase upload = Request.Files[0];
            if (Path.GetExtension(upload.FileName) == ".xlsx" || Path.GetExtension(upload.FileName) == ".xls")
            {
                ExcelPackage package = new ExcelPackage(upload.InputStream);
                DataTable dt = Manager.ExcelToDataTable(package);
                return validateAndSave(dt, DSSId);
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please upload valid excel file!";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult validateAndSave(DataTable dt, int DSSId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            bool hasError = false;
            dt.Columns.Add("HeaderId", typeof(Int32));
            dt.Columns.Add("DSSId", typeof(Int32));
            dt.Columns.Add("DSSNo", typeof(string));

            try
            {
                if (!dt.Columns.Contains("Error"))
                {
                    dt.Columns.Add("Error");
                }
                PAM006 objPAM006 = db.PAM006.Where(x => x.DSSId == DSSId).FirstOrDefault();

                foreach (DataRow item in dt.Rows)
                {
                    List<string> errors = new List<string>();
                    var description = item.Field<string>(0);
                    var docno = item.Field<string>(1);
                    var docrev = item.Field<string>(2);
                    var pages = item.Field<string>(3);
                    var remarks = item.Field<string>(4);//Observation 15939 on 31-07-2018

                    if (string.IsNullOrEmpty(description))
                    { errors.Add("Description is Mandatory"); hasError = true; }
                    if (string.IsNullOrEmpty(docno))
                    { errors.Add("Doc no is Mandatory"); hasError = true; }
                    if (string.IsNullOrEmpty(docrev))
                    { errors.Add("Doc rev is Mandatory"); hasError = true; }
                    if (!string.IsNullOrEmpty(description) && description.Length > 200)
                    { errors.Add("Description must be less than or equal to 200 character."); hasError = true; }
                    if (!string.IsNullOrEmpty(docno) && docno.Length > 200)
                    { errors.Add("Doc no  must be less than or equal to 200 character."); hasError = true; }
                    if (!string.IsNullOrEmpty(docrev) && docrev.Length > 15)
                    { errors.Add("Doc rev  must be less than or equal to 15 character."); hasError = true; }
                    if (!string.IsNullOrEmpty(pages) && pages.Length > 15)
                    { errors.Add("pages  must be less than or equal to 15 character."); hasError = true; }
                    if (!string.IsNullOrEmpty(remarks) && remarks.Length > 500)
                    { errors.Add("remarks  must be less than or equal to 500 character."); hasError = true; }

                    item.SetField(5, objPAM006.HeaderId);
                    item.SetField(6, DSSId);
                    item.SetField(7, objPAM006.DSSNo);

                    if (errors.Count > 0)
                    {
                        item.SetField<string>(8, string.Join(",", errors.ToArray()));
                    }
                }

                if (!hasError)
                {
                    string sourcePath = string.Empty;
                    string destinationPath = string.Empty;
                    string documentName = string.Empty;
                    string fileName = string.Empty;
                    string ContractNo = db.PAM001.Where(x => x.HeaderId == objPAM006.HeaderId).Select(x => x.ContractNo).FirstOrDefault();

                    sourcePath = "PAM007/ContractWiseDocuments/" + ContractNo;
                    //var existing = (new clsFileUpload()).GetDocuments(sourcePath);
                    FileUploadController _objFUC = new FileUploadController();
                    string Table_Name = "PAM007/" + ContractNo;
                    List<FCS001> existing = _objFUC.GetAllDocumentsByTableNameTableId(Table_Name, 1);

                    //var existing = lstDocuments.Select(x => new { DocName = x.Name, DocExtention = x.Name.Split('.').Last() }).ToList().OrderBy(x => x.DocName);
                    var listPAM007 = new List<PAM007>();
                    foreach (DataRow item in dt.Rows)
                    {
                        PAM007 objPAM007 = new PAM007();
                        objPAM007.HeaderId = objPAM006.HeaderId;
                        objPAM007.DSSId = DSSId;
                        objPAM007.Description = item.Field<string>(0);
                        objPAM007.DocNo = item.Field<string>(1);
                        objPAM007.DocRev = item.Field<string>(2);

                        int n = 0;
                        bool isNumeric = int.TryParse(Convert.ToString(item.Field<string>(3)), out n);
                        if (!isNumeric)
                            objPAM007.Pages = string.Empty;
                        else
                            objPAM007.Pages = item.Field<string>(3);

                        objPAM007.Remarks = item.Field<string>(4);//Observation 15939 on 31-07-2018
                        objPAM007.DSSNo = objPAM006.DSSNo;
                        objPAM007.CreatedBy = objClsLoginInfo.UserName;
                        objPAM007.CreatedOn = DateTime.Now;
                        objPAM007.Attachments = existing.Any(k => Path.GetFileNameWithoutExtension(k.Document_name) == objPAM007.DocNo) ? "Yes" : "No";

                        var objPAM07 = db.PAM007.Where(m => m.DocNo == objPAM007.DocNo && m.HeaderId == objPAM007.HeaderId).FirstOrDefault();
                        if (objPAM07 == null)
                        {
                            listPAM007.Add(objPAM007);
                            //db.PAM007.Add(objPAM007);
                            //db.SaveChanges();
                            //destinationPath = "PAM007/" + objPAM007.LineId;
                        }
                        else
                        {
                            #region Obs Id#17945

                            objPAM07.Description= item.Field<string>(0);
                            objPAM07.DocRev = item.Field<string>(2);
                            objPAM07.Pages = item.Field<string>(3);
                            objPAM07.Remarks = item.Field<string>(4);
                            db.SaveChanges();

                            #endregion
                        }
                        //else
                        //{
                        //    destinationPath = "PAM007/" + objPAM07.LineId;
                        //}                        
                        //// documentName = objPAM007.DocNo + "-" + objPAM007.DocRev;
                        //documentName = objPAM007.DocNo;
                        //if (existing.Any(x => x.DocName == documentName + "." + x.DocExtention))
                        //{
                        //    // fileName = documentName + "." + existing.Where(x => x.DocName == documentName).Select(x => x.DocExtention).FirstOrDefault();
                        //    fileName = existing.Where(x => x.DocName == documentName + "." + x.DocExtention).Select(x => x.DocName).FirstOrDefault();
                        //    (new clsFileUpload()).CopyFolderContentsAsync(sourcePath, destinationPath, fileName);
                        //}
                        //else
                        //{
                        //    flag++;
                        //}
                    }
                    if (listPAM007.Count > 0)
                    {
                        db.PAM007.AddRange(listPAM007);
                        db.SaveChanges();
                    }
                    if (flag > 0)
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Data Upload Successfully and Some of the file is missing in attachment In Line.So Please Check before Submit! ";
                    }
                    else
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Data Upload Successfully!";
                    }
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return ErrorExportToExcel(dt);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return null;
            }
        }

        public ActionResult ErrorExportToExcel(DataTable dt)
        {
            clsHelper.ResponceMsgWithFileName objResponseMsg = new clsHelper.ResponceMsgWithFileName();
            if (dt.Columns.Contains("HeaderId"))
            {
                dt.Columns.Remove("HeaderId");
            }
            if (dt.Columns.Contains("DSSId"))
            {
                dt.Columns.Remove("DSSId");
            }
            if (dt.Columns.Contains("DSSNo"))
            {
                dt.Columns.Remove("DSSNo");
            }
            try
            {
                MemoryStream ms = new MemoryStream();
                using (FileStream fs = System.IO.File.OpenRead(Server.MapPath("~/Areas/PAM/Views/DSS/DSS_Error.xlsx")))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();
                        int i = 2;
                        foreach (DataRow item in dt.Rows)
                        {
                            excelWorksheet.Cells[i, 1].Value = item.Field<string>(0);
                            excelWorksheet.Cells[i, 2].Value = item.Field<string>(1);
                            excelWorksheet.Cells[i, 3].Value = item.Field<string>(2);
                            excelWorksheet.Cells[i, 4].Value = item.Field<string>(3);
                            excelWorksheet.Cells[i, 5].Value = item.Field<string>(4);
                            excelWorksheet.Cells[i, 6].Value = item.Field<string>(5);
                            i++;
                        }
                        excelPackage.SaveAs(ms);
                    }
                }

                ms.Position = 0;
                string fileName;
                string excelFilePath = Helper.ExcelFilePath("ErrorList", out fileName);
                Byte[] bin = ms.ToArray();
                System.IO.File.WriteAllBytes(excelFilePath, bin);

                objResponseMsg.Key = false;
                objResponseMsg.FileName = fileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }
        #endregion

        #region --Bulk Export
        [HttpPost]
        public ActionResult GetbulkAttachmentDetails(string folderPath, bool includeFromSubfolders = false, string Type = "", string orderby = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            if (string.IsNullOrWhiteSpace(folderPath))
                return Json("", JsonRequestBehavior.AllowGet);

            var Files = (new clsFileUpload(Type)).GetDocuments(folderPath, includeFromSubfolders, orderby).Select(x => new { x.Name }).ToList().OrderBy(x => x.Name);
            if (Files.Count() > 0)
            {
                List<bulknamelist> bulkname = new List<bulknamelist>();
                foreach (var item in Files)
                {
                    bulknamelist bkname = new bulknamelist();
                    bkname.DocNo = Path.GetFileNameWithoutExtension(item.Name);
                    bulkname.Add(bkname);
                }
                string str = Helper.GenerateExcel(bulkname, objClsLoginInfo.UserName);
                objResponseMsg.Key = true;
                objResponseMsg.Value = str;
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "No More Bulk Data Available";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetbulkAttachmentDetails_FCS(string TableName,Int64 TableId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            if (string.IsNullOrWhiteSpace(TableName))
                return Json("", JsonRequestBehavior.AllowGet);

            //var Files = (new clsFileUpload(Type)).GetDocuments(folderPath, includeFromSubfolders, orderby).Select(x => new { x.Name }).ToList().OrderBy(x => x.Name);

            FileUploadController _objFUC = new FileUploadController();
            List<FCS001> Files = _objFUC.GetAllDocumentsByTableNameTableId(TableName, TableId);

            if (Files.Count() > 0)
            {
                List<bulknamelist> bulkname = new List<bulknamelist>();
                foreach (var item in Files)
                {
                    bulknamelist bkname = new bulknamelist();
                    bkname.DocNo = Path.GetFileNameWithoutExtension(item.Document_name);
                    bulkname.Add(bkname);
                }
                string str = Helper.GenerateExcel(bulkname, objClsLoginInfo.UserName);
                objResponseMsg.Key = true;
                objResponseMsg.Value = str;
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "No More Bulk Data Available";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();

            if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
            {
                var lstResult = db.SP_DSS_GETDSSDETAILS(1, int.MaxValue, "", whereCondition).Select(x => new { x.PAM, x.DSSNo, x.REvNo, x.Status }).ToList();
                if (lstResult != null && lstResult.Count > 0)
                {
                    string str = Helper.GenerateExcel(lstResult, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = str;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Data not available.";
                }
            }
            else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
            {
                var lstResult = db.SP_DSS_GETDSSLINEDETAILS(1, int.MaxValue, "", whereCondition).Select(x => new { x.Description, x.DocNo, x.DocRev, x.Pages, x.Remarks }).ToList();
                if (!lstResult.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No data available";
                }

                string str = Helper.GenerateExcel(lstResult, objClsLoginInfo.UserName);
                objResponseMsg.Key = true;
                objResponseMsg.Value = str;
            }
            else if (gridType == clsImplementationEnum.GridType.SUB_LINES.GetStringValue())
            {

            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Data not available.";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetAttachmentDetails(string folderPath, string FileName, bool includeFromSubfolders = false, string Type = "", string orderby = "")
        {
            if (string.IsNullOrWhiteSpace(folderPath))
                return Json("", JsonRequestBehavior.AllowGet);
            var Files = (new clsFileUpload(Type)).GetDocuments(folderPath, includeFromSubfolders, orderby);
            Files = Files.Where(i => i.NameWithoutExt.ToLower() == FileName.ToLower()).ToArray();
            return Json(Files, JsonRequestBehavior.AllowGet);
        }

        //FCS 
        public ActionResult GetAttachmentDetails_FCS(string FileName,DataTableParamModel parm, Int64? TableId, string TableName, int? ViewerId = 1)
        {
            try
            {
                ViewerId = ViewerId >= 1 ? ViewerId : 1;
                parm.sSearch = FileName;
                var _lst_document = db.SP_COMMON_GET_DOCUMENT_DETAILS(parm.iDisplayStart, parm.iDisplayLength, parm.sSearch, parm.sSortDir_0, parm.iSortCol_0, TableName, TableId, ViewerId).ToList();

                var _lst = _lst_document.Where(w => Path.GetFileNameWithoutExtension(w.Document_name) == FileName).ToList();
                int recordsTotal = 0;
                if (_lst.Count > 0)
                {
                    _lst_document = _lst;
                    recordsTotal = _lst_document.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                }
                else
                    _lst_document = new List<SP_COMMON_GET_DOCUMENT_DETAILS_Result>();

                return Json(new
                {
                    Echo = parm.sEcho,
                    iTotalRecord = recordsTotal,
                    iTotalDisplayRecords = recordsTotal,
                    data = _lst_document
                });
            }
            catch (Exception ex)
            {
                clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult UpdateAttachmentUploadForDSS(int LineId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();

            try
            {
                var objPAM006 = (from p6 in db.PAM006
                                 join p7 in db.PAM007 on p6.HeaderId equals p7.HeaderId
                                 join p1 in db.PAM001 on p6.HeaderId equals p1.HeaderId
                                 where p7.LineId == LineId
                                 select new { LineId = p7.LineId, ContractNo = p1.ContractNo, DocNo = p7.DocNo, Attachments = p7.Attachments }
                                   ).ToList();

                var listContract = objPAM006.Select(i => i.ContractNo).Distinct().ToList();

                foreach (var cont in listContract)
                {
                    var sourcePath = "PAM007/ContractWiseDocuments/" + cont;
                    //var existing = (new clsFileUpload()).GetDocuments(sourcePath);
                    FileUploadController _objFUC = new FileUploadController();
                    string Table_Name = "PAM007/" + cont;
                    List<FCS001> existing = _objFUC.GetAllDocumentsByTableNameTableId(Table_Name, 1);

                    var listPAM007 = objPAM006.Where(i => i.ContractNo == cont).ToList();
                    foreach (var doc in listPAM007)
                    {
                        if (existing.Any(k => Path.GetFileNameWithoutExtension(k.Document_name) == doc.DocNo))
                        {
                            db.PAM007.Where(i => i.LineId == doc.LineId).FirstOrDefault().Attachments = "Yes";
                        }
                        else
                        {
                            db.PAM007.Where(i => i.LineId == doc.LineId).FirstOrDefault().Attachments = "No";
                        }
                    }
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult BulkUpdateAttachmentUploadForDSS(int DSSId = 0, int LineId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objPAM006 = (from p6 in db.PAM006
                                 join p7 in db.PAM007 on p6.HeaderId equals p7.HeaderId
                                 join p1 in db.PAM001 on p6.HeaderId equals p1.HeaderId
                                 where (p7.Attachments == null || p7.Attachments=="No")
                                 && p7.DSSId == (DSSId == 0 ? p7.DSSId : DSSId)
                                 && p7.LineId == (LineId == 0 ? p7.LineId : LineId)
                                 select new { LineId = p7.LineId, ContractNo = p1.ContractNo, DocNo = p7.DocNo, Attachments = p7.Attachments }
                                  ).ToList();

                var listContract = objPAM006.Select(i => i.ContractNo).Distinct().ToList();

                foreach (var cont in listContract)
                {
                    var sourcePath = "PAM007/ContractWiseDocuments/" + cont;
                    //var existing = (new clsFileUpload()).GetDocuments(sourcePath);
                    FileUploadController _objFUC = new FileUploadController();
                    string Table_Name = "PAM007/" + cont;
                    List<FCS001> existing = _objFUC.GetAllDocumentsByTableNameTableId(Table_Name, 1);

                    var listPAM007 = objPAM006.Where(i => i.ContractNo == cont).ToList();
                    foreach (var doc in listPAM007)
                    {
                        if (existing.Any(k => Path.GetFileNameWithoutExtension(k.Document_name) == doc.DocNo))
                        {
                            db.PAM007.Where(i => i.LineId == doc.LineId).FirstOrDefault().Attachments = "Yes";
                        }
                        else
                        {
                            db.PAM007.Where(i => i.LineId == doc.LineId).FirstOrDefault().Attachments = "No";
                        }
                    }
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdatePrefixItemIdGen(int HeaderId, string PrefixItemIdGen, string SelectedDDL)
        {
            try
            {
                List<PAM002> _lstPAM002 = db.PAM002.Where(w => w.HeaderId == HeaderId).ToList();
                foreach(var obj in _lstPAM002)
                {
                    if (SelectedDDL == "JEP")
                        obj.PrefixItemIdGen = PrefixItemIdGen;
                    else
                        obj.PrefixItemIdGen = obj.Project;
                    db.SaveChanges();
                }

                return Json(new { Status = true, Msg = "Updated successfully." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Status = false, Msg = "Not Updated successfully." }, JsonRequestBehavior.AllowGet);
            }
        }
    }

    public class bulknamelist
    {
        public string Description { set; get; }
        public string DocNo { set; get; }
        public string DocRev { set; get; }
        public string Pages { set; get; }
        public string Remarks { set; get; }
    }

    public class DssHeaderDtl
    {
        public string Contract { get; set; }
        public string Customer { get; set; }
        public string ContractNo { get; set; }
        public string DSSNo { get; set; }
        public string RevNo { get; set; }
        public string CreatedOn { get; set; }
        public string Status { get; set; }
        public string HeaderId { get; set; }
        public string DssId { get; set; }
        public string MKTComments { get; set; }
        public string Comments { get; set; }
    }
}