﻿using IEMQS.Areas.Authorization.Controllers;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using OfficeOpenXml;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace IEMQS.Areas.FDMS.Controllers
{
    public class ReportController : clsBase
    {
        // GET: FDMS/Report
        #region Main Page
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }

        //load tabs wise data
        [SessionExpireFilter]
        [HttpPost]
        public ActionResult LoadTabWisePartial(string tabname)
        {
            ViewBag.tabname = tabname; //name of tab
            switch (tabname)
            {
                //for FDMS Details Report
                case "Detail":
                    ViewBag.Title = "Maintain FDMS Detail Report";
                    ViewBag.ReportType = clsImplementationEnum.FDMSReport.Detail.GetStringValue();
                    break;
                //for FDMS DPP Report
                case "DPP":
                    ViewBag.Title = "Maintain FDMS DPP Report";
                    ViewBag.ReportType = clsImplementationEnum.FDMSReport.DPP.GetStringValue();
                    break;
                //for FDMS Summary Report
                case "Summary":
                    ViewBag.Title = "Maintain FDMS Summary Report";
                    ViewBag.ReportType = clsImplementationEnum.FDMSReport.Summary.GetStringValue();
                    break;
                default:
                    ViewBag.Title = "Maintain FDMS Detail Report";
                    ViewBag.ReportType = clsImplementationEnum.FDMSReport.Detail.GetStringValue();
                    break;
            }
            return PartialView("_LoadReports");
        }
        #endregion

        #region Select2 Function

        //fetch all distinct projects from FDS001 
        public JsonResult GetProjects(string search)
        {
            try
            {
                List<fdmsreport> lstProjects = new List<fdmsreport>();
                lstProjects = (from c1 in db.FDS001
                               join c2 in db.COM001 on c1.Project equals c2.t_cprj
                               where (c2.t_dsca.ToLower().Contains(search.ToLower()) || c2.t_cprj.Contains(search.ToLower()))
                               select new fdmsreport
                               {
                                   id = c2.t_cprj,
                                   text = c2.t_cprj + " - " + c2.t_dsca
                               }).Distinct().ToList();

                if (lstProjects.Count > 0)
                {
                    lstProjects.Insert(0, new fdmsreport { id = "ALL", text = "ALL" });//Set "ALL" as first data of control
                }

                return Json(lstProjects, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        //fetch all departments from FDS002
        public ActionResult GetDepartmentList(string search)
        {
            try
            {
                List<fdmsreport> lstDepartment = new List<fdmsreport>();
                lstDepartment = (from c1 in db.FDS002
                                 join c2 in db.COM002 on c1.Department equals c2.t_dimx
                                 where (c2.t_dimx.ToLower().Contains(search.ToLower()) || c2.t_desc.ToLower().Contains(search.ToLower()))
                                 select new fdmsreport
                                 {
                                     id = c2.t_dimx,
                                     text = c2.t_dimx + " - " + c2.t_desc
                                 }).Distinct().ToList();

                if (lstDepartment.Count > 0)
                {
                    lstDepartment.Insert(0, new fdmsreport { id = "ALL", text = "ALL" });//Set "ALL" as first data of control
                }
                return Json(lstDepartment, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        //fetch all FDMS status from enum 
        public ActionResult GetFDMSStatusList(string search)
        {
            try
            {
                List<fdmsreport> lstDepartment = new List<fdmsreport>();
                var lstStatus = clsImplementationEnum.GetFDMSStatus().ToList();

                var lstStatusAll = (from lst in lstStatus
                                    where lst.ToLower().IndexOf(search.ToLower()) > -1
                                    select new
                                    {
                                        id = lst,
                                        text = lst
                                    }).ToList();
                if (lstStatusAll.Count > 0)
                {
                    lstStatusAll.Insert(0, new { id = "ALL", text = "ALL" });//Set "ALL" as first data of control
                }
                return Json(lstStatusAll, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        //fetch all BU 
        public JsonResult GetAllBUSelect2(string search)
        {
            int BU = Convert.ToInt32(clsImplementationEnum.BLDNumber.BU.GetStringValue());
            var lstBUs = Manager.getBUsByID(BU);
            try
            {
                var items = (from li in lstBUs
                           //  where li.t_dimx.Contains(search) || li.t_desc.Contains(search)
                             select new
                             {
                                 id = li.t_dimx,
                                 text = li.t_desc,
                             }).ToList();
                if (items.Count() > 0)
                {
                    items.Insert(0, new { id = "ALL", text = "ALL" }); //Set "ALL" as first data of control
                }
                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region set values in hidden fields
        public ActionResult getAllDepartment(string dept)
        {

            ResponceMsg objResponceMsg = new ResponceMsg();
            var lstDepartment = (from c1 in db.FDS002
                                 join c2 in db.COM002 on c1.Department equals c2.t_dimx
                                 select c2.t_dimx).Distinct().ToList();
            string[] arrayDept = dept.Split(',').ToArray();

            if (arrayDept.Contains("ALL")) //if control contains 'all' as selected option or all with other value(ex: ALL,3858)
            {
                objResponceMsg.Key = true;
                objResponceMsg.Value = string.Join(",", lstDepartment);
            }
            else
            { objResponceMsg.Key = false; objResponceMsg.Value = dept; }

            return Json(objResponceMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getAllProject(string project)
        {
            ResponceMsg objResponceMsg = new ResponceMsg();
            var lstProjects = (from c1 in db.FDS001
                               join c2 in db.COM001 on c1.Project equals c2.t_cprj
                               select c2.t_cprj).Distinct().ToList();

            string[] arrayProject = project.Split(',').ToArray();
            if (arrayProject.Contains("ALL")) //if control contains 'all' as selected option or all with other value(ex: ALL,project code)
            {
                objResponceMsg.Key = true;
                objResponceMsg.Value = string.Join(",", lstProjects);
            }
            else
            { objResponceMsg.Key = false; objResponceMsg.Value = project; }
            return Json(objResponceMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getAllStatus(string status)
        {
            ResponceMsg objResponceMsg = new ResponceMsg();
            var lssStatus = clsImplementationEnum.GetFDMSStatus().ToList();

            string[] arraystatus = status.Split(',').ToArray();
            if (arraystatus.Contains("ALL")) //if control contains 'all' as selected option or all with other value(ex: ALL,Draft)
            { objResponceMsg.Key = true; objResponceMsg.Value = string.Join(",", lssStatus); }
            else
            { objResponceMsg.Key = false; objResponceMsg.Value = status; }

            return Json(objResponceMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getAllBU(string bu)
        {
            ResponceMsg objResponceMsg = new ResponceMsg();
            int BU = Convert.ToInt32(clsImplementationEnum.BLDNumber.BU.GetStringValue());
            var lstBUs = db.COM002.Where(x => x.t_dtyp == BU).Select(o => o.t_dimx).Distinct().ToList();

            string[] arrayBU = bu.Split(',').ToArray();
            if (arrayBU.Contains("ALL"))
            { objResponceMsg.Key = true; objResponceMsg.Value = string.Join(",", lstBUs); } //if control contains 'all' as selected option or all with other value(ex: ALL,01)
            else
            { objResponceMsg.Key = false; objResponceMsg.Value = bu; }

            return Json(objResponceMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}