﻿using IEMQS.Areas.Authorization.Controllers;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using OfficeOpenXml;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace IEMQS.Areas.FDMS.Controllers
{
    public class FDMSController : clsBase
    {
        // GET: FDMS/FDMS

        #region Header index
        [SessionExpireFilter]
        public ActionResult Index(string Project)
        {
            ViewBag.chkProject = Project;
            return View();
        }

        [HttpPost]
        public ActionResult GetHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("GetHeaderGridDataPartial");
        }

        [HttpPost]
        public JsonResult LoadHeaderData(JQueryDataTableParamModel param, string Project)
        {
            try
            {
                if (Project != null && Project != "")
                {
                    var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                    var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                    int StartIndex = param.iDisplayStart + 1;
                    int EndIndex = param.iDisplayStart + param.iDisplayLength;
                    var user = objClsLoginInfo.UserName;
                    List<string> lstRole = new List<string>();
                    lstRole = getRoleFromTable(user);

                    string strWhere = string.Empty;

                    if (param.CTQCompileStatus.ToUpper() == "PENDING")
                    {
                        if (lstRole.Count > 0)
                        {
                            strWhere += "(";
                        }
                        for (int i = 0; i < lstRole.Count; i++)
                        {
                            var role = lstRole[i];
                            if (role == clsImplementationEnum.UserRoleName.ENGG3.GetStringValue())
                            {
                                strWhere += " (1=1 and status in('" + clsImplementationEnum.FDMSStatus.RetFDR.GetStringValue() + "','" + clsImplementationEnum.FDMSStatus.FDR.GetStringValue() + "'))";
                            }
                            else if (role == clsImplementationEnum.UserRoleName.QC2.GetStringValue())
                            {
                                strWhere += " (1=1 and status in('" + clsImplementationEnum.FDMSStatus.DRAFT.GetStringValue() + "','" + clsImplementationEnum.FDMSStatus.SCNVer.GetStringValue() + "','" + clsImplementationEnum.FDMSStatus.RetSCN.GetStringValue() + "','" + clsImplementationEnum.FDMSStatus.RetDocAssign.GetStringValue() + "' , '" + clsImplementationEnum.FDMSStatus.DocAssign.GetStringValue() + "' , '" + clsImplementationEnum.FDMSStatus.DocVeri.GetStringValue() + "', '" + clsImplementationEnum.FDMSStatus.RetDocVeri.GetStringValue() + "'))";
                            }
                            else if (role == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                            {
                                strWhere += " (1=1 and status in('" + clsImplementationEnum.FDMSStatus.DRAFT.GetStringValue() + "','" + clsImplementationEnum.FDMSStatus.RetDraft.GetStringValue() + "','" + clsImplementationEnum.FDMSStatus.RetDocAssign.GetStringValue() + "' , '" + clsImplementationEnum.FDMSStatus.DocAssign.GetStringValue() + "' , '" + clsImplementationEnum.FDMSStatus.DocVeri.GetStringValue() + "', '" + clsImplementationEnum.FDMSStatus.RetDocVeri.GetStringValue() + "', '" + clsImplementationEnum.FDMSStatus.QAAccept.GetStringValue() + "', '" + clsImplementationEnum.FDMSStatus.RetQA.GetStringValue() + "'))";
                            }
                            else if (role == clsImplementationEnum.UserRoleName.QA2.GetStringValue())
                            {
                                strWhere += " (1=1 and status in('" + clsImplementationEnum.FDMSStatus.DocAssign.GetStringValue() + "' , '" + clsImplementationEnum.FDMSStatus.RetDocAssign.GetStringValue() + "' , '" + clsImplementationEnum.FDMSStatus.DocVeri.GetStringValue() + "', '" + clsImplementationEnum.FDMSStatus.RetDocVeri.GetStringValue() + "'))";
                            }
                            else if (role == clsImplementationEnum.UserRoleName.QA3.GetStringValue())
                            {
                                strWhere += " (1=1 and status in('" + clsImplementationEnum.FDMSStatus.QAAccept.GetStringValue() + "','" + clsImplementationEnum.FDMSStatus.DocAssign.GetStringValue() + "' , '" + clsImplementationEnum.FDMSStatus.RetQA.GetStringValue() + "', '" + clsImplementationEnum.FDMSStatus.RetDocAssign.GetStringValue() + "', '" + clsImplementationEnum.FDMSStatus.DocVeri.GetStringValue() + "', '" + clsImplementationEnum.FDMSStatus.RetDocVeri.GetStringValue() + "', '" + clsImplementationEnum.FDMSStatus.QAAccept.GetStringValue() + "', '" + clsImplementationEnum.FDMSStatus.RetQA.GetStringValue() + "'))";
                            }
                            else if (role == clsImplementationEnum.UserRoleName.PMG2.GetStringValue() || role == clsImplementationEnum.UserRoleName.PMG3.GetStringValue())
                            {
                                strWhere += " (1=1 and status in('" + clsImplementationEnum.FDMSStatus.PMGAccept.GetStringValue() + "'))";
                            }
                            else if (role == clsImplementationEnum.UserRoleName.ResponsiblePerson.GetStringValue())
                            {
                                strWhere += " (1=1 and status in('" + clsImplementationEnum.FDMSStatus.DocVeri.GetStringValue() + "','" + clsImplementationEnum.FDMSStatus.SCNVer.GetStringValue() + "','" + clsImplementationEnum.FDMSStatus.RetSCN.GetStringValue() + "'))";
                            }
                            else if (role == clsImplementationEnum.UserRoleName.Originator.GetStringValue())
                            {
                                strWhere += " ( 1=1 and Originator = " + user + " and status in('" + clsImplementationEnum.FDMSStatus.DRAFT.GetStringValue() + "','" + clsImplementationEnum.FDMSStatus.RetDraft.GetStringValue() + "'))";
                            }
                            else
                            {
                                strWhere += " (1=1 and status in('" + clsImplementationEnum.FDMSStatus.DRAFT.GetStringValue() + "'))";
                            }

                            if (i != lstRole.Count - 1)
                            {
                                strWhere += " or ";
                            }
                        }
                        if (lstRole.Count > 0)
                        {
                            strWhere += ")";
                        }
                    }
                    else
                    {
                        strWhere += "1=1";
                    }

                    string strSortOrder = string.Empty;
                    strWhere += " and Project='" + Project.ToString() + "'";
                    string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                    string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                    if (!string.IsNullOrWhiteSpace(sortColumnName))
                    {
                        strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                    }
                    string[] columnName = { "Location", "Project", "Status", "BU", "DesignEngineer", "QCManager", "DesignEngineer", "QAManager", "FDMSName", "PMGManager" };
                    if (!string.IsNullOrWhiteSpace(param.sSearch))
                    {
                        strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                    }
                    else
                    {
                        strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                    }
                    var lstResult = db.SP_FDMS_GET_CREATE
                                        (
                                        StartIndex, EndIndex, strSortOrder, strWhere
                                        ).ToList();
                    lstResult = lstResult.Where(x => x.Project.Contains(Project)).ToList();

                    //if (!string.IsNullOrWhiteSpace(param.sSearch))
                    //{
                    //    lstResult = lstResult.Where(c => c.Location.Contains(param.sSearch) || c.Project.Contains(param.sSearch)
                    //    || c.Location.Contains(param.sSearch) || c.Status.Contains(param.sSearch) ||
                    //   c.BU.Contains(param.sSearch) || c.DesignEngineer.Contains(param.sSearch) || c.QCManager.Contains(param.sSearch) ||
                    //   c.DesignEngineer.Contains(param.sSearch) || c.QCManager.Contains(param.sSearch) || c.QAManager.Contains(param.sSearch) ||
                    //   c.QAEngineer.Contains(param.sSearch) || c.PMGManager.Contains(param.sSearch) || c.FDMSName.Contains(param.sSearch)).ToList();
                    //}

                    var data = (from uc in lstResult
                                select new[]
                                {
                                Convert.ToString(uc.FDMSName),
                                Convert.ToString(uc.Project),
                                Convert.ToString(uc.Location),
                                Convert.ToString(uc.BU),
                                Convert.ToString(uc.Status),
                                Convert.ToString(uc.DesignEngineer),
                                Convert.ToString(uc.QCManager),
                                Convert.ToString(uc.QAManager),
                                Convert.ToString(uc.PMGManager),
                                Convert.ToString(uc.QAEngineer),
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.HeaderId)
                            }).ToList();
                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                        iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                        aaData = data,
                        strSortOrder = strSortOrder,
                        whereCondition = strWhere
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                    var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                    int StartIndex = param.iDisplayStart + 1;
                    int EndIndex = param.iDisplayStart + param.iDisplayLength;
                    var user = objClsLoginInfo.UserName;
                    List<string> lstRole = new List<string>();
                    lstRole = getRoleFromTable(user);

                    string strWhere = string.Empty;
                    if (param.CTQCompileStatus.ToUpper() == "PENDING")
                    {
                        if (lstRole.Count > 0)
                        {
                            strWhere += "(";
                        }
                        for (int i = 0; i < lstRole.Count; i++)
                        {
                            var role = lstRole[i];
                            if (role == clsImplementationEnum.UserRoleName.ENGG3.GetStringValue())
                            {
                                strWhere += " (1=1 and status in('" + clsImplementationEnum.FDMSStatus.RetFDR.GetStringValue() + "','" + clsImplementationEnum.FDMSStatus.FDR.GetStringValue() + "'))";
                            }
                            else if (role == clsImplementationEnum.UserRoleName.QC2.GetStringValue())
                            {
                                strWhere += " (1=1 and status in('" + clsImplementationEnum.FDMSStatus.DRAFT.GetStringValue() + "','" + clsImplementationEnum.FDMSStatus.SCNVer.GetStringValue() + "','" + clsImplementationEnum.FDMSStatus.RetSCN.GetStringValue() + "','" + clsImplementationEnum.FDMSStatus.RetDocAssign.GetStringValue() + "' , '" + clsImplementationEnum.FDMSStatus.DocAssign.GetStringValue() + "' , '" + clsImplementationEnum.FDMSStatus.DocVeri.GetStringValue() + "', '" + clsImplementationEnum.FDMSStatus.RetDocVeri.GetStringValue() + "'))";
                            }
                            else if (role == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                            {
                                strWhere += " (1=1 and status in('" + clsImplementationEnum.FDMSStatus.DRAFT.GetStringValue() + "','" + clsImplementationEnum.FDMSStatus.RetDraft.GetStringValue() + "','" + clsImplementationEnum.FDMSStatus.RetDocAssign.GetStringValue() + "' , '" + clsImplementationEnum.FDMSStatus.DocAssign.GetStringValue() + "' , '" + clsImplementationEnum.FDMSStatus.DocVeri.GetStringValue() + "', '" + clsImplementationEnum.FDMSStatus.RetDocVeri.GetStringValue() + "', '" + clsImplementationEnum.FDMSStatus.QAAccept.GetStringValue() + "', '" + clsImplementationEnum.FDMSStatus.RetQA.GetStringValue() + "'))";
                            }
                            else if (role == clsImplementationEnum.UserRoleName.QA2.GetStringValue())
                            {
                                strWhere += " (1=1 and status in('" + clsImplementationEnum.FDMSStatus.DocAssign.GetStringValue() + "' , '" + clsImplementationEnum.FDMSStatus.RetDocAssign.GetStringValue() + "' , '" + clsImplementationEnum.FDMSStatus.DocVeri.GetStringValue() + "', '" + clsImplementationEnum.FDMSStatus.RetDocVeri.GetStringValue() + "'))";
                            }
                            else if (role == clsImplementationEnum.UserRoleName.QA3.GetStringValue())
                            {
                                strWhere += " (1=1 and status in('" + clsImplementationEnum.FDMSStatus.QAAccept.GetStringValue() + "','" + clsImplementationEnum.FDMSStatus.DocAssign.GetStringValue() + "' , '" + clsImplementationEnum.FDMSStatus.RetQA.GetStringValue() + "', '" + clsImplementationEnum.FDMSStatus.RetDocAssign.GetStringValue() + "', '" + clsImplementationEnum.FDMSStatus.DocVeri.GetStringValue() + "', '" + clsImplementationEnum.FDMSStatus.RetDocVeri.GetStringValue() + "', '" + clsImplementationEnum.FDMSStatus.QAAccept.GetStringValue() + "', '" + clsImplementationEnum.FDMSStatus.RetQA.GetStringValue() + "'))";
                            }
                            else if (role == clsImplementationEnum.UserRoleName.PMG2.GetStringValue() || role == clsImplementationEnum.UserRoleName.PMG3.GetStringValue())
                            {
                                strWhere += " (1=1 and status in('" + clsImplementationEnum.FDMSStatus.PMGAccept.GetStringValue() + "'))";
                            }
                            else if (role == clsImplementationEnum.UserRoleName.ResponsiblePerson.GetStringValue())
                            {
                                strWhere += " (1=1 and status in('" + clsImplementationEnum.FDMSStatus.DocVeri.GetStringValue() + "','" + clsImplementationEnum.FDMSStatus.SCNVer.GetStringValue() + "','" + clsImplementationEnum.FDMSStatus.RetSCN.GetStringValue() + "'))";
                            }
                            else if (role == clsImplementationEnum.UserRoleName.Originator.GetStringValue())
                            {
                                strWhere += " ( 1=1 and Originator = " + user + " and status in('" + clsImplementationEnum.FDMSStatus.DRAFT.GetStringValue() + "','" + clsImplementationEnum.FDMSStatus.RetDraft.GetStringValue() + "'))";
                            }
                            else
                            {
                                strWhere += " (1=1 and status in('" + clsImplementationEnum.FDMSStatus.DRAFT.GetStringValue() + "'))";
                            }

                            if (i != lstRole.Count - 1)
                            {
                                strWhere += " or ";
                            }
                        }

                        if (lstRole.Count > 0)
                        {
                            strWhere += ")";
                        }
                    }
                    else
                    {
                        strWhere += "1=1";
                    }

                    string strSortOrder = string.Empty;
                    string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                    string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                    if (!string.IsNullOrWhiteSpace(sortColumnName))
                    {
                        strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                    }
                    string[] columnName = { "Location", "Project", "Status", "BU", "DesignEngineer", "QCManager", "DesignEngineer", "QAManager", "FDMSName", "PMGManager" };
                    if (!string.IsNullOrWhiteSpace(param.sSearch))
                    {
                        strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                    }
                    else
                    {
                        strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                    }
                    var lstResult = db.SP_FDMS_GET_CREATE
                                        (
                                        StartIndex, EndIndex, strSortOrder, strWhere
                                        ).ToList();
                    //if (!string.IsNullOrWhiteSpace(param.sSearch))
                    //{
                    //    lstResult = lstResult.Where(c => c.Location.Contains(param.sSearch) || c.Project.Contains(param.sSearch)
                    //    || c.Location.Contains(param.sSearch) || c.Status.Contains(param.sSearch) ||
                    //   c.BU.Contains(param.sSearch) || c.DesignEngineer.Contains(param.sSearch) || c.QCManager.Contains(param.sSearch) ||
                    //   c.DesignEngineer.Contains(param.sSearch) || c.QCManager.Contains(param.sSearch) || c.QAManager.Contains(param.sSearch) ||
                    //   c.QAEngineer.Contains(param.sSearch) || c.PMGManager.Contains(param.sSearch) || c.FDMSName.Contains(param.sSearch)).ToList();
                    //}

                    var data = (from uc in lstResult
                                select new[]
                                {
                                Convert.ToString(uc.FDMSName),
                                Convert.ToString(uc.Project),
                                Convert.ToString(uc.Location),
                                Convert.ToString(uc.BU),
                                Convert.ToString(uc.Status),
                                Convert.ToString(uc.DesignEngineer),
                                Convert.ToString(uc.QCManager),
                                Convert.ToString(uc.QAManager),
                                Convert.ToString(uc.PMGManager),
                                Convert.ToString(uc.QAEngineer),
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.HeaderId)
                            }).ToList();
                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                        iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                        aaData = data,
                        strSortOrder = strSortOrder,
                        whereCondition = strWhere
                    }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region create / detail page
        [SessionExpireFilter]
        public ActionResult CreateFDMS(int Id = 0, string error = null, string Project = "")
        {
            FDS001 objFDS001 = new FDS001();
            var locaDescription = (from a in db.COM002
                                   where a.t_dimx == objClsLoginInfo.Location
                                   select a.t_desc).FirstOrDefault();
            var Location = (from a in db.COM002
                            where a.t_dtyp == 1 && a.t_dimx != ""
                            select new { a.t_dimx, Desc = a.t_desc }).ToList();

            ViewBag.Location = new SelectList(Location, "t_dimx", "Desc");

            var location = objClsLoginInfo.Location;

            if (!string.IsNullOrEmpty(error))
                ViewBag.error = error;

            if (!string.IsNullOrEmpty(Project))
            {
                string BUid = db.COM001.Where(i => i.t_cprj == Project).FirstOrDefault().t_entu;
                var BUDescription = (from a in db.COM002
                                     where a.t_dtyp == 2 && a.t_dimx == BUid
                                     select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
                ViewBag.BU = BUDescription.BUDesc;
                ViewBag.Project = Manager.GetProjectAndDescription(Project);

                string YY = "";
                var ExistsFDS = db.FDS001.Any(x => x.Project == Project);
                if (ExistsFDS == false)
                {
                    YY = "01";
                    ViewBag.FDMS = YY;
                }
                else
                {
                    var existFDMS = db.FDS001.OrderByDescending(p => p.Project == Project).FirstOrDefault().FDMSName;
                    var obj = (from a in db.FDS001
                               where a.Project == Project
                               orderby a.HeaderId descending
                               select a).FirstOrDefault();
                    //if (obj.Status == clsImplementationEnum.FDMSStatus.Completed.GetStringValue())
                    //{
                    YY = obj.FDMSName.Split('-')[2].ToString().Trim();
                    int yy = Convert.ToInt32(YY);
                    if (Id == 0)
                        yy += 1;
                    YY = "0" + yy.ToString();

                    ViewBag.FDMS = YY;

                }

                //ViewBag.FDMS = db.FDS001.Where(x => x.Project == Project).FirstOrDefault().FDMSName;                
            }

            if (Id > 0)
            {
                objFDS001 = db.FDS001.Where(x => x.HeaderId == Id).FirstOrDefault();
                var origin = (from a in db.FDS001
                              where a.HeaderId == Id
                              select a.Originator).FirstOrDefault();
                var namedesc = (from a in db.COM003
                                where a.t_psno == origin && a.t_actv == 1
                                select a.t_name).FirstOrDefault();

                ViewBag.Initiator = origin + " - " + namedesc;
                if (objFDS001.QAEngineer.Trim() == objClsLoginInfo.UserName.Trim())
                {
                    ViewBag.Role = "qaengg";
                    List<FDS002> lstSubmitted = db.FDS002.Where(h => h.HeaderId == Id && h.SubmittoPMG == true).ToList();
                    List<FDS002> lstheader = db.FDS002.Where(h => h.HeaderId == Id).ToList();
                    if (lstSubmitted != null && lstSubmitted.Count == lstheader.Count)
                    {
                        ViewBag.button = "disablebutton";
                    }
                    else
                    {
                        ViewBag.button = "visible";
                    }
                }
                if (objFDS001.PMGManager.Trim() == objClsLoginInfo.UserName.Trim() && objFDS001.Status == clsImplementationEnum.FDMSStatus.PMGAccept.GetStringValue())
                { ViewBag.button = "pmgvisible"; }
                ViewBag.lineDate = Convert.ToDateTime(objFDS001.LineSubmitDate).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            else
            {
                objFDS001.Status = clsImplementationEnum.FDMSStatus.DRAFT.GetStringValue();
                ViewBag.Initiator = objClsLoginInfo.UserName + " - " + objClsLoginInfo.Name;
            }
            ViewBag.chkProject = Project;
            return View(objFDS001);
        }
        #endregion

        #region code added by Nikita
        // start :(added by nikita vibhandik as per Task assigned by - Abhishek Sir)
        //to check whether PMG accepted documnets or not
        [HttpPost]
        public bool PMGExists(int headerid)
        {
            bool Flag = false;
            if (headerid > 0)
            {
                FDS001 objFDS001 = db.FDS001.Where(x => x.HeaderId == headerid && x.PMGAccepOn != null && x.DateofDocAcceptanceByPMG != null).FirstOrDefault();
                if (objFDS001 != null)
                {
                    Flag = true;
                }
            }
            return Flag;
        }
        public class ResponceMsgWithStatus : clsHelper.ResponseMsg
        {
            public string status;
            public string Revision;
            public string projdesc;
            public string date;
            public int headerid;
        }
        //capture document acceptance  date time for PMG
        public ActionResult DocAcceptedBYPMG(int HeaderId)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            FDS001 objUpdate = db.FDS001.Where(h => h.HeaderId == HeaderId).FirstOrDefault();
            if (objUpdate != null)
            {
                objUpdate.PMGAccepOn = DateTime.Now;
                objUpdate.DateofDocAcceptanceByPMG = DateTime.Now;
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Details Accepted by PMG";
                objResponseMsg.date = Convert.ToString(Convert.ToDateTime(objUpdate.PMGAccepOn).ToString("dd/MM/yyyy"));
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Details already Accepted by PMG";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //pop up for view submitted document(Lines table)
        public ActionResult SubmittedDocuments(int HeaderId)
        {
            FDS001 objFDS001 = db.FDS001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            return PartialView("_SubmittedDocuments", objFDS001);
        }

        //submit attachment to PMg, set flag for SubmittoPMG and capture DocumentSubmissionDate
        public ActionResult SubmitAttachmentstoPMG(int HeaderId)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            FDS001 objHeader = db.FDS001.Where(h => h.HeaderId == HeaderId).FirstOrDefault();
            List<FDS002> lstFDS002 = db.FDS002.Where(h => h.HeaderId == HeaderId && h.SubmittoPMG == false).ToList();
            List<FDS002> lstSubmitted = db.FDS002.Where(h => h.HeaderId == HeaderId && h.SubmittoPMG == true).ToList();
            List<FDS002> lstheader = db.FDS002.Where(h => h.HeaderId == HeaderId).ToList();

            if (lstheader.Count > 0)
            {
                if (lstFDS002 != null && lstFDS002.Count > 0)
                {
                    lstFDS002.ForEach(x => x.SubmittoPMG = true);
                    lstFDS002.ForEach(x => x.LastDocumentVerificationDatebyQA = DateTime.Now);
                    objHeader.DateOFDocSubmissionTOPMG = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details Submitted to PMG";
                }
                if (lstSubmitted.Count > 0 && lstSubmitted.Count == lstheader.Count)
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details already Submitted to PMG";
                }
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Documents not available";
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Save header data
        [HttpPost]
        public ActionResult SaveFDMSHeader(FDS001 fds001)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                if (fds001.HeaderId > 0)
                {
                    FDS001 objFDS001 = db.FDS001.Where(x => x.HeaderId == fds001.HeaderId).FirstOrDefault();
                    if (objFDS001 != null)
                    {
                        objFDS001.FDMSName = fds001.FDMSName;
                        objFDS001.FDMSDesc = fds001.FDMSDesc;
                        objFDS001.Project = fds001.Project.Split('-')[0].Trim();
                        objFDS001.Location = fds001.Location.Split('-')[0];
                        objFDS001.BU = fds001.BU.Split('-')[0];
                        objFDS001.DesignEngineer = fds001.DesignEngineer.Split('-')[0];
                        objFDS001.QCManager = fds001.QCManager.Split('-')[0];
                        objFDS001.QAManager = fds001.QAManager.Split('-')[0];
                        objFDS001.PMGManager = fds001.PMGManager.Split('-')[0];
                        objFDS001.QAEngineer = fds001.QAEngineer.Split('-')[0];
                        objFDS001.EditedBy = objClsLoginInfo.UserName;
                        objFDS001.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.FDMS.Update.ToString();
                        objResponseMsg.status = objFDS001.Status;
                        objResponseMsg.headerid = objFDS001.HeaderId;

                    }
                }
                else
                {
                    FDS001 objFDS001 = new FDS001();
                    objFDS001.FDMSName = fds001.FDMSName;
                    objFDS001.FDMSDesc = fds001.FDMSDesc;
                    objFDS001.Status = clsImplementationEnum.FDMSStatus.DRAFT.GetStringValue();
                    objFDS001.Project = fds001.Project.Split('-')[0].Trim();
                    objFDS001.Location = fds001.Location.Split('-')[0];
                    objFDS001.BU = fds001.BU.Split('-')[0];
                    objFDS001.DesignEngineer = fds001.DesignEngineer.Split('-')[0];
                    objFDS001.QCManager = fds001.QCManager.Split('-')[0];
                    objFDS001.QAManager = fds001.QAManager.Split('-')[0];
                    objFDS001.PMGManager = fds001.PMGManager.Split('-')[0];
                    objFDS001.QAEngineer = fds001.QAEngineer.Split('-')[0];
                    objFDS001.Originator = objClsLoginInfo.UserName;
                    objFDS001.CreatedOn = DateTime.Now;
                    db.FDS001.Add(objFDS001);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.FDMS.Create.ToString();
                    objResponseMsg.status = objFDS001.Status;
                    objResponseMsg.headerid = objFDS001.HeaderId;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.FDMS.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }
        #endregion

        #region send/Promote to FDR state
        [HttpPost]
        public ActionResult sendToFDR(int headerid, string Project)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                FDS001 objFDS001 = db.FDS001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                {
                    if (objFDS001 != null)
                    {
                        objFDS001.Status = clsImplementationEnum.FDMSStatus.FDR.GetStringValue();
                        objFDS001.EditedBy = objClsLoginInfo.UserName;
                        objFDS001.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        #region Send Mail
                        string emailTo = objFDS001.QAEngineer;
                        string cc1 = objFDS001.QCManager;
                        string cc2 = objFDS001.QAEngineer;

                        if (!string.IsNullOrWhiteSpace(emailTo))
                        {
                            Hashtable _ht = new Hashtable();
                            EmailSend _objEmail = new EmailSend();
                            _objEmail.MailCc = Manager.GetMailIdFromPsNo(cc1) + ";" + Manager.GetMailIdFromPsNo(cc2);
                            _ht["[Approver]"] = Manager.GetUserNameFromPsNo(emailTo);
                            _ht["[FDMSNo]"] = objFDS001.FDMSName;
                            _ht["[Initiator]"] = Manager.GetUserNameFromPsNo(objFDS001.DesignEngineer);
                            _ht["[FromState]"] = clsImplementationEnum.FDMSStatus.DRAFT.GetStringValue();
                            _ht["[ToState]"] = objFDS001.Status;
                            _ht["[FDMSLink]"] = WebsiteURL + "/FDMS/FDMS/CreateFDMs?Id=" + objFDS001.HeaderId;
                            MAIL001 objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.FDMS.PromoteFDMS).SingleOrDefault();
                            _objEmail.MailToAdd = Manager.GetMailIdFromPsNo(objFDS001.Originator); //initiator
                            _ht["[Subject]"] = objFDS001.FDMSName + " is promoted to " + objFDS001.Status;
                            _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                        }
                        #endregion

                        #region Send Notification
                        string notificationmessage = objFDS001.FDMSName + " is submitted to " + objFDS001.Status;
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.ENGG3.GetStringValue(), objFDS001.Project, objFDS001.BU, objFDS001.Location, notificationmessage, clsImplementationEnum.NotificationType.Information.GetStringValue());
                        #endregion

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.FDMS.sentToFDR.ToString();
                        ViewBag.chkProject = Project;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.FDMS.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult EmailForFDRAttachment(int headerid)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                FDS001 objFDS001 = db.FDS001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                {
                    if (objFDS001 != null)
                    {
                        #region Send Mail
                        string emailTo = objFDS001.DesignEngineer + ";" + objFDS001.QAEngineer;

                        if (!string.IsNullOrWhiteSpace(emailTo))
                        {
                            Hashtable _ht = new Hashtable();
                            EmailSend _objEmail = new EmailSend();

                            _ht["[Responsible QA Engineer]"] = Manager.GetUserNameFromPsNo(objFDS001.QAEngineer);
                            _ht["[FDMSNo]"] = objFDS001.FDMSName;
                            _ht["[Responsible Design Engineer]"] = Manager.GetUserNameFromPsNo(objFDS001.DesignEngineer);
                            _ht["[ToState]"] = objFDS001.Status;
                            _ht["[FDMSLink]"] = WebsiteURL + "/FDMS/FDMS/CreateFDMs?Id=" + objFDS001.HeaderId;

                            MAIL001 objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.FDMS.AttachedDocFDMS).SingleOrDefault();
                            //_objEmail.MailToAdd = Manager.GetMailIdFromPsNo(objFDS001.Originator); //initiator
                            _objEmail.MailToAdd = Manager.GetMailIdFromPsNo(objFDS001.DesignEngineer);
                            _objEmail.MailToAdd = Manager.GetMailIdFromPsNo(objFDS001.QAEngineer);
                            _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "Email sent successfully";
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.FDMS.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }
        #endregion

        #region promote header
        [HttpPost]
        public ActionResult Promote(int headerid, string status)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            bool datakey = true;
            try
            {
                string Message = "";
                string emailTo = string.Empty;
                string ccTo = string.Empty;
                string assignees = string.Empty;
                string role = string.Empty;
                var lstAssignees = db.FDS002.Where(x => x.HeaderId == headerid && x.ResponsiblePerson.Trim() != null).Select(o => o.ResponsiblePerson.Trim()).ToList();

                FDS001 objFDS001 = db.FDS001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                {
                    if (objFDS001 != null)
                    {
                        var objFDS002 = db.FDS002.Where(x => x.HeaderId == headerid).ToList();
                        if (status == clsImplementationEnum.FDMSStatus.FDR.GetStringValue())
                        {
                            objFDS001.Status = clsImplementationEnum.FDMSStatus.DocAssign.GetStringValue();
                            objFDS001.FDRBy = objClsLoginInfo.UserName;
                            objFDS001.FDROn = DateTime.Now;
                            role = clsImplementationEnum.UserRoleName.QA3.GetStringValue();
                            Message = clsImplementationMessage.FDMS.FDRPromte.ToString();
                            emailTo = Manager.GetMailIdFromPsNo(objFDS001.QAEngineer);
                            ccTo = Manager.GetMailIdFromPsNo(objFDS001.DesignEngineer);
                        }
                        else if (status == clsImplementationEnum.FDMSStatus.DocAssign.GetStringValue())
                        {
                            if (objFDS002.Count > 0)
                            {
                                objFDS001.Status = clsImplementationEnum.FDMSStatus.SCNVer.GetStringValue();
                                objFDS001.DocAssBy = objClsLoginInfo.UserName;
                                objFDS001.DocAssOn = DateTime.Now;

                                Message = clsImplementationMessage.FDMS.DocPromte.ToString();
                                //for email 
                                //list of assignees from fds002
                                if (lstAssignees.Count > 0 || lstAssignees != null)
                                {
                                    for (var i = 0; i < lstAssignees.Count; i++)
                                    {
                                        lstAssignees[i] = Manager.GetMailIdFromPsNo(lstAssignees[i]);
                                    }
                                    assignees = String.Join(";", lstAssignees);
                                    //mail to QC2 role
                                    emailTo = Manager.GetMailIdFromPsNo(objFDS001.QCManager) + ";" + assignees;

                                }
                                else
                                {
                                    emailTo = Manager.GetMailIdFromPsNo(objFDS001.QCManager);
                                }
                                //mail to QA3
                                role = clsImplementationEnum.UserRoleName.QC2.GetStringValue();
                                ccTo = Manager.GetMailIdFromPsNo(objFDS001.QAEngineer);
                            }
                            else
                            {
                                datakey = false;
                                Message = "Documents not available";
                            }
                        }
                        else if (status == clsImplementationEnum.FDMSStatus.SCNVer.GetStringValue())
                        {
                            if (objFDS002.Count > 0)
                            {
                                objFDS001.Status = clsImplementationEnum.FDMSStatus.DocVeri.GetStringValue();
                                objFDS001.SCNBy = objClsLoginInfo.UserName;
                                objFDS001.SCNOn = DateTime.Now;
                                Message = clsImplementationMessage.FDMS.SCNPromte.ToString();
                                if (lstAssignees.Count > 0 || lstAssignees != null)
                                {
                                    for (var i = 0; i < lstAssignees.Count; i++)
                                    {
                                        lstAssignees[i] = Manager.GetMailIdFromPsNo(lstAssignees[i]);
                                    }
                                    assignees = String.Join(";", lstAssignees);
                                }
                                emailTo = assignees;
                                role = clsImplementationEnum.UserRoleName.QA3.GetStringValue() + "," + clsImplementationEnum.UserRoleName.QC3.GetStringValue();
                                ccTo = Manager.GetMailIdFromPsNo(objFDS001.QAEngineer);
                            }
                            else
                            {
                                datakey = false;
                                Message = "Documents not available";
                            }
                        }
                        else if (status == clsImplementationEnum.FDMSStatus.DocVeri.GetStringValue())
                        {
                            objFDS001.Status = clsImplementationEnum.FDMSStatus.QAAccept.GetStringValue();
                            objFDS001.DocVeriBy = objClsLoginInfo.UserName;
                            objFDS001.DocVeriOn = DateTime.Now;
                            Message = clsImplementationMessage.FDMS.DocVerificationPromte.ToString();
                            role = clsImplementationEnum.UserRoleName.QA3.GetStringValue();
                            emailTo = Manager.GetMailIdFromPsNo(objFDS001.QAEngineer);
                            ccTo = Manager.GetMailIdFromPsNo(objFDS001.QAEngineer);

                            //if (objFDS002.Count > 0)
                            //{
                            //    var lstAccepted = db.FDS002.Where(x => x.HeaderId == headerid).ToList();
                            //    if (lstAccepted.Any(a => a.Status != clsImplementationEnum.LineStatusFDMS.Accepted.GetStringValue()))
                            //    {
                            //        datakey = false;
                            //        Message = "To approve FDMS , Documents must be accepted..!! ";
                            //    }
                            //    else
                            //    {
                            //        objFDS001.Status = clsImplementationEnum.FDMSStatus.QAAccept.GetStringValue();
                            //        objFDS001.DocVeriBy = objClsLoginInfo.UserName;
                            //        objFDS001.DocVeriOn = DateTime.Now;
                            //        Message = clsImplementationMessage.FDMS.DocVerificationPromte.ToString();
                            //        role = clsImplementationEnum.UserRoleName.QA3.GetStringValue();
                            //        emailTo = Manager.GetMailIdFromPsNo(objFDS001.QAEngineer);
                            //        ccTo = Manager.GetMailIdFromPsNo(objFDS001.QAEngineer);
                            //    }
                            //}
                            //else
                            //{
                            //    datakey = false;
                            //    Message = "Documents not available";
                            //}
                        }
                        else if (status == clsImplementationEnum.FDMSStatus.QAAccept.GetStringValue())
                        {
                            if (objFDS002.Count > 0)
                            {
                                List<FDS002> lstFDS002 = db.FDS002.Where(h => h.HeaderId == headerid && h.SubmittoPMG == true).ToList();
                                var folderPath = "FDS001//Documents//" + headerid;
                                //var existing = (new clsFileUpload()).GetDocuments(folderPath);

                                IEMQS.Areas.Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                                if (!_objFUC.CheckAnyDocumentsExits(folderPath, headerid))
                                {
                                    datakey = false;
                                    Message = "Please Upload Documents..!!";
                                }
                                if (datakey)
                                {
                                    if (lstFDS002 != null && lstFDS002.Count > 0)
                                    {
                                        var lstAccepted = db.FDS002.Where(x => x.HeaderId == headerid).ToList();
                                        if (lstAccepted.Any(a => a.Status != clsImplementationEnum.LineStatusFDMS.Accepted.GetStringValue()))
                                        {
                                            datakey = false;
                                            Message = "To approve FDMS , Documents must be accepted..!! ";
                                        }
                                        else
                                        {
                                            objFDS001.Status = clsImplementationEnum.FDMSStatus.PMGAccept.GetStringValue();
                                            objFDS001.QAVeriBy = objClsLoginInfo.UserName;
                                            objFDS001.QAVeriOn = DateTime.Now;
                                            role = clsImplementationEnum.UserRoleName.PMG2.GetStringValue();
                                            Message = clsImplementationMessage.FDMS.QAPromte.ToString();
                                            emailTo = Manager.GetMailIdFromPsNo(objFDS001.PMGManager);
                                            ccTo = Manager.GetMailIdFromPsNo(objFDS001.QAEngineer);
                                        }
                                    }
                                    else
                                    {
                                        datakey = false;
                                        Message = "Please submit documents before sending it to PMG";
                                    }
                                }
                            }
                            else
                            {
                                datakey = false;
                                Message = "Documents not available";
                            }
                        }
                        else if (status == clsImplementationEnum.FDMSStatus.PMGAccept.GetStringValue())
                        {
                            if (objFDS001.PMGAccepOn != null)
                            {
                                if (objFDS002 != null)
                                {
                                    var lstAccepted = db.FDS002.Where(x => x.HeaderId == headerid).ToList();
                                    if (lstAccepted.Any(a => a.Status != clsImplementationEnum.LineStatusFDMS.Accepted.GetStringValue()))
                                    {
                                        datakey = false;
                                        Message = "To approve FDMS , Documents must be accepted..!! ";
                                    }
                                    else
                                    {
                                        objFDS001.Status = clsImplementationEnum.FDMSStatus.Completed.GetStringValue();
                                        objFDS001.PMGAccepBy = objClsLoginInfo.UserName;
                                        //objFDS001.PMGAccepOn = DateTime.Now;
                                        objFDS001.CompletedBy = objClsLoginInfo.UserName;
                                        objFDS001.CompletedOn = DateTime.Now;
                                        Message = clsImplementationMessage.FDMS.PMGPromte.ToString();
                                        role = clsImplementationEnum.UserRoleName.ENGG3.GetStringValue();
                                        emailTo = Manager.GetMailIdFromPsNo(objFDS001.DesignEngineer);
                                        ccTo = Manager.GetMailIdFromPsNo(objFDS001.PMGManager);
                                    }
                                }
                                else
                                {
                                    datakey = false;
                                    Message = "Documents not available";
                                }
                            }
                            else
                            {
                                datakey = false;
                                Message = "Please accept documents before sending it to customer";
                            }
                        }
                        else if (status == clsImplementationEnum.FDMSStatus.RetDraft.GetStringValue())
                        {
                            objFDS001.Status = clsImplementationEnum.FDMSStatus.FDR.GetStringValue();
                            role = clsImplementationEnum.UserRoleName.QA3.GetStringValue();
                            Message = clsImplementationMessage.FDMS.sentToFDR.ToString();
                            emailTo = Manager.GetMailIdFromPsNo(objFDS001.QAEngineer);
                            ccTo = Manager.GetMailIdFromPsNo(objFDS001.QCManager) + ";" + Manager.GetMailIdFromPsNo(objFDS001.QAEngineer);

                        }
                        else if (status == clsImplementationEnum.FDMSStatus.RetFDR.GetStringValue())
                        {
                            objFDS001.Status = clsImplementationEnum.FDMSStatus.DocAssign.GetStringValue();
                            objFDS001.FDRBy = objClsLoginInfo.UserName;
                            objFDS001.FDROn = DateTime.Now;
                            role = clsImplementationEnum.UserRoleName.QA3.GetStringValue();
                            Message = clsImplementationMessage.FDMS.FDRPromte.ToString();
                            emailTo = Manager.GetMailIdFromPsNo(objFDS001.QAEngineer);
                            ccTo = Manager.GetMailIdFromPsNo(objFDS001.DesignEngineer);
                        }
                        else if (status == clsImplementationEnum.FDMSStatus.RetDocAssign.GetStringValue())
                        {
                            objFDS001.Status = clsImplementationEnum.FDMSStatus.SCNVer.GetStringValue();
                            objFDS001.DocAssBy = objClsLoginInfo.UserName;

                            objFDS001.DocAssOn = DateTime.Now;
                            Message = clsImplementationMessage.FDMS.DocPromte.ToString();
                            if (lstAssignees.Count > 0 || lstAssignees != null)
                            {
                                for (var i = 0; i < lstAssignees.Count; i++)
                                {
                                    lstAssignees[i] = Manager.GetMailIdFromPsNo(lstAssignees[i]);
                                }
                                assignees = String.Join(";", lstAssignees);
                                //mail to QC2 role
                                emailTo = Manager.GetMailIdFromPsNo(objFDS001.QCManager) + ";" + assignees;
                                role = clsImplementationEnum.UserRoleName.QC2.GetStringValue();
                            }
                            else
                            {
                                emailTo = Manager.GetMailIdFromPsNo(objFDS001.QCManager);
                            }
                            //mail to QA3
                            ccTo = Manager.GetMailIdFromPsNo(objFDS001.QAEngineer);
                        }
                        else if (status == clsImplementationEnum.FDMSStatus.RetSCN.GetStringValue())
                        {
                            objFDS001.Status = clsImplementationEnum.FDMSStatus.DocVeri.GetStringValue();
                            objFDS001.SCNBy = objClsLoginInfo.UserName;
                            objFDS001.SCNOn = DateTime.Now;
                            Message = clsImplementationMessage.FDMS.SCNPromte.ToString();
                            if (lstAssignees.Count > 0 || lstAssignees != null)
                            {
                                for (var i = 0; i < lstAssignees.Count; i++)
                                {
                                    lstAssignees[i] = Manager.GetMailIdFromPsNo(lstAssignees[i]);
                                }
                                assignees = String.Join(";", lstAssignees);
                            }
                            emailTo = assignees;
                            role = clsImplementationEnum.UserRoleName.QA3.GetStringValue() + "," + clsImplementationEnum.UserRoleName.QC3.GetStringValue();
                            ccTo = Manager.GetMailIdFromPsNo(objFDS001.QAEngineer);
                        }
                        else if (status == clsImplementationEnum.FDMSStatus.RetDocVeri.GetStringValue())
                        {
                            //add date
                            objFDS001.LineSubmitDate = DateTime.Now.AddDays(3);
                            objFDS001.Status = clsImplementationEnum.FDMSStatus.QAAccept.GetStringValue();
                            objFDS001.QAVeriBy = objClsLoginInfo.UserName;
                            objFDS001.QAVeriOn = DateTime.Now;
                            Message = clsImplementationMessage.FDMS.DocVerificationPromte.ToString();
                            role = clsImplementationEnum.UserRoleName.QA3.GetStringValue();
                            emailTo = Manager.GetMailIdFromPsNo(objFDS001.QAEngineer);
                            ccTo = Manager.GetMailIdFromPsNo(objFDS001.QAEngineer);
                        }
                        else if (status == clsImplementationEnum.FDMSStatus.RetQA.GetStringValue())
                        {
                            objFDS001.Status = clsImplementationEnum.FDMSStatus.PMGAccept.GetStringValue();
                            objFDS001.PMGAccepBy = objClsLoginInfo.UserName;
                            objFDS001.PMGAccepOn = DateTime.Now;
                            role = clsImplementationEnum.UserRoleName.PMG2.GetStringValue();
                            Message = clsImplementationMessage.FDMS.QAPromte.ToString();
                            emailTo = Manager.GetMailIdFromPsNo(objFDS001.DesignEngineer);
                            ccTo = Manager.GetMailIdFromPsNo(objFDS001.PMGManager);
                        }

                        objFDS001.EditedBy = objClsLoginInfo.UserName;
                        objFDS001.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Key = datakey;
                        objResponseMsg.Value = Message;

                        #region Send Mail

                        string fromState = status;
                        string toState = objFDS001.Status;
                        string FDMSNo = objFDS001.FDMSName;
                        string[] lstemailTo = new string[0];
                        bool sendToMultiple = false;
                        if (emailTo.ToLower().Contains(';'))
                        {
                            lstemailTo = emailTo.Split(';');
                            sendToMultiple = true;
                        }

                        if (!string.IsNullOrWhiteSpace(emailTo))
                        {
                            if (sendToMultiple)
                                foreach (var individualmail in lstemailTo)
                                {
                                    Hashtable _ht = new Hashtable();
                                    EmailSend _objEmail = new EmailSend();
                                    _ht["[CC]"] = ccTo;
                                    _ht["[Approver]"] = Manager.GetUserNameFromPsNo(individualmail);
                                    _ht["[FDMSNo]"] = FDMSNo;
                                    _ht["[Initiator]"] = Manager.GetUserNameFromPsNo(objClsLoginInfo.UserName);
                                    _ht["[FromState]"] = fromState;
                                    _ht["[ToState]"] = toState;
                                    _ht["[FDMSLink]"] = WebsiteURL + "/FDMS/FDMS/CreateFDMs?Id=" + objFDS001.HeaderId;
                                    MAIL001 objTemplateMaster = new MAIL001();
                                    if (objFDS001.Status != clsImplementationEnum.FDMSStatus.Completed.GetStringValue())
                                    {
                                        objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.FDMS.PromoteFDMS).SingleOrDefault();
                                        _ht["[Subject]"] = objFDS001.FDMSName + " Is promoted to " + toState;
                                    }
                                    else
                                    {
                                        objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.FDMS.CompleteFDMS).SingleOrDefault();
                                        _ht["[Subject]"] = objFDS001.FDMSName + " Is " + toState;
                                    }
                                    _objEmail.MailToAdd = Manager.GetMailIdFromPsNo(individualmail);
                                    _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                                }
                            else
                            {
                                Hashtable _ht = new Hashtable();
                                EmailSend _objEmail = new EmailSend();
                                _ht["[CC]"] = ccTo;
                                _ht["[Approver]"] = Manager.GetUserNameFromPsNo(emailTo);
                                _ht["[FDMSNo]"] = FDMSNo;
                                _ht["[Initiator]"] = Manager.GetUserNameFromPsNo(objClsLoginInfo.UserName);
                                _ht["[FromState]"] = fromState;
                                _ht["[ToState]"] = toState;
                                _ht["[FDMSLink]"] = WebsiteURL + "/FDMS/FDMS/CreateFDMs?Id=" + objFDS001.HeaderId;
                                MAIL001 objTemplateMaster = new MAIL001();
                                if (objFDS001.Status != clsImplementationEnum.FDMSStatus.Completed.GetStringValue())
                                {
                                    objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.FDMS.PromoteFDMS).SingleOrDefault();
                                    _ht["[Subject]"] = objFDS001.FDMSName + " Is promoted to " + toState;
                                }
                                else
                                {
                                    objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.FDMS.CompleteFDMS).SingleOrDefault();
                                    _ht["[Subject]"] = objFDS001.FDMSName + " Is " + toState;
                                }
                                _objEmail.MailToAdd = Manager.GetMailIdFromPsNo(emailTo);
                                _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                            }
                        }
                        #endregion


                        #region Send Notification
                        if (datakey && !string.IsNullOrWhiteSpace(role))
                        {
                            string notificationmessage = objFDS001.FDMSName + " is submitted to " + objFDS001.Status;
                            (new clsManager()).SendNotification(role, objFDS001.Project, objFDS001.BU, objFDS001.Location, notificationmessage, clsImplementationEnum.NotificationType.Information.GetStringValue());
                        }
                        #endregion

                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.FDMS.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }
        #endregion

        #region Demote header
        [HttpPost]
        public ActionResult Demote(int headerid, string Comments)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                FDS001 objFDS001 = db.FDS001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                {
                    string Message = "";
                    string emailTo = string.Empty;
                    string ccTo = string.Empty;
                    string assignees = string.Empty;
                    string role = string.Empty;
                    if (objFDS001 != null)
                    {
                        string status = objFDS001.Status;

                        if (status == clsImplementationEnum.FDMSStatus.FDR.GetStringValue())
                        {
                            objFDS001.Status = clsImplementationEnum.FDMSStatus.RetDraft.GetStringValue();
                            objFDS001.ReturnToDraftComments = Comments;
                            objFDS001.FDROn = null;
                            Message = clsImplementationMessage.FDMS.FDRDemote.ToString();
                            emailTo = Manager.GetMailIdFromPsNo(objFDS001.QCManager) + ";" + Manager.GetMailIdFromPsNo(objFDS001.QAEngineer);
                            ccTo = Manager.GetMailIdFromPsNo(objFDS001.DesignEngineer);
                            role = clsImplementationEnum.UserRoleName.QC3.GetStringValue() + "," + clsImplementationEnum.UserRoleName.QC2.GetStringValue();
                        }
                        else if (status == clsImplementationEnum.FDMSStatus.DocAssign.GetStringValue())
                        {
                            objFDS001.Status = clsImplementationEnum.FDMSStatus.RetFDR.GetStringValue();
                            objFDS001.ReturnToFDRComments = Comments;
                            objFDS001.FDROn = null;
                            Message = clsImplementationMessage.FDMS.DocDemote.ToString();
                            emailTo = Manager.GetMailIdFromPsNo(objFDS001.DesignEngineer);
                            ccTo = Manager.GetMailIdFromPsNo(objFDS001.QAEngineer);
                            role = clsImplementationEnum.UserRoleName.ENGG3.GetStringValue();
                        }
                        else if (status == clsImplementationEnum.FDMSStatus.SCNVer.GetStringValue())
                        {
                            objFDS001.Status = clsImplementationEnum.FDMSStatus.RetDocAssign.GetStringValue();
                            objFDS001.ReturnToDocAssignComments = Comments;
                            objFDS001.DocVeriOn = null;
                            role = clsImplementationEnum.UserRoleName.QA3.GetStringValue();
                            Message = clsImplementationMessage.FDMS.SCNDemote.ToString();
                            emailTo = Manager.GetMailIdFromPsNo(objFDS001.QAEngineer);
                            ccTo = Manager.GetMailIdFromPsNo(objFDS001.QCManager);
                        }
                        else if (status == clsImplementationEnum.FDMSStatus.DocVeri.GetStringValue())
                        {
                            objFDS001.Status = clsImplementationEnum.FDMSStatus.RetSCN.GetStringValue();
                            objFDS001.ReturnToSCNComments = Comments;
                            objFDS001.SCNOn = null;
                            role = clsImplementationEnum.UserRoleName.QC2.GetStringValue();
                            Message = clsImplementationMessage.FDMS.DocVerificatioDemote.ToString();
                            emailTo = Manager.GetMailIdFromPsNo(objFDS001.QCManager);
                            ccTo = Manager.GetMailIdFromPsNo(objFDS001.QAEngineer);
                        }
                        else if (status == clsImplementationEnum.FDMSStatus.QAAccept.GetStringValue())
                        {
                            objFDS001.Status = clsImplementationEnum.FDMSStatus.RetDocVeri.GetStringValue();
                            objFDS001.DocVeriOn = null;
                            objFDS001.ReturnToDocVeriComments = Comments;
                            role = clsImplementationEnum.UserRoleName.QA3.GetStringValue();
                            Message = clsImplementationMessage.FDMS.QADemote.ToString();
                            emailTo = Manager.GetMailIdFromPsNo(objFDS001.QAEngineer);
                            ccTo = Manager.GetMailIdFromPsNo(objFDS001.QAEngineer);
                        }
                        else if (status == clsImplementationEnum.FDMSStatus.PMGAccept.GetStringValue())
                        {
                            objFDS001.Status = clsImplementationEnum.FDMSStatus.RetQA.GetStringValue();
                            objFDS001.QAVeriOn = null;
                            objFDS001.PMGAccepOn = null;
                            objFDS001.CompletedOn = null;
                            role = clsImplementationEnum.UserRoleName.QA3.GetStringValue();
                            objFDS001.ReturnToQAComments = Comments;
                            Message = clsImplementationMessage.FDMS.PMGDemote.ToString();
                            emailTo = Manager.GetMailIdFromPsNo(objFDS001.QAEngineer);
                            ccTo = Manager.GetMailIdFromPsNo(objFDS001.PMGManager);
                        }
                        else if (status == clsImplementationEnum.FDMSStatus.RetFDR.GetStringValue())
                        {
                            objFDS001.Status = clsImplementationEnum.FDMSStatus.RetDraft.GetStringValue();
                            objFDS001.FDROn = null;
                            objFDS001.ReturnToDraftComments = Comments;
                            Message = clsImplementationMessage.FDMS.FDRDemote.ToString();
                            emailTo = Manager.GetMailIdFromPsNo(objFDS001.QCManager) + ";" + Manager.GetMailIdFromPsNo(objFDS001.QAEngineer);
                            ccTo = Manager.GetMailIdFromPsNo(objFDS001.DesignEngineer);
                            role = clsImplementationEnum.UserRoleName.QC3.GetStringValue() + "," + clsImplementationEnum.UserRoleName.QC2.GetStringValue();
                        }
                        else if (status == clsImplementationEnum.FDMSStatus.RetDocAssign.GetStringValue())
                        {
                            objFDS001.Status = clsImplementationEnum.FDMSStatus.RetFDR.GetStringValue();
                            objFDS001.DocAssOn = null;
                            objFDS001.ReturnToFDRComments = Comments;
                            Message = clsImplementationMessage.FDMS.DocDemote.ToString();
                            emailTo = Manager.GetMailIdFromPsNo(objFDS001.DesignEngineer);
                            ccTo = Manager.GetMailIdFromPsNo(objFDS001.QAEngineer);
                            role = clsImplementationEnum.UserRoleName.ENGG3.GetStringValue();
                        }
                        else if (status == clsImplementationEnum.FDMSStatus.RetSCN.GetStringValue())
                        {
                            objFDS001.Status = clsImplementationEnum.FDMSStatus.RetDocAssign.GetStringValue();
                            objFDS001.SCNOn = null;
                            objFDS001.ReturnToDocAssignComments = Comments;
                            Message = clsImplementationMessage.FDMS.SCNDemote.ToString();
                            emailTo = Manager.GetMailIdFromPsNo(objFDS001.QAEngineer);
                            ccTo = Manager.GetMailIdFromPsNo(objFDS001.QCManager);
                            role = clsImplementationEnum.UserRoleName.QA3.GetStringValue();
                        }
                        else if (status == clsImplementationEnum.FDMSStatus.RetDocVeri.GetStringValue())
                        {
                            objFDS001.Status = clsImplementationEnum.FDMSStatus.RetSCN.GetStringValue();
                            objFDS001.DocVeriOn = null;
                            objFDS001.ReturnToSCNComments = Comments;
                            Message = clsImplementationMessage.FDMS.DocVerificatioDemote.ToString();
                            emailTo = Manager.GetMailIdFromPsNo(objFDS001.QCManager);
                            ccTo = Manager.GetMailIdFromPsNo(objFDS001.QAEngineer);
                            role = clsImplementationEnum.UserRoleName.QC2.GetStringValue();
                        }
                        else if (status == clsImplementationEnum.FDMSStatus.RetQA.GetStringValue())
                        {
                            objFDS001.Status = clsImplementationEnum.FDMSStatus.RetDocVeri.GetStringValue();
                            objFDS001.QAVeriOn = null;
                            objFDS001.ReturnToQAComments = Comments;
                            Message = clsImplementationMessage.FDMS.QADemote.ToString();
                            emailTo = Manager.GetMailIdFromPsNo(objFDS001.QAEngineer);
                            ccTo = Manager.GetMailIdFromPsNo(objFDS001.QAEngineer);
                            role = clsImplementationEnum.UserRoleName.QA3.GetStringValue();
                        }
                        objFDS001.EditedBy = objClsLoginInfo.UserName;
                        objFDS001.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = Message;

                        #region Send Mail

                        string fromState = status;
                        string toState = objFDS001.Status;
                        string FDMSNo = objFDS001.FDMSName;
                        string[] lstemailTo = new string[0];
                        bool sendToMultiple = false;
                        if (emailTo.ToLower().Contains(';'))
                        {
                            lstemailTo = emailTo.Split(';');
                            sendToMultiple = true;
                        }

                        if (!string.IsNullOrWhiteSpace(emailTo))
                        {
                            if (sendToMultiple)
                                foreach (var individualmail in lstemailTo)
                                {
                                    Hashtable _ht = new Hashtable();
                                    EmailSend _objEmail = new EmailSend();
                                    _ht["[CC]"] = ccTo;
                                    _ht["[Approver]"] = Manager.GetUserNameFromPsNo(objClsLoginInfo.UserName);
                                    _ht["[FDMSNo]"] = FDMSNo;
                                    _ht["[Initiator]"] = Manager.GetUserNameFromPsNo(objFDS001.DesignEngineer);
                                    _ht["[CCPerson]"] = Manager.GetUserNameFromPsNo(objClsLoginInfo.UserName);
                                    _ht["[FromState]"] = fromState;
                                    _ht["[ToState]"] = toState;
                                    _ht["[ReturnRemark]"] = Comments;
                                    _ht["[FDMSLink]"] = WebsiteURL + "/FDMS/FDMS/CreateFDMs?Id=" + objFDS001.HeaderId;
                                    MAIL001 objTemplateMaster = new MAIL001();
                                    objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.FDMS.DemoteFDMS).SingleOrDefault();
                                    _ht["[Subject]"] = objFDS001.FDMSName + " is demoted to" + objFDS001.Status;
                                    _objEmail.MailToAdd = Manager.GetMailIdFromPsNo(individualmail);
                                    _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                                }
                            else
                            {
                                Hashtable _ht = new Hashtable();
                                EmailSend _objEmail = new EmailSend();
                                _ht["[CC]"] = ccTo;
                                _ht["[Approver]"] = Manager.GetUserNameFromPsNo(objClsLoginInfo.UserName);
                                _ht["[FDMSNo]"] = FDMSNo;
                                _ht["[Initiator]"] = Manager.GetUserNameFromPsNo(objFDS001.DesignEngineer);
                                _ht["[CCPerson]"] = Manager.GetUserNameFromPsNo(objClsLoginInfo.UserName);
                                _ht["[FromState]"] = fromState;
                                _ht["[ToState]"] = toState;
                                _ht["[FDMSLink]"] = WebsiteURL + "/FDMS/FDMS/CreateFDMs?Id=" + objFDS001.HeaderId;
                                MAIL001 objTemplateMaster = new MAIL001();
                                objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.FDMS.DemoteFDMS).SingleOrDefault();
                                _ht["[Subject]"] = objFDS001.FDMSName + " is demoted to" + objFDS001.Status;
                                _objEmail.MailToAdd = Manager.GetMailIdFromPsNo(emailTo);
                                _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                            }
                        }
                        #endregion


                        #region Send Notification
                        if (objResponseMsg.Key && !string.IsNullOrWhiteSpace(role))
                        {
                            string notificationmessage = objFDS001.FDMSName + " is demoted to " + toState;
                            (new clsManager()).SendNotification(role, objFDS001.Project, objFDS001.BU, objFDS001.Location, notificationmessage, clsImplementationEnum.NotificationType.Information.GetStringValue());
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.FDMS.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }
        #endregion

        [HttpPost]
        public ActionResult GetInsertView(string documentNum, string Headerid)
        {
            ViewBag.documentNum = documentNum;
            ViewBag.Headerid = Convert.ToInt32(Headerid);
            FDS002 objFDS002 = new FDS002();
            return PartialView("_DocumentAssignmentLines", objFDS002);
        }
        [HttpPost]
        public ActionResult GetUpdateView(int headerid, int lineid)
        {
            ViewBag.Headerid = Convert.ToInt32(headerid);
            FDS002 objFDS002 = new FDS002();
            objFDS002 = db.FDS002.Where(x => x.LineId == lineid).FirstOrDefault();
            return PartialView("_DocumentAssignmentLines", objFDS002);
        }
        [HttpPost]
        public ActionResult GetActionByDept(string emp)
        {
            string dept = Manager.GetDepartmentByPsno(emp);
            return Json(dept, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetEmployeeResult(string term, string Dept)
        {
            List<Employee> lstEmployee = getEmployeeAutocompletebyDept(term, Dept).ToList();
            return Json(lstEmployee, JsonRequestBehavior.AllowGet);
        }
        public List<Employee> getEmployeeAutocompletebyDept(string term, string dept)
        {
            List<Employee> lstEmployee = new List<Employee>();
            lstEmployee = (from a in db.COM003
                           where (a.t_actv == 1 && (a.t_psno.Contains(term) ||
                           a.t_name.Contains(term) ||
                           (a.t_psno + " - " + a.t_name).Contains(term))) && (a.t_depc == dept)
                           select
                           new Employee { psnum = a.t_psno, name = a.t_psno + " - " + a.t_name }).ToList();
            return lstEmployee;
        }
        [HttpPost]
        public ActionResult DeleteFDMSLine(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                FDS002 objFDS002 = db.FDS002.Where(x => x.LineId == Id).FirstOrDefault();
                db.FDS002.Remove(objFDS002);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult updateQA(int headerid, string retaintion, string Cess)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                FDS001 objFDS001 = db.FDS001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                objFDS001.retaintion = retaintion;
                objFDS001.Cess = Cess;
                db.SaveChanges();
                objResponseMsg.Key = true;

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult updateDelay(string DelayComment, string headerid)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int hd = Convert.ToInt32(headerid);
                FDS001 objFDS001 = db.FDS001.Where(x => x.HeaderId == hd).FirstOrDefault();
                objFDS001.DelayComment = DelayComment;
                db.SaveChanges();
                objResponseMsg.Key = true;

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult updatePMG(string headerid, string subcust, string custdate)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int hd = Convert.ToInt32(headerid);
                if (!string.IsNullOrWhiteSpace(custdate))
                {
                    FDS001 objFDS001 = db.FDS001.Where(x => x.HeaderId == hd).FirstOrDefault();
                    objFDS001.SubmittedToCust = true;

                    objFDS001.SubmittedToCustOn = Convert.ToDateTime(DateTime.ParseExact(custdate.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture));
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Key = false;
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult updateDelay2(string DelayComment, string LineID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var Status = "";
                int hd = Convert.ToInt32(LineID);
                Status = (from a in db.FDS002
                          where a.LineId == hd
                          select a.Status).FirstOrDefault();
                if (Status.ToLower() == clsImplementationEnum.LineStatusFDMS.Assigned.GetStringValue().ToLower())
                {
                    FDS002 objFDS001 = db.FDS002.Where(x => x.LineId == hd).FirstOrDefault();
                    objFDS001.DelayComments = DelayComment;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "you can't Update Delay Comments as line is " + Status + "..!";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SaveFDMSLines(FDS002 fds002, FormCollection fc)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                bool maildate = false;
                bool validDate = true;
                if (!string.IsNullOrWhiteSpace(fc["MailRestartdate0"].ToString()))
                {
                    if (CheckDate(fc["MailRestartdate0"].ToString()))
                    {
                        maildate = true;
                    }
                    else
                    {
                        validDate = false;
                        maildate = false;
                    }
                }
                //&& CheckDate(fc["DocumentSubmissionDate0"].ToString())
                if (validDate && CheckDate(fc["TargetDate0"].ToString()))
                {
                    if (!CheckTargetDate(fc["TargetDate0"].ToString(), false))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Target Date value must be greater than or equals to todays date";
                    }
                    else
                    {

                        int hd = Convert.ToInt32(fc["HeaderId0"].ToString());
                        FDS001 obj = db.FDS001.Where(a => a.HeaderId == hd).FirstOrDefault();
                        FDS002 objFDS002 = new FDS002();
                        objFDS002.HeaderId = hd;
                        objFDS002.FDMSName = obj.FDMSName;
                        objFDS002.FDMSDocumentName = fc["FDMSDocumentName0"].ToString();
                        objFDS002.DocumentNumber = fc["DocumentNumber0"].ToString();
                        objFDS002.Status = clsImplementationEnum.LineStatusFDMS.Assigned.GetStringValue();
                        objFDS002.Department = fc["Department0"].ToString().Split('-')[0].Trim();
                        objFDS002.ResponsiblePerson = fc["ResponsiblePerson0"].ToString().Split('-')[0].Trim();
                        objFDS002.TargetDate = Convert.ToDateTime(fc["TargetDate0"].ToString());
                        string TargetDate = fc["TargetDate0"].ToString();
                        if (!string.IsNullOrWhiteSpace(TargetDate))
                        {
                            objFDS002.TargetDate = Convert.ToDateTime(TargetDate);// ; DateTime.ParseExact(TargetDate.ToString(), @"d/M/yyyy", CultureInfo.InvariantCulture);
                        }

                        objFDS002.DocumentAssignedDate = DateTime.Now;
                        if (maildate)
                        {
                            objFDS002.MailRestartdate = Convert.ToDateTime(fc["MailRestartdate0"]);
                            string MailRestartdate = fc["MailRestartdate0"].ToString();
                            if (!string.IsNullOrWhiteSpace(MailRestartdate))
                            {
                                objFDS002.MailRestartdate = Convert.ToDateTime(MailRestartdate);//
                            }
                        }
                        objFDS002.CreatedBy = objClsLoginInfo.UserName;
                        objFDS002.CreatedOn = DateTime.Now;
                        db.FDS002.Add(objFDS002);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.FDMS.LInsert.ToString();
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Invalid Date";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateData(string rowId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (columnName == "TargetDate" || columnName == "DocumentSubmissionDate" || columnName == "MailRestartdate")
                {
                    if (CheckDate(columnValue))
                    {
                        if (columnName == "TargetDate" && CheckTargetDate(columnValue, false))
                        {
                            db.SP_FDMS_STAGEMASTER_ROW_UPDATE(Convert.ToInt32(rowId), columnName, columnValue);
                            objResponseMsg.Key = true;
                        }
                        else
                        {
                            db.SP_FDMS_STAGEMASTER_ROW_UPDATE(Convert.ToInt32(rowId), columnName, columnValue);
                            objResponseMsg.Key = true;
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Invalid Date";
                    }
                }
                //else if (columnName == "Department")
                //{

                //    objResponseMsg.Key = true;

                //}
                else if (columnName == "ResponsiblePerson")
                {
                    columnValue = columnValue.Split('-')[0].ToString().Trim();
                    db.SP_FDMS_STAGEMASTER_ROW_UPDATE(Convert.ToInt32(rowId), columnName, columnValue);
                    string dept = Manager.GetDepartmentByPsno(columnValue).Trim();
                    db.SP_FDMS_STAGEMASTER_ROW_UPDATE(Convert.ToInt32(rowId), "Department", dept.Split('-')[0].ToString());
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = dept;

                }
                else
                {
                    db.SP_FDMS_STAGEMASTER_ROW_UPDATE(Convert.ToInt32(rowId), columnName, columnValue);
                    objResponseMsg.Key = true;
                }

            }
            catch
            {
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getFile(string HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            //MemoryStream ms = new MemoryStream();
            int header = Convert.ToInt32(HeaderId);
            var objStatus = db.FDS001.Where(o => o.HeaderId == header).Select(x => x.Status).FirstOrDefault();
            string QAAccept = clsImplementationEnum.FDMSStatus.QAAccept.GetStringValue();
            string RetQA = clsImplementationEnum.FDMSStatus.RetQA.GetStringValue();
            FileUploadController fuController = new FileUploadController();
            if (objStatus == QAAccept || objStatus == RetQA)
            {
                //objResponseMsg.Value = "FDS001/" + HeaderId;
                //clsUpload.GetFilesFromFolder("FDS001/" + HeaderId, ms);

                var _lst = fuController.GetAllDocumentsByTableNameTableId("FDS001", header);
                string strValue = "";
                foreach(var v in _lst)
                {
                    if (strValue == "")
                        strValue = v.MainDocumentPath;
                    else
                        strValue += "," + v.MainDocumentPath;
                }
                objResponseMsg.Value = strValue;
            }
            else
            {
                //objResponseMsg.Value = "FDS001/Documents/" + HeaderId;
                var _lst = fuController.GetAllDocumentsByTableNameTableId("FDS001//Documents//" + HeaderId, header);
                string strValue = "";
                foreach (var v in _lst)
                {
                    if (strValue == "")
                        strValue = v.MainDocumentPath;
                    else
                        strValue += "," + v.MainDocumentPath;
                }
                objResponseMsg.Value = strValue;
                //clsUpload.GetFilesFromFolder("FDS001/Documents/" + HeaderId, ms);
            }
            //ms.Seek(0, SeekOrigin.Begin);

            //return File(ms, "application/octet-stream", "FDMS-11.zip");
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        #region (Code commented by Nikita Vibhandik - 9/11/2017 added 3 columns [observation no. 13687])
        //[HttpPost]
        //public ActionResult LoadFDMSLines(JQueryDataTableParamModel param)
        //{
        //    try
        //    {
        //        var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
        //        var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
        //        int StartIndex = param.iDisplayStart + 1;
        //        int EndIndex = param.iDisplayStart + param.iDisplayLength;
        //        int headerid = Convert.ToInt32(param.Headerid);


        //        FDS002 objFDS002 = db.FDS002.FirstOrDefault(x => x.HeaderId == headerid);
        //        string strWhere = string.Empty;
        //        if (!string.IsNullOrWhiteSpace(param.sSearch))
        //        {
        //            strWhere = " (FDMSName like '%" + param.sSearch
        //                + "%' or FDMSDocumentName like '%" + param.sSearch
        //                + "%' or DocumentNumber like '%" + param.sSearch
        //                + "%' or Department +' - '+ i.t_desc like '%" + param.sSearch
        //                + "%' or ResponsiblePerson +' - '+ i.t_name like '%" + param.sSearch
        //                + "%' or Status like '%" + param.sSearch
        //                + "%' or TargetDate like '%" + param.sSearch
        //                + "%')";
        //        }
        //        else
        //        {
        //            strWhere = "1=1";
        //        }
        //        var lstResult = db.SP_FDMS_GET_LINES
        //                        (
        //                        StartIndex, EndIndex, "", headerid, strWhere
        //                        ).ToList();

        //        var data = (from uc in lstResult
        //                    select new[]
        //                    {
        //                        Convert.ToString(uc.LineId),
        //                        Convert.ToString(uc.FDMSName),
        //                        Convert.ToString(uc.FDMSDocumentName),
        //                        Convert.ToString(uc.DocumentNumber),
        //                        Convert.ToString(uc.Department),
        //                        Convert.ToString(uc.ResponsiblePerson),
        //                        Convert.ToString(Convert.ToDateTime(uc.TargetDate).ToString("dd/MM/yyyy")),
        //                        GetCommentsStyle(objFDS002.Status,uc.DelayComments , uc.ResponsiblePerson , uc.HeaderId.ToString()),
        //                        GetRemarkStyle(objFDS002.Status,uc.RemarkBySubmitter , uc.ResponsiblePerson  , uc.HeaderId.ToString()),
        //                        Convert.ToString(uc.LineId),
        //                        Manager.GetBigDocuments("FDS001/" + uc.HeaderId + "/"  + uc.LineId),
        //                        Convert.ToString(uc.LineId),
        //                        Convert.ToString(uc.LineId),
        //                        Convert.ToString(uc.Status),
        //                        CheckShowAttachment(uc.LineId).ToString(),
        //                        checkRadioButton(uc.LineId).ToString()

        //                   }).ToList();
        //        int flag1 = 0, flag2 = 0;
        //        foreach (var val in lstResult)
        //        {
        //            if (val.Status == "Submitted")
        //            {
        //                flag1 = 0;
        //            }
        //            else
        //            {
        //                flag1 = flag1 + 1;
        //            }
        //            if (val.Status == "Accepted")
        //            {
        //                flag2 = 0;
        //            }
        //            else
        //            {
        //                flag2 = flag1 + 1;
        //            }
        //        }

        //        return Json(new
        //        {
        //            sEcho = Convert.ToInt32(param.sEcho),
        //            iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
        //            iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
        //            aaData = data
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        return Json(new
        //        {
        //            sEcho = param.sEcho,
        //            iTotalRecords = "0",
        //            iTotalDisplayRecords = "0",
        //            aaData = ""
        //        }, JsonRequestBehavior.AllowGet);
        //    }

        //}
        #endregion
        [HttpPost]
        public string getbutton(int lineid, int headerid, string colname, string value, string linestatus, string headerstatus, string role)
        {

            var retString = "";
            if (role == "QAManager" || role == "QAEngineer" || role == "QCManager" || role == "QCEngineer")
            {

                retString = Helper.GenerateTextbox(lineid, colname, value, "UpdateLineDetails(this, " + lineid + ");", ((string.Equals(linestatus, clsImplementationEnum.LineStatusFDMS.Submitted.GetStringValue())) && (string.Equals(headerstatus, clsImplementationEnum.FDMSStatus.DocVeri.GetStringValue()))) ? false : true, "", "2000");
            }
            else
            {
                retString = Helper.GenerateTextbox(lineid, colname, value, "UpdateLineDetails(this, " + lineid + ");", true, "", "2000");
            }

            return retString;// "<input type='text' readonly= 'readonly' name='txtReturnRemark' class='form-control input-sm input-small input-inline' value='" + returnRemarks + "' />";
        }

        #region verification lines
        [HttpPost]
        public ActionResult LoadFDMSLines(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                int headerid = Convert.ToInt32(param.Headerid);

                FDS002 objFDS002 = db.FDS002.FirstOrDefault(x => x.HeaderId == headerid);
                string strWhere = string.Empty;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (FDMSName like '%" + param.sSearch
                        + "%' or FDMSDocumentName like '%" + param.sSearch
                        + "%' or DocumentNumber like '%" + param.sSearch
                        + "%' or Department +' - '+ i.t_desc like '%" + param.sSearch
                        + "%' or ResponsiblePerson +' - '+ i.t_name like '%" + param.sSearch
                        + "%' or Status like '%" + param.sSearch
                        + "%' or TargetDate like '%" + param.sSearch
                        + "%')";
                }
                else
                {
                    strWhere = "1=1";
                }
                bool isPopup = param.Flag; //submitted docs
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_FDMS_GET_LINES
                                (
                                StartIndex, EndIndex, strSortOrder, headerid, strWhere
                                ).ToList();
                int newRecordId = 0;
                var newRecord = new[] {
                                    Helper.GenerateHidden(newRecordId, "FDMSName",""),
                                    Helper.GenerateHidden(newRecordId, "HeaderId", headerid.ToString()),
                                    Helper.GenerateTextbox(newRecordId, "FDMSDocumentName" ,"", "", false, "", "50"),
                                    Helper.GenerateTextbox(newRecordId, "DocumentNumber","", "", false, "", "15"),
                                    Helper.HTMLAutoComplete(newRecordId,"ResponsiblePerson"),
                                    Helper.GenerateTextbox(newRecordId,"Department","","",true),
                                    Helper.GenerateTextbox(newRecordId, "TargetDate"),
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    clsImplementationEnum.LineStatusFDMS.Assigned.GetStringValue(),
                                    CheckShowAttachment(0).ToString(),
                                    checkRadioButton(0).ToString(),
                                    "0",
                                    "",
                                    "",//Helper.GenerateTextbox(newRecordId, "DocumentSubmissionDate"),
                                    "",
                                    "",
                                    Helper.GenerateTextbox(newRecordId, "MailRestartdate"),
                                    "No",
                                    Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveVerificationNewRecord();" ),
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.FDMSName),
                                Convert.ToString(uc.FDMSDocumentName),
                                Convert.ToString(uc.DocumentNumber),
                                //Convert.ToString(uc.Department),
                                isPopup ? uc.ResponsiblePerson:   Helper.HTMLAutoComplete(uc.LineId,"ResponsiblePerson",uc.ResponsiblePerson , "UpdateRPDetails(this, "+ uc.LineId +", true);",(param.Department == "QAEngineer" && uc.Status == clsImplementationEnum.LineStatusFDMS.Assigned.GetStringValue() && (param.MTStatus == clsImplementationEnum.FDMSStatus.SCNVer.GetStringValue() || param.MTStatus == clsImplementationEnum.FDMSStatus.RetSCN.GetStringValue() || param.MTStatus == clsImplementationEnum.FDMSStatus.DocAssign.GetStringValue() || param.MTStatus == clsImplementationEnum.FDMSStatus.DocVeri.GetStringValue() || param.MTStatus == clsImplementationEnum.FDMSStatus.RetDocVeri.GetStringValue())) ? false : true),
                                isPopup ? uc.Department:  Helper.GenerateTextbox(uc.LineId,"Department",uc.Department ,"",true),
                                //Convert.ToString(uc.ResponsiblePerson),
                                isPopup ? Convert.ToDateTime(uc.TargetDate).ToString("dd/MM/yyyy",CultureInfo.InvariantCulture) : Helper.GenerateTextbox(uc.LineId, "TargetDate",Convert.ToDateTime(uc.TargetDate).ToString("yyyy-MM-dd",CultureInfo.InvariantCulture) , "UpdateLineDetails(this, "+ uc.LineId +");",((param.MTStatus == clsImplementationEnum.FDMSStatus.SCNVer.GetStringValue() && param.Department == "QCManager" && uc.Status == clsImplementationEnum.LineStatusFDMS.Assigned.GetStringValue() ) || ((param.MTStatus == clsImplementationEnum.FDMSStatus.DocVeri.GetStringValue() ||param.MTStatus == clsImplementationEnum.FDMSStatus.RetDocVeri.GetStringValue() || param.MTStatus == clsImplementationEnum.FDMSStatus.RetSCN.GetStringValue() || param.MTStatus == clsImplementationEnum.FDMSStatus.SCNVer.GetStringValue()  ) && uc.Status == clsImplementationEnum.LineStatusFDMS.Assigned.GetStringValue()  && param.Department == "QAEngineer")) ? false : true),
                                isPopup ? uc.DelayComments: GetCommentsStyle(uc.Status,uc.DelayComments , uc.ResponsiblePerson , uc.HeaderId.ToString()),
                                isPopup ? uc.RemarkBySubmitter:  GetRemarkStyle(uc.Status,uc.RemarkBySubmitter , uc.ResponsiblePerson  , uc.HeaderId.ToString()),
                                Convert.ToString(uc.LineId),
                                Manager.GetBigDocuments("FDS001/" + uc.HeaderId + "/"  + uc.LineId),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.Status),
                                CheckShowAttachment(uc.LineId).ToString(),
                                checkRadioButton(uc.LineId).ToString(),
                                Convert.ToString(uc.LineId),
                                uc.DocumentAssignedDate == null || uc.DocumentAssignedDate.Value==DateTime.MinValue? "NA" :uc.DocumentAssignedDate.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture),
                                uc.DocumentSubmissionDate == null || uc.DocumentSubmissionDate.Value==DateTime.MinValue? "NA" :uc.DocumentSubmissionDate.Value.ToString("dd/MM/yyyy" ,CultureInfo.InvariantCulture),
                                uc.LastDocumentVerificationDatebyQA == null || uc.LastDocumentVerificationDatebyQA.Value==DateTime.MinValue? "NA" :uc.LastDocumentVerificationDatebyQA.Value.ToString("dd/MM/yyyy" ,CultureInfo.InvariantCulture),
                                isPopup ?  uc.RejectionComments:  getbutton(uc.LineId, uc.HeaderId,"RejectionComments",uc.RejectionComments,uc.Status, param.MTStatus, param.Department),
                                //uc.MailRestartdate==null || uc.MailRestartdate.Value==DateTime.MinValue?"":uc.MailRestartdate.Value.ToString("dd/MM/yyyy" ,CultureInfo.InvariantCulture),
                                isPopup ? Convert.ToDateTime(uc.MailRestartdate).ToString("dd/MM/yyyy",CultureInfo.InvariantCulture):  Helper.GenerateTextbox(uc.LineId, "MailRestartdate",uc.MailRestartdate==null || uc.MailRestartdate.Value==DateTime.MinValue ? "":Convert.ToDateTime(uc.MailRestartdate).ToString("yyyy-MM-dd",CultureInfo.InvariantCulture), "UpdateLineDetails(this, "+ uc.LineId +");",checkstatus(param.Department,uc.Status,param.MTStatus) == true? false : true),

                                Convert.ToString(uc.SubmittoPMG == true ? "Yes":"No"),
                                "<center>"+ HTMLActionString(uc.LineId, "Delete", "Delete Record", "fa fa-trash-o", "DeleteRecord(" + uc.LineId + ");",((param.Department=="QAManager" || param.Department == "QAEngineer" || param.Department == "QCManager" || param.Department == "QCEngineer" ) && string.Equals(uc.Status, clsImplementationEnum.LineStatusFDMS.Assigned.GetStringValue())?true:false))+""+"</center>",
                                }).ToList();
                int flag1 = 0, flag2 = 0;
                foreach (var val in lstResult)
                {
                    if (val.Status.ToLower() == clsImplementationEnum.LineStatusFDMS.Submitted.ToString().ToLower())
                    {
                        flag1 = 0;
                    }
                    else
                    {
                        flag1 = flag1 + 1;
                    }
                    if (val.Status == "Accepted")
                    {
                        flag2 = 0;
                    }
                    else
                    {
                        flag2 = flag1 + 1;
                    }
                }

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }
        #endregion

        #region functions for lines
        public bool checkstatus(string user, string linestatus, string headerstatus)
        {
            bool flag = false;
            if (user == "QAEngineer" && linestatus.ToLower() == clsImplementationEnum.LineStatusFDMS.Assigned.GetStringValue().ToLower()
                && (
                    headerstatus.ToLower() == clsImplementationEnum.FDMSStatus.DocAssign.GetStringValue().ToLower() ||
                    headerstatus.ToLower() == clsImplementationEnum.FDMSStatus.RetDocVeri.GetStringValue().ToLower() ||
                    headerstatus.ToLower() == clsImplementationEnum.FDMSStatus.SCNVer.GetStringValue().ToLower() ||
                    headerstatus.ToLower() == clsImplementationEnum.FDMSStatus.RetSCN.GetStringValue().ToLower() ||
                    headerstatus.ToLower() == clsImplementationEnum.FDMSStatus.DocVeri.GetStringValue().ToLower() ||
                    headerstatus.ToLower() == clsImplementationEnum.FDMSStatus.RetDocVeri.GetStringValue().ToLower() ||
                    headerstatus.ToLower() == clsImplementationEnum.FDMSStatus.QAAccept.GetStringValue().ToLower() ||
                    headerstatus.ToLower() == clsImplementationEnum.FDMSStatus.RetQA.GetStringValue().ToLower()
                ))
            {
                flag = true;
            }
            return flag;
        }

        [NonAction]
        private int checkRadioButton(int? LineId = 0)
        {
            if (!db.FDS002.Any(q => q.LineId == LineId))
                return 0;
            var objFDS002 = db.FDS002.FirstOrDefault(q => q.LineId == LineId);
            var user = objClsLoginInfo.UserName.Trim();
            var allowedViewUsers = new List<string>();
            var allowedEditUsers = new List<string>();


            allowedViewUsers.Add(objFDS002.ResponsiblePerson.Trim());
            allowedViewUsers.Add(objFDS002.FDS001.DesignEngineer.Trim());
            allowedViewUsers.Add(objFDS002.FDS001.PMGManager.Trim());

            allowedEditUsers.Add(objFDS002.FDS001.QAEngineer.Trim());
            allowedEditUsers.Add(objFDS002.FDS001.QAManager.Trim());
            allowedEditUsers.Add(objFDS002.FDS001.QCManager.Trim());

            if (allowedEditUsers.ToList().Contains(objFDS002.ResponsiblePerson.Trim()) && (objFDS002.Status.ToLower().Trim() == clsImplementationEnum.LineStatusFDMS.Submitted.GetStringValue().ToLower()))
                return 1;
            else if (allowedEditUsers.ToList().Contains(user) && (objFDS002.Status == clsImplementationEnum.LineStatusFDMS.Submitted.GetStringValue()))
                return 1;
            else if (objFDS002.ResponsiblePerson.Trim().Equals(user))
                return 0;
            else
                return 0;

        }

        private int CheckShowAttachment(int? LineId = 0)
        {
            if (!db.FDS002.Any(q => q.LineId == LineId))
                return 0;
            var objFDS002 = db.FDS002.FirstOrDefault(q => q.LineId == LineId);
            var user = objClsLoginInfo.UserName.Trim();
            var allowedViewUsers = new List<string>();
            var allowedEditUsers = new List<string>();

            allowedViewUsers.Add(objFDS002.FDS001.QAEngineer.Trim());
            allowedViewUsers.Add(objFDS002.FDS001.QAManager.Trim());
            allowedViewUsers.Add(objFDS002.FDS001.QCManager.Trim());
            allowedEditUsers.Add(objFDS002.ResponsiblePerson.Trim());
            //if (objFDS002.Status.ToLower().Equals("assigned"))

            //else
            //    allowedViewUsers.Add(objFDS002.ResponsiblePerson.Trim());

            if (allowedEditUsers.ToList().Contains(user))
                return 2;
            if (allowedViewUsers.ToList().Contains(user))
                return 1;
            return 0;
        }
        public string GetRemarkStyle(string status, string returnRemarks, string RM, string Hstatus)
        {
            string Role = "";
            int hid = Convert.ToInt32(Hstatus);
            string login = objClsLoginInfo.UserName;
            var Status = (from a in db.FDS001
                          where a.HeaderId == hid
                          select a.Status).FirstOrDefault();

            var Exists5 = db.FDS002.Any(x => x.ResponsiblePerson == login);
            if (Exists5 == true)
            {
                Role = "ResponsiblePerson";
            }
            else
            {
                Role = "";
            }
            RM = RM.Split('-')[0].ToString().Trim();
            if (Role.Equals("ResponsiblePerson") && RM.ToLower().Trim().Equals(login.ToLower().Trim()) && status == clsImplementationEnum.LineStatusFDMS.Assigned.GetStringValue() && (Status.Equals(clsImplementationEnum.FDMSStatus.DocVeri.GetStringValue(), StringComparison.OrdinalIgnoreCase) || Status.Equals(clsImplementationEnum.FDMSStatus.SCNVer.GetStringValue(), StringComparison.OrdinalIgnoreCase) || Status.Equals(clsImplementationEnum.FDMSStatus.RetDocVeri.GetStringValue(), StringComparison.OrdinalIgnoreCase) || Status.Equals(clsImplementationEnum.FDMSStatus.RetSCN.GetStringValue(), StringComparison.OrdinalIgnoreCase)))
            {
                return "<input type='text' name='txtReturnRemark' class='form-control'  maxlength='200'  value='" + returnRemarks + "' />";
            }
            else
            {
                return "<input type='text' readonly= 'readonly' name='txtReturnRemark' class='form-control' value='" + returnRemarks + "' />";
            }
        }
        public string GetCommentsStyle(string status, string delayComments, string RM, string Hstatus)
        {
            string Role = "";
            int hid = Convert.ToInt32(Hstatus);
            string login = objClsLoginInfo.UserName.Trim();
            // login = "91348309";
            var Status = (from a in db.FDS001
                          where a.HeaderId == hid
                          select a.Status).FirstOrDefault();
            var Exists5 = db.FDS002.Any(x => x.ResponsiblePerson == login);
            if (Exists5 == true)
            {
                Role = "ResponsiblePerson";
            }
            else
            {
                Role = "";
            }
            if (Role == "ResponsiblePerson" && RM.Split('-')[0].ToString().Trim() == login && status == clsImplementationEnum.LineStatusFDMS.Assigned.GetStringValue() && (Status.Equals(clsImplementationEnum.FDMSStatus.DocVeri.GetStringValue(), StringComparison.OrdinalIgnoreCase) || Status.Equals(clsImplementationEnum.FDMSStatus.RetDocVeri.GetStringValue(), StringComparison.OrdinalIgnoreCase) || Status.Equals(clsImplementationEnum.FDMSStatus.SCNVer.GetStringValue(), StringComparison.OrdinalIgnoreCase) || Status.Equals(clsImplementationEnum.FDMSStatus.RetSCN.GetStringValue(), StringComparison.OrdinalIgnoreCase)))
            {
                return "<input type='text' name='txtDelay' class='form-control' maxlength='100' value='" + delayComments + "' />";
            }
            else
            {
                return "<input type='text' readonly= 'readonly' name='txtDelay' class='form-control' value='" + delayComments + "' />";
            }
        }

        //submit new line
        [HttpPost]
        public ActionResult submitLines(string lineid)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int Line = Convert.ToInt32(lineid);
                FDS002 objFDS002 = db.FDS002.Where(x => x.LineId == Line).FirstOrDefault();
                if (objFDS002 != null)
                {
                    string assigned = clsImplementationEnum.LineStatusFDMS.Assigned.GetStringValue();
                    string Rejected = clsImplementationEnum.LineStatusFDMS.Rejected.GetStringValue();
                    if (objFDS002.Status.ToString() == assigned || objFDS002.Status.ToString() == Rejected)
                    {
                        objFDS002.Status = clsImplementationEnum.LineStatusFDMS.Submitted.GetStringValue();
                        objFDS002.DocumentSubmissionDate = DateTime.Now;
                        objFDS002.EditedBy = objClsLoginInfo.UserName;
                        objFDS002.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Key = true;

                        var headerid = (from a in db.FDS002
                                        where a.LineId == Line
                                        select a.HeaderId).FirstOrDefault();
                        FDS001 objFDS001 = db.FDS001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                        if (objFDS001 != null)
                        {
                            objFDS001.LineSubmitDate = DateTime.Now.AddDays(3);
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "You can not Change Status..!";
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Approve/reject for lines

        //approve button visibility
        [HttpPost]
        public ActionResult ApproveRejectVisibility(string HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int hd = Convert.ToInt32(HeaderId);
                if (!db.FDS001.Any(q => q.HeaderId == hd))
                    objResponseMsg.Value = "0";
                var objFDS001 = db.FDS001.FirstOrDefault(q => q.HeaderId == hd);
                var user = objClsLoginInfo.UserName.Trim();
                var allowedEditUsers = new List<string>();

                allowedEditUsers.Add(objFDS001.QAEngineer.Trim());
                allowedEditUsers.Add(objFDS001.QAManager.Trim());
                allowedEditUsers.Add(objFDS001.QCManager.Trim());

                if (allowedEditUsers.ToList().Contains(user))
                    objResponseMsg.Value = "1";
                else
                    objResponseMsg.Value = "0";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult IsQAQCSame(string HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int hd = Convert.ToInt32(HeaderId);
                if (!db.FDS001.Any(q => q.HeaderId == hd))
                    objResponseMsg.Value = "0";
                var objFDS001 = db.FDS001.FirstOrDefault(q => q.HeaderId == hd);
                var user = objClsLoginInfo.UserName.Trim();
                var allowedEditUsers = new List<string>();

                allowedEditUsers.Add(objFDS001.QAEngineer.Trim());
                allowedEditUsers.Add(objFDS001.QCManager.Trim());

                if (allowedEditUsers.All(x => x.ToString() == user))
                    objResponseMsg.Value = "2";
                else
                    objResponseMsg.Value = "0";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateReturnRemarks(int LineID, string changeText)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var Status = (from a in db.FDS002
                              where a.LineId == LineID
                              select a.Status).FirstOrDefault();
                if (Status != clsImplementationEnum.LineStatusFDMS.Assigned.GetStringValue())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "You can not modify Remarks";
                }
                else
                {
                    FDS002 objFDS002 = db.FDS002.Where(x => x.LineId == LineID).FirstOrDefault();
                    if (objFDS002 != null)
                    {
                        if (!string.IsNullOrWhiteSpace(changeText))
                        {
                            objFDS002.RemarkBySubmitter = changeText;
                            //objFDS002.Status = "Submitted";
                        }
                        else
                        {
                            objFDS002.RemarkBySubmitter = null;
                            // objFDS002.Status = "Assigned";
                        }

                        objFDS002.EditedBy = objClsLoginInfo.UserName;
                        objFDS002.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Line remarks successfully updated.";
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Line not available";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        //approve selected lines
        public ActionResult ApproveSelectedLines(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
                for (int i = 0; i < arrayHeaderIds.Length; i++)
                {
                    int lineid = Convert.ToInt32(arrayHeaderIds[i]);
                    FDS002 objFDS002 = db.FDS002.Where(x => x.LineId == lineid).FirstOrDefault();
                    objFDS002.Status = clsImplementationEnum.LineStatusFDMS.Accepted.GetStringValue();
                    objFDS002.EditedBy = objClsLoginInfo.UserName;
                    objFDS002.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.FDMS.LApprove.ToString();

                    #region Send Mail
                    string emailTo = string.Empty;
                    string ccTo = string.Empty;
                    string assignees = string.Empty;
                    var lstAssignees = db.FDS002.Where(x => x.HeaderId == objFDS002.HeaderId && x.ResponsiblePerson != null).Select(o => o.ResponsiblePerson).ToList();
                    if (lstAssignees.Count > 0 || lstAssignees != null)
                    {
                        for (var j = 0; j < lstAssignees.Count; j++)
                        {
                            lstAssignees[j] = Manager.GetMailIdFromPsNo(lstAssignees[j]);
                        }
                        assignees = String.Join(";", lstAssignees);
                        //mail to QC2 role
                        emailTo = assignees;
                    }
                    else
                    {
                        emailTo = objFDS002.ResponsiblePerson;
                    }
                    string FDMSNo = objFDS002.FDMSName;
                    string FDMSDoc = objFDS002.DocumentNumber;
                    string AssigneeName = objFDS002.ResponsiblePerson;

                    string TargetDate = Convert.ToDateTime(objFDS002.TargetDate).ToString("dd/MM/yyyy");
                    string[] lstemailTo = new string[0];
                    bool sendToMultiple = false;
                    if (emailTo.ToLower().Contains(';'))
                    {
                        lstemailTo = emailTo.Split(';');
                        sendToMultiple = true;
                    }

                    if (!string.IsNullOrWhiteSpace(emailTo))
                    {
                        if (sendToMultiple)
                            foreach (var individualmail in lstemailTo)
                            {
                                Hashtable _ht = new Hashtable();
                                EmailSend _objEmail = new EmailSend();
                                _ht["[CC]"] = Manager.GetMailIdFromPsNo(objFDS002.FDS001.QAEngineer);

                                _ht["[FDMSLink]"] = WebsiteURL + "/FDMS/FDMS/CreateFDMs?Id=" + objFDS002.HeaderId;
                                _ht["[FDMSNo]"] = FDMSNo;
                                _ht["[ToPerson]"] = Manager.GetUserNameFromPsNo(individualmail);
                                _ht["[AssigneeName]"] = objFDS002.ResponsiblePerson;
                                _ht["[FDMSNo]"] = FDMSNo;
                                _ht["[TargetDate]"] = objFDS002.TargetDate;
                                MAIL001 objTemplateMaster = new MAIL001();
                                objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.FDMS.LineApprove).SingleOrDefault();
                                _objEmail.MailToAdd = Manager.GetMailIdFromPsNo(individualmail);
                                _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                            }
                        else
                        {
                            Hashtable _ht = new Hashtable();
                            EmailSend _objEmail = new EmailSend();
                            _ht["[CC]"] = Manager.GetMailIdFromPsNo(objFDS002.FDS001.QAEngineer);

                            _ht["[FDMSLink]"] = WebsiteURL + "/FDMS/FDMS/CreateFDMs?Id=" + objFDS002.HeaderId;
                            _ht["[FDMSNo]"] = FDMSNo;
                            _ht["[ToPerson]"] = Manager.GetUserNameFromPsNo(emailTo);
                            _ht["[AssigneeName]"] = objFDS002.ResponsiblePerson;
                            _ht["[FDMSNo]"] = FDMSNo;
                            _ht["[TargetDate]"] = objFDS002.TargetDate;
                            MAIL001 objTemplateMaster = new MAIL001();
                            objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.FDMS.LineApprove).SingleOrDefault();
                            _objEmail.MailToAdd = Manager.GetMailIdFromPsNo(emailTo);
                            _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        //reject selected lines
        public ActionResult RejectSelectedLines(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
                for (int i = 0; i < arrayHeaderIds.Length; i++)
                {
                    int lineid = Convert.ToInt32(arrayHeaderIds[i]);
                    FDS002 objFDS002 = db.FDS002.Where(x => x.LineId == lineid).FirstOrDefault();
                    if (!string.IsNullOrWhiteSpace(objFDS002.RejectionComments))
                    {
                        objFDS002.Status = clsImplementationEnum.LineStatusFDMS.Rejected.GetStringValue();
                        objFDS002.EditedBy = objClsLoginInfo.UserName;
                        objFDS002.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.FDMS.LReject.ToString();

                        #region Send Mail
                        string emailTo = string.Empty;
                        string ccTo = string.Empty;
                        string assignees = string.Empty;
                        var lstAssignees = db.FDS002.Where(x => x.LineId == objFDS002.LineId && x.ResponsiblePerson != null).Select(o => o.ResponsiblePerson).ToList();
                        if (lstAssignees.Count > 0 || lstAssignees != null)
                        {
                            for (var j = 0; j < lstAssignees.Count; j++)
                            {
                                lstAssignees[j] = Manager.GetMailIdFromPsNo(lstAssignees[j]);
                            }
                            assignees = String.Join(";", lstAssignees);
                            //mail to QC2 role
                            emailTo = assignees;
                        }
                        else
                        {
                            emailTo = objFDS002.ResponsiblePerson;
                        }
                        string FDMSNo = objFDS002.FDMSName;
                        string FDMSDoc = objFDS002.DocumentNumber;
                        string AssigneeName = objFDS002.ResponsiblePerson;

                        string TargetDate = Convert.ToDateTime(objFDS002.TargetDate).ToString("dd/MM/yyyy");
                        string[] lstemailTo = new string[0];
                        bool sendToMultiple = false;
                        if (emailTo.ToLower().Contains(';'))
                        {
                            lstemailTo = emailTo.Split(';');
                            sendToMultiple = true;
                        }

                        if (!string.IsNullOrWhiteSpace(emailTo))
                        {
                            if (sendToMultiple)
                                foreach (var individualmail in lstemailTo)
                                {
                                    Hashtable _ht = new Hashtable();
                                    EmailSend _objEmail = new EmailSend();
                                    _ht["[CC]"] = Manager.GetMailIdFromPsNo(objFDS002.FDS001.QAEngineer);
                                    _ht["[FDMSLink]"] = WebsiteURL + "/FDMS/FDMS/CreateFDMs?Id=" + objFDS002.HeaderId;
                                    _ht["[FDMSNo]"] = FDMSNo;
                                    _ht["[ToPerson]"] = Manager.GetUserNameFromPsNo(individualmail);
                                    _ht["[AssigneeName]"] = objFDS002.ResponsiblePerson;
                                    _ht["[FDMSNo]"] = FDMSNo;
                                    _ht["[TargetDate]"] = objFDS002.TargetDate;
                                    MAIL001 objTemplateMaster = new MAIL001();
                                    objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.FDMS.LineReject).SingleOrDefault();
                                    _objEmail.MailToAdd = Manager.GetMailIdFromPsNo(individualmail);
                                    _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                                }
                            else
                            {
                                Hashtable _ht = new Hashtable();
                                EmailSend _objEmail = new EmailSend();
                                _ht["[CC]"] = Manager.GetMailIdFromPsNo(objFDS002.FDS001.QAEngineer);
                                _ht["[FDMSLink]"] = WebsiteURL + "/FDMS/FDMS/CreateFDMs?Id=" + objFDS002.HeaderId;
                                _ht["[FDMSNo]"] = FDMSNo;
                                _ht["[ToPerson]"] = Manager.GetUserNameFromPsNo(emailTo);
                                _ht["[AssigneeName]"] = objFDS002.ResponsiblePerson;
                                _ht["[FDMSNo]"] = FDMSNo;
                                _ht["[TargetDate]"] = objFDS002.TargetDate;
                                MAIL001 objTemplateMaster = new MAIL001();
                                objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.FDMS.LineReject).SingleOrDefault();
                                _objEmail.MailToAdd = Manager.GetMailIdFromPsNo(emailTo);
                                _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Rejection Remark is required";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion


        //verifyApproverQA

        #region to update SCN date
        [HttpPost]
        public ActionResult SCNUpdate(FDS001 fds001, string SCNDate)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                if (fds001.HeaderId > 0)
                {
                    FDS001 objFDS001 = db.FDS001.Where(x => x.HeaderId == fds001.HeaderId).FirstOrDefault();
                    if (objFDS001 != null)
                    {
                        objFDS001.SCNDate = Convert.ToDateTime(DateTime.ParseExact(SCNDate.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture));
                        //objFDS001.SCNDate = Convert.ToDateTime(SCNDate.ToString());
                        objFDS001.EditedBy = objClsLoginInfo.UserName;
                        objFDS001.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.FDMS.Update.ToString();
                        objResponseMsg.status = objFDS001.Status;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please Provide SCN Date";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }
        #endregion

        [HttpPost]
        public ActionResult LoadFDMSLinesRM(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                int headerid = Convert.ToInt32(param.Headerid);
                FDS002 objFDS002 = db.FDS002.FirstOrDefault(x => x.HeaderId == headerid);
                string strWhere = "ResponsiblePerson = " + objClsLoginInfo.UserName;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (FDMSName like '%" + param.sSearch
                        + "%' or FDMSDocumentName like '%" + param.sSearch
                        + "%' or DocumentNumber like '%" + param.sSearch
                        + "%' or Department +' - '+ i.t_desc like '%" + param.sSearch
                        + "%' or ResponsiblePerson +' - '+ i.t_name like '%" + param.sSearch
                        + "%' or Status like '%" + param.sSearch
                        + "%' or TargetDate like '%" + param.sSearch
                        + "%')";
                }
                else
                {
                    strWhere += " and 1=1";
                }
                var lstResult = db.SP_FDMS_GET_LINES
                                (
                                StartIndex, EndIndex, "", headerid, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                            {
                          Convert.ToString(uc.LineId),
                           Convert.ToString(uc.FDMSName),
                           Convert.ToString(uc.FDMSDocumentName),
                           Convert.ToString(uc.DocumentNumber),
                           Convert.ToString(uc.Department),
                           Convert.ToString(uc.ResponsiblePerson),
                           Convert.ToString(Convert.ToDateTime(DateTime.ParseExact(uc.TargetDate.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture))),
                           GetCommentsStyle(uc.Status,uc.DelayComments , uc.ResponsiblePerson , uc.HeaderId.ToString()),
                           GetRemarkStyle(uc.Status,uc.RemarkBySubmitter , uc.ResponsiblePerson  , uc.HeaderId.ToString()),
                           Convert.ToString(uc.LineId),
                           Manager.GetBigDocuments("FDS001/" + uc.HeaderId + "/"  + uc.LineId),
                           Convert.ToString(uc.LineId),
                            Convert.ToString(uc.LineId),
                           Convert.ToString(uc.Status),
                           (CheckShowAttachment(uc.LineId).ToString()),
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult verifyApproverRM(string project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string psnum = objClsLoginInfo.UserName;
                bool QAM = db.FDS001.Where(i => i.QAManager == psnum && i.Project == project).Any();
                var headerid = (from a in db.FDS001
                                where a.Project == project
                                select a.HeaderId).FirstOrDefault();
                bool RM = db.FDS002.Where(i => i.ResponsiblePerson == psnum && i.HeaderId == headerid).Any();
                if (QAM == true && RM == true)
                {
                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Key = false;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult verifyApproverQA(string project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string psnum = objClsLoginInfo.UserName;
                bool QAM = db.FDS001.Where(i => i.QAEngineer == psnum && i.Project == project).Any();
                var headerid = (from a in db.FDS001
                                where a.Project == project
                                select a.HeaderId).FirstOrDefault();
                bool RM = db.FDS002.Where(i => i.ResponsiblePerson == psnum && i.HeaderId == headerid).Any();
                if (QAM == true && RM == true)
                {
                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Key = false;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GETPartial(string lineId)
        {
            int LIne = Convert.ToInt32(lineId);
            var HeaderId = (from a in db.FDS002
                            where a.LineId == LIne
                            select a.HeaderId).FirstOrDefault();
            //ViewBag.PreFill = Manager.getDocuments("FDS001/" + HeaderId + "/" + lineId);
            return PartialView("_AddUploadLines");
        }

        [HttpPost]
        public ActionResult retrackLine(string lineid)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int LIne = Convert.ToInt32(lineid);
                var Status = (from a in db.FDS002
                              where a.LineId == LIne
                              select a.Status).FirstOrDefault();
                if (Status == clsImplementationEnum.LineStatusFDMS.Submitted.GetStringValue() || Status == clsImplementationEnum.LineStatusFDMS.Rejected.GetStringValue())
                {
                    FDS002 objFDS002 = db.FDS002.Where(x => x.LineId == LIne).FirstOrDefault();
                    objFDS002.Status = clsImplementationEnum.LineStatusFDMS.Assigned.GetStringValue();
                    objFDS002.DocumentSubmissionDate = null;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "You can not Change Status..!";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UploadatLines(string lineid, bool hasAttachments, Dictionary<string, string> Attach)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int LIne = Convert.ToInt32(lineid);
                var Status = (from a in db.FDS002
                              where a.LineId == LIne
                              select a.Status).FirstOrDefault();
                var HeaderId = (from a in db.FDS002
                                where a.LineId == LIne
                                select a.HeaderId).FirstOrDefault();
                if (Status != clsImplementationEnum.LineStatusFDMS.Assigned.GetStringValue())
                    objResponseMsg.Key = false;
                else
                {
                    var folderPath = "FDS001/" + HeaderId + "/" + lineid;
                    Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //public ActionResult ViewHeader(string HeaderId)
        //{
        //    clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
        //    try
        //    {
        //        if (string.IsNullOrWhiteSpace(HeaderId))
        //            throw new Exception("Invalid Request");
        //        int headerId = Int32.Parse(HeaderId);
        //        FDS002 objFDS002 = db.FDS002.FirstOrDefault(x => x.LineId == headerId);
        //        if (objFDS002 != null)
        //        {

        //        }
        //        else
        //        {
        //            objResponseMsg.Key = false;
        //            objResponseMsg.Value = "Document Not Found";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        objResponseMsg.Key = false;
        //        objResponseMsg.Value = ex.ToString();
        //    }
        //    return Json(objResponseMsg);
        //}

        #region import exoprt FDMS lines excel
        //for uploading the FMDS lines
        [HttpPost]
        public ActionResult ReadExcel(HttpPostedFileBase upload, int HeaderId = 0)
        {
            if (upload == null) return RedirectToAction("CreateFDMS", new { Id = HeaderId, error = "Please select a file first!" });
            if (Path.GetExtension(upload.FileName).ToLower() == ".xlsx" || Path.GetExtension(upload.FileName).ToLower() == ".xls" || Path.GetExtension(upload.FileName).ToLower() == ".xlsm")
            {
                ExcelPackage package = new ExcelPackage(upload.InputStream);
                DataTable dt = ToDataTable(package);
                var header = db.FDS001.Where(q => q.HeaderId == HeaderId).FirstOrDefault();
                return validateAndSave(dt, header.FDMSName, HeaderId);
            }
            else
            {
                return RedirectToAction("CreateFDMS", new { Id = HeaderId });
            }

        }

        public DataTable ToDataTable(ExcelPackage package)
        {
            ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();
            DataTable dt = new DataTable();
            foreach (var firstRowCell in excelWorksheet.Cells[1, 1, 1, excelWorksheet.Dimension.End.Column])
            {
                dt.Columns.Add(firstRowCell.Text);
            }
            for (var rowNumber = 2; rowNumber <= excelWorksheet.Dimension.End.Row; rowNumber++)
            {
                var row = excelWorksheet.Cells[rowNumber, 1, rowNumber, excelWorksheet.Dimension.End.Column];
                var newRow = dt.NewRow();
                foreach (var cell in row)
                {
                    newRow[cell.Start.Column - 1] = cell.Text;
                }
                dt.Rows.Add(newRow);
            }
            return dt;
        }

        //validate mandatory fields and date from excel
        public ActionResult validateAndSave(DataTable dt, string fdmsName, int headerid)
        {

            bool hasError = false;
            string[] formats = { "dd/MM/yyyy", "dd/MMM/yyyy" };
            DateTime dateValue;
            if (!dt.Columns.Contains("Error"))
            {
                dt.Columns.Add("Error", typeof(string));
            }
            foreach (DataRow item in dt.Rows)
            {
                string psno = item.Field<string>(2).Split('-')[0];
                List<string> errors = new List<string>();

                string doc = item.Field<string>(0);
                string docnumb = item.Field<string>(1);

                if (string.IsNullOrWhiteSpace(doc) || string.IsNullOrWhiteSpace(docnumb))
                {
                    errors.Add("Mandatory fields are required"); hasError = true;
                }
                if (!string.IsNullOrWhiteSpace(doc) && doc.Length > 50)
                {
                    errors.Add("Document name have maximum limit of 50 characters"); hasError = true;
                }
                if (!string.IsNullOrWhiteSpace(docnumb) && docnumb.Length > 15)
                {
                    errors.Add("Document number have maximum limit of 15 characters"); hasError = true;
                }
                //if (string.IsNullOrWhiteSpace(docnumb))
                //{
                //    errors.Add("Document Number is mandatory"); hasError = true;
                //}

                //string depc = item.Field<string>(2).Split('-')[0];
                //if (!(IsAllDigits(depc) && db.COM003.Any(x => x.t_depc == depc)))
                //{
                //    errors.Add("Invalid Department name"); hasError = true;

                //}
                //else
                //{ hasDept = true; }
                ////else
                //{
                if (!(IsAllDigits(psno) && db.COM003.Any(x => x.t_psno == psno && x.t_actv == 1)))
                {
                    errors.Add("Invalid Psno"); hasError = true;
                }
                //if (!string.IsNullOrEmpty(depc) && hasDept)
                //{
                //    if (!(IsAllDigits(psno) && db.COM003.Any(x => x.t_psno == psno && x.t_actv == 1)))
                //    {
                //        errors.Add("Invalid Psno"); hasError = true;
                //    }
                //    else if (!(IsAllDigits(psno) && db.COM003.Any(x => x.t_psno == psno && x.t_depc == depc && x.t_actv == 1)))
                //    { errors.Add("Person does not belong to selected Department"); hasError = true; }

                //    //if (!(IsAllDigits(psno) && db.COM003.Any(x => x.t_psno == psno && x.t_depc == depc && x.t_actv == 1)))
                //    //{ errors.Add("Invalid Psno"); hasError = true; }
                //}
                //else
                //{
                //    { errors.Add("Person does not belong to selected Department"); hasError = true; }
                //}
                //}
                string date = item.Field<string>(3).Replace("-", "/");

                if (!DateTime.TryParseExact(date,
                     formats, null,
                       DateTimeStyles.None,
                       out dateValue))
                { errors.Add("Invalid Date value"); hasError = true; }

                ////DateTime.ParseExact(date, @"d/M/yyyy", CultureInfo.InvariantCulture);
                //if (!DateTime.TryParse(date, out dateValue))
                //{ errors.Add("Invalid Date value"); hasError = true; }

                if (!CheckTargetDate(date, true))
                { errors.Add("Target Date value must be greater than or equals to todays date"); hasError = true; }

                if (errors.Count > 0)
                {
                    item.SetField<string>(4, string.Join(",", errors.ToArray()));
                }

            }
            if (!hasError)
            {
                ViewBag.getError = "";
                foreach (DataRow item in dt.Rows)
                {
                    string tDate = item.Field<string>(3).Replace("-", "/");
                    db.FDS002.Add(new FDS002()
                    {
                        HeaderId = headerid,
                        FDMSName = fdmsName,
                        FDMSDocumentName = item.Field<string>(0),
                        DocumentNumber = item.Field<string>(1),
                        ResponsiblePerson = item.Field<string>(2).Split('-')[0],
                        Department = Manager.GetDepartmentByPsno(item.Field<string>(2).Split('-')[0]).Split('-')[0],
                        TargetDate = DateTime.ParseExact(tDate, @"d/M/yyyy", CultureInfo.InvariantCulture),
                        //DocumentSubmissionDate = DateTime.Parse(item.Field<string>(5)),
                        //MailRestartdate = DateTime.Parse(item.Field<string>(5)),
                        Status = clsImplementationEnum.LineStatusFDMS.Assigned.GetStringValue(),
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now,
                        DocumentAssignedDate = DateTime.Now
                    }
                   );
                    db.SaveChanges();
                }
                return RedirectToAction("CreateFDMS", new { Id = headerid });
            }
            else
            {

                return ExportToExcel(dt);
            }
        }

        //insert column wise data of excel into database
        public ActionResult ExportToExcel(DataTable dt)
        {
            try
            {
                MemoryStream ms = new MemoryStream();
                using (FileStream fs = System.IO.File.OpenRead(Server.MapPath("~/Areas/FDMS/Views/FDMS/ErrorUploadTemplate.xlsm")))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();
                        int i = 2;
                        foreach (DataRow item in dt.Rows)
                        {
                            excelWorksheet.Cells[i, 1].Value = item.Field<string>(0);
                            excelWorksheet.Cells[i, 2].Value = item.Field<string>(1);
                            excelWorksheet.Cells[i, 3].Value = item.Field<string>(2);
                            excelWorksheet.Cells[i, 4].Value = item.Field<string>(3);
                            excelWorksheet.Cells[i, 5].Value = item.Field<string>(4);
                            //excelWorksheet.Cells[i, 6].Value = item.Field<string>(5);
                            //excelWorksheet.Cells[i, 7].Value = item.Field<string>(6);
                            //excelWorksheet.Cells[i, 6].Value = item.Field<string>(7);
                            i++;
                        }
                        excelPackage.SaveAs(ms);
                    }
                }

                ms.Position = 0;
                //ViewBag.suc = "false";
                //return File(ms, "Not");


                return new FileStreamResult(ms, "application/xlsm")
                {
                    FileDownloadName = "ErrorList.xlsm"
                };
            }
            catch (Exception)
            {
                throw;
            }
        }

        //FDMS lines tamplate
        public FileResult DownloadSample()
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(Server.MapPath("~/Areas/FDMS/Views/FDMS/LatestUploadTemplate.xlsm"));
            string fileName = " UploadTemplate.xlsm";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        //FDMS lines tamplate
        public FileStreamResult SampleExcel()
        {
            try
            {
                MemoryStream ms = new MemoryStream();
                using (FileStream fs = System.IO.File.OpenRead(Server.MapPath("~/Areas/FDMS/Views/FDMS/LatestUploadTemplate.xlsm")))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();
                        excelPackage.SaveAs(ms);
                    }
                }

                ms.Position = 0;

                return new FileStreamResult(ms, "application/xlsm")
                {
                    FileDownloadName = "UploadTemplate.xlsm"
                };
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion


        #region Common function
        #region not in use
        [HttpPost]
        public ActionResult UploadFDR(string headerid, bool hasAttachments, Dictionary<string, string> Attach)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (hasAttachments == true)
                {
                    var folderPath = "FDS001/" + headerid;
                    Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please Attach Documents First";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UploadFDR1(string headerid, bool hasAttachments, Dictionary<string, string> Attach)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (hasAttachments == true)
                {
                    var folderPath = "FDS001/" + headerid + "/Cess";
                    Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please Attach Documents First";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UploadFDR2(string headerid, bool hasAttachments, Dictionary<string, string> Attach)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (hasAttachments == true)
                {
                    var folderPath = "FDS001/" + headerid + "/Ret";
                    Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please Attach Documents First";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        [HttpPost]
        public ActionResult CheckDateformat(string headerid)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int hd = Convert.ToInt32(headerid);
                var LineSubmitDate = (from a in db.FDS001
                                      where a.HeaderId == hd
                                      select a.LineSubmitDate).FirstOrDefault();
                if (LineSubmitDate.HasValue && (LineSubmitDate.Value < DateTime.Now.Date))
                { objResponseMsg.Key = true; }
                else
                { objResponseMsg.Key = false; }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CheckDateformat2(string headerid)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {

                int hd = Convert.ToInt32(headerid);
                //FDS002 f1 = db.FDS002.Where(x => x.LineId == hd).FirstOrDefault();
                var TargetDate = (from a in db.FDS002
                                  where a.LineId == hd
                                  select a.TargetDate).FirstOrDefault();
                if (TargetDate.HasValue && TargetDate.Value < DateTime.Now.Date)
                    objResponseMsg.Key = true;

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult verifyPSNum(string psnum)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                psnum = psnum.Split('-')[0].ToString().Trim();
                var Exists = db.COM003.Any(x => x.t_psno == psnum && x.t_actv == 1);
                if (Exists == false)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Invalid Employee..! Please Try Again..!";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult verifyPSNum1(string psnum, string dept)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                dept = dept.Split('-')[0].ToString().Trim();
                psnum = psnum.Split('-')[0].ToString().Trim();
                var Exists = db.COM003.Any(x => x.t_psno == psnum && x.t_depc == dept && x.t_actv == 1);
                if (Exists == false)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Invalid Employee..! Please Try Again..!";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetApproverResult(string Role, string term)
        {
            List<string> lstApprovers = GetList(Role, objClsLoginInfo.Location, objClsLoginInfo.UserName, term).ToList();
            return Json(lstApprovers, JsonRequestBehavior.AllowGet);
        }
        //not in use(added by nikita)
        public List<ApproverModel> GetMEWRoleList(string Location, string currentEmployee, string terms, string HTRtype)
        {
            List<ApproverModel> lstApproverModel = new List<ApproverModel>();
            string[] role = { "QA2", "QA3", "QC3" };

            int[] RoleId = db.ATH004.Where(x => role.Contains(x.Role)).Select(x => x.Id).ToArray();
            var lstApproverList = db.ATH001.
                    Join(db.COM003, x => x.Employee, y => y.t_psno,
                    (x, y) => new { x, y })
                    .Where(z => z.y.t_actv == 1 && RoleId.Contains(z.x.Role) && z.x.Location == Location && (z.x.Employee.Contains(terms) || z.y.t_name.Contains(terms) || (z.x.Employee + " - " + z.y.t_name).Contains(terms)))
                    .Select(z => new ApproverModel
                    {
                        Code = z.x.Employee,
                        Name = z.x.Employee + " - " + z.y.t_name
                    }).Distinct().ToList();


            if (lstApproverList.Count() > 0)
            {
                lstApproverModel = lstApproverList.Select(x => new ApproverModel { Code = x.Code, Name = x.Name }).Distinct().ToList();
            }

            return lstApproverModel;
        }
        public List<string> GetList(string Role, string Location, string currentEmployee, string terms)
        {
            int RoleId, RoleId1, RoleId2;

            List<string> lstModel = new List<string>();
            bool multipleRole = false;
            string[] role = new string[] { "", "", "" };
            if (Role == "QAEngineer")
            {
                multipleRole = true;
                role[0] = clsImplementationEnum.UserRoleName.QA2.GetStringValue();
                role[1] = clsImplementationEnum.UserRoleName.QA3.GetStringValue();
                role[2] = clsImplementationEnum.UserRoleName.QC3.GetStringValue();
            }
            if (Role == "PMGManager")
            {
                multipleRole = true;
                role[0] = clsImplementationEnum.UserRoleName.PMG2.GetStringValue();
                role[1] = clsImplementationEnum.UserRoleName.PMG3.GetStringValue();
            }

            if (multipleRole)
            {
                //string[] role = { "QA2", "QA3", "QC3" };
                int[] RoleId3 = db.ATH004.Where(x => role.Contains(x.Role)).Select(x => x.Id).ToArray();
                RoleId = db.ATH004.Where(x => x.Role == Role).Select(x => x.Id).FirstOrDefault();
                var lst = db.ATH001.
                        Join(db.COM003, x => x.Employee, y => y.t_psno,
                        (x, y) => new { x, y })
                        .Where(z => z.y.t_actv == 1 && RoleId3.Contains(z.x.Role) && (z.x.Employee.Contains(terms) || z.y.t_name.Contains(terms) || (z.x.Employee + " - " + z.y.t_name).Contains(terms)))
                        .Select(z => new DataModel
                        {
                            Code = z.x.Employee,
                            Name = z.x.Employee + " - " + z.y.t_name
                        }).Distinct().ToList();

                if (lst.Count() > 0)
                {
                    lstModel = lst.Select(x => x.Name).ToList();
                }
            }
            else
            {

                int count = Role.Split('|').Length;
                if (count == 1)
                {
                    RoleId = db.ATH004.Where(x => x.Role == Role).Select(x => x.Id).FirstOrDefault();
                    var lst = db.ATH001.
                            Join(db.COM003, x => x.Employee, y => y.t_psno,
                            (x, y) => new { x, y })
                            .Where(z => z.y.t_actv == 1 && z.x.Role == RoleId && (z.x.Employee.Contains(terms) || z.y.t_name.Contains(terms) || (z.x.Employee + " - " + z.y.t_name).Contains(terms)))
                            .Select(z => new DataModel
                            {
                                Code = z.x.Employee,
                                Name = z.x.Employee + " - " + z.y.t_name
                            }).Distinct().ToList();

                    if (lst.Count() > 0)
                    {
                        lstModel = lst.Select(x => x.Name).ToList();
                    }
                }
                else
                {
                    string Role1 = Role.Split('|')[0].ToString().Trim();
                    string Role2 = Role.Split('|')[1].ToString().Trim();
                    RoleId1 = db.ATH004.Where(x => x.Role == Role1).Select(x => x.Id).FirstOrDefault();
                    RoleId2 = db.ATH004.Where(x => x.Role == Role2).Select(x => x.Id).FirstOrDefault();
                    var lst = db.ATH001.
                          Join(db.COM003, x => x.Employee, y => y.t_psno,
                          (x, y) => new { x, y })
                          .Where(z => z.y.t_actv == 1 && (z.x.Role == RoleId1 || z.x.Role == RoleId2) && z.x.Location == Location && (z.x.Employee.Contains(terms) || z.y.t_name.Contains(terms) || (z.x.Employee + " - " + z.y.t_name).Contains(terms)))
                          .Select(z => new DataModel
                          {
                              Code = z.x.Employee,
                              Name = z.x.Employee + " - " + z.y.t_name
                          }).ToList();
                    if (lst.Count() > 0)
                    {
                        lstModel = lst.Select(x => x.Name).ToList();
                    }
                }
            }
            List<string> lstModelDistinct = new List<string>();
            foreach (string s in lstModel)
            {
                if (!lstModelDistinct.Contains(s))
                {
                    lstModelDistinct.Add(s);
                }
            }
            lstModel = lstModelDistinct;


            return lstModel;
        }

        [NonAction]
        public List<string> getRoleFromTable(string psnum)
        {
            List<string> lstRole = new List<string>();
            lstRole = (from a in db.ATH001
                       join b in db.ATH004 on a.Role equals b.Id
                       where a.Employee == psnum
                       select b.Role).Distinct().ToList();
            var datao = (from f in db.FDS001
                         where f.Originator == psnum
                         select f.Originator).FirstOrDefault();

            var datar = (from f in db.FDS002
                         where f.ResponsiblePerson == psnum
                         select f.ResponsiblePerson).FirstOrDefault();
            if (!string.IsNullOrWhiteSpace(datar))
                lstRole.Add("ResponsiblePerson");

            if (!string.IsNullOrWhiteSpace(datao))
                lstRole.Add("Originator");

            return lstRole;
            //check for design engineer
            //var data = (from f in db.FDS001
            //            where f.DesignEngineer == psnum
            //            select f).FirstOrDefault();

            //if (data != null)
            //{
            //    return "Designer";
            //}
            //else
            //{
            //    //QC Manager
            //    data = (from f in db.FDS001
            //            where f.QCManager == psnum
            //            select f).FirstOrDefault();
            //    if (data != null)
            //    {
            //        return "QCManager";
            //    }
            //    else
            //    {
            //        // QA Manager
            //        data = (from f in db.FDS001
            //                where f.QAManager == psnum
            //                select f).FirstOrDefault();
            //        if (data != null)
            //        {
            //            return "QAManager";
            //        }
            //        else
            //        {
            //            // QA Engineer
            //            data = (from f in db.FDS001
            //                    where f.QAEngineer == psnum
            //                    select f).FirstOrDefault();
            //            if (data != null)
            //            {
            //                return "QAEngineer";
            //            }
            //            else
            //            {
            //                //PMG Manager
            //                data = (from f in db.FDS001
            //                        where f.PMGManager == psnum
            //                        select f).FirstOrDefault();
            //                if (data != null)
            //                {
            //                    return "PMGManager";
            //                }
            //                else
            //                {
            //                    // Responsible Person
            //                       var datar = (from f in db.FDS002
            //                                where f.ResponsiblePerson == psnum
            //                                select f).FirstOrDefault();
            //                        if (datar != null)
            //                        {
            //                            return "ResponsiblePerson";
            //                        }
            //                        else
            //                        {
            //                            return "";
            //                        }
            //                }
            //            }
            //        }

            //    }
            //}
        }
        public string GetCodeAndNameEmployee(string psnum)
        {
            var BUDescription = db.COM003.Where(i => i.t_psno == psnum && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
            return BUDescription;
        }
        public string GetCodeAndNameDept(string dept)
        {
            var BUDescription = db.COM002.Where(i => i.t_dimx == dept && i.t_dtyp == 3).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
            return BUDescription;
        }
        public string GetCodeAndNameBU(string BU)
        {
            var BUDescription = db.COM002.Where(i => i.t_dimx == BU && i.t_dtyp == 2).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
            return BUDescription;
        }
        public string GetCodeAndNameLoca(string Location)
        {
            var LocaDescription = db.COM002.Where(i => i.t_dimx == Location && i.t_dtyp == 1).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
            return LocaDescription;
        }
        public string GetCodeAndNameByProject(string project)
        {
            var projdesc = db.COM001.Where(i => i.t_cprj == project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            return projdesc;
        }
        [HttpPost]
        public ActionResult getCodeValue(string Project, string DesignEngineer, string QAEngineer, string QCManager, string QAManager, string PMGManager)
        {
            ResponceMsg objResponseMsg = new ResponceMsg();
            try
            {

                objResponseMsg.Project = db.COM001.Where(i => i.t_cprj == Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objResponseMsg.DesignEngineer = db.COM003.Where(i => i.t_psno == DesignEngineer && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
                objResponseMsg.QAEngineer = db.COM003.Where(i => i.t_psno == QAEngineer && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
                objResponseMsg.QCManager = db.COM003.Where(i => i.t_psno == QCManager && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
                objResponseMsg.QAManager = db.COM003.Where(i => i.t_psno == QAManager && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
                objResponseMsg.PMGManager = db.COM003.Where(i => i.t_psno == PMGManager && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.FDMS.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        protected bool CheckDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;
            }
            catch
            {
                return false;
            }
        }
        protected bool CheckTargetDate(String date, bool excel)
        {
            try
            {
                DateTime dt;
                if (excel)
                {
                    dt = Convert.ToDateTime(DateTime.ParseExact(date, @"d/M/yyyy", CultureInfo.InvariantCulture));
                }
                else
                {
                    dt = DateTime.Parse(date);
                }
                if (dt >= DateTime.Now.Date)
                { return true; }
                else
                { return false; }
            }
            catch
            {
                return false;
            }
        }


        [HttpPost]
        public ActionResult getRole(string project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int hd = Convert.ToInt32(project);
                string Role = "";
                var loginPerson = objClsLoginInfo.UserName;

                string scnstatus = clsImplementationEnum.FDMSStatus.SCNVer.GetStringValue();
                string retscnstatus = clsImplementationEnum.FDMSStatus.RetSCN.GetStringValue();
                string retFDRstatus = clsImplementationEnum.FDMSStatus.RetFDR.GetStringValue();
                string FDRstatus = clsImplementationEnum.FDMSStatus.FDR.GetStringValue();
                string DocSubmissionstatus = clsImplementationEnum.FDMSStatus.QAAccept.GetStringValue();
                string retDocSubmissionstatus = clsImplementationEnum.FDMSStatus.RetQA.GetStringValue();
                string DocVerstatus = clsImplementationEnum.FDMSStatus.DocVeri.GetStringValue();
                string retDocVerstatus = clsImplementationEnum.FDMSStatus.RetDocVeri.GetStringValue();
                string DocAssign = clsImplementationEnum.FDMSStatus.DocAssign.GetStringValue();
                string retDocAssign = clsImplementationEnum.FDMSStatus.RetDocAssign.GetStringValue();
                string QAAccept = clsImplementationEnum.FDMSStatus.QAAccept.GetStringValue();
                string RetQA = clsImplementationEnum.FDMSStatus.RetQA.GetStringValue();
                string PMGAccept = clsImplementationEnum.FDMSStatus.PMGAccept.GetStringValue();

                var headerid = (from a in db.FDS001
                                where a.HeaderId == hd
                                select a.Project).FirstOrDefault();
                var Exists = db.FDS001.Any(x => x.DesignEngineer == loginPerson && x.HeaderId == hd);
                if (Exists == true)
                {
                    Role = getRoleByStatusWise(hd, "DesignEngineer");
                }
                else
                {
                    var Exists1 = db.FDS001.Any(x => x.QAManager == loginPerson && x.HeaderId == hd);
                    if (Exists1 == true)
                    {
                        Role = "QAManager";
                        Role = getRoleByStatusWise(hd, "QAManager");
                        var fdrstage = db.FDS001.Where(x => x.HeaderId == hd &&
                                                (x.Status == FDRstatus || x.Status == FDRstatus
                                                || x.Status == scnstatus || x.Status == retscnstatus
                                                || x.Status == DocSubmissionstatus || x.Status == retDocSubmissionstatus
                                                //|| x.Status == DocVerstatus || x.Status == retDocVerstatus
                                                ) && (x.QAEngineer == loginPerson)).FirstOrDefault();
                        if (fdrstage != null)
                        {
                            Role = "QAEngineer";
                        }
                    }
                    else
                    {
                        var Exists2 = db.FDS001.Any(x => x.QCManager == loginPerson && x.HeaderId == hd);
                        if (Exists2 == true)
                        {

                            //Role = "QCManager";
                            Role = getRoleByStatusWise(hd, "QCManager");
                            var fdrstage = db.FDS001.Where(x => x.HeaderId == hd &&
                                               (x.Status == FDRstatus || x.Status == FDRstatus
                                               //|| x.Status == DocVerstatus || x.Status == retDocVerstatus
                                               ) && (x.QAEngineer == loginPerson)).FirstOrDefault();
                            if (fdrstage != null)
                            {
                                Role = "QAEngineer";
                            }

                        }
                        else
                        {
                            var Exists3 = db.FDS001.Any(x => x.PMGManager == loginPerson && x.HeaderId == hd);
                            if (Exists3 == true)
                            {
                                Role = getRoleByStatusWise(hd, "PMGManager");
                            }
                            else
                            {
                                var Exists4 = db.FDS001.Any(x => x.QAEngineer == loginPerson && x.HeaderId == hd);
                                if (Exists4 == true)
                                {
                                    Role = "QAEngineer";
                                }
                                else
                                {
                                    var Exists5 = db.FDS002.Any(x => x.ResponsiblePerson == loginPerson && x.HeaderId == hd);
                                    if (Exists5 == true)
                                    {
                                        Role = "ResponsiblePerson";
                                    }
                                    else
                                    {
                                        Role = "";
                                    }
                                }
                            }
                        }
                    }
                }
                objResponseMsg.Value = Role;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public bool GetAttachments(int lineid)
        {
            var HeaderId = (from a in db.FDS002
                            where a.LineId == lineid
                            select a.HeaderId).FirstOrDefault();
            //var result = (new clsFileUpload()).CheckAttachmentUpload("FDS001/" + HeaderId + "/" + lineid.ToString());
            IEMQS.Areas.Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
            var result = _objFUC.CheckAnyDocumentsExits("FDS001//" + HeaderId + "//" + lineid.ToString(), lineid);
            //bool result;
            //if (v.Count() > 0)
            //{
            //    result = true;
            //}
            //else
            //{
            //    result = false;
            //}

            return result;
        }
        [HttpPost]
        public bool GetRemarks(int lineid)
        {
            bool result;
            var RemarkBySubmitter = (from a in db.FDS002
                                     where a.LineId == lineid
                                     select a.RemarkBySubmitter).FirstOrDefault();
            if (string.IsNullOrEmpty(RemarkBySubmitter))
                result = false;
            else
                result = true;

            return result;
        }
        [HttpPost]
        public bool GetComments(int lineid)
        {
            bool result;
            var DelayComments = (from a in db.FDS002
                                 where a.LineId == lineid
                                 select a.DelayComments).FirstOrDefault();
            if (string.IsNullOrEmpty(DelayComments))
                result = false;
            else
                result = true;

            return result;
        }

        [HttpPost]
        public ActionResult getSubmitted(int headerid)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                int flag1 = 0;
                var lstResult = db.SP_FDMS_GET_LINES_Status
                                (headerid).ToList();
                foreach (var val in lstResult)
                {
                    if (val.Status == clsImplementationEnum.LineStatusFDMS.Submitted.GetStringValue())
                    {
                        flag1 = 0;
                        break;
                    }
                    else
                    {
                        flag1 = flag1 + 1;
                    }
                }
                objResponseMsg.Value = flag1.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.FDMS.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult getAssigned(int headerid)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                int flag1 = 0;
                var lstResult = db.SP_FDMS_GET_LINES_Status
                                (headerid).ToList();
                foreach (var val in lstResult)
                {
                    if (val.Status == clsImplementationEnum.LineStatusFDMS.Submitted.GetStringValue() || val.Status == clsImplementationEnum.LineStatusFDMS.Accepted.GetStringValue())
                    {
                        flag1 = flag1 + 1;
                    }
                    else
                    {
                        flag1 = 0;
                        break;
                    }
                }
                objResponseMsg.Value = flag1.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.FDMS.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult getAccepted(int headerid)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                bool accepted = true;
                var lstResult = db.SP_FDMS_GET_LINES_Status
                                (headerid).ToList();
                if (lstResult.Count > 0 || lstResult.Count != 0)
                {
                    foreach (var val in lstResult)
                    {
                        if (!val.Status.Trim().ToLower().Equals(clsImplementationEnum.LineStatusFDMS.Accepted.GetStringValue().ToLower()))
                        {
                            accepted = false;
                            break;
                        }
                    }
                }
                else
                {
                    accepted = false;
                }
                objResponseMsg.Key = accepted;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.FDMS.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public string getRoleByStatusWise(int hd, string role)
        {
            string Role = string.Empty;
            string loginPerson = objClsLoginInfo.UserName;
            string scnstatus = clsImplementationEnum.FDMSStatus.SCNVer.GetStringValue();
            string retscnstatus = clsImplementationEnum.FDMSStatus.RetSCN.GetStringValue();
            string retFDRstatus = clsImplementationEnum.FDMSStatus.RetFDR.GetStringValue();
            string FDRstatus = clsImplementationEnum.FDMSStatus.FDR.GetStringValue();
            string DocSubmissionstatus = clsImplementationEnum.FDMSStatus.QAAccept.GetStringValue();
            string retDocSubmissionstatus = clsImplementationEnum.FDMSStatus.RetQA.GetStringValue();
            string DocVerstatus = clsImplementationEnum.FDMSStatus.DocVeri.GetStringValue();
            string retDocVerstatus = clsImplementationEnum.FDMSStatus.RetDocVeri.GetStringValue();
            string DocAssign = clsImplementationEnum.FDMSStatus.DocAssign.GetStringValue();
            string retDocAssign = clsImplementationEnum.FDMSStatus.RetDocAssign.GetStringValue();
            string QAAccept = clsImplementationEnum.FDMSStatus.QAAccept.GetStringValue();
            string RetQA = clsImplementationEnum.FDMSStatus.RetQA.GetStringValue();
            string PMGAccept = clsImplementationEnum.FDMSStatus.PMGAccept.GetStringValue();

            var headerid = (from a in db.FDS001
                            where a.HeaderId == hd
                            select a.Project).FirstOrDefault();

            var fdrstage = db.FDS001.Where(x => x.HeaderId == hd &&
                                             (x.Status == FDRstatus || x.Status == FDRstatus) && (x.DesignEngineer == loginPerson)).FirstOrDefault();

            var docassignmentstage = db.FDS001.Where(x => x.HeaderId == hd &&
                                  (x.Status == retDocAssign || x.Status == DocAssign || x.Status == DocSubmissionstatus || x.Status == retDocSubmissionstatus || x.Status == QAAccept || x.Status == RetQA) && (x.QAEngineer == loginPerson)).FirstOrDefault();

            var scnStage = db.FDS001.Where(x => x.HeaderId == hd &&
                                  (x.Status == scnstatus || x.Status == retscnstatus) && (x.QCManager == loginPerson)).FirstOrDefault();

            var pmgStage = db.FDS001.Where(x => x.HeaderId == hd &&
                                    (x.Status == PMGAccept) && (x.PMGManager == loginPerson)).FirstOrDefault();

            var notPMGstage = db.FDS002.Any(x => x.ResponsiblePerson == loginPerson && x.HeaderId == hd && ((x.FDS001.Status != PMGAccept && x.FDS001.PMGManager == loginPerson) || ((x.FDS001.Status != FDRstatus || x.FDS001.Status != retFDRstatus) && x.FDS001.DesignEngineer == loginPerson)));
            if (fdrstage != null)
            {
                Role = "DesignEngineer";
            }
            else if (docassignmentstage != null)
            {
                Role = "QAEngineer";
            }

            else if (scnStage != null)
            {
                Role = "QCManager";
            }
            else if (notPMGstage == true)
            {
                Role = "ResponsiblePerson";
            }

            else if (pmgStage != null)
            {
                Role = "PMGManager";
            }
            else
            { Role = role; }
            return Role;
        }

        [HttpPost]
        public ActionResult getRoleForNew()
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {

                var lstResult = (from a in db.ATH001
                                 join b in db.ATH004 on a.Role equals b.Id
                                 where a.Employee.Trim().Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                                 select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();

                string psnum = objClsLoginInfo.UserName;
                if (lstResult.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.QC2.GetStringValue(), StringComparison.OrdinalIgnoreCase)
                                       || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), StringComparison.OrdinalIgnoreCase)
                                       || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.QA3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Key = false;
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.FDMS.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult getInlineFDMSLines(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                string strSortOrder = string.Empty;
                int headerid = Convert.ToInt32(param.Headerid);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (FDMSName like '%" + param.sSearch
                       + "%' or FDMSDocumentName like '%" + param.sSearch
                       + "%' or DocumentNumber like '%" + param.sSearch
                       + "%' or Department +' - '+ i.t_desc like '%" + param.sSearch
                       + "%' or ResponsiblePerson +' - '+ i.t_name like '%" + param.sSearch
                       + "%' or Status like '%" + param.sSearch
                       + "%' or TargetDate like '%" + param.sSearch
                       + "%')";
                }
                else
                {
                    strWhere = "1=1";
                }
                var lstResult = db.SP_FDMS_GET_LINES
                                (
                                StartIndex, EndIndex, strSortOrder, headerid, strWhere
                                ).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                    Helper.GenerateHidden(newRecordId, "LineId", ""),
                                    Helper.GenerateHidden(newRecordId, "HeaderId", headerid.ToString()),
                                    Helper.GenerateHidden(newRecordId, "FDMSName"),
                                    Helper.GenerateTextbox(newRecordId, "FDMSDocumentName" ,"", "", false, "", "50"),
                                    Helper.GenerateTextbox(newRecordId, "DocumentNumber","", "", false, "", "15"),

                                    //GenerateAutoComplete(newRecordId,"txtDept","","",false,"","Department")+""+
                                    Helper.HTMLAutoComplete(newRecordId,"ResponsiblePerson"),
                                    Helper.GenerateTextbox(newRecordId,"Department","","",true),
                                  //  GenerateAutoComplete(newRecordId,"txtEmp","","",false,"","ResponsiblePerson")+""+
                                 
                                    Helper.GenerateTextbox(newRecordId, "TargetDate"),
                                  clsImplementationEnum.LineStatusFDMS.Assigned.GetStringValue(),
                                    "",
                                    "NA",
                                     "NA",//   Helper.GenerateTextbox(newRecordId, "DocumentSubmissionDate"),
                                    "NA",
                                    "",
                                    Helper.GenerateTextbox(newRecordId, "MailRestartdate"),
                                          "No",
                                    Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveNewRecord();" ),
                                };


                var data = (from uc in lstResult
                            select new[]
                            {
                                Helper.GenerateHidden(uc.LineId, "LineId", Convert.ToString(uc.LineId)),
                                Helper.GenerateHidden(uc.HeaderId, "HeaderId", Convert.ToString(uc.HeaderId)),
                                Helper.GenerateHidden(uc.LineId, "FDMSName", uc.FDMSName),
                                Helper.GenerateTextbox(uc.LineId, "FDMSDocumentName", uc.FDMSDocumentName , "UpdateLineDetails(this, "+ uc.LineId +");",false,"","50"),
                                Helper.GenerateTextbox(uc.LineId, "DocumentNumber", uc.DocumentNumber , "UpdateLineDetails(this, "+ uc.LineId +");",false,"","15"),
                                // GenerateAutoComplete(uc.LineId, "txtDept",uc.Department,"UpdateLineDetails(this, "+ uc.LineId +");",false,"", "Department" , false) +""+
                                // GenerateAutoComplete(uc.LineId, "txtEmp",uc.ResponsiblePerson ,"UpdateLineDetails(this, "+ uc.LineId +");",false,"", "ResponsiblePerson" , false) +""+ 
                                Helper.HTMLAutoComplete(uc.LineId,"ResponsiblePerson",uc.ResponsiblePerson , "UpdateLineDetails(this, "+ uc.LineId +");"),
                                Helper.GenerateTextbox(uc.LineId,"Department",uc.Department, "",true),
                                Helper.GenerateTextbox(uc.LineId, "TargetDate",Convert.ToDateTime(uc.TargetDate).ToString("yyyy-MM-dd",CultureInfo.InvariantCulture), "UpdateLineDetails(this, "+ uc.LineId +");"),
                                Convert.ToString(uc.Status),
                                Convert.ToString(uc.RemarkBySubmitter),
                                uc.DocumentAssignedDate == null || uc.DocumentAssignedDate.Value==DateTime.MinValue? "NA" :uc.DocumentAssignedDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                uc.DocumentSubmissionDate == null || uc.DocumentSubmissionDate.Value==DateTime.MinValue? "NA" :uc.DocumentSubmissionDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                uc.LastDocumentVerificationDatebyQA == null || uc.LastDocumentVerificationDatebyQA.Value==DateTime.MinValue? "NA" :uc.LastDocumentVerificationDatebyQA.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                Convert.ToString(uc.RejectionComments),
                                Helper.GenerateTextbox(uc.LineId, "MailRestartdate",(uc.MailRestartdate==null || uc.MailRestartdate.Value==DateTime.MinValue?"":uc.MailRestartdate.Value.ToString("yyyy-MM-dd")), "UpdateLineDetails(this, "+ uc.LineId +");"),
                                Convert.ToString(uc.SubmittoPMG == true ? "Yes":"No"),
                                HTMLActionString(uc.LineId,"Delete", "Delete Rocord", "fa fa-trash-o", "DeleteRecord("+ uc.LineId +");"),
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        public string HTMLActionString(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", bool isVisible = true, bool atagrequired = false)
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick=" + onClickMethod : "";
            if (isVisible)
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' Title='" + buttonTooltip + "' class='iconspace " + className + "'" + onClickEvent + " ></i>";
            }
            else
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' Title='" + buttonTooltip + "' class='disabledicon " + className + "'" + onClickEvent + " ></i>";
            }
            return htmlControl;
        }

        public string GenerateAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false)
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' hdElement='" + hdElementId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + (disabled ? "disabled" : "") + " />";

            return strAutoComplete;
        }

        bool IsAllDigits(string s)
        {
            return s.All(char.IsDigit);
        }

        public ActionResult verifyProject(string project)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                project = project.Split('-')[0].ToString().Trim();
                var Exists = db.COM001.Any(x => x.t_cprj == project);
                if (Exists == false)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Invalid Project..! Please Try Again..!";
                }
                else
                {
                    string YY = "";
                    var ExistsFDS = db.FDS001.Any(x => x.Project == project);
                    if (ExistsFDS == false)
                    {
                        YY = "01";
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = YY;
                    }
                    else
                    {
                        var existFDMS = db.FDS001.OrderByDescending(p => p.Project == project).FirstOrDefault().FDMSName;
                        var obj = (from a in db.FDS001
                                   where a.Project == project
                                   orderby a.HeaderId descending
                                   select a).FirstOrDefault();
                        //if (obj.Status == clsImplementationEnum.FDMSStatus.Completed.GetStringValue())
                        //{
                        YY = obj.FDMSName.Split('-')[2].ToString().Trim();
                        int yy = Convert.ToInt32(YY) + 1;
                        YY = "0" + yy.ToString();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = YY;
                        //}
                        //else
                        //{
                        //    objResponseMsg.Key = false;
                        //    objResponseMsg.Value = existFDMS + " is still Pending..!";
                        //}
                    }

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.FDMS.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.TimelineTitle = "FDMS History";
            model.Title = "FDMS";
            FDS001 objFDS001 = db.FDS001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

            model.CreatedBy = Manager.GetUserNameFromPsNo(objFDS001.Originator);
            model.CreatedOn = objFDS001.CreatedOn;
            model.FDRBy = Manager.GetUserNameFromPsNo(objFDS001.FDRBy);
            model.FDROn = objFDS001.FDROn;
            model.DOCAssBy = Manager.GetUserNameFromPsNo(objFDS001.DocAssBy);
            model.DOCAssOn = objFDS001.DocAssOn;
            model.SCNBy = Manager.GetUserNameFromPsNo(objFDS001.SCNBy);
            model.SCNOn = objFDS001.SCNOn;
            model.DocVeriBy = Manager.GetUserNameFromPsNo(objFDS001.DocVeriBy);
            model.DocVeriOn = objFDS001.DocVeriOn;
            model.QAVeriBy = Manager.GetUserNameFromPsNo(objFDS001.QAVeriBy);
            model.QAVeriOn = objFDS001.QAVeriOn;
            model.PMGAccepBy = Manager.GetUserNameFromPsNo(objFDS001.PMGAccepBy);
            model.PMGAccepOn = objFDS001.PMGAccepOn;
            model.CompletedBy = Manager.GetUserNameFromPsNo(objFDS001.CompletedBy);
            model.CompletedOn = objFDS001.CompletedOn;


            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }

        [HttpPost]
        public string ValidDateformat(DateTime date)
        {
            string validdate = "";

            if (date == DateTime.MinValue || date == null)

            { validdate = Convert.ToDateTime(DateTime.ParseExact(date.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture)).ToString(); }
            else
            { validdate = "NA"; }

            return validdate;
        }

        [HttpPost]
        public ActionResult GetBU(string projectcode)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            string BUid = db.COM001.Where(i => i.t_cprj == projectcode).FirstOrDefault().t_entu;
            var BUDescription = (from a in db.COM002
                                 where a.t_dtyp == 2 && a.t_dimx == BUid
                                 select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
            objResponseMsg.Value = BUDescription.BUDesc;
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Export Excel for all grid(Common functionality)
        //Code By : Nikita , Export Functionality
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "", int headerid = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_FDMS_GET_CREATE(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      FDMSName = uc.FDMSName,
                                      Project = uc.Project,
                                      Location = uc.Location,
                                      BU = uc.BU,
                                      Status = uc.Status,
                                      DesignEngineer = uc.DesignEngineer,
                                      QCManager = uc.QCManager,
                                      QAManager = uc.QAManager,
                                      PMGManager = uc.PMGManager,
                                      QAEngineer = uc.QAEngineer,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.FDMSAssigneeLines.GetStringValue())
                {//inline grid data
                    var lst = db.SP_FDMS_GET_LINES(1, int.MaxValue, strSortOrder, headerid, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from uc in lst
                                  select new
                                  {
                                      FDMSName = uc.FDMSName,
                                      FDMSDocumentName = uc.FDMSDocumentName,
                                      DocumentNumber = uc.DocumentNumber,
                                      Department = uc.Department,
                                      ResponsiblePerson = uc.ResponsiblePerson,
                                      TargetDate = uc.TargetDate,
                                      Status = uc.Status,
                                      RemarkBySubmitter = uc.RemarkBySubmitter,
                                      DocumentAssignedDate = uc.DocumentAssignedDate == null || uc.DocumentAssignedDate == DateTime.MinValue ? "NA" : uc.DocumentAssignedDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      DocumentSubmissionDate = uc.DocumentSubmissionDate == null || uc.DocumentSubmissionDate.Value == DateTime.MinValue ? "NA" : uc.DocumentSubmissionDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      LastDocumentVerificationDatebyQA = uc.LastDocumentVerificationDatebyQA == null || uc.LastDocumentVerificationDatebyQA.Value == DateTime.MinValue ? "NA" : uc.LastDocumentVerificationDatebyQA.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      RejectionComments = uc.RejectionComments,
                                      MailRestartdate = uc.MailRestartdate == null || uc.MailRestartdate.Value == DateTime.MinValue ? "" : uc.MailRestartdate.Value.ToString("yyyy-MM-dd"),
                                      SubmittoPMG = uc.SubmittoPMG == true ? "Yes" : "No",
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.FDMSverificationLines.GetStringValue())
                {
                    //verififcation lines data
                    var lst = db.SP_FDMS_GET_LINES(1, int.MaxValue, strSortOrder, headerid, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      FDMSName = uc.FDMSName,
                                      FDMSDocumentName = uc.FDMSDocumentName,
                                      DocumentNumber = uc.DocumentNumber,
                                      Department = uc.Department,
                                      ResponsiblePerson = uc.ResponsiblePerson,
                                      TargetDate = uc.TargetDate,
                                      DelayComments = uc.DelayComments,
                                      RemarkBySubmitter = uc.RemarkBySubmitter,
                                      Status = Convert.ToString(uc.Status),
                                      DocumentAssignedDate = (uc.DocumentAssignedDate == null || uc.DocumentAssignedDate == DateTime.MinValue ? "NA" : uc.DocumentAssignedDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)),
                                      DocumentSubmissionDate = (uc.DocumentSubmissionDate == null || uc.DocumentSubmissionDate.Value == DateTime.MinValue ? "NA" : uc.DocumentSubmissionDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)),
                                      LastDocumentVerificationDatebyQA = (uc.LastDocumentVerificationDatebyQA == null || uc.LastDocumentVerificationDatebyQA.Value == DateTime.MinValue ? "NA" : uc.LastDocumentVerificationDatebyQA.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)),
                                      RejectionComments = uc.RejectionComments,
                                      MailRestartdate = uc.MailRestartdate == null || uc.MailRestartdate.Value == DateTime.MinValue ? "" : uc.MailRestartdate.Value.ToString("yyyy-MM-dd"),
                                      SubmittoPMG = (uc.SubmittoPMG == true ? "Yes" : "No"),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error for generation of excel, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }


        #endregion

        #region FileUpload

        public ActionResult LoadFileUploadPartial(int HeaderId, int LineId, string ControlId)
        {
            //ViewBag.HeaderId = HeaderId;
            //ViewBag.LineId = LineId;
            //ViewBag.ControlId = ControlId;
            //var objFDS002 = db.FDS002.Where(i => i.LineId == LineId).FirstOrDefault();
            //if (objFDS002 != null && objFDS002.ResponsiblePerson.Trim() == objClsLoginInfo.UserName.Trim() && !((objFDS002.Status == clsImplementationEnum.LineStatusFDMS.Accepted.GetStringValue()) || (objFDS002.Status == clsImplementationEnum.LineStatusFDMS.Submitted.GetStringValue())))
            //{
            //    ViewBag.DisplayAttachmentbtn = true;
            //    ViewBag.HideDeleteAction = false;
            //}
            //else
            //{
            //    ViewBag.DisplayAttachmentbtn = false;
            //    ViewBag.HideDeleteAction = true;
            //}
            //return PartialView("_LoadFileUploadPartial");

            var objFDS002 = db.FDS002.Where(i => i.LineId == LineId).FirstOrDefault();
            if (objFDS002 != null && objFDS002.ResponsiblePerson.Trim() == objClsLoginInfo.UserName.Trim() && !((objFDS002.Status == clsImplementationEnum.LineStatusFDMS.Accepted.GetStringValue()) || (objFDS002.Status == clsImplementationEnum.LineStatusFDMS.Submitted.GetStringValue())))
            {
                return Json(new
                {
                    DisplayAttachmentbtn = true
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new
                {
                    DisplayAttachmentbtn = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion  
    }
    public class DataModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
    public class ResponceMsg
    {
        public Boolean Key { get; set; }
        public string Value { get; set; }
        public string Project { get; set; }
        public string DesignEngineer { get; set; }
        public string QAEngineer { get; set; }
        public string QCManager { get; set; }
        public string QAManager { get; set; }
        public string PMGManager { get; set; }
    }

}