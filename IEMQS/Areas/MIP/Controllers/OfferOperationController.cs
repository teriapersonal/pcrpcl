﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQS.Models;
using System.Data.Entity.Core.Objects;
using IEMQS.Areas.Utility.Models;
using System.Data;
using OfficeOpenXml;
using System.Drawing;
using System.IO;

namespace IEMQS.Areas.MIP.Controllers
{
    public class OfferOperationController : clsBase
    {
        // GET: MIP/OfferOperation
        #region Index Page
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Index()
        {
            ViewBag.Title = "Offer Operation";
            return View();
        }

        public ActionResult GetIndexGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetIndexGridDataPartial");
        }

        public JsonResult LoadMIPIndexDataTable(JQueryDataTableParamModel param)
        {
            try
            {
                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "m3.BU", "m3.Location");

                if (param.Status.Equals("PENDING", StringComparison.OrdinalIgnoreCase))
                    whereCondition += " and m4.inspectionstatus  in('" + clsImplementationEnum.MIPInspectionStatus.ReadyToOffer.GetStringValue() + "','" + clsImplementationEnum.MIPInspectionStatus.Returned.GetStringValue() + "')";

                string[] columnName = { "m3.QualityProject", "(m3.Project+'-'+com1.t_dsca)", "(LTRIM(RTRIM(m3.Location))+'-'+LTRIM(RTRIM(lcom2.t_desc)))", "(LTRIM(RTRIM(m3.BU))+'-'+LTRIM(RTRIM(bcom2.t_desc)))", "m3.ComponentNo", "m3.DocNo", "m3.MIPType" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);

                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstHeader = db.SP_MIP_GET_OFFER_HEADER_DATA(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstHeader
                          select new[] {
                        a.QualityProject,
                        a.Project,
                        a.BU,
                        a.Location,
                        a.ComponentNo,
                        Manager.GetCategoryOnlyDescription(a.DocNo,a.BU.Split('-')[0],a.Location.Split('-')[0]  ,"MIP_DocNo" ),
                        Manager.GetCategoryOnlyDescription(a.MIPType,a.BU.Split('-')[0],a.Location.Split('-')[0]  ,"MIP_Type" ),
                        Helper.HTMLActionString(Convert.ToInt32(a.HeaderId), "View", "View Details", "fa fa-eye", "", "/MIP/OfferOperation/OfferOpearationDetails/"+a.HeaderId )
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult LoadMIPHeaderDataTable(JQueryDataTableParamModel param)
        {
            try
            {
                UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
                string isReoffer = "false";
                int LineId = 0;
                int.TryParse(param.Headerid, out LineId);

                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "m3.BU", "m3.Location");
                if (param.Status.Equals("PENDING", StringComparison.OrdinalIgnoreCase))
                {
                    whereCondition += " and m4.InspectionStatus= '" + clsImplementationEnum.MIPLineStatus.ReadyToOffer.GetStringValue() + "'";
                }
                else if (param.Status.Equals("REOFFER", StringComparison.OrdinalIgnoreCase))
                {
                    isReoffer = "true";
                    whereCondition += " and m4.InspectionStatus in ('" + clsImplementationEnum.MIPInspectionStatus.Returned.GetStringValue() + "')";
                }

                MIP004 obj = db.MIP004.Where(x => x.LineId == LineId).FirstOrDefault();
                if (obj != null)
                    whereCondition += "and m3.headerId =" + obj.HeaderId;
                string[] columnName = { "m3.QualityProject", "(m3.Project+'-'+com1.t_dsca)", "(LTRIM(RTRIM(m3.Location))+'-'+LTRIM(RTRIM(lcom2.t_desc)))", "(LTRIM(RTRIM(m3.BU))+'-'+LTRIM(RTRIM(bcom2.t_desc)))", "m4.OperationNo", "m4.ActivityDescription", "m4.RefDocument", "m4.RefDocRevNo", "m4.PIA", "m4.IAgencyLNT", "m4.IAgencyVVPT", "m4.IAgencySRO", "m4.RecordsDocuments", "m4.Observations", "m4.InspectionStatus" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                //else
                //    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);

                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstHeader = db.SP_MIP_HEADER_GET_LINES_DATA(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var lstPIA = Manager.GetSubCatagories("PIA", obj.BU, obj.Location);
                var lstIAgencyLNT = Manager.GetSubCatagories("MIP Inspection Agency L&T", obj.BU, obj.Location);
                var lstIAgencyVVPT = Manager.GetSubCatagories("MIP Inspection Agency VVPT", obj.BU, obj.Location);
                var lstIAgencySRO = Manager.GetSubCatagories("MIP Inspection Agency SRO", obj.BU, obj.Location);
                var returnStatus = clsImplementationEnum.MIPInspectionStatus.Returned.GetStringValue();
                var rtoStatus = clsImplementationEnum.MIPInspectionStatus.ReadyToOffer.GetStringValue();
                var res = from uc in lstHeader
                          select new[] {
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               uc.OperationNo.ToString("G29"),
                               uc.ActivityDescription,
                               uc.RefDocument,
                               uc.RefDocRevNo,
                               lstPIA.Where(w=>w.Code == uc.PIA).Select(s=>s.Description).FirstOrDefault(),
                               lstIAgencyLNT.Where(w=>w.Code == uc.IAgencyLNT).Select(s=>s.Description).FirstOrDefault(),
                               lstIAgencyVVPT.Where(w=>w.Code == uc.IAgencyVVPT).Select(s=>s.Description).FirstOrDefault(),
                               lstIAgencySRO.Where(w=>w.Code == uc.IAgencySRO).Select(s=>s.Description).FirstOrDefault(),
                               uc.RecordsDocuments,
                               uc.Observations,
                               "R"+uc.LineRevNo,
                               "R"+uc.MIPRevNo,
                               uc.InspectionStatus,
                        Helper.HTMLActionString(uc.LineId, "View", "View Details", "fa fa-eye", "", "/MIP/OfferOperation/Details/"+uc.LineId )
                        + Helper.GenerateActionIcon(uc.LineId, "RequestOffer", "Request Offer", "fa fa-paper-plane", "RequestOffer("+uc.LineId+","+isReoffer+")", "", isReoffer == "true" ? uc.InspectionStatus != returnStatus: uc.InspectionStatus != rtoStatus)
                        + Helper.GenerateActionIcon(uc.HeaderId, "notApplicable", "Not Applicable", "fa fa-close", "OfferApplicable("+uc.LineId+","+isReoffer+")","",uc.InspectionStatus != clsImplementationEnum.TravelerInspectionStatus.ReadyToOffer.GetStringValue(),false,"color:red")
                        + Manager.generateMIPGridButtons(uc.LineId,"MIP Details",50,"MIP Details","Request Details","MIP Request Details")
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Header Details Page

        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult OfferOpearationDetails(int id = 0)
        {
            MIP003 objMIP003 = new MIP003();
            MIP004 objMIP004 = db.MIP004.Where(i => i.LineId == id).FirstOrDefault();
            if (objMIP004 != null)
            {
                if (id > 0)
                {
                    objMIP003 = db.MIP003.Where(i => i.HeaderId == objMIP004.HeaderId).FirstOrDefault();
                    ViewBag.Project = Manager.GetProjectAndDescription(objMIP003.Project);
                    ViewBag.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objMIP003.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                    ViewBag.DocNoDesc = Manager.GetCategoryOnlyDescription(objMIP003.DocNo, objMIP003.BU, objMIP003.Location, "MIP_DocNo");
                    ViewBag.MIPTypeDesc = Manager.GetCategoryOnlyDescription(objMIP003.MIPType, objMIP003.BU, objMIP003.Location, "MIP_Type");
                }
                else
                {
                    objMIP003.Location = objClsLoginInfo.Location;
                }
                ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objMIP003.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            }
            ViewBag.Id = id;
            return View(objMIP003);
        }

        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            MIP003 objMIP003 = new MIP003();
            MIP004 objMIP004 = new MIP004();
            if (id > 0)
            {
                objMIP004 = db.MIP004.Where(i => i.LineId == id).FirstOrDefault();
                if (objMIP004 != null)
                {
                    objMIP003 = db.MIP003.Where(i => i.HeaderId == objMIP004.HeaderId).FirstOrDefault();

                    ViewBag.ProjectCode = objMIP003.Project;
                    ViewBag.QualityProject = objMIP003.QualityProject;
                    ViewBag.LocationCode = objMIP003.Location;
                    ViewBag.BUCode = objMIP003.BU;
                    ViewBag.DocNo = objMIP003.DocNo;
                    ViewBag.MIPType = objMIP003.MIPType;
                    ViewBag.RevNo = "R" + objMIP003.RevNo;
                    ViewBag.ComponentNo = objMIP003.ComponentNo;
                    ViewBag.DrawingNo = objMIP003.DrawingNo;
                    ViewBag.RawMaterialGrade = objMIP003.RawMaterialGrade;
                    ViewBag.PlateNo = objMIP003.PlateNo;
                    ViewBag.DocNoDesc = Manager.GetCategoryOnlyDescription(objMIP003.DocNo, objMIP003.BU, objMIP003.Location, "MIP_DocNo");
                    ViewBag.MIPTypeDesc = Manager.GetCategoryOnlyDescription(objMIP003.MIPType, objMIP003.BU, objMIP003.Location, "MIP_Type");
                    ViewBag.Project = Manager.GetProjectAndDescription(objMIP003.Project);
                    ViewBag.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objMIP003.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                    if (objMIP004.InspectionStatus == clsImplementationEnum.MIPInspectionStatus.Returned.GetStringValue())
                        ViewBag.IsReoffer = "true";
                }
            }
            else
            {
                objMIP003.Location = objClsLoginInfo.Location;
            }
            ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objMIP003.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            ViewBag.Title = "Offer MIP";
            return View(objMIP004);
        }

        public ActionResult GetMIPHeaderDataPartial(string Status, string QualityProject, string Location, string BU, string lineid)
        {
            ViewBag.status = Status;
            ViewBag.qualityProject = QualityProject;
            ViewBag.location = Location;
            ViewBag.bu = BU;
            ViewBag.lineid = lineid;

            UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            return PartialView("_GetHeaderDataPartial");
        }

        [HttpPost]
        public JsonResult RequestOffered(int lineid, bool isReoffer = false)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                MIP004 objMIP004 = new MIP004();
                string rto = clsImplementationEnum.MIPInspectionStatus.ReadyToOffer.GetStringValue();
                string Approved = clsImplementationEnum.MIPHeaderStatus.Approved.GetStringValue();
                string rejected = clsImplementationEnum.MIPInspectionStatus.Rejected.GetStringValue();
                string returned = clsImplementationEnum.MIPInspectionStatus.Returned.GetStringValue();
                if (isReoffer)
                {
                    objMIP004 = db.MIP004.Where(x => x.LineId == lineid && (x.InspectionStatus == returned)).FirstOrDefault();
                    if (objMIP004 != null)
                    {
                        if (objMIP004.MIP003.Status == Approved)
                        {
                            #region Offer Entry
                            objMIP004.InspectionStatus = clsImplementationEnum.MIPInspectionStatus.UnderInspection.GetStringValue();
                            objMIP004.EditedBy = objClsLoginInfo.UserName;
                            objMIP004.EditedOn = DateTime.Now;
                            MIP050 objSrcMIP050 = db.MIP050.Where(x => x.RequestNo == objMIP004.RequestNo && x.RequestNoSequence == objMIP004.RequestSequenceNo).FirstOrDefault();
                            if (objSrcMIP050 != null)
                            {
                                objMIP004.RequestSequenceNo = objMIP004.RequestSequenceNo + 1;

                                MIP050 objMIP050 = db.MIP050.Add(new MIP050
                                {
                                    QualityProject = objSrcMIP050.QualityProject,
                                    Project = objSrcMIP050.Project,
                                    BU = objSrcMIP050.BU,
                                    Location = objSrcMIP050.Location,
                                    DocNo = objSrcMIP050.DocNo,
                                    MIPType = objSrcMIP050.MIPType,
                                    RevNo = objSrcMIP050.RevNo,
                                    ComponentNo = objSrcMIP050.ComponentNo,
                                    DrawingNo = objSrcMIP050.DrawingNo,
                                    RawMaterialGrade = objSrcMIP050.RawMaterialGrade,
                                    PlateNo = objSrcMIP050.PlateNo,
                                    IterationNo = objSrcMIP050.IterationNo,
                                    RequestNo = objSrcMIP050.RequestNo,
                                    RequestNoSequence = objMIP004.RequestSequenceNo,
                                    OperationNo = objSrcMIP050.OperationNo,
                                    LineRevNo = objSrcMIP050.LineRevNo,
                                    MIPRevNo = objSrcMIP050.MIPRevNo,
                                    ActivityDescription = objSrcMIP050.ActivityDescription,
                                    RefDocument = objSrcMIP050.RefDocument,
                                    RefDocRevNo = objSrcMIP050.RefDocRevNo,
                                    PIA = objSrcMIP050.PIA,
                                    IAgencyLNT = objSrcMIP050.IAgencyLNT,
                                    IAgencyVVPT = objSrcMIP050.IAgencyVVPT,
                                    IAgencySRO = objSrcMIP050.IAgencySRO,
                                    RecordsDocuments = objSrcMIP050.RecordsDocuments,
                                    Observations = objSrcMIP050.Observations,
                                    CreatedBy = objClsLoginInfo.UserName,
                                    CreatedOn = DateTime.Now,
                                    OfferedBy = objClsLoginInfo.UserName,
                                    OfferedOn = DateTime.Now
                                });
                                db.SaveChanges();

                                objMIP004.RequestNo = objMIP050.RequestNo;
                                objMIP004.IterationNo = objMIP050.IterationNo;
                                objMIP004.RequestSequenceNo = objMIP050.RequestNoSequence;
                                db.SaveChanges();
                            }
                            //#region Send Notification
                            //(new clsManager()).SendNotificationBULocationWise((clsImplementationEnum.UserRoleName.QI1.GetStringValue() + ',' + clsImplementationEnum.UserRoleName.QI2.GetStringValue() + ',' + clsImplementationEnum.UserRoleName.QI3.GetStringValue()), objMIP004.BU, objMIP004.Location,
                            //    "Traveler:  Request " + objMIP050.RequestNo + " of Activity: " + objMIP050.Activity + " & Project No: " + objMIP050.ProjectNo + " has been offered for Inspection", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/TVL/AttendInspection/Details/" + objMIP050.RequestId);
                            //#endregion

                            #endregion

                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "Data Re-Offered Successfully";
                            objResponseMsg.Status = objMIP004.InspectionStatus;
                            objResponseMsg.HeaderId = objMIP004.HeaderId;
                        }
                        else
                        {
                            objResponseMsg.Value = "Component: " + objMIP004.MIP003.ComponentNo + " is in '" + objMIP004.MIP003.Status + "' in MIP Header. Please approve component first.";
                        }
                    }
                    else
                    {
                        objResponseMsg.Value = "Data not available.";
                    }
                }
                else
                {
                    objMIP004 = db.MIP004.Where(x => x.LineId == lineid && (x.InspectionStatus == rto)).FirstOrDefault();
                    if (objMIP004 != null)
                    {
                        if (objMIP004.MIP003.Status == Approved)
                        {
                            #region Offer Entry
                            objMIP004.InspectionStatus = clsImplementationEnum.MIPInspectionStatus.UnderInspection.GetStringValue();
                            objMIP004.EditedBy = objClsLoginInfo.UserName;
                            objMIP004.EditedOn = DateTime.Now;

                            long RequestNo = 0;
                            int ReqSeqNo = 1;
                            ObjectParameter outParm = new ObjectParameter("result", typeof(long));
                            var SPResult = db.SP_MIP_GET_NEXT_REQUEST_NO(objMIP004.BU, objMIP004.Location, outParm);
                            RequestNo = (long)outParm.Value;

                            MIP050 objMIP050 = db.MIP050.Add(new MIP050
                            {
                                QualityProject = objMIP004.QualityProject,
                                Project = objMIP004.MIP003.Project,
                                BU = objMIP004.MIP003.BU,
                                Location = objMIP004.MIP003.Location,
                                DocNo = objMIP004.MIP003.DocNo,
                                MIPType = objMIP004.MIP003.MIPType,
                                RevNo = objMIP004.MIP003.RevNo,
                                ComponentNo = objMIP004.MIP003.ComponentNo,
                                DrawingNo = objMIP004.MIP003.DrawingNo,
                                RawMaterialGrade = objMIP004.MIP003.RawMaterialGrade,
                                PlateNo = objMIP004.MIP003.PlateNo,
                                IterationNo = 1,
                                RequestNo = RequestNo,
                                RequestNoSequence = ReqSeqNo,
                                OperationNo = objMIP004.OperationNo,
                                LineRevNo = objMIP004.LineRevNo,
                                MIPRevNo = objMIP004.MIPRevNo,
                                ActivityDescription = objMIP004.ActivityDescription,
                                RefDocument = objMIP004.RefDocument,
                                RefDocRevNo = objMIP004.RefDocRevNo,
                                PIA = objMIP004.PIA,
                                IAgencyLNT = objMIP004.IAgencyLNT,
                                IAgencyVVPT = objMIP004.IAgencyVVPT,
                                IAgencySRO = objMIP004.IAgencySRO,
                                RecordsDocuments = objMIP004.RecordsDocuments,
                                Observations = objMIP004.Observations,
                                CreatedBy = objClsLoginInfo.UserName,
                                CreatedOn = DateTime.Now,
                                OfferedBy = objClsLoginInfo.UserName,
                                OfferedOn = DateTime.Now
                            });
                            db.SaveChanges();

                            objMIP004.RequestNo = objMIP050.RequestNo;
                            objMIP004.IterationNo = objMIP050.IterationNo;
                            objMIP004.RequestSequenceNo = objMIP050.RequestNoSequence;
                            db.SaveChanges();

                            //#region Send Notification
                            //(new clsManager()).SendNotificationBULocationWise((clsImplementationEnum.UserRoleName.QI1.GetStringValue() + ',' + clsImplementationEnum.UserRoleName.QI2.GetStringValue() + ',' + clsImplementationEnum.UserRoleName.QI3.GetStringValue()), objMIP004.BU, objMIP004.Location,
                            //    "Traveler:  Request " + objMIP050.RequestNo + " of Activity: " + objMIP050.Activity + " & Project No: " + objMIP050.ProjectNo + " has been offered for Inspection", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/TVL/AttendInspection/Details/" + objMIP050.RequestId);
                            //#endregion

                            #endregion

                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "Data Offered Successfully";
                            objResponseMsg.Status = objMIP004.InspectionStatus;
                        }
                        else
                        {
                            objResponseMsg.Value = "Component: " + objMIP004.MIP003.ComponentNo + " is in '" + objMIP004.MIP003.Status + "' in MIP Header. Please approve component first.";
                        }
                    }
                    else
                    {
                        objResponseMsg.Value = "Data not available.";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg);
        }

        [HttpPost]
        public ActionResult OperationApplicable(int? lineid)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string clear = clsImplementationEnum.MIPInspectionStatus.Cleared.GetStringValue();
                string rto = clsImplementationEnum.MIPInspectionStatus.ReadyToOffer.GetStringValue();
                string NA = clsImplementationEnum.MIPInspectionStatus.Not_Applicatble.GetStringValue();
                MIP004 objMIP004 = db.MIP004.Where(x => x.LineId == lineid).FirstOrDefault();
                if (objMIP004 != null)
                {
                    if (objMIP004.InspectionStatus == rto)
                    {
                        objMIP004.InspectionStatus = clsImplementationEnum.MIPInspectionStatus.Not_Applicatble.GetStringValue();
                        objMIP004.InspectedBy = objClsLoginInfo.UserName;
                        objMIP004.InspectedOn = DateTime.Now;
                        db.SaveChanges();

                        MIP004 nextobjMIP004 = db.MIP004.Where(x => x.HeaderId == objMIP004.HeaderId && ((x.InspectionStatus != clear && x.InspectionStatus != NA && x.InspectionStatus == null) || x.InspectionStatus == rto)).OrderBy(x => x.OperationNo).FirstOrDefault();
                        if (nextobjMIP004 != null)
                        {
                            nextobjMIP004.InspectionStatus = rto;
                            nextobjMIP004.EditedBy = objClsLoginInfo.UserName;
                            nextobjMIP004.EditedOn = DateTime.Now;
                        }
                        objMIP004.EditedBy = objClsLoginInfo.UserName;
                        objMIP004.EditedOn = DateTime.Now;
                        db.SaveChanges();

                        #region Send Notification
                        //(new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), objMIP004.Project, objMIP004.BU, objMIP004.Location, "Stage: " + objMIP004.StageCode + " of Part: " + objMIP004.PartNo + " & Project No: " + objMIP004.QualityProject + " has been offered for Inspection", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/PartlistNDEInspection/ViewOfferDetails?RequestId=" + objTVL050.RequestId);
                        #endregion

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Operation has been updated successfully";
                        objResponseMsg.Status = objMIP004.InspectionStatus;
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Data not available.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }

            return Json(objResponseMsg);
        }

        [HttpPost]
        public JsonResult GetQualityProjectWiseProjectBUDetail(string qualityProject)
        {
            QualityProjectsDetails objProjectsDetails = new QualityProjectsDetails();
            var project = db.QMS010.Where(i => i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase)).Select(i => i.Project).FirstOrDefault();

            objProjectsDetails.ProjectCode = project;
            objProjectsDetails.projectDescription = Manager.GetProjectAndDescription(project);

            var ProjectWiseBULocation = Manager.getProjectWiseBULocation(project);

            objProjectsDetails.BUCode = ProjectWiseBULocation.QCPBU.Split('-')[0].Trim();
            objProjectsDetails.BUDescription = ProjectWiseBULocation.QCPBU;

            objProjectsDetails.LocationCode = objClsLoginInfo.Location;
            objProjectsDetails.LocationDescription = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objClsLoginInfo.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            return Json(objProjectsDetails, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Group Offer
        public ActionResult GroupOfferPartial()
        {
            //var whereCondition = " m4.inspectionstatus  in('" + clsImplementationEnum.MIPInspectionStatus.ReadyToOffer.GetStringValue() + "')";
            //whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "m4.BU", "m4.Location");
            //ViewBag.lstPandingQualityProject = db.SP_MIP_GET_OFFER_HEADER_DATA(0, int.MaxValue, "", whereCondition).Select(s => s.QualityProject).Distinct().ToList();
            var BUs = Manager.GetUserwiseBU(objClsLoginInfo.UserName);
            var Locations = Manager.GetUserwiseLocation(objClsLoginInfo.UserName);
            var rtoStatus = clsImplementationEnum.MIPInspectionStatus.ReadyToOffer.GetStringValue();
            ViewBag.lstPandingQualityProject = db.MIP004.Where(w => w.InspectionStatus == rtoStatus && BUs.Contains(w.BU) && Locations.Contains(w.Location)).Select(s => s.QualityProject).Distinct().ToList();
            return PartialView("_GroupOfferPartial");
        }

        [HttpPost]
        public JsonResult GetQualityProjectWiseProjectBUDetailWithComponent(string qualityProject)
        {
            QualityProjectsDetails objProjectsDetails = new QualityProjectsDetails();
            var objproject = db.QMS010.Where(i => i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
            var project = objproject.Project;
            objProjectsDetails.ProjectCode = project;
            objProjectsDetails.projectDescription = Manager.GetProjectAndDescription(project);

            var ProjectWiseBULocation = Manager.getProjectWiseBULocation(project);

            objProjectsDetails.BUCode = ProjectWiseBULocation.QCPBU.Split('-')[0].Trim();
            objProjectsDetails.BUDescription = ProjectWiseBULocation.QCPBU;

            objProjectsDetails.LocationCode = objClsLoginInfo.Location;
            objProjectsDetails.LocationDescription = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objClsLoginInfo.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            var lineStatus = clsImplementationEnum.MIPInspectionStatus.ReadyToOffer.GetStringValue();
            var lstLoc = Manager.GetUserwiseLocation(objClsLoginInfo.UserName);
            var lstMIP004 = db.MIP004.Where(w => w.QualityProject == qualityProject && w.BU == objProjectsDetails.BUCode && lstLoc.Contains(w.Location)).ToList();
            var ComponentNo = lstMIP004.Where(w => w.InspectionStatus == lineStatus).Select(s => s.ComponentNo).OrderBy(o => o).Distinct().ToList();
            var approved = clsImplementationEnum.MIPLineStatus.Approved.GetStringValue();
            var lstAnnexure = (from lst in lstMIP004
                               join hdr in db.MIP003 on lst.HeaderId equals hdr.HeaderId
                               where lst.Status == approved
                               select hdr).Distinct().ToList();

            var Annexure = lstAnnexure.Select(x => new { Code = x.DocNo, Description = Manager.GetCategoryOnlyDescription(x.DocNo, objProjectsDetails.BUCode, objproject.Location, "MIP_DocNo") }).Distinct().ToList();
            var MIPType = lstAnnexure.Select(x => new { Code = x.MIPType, Description = Manager.GetCategoryOnlyDescription(x.MIPType, objProjectsDetails.BUCode, objproject.Location, "MIP_Type") }).Distinct().ToList();

            return Json(new { ProjectsDetails = objProjectsDetails, ComponentNo = ComponentNo, Annexure = Annexure, MIPType = MIPType }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetAnnexureDetail(string qualityProject, string bu, string loc, string componentNoFrom, string componentNoTo)
        {

            var lstLoc = Manager.GetUserwiseLocation(objClsLoginInfo.UserName);
            var lineStatus = clsImplementationEnum.MIPInspectionStatus.ReadyToOffer.GetStringValue();
            var lstComponentNo = db.MIP004.Where(w => w.QualityProject == qualityProject && w.BU.Trim() == bu.Trim() && lstLoc.Contains(w.Location) && w.InspectionStatus == lineStatus).OrderBy(o => o.ComponentNo).ToList();
            bool started = false;

            var lstAnnexure = new List<DropdownModel>();
            foreach (var item in lstComponentNo)
            {
                if (item.ComponentNo == componentNoFrom || item.ComponentNo == componentNoTo)
                    started = true;
                if (started == false)
                    continue;
                lstAnnexure.Add(new DropdownModel { Code = item.MIP003.DocNo, Description = Manager.GetCategoryOnlyDescription(item.MIP003.DocNo, bu, loc, "MIP_DocNo") });
                if (item.ComponentNo == componentNoTo)
                    break;
            }
            var Annexure = lstAnnexure.Distinct().OrderBy(o => o.Code).ToList();
            return Json(Annexure, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetOperationNoWiseProjectBUDetail(string qualityProject, string bu, string componentNoFrom, string componentNoTo, string annexure)
        {
            var lstLoc = Manager.GetUserwiseLocation(objClsLoginInfo.UserName);
            var lineStatus = clsImplementationEnum.MIPInspectionStatus.ReadyToOffer.GetStringValue();
            var lstComponentNo = db.MIP004.Where(w => w.QualityProject == qualityProject && w.BU == bu && lstLoc.Contains(w.Location) && w.InspectionStatus == lineStatus && w.MIP003.DocNo == annexure).OrderBy(o => o.ComponentNo).ToList();
            bool started = false;
            var OperationNo = new List<decimal>();
            foreach (var item in lstComponentNo)
            {
                if (item.ComponentNo == componentNoFrom || item.ComponentNo == componentNoTo)
                    started = true;
                if (started == false)
                    continue;
                OperationNo.Add(item.OperationNo);
                if (item.ComponentNo == componentNoTo)
                    break;
            }
            var lstOperationNo = OperationNo.Distinct().OrderBy(o => o).Select(s => s.ToString("G29")).ToList();
            return Json(lstOperationNo, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GroupOffer(string lineIds) //qualityProject, string BU, string location, string componentNoFrom, string componentNoTo, decimal operationNo)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string rto = clsImplementationEnum.MIPInspectionStatus.ReadyToOffer.GetStringValue();
                string Approved = clsImplementationEnum.MIPHeaderStatus.Approved.GetStringValue();
                string rejected = clsImplementationEnum.MIPInspectionStatus.Rejected.GetStringValue();
                string returned = clsImplementationEnum.MIPInspectionStatus.Returned.GetStringValue();
                var LineIds = lineIds.Split(',').Select(s => int.Parse(s));// new List<int>();
                var NotApprovedComponentNos = new List<string>();

                foreach (var lineid in LineIds)
                {
                    var objMIP004 = db.MIP004.FirstOrDefault(x => x.LineId == lineid);
                    if (objMIP004.MIP003.Status == Approved)
                    {
                        #region Offer Entry

                        objMIP004.InspectionStatus = clsImplementationEnum.MIPInspectionStatus.UnderInspection.GetStringValue();
                        objMIP004.EditedBy = objClsLoginInfo.UserName;
                        objMIP004.EditedOn = DateTime.Now;

                        long RequestNo = 0;
                        int ReqSeqNo = 1;
                        ObjectParameter outParm = new ObjectParameter("result", typeof(long));
                        var SPResult = db.SP_MIP_GET_NEXT_REQUEST_NO(objMIP004.BU, objMIP004.Location, outParm);
                        RequestNo = (long)outParm.Value;

                        MIP050 objMIP050 = db.MIP050.Add(new MIP050
                        {
                            QualityProject = objMIP004.QualityProject,
                            Project = objMIP004.MIP003.Project,
                            BU = objMIP004.MIP003.BU,
                            Location = objMIP004.MIP003.Location,
                            DocNo = objMIP004.MIP003.DocNo,
                            MIPType = objMIP004.MIP003.MIPType,
                            RevNo = objMIP004.MIP003.RevNo,
                            ComponentNo = objMIP004.MIP003.ComponentNo,
                            DrawingNo = objMIP004.MIP003.DrawingNo,
                            RawMaterialGrade = objMIP004.MIP003.RawMaterialGrade,
                            PlateNo = objMIP004.MIP003.PlateNo,
                            IterationNo = 1,
                            RequestNo = RequestNo,
                            RequestNoSequence = ReqSeqNo,
                            OperationNo = objMIP004.OperationNo,
                            LineRevNo = objMIP004.LineRevNo,
                            MIPRevNo = objMIP004.MIPRevNo,
                            ActivityDescription = objMIP004.ActivityDescription,
                            RefDocument = objMIP004.RefDocument,
                            RefDocRevNo = objMIP004.RefDocRevNo,
                            PIA = objMIP004.PIA,
                            IAgencyLNT = objMIP004.IAgencyLNT,
                            IAgencyVVPT = objMIP004.IAgencyVVPT,
                            IAgencySRO = objMIP004.IAgencySRO,
                            RecordsDocuments = objMIP004.RecordsDocuments,
                            Observations = objMIP004.Observations,
                            CreatedBy = objClsLoginInfo.UserName,
                            CreatedOn = DateTime.Now,
                            OfferedBy = objClsLoginInfo.UserName,
                            OfferedOn = DateTime.Now
                        });
                        db.SaveChanges();

                        objMIP004.RequestNo = objMIP050.RequestNo;
                        objMIP004.IterationNo = objMIP050.IterationNo;
                        objMIP004.RequestSequenceNo = objMIP050.RequestNoSequence;
                        db.SaveChanges();

                        //#region Send Notification
                        //(new clsManager()).SendNotificationBULocationWise((clsImplementationEnum.UserRoleName.QI1.GetStringValue() + ',' + clsImplementationEnum.UserRoleName.QI2.GetStringValue() + ',' + clsImplementationEnum.UserRoleName.QI3.GetStringValue()), objMIP004.BU, objMIP004.Location,
                        //    "Traveler:  Request " + objMIP050.RequestNo + " of Activity: " + objMIP050.Activity + " & Project No: " + objMIP050.ProjectNo + " has been offered for Inspection", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/TVL/AttendInspection/Details/" + objMIP050.RequestId);
                        //#endregion

                        #endregion

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Data Offered Successfully";
                        objResponseMsg.Status = objMIP004.InspectionStatus;
                    }
                    else
                    {
                        NotApprovedComponentNos.Add(objMIP004.MIP003.ComponentNo);
                    }
                }
                if (NotApprovedComponentNos.Count() > 0)
                {
                    objResponseMsg.Value = "Components: " + String.Join(",", NotApprovedComponentNos) + " is not 'Approved' in MIP Header. Please approve component first.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg);
        }

        public JsonResult loadOperationListGroupOfferDataTable(JQueryDataTableParamModel param, string componentNoFrom, string componentNoTo, decimal operationNo)
        {
            try
            {
                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "m3.BU", "m3.Location");
                var lineStatus = clsImplementationEnum.MIPInspectionStatus.ReadyToOffer.GetStringValue();
                whereCondition += " and m4.InspectionStatus= '" + lineStatus + "'";
                var lstLoc = Manager.GetUserwiseLocation(objClsLoginInfo.UserName);

                var objMIP004 = db.MIP004.Where(w => w.QualityProject == param.Project && w.BU == param.BU && lstLoc.Contains(w.Location) && w.OperationNo == operationNo && w.InspectionStatus == lineStatus).OrderBy(o => o.ComponentNo).ToList();
                bool started = false;
                var lstComponentNo = new List<string>();
                foreach (var item in objMIP004)
                {
                    if (item.ComponentNo == componentNoFrom || item.ComponentNo == componentNoTo)
                        started = true;
                    if (started == false)
                        continue;
                    lstComponentNo.Add(item.ComponentNo);
                    if (item.ComponentNo == componentNoTo)
                        break;
                }

                whereCondition += " and m4.QualityProject='" + param.Project + "' and m4.BU='" + param.BU + "'"
                                 + " and M4.ComponentNo in ('" + String.Join("','", lstComponentNo) + "') "
                                 + " and M3.DocNo='" + param.FilterDocNo + "' and m4.OperationNo= " + operationNo;

                string[] columnName = { "M4.ComponentNo", "M4.ActivityDescription", "M4.RefDocument", "M4.RefDocRevNo", "M4.PIA", "M4.IAgencyLNT", "M4.IAgencyVVPT", "M4.IAgencySRO", "M4.RecordsDocuments", "M4.Observations", "M4.LineRevNo", "M4.MIPRevNo" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);

                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstHeader = db.SP_MIP_HEADER_GET_LINES_DATA(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                var CheckedIds = "";
                if (param.bCheckedAll && lstHeader.Any())
                    CheckedIds = String.Join(",", db.SP_MIP_HEADER_GET_LINES_DATA(1, 0, strSortOrder, whereCondition).Select(s => s.LineId).ToList());

                var res = from uc in lstHeader
                          select new[] {
                               Convert.ToString(uc.LineId),
                               Convert.ToString(uc.HeaderId),
                               uc.ComponentNo,
                               uc.ActivityDescription,
                               uc.RefDocument,
                               uc.RefDocRevNo,
                               Manager.GetCategoryOnlyDescription(uc.PIA,uc.BU.Split('-')[0], uc.Location.Split('-')[0], "PIA" ),
                               Manager.GetCategoryOnlyDescription(uc.IAgencyLNT,uc.BU.Split('-')[0], uc.Location.Split('-')[0], "MIP Inspection Agency L&T" ),
                               Manager.GetCategoryOnlyDescription(uc.IAgencyVVPT,uc.BU.Split('-')[0], uc.Location.Split('-')[0], "MIP Inspection Agency VVPT"),
                               Manager.GetCategoryOnlyDescription(uc.IAgencySRO,uc.BU.Split('-')[0], uc.Location.Split('-')[0], "MIP Inspection Agency SRO"),
                               uc.RecordsDocuments,
                               uc.Observations,
                               "R"+uc.LineRevNo,
                               "R"+uc.MIPRevNo,
                               Helper.HTMLActionString(Convert.ToInt32(uc.HeaderId), "View", "View Details", "fa fa-eye", "", "/MIP/OfferOperation/Details/"+uc.LineId )
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition,
                    checkedIds = CheckedIds
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Timeline
        public ActionResult ShowHistoryTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.TimelineTitle = "MIP Timeline";

            if (LineId > 0)
            {
                model.Title = "MIP Lines";
                MIP002_Log objMIP002 = db.MIP002_Log.Where(x => x.Id == LineId).FirstOrDefault();
                model.CreatedBy = objMIP002.CreatedBy != null ? Manager.GetUserNameFromPsNo(objMIP002.CreatedBy) : null;
                model.CreatedOn = objMIP002.CreatedOn;
                model.EditedBy = objMIP002.EditedBy != null ? Manager.GetUserNameFromPsNo(objMIP002.EditedBy) : null;
                model.EditedOn = objMIP002.EditedOn;
            }
            else
            {
                model.Title = "MIP Header";
                MIP001_Log objMIP001 = db.MIP001_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objMIP001.CreatedBy != null ? Manager.GetUserNameFromPsNo(objMIP001.CreatedBy) : null;
                model.CreatedOn = objMIP001.CreatedOn;
                model.EditedBy = objMIP001.EditedBy != null ? Manager.GetUserNameFromPsNo(objMIP001.EditedBy) : null;
                model.EditedOn = objMIP001.EditedOn;
                model.SubmittedBy = objMIP001.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objMIP001.SubmittedBy) : null;
                model.SubmittedOn = objMIP001.SubmittedOn;
                if (objMIP001.PMGReturnedBy != null)
                {
                    model.ReturnedBy = Manager.GetUserNameFromPsNo(objMIP001.PMGReturnedBy);
                    model.ReturnedOn = objMIP001.PMGReturnedOn;
                }
                else
                {
                    model.ReturnedBy = null;
                    model.ReturnedOn = null;
                }
                model.PMGReleaseBy = objMIP001.PMGApprovedBy != null ? Manager.GetUserNameFromPsNo(objMIP001.PMGApprovedBy) : null;
                model.PMGReleaseOn = objMIP001.PMGApprovedOn;

            }
            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        #endregion

        #region Common Methods

        public UserRoleAccessDetails GetUserAccessRights()
        {
            UserRoleAccessDetails objUserRoleAccessDetails = new UserRoleAccessDetails();

            try
            {
                var role = Manager.GetUserRoles(objClsLoginInfo.UserName);
                if (role.Where(i => i.Role.Equals(clsImplementationEnum.UserRoleName.PMG2.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.PMG2.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Approver.GetStringValue();
                }
                else if (role.Where(i => i.Role.Equals(clsImplementationEnum.UserRoleName.PMG3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.PMG3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Initiator.GetStringValue();
                }
                else
                {
                    objUserRoleAccessDetails.UserRole = "";
                    objUserRoleAccessDetails.UserDesignation = "";
                }
            }
            catch
            {
            }
            return objUserRoleAccessDetails;
        }

        public class ResponceMsgWithMultiValue : clsHelper.ResponseMsg
        {
            public string pendingMIP { get; set; }
            public string submittedMIP { get; set; }
            public bool isIdenticalProjectEditable { get; set; }
        }
        #endregion

        #region ExcelHelper
        public JsonResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "", int Headerid = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.MAINTAININDEX.GetStringValue())
                {
                    var lst = db.SP_MIP_GET_OFFER_HEADER_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from a in lst
                                  select new
                                  {
                                      QualityProject = a.QualityProject,
                                      Project = a.Project,
                                      BU = a.BU,
                                      Location = a.Location,
                                      ComponentNo = a.ComponentNo,
                                      MIP_DocNo = Manager.GetCategoryOnlyDescription(a.DocNo, a.BU.Split('-')[0], a.Location.Split('-')[0], "MIP_DocNo"),
                                      MIP_Type = Manager.GetCategoryOnlyDescription(a.MIPType, a.BU.Split('-')[0], a.Location.Split('-')[0], "MIP_Type"),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_MIP_GET_HEADER_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      DocNo = Manager.GetCategoryOnlyDescription(uc.DocNo, uc.BU, uc.Location, "MIP_DocNo"),
                                      MIPType = Manager.GetCategoryOnlyDescription(uc.MIPType, uc.BU, uc.Location, "MIP_Type"),
                                      PMGReturnRemarks = uc.PMGReturnRemarks,
                                      RevNo = "R" + uc.RevNo,
                                      Status = uc.Status,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_MIP_GET_LINES_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    MIP001 objMIP001 = db.MIP001.Where(c => c.HeaderId == Headerid).FirstOrDefault();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var lstPIA = Manager.GetSubCatagories("PIA", objMIP001.BU, objMIP001.Location);
                    var lstIAgencyLNT = Manager.GetSubCatagories("MIP Inspection Agency L&T", objMIP001.BU, objMIP001.Location);
                    var lstIAgencyVVPT = Manager.GetSubCatagories("MIP Inspection Agency VVPT", objMIP001.BU, objMIP001.Location);
                    var lstIAgencySRO = Manager.GetSubCatagories("MIP Inspection Agency SRO", objMIP001.BU, objMIP001.Location);

                    var newlst = (from uc in lst
                                  select new
                                  {
                                      OperationNo = uc.OperationNo,
                                      ActivityDescription = uc.ActivityDescription,
                                      RefDocument = uc.RefDocument,
                                      RefDocRevNo = uc.RefDocRevNo,
                                      PIA = lstPIA.Where(w => w.Code == uc.PIA).Select(s => s.Description).FirstOrDefault(),
                                      IAgencyLNT = lstIAgencyLNT.Where(w => w.Code == uc.IAgencyLNT).Select(s => s.Description).FirstOrDefault(),
                                      IAgencyVVPT = lstIAgencyVVPT.Where(w => w.Code == uc.IAgencyVVPT).Select(s => s.Description).FirstOrDefault(),
                                      IAgencySRO = lstIAgencySRO.Where(w => w.Code == uc.IAgencySRO).Select(s => s.Description).FirstOrDefault(),
                                      LineRevNo = "R" + uc.LineRevNo,
                                      MIPRevNo = "R" + uc.MIPRevNo,
                                      Remarks = uc.Remarks,
                                      Status = uc.Status
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region offer & clear outside component
        [SessionExpireFilter]//, UserPermissions, AllowAnonymous]
        public ActionResult OfferClearOutsideComponent()
        {
            var BUs = Manager.GetUserwiseBU(objClsLoginInfo.UserName);
            var Locations = Manager.GetUserwiseLocation(objClsLoginInfo.UserName);
            var status = clsImplementationEnum.MIPLineStatus.Approved.GetStringValue();

            ViewBag.lstPandingQualityProject = db.MIP004.Where(w => w.Status == status && BUs.Contains(w.BU) && Locations.Contains(w.Location)).Select(s => s.QualityProject).Distinct().ToList();
            ViewBag.Title = "Offer and Clear Outside Component";
            return View();
        }

        [HttpPost]
        public JsonResult GetComponentNoForOfferClear(string term, string qproject, string bu, string location, string annexure)
        {
            var approved = clsImplementationEnum.MIPLineStatus.Approved.GetStringValue();
            var lineStatus = clsImplementationEnum.MIPInspectionStatus.ReadyToOffer.GetStringValue();
            decimal opn = Convert.ToDecimal(1.07);
            var lstLoc = Manager.GetUserwiseLocation(objClsLoginInfo.UserName);
            //var ComponentNo = db.MIP004.Where(w => w.QualityProject == qualityProject && w.BU == objProjectsDetails.BUCode && lstLoc.Contains(w.Location) && w.InspectionStatus == lineStatus).Select(s => s.ComponentNo).OrderBy(o => o).Distinct().ToList();
            var ComponentNo = db.MIP004.Where(w => w.QualityProject == qproject && w.BU == bu
                                                    && (w.InspectionStatus == lineStatus || w.InspectionStatus == "" || w.InspectionStatus == null)
                                                    && w.Status == approved
                                                    && lstLoc.Contains(w.Location)
                                                    && (w.OperationNo <= opn)
                                                    && (term == "" || w.ComponentNo.Contains(term))
                                                    && w.ComponentNo != null
                                                    && w.MIP003.DocNo == annexure //&& w.MIP003.MIPType == miptype
                                                    ).OrderBy(o => o.ComponentNo).Select(s => new { Text = s.ComponentNo, Value = s.ComponentNo }).Distinct().ToList();

            return Json(ComponentNo, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetOperationNoForOfferClear(string term, string componentNoFrom, string componentNoTo)
        {
            double opn = 1.07;
            var approved = clsImplementationEnum.MIPLineStatus.Approved.GetStringValue();
            var lstLoc = Manager.GetUserwiseLocation(objClsLoginInfo.UserName);
            var lineStatus = clsImplementationEnum.MIPInspectionStatus.ReadyToOffer.GetStringValue();
            var lstComponentNo = db.MIP004.Where(w => (w.InspectionStatus == lineStatus || w.InspectionStatus == "" || w.InspectionStatus == null)
                                                    && w.Status == approved
                                                    && lstLoc.Contains(w.Location)
                                                    && (w.OperationNo <= Convert.ToDecimal(opn))).OrderBy(o => o.ComponentNo).Distinct().ToList();
            bool started = false;
            var OperationNo = new List<decimal>();
            foreach (var item in lstComponentNo)
            {
                if (item.ComponentNo == componentNoFrom)
                    started = true;
                if (started == false)
                    continue;
                OperationNo.Add(item.OperationNo);
                if (item.ComponentNo == componentNoTo)
                    break;
            }
            var lstOperationNo = OperationNo.Distinct().OrderBy(o => o).Select(s => s.ToString("G29")).ToList();
            return Json(lstOperationNo, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GroupOfferClear(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string componentnofrom = fc["txtFromComponent"];
                string componentnoto = fc["txtToComponent"];
                decimal operationnofrom = Convert.ToDecimal(fc["txtFromOperation"]);
                decimal operationnoto = Convert.ToDecimal(fc["txtToOperation"]);

                string qproject = fc["QualityProject"];
                string project = fc["Project"];
                string bu = fc["BU"];
                string location = fc["Location"];
                string annexure = fc["Annexure"];

                string lineStatus = clsImplementationEnum.MIPInspectionStatus.ReadyToOffer.GetStringValue();
                string approved = clsImplementationEnum.MIPHeaderStatus.Approved.GetStringValue();
                string clear = clsImplementationEnum.MIPInspectionStatus.Cleared.GetStringValue();
                string NA = clsImplementationEnum.MIPInspectionStatus.Not_Applicatble.GetStringValue();
                string underinspection = clsImplementationEnum.MIPInspectionStatus.UnderInspection.GetStringValue();

                decimal opn = Convert.ToDecimal(1.07);
                var ComponentNolist = new List<string>();
                var OfferOperationList = new List<OperationList>();

                var lstLoc = Manager.GetUserwiseLocation(objClsLoginInfo.UserName);
                List<MIP004> lstMIP004 = db.MIP004.Where(w => w.QualityProject == qproject && w.BU == bu && lstLoc.Contains(w.Location)
                                                         && w.Status == approved
                                                         && w.ComponentNo != null
                                                         && w.MIP003.DocNo == annexure
                                                         && (w.OperationNo <= opn)).OrderBy(o => o.ComponentNo)
                                                         .Distinct().ToList();

                var lstComponents = lstMIP004.Where(w => w.InspectionStatus == lineStatus || w.InspectionStatus == "" || w.InspectionStatus == null).Select(x => x.ComponentNo).OrderBy(o => o).Distinct().ToList();

                bool started = false;
                foreach (var item in lstComponents)
                {
                    if (item.ToLower() == componentnofrom.ToLower())
                        started = true;
                    if (started == false)
                        continue;
                    ComponentNolist.Add(item);
                    if (item.ToLower() == componentnoto.ToLower())
                        break;
                }

                string Components = string.Join(",", ComponentNolist.ToList());
                string Loc = string.Join(",", lstLoc.ToList());

                var result = db.SP_MIP_OFFER_AND_CLEAR_OUTSIDE_COMPONENT(qproject, Components, Loc, operationnofrom, operationnoto, objClsLoginInfo.UserName, annexure, "").FirstOrDefault();

                if (result != null ? !string.IsNullOrWhiteSpace(result.HeaderNotApproved) : false)
                {
                    var res = result.HeaderNotApproved.Split(',').ToArray();
                    objResponseMsg.dataValue = "Components: " + string.Join(",", res.Distinct().ToList()) + "  is not 'Approved' in MIP Header. Please approve component first.";
                }

                if (result != null ? !string.IsNullOrWhiteSpace(result.skippedComponent) : false)
                {
                    var res = result.skippedComponent.Split(',').ToArray();
                    objResponseMsg.NotApplicableForParallel = "Components: " + string.Join(",", res.Distinct().ToList()) + "  is/are not 'Cleared'. Please clear operation first.";
                }

                if (result != null ? !string.IsNullOrWhiteSpace(result.successComponent) : false)
                {
                    var res = result.successComponent.Split(',').ToArray();
                    objResponseMsg.Value = "Components: " + string.Join(",", res.Distinct().ToList()) + " Offered & Cleared successfully.";
                }
                else
                {
                    objResponseMsg.Value = "Operations: " + result.TotalOfferedOperation + " Offered & Cleared successfully.";
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg);
        }

        [HttpPost]
        public JsonResult OperationGroupOfferClearbyCode_Not_in_used(FormCollection fc) //qualityProject, string BU, string location, string componentNoFrom, string componentNoTo, decimal operationNo)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string componentnofrom = fc["txtFromComponent"];
                string componentnoto = fc["txtToComponent"];
                decimal operationnofrom = Convert.ToDecimal(fc["txtFromOperation"]);
                decimal operationnoto = Convert.ToDecimal(fc["txtToOperation"]);

                string qproject = fc["QualityProject"];
                string project = fc["Project"];
                string bu = fc["BU"];
                string location = fc["Location"];

                string lineStatus = clsImplementationEnum.MIPInspectionStatus.ReadyToOffer.GetStringValue();
                string approved = clsImplementationEnum.MIPHeaderStatus.Approved.GetStringValue();
                string clear = clsImplementationEnum.MIPInspectionStatus.Cleared.GetStringValue();
                string NA = clsImplementationEnum.MIPInspectionStatus.Not_Applicatble.GetStringValue();
                string underinspection = clsImplementationEnum.MIPInspectionStatus.UnderInspection.GetStringValue();

                decimal opn = Convert.ToDecimal(1.07);
                var ComponentNolist = new List<string>();
                var OfferOperationList = new List<OperationList>();

                var lstLoc = Manager.GetUserwiseLocation(objClsLoginInfo.UserName);
                List<MIP004> lstMIP004 = db.MIP004.Where(w => w.QualityProject == qproject && w.BU == bu && lstLoc.Contains(w.Location)
                                                         && w.Status == approved
                                                         && w.ComponentNo != null
                                                         && (w.OperationNo <= opn)).OrderBy(o => o.ComponentNo)
                                                         .Distinct().ToList();

                var lstComponents = lstMIP004.Where(w => w.InspectionStatus == lineStatus || w.InspectionStatus == "" || w.InspectionStatus == null).Select(x => x.ComponentNo).OrderBy(o => o).Distinct().ToList();

                bool started = false;
                foreach (var item in lstComponents)
                {
                    if (item.ToLower() == componentnofrom.ToLower())
                        started = true;
                    if (started == false)
                        continue;
                    ComponentNolist.Add(item);
                    if (item.ToLower() == componentnoto.ToLower())
                        break;
                }

                var lstOperationNobyComponent = lstMIP004.Where(x => ComponentNolist.Distinct().Contains(x.ComponentNo))
                                              .OrderBy(o => o.OperationNo)
                                              .ToList();
                bool startedop = false;
                foreach (var item in lstOperationNobyComponent)
                {
                    if (Convert.ToDecimal(item.OperationNo) == operationnofrom && item.ComponentNo == componentnofrom)
                        startedop = true;
                    if (startedop == false)
                        continue;
                    if ((item.InspectionStatus == lineStatus || item.InspectionStatus == "" || item.InspectionStatus == null) && item.Status == approved)
                    {
                        OfferOperationList.Add(new OperationList { operation = Convert.ToDecimal(item.OperationNo), lineid = item.LineId, component = item.ComponentNo });
                    }
                    if (Convert.ToDecimal(item.OperationNo) == operationnoto)
                        if (item.ComponentNo == componentnoto)
                        {
                            break;
                        }
                }

                var Offered = new List<string>();
                var NotApprovedComponentNos = new List<string>();
                var NotClearpreviousOperation = new List<string>();
                string skipcomponent = string.Empty;
                foreach (var offeroperation in OfferOperationList)
                {
                    var objMIP004 = db.MIP004.FirstOrDefault(x => x.LineId == offeroperation.lineid);
                    var LineIds = OfferOperationList.Select(x => x.lineid);
                    var operation = OfferOperationList.Select(x => x.operation);

                    if (db.MIP004.Any(x => x.HeaderId == objMIP004.HeaderId
                            && !LineIds.Contains(x.LineId)
                            && (x.InspectionStatus == underinspection || x.InspectionStatus == lineStatus)))
                    {
                        skipcomponent = objMIP004.ComponentNo;
                        NotClearpreviousOperation.Add(objMIP004.ComponentNo);
                    }
                    else
                    {
                        if (objMIP004.MIP003.Status == approved
                            && (objMIP004.InspectionStatus == lineStatus || string.IsNullOrWhiteSpace(objMIP004.InspectionStatus))
                            && objMIP004.Status == approved)
                        {
                            #region Offer & Clear Entry

                            objMIP004.InspectionStatus = clsImplementationEnum.MIPInspectionStatus.Cleared.GetStringValue();
                            objMIP004.EditedBy = objClsLoginInfo.UserName;
                            objMIP004.EditedOn = DateTime.Now;
                            objMIP004.InspectedBy = objClsLoginInfo.UserName;
                            objMIP004.InspectedOn = DateTime.Now;
                            db.SaveChanges();

                            MIP004 nextobjMIP004 = db.MIP004.Where(x => x.HeaderId == objMIP004.HeaderId && (((x.InspectionStatus != clear && x.InspectionStatus != NA) && x.InspectionStatus == null) || x.InspectionStatus == lineStatus)).OrderBy(x => x.OperationNo).FirstOrDefault();
                            if (nextobjMIP004 != null)
                            {
                                nextobjMIP004.InspectionStatus = clsImplementationEnum.MIPInspectionStatus.ReadyToOffer.GetStringValue();
                                nextobjMIP004.EditedBy = objClsLoginInfo.UserName;
                                nextobjMIP004.EditedOn = DateTime.Now;
                            }

                            db.SaveChanges();

                            long RequestNo = 0;
                            int ReqSeqNo = 1;
                            ObjectParameter outParm = new ObjectParameter("result", typeof(long));
                            var SPResult = db.SP_MIP_GET_NEXT_REQUEST_NO(objMIP004.BU, objMIP004.Location, outParm);
                            RequestNo = (long)outParm.Value;

                            MIP050 objMIP050 = db.MIP050.Add(new MIP050
                            {
                                QualityProject = objMIP004.QualityProject,
                                Project = objMIP004.MIP003.Project,
                                BU = objMIP004.MIP003.BU,
                                Location = objMIP004.MIP003.Location,
                                DocNo = objMIP004.MIP003.DocNo,
                                MIPType = objMIP004.MIP003.MIPType,
                                RevNo = objMIP004.MIP003.RevNo,
                                ComponentNo = objMIP004.MIP003.ComponentNo,
                                DrawingNo = objMIP004.MIP003.DrawingNo,
                                RawMaterialGrade = objMIP004.MIP003.RawMaterialGrade,
                                PlateNo = objMIP004.MIP003.PlateNo,
                                IterationNo = 1,
                                RequestNo = RequestNo,
                                RequestNoSequence = ReqSeqNo,
                                OperationNo = objMIP004.OperationNo,
                                LineRevNo = objMIP004.LineRevNo,
                                MIPRevNo = objMIP004.MIPRevNo,
                                ActivityDescription = objMIP004.ActivityDescription,
                                RefDocument = objMIP004.RefDocument,
                                RefDocRevNo = objMIP004.RefDocRevNo,
                                PIA = objMIP004.PIA,
                                IAgencyLNT = objMIP004.IAgencyLNT,
                                IAgencyVVPT = objMIP004.IAgencyVVPT,
                                IAgencySRO = objMIP004.IAgencySRO,
                                RecordsDocuments = objMIP004.RecordsDocuments,
                                Observations = objMIP004.Observations,
                                CreatedBy = objClsLoginInfo.UserName,
                                CreatedOn = DateTime.Now,
                                OfferedBy = objClsLoginInfo.UserName,
                                OfferedOn = DateTime.Now,
                                TestResult = clear,
                                InspectedBy = objClsLoginInfo.UserName,
                                InspectedOn = DateTime.Now
                            });
                            db.SaveChanges();

                            objMIP004.RequestNo = objMIP050.RequestNo;
                            objMIP004.IterationNo = objMIP050.IterationNo;
                            objMIP004.RequestSequenceNo = objMIP050.RequestNoSequence;
                            db.SaveChanges();

                            //#region Send Notification
                            //(new clsManager()).SendNotificationBULocationWise((clsImplementationEnum.UserRoleName.QI1.GetStringValue() + ',' + clsImplementationEnum.UserRoleName.QI2.GetStringValue() + ',' + clsImplementationEnum.UserRoleName.QI3.GetStringValue()), objMIP004.BU, objMIP004.Location,
                            //    "Traveler:  Request " + objMIP050.RequestNo + " of Activity: " + objMIP050.Activity + " & Project No: " + objMIP050.ProjectNo + " has been offered for Inspection", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/TVL/AttendInspection/Details/" + objMIP050.RequestId);
                            //#endregion

                            #endregion
                            Offered.Add(objMIP004.ComponentNo);
                            objResponseMsg.Status = objMIP004.InspectionStatus;
                        }
                        else
                        {
                            NotApprovedComponentNos.Add(objMIP004.MIP003.ComponentNo);
                        }
                    }
                }

                if (NotApprovedComponentNos.Count() > 0)
                {
                    objResponseMsg.dataValue = "Components: " + String.Join(",", NotApprovedComponentNos.Distinct()) + " is not 'Approved' in MIP Header. Please approve component first.";
                }

                if (NotClearpreviousOperation.Count() > 0)
                {
                    objResponseMsg.NotApplicableForParallel = "Components: " + String.Join(",", NotClearpreviousOperation.Distinct()) + " is not 'Cleared'. Please clear operation first.";
                }

                if (Offered.Count() > 0)
                {
                    objResponseMsg.Value = "Components: " + String.Join(",", Offered.Distinct()) + " Offered & Cleared successfully.";
                }
                else
                {
                    objResponseMsg.Value = "Components not found";
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg);
        }

        public class OperationList
        {
            public string component;
            public decimal operation;
            public int lineid;
        }
        #endregion

        #region Import for bulk offer
        [SessionExpireFilter]
        public ActionResult ImportExcel(HttpPostedFileBase upload)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            if (upload != null)
            {
                if (Path.GetExtension(upload.FileName).ToLower() == ".xlsx" || Path.GetExtension(upload.FileName).ToLower() == ".xls")
                {
                    ExcelPackage package = new ExcelPackage(upload.InputStream);
                    ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();

                    DataTable dtHeaderExcel = new DataTable();
                    List<CategoryData> errors = new List<CategoryData>();
                    string Approved = clsImplementationEnum.MIPHeaderStatus.Approved.GetStringValue();
                    string Draft = clsImplementationEnum.MIPHeaderStatus.Draft.GetStringValue();
                    string Returned = clsImplementationEnum.MIPHeaderStatus.Returned.GetStringValue();
                    bool isError;

                    DataSet ds = ToChechValidation(package, out isError);
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Record Found!";
                    }
                    else if (!isError)
                    {
                        try
                        {
                            string rto = clsImplementationEnum.MIPInspectionStatus.ReadyToOffer.GetStringValue();
                            string rejected = clsImplementationEnum.MIPInspectionStatus.Rejected.GetStringValue();
                            string returned = clsImplementationEnum.MIPInspectionStatus.Returned.GetStringValue();
                            List<MIP050> lstMIP050 = new List<MIP050>();
                            foreach (DataRow item in ds.Tables[0].Rows)
                            {
                                string Location = item.Field<string>("Location");
                                string QualityProject = item.Field<string>("Quality Project");
                                string Annexure = item.Field<string>("Annexure");
                                string MIP_Type = item.Field<string>("MIP Type");
                                string ComponentNo = item.Field<string>("Component No.");
                                decimal OperationNo = 0;
                                Decimal.TryParse(item.Field<string>("Operation No"), out OperationNo);

                                var DocNoCode = item.Field<string>("AnnexureCode");
                                var MIPTypeCode = item.Field<string>("MIPTypeCode");
                                int MIP004LineId = Convert.ToInt32(item.Field<string>("MIP004LineId"));
                                var lstLoc = Manager.GetUserwiseLocation(objClsLoginInfo.UserName);
                                var objMIP004 = db.MIP004.FirstOrDefault(x => x.QualityProject.Trim() == QualityProject.Trim() && x.Location == Location && x.MIP003.MIPType.Trim() == MIPTypeCode.Trim() && x.MIP003.DocNo.Trim() == DocNoCode.Trim() && x.ComponentNo.Trim() == ComponentNo.Trim() && x.OperationNo == OperationNo);

                                if (objMIP004 != null ? objMIP004.MIP003.Status == Approved : false)
                                {
                                    #region Offer Entry
                                    if (objMIP004.InspectionStatus == clsImplementationEnum.MIPInspectionStatus.ReadyToOffer.GetStringValue())
                                    {
                                        objMIP004.InspectionStatus = clsImplementationEnum.MIPInspectionStatus.UnderInspection.GetStringValue();
                                        objMIP004.EditedBy = objClsLoginInfo.UserName;
                                        objMIP004.EditedOn = DateTime.Now;

                                        long RequestNo = 0;
                                        int ReqSeqNo = 1;
                                        ObjectParameter outParm = new ObjectParameter("result", typeof(long));
                                        var SPResult = db.SP_MIP_GET_NEXT_REQUEST_NO(objMIP004.BU, objMIP004.Location, outParm);
                                        RequestNo = (long)outParm.Value;
                                        MIP050 objMIP050 = new MIP050();

                                        objMIP050.QualityProject = objMIP004.QualityProject;
                                        objMIP050.Project = objMIP004.MIP003.Project;
                                        objMIP050.BU = objMIP004.MIP003.BU;
                                        objMIP050.Location = objMIP004.MIP003.Location;
                                        objMIP050.DocNo = objMIP004.MIP003.DocNo;
                                        objMIP050.MIPType = objMIP004.MIP003.MIPType;
                                        objMIP050.RevNo = objMIP004.MIP003.RevNo;
                                        objMIP050.ComponentNo = objMIP004.MIP003.ComponentNo;
                                        objMIP050.DrawingNo = objMIP004.MIP003.DrawingNo;
                                        objMIP050.RawMaterialGrade = objMIP004.MIP003.RawMaterialGrade;
                                        objMIP050.PlateNo = objMIP004.MIP003.PlateNo;
                                        objMIP050.IterationNo = 1;
                                        objMIP050.RequestNo = RequestNo;
                                        objMIP050.RequestNoSequence = ReqSeqNo;
                                        objMIP050.OperationNo = objMIP004.OperationNo;
                                        objMIP050.LineRevNo = objMIP004.LineRevNo;
                                        objMIP050.MIPRevNo = objMIP004.MIPRevNo;
                                        objMIP050.ActivityDescription = objMIP004.ActivityDescription;
                                        objMIP050.RefDocument = objMIP004.RefDocument;
                                        objMIP050.RefDocRevNo = objMIP004.RefDocRevNo;
                                        objMIP050.PIA = objMIP004.PIA;
                                        objMIP050.IAgencyLNT = objMIP004.IAgencyLNT;
                                        objMIP050.IAgencyVVPT = objMIP004.IAgencyVVPT;
                                        objMIP050.IAgencySRO = objMIP004.IAgencySRO;
                                        objMIP050.RecordsDocuments = objMIP004.RecordsDocuments;
                                        objMIP050.Observations = objMIP004.Observations;
                                        objMIP050.CreatedBy = objClsLoginInfo.UserName;
                                        objMIP050.CreatedOn = DateTime.Now;
                                        objMIP050.OfferedBy = objClsLoginInfo.UserName;
                                        objMIP050.OfferedOn = DateTime.Now;
                                        lstMIP050.Add(objMIP050);

                                        objMIP004.RequestNo = objMIP050.RequestNo;
                                        objMIP004.IterationNo = objMIP050.IterationNo;
                                        objMIP004.RequestSequenceNo = objMIP050.RequestNoSequence;
                                        db.SaveChanges();

                                        //#region Send Notification
                                        //(new clsManager()).SendNotificationBULocationWise((clsImplementationEnum.UserRoleName.QI1.GetStringValue() + ',' + clsImplementationEnum.UserRoleName.QI2.GetStringValue() + ',' + clsImplementationEnum.UserRoleName.QI3.GetStringValue()), objMIP004.BU, objMIP004.Location,
                                        //    "Traveler:  Request " + objMIP050.RequestNo + " of Activity: " + objMIP050.Activity + " & Project No: " + objMIP050.ProjectNo + " has been offered for Inspection", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/TVL/AttendInspection/Details/" + objMIP050.RequestId);
                                        //#endregion

                                        #endregion
                                    }
                                }
                            }
                            db.MIP050.AddRange(lstMIP050);
                            db.SaveChanges();

                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "MIP Operation(s) offered successfully";
                        }
                        catch (Exception ex)
                        {
                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        }
                    }
                    else
                    {
                        objResponseMsg = ErrorExportToExcel(ds);
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Excel file is not valid";
                }
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please enter valid Project no.";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponceMsgWithFileName ErrorExportToExcel(DataSet ds)
        {
            clsHelper.ResponceMsgWithFileName objResponseMsg = new clsHelper.ResponceMsgWithFileName();
            try
            {
                MemoryStream ms = new MemoryStream();
                using (FileStream fs = System.IO.File.OpenRead(Server.MapPath("~/Resources/MIP/MIP - Error Template for Bulk Offer.xlsx")))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();
                        int i = 2;
                        foreach (DataRow item in ds.Tables[0].Rows)
                        {
                            string QualityProject = item.Field<string>("Quality Project");
                            string Location = item.Field<string>("Location");
                            string Annexure = item.Field<string>("Annexure");
                            string MIP_Type = item.Field<string>("MIP Type");
                            string ComponentNo = item.Field<string>("Component No.");
                            string strOperationNo = item.Field<string>("Operation No");

                            //QualityProject
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("QualityProjectErrorMsg")))
                            {
                                excelWorksheet.Cells[i, 1].Value = QualityProject + " (Note: " + item.Field<string>("QualityProjectErrorMsg") + " )";
                                excelWorksheet.Cells[i, 1].Style.Font.Color.SetColor(Color.Red);
                            }
                            else { excelWorksheet.Cells[i, 1].Value = QualityProject; }

                            //Location
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("LocationErrorMsg")))
                            {
                                excelWorksheet.Cells[i, 2].Value = Location + " (Note: " + item.Field<string>("LocationErrorMsg").ToString() + " )";
                                excelWorksheet.Cells[i, 2].Style.Font.Color.SetColor(Color.Red);
                            }
                            else { excelWorksheet.Cells[i, 2].Value = Location; }


                            //Annexure
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("AnnexureErrorMsg")))
                            {
                                excelWorksheet.Cells[i, 3].Value = Annexure + " (Note: " + item.Field<string>("AnnexureErrorMsg").ToString() + " )";
                                excelWorksheet.Cells[i, 3].Style.Font.Color.SetColor(Color.Red);
                            }
                            else { excelWorksheet.Cells[i, 3].Value = Annexure; }

                            //MIPType
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("MIPTypeErrorMsg")))
                            {
                                excelWorksheet.Cells[i, 4].Value = MIP_Type + " (Note: " + item.Field<string>("MIPTypeErrorMsg").ToString() + " )";
                                excelWorksheet.Cells[i, 4].Style.Font.Color.SetColor(Color.Red);
                            }
                            else { excelWorksheet.Cells[i, 4].Value = MIP_Type; }

                            //ComponentNo
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("ComponentNoErrorMsg")))
                            {
                                excelWorksheet.Cells[i, 5].Value = ComponentNo + " (Note: " + item.Field<string>("ComponentNoErrorMsg").ToString() + " )";
                                excelWorksheet.Cells[i, 5].Style.Font.Color.SetColor(Color.Red);
                            }
                            else { excelWorksheet.Cells[i, 5].Value = ComponentNo; }

                            //OperationNo
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("OperationNoErrorMsg")))
                            {
                                excelWorksheet.Cells[i, 6].Value = strOperationNo + " (Note: " + item.Field<string>("OperationNoErrorMsg").ToString() + " )";
                                excelWorksheet.Cells[i, 6].Style.Font.Color.SetColor(Color.Red);
                            }
                            else { excelWorksheet.Cells[i, 6].Value = strOperationNo; }

                            //Error
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("ErrorMsg")))
                            {
                                if (!string.IsNullOrWhiteSpace(excelWorksheet.Cells[1, 6].ToString()))
                                {
                                    excelWorksheet.Cells[1, 7].Value = "Error";
                                    excelWorksheet.Cells[1, 7].Style.Font.Color.SetColor(Color.Red);
                                }
                                excelWorksheet.Cells[i, 7].Value = item.Field<string>("ErrorMsg").ToString();
                                excelWorksheet.Cells[i, 7].Style.Font.Color.SetColor(Color.Red);
                            }
                            i++;
                        }
                        excelPackage.SaveAs(ms);
                    }
                }

                ms.Position = 0;

                string fileName;
                string excelFilePath = Helper.ExcelFilePath(objClsLoginInfo.UserName, out fileName);

                Byte[] bin = ms.ToArray();
                System.IO.File.WriteAllBytes(excelFilePath, bin);

                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error";
                objResponseMsg.FileName = fileName;
                return objResponseMsg;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }

        public DataSet ToChechValidation(ExcelPackage package, out bool isError)
        {
            ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();
            DataTable dtHeaderExcel = new DataTable();
            isError = false;
            int notesIndex = 0;
            try
            {
                #region Read Lines data from excel 
                foreach (var firstRowCell in excelWorksheet.Cells[1, 1, 1, excelWorksheet.Dimension.End.Column])
                {
                    dtHeaderExcel.Columns.Add(firstRowCell.Text);
                }

                for (var rowNumber = 2; rowNumber <= excelWorksheet.Dimension.End.Row; rowNumber++)
                {
                    var row = excelWorksheet.Cells[rowNumber, 1, rowNumber, excelWorksheet.Dimension.End.Column];
                    var newRow = dtHeaderExcel.NewRow();
                    if (excelWorksheet.Cells[rowNumber, 1].Value != null)
                    {
                        foreach (var cell in row)
                        {
                            newRow[cell.Start.Column - 1] = cell.Text;
                        }
                    }
                    else
                    {
                        notesIndex = rowNumber + 1;
                        break;
                    }

                    dtHeaderExcel.Rows.Add(newRow);
                }
                #endregion

                #region Validation for Lines

                dtHeaderExcel.Columns.Add("QualityProjectErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("AnnexureErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("MIPTypeErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("ComponentNoErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("OperationNoErrorMsg", typeof(string));

                dtHeaderExcel.Columns.Add("ErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("AnnexureCode", typeof(string));
                dtHeaderExcel.Columns.Add("MIPTypeCode", typeof(string));
                dtHeaderExcel.Columns.Add("MIP004LineId", typeof(string));
                dtHeaderExcel.Columns.Add("LocationErrorMsg", typeof(string));
                List<GLB002> lstMIPDocNoValues = new List<GLB002>();
                List<GLB002> lstMIPTypeValues = new List<GLB002>();
                bool isprojectnotvalid = false;
                string bu = string.Empty, project = string.Empty, Qproject = string.Empty;
                var lstLoc = Manager.GetUserwiseLocation(objClsLoginInfo.UserName);

                foreach (DataRow item in dtHeaderExcel.Rows)
                {
                    bool isRowError = false;
                    string QualityProject = item.Field<string>("Quality Project");
                    string Location = item.Field<string>("Location");
                    string Annexure = item.Field<string>("Annexure"), DocNoCode = null;
                    string MIP_Type = item.Field<string>("MIP Type"), MIPTypeCode = null;
                    string ComponentNo = item.Field<string>("Component No.");
                    string strOperationNo = item.Field<string>("Operation No");
                    decimal OperationNo = 0;
                    if (!lstLoc.Contains(Location))
                    {
                        item["LocationErrorMsg"] = "You are not belongs to authorised to location";
                        isError = true;
                        isRowError = true;
                    }
                    if (!isRowError)
                    {
                        if (string.IsNullOrWhiteSpace(QualityProject))
                        {
                            item["QualityProjectErrorMsg"] = "Quality Project is not valid";
                            isError = true;
                            isRowError = true;
                        }
                        else
                        {
                            if (QualityProject != Qproject)
                            {
                                var objproject = db.QMS010.Where(i => i.QualityProject.Trim().Equals(QualityProject.Trim(), StringComparison.OrdinalIgnoreCase) && i.Location.Trim() == Location.Trim()).FirstOrDefault();
                                if (objproject != null)
                                {
                                    Qproject = objproject.QualityProject;
                                    project = objproject.Project;
                                    bu = objproject.BU.Trim();
                                    lstMIPDocNoValues = Manager.GetSubCatagories("MIP_DocNo", bu, Location);
                                    lstMIPTypeValues = Manager.GetSubCatagories("MIP_Type", bu, Location);
                                    isprojectnotvalid = false;
                                }
                                else
                                {
                                    isprojectnotvalid = true;
                                    lstMIPDocNoValues = null;
                                    lstMIPTypeValues = null;
                                }
                            }
                            if (isprojectnotvalid)
                            {
                                item["QualityProjectErrorMsg"] = "Quality Project is not valid";
                                isError = true;
                            }

                            if (string.IsNullOrWhiteSpace(ComponentNo))
                            {
                                item["ComponentNoErrorMsg"] = "Component No is required";
                                isError = true; isRowError = true;
                            }
                            if (string.IsNullOrEmpty(Annexure))
                            {
                                item["AnnexureErrorMsg"] = "Annexure is required";
                                isError = true; isRowError = true;
                            }
                            else if ((DocNoCode = lstMIPDocNoValues.Where(w => w.Description.Trim() == Annexure.Trim()).Select(s => s.Code).FirstOrDefault()) == null)
                            {
                                item["AnnexureErrorMsg"] = "Doc No is not valid";
                                isError = true; isRowError = true;
                            }
                            else
                                item["AnnexureCode"] = DocNoCode;

                            if (string.IsNullOrEmpty(MIP_Type))
                            {
                                item["MIPTypeErrorMsg"] = "MIP Type is required";
                                isError = true; isRowError = true;
                            }
                            else if ((MIPTypeCode = lstMIPTypeValues.Where(w => w.Description.Trim() == MIP_Type.Trim()).Select(s => s.Code).FirstOrDefault()) == null)
                            {
                                item["MIPTypeErrorMsg"] = "MIP Type is not valid";
                                isError = true; isRowError = true;
                            }
                            else
                                item["MIPTypeCode"] = MIPTypeCode;

                            if (!Decimal.TryParse(strOperationNo, out OperationNo))
                            {
                                item["OperationNoErrorMsg"] = "Operation No is not valid";
                                isError = true; isRowError = true;
                            }

                            if (!isRowError)
                            {
                                if (!db.MIP004.Any(a => a.QualityProject.Trim() == QualityProject.Trim() && a.BU == bu && a.Location == Location && a.MIP003.DocNo.Trim() == DocNoCode.Trim() && a.MIP003.MIPType.Trim() == MIPTypeCode.Trim() && a.OperationNo == OperationNo))
                                {
                                    item["ErrorMsg"] = "No record found for this combination";
                                    isError = true;
                                }
                                else
                                {
                                    var obj = db.MIP004.Where(a => a.QualityProject.Trim() == QualityProject.Trim() && a.BU == bu && a.Location == Location && a.MIP003.DocNo.Trim() == DocNoCode.Trim() && a.MIP003.MIPType.Trim() == MIPTypeCode.Trim() && a.ComponentNo.Trim() == ComponentNo.Trim() && a.OperationNo == OperationNo).FirstOrDefault();
                                    if (obj != null)
                                    {
                                        if (obj.MIP003.Status != clsImplementationEnum.MIPLineStatus.Approved.GetStringValue())
                                        {
                                            item["ErrorMsg"] = "This Component is not 'Approved' in MIP Header. Please approve component first.";
                                            isError = true;
                                        }
                                        else
                                        {
                                            if (obj.InspectionStatus != clsImplementationEnum.MIPInspectionStatus.ReadyToOffer.GetStringValue())
                                            {
                                                item["ErrorMsg"] = "This operation is not in 'Ready to Offer' ";
                                                isError = true;
                                            }
                                            else
                                            {
                                                item["MIP004LineId"] = obj.LineId;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            DataSet ds = new DataSet();
            ds.Tables.Add(dtHeaderExcel);
            return ds;
        }


        #endregion
    }
}