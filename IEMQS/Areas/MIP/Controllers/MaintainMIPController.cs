﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQS.Models;
using IEMQS.Areas.NDE.Models;
using System.Data.Entity.Core.Objects;
using IEMQS.Areas.IPI.Models;
using IEMQS.Areas.Utility.Models;
using System.Data;
using OfficeOpenXml;
using System.IO;
using System.Drawing;

namespace IEMQS.Areas.MIP.Controllers
{
    public class MaintainMIPController : clsBase
    {
        // GET: MIP/MaintainMIP
        #region Index Page
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Index()
        {
            ViewBag.Title = clsImplementationEnum.MIPIndexTitle.maintainMIP.GetStringValue();
            ViewBag.IndexType = clsImplementationEnum.MIPIndexType.maintain.GetStringValue();

            return View();
        }

        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Approve()
        {
            ViewBag.Title = clsImplementationEnum.MIPIndexTitle.ApproveMIP.GetStringValue();
            ViewBag.IndexType = clsImplementationEnum.MIPIndexType.approve.GetStringValue();
            return View("Index");
        }

        public ActionResult GetIndexGridDataPartial(string status, string title, string indextype)
        {
            ViewBag.Status = status;
            ViewBag.GridTitle = title;
            ViewBag.IndexDataFor = indextype;
            return PartialView("_GetIndexGridDataPartial");
        }

        public ActionResult LoadMIPIndexDataTable(JQueryDataTableParamModel param)
        {
            try
            {
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "MIP001.BU", "MIP001.Location");
                string _urlform = string.Empty;
                string indextype = param.Department;

                if (string.IsNullOrWhiteSpace(indextype))
                    indextype = clsImplementationEnum.MIPIndexType.maintain.GetStringValue();
                if (param.Status.ToUpper() == "PENDING")
                {
                    if (indextype == clsImplementationEnum.MIPIndexType.approve.GetStringValue())
                        whereCondition += " and MIP001.PMGApprovedBy IS NULL and MIP001.Status in('" + clsImplementationEnum.MIPHeaderStatus.SentForApproval.GetStringValue() + "')";
                    if (indextype == clsImplementationEnum.MIPIndexType.maintain.GetStringValue())
                        whereCondition += " and MIP001.Status in('" + clsImplementationEnum.MIPHeaderStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.MIPHeaderStatus.Returned.GetStringValue() + "')";
                }

                string[] columnName = { "QualityProject", "(MIP001.Project+'-'+com1.t_dsca)", "(LTRIM(RTRIM(MIP001.Location))+'-'+LTRIM(RTRIM(lcom2.t_desc)))", "(LTRIM(RTRIM(MIP001.BU))+'-'+LTRIM(RTRIM(bcom2.t_desc)))" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);

                string sortColumnName = Convert.ToString(param.sColumns.Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstHeader = db.SP_MIP_GET_PROJECT_INDEX_DATA(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstHeader
                          select new[] {
                        a.QualityProject,
                        a.Project,
                        a.BU,
                        a.Location,
                        Helper.HTMLActionString(Convert.ToInt32(a.HeaderId), "View", "View Details", "fa fa-eye", "", "/MIP/MaintainMIP/Details/"+a.HeaderId+"?urlForm="+ indextype )
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }

        }

        #endregion

        #region Header Details Page
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Details(int id = 0, string urlForm = "")
        {
            MIP001 objMIP001 = new MIP001();

            if (id > 0)
            {
                objMIP001 = db.MIP001.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.Project = db.COM001.Where(i => i.t_cprj.Equals(objMIP001.Project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objMIP001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                ViewBag.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objMIP001.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                if (IsAnyApprovedStatus(objMIP001))
                    ViewBag.IDMNoDisabled = "1";
                else
                    ViewBag.IDMNoDisabled = "0";

            }
            else
            {
                ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objClsLoginInfo.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objMIP001.Location = objClsLoginInfo.Location;
                ViewBag.IDMNoDisabled = "1";
            }

            if (urlForm.ToLower() == clsImplementationEnum.MIPIndexType.maintain.GetStringValue().ToLower())
            {
                ViewBag.Title = clsImplementationEnum.MIPIndexTitle.maintainMIP.GetStringValue();
            }
            else if (urlForm.ToLower() == clsImplementationEnum.ADDIndexType.approve.GetStringValue().ToLower())
            {
                ViewBag.Title = clsImplementationEnum.MIPIndexTitle.ApproveMIP.GetStringValue();
            }
            else
            {
                ViewBag.formRedirect = clsImplementationEnum.MIPIndexType.maintain.GetStringValue();
                ViewBag.Title = clsImplementationEnum.MIPIndexTitle.maintainMIP.GetStringValue();
            }
            ViewBag.formRedirect = urlForm;

            return View(objMIP001);
        }

        public ActionResult GetMIPHeaderDataPartial(string Status, string QualityProject, string Location, string BU, string indextype)
        {
            ViewBag.status = Status;
            ViewBag.qualityProject = QualityProject;
            ViewBag.location = Location;
            ViewBag.bu = BU;
            ViewBag.indextype = indextype;
            if (string.IsNullOrWhiteSpace(indextype))
            {
                ViewBag.indextype = clsImplementationEnum.MIPIndexType.maintain.GetStringValue();
            }

            UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.DocNo = Manager.GetSubCatagories("MIP_DocNo", BU, Location).Select(i => new CategoryData { Value = i.Description, Code = i.Code, CategoryDescription = i.Description }).ToList();
            ViewBag.Type = Manager.GetSubCatagories("MIP_Type", BU, Location).Select(i => new CategoryData { Value = i.Description, Code = i.Code, CategoryDescription = i.Description }).ToList();

            return PartialView("_GetMIPHeaderDataPartial");
        }

        [HttpPost]
        public ActionResult GetMIPHeaderData(JQueryDataTableParamModel param, string qualityProj)
        {
            try
            {
                UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "MIP001.BU", "MIP001.Location");
                string strSortOrder = string.Empty;
                bool isRemarkEditable = false;

                if (!string.IsNullOrEmpty(qualityProj))
                {
                    whereCondition += " AND UPPER(MIP001.QualityProject) = '" + qualityProj.Trim().ToUpper() + "' AND UPPER(MIP001.Location) = '" + param.Location.Trim().ToUpper() + "'  AND UPPER(MIP001.BU) = '" + param.BU.Trim().ToUpper() + "'";
                }

                if (param.Status.ToUpper() == "PENDING")
                {
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.PMG2.GetStringValue() && objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Approver.GetStringValue())
                    {
                        isRemarkEditable = true;
                        whereCondition += " and MIP001.PMGApprovedBy IS NULL and MIP001.Status in('" + clsImplementationEnum.MIPHeaderStatus.SentForApproval.GetStringValue() + "')";
                    }
                    else if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.PMG3.GetStringValue() && objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Initiator.GetStringValue())
                    {
                        whereCondition += " and MIP001.Status in('" + clsImplementationEnum.MIPHeaderStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.MIPHeaderStatus.Returned.GetStringValue() + "')";
                    }
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                string[] columnName = { "DocNo", "MIPType", "RevNo", "PMGReturnRemarks", "Status" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
               
                var lstResult = db.SP_MIP_GET_HEADER_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                var CheckedIds = "";
                if(param.bCheckedAll && lstResult.Any())
                    CheckedIds = String.Join(",", db.SP_MIP_GET_HEADER_DATA(1, 0, strSortOrder, whereCondition).Select(s => s.HeaderId).ToList());
                
                int newRecordId = 0;
                var newRecord = new[] {
                                    "",
                                    Helper.GenerateHidden(newRecordId, "HeaderId", ""),
                                    GenerateAutoCompleteOnBlur(newRecordId,"txtDocNo","","checkExistingRecord(this,"+newRecordId+")","",false,"","DocNo")+""+Helper.GenerateHidden(newRecordId,"DocNo"),
                                    GenerateAutoCompleteOnBlur(newRecordId,"txtMIPType","","checkExistingRecord(this,"+newRecordId+")","",false,"","MIPType")+""+Helper.GenerateHidden(newRecordId,"MIPType"),
                                    "",
                                    Helper.GenerateNumericTextbox(newRecordId,"txtRevNo","0","",false,"",false),
                                    clsImplementationEnum.MIPHeaderStatus.Draft.GetStringValue(),
                                    Helper.GenerateGridButtonType(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveMIPHeader();")
                                };
                var enableEditForStatus = new string[] { clsImplementationEnum.MIPHeaderStatus.Draft.GetStringValue(), clsImplementationEnum.MIPHeaderStatus.Returned.GetStringValue() };
                bool isInitiator = (objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Initiator.GetStringValue());
                var data = (from uc in lstResult
                            select new[]
                            {
                               Convert.ToString(uc.HeaderId),
                               Helper.GenerateHidden(uc.HeaderId, "HeaderId", Convert.ToString(uc.HeaderId)),
                               GenerateAutoCompleteOnBlur(uc.HeaderId,"txtDocNo",uc.DocNo,"checkExistingRecord(this,"+uc.HeaderId+",true)","",false,"","DocNo",uc.DocNo,  ( !isInitiator || !(enableEditForStatus.Contains(uc.Status)) || (uc.IsLogExist.HasValue && uc.IsLogExist.Value)))+""+Helper.GenerateHidden(uc.HeaderId,"DocNo"),
                               GenerateAutoCompleteOnBlur(uc.HeaderId,"txtMIPType",uc.MIPType,"checkExistingRecord(this,"+uc.HeaderId+",true)","",false,"","MIPType", uc.MIPType,  ( !isInitiator || !(enableEditForStatus.Contains(uc.Status)) || (uc.IsLogExist.HasValue && uc.IsLogExist.Value)))+""+Helper.GenerateHidden(uc.HeaderId,"MIPType"),
                               GenerateRemark(uc.HeaderId, "PMGReturnRemarks", isRemarkEditable),
                               "R" + uc.RevNo,
                               uc.Status,
                               Helper.HTMLActionString(uc.HeaderId, "Delete", "Delete MIP", "fa fa-trash-o", "DeleteMIPHeaderWithConfirmation("+ uc.HeaderId +")", "", ( !isInitiator || !(enableEditForStatus.Contains(uc.Status)) || (uc.IsLogExist.HasValue && uc.IsLogExist.Value))) +
                               Helper.HTMLActionString(uc.HeaderId, "History", "History", "fa fa-history", "HistoryMIP("+ uc.HeaderId +")", "",uc.IsLogExist.HasValue && uc.IsLogExist.Value ? false : true) +
                               Helper.HTMLActionString(uc.HeaderId, "Revise", "Revise", "fa fa-retweet", "ReviseMIPHeaderWithConfirmation("+ uc.HeaderId +")", "", uc.Status == clsImplementationEnum.MIPHeaderStatus.Approved.GetStringValue() && isInitiator ? false : true)
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition,
                    checkedIds = CheckedIds
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveMIPHeader(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            MIP001 objMIP001 = new MIP001();
            int newRowIndex = 0;
            try
            {
                string strProject = !string.IsNullOrEmpty(fc["Project"]) ? Convert.ToString(fc["Project"]).Split('-')[0].Trim() : string.Empty;
                string strBU = !string.IsNullOrEmpty(fc["BU"]) ? Convert.ToString(fc["BU"]).Split('-')[0].Trim() : string.Empty;
                string strLocation = !string.IsNullOrEmpty(fc["Location"]) ? Convert.ToString(fc["Location"]).Split('-')[0].Trim() : string.Empty;
                string strQualityProject = !string.IsNullOrEmpty(fc["txtQualityProject"]) ? fc["txtQualityProject"] : string.Empty;
                string strDocNo = !string.IsNullOrEmpty(fc["DocNo" + newRowIndex]) ? fc["DocNo" + newRowIndex] : string.Empty;
                string strMIPType = !string.IsNullOrEmpty(fc["MIPType" + newRowIndex]) ? fc["MIPType" + newRowIndex] : string.Empty;
                int RevNo = !string.IsNullOrEmpty(fc["txtRevNo" + newRowIndex]) ? Convert.ToInt32(fc["txtRevNo" + newRowIndex].ToString()) : 0;

                if (fc != null)
                {
                    if (isMIPHeaderExist(strQualityProject, strProject, strBU, strLocation, strDocNo, strMIPType))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "MIP " + strDocNo + " for " + strQualityProject + " is already exist";
                    }
                    else
                    {
                        #region Add New MIP Header
                        objMIP001.QualityProject = strQualityProject;
                        objMIP001.Project = strProject;
                        objMIP001.BU = strBU;
                        objMIP001.Location = strLocation;
                        objMIP001.DocNo = strDocNo;
                        objMIP001.MIPType = strMIPType;
                        objMIP001.RevNo = RevNo;
                        objMIP001.Status = clsImplementationEnum.MIPHeaderStatus.Draft.GetStringValue();
                        objMIP001.CreatedBy = objClsLoginInfo.UserName;
                        objMIP001.CreatedOn = DateTime.Now;

                        var objMIP001First = db.MIP001.Where(i => i.QualityProject == objMIP001.QualityProject && i.Project == objMIP001.Project && i.BU == objMIP001.BU && i.Location == objMIP001.Location).FirstOrDefault();
                        if (objMIP001First != null && objMIP001First.IDMNumber != null)
                        {
                            objMIP001.IDMNumber = objMIP001First.IDMNumber;
                        }
                        db.MIP001.Add(objMIP001);
                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "MIP added successfully";
                        objResponseMsg.HeaderId = objMIP001.HeaderId;
                        objResponseMsg.HeaderStatus = objMIP001.Status;
                        objResponseMsg.RevNo = Convert.ToString(objMIP001.RevNo);
                        #endregion

                        if (!IsAnyApprovedStatus(objMIP001))
                        {
                            objResponseMsg.ActionKey = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateMIPHeader(int headerId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            MIP001 objMIP001 = db.MIP001.Where(i => i.HeaderId == headerId).FirstOrDefault();
            try
            {
                if (objMIP001.Status == clsImplementationEnum.CommonStatus.DRAFT.GetStringValue() || objMIP001.Status == clsImplementationEnum.CommonStatus.Returned.GetStringValue())
                {
                    if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                    {
                        db.SP_MIP_UPDATE_HEADER_COLUMN(headerId, columnName, columnValue);
                    }
                    if (headerId > 0)
                    {
                        objMIP001.EditedBy = objClsLoginInfo.UserName;
                        objMIP001.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Saved Successfully";
                    }
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Remarks = clsImplementationMessage.CommonMessages.AlreadySubmitted;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteMIPHeader(int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (headerId > 0)
                {
                    MIP001 objMIP001 = db.MIP001.FirstOrDefault(c => c.HeaderId == headerId);
                    if (objMIP001 != null)
                    {
                        if (objMIP001.Status == clsImplementationEnum.CommonStatus.DRAFT.GetStringValue() || objMIP001.Status == clsImplementationEnum.CommonStatus.Returned.GetStringValue())
                        {
                            MIP002 objParent = db.MIP002.FirstOrDefault(c => c.HeaderId == objMIP001.HeaderId);
                            if (objParent != null)
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = "Can't able to delete bacause already attached with MIP: " + objParent.RefDocument;
                            }
                            else
                            {
                                db.MIP002.RemoveRange(objMIP001.MIP002);
                                db.MIP001.Remove(objMIP001);
                                db.SaveChanges();

                                objResponseMsg.Key = true;
                                objResponseMsg.Value = "MIP deleted successfully";
                            }
                        }
                        else
                        {
                            objResponseMsg.Key = true;
                            objResponseMsg.Remarks = "This data is already submitted, it cannot be delete now!!";
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "MIP not found";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReviseMIPHeader(string headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (!string.IsNullOrEmpty(headerId))
                {
                    int intheaderId = Convert.ToInt32(headerId);
                    MIP001 objMIP001 = db.MIP001.FirstOrDefault(c => c.HeaderId == intheaderId);
                    if (objMIP001 != null)
                    {
                        if (objMIP001.Status != clsImplementationEnum.MIPHeaderStatus.Approved.GetStringValue())
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "This data is already Revised, it cannot be revise now!!";
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }
                        objMIP001.Status = clsImplementationEnum.MIPHeaderStatus.Draft.GetStringValue();
                        objMIP001.RevNo = objMIP001.RevNo + 1;
                        objMIP001.EditedBy = objClsLoginInfo.UserName;
                        objMIP001.EditedOn = DateTime.Now;
                        objMIP001.PMGApprovedBy = null;
                        objMIP001.PMGApprovedOn = null;
                        objMIP001.PMGReturnedBy = null;
                        objMIP001.PMGReturnedOn = null;
                        objMIP001.PMGReturnRemarks = null;
                        objMIP001.MIP002.ToList().ForEach(x =>
                        {
                            x.MIPRevNo = objMIP001.RevNo.Value;
                            x.EditedBy = objClsLoginInfo.UserName;
                            x.EditedOn = DateTime.Now;
                        });
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "MIP revised successfully";
                        if (!IsAnyApprovedStatus(objMIP001))
                        {
                            objResponseMsg.ActionKey = true;
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Record Not Found";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SubmitMIPHeader(string strHeader)
        {
            var objResponseMsg = new ResponceMsgWithMultiValue();
            List<MIP001> lstMIP001s = new List<MIP001>();
            try
            {
                if (!string.IsNullOrEmpty(strHeader))
                {
                    List<string> submittedMIP = new List<string>();
                    List<string> pendingMIP = new List<string>();
                    int[] headerIds = Array.ConvertAll(strHeader.Split(','), s => int.Parse(s));
                    foreach (int headerId in headerIds)
                    {
                        var MIP001 = db.MIP001.Where(x => x.HeaderId == headerId).FirstOrDefault();
                        var result = SendForApproval(headerId);
                        if (result)
                            submittedMIP.Add(MIP001.DocNo);
                        else
                            pendingMIP.Add(MIP001.DocNo);
                    }
                    if (submittedMIP.Count > 0)
                    {
                        objResponseMsg.submittedMIP = "MIP " + String.Join(",", submittedMIP) + " submitted successfully";
                        objResponseMsg.isIdenticalProjectEditable = false;
                    }
                    else
                        objResponseMsg.isIdenticalProjectEditable = true;
                    if (pendingMIP.Count > 0)
                        objResponseMsg.pendingMIP = "No Operation(s) Exist(s) For MIP :" + String.Join(",", pendingMIP) + ".Please Add Operations.";
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "MIP has been sucessfully submitted for approval";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please select atleast one record sending for approval";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApproveMIPHeader(string strHeader, string department)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            List<MIP001> lstMIP001s = new List<MIP001>();
            try
            {
                if (!string.IsNullOrEmpty(strHeader))
                {
                    var SendForApprovedHeaderDocNos = new List<string>();
                    string SendForApproval = clsImplementationEnum.MIPHeaderStatus.SentForApproval.GetStringValue();
                    int[] headerIds = Array.ConvertAll(strHeader.Split(','), s => int.Parse(s));
                    foreach (int headerId in headerIds)
                    {
                        if (department == clsImplementationEnum.UserRoleName.PMG2.GetStringValue())
                        {
                            MIP001 objMIP001 = db.MIP001.FirstOrDefault(i => i.HeaderId == headerId);
                            if (objMIP001 != null)
                            {
                                if (db.MIP003.Any(w => w.DocNo == objMIP001.DocNo && w.MIPType == objMIP001.MIPType && w.QualityProject == objMIP001.QualityProject && w.Location == objMIP001.Location && w.BU == objMIP001.BU && w.Status == SendForApproval))
                                    SendForApprovedHeaderDocNos.Add(objMIP001.DocNo);
                                else
                                    ApproveByPMG(headerId);
                            }
                        }
                    }
                    if (SendForApprovedHeaderDocNos.Count() > 0)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "MIP(s) " + String.Join(",", SendForApprovedHeaderDocNos) + " are in Send for Approve Stage in MIP Header. Please send request to QC2/3 for it approve.";
                    }
                    else
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "MIP(s) has been sucessfully approved";
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please select atleast one record for approve";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        private bool ApproveByPMG(int headerId)
        {
            var lineStatus = clsImplementationEnum.MIPHeaderStatus.SentForApproval.GetStringValue();
            MIP001 objMIP001 = db.MIP001.FirstOrDefault(i => i.HeaderId == headerId && i.Status == lineStatus);
            if (objMIP001 != null)
            {
                objMIP001.Status = clsImplementationEnum.MIPHeaderStatus.Approved.GetStringValue();
                objMIP001.PMGApprovedBy = objClsLoginInfo.UserName;
                objMIP001.PMGApprovedOn = DateTime.Now;
                db.SaveChanges();
                List<MIP001_Log> lstMIP001_Log = db.MIP001_Log.Where(c => c.HeaderId == objMIP001.HeaderId).ToList();
                if (lstMIP001_Log.Count > 0)
                {
                    var stageStatus = clsImplementationEnum.MIPLineStatus.Approved.GetStringValue();
                    var lstOperations = db.MIP002.Where(i => i.HeaderId == headerId).ToList();
                    if (lstOperations.Any())
                    {
                        InsUpdOnMIPHeader(objMIP001, lstOperations);
                        //objResponseMsg = InsUpdOnTestPlan(objQMS015.QualityId, lstStages);
                    }

                    lstMIP001_Log.ForEach(x =>
                    {
                        x.Status = clsImplementationEnum.MIPHeaderStatus.Superseded.GetStringValue();
                    });
                }
                #region Maintain Log Details
                MIP001_Log objMIP001_Log = new MIP001_Log();
                objMIP001_Log.HeaderId = objMIP001.HeaderId;
                objMIP001_Log.QualityProject = objMIP001.QualityProject;
                objMIP001_Log.Project = objMIP001.Project;
                objMIP001_Log.BU = objMIP001.BU;
                objMIP001_Log.Location = objMIP001.Location;
                objMIP001_Log.DocNo = objMIP001.DocNo;
                objMIP001_Log.MIPType = objMIP001.MIPType;
                objMIP001_Log.RevNo = objMIP001.RevNo;
                objMIP001_Log.Status = objMIP001.Status;
                objMIP001_Log.CreatedBy = objMIP001.CreatedBy;
                objMIP001_Log.CreatedOn = objMIP001.CreatedOn;
                objMIP001_Log.EditedBy = objMIP001.EditedBy;
                objMIP001_Log.EditedOn = objMIP001.EditedOn;
                objMIP001_Log.SubmittedBy = objMIP001.SubmittedBy;
                objMIP001_Log.SubmittedOn = objMIP001.SubmittedOn;
                objMIP001_Log.PMGApprovedBy = objMIP001.PMGApprovedBy;
                objMIP001_Log.PMGApprovedOn = objMIP001.PMGApprovedOn;
                objMIP001_Log.PMGReturnedBy = objMIP001.PMGReturnedBy;
                objMIP001_Log.PMGReturnedOn = objMIP001.PMGReturnedOn;
                objMIP001_Log.PMGReturnRemarks = objMIP001.PMGReturnRemarks;
                objMIP001_Log.IDMNumber = objMIP001.IDMNumber;
                db.MIP001_Log.Add(objMIP001_Log);
                db.SaveChanges();

                List<MIP002_Log> lstMIP002_Log = db.MIP002_Log.Where(c => c.HeaderId == objMIP001.HeaderId).ToList();
                if (lstMIP002_Log.Count > 0)
                {
                    lstMIP002_Log.ForEach(x =>
                    {
                        x.Status = clsImplementationEnum.MIPHeaderStatus.Superseded.GetStringValue();
                    });
                }

                List<MIP002> deletedLines = new List<MIP002>();
                foreach (MIP002 objMIP002 in objMIP001.MIP002)
                {
                    if (objMIP002.Status == clsImplementationEnum.MIPLineStatus.Deleted.GetStringValue())
                    {
                        deletedLines.Add(objMIP002);
                        continue;
                    }

                    objMIP002.Status = clsImplementationEnum.MIPLineStatus.Approved.GetStringValue();

                    MIP002_Log objMIP002_Log = new MIP002_Log();
                    objMIP002_Log.RefId = objMIP001_Log.Id;
                    objMIP002_Log.LineId = objMIP002.LineId;
                    objMIP002_Log.HeaderId = objMIP002.HeaderId;
                    objMIP002_Log.QualityProject = objMIP002.QualityProject;
                    objMIP002_Log.OperationNo = objMIP002.OperationNo;
                    objMIP002_Log.RefDocument = objMIP002.RefDocument;
                    objMIP002_Log.RefDocRevNo = objMIP002.RefDocRevNo;
                    objMIP002_Log.PIA = objMIP002.PIA;
                    objMIP002_Log.ActivityDescription = objMIP002.ActivityDescription;
                    objMIP002_Log.IAgencyLNT = objMIP002.IAgencyLNT;
                    objMIP002_Log.IAgencySRO = objMIP002.IAgencySRO;
                    objMIP002_Log.IAgencyVVPT = objMIP002.IAgencyVVPT;
                    objMIP002_Log.LineRevNo = objMIP002.LineRevNo;
                    objMIP002_Log.MIPRevNo = objMIP002.MIPRevNo;
                    objMIP002_Log.Remarks = objMIP002.Remarks;
                    objMIP002_Log.Status = objMIP002.Status;
                    objMIP002_Log.CreatedBy = objMIP002.CreatedBy;
                    objMIP002_Log.CreatedOn = objMIP002.CreatedOn;
                    objMIP002_Log.EditedBy = objMIP002.EditedBy;
                    objMIP002_Log.EditedOn = objMIP002.EditedOn;
                    db.MIP002_Log.Add(objMIP002_Log);
                }

                db.MIP002.RemoveRange(deletedLines);

                db.SaveChanges();

                #endregion

                #region Send Notification
                (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PMG3.GetStringValue(), objMIP001.Project, objMIP001.BU, objMIP001.Location, "MIP: " + objMIP001.DocNo + " is approved", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/MIP/MaintainMIP/Details/" + objMIP001.HeaderId);
                #endregion

                return true;
            }
            return false;
        }
        public void InsUpdOnMIPHeader(MIP001 objMIP001, List<MIP002> lstOperations)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();

            #region Update On Header (MIP003 & MIP004)
            var lstMIP003 = db.MIP003.Where(c => c.DocNo == objMIP001.DocNo && c.MIPType == objMIP001.MIPType && c.BU == objMIP001.BU && c.Location == objMIP001.Location && c.QualityProject == objMIP001.QualityProject).ToList();
            foreach (MIP003 item in lstMIP003)
            {
                bool isUpdated = false;
                foreach (var lst in lstOperations)
                {
                    if (!IsOffered(lst.LineId, objMIP001.BU, objMIP001.Location, objMIP001.DocNo, objMIP001.MIPType))
                    {
                        if (!(item.MIP004.Where(x => x.InspectionStatus != null).Max(x => x.OperationNo) > lst.OperationNo))
                        {
                            if (item.MIP004.Any(c => c.OperationNo == lst.OperationNo))
                            {
                                // check for deleted stage
                                if (lst.Status == clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue())
                                {
                                    #region delete stage from MIP HEADER
                                    MIP004 deletedStageMIP004 = db.MIP004.Where(c => c.HeaderId == item.HeaderId && c.OperationNo == lst.OperationNo).FirstOrDefault();
                                    if (deletedStageMIP004 != null)
                                    {
                                        deletedStageMIP004.Status = lst.Status;
                                        deletedStageMIP004.EditedBy = lst.EditedBy;
                                        deletedStageMIP004.EditedOn = lst.EditedOn;
                                        db.SaveChanges();
                                        isUpdated = true;
                                    }
                                    #endregion
                                }
                                else
                                {
                                    #region update stage in MIP HEADER
                                    MIP004 modifiedStageMIP004 = db.MIP004.Where(c => c.HeaderId == item.HeaderId && c.OperationNo == lst.OperationNo).FirstOrDefault();
                                    if (modifiedStageMIP004 != null)
                                    {
                                        //modifiedStageMIP004.LineRevNo = lst.LineRevNo;
                                        //modifiedStageMIP004.MIPRevNo = lst.MIPRevNo;
                                        modifiedStageMIP004.MIPRevNo = item.RevNo == null ? 0 : item.RevNo.Value;
                                        if (item.Status == clsImplementationEnum.MIPHeaderStatus.Approved.GetStringValue())
                                            modifiedStageMIP004.MIPRevNo = modifiedStageMIP004.MIPRevNo + 1;
                                        modifiedStageMIP004.ActivityDescription = lst.ActivityDescription;
                                        modifiedStageMIP004.RefDocument = lst.RefDocument;
                                        modifiedStageMIP004.RefDocRevNo = lst.RefDocRevNo;
                                        modifiedStageMIP004.PIA = lst.PIA;
                                        modifiedStageMIP004.IAgencyLNT = lst.IAgencyLNT;
                                        modifiedStageMIP004.IAgencyVVPT = lst.IAgencyVVPT;
                                        modifiedStageMIP004.IAgencySRO = lst.IAgencySRO;
                                        modifiedStageMIP004.EditedBy = lst.EditedBy;
                                        modifiedStageMIP004.EditedOn = lst.EditedOn;
                                        modifiedStageMIP004.Status = lst.Status;
                                        db.SaveChanges();
                                        isUpdated = true;
                                    }
                                    #endregion
                                }
                            }
                            else
                            {
                                #region add new stage in MIP HEADER
                                MIP004 newMIP004Stage = new MIP004();
                                newMIP004Stage.HeaderId = item.HeaderId;
                                newMIP004Stage.QualityProject = item.QualityProject;
                                newMIP004Stage.BU = item.BU;
                                newMIP004Stage.Location = item.Location;
                                newMIP004Stage.ComponentNo = item.ComponentNo;
                                newMIP004Stage.OperationNo = lst.OperationNo;
                                newMIP004Stage.LineRevNo = 0;
                                newMIP004Stage.MIPRevNo = item.RevNo == null ? 0 : item.RevNo.Value;
                                if (item.Status == clsImplementationEnum.MIPHeaderStatus.Approved.GetStringValue())
                                    newMIP004Stage.MIPRevNo = newMIP004Stage.MIPRevNo + 1;
                                newMIP004Stage.ActivityDescription = lst.ActivityDescription;
                                newMIP004Stage.RefDocument = lst.RefDocument;
                                newMIP004Stage.RefDocRevNo = lst.RefDocRevNo;
                                newMIP004Stage.PIA = lst.PIA;
                                newMIP004Stage.IAgencyLNT = lst.IAgencyLNT;
                                newMIP004Stage.IAgencyVVPT = lst.IAgencyVVPT;
                                newMIP004Stage.IAgencySRO = lst.IAgencySRO;
                                newMIP004Stage.EditedBy = lst.EditedBy;
                                newMIP004Stage.EditedOn = lst.EditedOn;
                                newMIP004Stage.CreatedBy = lst.CreatedBy;
                                newMIP004Stage.CreatedOn = lst.CreatedOn;
                                newMIP004Stage.Status = clsImplementationEnum.MIPLineStatus.Added.GetStringValue();
                                db.MIP004.Add(newMIP004Stage);
                                db.SaveChanges();
                                isUpdated = true;
                                #endregion
                            }
                        }
                    }
                }
                if (isUpdated)
                {
                    // update MIP HEADER
                    if (item.Status == clsImplementationEnum.MIPHeaderStatus.Approved.GetStringValue())
                    {
                        item.RevNo = item.RevNo + 1;
                    }
                    item.QCApprovedBy = null;
                    item.QCApprovedOn = null;
                    item.QCReturnedBy = null;
                    item.QCReturnedOn = null;
                    item.QCReturnRemarks = null;
                    item.Status = clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue();
                    item.EditedBy = objClsLoginInfo.UserName;
                    item.EditedOn = DateTime.Now;
                    db.SaveChanges();
                }
            }

            #endregion

        }

        [HttpPost]
        public ActionResult ReturnMIPHeader(string strHeader, string strReturnRemarks, string department)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (!string.IsNullOrEmpty(strHeader))
                {
                    int[] headerIds = Array.ConvertAll(strHeader.Split(','), s => int.Parse(s));
                    string[] returnRemarks = strReturnRemarks.Split(',');

                    for (int i = 0; i < headerIds.Length; i++)
                    {
                        if (department == clsImplementationEnum.UserRoleName.PMG2.GetStringValue())
                            ReturnByPMG(headerIds[i], returnRemarks[i]);
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "MIP(s) has been sucessfully returned";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please select atleast one record for return";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        private bool SendForApproval(int headerId)
        {
            MIP001 objMIP001 = db.MIP001.FirstOrDefault(i => i.HeaderId == headerId);
            var lineStatus = new string[] { clsImplementationEnum.MIPHeaderStatus.Draft.GetStringValue(), clsImplementationEnum.MIPHeaderStatus.Returned.GetStringValue() };
            if (objMIP001 != null && db.MIP002.Any(i => i.HeaderId == headerId) && lineStatus.Contains(objMIP001.Status))
            {
                objMIP001.Status = clsImplementationEnum.MIPHeaderStatus.SentForApproval.GetStringValue();
                objMIP001.SubmittedBy = objClsLoginInfo.UserName;
                objMIP001.SubmittedOn = DateTime.Now;

                objMIP001.PMGReturnedBy = null;
                objMIP001.PMGReturnedOn = null;
                objMIP001.PMGReturnRemarks = null;
                db.SaveChanges();

                #region Send Notification
                (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PMG2.GetStringValue(), objMIP001.Project, objMIP001.BU, objMIP001.Location, "MIP: " + objMIP001.DocNo + " has been submitted for your approval", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/MIP/MaintainMIP/Details/" + objMIP001.HeaderId + "?urlForm=a");
                #endregion

                return true;
            }
            return false;
        }

        private bool ReturnByPMG(int headerId, string returnRemark)
        {
            var lineStatus = clsImplementationEnum.MIPHeaderStatus.SentForApproval.GetStringValue();
            MIP001 objMIP001 = db.MIP001.FirstOrDefault(i => i.HeaderId == headerId && i.Status == lineStatus);
            if (objMIP001 != null)
            {
                objMIP001.Status = clsImplementationEnum.MIPHeaderStatus.Returned.GetStringValue();
                objMIP001.PMGReturnRemarks = returnRemark;
                objMIP001.PMGReturnedBy = objClsLoginInfo.UserName;
                objMIP001.PMGReturnedOn = DateTime.Now;
                db.SaveChanges();

                #region Send Notification
                (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PMG3.GetStringValue(), objMIP001.Project, objMIP001.BU, objMIP001.Location, "MIP: " + objMIP001.DocNo + " has been returned", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/MIP/MaintainMIP/Details/" + objMIP001.HeaderId);
                #endregion

                return true;
            }
            return false;
        }

        public bool isMIPHeaderExist(string qualityProject, string project, string bu, string location, string DocNo, string MIPType)
        {
            return db.MIP001.Any(i => i.DocNo.Equals(DocNo, StringComparison.OrdinalIgnoreCase) && i.MIPType.Equals(MIPType, StringComparison.OrdinalIgnoreCase) && i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase) && i.Project.Equals(project, StringComparison.OrdinalIgnoreCase) && i.BU.Equals(bu, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(location, StringComparison.OrdinalIgnoreCase));
        }
        #endregion

        #region Lines Details
        [HttpPost]
        public ActionResult GetMIPLinesPartial(int headerId = 0, string BU = "", string Loc = "", string indextype = "")
        {
            MIP001 objMIP001 = new MIP001();

            if (headerId > 0)
                objMIP001 = db.MIP001.Where(i => i.HeaderId == headerId).FirstOrDefault();
            else
            {
                objMIP001.Location = Loc;
                objMIP001.BU = BU;
            }
            UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;

            ViewBag.PIA = Manager.GetSubCatagories("PIA", objMIP001.BU, objMIP001.Location).Select(i => new CategoryData { Value = i.Description, Code = i.Code, CategoryDescription = i.Description }).ToList();
            ViewBag.AgencyLnT = Manager.GetSubCatagories("MIP Inspection Agency L&T", objMIP001.BU, objMIP001.Location).Select(i => new CategoryData { Value = i.Description, Code = i.Code, CategoryDescription = i.Description }).ToList();
            ViewBag.AgencyVVPT = Manager.GetSubCatagories("MIP Inspection Agency VVPT", objMIP001.BU, objMIP001.Location).Select(i => new CategoryData { Value = i.Description, Code = i.Code, CategoryDescription = i.Description }).ToList();
            ViewBag.AgencySRO = Manager.GetSubCatagories("MIP Inspection Agency SRO", objMIP001.BU, objMIP001.Location).Select(i => new CategoryData { Value = i.Description, Code = i.Code, CategoryDescription = i.Description }).ToList();
            ViewBag.DocNoDesc = Manager.GetCategoryOnlyDescription(objMIP001.DocNo, objMIP001.BU, objMIP001.Location, "MIP_DocNo");
            ViewBag.indextype = indextype;
            if (string.IsNullOrWhiteSpace(indextype))
                ViewBag.indextype = clsImplementationEnum.MIPIndexType.maintain.GetStringValue();

            return PartialView("_GetMIPLinesDataPartial", objMIP001);
        }
        public bool checkExistingRecord(int headerid, string colname, string colvalue, string QualityProject, string Location, string BU)
        {
            bool flag = false;
            if (!string.IsNullOrWhiteSpace(colname) && !string.IsNullOrWhiteSpace(colvalue))
            {
                if (colname == "DocNo")
                {
                    if (db.MIP001.Any(x => x.DocNo.Trim() == colvalue.Trim() && x.HeaderId != headerid && x.QualityProject == QualityProject && x.Location == Location && x.BU == BU))
                        flag = true;
                }
                if (colname == "MIPType")
                {
                    if (db.MIP001.Any(x => x.MIPType.Trim() == colvalue.Trim() && x.HeaderId != headerid && x.QualityProject == QualityProject && x.Location == Location && x.BU == BU))
                        flag = true;
                }
            }
            return flag;
        }
        [HttpPost]
        public ActionResult GetMIPLinesData(JQueryDataTableParamModel param)
        {
            try
            {
                UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();

                int intHeaderId = Convert.ToInt32(param.Headerid);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";
                string indextype = string.Empty;
                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(param.Headerid))
                {
                    whereCondition += " AND HeaderId = " + intHeaderId;
                }
                if (!string.IsNullOrEmpty(param.Department))
                {
                    indextype = param.Department;
                }
                else
                {
                    indextype = clsImplementationEnum.MIPIndexType.maintain.GetStringValue();
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                string[] columnName = { "OperationNo", "RefDocument", "RefDocRevNo", "PIA", "IAgencyLNT", "IAgencyVVPT", "IAgencySRO", "ActivityDescription", "LineRevNo", "Remarks", "Status" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                //else
                //    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);


                var lstResult = db.SP_MIP_GET_LINES_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                MIP001 objMIP001 = db.MIP001.Where(c => c.HeaderId == intHeaderId).FirstOrDefault();

                bool isEditable = ((objMIP001.Status == clsImplementationEnum.MIPHeaderStatus.Draft.GetStringValue() || objMIP001.Status == clsImplementationEnum.MIPHeaderStatus.Returned.GetStringValue()) && objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Initiator.GetStringValue());

                var StageList = (from a in db.QMS002
                                 where a.BU == objMIP001.BU && a.Location == objMIP001.Location
                                 select new { Code = a.StageCode, Desc = a.StageDesc }).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                   Convert.ToString(intHeaderId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "OperationNo", "", "", false, "", false, "6"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "ActivityDescription", "", "", false, "", false, "200"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "RefDocument", "", "", false, "", false, "100"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "RefDocRevNo", "", "", false, "", false, "10"),
                                   GenerateAutoCompleteOnBlur(newRecordId, "txtPIA","","","",false,"","PIA")+""+Helper.GenerateHidden(newRecordId,"PIA"),
                                   GenerateAutoCompleteOnBlur(newRecordId, "txtIAgencyLNT","","","",false,"","IAgencyLNT")+""+Helper.GenerateHidden(newRecordId,"IAgencyLNT"),
                                   GenerateAutoCompleteOnBlur(newRecordId, "txtIAgencyVVPT","","","",false,"","IAgencyVVPT")+""+Helper.GenerateHidden(newRecordId,"IAgencyVVPT"),
                                   GenerateAutoCompleteOnBlur(newRecordId, "txtIAgencySRO","","","",false,"","IAgencySRO")+""+Helper.GenerateHidden(newRecordId,"IAgencySRO"),
                                   "R0",
                                   "R"+objMIP001.RevNo,
                                    Helper.GenerateHTMLTextbox(newRecordId, "Remarks", "", "", false, "", false, "200"),
                                    clsImplementationEnum.MIPLineStatus.Added.GetStringValue(),
                                    Helper.GenerateGridButtonType(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveMIPLine();")
                                };
                var lineAttachStatus = clsImplementationEnum.SeamListInspectionStatus.ReworkNotAttached.GetStringValue();
                var lineDittachStatus = clsImplementationEnum.SeamListInspectionStatus.ReworkAttached.GetStringValue();
                var lineDeleteStatus = clsImplementationEnum.MIPLineStatus.Deleted.GetStringValue();
                var data = (from uc in lstResult
                            select new[]
                            {
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                                uc.OperationNo.ToString("G29"),
                               !IsOffered(uc.LineId,objMIP001.BU,objMIP001.Location,objMIP001.DocNo,objMIP001.MIPType) && isEditable ? Helper.GenerateHTMLTextboxOnChanged(uc.LineId, "ActivityDescription", uc.ActivityDescription, "UpdateMIPLine(this,"+ uc.LineId +","+ uc.HeaderId +")", false, "", (uc.Status == lineDeleteStatus), "200") : uc.ActivityDescription,
                               !IsOffered(uc.LineId,objMIP001.BU,objMIP001.Location,objMIP001.DocNo,objMIP001.MIPType) && isEditable ? Helper.GenerateHTMLTextboxOnChanged(uc.LineId, "RefDocument", uc.RefDocument, "UpdateMIPLine(this,"+ uc.LineId +","+ uc.HeaderId +")", false, "", (uc.Status == lineDeleteStatus), "100") : Convert.ToString(uc.RefDocument),
                               !IsOffered(uc.LineId,objMIP001.BU,objMIP001.Location,objMIP001.DocNo,objMIP001.MIPType) && isEditable ? Helper.GenerateHTMLTextboxOnChanged(uc.LineId, "RefDocRevNo", uc.RefDocRevNo, "UpdateMIPLine(this,"+ uc.LineId +","+ uc.HeaderId +")", false, "", (uc.Status == lineDeleteStatus), "10") : Convert.ToString(uc.RefDocRevNo),
                               !IsOffered(uc.LineId,objMIP001.BU,objMIP001.Location,objMIP001.DocNo,objMIP001.MIPType) && isEditable ? GenerateAutoCompleteOnBlur(uc.LineId,"txtPIA",Manager.GetCategoryOnlyDescription(uc.PIA,objMIP001.BU,objMIP001.Location  ,"PIA" ),"UpdateMIPLine(this,"+ uc.LineId +","+ uc.HeaderId +")","",false,"","PIA",(uc.PIA), (uc.Status == lineDeleteStatus))+""+Helper.GenerateHidden(uc.LineId,"PIA",(uc.PIA)) :Manager.GetCategoryOnlyDescription(uc.PIA,objMIP001.BU,objMIP001.Location  ,"PIA" ),
                               !IsOffered(uc.LineId,objMIP001.BU,objMIP001.Location,objMIP001.DocNo,objMIP001.MIPType) && isEditable ? GenerateAutoCompleteOnBlur(uc.LineId,"txtIAgencyLNT", Manager.GetCategoryOnlyDescription(uc.IAgencyLNT,objMIP001.BU,objMIP001.Location  ,"MIP Inspection Agency L&T" ),"UpdateMIPLine(this,"+ uc.LineId +","+ uc.HeaderId +")","",false,"","IAgencyLNT",uc.IAgencyLNT,(uc.Status == lineDeleteStatus))+""+Helper.GenerateHidden(uc.LineId,"IAgencyLNT",uc.IAgencyLNT)  :     Manager.GetCategoryOnlyDescription(uc.IAgencyLNT,objMIP001.BU,objMIP001.Location  ,"MIP Inspection Agency L&T" ),
                               !IsOffered(uc.LineId,objMIP001.BU,objMIP001.Location,objMIP001.DocNo,objMIP001.MIPType) && isEditable ? GenerateAutoCompleteOnBlur(uc.LineId,"txtIAgencyVVPT",Manager.GetCategoryOnlyDescription(uc.IAgencyVVPT,objMIP001.BU,objMIP001.Location,"MIP Inspection Agency VVPT"),"UpdateMIPLine(this,"+ uc.LineId +","+ uc.HeaderId +")","",false,"","IAgencyVVPT",uc.IAgencyVVPT,(uc.Status == lineDeleteStatus))+""+Helper.GenerateHidden(uc.LineId,"IAgencyVVPT",uc.IAgencyVVPT)  : Manager.GetCategoryOnlyDescription(uc.IAgencyVVPT,objMIP001.BU,objMIP001.Location,"MIP Inspection Agency VVPT"),
                               !IsOffered(uc.LineId,objMIP001.BU,objMIP001.Location,objMIP001.DocNo,objMIP001.MIPType) && isEditable ? GenerateAutoCompleteOnBlur(uc.LineId,"txtIAgencySRO", Manager.GetCategoryOnlyDescription(uc.IAgencySRO,objMIP001.BU,objMIP001.Location  ,"MIP Inspection Agency SRO"),"UpdateMIPLine(this,"+ uc.LineId +","+ uc.HeaderId +")","",false,"","IAgencySRO",uc.IAgencySRO,(uc.Status == lineDeleteStatus))+""+Helper.GenerateHidden(uc.LineId,"IAgencySRO",uc.IAgencySRO)  :      Manager.GetCategoryOnlyDescription(uc.IAgencySRO,objMIP001.BU,objMIP001.Location  ,"MIP Inspection Agency SRO"),
                               "R"+uc.LineRevNo,
                               "R"+uc.MIPRevNo,
                               !IsOffered(uc.LineId,objMIP001.BU,objMIP001.Location,objMIP001.DocNo,objMIP001.MIPType) && isEditable ? Helper.GenerateHTMLTextboxOnChanged(uc.LineId, "Remarks", uc.Remarks, "UpdateMIPLine(this,"+ uc.LineId +","+ uc.HeaderId +")", false, "", (uc.Status == lineDeleteStatus), "200") : Convert.ToString(uc.Remarks),
                               Convert.ToString(uc.Status),
                               "<nobr>"
                               + Helper.HTMLActionString(uc.LineId, "Delete", "Delete Operation", "fa fa-trash-o", "DeleteMIPLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +")", "", IsOffered(uc.LineId,objMIP001.BU,objMIP001.Location,objMIP001.DocNo,objMIP001.MIPType) || uc.LineRevNo != 0 || !isEditable || (uc.Status == lineDeleteStatus) )
                               + "</nobr>"

                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public bool IsOffered(int lineid, string bu, string location, string docno, string miptype)
        {
            bool flag = false;
            MIP002 objMIP002 = db.MIP002.Where(x => x.LineId == lineid).FirstOrDefault();
            MIP003 objmip003 = db.MIP003.Where(x => x.QualityProject == objMIP002.QualityProject && x.BU == bu && x.Location == location && x.DocNo == docno && x.MIPType == miptype).FirstOrDefault();
            if (objmip003 != null)
            {
                //var lineReadyToOffer = clsImplementationEnum.MIPInspectionStatus.ReadyToOffer.GetStringValue();
                var obj = db.MIP004.Where(x => x.HeaderId == objmip003.HeaderId && x.OperationNo == objMIP002.OperationNo).FirstOrDefault();
                if (obj != null)
                {
                    if (!string.IsNullOrWhiteSpace(obj.InspectionStatus))
                    {
                        flag = true;
                    }
                }
            }
            return flag;
        }

        [HttpPost]
        public ActionResult SaveMIPLine(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            MIP002 objMIP002 = new MIP002();
            int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    MIP001 objMIP001 = db.MIP001.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    decimal intOprNo = !string.IsNullOrEmpty(fc["OperationNo" + newRowIndex]) ? Convert.ToDecimal(fc["OperationNo" + newRowIndex]) : 0;
                    string strActivityDescription = !string.IsNullOrEmpty(fc["ActivityDescription" + newRowIndex]) ? Convert.ToString(fc["ActivityDescription" + newRowIndex]).Trim() : string.Empty;
                    string strRefDocument = !string.IsNullOrEmpty(fc["RefDocument" + newRowIndex]) ? Convert.ToString(fc["RefDocument" + newRowIndex]).Trim() : string.Empty;
                    string strDocRevNo = !string.IsNullOrEmpty(fc["RefDocRevNo" + newRowIndex]) ? Convert.ToString(fc["RefDocRevNo" + newRowIndex]).Trim() : string.Empty;
                    string strPIA = !string.IsNullOrEmpty(fc["PIA" + newRowIndex]) ? Convert.ToString(fc["PIA" + newRowIndex]).Trim() : string.Empty;
                    string strIAgencyLNT = !string.IsNullOrEmpty(fc["IAgencyLNT" + newRowIndex]) ? Convert.ToString(fc["IAgencyLNT" + newRowIndex]).Trim() : string.Empty;
                    string strIAgencyVVPT = !string.IsNullOrEmpty(fc["IAgencyVVPT" + newRowIndex]) ? Convert.ToString(fc["IAgencyVVPT" + newRowIndex]).Trim() : string.Empty;
                    string strIAgencySRO = !string.IsNullOrEmpty(fc["IAgencySRO" + newRowIndex]) ? Convert.ToString(fc["IAgencySRO" + newRowIndex]).Trim() : string.Empty;
                    string strRemarks = !string.IsNullOrEmpty(fc["Remarks" + newRowIndex]) ? Convert.ToString(fc["Remarks" + newRowIndex]).Trim() : string.Empty;

                    if (isMIPLIneExist(objMIP001.HeaderId, intOprNo))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = intOprNo +" Operation No is already exist";
                    }
                    else
                    {
                        MIP002 objParent = db.MIP002.FirstOrDefault(c => c.HeaderId == objMIP001.HeaderId);

                        #region Add New MIP Line
                        objMIP002.HeaderId = objMIP001.HeaderId;
                        objMIP002.QualityProject = objMIP001.QualityProject;
                        objMIP002.MIPRevNo = objMIP001.RevNo.Value;
                        objMIP002.OperationNo = intOprNo;
                        objMIP002.ActivityDescription = strActivityDescription;
                        objMIP002.PIA = strPIA;
                        objMIP002.IAgencyLNT = strIAgencyLNT;
                        objMIP002.IAgencyVVPT = strIAgencyVVPT;
                        objMIP002.IAgencySRO = strIAgencySRO;
                        objMIP002.Remarks = strRemarks;
                        objMIP002.RefDocument = strRefDocument;
                        objMIP002.RefDocRevNo = strDocRevNo;
                        objMIP002.LineRevNo = 0;
                        objMIP002.CreatedBy = objClsLoginInfo.UserName;
                        objMIP002.CreatedOn = DateTime.Now;
                        objMIP002.Status = clsImplementationEnum.MIPLineStatus.Added.GetStringValue();
                        //if (objParent != null)
                        //{
                        //    objMIP002.ParentLineId = objParent.LineId;
                        //}
                        db.MIP002.Add(objMIP002);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Operation added successfully";
                        objResponseMsg.HeaderId = objMIP001.HeaderId;
                        objResponseMsg.HeaderStatus = objMIP001.Status;
                        objResponseMsg.RevNo = Convert.ToString(objMIP001.RevNo);
                        #endregion
                    }
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateMIPLine(int lineId, int headerId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            MIP002 objMIP002 = new MIP002();
            try
            {
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    objMIP002 = db.MIP002.Where(i => i.LineId == lineId).FirstOrDefault();
                    if (objMIP002.MIPRevNo > 0)
                    {
                        var approved = clsImplementationEnum.MIPLineStatus.Approved.GetStringValue();
                        var objMIP002_log = db.MIP002_Log.FirstOrDefault(w => w.LineId == lineId && w.Status == approved);
                        if (objMIP002_log != null)
                        {
                            objMIP002.LineRevNo = objMIP002_log.LineRevNo + 1;
                            objMIP002.Status = clsImplementationEnum.MIPLineStatus.Modified.GetStringValue();
                        }
                    }
                    db.SP_MIP_UPDATE_LINE_COLUMN(lineId, columnName, columnValue);

                    objMIP002.EditedBy = objClsLoginInfo.UserName;
                    objMIP002.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.LineStatus = objMIP002.Status;
                    objResponseMsg.Value = objMIP002.LineRevNo + "";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteMIPLine(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                MIP002 objMIP002 = db.MIP002.FirstOrDefault(c => c.LineId == lineId);

                if (objMIP002 != null)
                {
                    var approvedStatus = clsImplementationEnum.MIPLineStatus.Approved.GetStringValue();
                    var objMIP002_Log = db.MIP002_Log.FirstOrDefault(f => f.LineId == lineId && f.Status == approvedStatus);
                    if (objMIP002_Log != null)
                    {
                        objMIP002.Status = clsImplementationEnum.MIPLineStatus.Deleted.GetStringValue();
                        objMIP002.EditedBy = objClsLoginInfo.UserName;
                        objMIP002.EditedOn = DateTime.Now;
                    }
                    else
                    {
                        db.MIP002.Remove(objMIP002);
                    }
                    db.SaveChanges();
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Operation deleted successfully";

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public bool isMIPLIneExist(int headerId, decimal oprNo)
        {
            var lineStatus = clsImplementationEnum.MIPLineStatus.Deleted.GetStringValue();
            return db.MIP002.Any(i => i.HeaderId == headerId && i.OperationNo == oprNo && i.Status != lineStatus);
        }

        #endregion

        #region History Detail Page

        [HttpPost]
        public ActionResult GetHistoryDataPartial(int HeaderId)
        {
            MIP001_Log objMIP001_Log = new MIP001_Log();
            objMIP001_Log = db.MIP001_Log.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            return PartialView("_GetMIPHistoryDataPartial", objMIP001_Log);
        }

        public ActionResult HistoryDetails(int? id)
        {
            MIP001_Log objMIP001 = new MIP001_Log();

            if (id > 0)
            {
                objMIP001 = db.MIP001_Log.Where(i => i.Id == id).FirstOrDefault();
                objMIP001.Project = db.COM001.Where(i => i.t_cprj.Equals(objMIP001.Project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objMIP001.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objMIP001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objMIP001.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objMIP001.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                ViewBag.RevNo = "R" + Convert.ToString(objMIP001.RevNo);

                ViewBag.isReturned = false;
                if (objMIP001.PMGReturnedBy != null)
                {
                    ViewBag.isReturned = true;
                    ViewBag.ReturnBy = objMIP001.PMGReturnedBy + " - " + Manager.GetUserNameFromPsNo(objMIP001.PMGReturnedBy);
                    ViewBag.ReturnDate = objMIP001.PMGReturnedOn.Value.ToString("dd/MM/yyyy hh:mm tt");
                }

                ViewBag.isReleased = false;
            }

            return View(objMIP001);
        }

        [HttpPost]
        public ActionResult GetMIPHistoryHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(param.Headerid))
                {
                    whereCondition += " AND MIP001.HeaderId = " + param.Headerid;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                string[] columnName = { "DocNo", "MIPType", "RevNo", "PMGReturnRemarks", "Status" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                var lstResult = db.SP_MIP_GET_HISTORY_HEADER_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                            {
                               Convert.ToString(uc.DocNo),
                               Convert.ToString(uc.MIPType),
                               Convert.ToString(uc.RevNo),
                               Convert.ToString(uc.Status),
                               "<a target='_blank' href='"+ WebsiteURL + "/MIP/MaintainMIP/HistoryDetails?id=" + uc.Id + "'><i style='margin-right:10px;' class='fa fa-eye'></i></a>" +
                               Helper.HTMLActionString(uc.HeaderId,"Timeline","Timeline","fa fa-clock-o", "ShowTimeline(\"/MIP/MaintainMIP/ShowHistoryTimeline?HeaderID="+ uc.Id +"\");"),
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetMIPHistoryLinesData(JQueryDataTableParamModel param, string RefId)
        {
            try
            {
                int intRefId = Convert.ToInt32(RefId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(RefId))
                {
                    whereCondition += " AND RefId = " + intRefId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                string[] columnName = { "OperationNo", "RefDocument", "RefDocRevNo", "PIA", "IAgencyLNT", "IAgencyVVPT", "IAgencySRO", "ActivityDescription", "LineRevNo", "Status" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);

                var lstResult = db.SP_MIP_GET_HISTORY_LINES_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                MIP001_Log objMIP001 = db.MIP001_Log.Where(c => c.Id == intRefId).FirstOrDefault();

                var data = (from uc in lstResult
                            select new[]
                            {
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               uc.OperationNo.ToString("G29"),
                               Convert.ToString(uc.ActivityDescription),
                               Convert.ToString(uc.RefDocument),
                               Convert.ToString(uc.RefDocRevNo),
                               Convert.ToString(Manager.GetCategoryOnlyDescription(uc.PIA, objMIP001.BU, objMIP001.Location, "PIA")),
                               Convert.ToString(Manager.GetCategoryOnlyDescription(uc.IAgencyLNT, objMIP001.BU, objMIP001.Location, "MIP Inspection Agency L&T")   ),
                               Convert.ToString( Manager.GetCategoryOnlyDescription(uc.IAgencyVVPT, objMIP001.BU, objMIP001.Location, "MIP Inspection Agency VVPT")),
                               Convert.ToString(Manager.GetCategoryOnlyDescription(uc.IAgencySRO, objMIP001.BU, objMIP001.Location, "MIP Inspection Agency SRO")   ),
                                Convert.ToString("R" + uc.LineRevNo),
                                Convert.ToString("R" + uc.MIPRevNo),
                               Convert.ToString(uc.Remarks),
                               Convert.ToString(uc.Status),
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = new string[]{ }
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Timeline
        //public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        //{
        //    TimelineViewModel model = new TimelineViewModel();

        //    model.TimelineTitle = "MIP Timeline";

        //    if (LineId > 0)
        //    {
        //        model.Title = "MIP Lines";
        //        MIP002 objMIP002 = db.MIP002.Where(x => x.LineId == LineId).FirstOrDefault();
        //        model.CreatedBy = objMIP002.CreatedBy != null ? Manager.GetUserNameFromPsNo(objMIP002.CreatedBy) : null;
        //        model.CreatedOn = objMIP002.CreatedOn;
        //        model.EditedBy = objMIP002.EditedBy != null ? Manager.GetUserNameFromPsNo(objMIP002.EditedBy) : null;
        //        model.EditedOn = objMIP002.EditedOn;
        //    }
        //    else
        //    {
        //        model.Title = "MIP Header";
        //        MIP001 objMIP001 = db.MIP001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
        //        model.CreatedBy = objMIP001.CreatedBy != null ? Manager.GetUserNameFromPsNo(objMIP001.CreatedBy) : null;
        //        model.CreatedOn = objMIP001.CreatedOn;
        //        model.EditedBy = objMIP001.EditedBy != null ? Manager.GetUserNameFromPsNo(objMIP001.EditedBy) : null;
        //        model.EditedOn = objMIP001.EditedOn;
        //        model.SubmittedBy = objMIP001.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objMIP001.SubmittedBy) : null;
        //        model.SubmittedOn = objMIP001.SubmittedOn;
        //        if (objMIP001.WEReturnedBy != null)
        //        {
        //            model.ReturnedBy = Manager.GetUserNameFromPsNo(objMIP001.WEReturnedBy);
        //            model.ReturnedOn = objMIP001.WEReturnedOn;
        //        }
        //        else if (objMIP001.QCReturnedBy != null)
        //        {
        //            model.ReturnedBy = Manager.GetUserNameFromPsNo(objMIP001.QCReturnedBy);
        //            model.ReturnedOn = objMIP001.QCReturnedOn;
        //        }
        //        else if (objMIP001.CustomerReturnedBy != null)
        //        {
        //            model.ReturnedBy = Manager.GetUserNameFromPsNo(objMIP001.CustomerReturnedBy);
        //            model.ReturnedOn = objMIP001.CustomerReturnedOn;
        //        }
        //        else if (objMIP001.PMGReturnedBy != null)
        //        {
        //            model.ReturnedBy = Manager.GetUserNameFromPsNo(objMIP001.PMGReturnedBy);
        //            model.ReturnedOn = objMIP001.PMGReturnedOn;
        //        }
        //        else
        //        {
        //            model.ReturnedBy = null;
        //            model.ReturnedOn = null;
        //        }
        //        model.WEApprovedBy = objMIP001.WEApprovedBy != null ? Manager.GetUserNameFromPsNo(objMIP001.WEApprovedBy) : null;
        //        model.WEApprovedOn = objMIP001.WEApprovedOn;
        //        model.QCApprovedBy = objMIP001.QCApprovedBy != null ? Manager.GetUserNameFromPsNo(objMIP001.QCApprovedBy) : null;
        //        model.QCApprovedOn = objMIP001.QCApprovedOn;
        //        model.CustomerApprovedBy = objMIP001.CustomerApprovedBy != null ? Manager.GetUserNameFromPsNo(objMIP001.CustomerApprovedBy) : null;
        //        model.CustomerApprovedOn = objMIP001.CustomerApprovedOn;
        //        model.PMGReleaseBy = objMIP001.PMGReleaseBy != null ? Manager.GetUserNameFromPsNo(objMIP001.PMGReleaseBy) : null;
        //        model.PMGReleaseOn = objMIP001.PMGReleaseOn;

        //    }

        //    return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        //}

        public ActionResult ShowHistoryTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();

            model.TimelineTitle = "MIP Timeline";

            if (LineId > 0)
            {
                model.Title = "MIP Lines";
                MIP002_Log objMIP002 = db.MIP002_Log.Where(x => x.Id == LineId).FirstOrDefault();
                model.CreatedBy = objMIP002.CreatedBy != null ? Manager.GetUserNameFromPsNo(objMIP002.CreatedBy) : null;
                model.CreatedOn = objMIP002.CreatedOn;
                model.EditedBy = objMIP002.EditedBy != null ? Manager.GetUserNameFromPsNo(objMIP002.EditedBy) : null;
                model.EditedOn = objMIP002.EditedOn;
            }
            else
            {
                model.Title = "MIP Header";
                MIP001_Log objMIP001 = db.MIP001_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objMIP001.CreatedBy != null ? Manager.GetUserNameFromPsNo(objMIP001.CreatedBy) : null;
                model.CreatedOn = objMIP001.CreatedOn;
                model.EditedBy = objMIP001.EditedBy != null ? Manager.GetUserNameFromPsNo(objMIP001.EditedBy) : null;
                model.EditedOn = objMIP001.EditedOn;
                model.SubmittedBy = objMIP001.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objMIP001.SubmittedBy) : null;
                model.SubmittedOn = objMIP001.SubmittedOn;
                if (objMIP001.PMGReturnedBy != null)
                {
                    model.ReturnedBy = Manager.GetUserNameFromPsNo(objMIP001.PMGReturnedBy);
                    model.ReturnedOn = objMIP001.PMGReturnedOn;
                }
                else
                {
                    model.ReturnedBy = null;
                    model.ReturnedOn = null;
                }
                model.PMGReleaseBy = objMIP001.PMGApprovedBy != null ? Manager.GetUserNameFromPsNo(objMIP001.PMGApprovedBy) : null;
                model.PMGReleaseOn = objMIP001.PMGApprovedOn;

            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        #endregion

        #region Common Methods

        //[HttpPost]
        //public ActionResult GetQualityProjectswiseMIP(string qualityProject, string location)
        //{
        //    var lstQualityProject = db.MIP001.Where(i => i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(location, StringComparison.OrdinalIgnoreCase)).OrderBy(i => i.DocNo).Select(i => new { id = i.DocNo, rev = i.RevNo }).ToList();

        //    var objData = new
        //    {
        //        count = lstQualityProject.Count,
        //        lstQualityProject = lstQualityProject
        //    };

        //    return Json(objData, JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        public ActionResult GetQualityProjects(string term)
        {
            string username = objClsLoginInfo.UserName;
            var lstATHLocation = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.Location).Distinct().ToList();
            var lstCOMLocation = db.COM003.Where(i => i.t_psno.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_loca).Distinct().ToList();

            var lstCOMBU = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.BU).Distinct().ToList();

            lstCOMLocation = lstATHLocation.Union(lstCOMLocation).ToList();

            List<Projects> lstQualityProject = new List<Projects>();
            if (!string.IsNullOrEmpty(term))
            {
                lstQualityProject = db.QMS010.Where(i => i.QualityProject.ToLower().Contains(term.ToLower())
                                                                 && lstCOMLocation.Contains(i.Location)
                                                                 && lstCOMBU.Contains(i.BU)
                                                                 && db.QMS001.Any(c => c.Project == i.Project && c.Location == i.Location && c.BU == i.BU)
                                                               ).Select(i => new Projects { Value = i.QualityProject, projectCode = i.QualityProject }).Distinct().ToList();//&& i.CreatedBy.Equals(username,StringComparison.OrdinalIgnoreCase)
            }
            else
            {
                lstQualityProject = db.QMS010.Where(i => lstCOMLocation.Contains(i.Location)
                                                        && lstCOMBU.Contains(i.BU)
                                                        && db.QMS001.Any(c => c.Project == i.Project && c.Location == i.Location && c.BU == i.BU)
                                                        ).Select(i => new Projects { Value = i.QualityProject, projectCode = i.QualityProject }).Take(10).Distinct().ToList();
            }

            return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
        }

        //public bool isMIPProject(string Project, string Location, string BU)
        //{
        //    return db.QMS001.Any(c => c.Project == Project && c.Location == Location && c.BU == BU && c.IsMIP == true);
        //}

        [HttpPost]
        public ActionResult GetQualityProjectWiseProjectBUDetail(string qualityProject)
        {
            QualityProjectsDetails objProjectsDetails = new QualityProjectsDetails();
            var project = db.QMS010.Where(i => i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase)).Select(i => i.Project).FirstOrDefault();

            objProjectsDetails.ProjectCode = project;
            objProjectsDetails.projectDescription = db.COM001.Where(i => i.t_cprj.Equals(project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + "-" + i.t_dsca).FirstOrDefault();// project;

            var ProjectWiseBULocation = Manager.getProjectWiseBULocation(project);

            objProjectsDetails.BUCode = ProjectWiseBULocation.QCPBU.Split('-')[0].Trim();
            objProjectsDetails.BUDescription = ProjectWiseBULocation.QCPBU;

            objProjectsDetails.LocationCode = objClsLoginInfo.Location;
            objProjectsDetails.LocationDescription = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objClsLoginInfo.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();

            return Json(objProjectsDetails, JsonRequestBehavior.AllowGet);
        }

        public UserRoleAccessDetails GetUserAccessRights()
        {
            UserRoleAccessDetails objUserRoleAccessDetails = new UserRoleAccessDetails();

            try
            {
                var role = (from a in db.ATH001
                            join b in db.ATH004 on a.Role equals b.Id
                            where a.Employee.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                            select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();

                if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG2.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.PMG2.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Approver.GetStringValue();
                }
                else if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.PMG3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Initiator.GetStringValue();
                }
                else
                {
                    objUserRoleAccessDetails.UserRole = "";
                    objUserRoleAccessDetails.UserDesignation = "";
                }
            }
            catch
            {

            }
            return objUserRoleAccessDetails;
        }

        //public string GetProject(string qualityProject)
        //{
        //    return db.QMS010.Where(i => i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase)).Select(i => i.Project).FirstOrDefault();
        //}
        //public string GetBU(string qualityProject)
        //{
        //    var project = db.QMS010.Where(i => i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase)).Select(i => i.Project).FirstOrDefault();
        //    return db.COM001.Where(i => i.t_cprj == project).FirstOrDefault().t_entu;
        //}
        //public string getCategoryDesc(string Key, string BU, string loc, string code)
        //{
        //    string catDesc = (from glb002 in db.GLB002
        //                      join glb001 in db.GLB001 on glb002.Category equals glb001.Id
        //                      where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true && BU.Contains(glb002.BU) && loc.Equals(glb002.Location) && code.Equals(glb002.Code)
        //                      select glb002.Description).FirstOrDefault();
        //    return catDesc;
        //}

        //public string HTMLActionString(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", string onHrefURL = "", bool isDisable = false)
        //{
        //    string htmlControl = "";
        //    string inputID = buttonName + "" + rowId.ToString();
        //    string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";
        //    string onHref = !string.IsNullOrEmpty(onHrefURL) ? "href='" + onHrefURL + "'" : "";

        //    if (isDisable)
        //    {
        //        htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer; opacity:0.3; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";
        //    }
        //    else
        //    {
        //        htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
        //    }

        //    if (!string.IsNullOrEmpty(onHref))
        //    {
        //        htmlControl = "<a " + onHref + " >" + htmlControl + "</a>";
        //    }

        //    return htmlControl;
        //}
        public string GenerateAutoCompleteOnBlur(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", string oldValue = "", bool disabled = false)
        {
            string strAutoComplete = string.Empty;
            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick=" + onClickMethod + "" : "";
            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' data-lineid='" + rowId + "' hdElement='" + hdElementId + "' value='" + inputValue + "' data-oldvalue='" + oldValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + " " + (disabled ? "disabled" : "") + " />";

            return strAutoComplete;
        }
        //public string GenerateAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false)
        //{
        //    string strAutoComplete = string.Empty;
        //    string inputID = columnName + "" + rowId.ToString();
        //    string hdElementId = hdElement + "" + rowId.ToString();
        //    string inputName = columnName;
        //    string inputValue = columnValue;
        //    string className = "autocomplete form-control";
        //    string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
        //    string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick=" + onClickMethod + "" : "";
        //    strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' data-lineid='" + rowId + "' hdElement='" + hdElement + "' value='" + inputValue + "' data-oldvalue='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + " " + (disabled ? "disabled" : "") + " />";

        //    return strAutoComplete;
        //}

        public string GenerateRemark(int headerId, string columnName, bool isEditable = false)
        {
            MIP001 objMIP001 = db.MIP001.FirstOrDefault(c => c.HeaderId == headerId);

            string remarkValue = (!string.IsNullOrWhiteSpace(objMIP001.PMGReturnedBy) ? objMIP001.PMGReturnRemarks : "");

            if (isEditable)
            {
                return Helper.GenerateTextbox(objMIP001.HeaderId, columnName, remarkValue, "", false, "", "100");
            }
            else
            {
                return remarkValue;
            }

        }
        public class ResponceMsgWithMultiValue : clsHelper.ResponseMsg
        {
            public string pendingMIP { get; set; }
            public string submittedMIP { get; set; }
            public bool isIdenticalProjectEditable { get; set; }
        }
        #endregion

        #region ExcelHelper
        [SessionExpireFilter]
        public ActionResult ImportExcel(HttpPostedFileBase upload, int aHeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            MIP001 objImport = db.MIP001.FirstOrDefault(f => f.HeaderId == aHeaderId);
            if (upload != null && objImport != null)
            {
                if (Path.GetExtension(upload.FileName).ToLower() == ".xlsx" || Path.GetExtension(upload.FileName).ToLower() == ".xls")
                {
                    ExcelPackage package = new ExcelPackage(upload.InputStream);
                    ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();

                    DataTable dtHeaderExcel = new DataTable();
                    List<CategoryData> errors = new List<CategoryData>();
                    string Approved = clsImplementationEnum.MIPHeaderStatus.Approved.GetStringValue();
                    string Draft = clsImplementationEnum.MIPHeaderStatus.Draft.GetStringValue();
                    string Returned = clsImplementationEnum.MIPHeaderStatus.Returned.GetStringValue();
                    bool isError;

                    DataSet ds = ToChechValidation(package, objImport, out isError);
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Record Found!";
                    }
                    else if (!isError)
                    {
                        try
                        {
                            foreach (DataRow item in ds.Tables[0].Rows)
                            {
                                var objMIP002 = new MIP002();
                                string PIA = item.Field<string>("PIA");
                                string IAgencyLNT = item.Field<string>("IAgencyLNT");
                                string IAgencyVVPT = item.Field<string>("IAgencyVVPT");
                                string IAgencySRO = item.Field<string>("IAgencySRO");
                                decimal OperationNo = 0;
                                Decimal.TryParse(item.Field<string>("OperationNo"), out OperationNo);
                                string ActivityDescription = item.Field<string>("ActivityDescription");
                                string RefDocument = item.Field<string>("RefDocument");
                                string RefDocRevNo = item.Field<string>("RefDocRevNo");
                                string LineRevNo = item.Field<string>("LineRevNo");
                                string MIPRevNo = item.Field<string>("MIPRevNo");
                                string Remarks = item.Field<string>("Remarks");
                                string Status = item.Field<string>("Status");

                                #region Add New MIP Master Operations
                                objMIP002.HeaderId = objImport.HeaderId;
                                objMIP002.QualityProject = objImport.QualityProject;
                                objMIP002.OperationNo = OperationNo;
                                objMIP002.ActivityDescription = ActivityDescription;
                                objMIP002.RefDocument = RefDocument;
                                objMIP002.RefDocRevNo = RefDocRevNo;
                                objMIP002.PIA = PIA;
                                objMIP002.IAgencyLNT = IAgencyLNT;
                                objMIP002.IAgencyVVPT = IAgencyVVPT;
                                objMIP002.IAgencySRO = IAgencySRO;
                                objMIP002.LineRevNo = 0;
                                objMIP002.MIPRevNo = objImport.RevNo.Value;
                                objMIP002.Remarks = Remarks;
                                objMIP002.Status = clsImplementationEnum.MIPLineStatus.Added.GetStringValue();
                                objMIP002.CreatedBy = objClsLoginInfo.UserName;
                                objMIP002.CreatedOn = DateTime.Now;

                                db.MIP002.Add(objMIP002);
                                db.SaveChanges();
                                #endregion
                            }
                            db.SaveChanges();

                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "MIP Operation added successfully";
                        }
                        catch (Exception ex)
                        {
                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        }
                    }
                    else
                    {
                        objResponseMsg = ErrorExportToExcel(ds);
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Excel file is not valid";
                }
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please enter valid Project no.";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponceMsgWithFileName ErrorExportToExcel(DataSet ds)
        {
            clsHelper.ResponceMsgWithFileName objResponseMsg = new clsHelper.ResponceMsgWithFileName();
            try
            {
                MemoryStream ms = new MemoryStream();
                using (FileStream fs = System.IO.File.OpenRead(Server.MapPath("~/Resources/MIP/MIP Operation Excel Template.xlsx")))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();
                        int i = 2;
                        foreach (DataRow item in ds.Tables[0].Rows)
                        {
                            //OperationNo
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("OperationNoErrorMsg")))
                            {
                                excelWorksheet.Cells[i, 1].Value = item.Field<string>(0) + " (Note: " + item.Field<string>("OperationNoErrorMsg") + " )";
                                excelWorksheet.Cells[i, 1].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 1].Value = item.Field<string>(0); }
                            //ActivityDescription
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("ActivityDescriptionErrorMsg")))
                            {
                                excelWorksheet.Cells[i, 2].Value = item.Field<string>(1) + " (Note: " + item.Field<string>("ActivityDescriptionErrorMsg").ToString() + " )";
                                excelWorksheet.Cells[i, 2].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 2].Value = item.Field<string>(1); }
                            //RefDocument
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("RefDocumentErrorMsg")))
                            {
                                excelWorksheet.Cells[i, 3].Value = item.Field<string>(2) + " (Note: " + item.Field<string>("RefDocumentErrorMsg").ToString() + " )";
                                excelWorksheet.Cells[i, 3].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 3].Value = item.Field<string>(2); }
                            //RefDocRevNo
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("RefDocRevNoErrorMsg")))
                            {
                                excelWorksheet.Cells[i, 4].Value = item.Field<string>(3) + " (Note: " + item.Field<string>("RefDocRevNoErrorMsg").ToString() + " )";
                                excelWorksheet.Cells[i, 4].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 4].Value = item.Field<string>(3); }
                            //PIA
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("PIAErrorMsg")))
                            {
                                excelWorksheet.Cells[i, 5].Value = item.Field<string>(4) + " (Note: " + item.Field<string>("PIAErrorMsg").ToString() + " )";
                                excelWorksheet.Cells[i, 5].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 5].Value = item.Field<string>(4); }
                            //IAgencyLNT
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("IAgencyLNTErrorMsg")))
                            {
                                excelWorksheet.Cells[i, 6].Value = item.Field<string>(5) + " (Note: " + item.Field<string>("IAgencyLNTErrorMsg").ToString() + " )";
                                excelWorksheet.Cells[i, 6].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 6].Value = item.Field<string>(5); }
                            //IAgencyVVPT
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("IAgencyVVPTErrorMsg")))
                            {
                                excelWorksheet.Cells[i, 7].Value = item.Field<string>(6) + " (Note: " + item.Field<string>("IAgencyVVPTErrorMsg").ToString() + " )";
                                excelWorksheet.Cells[i, 7].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 7].Value = item.Field<string>(6); }
                            //IAgencySRO
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("IAgencySROErrorMsg")))
                            {
                                excelWorksheet.Cells[i, 8].Value = item.Field<string>(7) + " (Note: " + item.Field<string>("IAgencySROErrorMsg").ToString() + " )";
                                excelWorksheet.Cells[i, 8].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 8].Value = item.Field<string>(7); }
                            //LineRevNo
                            excelWorksheet.Cells[i, 9].Value = item.Field<string>(8);
                            //MIPRevNo
                            excelWorksheet.Cells[i, 10].Value = item.Field<string>(9);
                            //Remarks
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("RemarksErrorMsg")))
                            {
                                excelWorksheet.Cells[i, 11].Value = item.Field<string>(10) + " (Note: " + item.Field<string>("RemarksErrorMsg").ToString() + " )";
                                excelWorksheet.Cells[i, 11].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 11].Value = item.Field<string>(10); }
                            //Status
                            excelWorksheet.Cells[i, 12].Value = item.Field<string>(11);
                            i++;
                        }
                        excelPackage.SaveAs(ms);
                    }
                }

                ms.Position = 0;

                string fileName;
                string excelFilePath = Helper.ExcelFilePath(objClsLoginInfo.UserName, out fileName);

                Byte[] bin = ms.ToArray();
                System.IO.File.WriteAllBytes(excelFilePath, bin);

                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error";
                objResponseMsg.FileName = fileName;
                return objResponseMsg;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }

        public DataSet ToChechValidation(ExcelPackage package, MIP001 objImport, out bool isError)
        {
            ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();
            DataTable dtHeaderExcel = new DataTable();
            isError = false;
            int notesIndex = 0;
            try
            {
                #region Read Lines data from excel 
                foreach (var firstRowCell in excelWorksheet.Cells[1, 1, 1, excelWorksheet.Dimension.End.Column])
                {
                    dtHeaderExcel.Columns.Add(firstRowCell.Text);
                }

                for (var rowNumber = 2; rowNumber <= excelWorksheet.Dimension.End.Row; rowNumber++)
                {
                    var row = excelWorksheet.Cells[rowNumber, 1, rowNumber, excelWorksheet.Dimension.End.Column];
                    var newRow = dtHeaderExcel.NewRow();
                    if (excelWorksheet.Cells[rowNumber, 1].Value != null)
                    {
                        foreach (var cell in row)
                        {
                            newRow[cell.Start.Column - 1] = cell.Text;
                        }
                    }
                    else
                    {
                        notesIndex = rowNumber + 1;
                        break;
                    }

                    dtHeaderExcel.Rows.Add(newRow);
                }
                #endregion

                #region Validation for Lines
                dtHeaderExcel.Columns.Add("OperationNoErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("ActivityDescriptionErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("RefDocumentErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("RefDocRevNoErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("PIAErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("IAgencyLNTErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("IAgencyVVPTErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("IAgencySROErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("LineRevNoErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("MIPRevNoErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("RemarksErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("StatusErrorMsg", typeof(string));

                var Deleted = clsImplementationEnum.MIPLineStatus.Deleted.GetStringValue();

                var glbCategories = new string[] { "PIA", "MIP Inspection Agency L&T", "MIP Inspection Agency VVPT", "MIP Inspection Agency SRO" };
                List<CategoryData> lstMIPValues = (from glb2 in db.GLB002
                                                   join glb1 in db.GLB001 on glb2.Category equals glb1.Id
                                                   where glbCategories.Contains(glb1.Category) && glb2.BU.Trim() == objImport.BU.Trim() && glb2.Location.Trim() == objImport.Location.Trim()
                                                   select new CategoryData { catId = glb1.Category, Code = glb2.Code, CategoryDescription = glb2.Description }).ToList();

                List<CategoryData> lstPIAValues = lstMIPValues.Where(w => w.catId == glbCategories[0]).ToList();
                List<CategoryData> lstAgencyLnTValues = lstMIPValues.Where(w => w.catId == glbCategories[1]).ToList();
                List<CategoryData> lstAgencyVVPTValues = lstMIPValues.Where(w => w.catId == glbCategories[2]).ToList();
                List<CategoryData> lstAgencySROValues = lstMIPValues.Where(w => w.catId == glbCategories[3]).ToList();

                foreach (DataRow item in dtHeaderExcel.Rows)
                {
                    string PIA = item.Field<string>("PIA"), PIACode = null;
                    string IAgencyLNT = item.Field<string>("IAgencyLNT"), IAgencyLNTCode = null;
                    string IAgencyVVPT = item.Field<string>("IAgencyVVPT"), IAgencyVVPTCode = null;
                    string IAgencySRO = item.Field<string>("IAgencySRO"), IAgencySROCode = null;
                    string strOperationNo = item.Field<string>("OperationNo");
                    decimal OperationNo = 0;
                    string ActivityDescription = item.Field<string>("ActivityDescription");
                    string RefDocument = item.Field<string>("RefDocument");
                    string RefDocRevNo = item.Field<string>("RefDocRevNo");
                    string LineRevNo = item.Field<string>("LineRevNo");
                    string MIPRevNo = item.Field<string>("MIPRevNo");
                    string Remarks = item.Field<string>("Remarks");
                    string Status = item.Field<string>("Status");

                    string errorMessage = string.Empty;


                    if (string.IsNullOrEmpty(PIA))
                    {
                        item["PIAErrorMsg"] = "PIA is required";
                        isError = true;
                    }
                    else if ((PIACode = lstPIAValues.Where(w => w.CategoryDescription == PIA).Select(s => s.Code).FirstOrDefault()) == null)
                    {
                        item["PIAErrorMsg"] = "PIA is not valid";
                        isError = true;
                    }
                    else
                        item["PIA"] = PIACode;

                    if (string.IsNullOrEmpty(IAgencyLNT))
                    {
                        item["IAgencyLNTErrorMsg"] = "Inspection Agency L&T is required";
                        isError = true;
                    }
                    else if ((IAgencyLNTCode = lstAgencyLnTValues.Where(w => w.CategoryDescription == IAgencyLNT).Select(s => s.Code).FirstOrDefault()) == null)
                    {
                        item["IAgencyLNTErrorMsg"] = "Inspection Agency L&T is not valid";
                        isError = true;
                    }
                    else
                        item["IAgencyLNT"] = IAgencyLNTCode;

                    if (string.IsNullOrEmpty(IAgencyVVPT))
                    {
                        item["IAgencyVVPTErrorMsg"] = "Inspection Agency VVPT is required";
                        isError = true;
                    }
                    else if ((IAgencyVVPTCode = lstAgencyVVPTValues.Where(w => w.CategoryDescription == IAgencyVVPT).Select(s => s.Code).FirstOrDefault()) == null)
                    {
                        item["IAgencyVVPTErrorMsg"] = "Inspection Agency VVPT is not valid";
                        isError = true;
                    }
                    else
                        item["IAgencyVVPT"] = IAgencyVVPTCode;

                    if (!string.IsNullOrEmpty(IAgencySRO) && (IAgencySROCode = lstAgencySROValues.Where(w => w.CategoryDescription == IAgencySRO).Select(s => s.Code).FirstOrDefault()) == null)
                    {
                        item["IAgencySROErrorMsg"] = "Inspection Agency SRO is not valid";
                        isError = true;
                    }
                    else
                        item["IAgencySRO"] = IAgencySROCode;

                    if (string.IsNullOrEmpty(ActivityDescription))
                    {
                        item["ActivityDescriptionErrorMsg"] = "Activity Description is required";
                        isError = true;
                    }
                    else if (ActivityDescription.Length > 200)
                    {
                        item["ActivityDescriptionErrorMsg"] = "Activity Description maxlength exceeded";
                        isError = true;
                    }

                    if (!string.IsNullOrEmpty(RefDocument) && RefDocument.Length > 100)
                    {
                        item["RefDocumentErrorMsg"] = "Ref. Document maxlength exceeded";
                        isError = true;
                    }

                    if (!string.IsNullOrEmpty(RefDocRevNo) && RefDocRevNo.Length > 5)
                    {
                        item["RefDocRevNoErrorMsg"] = "Ref. Document Rev No maxlength exceeded";
                        isError = true;
                    }

                    if (!string.IsNullOrEmpty(Remarks) && Remarks.Length > 100)
                    {
                        item["RemarksErrorMsg"] = "Return Remarks maxlength exceeded";
                        isError = true;
                    }

                    if (!Decimal.TryParse(strOperationNo, out OperationNo))
                    {
                        item["OperationNoErrorMsg"] = "Operation No is not valid";
                        isError = true;
                    }
                    else if (OperationNo <= 0)
                    {
                        item["OperationNoErrorMsg"] = "Operation No is required";
                        isError = true;
                    }
                    else if (db.MIP002.Any(a => a.QualityProject == objImport.QualityProject && a.MIP001.BU == objImport.BU && a.MIP001.Location == objImport.Location && a.MIP001.DocNo == objImport.DocNo && a.MIP001.MIPType == objImport.MIPType && a.OperationNo == OperationNo && a.Status != Deleted))
                    {
                        item["OperationNoErrorMsg"] = "MIP Master Operatino No: " + OperationNo + " for QualityProject :" + objImport.QualityProject + " is already exist";
                        isError = true;
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            DataSet ds = new DataSet();
            ds.Tables.Add(dtHeaderExcel);
            return ds;
        }


        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "", int Headerid = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.MAINTAININDEX.GetStringValue())
                {
                    var lst = db.SP_MIP_GET_PROJECT_INDEX_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from a in lst
                                  select new
                                  {
                                      QualityProject = a.QualityProject,
                                      Project = a.Project,
                                      BU = a.BU,
                                      Location = a.Location,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_MIP_GET_HEADER_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      DocNo = uc.DocNo,
                                      MIPType = uc.MIPType,
                                      PMGReturnRemarks = uc.PMGReturnRemarks,
                                      RevNo = "R" + Convert.ToString(uc.RevNo),
                                      Status = Convert.ToString(uc.Status),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_MIP_GET_LINES_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    MIP001 objMIP001 = db.MIP001.Where(c => c.HeaderId == Headerid).FirstOrDefault();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      OperationNo = uc.OperationNo.ToString("G29"),
                                      ActivityDescription = uc.ActivityDescription,
                                      RefDocument = uc.RefDocument,
                                      RefDocRevNo = uc.RefDocRevNo,
                                      PIA = Manager.GetCategoryOnlyDescription(uc.PIA, objMIP001.BU, objMIP001.Location, "PIA"),
                                      IAgencyLNT = Manager.GetCategoryOnlyDescription(uc.IAgencyLNT, objMIP001.BU, objMIP001.Location, "MIP Inspection Agency L&T"),
                                      IAgencyVVPT = Manager.GetCategoryOnlyDescription(uc.IAgencyVVPT, objMIP001.BU, objMIP001.Location, "MIP Inspection Agency VVPT"),
                                      IAgencySRO = Manager.GetCategoryOnlyDescription(uc.IAgencySRO, objMIP001.BU, objMIP001.Location, "MIP Inspection Agency SRO"),
                                      LineRevNo = "R" + uc.LineRevNo,
                                      MIPRevNo = "R" + uc.MIPRevNo,
                                      Remarks = uc.Remarks,
                                      Status = uc.Status
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        public bool IsAnyApprovedStatus(MIP001 objMIP001)
        {
            var strApprove = clsImplementationEnum.MIPHeaderStatus.Approved.GetStringValue();
            var IsAnyApprovedStatus = db.MIP001.Any(i => i.QualityProject == objMIP001.QualityProject && i.Project == objMIP001.Project && i.BU == objMIP001.BU && i.Location == objMIP001.Location && i.Status == strApprove);
            if (IsAnyApprovedStatus)
                return true;
            else
                return false;
        }

        public ActionResult UpdateIDMNo(int HeaderId, string IDMNo)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            MIP001 objMIP001 = new MIP001();
            objMIP001 = db.MIP001.Where(i => i.HeaderId == HeaderId).FirstOrDefault();
            try
            {
                if (IsAnyApprovedStatus(objMIP001))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.IsInformation = true;
                    objResponseMsg.Value = "There Is Any MIP In Appored Status So You Can Not Modified IDMNo.";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                var lstOBJMIP001 = db.MIP001.Where(i => i.QualityProject == objMIP001.QualityProject && i.Project == objMIP001.Project && i.BU == objMIP001.BU && i.Location == objMIP001.Location).ToList();
                if (lstOBJMIP001.Count > 0)
                {
                    lstOBJMIP001.ForEach(i => { i.IDMNumber = IDMNo; });
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Remarks = "IDM No. Updated Successfully!";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
    }
}