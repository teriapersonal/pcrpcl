﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQS.Models;
using IEMQS.Areas.NDE.Models;
using System.Data.Entity.Core.Objects;
using IEMQS.Areas.IPI.Models;
using IEMQS.Areas.Utility.Models;
using System.Data;
using OfficeOpenXml;
using System.IO;
using System.Drawing;

namespace IEMQS.Areas.MIP.Controllers
{
    public class GenerateEMRController : clsBase
    {
        // GET: MIP/GenerateEMR
        #region Index Page
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Index()
        {
            ViewBag.Title = clsImplementationEnum.MIPHeaderIndexTitle.GenerateEMR.GetStringValue();
            ViewBag.IndexType = clsImplementationEnum.MIPIndexType.maintain.GetStringValue();
            var objMIP003 = new MIP003();
            return View(objMIP003);
        }

        public ActionResult GetMIPHeaderDataPartial(string Status, string QualityProject, string Location, string BU,string Assembly)
        {
            ViewBag.status = Status;
            ViewBag.qualityProject = QualityProject;
            ViewBag.location = Location;
            ViewBag.bu = BU;
            ViewBag.assembly = Assembly;

            UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            return PartialView("_GetMIPHeaderDataPartial");
        }

        [HttpPost]
        public ActionResult GetMIPHeaderData(JQueryDataTableParamModel param, string qualityProj, string status, string location, string bu, string assembly)
        {
            try
            {
                UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "MIP003.BU", "MIP003.Location");
                string strSortOrder = string.Empty;
                if (!string.IsNullOrEmpty(qualityProj))
                {
                    whereCondition += " AND UPPER(MIP003.QualityProject) = '" + qualityProj.Trim().ToUpper() + "'  AND UPPER(MIP003.BU) = '" + bu.Trim().ToUpper() + "' AND MIP003.ComponentNo like '" + assembly + @"%'
                        AND 1 = (SELECT(CASE WHEN count(*) <= 1 THEN 1 ELSE 0 END) FROM MIP004 WHERE MIP004.HeaderId = MIP003.HeaderId AND ISNULL(MIP004.InspectionStatus, '') NOT IN('" + clsImplementationEnum.MIPInspectionStatus.Cleared.GetStringValue() + "', '"+ clsImplementationEnum.MIPInspectionStatus.Not_Applicatble.GetStringValue()+"' ))";
                }
                //if (status.ToUpper() == "PENDING")
                //{
                //    if ((objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QI2.GetStringValue() || objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QI3.GetStringValue()) && objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Approver.GetStringValue())
                //    {
                //        whereCondition += " and MIP003.QCApprovedBy IS NULL and MIP003.Status in('" + clsImplementationEnum.MIPHeaderStatus.Approved.GetStringValue() + "')";
                //    }
                //}

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                string[] columnName = { "DocNo", "MIPType", "ComponentNo", "DrawingNo", "RawMaterialGrade", "PlateNo", "RevNo", "QCReturnRemarks", "Status" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);

                var lstResult = db.SP_MIP_HEADER_GET_HEADER_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                            {
                               Convert.ToString(uc.HeaderId),
                               Helper.GenerateHidden(uc.HeaderId, "HeaderId", Convert.ToString(uc.HeaderId)),
                               Manager.GetCategoryOnlyDescription(uc.DocNo,uc.BU,uc.Location  ,"MIP_DocNo" ),
                               Manager.GetCategoryOnlyDescription(uc.MIPType,uc.BU,uc.Location  ,"MIP_Type" ),
                               !string.IsNullOrWhiteSpace(uc.ComponentNo) ?uc.ComponentNo.ToUpper() : "",//((uc.Status == clsImplementationEnum.MIPHeaderStatus.Draft.GetStringValue() || uc.Status == clsImplementationEnum.MIPHeaderStatus.Returned.GetStringValue()) && objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Initiator.GetStringValue() ) ? Helper.GenerateTextbox(uc.HeaderId, "ComponentNo", uc.ComponentNo, "UpdateMIPHeader(this, "+ uc.HeaderId +")", (uc.Status == clsImplementationEnum.LTFPSLineStatus.Deleted.GetStringValue()), "", "25","uppercase"): uc.ComponentNo,
                               ((uc.Status == clsImplementationEnum.MIPHeaderStatus.Draft.GetStringValue() || uc.Status == clsImplementationEnum.MIPHeaderStatus.Returned.GetStringValue()) && objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Initiator.GetStringValue() ) ? Helper.GenerateTextbox(uc.HeaderId, "DrawingNo", uc.DrawingNo, "UpdateMIPHeader(this, "+ uc.HeaderId +")", (uc.Status == clsImplementationEnum.LTFPSLineStatus.Deleted.GetStringValue()), "", "25"): uc.DrawingNo,
                               ((uc.Status == clsImplementationEnum.MIPHeaderStatus.Draft.GetStringValue() || uc.Status == clsImplementationEnum.MIPHeaderStatus.Returned.GetStringValue()) && objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Initiator.GetStringValue() ) ? Helper.GenerateTextbox(uc.HeaderId, "RawMaterialGrade", uc.RawMaterialGrade, "UpdateMIPHeader(this, "+ uc.HeaderId +")", (uc.Status == clsImplementationEnum.LTFPSLineStatus.Deleted.GetStringValue()), "", "20"): uc.RawMaterialGrade,
                               ((uc.Status == clsImplementationEnum.MIPHeaderStatus.Draft.GetStringValue() || uc.Status == clsImplementationEnum.MIPHeaderStatus.Returned.GetStringValue()) && objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Initiator.GetStringValue() ) ? Helper.GenerateTextbox(uc.HeaderId, "PlateNo", uc.PlateNo, "UpdateMIPHeader(this, "+ uc.HeaderId +")", (uc.Status == clsImplementationEnum.LTFPSLineStatus.Deleted.GetStringValue()), "", "20"): uc.PlateNo,
                               uc.QCReturnRemarks,
                               "R" + Convert.ToString(uc.RevNo),
                               Convert.ToString(uc.Status),
                               "<nobr><center>"+
                                Helper.HTMLActionString(Convert.ToInt32(uc.HeaderId), "View", "View Details", "fa fa-eye", "", "/MIP/MaintainMIPHeader/Details/"+uc.HeaderId+"?urlForm=a")
                                +"</center></nobr>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        [HttpPost]
        [SessionExpireFilter]
        public ActionResult DownloadEMR(string HeaderIds)
        {
            var objResponseMsg = new clsHelper.ResponceMsgWithObject();

            if (String.IsNullOrWhiteSpace(HeaderIds))
            {
                objResponseMsg.Value = "Please select Component";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            var headerIds = HeaderIds.Split(',').Select(s => int.Parse(s));
            var clearStatus = new string[] { clsImplementationEnum.MIPInspectionStatus.Cleared.GetStringValue(), clsImplementationEnum.MIPInspectionStatus.Not_Applicatble.GetStringValue() };

            var lstFolderPaths = db.MIP004.Where(w => headerIds.Contains(w.HeaderId) && clearStatus.Contains(w.InspectionStatus)).Select(s => "MIP050/" + s.RequestNo);
            string FolderPaths = string.Join(",", lstFolderPaths);
            var objMIP003 = db.MIP004.FirstOrDefault(f => headerIds.Contains(f.HeaderId )).MIP003;
            string Filename = objMIP003.QualityProject +"_"+ (!string.IsNullOrWhiteSpace(objMIP003.ComponentNo) && objMIP003.ComponentNo.Length > 4? objMIP003.ComponentNo.Substring(0, objMIP003.ComponentNo.Length-4): objMIP003.ComponentNo); //@(Model.QualityProject + "_" + Model.ComponentNo)

            objResponseMsg.Key = true;
            objResponseMsg.data = new { FolderPaths=FolderPaths, Filename= Filename };
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetQualityProjects(string term)
        {
            string username = objClsLoginInfo.UserName;
            var lstATHLocation = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.Location).Distinct().ToList();
            var lstCOMLocation = db.COM003.Where(i => i.t_psno.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_loca).Distinct().ToList();
            lstCOMLocation = lstATHLocation.Union(lstCOMLocation).ToList();

            var lstCOMBU = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.BU).Distinct().ToList();

            List<Projects> lstQualityProject = new List<Projects>();
            if (!string.IsNullOrEmpty(term))
            {
                lstQualityProject = db.MIP003.Where(i => i.QualityProject.ToLower().Contains(term.ToLower())
                                                                 && lstCOMLocation.Contains(i.Location)
                                                                 && lstCOMBU.Contains(i.BU)
                                                               ).Select(i => new Projects { Value = i.QualityProject, projectCode = i.QualityProject, BU = i.BU }).Distinct().ToList();
            }
            else
            {
                lstQualityProject = db.MIP003.Where(i => lstCOMLocation.Contains(i.Location)
                                                        && lstCOMBU.Contains(i.BU)
                                                        ).Select(i => new Projects { Value = i.QualityProject, projectCode = i.QualityProject, BU = i.BU }).Distinct().Take(10).ToList();
            }

            return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [SessionExpireFilter]
        public ActionResult GetQualityProjectWiseProjectBUDetailWithAssembly(string qualityProject)
        {
            QualityProjectsDetails objProjectsDetails = new QualityProjectsDetails();
            var project = db.QMS010.Where(i => i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase)).Select(i => i.Project).FirstOrDefault();

            objProjectsDetails.ProjectCode = project;
            objProjectsDetails.projectDescription = db.COM001.Where(i => i.t_cprj.Equals(project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + "-" + i.t_dsca).FirstOrDefault();// project;

            var ProjectWiseBULocation = Manager.getProjectWiseBULocation(project);

            objProjectsDetails.BUCode = ProjectWiseBULocation.QCPBU.Split('-')[0].Trim();
            objProjectsDetails.BUDescription = ProjectWiseBULocation.QCPBU;

            objProjectsDetails.LocationCode = objClsLoginInfo.Location;
            objProjectsDetails.LocationDescription = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objClsLoginInfo.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            var lineStatus = new string[] { clsImplementationEnum.MIPInspectionStatus.Cleared.GetStringValue(), clsImplementationEnum.MIPInspectionStatus.Not_Applicatble.GetStringValue() };
            var Assembly = new List<string>();
            var lstLoc = Manager.GetUserwiseLocation(objClsLoginInfo.UserName);
            var lstMIP003 = db.MIP003.Where(w => w.QualityProject == qualityProject && w.BU == objProjectsDetails.BUCode  && lstLoc.Contains(w.Location)).OrderBy(o=>o.ComponentNo).ToList();
            foreach (var item in lstMIP003)
            {
                if (!String.IsNullOrWhiteSpace(item.ComponentNo) && item.ComponentNo.Length  > 4)
                {
                    var totalNotClear = item.MIP004.Where(w => !lineStatus.Contains(w.InspectionStatus)).Count();//1
                    if (totalNotClear <= 1) // cleared
                        Assembly.Add(item.ComponentNo.Substring(0, item.ComponentNo.Length - 4));
                }
            }
            Assembly = Assembly.Distinct().ToList();
            return Json(new { ProjectsDetails = objProjectsDetails, Assembly = Assembly }, JsonRequestBehavior.AllowGet);
        }
        public UserRoleAccessDetails GetUserAccessRights()
        {
            UserRoleAccessDetails objUserRoleAccessDetails = new UserRoleAccessDetails();

            try
            {
                var role = (from a in db.ATH001
                            join b in db.ATH004 on a.Role equals b.Id
                            where a.Employee.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                            select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();

                if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.QI2.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.QI2.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Approver.GetStringValue();
                }
                else if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.QI3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.QI3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Approver.GetStringValue();
                }
                else if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.PMG3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Initiator.GetStringValue();
                }
                else
                {
                    objUserRoleAccessDetails.UserRole = "";
                    objUserRoleAccessDetails.UserDesignation = "";
                }
            }
            catch
            {

            }
            return objUserRoleAccessDetails;
        }
    }
}