﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQS.Models;
using IEMQS.Areas.Utility.Models;
using System.Data;
using OfficeOpenXml;
using System.IO;
using System.Drawing;
using System.Collections;

namespace IEMQS.Areas.MIP.Controllers
{
    public class MaintainMIPHeaderController : clsBase
    {
        // GET: MIP/MaintainMIPHeader
        #region Index Page
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Index()
        {
            ViewBag.Title = clsImplementationEnum.MIPHeaderIndexTitle.maintainMIP.GetStringValue();
            ViewBag.IndexType = clsImplementationEnum.MIPIndexType.maintain.GetStringValue();

            return View();
        }

        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Approve()
        {
            ViewBag.Title = clsImplementationEnum.MIPHeaderIndexTitle.ApproveMIP.GetStringValue();
            ViewBag.IndexType = clsImplementationEnum.MIPIndexType.approve.GetStringValue();
            return View("Index");
        }

        public ActionResult GetIndexGridDataPartial(string status, string title, string indextype)
        {
            ViewBag.Status = status;
            ViewBag.GridTitle = title;
            ViewBag.IndexDataFor = indextype;
            return PartialView("_GetIndexGridDataPartial");
        }

        public JsonResult LoadMIPIndexDataTable(JQueryDataTableParamModel param)
        {
            try
            {
                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "MIP003.BU", "MIP003.Location");
                string _urlform = string.Empty;
                string indextype = param.Department;

                if (string.IsNullOrWhiteSpace(indextype))
                    indextype = clsImplementationEnum.MIPIndexType.maintain.GetStringValue();
                if (param.Status.ToUpper() == "PENDING")
                {
                    if (indextype == clsImplementationEnum.MIPIndexType.approve.GetStringValue())
                        whereCondition += " and MIP003.QCApprovedBy IS NULL and MIP003.Status in('" + clsImplementationEnum.MIPHeaderStatus.SentForApproval.GetStringValue() + "')";
                    if (indextype == clsImplementationEnum.MIPIndexType.maintain.GetStringValue())
                        whereCondition += " and MIP003.Status in('" + clsImplementationEnum.MIPHeaderStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.MIPHeaderStatus.Returned.GetStringValue() + "')";
                }

                string[] columnName = { "QualityProject", "(MIP003.Project+'-'+com1.t_dsca)", "(LTRIM(RTRIM(MIP003.Location))+'-'+LTRIM(RTRIM(lcom2.t_desc)))", "(LTRIM(RTRIM(MIP003.BU))+'-'+LTRIM(RTRIM(bcom2.t_desc)))" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);

                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Request["sSortDir_0"];
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstHeader = db.SP_MIP_HEADER_GET_PROJECT_INDEX_DATA(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstHeader
                          select new[] {
                        a.QualityProject,
                        a.Project,
                        a.BU,
                        a.Location,
                        Helper.HTMLActionString(Convert.ToInt32(a.HeaderId), "View", "View Details", "fa fa-eye", "", "/MIP/MaintainMIPHeader/Details/"+a.HeaderId+"?urlForm="+ indextype )
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult MIPSession()
        {
            ViewBag.QualityProject = GetQualityProjects(string.Empty).Data;
            return View();
        }

        [HttpPost]
        public JsonResult LoadMIPSessionGridData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = 0;
                int EndIndex = 0;

                StartIndex = param.iDisplayStart + 1;
                EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = "QualityProject";
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion
                strWhereCondition += "1=1 ";
                //search Condition 
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = { "a.QualityProject", "Category", "Range" };

                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(param.SearchFilter))

                        strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var lstResult = db.SP_GetSession(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.QualityProject),
                               Convert.ToString(uc.Category),
                               Convert.ToString(uc.Range),
                               $@"<span>
                                    <a id='delete{ uc.HeaderId }' title='Delete' class='blue action' onclick='DeleteMIPSession({uc.HeaderId})'><i class='fa fa-trash'></i></a>
                                    <a id='add{ uc.HeaderId }' title='Add Sub Category' class='blue action' onclick='AddMIPSubCategory({uc.HeaderId})'><i class='fa fa-plus'></i></a>
                                </span>"
                          }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetInlineEditableRow(int id, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();

                if (id == 0)
                {
                    int newRecordId = 0;
                    var newRecord = new[] {
                                        "0"+Helper.GenerateHidden(newRecordId,"BU"),
                                        Helper.HTMLAutoComplete(newRecordId,"QualityProject","","",false,"","",false,"","","QualityProject","form-control"),
                                        Helper.HTMLAutoComplete(newRecordId,"Category","","",false,"","",false,"","","Category","form-control"),
                                        Helper.GenerateTextbox(newRecordId, "Range","", "",false, "","","form-control",false,"","Range"),
                                        Helper.GenerateGridButtonNew(newRecordId, "Save", "Save Rocord", "fa fa-save", "AddMIPSession()" ),
                                    };
                    data.Add(newRecord);
                }
                else
                {
                    var lstResult = db.SP_GetSession(1, 0, "", "HeaderId = " + id).Take(1).ToList();

                    if (isReadOnly)
                        data = (from uc in lstResult
                                select new[]
                               {
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.QualityProject),
                               Convert.ToString(uc.Category),
                               Convert.ToString(uc.Range),
                               "<span><a id='delete" + uc.HeaderId + "' title='Delete' class='blue action' onclick='DeleteMIPSession(" + uc.HeaderId + ")'>"+
                                        "<i class='fa fa-trash'></i></a></span>"
                          }).ToList();
                    else
                        data = (from uc in lstResult
                                select new[]
                               {
                                 Convert.ToString(uc.HeaderId),
                                 Helper.HTMLAutoComplete(uc.HeaderId,"QualityProject",uc.QualityProject,"SaveData(this, "+ uc.HeaderId +");",true,"","",false,"","","QualityProject","form-control")+Helper.GenerateHidden(uc.HeaderId,"BU"),
                                 Helper.HTMLAutoComplete(uc.HeaderId,"Category",uc.Category,"SaveData(this, "+ uc.HeaderId +");",false,"","",true,"","","Category","form-control"),
                                 Helper.GenerateTextbox(uc.HeaderId, "Range",uc.Range, "SaveData(this, "+ uc.HeaderId +");",false, "","","form-control",false,"","Range"),
                                 Helper.GenerateGridButtonNew(uc.HeaderId, "Cancel", "Cancel", "fa fa-close", "ReloadGrid()" )
                          }).ToList();

                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult UpdateDetails(int headerId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    var obj = db.MIP005.FirstOrDefault(m => m.HeaderId == headerId);
                    var lst = db.MIP005.Where(m => m.HeaderId != headerId && m.QualityProject == obj.QualityProject && m.BU == obj.BU && m.Location == obj.Location).ToList();
                    var isExistCategory = lst.Where(m => m.Category == obj.Category);
                    bool isRangeExist = false;
                    List<string> lstRange = new List<string>();
                    foreach (var item in lst)
                        lstRange.AddRange(item.Range.Split(','));

                    foreach (var item in columnValue.Split(','))
                    {
                        if (!isRangeExist)
                            isRangeExist = lstRange.Contains(item);
                    }
                    if (isExistCategory.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Record already exist with Category";
                    }
                    else if (isRangeExist)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Record already exist with Range";
                    }
                    else
                    {
                        db.SP_COMMON_TABLE_UPDATE("MIP005", headerId, "HeaderId", columnName, columnValue, Manager.GetSystemUserPSNo());
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SaveNewRecord(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string QualityPrject = !string.IsNullOrEmpty(fc["QualityProject0"]) ? fc["QualityProject0"].TrimEnd(',') : string.Empty;
                string BU = !string.IsNullOrEmpty(fc["BU0"]) ? fc["BU0"].TrimEnd(',') : string.Empty;
                string Range = !string.IsNullOrEmpty(fc["Range0"]) ? fc["Range0"].TrimEnd(',') : string.Empty;
                string Category = !string.IsNullOrEmpty(fc["Category0"]) ? fc["Category0"].TrimEnd(',') : string.Empty;

                var lst = db.MIP005.Where(m => m.QualityProject == QualityPrject && m.BU == BU && m.Location == objClsLoginInfo.Location).ToList();
                var isCategoryExist = lst.Where(m => m.Category == Category);
                bool isRangeExist = false;
                List<string> lstRange = new List<string>();
                foreach (var item in lst)
                    lstRange.AddRange(item.Range.Split(','));

                foreach (var item in Range.Split(','))
                {
                    if (!isRangeExist)
                        isRangeExist = lstRange.Contains(item);
                }
                if (isCategoryExist.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Record already exist with Category";
                }
                else if (isRangeExist)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Record already exist with Range";
                }
                else
                {
                    MIP005 objMIP005 = new MIP005
                    {
                        QualityProject = QualityPrject,
                        BU = BU,
                        Category = Category,
                        Range = Range,
                        Location = objClsLoginInfo.Location,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    };
                    db.MIP005.Add(objMIP005);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.DataAdded.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteMIPSession(int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (headerId > 0)
                {
                    MIP005 objMIP005 = db.MIP005.FirstOrDefault(c => c.HeaderId == headerId);
                    if (objMIP005 != null)
                    {
                        db.MIP006.RemoveRange(objMIP005.MIP006);
                        db.MIP005.Remove(objMIP005);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "MIP deleted successfully";
                        //}
                    }
                    else
                    {
                        objResponseMsg.Value = "MIP not found";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SubCategoryGridPartial(int HeaderId)
        {
            ViewBag.HeaderId = HeaderId;
            var objCategory = db.GLB001.FirstOrDefault(m => m.Category == "MIP_Type");
            var Category = Convert.ToInt32(objCategory.Id);
            ViewBag.Category = db.GLB002.Where(m => m.Category == Category).Select(m => new { Value = m.Category, Text = m.Code }).Distinct().ToList();
            return PartialView("_SubCategoryGridPartial");
        }
        [HttpPost]
        public JsonResult LoadMIPSubCategoryGridData(JQueryDataTableParamModel param, int HeaderId)
        {
            try
            {
                int StartIndex = 0;
                int EndIndex = 0;

                StartIndex = param.iDisplayStart + 1;
                EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = "SubCategory";
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion
                strWhereCondition += " 1=1 ";
                if (HeaderId > 0)
                    strWhereCondition += "and HeaderId= " + HeaderId;
                //search Condition 
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = { "SubCategory", "Quantity" };

                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(param.SearchFilter))

                        strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var lstResult = db.SP_GET_MIP_SUB_CATEGORY(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.LineId),
                               Convert.ToString(uc.SubCategory),
                               Convert.ToString(uc.Quantity),
                               $@"<span>
                                    <a id='deleteCategory{ uc.LineId }' title='Delete' class='blue action' onclick='DeleteMIPSubCategory({uc.LineId})'><i class='fa fa-trash'></i></a>
                                </span>"
                          }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult GetMIPSubCategoryInlineEditableRow(int id, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();

                if (id == 0)
                {
                    int newRecordId = 0;
                    var newRecord = new[] {
                                        "0",
                                        Helper.HTMLAutoComplete(newRecordId,"SubCategory","","",false,"","",false,"","","SubCategory","form-control"),
                                        Helper.GenerateTextbox(newRecordId, "Quantity","", "",false, "","","form-control",false,"","Quantity"),
                                        Helper.GenerateGridButtonNew(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveMIPSubCategory()" ),
                                    };
                    data.Add(newRecord);
                }
                else
                {
                    var lstResult = db.SP_GET_MIP_SUB_CATEGORY(1, 0, "", "LineId = " + id).Take(1).ToList();

                    if (isReadOnly)
                        data = (from uc in lstResult
                                select new[]
                               {
                               Convert.ToString(uc.LineId),
                               Convert.ToString(uc.SubCategory),
                               Convert.ToString(uc.Quantity),
                               "<span><a id='delete" + uc.LineId + "' title='Delete' class='blue action' onclick='DeleteMIPSession(" + uc.LineId + ")'>"+
                                        "<i class='fa fa-trash'></i></a></span>"
                          }).ToList();
                    else
                        data = (from uc in lstResult
                                select new[]
                               {
                                 Convert.ToString(uc.LineId),
                                 Helper.HTMLAutoComplete(uc.LineId,"SubCategory",uc.SubCategory,"SaveMIPSubCategoryData(this, "+ uc.LineId +");",true,"","",false,"","","SubCategory","form-control"),
                                 Helper.GenerateTextbox(uc.LineId, "Quantity",Convert.ToString(uc.Quantity), "SaveMIPSubCategoryData(this, "+ uc.LineId +");",false, "","","form-control",false,"","Quantity"),
                                 Helper.GenerateGridButtonNew(uc.LineId, "Cancel", "Cancel", "fa fa-close", "ReloadMIPSubCtegoryGridGrid()" )
                          }).ToList();

                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SaveMIPCategoryNewRecord(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string SubCategory = !string.IsNullOrEmpty(fc["SubCategory0"]) ? fc["SubCategory0"].Replace(",", "") : string.Empty;
                int HeaderId = Convert.ToInt32(fc["hdnHeaderId"].Replace(",", ""));
                bool isExist = db.MIP006.Any(m => m.SubCategory == SubCategory && m.HeaderId == HeaderId);
                if (!isExist)
                {
                    MIP006 objMIP006 = new MIP006
                    {
                        SubCategory = SubCategory,
                        Quantity = Convert.ToInt32(fc["Quantity0"].Replace(",", "")),
                        HeaderId = HeaderId,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    };
                    db.MIP006.Add(objMIP006);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.DataAdded.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult DeleteMIPSubCategory(int LineId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (LineId > 0)
                {
                    MIP006 objMIP006 = db.MIP006.FirstOrDefault(c => c.LineId == LineId);
                    if (objMIP006 != null)
                    {
                        db.MIP006.Remove(objMIP006);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "MIP Sub Category deleted successfully";
                        //}
                    }
                    else
                    {
                        objResponseMsg.Value = "MIP not found";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateMIPSubCategoryDetails(int LineId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    db.SP_COMMON_TABLE_UPDATE("MIP006", LineId, "LineId", columnName, columnValue, Manager.GetSystemUserPSNo());
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Header Details Page
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Details(int id = 0, string urlForm = "")
        {
            MIP003 objMIP003 = new MIP003();
            if (id > 0)
            {
                objMIP003 = db.MIP003.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.Project = db.COM001.Where(i => i.t_cprj.Equals(objMIP003.Project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objMIP003.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                ViewBag.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objMIP003.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            }
            else
            {
                ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objClsLoginInfo.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objMIP003.Location = objClsLoginInfo.Location;
            }

            if (urlForm.ToLower() == clsImplementationEnum.MIPIndexType.approve.GetStringValue().ToLower())
            {
                ViewBag.Title = clsImplementationEnum.MIPHeaderIndexTitle.ApproveMIP.GetStringValue();
            }
            else
            {
                ViewBag.formRedirect = clsImplementationEnum.MIPIndexType.maintain.GetStringValue();
                ViewBag.Title = clsImplementationEnum.MIPHeaderIndexTitle.maintainMIP.GetStringValue();
            }
            ViewBag.formRedirect = urlForm;

            return View(objMIP003);
        }

        public ActionResult GetMIPHeaderDataPartial(string Status, string QualityProject, string Location, string BU, string indextype)
        {
            ViewBag.status = Status;
            ViewBag.qualityProject = QualityProject;
            ViewBag.location = Location;
            ViewBag.bu = BU;
            ViewBag.indextype = indextype;
            if (string.IsNullOrWhiteSpace(indextype))
                ViewBag.indextype = clsImplementationEnum.MIPIndexType.maintain.GetStringValue();

            UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            string approved = clsImplementationEnum.MIPHeaderStatus.Approved.GetStringValue();
            var ApprovedMipMst = db.MIP001_Log.Where(w => w.Status == approved && w.QualityProject == QualityProject && w.Location == Location && w.BU == BU).ToList();
            ViewBag.DocNo = ApprovedMipMst.Select(i => new CategoryData { id = i.Id + "", Code = i.Id + "", Value = i.DocNo, CategoryDescription = Manager.GetCategoryOnlyDescription(i.DocNo, i.BU, i.Location, "MIP_DocNo"), catId = i.MIPType, catValue = Manager.GetCategoryOnlyDescription(i.MIPType, i.BU, i.Location, "MIP_Type"), qualityCode = i.DocNo + "|" + i.MIPType }).ToList();

            return PartialView("_GetMIPHeaderDataPartial");
        }

        public ActionResult GetMIPBulkPrintPartial(string qualityproject, string project, string location, string bu)
        {
            ViewBag.qualityProject = qualityproject;
            ViewBag.location = location;
            ViewBag.bu = bu;
            string whereCondition = "1=1";
            if (!string.IsNullOrEmpty(qualityproject))
                whereCondition += " AND UPPER(MIP003.QualityProject) = '" + qualityproject.Trim().ToUpper() + "'AND MIP003.Project='" + project.Trim() + "' AND UPPER(MIP003.Location) = '" + location.Trim().ToUpper() + "'  AND UPPER(MIP003.BU) = '" + bu.Trim().ToUpper() + "'";

            var lstResult = db.SP_MIP_HEADER_GET_HEADER_DATA(0, int.MaxValue, " ", whereCondition).ToList();

            if (lstResult.Count() > 0)
            {
                ViewBag.ComponentNo = lstResult.Select(i => new { Text = i.ComponentNo, Value = i.ComponentNo }).OrderBy(x => x.Value).Distinct().ToList();
            }
            else
            {
                ViewBag.ComponentNo = null;
            }

            return PartialView("_GetMIPBulkPrintPartial");
        }

        [HttpPost]
        public JsonResult GetMIPHeaderData(JQueryDataTableParamModel param, string qualityProj, string indextype)
        {
            try
            {
                UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "MIP003.BU", "MIP003.Location");
                string strSortOrder = string.Empty;
                bool isRemarkEditable = false;
                bool isPrint = false;
                bool isInitiator = objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Initiator.GetStringValue();
                if (!string.IsNullOrEmpty(qualityProj))
                    whereCondition += " AND UPPER(MIP003.QualityProject) = '" + qualityProj.Trim().ToUpper() + "' AND UPPER(MIP003.Location) = '" + param.Location.Trim().ToUpper() + "'  AND UPPER(MIP003.BU) = '" + param.BU.Trim().ToUpper() + "'";
                if ((objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QI2.GetStringValue() || objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QI3.GetStringValue()) && objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Approver.GetStringValue())
                    isPrint = true;

                if (param.Status.ToUpper() == "PENDING")
                {
                    if ((objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QI2.GetStringValue() || objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QI3.GetStringValue()) && objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Approver.GetStringValue())
                    {
                        isRemarkEditable = true;
                        whereCondition += " and MIP003.QCApprovedBy IS NULL and MIP003.Status in('" + clsImplementationEnum.MIPHeaderStatus.SentForApproval.GetStringValue() + "')";
                    }
                    else if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.PMG3.GetStringValue() && isInitiator)
                    {
                        whereCondition += " and MIP003.Status in('" + clsImplementationEnum.MIPHeaderStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.MIPHeaderStatus.Returned.GetStringValue() + "')";
                    }
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                string[] columnName = { "DocNo", "MIPType", "ComponentNo", "DrawingNo", "RawMaterialGrade", "PlateNo", "RevNo", "QCReturnRemarks", "Status" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);

                var lstResult = db.SP_MIP_HEADER_GET_HEADER_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                var CheckedIds = "";
                if (param.bCheckedAll && lstResult.Any())
                    CheckedIds = String.Join(",", db.SP_MIP_HEADER_GET_HEADER_DATA(1, 0, strSortOrder, whereCondition).Select(s => s.HeaderId).ToList());

                int newRecordId = 0;
                var newRecord = new[] {
                                    "",
                                    Helper.GenerateHidden(newRecordId, "HeaderId", ""),
                                    GenerateAutoCompleteOnBlur(newRecordId,"txtDocNo","","","",false,"","DocNo")+""+Helper.GenerateHidden(newRecordId,"DocNo"),
                                    GenerateAutoCompleteOnBlur(newRecordId,"txtMIPType","","","",true,"","MIPType")+""+Helper.GenerateHidden(newRecordId,"MIPType"),
                                    Helper.GenerateTextbox(newRecordId, "ComponentNo", "", "", false, "", "25","uppercase"),
                                    Helper.GenerateTextbox(newRecordId, "DrawingNo", "", "", false, "", "30"),
                                    Helper.GenerateTextbox(newRecordId, "RawMaterialGrade", "", "", false, "", "20"),
                                    Helper.GenerateTextbox(newRecordId, "PlateNo", "", "", false, "", "20"),
                                    "",
                                    "R0",
                                    clsImplementationEnum.MIPHeaderStatus.Draft.GetStringValue(),
                                    Helper.GenerateGridButtonType(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveMIPHeader();")
                                };
                var lineStatus = new string[] { clsImplementationEnum.MIPHeaderStatus.Draft.GetStringValue(), clsImplementationEnum.MIPHeaderStatus.Returned.GetStringValue() };
                var deleteStatus = clsImplementationEnum.LTFPSLineStatus.Deleted.GetStringValue();
                var approvedStatus = clsImplementationEnum.LTFPSLineStatus.Approved.GetStringValue();
                var data = (from uc in lstResult
                            select new[]
                            {
                               Convert.ToString(uc.HeaderId),
                               Helper.GenerateHidden(uc.HeaderId, "HeaderId", Convert.ToString(uc.HeaderId)),
                               Manager.GetCategoryOnlyDescription(uc.DocNo,uc.BU,uc.Location  ,"MIP_DocNo" ),
                               Manager.GetCategoryOnlyDescription(uc.MIPType,uc.BU,uc.Location  ,"MIP_Type" ),
                               uc.ComponentNo,
                               (lineStatus.Contains(uc.Status) && isInitiator) ? Helper.GenerateTextbox(uc.HeaderId, "DrawingNo", uc.DrawingNo, "UpdateMIPHeader(this, "+ uc.HeaderId +")", (uc.Status == deleteStatus), "", "30"): uc.DrawingNo,
                               (lineStatus.Contains(uc.Status) && isInitiator) ? Helper.GenerateTextbox(uc.HeaderId, "RawMaterialGrade", uc.RawMaterialGrade, "UpdateMIPHeader(this, "+ uc.HeaderId +")", (uc.Status == deleteStatus), "", "20"): uc.RawMaterialGrade,
                               (lineStatus.Contains(uc.Status) && isInitiator) ? Helper.GenerateTextbox(uc.HeaderId, "PlateNo", uc.PlateNo, "UpdateMIPHeader(this, "+ uc.HeaderId +")", (uc.Status == deleteStatus), "", "20"): uc.PlateNo,
                                GenerateRemark(uc.HeaderId, "QCReturnRemarks", objUserRoleAccessDetails.UserDesignation, objUserRoleAccessDetails.UserRole, isRemarkEditable),
                               "R" + Convert.ToString(uc.RevNo),
                               uc.Status,
                               "<nobr><center>"+
                               Helper.HTMLActionString(uc.HeaderId, "Delete", "Delete MIP", "fa fa-trash-o", "DeleteMIPHeaderWithConfirmation("+ uc.HeaderId +")", "", (!isInitiator || !(lineStatus.Contains(uc.Status) && uc.RevNo == 0))) +
                               Helper.GenerateActionIcon(uc.HeaderId, "Print", "Print Detail", "fa fa-print", "PrintReport('" +uc.QualityProject+"','"+ uc.Project+"','"+uc.BU+"','"+uc.Location+"','"+uc.DocNo+"','"+uc.ComponentNo+"','"+uc.MIPType+"')","",lineStatus.Contains(uc.Status))+
                               Helper.HTMLActionString(uc.HeaderId, "History", "History", "fa fa-history", "HistoryMIP("+ uc.HeaderId +")", "", uc.RevNo > 0 || uc.Status == approvedStatus ? false : true) +
                               Helper.HTMLActionString(uc.HeaderId, "Revise", "Revise", "fa fa-retweet", "ReviseMIPHeaderWithConfirmation("+ uc.HeaderId +")", "", uc.Status == approvedStatus && isInitiator ? false : true)
                               +"</center></nobr>"
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition,
                    checkedIds = CheckedIds
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult SaveMIPHeader(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            MIP003 objMIP003 = new MIP003();
            int newRowIndex = 0;
            try
            {
                string strProject = !string.IsNullOrEmpty(fc["Project"]) ? Convert.ToString(fc["Project"]).Split('-')[0].Trim() : string.Empty;
                string strBU = !string.IsNullOrEmpty(fc["BU"]) ? Convert.ToString(fc["BU"]).Split('-')[0].Trim() : string.Empty;
                string strLocation = !string.IsNullOrEmpty(fc["Location"]) ? Convert.ToString(fc["Location"]).Split('-')[0].Trim() : string.Empty;
                string strQualityProject = !string.IsNullOrEmpty(fc["txtQualityProject"]) ? fc["txtQualityProject"] : string.Empty;
                string strDocNo = !string.IsNullOrEmpty(fc["DocNo" + newRowIndex]) ? fc["DocNo" + newRowIndex] : string.Empty;
                string strMIPType = !string.IsNullOrEmpty(fc["MIPType" + newRowIndex]) ? fc["MIPType" + newRowIndex] : string.Empty;
                string strComponentNo = !string.IsNullOrEmpty(fc["ComponentNo" + newRowIndex]) ? fc["ComponentNo" + newRowIndex] : string.Empty;
                string strDrawingNo = !string.IsNullOrEmpty(fc["DrawingNo" + newRowIndex]) ? fc["DrawingNo" + newRowIndex] : string.Empty;
                string strRawMaterialGrade = !string.IsNullOrEmpty(fc["RawMaterialGrade" + newRowIndex]) ? fc["RawMaterialGrade" + newRowIndex] : string.Empty;
                string strPlateNo = !string.IsNullOrEmpty(fc["PlateNo" + newRowIndex]) ? fc["PlateNo" + newRowIndex] : string.Empty;

                if (fc != null)
                {
                    //if (isMIPHeaderExist(strQualityProject, strProject, strBU, strLocation, strDocNo, strMIPType))
                    //{
                    //    objResponseMsg.Key = false;
                    //    objResponseMsg.Value = "MIP Document No. " + strDocNo + " for " + strQualityProject + " is already exist";
                    //}
                    //else if (isMIPHeaderComponentNoExist(strQualityProject, strProject, strBU, strLocation, strComponentNo))
                    //{
                    //    objResponseMsg.Key = false;
                    //    objResponseMsg.Value = "MIP Component No " + strComponentNo + " for " + strQualityProject + " is already exist";
                    //}
                    if (isMIPHeaderComponentNoCombinationExist(strQualityProject, strProject, strBU, strLocation, strDocNo, strMIPType, strComponentNo))
                    {
                        objResponseMsg.Value = "MIP Component No " + strComponentNo + " for QualityProject :" + strQualityProject + " & Document No: " + strDocNo + " is already exist";
                    }
                    else
                    {
                        #region Add New MIP Header
                        objMIP003.QualityProject = strQualityProject;
                        objMIP003.Project = strProject;
                        objMIP003.BU = strBU;
                        objMIP003.Location = strLocation;
                        objMIP003.DocNo = strDocNo;
                        objMIP003.MIPType = strMIPType;
                        objMIP003.ComponentNo = strComponentNo;
                        objMIP003.DrawingNo = strDrawingNo;
                        objMIP003.RawMaterialGrade = strRawMaterialGrade;
                        objMIP003.PlateNo = strPlateNo;
                        objMIP003.RevNo = 0;
                        objMIP003.Status = clsImplementationEnum.MIPHeaderStatus.Draft.GetStringValue();
                        objMIP003.CreatedBy = objClsLoginInfo.UserName;
                        objMIP003.CreatedOn = DateTime.Now;

                        db.MIP003.Add(objMIP003);
                        db.SaveChanges();
                        string Approved = clsImplementationEnum.MIPHeaderStatus.Approved.GetStringValue();
                        var objMIP001_log = db.MIP001_Log.FirstOrDefault(w => w.Status == Approved && w.QualityProject == objMIP003.QualityProject && w.Location == objMIP003.Location && w.BU == objMIP003.BU && w.DocNo == strDocNo && w.MIPType == strMIPType);
                        if (objMIP001_log != null)
                        {
                            var objMIP002_Log = db.MIP002_Log.Where(w => w.Status == Approved && w.HeaderId == objMIP001_log.HeaderId).ToList();
                            var objMIP004 = new List<MIP004>();
                            foreach (var item in objMIP002_Log)
                            {
                                objMIP004.Add(new MIP004()
                                {
                                    HeaderId = objMIP003.HeaderId,
                                    QualityProject = item.QualityProject,
                                    OperationNo = item.OperationNo,
                                    LineRevNo = 0,
                                    MIPRevNo = 0,
                                    ActivityDescription = item.ActivityDescription,
                                    RefDocument = item.RefDocument,
                                    RefDocRevNo = item.RefDocRevNo,
                                    PIA = item.PIA,
                                    IAgencyLNT = item.IAgencyLNT,
                                    IAgencyVVPT = item.IAgencyVVPT,
                                    IAgencySRO = item.IAgencySRO,
                                    RecordsDocuments = "",
                                    Observations = "",
                                    CreatedBy = item.CreatedBy,
                                    CreatedOn = item.CreatedOn,
                                    EditedBy = item.EditedBy,
                                    EditedOn = item.EditedOn,
                                    BU = objMIP003.BU,
                                    Location = objMIP003.Location,
                                    ComponentNo = objMIP003.ComponentNo,
                                    Status = clsImplementationEnum.MIPLineStatus.Added.GetStringValue(),
                                });
                            }
                            db.MIP004.AddRange(objMIP004);
                            db.SaveChanges();
                        }

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "MIP added successfully";
                        objResponseMsg.HeaderId = objMIP003.HeaderId;
                        objResponseMsg.HeaderStatus = objMIP003.Status;
                        objResponseMsg.RevNo = Convert.ToString(objMIP003.RevNo);
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateMIPHeader(int headerId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (headerId > 0)
                {
                    if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                    {
                        if (columnName == "ComponentNo")
                        {
                            MIP003 objMIP003 = db.MIP003.FirstOrDefault(w => w.HeaderId == headerId);
                            if (db.MIP003.Any(w => w.HeaderId != headerId && w.QualityProject == objMIP003.QualityProject && w.Location == objMIP003.Location && w.BU == objMIP003.BU && w.ComponentNo == columnValue))
                            {
                                objResponseMsg.Value = "This Component No. is already taken!";
                                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                            }
                        }
                        db.SP_COMMON_TABLE_UPDATE("MIP003", headerId, "HeaderID", columnName, columnValue, objClsLoginInfo.UserName);//  SP_MIP_HEADER_UPDATE_HEADER_COLUMN(headerId, columnName, columnValue);
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Saved Successfully";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteMIPHeader(int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (headerId > 0)
                {
                    MIP003 objMIP003 = db.MIP003.FirstOrDefault(c => c.HeaderId == headerId);
                    if (objMIP003 != null)
                    {
                        #region Status Validation
                        var lineStatus = new string[] { clsImplementationEnum.MIPHeaderStatus.Draft.GetStringValue(), clsImplementationEnum.MIPHeaderStatus.Returned.GetStringValue() };
                        if (!lineStatus.Contains(objMIP003.Status))
                        {
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "MIP can not deleted when it in '" + objMIP003.Status + "' status";
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }
                        #endregion
                        //MIP004 objParent = db.MIP004.FirstOrDefault(c => c.HeaderId == objMIP003.HeaderId);
                        //if (objParent != null)
                        //{
                        //    objResponseMsg.Value = "Can't able to delete bacause already attached with MIP: " + objParent.RefDocument;
                        //}
                        //else
                        //{
                        db.MIP004.RemoveRange(objMIP003.MIP004);
                        db.MIP003.Remove(objMIP003);
                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "MIP deleted successfully";
                        //}
                    }
                    else
                    {
                        objResponseMsg.Value = "MIP not found";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ReviseMIPHeader(string headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (!string.IsNullOrEmpty(headerId))
                {
                    int intheaderId = Convert.ToInt32(headerId);
                    MIP003 objMIP003 = db.MIP003.FirstOrDefault(c => c.HeaderId == intheaderId);
                    if (objMIP003 != null)
                    {
                        #region Status Validation
                        if (objMIP003.Status != clsImplementationEnum.MIPHeaderStatus.Approved.GetStringValue())
                        {
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "MIP can not revise when it in '" + objMIP003.Status + "' status";
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }
                        #endregion
                        objMIP003.Status = clsImplementationEnum.MIPHeaderStatus.Draft.GetStringValue();
                        objMIP003.RevNo = objMIP003.RevNo + 1;
                        objMIP003.EditedBy = objClsLoginInfo.UserName;
                        objMIP003.EditedOn = DateTime.Now;
                        objMIP003.QCApprovedBy = null;
                        objMIP003.QCApprovedOn = null;
                        objMIP003.QCReturnedBy = null;
                        objMIP003.QCReturnedOn = null;
                        objMIP003.QCReturnRemarks = null;
                        objMIP003.MIP004.ToList().ForEach(x =>
                        {
                            x.MIPRevNo = objMIP003.RevNo.Value;
                            x.EditedBy = objClsLoginInfo.UserName;
                            x.EditedOn = DateTime.Now;
                        });
                        db.SaveChanges();
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "MIP revised successfully";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SubmitMIPHeader(string strHeader)
        {
            ResponceMsgWithMultiValue objResponseMsg = new ResponceMsgWithMultiValue();
            try
            {
                if (!string.IsNullOrEmpty(strHeader))
                {
                    List<string> submittedMIP = new List<string>();
                    List<string> pendingMIP = new List<string>();
                    int[] headerIds = Array.ConvertAll(strHeader.Split(','), s => int.Parse(s));
                    foreach (int headerId in headerIds)
                    {
                        var MIP003 = db.MIP003.Where(x => x.HeaderId == headerId).FirstOrDefault();
                        var result = SendForApproval(headerId);
                        if (result)
                            submittedMIP.Add(MIP003.DocNo);
                        else
                            pendingMIP.Add(MIP003.DocNo);
                    }
                    if (submittedMIP.Count > 0)
                    {
                        objResponseMsg.submittedMIP = "MIP " + String.Join(",", submittedMIP) + " submitted successfully";
                        objResponseMsg.isIdenticalProjectEditable = false;
                    }
                    else
                        objResponseMsg.isIdenticalProjectEditable = true;
                    if (pendingMIP.Count > 0)
                        objResponseMsg.pendingMIP = "MIP " + String.Join(",", pendingMIP) + " not submitted MIP not linked with any operation.";
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "MIP has been sucessfully submitted for approval";
                }
                else
                {
                    objResponseMsg.Value = "Please select atleast one record sending for approval";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult PrintParam(string qualityProject, string bu, string location, string componentnofrom, string componentnoto)
        {
            ResponceMsgWithMultiValue objResponseMsg = new ResponceMsgWithMultiValue();
            try
            {
                //string Components =
                var lstComponentNo = db.MIP004.Where(w => w.QualityProject == qualityProject && w.BU.Trim() == bu.Trim() && w.Location == location).OrderBy(o => o.ComponentNo).ToList();
                bool start = false;
                List<string> newComponentNoist = new List<string>();
                var lstAnnexure = new List<DropdownModel>();
                foreach (MIP004 seam in lstComponentNo)
                {
                    if (seam.ComponentNo == componentnofrom)
                    {
                        start = true;
                    }
                    if (start)
                        newComponentNoist.Add(seam.ComponentNo);
                    if (seam.ComponentNo == componentnoto)
                    {
                        break;
                    }
                }
                if (newComponentNoist.Count > 0)
                    objResponseMsg.Value = string.Join(",", newComponentNoist);

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ApproveMIPHeader(string strHeader, string department)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (!string.IsNullOrEmpty(strHeader))
                {
                    int[] headerIds = Array.ConvertAll(strHeader.Split(','), s => int.Parse(s));
                    foreach (int headerId in headerIds)
                    {
                        if (department == clsImplementationEnum.UserRoleName.QI2.GetStringValue() || department == clsImplementationEnum.UserRoleName.QI3.GetStringValue())
                            ApproveByQC(headerId);
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "MIP(s) has been sucessfully approved";
                }
                else
                {
                    objResponseMsg.Value = "Please select atleast one record for approve";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        private bool ApproveByQC(int headerId)
        {
            var lineStatus = clsImplementationEnum.MIPHeaderStatus.SentForApproval.GetStringValue();
            MIP003 objMIP003 = db.MIP003.FirstOrDefault(i => i.HeaderId == headerId && i.Status == lineStatus);
            if (objMIP003 != null)
            {
                objMIP003.Status = clsImplementationEnum.MIPHeaderStatus.Approved.GetStringValue();
                objMIP003.QCApprovedBy = objClsLoginInfo.UserName;
                objMIP003.QCApprovedOn = DateTime.Now;
                db.SaveChanges();

                #region Maintain Log Details
                List<MIP003_Log> lstMIP003_Log = db.MIP003_Log.Where(c => c.HeaderId == objMIP003.HeaderId).ToList();
                if (lstMIP003_Log.Count > 0)
                {
                    lstMIP003_Log.ForEach(x =>
                    {
                        x.Status = clsImplementationEnum.MIPHeaderStatus.Superseded.GetStringValue();
                    });
                }

                MIP003_Log objMIP003_Log = new MIP003_Log();

                objMIP003_Log.HeaderId = objMIP003.HeaderId;
                objMIP003_Log.QualityProject = objMIP003.QualityProject;
                objMIP003_Log.Project = objMIP003.Project;
                objMIP003_Log.BU = objMIP003.BU;
                objMIP003_Log.Location = objMIP003.Location;
                objMIP003_Log.DocNo = objMIP003.DocNo;
                objMIP003_Log.MIPType = objMIP003.MIPType;
                objMIP003_Log.RevNo = objMIP003.RevNo;
                objMIP003_Log.ComponentNo = objMIP003.ComponentNo;
                objMIP003_Log.DrawingNo = objMIP003.DrawingNo;
                objMIP003_Log.RawMaterialGrade = objMIP003.RawMaterialGrade;
                objMIP003_Log.PlateNo = objMIP003.PlateNo;
                objMIP003_Log.Status = objMIP003.Status;
                objMIP003_Log.CreatedBy = objMIP003.CreatedBy;
                objMIP003_Log.CreatedOn = objMIP003.CreatedOn;
                objMIP003_Log.EditedBy = objMIP003.EditedBy;
                objMIP003_Log.EditedOn = objMIP003.EditedOn;
                objMIP003_Log.SubmittedBy = objMIP003.SubmittedBy;
                objMIP003_Log.SubmittedOn = objMIP003.SubmittedOn;
                objMIP003_Log.QCApprovedBy = objMIP003.QCApprovedBy;
                objMIP003_Log.QCApprovedOn = objMIP003.QCApprovedOn;
                objMIP003_Log.QCReturnedBy = objMIP003.QCReturnedBy;
                objMIP003_Log.QCReturnedOn = objMIP003.QCReturnedOn;
                objMIP003_Log.QCReturnRemarks = objMIP003.QCReturnRemarks;
                db.MIP003_Log.Add(objMIP003_Log);
                db.SaveChanges();

                var lstMIP004_Log = db.MIP004_Log.Where(c => c.HeaderId == objMIP003.HeaderId).ToList();
                if (lstMIP004_Log.Count > 0)
                {
                    lstMIP004_Log.ForEach(x =>
                    {
                        x.Status = clsImplementationEnum.MIPLineStatus.Superseded.GetStringValue();
                    });
                }
                List<MIP004> deletedLines = new List<MIP004>();
                string LineStatusReadyToOffer = clsImplementationEnum.MIPLineStatus.ReadyToOffer.GetStringValue();
                string ClearedStatus = clsImplementationEnum.MIPInspectionStatus.Cleared.GetStringValue();
                string naStatus = clsImplementationEnum.MIPInspectionStatus.Not_Applicatble.GetStringValue();
                string UnderInspection = clsImplementationEnum.MIPInspectionStatus.UnderInspection.GetStringValue();
                string Returned = clsImplementationEnum.MIPInspectionStatus.Returned.GetStringValue();
                string LineStatusDeleted = clsImplementationEnum.MIPLineStatus.Deleted.GetStringValue();

                foreach (MIP004 objMIP004 in objMIP003.MIP004.Where(w => w.InspectionStatus != LineStatusReadyToOffer))
                {
                    if (objMIP004.Status == LineStatusDeleted)
                    {
                        deletedLines.Add(objMIP004);
                        continue;
                    }

                    objMIP004.Status = clsImplementationEnum.MIPLineStatus.Approved.GetStringValue();

                    MIP004_Log objMIP004_Log = new MIP004_Log();
                    objMIP004_Log.RefId = objMIP003_Log.Id;
                    objMIP004_Log.LineId = objMIP004.LineId;
                    objMIP004_Log.HeaderId = objMIP004.HeaderId;
                    objMIP004_Log.QualityProject = objMIP004.QualityProject;
                    objMIP004_Log.OperationNo = objMIP004.OperationNo;
                    objMIP004_Log.LineRevNo = objMIP004.LineRevNo;
                    objMIP004_Log.MIPRevNo = objMIP004.MIPRevNo;
                    objMIP004_Log.ActivityDescription = objMIP004.ActivityDescription;
                    objMIP004_Log.RefDocument = objMIP004.RefDocument;
                    objMIP004_Log.RefDocRevNo = objMIP004.RefDocRevNo;
                    objMIP004_Log.PIA = objMIP004.PIA;
                    objMIP004_Log.IAgencyLNT = objMIP004.IAgencyLNT;
                    objMIP004_Log.IAgencyVVPT = objMIP004.IAgencyVVPT;
                    objMIP004_Log.IAgencySRO = objMIP004.IAgencySRO;
                    objMIP004_Log.RecordsDocuments = objMIP004.RecordsDocuments;
                    objMIP004_Log.Observations = objMIP004.Observations;
                    objMIP004_Log.CreatedBy = objMIP004.CreatedBy;
                    objMIP004_Log.CreatedOn = objMIP004.CreatedOn;
                    objMIP004_Log.EditedBy = objMIP004.EditedBy;
                    objMIP004_Log.EditedOn = objMIP004.EditedOn;
                    objMIP004_Log.Status = objMIP004.Status;
                    db.MIP004_Log.Add(objMIP004_Log);
                }

                db.MIP004.RemoveRange(deletedLines);

                var objlstOffer = objMIP003.MIP004.Where(w => w.InspectionStatus == LineStatusReadyToOffer).ToList();
                foreach (var readyToOfferStage in objlstOffer)
                {
                    readyToOfferStage.InspectionStatus = null;
                    db.SaveChanges();
                }
                //var underInspection = objMIP003.MIP004.OrderBy(o => o.OperationNo).Any(x => x.InspectionStatus == ClearedStatus || x.InspectionStatus == naStatus || x.InspectionStatus == UnderInspection || x.InspectionStatus == Returned);
                //if (!underInspection)
                //{
                //    var objMIP004Offer = objMIP003.MIP004.OrderBy(o => o.OperationNo).FirstOrDefault(w => w.Status != LineStatusDeleted && (w.InspectionStatus == LineStatusReadyToOffer || w.InspectionStatus == null));
                //    if (objMIP004Offer != null)
                //    {
                //        objMIP004Offer.InspectionStatus = LineStatusReadyToOffer;
                //    }
                //}
                MIP004 nextobjMIP004 = db.MIP004.Where(x => x.HeaderId == objMIP003.HeaderId && ((x.InspectionStatus != ClearedStatus && x.InspectionStatus != UnderInspection && x.InspectionStatus != naStatus && x.InspectionStatus == null) || x.InspectionStatus == LineStatusReadyToOffer)).OrderBy(x => x.OperationNo).FirstOrDefault();
                if (nextobjMIP004 != null)
                {
                    nextobjMIP004.InspectionStatus = LineStatusReadyToOffer;
                    nextobjMIP004.EditedBy = objClsLoginInfo.UserName;
                    nextobjMIP004.EditedOn = DateTime.Now;
                }
                db.SaveChanges();
                #endregion

                #region Send Notification
                (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PMG3.GetStringValue(), objMIP003.Project, objMIP003.BU, objMIP003.Location, "MIP: " + objMIP003.DocNo + " is Approved", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/MIP/MaintainMIPHeader/Details/" + objMIP003.HeaderId);
                #endregion

                return true;
            }
            return false;
        }

        [HttpPost]
        public JsonResult ReturnMIPHeader(string strHeader, string strReturnRemarks, string department)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (!string.IsNullOrEmpty(strHeader))
                {
                    int[] headerIds = Array.ConvertAll(strHeader.Split(','), s => int.Parse(s));
                    string[] returnRemarks = strReturnRemarks.Split(',');

                    for (int i = 0; i < headerIds.Length; i++)
                    {
                        if (department == clsImplementationEnum.UserRoleName.QI2.GetStringValue() || department == clsImplementationEnum.UserRoleName.QI3.GetStringValue())
                            ReturnByQC(headerIds[i], returnRemarks[i]);
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "MIP(s) has been sucessfully returned";
                }
                else
                {
                    objResponseMsg.Value = "Please select atleast one record for return";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        private bool SendForApproval(int headerId)
        {
            MIP003 objMIP003 = db.MIP003.FirstOrDefault(i => i.HeaderId == headerId);
            var lineStatus = new string[] { clsImplementationEnum.MIPHeaderStatus.Draft.GetStringValue(), clsImplementationEnum.MIPHeaderStatus.Returned.GetStringValue() };
            if (objMIP003 != null && db.MIP004.Any(i => i.HeaderId == headerId) && lineStatus.Contains(objMIP003.Status))
            {
                objMIP003.Status = clsImplementationEnum.MIPHeaderStatus.SentForApproval.GetStringValue();
                objMIP003.SubmittedBy = objClsLoginInfo.UserName;
                objMIP003.SubmittedOn = DateTime.Now;
                objMIP003.QCApprovedBy = null;
                objMIP003.QCApprovedOn = null;
                objMIP003.QCReturnedBy = null;
                objMIP003.QCReturnedOn = null;
                objMIP003.QCReturnRemarks = null;
                db.SaveChanges();

                #region Send Notification
                (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.QI2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.QI3.GetStringValue(), objMIP003.Project, objMIP003.BU, objMIP003.Location, "MIP: " + objMIP003.DocNo + " has been submitted for your approval", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/MIP/MaintainMIPHeader/Details/" + objMIP003.HeaderId + "?urlForm=a");
                #endregion
                return true;
            }
            return false;
        }

        private bool ReturnByQC(int headerId, string returnRemark)
        {
            var lineStatus = clsImplementationEnum.MIPHeaderStatus.SentForApproval.GetStringValue();
            MIP003 objMIP003 = db.MIP003.FirstOrDefault(i => i.HeaderId == headerId && lineStatus == i.Status);
            if (objMIP003 != null)
            {
                objMIP003.Status = clsImplementationEnum.MIPHeaderStatus.Returned.GetStringValue();
                objMIP003.QCReturnRemarks = returnRemark;
                objMIP003.QCReturnedBy = objClsLoginInfo.UserName;
                objMIP003.QCReturnedOn = DateTime.Now;
                db.SaveChanges();

                #region Send Notification
                (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PMG3.GetStringValue(), objMIP003.Project, objMIP003.BU, objMIP003.Location, "MIP: " + objMIP003.DocNo + " has been returned", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/MIP/MaintainMIPHeader/Details/" + objMIP003.HeaderId);
                #endregion
                return true;
            }
            return false;
        }

        public bool isMIPHeaderExist(string qualityProject, string project, string bu, string location, string DocNo, string MIPType)
        {
            return db.MIP003.Any(i => i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase) && i.Project.Equals(project, StringComparison.OrdinalIgnoreCase) && i.BU.Equals(bu, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && i.DocNo.Equals(DocNo, StringComparison.OrdinalIgnoreCase) && i.MIPType.Equals(MIPType, StringComparison.OrdinalIgnoreCase));
        }
        public bool isMIPHeaderComponentNoExist(string qualityProject, string project, string bu, string location, string ComponentNo)
        {
            return db.MIP003.Any(i => i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase) && i.Project.Equals(project, StringComparison.OrdinalIgnoreCase) && i.BU.Equals(bu, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && i.ComponentNo.Equals(ComponentNo, StringComparison.OrdinalIgnoreCase));
        }
        public bool isMIPHeaderComponentNoCombinationExist(string qualityProject, string project, string bu, string location, string DocNo, string MIPType, string ComponentNo)
        {
            return db.MIP003.Any(i => i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase) && i.Project.Equals(project, StringComparison.OrdinalIgnoreCase) && i.BU.Equals(bu, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && i.DocNo.Trim().Equals(DocNo.Trim(), StringComparison.OrdinalIgnoreCase) && i.MIPType.Trim().Equals(MIPType.Trim(), StringComparison.OrdinalIgnoreCase) && i.ComponentNo.Trim().Equals(ComponentNo.Trim(), StringComparison.OrdinalIgnoreCase));
        }
        #endregion

        #region Lines Details
        [HttpPost]
        public ActionResult GetMIPLinesPartial(int headerId = 0, string BU = "", string Loc = "", string indextype = "")
        {
            MIP003 objMIP003 = new MIP003();

            if (headerId > 0)
                objMIP003 = db.MIP003.Where(i => i.HeaderId == headerId).FirstOrDefault();
            else
            {
                objMIP003.Location = Loc;
                objMIP003.BU = BU;
            }
            UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;

            ViewBag.PIA = Manager.GetSubCatagories("PIA", objMIP003.BU, objMIP003.Location).Select(i => new CategoryData { Value = i.Description, Code = i.Code, CategoryDescription = i.Description }).ToList();
            ViewBag.AgencyLnT = Manager.GetSubCatagories("MIP Inspection Agency L&T", objMIP003.BU, objMIP003.Location).Select(i => new CategoryData { Value = i.Description, Code = i.Code, CategoryDescription = i.Description }).ToList();
            ViewBag.AgencyVVPT = Manager.GetSubCatagories("MIP Inspection Agency VVPT", objMIP003.BU, objMIP003.Location).Select(i => new CategoryData { Value = i.Description, Code = i.Code, CategoryDescription = i.Description }).ToList();
            ViewBag.AgencySRO = Manager.GetSubCatagories("MIP Inspection Agency SRO", objMIP003.BU, objMIP003.Location).Select(i => new CategoryData { Value = i.Description, Code = i.Code, CategoryDescription = i.Description }).ToList();

            ViewBag.indextype = indextype;
            if (string.IsNullOrWhiteSpace(indextype))
                ViewBag.indextype = clsImplementationEnum.MIPIndexType.maintain.GetStringValue();

            return PartialView("_GetMIPLinesDataPartial", objMIP003);
        }

        [HttpPost]
        public JsonResult GetMIPLinesData(JQueryDataTableParamModel param)
        {
            try
            {
                UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();

                int intHeaderId = Convert.ToInt32(param.Headerid);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";
                string indextype = string.Empty;
                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(param.Headerid))
                {
                    whereCondition += " AND M4.HeaderId = " + intHeaderId;
                }
                if (!string.IsNullOrEmpty(param.Department))
                    indextype = param.Department;
                else
                    indextype = clsImplementationEnum.MIPIndexType.maintain.GetStringValue();

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                string[] columnName = { "OperationNo", "RefDocument", "RefDocRevNo", "PIA", "IAgencyLNT", "IAgencyVVPT", "IAgencySRO", "ActivityDescription", "RecordsDocuments", "Observations", "LineRevNo", "m4.Status" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                //else
                //    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);

                var lstResult = db.SP_MIP_HEADER_GET_LINES_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                MIP003 objMIP003 = db.MIP003.Where(c => c.HeaderId == intHeaderId).FirstOrDefault();

                bool isEditable = ((objMIP003.Status == clsImplementationEnum.MIPHeaderStatus.Draft.GetStringValue() || objMIP003.Status == clsImplementationEnum.MIPHeaderStatus.Returned.GetStringValue()) && objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Initiator.GetStringValue());

                int newRecordId = 0;
                var newRecord = new[] {
                                   Convert.ToString(intHeaderId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "OperationNo", "", "", false, "", false, "6"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "ActivityDescription", "", "", false, "", false, "200"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "RefDocument", "", "", false, "", false, "100"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "RefDocRevNo", "", "", false, "", false, "10"),
                                   GenerateAutoCompleteOnBlur(newRecordId, "txtPIA","","","",false,"","PIA")+""+Helper.GenerateHidden(newRecordId,"PIA"),
                                   GenerateAutoCompleteOnBlur(newRecordId, "txtIAgencyLNT","","","",false,"","IAgencyLNT")+""+Helper.GenerateHidden(newRecordId,"IAgencyLNT"),
                                   GenerateAutoCompleteOnBlur(newRecordId, "txtIAgencyVVPT","","","",false,"","IAgencyVVPT")+""+Helper.GenerateHidden(newRecordId,"IAgencyVVPT"),
                                   GenerateAutoCompleteOnBlur(newRecordId, "txtIAgencySRO","","","",false,"","IAgencySRO")+""+Helper.GenerateHidden(newRecordId,"IAgencySRO"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "RecordsDocuments", "", "", false, "", false, "200"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Observations", "", "", false, "", false, "200"),
                                   "R0",
                                   "R0",
                                   clsImplementationEnum.MIPLineStatus.Added.GetStringValue(),
                                   Helper.GenerateGridButtonType(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveMIPLine();")
                                };
                var lineReadyToOfferStatus = clsImplementationEnum.MIPLineStatus.ReadyToOffer.GetStringValue();
                var lineDeletedStatus = clsImplementationEnum.MIPLineStatus.Deleted.GetStringValue();
                var isOffer = new string[] { clsImplementationEnum.MIPInspectionStatus.ReadyToOffer.GetStringValue(), clsImplementationEnum.MIPInspectionStatus.UnderInspection.GetStringValue(), clsImplementationEnum.MIPInspectionStatus.Cleared.GetStringValue(), clsImplementationEnum.MIPInspectionStatus.Rejected.GetStringValue(), clsImplementationEnum.MIPInspectionStatus.Returned.GetStringValue() };

                var data = (from uc in lstResult
                            select new[]
                            {
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                                uc.OperationNo.ToString("G29"),
                                //isEditable ? Helper.GenerateHTMLTextboxOnChanged(uc.LineId, "ActivityDescription", uc.ActivityDescription, "UpdateMIPLine(this,"+ uc.LineId +","+ uc.HeaderId +")", false, "", (uc.Status == clsImplementationEnum.MIPLineStatus.Deleted.GetStringValue()), "200")
                                uc.ActivityDescription,
                                //isEditable ? Helper.GenerateHTMLTextboxOnChanged(uc.LineId, "RefDocument", uc.RefDocument, "UpdateMIPLine(this,"+ uc.LineId +","+ uc.HeaderId +")", false, "", (uc.Status == clsImplementationEnum.MIPLineStatus.Deleted.GetStringValue()), "100") : 
                                uc.RefDocument,
                               //isEditable ? Helper.GenerateHTMLTextboxOnChanged(uc.LineId, "RefDocRevNo", uc.RefDocRevNo, "UpdateMIPLine(this,"+ uc.LineId +","+ uc.HeaderId +")", false, "", (uc.Status == clsImplementationEnum.MIPLineStatus.Deleted.GetStringValue()), "5") :
                               uc.RefDocRevNo,
                               //isEditable ? GenerateAutoCompleteOnBlur(uc.LineId,"txtPIA",Manager.GetCategoryOnlyDescription(uc.PIA,objMIP003.BU,objMIP003.Location  ,"PIA" ),"UpdateMIPLine(this,"+ uc.LineId +","+ uc.HeaderId +")","",false,"","PIA",(uc.PIA), (uc.Status == clsImplementationEnum.LTFPSLineStatus.Deleted.GetStringValue()))+""+Helper.GenerateHidden(uc.LineId,"PIA",(uc.PIA)) :
                               Manager.GetCategoryOnlyDescription(uc.PIA,uc.BU.Split('-')[0],uc.Location.Split('-')[0]  ,"PIA" ),
                               //isEditable ? GenerateAutoCompleteOnBlur(uc.LineId,"txtIAgencyLNT",Manager.GetCategoryOnlyDescription(uc.IAgencyLNT,objMIP003.BU,objMIP003.Location  ,"MIP Inspection Agency L&T" ),"UpdateMIPLine(this,"+ uc.LineId +","+ uc.HeaderId +")","",false,"","IAgencyLNT",uc.IAgencyLNT,(uc.Status == clsImplementationEnum.LTFPSLineStatus.Deleted.GetStringValue()))+""+Helper.GenerateHidden(uc.LineId,"IAgencyLNT",uc.IAgencyLNT)  :
                               Manager.GetCategoryOnlyDescription(uc.IAgencyLNT,uc.BU.Split('-')[0],uc.Location.Split('-')[0]  ,"MIP Inspection Agency L&T" ),
                               //isEditable ? GenerateAutoCompleteOnBlur(uc.LineId,"txtIAgencyVVPT",Manager.GetCategoryOnlyDescription(uc.IAgencyVVPT,objMIP003.BU,objMIP003.Location,"MIP Inspection Agency VVPT"),"UpdateMIPLine(this,"+ uc.LineId +","+ uc.HeaderId +")","",false,"","IAgencyVVPT",uc.IAgencyVVPT,(uc.Status == clsImplementationEnum.LTFPSLineStatus.Deleted.GetStringValue()))+""+Helper.GenerateHidden(uc.LineId,"IAgencyVVPT",uc.IAgencyVVPT)  :
                               Manager.GetCategoryOnlyDescription(uc.IAgencyVVPT,uc.BU.Split('-')[0],uc.Location.Split('-')[0],"MIP Inspection Agency VVPT"),
                               //isEditable ? GenerateAutoCompleteOnBlur(uc.LineId,"txtIAgencySRO",Manager.GetCategoryOnlyDescription(uc.IAgencySRO,objMIP003.BU,objMIP003.Location  ,"MIP Inspection Agency SRO"),"UpdateMIPLine(this,"+ uc.LineId +","+ uc.HeaderId +")","",false,"","IAgencySRO",uc.IAgencySRO,(uc.Status == clsImplementationEnum.LTFPSLineStatus.Deleted.GetStringValue()))+""+Helper.GenerateHidden(uc.LineId,"IAgencySRO",uc.IAgencySRO)  : 
                               Manager.GetCategoryOnlyDescription(uc.IAgencySRO,uc.BU.Split('-')[0],uc.Location.Split('-')[0]  ,"MIP Inspection Agency SRO"),
                               isEditable && !isOffer.Contains(uc.InspectionStatus) ? Helper.GenerateHTMLTextboxOnChanged(uc.LineId, "RecordsDocuments", uc.RecordsDocuments, "UpdateMIPLine(this,"+ uc.LineId +","+ uc.HeaderId +")", false, "", (uc.Status == lineDeletedStatus), "200") : uc.RecordsDocuments,
                               isEditable && !isOffer.Contains(uc.InspectionStatus) ? Helper.GenerateHTMLTextboxOnChanged(uc.LineId, "Observations", uc.Observations, "UpdateMIPLine(this,"+ uc.LineId +","+ uc.HeaderId +")", false, "", (uc.Status == lineDeletedStatus), "200") : uc.Observations,
                               "R"+uc.LineRevNo,
                               "R"+uc.MIPRevNo,
                               uc.Status,
                               "<nobr>"
                               + Helper.HTMLActionString(uc.LineId, "Delete", "Delete Operation", "fa fa-trash-o", "DeleteMIPLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +")", "", (!isOffer.Contains(uc.InspectionStatus) || uc.LineRevNo != 0 || !isEditable || (uc.Status == lineDeletedStatus)) )
                               + "</nobr>"
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult SaveMIPLine(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            MIP004 objMIP004 = new MIP004();
            int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    MIP003 objMIP003 = db.MIP003.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    decimal intOprNo = !string.IsNullOrEmpty(fc["OperationNo" + newRowIndex]) ? Convert.ToDecimal(fc["OperationNo" + newRowIndex]) : 0;
                    string strActivityDescription = !string.IsNullOrEmpty(fc["ActivityDescription" + newRowIndex]) ? fc["ActivityDescription" + newRowIndex].Trim() : string.Empty;
                    string strRefDocument = !string.IsNullOrEmpty(fc["RefDocument" + newRowIndex]) ? fc["RefDocument" + newRowIndex].Trim() : string.Empty;
                    string strDocRevNo = !string.IsNullOrEmpty(fc["RefDocRevNo" + newRowIndex]) ? fc["RefDocRevNo" + newRowIndex].Trim() : string.Empty;
                    string strPIA = !string.IsNullOrEmpty(fc["PIA" + newRowIndex]) ? fc["PIA" + newRowIndex].Trim() : string.Empty;
                    string strIAgencyLNT = !string.IsNullOrEmpty(fc["IAgencyLNT" + newRowIndex]) ? fc["IAgencyLNT" + newRowIndex].Trim() : string.Empty;
                    string strIAgencyVVPT = !string.IsNullOrEmpty(fc["IAgencyVVPT" + newRowIndex]) ? fc["IAgencyVVPT" + newRowIndex].Trim() : string.Empty;
                    string strIAgencySRO = !string.IsNullOrEmpty(fc["IAgencySRO" + newRowIndex]) ? fc["IAgencySRO" + newRowIndex].Trim() : string.Empty;
                    string strRecordsDocuments = !string.IsNullOrEmpty(fc["RecordsDocuments" + newRowIndex]) ? fc["RecordsDocuments" + newRowIndex].Trim() : string.Empty;
                    string strObservations = !string.IsNullOrEmpty(fc["Observations" + newRowIndex]) ? fc["Observations" + newRowIndex].Trim() : string.Empty;

                    if (isMIPLIneExist(objMIP003.HeaderId, intOprNo))
                    {
                        objResponseMsg.Value = "Operation No is already exist";
                    }
                    else
                    {
                        MIP004 objParent = db.MIP004.FirstOrDefault(c => c.HeaderId == objMIP003.HeaderId);

                        #region Add New MIP Line
                        objMIP004.HeaderId = objMIP003.HeaderId;
                        objMIP004.QualityProject = objMIP003.QualityProject;
                        objMIP004.MIPRevNo = objMIP003.RevNo.Value;
                        objMIP004.OperationNo = intOprNo;
                        objMIP004.ActivityDescription = strActivityDescription;
                        objMIP004.PIA = strPIA;
                        objMIP004.IAgencyLNT = strIAgencyLNT;
                        objMIP004.IAgencyVVPT = strIAgencyVVPT;
                        objMIP004.IAgencySRO = strIAgencySRO;
                        objMIP004.RefDocument = strRefDocument;
                        objMIP004.RefDocRevNo = strDocRevNo;
                        objMIP004.RecordsDocuments = strRecordsDocuments;
                        objMIP004.Observations = strObservations;
                        objMIP004.LineRevNo = 0;
                        objMIP004.CreatedBy = objClsLoginInfo.UserName;
                        objMIP004.CreatedOn = DateTime.Now;
                        objMIP004.Status = clsImplementationEnum.MIPLineStatus.Added.GetStringValue();
                        objMIP004.BU = objMIP003.BU;
                        objMIP004.Location = objMIP003.Location;
                        objMIP004.ComponentNo = objMIP003.ComponentNo;

                        db.MIP004.Add(objMIP004);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Operation added successfully";
                        objResponseMsg.HeaderId = objMIP003.HeaderId;
                        objResponseMsg.HeaderStatus = objMIP003.Status;
                        objResponseMsg.RevNo = Convert.ToString(objMIP003.RevNo);
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateMIPLine(int lineId, int headerId, string columnName, string columnValue)
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            MIP004 objMIP004 = new MIP004();
            try
            {
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    objMIP004 = db.MIP004.Where(i => i.LineId == lineId).FirstOrDefault();
                    if (objMIP004.MIPRevNo > 0)
                    {
                        var approved = clsImplementationEnum.MIPLineStatus.Approved.GetStringValue();
                        var objMIP004_log = db.MIP004_Log.FirstOrDefault(w => w.LineId == lineId && w.Status == approved);
                        if (objMIP004_log != null)
                        {
                            objMIP004.LineRevNo = objMIP004_log.LineRevNo + 1;
                            objMIP004.Status = clsImplementationEnum.MIPLineStatus.Modified.GetStringValue();
                        }
                    }
                    db.SP_COMMON_TABLE_UPDATE("MIP004", lineId, "LineID", columnName, columnValue, objClsLoginInfo.UserName); //db.SP_MIP_HEADER_UPDATE_LINE_COLUMN(lineId, columnName, columnValue);

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.LineStatus = objMIP004.Status;
                    objResponseMsg.Value = objMIP004.LineRevNo + ""; // this will update LineRevNo without reload table
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteMIPLine(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                MIP004 objMIP004 = db.MIP004.FirstOrDefault(c => c.LineId == lineId);
                if (objMIP004 != null)
                {
                    if (objMIP004.MIP003.RevNo > 0)
                    {
                        objMIP004.Status = clsImplementationEnum.MIPLineStatus.Deleted.GetStringValue();
                        objMIP004.EditedBy = objClsLoginInfo.UserName;
                        objMIP004.EditedOn = DateTime.Now;
                    }
                    else
                    {
                        db.MIP004.Remove(objMIP004);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Operation deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public bool isMIPLIneExist(int headerId, decimal oprNo)
        {
            return db.MIP004.Any(i => i.HeaderId == headerId && i.OperationNo == oprNo);
        }

        #endregion

        #region History Detail Page

        [HttpPost]
        public ActionResult GetHistoryDataPartial(int HeaderId)
        {
            MIP003_Log objMIP003_Log = new MIP003_Log();
            objMIP003_Log = db.MIP003_Log.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            return PartialView("_GetMIPHistoryDataPartial", objMIP003_Log);
        }

        public ActionResult HistoryDetails(int? id)
        {
            MIP003_Log objMIP003 = new MIP003_Log();
            if (id > 0)
            {
                objMIP003 = db.MIP003_Log.Where(i => i.Id == id).FirstOrDefault();
                ViewBag.DocNoDesc = Manager.GetCategoryOnlyDescription(objMIP003.DocNo, objMIP003.BU, objMIP003.Location, "MIP_DocNo");
                objMIP003.Project = Manager.GetProjectAndDescription(objMIP003.Project);
                objMIP003.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objMIP003.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objMIP003.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objMIP003.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                ViewBag.RevNo = "R" + objMIP003.RevNo;
                ViewBag.isReturned = false;
                if (objMIP003.QCReturnedBy != null)
                {
                    ViewBag.isReturned = true;
                    ViewBag.ReturnBy = objMIP003.QCReturnedBy + " - " + Manager.GetUserNameFromPsNo(objMIP003.QCReturnedBy);
                    ViewBag.ReturnDate = objMIP003.QCReturnedOn.Value.ToString("dd/MM/yyyy hh:mm tt");
                }
                ViewBag.isReleased = false;
            }
            return View(objMIP003);
        }

        [HttpPost]
        public JsonResult GetMIPHistoryHeaderData(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                    whereCondition += " AND MIP003.HeaderId = " + headerId;

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                string[] columnName = { "DocNo", "MIPType", "ComponentNo", "DrawingNo", "RawMaterialGrade", "PlateNo", "RevNo", "QCReturnRemarks", "Status" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);

                var lstResult = db.SP_MIP_HEADER_GET_HISTORY_HEADER_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                            {
                                Manager.GetCategoryOnlyDescription(uc.DocNo,uc.BU.Split('-')[0],uc.Location.Split('-')[0]  ,"MIP_DocNo" ),
                                Manager.GetCategoryOnlyDescription(uc.MIPType,uc.BU.Split('-')[0],uc.Location.Split('-')[0]  ,"MIP_Type" ),
                                !string.IsNullOrWhiteSpace(uc.ComponentNo) ?uc.ComponentNo.ToUpper() : "",
                                uc.DrawingNo,
                                uc.RawMaterialGrade,
                                uc.PlateNo,
                                uc.QCReturnRemarks,
                               "R" + uc.RevNo,
                                uc.Status,
                               "<a target='_blank' href='"+ WebsiteURL + "/MIP/MaintainMIPHeader/HistoryDetails?id=" + uc.Id + "'><i style='margin-right:10px;' class='fa fa-eye'></i></a>" +
                               Helper.HTMLActionString(uc.HeaderId,"Timeline","Timeline","fa fa-clock-o", "ShowTimeline(\"/MIP/MaintainMIPHeader/ShowHistoryTimeline?HeaderID="+ uc.Id +"\");"),
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetMIPHistoryLinesData(JQueryDataTableParamModel param, string RefId)
        {
            try
            {
                int intRefId = Convert.ToInt32(RefId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";
                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(RefId))
                    whereCondition += " AND RefId = " + intRefId;

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                string[] columnName = { "OperationNo", "RefDocument", "RefDocRevNo", "PIA", "IAgencyLNT", "IAgencyVVPT", "IAgencySRO", "ActivityDescription", "RecordsDocuments", "Observations", "LineRevNo", "Status" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);

                var lstResult = db.SP_MIP_HEADER_GET_HISTORY_LINES_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                MIP003_Log objMIP003 = db.MIP003_Log.Where(c => c.Id == intRefId).FirstOrDefault();

                var data = (from uc in lstResult
                            select new[]
                            {
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                                uc.OperationNo.ToString("G29"),
                               uc.ActivityDescription,
                               uc.RefDocument,
                               uc.RefDocRevNo,
                               Manager.GetCategoryOnlyDescription(uc.PIA, objMIP003.BU, objMIP003.Location, "PIA"),
                               Manager.GetCategoryOnlyDescription(uc.IAgencyLNT, objMIP003.BU, objMIP003.Location, "MIP Inspection Agency L&T"),
                               Manager.GetCategoryOnlyDescription(uc.IAgencyVVPT, objMIP003.BU, objMIP003.Location, "MIP Inspection Agency VVPT"),
                               Manager.GetCategoryOnlyDescription(uc.IAgencySRO, objMIP003.BU, objMIP003.Location, "MIP Inspection Agency SRO"),
                               uc.RecordsDocuments,
                               uc.Observations,
                               "R" + uc.LineRevNo,
                               "R" + uc.MIPRevNo,
                               uc.Status,
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Timeline
        public ActionResult ShowHistoryTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();

            model.TimelineTitle = "MIP Timeline";

            if (LineId > 0)
            {
                model.Title = "MIP Lines";
                MIP004_Log objMIP004 = db.MIP004_Log.Where(x => x.Id == LineId).FirstOrDefault();
                model.CreatedBy = objMIP004.CreatedBy != null ? Manager.GetUserNameFromPsNo(objMIP004.CreatedBy) : null;
                model.CreatedOn = objMIP004.CreatedOn;
                model.EditedBy = objMIP004.EditedBy != null ? Manager.GetUserNameFromPsNo(objMIP004.EditedBy) : null;
                model.EditedOn = objMIP004.EditedOn;
            }
            else
            {
                model.Title = "MIP Header";
                MIP003_Log objMIP003 = db.MIP003_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objMIP003.CreatedBy != null ? Manager.GetUserNameFromPsNo(objMIP003.CreatedBy) : null;
                model.CreatedOn = objMIP003.CreatedOn;
                model.EditedBy = objMIP003.EditedBy != null ? Manager.GetUserNameFromPsNo(objMIP003.EditedBy) : null;
                model.EditedOn = objMIP003.EditedOn;
                model.SubmittedBy = objMIP003.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objMIP003.SubmittedBy) : null;
                model.SubmittedOn = objMIP003.SubmittedOn;
                if (objMIP003.QCReturnedBy != null)
                {
                    model.ReturnedBy = Manager.GetUserNameFromPsNo(objMIP003.QCReturnedBy);
                    model.ReturnedOn = objMIP003.QCReturnedOn;
                }
                else
                {
                    model.ReturnedBy = null;
                    model.ReturnedOn = null;
                }
                //model.QCReleaseBy = objMIP003.QCApprovedBy != null ? Manager.GetUserNameFromPsNo(objMIP003.QCApprovedBy) : null;
                //model.QCReleaseOn = objMIP003.QCApprovedOn;
            }
            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        #endregion

        #region Common Methods

        [HttpPost]
        public JsonResult GetQualityProjects(string term)
        {
            string username = objClsLoginInfo.UserName;
            var lstATHLocation = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.Location).Distinct().ToList();
            var lstCOMLocation = db.COM003.Where(i => i.t_psno.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_loca).Distinct().ToList();
            lstCOMLocation = lstATHLocation.Union(lstCOMLocation).ToList();

            var lstCOMBU = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.BU).Distinct().ToList();

            List<Projects> lstQualityProject = new List<Projects>();
            if (!string.IsNullOrEmpty(term))
            {
                lstQualityProject = db.QMS010.Where(i => i.QualityProject.ToLower().Contains(term.ToLower())
                                                                 && lstCOMLocation.Contains(i.Location)
                                                                 && lstCOMBU.Contains(i.BU)
                                                                 && db.QMS001.Any(c => c.Project == i.Project && c.Location == i.Location && c.BU == i.BU)
                                                               ).Select(i => new Projects { Value = i.QualityProject, projectCode = i.QualityProject, BU = i.BU }).Distinct().ToList();//&& i.CreatedBy.Equals(username,StringComparison.OrdinalIgnoreCase)
            }
            else
            {
                lstQualityProject = db.QMS010.Where(i => lstCOMLocation.Contains(i.Location)
                                                        && lstCOMBU.Contains(i.BU)
                                                        && db.QMS001.Any(c => c.Project == i.Project && c.Location == i.Location && c.BU == i.BU)
                                                        ).Select(i => new Projects { Value = i.QualityProject, projectCode = i.QualityProject, BU = i.BU }).Take(10).Distinct().ToList();
            }

            return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetQualityProjectWiseProjectBUDetail(string qualityProject)
        {
            QualityProjectsDetails objProjectsDetails = new QualityProjectsDetails();
            var project = db.QMS010.Where(i => i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase)).Select(i => i.Project).FirstOrDefault();

            objProjectsDetails.ProjectCode = project;
            objProjectsDetails.projectDescription = db.COM001.Where(i => i.t_cprj.Equals(project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + "-" + i.t_dsca).FirstOrDefault();// project;

            var ProjectWiseBULocation = Manager.getProjectWiseBULocation(project);

            objProjectsDetails.BUCode = ProjectWiseBULocation.QCPBU.Split('-')[0].Trim();
            objProjectsDetails.BUDescription = ProjectWiseBULocation.QCPBU;

            objProjectsDetails.LocationCode = objClsLoginInfo.Location;
            objProjectsDetails.LocationDescription = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objClsLoginInfo.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();

            return Json(objProjectsDetails, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetCategory(string term, string BU = "")
        {
            List<Projects> lstCategory = new List<Projects>();
            //if (!string.IsNullOrEmpty(term))
            //{
            lstCategory = db.GLB002.Where(i => i.BU == BU && i.Location == objClsLoginInfo.Location)
                                             .Select(i => new Projects { Value = i.Code }).Distinct().ToList();//&& i.CreatedBy.Equals(username,StringComparison.OrdinalIgnoreCase)
            //}
            //else
            //{
            //    lstCategory = db.GLB002.Where(i => i.BU == BU && i.Location == objClsLoginInfo.Location)
            //                                             .Select(i => new Projects { Value = i.Code })
            //                                             .Take(10).Distinct().ToList();
            //}
            return Json(lstCategory, JsonRequestBehavior.AllowGet);
        }

        public UserRoleAccessDetails GetUserAccessRights()
        {
            UserRoleAccessDetails objUserRoleAccessDetails = new UserRoleAccessDetails();
            try
            {
                var role = (from a in db.ATH001
                            join b in db.ATH004 on a.Role equals b.Id
                            where a.Employee.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                            select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();

                if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.QI2.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.QI2.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Approver.GetStringValue();
                }
                else if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.QI3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.QI3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Approver.GetStringValue();
                }
                else if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.PMG3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Initiator.GetStringValue();
                }
                else
                {
                    objUserRoleAccessDetails.UserRole = "";
                    objUserRoleAccessDetails.UserDesignation = "";
                }
            }
            catch
            {
            }
            return objUserRoleAccessDetails;
        }

        /*public string HTMLActionString(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", string onHrefURL = "", bool isDisable = false)
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";
            string onHref = !string.IsNullOrEmpty(onHrefURL) ? "href='" + onHrefURL + "'" : "";

            if (isDisable)
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer; opacity:0.3; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";
            }
            else
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            }

            if (!string.IsNullOrEmpty(onHref))
            {
                htmlControl = "<a " + onHref + " >" + htmlControl + "</a>";
            }

            return htmlControl;
        }*/
        public string GenerateAutoCompleteOnBlur(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", string oldValue = "", bool disabled = false)
        {
            string strAutoComplete = string.Empty;
            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick=" + onClickMethod + "" : "";
            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' data-lineid='" + rowId + "' hdElement='" + hdElementId + "' value='" + inputValue + "' data-oldvalue='" + oldValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + " " + (disabled ? "disabled" : "") + " />";

            return strAutoComplete;
        }

        public string GenerateRemark(int headerId, string columnName, string userAccessRole, string userRole, bool isEditable = false)
        {
            MIP003 objMIP003 = db.MIP003.FirstOrDefault(c => c.HeaderId == headerId);
            string remarkValue = (!string.IsNullOrWhiteSpace(objMIP003.QCReturnedBy) ? objMIP003.QCReturnRemarks : "");
            if (isEditable)
                return Helper.GenerateTextbox(objMIP003.HeaderId, columnName, remarkValue, "", false, "", "200");
            return remarkValue;
        }
        public class ResponceMsgWithMultiValue : clsHelper.ResponseMsg
        {
            public string pendingMIP { get; set; }
            public string submittedMIP { get; set; }
            public bool isIdenticalProjectEditable { get; set; }
        }
        #endregion

        #region ExcelHelper
        [SessionExpireFilter]
        public JsonResult ImportExcel(HttpPostedFileBase upload, string aQualityProject, string aProject, string aBU, string aLocation)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            MIP003 objImport = new MIP003();
            objImport.QualityProject = aQualityProject.Trim();
            objImport.Project = aProject.Trim();
            objImport.BU = aBU.Trim();
            objImport.Location = aLocation.Trim();
            if (upload != null && !string.IsNullOrWhiteSpace(aProject))
            {
                if (Path.GetExtension(upload.FileName).ToLower() == ".xlsx" || Path.GetExtension(upload.FileName).ToLower() == ".xls")
                {
                    ExcelPackage package = new ExcelPackage(upload.InputStream);
                    ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();

                    DataTable dtHeaderExcel = new DataTable();
                    List<CategoryData> errors = new List<CategoryData>();
                    string Approved = clsImplementationEnum.MIPHeaderStatus.Approved.GetStringValue();
                    string Draft = clsImplementationEnum.MIPHeaderStatus.Draft.GetStringValue();
                    string Returned = clsImplementationEnum.MIPHeaderStatus.Returned.GetStringValue();
                    bool isError;

                    DataSet ds = ToChechValidation(package, objImport, out isError);
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                        objResponseMsg.Value = "No Record Found!";
                    }
                    else if (!isError)
                    {
                        try
                        {
                            foreach (DataRow item in ds.Tables[0].Rows)
                            {
                                MIP003 objMIP003 = new MIP003();
                                string DocNo = item.Field<string>("DocNo");
                                string MIPType = item.Field<string>("MIPType");
                                string ComponentNo = item.Field<string>("ComponentNo");
                                string DrawingNo = item.Field<string>("DrawingNo");
                                string RawMaterialGrade = item.Field<string>("RawMaterialGrade");
                                string PlateNo = item.Field<string>("PlateNo");
                                string QCReturnRemarks = item.Field<string>("QCReturnRemarks");

                                #region Add New MIP Header
                                objMIP003.QualityProject = objImport.QualityProject;
                                objMIP003.Project = objImport.Project;
                                objMIP003.BU = objImport.BU;
                                objMIP003.Location = objImport.Location;
                                objMIP003.DocNo = DocNo;
                                objMIP003.MIPType = MIPType;
                                objMIP003.ComponentNo = ComponentNo;
                                objMIP003.DrawingNo = DrawingNo;
                                objMIP003.RawMaterialGrade = RawMaterialGrade;
                                objMIP003.PlateNo = PlateNo;
                                objMIP003.RevNo = 0;
                                objMIP003.Status = clsImplementationEnum.MIPHeaderStatus.Draft.GetStringValue();
                                objMIP003.CreatedBy = objClsLoginInfo.UserName;
                                objMIP003.CreatedOn = DateTime.Now;

                                db.MIP003.Add(objMIP003);
                                db.SaveChanges();
                                var objMIP001_log = db.MIP001_Log.FirstOrDefault(w => w.Status == Approved && w.QualityProject == objMIP003.QualityProject && w.Location == objMIP003.Location && w.BU == objMIP003.BU && w.DocNo == DocNo && w.MIPType == MIPType);
                                if (objMIP001_log != null)
                                {
                                    var objMIP002_Log = db.MIP002_Log.Where(w => w.Status == Approved && w.HeaderId == objMIP001_log.HeaderId).ToList();
                                    var objMIP004 = new List<MIP004>();
                                    foreach (var item002 in objMIP002_Log)
                                    {
                                        objMIP004.Add(new MIP004()
                                        {
                                            HeaderId = objMIP003.HeaderId,
                                            QualityProject = item002.QualityProject,
                                            OperationNo = item002.OperationNo,
                                            LineRevNo = 0,
                                            MIPRevNo = 0,
                                            ActivityDescription = item002.ActivityDescription,
                                            RefDocument = item002.RefDocument,
                                            RefDocRevNo = item002.RefDocRevNo,
                                            PIA = item002.PIA,
                                            IAgencyLNT = item002.IAgencyLNT,
                                            IAgencyVVPT = item002.IAgencyVVPT,
                                            IAgencySRO = item002.IAgencySRO,
                                            RecordsDocuments = "",
                                            Observations = "",
                                            CreatedBy = item002.CreatedBy,
                                            CreatedOn = item002.CreatedOn,
                                            EditedBy = item002.EditedBy,
                                            EditedOn = item002.EditedOn,
                                            BU = objMIP003.BU,
                                            Location = objMIP003.Location,
                                            ComponentNo = objMIP003.ComponentNo,
                                            Status = clsImplementationEnum.MIPLineStatus.Added.GetStringValue(),
                                        });
                                    }
                                    db.MIP004.AddRange(objMIP004);
                                    db.SaveChanges();
                                }
                                #endregion
                            }
                            db.SaveChanges();

                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "MIP imported successfully";
                        }
                        catch (Exception ex)
                        {
                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        }
                    }
                    else
                    {
                        objResponseMsg = ErrorExportToExcel(ds);
                    }
                }
                else
                {
                    objResponseMsg.Value = "Excel file is not valid";
                }
            }
            else
            {
                objResponseMsg.Value = "Please enter valid Project no.";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponceMsgWithFileName ErrorExportToExcel(DataSet ds)
        {
            clsHelper.ResponceMsgWithFileName objResponseMsg = new clsHelper.ResponceMsgWithFileName();
            try
            {
                MemoryStream ms = new MemoryStream();
                using (FileStream fs = System.IO.File.OpenRead(Server.MapPath("~/Resources/MIP/MIP Excel Template.xlsx")))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();
                        int i = 2;
                        foreach (DataRow item in ds.Tables[0].Rows)
                        {
                            //Doc No.
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("DocNoErrorMsg")))
                            {
                                excelWorksheet.Cells[i, 1].Value = item.Field<string>(0) + " (Note: " + item.Field<string>("DocNoErrorMsg") + " )";
                                excelWorksheet.Cells[i, 1].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 1].Value = item.Field<string>(0); }

                            //MIPType
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("MIPTypeErrorMsg")))
                            {
                                excelWorksheet.Cells[i, 2].Value = item.Field<string>(1) + " (Note: " + item.Field<string>("MIPTypeErrorMsg").ToString() + " )";
                                excelWorksheet.Cells[i, 2].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 2].Value = item.Field<string>(1); }

                            //ComponentNo
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("ComponentNoErrorMsg")))
                            {
                                excelWorksheet.Cells[i, 3].Value = item.Field<string>(2) + " (Note: " + item.Field<string>("ComponentNoErrorMsg").ToString() + " )";
                                excelWorksheet.Cells[i, 3].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 3].Value = item.Field<string>(2); }

                            //DrawingNo
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("DrawingNoErrorMsg")))
                            {
                                excelWorksheet.Cells[i, 4].Value = item.Field<string>(3) + " (Note: " + item.Field<string>("DrawingNoErrorMsg").ToString() + " )";
                                excelWorksheet.Cells[i, 4].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 4].Value = item.Field<string>(3); }

                            //RawMaterialGrade
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("RawMaterialGradeErrorMsg")))
                            {
                                excelWorksheet.Cells[i, 5].Value = item.Field<string>(4) + " (Note: " + item.Field<string>("RawMaterialGradeErrorMsg").ToString() + " )";
                                excelWorksheet.Cells[i, 5].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 5].Value = item.Field<string>(4); }

                            //PlateNo
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("PlateNoErrorMsg")))
                            {
                                excelWorksheet.Cells[i, 6].Value = item.Field<string>(5) + " (Note: " + item.Field<string>("PlateNoErrorMsg").ToString() + " )";
                                excelWorksheet.Cells[i, 6].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 6].Value = item.Field<string>(5); }

                            //Return remarks
                            excelWorksheet.Cells[i, 7].Value = item.Field<string>(6);
                            //RevNo
                            excelWorksheet.Cells[i, 8].Value = item.Field<string>(7);
                            //status
                            excelWorksheet.Cells[i, 9].Value = item.Field<string>(8);
                            i++;
                        }
                        excelPackage.SaveAs(ms);
                    }
                }

                ms.Position = 0;

                string fileName;
                string excelFilePath = Helper.ExcelFilePath(objClsLoginInfo.UserName, out fileName);

                Byte[] bin = ms.ToArray();
                System.IO.File.WriteAllBytes(excelFilePath, bin);

                objResponseMsg.Value = "Error";
                objResponseMsg.FileName = fileName;
                return objResponseMsg;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }

        public DataSet ToChechValidation(ExcelPackage package, MIP003 objImport, out bool isError)
        {
            ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();
            DataTable dtHeaderExcel = new DataTable();
            isError = false;
            int notesIndex = 0;
            try
            {
                #region Read Lines data from excel 
                foreach (var firstRowCell in excelWorksheet.Cells[1, 1, 1, excelWorksheet.Dimension.End.Column])
                {
                    dtHeaderExcel.Columns.Add(firstRowCell.Text);
                }

                for (var rowNumber = 2; rowNumber <= excelWorksheet.Dimension.End.Row; rowNumber++)
                {
                    var row = excelWorksheet.Cells[rowNumber, 1, rowNumber, excelWorksheet.Dimension.End.Column];
                    var newRow = dtHeaderExcel.NewRow();
                    if (excelWorksheet.Cells[rowNumber, 1].Value != null && excelWorksheet.Cells[rowNumber, 2].Value != null
                        && excelWorksheet.Cells[rowNumber, 3].Value != null)
                    {
                        foreach (var cell in row)
                        {
                            newRow[cell.Start.Column - 1] = cell.Text;
                        }
                    }
                    else
                    {
                        notesIndex = rowNumber + 1;
                        break;
                    }
                    dtHeaderExcel.Rows.Add(newRow);
                }
                #endregion

                #region Validation for Lines
                dtHeaderExcel.Columns.Add("DocNoErrorMsg", typeof(string));//column 8
                dtHeaderExcel.Columns.Add("MIPTypeErrorMsg", typeof(string));//column 9
                dtHeaderExcel.Columns.Add("ComponentNoErrorMsg", typeof(string));//column 10
                dtHeaderExcel.Columns.Add("DrawingNoErrorMsg", typeof(string));//column 11
                dtHeaderExcel.Columns.Add("RawMaterialGradeErrorMsg", typeof(string));//column 12
                dtHeaderExcel.Columns.Add("PlateNoErrorMsg", typeof(string));//column 13
                dtHeaderExcel.Columns.Add("StatusErrorMsg", typeof(string));//column 14

                var lstMIPDocNoValues = Manager.GetSubCatagories("MIP_DocNo", objImport.BU.Trim(), objImport.Location.Trim());
                var lstMIPTypeValues = Manager.GetSubCatagories("MIP_Type", objImport.BU.Trim(), objImport.Location.Trim());

                foreach (DataRow item in dtHeaderExcel.Rows)
                {
                    string DocNo = item.Field<string>("DocNo"), DocNoCode = null;
                    string MIPType = item.Field<string>("MIPType"), MIPTypeCode = null;
                    string ComponentNo = item.Field<string>("ComponentNo");
                    string DrawingNo = item.Field<string>("DrawingNo");
                    string RawMaterialGrade = item.Field<string>("RawMaterialGrade");
                    string PlateNo = item.Field<string>("PlateNo");
                    string QCReturnRemarks = item.Field<string>("QCReturnRemarks");

                    string errorMessage = string.Empty;

                    if (string.IsNullOrEmpty(DocNo))
                    {
                        item["DocNoErrorMsg"] = "Doc No is required";
                        isError = true;
                    }
                    else if ((DocNoCode = lstMIPDocNoValues.Where(w => w.Description == DocNo).Select(s => s.Code).FirstOrDefault()) == null)
                    {
                        item["DocNoErrorMsg"] = "Doc No is not valid";
                        isError = true;
                    }
                    else
                        item["DocNo"] = DocNoCode;

                    if (string.IsNullOrEmpty(MIPType))
                    {
                        item["MIPTypeErrorMsg"] = "MIP Type is required";
                        isError = true;
                    }
                    else if ((MIPTypeCode = lstMIPTypeValues.Where(w => w.Description == MIPType).Select(s => s.Code).FirstOrDefault()) == null)
                    {
                        item["MIPTypeErrorMsg"] = "MIP Type is not valid";
                        isError = true;
                    }
                    else
                        item["MIPType"] = MIPTypeCode;

                    if (string.IsNullOrEmpty(DrawingNo))
                    {
                        item["DrawingNoErrorMsg"] = "Drawing No is required";
                        isError = true;
                    }
                    else if (DrawingNo.Length > 30)
                    {
                        item["DrawingNoErrorMsg"] = "Drawing No maxlength exceeded";
                        isError = true;
                    }

                    if (string.IsNullOrEmpty(RawMaterialGrade))
                    {
                        item["RawMaterialGradeErrorMsg"] = "Raw Material Grade is required";
                        isError = true;
                    }
                    else if (RawMaterialGrade.Length > 20)
                    {
                        item["RawMaterialGradeErrorMsg"] = "Raw Material Grade maxlength exceeded";
                        isError = true;
                    }

                    if (string.IsNullOrEmpty(PlateNo))
                    {
                        item["PlateNoErrorMsg"] = "Plate No is required";
                        isError = true;
                    }
                    else if (PlateNo.Length > 20)
                    {
                        item["PlateNoErrorMsg"] = "Plate No maxlength exceeded";
                        isError = true;
                    }

                    if (string.IsNullOrEmpty(ComponentNo))
                    {
                        item["ComponentNoErrorMsg"] = "Component No is required";
                        isError = true;
                    }
                    else if (ComponentNo.Length > 25)
                    {
                        item["ComponentNoErrorMsg"] = "Component No maxlength exceeded";
                        isError = true;
                    }
                    else
                    {
                        item["ComponentNo"] = ComponentNo = ComponentNo.ToUpper();
                        if (isMIPHeaderComponentNoCombinationExist(objImport.QualityProject, objImport.Project, objImport.BU, objImport.Location, DocNoCode, MIPTypeCode, ComponentNo))
                        {
                            item["ComponentNoErrorMsg"] = "MIP Component No " + ComponentNo + " for QualityProject :" + objImport.QualityProject + " & Document No: " + DocNo + " is already exist";
                            isError = true;
                        }
                    }
                    if (!db.MIP001.Any(a => a.QualityProject == objImport.QualityProject && a.Project == objImport.Project && a.BU == objImport.BU && a.Location == objImport.Location && a.DocNo == DocNoCode && a.MIPType == MIPTypeCode))
                    {
                        item["DocNoErrorMsg"] = "MIP Master Document No: " + DocNo + " & MIP Type: " + MIPType + " for QualityProject :" + objImport.QualityProject + " is not exist";
                        isError = true;
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            DataSet ds = new DataSet();
            ds.Tables.Add(dtHeaderExcel);
            return ds;
        }

        public JsonResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "", int Headerid = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.MAINTAININDEX.GetStringValue())
                {
                    var lst = db.SP_MIP_HEADER_GET_PROJECT_INDEX_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from a in lst
                                  select new
                                  {
                                      QualityProject = a.QualityProject,
                                      Project = a.Project,
                                      BU = a.BU,
                                      Location = a.Location,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_MIP_HEADER_GET_HEADER_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      DocNo = Manager.GetCategoryOnlyDescription(uc.DocNo, uc.BU, uc.Location, "MIP_DocNo"),
                                      MIPType = Manager.GetCategoryOnlyDescription(uc.MIPType, uc.BU, uc.Location, "MIP_Type"),
                                      ComponentNo = !string.IsNullOrWhiteSpace(uc.ComponentNo) ? uc.ComponentNo.ToUpper() : "",
                                      DrawingNo = uc.DrawingNo,
                                      RawMaterialGrade = uc.RawMaterialGrade,
                                      PlateNo = uc.PlateNo,
                                      QCReturnRemarks = uc.QCReturnRemarks,
                                      RevNo = "R" + Convert.ToString(uc.RevNo),
                                      Status = Convert.ToString(uc.Status),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_MIP_HEADER_GET_LINES_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    MIP001 objMIP001 = db.MIP001.Where(c => c.HeaderId == Headerid).FirstOrDefault();
                    if (!lst.Any())
                    {
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      OperationNo = uc.OperationNo.ToString("G29"),
                                      ActivityDescription = uc.ActivityDescription,
                                      RefDocument = uc.RefDocument,
                                      RefDocRevNo = uc.RefDocRevNo,
                                      PIA = Manager.GetCategoryOnlyDescription(uc.PIA, objMIP001.BU, objMIP001.Location, "PIA"),
                                      IAgencyLNT = Manager.GetCategoryOnlyDescription(uc.IAgencyLNT, objMIP001.BU, objMIP001.Location, "MIP Inspection Agency L&T"),
                                      IAgencyVVPT = Manager.GetCategoryOnlyDescription(uc.IAgencyVVPT, objMIP001.BU, objMIP001.Location, "MIP Inspection Agency VVPT"),
                                      IAgencySRO = Manager.GetCategoryOnlyDescription(uc.IAgencySRO, objMIP001.BU, objMIP001.Location, "MIP Inspection Agency SRO"),
                                      RecordsDocuments = uc.RecordsDocuments,
                                      Observations = uc.Observations,
                                      LineRevNo = "R" + uc.LineRevNo,
                                      MIPRevNo = "R" + uc.MIPRevNo,
                                      Status = uc.Status
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

    }
}