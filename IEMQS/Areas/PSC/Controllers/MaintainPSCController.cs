﻿using IEMQS.Areas.PSC.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Unity.Injection;
using static IEMQSImplementation.clsBase;

namespace IEMQS.Areas.PSC.Controllers
{
    public class MaintainPSCController : clsBase
    {
        // GET: PSC/MaintainPSC
        [SessionExpireFilter]
        public ActionResult Index()
        {
            ViewBag.Title = "Maintain PSC";
            return View();
        }
        [SessionExpireFilter]
        public ActionResult Detail(int? headerId)
        {
            try
            {
                if (headerId == null)
                {
                    ViewData["LineField"] = db.PSC001.ToList();
                }
                else
                {
                    var Query = "select SrNo as 'LineId'," + headerId + " as 'HeaderId',SrNo,CheckPoints,[Type],NULL as 'Complied',NULL as 'ObjectiveEvidence','NULL' as 'CreatedBy','NULL' as 'EditedBy',NULL as 'CreatedOn',NULL as 'EditedOn' from PSC001 where SrNo not in (select SrNo from PSC011 Where HeaderId='" + headerId + "') Union Select * from PSC011 where HeaderId='" + headerId + "' Order By SrNo asc";

                    ViewData["LineFieldWithData"] = db.Database.SqlQuery<PSC011>(Query).ToList();

                    ViewBag.HeaderData = db.PSC010.Where(x => x.HeaderId == headerId).ToList();
                    var Carriedpsno = db.PSC010.Where(x => x.HeaderId == headerId).Select(x => x.CarriedOutBy).FirstOrDefault();
                    var AckPsno = db.PSC010.Where(x => x.HeaderId == headerId).Select(x => x.AckBy).FirstOrDefault();
                    ViewBag.CarriedOutName = db.COM003.Where(x => x.t_psno == Carriedpsno).Select(x => x.t_name).FirstOrDefault();
                    ViewBag.ACkName = db.COM003.Where(x => x.t_psno == AckPsno).Select(x => x.t_name).FirstOrDefault();
                }

                ViewBag.HeaderId = headerId;
                ViewBag.isAuthAdmin = (IsAuthAdmin(objClsLoginInfo.UserName) ? 1 : 0);
                var Role = clsImplementationEnum.UserRoleName.MFG1.GetStringValue() + "," + clsImplementationEnum.UserRoleName.MFG2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PROD3.GetStringValue();
                ViewBag.Users = (new GeneralController()).GetRoleWiseEmployee(Role).ToList();
                UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
                ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            }
            catch (Exception)
            {
                throw;
            }
            return View();
        }

        public string GetName(string ackpsno)
        {
            ViewBag.ACkName = db.COM003.Where(x => x.t_psno == ackpsno).Select(x => x.t_name).FirstOrDefault();
            return ViewBag.ACkName;
        }
       
        private bool IsAuthAdmin(string UserPSNo)
        {
            var UserRoles = (from a in db.ATH001
                             join b in db.ATH004 on a.Role equals b.Id
                             where a.Employee.Equals(UserPSNo, StringComparison.OrdinalIgnoreCase)
                             select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();

            bool isAdmin = false;
            if (UserRoles.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.ITA1.GetStringValue(), StringComparison.OrdinalIgnoreCase) || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.ITA2.GetStringValue(), StringComparison.OrdinalIgnoreCase) || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.ITA3.GetStringValue(), StringComparison.OrdinalIgnoreCase) || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG1.GetStringValue(), StringComparison.OrdinalIgnoreCase) || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG2.GetStringValue(), StringComparison.OrdinalIgnoreCase) || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
            {
                isAdmin = true;
            }
            return isAdmin;
        }

        [HttpPost]
        public ActionResult GetAllAuthorisedQualityProject(string term)
        {
            try
            {
                var lstProjectByUser = Manager.getProjectsByUser(objClsLoginInfo.UserName.Trim());
                var item = (from lst in lstProjectByUser
                            join qms10 in db.QMS010 on lst.projectCode equals qms10.Project
                            where (term == "" || qms10.QualityProject.ToLower().Trim().Contains(term.ToLower().Trim()))
                            orderby qms10.QualityProject
                            select new { Value = qms10.QualityProject, Text = qms10.QualityProject }).Distinct().Take(10).ToList();

                return Json(item, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        //public ActionResult GetAllPartbyProject(string param = "")
        //{
        //    try
        //    {
        //        string project = db.QMS010.Where(x => x.QualityProject == param).Select(x => x.Project).FirstOrDefault();
        //        if (!string.IsNullOrWhiteSpace(project))
        //        {
        //            var result = (from vw in db.VW_IPI_GETHBOMLIST
        //                          where vw.Project == project
        //                          select new { FindNo = vw.FindNo, Part = vw.Part, Description = vw.Description }).Distinct().ToList();

        //            var items = (from li in result
        //                         select new
        //                         {
        //                             id = li.FindNo.ToString(),
        //                             text = li.Part.ToString() + " - " + li.Description.ToString()
        //                         }).Distinct().ToList();
        //            return Json(items, JsonRequestBehavior.AllowGet);
        //        }
        //        else
        //        {
        //            return Json(null, JsonRequestBehavior.AllowGet);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        return Json("", JsonRequestBehavior.AllowGet);
        //    }
        //}

        public ActionResult GetProjectWiseSeamNo(string param = "")
        {
            try
            {
                string project = db.QMS010.Where(x => x.QualityProject == param).Select(x => x.Project).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(project))
                {
                    var Seam = db.QMS012.Where(x => x.Project == project).Select(x => new { id = x.LineId, text = x.SeamNo }).Distinct().ToList();

                    return Json(Seam, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveHeaderData(ProcessSpotCheckModel model)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var temp = db.PSC010.Find(model.HeaderId);
                PSC010 p = temp == null ? new PSC010() : temp;                
                if (model.button != "Acknowledge")
                {
                    p.Dept = model.Dept;
                    p.Project = model.Project;
                    p.ShopInCharge = model.ShopInCharge;
                    p.ItemNo = model.ItemNo.TrimEnd(',');
                    p.SeamNo = model.SeamNo.TrimEnd(',');
                    p.DescriptionOfActivity = model.DescriptionOfActivity;
                    p.CarriedOutBy = model.CarriedOutBy;
                    p.Reciepient = model.Reciepient.TrimEnd(',');
                    p.Customer = model.Customer;
                    p.ActionTaken = model.ActionTaken;
                    p.Status = "Pending";
                    string[] r = p.Reciepient.Split(',');
                    for (int i = 0; i < r.Count(); i++)
                    {
                        if (objClsLoginInfo.UserName == r[i])
                        {
                            p.Status = "Submited";
                        }                        
                    }
                }
                else
                    p.Status = "Submited";

                if (temp == null)
                {
                    p.SPCNo = "PSC-" + GenerateReportNumber();
                    p.ReportNo = GenerateReportNumber();
                    p.CreatedBy = objClsLoginInfo.UserName;
                    p.CreatedOn = DateTime.Now;
                    db.PSC010.Add(p);
                }
                else
                {
                    p.AckBy = model.AckBy;
                    p.ReportNo = temp.ReportNo;
                    p.EditedBy = objClsLoginInfo.UserName;
                    p.EditedOn = DateTime.Now;

                }
                db.SaveChanges();
                Int64? HeaderId = p.HeaderId;
                string ReportNo = "PSC-" + p.ReportNo;
                bool success = false;
                int check = 0;
                if (model.LineDetails != null)
                {
                    var lineGroup = model.LineDetails.GroupBy(x => x.Type);
                    {
                        foreach (var grp in lineGroup)
                        {
                            foreach (var line in grp)
                            {
                                var lineTemp = db.PSC011.Find(line.LineId);
                                PSC011 ps = lineTemp == null ? new PSC011() : lineTemp;

                                ps.HeaderId = HeaderId;
                                ps.SrNo = line.SrNo;
                                ps.CheckPoints = line.CheckPoints;
                                ps.Type = line.Type;
                                ps.Complied = line.Complied;
                                ps.ObjectiveEvidence = line.ObjectiveEvidence;

                                if (lineTemp == null)
                                {
                                    ps.CreatedBy = objClsLoginInfo.UserName;
                                    ps.CreatedOn = DateTime.Now;
                                    if (ps.Complied =="2")
                                    {
                                        check = 1;
                                    }
                                    db.PSC011.Add(ps);
                                    success = true;
                                }
                                else
                                {
                                    ps.EditedBy = objClsLoginInfo.UserName;
                                    ps.EditedOn = DateTime.Now;
                                }
                                db.SaveChanges();
                            }
                        }
                    }
                }
                if(check==0)
                {
                    var statustemp = db.PSC010.Find(HeaderId);
                    PSC010 pstatus = statustemp == null ? new PSC010() : statustemp;
                    pstatus.Status = "Submited";
                    pstatus.AckBy= model.Reciepient.TrimEnd(',');
                    //pstatus.EditedBy = objClsLoginInfo.UserName;
                    pstatus.EditedOn = DateTime.Now;
                    db.SaveChanges();
                }
                if (success)
                {
                    #region generate pdf

                    string filenameformat = ReportNo;
                    string downloadpath = "~/Resources/PSCEmailAttachments/";
                    string Reporturl = "/PSC/PSC_Report";

                    List<SSRSParam> reportParams = new List<SSRSParam>();
                    reportParams.Add(new SSRSParam { Param = "HeaderId", Value = Convert.ToString(HeaderId) });

                    //string enumICSReportImageURL = clsImplementationEnum.ConfigParameter.ExternalImagePath.GetStringValue();

                    //string ICSReportImageURL = Manager.GetConfigValue(enumICSReportImageURL).ToString();

                    string reportFilename = (new Utility.Controllers.GeneralController()).GeneratePDFfromSSRSreport(Server.MapPath(downloadpath + filenameformat + ".pdf"), Reporturl, reportParams);

                    //string reportImageFilename = (new Utility.Controllers.GeneralController()).GenerateUploadImagefromSSRSreport(Server.MapPath(downloadpath + filenameformat + ".jpeg"), Reporturl, reportParams);

                    string folderpath = "PSC001/" + HeaderId;
                    var doc = (new clsFileUpload()).GetDocuments(folderpath, true).ToList();

                    #endregion

                    #region Send Mail
                    List<string> emailTo = new List<string>();
                    string emailfrom = objClsLoginInfo.UserName;

                    if (!string.IsNullOrWhiteSpace(model.Reciepient))
                    {
                        foreach (var person in model.Reciepient.Split(','))
                        {
                            emailTo.Add(Manager.GetMailIdFromPsNo(person));
                        }
                    }

                    if (emailTo.Count > 0)
                    {
                        try
                        {
                            Hashtable _ht = new Hashtable();
                            EmailSend _objEmail = new EmailSend();
                            //_ht["[CC]"] = ccTo;
                            _ht["[PSCNo]"] = ReportNo;
                            _ht["[Date]"] = DateTime.Now.ToString("dd-MM-yyyy");
                            _ht["[FromEmail]"] = Manager.GetUserNameFromPsNo(emailfrom);
                            _ht["[ProjectNo]"] = model.Project;
                            PSC010 objPSC010 = db.PSC010.Where(u => u.HeaderId == HeaderId).SingleOrDefault();
                            string link = WebsiteURL + "/PSC/MaintainPSC/Detail?headerId=" + objPSC010.HeaderId;
                            _ht["[Link]"] = link;
                            string contentID = ReportNo;
                            //att.ContentId = contentID;

                            _ht["[PSCReportPath]"] = "cid:" + contentID;
                            MAIL001 objTemplateMaster = new MAIL001();
                            if (model.LineDetails != null)
                            {
                                bool AllTwo = false;
                                var lineGroup = model.LineDetails.GroupBy(x => x.Type);
                                {
                                    foreach (var grp in lineGroup)
                                    {
                                        foreach (var line in grp)
                                        {
                                            if (line.Complied == "2")
                                            {
                                                AllTwo = true;
                                                break;
                                            }
                                            else
                                            {
                                                AllTwo = false;
                                            }
                                        }
                                        if (AllTwo)
                                        {
                                            break;
                                        }
                                    }

                                    if (AllTwo)
                                    {
                                        p.Status = "Pending";
                                        db.SaveChanges();
                                        objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.PSCNC.PSCReport).SingleOrDefault();
                                        _objEmail.MailToAdd = string.Join(",", emailTo.Distinct());
                                        _objEmail.MailCc = Manager.GetMailIdFromPsNo(emailfrom);
                                        _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                                    }
                                    else
                                    {
                                        objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.PSC.PSCReport).SingleOrDefault();
                                        _objEmail.MailToAdd = string.Join(",", emailTo.Distinct());
                                        _objEmail.MailCc = Manager.GetMailIdFromPsNo(emailfrom);
                                        _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                                    }
                                }
                            }

                        }
                        catch (Exception ex)
                        {
                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        }
                    }
                    #endregion
                }

                if (model.HeaderId > 0)
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update;
                    objResponseMsg.headerid = HeaderId.ToString();

                    return Json(objResponseMsg,
                                "application/json",
                                Encoding.UTF8,
                                JsonRequestBehavior.AllowGet);
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;
                    objResponseMsg.headerid = HeaderId.ToString();

                    return Json(objResponseMsg,
                                "application/json",
                                Encoding.UTF8,
                                JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception)
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error;

                return Json(objResponseMsg,
                            "application/json",
                            Encoding.UTF8,
                            JsonRequestBehavior.AllowGet);
            }
        }
        private int? GenerateReportNumber()
        {
            try
            {
                int? ReportNo = db.PSC010.Select(x => x.ReportNo).Count();
                int? Count = ReportNo == null ? 0 : ReportNo + 1;
                return Count;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpPost]
        public ActionResult LoadDataGridPartial(string status, string title, string indextype)
        {
            ViewBag.Status = status;
            ViewBag.GridTitle = title;
            ViewBag.IndexDataFor = indextype;
            return PartialView("_GetIndexGridDataPartial");
        }

        [HttpPost]
        public JsonResult LoadDataGridData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                strWhereCondition += "1=1 ";
                if (param.Status.ToLower() == "pending")
                {
                    strWhereCondition += " and Status  in ('pending') ";
                }
                //search Condition 

                string[] columnName = {
                           "SPCNo",
                           "Project",
                           "SeamNo",
                           "Dept",
                           "ShopInCharge",
                           "DescriptionOfActivity",
                           "Status"};
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                var lstResult = db.SP_PSC_INDEX_DATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = (from bal in lstResult
                            select new[]
                           {
                            Convert.ToString(bal.HeaderId),
                            Convert.ToString(bal.SPCNo),
                            Convert.ToString(bal.Project),
                            Convert.ToString(bal.SeamNo),
                            Convert.ToString(bal.Dept),
                            Convert.ToString(bal.ShopInCharge),
                            Convert.ToString(bal.DescriptionOfActivity),
                            Convert.ToString(bal.Status),
                            Convert.ToString(bal.CreatedBy),
                            bal.CreatedOn==null || bal.CreatedOn.Value==DateTime.MinValue?"":bal.CreatedOn.Value.ToString("dd/MM/yyyy" ,CultureInfo.InvariantCulture),
                            Helper.GenerateActionIcon(Convert.ToInt16(bal.HeaderId), "View", "View Detail", "fa fa-eye", "",WebsiteURL +"/PSC/MaintainPSC/Detail?headerId="+bal.HeaderId ,false)+    Helper.GenerateActionIcon(Convert.ToInt16(bal.HeaderId), "Print", "Print Detail", "fa fa-print","printReport("+bal.HeaderId+")","" ,false)

                          }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_PSC_INDEX_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      SPCNo = li.SPCNo,
                                      Project = li.Project,
                                      SeamNo = li.SeamNo,
                                      ShopInCharge = li.ShopInCharge,
                                      DescriptionOfActivity = li.DescriptionOfActivity,
                                      Status = li.Status,
                                      CreatedBy = li.CreatedBy,
                                      //QualityProject = li.QualityProject,
                                      //SeamListNo = li.SeamListNo,
                                      //SeamListRev = "R" + li.SeamListRev,
                                      //Status = li.Status,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                //else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                //{
                //    var lst = db.SP_IPI_GETSEAMLISTLINES(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                //    if (!lst.Any())
                //    {
                //        objResponseMsg.Key = false;
                //        objResponseMsg.Value = "No data available";
                //    }

                //    var newlst = (from li in lst
                //                  select new
                //                  {
                //                      WeldType = Convert.ToString(li.WeldType),
                //                      SeamCategory = Convert.ToString(li.SeamDescription),
                //                      Number = Convert.ToString(li.ManualSeamNo),
                //                      Revision = "R" + Convert.ToString(li.SeamRev),
                //                      Position = Convert.ToString(li.Position),
                //                      AssemblyNumber = Convert.ToString(li.AssemblyNo),
                //                      FinishedThickness = Convert.ToString(li.Thickness),
                //                      ThicknessOfParts = Convert.ToString(li.ThicknessDefault),
                //                      WEPDetail = Convert.ToString(li.WEPDetail),
                //                      ViewDrawing = Convert.ToString(li.ViewDrawing),
                //                      SeamLengthArea = Convert.ToString(li.SeamLengthArea),
                //                      Remarks = Convert.ToString(li.Remarks),
                //                      SeamQuantity = Convert.ToString(li.SeamQuantity),
                //                      Action = "Edit"
                //                  }).ToList();

                //    strFileName = GenerateExcelSeamList(newlst, objClsLoginInfo.UserName);
                //}

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
    }
}