﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.DevApp.Controllers
{
    public class MaintainFormsController : clsBase
    {
        // GET: DevApp/MaintainForms
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }
        [SessionExpireFilter]
        [AllowAnonymous]
        public ActionResult LayoutPreviewPartial(int? Id)
        {
            DAT001 objDAT001 = new DAT001();
            objDAT001 = db.DAT001.Where(m => m.FormId == Id).SingleOrDefault();

            if (objDAT001 != null)
            {
                objDAT001.HTMLContent = objDAT001.HTMLContent;
            }
            return PartialView("_LayoutPreviewPartial", objDAT001);

        }
        public ActionResult LayoutDesignPartial(int id=0)
        {            
            return PartialView("_LayoutDesignPartial");
        }

        [HttpGet]
        public FileStreamResult DownLoadFile(int? id)
        {
            //List<FileDetailsModel> ObjFiles = GetFileList();
            var objDAT001 = db.DAT001.Where(i => i.FormId == id).ToList();
            var FileById = (from FC in objDAT001
                            where FC.FormId.Equals(id)
                            select new { FC.FormName, FC.HTMLContent }).ToList().FirstOrDefault();
            var byteArray = Encoding.ASCII.GetBytes(FileById.HTMLContent);
            var stream = new MemoryStream(byteArray);
            return File(stream, "text/html", FileById.FormName+".html");
        }
    }
}