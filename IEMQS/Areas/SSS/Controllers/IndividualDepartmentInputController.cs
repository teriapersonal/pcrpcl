﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.PAM.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace IEMQS.Areas.SSS.Controllers
{
    public class IndividualDepartmentInputController : clsBase
    {
        // GET: SSS/IndividualDepartmentInput
        string check = string.Empty;



        public ActionResult Index()
        {

            return View();
        }
        public ActionResult IndividualInputPartialData(string status)
        {
            ViewBag.Status = status;
            //ViewBag.SSSId = SSSId;
            return PartialView("_IndividualInputPartialData");
        }
        public ActionResult IndividualInput(int roleId)
        {

            ViewBag.RoleId = roleId;
            return View();
        }
        public ActionResult LoadIndividualInputPartialData(JQueryDataTableParamModel param, string status)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                //SSS001 objSSS001 = new SSS001();
                //whereCondition += " and RecievedBy ='" + objClsLoginInfo.UserName + "'";


                var Roles = objClsLoginInfo.GetUserRoleList();
                if (status.ToUpper() == "PENDING")
                {
                    if (Roles.Contains(clsImplementationEnum.UserRoleName.PMG3.GetStringValue()) || Roles.Contains(clsImplementationEnum.UserRoleName.PMG2.GetStringValue()) ||
                        Roles.Contains(clsImplementationEnum.UserRoleName.PMG1.GetStringValue()) || Roles.Contains(clsImplementationEnum.UserRoleName.ENGG3.GetStringValue()) ||
                        Roles.Contains(clsImplementationEnum.UserRoleName.ENGG2.GetStringValue()) || Roles.Contains(clsImplementationEnum.UserRoleName.ENGG1.GetStringValue()))
                    {
                        whereCondition += " and s1.status in ('" + SSSStatus.Submitted.GetStringValue() + "')";
                    }
                    else
                    {
                        whereCondition += " and s1.status in ('" + SSSStatus.Submitted.GetStringValue() + "') and RecievedBy ='" + objClsLoginInfo.UserName + "'";
                    }
                }
                else
                {
                    if (Roles.Contains(clsImplementationEnum.UserRoleName.PMG3.GetStringValue()) || Roles.Contains(clsImplementationEnum.UserRoleName.PMG2.GetStringValue()) ||
                        Roles.Contains(clsImplementationEnum.UserRoleName.PMG1.GetStringValue()) || Roles.Contains(clsImplementationEnum.UserRoleName.ENGG3.GetStringValue()) ||
                        Roles.Contains(clsImplementationEnum.UserRoleName.ENGG2.GetStringValue()) || Roles.Contains(clsImplementationEnum.UserRoleName.ENGG1.GetStringValue()))
                    {
                        whereCondition += " and s1.status in ('" + SSSStatus.Submitted.GetStringValue() + "')";
                    }
                    else
                    {
                        //whereCondition += " and s1.status in ('') and RecievedBy ='" + objClsLoginInfo.UserName + "'";
                        whereCondition += " and RecievedBy ='" + objClsLoginInfo.UserName + "'";
                    }
                }

                string[] columnName = { "s1.SSSId", "s1.SSSNo", "s1.Contract", "s1.Customer", "s1.DSSNo", "s1.CSRMeetingOn", "s2.LineId", "c3.t_psno+'-'+c3.t_name", "s1.Status" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstPam = db.SP_SSS_GETINDIVIDUALDEPTHEADER(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                var res = from h in lstPam
                          select new[] {
                              h.SSSNo.ToString(),
                              h.Contract.ToString(),
                              h.Customer.ToString(),
                              Convert.ToString(Convert.ToDateTime(h.CSRMeetingOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(h.CSRMeetingOn).ToString("dd/MM/yyyy")),
                              h.DeptId.ToString(),
                              h.CreatedBy.ToString(),
                              h.Status.ToString(),
                              "", //Action
                              h.SSSId.ToString(),
                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetSSSDepartment(int SSSId)
        {
            ViewBag.SSSId = SSSId.ToString();
            ViewBag.Status = db.SSS001.Where(i => i.SSSId == SSSId).FirstOrDefault().Status.ToLower();
            ViewBag.lstDepartment = db.COM002.Where(x => x.t_dtyp == 3).Select(x => new BULocWiseCategoryModel { CatDesc = (x.t_dimx + "-" + x.t_desc), CatID = x.t_dimx }).ToList();

            return PartialView("~/Areas/SSS/Views/Shared/_SSSDepartmentDetails.cshtml");
        }

        public ActionResult AddSSS(int SSSId = 0, int DeptLineId = 0)
        {
            var objSSS001 = db.SSS001.Where(x => x.SSSId == SSSId).ToList().FirstOrDefault();
            if (objSSS001 == null)
            {
                objSSS001 = new SSS001();
                objSSS001.SSSId = 0;
                objSSS001.DSSId = 0;
                objSSS001.Status = SSSStatus.Draft.GetStringValue();
            }
            else
            {
                var lstDepartment = db.COM002.Where(x => x.t_dtyp == 3).Select(x => new BULocWiseCategoryModel { CatDesc = (x.t_dimx + "-" + x.t_desc), CatID = x.t_dimx }).ToList();
                ViewBag.lstDepartment = lstDepartment;
            }
            ViewBag.DeptLineId = DeptLineId;
            return View("IndividualInput", objSSS001);
        }

        public ActionResult GetIndividualDept(int SSSId, bool DisplayDSS, int DeptLineId)
        {
            ViewBag.SSSId = SSSId.ToString();
            ViewBag.DeptLineId = DeptLineId.ToString();

            return PartialView("~/Areas/SSS/Views/Shared/_GetIndividualDepartmentInputGridPartial.cshtml");
        }

        public ActionResult GetSSSHeaderDetails(int SSSId, int DeptLineId)
        {
            var objSSS001 = db.SSS001.Where(x => x.SSSId == SSSId).FirstOrDefault();
            if (objSSS001 == null)
            {
                objSSS001 = new SSS001();
                objSSS001.SSSId = 0;
                objSSS001.DSSId = 0;

            }
            else
            {
                ViewBag.Contract = db.Database.SqlQuery<string>(@"SELECT t_cono +'-'+ t_desc FROM    " + LNLinkedServer + ".dbo.ttpctm100175 where t_cono = '" + objSSS001.Contract + "'").FirstOrDefault();
                ViewBag.Customer = db.Database.SqlQuery<string>(@"SELECT  t_bpid +'-'+ t_nama   from " + LNLinkedServer + ".dbo.ttccom100175  where t_bpid = '" + objSSS001.Customer + "'").FirstOrDefault();

            }
            //ViewBag.DisplayDSS = DisplayDSS;
            return PartialView("IndividualInput", objSSS001);
        }


        [HttpPost]
        public ActionResult DeleteLine(int LineID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                SSS003 objSSS003 = db.SSS003.Where(x => x.LineId == LineID).FirstOrDefault();

                if (objSSS003 != null)
                {
                    db.SSS003.Remove(objSSS003);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = " successfully deleted.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Line not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult loadIndividualDeptInput(JQueryDataTableParamModel param, int SSSId)
        {
            try
            {
                SSS003 objSSS003 = db.SSS003.Where(i => i.SSSId == SSSId).FirstOrDefault();
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                //var lstdept = db.COM002.Where(x => x.t_dtyp == 3).ToList();
                //var lstEmp = db.COM003.ToList();

                string whereCondition = "1=1";

                var Roles = objClsLoginInfo.GetUserRoleList();
                if (Roles.Contains(clsImplementationEnum.UserRoleName.PMG3.GetStringValue()) || Roles.Contains(clsImplementationEnum.UserRoleName.PMG2.GetStringValue()) ||
                    Roles.Contains(clsImplementationEnum.UserRoleName.PMG1.GetStringValue()) || Roles.Contains(clsImplementationEnum.UserRoleName.ENGG3.GetStringValue()) ||
                    Roles.Contains(clsImplementationEnum.UserRoleName.ENGG2.GetStringValue()) || Roles.Contains(clsImplementationEnum.UserRoleName.ENGG1.GetStringValue()))
                {
                    whereCondition += " and SSSId= " + SSSId.ToString();
                }
                else
                {
                    whereCondition += " and SSSId= " + SSSId.ToString() + " and createdBy=" + objClsLoginInfo.UserName;
                }

                string[] columnName = { "SpecNo", "ApplicableClause", "DescriptionOfClause", "Interpretation", "Design", "Material", "Welding", "Fabrication", "DeviationFromCustomer", "Remarks" };
                List<SelectListItem> BoolenList = new List<SelectListItem>() { new SelectListItem { Text = "Select", Value = "" }, new SelectListItem { Text = "Yes", Value = "True" }, new SelectListItem { Text = "No", Value = "False" } };
                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                var lstPam = db.SP_SSS_DI_GETIndividaualDept(StartIndex, EndIndex, "", whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();


                int newRecordId = 0;
                var newRecord = new[] {
                                  "",
                                 "",
                                 "",
                                 //Helper.GenerateHidden(newRecordId, "SSSId", Convert.ToString(SSSId)),
                                 Helper.GenerateTextbox(newRecordId, "SpecNo","","",false,"","200"),
                                 Helper.GenerateTextbox(newRecordId, "ApplicableClause","","",false,"","200"),
                                 Helper.GenerateTextbox(newRecordId, "DescriptionOfClause","","",false,"","500"),
                                 Helper.GenerateTextbox(newRecordId, "Interpretation","","",false,"","500"),
                                 Helper.GenerateDropdown(newRecordId, "Design", new SelectList(BoolenList, "Value", "Text"), "No"),
                                 Helper.GenerateDropdown(newRecordId, "Material", new SelectList(BoolenList, "Value", "Text"), "No"),
                                 Helper.GenerateDropdown(newRecordId, "Welding", new SelectList(BoolenList, "Value", "Text"), "No"),
                                 Helper.GenerateDropdown(newRecordId, "Fabrication", new SelectList(BoolenList, "Value", "Text"), "No"),
                                 Helper.GenerateTextbox(newRecordId, "DeviationFromCustomer","","",false,"","500"),
                                 Helper.GenerateTextbox(newRecordId, "Remarks","","",false,"","500"),
                                 GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveLine(0);" ),
                                };

                var res = (from h in lstPam
                           select new[] {
                                Convert.ToString(h.LineId),
                                Helper.GenerateHidden(h.LineId, "SSSId", Convert.ToString(h.SSSId)),
                                //GenerateTextboxFor(h.LineId,"", "SSr.No",    Convert.ToString(h.ROW_NO),"","",true,"","50"),
                                Convert.ToString(h.ROW_NO),
                                GenerateTextboxFor(h.LineId,"", "SpecNo",    Convert.ToString(h.SpecNo),"","",true,"","200"),
                                GenerateTextboxFor(h.LineId,"", "ApplicableClause",    Convert.ToString(h.ApplicableClause),"","",true,"","200"),
                                GenerateTextboxFor(h.LineId,"", "DescriptionOfClause",    Convert.ToString(h.DescriptionOfClause),"","",true,"","500"),
                                GenerateTextboxFor(h.LineId,"", "Interpretation",    Convert.ToString(h.Interpretation),"","",true,"","500"),
                                GenerateDropdown  (h.LineId,"", "Design", new SelectList(BoolenList, "Value", "Text", Convert.ToString(h.Design)),"","","",true,""),
                                GenerateDropdown  (h.LineId,"", "Material", new SelectList(BoolenList, "Value", "Text", Convert.ToString(h.Material)),"","","",true,""),
                                GenerateDropdown  (h.LineId,"", "Welding", new SelectList(BoolenList, "Value", "Text", Convert.ToString(h.Welding)),"","","",true,""),
                                GenerateDropdown  (h.LineId,"", "Fabrication", new SelectList(BoolenList, "Value", "Text", Convert.ToString(h.Fabrication)),"","","",true,""),
                                GenerateTextboxFor(h.LineId,"", "DeviationFromCustomer",    Convert.ToString(h.DeviationFromCustomer),"","",true,"","500"),
                                GenerateTextboxFor(h.LineId,"", "Remarks",    Convert.ToString(h.Remarks),"","",true,"","500"),
                                h.CreatedBy==objClsLoginInfo.UserName? (HTMLActionString(h.LineId,"","Delete","Delete Record","fa fa-trash-o iconspace deleteline","DeleteRecord("+ h.LineId +");")
                                                                        + HTMLActionString(h.LineId,"","Edit","Edit Record","fa fa-pencil-square-o iconspace editline","")):"",
                                }).ToList();

                res.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = "",//strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }


        public static string GenerateTextboxFor(int rowId, string status, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = true, string inputStyle = "", string maxLength = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control";

            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onChange='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";
            if (string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
            {
                htmlControl = "<input disabled type='text' id='" + inputID + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " maxlength='" + maxLength + "'  />";
            }
            else
            {
                htmlControl = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + " maxlength='" + maxLength + "'  />";
            }

            return htmlControl;
        }
        public static string GenerateDropdown(int rowId, string status, string columnName, SelectList itemList, string defaultSelectionText = "", string onChangeMethod = "", string OnClickMethod = "", bool isReadOnly = true, string inputStyle = "")
        {
            string selectOptions = "";
            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string className = "form-control col-md-3";
            string onChangedEvent = !string.IsNullOrEmpty(onChangeMethod) ? "onchange='" + onChangeMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(OnClickMethod) ? "onclick='" + OnClickMethod + "'" : "";
            if (defaultSelectionText.Length > 0)
            {
                selectOptions += "<option value=''>" + defaultSelectionText + "</option>";
            }

            foreach (var item in itemList)
            {
                if (item.Selected)
                {
                    selectOptions += "<option selected value=" + item.Value + ">" + item.Text + "</option>";
                }
                else
                {
                    selectOptions += "<option value=" + item.Value + ">" + item.Text + "</option>";
                }
            }
            if (string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
            {
                return "<select id='" + inputID + "' disabled name='" + inputID + "' " + onChangedEvent + " colname='" + columnName + "' class='" + className + "' style='" + inputStyle + "' >" + selectOptions + "</select>";
            }
            else
            {
                return "<select id='" + inputID + "' name='" + inputID + "' " + onChangedEvent + " colname='" + columnName + "' class='" + className + "' style='" + inputStyle + "'  " + onClickEvent + " >" + selectOptions + "</select>";
            }
        }

        public ActionResult SaveIndividualDeptInput(FormCollection fc, int Id = 0, int SSSId = 0, int DeptLineId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {

                SSS003 objSSS003 = null;
                SSS002 objSSS002 = new SSS002();
                if (Id > 0)
                {
                    objSSS003 = db.SSS003.Where(x => x.LineId == Id).FirstOrDefault();
                    objSSS003.SpecNo = fc["SpecNo" + Id];
                    objSSS003.ApplicableClause = fc["ApplicableClause" + Id];
                    objSSS003.DescriptionOfClause = fc["DescriptionOfClause" + Id];
                    objSSS003.Interpretation = fc["Interpretation" + Id];
                    objSSS003.Design = !string.IsNullOrEmpty(fc["Design" + Id].ToString()) ? Convert.ToBoolean(fc["Design" + Id]) : false;
                    objSSS003.Material = !string.IsNullOrEmpty(fc["Material" + Id].ToString()) ? Convert.ToBoolean(fc["Material" + Id]) : false;
                    objSSS003.Fabrication = !string.IsNullOrEmpty(fc["Fabrication" + Id].ToString()) ? Convert.ToBoolean(fc["Fabrication" + Id]) : false;
                    objSSS003.Welding = !string.IsNullOrEmpty(fc["Welding" + Id].ToString()) ? Convert.ToBoolean(fc["Welding" + Id]) : false;
                    objSSS003.DeviationFromCustomer = fc["DeviationFromCustomer" + Id];
                    objSSS003.Remarks = fc["Remarks" + Id];
                    objSSS003.EditedBy = objClsLoginInfo.UserName;
                    objSSS003.EditedOn = DateTime.Now;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                    db.SaveChanges();
                }
                else
                {
                    objSSS003 = new SSS003();
                    objSSS003.SSSId = SSSId;
                    objSSS003.DeptLineId = DeptLineId;
                    objSSS003.SpecNo = fc["SpecNo" + Id];
                    objSSS003.ApplicableClause = fc["ApplicableClause" + Id];
                    objSSS003.DescriptionOfClause = fc["DescriptionOfClause" + Id];
                    objSSS003.Interpretation = fc["Interpretation" + Id];
                    objSSS003.Design = !string.IsNullOrEmpty(fc["Design" + Id].ToString()) ? Convert.ToBoolean(fc["Design" + Id]) : false;
                    objSSS003.Material = !string.IsNullOrEmpty(fc["Material" + Id].ToString()) ? Convert.ToBoolean(fc["Material" + Id]) : false;
                    objSSS003.Fabrication = !string.IsNullOrEmpty(fc["Fabrication" + Id].ToString()) ? Convert.ToBoolean(fc["Fabrication" + Id]) : false;
                    objSSS003.Welding = !string.IsNullOrEmpty(fc["Welding" + Id].ToString()) ? Convert.ToBoolean(fc["Welding" + Id]) : false;
                    objSSS003.DeviationFromCustomer = fc["DeviationFromCustomer" + Id];
                    //Convert.ToString(objSSS003.Remarks);

                    objSSS003.Remarks = fc["Remarks" + Id];
                    objSSS003.CreatedBy = objClsLoginInfo.UserName;
                    objSSS003.CreatedOn = DateTime.Now;
                    db.SSS003.Add(objSSS003);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public string GetChecklistCheckStyle(int LineId, string ColumnName, bool? isCheck)
        {
            //Session["ischeck"] = isCheck;
            check = Convert.ToString(isCheck);
            string rtnchecjbox = "";
            // string rtnremarks = "";
            if (isCheck == null)
                isCheck = false;
            rtnchecjbox = "<input type='checkbox' name='" + ColumnName + "" + LineId.ToString() + "' id='" + ColumnName + "" + LineId.ToString() + "' class='make-switch col-md-3' data-on-text='Yes' data-off-text='No' data-on-color='success' data-off-color='danger' data-size='normal' onchange='UpdateAccepted(this," + LineId.ToString() + ")' ";

            if (isCheck == true)
            {
                rtnchecjbox += " checked='checked' ";

            }


            rtnchecjbox += "/>";
            return rtnchecjbox;
        }

        public string GenerateAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string cssClass = "")
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control " + (cssClass != string.Empty ? cssClass : "");
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick=" + onClickMethod + "" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' data-lineid='" + rowId + "' hdElement='" + hdElement + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + " " + (disabled ? "disabled" : "") + " />";

            return strAutoComplete;
        }
        public static string GenerateGridButton(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";


            htmlControl = "<a id='" + inputID + "' name='" + inputID + "' class='btn btn-outline btn-circle btn-sm blue' " + onClickEvent + " ><i class='" + className + "'></i> " + buttonName + "</a>";

            //htmlControl = "<i  data-modal='' id='" + inputID + "' name='" + inputID + "' style='cursor:Pointer;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";

            return htmlControl;
        }
        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (!string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
            {
                if (string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue()))
                {
                    htmlControl = "";// "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }
                else
                {
                    htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }
            }

            return htmlControl;
        }
        [HttpPost]
        public ActionResult SaveIndividualInput(SSS003 objSSS003)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (Convert.ToInt32(objSSS003.SSSId) > 0)
                {
                    var objSSS003Update = db.SSS003.Where(i => i.SSSId == objSSS003.SSSId).FirstOrDefault();
                    objSSS003Update.EditedBy = objClsLoginInfo.UserName;
                    objSSS003Update.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.HeaderId = objSSS003Update.SSSId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Updated Successfully";
                }
                else
                {
                    SSS003 objSSS003New = new SSS003();
                    objSSS003New.SpecNo = objSSS003.SpecNo;
                    objSSS003New.ApplicableClause = objSSS003.ApplicableClause;
                    objSSS003New.DescriptionOfClause = objSSS003.DescriptionOfClause;
                    objSSS003New.Interpretation = objSSS003.Interpretation;
                    objSSS003New.Design = objSSS003.Design;
                    objSSS003New.Material = objSSS003.Material;
                    objSSS003New.Welding = objSSS003.Welding;
                    objSSS003New.Fabrication = objSSS003.Fabrication;
                    objSSS003New.DeviationFromCustomer = objSSS003.DeviationFromCustomer;
                    objSSS003New.Remarks = objSSS003.Remarks;
                    objSSS003New.CreatedBy = objClsLoginInfo.UserName;
                    objSSS003New.CreatedOn = DateTime.Now;
                    db.SSS003.Add(objSSS003New);
                    db.SaveChanges();

                    objResponseMsg.HeaderId = objSSS003New.SSSId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Individual Input Save Successfully";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet); ;
        }

    }

}