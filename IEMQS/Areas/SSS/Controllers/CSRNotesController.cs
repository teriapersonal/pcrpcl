﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.SSS.Controllers
{
    public class CSRNotesController : clsBase
    {
        // GET: SSS/CSRNotes
        public ActionResult Index(int roleId)
        {
            ViewBag.RoleId = roleId;
            return View();
        }

        public ActionResult IndexDE()
        {
            return RedirectToAction("Index", "CSRNotes", new { roleId = Convert.ToInt32(RoleForSSS.DE) });
        }
        public ActionResult IndexPMG()
        {
            return RedirectToAction("Index", "CSRNotes", new { roleId = Convert.ToInt32(RoleForSSS.PMG) });
        }
        public ActionResult IndexMOM()
        {
            return RedirectToAction("Index", "CSRNotes", new { roleId = Convert.ToInt32(RoleForSSS.MOM) });
        }
        public ActionResult GetCSRNotesDataGridPartial(string status, string RoleId)
        {
            ViewBag.Status = status;
            ViewBag.RoleId = RoleId;
            return PartialView("_GetCSRNotesDataGridPartial");
        }
        public ActionResult GetCSRNotestLineDataGridPartial(int SSSId, int CSRId = 0)
        {
            ViewBag.lstDepartment = db.COM002.Where(x => x.t_dtyp == 3).Select(x => new BULocWiseCategoryModel { CatDesc = (x.t_dimx + "-" + x.t_desc), CatID = x.t_dimx }).ToList();
            if (CSRId == 0)
            {
                var lstPam = db.SP_SSS_GETCSRNotesLines(SSSId, CSRId, 0, 0, "", "SSSId=" + SSSId.ToString()).ToList();
                if (lstPam != null && lstPam.Count > 0)
                {
                    var CSRIdNew = db.SSS022.Where(i => i.SSSId == SSSId).FirstOrDefault().CSRId;
                    List<SSS023> listSSS023 = new List<SSS023>();
                    lstPam.ForEach(i =>
                    {
                        listSSS023.Add(new SSS023
                        {
                            CSRId = CSRIdNew,
                            SpecNo = i.SpecNo,
                            ApplicableClause = i.ApplicableClause,
                            DescriptionOfClause = i.DescriptionOfClause,
                            ResDept = i.ResDept,
                            ActionOn = DateTime.Now,
                            CreatedBy = objClsLoginInfo.UserName,
                            CreatedOn = DateTime.Now,
                            EditedBy = objClsLoginInfo.UserName,
                            EditedOn = DateTime.Now,
                        });
                    });
                    db.SSS023.AddRange(listSSS023);
                    db.SaveChanges();
                }
            }
            ViewBag.SSSId = SSSId;
            ViewBag.CSRId = CSRId;
            return PartialView("_GetCSRNotestLineDataGridPartial");
        }
        public ActionResult loadCSRNotestDataTable(JQueryDataTableParamModel param, string status, int RoleId)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                //whereCondition += " and loidate.t_csbu collate SQL_Latin1_General_CP850_CI_AI in (select * from dbo.fn_split(dbo.GETBUFROMUSER('" + objClsLoginInfo.UserName + "'),',')) ";

                if (status.ToUpper() == "PENDING")
                {
                    //if (RoleId == Convert.ToInt32(RoleForSSS.DE))
                    //{
                    //    whereCondition += " and s1.status in ('" + SSSStatus.Submitted.GetStringValue() + "')";
                    //}
                    //else if (RoleId == Convert.ToInt32(RoleForSSS.PMG))
                    //{
                    //    whereCondition += " and s1.status in ('" + SSSStatus.Submitted.GetStringValue() + "') and ISNULL(s22.CSRId,'0')>0  and s22.status in ('" + SSSStatus.Submitted.GetStringValue() + "')";
                    //}
                    if (RoleId == Convert.ToInt32(RoleForSSS.DE))
                    {
                        whereCondition += " and s1.status in ('" + SSSStatus.Submitted.GetStringValue() + "') and s22.status in ('" + SSSStatus.Draft.GetStringValue() + "')";
                    }
                    else if (RoleId == Convert.ToInt32(RoleForSSS.PMG))
                    {
                        whereCondition += " and s1.status in ('" + SSSStatus.Submitted.GetStringValue() + "') and s22.status in ('" + SSSStatus.Submitted.GetStringValue() + "')";
                    }
                    else if (RoleId == Convert.ToInt32(RoleForSSS.MOM))
                    {
                        whereCondition += " and status in ('" + SSSStatus.Draft.GetStringValue() + "') and ParticipantList=" + objClsLoginInfo.UserName;
                    }
                }
                else
                {
                    if (RoleId == Convert.ToInt32(RoleForSSS.MOM))
                    {
                        whereCondition += " and ParticipantList=" + objClsLoginInfo.UserName;
                    }
                    else
                    {
                        //whereCondition += " and s1.status in ('')";
                        whereCondition += " and s1.status in ('" + SSSStatus.Submitted.GetStringValue() + "')";
                    }
                }
                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                if (RoleId == Convert.ToInt32(RoleForSSS.MOM))
                {
                    string[] columnName = { "SSSNo", "Contract", "Customer", "CSRMeetingOn", "Status" };

                    if (!string.IsNullOrWhiteSpace(param.sSearch))
                    {
                        whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                    }
                    else
                    {
                        whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                    }


                    //var lstPam = db.SP_SSS_GETPCCInput(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                    var lstPam = db.SP_SSS_GETFreezeCSRNotes(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                    int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                    var res = from h in lstPam
                              select new[] {
                              h.SSSNo.ToString(),
                              h.Contract.ToString(),
                              h.Customer.ToString(),
                              Convert.ToString(Convert.ToDateTime(h.CSRMeetingOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(h.CSRMeetingOn).ToString("dd/MM/yyyy")),
                              h.Status!=null ?h.Status.ToString():"",//h.Status!=null ?h.Status.ToString():"",
                              "", //Action
                              h.SSSId.ToString(),
                    };
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string[] columnName = { "s1.SSSNo", "s1.Contract", "s1.Customer", "s1.CSRMeetingOn", "s22.Status" };

                    if (!string.IsNullOrWhiteSpace(param.sSearch))
                    {
                        whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                    }
                    else
                    {
                        whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                    }

                    //var lstPam = db.SP_SSS_GETPCCInput(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                    var lstPam = db.SP_SSS_GETCSRNotes(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                    int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                    var res = from h in lstPam
                              select new[] {
                              h.SSSNo.ToString(),
                              h.Contract.ToString(),
                              h.Customer.ToString(),
                              Convert.ToString(Convert.ToDateTime(h.CSRMeetingOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(h.CSRMeetingOn).ToString("dd/MM/yyyy")),
                              h.Status!=null ?h.Status.ToString():"",//h.Status!=null ?h.Status.ToString():"",
                              "", //Action
                              h.SSSId.ToString(),
                    };
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [SessionExpireFilter]
        public ActionResult CSRNotesForm(int SSSId = 0, int RoleId = 0)
        {
            ViewBag.SSSId = SSSId;
            var objSSS022 = db.SSS022.Where(i => i.SSSId == SSSId).FirstOrDefault();
            if (objSSS022 == null)
            {
                objSSS022 = new SSS022();
                objSSS022.SSSId = SSSId;
                objSSS022.Status = PCCInputStatus.Draft.GetStringValue();
            }

            ViewBag.RoleId = RoleId;

            return View(objSSS022);
        }
        [HttpPost]
        public ActionResult SaveCSRNotes(SSS022 objSSS022, string participant)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (Convert.ToInt32(objSSS022.CSRId) > 0)
                {
                    var objSSS022Update = db.SSS022.Where(i => i.CSRId == objSSS022.CSRId).FirstOrDefault();
                    objSSS022Update.Notes = objSSS022.Notes;
                    objSSS022Update.ParticipantList = objSSS022.ParticipantList;
                    objSSS022Update.EditedBy = objClsLoginInfo.UserName;
                    objSSS022Update.EditedOn = DateTime.Now;

                    db.SaveChanges();

                    objResponseMsg.HeaderId = objSSS022Update.CSRId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "CSR Notes Updated Successfully";
                }
                else
                {
                    SSS001 objSSS001 = db.SSS001.Where(i => i.SSSId == objSSS022.SSSId).First();

                    SSS022 objSSS004New = new SSS022();
                    objSSS004New.SSSId = objSSS022.SSSId;
                    objSSS004New.DSSId = objSSS001.DSSId;
                    objSSS004New.DSSNo = objSSS001.DSSNo;
                    objSSS004New.Contract = objSSS001.Contract;
                    objSSS004New.Customer = objSSS001.Customer;
                    objSSS004New.Notes = objSSS022.Notes;
                    objSSS004New.ParticipantList = objSSS022.ParticipantList;
                    objSSS004New.Status = SSSStatus.Draft.GetStringValue();
                    objSSS004New.CreatedBy = objClsLoginInfo.UserName;
                    objSSS004New.CreatedOn = DateTime.Now;
                    objSSS004New.Status = SSSStatus.Draft.GetStringValue();
                    db.SSS022.Add(objSSS004New);
                    db.SaveChanges();

                    objResponseMsg.HeaderId = objSSS004New.CSRId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "CSR Notes Save Successfully";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet); ;
        }

        [HttpPost]
        public ActionResult SubmitCSRNotes(int CSRId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                SSS022 objSSS022 = db.SSS022.Where(x => x.CSRId == CSRId).FirstOrDefault();

                if (objSSS022 != null)
                {
                    if (objSSS022.SSS023.Count > 0)
                    {

                        objSSS022.SubmittedBy = objClsLoginInfo.UserName;
                        objSSS022.SubmittedOn = DateTime.Now;
                        objSSS022.Status = PCCInputStatus.Submitted.GetStringValue();
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "CSR Notes Submitted Successfully.";
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Please Add Atleast One Details";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetEmployees(string search)
        {
            try
            {
                if (search == null)
                {
                    search = "";
                }
                var items = db.COM003.Where(i => (i.t_psno.Contains(search) || i.t_name.Contains(search)) && i.t_actv == 1).Select(x => new { id = x.t_psno, text = x.t_psno + "-" + x.t_name, }).ToList();
                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        #region Lines
        public ActionResult loadCSRNotesLineDataTable(JQueryDataTableParamModel param, int SSSId, int CSRId)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                //var lstdept = db.COM002.Where(x => x.t_dtyp == 3).ToList();

                string whereCondition = "1=1";
                if (CSRId == 0)
                {
                    whereCondition += " and SSSId= " + SSSId.ToString();
                }
                else
                {
                    whereCondition += " and CSRId= " + CSRId.ToString();
                }
                string[] columnName = { "SpecNo", "ApplicableClause", "DescriptionOfClause", "ResDept" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                var lstPam = db.SP_SSS_GETCSRNotesLines(SSSId, CSRId, StartIndex, EndIndex, "", whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();


                int newRecordId = 0;
                var newRecord = new[] {
                                    "",
                                    Helper.GenerateHidden(newRecordId, "CSRId", Convert.ToString(CSRId)),
                                    GenerateAutoComplete(newRecordId, "SpecNo","","", "", false,"","SpecNo",false,""),
                                    GenerateAutoComplete(newRecordId, "ApplicableClause","","", "", false,"","ApplicableClause",false,""),
                                    GenerateAutoComplete(newRecordId, "DescriptionOfClause","","", "", false,"","DescriptionOfClause",false,""),
                                    GenerateAutoComplete(newRecordId, "ResDept","","", "", false,"","ResDept",false,"autocomplete")+""+Helper.GenerateHidden(newRecordId, "ddlResDept", Convert.ToString(newRecordId)),
                                    GenerateAutoComplete(newRecordId, "ActionOn","","", "", false,"","ActionOn",false,""),
                                    GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveLine(0);" )
                                };

                var res = (from h in lstPam
                           select new[] {
                                    Convert.ToString(h.LineId),
                                    Helper.GenerateHidden(h.LineId, "CSRId", Convert.ToString(h.CSRId)),
                                    GenerateAutoComplete(h.LineId, "SpecNo",h.SpecNo.ToString(),"", "", true,"","SpecNo",false),
                                    GenerateAutoComplete(h.LineId, "ApplicableClause",h.ApplicableClause.ToString(),"", "", true,"","ApplicableClause",false,""),
                                    GenerateAutoComplete(h.LineId, "DescriptionOfClause",h.DescriptionOfClause.ToString(),"", "", true,"","DescriptionOfClause",false,"",""),
                                    GenerateAutoComplete(h.LineId, "ResDept",GetDepartmentCodeAndName(h.ResDept.ToString()),"", "", true,"","ResDept",false,"autocomplete")+""+Helper.GenerateHidden(h.LineId, "ddlResDept", h.ResDept.ToString()),
                                    GenerateAutoComplete(h.LineId, "ActionOn",(h?.ActionOn!=null?(h?.ActionOn.Value.ToString("dd/MM/yyyy")):""),"", "", true,"","ActionOn",false),
                                    HTMLActionString(h.LineId,"","Delete","Delete Record","fa fa-trash-o iconspace deleteline","DeleteLine("+ h.CSRId+ "," + h.LineId +"); ")
                                    + HTMLActionString(h.LineId,"","Edit","Edit Record","fa fa-pencil-square-o iconspace editline","")
                                    }).ToList();

                res.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = "",//strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveCSRNotesLine(FormCollection fc, int Id = 0, int CSRId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                SSS023 objSSS023 = null;
                if (Id > 0)
                {
                    objSSS023 = db.SSS023.Where(x => x.LineId == Id).FirstOrDefault();
                    objSSS023.SpecNo = fc["SpecNo" + Id];
                    objSSS023.ApplicableClause = fc["ApplicableClause" + Id];
                    objSSS023.DescriptionOfClause = fc["DescriptionOfClause" + Id];
                    objSSS023.ResDept = fc["ddlResDept" + Id];
                    if (fc["ActionOn" + Id] == string.Empty)
                    {
                        objSSS023.ActionOn = null;
                    }
                    else
                    {
                        objSSS023.ActionOn = Convert.ToDateTime(fc["ActionOn" + Id]);
                    }
                    objSSS023.EditedBy = objClsLoginInfo.UserName;
                    objSSS023.EditedOn = DateTime.Now;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                    db.SaveChanges();
                }
                else
                {
                    objSSS023 = new SSS023();
                    objSSS023.CSRId = CSRId;
                    objSSS023.SpecNo = fc["SpecNo" + Id];
                    objSSS023.ApplicableClause = fc["ApplicableClause" + Id];
                    objSSS023.DescriptionOfClause = fc["DescriptionOfClause" + Id];
                    objSSS023.ResDept = fc["ddlResDept" + Id];
                    if (fc["ActionOn" + Id] == string.Empty)
                    {
                        objSSS023.ActionOn = null;
                    }
                    else
                    {
                        objSSS023.ActionOn = Convert.ToDateTime(fc["ActionOn" + Id]);
                    }
                    objSSS023.CreatedBy = objClsLoginInfo.UserName;
                    objSSS023.CreatedOn = DateTime.Now;
                    db.SSS023.Add(objSSS023);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteCSRNotesLine(int LineID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                SSS023 objSSS023 = db.SSS023.Where(x => x.LineId == LineID).FirstOrDefault();

                if (objSSS023 != null)
                {
                    db.SSS023.Remove(objSSS023);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Line successfully deleted.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Line not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public string GetDepartmentCodeAndName(string Code)
        {
            return db.COM002.Where(x => x.t_dtyp == 3 && x.t_dimx == Code).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
        }
        #endregion

        #region Export Excell
        //Code By : Anil Hadiyal on 28/12/2017, Export Functionality
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "", int SSSId = 0, int CSRId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_SSS_GETSubcontractingCell(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      SSSNo = li.SSSNo,
                                      Contract = li.Contract,
                                      Customer = li.Customer,
                                      CSRMeetingOn = li?.CSRMeetingOn,
                                      TargetDate = li?.TargetDate,
                                      Status = li.Status,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_SSS_GETCSRNotesLines(SSSId, CSRId, 1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      SpecNo = li.SpecNo,
                                      ApplicableClause = li.ApplicableClause,
                                      DescriptionOfClause = li.DescriptionOfClause,
                                      ResDept = GetDepartmentCodeAndName(li.ResDept.ToString()),
                                      ActionOn = (li?.ActionOn != null ? (li?.ActionOn.Value.ToString("dd/MM/yyyy")) : ""),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        public string GenerateAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string cssClass = "", string title = "")
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control " + (cssClass != string.Empty ? cssClass : "");
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick=" + onClickMethod + "" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' data-lineid='" + rowId + "' hdElement='" + hdElement + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + " " + (disabled ? "disabled" : "") + " title='" + title + "' />";

            return strAutoComplete;
        }
        public static string GenerateGridButton(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";


            htmlControl = "<a id='" + inputID + "' name='" + inputID + "' class='btn btn-outline btn-circle btn-sm blue' " + onClickEvent + " ><i class='" + className + "'></i> " + buttonName + "</a>";

            //htmlControl = "<i  data-modal='' id='" + inputID + "' name='" + inputID + "' style='cursor:Pointer;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";

            return htmlControl;
        }
        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (!string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
            {
                if (string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue()))
                {
                    htmlControl = "";// "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }
                else
                {
                    htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }
            }

            return htmlControl;
        }

    }
}