﻿using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.SSS.Controllers
{
    public class InputFormsController : clsBase
    {
        // GET: SSS/InputForms
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult WeldingInput(int SSSId = 0)
        {
            ViewBag.SSSId = SSSId;
            var objSSS004 = db.SSS004.Where(i => i.SSSId == SSSId).FirstOrDefault();
            if (objSSS004 == null)
            {
                objSSS004 = new SSS004();
                objSSS004.SSSId = SSSId;
            }
            return View("InputWEMain", objSSS004);
        }
    }
}