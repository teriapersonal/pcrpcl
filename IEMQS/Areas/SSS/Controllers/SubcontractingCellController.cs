﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.SSS.Controllers
{
    public class SubcontractingCellController : clsBase
    {
        // GET: SSS/SubcontractingCell
        public ActionResult Index(int roleId)
        {
            ViewBag.RoleId = roleId;
            return View();
        }

        public ActionResult IndexDE()
        {
            return RedirectToAction("Index", "SubcontractingCell", new { roleId = Convert.ToInt32(RoleForSSS.DE) });
        }
        public ActionResult IndexSUB()
        {
            return RedirectToAction("Index", "SubcontractingCell", new { roleId = Convert.ToInt32(RoleForSSS.SUB) });
        }
        public ActionResult GETSubcontractingCellPartial(string status, string RoleId)
        {
            ViewBag.Status = status;
            ViewBag.RoleId = RoleId;
            return PartialView("_GetSubconstractingCellDataGridPartial");
        }
        public ActionResult GetSubcontractingCellLinePartial(int ExpansionMockUpId = 0)
        {
            List<string> lstDishedEndType = getDishedEndType();
            ViewBag.DishedEndType = lstDishedEndType.Select(i => new BULocWiseCategoryModel { CatID = i, CatDesc = i }).ToList();
            return PartialView("_GetSubcontractingCellLinePartial");
        }
        public ActionResult loadSubcontractingCellDataTable(JQueryDataTableParamModel param, string status, int RoleId)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                //whereCondition += " and loidate.t_csbu collate SQL_Latin1_General_CP850_CI_AI in (select * from dbo.fn_split(dbo.GETBUFROMUSER('" + objClsLoginInfo.UserName + "'),',')) ";

                if (status.ToUpper() == "PENDING")
                {
                    if (RoleId == Convert.ToInt32(RoleForSSS.DE))
                    {
                        whereCondition += " and s1.status in ('" + PCCInputStatus.Submitted.GetStringValue() + "')";
                    }
                    else if (RoleId == Convert.ToInt32(RoleForSSS.SUB))
                    {
                        whereCondition += " and s1.status in ('" + PCCInputStatus.Submitted.GetStringValue() + "') and ISNULL(s18.HeaderId,'0')>0 and s18.ToPerson='" + objClsLoginInfo.UserName + "' and s18.status in ('" + PCCInputStatus.Submitted.GetStringValue() + "')";
                    }
                }
                else
                {
                    whereCondition += " and s1.status in ('')";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = { "s1.SSSNo", "s1.Contract", "s1.Customer", "s1.CSRMeetingOn", "TargetDate", "s18.Status" };

                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                //var lstPam = db.SP_SSS_GETPCCInput(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                var lstPam = db.SP_SSS_GETSubcontractingCell(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                var res = from h in lstPam
                          select new[] {
                              h.SSSNo.ToString(),
                              h.Contract.ToString(),
                              h.Customer.ToString(),
                              Convert.ToString(Convert.ToDateTime(h.CSRMeetingOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(h.CSRMeetingOn).ToString("dd/MM/yyyy")),
                              Convert.ToString(Convert.ToDateTime(h.TargetDate).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(h.TargetDate).ToString("dd/MM/yyyy")),
                              h.Status!=null ?h.Status.ToString():"",//h.Status!=null ?h.Status.ToString():"",
                              "", //Action
                              h.SSSId.ToString(),
                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [SessionExpireFilter]
        public ActionResult SubcontractingCellForm(int SSSId = 0, int RoleId = 0)
        {
            ViewBag.SSSId = SSSId;
            var objSSS018 = db.SSS018.Where(i => i.SSSId == SSSId).FirstOrDefault();
            if (objSSS018 == null)
            {
                objSSS018 = new SSS018();
                objSSS018.SSSId = SSSId;
                objSSS018.Status = PCCInputStatus.Draft.GetStringValue();
            }
            var listEmployee = Manager.GetApprover(clsImplementationEnum.UserRoleName.SUB3.GetStringValue(), objClsLoginInfo.Location, true, objClsLoginInfo.UserName, "");
            ViewBag.lstToEmployee = listEmployee.Select(i => new BULocWiseCategoryModel { CatID = i.id, CatDesc = i.text }).ToList();
            if (objSSS018.ToPerson != null && objSSS018.ToPerson != string.Empty)
            {
                ViewBag.ToUserName = objSSS018.ToPerson + "-" + (db.COM003.Where(i => i.t_actv == 1).FirstOrDefault(i => i.t_psno == objSSS018.ToPerson).t_name);
            }
            if (objSSS018.SubmittedBy != null && objSSS018.SubmittedBy != string.Empty)
            {
                ViewBag.FromUserName = objSSS018.SubmittedBy + "-" + (db.COM003.Where(i => i.t_actv == 1).FirstOrDefault(i => i.t_psno == objSSS018.SubmittedBy).t_name);

            }
            ViewBag.RoleId = RoleId;
            return View(objSSS018);
        }
        [HttpPost]
        public ActionResult SaveSubcontractingCell(SSS018 objSSS018)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (Convert.ToInt32(objSSS018.HeaderId) > 0)
                {
                    var objSSS018Update = db.SSS018.Where(i => i.HeaderId == objSSS018.HeaderId).FirstOrDefault();
                    objSSS018Update.ToPerson = objSSS018.ToPerson;
                    objSSS018Update.Notes = objSSS018.Notes;
                    objSSS018Update.EditedBy = objClsLoginInfo.UserName;
                    objSSS018Update.EditedOn = DateTime.Now;
                    if (objSSS018Update.Status.ToLower() == SSSStatus.Draft.GetStringValue().ToLower())
                    {
                        objSSS018Update.TargetDate = objSSS018.TargetDate.HasValue ? Manager.getDateTime(objSSS018.TargetDate.Value.ToShortDateString()) : objSSS018.TargetDate;
                    }
                    db.SaveChanges();

                    //if (objSSS018Update.Status.ToLower() == SSSStatus.Draft.GetStringValue().ToLower())
                    //{
                    //    var folderPath = "SSS018/" + objSSS018.HeaderId;
                    //    Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                    //}

                    objResponseMsg.HeaderId = objSSS018Update.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Subcontracting Cell Updated Successfully";
                }
                else
                {
                    SSS001 objSSS001 = db.SSS001.Where(i => i.SSSId == objSSS018.SSSId).First();

                    SSS018 objSSS004New = new SSS018();
                    objSSS004New.SSSId = objSSS018.SSSId;
                    objSSS004New.ContractNo = objSSS001.Contract;
                    objSSS004New.Customer = objSSS001.Customer;
                    objSSS004New.ToPerson = objSSS018.ToPerson;
                    objSSS004New.TargetDate = objSSS018.TargetDate.HasValue ? Manager.getDateTime(objSSS018.TargetDate.Value.ToShortDateString()) : objSSS018.TargetDate;
                    objSSS004New.Notes = objSSS018.Notes;
                    objSSS004New.Status = SSSStatus.Draft.GetStringValue();
                    objSSS004New.CreatedBy = objClsLoginInfo.UserName;
                    objSSS004New.CreatedOn = DateTime.Now;
                    objSSS004New.Status = SSSStatus.Draft.GetStringValue();
                    db.SSS018.Add(objSSS004New);
                    db.SaveChanges();

                    //var folderPath = "SSS018/" + objSSS004New.HeaderId;
                    //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);

                    objResponseMsg.HeaderId = objSSS004New.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Subcontracting Cell Save Successfully";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet); ;
        }

        [HttpPost]
        public ActionResult SubmitSubcontractingCell(int HeaderId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                SSS018 objSSS018 = db.SSS018.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                if (objSSS018 != null)
                {
                    if (objSSS018.SSS019.Count > 0)
                    {

                        objSSS018.SubmittedBy = objClsLoginInfo.UserName;
                        objSSS018.SubmittedOn = DateTime.Now;
                        objSSS018.Status = PCCInputStatus.Submitted.GetStringValue();
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Expansion Mock Up Submitted Successfully.";
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Please Add Atleast One Details";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        #region Lines
        public ActionResult loadSubcontractingCellLineDataTable(JQueryDataTableParamModel param, int HeaderId)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                //var lstdept = db.COM002.Where(x => x.t_dtyp == 3).ToList();
                //var lstEmp = db.COM003.ToList();

                string whereCondition = "1=1";
                whereCondition += " and HeaderId= " + HeaderId.ToString();

                string[] columnName = { "ShellId", "DishedEndType", "DishedEndRadius1", "DishedEndRadius2", "FaceLength", "MinThk", "NomThk", "Material", "BlankDia" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                var lstPam = db.SP_SSS_GETSubcontractingCellLines(StartIndex, EndIndex, "", whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();


                int newRecordId = 0;
                var newRecord = new[] {
                                    "",
                                    Helper.GenerateHidden(newRecordId, "HeaderId", Convert.ToString(HeaderId)),
                                    GenerateAutoComplete(newRecordId, "ShellId","","", "", false,"","ShellId",false,"numeric"),                                    
                                    GenerateAutoComplete(newRecordId, "DishedEndType","","", "", false,"","DishedEndType",false,"autocomplete"),
                                    GenerateAutoComplete(newRecordId, "DishedEndRadius1","","", "", false,"","DishedEndRadius1",false,"numeric"),
                                    GenerateAutoComplete(newRecordId, "DishedEndRadius2","","", "", false,"","DishedEndRadius2",false,"numeric"),
                                    GenerateAutoComplete(newRecordId, "FaceLength","","", "", false,"","FaceLength",false,""),
                                    GenerateAutoComplete(newRecordId, "MinThk","","", "", false,"","MinThk",false,"numeric"),
                                    GenerateAutoComplete(newRecordId, "NomThk","","", "", false,"","NomThk",false,"numeric"),
                                    GenerateAutoComplete(newRecordId, "Material","","", "", false,"","Material",false,""),
                                    GenerateAutoComplete(newRecordId, "BlankDia","","", "", false,"","BlankDia",false,"numeric"),
                                    GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveLine(0);" )
                                };

                var res = (from h in lstPam
                           select new[] {
                                    Convert.ToString(h.LineId),
                                    Helper.GenerateHidden(h.LineId, "HeaderId", Convert.ToString(h.HeaderId)),
                                    GenerateAutoComplete(h.LineId, "ShellId",h?.ShellId.ToString(),"", "", true,"","ShellId",false,"numeric"),
                                    GenerateAutoComplete(h.LineId, "DishedEndType",h.DishedEndType.ToString(),"", "", true,"","DishedEndType",false,"autocomplete"),
                                    GenerateAutoComplete(h.LineId, "DishedEndRadius1",h?.DishedEndRadius1.ToString(),"", "", true,"","DishedEndRadius1",false,"numeric"),
                                    GenerateAutoComplete(h.LineId, "DishedEndRadius2",h?.DishedEndRadius2.ToString(),"", "", true,"","DishedEndRadius2",true,"numeric"),
                                    GenerateAutoComplete(h.LineId, "FaceLength",h.FaceLength.ToString(),"", "", true,"","FaceLength",false,""),
                                    GenerateAutoComplete(h.LineId, "MinThk",h?.MinThk.ToString(),"", "", true,"","MinThk",false,"numeric"),
                                    GenerateAutoComplete(h.LineId, "NomThk",h?.NomThk.ToString(),"", "", true,"","NomThk",false,"numeric"),
                                    GenerateAutoComplete(h.LineId, "Material",h.Material,"", "", true,"","Material",false,""),
                                    GenerateAutoComplete(h.LineId, "BlankDia",h?.BlankDia.ToString(),"", "", true,"","BlankDia",false,"numeric"),                                    
                                    HTMLActionString(h.LineId,"","Delete","Delete Record","fa fa-trash-o iconspace deleteline","DeleteLine("+ h.HeaderId+ "," + h.LineId +"); ")
                                    + HTMLActionString(h.LineId,"","Edit","Edit Record","fa fa-pencil-square-o iconspace editline","")
                                    }).ToList();

                res.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = "",//strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveSubcontractingCellLine(FormCollection fc, int Id = 0, int HeaderId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {

                SSS019 objSSS019 = null;

                if (Id > 0)
                {
                    objSSS019 = db.SSS019.Where(x => x.LineId == Id).FirstOrDefault();
                    objSSS019.DishedEndType = fc["DishedEndType" + Id];
                    if (fc["ShellId" + Id] == string.Empty)
                    {
                        objSSS019.ShellId = null;
                    }
                    else
                    {
                        objSSS019.ShellId = Convert.ToInt32(fc["ShellId" + Id]);
                    }
                    if (fc["DishedEndRadius1" + Id] == string.Empty)
                    {
                        objSSS019.DishedEndRadius1 = null;
                    }
                    else
                    {
                        objSSS019.DishedEndRadius1 = Convert.ToInt32(fc["DishedEndRadius1" + Id]);
                    }
                    objSSS019.FaceLength = fc["FaceLength" + Id];
                    if (fc["DishedEndRadius2" + Id] == string.Empty)
                    {
                        objSSS019.DishedEndRadius2 = null;
                    }
                    else
                    {
                        objSSS019.DishedEndRadius2 = Convert.ToInt32(fc["DishedEndRadius2" + Id]);
                    }

                    if (fc["MinThk" + Id] == string.Empty)
                    {
                        objSSS019.MinThk = null;
                    }
                    else
                    {
                        objSSS019.MinThk = Convert.ToInt32(fc["MinThk" + Id]);
                    }

                    objSSS019.Material = fc["Material" + Id];
                    if (fc["NomThk" + Id] == string.Empty)
                    {
                        objSSS019.NomThk = null;
                    }
                    else
                    {
                        objSSS019.NomThk = Convert.ToInt32(fc["NomThk" + Id]);
                    }
                    if (fc["BlankDia" + Id] == string.Empty)
                    {
                        objSSS019.BlankDia = null;
                    }
                    else
                    {
                        objSSS019.BlankDia = Convert.ToInt32(fc["BlankDia" + Id]);
                    }

                    objSSS019.EditedBy = objClsLoginInfo.UserName;
                    objSSS019.EditedOn = DateTime.Now;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                    db.SaveChanges();
                }
                else
                {
                    objSSS019 = new SSS019();
                    objSSS019.HeaderId = HeaderId;
                    objSSS019.SSSId = db.SSS018.FirstOrDefault(i => i.HeaderId == HeaderId).SSSId;
                    objSSS019.DishedEndType = fc["DishedEndType" + Id];
                    if (fc["ShellId" + Id] == string.Empty)
                    {
                        objSSS019.ShellId = null;
                    }
                    else
                    {
                        objSSS019.ShellId = Convert.ToInt32(fc["ShellId" + Id]);
                    }
                    if (fc["DishedEndRadius1" + Id] == string.Empty)
                    {
                        objSSS019.DishedEndRadius1 = null;
                    }
                    else
                    {
                        objSSS019.DishedEndRadius1 = Convert.ToInt32(fc["DishedEndRadius1" + Id]);
                    }
                    objSSS019.FaceLength = fc["FaceLength" + Id];
                    if (fc["DishedEndRadius2" + Id] == string.Empty)
                    {
                        objSSS019.DishedEndRadius2 = null;
                    }
                    else
                    {
                        objSSS019.DishedEndRadius2 = Convert.ToInt32(fc["DishedEndRadius2" + Id]);
                    }
                    
                    if (fc["MinThk" + Id] == string.Empty)
                    {
                        objSSS019.MinThk = null;
                    }
                    else
                    {
                        objSSS019.MinThk = Convert.ToInt32(fc["MinThk" + Id]);
                    }

                    objSSS019.Material = fc["Material" + Id];
                    if (fc["NomThk" + Id] == string.Empty)
                    {
                        objSSS019.NomThk = null;
                    }
                    else
                    {
                        objSSS019.NomThk = Convert.ToInt32(fc["NomThk" + Id]);
                    }
                    if (fc["BlankDia" + Id] == string.Empty)
                    {
                        objSSS019.BlankDia = null;
                    }
                    else
                    {
                        objSSS019.BlankDia = Convert.ToInt32(fc["BlankDia" + Id]);
                    }
                    objSSS019.CreatedBy = objClsLoginInfo.UserName;
                    objSSS019.CreatedOn = DateTime.Now;
                    db.SSS019.Add(objSSS019);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteSubcontractingCellLine(int LineID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                SSS019 objSSS019 = db.SSS019.Where(x => x.LineId == LineID).FirstOrDefault();

                if (objSSS019 != null)
                {
                    db.SSS019.Remove(objSSS019);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Line successfully deleted.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Line not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Export Excell
        //Code By : Anil Hadiyal on 28/12/2017, Export Functionality
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_SSS_GETSubcontractingCell(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      SSSNo = li.SSSNo,
                                      Contract = li.Contract,
                                      Customer = li.Customer,
                                      CSRMeetingOn = li?.CSRMeetingOn,
                                      TargetDate = li?.TargetDate,
                                      Status = li.Status,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_SSS_GETSubcontractingCellLines(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      ShellId = li?.ShellId,
                                      DishedEndType = li.DishedEndType,
                                      DishedEndRadius1 = li?.DishedEndRadius1,
                                      DishedEndRadius2 = li?.DishedEndRadius2,
                                      FaceLength = li.FaceLength,
                                      MinThk = li?.MinThk,
                                      NomThk = li?.NomThk,
                                      Material = li.Material,
                                      BlankDia = li?.BlankDia,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        public string GenerateAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string cssClass = "", string title = "")
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control " + (cssClass != string.Empty ? cssClass : "");
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick=" + onClickMethod + "" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' data-lineid='" + rowId + "' hdElement='" + hdElement + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + " " + (disabled ? "disabled" : "") + " title='" + title + "' />";

            return strAutoComplete;
        }
        public static string GenerateGridButton(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";


            htmlControl = "<a id='" + inputID + "' name='" + inputID + "' class='btn btn-outline btn-circle btn-sm blue' " + onClickEvent + " ><i class='" + className + "'></i> " + buttonName + "</a>";

            //htmlControl = "<i  data-modal='' id='" + inputID + "' name='" + inputID + "' style='cursor:Pointer;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";

            return htmlControl;
        }
        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (!string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
            {
                if (string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue()))
                {
                    htmlControl = "";// "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }
                else
                {
                    htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }
            }

            return htmlControl;
        }
        public static List<string> getDishedEndType()
        {
            List<string> items = GetEnumDescription<DishedEndType>();
            return items;
        }
        public static List<string> GetEnumDescription<T>() where T : struct
        {
            Type t = typeof(T);
            return !t.IsEnum ? null : Enum.GetValues(t).Cast<Enum>().Select(x => x.GetStringValue()).ToList();
        }

    }
}