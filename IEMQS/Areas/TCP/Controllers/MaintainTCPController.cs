﻿using IEMQS.Areas.NDE.Models;
using IEMQS.DESCore.Data;
using IEMQS.Models;
using IEMQS.PLMBOMService;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsHelper;

namespace IEMQS.Areas.TCP.Controllers
{
    public class MaintainTCPController : clsBase
    {

        HedPLMBaaNProjectCreationWebServiceService PLMService = new HedPLMBaaNProjectCreationWebServiceService();
        // GET: TCP/MaintainTCP
        [SessionExpireFilter, UserPermissions]
        public ActionResult Index(string Project)
        {
            ViewBag.chkProject = Project;
            return View();
        }

        public ActionResult GetTCPDataGridPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetTCPDataGridPartial");
        }
        public ActionResult loadTCPHeaderDataTable(JQueryDataTableParamModel param, string status, string Project)
        {
            try
            {
                if (Project != null && Project != "")
                {
                    int? StartIndex = param.iDisplayStart + 1;
                    int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                    string whereCondition = "( 1=1 ";

                    //whereCondition += " and loidate.t_csbu collate SQL_Latin1_General_CP850_CI_AI in (select * from dbo.fn_split(dbo.GETBUFROMUSER('" + objClsLoginInfo.UserName + "'),',')) ";
                    whereCondition += " and Project='" + Project.ToString() + "'";
                    if (status.ToUpper() == "PENDING")
                    {
                        //whereCondition += " AND (CreatedUser.t_depc='" + objClsLoginInfo.Department + "' AND tcph.Status IN ('" + clsImplementationEnum.TCPHeaderStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.TCPHeaderStatus.RespondedByAddressee.GetStringValue() + "')) OR (AddresseeUser.t_depc='" + objClsLoginInfo.Department + "' AND tcph.Status IN ('" + clsImplementationEnum.TCPHeaderStatus.SubmittedToAddresse.GetStringValue() + "','" + clsImplementationEnum.TCPHeaderStatus.RespondedByAssignee.GetStringValue() + "')) OR (AssigneeUser.t_depc='" + objClsLoginInfo.Department + "' AND tcph.Status IN ('" + clsImplementationEnum.TCPHeaderStatus.SubmittedToAssignee.GetStringValue() + "')) )";
                        whereCondition += " AND (tcph.Status IN ('" + clsImplementationEnum.TCPHeaderStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.TCPHeaderStatus.RespondedByAddressee.GetStringValue() + "')) OR (tcph.Status IN ('" + clsImplementationEnum.TCPHeaderStatus.SubmittedToAddresse.GetStringValue() + "','" + clsImplementationEnum.TCPHeaderStatus.RespondedByAssignee.GetStringValue() + "')) OR (tcph.Status IN ('" + clsImplementationEnum.TCPHeaderStatus.SubmittedToAssignee.GetStringValue() + "')) )";
                    }
                    else
                    {

                        List<string> lstProjectNo = new List<string>();

                        bool IsPLMProject = Manager.IsPLMProject(Project);
                        if (IsPLMProject)
                        {
                            //string lstProjects = PLMService.getProjectListByProjectOwne(objClsLoginInfo.UserName);
                            lstProjectNo = db.TCP004.Where(i => i.PSNO == objClsLoginInfo.UserName && i.IsProjectOwner == true).Select(i => i.ProjectNo).ToList();
                        }
                        else
                        {
                            using (IEMQSDESEntities db = new IEMQSDESEntities())
                            {
                                try
                                {
                                    lstProjectNo = db.DES051.Where(x => x.Owner == objClsLoginInfo.UserName && x.Project != null).Select(x => x.Project).Distinct().ToList();
                                }
                                catch (Exception ex)
                                {
                                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                                }
                            }
                        }
                        string lstProjects = string.Join(",", lstProjectNo);
                        //whereCondition += " AND (CreatedUser.t_depc='" + objClsLoginInfo.Department + "' OR AddresseeUser.t_depc='" + objClsLoginInfo.Department + "' OR AssigneeUser.t_depc='" + objClsLoginInfo.Department + "' OR  Project IN (SELECT Value FROM dbo.fn_Split('" + lstProjects + "', ',')) OR HeaderId in (Select HeaderId FROM TCP002 WHERE CreatedBy = '" + objClsLoginInfo.UserName + "') ) )";
                        whereCondition += " )";//AND (Project IN (SELECT Value FROM dbo.fn_Split('" + lstProjects + "', ',')) OR HeaderId in (Select HeaderId FROM TCP002 WHERE CreatedBy = '" + objClsLoginInfo.UserName + "') ) )";
                    }

                    string[] columnName = { "HeaderId", "BU", "Project", "TCPNo", "DrawingNo", "PartNo", "SeamNo", "QueryType", "QueryDescription", "Addressee", "Status" };
                    if (!string.IsNullOrWhiteSpace(param.sSearch))
                    {
                        whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                    }
                    else
                    {
                        whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                    }

                    #region Sorting
                    var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                    string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                    string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                    string strSortOrder = string.Empty;
                    if (!string.IsNullOrWhiteSpace(sortColumnName))
                    {
                        strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                    }
                    #endregion

                    var lstPam1 = db.SP_TCP_GET_TCP_Header(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                    lstPam1 = lstPam1.Where(x => x.Project.Contains(Project)).ToList();

                    int? totalRecords = lstPam1.Select(i => i.TotalCount).FirstOrDefault();

                    var res = from h in lstPam1
                              select new[] {

                               Convert.ToString(h.BU),
                               Convert.ToString(h.Project),
                               Convert.ToString(h.TCPNo),
                               Convert.ToString(h.DrawingNo),
                               //h.SeamNo.ToString(),
                               Convert.ToString(h.QueryType),
                               Convert.ToString(h.CreatedBy),
                               //h.QueryDescription.ToString(),
                               Convert.ToString(h.Addressee),
                               Convert.ToString(h.Status),
                               "", //Action,
                               Convert.ToString(h.HeaderId)
                    };
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                        strSortOrder = strSortOrder,
                        whereCondition = whereCondition
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    int? StartIndex = param.iDisplayStart + 1;
                    int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                    string whereCondition = "( 1=1 ";

                    //whereCondition += " and loidate.t_csbu collate SQL_Latin1_General_CP850_CI_AI in (select * from dbo.fn_split(dbo.GETBUFROMUSER('" + objClsLoginInfo.UserName + "'),',')) ";

                    if (status.ToUpper() == "PENDING")
                    {
                        //whereCondition += " AND (CreatedUser.t_depc='" + objClsLoginInfo.Department + "' AND tcph.Status IN ('" + clsImplementationEnum.TCPHeaderStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.TCPHeaderStatus.RespondedByAddressee.GetStringValue() + "')) OR (AddresseeUser.t_depc='" + objClsLoginInfo.Department + "' AND tcph.Status IN ('" + clsImplementationEnum.TCPHeaderStatus.SubmittedToAddresse.GetStringValue() + "','" + clsImplementationEnum.TCPHeaderStatus.RespondedByAssignee.GetStringValue() + "')) OR (AssigneeUser.t_depc='" + objClsLoginInfo.Department + "' AND tcph.Status IN ('" + clsImplementationEnum.TCPHeaderStatus.SubmittedToAssignee.GetStringValue() + "')) )";
                        whereCondition += " AND (tcph.Status IN ('" + clsImplementationEnum.TCPHeaderStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.TCPHeaderStatus.RespondedByAddressee.GetStringValue() + "')) OR (tcph.Status IN ('" + clsImplementationEnum.TCPHeaderStatus.SubmittedToAddresse.GetStringValue() + "','" + clsImplementationEnum.TCPHeaderStatus.RespondedByAssignee.GetStringValue() + "')) OR (tcph.Status IN ('" + clsImplementationEnum.TCPHeaderStatus.SubmittedToAssignee.GetStringValue() + "')) )";
                    }
                    else
                    {
                        List<string> lstProjectNo = new List<string>();
                        //string lstProjects = PLMService.getProjectListByProjectOwner(objClsLoginInfo.UserName);
                        bool IsPLMProject = Manager.IsPLMProject(Project);
                        if (IsPLMProject)
                        {
                            lstProjectNo = db.TCP004.Where(i => i.PSNO == objClsLoginInfo.UserName && i.IsProjectOwner == true).Select(i => i.ProjectNo).ToList();
                        }
                        else
                        {

                            using (IEMQSDESEntities db = new IEMQSDESEntities())
                            {
                                try
                                {
                                    lstProjectNo = db.DES051.Where(x => x.Owner == objClsLoginInfo.UserName && x.Project != null).Select(x => x.Project).Distinct().ToList();
                                }
                                catch (Exception ex)
                                {
                                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                                }
                            }
                        }
                        string lstProjects = string.Join(",", lstProjectNo);
                        //whereCondition += " AND (CreatedUser.t_depc='" + objClsLoginInfo.Department + "' OR AddresseeUser.t_depc='" + objClsLoginInfo.Department + "' OR AssigneeUser.t_depc='" + objClsLoginInfo.Department + "' OR  Project IN (SELECT Value FROM dbo.fn_Split('" + lstProjects + "', ',')) OR HeaderId in (Select HeaderId FROM TCP002 WHERE CreatedBy = '" + objClsLoginInfo.UserName + "') ) )";
                        whereCondition += " )";//AND (Project IN (SELECT Value FROM dbo.fn_Split('" + lstProjects + "', ',')) OR HeaderId in (Select HeaderId FROM TCP002 WHERE CreatedBy = '" + objClsLoginInfo.UserName + "') ) )";
                    }

                    string[] columnName = { "HeaderId", "BU", "Project", "TCPNo", "DrawingNo", "PartNo", "SeamNo", "QueryType", "QueryDescription", "Addressee", "Status" };
                    if (!string.IsNullOrWhiteSpace(param.sSearch))
                    {
                        whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                    }
                    else
                    {
                        whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                    }

                    #region Sorting
                    var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                    string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                    string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                    string strSortOrder = string.Empty;
                    if (!string.IsNullOrWhiteSpace(sortColumnName))
                    {
                        strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                    }
                    #endregion

                    var lstPam = db.SP_TCP_GET_TCP_Header(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                    int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                    var res = from h in lstPam
                              select new[] {

                               Convert.ToString(h.BU),
                               Convert.ToString(h.Project),
                               Convert.ToString(h.TCPNo),
                               Convert.ToString(h.DrawingNo),
                               //h.SeamNo.ToString(),
                               Convert.ToString(h.QueryType),
                               Convert.ToString(h.CreatedBy),
                               //h.QueryDescription.ToString(),
                               Convert.ToString(h.Addressee),
                               Convert.ToString(h.Status),
                               "", //Action,
                               Convert.ToString(h.HeaderId)
                    };
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                        strSortOrder = strSortOrder,
                        whereCondition = whereCondition
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]
        public ActionResult GenerateQuery(int? id, string Project)
        {
            var objTCH_Header = new TCP001();


            //List<AutoCompleteModel> lstQueryType = new List<AutoCompleteModel>();
            //var lstType = clsImplementationEnum.getQueryType().ToList();
            //foreach (var item in lstType)
            //{
            //    lstQueryType.Add(new AutoCompleteModel { Text = item, Value = item });
            //}
            //ViewBag.lstQueryType = lstQueryType;

            if (!string.IsNullOrEmpty(Project))
            {
                if (id == null)
                {
                    string BU = db.COM001.Where(i => i.t_cprj == Project).FirstOrDefault().t_entu;

                    var BUDescription = (from a in db.COM002
                                         where a.t_dimx == BU
                                         select new { a.t_desc }).FirstOrDefault().t_desc;
                    ViewBag.BUCode = BU;
                    ViewBag.BUDesc = BUDescription;
                    ViewBag.Project = Manager.GetProjectAndDescription(Project);
                }

            }
            if (id != null)
            {
                objTCH_Header = db.TCP001.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.BU = objTCH_Header.BU;
                ViewBag.Project = Manager.GetProjectAndDescription(objTCH_Header.Project);
                ViewBag.DrawingNo = objTCH_Header.DrawingNo;
                ViewBag.PartNo = objTCH_Header.PartNo;
                ViewBag.SeamNo = objTCH_Header.SeamNo; //db.SP_TCP_GET_SearchSeamNo(CompanyId, objTCH_Header.Project, objTCH_Header.SeamNo, true).FirstOrDefault().text;
                ViewBag.QueryType = objTCH_Header.QueryType;
                ViewBag.Addressee = Manager.GetPsidandDescription(objTCH_Header.Addressee);
                ViewBag.AddresseeDepartment = Manager.GetDepartmentandDescription(objTCH_Header.AddresseeDepartment);
                if (objTCH_Header.Assignee != null)
                {
                    ViewBag.Assignee = Manager.GetPsidandDescription(objTCH_Header.Assignee);
                }
            }
            else
            {
                objTCH_Header.Status = clsImplementationEnum.TCPHeaderStatus.Draft.GetStringValue();
                objTCH_Header.CreatedBy = objClsLoginInfo.UserName;
            }
            ViewBag.chkProject = Project;
            return View(objTCH_Header);
        }
        [SessionExpireFilter]
        public ActionResult SaveTCPHeader(TCP001 tcpHeader, string Project)
        {
            ResponseMsg objResponseMsg = new ResponseMsg();
            try
            {
                objResponseMsg = SaveTCPHeaderData(tcpHeader);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            ViewBag.chkProject = Project;
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [SessionExpireFilter]
        public ActionResult SaveTCPRespond(TCP001 tcpHeader)
        {
            ResponceMsgTCP objResponseMsg = new ResponceMsgTCP();
            try
            {
                if (tcpHeader.HeaderId > 0)
                {
                    var objTCP001 = db.TCP001.Where(x => x.HeaderId == tcpHeader.HeaderId).FirstOrDefault();

                    if (objTCP001.Assignee == objClsLoginInfo.UserName)
                    {
                        if (objTCP001.TCP002.Any(c => c.CreatedBy == objClsLoginInfo.UserName && c.CreatedOn > objTCP001.SubmittedtoAssigneeOn))
                        {
                            objTCP001.Status = clsImplementationEnum.TCPHeaderStatus.RespondedByAssignee.GetStringValue();
                            objTCP001.RespondedfromAssigneeBy = objClsLoginInfo.UserName;
                            objTCP001.RespondedfromAssigneeOn = DateTime.Now;
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Response(comment) required for respond";
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else if (objTCP001.Addressee == objClsLoginInfo.UserName)
                    {
                        if (objTCP001.TCP002.Any(c => c.CreatedBy == objClsLoginInfo.UserName && c.CreatedOn > objTCP001.SubmittedtoAddresseeOn))
                        {
                            objTCP001.Status = clsImplementationEnum.TCPHeaderStatus.RespondedByAddressee.GetStringValue();
                            objTCP001.RespondedfromAddresseeBy = objClsLoginInfo.UserName;
                            objTCP001.RespondedfromAddresseeOn = DateTime.Now;
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Response(comment) required for respond";
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }
                    }

                    //if (tcpHeader.Status.ToLower() == clsImplementationEnum.TCPHeaderStatus.RespondedByAssignee.GetStringValue().ToLower())
                    //{
                    //    objTCP001.Status = clsImplementationEnum.TCPHeaderStatus.RespondedByAddressee.GetStringValue();
                    //    objTCP001.RespondedfromAddresseeBy = objClsLoginInfo.UserName;
                    //    objTCP001.RespondedfromAddresseeOn = DateTime.Now;
                    //}
                    //else if (tcpHeader.Status.ToLower() == clsImplementationEnum.TCPHeaderStatus.SubmittedToAssignee.GetStringValue().ToLower())
                    //{
                    //    objTCP001.Status = clsImplementationEnum.TCPHeaderStatus.RespondedByAssignee.GetStringValue();
                    //    objTCP001.RespondedfromAssigneeBy = objClsLoginInfo.UserName;
                    //    objTCP001.RespondedfromAssigneeOn = DateTime.Now;
                    //}
                    objTCP001.Assignee = tcpHeader.Assignee;
                    objTCP001.EditedBy = objClsLoginInfo.UserName;
                    objTCP001.EditedOn = DateTime.Now;

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Id = objTCP001.HeaderId;
                    objResponseMsg.Value = "Respond Submitted Successfully.";
                    objResponseMsg.Status = objTCP001.Status;
                    objResponseMsg.Assignee = objTCP001.Assignee;
                    objResponseMsg.Addressee = objTCP001.Addressee;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [SessionExpireFilter]
        public ResponceMsgTCP SaveTCPHeaderData(TCP001 tcpHeader)
        {
            ResponceMsgTCP objResponseMsg = new ResponceMsgTCP();
            try
            {
                string projectowner = string.Empty;
                bool IsPLMProject = Manager.IsPLMProject(tcpHeader.Project);
                if (IsPLMProject)
                {
                    var objTCP004 = db.TCP004.Where(i => i.ProjectNo == tcpHeader.Project && i.IsProjectOwner == true).FirstOrDefault();
                    projectowner = objTCP004 != null ? objTCP004.PSNO : ""; //PLMService.getProjectOwner(tcpHeader.Project);
                }
                else
                {
                    using (IEMQSDESEntities db = new IEMQSDESEntities())
                    {
                        try
                        {
                            var objDES051 = db.DES051.Where(x => x.Project == tcpHeader.Project && x.Project != null).FirstOrDefault();
                            projectowner = objDES051 != null ? objDES051.Owner : "";
                        }
                        catch (Exception ex)
                        {
                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        }
                    }
                }

                if (tcpHeader.HeaderId > 0)
                {
                    var objTCP001 = db.TCP001.Where(x => x.HeaderId == tcpHeader.HeaderId).FirstOrDefault();
                    var ExistingProjectNo = objTCP001.Project;
                    objTCP001.BU = tcpHeader.BU;
                    objTCP001.Project = tcpHeader.Project;
                    objTCP001.TCPNo = tcpHeader.Project != ExistingProjectNo ? GetMaxTCPNo(tcpHeader.Project) : tcpHeader.TCPNo;
                    objTCP001.DrawingNo = tcpHeader.DrawingNo;
                    objTCP001.PartNo = tcpHeader.PartNo;
                    objTCP001.SeamNo = tcpHeader.SeamNo;
                    objTCP001.QueryType = tcpHeader.QueryType;
                    objTCP001.QueryDescription = tcpHeader.QueryDescription;
                    objTCP001.Addressee = tcpHeader.Addressee;
                    objTCP001.AddresseeDepartment = tcpHeader.AddresseeDepartment;
                    objTCP001.Subject = tcpHeader.Subject;
                    objTCP001.Status = tcpHeader.Status;
                    objTCP001.ProjectOwner = projectowner;
                    objTCP001.EditedBy = objClsLoginInfo.UserName;
                    objTCP001.EditedOn = DateTime.Now;

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Id = objTCP001.HeaderId;
                    objResponseMsg.Value = "Data Updated Successfully.";
                    objResponseMsg.Status = objTCP001.Status;
                }
                else
                {
                    TCP001 objTCP001 = new TCP001();
                    objTCP001.BU = tcpHeader.BU;
                    objTCP001.Project = tcpHeader.Project;
                    objTCP001.ProjectOwner = projectowner;
                    objTCP001.TCPNo = GetMaxTCPNo(tcpHeader.Project);//tcpHeader.TCPNo;
                    objTCP001.DrawingNo = tcpHeader.DrawingNo;
                    objTCP001.PartNo = tcpHeader.PartNo;
                    objTCP001.SeamNo = tcpHeader.SeamNo;
                    objTCP001.QueryType = tcpHeader.QueryType;
                    objTCP001.QueryDescription = tcpHeader.QueryDescription;
                    objTCP001.Addressee = tcpHeader.Addressee;
                    objTCP001.AddresseeDepartment = tcpHeader.AddresseeDepartment;
                    objTCP001.Subject = tcpHeader.Subject;
                    objTCP001.Status = tcpHeader.Status;
                    objTCP001.CreatedBy = objClsLoginInfo.UserName;
                    objTCP001.CreatedOn = DateTime.Now;
                    objTCP001.Status = clsImplementationEnum.TCPHeaderStatus.Draft.GetStringValue();
                    db.TCP001.Add(objTCP001);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Id = objTCP001.HeaderId;
                    objResponseMsg.Value = "Data save successfully.";
                    objResponseMsg.Status = objTCP001.Status;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return objResponseMsg;
        }

        [SessionExpireFilter]
        public ActionResult ViewTCPMaster(int id = 0)
        {
            TCP003 objTCP003 = new TCP003();
            if (id > 0)
            {
                objTCP003 = db.TCP003.Where(x => x.Id == id).FirstOrDefault();
                objTCP003.Department = objTCP003.Department;
                objTCP003.QueryType = objTCP003.QueryType;
            }
            return View(objTCP003);
        }

        [HttpPost]
        public ActionResult LoadTCPDeptWiseQueryTypeData(JQueryDataTableParamModel param)
        {
            try
            {

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                int CategoryId = Convert.ToInt32(param.CategoryId);
                string strWhere = "1=1";
                if (!string.IsNullOrWhiteSpace(param.Department))
                {
                    strWhere += " and Department='" + param.Department + "'";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (Department like '%" + param.sSearch
                        + "%' or QueryType like '%" + param.sSearch
                        + "%')";
                }
                else
                {
                    foreach (string item in Request["sColumns"].Split(','))
                    {
                        string colNames = item.Trim();
                        string colFilter = Convert.ToString(Request["flt" + colNames]);
                        if (!string.IsNullOrWhiteSpace(colFilter))
                        {
                            if (colNames == "Department")
                                strWhere += " and a.Department like '%" + colFilter + "%' ";
                            else if (colNames == "QueryType")
                                strWhere += " and a.QueryType like '%" + colFilter + "%' ";
                        }
                    }
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                #region Sorting
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion
                var lstResult = db.SP_TCP_GET_DEPARTMENT_WISE_QUERYTYPE
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {

                           Convert.ToString(uc.DepartmentDesc),
                           Convert.ToString(uc.QueryType),
                           Convert.ToString(uc.IsActive == true ? "Yes" : "No"),
                           Convert.ToString(uc.CreatedBy),
                            uc.CreatedOn == null || uc.CreatedOn.Value==DateTime.MinValue? string.Empty :uc.CreatedOn.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture),
                           "<nobr><center>"
                           +  Helper.GenerateActionIcon(uc.Id, "Edit", "", "fa fa-edit", "UpdateRecord("+uc.Id+",'"+uc.Department+"','"+uc.DepartmentDesc+"','"+uc.QueryType+"','"+uc.IsActive+"')" ,"",false)
                           +  Helper.GenerateActionIcon(uc.Id, "Delete", "", "fa fa-trash-o", "DeleteRecord("+uc.Id+")" ,"",false)
                           + "</center></nobr>",
            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0,
                    iTotalDisplayRecords = lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }
        [SessionExpireFilter]
        public ActionResult SaveDeptWiseQueryType(TCP003 tcp003)
        {
            ResponceMsgTCP objResponseMsg = new ResponceMsgTCP();
            try
            {
                if (db.TCP003.Any(x => x.Department == tcp003.Department && x.QueryType == tcp003.QueryType && x.Id != tcp003.Id))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Status = "Info";
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate;
                }
                else
                {
                    if (tcp003.Id > 0)
                    {
                        var objTCP003 = db.TCP003.Where(x => x.Id == tcp003.Id).FirstOrDefault();
                        if (!db.TCP001.Any(o => o.AddresseeDepartment == objTCP003.Department && o.QueryType == objTCP003.QueryType))
                        {
                            objTCP003.Department = tcp003.Department;
                            objTCP003.QueryType = tcp003.QueryType;
                            objTCP003.IsActive = tcp003.IsActive;
                            objTCP003.EditedBy = objClsLoginInfo.UserName;
                            objTCP003.EditedOn = DateTime.Now;
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update;
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Status = "Info";
                            objResponseMsg.Value = "You cannot Update this record,This Record is already in use";
                        }
                    }
                    else
                    {
                        TCP003 objTCP003 = new TCP003();
                        objTCP003.Department = tcp003.Department;
                        objTCP003.QueryType = tcp003.QueryType;
                        objTCP003.IsActive = tcp003.IsActive;
                        objTCP003.CreatedBy = objClsLoginInfo.UserName;
                        objTCP003.CreatedOn = DateTime.Now;
                        db.TCP003.Add(objTCP003);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;

                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost, SessionExpireFilter]
        public ActionResult DeleteQueryType(int Id)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                TCP003 objTCP003 = db.TCP003.Where(x => x.Id == Id).FirstOrDefault();
                if (objTCP003 != null)
                {
                    if (!db.TCP001.Any(o => o.AddresseeDepartment == objTCP003.Department && o.QueryType == objTCP003.QueryType))
                    {
                        db.TCP003.Remove(objTCP003);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Delete.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Status = "Info";
                        objResponseMsg.Value = "You cannot delete this record,This Record is already in use";
                    }
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [SessionExpireFilter]
        public ActionResult SubmitTCP(TCP001 tcpHeader)
        {
            ResponceMsgTCP objResponseMsg = new ResponceMsgTCP();
            try
            {
                if (tcpHeader.Status.ToLower() == clsImplementationEnum.TCPHeaderStatus.Draft.GetStringValue().ToLower())
                {
                    objResponseMsg = SaveTCPHeaderData(tcpHeader);

                    var objTCP001 = db.TCP001.Where(x => x.HeaderId == objResponseMsg.Id).FirstOrDefault();
                    objTCP001.Status = clsImplementationEnum.TCPHeaderStatus.SubmittedToAddresse.GetStringValue();
                    objTCP001.SubmittedtoAddresseeBy = objClsLoginInfo.UserName;
                    objTCP001.SubmittedtoAddresseeOn = DateTime.Now;
                    objTCP001.EditedBy = objClsLoginInfo.UserName;
                    objTCP001.EditedOn = DateTime.Now;
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Id = objTCP001.HeaderId;
                    objResponseMsg.Value = "TCP Submitted Successfully.";
                    objResponseMsg.Status = objTCP001.Status;
                    objResponseMsg.Assignee = objTCP001.Assignee;
                    objResponseMsg.Addressee = objTCP001.Addressee;
                }
                else
                {
                    if (tcpHeader.HeaderId > 0)
                    {
                        var objTCP001 = db.TCP001.Where(x => x.HeaderId == tcpHeader.HeaderId).FirstOrDefault();
                        //if (tcpHeader.Status.ToLower() == clsImplementationEnum.TCPHeaderStatus.SubmittedToAddresse.GetStringValue().ToLower())
                        if (objTCP001.CreatedBy == objClsLoginInfo.UserName)
                        {
                            if (objTCP001.Status == clsImplementationEnum.TCPHeaderStatus.RespondedByAddressee.GetStringValue() && !objTCP001.TCP002.Any(c => c.CreatedBy == objClsLoginInfo.UserName && c.CreatedOn > objTCP001.RespondedfromAddresseeOn))
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = "Response(comment) required for Re-Submit To Addresse";
                                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                            }
                            objTCP001.Status = clsImplementationEnum.TCPHeaderStatus.SubmittedToAddresse.GetStringValue();
                            objTCP001.SubmittedtoAddresseeBy = objClsLoginInfo.UserName;
                            objTCP001.SubmittedtoAddresseeOn = DateTime.Now;
                            objTCP001.Addressee = tcpHeader.Addressee;
                        }
                        else if (objTCP001.Addressee == objClsLoginInfo.UserName || objTCP001.Assignee == objClsLoginInfo.UserName)
                        {
                            if ((objTCP001.Status == clsImplementationEnum.TCPHeaderStatus.SubmittedToAssignee.GetStringValue() || objTCP001.Status == clsImplementationEnum.TCPHeaderStatus.SubmittedToAddresse.GetStringValue()) && !objTCP001.TCP002.Any(c => c.CreatedBy == objClsLoginInfo.UserName && c.CreatedOn > objTCP001.SubmittedtoAddresseeOn))
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = "Response(comment) required for Submit to Assignee";
                                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                            }
                            else if ((objTCP001.Status == clsImplementationEnum.TCPHeaderStatus.RespondedByAssignee.GetStringValue() || objTCP001.Status == clsImplementationEnum.TCPHeaderStatus.RespondedByAddressee.GetStringValue()) && !objTCP001.TCP002.Any(c => c.CreatedBy == objClsLoginInfo.UserName && c.CreatedOn > objTCP001.RespondedfromAssigneeOn))
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = "Response(comment) required for Submit to Assignee";
                                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                            }
                            objTCP001.Status = clsImplementationEnum.TCPHeaderStatus.SubmittedToAssignee.GetStringValue();
                            objTCP001.SubmittedtoAssigneeBy = objClsLoginInfo.UserName;
                            objTCP001.SubmittedtoAssigneeOn = DateTime.Now;
                            objTCP001.Assignee = tcpHeader.Assignee;
                        }

                        objTCP001.EditedBy = objClsLoginInfo.UserName;
                        objTCP001.EditedOn = DateTime.Now;

                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Id = objTCP001.HeaderId;
                        objResponseMsg.Value = "TCP Submitted Successfully.";
                        objResponseMsg.Status = objTCP001.Status;
                        objResponseMsg.Assignee = objTCP001.Assignee;
                        objResponseMsg.Addressee = objTCP001.Addressee;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [SessionExpireFilter]
        public ActionResult AcceptTCPHeader(TCP001 tcpHeader)
        {
            ResponceMsgTCP objResponseMsg = new ResponceMsgTCP();
            try
            {
                var objTCP001 = db.TCP001.Where(x => x.HeaderId == tcpHeader.HeaderId).FirstOrDefault();
                if (tcpHeader.Status.ToLower() == clsImplementationEnum.TCPHeaderStatus.RespondedByAddressee.GetStringValue().ToLower())
                {
                    objTCP001.Status = clsImplementationEnum.TCPHeaderStatus.Closed.GetStringValue();
                    objTCP001.EditedBy = objClsLoginInfo.UserName;
                    objTCP001.EditedOn = DateTime.Now;
                    objTCP001.ClosedBy = objClsLoginInfo.UserName;
                    objTCP001.ClosedOn = DateTime.Now;
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Id = objTCP001.HeaderId;
                    objResponseMsg.Value = "TCP Accepted Successfully.";
                    objResponseMsg.Status = objTCP001.Status;
                    objResponseMsg.Assignee = objTCP001.Assignee;
                    objResponseMsg.Addressee = objTCP001.Addressee;
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Id = objTCP001.HeaderId;
                    objResponseMsg.Value = "TCP Already Accepted!";
                    objResponseMsg.Status = objTCP001.Status;
                    objResponseMsg.Assignee = objTCP001.Assignee;
                    objResponseMsg.Addressee = objTCP001.Addressee;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult loadTCPCommentDataTable(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                var objTCP001 = db.TCP001.FirstOrDefault(f => f.HeaderId + "" == headerId);
                var IsResponsable = ((objTCP001.Status == clsImplementationEnum.TCPHeaderStatus.SubmittedToAddresse.GetStringValue() || objTCP001.Status == clsImplementationEnum.TCPHeaderStatus.RespondedByAssignee.GetStringValue()) && objTCP001.Addressee == objClsLoginInfo.UserName)
                    || (objTCP001.Status == clsImplementationEnum.TCPHeaderStatus.SubmittedToAssignee.GetStringValue() && objTCP001.Assignee == objClsLoginInfo.UserName)
                    || (objTCP001.Status == clsImplementationEnum.TCPHeaderStatus.RespondedByAddressee.GetStringValue() && objTCP001.CreatedBy == objClsLoginInfo.UserName);
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                var whereCondition = " HeaderId=" + headerId;

                string[] columnName = { "Comments", "t_psno", "t_name" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstPam = db.SP_TCP_GET_TCPComments(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                int newRecordId = 0;
                var newRecord = new[] {
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Comments", "", "", false, "", false, "5000"),
                                   "",
                                   "",
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveTCPComments();")
                                };

                var res = (from h in lstPam
                           select new[] {
                               h.HeaderId.ToString(),
                               h.LineId.ToString(),
                               "<span id='lblComment"+h.LineId+"'>"+h.Comments + "</span>",
                               h.CreatedBy.ToString(),
                               h.CreatedOn.ToString(),
                               IsValidForEditComment(h.CreatedBy) && IsResponsable ? Helper.GenerateGridButton( h.LineId, "Edit", "Edit Rocord", "fa fa-edit", "EditTCPComment("+h.LineId+")") + Helper.GenerateGridButton( h.LineId, "Delete", "Delete Rocord", "fa fa-trash", "DeleteTCPComment("+h.LineId+")") : ""
                    }).ToList();

                res.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        private bool isLastCommentFound = false;
        public bool IsValidForEditComment(string ResponseBy)
        {
            if (!isLastCommentFound && ResponseBy == objClsLoginInfo.UserName + "-" + objClsLoginInfo.Name)
                return true;
            isLastCommentFound = true;
            return false;
        }
        [HttpPost, SessionExpireFilter]
        public ActionResult SaveTCPComments(FormCollection fc)
        {
            ResponceMsgTCP objResponseMsg = new ResponceMsgTCP();
            TCP002 objTCP002 = new TCP002();
            int LineId = String.IsNullOrWhiteSpace(fc["LineId"]) ? 0 : Convert.ToInt32(fc["LineId"]);
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    string strComments = !string.IsNullOrEmpty(fc["Comments" + LineId]) ? Convert.ToString(fc["Comments" + LineId]).Trim() : "";
                    if (LineId > 0)
                    {
                        objTCP002 = db.TCP002.FirstOrDefault(w => w.LineId == LineId);
                        objTCP002.Comments = strComments;
                        objTCP002.EditedBy = objClsLoginInfo.UserName;
                        objTCP002.EditedOn = DateTime.Now;
                        objResponseMsg.Value = "Comment updated successfully";
                    }
                    else
                    {
                        #region Add New Spot Line1
                        objTCP002.HeaderId = refHeaderId;
                        objTCP002.Comments = strComments;
                        objTCP002.CreatedBy = objClsLoginInfo.UserName;
                        objTCP002.CreatedOn = DateTime.Now;
                        db.TCP002.Add(objTCP002);
                        objResponseMsg.Value = "Comment added successfully";
                        #endregion
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Id = objTCP002.LineId;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, SessionExpireFilter]
        public ActionResult DeleteTCPComments(int Id)
        {
            ResponceMsgTCP objResponseMsg = new ResponceMsgTCP();
            try
            {
                var objTCP002 = db.TCP002.FirstOrDefault(w => w.LineId == Id);
                db.TCP002.Remove(objTCP002);
                objResponseMsg.Value = "Comment deleted successfully";
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #region AutoComplete Method
        //for TCP Master
        public JsonResult GetDepartments(string search)
        {
            try
            {
                var isSearchEmpty = String.IsNullOrWhiteSpace(search);
                List<Department> lstDepartment = new List<Department>();
                lstDepartment = (from com2 in db.COM002
                                 where ((com2.t_dimx + " - " + com2.t_desc).Contains(search) && com2.t_dtyp == 3)
                                 select
                                 new Department
                                 {
                                     dcode = com2.t_dimx,
                                     ddesc = com2.t_dimx + " - " + com2.t_desc
                                 }).Distinct().Take(isSearchEmpty ? 10 : int.MaxValue).ToList();

                return Json(lstDepartment, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetAllBU(string term)
        {
            List<AutoCompleteModel> lstBU = new List<AutoCompleteModel>();
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstBU = (from ath1 in db.ATH001
                         join com2 in db.COM002 on ath1.BU equals com2.t_dimx
                         where ath1.Employee.Trim() == objClsLoginInfo.UserName.Trim()
                                && ath1.BU != ""
                                && (ath1.BU.Contains(term) || com2.t_desc.ToLower().Contains(term.ToLower()))
                         select new AutoCompleteModel { Value = ath1.BU, Text = com2.t_desc }).Distinct().ToList();
            }
            else
            {
                lstBU = (from ath1 in db.ATH001
                         join com2 in db.COM002 on ath1.BU equals com2.t_dimx
                         where ath1.Employee.Trim() == objClsLoginInfo.UserName.Trim()
                                && ath1.BU != ""
                         select new AutoCompleteModel { Value = ath1.BU, Text = com2.t_desc }).Distinct().ToList();
            }
            return Json(lstBU, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetNewTCPNo(string Project)
        {
            var TCPNO = GetMaxTCPNo(Project);
            return Json(TCPNO, JsonRequestBehavior.AllowGet);
        }

        public string GetMaxTCPNo(string Project)
        {
            var TCPNO = string.Empty;
            if (!string.IsNullOrEmpty(Project))
            {
                var mtNumber = "1";
                var objTCP001 = db.TCP001.Where(i => i.Project == Project).ToList();
                if (objTCP001.Count > 0)
                {
                    int MaxNo = 0;
                    foreach (var item in objTCP001)
                    {
                        string SplitId = item.TCPNo.Split('-').Last();
                        if (MaxNo < Convert.ToInt32(SplitId))
                        {
                            MaxNo = Convert.ToInt32(SplitId);
                        }
                    }
                    mtNumber = (MaxNo + 1).ToString();
                }
                TCPNO = Project + "-TC-" + mtNumber.PadLeft(3, '0');
            }
            return TCPNO;
        }

        public ActionResult GetUserDepartment(string UserPSNumber)
        {
            var Dept = string.Empty;
            if (!string.IsNullOrEmpty(UserPSNumber))
            {
                Dept = db.SP_TCP_GET_USER_DEPARTMENT(UserPSNumber).FirstOrDefault();
            }
            return Json(Dept, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetPartNo(string term, string Project)
        {
            List<AutoCompleteModel> lstPartNo = new List<AutoCompleteModel>();
            if (!string.IsNullOrEmpty(Project))
            {
                if (string.IsNullOrEmpty(term))
                {
                    lstPartNo = Manager.GetHBOMData(Project).OrderBy(i => i.FindNo).Distinct().Take(10).Select(i => new AutoCompleteModel() { Value = i.FindNo.ToString(), Text = i.FindNo.ToString() }).ToList();

                    //lstPartNo = db.VW_IPI_GETHBOMLIST.Where(x => x.Project.Equals(Project)).OrderBy(i => i.FindNo).Distinct().Take(10).Select(i => new AutoCompleteModel() { Value = i.FindNo.ToString(), Text = i.FindNo.ToString() }).ToList();
                }
                else
                {
                    lstPartNo = Manager.GetHBOMData(Project).Where(x => x.FindNo.Contains(term)).OrderBy(i => i.FindNo).Distinct().Select(i => new AutoCompleteModel() { Value = i.FindNo.ToString(), Text = i.FindNo.ToString() }).ToList();

                    //lstPartNo = db.VW_IPI_GETHBOMLIST.Where(x => x.Project.Equals(Project) && x.FindNo.Contains(term)).OrderBy(i => i.FindNo).Distinct().Select(i => new AutoCompleteModel() { Value = i.FindNo.ToString(), Text = i.FindNo.ToString() }).ToList();
                }
            }
            return Json(lstPartNo, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSeamNo(string term, string Project)
        {
            List<AutoCompleteModel> lstSeamNo = new List<AutoCompleteModel>();
            if (!string.IsNullOrEmpty(Project))
            {
                if (string.IsNullOrEmpty(term))
                {
                    dynamic lst = db.SP_TCP_GET_SearchSeamNo_IEMQS(Project, term).ToList();
                    if (lst.Count == 0)
                    {
                        lst = db.SP_TCP_GET_SearchSeamNo(Project, term, false).Take(10).ToList();
                    }

                    foreach (var item in lst)
                    {
                        lstSeamNo.Add(new AutoCompleteModel { Text = item.text.ToString(), Value = item.value.ToString() });
                    }
                }
                else
                {
                    dynamic lst = db.SP_TCP_GET_SearchSeamNo_IEMQS(Project, term).ToList();
                    if (lst.Count == 0)
                    {
                        lst = db.SP_TCP_GET_SearchSeamNo(Project, term, false).ToList();
                    }

                    foreach (var item in lst)
                    {
                        lstSeamNo.Add(new AutoCompleteModel { Text = item.text.ToString(), Value = item.value.ToString() });
                    }
                }
            }
            return Json(lstSeamNo, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetEmployee(string Project)
        {
            List<BULocWiseCategoryModel> lstAdressee = new List<BULocWiseCategoryModel>();
            List<string> lst = new List<string>();
            bool IsPLMProject = Manager.IsPLMProject(Project);
            if (IsPLMProject)
            {
                lst = db.TCP004.Where(i => i.ProjectNo == Project && i.IsProjectOwner == false).Select(i => i.PSNO).ToList();
            }
            else
            {
                // var lst = db.TCP004.Where(i => i.ProjectNo == Project && i.IsProjectOwner == false).Select(i => i.PSNO).ToList(); //PLMService.getProjectMembers(Project).ToList();
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    try
                    {
                        lst = db.DES051.Where(x => x.Project == Project && x.Project != null).Select(x => x.Owner).Distinct().ToList();
                    }
                    catch (Exception ex)
                    {
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    }
                }
            }
            lst.ForEach(x =>
                lstAdressee.Add(new BULocWiseCategoryModel { CatDesc = Manager.GetPsidandDescription(x.ToString()), CatID = x.ToString() })
            );

            var objData = new
            {
                count = lstAdressee.Count,
                lstMembers = lstAdressee
            };

            return Json(objData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetQueryType(string BU)
        {
            List<AutoCompleteModel> lstQueryType = new List<AutoCompleteModel>();
            var CatId = db.GLB001.Where(x => x.Category.ToLower() == "Query Type".ToLower()).FirstOrDefault().Id;

            var lstQtype = db.GLB002.Where(x => x.Category == CatId && x.BU == BU && x.Location == objClsLoginInfo.Location).ToList();

            lstQueryType = lstQtype.Select(x => new AutoCompleteModel { Text = x.Description, Value = x.Description }).ToList();

            var objData = new
            {
                count = lstQueryType.Count,
                lstQueryType = lstQueryType
            };

            return Json(objData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDepartment(string Project)
        {
            List<BULocWiseCategoryModel> lstDepartment = new List<BULocWiseCategoryModel>();
            List<string> lst = new List<string>();
            bool IsPLMProject = Manager.IsPLMProject(Project);
            if (IsPLMProject)
            {
                lst = db.TCP004.Where(i => i.ProjectNo == Project && i.IsProjectOwner == false).Select(i => i.PSNO).ToList(); //PLMService.getProjectMembers(Project).ToList();
            }
            else
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    try
                    {
                        lst = db.DES051.Where(x => x.Project == Project).Select(x => x.Owner).Distinct().ToList();
                    }
                    catch (Exception ex)
                    {
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    }
                }
            }

            lstDepartment = (from com003 in db.COM003
                             join com2 in db.COM002 on com003.t_depc equals com2.t_dimx
                             where lst.Contains(com003.t_psno)
                             select new BULocWiseCategoryModel { CatDesc = com003.t_depc + " - " + com2.t_desc, CatID = com003.t_depc.ToString() }
                             ).Distinct().ToList();

            var objData = new
            {
                count = lstDepartment.Count,
                lstMembers = lstDepartment
            };

            return Json(objData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDeptWiseEmployee(string Project, string dept)
        {
            List<BULocWiseCategoryModel> lstAdressee = new List<BULocWiseCategoryModel>();
            List<string> lst = new List<string>();
            bool IsPLMProject = Manager.IsPLMProject(Project);
            if (IsPLMProject)
            {
                lst = db.TCP004.Where(i => i.ProjectNo == Project && i.IsProjectOwner == false).Select(i => i.PSNO).ToList(); //PLMService.getProjectMembers(Project).ToList();
            }
            else
            {


                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    try
                    {
                        lst = db.DES051.Where(x => x.Project == Project).Select(x => x.Owner).Distinct().ToList();
                    }
                    catch (Exception ex)
                    {
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    }
                }
            }
            // var lst = db.TCP004.Where(i => i.ProjectNo == Project && i.IsProjectOwner == false).Select(i => i.PSNO).ToList(); //PLMService.getProjectMembers(Project).ToList();

            lstAdressee = (from com003 in db.COM003
                           join com2 in db.COM002 on com003.t_depc equals com2.t_dimx
                           where lst.Contains(com003.t_psno) && com003.t_depc == dept
                           select new BULocWiseCategoryModel { CatDesc = com003.t_psno + "-" + com003.t_name, CatID = com003.t_psno.ToString() }
                            ).Distinct().ToList();

            var objData = new
            {
                count = lstAdressee.Count,
                lstMembers = lstAdressee
            };

            return Json(objData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDeptWiseQueryType(string dept)
        {
            List<AutoCompleteModel> lstQueryType = new List<AutoCompleteModel>();
            lstQueryType = db.TCP003.Where(x => x.Department == dept && x.IsActive == true).Select(i =>
                                            new AutoCompleteModel { Text = i.QueryType, Value = i.QueryType })
                                           .ToList();

            var objData = new
            {
                count = lstQueryType.Count,
                lstQueryType = lstQueryType
            };

            return Json(objData, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public JsonResult GetAllProjectWiseOwnerAndTeamMember(string ProjectNo = "")
        {
            ResponceMsgTCP objResponseMsg = new ResponceMsgTCP();
            #region old code 
            /* try
        {
            var listProject = db.COM001.Where(i => i.t_cprj == (ProjectNo == "" ? i.t_cprj : ProjectNo)).OrderBy(i => i.t_cprj).ToList();

            foreach (var project in listProject)
            {
                try
                {
                    var listTCP004 = new List<TCP004>();
                    var listRemove = db.TCP004.Where(i => i.ProjectNo == project.t_cprj);

                    if (listRemove != null)
                    {
                        string projectowner = PLMService.getProjectOwner(project.t_cprj);
                        if (!string.IsNullOrEmpty(projectowner))
                        {
                            listTCP004.Add(new TCP004()
                            {
                                CreatedOn = DateTime.Now,
                                IsProjectOwner = true,
                                ProjectNo = project.t_cprj,
                                PSNO = projectowner
                            });
                        }
                    }

                    var lstMembers = PLMService.getProjectMembers(project.t_cprj);
                    if (lstMembers != null)
                    {
                        foreach (var member in lstMembers)
                        {
                            if (!string.IsNullOrEmpty(member))
                            {
                                listTCP004.Add(new TCP004()
                                {
                                    CreatedOn = DateTime.Now,
                                    IsProjectOwner = false,
                                    ProjectNo = project.t_cprj,
                                    PSNO = member
                                });
                            }
                        }
                    }
                    db.TCP004.RemoveRange(listRemove);
                    db.TCP004.AddRange(listTCP004);

                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                }
            }

            objResponseMsg.Key = true;
            objResponseMsg.Value = "Project Owner And Team Member Addedd Successfully!";
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            objResponseMsg.Key = false;
            objResponseMsg.Value = ex.Message;
        }
        */
            #endregion
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
    }
}