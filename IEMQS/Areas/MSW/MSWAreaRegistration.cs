﻿using System.Web.Mvc;

namespace IEMQS.Areas.MSW
{
    public class MSWAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "MSW";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "MSW_default",
                "MSW/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}