﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IEMQSImplementation;

namespace IEMQS.Areas.MSW.Models
{
    public class SourceMSW
    {
        public MSW001 SourceHeader { get; set; }
        public List<MSW002> SourceCategories { get; set; }
    }
}