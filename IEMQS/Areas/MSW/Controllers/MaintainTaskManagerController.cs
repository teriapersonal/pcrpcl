﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.MSW.Controllers
{
    public class MaintainTaskManagerController : clsBase
    {
        // GET: MSW/MaintainTaskManager
        [AllowAnonymous]
        [SessionExpireFilter]
        [UserPermissions]
        public ActionResult Index()
        {
            var Location = (from a in db.COM002
                            where a.t_dtyp == 1 && a.t_dimx != ""
                            select new { a.t_dimx, Desc = a.t_desc }).ToList();

            ViewBag.Loca = new SelectList(Location, "t_dimx", "Desc");
            return View();
        }
        [HttpPost]
        public ActionResult getData(JQueryDataTableParamModel param)
        {
            try
            {

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                int CategoryId = Convert.ToInt32(param.CategoryId);
                //string strWhere = string.Empty;
                string strWhere = "1=1";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (TaskManager like '%" + param.sSearch
                        + "%' or Location like '%" + param.sSearch
                        + "%' or CreatedBy like '%" + param.sSearch
                        + "%' or CreatedOn like '%" + param.sSearch
                        + "%')";

                }
                else
                {
                    //strWhere = "1=1";
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_MSW_GET_TASKMANAGER
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {

                           Convert.ToString(uc.Location),
                           Convert.ToString(uc.TaskManager),
                           Convert.ToString(uc.CreatedBy),
                           uc.CreatedOn == null || uc.CreatedOn.Value == DateTime.MinValue ? "" : uc.CreatedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                           Convert.ToString(uc.Id),
                           Convert.ToString(uc.Id)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        public ActionResult GetDataHtml(int Id)
        {
            MSW003 objMSW003 = new MSW003();

            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            var TaskManager = (from a in db.MSW003
                               where a.Id == Id
                               select new { a.TaskManager }).FirstOrDefault();


            var Location = (from a in db.MSW003
                            where a.Id == Id
                            select new { a.Location }).FirstOrDefault();


            objResponseMsg.Value = Location + "|" + TaskManager + "|" + Id;
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveTaskManager(MSW003 msw003, FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var TaskManager = fc["txtTaskManager"].ToString().Trim();
                var Location = fc["ddlLoca"].ToString().Trim();
                var isvalid = db.MSW003.Any(x => x.TaskManager == TaskManager && x.Location == Location);
                if (isvalid == false)
                {
                    if (msw003.Id > 0)
                    {
                        MSW003 objMSW003 = db.MSW003.Where(x => x.Id == msw003.Id).FirstOrDefault();
                        objMSW003.Location = fc["ddlLoca"].ToString().Trim();
                        objMSW003.TaskManager = fc["txtTaskManager"].ToString().Trim();
                        objMSW003.EditedBy = objClsLoginInfo.UserName;
                        objMSW003.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                    }
                    else
                    {

                        MSW003 objMSW003 = new MSW003();
                        objMSW003.Location = fc["ddlLoca"].ToString().Trim();
                        objMSW003.TaskManager = fc["txtTaskManager"].ToString().Trim();
                        objMSW003.CreatedBy = objClsLoginInfo.UserName;
                        objMSW003.CreatedOn = DateTime.Now;
                        db.MSW003.Add(objMSW003);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.Message.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteTaskManager(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                MSW003 objMSW003 = db.MSW003.Where(x => x.Id == Id).FirstOrDefault();
                db.MSW003.Remove(objMSW003);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateTaskManager(MSW003 msw003, FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var id = Convert.ToInt32(fc["hfId"].ToString());
                if (Convert.ToInt32(fc["hfId"].ToString()) > 0)
                {
                    MSW003 objMSW003 = db.MSW003.Where(x => x.Id == id).FirstOrDefault();
                    objMSW003.Location = fc["ddlLoca"].ToString().Trim();
                    objMSW003.TaskManager = fc["txtTaskManager"].ToString().Trim();
                    objMSW003.EditedBy = objClsLoginInfo.UserName;
                    objMSW003.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_MSW_GET_TASKMANAGER(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {

                                      Location = Convert.ToString(uc.Location),
                                      TaskManager = Convert.ToString(uc.TaskManager),
                                      CreatedBy = Convert.ToString(uc.CreatedBy),
                                      CreatedOn = uc.CreatedOn == null || uc.CreatedOn.Value == DateTime.MinValue ? "" : uc.CreatedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

    }
}