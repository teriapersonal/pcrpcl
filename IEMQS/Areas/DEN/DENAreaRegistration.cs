﻿using System.Web.Mvc;

namespace IEMQS.Areas.DEN
{
    public class DENAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "DEN";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "DEN_default",
                "DEN/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}