﻿using IEMQS.Models;
using IEMQSImplementation;
using IEMQSImplementation.PCR;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQS.Areas.PCR.Controllers.MaintainStockController;
using IEMQS.ILN.Framework;
using System.Threading.Tasks;
using Newtonsoft.Json;
using IEMQS.ILN.Models;
using Newtonsoft.Json.Converters;
using System.Reflection;

namespace IEMQS.Areas.PCR.Controllers
{
    public class ReleasePCLController : CommonController
    {
        private const string PLTType = "PLT";

        public ActionResult Index()
        {
            bool IsValidUser = CheckUserIsValid(ReleasePCLAction);
            if(IsValidUser)
            {
                ViewBag.Role = "PLNG3";
                ViewBag.TabName = "Release PCL";
                ViewBag.PCLStatusEnum = Enum.GetValues(typeof(clsImplementationEnum.PCLStatus)).Cast<clsImplementationEnum.PCLStatus>().Select(x => new { name = x.GetStringValue() });
                return View("Index");
            }
            else
                return new RedirectResult("~/Authentication/Authenticate/AccessDenied");
        }

        public async Task<PartialViewResult> _ReleasePCL()
        {
            ViewBag.WorkCenterDeliver = await GetWorkCenterWithLocation(0);
            ViewBag.PlannerNames = await GetPlannerNames();
            return PartialView("_ReleasePCL");
        }

        public PartialViewResult _ReleasePCLSingleOccurrence()
        {
            return PartialView("_ReleasePCLSingleOccurrence");
        }

        public async Task<PartialViewResult> _MaintainDimensionForReturnBalance(int id)
        {
            ViewBag.All = await GetAllAsync(id);           
            return PartialView("_MaintainDimensionForReturnBalance");
        }
        public async Task<JsonResult> RefreshAll(int id)
        {
            dynamic r = await GetAllAsync(id);
            return Json(r);
        }

        private async Task<dynamic> GetAllAsync(int id)
        {
            idVar = id;
            dynamic all = new ExpandoObject();
            List<dynamic> columns = new List<dynamic>();
            List<dynamic> columnConfigs = new List<dynamic>();
            GetColumnConfigObject(ref columns, ref columnConfigs);
            all.OriginalData = await (new DataAdapter().GetOriginalReturnDistributionData(objClsLoginInfo.UserName, id));
            all.MultiOccurenceTableData = await LoadMaintainDistributionReturnBalanceData(id);            
            all.MultiOccurenceColumns = columns;
            all.ColumnsConfigs = columnConfigs;
            all.PCLNumberList = await GetPCLNumberList();            
            return all;

        }

        private void GetColumnConfigObject(ref List<dynamic> columns,ref  List<dynamic> columnConfigs)
        {   
            PropertyInfo[] propertyInfos = typeof(RETURN_BALANCE_DISTRIBUTION_Result).GetProperties(); //.Select(x => x.Name).ToList();
            
            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                ColumnAttribute attribute = (ColumnAttribute)propertyInfo.GetCustomAttributes(typeof(ColumnAttribute), false).FirstOrDefault();                
                if(attribute != null)
                {
                    dynamic columnConfig = new ExpandoObject();
                    columnConfig.NonSearchable = attribute.IsNonSearchable;
                    columnConfig.NonSortable = attribute.IsNonSortable;
                    columnConfig.Invisible = attribute.IsInVisible;
                    columnConfig.Type = attribute.Type;
                    columnConfig.ColName = propertyInfo.Name;
                    columnConfig.Index = attribute.Index;

                    columnConfigs.Add(columnConfig);
                    dynamic column = new ExpandoObject();
                    column.title = attribute.columnName;
                    column.data = propertyInfo.Name;
                    columns.Add(column);
                }
                
            }
        }
        
        private  async Task<List<RETURN_BALANCE_DISTRIBUTION_Result>> LoadMaintainDistributionReturnBalanceData(int id)
        {

            try
            {
                var lstData = await (new DataAdapter().GetReturnDistributionData(objClsLoginInfo.UserName, id));               
                return lstData;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }


        public async Task<ActionResult> LoadReleasePCLDataTable(JQueryDataTableParamModel param, string status)
        {

            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstData = await (new DataAdapter().GetReleasePCLData(objClsLoginInfo.UserName));

                var res = new List<dynamic>();
                foreach (var h in lstData)
                {

                    res.Add(new
                    {
                        Id = Convert.ToString(h.Id),
                        PCLNumber = Convert.ToString(h.PCLNumber),
                        Revision = Convert.ToString(h.Revision),
                        TotalConsumedArea = Convert.ToString(roundOffValue(h.TotalConsumedArea, 2)),
                        ScrapQuantity = Convert.ToString(h.ScrapQuantity),
                        ReturnArea = Convert.ToString(roundOffValue(h.ReturnArea, 2)),
                        PCLStatus = Convert.ToString(h.PCLStatus),
                        ReturnRemark = Convert.ToString(h.ReturnRemark),
                        TotalCuttingLength = GetTotalCuttingLengthColumnDetails(Convert.ToInt32(h.Id), h.PCLStatus, h.TotalCuttingLength),
                        StockNumber = Convert.ToString(h.StockNumber),
                        TotalArea = Convert.ToString(roundOffValue(h.TotalArea, 2)),
                        WCDeliver = GetWCDeliverColumnDetails(Convert.ToInt32(h.Id), h.PCLStatus, h.WCDeliverName, h.WCDeliver),
                        WCDeliverName = Convert.ToString(h.WCDeliverName),
                        LoadPlantMachine = GetLoadPlantMachineColumnDetails(Convert.ToInt32(h.Id), h.PCLStatus, h.LoadPlantMachine),
                        PlannerName = Convert.ToString(h.PlannerName),
                        Remark = GetRemarkColumnDetails(Convert.ToInt32(h.Id), h.PCLStatus, h.Remark),
                        MovementType = GetMovementTypeColumnDetails(Convert.ToInt32(h.Id), h.PCLStatus, h.MovementType),
                        PCLReleaseDate = Convert.ToString(h.PCLReleaseDate),
                        ReturnBalance = GetReturnBalanceColumnDetails(Convert.ToInt32(h.Id), h.PCLStatus, h.ReturnBalance, h.DisableReturnBalance),
                        AllowedStage1 = Convert.ToString(h.AllowedStage1),
                        AllowedStage1Name = GetAllowedStage1NameColumnDetails(Convert.ToInt32(h.Id), h.PCLStatus, h.AllowedStage1Name, h.DisableSTGI),

                        Action = Helper.HTMLActionString(Convert.ToInt32(h.Id), "Edit", "Edit Record", "fa fa-pencil-square-o", "EnabledEditLine(" + h.Id + ");"),

                    });
                }
                ViewBag.ReleasePCLData = res;
                int? totalRecords = lstData.Count();
                return Json(new
                {
                    param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder,
                    whereCondition,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = string.Empty
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public override async Task<ActionResult> ExecuteAction(string input)
        {
            StockResponseMsg objResponseMsg = new StockResponseMsg();
            try
            {
                bool IsValidUser = CheckUserIsValid(ReleasePCLAction); //To Be Changed
                if (!IsValidUser)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.IsInformation = true;
                    objResponseMsg.Value = UnAthorizedForUpdateData;
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                ActionInput actionInput = JsonConvert.DeserializeObject<ActionInput>(input);
                Dictionary<string, object> dict = JsonConvert.DeserializeObject<Dictionary<string, object>>(actionInput.Parameters.ToString());
                ActionOutput actionOutput = new ActionOutput();
                DataAdapter dataAdapter = new DataAdapter();
                ActionType actionType = (ActionType)Enum.Parse(typeof(ActionType), actionInput.ActionName);
                switch (actionType)
                {
                    case ActionType.ReleasePCL:
                        {
                            int Id = Convert.ToInt32(dict["Id"]);
                            bool UpdateScrap = Convert.ToBoolean(dict["UpdateScrap"]);
                            actionOutput = await dataAdapter.ReleasePlateCuttingLayout(objClsLoginInfo.UserName, Id, UpdateScrap);

                            break;
                        }
                    case ActionType.ValidateReturnDistributionArea:
                        {
                            string PCLNumber = Convert.ToString(dict["PCLNumber"]);
                            float TotalArea = (float) Convert.ToDouble(dict["TotalArea"]);
                            int Revision = Convert.ToInt32(dict["Revision"]);
                            actionOutput = await dataAdapter.ValidateReturnDistributionArea(PCLNumber, Revision, TotalArea);

                            break;
                        }
                    case ActionType.ModifyPCL:
                        {
                            int Id = Convert.ToInt32(dict["Id"]);
                            string remarkReason = Convert.ToString(dict["RemarkReason"]);
                            string pclModifiedRemark = Convert.ToString(dict["PCLModifiedRemark"]);
                            actionOutput = await dataAdapter.ModifyPlateCuttingLayout(Id, remarkReason, pclModifiedRemark, objClsLoginInfo.UserName);
                            break;
                        }

                }
                objResponseMsg.Value = actionOutput.Message;
                switch (actionOutput.Result)
                {
                    case OutputType.Success:
                        {
                            objResponseMsg.Key = true;
                            break;
                        }
                    case OutputType.Warning:
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.IsInformation = true;
                            break;
                        }
                    case OutputType.Error:
                        {
                            objResponseMsg.Key = false;
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg);
        }

        public new enum ActionType
        {
            [StringValue("Release PCL")]
            ReleasePCL = 0,
            [StringValue("Validate Return Distribution Area")]
            ValidateReturnDistributionArea = 1,
            [StringValue("ModifyPCL")]
            ModifyPCL = 2
        }

        #region Column Generate Functions
        private string GetTotalCuttingLengthColumnDetails(int Id, string PCLStatus, double? TotalCuttingLength)
        {
            var isDisabled = false;
            if(PCLStatus != null)//Full condition is not yet known....
            {
                isDisabled = true;
            }
            return Helper.GenerateTextbox(
                    Convert.ToInt32(Id), "TotalCuttingLength",
                                            (TotalCuttingLength.HasValue ? TotalCuttingLength.Value.ToString() : string.Empty),
                                            string.Empty, false, BorderNoneStyle, "18", "form-control totalcuttinglength inputField", isDisabled);
        }

        private string GetAllowedStage1NameColumnDetails(int Id, string PCLStatus, int? AllowedStage1Name, bool? DisableSTGI)
        {
            var isDisabled = Convert.ToBoolean(DisableSTGI);
            string allowStage1Name;
            if (PCLStatus != null)
            {
                isDisabled = true;
            }

            if (AllowedStage1Name == null)
            {
                allowStage1Name = clsImplementationEnum.yesno.No.GetStringValue();
            }
            else
            {
                allowStage1Name = Enum.GetName(typeof(clsImplementationEnum.yesno), AllowedStage1Name);
            }
            return Helper.HTMLAutoComplete(
                Convert.ToInt32(Id), "AllowedStage1Name", allowStage1Name, string.Empty, false, BorderNoneStyle, string.Empty,
                isDisabled, "100", string.Empty, "AllowedStage1Name", "form-control allowedstage1name inputField");
        }

        private string GetReturnBalanceColumnDetails(int Id, string PCLStatus, long? ReturnBalance, bool? DisableReturnBalance)
        {
            var isDisabled = Convert.ToBoolean(DisableReturnBalance);
            if (PCLStatus != null)
            {
                isDisabled = true;
            }
            return (Helper.GenerateTextbox(Id, "ReturnBalance", (ReturnBalance.HasValue ? ReturnBalance.Value.ToString() : string.Empty), string.Empty, isDisabled, string.Empty, "18", "form-control numeric"));
        }

        private string GetWCDeliverColumnDetails(int Id, string PCLStatus, string WCDeliverDesc, string WCDeliver)
        {
            var isDisabled = false;
            if (PCLStatus != null)
            {
                isDisabled = true;
            }
            return Helper.HTMLAutoComplete(
                Convert.ToInt32(Id), "WCDeliver", Convert.ToString(WCDeliverDesc), string.Empty, false, BorderNoneStyle, string.Empty,
                isDisabled, "100", string.Empty, "W/C Deliver", "form-control wcdeliver inputField");
        }

        private string GetPlannerColumnDetails(int Id, string PCLStatus, string PlannerName, string Planner)
        {
            var isDisabled = false;
            if (PCLStatus != null)
            {
                isDisabled = true;
            }
            return Helper.HTMLAutoComplete(
                Convert.ToInt32(Id), "Planner", Convert.ToString(PlannerName), string.Empty, false, BorderNoneStyle, string.Empty,
                isDisabled, "100", string.Empty, "Planner", "form-control planner inputField");
        }

        private string GetLoadPlantMachineColumnDetails(int Id, string PCLStatus, string LoadPlantMachine)
        {
            var isDisabled = false;
            if (PCLStatus != null)
            {
                isDisabled = true;
            }
            return Helper.HTMLAutoComplete(
                Convert.ToInt32(Id), "LoadPlantMachine", Convert.ToString(LoadPlantMachine), string.Empty, false, BorderNoneStyle, string.Empty,
                isDisabled, "100", string.Empty, "LoadPlantMachine", "form-control loadplantmachine inputField");
        }

        private string GetRemarkColumnDetails(int Id, string PCLStatus, string Remark)
        {
            var isDisabled = false;
            if (PCLStatus != null)
            {
                isDisabled = true;
            }
            return Helper.GenerateTextbox(
                Convert.ToInt32(Id), "Remark", Convert.ToString(Remark), string.Empty,
                false, BorderNoneStyle, "100", "form-control remark inputField", isDisabled);
        }

        private string GetMovementTypeColumnDetails(int Id, string PCLStatus, int? MovementType)
        {
            var isDisabled = false;
            clsImplementationEnum.MovementType movementType;
            if (PCLStatus != null)
            {
                isDisabled = true;
            }
            if (MovementType == null)
            {
                movementType = clsImplementationEnum.MovementType.AreaWiseCutting;
            }
            else
            {
                movementType = (clsImplementationEnum.MovementType)Enum.Parse(typeof(clsImplementationEnum.MovementType), MovementType.ToString());
            }

            return Helper.HTMLAutoComplete(
                Convert.ToInt32(Id), "MovementType", movementType.GetStringValue(), string.Empty, false, BorderNoneStyle, string.Empty,
                isDisabled, "100", string.Empty, "MovementType", "form-control movementtype inputField");
        }
        #endregion
    }
}
