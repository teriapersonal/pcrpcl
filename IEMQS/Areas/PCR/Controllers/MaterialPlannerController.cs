﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQS.ILN.Framework;
using System.Threading.Tasks;
using Newtonsoft.Json;
using IEMQS.ILN.Models;

namespace IEMQS.Areas.PCR.Controllers
{
    public class MaterialPlannerController : CommonController
    {

        #region Material Planner Views

        [SessionExpireFilter, UserPermissions, LastURLHistory]
        public ActionResult Index()
        {
            bool IsValidUser = CheckUserIsValid(MaterialPlannerAction);
            if (IsValidUser)
            {
                //ViewBag.StockNumberList = await GetStocksAsync(null);
                ViewBag.PCRLineStatusEnum = Enum.GetValues(typeof(clsImplementationEnum.PCRLineStatus)).Cast<clsImplementationEnum.PCRLineStatus>().Select(x => new { name = x.GetStringValue() });
                return View();
            }
            else
                return new RedirectResult("~/Authentication/Authenticate/AccessDenied");
        }
            
            
        

        [SessionExpireFilter]
        public async Task<PartialViewResult> _MaterialPlannerMultiOccurence()
        {
            ViewBag.WorkCenterDeliver = await GetWorkCenterWithLocation(0);
            ViewBag.CuttingLocation = db.SP_GENERAL_GET_CODE_DESCRIPTION_FROM_LN_TABLE("tltsfc513", "t_loca", "t_cloc").ToList().Select(i => new { CatID = i.Code, CatDesc = i.Description }).ToList();
            return PartialView("_MaterialPlannerMultiOccurence");
        }

        [SessionExpireFilter]
        public PartialViewResult _MaterialPlannerSingleOccurence()
        {
            return PartialView("_MaterialPlannerSingleOccurence");
        }

        #endregion

        [SessionExpireFilter]
        public async Task<ActionResult> LoadMaterialPlannerDataTable(JQueryDataTableParamModel param, string status)
        {

            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                //var lstHeader = db.SP_PCR_GET_PCR_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                var lstData = await (new DataAdapter().GetMaterialPlannerData(null, StartIndex, EndIndex));

                var res = new List<dynamic>();
                foreach (var h in lstData)
                {

                    res.Add(new
                    {
                        Id = Convert.ToString(h.Id),
                        PCRNumber = Convert.ToString(h.PCRNumber),
                        PCRLineNo = Convert.ToString(h.PCRLineNo),
                        PCRLineRevision = Convert.ToString(h.PCRLineRevision),
                        PCRType = ((clsImplementationEnum.PCRType)Enum.Parse(typeof(clsImplementationEnum.PCRType), Convert.ToString(h.PCRType))).GetStringValue(),
                        PCRRequirementDate = GetPCRRequirementDateColumnDetails(h.PCRLineStatus, Convert.ToInt32(h.Id), h.PCRRequirementDate),
                        PCRLineStatus = ((clsImplementationEnum.PCRLineStatus)Enum.Parse(typeof(clsImplementationEnum.PCRLineStatus), Convert.ToString(h.PCRLineStatus))).GetStringValue(),
                        PartNumber = Convert.ToString(h.PartNumber),
                        ManufacturingItem = GetManufacturingItemColumnDetails(h.PCRLineStatus, Convert.ToInt32(h.Id), h.ManufacturingItem),
                        ManufacturingItemDesc = Convert.ToString(h.ManufacturingItemDesc),

                        ChildItem = GetChildItemColumnDetails(h.PCRLineStatus, Convert.ToInt32(h.Id), h.ChildItem),
                        ChildItemDesc = Convert.ToString(h.ChildItemDesc),
                        ScrapQuantity = Convert.ToString(h.ScrapQuantity),

                        NestedQuantity = GetNestedQuantityColumnDetails(h.PCRLineStatus, Convert.ToInt32(h.Id), h.NestedQuantity),
                        AreaConsumed = GetAreaConsumedColumnDetails(h.PCRLineStatus, h.StockNumber, Convert.ToInt32(h.Id), h.AreaConsumed),

                        Length = Convert.ToString(roundOffValue(h.Length, 2)),
                        Width = Convert.ToString(roundOffValue(h.Width, 2)),
                        NoOfPieces = Convert.ToString(h.NoOfPieces),
                        StockNumber = GetStockNumberDetails(h.PCRLineStatus, h.StockRevision, Convert.ToInt32(h.Id), h.AreaConsumed, h.StockNumber),

                        PCLNumber = GetPCLNumberColumnDetails(h.PCRLineStatus, Convert.ToInt32(h.Id), h.PCLNumber),
                        //MovementType = GetMovementTypeColumnDetails(h.PCRLineStatus, Convert.ToInt32(h.Id), h.MovementType),
                        PCRPlannerRemark = GetPCRPlannerRemarkColumnDetails(h.PCRLineStatus, Convert.ToInt32(h.Id), h.PCRPlannerRemark),
                        StockRevision = GetStockRevisionColumnDetails(Convert.ToInt32(h.Id), h.StockRevision),
                        MaterialPlanner = GetMaterialPlannerColumnDetails(h.PCRLineStatus, Convert.ToInt32(h.Id), h.MaterialPlanner),

                        PCRCreationDate = Convert.ToString(h.PCRCreationDate),
                        PCRReleaseDate = Convert.ToString(h.PCRReleaseDate),
                        PCLCreationDate = Convert.ToString(h.PCLCreationDate),
                        PCLReleaseDate = Convert.ToString(h.PCLReleaseDate),
                        Project = Convert.ToString(h.Project),
                        Element = Convert.ToString(h.Element),
                        ManufacturingItemPosition = Convert.ToString(h.ManufacturingItemPosition),
                        ProjectDesc = Convert.ToString(h.ProjectDesc),
                        ElementDesc = Convert.ToString(h.ElementDesc),

                        //GrainOrientation = GetGrainOrientationColumnDetails(h.PCRLineStatus, Convert.ToInt32(h.Id), h.GrainOrientation),
                        //ShapeType = GetShapeTypeColumnDetails(h.PCRLineStatus, Convert.ToInt32(h.Id), h.ShapeType),
                        ShapeFile = GetShapeFileColumnDetails(h.PCRLineStatus, Convert.ToInt32(h.Id), h.ShapeFile),
                        Thickness = Convert.ToString(h.Thickness),
                        WCDeliver = GetWCDeliverColumnDetails(h.PCRLineStatus, Convert.ToInt32(h.Id), h.WCDeliverName, h.WCDeliver),

                        WCDeliverName = Convert.ToString(h.WCDeliverName),
                        MaterialSpecification = Convert.ToString(h.MaterialSpecification),
                        PCRHardCopyNo = Convert.ToString(h.PCRHardCopyNo),
                        CuttingLocation = Convert.ToString(h.CuttingLocation),
                        UseAlternateStock = GetUseAlternateStockColumnDetails(h.PCRLineStatus, Convert.ToInt32(h.Id), h.AreaConsumed, h.UseAlternateStock),// needs Input
                        AlternateStockRemark = GetAlternateStockRemarkColumnDetails(h.PCRLineStatus, h.UseAlternateStock, Convert.ToInt32(h.Id), h.AlternateStockRemark),
                        //LengthModified = GetLengthModifiedColumnDetails(h.PCRLineStatus, Convert.ToInt32(h.Id), h.LengthModified),
                        //WidthModified = GetWidthModifiedColumnDetails(h.PCRLineStatus, Convert.ToInt32(h.Id), h.WidthModified),

                        CuttingLocationModified = GetCuttingLocationModifiedColumnDetails(h.PCRLineStatus, Convert.ToInt32(h.Id), h.CuttingLocationModified),
                        OriginalItem = Convert.ToString(h.OriginalItem),
                        EmployeeName = Convert.ToString(h.EmployeeName),
                        PCRInitiator = Convert.ToString(h.PCRInitiator),
                        ItemRevision = Convert.ToString(h.ItemRevision),
                        LinkToSCR = Convert.ToString(h.LinkToSCR),
                        SCRNumber = Convert.ToString(h.SCRNumber),
                        ARMCode = Convert.ToString(h.ARMCode),
                        ItypeType = Convert.ToString(h.Itemtype),
                        PCRParameter = Convert.ToString(h.PCRParameter),
                        LocationAddress = Convert.ToString(h.LocationAddress),
                        Action = Helper.HTMLActionString(Convert.ToInt32(h.Id), "Edit", "Edit Record", "fa fa-pencil-square-o", "EnabledEditLine(" + h.Id + ");"),

                    });
                }
                int? totalRecords = lstData.Count();
                return Json(new
                {
                    param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder,
                    whereCondition,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = string.Empty
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public new enum ActionType
        {
            [StringValue("Validate Generate Plate Cutting Layout")]
            ValidateGeneratePCL = 0,
            [StringValue("Generate Plate Cutting Layout")]
            GeneratePlateCuttingLayout = 1,
            [StringValue("Validate Return Plate Cutting Request")]
            ValidateReturnPlateCuttingRequest = 2,
            [StringValue("Return Plate Cutting Request")]
            ReturnPlateCuttingRequest = 3,
            [StringValue("Unlink Stock")]
            UnlinkStock = 4,
            [StringValue("ValidateStockNumber")]
            ValidateStockNumber = 5
        }

        public override async Task<ActionResult> ExecuteAction(string input)
        {
            ActionResponse actionResponse = new ActionResponse();
            try
            {
                bool IsValidUser = CheckUserIsValid(MaterialPlannerAction);
                if (!IsValidUser)
                {
                    actionResponse.Key = false;
                    actionResponse.IsInformation = true;
                    actionResponse.Value = UnAthorizedForUpdateData;
                    return Json(actionResponse, JsonRequestBehavior.AllowGet);
                }
                ActionInput actionInput = JsonConvert.DeserializeObject<ActionInput>(input);
                Dictionary<string, object> dict = JsonConvert.DeserializeObject<Dictionary<string, object>>(actionInput.Parameters.ToString());
                ActionOutput actionOutput = new ActionOutput();
                DataAdapter dataAdapter = new DataAdapter();
                ActionType actionType = (ActionType)Enum.Parse(typeof(ActionType), actionInput.ActionName);
                switch (actionType)
                {
                    case ActionType.ValidateGeneratePCL:
                        {
                            int Id = Convert.ToInt32(dict["Id"]);
                            actionOutput = await dataAdapter.ValidateGeneratePCL(Id);

                            break;
                        }
                    case ActionType.GeneratePlateCuttingLayout:
                        {
                            int Id = Convert.ToInt32(dict["Id"]);
                            actionOutput = await dataAdapter.GeneratePlateCuttingLayout(Id, objClsLoginInfo.UserName);
                            break;
                        }
                    case ActionType.ValidateReturnPlateCuttingRequest:
                        {
                            int Id = Convert.ToInt32(dict["Id"]);
                            actionOutput = await dataAdapter.ValidateReturnPlateCuttingRequest(Id);
                            break;
                        }
                    case ActionType.ReturnPlateCuttingRequest:
                        {
                            int Id = Convert.ToInt32(dict["Id"]);
                            string remarks = Convert.ToString(dict["Remarks"]);
                            actionOutput = await dataAdapter.ReturnPlateCuttingRequest(Id, remarks, objClsLoginInfo.UserName);
                            break;
                        }
                    case ActionType.UnlinkStock:
                        {
                            int Id = Convert.ToInt32(dict["Id"]);
                            actionOutput = await dataAdapter.UnlinkStock(Id, objClsLoginInfo.UserName);
                            break;
                        }
                    case ActionType.ValidateStockNumber:
                        {
                            int Id = Convert.ToInt32(dict["Id"]);
                            string stockNumber = Convert.ToString(dict["StockNumber"]);
                            int stockRevision = Convert.ToInt32(dict["StockRevision"]);
                            int useAlternateStock = Convert.ToInt32(dict["UseAlternateStock"]);
                            actionOutput = await dataAdapter.ValidateStockNumber(Id, stockNumber, stockRevision, useAlternateStock, objClsLoginInfo.UserName);
                            break;
                        }
                }
                actionResponse.Value = actionOutput.Message;
                switch (actionOutput.Result)
                {
                    case OutputType.Success:
                        {
                            actionResponse.Key = true;
                            break;
                        }
                    case OutputType.Warning:
                        {
                            actionResponse.Key = false;
                            actionResponse.IsInformation = true;
                            break;
                        }
                    case OutputType.Error:
                        {
                            actionResponse.Key = false;
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                actionResponse.Key = false;
                actionResponse.Value = ex.Message;
            }
            return Json(actionResponse);
        }


        #region Column Generate Functions

        private string GetPCRRequirementDateColumnDetails(int? pcrLineStatus, int Id, DateTime? pcrRequirementDate)
        {
            bool isDisabled = true;
            if (pcrLineStatus == (int)clsImplementationEnum.PCRLineStatus.Created
                || pcrLineStatus == (int)clsImplementationEnum.PCRLineStatus.ReleasedtoMaterialPlanner)
            {
                isDisabled = false;
            }
            var pcrDate = pcrRequirementDate.HasValue ? pcrRequirementDate.Value.ToString("dd/MM/yyyy") : string.Empty;
            return Helper.GenerateHTMLDateControl(Convert.ToInt32(Id), "PCRRequirementDate", pcrDate, isDisabled,
                "datecontrol", "inputField", BorderNoneStyle, string.Empty, true);
        }

        private string GetManufacturingItemColumnDetails(int? pcrLineStatus, int Id, string manufacturingItem)
        {
            bool isDisabled = true;
            //if (pcrLineStatus == (int)clsImplementationEnum.PCRLineStatus.PCLReleased || pcrLineStatus == (int)clsImplementationEnum.PCRLineStatus.PCLGenerated)
            //{
            //    isDisabled = true;
            //}
            return Helper.GenerateTextbox(
                Convert.ToInt32(Id), "ManufacturingItem", manufacturingItem, string.Empty,
                false, BorderNoneStyle, "100", "form-control manufacturingitem inputField", isDisabled);
        }

        private string GetChildItemColumnDetails(int? pcrLineStatus, int Id, string childItem)
        {
            bool isDisabled = true;
            //if (pcrLineStatus == (int)clsImplementationEnum.PCRLineStatus.PCLReleased || pcrLineStatus == (int)clsImplementationEnum.PCRLineStatus.PCLGenerated)
            //{
            //    isDisabled = true;
            //}
            return Helper.GenerateTextbox(
                Convert.ToInt32(Id), "ChildItem", childItem, string.Empty,
                false, BorderNoneStyle, "100", "form-control childitem inputField", isDisabled);
        }

        private string GetPCLNumberColumnDetails(int? pcrLineStatus, int Id, string pclNumber)
        {           
            return Helper.GenerateTextbox(
                Convert.ToInt32(Id), "PCLNumber", pclNumber, string.Empty,
                true, BorderNoneStyle, "100", "form-control pclnumber inputField", true);
        }

        private string GetPCRPlannerRemarkColumnDetails(int? pcrLineStatus, int Id, string pcrPlannerRemark)
        {
            bool isDisabled = true;
            if (pcrLineStatus == (int)clsImplementationEnum.PCRLineStatus.Created
                || pcrLineStatus == (int)clsImplementationEnum.PCRLineStatus.ReleasedtoMaterialPlanner)
            {
                isDisabled = false;
            }
            return Helper.GenerateTextbox(
                Convert.ToInt32(Id), "PCRPlannerRemark", pcrPlannerRemark, string.Empty,
                false, BorderNoneStyle, "100", "form-control pcrplannerremark inputField", isDisabled);
        }

        private string GetMaterialPlannerColumnDetails(int? pcrLineStatus, int Id, string materialPlanner)
        {
            bool isDisabled = true;
            if (pcrLineStatus == (int)clsImplementationEnum.PCRLineStatus.Created
                || pcrLineStatus == (int)clsImplementationEnum.PCRLineStatus.ReleasedtoMaterialPlanner)
            {
                isDisabled = false;
            }
            return Helper.GenerateTextbox(
                Convert.ToInt32(Id), "MaterialPlanner", materialPlanner, string.Empty,
                false, BorderNoneStyle, "100", "form-control materialplanner inputField", isDisabled);
        }

        private string GetGrainOrientationColumnDetails(int? pcrLineStatus, int Id, string grainOrientation)
        {
            bool isDisabled = false;
            if (pcrLineStatus == (int)clsImplementationEnum.PCRLineStatus.PCLReleased || pcrLineStatus == (int)clsImplementationEnum.PCRLineStatus.PCLGenerated)
            {
                isDisabled = true;
            }
            return Helper.GenerateTextbox(
                Convert.ToInt32(Id), "GrainOrientation", grainOrientation, string.Empty,
                false, BorderNoneStyle, "100", "form-control grainorientation inputField", isDisabled);
        }

        private string GetShapeTypeColumnDetails(int? pcrLineStatus, int Id, string shapeType)
        {
            bool isDisabled = false;
            if (pcrLineStatus == (int)clsImplementationEnum.PCRLineStatus.PCLReleased || pcrLineStatus == (int)clsImplementationEnum.PCRLineStatus.PCLGenerated)
            {
                isDisabled = true;
            }
            return Helper.GenerateTextbox(
                Convert.ToInt32(Id), "ShapeType", shapeType, string.Empty,
                false, BorderNoneStyle, "100", "form-control shapetype inputField", isDisabled);
        }

        private string GetShapeFileColumnDetails(int? pcrLineStatus, int Id, string shapeFile)
        {
            bool isDisabled = true;
            if (pcrLineStatus == (int)clsImplementationEnum.PCRLineStatus.Created
                || pcrLineStatus == (int)clsImplementationEnum.PCRLineStatus.ReleasedtoMaterialPlanner)
            {
                isDisabled = false;
            }
            return Helper.GenerateTextbox(
                Convert.ToInt32(Id), "ShapeFile", shapeFile, string.Empty,
                false, BorderNoneStyle, "100", "form-control shapefile inputField", isDisabled);
        }

        private string GetWCDeliverColumnDetails(int? pcrLineStatus, int Id, string wCDeliverDesc, string wCDeliver)
        {
            bool isDisabled = true;
            if (pcrLineStatus == (int)clsImplementationEnum.PCRLineStatus.Created
                || pcrLineStatus == (int)clsImplementationEnum.PCRLineStatus.ReleasedtoMaterialPlanner)
            {
                isDisabled = false;
            }
            return Helper.HTMLAutoComplete(
                Convert.ToInt32(Id), "WCDeliver", wCDeliverDesc, string.Empty, false, BorderNoneStyle, string.Empty,
                isDisabled, "100", string.Empty, "W/C Deliver", "form-control wcdeliver inputField");
        }

        private string GetAlternateStockRemarkColumnDetails(int? pcrLineStatus, int? useAlternateStock, int Id, string alternateStockRemark)
        {
            bool isDisabled = false;            
            if(pcrLineStatus == (int)clsImplementationEnum.PCRLineStatus.PCLReleased || pcrLineStatus == (int)clsImplementationEnum.PCRLineStatus.PCLGenerated){
                isDisabled = true;
            }
            else if(useAlternateStock.HasValue == false)
            {
                isDisabled = true;
            }
            else
            {
                if((int)clsImplementationEnum.yesno.No == useAlternateStock.Value - 1){
                    isDisabled = true;
                }
            }           

            return Helper.GenerateTextbox(
                Convert.ToInt32(Id), "AlternateStockRemark", alternateStockRemark, string.Empty,
                false, BorderNoneStyle, "500", "form-control alternatestockremark inputField", isDisabled);
        }


        private string GetNestedQuantityColumnDetails(int? pcrLineStatus, int Id, long? nestedQuantity)
        {
            bool isDisabled = true;
            if (pcrLineStatus == (int)clsImplementationEnum.PCRLineStatus.Created 
                || pcrLineStatus == (int)clsImplementationEnum.PCRLineStatus.ReleasedtoMaterialPlanner)
            {
                isDisabled = false;
            }
            return Helper.GenerateTextbox(
                Convert.ToInt32(Id), "NestedQuantity", (nestedQuantity.HasValue ? Convert.ToString(nestedQuantity.Value) : string.Empty),
                string.Empty, false, BorderNoneStyle,
                "9", "form-control nestedquantity inputField", isDisabled);
        }

        private string GetAreaConsumedColumnDetails(int? pcrLineStatus, string stockNumber, int Id, double? areaConsumed)
        {
            bool isDisabled = false;
            areaConsumed = roundOffValue(areaConsumed, 2);            
            if (pcrLineStatus == (int)clsImplementationEnum.PCRLineStatus.PCLReleased || pcrLineStatus == (int)clsImplementationEnum.PCRLineStatus.PCLGenerated)
            {
                isDisabled = true;
            }
            else
            {
                if (!string.IsNullOrEmpty(stockNumber))
                {
                    isDisabled = false;
                }
                else
                {
                    isDisabled = true;
                }
            }
            return Helper.GenerateTextbox(
                Convert.ToInt32(Id), "AreaConsumed", (areaConsumed.HasValue ? Convert.ToString(areaConsumed.Value) : string.Empty),
                string.Empty, false, BorderNoneStyle,
                "9", "form-control areaconsumed inputField", isDisabled);
        }

        private string GetLengthModifiedColumnDetails(int? pcrLineStatus, int Id, double? lengthModified)
        {
            bool isDisabled = false;
            if (pcrLineStatus == (int)clsImplementationEnum.PCRLineStatus.PCLReleased || pcrLineStatus == (int)clsImplementationEnum.PCRLineStatus.PCLGenerated)
            {
                isDisabled = true;
            }
            return (Helper.GenerateTextbox(Id, "LengthModified", 
                (lengthModified.HasValue ? lengthModified.Value.ToString() : string.Empty), string.Empty, isDisabled, string.Empty, "18", "form-control numeric"));

        }

        private string GetStockRevisionColumnDetails(int Id, int? stockRevision)
        {
            return Helper.GenerateTextbox(
                Id,
                "StockRevision",
                stockRevision.HasValue ? stockRevision.Value.ToString() : string.Empty,
                string.Empty,
                true,
                BorderNoneStyle,
                "18",
                "form-control stock-revision numeric",
                true,
                string.Empty,
                string.Empty
                );
        }

        private string GetWidthModifiedColumnDetails(int? pcrLineStatus, int Id, double? widthModified)
        {
            bool isDisabled = false;
            if (pcrLineStatus == (int)clsImplementationEnum.PCRLineStatus.PCLReleased || pcrLineStatus == (int)clsImplementationEnum.PCRLineStatus.PCLGenerated)
            {
                isDisabled = true;
            }

            return (Helper.GenerateTextbox(Id, "WidthModified", (widthModified.HasValue ? widthModified.Value.ToString() : string.Empty), string.Empty, isDisabled, string.Empty, "18", "form-control numeric"));
        }

        private string GetCuttingLocationModifiedColumnDetails(int? pcrLineStatus, int Id, string cuttingLocationModified)
        {
            bool isDisabled = true;
            // user can change without restriction
            if (pcrLineStatus == (int)clsImplementationEnum.PCRLineStatus.Created || pcrLineStatus == (int)clsImplementationEnum.PCRLineStatus.ReleasedtoMaterialPlanner)
            {
                isDisabled = false;
            }
            return Helper.HTMLAutoComplete(
                Convert.ToInt32(Id), "CuttingLocationModified", cuttingLocationModified, string.Empty, false, BorderNoneStyle, string.Empty,
                isDisabled, "100", string.Empty, "Modify Cutting Location", "form-control cuttinglocationmodified inputField");
        }

        private string GetStockNumberDetails(int? pcrLineStatus, int? stockRevision, int Id, float? areaConsumed, string stockNumber)
        {
            string parent = "<div class = 'zoom-container' >";
            bool isDisabled;            
            if (pcrLineStatus == (int)clsImplementationEnum.PCRLineStatus.Created
                || pcrLineStatus == (int)clsImplementationEnum.PCRLineStatus.ReleasedtoMaterialPlanner)
            {
                if (areaConsumed.HasValue && areaConsumed.Value > 0)
                {
                    isDisabled = true;
                }
                else
                {
                    isDisabled = false;
                }

            }
            else
            {
                isDisabled = true;
            }
            string s = "form-control stock-number inputField";// isDisabled ? string.Empty : "; width:80%; display:inline;";
            if (isDisabled)
            {
                s += " stock-number-only";
            }
            else
            {
                s+= " stock-number-with-zoom";
            }
            string item = Helper.HTMLAutoComplete(
                Convert.ToInt32(Id), "StockNumber", stockNumber, string.Empty, false, BorderNoneStyle, string.Empty,
                true, "100", string.Empty, "Stock Number", s);
            string s2 = "glyphicon glyphicon-search";
            if (isDisabled)
            {
                s2 += " zoom-hidden";
            }
            else
            {
                s2 += " zoom-display";
            }
            string zoom = "<span class='" + s2 + "' ";
            
            if(!isDisabled)
            {
                zoom += " onclick='fnOpenZoom(this);' ";
            }
            zoom += " > </span> ";
            return parent + item + zoom + "</div>";
        }


        private string GetMovementTypeColumnDetails(int? pcrLineStatus, int Id, int? movementType)
        {
            bool isDisabled = false;
            if (pcrLineStatus == (int)clsImplementationEnum.PCRLineStatus.PCLReleased || pcrLineStatus == (int)clsImplementationEnum.PCRLineStatus.PCLGenerated)
            {
                isDisabled = true;
            }
            clsImplementationEnum.MovementType value;
            if (movementType.HasValue ==false)
            {
                value = clsImplementationEnum.MovementType.AreaWiseCutting;
            }
            else
            {
                value = (clsImplementationEnum.MovementType)Enum.Parse(typeof(clsImplementationEnum.MovementType), movementType.Value.ToString());
            }

            return Helper.HTMLAutoComplete(
                Convert.ToInt32(Id), "MovementType", value.GetStringValue(), string.Empty, false, BorderNoneStyle, string.Empty,
                isDisabled, "20", string.Empty, "MovementType", "form-control movementtype inputField");
        }

        private string GetUseAlternateStockColumnDetails(int? pcrLineStatus, int Id, float? areaConsumed, int? useAlternateStock)
        {
            bool isDisabled = false;            
            clsImplementationEnum.yesno value;
            if (pcrLineStatus == (int)clsImplementationEnum.PCRLineStatus.ReleasedtoMaterialPlanner || pcrLineStatus == (int)clsImplementationEnum.PCRLineStatus.Created)
            {
                if (areaConsumed.HasValue && areaConsumed.Value > 0)
                {
                    isDisabled = true;
                }
                else
                {
                    isDisabled = false;
                }
            }
            else
            {
                isDisabled = true;
            }

            if (useAlternateStock.HasValue == false)
            {
                value = clsImplementationEnum.yesno.No;
            }
            else
            {
                value = (clsImplementationEnum.yesno)Enum.Parse(typeof(clsImplementationEnum.yesno), (useAlternateStock.Value - 1).ToString());
            }
            return Helper.HTMLAutoComplete(
                Convert.ToInt32(Id), "UseAlternateStock", value.GetStringValue(), string.Empty, false, BorderNoneStyle, string.Empty,
                isDisabled, "5", string.Empty, "UseAlternateStock", "form-control usealternatestock inputField");
        }
        #endregion

        public async Task<JsonResult> GetStockList(string input)
        {
            ActionInput actionInput = JsonConvert.DeserializeObject<ActionInput>(input);
            Dictionary<string, object> dict = JsonConvert.DeserializeObject<Dictionary<string, object>>(actionInput.Parameters.ToString());
            int pcrLineId = Convert.ToInt32(dict["Id"].ToString());            
            List<IEMQSImplementation.PCR.Stock> stockList = await GetStocksAsync(pcrLineId);
            object data = JsonConvert.SerializeObject(stockList);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        private async Task<List<IEMQSImplementation.PCR.Stock>> GetStocksAsync(int pcrLineId)
        {
            return await (new DataAdapter().GetStockNumberList(objClsLoginInfo.UserName,  pcrLineId));
        }

    }
}