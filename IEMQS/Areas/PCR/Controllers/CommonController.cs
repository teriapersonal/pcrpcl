﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQS.ILN.Framework;
using System.Threading.Tasks;
using Newtonsoft.Json;
using IEMQS.ILN.Models;
using Newtonsoft.Json.Converters;
using IEMQSImplementation.PCR;

namespace IEMQS.Areas.PCR.Controllers
{
	public class CommonController : clsBase
	{
		protected const string GeneratePCRAction = "GeneratePCR";
		protected const string MaterialPlannerAction = "MaterialPlanner";
		protected const string ReleasePCLAction = "ReleasePCL";
		protected const string PCLWorkflowAction = "PCLWorkflow";
		protected const string UnAthorizedForUpdateData = "You are un-authorized for update data!";
		protected const string EmailSubmittedSuccess = "Email submitted successfully";
		protected const string BorderNoneStyle = "border:none";
        protected static object optionsObj = new object();
        protected static string screenNameVar = "";
        protected static int idVar = 0;
        protected static string projectVar = "";


		protected virtual bool CheckUserIsValid(string actionName)
		{
			bool IsValid = false;
			if (objClsLoginInfo.listMenuResult != null)
			{
				var lstMenuResult = objClsLoginInfo.listMenuResult;
				if (lstMenuResult.Any(i => (string.IsNullOrEmpty(i.Action) ? string.Empty : i.Action.ToLower()) == actionName.ToLower()))
				{
					IsValid = true;
				}
			}
			return true;// IsValid;
		}

		#region Excel Generate
		public async Task<ActionResult> GenerateExcel(string whereCondition, string strSortOrder, string gridType, dynamic optionsObj)
		{
			var objResponseMsg = new clsHelper.ResponseMsg();
			try
			{
				string strFileName = string.Empty;
				if (gridType == clsImplementationEnum.GridType.GeneratePCR.GetStringValue())
				{
					int? StartIndex = 1;
					int? EndIndex = 10; // You need all the fields by the way or ones displayed on the screen
					//string project = Request["Project"].ToString();
					var lst = await (new DataAdapter().GetPCRData(projectVar, strSortOrder, StartIndex, EndIndex));
					if (!lst.Any())
					{
						objResponseMsg.Key = false;
						objResponseMsg.Value = "No Data Found";
						return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
					}

                    var data = (from h in lst
                                select new
                                {
									Id = Convert.ToString(h.Id),
									h.Element,
									ManufacturingItem = h.ManufacturingItem + " - " + h.ManufacturingItemDesc,
									BudgetLine = Convert.ToString(h.PartNumber),
									Sequence = Convert.ToString(h.Sequence),
									FindNo = Convert.ToString(h.FindNo),
									h.ChildItemSegment,
									h.ChildItem,
									h.ChildItemDesc,
									Length = Convert.ToString(h.Length),
									Width = Convert.ToString(h.Width),
									LengthModified = Convert.ToString(h.LengthModified),
									WidthModified = Convert.ToString(h.WidthModified),
									NoOfPieces = Convert.ToString(h.NoOfPieces),
									RequiredQuantity = Convert.ToString(h.RequiredQuantity),
									ItemMaterialGrade = h.ItemMaterialGrade + " - " + h.ItemMaterialGradeName,
									PCRNumber = Convert.ToString(h.PCRNumber),
									PCRLineNo = Convert.ToString(h.PCRLineNo),
									PCRType = Convert.ToString(h.PCRType),
									PCRRequirementDate = Convert.ToString(h.PCRRequirementDate),
									WCDeliver = Convert.ToString(h.WCDeliverName),
									PlannerRemark = Convert.ToString(h.PlannerRemark),
									CuttingLocation = Convert.ToString(h.CuttingLocation),
									Priority = Convert.ToString(h.Priority),
									Location = Convert.ToString(h.Location),
									h.ItemRevision,
									SubLocation = Convert.ToString(h.SubLocation),
									h.PCRTypeName,
									h.WCDeliverName,
									Thickness = Convert.ToString(h.Thickness)
								}).ToList();
					strFileName = Helper.GenerateExcel(data, objClsLoginInfo.UserName);
				}
				else if (gridType == clsImplementationEnum.GridType.GeneratePCL.GetStringValue())
				{

					int? StartIndex = 1;
					int? EndIndex = 10;

					//var lst = db.SP_PCR_GET_PCR_MATERIAL_PLANNER_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
					var lst = await(new DataAdapter().GetMaterialPlannerData(null, StartIndex, EndIndex));
					if (!lst.Any())
					{
						objResponseMsg.Key = false;
						objResponseMsg.Value = "No Data Found";
						return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
					}
					var data = (from h in lst
								select new
								{
									Id = Convert.ToString(h.Id),
									PCRNumber = Convert.ToString(h.PCRNumber),
									PCRLineNo = Convert.ToString(h.PCRLineNo),
									PCRLineRevision = Convert.ToString(h.PCRLineRevision),
									PCRType = ((clsImplementationEnum.PCRType)Enum.Parse(typeof(clsImplementationEnum.PCRType), Convert.ToString(h.PCRType))).GetStringValue(),
									PCRRequirementDate = Convert.ToString(h.PCRRequirementDate),
									PCRLineStatus = ((clsImplementationEnum.PCRLineStatus)Enum.Parse(typeof(clsImplementationEnum.PCRLineStatus), Convert.ToString(h.PCRLineStatus))).GetStringValue(),
									PartNumber = Convert.ToString(h.PartNumber),
									ManufacturingItem = Convert.ToString(h.ManufacturingItem),
									ManufacturingItemDesc = Convert.ToString(h.ManufacturingItemDesc),

									ChildItem = Convert.ToString(h.ChildItem),
									ChildItemDesc = Convert.ToString(h.ChildItemDesc),
									ScrapQuantity = Convert.ToString(h.ScrapQuantity),

									NestedQuantity = Convert.ToString(h.NestedQuantity),
									AreaConsumed = Convert.ToString(h.AreaConsumed),

									Length = Convert.ToString(roundOffValue(h.Length, 2)),
									Width = Convert.ToString(roundOffValue(h.Width, 2)),
									NoOfPieces = Convert.ToString(h.NoOfPieces),
									StockNumber = Convert.ToString(h.StockNumber),
									PCLNumber = Convert.ToString(h.PCLNumber),

									PCRPlannerRemark = Convert.ToString(h.PCRPlannerRemark),
									StockRevision = Convert.ToString(h.StockRevision),
									MaterialPlanner = Convert.ToString(h.MaterialPlanner),
									PCRCreationDate = Convert.ToString(h.PCRCreationDate),
									PCRReleaseDate = Convert.ToString(h.PCRReleaseDate),
									PCLCreationDate = Convert.ToString(h.PCLCreationDate),
									PCLReleaseDate = Convert.ToString(h.PCLReleaseDate),
									Project = Convert.ToString(h.Project),
									Element = Convert.ToString(h.Element),
									ManufacturingItemPosition = Convert.ToString(h.ManufacturingItemPosition),
									ProjectDesc = Convert.ToString(h.ProjectDesc),
									ElementDesc = Convert.ToString(h.ElementDesc),
									ShapeFile = Convert.ToString(h.ShapeFile),
									Thickness = Convert.ToString(h.Thickness),
									WCDeliver = Convert.ToString(h.WCDeliverName),
									MaterialSpecification = Convert.ToString(h.MaterialSpecification),
									PCRHardCopyNo = Convert.ToString(h.PCRHardCopyNo),
									CuttingLocation = Convert.ToString(h.CuttingLocation),
									UseAlternateStock = Convert.ToString(Enum.GetName(typeof(clsImplementationEnum.yesno), h.UseAlternateStock)),
									AlternateStockRemark = Convert.ToString(h.AlternateStockRemark),
									CuttingLocationModified = Convert.ToString(h.CuttingLocationModified),
									OriginalItem = Convert.ToString(h.OriginalItem),
									EmployeeName = Convert.ToString(h.EmployeeName),
									PCRInitiator = Convert.ToString(h.PCRInitiator),
									ItemRevision = Convert.ToString(h.ItemRevision),
									LinkToSCR = Convert.ToString(h.LinkToSCR),
									SCRNumber = Convert.ToString(h.SCRNumber),
									ARMCode = Convert.ToString(h.ARMCode)
								}).ToList();

					strFileName = Helper.GenerateExcel(data, objClsLoginInfo.UserName);
				}
				else if (gridType == clsImplementationEnum.GridType.ReleasePCL.GetStringValue())
                {
					var lst = await (new DataAdapter().GetReleasePCLData(objClsLoginInfo.UserName));
					if (!lst.Any())
					{
						objResponseMsg.Key = false;
						objResponseMsg.Value = "No Data Found";
						return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
					}

					var data = (from h in lst
								select new
								{
									Id = Convert.ToString(h.Id),
									PCLNumber = Convert.ToString(h.PCLNumber),
									Revision = Convert.ToString(h.Revision),
									TotalConsumedArea = Convert.ToString(roundOffValue(h.TotalConsumedArea, 2)),
									ScrapQuantity = Convert.ToString(h.ScrapQuantity),
									ReturnArea = Convert.ToString(roundOffValue(h.ReturnArea, 2)),
									PCLStatus = Convert.ToString(h.PCLStatus),
									ReturnRemark = Convert.ToString(h.ReturnRemark),
									TotalCuttingLength = Convert.ToString(h.TotalCuttingLength),
									StockNumber = Convert.ToString(h.StockNumber),
									TotalArea = Convert.ToString(roundOffValue(h.TotalArea, 2)),
									WCDeliver = Convert.ToString(h.WCDeliverName),
									LoadPlantMachine = Convert.ToString(""), //h.LoadPlantMachine
									PlannerName = Convert.ToString(h.PlannerName),
									Remark = Convert.ToString(h.Remark),
									MovementType = h.MovementType == null ? "Area Wise Cutting" : ((clsImplementationEnum.MovementType)Enum.Parse(typeof(clsImplementationEnum.MovementType), Convert.ToString(h.MovementType))).GetStringValue(),
									PCLReleaseDate = Convert.ToString(h.PCLReleaseDate),
									ReturnBalance = Convert.ToString(h.ReturnBalance),
									AllowedStage1 = Convert.ToString(h.AllowedStage1),
									AllowedStage1Name = h.AllowedStage1Name == null ? "No" : Convert.ToString(Enum.GetName(typeof(clsImplementationEnum.yesno), h.AllowedStage1Name))
								}).ToList();

					strFileName = Helper.GenerateExcel(data, objClsLoginInfo.UserName);
				}
				else if (gridType == clsImplementationEnum.GridType.ReturnBalance.GetStringValue())
				{
                    var lst = await (new DataAdapter().GetReturnDistributionData(objClsLoginInfo.UserName, idVar));
                    if (!lst.Any())
					{
						objResponseMsg.Key = false;
						objResponseMsg.Value = "No Data Found";
						return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
					}

					strFileName = Helper.GenerateExcel(lst, objClsLoginInfo.UserName);
				}
				else if (gridType == clsImplementationEnum.GridType.PCLWorkflow.GetStringValue())
				{
					//optionsObj.Where(v => v.Key == keyNameVariable).Select(x => x.Value).FirstOrDefault();
					//var val = ((dynamic)optionsObj).ScreenName;
					var lst = await (new DataAdapter().GetPCLWorkflowData(objClsLoginInfo.UserName, screenNameVar));
					if (!lst.Any())
					{
						objResponseMsg.Key = false;
						objResponseMsg.Value = "No Data Found";
						return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
					}

					strFileName = Helper.GenerateExcel(lst, objClsLoginInfo.UserName);
				}
				objResponseMsg.Key = true;
				objResponseMsg.Value = strFileName;
				return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
				objResponseMsg.Key = false;
				objResponseMsg.Value = "Error in excel generate, Please try again";
				return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
			}
		}
		#endregion

		public virtual async Task<ActionResult> ExecuteAction(string input)
		{
			ActionResponse actionResponse = new ActionResponse();
			try
			{
				//user validation...

				ActionInput actionInput = JsonConvert.DeserializeObject<ActionInput>(input);
				Dictionary<string, object> dict = JsonConvert.DeserializeObject<Dictionary<string, object>>(actionInput.Parameters.ToString());
				ActionOutput actionOutput = new ActionOutput();
				DataAdapter dataAdapter = new DataAdapter();
				ActionType actionType = (ActionType)Enum.Parse(typeof(ActionType), actionInput.ActionName);// actionName Not Edited in JS
				switch (actionType)
				{
					case ActionType.AddOrEditRows:
						{
							clsImplementationEnum.Callers caller = (clsImplementationEnum.Callers)Enum.Parse(typeof(clsImplementationEnum.Callers), Convert.ToString(dict["Caller"]));
							int mode = Convert.ToInt32(dict["Mode"]);
							bool isDuplicate = Convert.ToBoolean(dict["IsDuplicate"]);
							switch (caller)
							{
								case clsImplementationEnum.Callers.GeneratePCR:
									{
										List<SP_PCR_GET_PCR_DATA_Result> parameters = JsonConvert.DeserializeObject<List<SP_PCR_GET_PCR_DATA_Result>>(Convert.ToString(dict["Rows"]), new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });
										actionOutput = await dataAdapter.AddOrEditRows(caller, parameters, objClsLoginInfo.UserName, mode, isDuplicate);
										//SendAnEmail(2, new List<string>(), new List<string>());
										break;
									}
								case clsImplementationEnum.Callers.MaterialPlanner:
									{
										List<SP_PCR_GET_MATERIAL_PLANNER_DATA_Result> parameters = JsonConvert.DeserializeObject<List<SP_PCR_GET_MATERIAL_PLANNER_DATA_Result>>(Convert.ToString(dict["Rows"]), new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });
										actionOutput = await dataAdapter.AddOrEditRows(caller, parameters, objClsLoginInfo.UserName, mode, isDuplicate);
										break;
									}
								case clsImplementationEnum.Callers.ReleasePCL:
									{
										List<RELEASE_PCL_DATA_Result> parameters = JsonConvert.DeserializeObject<List<RELEASE_PCL_DATA_Result>>(Convert.ToString(dict["Rows"]), new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });
										actionOutput = await dataAdapter.AddOrEditRows(caller, parameters, objClsLoginInfo.UserName, mode, isDuplicate);
										break;
									}
								case clsImplementationEnum.Callers.MaintainReturnDistribution:
									{
										List<RETURN_BALANCE_DISTRIBUTION_Result> parameters = JsonConvert.DeserializeObject<List<RETURN_BALANCE_DISTRIBUTION_Result>>(Convert.ToString(dict["Rows"]), new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });
										actionOutput = await dataAdapter.AddOrEditRows(caller, parameters, objClsLoginInfo.UserName, mode, isDuplicate);
										break;
									}
								case clsImplementationEnum.Callers.PCLWorkflow:
									{
										//List<PCL_WORKFLOW_Result.Columns> parameters = JsonConvert.DeserializeObject<List<PCL_WORKFLOW_Result.Columns>>(Convert.ToString(dict["Columns"]), new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });
										//actionOutput = await dataAdapter.AddOrEditRows(caller, parameters, objClsLoginInfo.UserName, mode, isDuplicate);
										break;
									}
							}

							break;
						}
					case ActionType.BeforeReWrite:
						{
							clsImplementationEnum.Callers caller = (clsImplementationEnum.Callers)Enum.Parse(typeof(clsImplementationEnum.Callers), Convert.ToString(dict["Caller"]));
							switch (caller)
							{
								case clsImplementationEnum.Callers.GeneratePCR:
									{
										List<SP_PCR_GET_PCR_DATA_Result> parameters = JsonConvert.DeserializeObject<List<SP_PCR_GET_PCR_DATA_Result>>(Convert.ToString(dict["Rows"]), new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });
										actionOutput = await dataAdapter.BeforeReWrite(caller, parameters, objClsLoginInfo.UserName);
										break;
									}
								case clsImplementationEnum.Callers.MaterialPlanner:
									{
										List<SP_PCR_GET_MATERIAL_PLANNER_DATA_Result> parameters = JsonConvert.DeserializeObject<List<SP_PCR_GET_MATERIAL_PLANNER_DATA_Result>>(Convert.ToString(dict["Rows"]), new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });
										actionOutput = await dataAdapter.BeforeReWrite(caller, parameters, objClsLoginInfo.UserName);
										break;
									}
							}

							break;
						}
					case ActionType.SendAnEmail:
						{
							SendEmailForActionType sendEmailForActionType = (SendEmailForActionType)Enum.Parse(typeof(SendEmailForActionType), Convert.ToString(dict["SendEmailForActionName"]));// actionName Not Edited in JS
							switch (sendEmailForActionType)
							{
								case SendEmailForActionType.ReturnPlateCuttingRequest:
									{
										int Id = Convert.ToInt32(dict["Id"]);
										string remarks = Convert.ToString(dict["Remarks"]);
										List<SP_PCR_GET_MATERIAL_PLANNER_DATA_Result> parameters = JsonConvert.DeserializeObject<List<SP_PCR_GET_MATERIAL_PLANNER_DATA_Result>>(Convert.ToString(dict["Rows"]), new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });
										actionOutput = await SendAnEmail(Id, MailTemplates.PCR.ReturnPCR, remarks, parameters, new List<string>(), new List<string>());
										//actionOutput = await dataAdapter.BeforeReWrite(caller, parameters, objClsLoginInfo.UserName);
										break;
									}
							}

							break;
						}
				}
				actionResponse.Value = actionOutput.Message;
				switch (actionOutput.Result)
				{
					case OutputType.Success:
						{
							actionResponse.Key = true;
							break;
						}
					case OutputType.Warning:
						{
							actionResponse.Key = false;
							actionResponse.IsInformation = true;
							break;
						}
					case OutputType.Error:
						{
							actionResponse.Key = false;
							break;
						}
				}
			}
			catch (Exception ex)
			{
				//Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
				actionResponse.Key = false;
				actionResponse.Value = ex.Message;
			}
			return Json(actionResponse);
		}

		protected async Task<object> GetWorkCenterWithLocation(int showAll)
		{
			DataAdapter dataAdapter = new DataAdapter();
			List<SP_PCR_LOOK_UP> wcListResult = await dataAdapter.GetWorkCenterWithLocation(showAll);
            object o = wcListResult.Select(i => new { CatID = i.Code, CatDesc = i.CodeDescription, RelatedField = i.ParentID }).ToList();
            return o;
        }

		protected async Task<object> GetEnumData()
		{
			DataAdapter dataAdapter = new DataAdapter();
			List<SP_PCR_LOOK_UP> wcListResult = await dataAdapter.GetEnumData(objClsLoginInfo.UserName);
			object o = wcListResult.Select(i => new { CatID = i.Code, CatDesc = i.CodeDescription, TextDesc = i.Description }).ToList();
			return o;
		}

		protected async Task<object> GetPCLNumberList()
		{
			DataAdapter dataAdapter = new DataAdapter();
			List<SP_PCR_LOOK_UP> pclNumberListResult = await dataAdapter.GetPCLNumberList();
			object o = pclNumberListResult.Select(i => new { CatID = i.Code, CatDesc = i.CodeDescription }).ToList();
			return o;
		}

		protected async Task<object> GetLocationWithAddress()
		{
			DataAdapter dataAdapter = new DataAdapter();
			List<SP_PCR_LOOK_UP> locationListResult = await dataAdapter.GetLocationWithAddress();
			object o = locationListResult.Select(i => new { CatID = i.Code, CatDesc = i.CodeDescription, RelatedField = i.ParentID }).ToList();
			return o;
		}

		protected async Task<object> GetPlannerNames()
		{
			DataAdapter dataAdapter = new DataAdapter();
			List<SP_PCR_LOOK_UP> plannerNamesResult = await dataAdapter.GetPlannerNames(objClsLoginInfo.UserName);
			object o = plannerNamesResult.Select(i => new { CatID = i.Code, Desc = i.Description, CatDesc = i.CodeDescription }).ToList();
			return o;
		}

		public double? roundOffValue(double? value, int places)
        {
			if (value.HasValue)
			{
				value = Math.Round((double)value, places);
			}
			return value;
		}
		public enum ActionType
		{
			[StringValue("Add Or Edit Rows")]
			AddOrEditRows = 0,
			[StringValue("Before Re Write")]
			BeforeReWrite = 1,
			[StringValue("Send An Email")]
			SendAnEmail = 2
		}
		public enum SendEmailForActionType
        {
			[StringValue("Return Plate Cutting Request")]
			ReturnPlateCuttingRequest = 0,
		}
		protected class ActionResponse
        {
			public bool IsInformation;
			public bool Key;
			public string Value;
		}

		public async Task<ActionOutput> SendAnEmail<T>(int rowNumber, string mailTemplateName, string remarks, List<T> values, List<string> emailTo, List<string> emailCc)
		{
			clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
			ActionOutput output = new ActionOutput();
			try
			{
				DateTime? actionon = DateTime.Now;
				string emailfrom = "";

				List<string> lstrole = objClsLoginInfo.GetUserRoleList();

				emailfrom = objClsLoginInfo.UserName;
				#region Send Mail
				//if (!string.IsNullOrWhiteSpace("rahulsonwanshi3@gmail.com"))
				//{
				//	foreach (var person in objPCR001.SendEmailTo.Split(','))
				//	{
				//		emailTo.Add(Manager.GetMailIdFromPsNo(person));
				//	}
				//}
				emailTo.Add("rahulsonwanshi3@gmail.com");
				emailTo.Add("ANUBHAV_BANSAL.Contract@larsentoubro.com");
				//values
				//if (!string.IsNullOrWhiteSpace(objPCR001.CreatedBy))
				//{
				//	foreach (var person in objPCR001.CreatedBy.Split(','))
				//	{
				//		emailCc.Add(Manager.GetMailIdFromPsNo(person));
				//	}
				//}

				if (emailTo.Count > 0)
				{
					try
					{
						Hashtable _ht = new Hashtable();
						EmailSend _objEmail = new EmailSend();
						_ht["[FromEmail]"] = Manager.GetUserNameFromPsNo(emailfrom);
						_ht["[Remarks]"] = remarks; //This still needs to be refined
						_ht["[ReturnDate]"] = actionon;

						var properties = typeof(T).GetProperties();
                        foreach (var item in values)
                        {
                            foreach (var property in properties)
                            {
                                var name = property.Name;
                                var value = property.GetValue(item, null);
								_ht["[" + name + "]"] = value;
							}
                        }


                        MAIL001 objTemplateMaster = new MAIL001();
						objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == mailTemplateName).SingleOrDefault();
						_objEmail.MailToAdd = string.Join(",", emailTo.Distinct());
						_objEmail.MailCc = string.Join(",", emailCc.Distinct());
						_objEmail.SendMail(_objEmail, _ht, objTemplateMaster);

						output.Result = OutputType.Success;
						output.Message = EmailSubmittedSuccess;
					}
					catch (Exception ex)
					{
						output.Result = OutputType.Error;
						output.Message = ex.Message;
					}
				}
				#endregion

			}
			catch (Exception ex)
			{
				output.Result = OutputType.Error;
				output.Message = ex.Message;
			}
			return output;
		}

	}
}