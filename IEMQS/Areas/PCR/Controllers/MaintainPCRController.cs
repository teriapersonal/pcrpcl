﻿using IEMQS.Models;
using IEMQSImplementation;
using IEMQSImplementation.PCR;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQS.ILN.Framework;
using System.Threading.Tasks;
using Newtonsoft.Json;
using IEMQS.ILN.Models;
using Newtonsoft.Json.Converters;

namespace IEMQS.Areas.PCR.Controllers
{
    public class MaintainPCRController : CommonController
    {   
        
        #region Generate PCR
        
        [SessionExpireFilter, UserPermissions, LastURLHistory]
        public ActionResult GeneratePCR()
        {
            bool IsValidUser = CheckUserIsValid(GeneratePCRAction);
            if (IsValidUser)
            {
                ViewBag.TabName = "GeneratePCR";
                return View("Index");
            }
            else
                return new RedirectResult("~/Authentication/Authenticate/AccessDenied");
        }        

        [SessionExpireFilter]
        public async Task<PartialViewResult> _GeneratePCR()
        {
            bool IsValidUser = CheckUserIsValid(GeneratePCRAction);
            
            ViewBag.Location = await GetLocationWithAddress(); 
            ViewBag.WorkCenterDeliver = await GetWorkCenterWithLocation(1);
            ViewBag.CuttingLocation = db.SP_GENERAL_GET_CODE_DESCRIPTION_FROM_LN_TABLE("tltsfc513", "t_loca", "t_cloc").ToList().Select(i => new { CatID = i.Code, CatDesc = i.Description }).ToList();
            ViewBag.PCRType = clsImplementationEnum.getPCRType();
            return PartialView("_GeneratePCR");
        }

        public PartialViewResult _GeneratePCRSingleOccurrence()
        {
            bool IsValidUser = CheckUserIsValid(GeneratePCRAction);
            ViewBag.PCRIsValidUser = IsValidUser; //not sure if it is used...
            return PartialView("_GeneratePCRSingleOccurrence");
        }

        [SessionExpireFilter]
        public async Task<ActionResult> LoadPCRDataTable(JQueryDataTableParamModel param, string status)
        {

            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";
                string project = Request["Project"].ToString();
                projectVar = Request["Project"].ToString();

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lst = await (new DataAdapter().GetPCRData(project, strSortOrder, StartIndex, EndIndex));
               

                var res = new List<dynamic>();
                foreach (var h in lst)
                {

                    res.Add(new
                    {
                        Id = Convert.ToString(h.Id),                        
                        h.Element,
                        ManufacturingItem = h.ManufacturingItem + " - " + h.ManufacturingItemDesc,
                        BudgetLine = Convert.ToString(h.PartNumber),
                        Sequence = Convert.ToString(h.Sequence),
                        FindNo = Convert.ToString(h.FindNo),
                        h.ChildItemSegment,
                        h.ChildItem,
                        h.ChildItemDesc,
                        Length = Convert.ToString(h.Length),
                        Width = Convert.ToString(h.Width),
                        LengthModified = GetLengthModifiedColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.LengthModified, h.PCRTypeName),
                        WidthModified = GetWidthModifiedColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.WidthModified, h.PCRTypeName),
                        NoOfPieces = Convert.ToString(h.NoOfPieces),
                        RequiredQuantity = GetRequiredQuantityColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.RequiredQuantity),
                        ItemMaterialGrade = h.ItemMaterialGrade + " - " + h.ItemMaterialGradeName,
                        PCRNumber = Convert.ToString(h.PCRNumber),
                        PCRLineNo = Convert.ToString(h.PCRLineNo),
                        PCRType = GetPCRTypeColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.PCRTypeName, h.PCRType),
                        PCRRequirementDate = GetPCRRequirementDateColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.PCRRequirementDate),
                        WCDeliver = GetWCDeliverColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.WCDeliverName, h.WCDeliver),
                        PlannerRemark = GetPlannerRemarkColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.PlannerRemark),
                        CuttingLocation = GetCuttingLocationColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.CuttingLocation),
                        Priority = GetPriorityColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.Priority),
                        Location = GetLocationColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.LocationName, h.Location),
                        h.ItemRevision,
                        SubLocation = GetSubLocationColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.SubLocation),
                        h.PCRTypeName,
                        h.WCDeliverName,
                        Thickness = Convert.ToString(h.Thickness)
                    });
                }

                int? totalRecords = lst.Count();
                return Json(new
                {
                    param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder,
                    whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = string.Empty
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public new enum ActionType
        {
            [StringValue("Fetch Data")]
            FetchData = 0,
            [StringValue("Generate Plate Cutting Request")]
            GeneratePlateCuttingRequest = 1,
            [StringValue("Validate Generate PCR Budget Lines")]
            ValidateGeneratePCRBudgetLines = 2,
            [StringValue("Add Or Edit Rows")]
            AddOrEditRows = 3,
            [StringValue("Retract Plate Cutting Request")]
            RetractPlateCuttingRequest = 4
        }
        
        public override async Task<ActionResult> ExecuteAction(string input)
        {
            ActionResponse actionResponse = new ActionResponse();
            try
            {
                bool IsValidUser = CheckUserIsValid(GeneratePCRAction);
                if (!IsValidUser)
                {
                    actionResponse.Key = false;
                    actionResponse.IsInformation = true;
                    actionResponse.Value = UnAthorizedForUpdateData;
                    return Json(actionResponse, JsonRequestBehavior.AllowGet);
                }
                ActionInput actionInput = JsonConvert.DeserializeObject<ActionInput>(input);
                Dictionary<string, object> dict = JsonConvert.DeserializeObject<Dictionary<string, object>>(actionInput.Parameters.ToString());
                ActionOutput actionOutput = new ActionOutput();
                DataAdapter dataAdapter = new DataAdapter();
                ActionType actionType = (ActionType)Enum.Parse(typeof(ActionType), actionInput.ActionName);
                switch (actionType)
                {
                    case ActionType.FetchData:
                        {
                            string projectValue = Convert.ToString(dict["t_cprj"]);
                            actionOutput = await dataAdapter.FetchData(projectValue, objClsLoginInfo.UserName);

                            break;
                        }
                    case ActionType.GeneratePlateCuttingRequest:
                        {
                            string Ids = Convert.ToString(dict["Ids"]);
                            string seperator = Convert.ToString(dict["Seperator"]);
                            actionOutput = await dataAdapter.GeneratePlateCuttingRequest(Ids, seperator, objClsLoginInfo.UserName);
                            break;
                        }
                    case ActionType.RetractPlateCuttingRequest:
                        {
                            int Id = Convert.ToInt32(dict["Id"]);
                            string project = Convert.ToString(dict["Project"]);
                            string element = Convert.ToString(dict["Element"]);
                            //int budgetLineId = Convert.ToInt32(dict["BudgetLineId"]);
                            actionOutput = await dataAdapter.RetractPlateCuttingRequest(Id, project, element, objClsLoginInfo.UserName);
                            break;
                        }
                    case ActionType.ValidateGeneratePCRBudgetLines:
                        {
                            string projectIds = Convert.ToString(dict["ProjectIds"]);
                            string elementIds = Convert.ToString(dict["ElementIds"]);
                            string budgetLineIds = Convert.ToString(dict["BudgetLineIds"]);
                            string childItemIds = Convert.ToString(dict["ChildItemIds"]);
                            string seperator = Convert.ToString(dict["Seperator"]);
                            actionOutput = await dataAdapter.ValidateGeneratePCRBudgetLines(projectIds, elementIds, budgetLineIds, childItemIds, seperator);
                            break;
                        }                   
                }
                actionResponse.Value = actionOutput.Message;
                switch (actionOutput.Result)
                {
                    case OutputType.Success:
                        {
                            actionResponse.Key = true;
                            break;
                        }
                    case OutputType.Warning:
                        {
                            actionResponse.Key = false;
                            actionResponse.IsInformation = true;
                            break;
                        }
                    case OutputType.Error:
                        {
                            actionResponse.Key = false;                            
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                actionResponse.Key = false;
                actionResponse.Value = ex.Message;
            }
            return Json(actionResponse);
        }

        #endregion
        public async Task<ActionResult> GetProjectList()
        {
            DataAdapter dataAdapter = new DataAdapter();
            List<IDictionary<string, object>> projectList = await dataAdapter.GetProjectList(objClsLoginInfo.UserName);
            object tableData = JsonConvert.SerializeObject(projectList);
            return Json(tableData, JsonRequestBehavior.AllowGet);
        }

        #region Column Generate Function       

        #region Generate PCR
        private string GetLengthModifiedColumnDetails(string PCRNumber, int Id, double? LengthModified, string PCRTypeName)
        {
            bool isDisabled;
            if (!string.IsNullOrEmpty(PCRNumber))
            {
                isDisabled = true;                
            }
            else
            {
                
                if (PCRTypeName == clsImplementationEnum.PCRType.ModifiedCircle.GetStringValue() ||
                        PCRTypeName == clsImplementationEnum.PCRType.ModifiedRectangle.GetStringValue() ||
                        PCRTypeName == clsImplementationEnum.PCRType.ModifiedRing.GetStringValue() ||
                        PCRTypeName == clsImplementationEnum.PCRType.ModifiedRingSegment.GetStringValue())
                {                    
                    isDisabled = false;
                }
                else
                {
                    isDisabled = true;                    
                }
            }
            
            return Helper.GenerateTextbox(Id, "LengthModified", 
                (LengthModified.HasValue ? LengthModified.Value.ToString() : string.Empty), string.Empty, false, 
                BorderNoneStyle, "18", "form-control numeric inputField", isDisabled);
        }
        private string GetWidthModifiedColumnDetails(string PCRNumber, int Id, double? WidthModified, string PCRTypeName)
        {
            bool isDisabled;
            if (!string.IsNullOrEmpty(PCRNumber))
            {
                isDisabled = true;                
            }
            else
            {

                if (PCRTypeName == clsImplementationEnum.PCRType.ModifiedCircle.GetStringValue() ||
                        PCRTypeName == clsImplementationEnum.PCRType.ModifiedRectangle.GetStringValue() ||
                        PCRTypeName == clsImplementationEnum.PCRType.ModifiedRing.GetStringValue() ||
                        PCRTypeName == clsImplementationEnum.PCRType.ModifiedRingSegment.GetStringValue())
                {                    
                    isDisabled = false;
                }
                else
                {
                    isDisabled = true;                    
                }
            }
            return Helper.GenerateTextbox(Id, "WidthModified", 
                (WidthModified.HasValue ? WidthModified.Value.ToString() : string.Empty), string.Empty, false, 
                BorderNoneStyle, "18", "form-control numeric inputField", isDisabled);
        }
        private string GetPCRRequirementDateColumnDetails(string PCRNumber, int Id,  DateTime? PCRRequirementDate)
        {            
            var isDisabled = false;
            if (!string.IsNullOrEmpty(PCRNumber))
            {
                isDisabled = true;                
            }            
            var PCRDate = PCRRequirementDate.HasValue ? PCRRequirementDate.Value.ToString("dd/MM/yyyy") : string.Empty;
            return Helper.GenerateHTMLDateControl(Convert.ToInt32(Id), "PCRRequirementDate", PCRDate, isDisabled, 
                "datecontrol", "inputField", BorderNoneStyle, string.Empty, true);
        }
        private string GetRequiredQuantityColumnDetails(string PCRNumber, int Id, double? RequiredQuantity)
        {
            var isDisabled = false;
            if (!string.IsNullOrEmpty(PCRNumber))
            {
                isDisabled = true;
            }
            return Helper.GenerateTextbox(
                    Convert.ToInt32(Id), "RequiredQuantity",
                                            (RequiredQuantity.HasValue ? RequiredQuantity.Value.ToString() : string.Empty),
                                            string.Empty, false, BorderNoneStyle, "18", "form-control requiredquantity inputField", isDisabled);
        }
        private string GetWCDeliverColumnDetails(string PCRNumber, int Id, string WCDeliverName, string WCDeliver)
        {
            var isDisabled = false;
            if (!string.IsNullOrEmpty(PCRNumber))
            {
                isDisabled = true;
            }
            return Helper.HTMLAutoComplete(
                Convert.ToInt32(Id), "WCDeliver", Convert.ToString(WCDeliverName), string.Empty, false, BorderNoneStyle, string.Empty,
                isDisabled, "100", string.Empty, "W/C Deliver", "form-control wcdeliver inputField"); ;
        }
        private string GetPCRTypeColumnDetails(string PCRNumber, int Id, string PCRTypeName, string PCRType)
        {
            var isDisabled = false;
            if (!string.IsNullOrEmpty(PCRNumber))
            {
                isDisabled = true;
            }
            return Helper.HTMLAutoComplete(
                Convert.ToInt32(Id), "PCRType", Convert.ToString(PCRTypeName), string.Empty, false, BorderNoneStyle, string.Empty, 
                isDisabled, "100", string.Empty, "PCRType", "form-control pcrtype inputField");
        }
        private string GetPlannerRemarkColumnDetails(string PCRNumber, int Id, string PlannerRemark)
        {
            var isDisabled = false;
            if (!string.IsNullOrEmpty(PCRNumber))
            {
                isDisabled = true;
            }
            return Helper.GenerateTextbox(
                Convert.ToInt32(Id), "PlannerRemark", Convert.ToString(PlannerRemark), string.Empty, false, 
                BorderNoneStyle, "100", "form-control plannerremark inputField", isDisabled);
        }
        private string GetCuttingLocationColumnDetails(string PCRNumber, int Id, string CuttingLocation)
        {
            var isDisabled = false;
            if (!string.IsNullOrEmpty(PCRNumber))
            {
                isDisabled = true;
            }
            return Helper.HTMLAutoComplete(
                Convert.ToInt32(Id), "CuttingLocation", Convert.ToString(CuttingLocation), string.Empty, false, BorderNoneStyle,string.Empty, 
                isDisabled, "100", string.Empty, "Cutting Location", "form-control cuttinglocation inputField");
        }
        private string GetLocationColumnDetails(string PCRNumber, int Id, string LocationName, string Location)
        {
            var isDisabled = false;
            if (!string.IsNullOrEmpty(PCRNumber))
            {
                isDisabled = true;
            }
            return Helper.HTMLAutoComplete(
                Convert.ToInt32(Id), "Location", Convert.ToString(LocationName), string.Empty, false, BorderNoneStyle, string.Empty,
                isDisabled, "100", string.Empty, "Location", "form-control location inputField");
        }
        private string GetSubLocationColumnDetails(string PCRNumber, int Id, string SubLocation)
        {
            var isDisabled = false;
            if (!string.IsNullOrEmpty(PCRNumber))
            {
                isDisabled = true;
            }
            return Helper.GenerateTextbox(
                Convert.ToInt32(Id), "SubLocation", Convert.ToString(SubLocation), string.Empty, 
                false, BorderNoneStyle, "40", "form-control sublocation inputField", isDisabled);
        }
        private string GetPriorityColumnDetails(string PCRNumber, int Id, double? Priority)
        {
            var isDisabled = false;
            if (!string.IsNullOrEmpty(PCRNumber))
            {
                isDisabled = true;
            }
            return Helper.GenerateTextbox(
                Convert.ToInt32(Id), "Priority", (Priority.HasValue ? Convert.ToString(Priority.Value) : string.Empty), 
                string.Empty, false, BorderNoneStyle, 
                "9", "form-control priority inputField", isDisabled);
        }

                
        private bool IsEnableElementSelection(string PCRNumber, double? RequiredQuantity, DateTime? PCRRequirementDate, string Location, string WCDeliverName, string CuttingLocation, double? Priority)
        {
            if ( RequiredQuantity.HasValue &&
                PCRRequirementDate.HasValue &&
                !string.IsNullOrEmpty(Location) &&
                !string.IsNullOrEmpty(WCDeliverName) &&
                !string.IsNullOrEmpty(CuttingLocation) &&
                Priority.HasValue &&
                string.IsNullOrEmpty(PCRNumber)
              )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
        #endregion

        #endregion

        #region Common Functions
                
        [HttpPost]
        public ActionResult GetActiveEmployee(string search = "", string param = "")
        {
            var lstEmployee = Manager.GetActiveEmployee();
            List<ddlValue> lst = new List<ddlValue>();
            lst = lstEmployee.Select(x => new ddlValue { id = x.id.ToString(), text = x.text.ToString() }).ToList();
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }    
}