﻿using IEMQS.Models;
using IEMQSImplementation;
using IEMQSImplementation.PCR;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQS.Areas.PCR.Controllers.MaintainStockController;
using IEMQS.ILN.Framework;
using System.Threading.Tasks;
using Newtonsoft.Json;
using IEMQS.ILN.Models;
using Newtonsoft.Json.Converters;
using System.Reflection;

namespace IEMQS.Areas.PCR.Controllers
{
    public class PCLWorkflowController : CommonController
    {
        private const string PLTType = "PLT";

        public ActionResult FirstStageClearance()
        {
            bool IsValidUser = CheckUserIsValid(PCLWorkflowAction);
            if (IsValidUser)
            {
                ViewBag.Role = "PLNG3";
                ViewBag.TabName = "PCL WorkFlow";
                ViewBag.ScreenName = clsImplementationEnum.Callers.FirstStageClearance.GetStringValue();
                return View("Index");
            }
            else
                return new RedirectResult("~/Authentication/Authenticate/AccessDenied");
        }
        
        public ActionResult ConfirmPlateCuttingRequest()
        {
            bool IsValidUser = CheckUserIsValid(PCLWorkflowAction);
            if (IsValidUser)
            {
                ViewBag.Role = "PLNG3";
                ViewBag.TabName = "PCL WorkFlow";
                ViewBag.ScreenName = clsImplementationEnum.Callers.ConfirmPlateCuttingRequest.GetStringValue();
                return View("Index");
            }
            else
                return new RedirectResult("~/Authentication/Authenticate/AccessDenied");
        }

        public ActionResult ReceivePlate()
        {
            bool IsValidUser = CheckUserIsValid(PCLWorkflowAction);
            if (IsValidUser)
            {
                ViewBag.Role = "PLNG3";
                ViewBag.TabName = "PCL WorkFlow";
                ViewBag.ScreenName = clsImplementationEnum.Callers.ReceivePlate.GetStringValue();
                return View("Index");
            }
            else
                return new RedirectResult("~/Authentication/Authenticate/AccessDenied");
        }

        public ActionResult ProcessPlateCuttingRequest()
        {
            bool IsValidUser = CheckUserIsValid(PCLWorkflowAction);
            if (IsValidUser)
            {
                ViewBag.Role = "PLNG3";
                ViewBag.TabName = "PCL WorkFlow";
                ViewBag.ScreenName = clsImplementationEnum.Callers.ProcessPlateCuttingRequest.GetStringValue();
                return View("Index");
            }
            else
                return new RedirectResult("~/Authentication/Authenticate/AccessDenied");
        }

        public ActionResult SecondStageClearance()
        {
            bool IsValidUser = CheckUserIsValid(PCLWorkflowAction);
            if (IsValidUser)
            {
                ViewBag.Role = "PLNG3";
                ViewBag.TabName = "PCL WorkFlow";
                ViewBag.ScreenName = clsImplementationEnum.Callers.SecondStageClearance.GetStringValue();
                return View("Index");
            }
            else
                return new RedirectResult("~/Authentication/Authenticate/AccessDenied");
        }

        public async Task<PartialViewResult> _PCLWorkFlow(string screenName)
        {
            ViewBag.All = await GetAllAsync(screenName);
            return PartialView("_PCLWorkFlow");
        }

        public async Task<JsonResult> RefreshAll(string screenName)
        {
            dynamic r = await GetAllAsync(screenName);
            return Json(r);
        }

        private async Task<dynamic> GetAllAsync(string screenName)
        {
            screenNameVar = screenName;
            dynamic all = new ExpandoObject();
            List<dynamic> columns = new List<dynamic>();
            List<dynamic> columnConfigs = new List<dynamic>();
            GetColumnConfigObject(ref columns, ref columnConfigs, screenName);
            all.MultiOccurenceTableData = await LoadPCLWorkflowData(screenName);
            all.PCLEnumData = await GetEnumData();
            all.MultiOccurenceColumns = columns;
            
            all.ColumnsConfigs = columnConfigs;
            return all;

        }

        private void GetColumnConfigObject(ref List<dynamic> columns, ref List<dynamic> columnConfigs,string screenName)
        {
            PropertyInfo[] propertyInfos = typeof(PCL_WORKFLOW_Result.Columns).GetProperties(); //.Select(x => x.Name).ToList();
            PropertyInfo[] actionPropertyInfos = typeof(PCL_WORKFLOW_Result.ActionRender).GetProperties(); //.Select(x => x.Name).ToList();
            int Index = 0;// You can also add custom column Names

            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                ColumnAttribute attribute = (ColumnAttribute)propertyInfo.GetCustomAttributes(typeof(ColumnAttribute), false).FirstOrDefault();
                if (attribute != null &&
                    (attribute.screenNames.Contains(0) || 
                    attribute.screenNames.Contains((int)Enum.Parse(typeof(clsImplementationEnum.Callers), screenName.Replace(" ", "")))))
                {
                    dynamic columnConfig = new ExpandoObject();
                    columnConfig.NonSearchable = attribute.IsNonSearchable;
                    columnConfig.NonSortable = attribute.IsNonSortable;
                    columnConfig.Invisible = attribute.IsInVisible;
                    columnConfig.Type = attribute.Type;
                    columnConfig.ColName = propertyInfo.Name;
                    columnConfig.Index = Index;

                    columnConfigs.Add(columnConfig);
                    dynamic column = new ExpandoObject();
                    column.title = attribute.columnName;
                    column.data = propertyInfo.Name;
                    columns.Add(column);
                    Index++;
                }

            }

            //This needs to be fixed as well
            foreach (PropertyInfo propertyInfo in actionPropertyInfos)
            {
                ActionColumnAttribute attribute = (ActionColumnAttribute)propertyInfo.GetCustomAttributes(typeof(ActionColumnAttribute), false).FirstOrDefault();
                if (attribute != null &&
                    (attribute.screenNames.Contains(0) ||
                    attribute.screenNames.Contains((int)Enum.Parse(typeof(clsImplementationEnum.Callers), screenName.Replace(" ", "")))))
                {
                    dynamic columnConfig = new ExpandoObject();
                    columnConfig.NonSearchable = attribute.IsNonSearchable;
                    columnConfig.NonSortable = attribute.IsNonSortable;
                    columnConfig.Invisible = attribute.IsInVisible;
                    columnConfig.Type = attribute.Type;
                    columnConfig.ColName = propertyInfo.Name;
                    columnConfig.IsApproveAction = attribute.IsApproveAction;
                    columnConfig.IsRejectAction = attribute.IsRejectAction;
                    columnConfig.Index = Index;

                    columnConfigs.Add(columnConfig);
                    dynamic column = new ExpandoObject();
                    column.title = attribute.columnName;
                    column.data = propertyInfo.Name;
                    columns.Add(column);
                    Index++;
                }

            }
        }

        public new enum ActionType
        {

            [StringValue("Approve Or Reject Row")]
            ApproveOrRejectRow = 0,
            [StringValue("Modify PCL")]
            ModifyPCL = 1,

        }

        public override async Task<ActionResult> ExecuteAction(string input)
        {
            StockResponseMsg objResponseMsg = new StockResponseMsg();
            try
            {
                bool IsValidUser = CheckUserIsValid(PCLWorkflowAction);
                if (!IsValidUser)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.IsInformation = true;
                    objResponseMsg.Value = UnAthorizedForUpdateData;
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                //left the parameters for now as it is not clear whether the fields are editable or not
                ActionInput actionInput = JsonConvert.DeserializeObject<ActionInput>(input);
                Dictionary<string, object> dict = JsonConvert.DeserializeObject<Dictionary<string, object>>(actionInput.Parameters.ToString());
                ActionOutput actionOutput = new ActionOutput();
                DataAdapter dataAdapter = new DataAdapter();
                ActionType actionType = (ActionType)Enum.Parse(typeof(ActionType), actionInput.ActionName);
                switch (actionType)
                {
                    case ActionType.ApproveOrRejectRow:
                        {
                            int Id = Convert.ToInt32(dict["Id"]);
                            bool IsApprove = Convert.ToBoolean(dict["IsApprove"]);
                            string CurrentScreen = Convert.ToString(dict["CurrentScreen"]);
                            actionOutput = await dataAdapter.ApproveOrRejectRow(objClsLoginInfo.UserName, Id, CurrentScreen, IsApprove);

                            break;
                        }
                }
                objResponseMsg.Value = actionOutput.Message;
                switch (actionOutput.Result)
                {
                    case OutputType.Success:
                        {
                            objResponseMsg.Key = true;
                            break;
                        }
                    case OutputType.Warning:
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.IsInformation = true;
                            break;
                        }
                    case OutputType.Error:
                        {
                            objResponseMsg.Key = false;
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg);
        }

        private async Task<List<PCL_WORKFLOW_Result.Columns>> LoadPCLWorkflowData(string screenName)
        {

            try
            {
                var lstData = await (new DataAdapter().GetPCLWorkflowData(objClsLoginInfo.UserName, screenName));
                return lstData;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }
    }
}