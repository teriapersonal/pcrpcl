﻿using System.Web.Mvc;

namespace IEMQS.Areas.COP
{
    public class COPAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "COP";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "COP_default",
                "COP/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}