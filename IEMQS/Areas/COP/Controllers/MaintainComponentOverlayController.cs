﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace IEMQS.Areas.COP.Controllers
{
    [SessionExpireFilter]
    public class MaintainComponentOverlayController : clsBase
    {
        // GET: COP/MaintainComponentOverlay
        [UserPermissions, SessionExpireFilter]
        public ActionResult IndexOPLNG()
        {

            ViewBag.Title = "Component Overlay Plan";
            BindDropDown(null);
            var objCOP001 = new COP001();
            objCOP001.Start = "NS";
            objCOP001.BM_NDT = "NS";
            objCOP001.BL_OL = "NS";
            objCOP001.BL_PT = "NS";
            objCOP001.OL = "NS";
            objCOP001.GF_BL_PT = "NS";
            objCOP001.GF_OL = "NS";
            objCOP001.M_Cing = "NS";
            objCOP001.NDT = "NS";
            objCOP001.ICS = "NS";
            objCOP001.DISP = "No";
            objCOP001.PWHT = "NA";
            objCOP001.AfterPWHTDispatch = "NA";
            ViewBag.Role = "oplng";
            ViewBag.COPDashboardURL = GetCOPDashboardLink();
            return View("Index", objCOP001);
        }

        [UserPermissions, SessionExpireFilter]
        public ActionResult IndexJPLNG()
        {

            ViewBag.Title = "Component Overlay Plan";
            BindDropDown(null);
            var objCOP001 = new COP001();
            objCOP001.Start = "NS";
            objCOP001.BM_NDT = "NS";
            objCOP001.BL_OL = "NS";
            objCOP001.BL_PT = "NS";
            objCOP001.OL = "NS";
            objCOP001.GF_BL_PT = "NS";
            objCOP001.GF_OL = "NS";
            objCOP001.M_Cing = "NS";
            objCOP001.NDT = "NS";
            objCOP001.ICS = "NS";
            objCOP001.DISP = "No";
            objCOP001.PWHT = "NA";
            objCOP001.AfterPWHTDispatch = "NA";
            ViewBag.Role = "jplng";
            ViewBag.COPDashboardURL = GetCOPDashboardLink();
            return View("Index", objCOP001);
        }

        [UserPermissions, SessionExpireFilter]
        public ActionResult IndexOPROD()
        {

            ViewBag.Title = "Component Overlay Plan";
            BindDropDown(null);
            var objCOP001 = new COP001();
            objCOP001.Start = "NS";
            objCOP001.BM_NDT = "NS";
            objCOP001.BL_OL = "NS";
            objCOP001.BL_PT = "NS";
            objCOP001.OL = "NS";
            objCOP001.GF_BL_PT = "NS";
            objCOP001.GF_OL = "NS";
            objCOP001.M_Cing = "NS";
            objCOP001.NDT = "NS";
            objCOP001.ICS = "NS";
            objCOP001.DISP = "No";
            objCOP001.PWHT = "NA";
            objCOP001.AfterPWHTDispatch = "NA";
            ViewBag.Role = "oprod";
            ViewBag.COPDashboardURL = GetCOPDashboardLink();
            return View("Index", objCOP001);
        }

        [UserPermissions, SessionExpireFilter]
        public ActionResult DisplayCOP()
        {

            ViewBag.Title = "Component Overlay Plan";
            BindDropDown(null);
            ViewBag.IsDisplay = true;
            ViewBag.COPDashboardURL = GetCOPDashboardLink();
            return View("Index", new COP001());
        }

        [HttpPost]
        public ActionResult LoadDataGridPartial(string status, string role, bool isDisplay)
        {
            ViewBag.Status = status;
            ViewBag.Role = role;
            ViewBag.IsDisplay = isDisplay;
            //ViewBag.MachineCode = db.COP002.Select(x => new { label = x.MachineCode + "-" + x.MachineName, id = x.MachineCode, value = x.CameraURL }).Distinct().ToList();

            BindDropDown(null);
            return PartialView("_GetGridDataPartial");
        }

        [HttpPost]
        public ActionResult LoadDataGridData(JQueryDataTableParamModel param, string dispatchStatus)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                strWhereCondition += "1=1 ";

                #endregion

                //search Condition 

                string[] columnName = { "Project","BU","Mark","ItemId","Overlay","MOC","ID_BlankDia","Length","Part","Description",
                    "ID_Weld_Height","GF_OD","GF_Weld_Height","BF_OD","BF_Weld_Height","Groove_On_GF","ID_Weld_KG","WP_For_ID",
                    "Face_Weld_KG","WP_For_Face","Groove_Weld_KG","WP_For_Groove","Back_Face_Weld_KG","WP_For_BackFace","Total_Weld_KG",
                    "Procss","Comp_Assly","TypeOfComponent","PlannedReceipt","RequestedOn","IMRStatus","SeamStatus","Cont","Start","BM_NDT",
                    "BL_OL","BL_PT","OL","GF_BL_PT","GF_OL","M_Cing","NDT","ICS","DISP","Category","CycleTime","WeekNo","Sc","PONo","deliveryLocation",
                    "POLine","cm006.t_nama","Remarks","MachiningLocation","MCDatetime","MCingDays","QualityProject","[COP001].MachineCode","cop002.MachineName"
                };
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhereCondition += MakeDatatableForCOPSearch(param.SearchFilter);
                }

                if (!string.IsNullOrEmpty(dispatchStatus))
                {
                    strWhereCondition += " AND DISP = '" + dispatchStatus + "'";
                }

                var lstResult = db.SP_COP_GET_INDEX_DATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();
                var dtPlannedReceipt = new DateTime();
                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.HeaderId),
                                uc.Project,
                                uc.QualityProject,
                                uc.Customer,
                                uc.BU,
                                uc.ItemId,
                                uc.Description,
                                uc.Mark,
                                uc.TypeOfComponent,
                                GenerateCheckbox(uc.HeaderId,"WIP",uc.WIP,true),
                                GenerateCheckbox(uc.HeaderId,"IsPlanned",uc.IsPlanned,true),
                                (uc.Overlay != null ? (uc.Overlay == true ? "Yes":"No") : "" ),
                                uc.MOC,
                                Convert.ToString(uc.ID_BlankDia),
                                Convert.ToString(uc.Category),
                                Convert.ToString(uc.Length),
                                Convert.ToString(uc.ID_Weld_Height),
                                Convert.ToString(uc.GF_OD),
                                Convert.ToString(uc.GF_Weld_Height),
                                Convert.ToString(uc.BF_OD),
                                Convert.ToString(uc.BF_Weld_Height),
                                Convert.ToString(uc.Groove_On_GF),
                                Convert.ToString(uc.ID_Weld_KG),
                                Convert.ToString(uc.WP_For_ID),
                                Convert.ToString(uc.Face_Weld_KG),
                                Convert.ToString(uc.WP_For_Face),
                                Convert.ToString(uc.Groove_Weld_KG),
                                Convert.ToString(uc.WP_For_Groove),
                                Convert.ToString(uc.Back_Face_Weld_KG),
                                Convert.ToString(uc.WP_For_BackFace),
                                Convert.ToString(uc.Total_Weld_KG),
                                uc.Part,
                                uc.Procss,
                                uc.Comp_Assly,
                                (DateTime.TryParse(uc.PlannedReceipt+"",out dtPlannedReceipt) ? dtPlannedReceipt.ToString("dd/MM/yyyy") : uc.PlannedReceipt),
                                uc.IMRStatus,
                                uc.SeamStatus,
                                uc.RequestedOn.HasValue? uc.RequestedOn.Value.ToString("dd/MM/yyyy"):"",
                                Convert.ToString(uc.CycleTime),
                                uc.FromDate.HasValue ? "Week " + Convert.ToString(uc.WeekNo) +" ["+ uc.FromDate.Value.ToShortDateString() +" to "+uc.ToDate.Value.ToShortDateString() +"]" :"",
                                uc.Start,
                                uc.BM_NDT,
                                uc.BL_OL,
                                uc.BL_PT,
                                uc.OL,
                                uc.GF_BL_PT,
                                uc.GF_OL,
                                uc.M_Cing,
                                uc.NDT,
                                uc.ICS,
                                uc.DISP,
                                (uc.DispatchDate.HasValue? uc.DispatchDate.Value.ToString("dd/MM/yyyy"): ""),
                                uc.deliveryLocation,
                                uc.PWHT,
                                uc. AfterPWHTDispatch,
                                uc.HasAttachment?"Yes":"",
                                uc.Remarks,
                                uc.MachiningLocation,
                                (uc.MCDatetime.HasValue? uc.MCDatetime.Value.ToString("dd/MM/yyyy"): ""),
                                //(uc.MCTime.HasValue? uc.MCTime.Value.ToString("hh:mm:tt"): ""),
                                (uc.MCingDays.HasValue? uc.MCingDays.Value.ToString()+" Days": ""),
                                Convert.ToString(uc.MachineName),
                                Convert.ToString(uc.MachineCode),
                                Convert.ToString(uc.CameraURL),
                                Convert.ToString(uc.DeleteRemarks),
                                "<span><a id='View" + uc.HeaderId + "' name='View' title='View' class='blue action "+(uc.HasAttachment == true ? "save" : "") + "' onclick='ViewRecord(" + uc.HeaderId + ")'>"+
                                        "<i class='fa fa-eye'></i></a> &nbsp &nbsp"+
                                        "<a id='Attachment" + uc.HeaderId + "' name='Attachment' title='Attachment' class='blue action "+(uc.HasAttachment == true ? "save" : "") + "' onclick='ViewAttachment(" + uc.HeaderId + ")'>"+
                                        "<i class='fa fa-paperclip'></i></a> &nbsp &nbsp"+
                                        "<a id=\"CameraURL\"" + uc.HeaderId + " name=\"CameraURL\"" + uc.HeaderId + " title=\"URL\" class=\"blue action\" onclick=\"RedirectCameraURL(this," + uc.HeaderId + ")\">"+
                                        "<i class=\"iconclass fa fa-camera\"></i></a> &nbsp &nbsp"+
                                        "<a id='QRcode" + uc.HeaderId + "' name='QRcode' title=\"QRcode\" class=\"blue action\" onclick=\"ViewQRcode(this," + uc.HeaderId +")\">"+
                                        "<i class=\"iconclass fa fa-qrcode\"></i></a> &nbsp &nbsp"+
                                        "<a id='Seam" + uc.HeaderId + "' name='Seam' title=\"Seam\" class=\"blue action\" onclick=\"Seamlist(this," + uc.HeaderId +")\">"+
                                        "<i class=\"iconclass fa fa-th\"></i></a>" +
                                "</span>",
                                uc.OldPart,
                                (uc.IsPartUpdated == null ? "false" : Convert.ToString(uc.IsPartUpdated)),

                          }).ToList();

                // data.Insert(0, null);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public string MakeDatatableForCOPSearch(string jsonSearchFilter)
        {
            string strWhereCondition = string.Empty;
            try
            {
                List<SearchFilter> searchflt = new JavaScriptSerializer().Deserialize<List<SearchFilter>>(jsonSearchFilter);
                foreach (var item in searchflt)
                {
                    if (!string.IsNullOrWhiteSpace(item.Value) && item.Value != "false")
                    {
                        if (item.ColumnName == "Overlay")
                        {
                            item.Value = item.Value == "Blank" ? null : (item.Value == "Yes" ? "1" : "0");
                            item.FilterType = item.Value == null ? "" : item.FilterType;
                        }
                        strWhereCondition += Manager.MakeDatatableSearchWithMultipleCondition(item.FilterType, item.ColumnName, item.Value, item.DataType);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
            return strWhereCondition;
        }
        public static string MachineCode(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string maxlength = "", string validate = "", string placeholder = "", string className = "")
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            className = "autocomplete form-control " + className;
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string FieldValidate = !string.IsNullOrEmpty(validate) ? "validate='" + validate + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onchange='" + onBlurMethod + "'" : "";
            string placeHolder = !string.IsNullOrEmpty(placeholder) ? "placeholder='" + placeholder + "'" : "";
            inputStyle = "min-width:100px!important;display:inline-block";
            strAutoComplete = "<input type=\"text\" " + (isReadOnly ? "readonly=\"readonly\"" : "") + " spara=\"" + rowId + "\" id=\"" + inputID + "\" hdElement=\"" + hdElementId + "\" value=\"" + inputValue + "\" colname=\"" + columnName + "\" name=\"" + inputID + "\" class=\"" + className + "\" style=\"" + inputStyle + "\"  " + onBlurEvent + " " + (disabled ? "disabled" : "") + " " + MaxCharcters + " " + FieldValidate + " " + placeHolder + " />";


            return strAutoComplete;
        }

        public ActionResult GetFormDataPartial(int HeaderId, string Role)
        {
            ViewBag.HeaderId = HeaderId;
            ViewBag.Role = Role;
            return PartialView("_FormDataPartial");
        }

        public JsonResult GetDetails(int HeaderId)
        {
            var objResponseMsg = new resCOP001();
            try
            {
                var Copdata = db.COP001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                if (Copdata != null)
                {
                    var result = new string[] { clsImplementationEnum.MachineLocation.VTLWest.GetStringValue()
                    , clsImplementationEnum.MachineLocation.TitanVTL.GetStringValue() , clsImplementationEnum.MachineLocation.KVTL.GetStringValue()
                    , clsImplementationEnum.MachineLocation.MKVTL.GetStringValue(), clsImplementationEnum.MachineLocation.L50.GetStringValue()
                    , clsImplementationEnum.MachineLocation.L45CNC.GetStringValue(), clsImplementationEnum.MachineLocation.L50Manual.GetStringValue()
                    , clsImplementationEnum.MachineLocation.Lathe.GetStringValue(), clsImplementationEnum.MachineLocation.Outsourced.GetStringValue()
                    , clsImplementationEnum.MachineLocation.NOTREQUIRED.GetStringValue()};

                    var items = (from li in result
                                 select new SelectItemList
                                 {
                                     id = li.ToString(),
                                     text = li.ToString()
                                 }).ToList();

                    var MachineCode = (from mls in db.COP002
                                       select new SelectItemList
                                       {
                                           id = mls.MachineCode.ToString(),
                                           text = mls.MachineCode.ToString()
                                       }).ToList();

                    objResponseMsg.maclocations = items;
                    objResponseMsg.machines = MachineCode;
                    objResponseMsg.D_RequestedOn = (Copdata.RequestedOn.HasValue ? Copdata.RequestedOn.Value.ToString("dd/MM/yyyy") : "");
                    objResponseMsg.D_DispatchDate = (Copdata.DispatchDate.HasValue ? Copdata.DispatchDate.Value.ToString("yyyy-MM-dd") : "");
                    objResponseMsg.D_MCDatetime = (Copdata.MCDatetime.HasValue ? Copdata.MCDatetime.Value.ToString("yyyy-MM-dd") : "");
                    objResponseMsg.objcop = Copdata;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "SuccessFully DeSelected All";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Record found!";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg);
        }

        [HttpPost]
        public JsonResult GetInlineEditableRow(int id, string role, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();

                var result = new string[] { clsImplementationEnum.MachineLocation.VTLWest.GetStringValue()
                    , clsImplementationEnum.MachineLocation.TitanVTL.GetStringValue() , clsImplementationEnum.MachineLocation.KVTL.GetStringValue()
                    , clsImplementationEnum.MachineLocation.MKVTL.GetStringValue(), clsImplementationEnum.MachineLocation.L50.GetStringValue()
                    , clsImplementationEnum.MachineLocation.L45CNC.GetStringValue(), clsImplementationEnum.MachineLocation.L50Manual.GetStringValue()
                    , clsImplementationEnum.MachineLocation.Lathe.GetStringValue(), clsImplementationEnum.MachineLocation.Outsourced.GetStringValue()
                    , clsImplementationEnum.MachineLocation.NOTREQUIRED.GetStringValue()};

                var items = (from li in result
                             select new SelectItemList
                             {
                                 id = li.ToString(),
                                 text = li.ToString()
                             }).ToList();

                var MachineCode = (from mls in db.COP002
                                   select new SelectItemList
                                   {
                                       id = mls.MachineCode.ToString(),
                                       text = mls.MachineCode.ToString()
                                   }).ToList();
                if (id == 0)
                {
                    var QPitems = new SelectList(new List<string>());
                    int newRecordId = 0;
                    var newRecord = new[] {
                                        "0",
                                        Helper.HTMLAutoComplete(newRecordId,"Project","","",false,"","",false,"","","Project","form-control"),
                                        Helper.GenerateDropdownWithSelected(newRecordId,"QualityProject",QPitems,""),
                                        Helper.GenerateTextbox(newRecordId, "Customer","", "",true, "","","form-control"),
                                        Helper.HTMLAutoComplete(newRecordId, "BU",  "", "", false, "", "",false,"","","BU","form-control"),
                                        Helper.HTMLAutoComplete(newRecordId,"ItemId","","",false,"","",false,"","","Item","form-control"),
                                        Helper.GenerateTextbox(newRecordId,"Description",  "", "",true, "", "","form-control"),
                                        Helper.GenerateTextbox(newRecordId, "Mark",  "", "", false, "", "10","form-control mark"),
                                        Helper.HTMLAutoComplete(newRecordId, "TypeOfComponent","","",false,"","",false,"","","TypeOfComponent","form-control"),
                                        GenerateCheckbox(newRecordId,"WIP",false),
                                        GenerateCheckbox(newRecordId,"IsPlanned",false),
                                        Helper.HTMLAutoComplete(newRecordId, "Overlay","","",false,"","",(role == "oplng"?true:false),"","","Start","form-control"),
                                        Helper.HTMLAutoComplete(newRecordId,"MOC",  "", "", false, "", "",false,"","","MOC","form-control"),
                                        Helper.GenerateTextbox(newRecordId,"ID_BlankDia",  "", "ID_BlankDia_Change(this)", false, "", "","numeric",false,"","ID Blank(mm)"),
                                        Helper.HTMLAutoComplete(newRecordId, "Category",  "", "Category_Blur(this)", false, "", "",false,"","","Category","form-control"),
                                        Helper.GenerateTextbox(newRecordId,"Length",  "", "", false, "", "","numeric",false,"","Length"),

                                        Helper.GenerateTextbox(newRecordId, "ID_Weld_Height",  "", "", true, "", "5","form-control numeric5 ID_Weld_Height"),
                                        Helper.GenerateTextbox(newRecordId, "GF_OD",  "", "", true, "", "5","form-control numeric5 GF_OD"),
                                        Helper.GenerateTextbox(newRecordId, "GF_Weld_Height",  "", "", true, "", "5","form-control numeric5 GF_Weld_Height"),
                                        Helper.GenerateTextbox(newRecordId, "BF_OD",  "", "", true, "", "5","form-control numeric5 BF_OD"),
                                        Helper.GenerateTextbox(newRecordId, "BF_Weld_Height",  "", "", true, "", "5","form-control numeric5 BF_Weld_Height"),

                                        Helper.HTMLAutoComplete(newRecordId, "Groove_On_GF",clsImplementationEnum.Dispatch.No.GetStringValue(),"",false,"","",false,"","","Groove_On_GF","form-control"),
                                        Helper.HTMLAutoComplete(newRecordId, "ID_Weld_KG","","",true,"","",false,"","","ID_Weld_KG","form-control"),
                                        Helper.HTMLAutoComplete(newRecordId, "WP_For_ID","","",true,"","",false,"","","WP_For_ID","form-control"),
                                        Helper.HTMLAutoComplete(newRecordId, "Face_Weld_KG","","",true,"","",false,"","","Face_Weld_KG","form-control"),
                                        Helper.HTMLAutoComplete(newRecordId, "WP_For_Face","","",true,"","",false,"","","WP_For_Face","form-control"),

                                        Helper.HTMLAutoComplete(newRecordId, "Groove_Weld_KG","","",true,"","",false,"","","Groove_Weld_KG","form-control"),
                                        Helper.HTMLAutoComplete(newRecordId, "WP_For_Groove",clsImplementationEnum.WeldProcessForCOP.MEGTAW.GetStringValue(),"",false,"","",false,"","","WP_For_Groove","form-control"),
                                        Helper.HTMLAutoComplete(newRecordId, "Back_Face_Weld_KG","","",true,"","",false,"","","Back_Face_Weld_KG","form-control"),
                                        Helper.HTMLAutoComplete(newRecordId, "WP_For_BackFace","","",true,"","",false,"","","WP_For_BackFace","form-control"),
                                        Helper.HTMLAutoComplete(newRecordId, "Total_Weld_KG","","",true,"","",false,"","","Total_Weld_KG","form-control"),

                                        Helper.HTMLAutoComplete(newRecordId,"Part","","",false,"","",false,"","","Part","form-control"),
                                        Helper.HTMLAutoComplete(newRecordId,"Procss","","",false,"","",false,"","","Process","form-control"),
                                        Helper.HTMLAutoComplete(newRecordId,"Comp_Assly","","",false,"","",true,"","","Comp_Assly","form-control"),
                                        Helper.HTMLAutoComplete(newRecordId, "PlannedReceipt",  "", "", false, "", "") + Helper.GenerateHidden(newRecordId, "POLine",  "") + Helper.GenerateHidden(newRecordId, "PONo",  ""),
                                         "",
                                         "",
                                        Helper.GenerateTextbox(newRecordId, "RequestedOn",  "", "", false,"","","form-control datePicker",(role == "oplng"?true:false), "", ""),
                                        Helper.GenerateTextbox(newRecordId, "CycleTime",  "", "", false, "", "","",true),
                                        Helper.HTMLAutoComplete(newRecordId, "WeekNo","","",false,"","",false,"","","Pipelined Schedule","form-control")+ Helper.GenerateHidden(newRecordId, "FromDate",  "")+ Helper.GenerateHidden(newRecordId, "ToDate",  ""),
                                        Helper.HTMLAutoComplete(newRecordId, "Start",clsImplementationEnum.COP_CommonStatus.NS.GetStringValue(),"",false,"","",false,"","","Start","form-control"),
                                        Helper.HTMLAutoComplete(newRecordId, "BM_NDT",clsImplementationEnum.COP_CommonStatus.NS.GetStringValue(),"",false,"","",false,"","","BM_NDT","form-control"),
                                        Helper.HTMLAutoComplete(newRecordId, "BL_OL",clsImplementationEnum.COP_CommonStatus.NS.GetStringValue(),"",false,"","",false,"","","BL_OL","form-control"),
                                        Helper.HTMLAutoComplete(newRecordId, "BL_PT",clsImplementationEnum.COP_CommonStatus.NS.GetStringValue(),"",false,"","",false,"","","BL_PT","form-control"),
                                        Helper.HTMLAutoComplete(newRecordId, "OL",clsImplementationEnum.COP_CommonStatus.NS.GetStringValue(),"",false,"","",false,"","","OL","form-control"),
                                        Helper.HTMLAutoComplete(newRecordId, "GF_BL_PT",clsImplementationEnum.COP_CommonStatus.NS.GetStringValue(),"",false,"","",false,"","","GF_BL_PT","form-control"),
                                        Helper.HTMLAutoComplete(newRecordId, "GF_OL",clsImplementationEnum.COP_CommonStatus.NS.GetStringValue(),"",false,"","",false,"","","GF_OL","form-control"),
                                        Helper.HTMLAutoComplete(newRecordId, "M_Cing",clsImplementationEnum.COP_CommonStatus.NS.GetStringValue(),"",false,"","",false,"","","M_Cing","form-control"),
                                        Helper.HTMLAutoComplete(newRecordId, "NDT",clsImplementationEnum.COP_CommonStatus.NS.GetStringValue(),"",false,"","",false,"","","NDT","form-control"),
                                        Helper.HTMLAutoComplete(newRecordId, "ICS",clsImplementationEnum.COP_CommonStatus.NS.GetStringValue(),"",false,"","",false,"","","ICS","form-control"),
                                        Helper.HTMLAutoComplete(newRecordId, "DISP",clsImplementationEnum.Dispatch.No.GetStringValue(),"DispacthChange("+newRecordId+")",false,"","",false,"","","Dispacth","form-control"),
                                        Helper.GenerateTextbox(newRecordId,"DispatchDate"),
                                        Helper.GenerateTextbox(newRecordId, "deliveryLocation",  "", "", false, "", "100","",false,"","deliveryLocation"),
                                        Helper.HTMLAutoComplete(newRecordId, "PWHT",clsImplementationEnum.COP_CommonStatus.NA.GetStringValue(),"PWHTChange("+newRecordId+")",false,"","",false,"","","PWHT","form-control"),
                                        Helper.HTMLAutoComplete(newRecordId, "AfterPWHTDispatch",clsImplementationEnum.COP_CommonStatus.NA.GetStringValue(),"",false,"","",false,"","","AfterPWHTDispatch","form-control"),
                                        "",
                                        Helper.GenerateTextbox(newRecordId, "Remarks",  "", "", false, "", "100","",(role == "jplng"?true:false),"","Remarks"),
                                        MultiSelectDropdown(items,newRecordId,"MachiningLocation",(role == "jplng"?true:false),"","","MachiningLocation"),
                                        Helper.GenerateTextbox(newRecordId,"MCDatetime"),
                                        //Helper.GenerateTextbox(newRecordId,"MCTime"),
                                        Helper.HTMLAutoComplete(newRecordId, "txtMCingDays","","",false,"","",false,"","","MCingDays","form-control") + Helper.GenerateHidden(newRecordId, "MCingDays",  ""),
                                        MultiSelectDropdownForMachine(MachineCode,newRecordId,"ddlMachineCode",(role == "jplng"?true:false),"","","MachineCode"),
                                       // Helper.HTMLAutoComplete(newRecordId, "txtMachineCode","","",false,"","",false,"","","MachineCode","form-control") + Helper.GenerateHidden(newRecordId, "MachineCode",  ""),
                                        "",
                                        "",
                                        "",
                                        Helper.GenerateGridButtonNew(newRecordId, "Save", "Save Rocord", "fa fa-save", "AddComponent()" ),
                                    };
                    data.Add(newRecord);
                }
                else
                {
                    var lstResult = db.SP_COP_GET_INDEX_DATA(1, 0, "", "HeaderId = " + id).Take(1).ToList();
                    var dtPlannedReceipt = new DateTime();
                    if (isReadOnly)
                    {
                        data = (from uc in lstResult
                                select new[]
                               {
                                Convert.ToString(uc.HeaderId),
                                uc.Project,
                                uc.QualityProject,
                                uc.Customer,
                                uc.BU,
                                uc.ItemId,
                                uc.Description,
                                uc.Mark,
                                uc.TypeOfComponent,
                                GenerateCheckbox(uc.HeaderId,"WIP",uc.WIP,true),
                                GenerateCheckbox(uc.HeaderId,"IsPlanned",uc.IsPlanned,true),
                                (uc.Overlay==true?clsImplementationEnum.Overlay.Yes.GetStringValue():clsImplementationEnum.Overlay.No.GetStringValue()),
                                uc.MOC,
                                Convert.ToString(uc.ID_BlankDia),
                                Convert.ToString(uc.Category),
                                Convert.ToString(uc.Length),
                                Convert.ToString(uc.ID_Weld_Height),
                                Convert.ToString(uc.GF_OD),
                                Convert.ToString(uc.GF_Weld_Height),
                                Convert.ToString(uc.BF_OD),
                                Convert.ToString(uc.BF_Weld_Height),
                                Convert.ToString(uc.Groove_On_GF),
                                Convert.ToString(uc.ID_Weld_KG),
                                Convert.ToString(uc.WP_For_ID),
                                Convert.ToString(uc.Face_Weld_KG),
                                Convert.ToString(uc.WP_For_Face),
                                Convert.ToString(uc.Groove_Weld_KG),
                                Convert.ToString(uc.WP_For_Groove),
                                Convert.ToString(uc.Back_Face_Weld_KG),
                                Convert.ToString(uc.WP_For_BackFace),
                                Convert.ToString(uc.Total_Weld_KG),
                                uc.Part,
                                uc.Procss,
                                uc.Comp_Assly,
                                (DateTime.TryParse(uc.PlannedReceipt+"",out dtPlannedReceipt) ? dtPlannedReceipt.ToString("dd/MM/yyyy") : uc.PlannedReceipt),
                                uc.IMRStatus,
                                uc.SeamStatus,
                                uc.RequestedOn.HasValue? uc.RequestedOn.Value.ToString("dd/MM/yyyy"):"",
                                Convert.ToString(uc.CycleTime),
                                uc.FromDate.HasValue ? "Week " + Convert.ToString(uc.WeekNo) +" ["+ uc.FromDate.Value.ToShortDateString() +" to "+uc.ToDate.Value.ToShortDateString() +"]" :"",
                                uc.Start,
                                uc.BM_NDT,
                                uc.BL_OL,
                                uc.BL_PT,
                                uc.OL,
                                uc.GF_BL_PT,
                                uc.GF_OL,
                                uc.M_Cing,
                                uc.NDT,
                                uc.ICS,
                                uc.DISP,
                               (uc.DispatchDate.HasValue? uc.DispatchDate.Value.ToString("dd/MM/yyyy"): ""),
                               uc.deliveryLocation,
                               uc.PWHT,
                               uc.AfterPWHTDispatch,
                                uc.HasAttachment?"Yes":"No",
                                uc.Remarks,
                                uc.MachiningLocation,
                                (uc.MCDatetime.HasValue? uc.MCDatetime.Value.ToString("dd/MM/yyyy"): ""),
                                //(uc.MCTime.HasValue? uc.MCTime.Value.ToString("hh:mm:tt"): ""),
                                (uc.MCingDays.HasValue? uc.MCingDays.Value.ToString()+" Days": ""),
                                Convert.ToString(uc.MachineName),
                                Convert.ToString(uc.MachineCode),
                                Convert.ToString(uc.CameraURL),
                                Convert.ToString(uc.DeleteRemarks),
                                 "<span><a id='View" + uc.HeaderId + "' name='View' title='View' class='blue action "+(uc.HasAttachment == true ? "save" : "") + "' onclick='ViewRecord(" + uc.HeaderId + ")'>"+
                                        "<i class='fa fa-eye'></i></a> &nbsp &nbsp"+
                                        "<a id='Attachment" + uc.HeaderId + "' name='Attachment' title='Attachment' class='blue action "+(uc.HasAttachment == true ? "save" : "") + "' onclick='ViewAttachment(" + uc.HeaderId + ")'>"+
                                        "<i class='fa fa-paperclip'></i></a>&nbsp &nbsp"+
                                        "<a id='CameraURL" + uc.HeaderId + "' name='CameraURL' title=\"URL\" class=\"blue action\" onclick=\"RedirectCameraURL(this," + uc.HeaderId + ")\">"+
                                        "<i class=\"iconclass fa fa-camera\"></i></a>&nbsp &nbsp"+
                                        "<a id='QRcode" + uc.HeaderId + "' name='QRcode' title=\"QRcode\" class=\"blue action\" onclick=\"ViewQRcode(this," + uc.HeaderId +")\">"+
                                        "<i class=\"iconclass fa fa-qrcode\"></i></a>&nbsp &nbsp"+
                                        "<a id='Seam" + uc.HeaderId + "' name='Seam' title=\"Seam\" class=\"blue action\" onclick=\"Seamlist(this," + uc.HeaderId +")\">"+
                                        "<i class=\"iconclass fa fa-th\"></i></a>" +
                                        "</span>"
                          }).ToList();
                    }
                    else
                    {
                        if (role == "oplng")
                        {
                            data = (from uc in lstResult
                                    select new[] {
                                        uc.HeaderId.ToString(),
                                        Helper.HTMLAutoComplete(uc.HeaderId,"Project",uc.Project,"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",true,"","","Project","form-control editable"),
                                        Helper.GenerateDropdown(uc.HeaderId,"QualityProject",new SelectList(new List<SelectListItem>() { new SelectListItem { Text = uc.QualityProject, Value = uc.QualityProject } },"Value", "Text",uc.QualityProject),"","UpdateData(this, "+ uc.HeaderId+")"),
                                        Helper.GenerateTextbox(uc.HeaderId, "Customer",uc.Customer,"UpdateData(this, "+ uc.HeaderId+",true)",true, "","","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "BU",uc.BU,"UpdateData(this, "+ uc.HeaderId+",true)", false, "", "",false,"","","BU","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId,"ItemId",uc.ItemId,"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",true,"","","Item","form-control editable"),
                                        Helper.GenerateTextbox(uc.HeaderId,"Description",uc.Description, "UpdateData(this, "+ uc.HeaderId+",true)", false, "", "","form-control editable",true),
                                        Helper.GenerateTextbox(uc.HeaderId, "Mark",uc.Mark,"UpdateData(this, "+ uc.HeaderId+",true)", false, "", "10","form-control editable mark"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "TypeOfComponent",uc.TypeOfComponent,"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",false,"","","TypeOfComponent","form-control editable"),
                                        Helper.GenerateCheckboxWithEvent(uc.HeaderId,"WIP",uc.WIP,"WIPChanged(this,\"WIP\", "+ uc.HeaderId+")",true),
                                        Helper.GenerateCheckboxWithEvent(uc.HeaderId,"IsPlanned",uc.IsPlanned,"IsPlannedChanged(this,\"IsPlanned\", "+ uc.HeaderId+")",true),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "Overlay",(uc.Overlay.HasValue ? (uc.Overlay.Value  ? clsImplementationEnum.Overlay.Yes.GetStringValue() :  clsImplementationEnum.Overlay.No.GetStringValue()): "")
                                        ,"UpdateData(this, "+ uc.HeaderId+",true)",false,"",Convert.ToString(uc.HeaderId),true,"","","Overlay","editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId,"MOC",  uc.MOC,"UpdateData(this, "+ uc.HeaderId+",true)", false, "", "",false,"","","MOC","form-control editable"),
                                        Helper.GenerateTextbox(uc.HeaderId,"ID_BlankDia",uc.ID_BlankDia.HasValue ? Convert.ToString(uc.ID_BlankDia.Value) : "" ,"UpdateData(this, "+ uc.HeaderId+",true)", false, "", "","form-control editable numeric",false,"ID_BlankDia_Change(this)","ID Blank(mm)"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "Category",  uc.Category,"UpdateData(this, "+ uc.HeaderId+",true)", false, "", "",false,"","","Category","form-control editable"),
                                        Helper.GenerateTextbox(uc.HeaderId,"Length",uc.Length.HasValue ? Convert.ToString(uc.Length.Value) : "" ,"UpdateData(this, "+ uc.HeaderId+",true)" , false, "", "","form-control editable numeric",false,"","Length"),
                                        Helper.GenerateTextbox(uc.HeaderId, "ID_Weld_Height",uc.ID_Weld_Height,"UpdateData(this, "+ uc.HeaderId+",true)", true, "", "5","form-control editable numeric5 ID_Weld_Height"),
                                        Helper.GenerateTextbox(uc.HeaderId, "GF_OD",uc.GF_OD,"UpdateData(this, "+ uc.HeaderId+",true)", true, "", "5","form-control editable numeric5 GF_OD"),
                                        Helper.GenerateTextbox(uc.HeaderId, "GF_Weld_Height",uc.GF_Weld_Height,"UpdateData(this, "+ uc.HeaderId+",true)", true, "", "5","form-control editable numeric5 GF_Weld_Height"),
                                        Helper.GenerateTextbox(uc.HeaderId, "BF_OD",uc.BF_OD,"UpdateData(this, "+ uc.HeaderId+",true)", true, "", "5","form-control editable numeric5 BF_OD"),
                                        Helper.GenerateTextbox(uc.HeaderId, "BF_Weld_Height",uc.BF_Weld_Height,"UpdateData(this, "+ uc.HeaderId+",true)", true, "", "5","form-control editable numeric5 BF_Weld_Height"),

                                        Helper.HTMLAutoComplete(uc.HeaderId, "Groove_On_GF",(uc.Groove_On_GF != null ? uc.Groove_On_GF : clsImplementationEnum.Dispatch.No.GetStringValue()),"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",false,"","","Groove_On_GF","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "ID_Weld_KG",uc.ID_Weld_KG,"",true,"","",false,"","","ID_Weld_KG","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "WP_For_ID",uc.WP_For_ID,"UpdateData(this, "+ uc.HeaderId+",true)",true,"","",false,"","","WP_For_ID","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "Face_Weld_KG",uc.Face_Weld_KG,"",true,"","",false,"","","Face_Weld_KG","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "WP_For_Face",uc.WP_For_Face,"UpdateData(this, "+ uc.HeaderId+",true)",true,"","",false,"","","WP_For_Face","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "Groove_Weld_KG",uc.Groove_Weld_KG,"",true,"","",false,"","","Groove_Weld_KG","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "WP_For_Groove",uc.WP_For_Groove,"UpdateData(this, "+ uc.HeaderId+",true)",true,"","",false,"","","WP_For_Groove","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "Back_Face_Weld_KG",uc.Back_Face_Weld_KG,"",true,"","",false,"","","Back_Face_Weld_KG","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "WP_For_BackFace",uc.WP_For_BackFace,"UpdateData(this, "+ uc.HeaderId+",true)",true,"","",false,"","","WP_For_BackFace","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "Total_Weld_KG",uc.Total_Weld_KG,"",true,"","",false,"","","Total_Weld_KG","form-control editable"),

                                        Helper.HTMLAutoComplete(uc.HeaderId,"Part",uc.Part,"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",false,"","","Part","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId,"Procss",uc.Procss,"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",false,"","","Process","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId,"Comp_Assly",uc.Comp_Assly,"",false,"","",true,"","","Comp_Assly","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "PlannedReceipt",uc.PlannedReceipt, "", false, "","",false,"","","Planned Receipt", "from-control editable") + Helper.GenerateHidden(uc.HeaderId, "POLine",  uc.POLine.HasValue? Convert.ToString(uc.POLine.Value):"") + Helper.GenerateHidden(uc.HeaderId, "PONo",  uc.PONo),
                                        Helper.HTMLAutoComplete(uc.HeaderId,"IMRStatus",uc.IMRStatus,"UpdateData(this, "+ uc.HeaderId+",false)",false,"","",false,"","","IMRStatus","form-control editable"),
                                        uc.SeamStatus,
                                        Helper.GenerateTextbox(uc.HeaderId, "RequestedOn",uc.RequestedOn.HasValue?uc.RequestedOn.Value.ToString("dd/MM/yyyy"):"", "", false, "", "","form-control editable datePicker",(role == "oplng"?true:false),"","RequestedOn"),
                                        Helper.GenerateTextbox(uc.HeaderId, "CycleTime",uc.CycleTime.HasValue?Convert.ToString(uc.CycleTime.Value) :"", "UpdateData(this, "+ uc.HeaderId+",true)", false, "", "","form-control editable",true),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "WeekNo",( uc.FromDate.HasValue ? "Week " + Convert.ToString(uc.WeekNo) +" ["+ uc.FromDate.Value.ToShortDateString() +" to "+uc.ToDate.Value.ToShortDateString() +"]" :""),"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",false,"","","Pipelined Schedule","form-control editable")+ Helper.GenerateHidden(uc.HeaderId, "FromDate",  "")+ Helper.GenerateHidden(uc.HeaderId, "ToDate",  ""),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "Start",uc.Start,"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",false,"","","Start","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "BM_NDT",uc.BM_NDT,"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",false,"","","BM_NDT","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "BL_OL",uc.BL_OL,"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",false,"","","BL_OL","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "BL_PT",uc.BL_PT,"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",false,"","","BL_PT","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "OL",uc.OL,"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",false,"","","OL","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "GF_BL_PT",uc.GF_BL_PT,"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",false,"","","GF_BL_PT","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "GF_OL",uc.GF_OL,"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",false,"","","GF_OL","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "M_Cing",uc.M_Cing,"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",false,"","","M_Cing","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "NDT",uc.NDT,"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",false,"","","NDT","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "ICS",uc.ICS,"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",false,"","","ICS","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "DISP",uc.DISP,"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",false,"","","Dispacth","form-control editable"),
                                        Helper.GenerateTextbox(uc.HeaderId,"DispatchDate",uc.DispatchDate.HasValue? uc.DispatchDate.Value.ToString("yyyy-MM-dd"): "","UpdateData(this, "+ uc.HeaderId+",false)", false, "", "","form-control editable",false),
                                        Helper.GenerateTextbox(uc.HeaderId, "deliveryLocation",Convert.ToString(uc.deliveryLocation), "UpdateData(this, "+ uc.HeaderId+",false)", false, "", "100","form-control editable",false),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "PWHT",string.IsNullOrEmpty(uc.PWHT)?clsImplementationEnum.COP_CommonStatus.NA.GetStringValue():uc.PWHT,"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",false,"","","PWHT","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "AfterPWHTDispatch",string.IsNullOrEmpty(uc.AfterPWHTDispatch)?clsImplementationEnum.COP_CommonStatus.NA.GetStringValue():uc.AfterPWHTDispatch,"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",false,"","","AfterPWHTDispatch","form-control editable"),
                                        "",
                                        Helper.GenerateTextbox(uc.HeaderId, "Remarks",Convert.ToString(uc.Remarks), "UpdateData(this, "+ uc.HeaderId+",false)", false, "", "100","form-control editable",(role == "jplng"?true:false)),
                                        MultiSelectDropdown(items,uc.HeaderId,uc.MachiningLocation,(role == "jplng"?true:false),"UpdateData(this, "+ uc.HeaderId +",true)","","MachiningLocation"),
                                        Helper.GenerateTextbox(uc.HeaderId,"MCDatetime",uc.MCDatetime.HasValue? uc.MCDatetime.Value.ToString("yyyy-MM-dd"): "","UpdateData(this, "+ uc.HeaderId+",false)", false, "", "","form-control editable",false),                                        
                                        //Helper.GenerateTextbox(uc.HeaderId,"MCTime",uc.MCTime.HasValue? uc.MCTime.Value.ToString("hh:mm:tt"): "","UpdateData(this, "+ uc.HeaderId+",false)", false, "", "100","form-control timepicker timepicker-24 editable",false,"UpdateData(this, "+ uc.HeaderId+",false)"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "txtMCingDays",string.IsNullOrEmpty(uc.MCingDays.ToString())?"":uc.MCingDays.ToString()+" Days","UpdateData(this, "+ uc.HeaderId+",false)",false,"","MCingDays",false,"","","Select","form-control") + Helper.GenerateHidden(uc.HeaderId, "MCingDays",  uc.MCingDays.ToString()),
                                       // Helper.HTMLAutoComplete(uc.HeaderId, "txtMachineCode",uc.MachineName,"UpdateData(this, "+ uc.HeaderId+",false)",false,"","MachineCode",false,"","","Select","form-control") + Helper.GenerateHidden(uc.HeaderId, "MachineCode",  uc.MachineCode),
                                        MultiSelectDropdownForMachine(MachineCode,uc.HeaderId,uc.MachineCode,(role == "jplng"?true:false),"UpdateData(this, "+ uc.HeaderId +",true)","","MachineCode"),
                                        Convert.ToString(uc.MachineCode),
                                        Convert.ToString(uc.CameraURL),
                                        Convert.ToString(uc.DeleteRemarks),
                                        "<span class='editable'><a id='Cancel' name='Cancel' title='Cancel' class='blue action' onclick='ReloadGrid()'>"+
                                        "<i class='fa fa-close'></i></a> &nbsp &nbsp <a id='View" + uc.HeaderId + "' name='View' title='View' class='blue action "+(uc.HasAttachment == true ? "save" : "") + "' onclick='ViewRecord(" + uc.HeaderId + ")'>"+
                                        "<i class='fa fa-eye'></i></a> &nbsp &nbsp<a id='Attachment" + uc.HeaderId + "' name='Attachment' title='Attachment' class='blue action "+(uc.HasAttachment == true ? "save" : "") + "' onclick='ViewAttachment(" + uc.HeaderId + ")'>"+
                                        "<i class='fa fa-paperclip'></i></a> &nbsp &nbsp" +
                                       "<a id='CameraURL" + uc.HeaderId + "' name='CameraURL' title=\"URL\" class=\"blue action\" onclick=\"RedirectCameraURL(this," + uc.HeaderId + ")\">"+
                                        "<i class=\"iconclass fa fa-camera\"></i></a>&nbsp &nbsp"+
                                        "<a id='QRcode" + uc.HeaderId + "' name='QRcode' title=\"QRcode\" class=\"blue action\" onclick=\"ViewQRcode(this," + uc.HeaderId +")\">"+
                                        "<i class=\"iconclass fa fa-qrcode\"></i></a>&nbsp &nbsp"+
                                        "<a id='Seam" + uc.HeaderId + "' name='Seam' title=\"Seam\" class=\"blue action\" onclick=\"Seamlist(this," + uc.HeaderId +")\">"+
                                        "<i class=\"iconclass fa fa-th\"></i></a>" +
                                        "</span>",
                          }).ToList();
                        }
                        else if (role == "jplng")
                        {
                            data = (from uc in lstResult
                                    select new[] {
                            uc.HeaderId.ToString(),
                                Helper.HTMLAutoComplete(uc.HeaderId,"Project",uc.Project,"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",true,"","","Project","form-control editable"),
                                        Helper.GenerateDropdown(uc.HeaderId,"QualityProject",new SelectList(new List<SelectListItem>() { new SelectListItem { Text = uc.QualityProject, Value = uc.QualityProject } },"Value", "Text",uc.QualityProject),"","UpdateData(this, "+ uc.HeaderId+")",true),
                                        Helper.GenerateTextbox(uc.HeaderId, "Customer",uc.Customer,"UpdateData(this, "+ uc.HeaderId+",true)",false, "","","form-control editable",true),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "BU",uc.BU,"UpdateData(this, "+ uc.HeaderId+",true)", false, "", "",true,"","","BU","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId,"ItemId",uc.ItemId,"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",true,"","","Item","form-control editable"),
                                        Helper.GenerateTextbox(uc.HeaderId,"Description",uc.Description, "UpdateData(this, "+ uc.HeaderId+",true)", true, "", "","form-control editable"),
                                        Helper.GenerateTextbox(uc.HeaderId, "Mark",uc.Mark,"UpdateData(this, "+ uc.HeaderId+",true)", false, "", "10","form-control editable mark",true),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "TypeOfComponent",uc.TypeOfComponent,"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",true,"","","TypeOfComponent","form-control editable"),
                                        GenerateCheckbox(uc.HeaderId,"WIP",uc.WIP,true),
                                        GenerateCheckbox(uc.HeaderId,"IsPlanned",uc.IsPlanned,true),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "Overlay",(uc.Overlay.HasValue ? (uc.Overlay.Value  ? clsImplementationEnum.Overlay.Yes.GetStringValue() :  clsImplementationEnum.Overlay.No.GetStringValue()): "")
                                        ,"UpdateData(this, "+ uc.HeaderId+",true)",false,"",Convert.ToString(uc.HeaderId),false,"","","Overlay","editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId,"MOC",  uc.MOC,"UpdateData(this, "+ uc.HeaderId+",true)", false, "", "",true,"","","MOC","form-control editable"),
                                        Helper.GenerateTextbox(uc.HeaderId,"ID_BlankDia",uc.ID_BlankDia.HasValue ? Convert.ToString(uc.ID_BlankDia.Value) : "" ,"UpdateData(this, "+ uc.HeaderId+",true)", false, "", "","form-control editable numeric",true,"ID_BlankDia_Change(this)","ID Blank(mm)"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "Category",  uc.Category,"UpdateData(this, "+ uc.HeaderId+",true)", false, "", "",true,"","","Category","form-control editable"),
                                        Helper.GenerateTextbox(uc.HeaderId,"Length",uc.Length.HasValue ? Convert.ToString(uc.Length.Value) : "" ,"UpdateData(this, "+ uc.HeaderId+",true)" , false, "", "","form-control editable numeric",true,"","Length"),

                                        Helper.GenerateTextbox(uc.HeaderId, "ID_Weld_Height",uc.ID_Weld_Height,"UpdateData(this, "+ uc.HeaderId+",true)", true, "", "5","form-control editable numeric5 ID_Weld_Height"),
                                        Helper.GenerateTextbox(uc.HeaderId, "GF_OD",uc.GF_OD,"UpdateData(this, "+ uc.HeaderId+",true)", true, "", "5","form-control editable numeric5 GF_OD"),
                                        Helper.GenerateTextbox(uc.HeaderId, "GF_Weld_Height",uc.GF_Weld_Height,"UpdateData(this, "+ uc.HeaderId+",true)", true, "", "5","form-control editable numeric5 GF_Weld_Height"),
                                        Helper.GenerateTextbox(uc.HeaderId, "BF_OD",uc.BF_OD,"UpdateData(this, "+ uc.HeaderId+",true)", true, "", "5","form-control editable numeric5 BF_OD"),
                                        Helper.GenerateTextbox(uc.HeaderId, "BF_Weld_Height",uc.BF_Weld_Height,"UpdateData(this, "+ uc.HeaderId+",true)", true, "", "5","form-control editable numeric5 BF_Weld_Height"),

                                        Helper.HTMLAutoComplete(uc.HeaderId, "Groove_On_GF",uc.Groove_On_GF,"UpdateData(this, "+ uc.HeaderId+",true)",true,"","",false,"","","Groove_On_GF","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "ID_Weld_KG",uc.ID_Weld_KG,"",true,"","",false,"","","ID_Weld_KG","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "WP_For_ID",uc.WP_For_ID,"UpdateData(this, "+ uc.HeaderId+",true)",true,"","",false,"","","WP_For_ID","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "Face_Weld_KG",uc.Face_Weld_KG,"",true,"","",false,"","","Face_Weld_KG","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "WP_For_Face",uc.WP_For_Face,"UpdateData(this, "+ uc.HeaderId+",true)",true,"","",false,"","","WP_For_Face","form-control editable"),

                                        Helper.HTMLAutoComplete(uc.HeaderId, "Groove_Weld_KG",uc.Groove_Weld_KG,"",true,"","",false,"","","Groove_Weld_KG","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "WP_For_Groove",uc.WP_For_Groove,"UpdateData(this, "+ uc.HeaderId+",true)",true,"","",false,"","","WP_For_Groove","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "Back_Face_Weld_KG",uc.Back_Face_Weld_KG,"",true,"","",false,"","","Back_Face_Weld_KG","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "WP_For_BackFace",uc.WP_For_BackFace,"UpdateData(this, "+ uc.HeaderId+",true)",true,"","",false,"","","WP_For_BackFace","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "Total_Weld_KG",uc.Total_Weld_KG,"",true,"","",false,"","","Total_Weld_KG","form-control editable"),

                                        Helper.HTMLAutoComplete(uc.HeaderId,"Part",uc.Part,"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",true,"","","Part","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId,"Procss",uc.Procss,"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",true,"","","Process","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId,"Comp_Assly",uc.Comp_Assly,"",false,"","",false,"","","Comp_Assly","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "PlannedReceipt",uc.PlannedReceipt, "", false, "", Convert.ToString(uc.HeaderId),true,"","","Planned Receipt","from-control editable") + Helper.GenerateHidden(uc.HeaderId, "POLine",  uc.POLine.HasValue? Convert.ToString(uc.POLine.Value):"") + Helper.GenerateHidden(uc.HeaderId, "PONo",  uc.PONo),
                                        Helper.HTMLAutoComplete(uc.HeaderId,"IMRStatus",uc.IMRStatus,"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",true,"","","IMRStatus","form-control editable"),
                                        uc.SeamStatus,
                                        Helper.GenerateTextbox(uc.HeaderId, "RequestedOn",uc.RequestedOn.HasValue?uc.RequestedOn.Value.ToString("dd/MM/yyyy"):"", "", false, "","", "form-control editable datePicker",(role == "oplng"?true:false),"","Rquested On"),
                                        Helper.GenerateTextbox(uc.HeaderId, "CycleTime",uc.CycleTime.HasValue?Convert.ToString(uc.CycleTime.Value) :"", "UpdateData(this, "+ uc.HeaderId+",true)", false, "","", "form-control editable",false,"","Cycle Time"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "WeekNo",( uc.FromDate.HasValue ? "Week " + Convert.ToString(uc.WeekNo) +" ["+ uc.FromDate.Value.ToShortDateString() +" to "+uc.ToDate.Value.ToShortDateString() +"]" :""),"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",true,"","","Pipelined Schedule","form-control editable")+ Helper.GenerateHidden(uc.HeaderId, "FromDate",  "")+ Helper.GenerateHidden(uc.HeaderId, "ToDate",  ""),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "Start",uc.Start,"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",true,"","","Start","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "BM_NDT",uc.BM_NDT,"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",true,"","","BM_NDT","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "BL_OL",uc.BL_OL,"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",true,"","","BL_OL","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "BL_PT",uc.BL_PT,"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",true,"","","BL_PT","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "OL",uc.OL,"UpdateData(this, "+ uc.HeaderId+",true)",false,"","OL",true,"","","","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "GF_BL_PT",uc.GF_BL_PT,"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",true,"","","GF_BL_PT","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "GF_OL",uc.GF_OL,"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",true,"","","GF_OL","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "M_Cing",uc.M_Cing,"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",true,"","","M_Cing","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "NDT",uc.NDT,"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",true,"","","NDT","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "ICS",uc.ICS,"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",true,"","","ICS","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "DISP",uc.DISP,"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",true,"","","Dispacth","form-control editable"),
                                        Helper.GenerateTextbox(uc.HeaderId,"DispatchDate",uc.DispatchDate.HasValue? uc.DispatchDate.Value.ToString("yyyy-MM-dd"): "","UpdateData(this, "+ uc.HeaderId+",false)",false,"","","form-control editable",true),
                                        Helper.GenerateTextbox(uc.HeaderId, "deliveryLocation",Convert.ToString(uc.deliveryLocation), "UpdateData(this, "+ uc.HeaderId+",false)", false, "", "100","form-control editable",false),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "PWHT",string.IsNullOrEmpty(uc.PWHT)?clsImplementationEnum.COP_CommonStatus.NA.GetStringValue():uc.PWHT,"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",true,"","","PWHT","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "PWHT",string.IsNullOrEmpty(uc.AfterPWHTDispatch)?clsImplementationEnum.COP_CommonStatus.NA.GetStringValue():uc.AfterPWHTDispatch,"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",true,"","","AfterPWHTDispatch","form-control editable"),
                                        "",
                                        Helper.GenerateTextbox(uc.HeaderId, "Remarks",Convert.ToString(uc.Remarks), "UpdateData(this, "+ uc.HeaderId+",false)", false, "", "100","form-control editable",(role == "jplng"?true:false)),
                                        MultiSelectDropdown(items,uc.HeaderId,uc.MachiningLocation,(role == "jplng"?true:false),"UpdateData(this, "+ uc.HeaderId +",true)","","MachiningLocation"),
                                        Helper.GenerateTextbox(uc.HeaderId,"MCDatetime",uc.MCDatetime.HasValue? uc.MCDatetime.Value.ToString("yyyy-MM-dd"): "","UpdateData(this, "+ uc.HeaderId+",false)",false,"","","form-control editable",true),
                                        //Helper.GenerateTextbox(uc.HeaderId,"MCTime",uc.MCTime.HasValue? uc.MCTime.Value.ToString("hh:mm:tt"): "","UpdateData(this, "+ uc.HeaderId+",false)",false,"","","form-control timepicker timepicker-24 editable",true,"UpdateData(this, "+ uc.HeaderId+",false)"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "txtMCingDays",string.IsNullOrEmpty(uc.MCingDays.ToString())?"":uc.MCingDays.ToString()+" Days","UpdateData(this, "+ uc.HeaderId+",false)",false,"","",true,"","","MCingDays","form-control") + Helper.GenerateHidden(uc.HeaderId, "MCingDays",  uc.MCingDays.ToString()),
                                     //   Helper.HTMLAutoComplete(uc.HeaderId, "txtMachineCode",uc.MachineName,"UpdateData(this, "+ uc.HeaderId+",false)",false,"","",true,"","","MachineCode","form-control") + Helper.GenerateHidden(uc.HeaderId, "MachineCode",  uc.MachineCode),
                                       MultiSelectDropdownForMachine(MachineCode,uc.HeaderId,uc.MachineCode,(role == "jplng"?true:false),"UpdateData(this, "+ uc.HeaderId +",true)","","MachineCode"),
                                        Convert.ToString(uc.MachineCode),
                                        Convert.ToString(uc.CameraURL),
                                        Convert.ToString(uc.DeleteRemarks),
                                        "<span class='editable'><a id='Cancel' name='Cancel' title='Cancel' class='blue action' onclick='ReloadGrid()'>"+
                                         "<i class='fa fa-close'></i></a> &nbsp &nbsp <a id='View" + uc.HeaderId + "' name='View' title='View' class='blue action "+(uc.HasAttachment == true ? "save" : "") + "' onclick='ViewRecord(" + uc.HeaderId + ")'>"+
                                        "<i class='fa fa-eye'></i></a> &nbsp &nbsp <a id='Attachment" + uc.HeaderId + "' name='Attachment' title='Attachment' class='blue action "+(uc.HasAttachment == true ? "save" : "") + "' onclick='ViewAttachment(" + uc.HeaderId + ")'>"+
                                        "<i class='fa fa-paperclip'></i></a> &nbsp &nbsp" +
                                        "<a id='CameraURL" + uc.HeaderId + "' name='CameraURL' title=\"URL\" class=\"blue action\" onclick=\"RedirectCameraURL(this," + uc.HeaderId + ")\">"+
                                        "<i class=\"iconclass fa fa-camera\"></i></a>&nbsp &nbsp"+
                                        "<a id='QRcode" + uc.HeaderId + "' name='QRcode' title=\"QRcode\" class=\"blue action\" onclick=\"ViewQRcode(this," + uc.HeaderId +")\">"+
                                        "<i class=\"iconclass fa fa-qrcode\"></i></a>&nbsp &nbsp"+
                                        "<a id='Seam" + uc.HeaderId + "' name='Seam' title=\"Seam\" class=\"blue action\" onclick=\"Seamlist(this," + uc.HeaderId +")\">"+
                                        "<i class=\"iconclass fa fa-th\"></i></a>" +
                                        "</span>",
                          }).ToList();
                        }
                        else
                        {
                            data = null;
                        }
                    }
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveNewRecord(FormCollection fc)
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var cop001 = new COP001();
                double? BlankDia = null;
                if (fc["ID_BlankDia" + 0] != null && fc["ID_BlankDia" + 0] != "")
                    BlankDia = Convert.ToDouble(fc["ID_BlankDia" + 0]);
                cop001.Project = fc["Project" + 0];
                cop001.QualityProject = fc["QualityProject" + 0];
                cop001.BU = fc["BU" + 0];
                cop001.Mark = fc["Mark" + 0] != null ? fc["Mark" + 0].ToString().ToUpper() : "";
                cop001.ItemId = fc["ItemId" + 0];
                cop001.Description = fc["Description" + 0];
                if (fc["Overlay" + 0] == null) { cop001.Overlay = null; }
                else { cop001.Overlay = (fc["Overlay" + 0].ToString().ToLower() == "no" ? false : true); }
                //cop001.Overlay = fc["Overlay" + 0] == null ? false : (fc["Overlay" + 0].ToString().ToLower() == "no" ? false : true);// Convert.ToBoolean(fc["Overlay" + 0]);
                cop001.MOC = fc["MOC" + 0];
                cop001.ID_BlankDia = BlankDia; // Convert.ToDouble(fc["ID_BlankDia" + 0]);
                if (fc["WIP" + 0] == null) { cop001.WIP = null; }
                else { cop001.WIP = (fc["WIP" + 0].ToString().ToLower() == "on" ? true : false); }
                if (fc["IsPlanned" + 0] == null) { cop001.IsPlanned = null; }
                else { cop001.IsPlanned = (fc["IsPlanned" + 0].ToString().ToLower() == "on" ? true : false); }
                cop001.Length = Convert.ToDouble(fc["Length" + 0]);
                cop001.ID_Weld_Height = fc["ID_Weld_Height" + 0];
                cop001.GF_OD = fc["GF_OD" + 0];
                cop001.GF_Weld_Height = fc["GF_Weld_Height" + 0];
                cop001.BF_OD = fc["BF_OD" + 0];
                cop001.BF_Weld_Height = fc["BF_Weld_Height" + 0];

                cop001.Groove_On_GF = fc["Groove_On_GF" + 0];
                cop001.ID_Weld_KG = fc["ID_Weld_KG" + 0];
                cop001.WP_For_ID = fc["WP_For_ID" + 0];
                cop001.Face_Weld_KG = fc["Face_Weld_KG" + 0];
                cop001.WP_For_Face = fc["WP_For_Face" + 0];

                cop001.Groove_Weld_KG = fc["Groove_Weld_KG" + 0];
                cop001.WP_For_Groove = fc["WP_For_Groove" + 0];
                cop001.Back_Face_Weld_KG = fc["Back_Face_Weld_KG" + 0];
                cop001.WP_For_BackFace = fc["WP_For_BackFace" + 0];
                cop001.Total_Weld_KG = fc["Total_Weld_KG" + 0];

                cop001.Part = fc["Part" + 0];
                cop001.OldPart = fc["Part" + 0];
                cop001.Procss = fc["Procss" + 0];
                cop001.Comp_Assly = fc["Comp_Assly" + 0];
                cop001.TypeOfComponent = fc["TypeOfComponent" + 0];
                cop001.RequestedOn = (!string.IsNullOrEmpty(fc["RequestedOn" + 0]) ? Convert.ToDateTime(fc["RequestedOn" + 0]) : (DateTime?)null);
                cop001.IMRStatus = fc["IMRStatus" + 0];
                cop001.Start = fc["Start" + 0];
                cop001.BM_NDT = fc["BM_NDT" + 0];
                cop001.BL_OL = fc["BL_OL" + 0];
                cop001.BL_PT = fc["BL_PT" + 0];
                cop001.OL = fc["OL" + 0];
                cop001.GF_BL_PT = fc["GF_BL_PT" + 0];
                cop001.GF_OL = fc["GF_OL" + 0];
                cop001.M_Cing = fc["M_Cing" + 0];
                cop001.NDT = fc["NDT" + 0];
                cop001.ICS = fc["ICS" + 0];
                cop001.DISP = fc["DISP" + 0];
                if (!string.IsNullOrWhiteSpace(fc["DispatchDate" + 0]))
                {
                    cop001.DispatchDate = Convert.ToDateTime(fc["DispatchDate" + 0]);
                }
                cop001.deliveryLocation = fc["deliveryLocation" + 0];
                cop001.PWHT = fc["PWHT" + 0];
                cop001.AfterPWHTDispatch = fc["AfterPWHTDispatch" + 0];
                if (!string.IsNullOrWhiteSpace(fc["MCDatetime" + 0]))
                {
                    cop001.MCDatetime = Convert.ToDateTime(fc["MCDatetime" + 0]);
                }
                cop001.CreatedOn = DateTime.Now;
                cop001.CreatedBy = objClsLoginInfo.UserName;
                cop001.Category = fc["Category" + 0];
                cop001.CycleTime = Convert.ToDouble(fc["CycleTime" + 0]);
                cop001.FromDate = (!string.IsNullOrEmpty(fc["FromDate" + 0]) ? Convert.ToDateTime(fc["FromDate" + 0]) : (DateTime?)null);
                cop001.ToDate = (!string.IsNullOrEmpty(fc["ToDate" + 0]) ? Convert.ToDateTime(fc["ToDate" + 0]) : (DateTime?)null);
                cop001.PlannedReceipt = fc["PlannedReceipt" + 0];
                cop001.PONo = fc["PONo" + 0];
                cop001.POLine = (!string.IsNullOrEmpty(fc["POLine" + 0]) ? Convert.ToInt32(fc["POLine" + 0]) : (int?)null);
                cop001.POSeq = Convert.ToDouble(fc["POSeq" + 0]);
                cop001.ReceiptSeq = Convert.ToDouble(fc["ReceiptSeq" + 0]);
                cop001.ReceiptNo = fc["ReceiptNo" + 0];
                cop001.Remarks = fc["Remarks" + 0];
                cop001.HasAttachment = Convert.ToBoolean(fc["HasAttachment" + 0]);
                cop001.MachineCode = fc["ddlMachineCode" + 0];
                var temp1 = string.Empty;
                var temp = !string.IsNullOrEmpty(fc["WeekNo" + 0]) ? fc["WeekNo" + 0] : string.Empty;
                if (!string.IsNullOrEmpty(temp))
                {
                    temp1 = temp.Substring(5, 1);
                    cop001.WeekNo = Convert.ToDouble(temp1);
                }
                // make changes from here 

                var isExist = false;
                if (db.COP001.Where(x => x.Project == cop001.Project &&
                                    x.ItemId == cop001.ItemId &&
                                    x.BU == cop001.BU).Any())
                {
                    isExist = true;
                    // if you are uncommetting line 497 to 564 code then commetted below 3 line code.
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.Message;
                    return Json(objResponseMsg);
                }

                var itemDetails = GetListItemIdEnt(cop001.Project, cop001.ItemId);
                if (itemDetails != null)
                {
                    var BOMQnty = Convert.ToInt32(itemDetails.FirstOrDefault().BOMQty);

                    if (BOMQnty > 0)
                    {
                        IEMQSEntitiesContext context = null;
                        for (int i = 1; i <= BOMQnty; i++)
                        {
                            context = new IEMQSEntitiesContext();
                            cop001.SequenceNo = i;
                            cop001.CreatedBy = objClsLoginInfo.UserName;
                            cop001.CreatedOn = DateTime.Now;
                            context.COP001.Add(cop001);
                            context.SaveChanges();
                            context.SP_COP_FORMULA_UPDATE(cop001.HeaderId, "ID_BlankDia", Convert.ToString(cop001.ID_BlankDia), objClsLoginInfo.UserName);
                            //db.SP_COP_FORMULA_UPDATE(cop001.HeaderId, "ID_BlankDia", Convert.ToString(cop001.ID_BlankDia), objClsLoginInfo.UserName);
                            string No = clsImplementationEnum.Dispatch.No.GetStringValue();
                            if (cop001.Groove_On_GF == No)
                            {
                                var Groove_Weld_KG = context.COP001.Where(x => x.HeaderId == cop001.HeaderId).FirstOrDefault().Groove_Weld_KG;
                                Groove_Weld_KG = "0";
                                context.SaveChanges();
                            }
                            context.SaveChanges();
                            context = null;
                        }

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Data save successfully.";
                        // below code is for add or remove base on existing or not existing BOM Qnty.

                        //var existingRecordCount = 0;
                        //if (isExist)
                        //{
                        //    existingRecordCount = db.COP001.Where(x => x.Project == cop001.Project &&
                        //           x.ItemId == cop001.ItemId &&
                        //           x.BU == cop001.BU).Count();
                        //}
                        //if (BOMQnty != existingRecordCount)
                        //{
                        //    if (existingRecordCount >= 0)
                        //    {
                        //        for (int i = 1; i <= BOMQnty; i++)
                        //        {
                        //            cop001.SequenceNo = i;
                        //            cop001.CreatedBy = objClsLoginInfo.UserName;
                        //            cop001.CreatedOn = DateTime.Now;
                        //            db.COP001.Add(cop001);
                        //            db.SaveChanges();
                        //        }
                        //        objResponseMsg.Key = true;
                        //        objResponseMsg.Value = "Data save successfully.";
                        //    }
                        //    else if (BOMQnty > existingRecordCount)
                        //    {
                        //        var lastSequenceNo = db.COP001.Where(x => x.Project == cop001.Project &&
                        //                x.ItemId == cop001.ItemId &&
                        //                x.BU == cop001.BU).LastOrDefault().SequenceNo;
                        //        var updatedBOMQnty = BOMQnty - lastSequenceNo;

                        //        for (int i = 1; i <= updatedBOMQnty; i++)
                        //        {
                        //            cop001.SequenceNo = lastSequenceNo + i;
                        //            cop001.CreatedBy = objClsLoginInfo.UserName;
                        //            cop001.CreatedOn = DateTime.Now;
                        //            db.COP001.Add(cop001);
                        //            db.SaveChanges();
                        //        }


                        //        objResponseMsg.Key = true;
                        //        objResponseMsg.Value = clsImplementationMessage.CommonMessages.DataUpdate;
                        //    }
                        //    else if (BOMQnty < existingRecordCount)
                        //    {
                        //        var lastSequenceNo = db.COP001.Where(x => x.Project == cop001.Project &&
                        //               x.ItemId == cop001.ItemId &&
                        //               x.BU == cop001.BU).LastOrDefault().SequenceNo;
                        //        var updatedBOMQnty = lastSequenceNo - BOMQnty;

                        //        for (int i = 1; i <= updatedBOMQnty; i++)
                        //        {
                        //            var lastData = db.COP001.Where(x => x.Project == cop001.Project &&
                        //                x.ItemId == cop001.ItemId &&
                        //                x.BU == cop001.BU).LastOrDefault();
                        //            db.COP001.Remove(lastData);
                        //        }

                        //        objResponseMsg.Key = true;
                        //        objResponseMsg.Value = clsImplementationMessage.CommonMessages.DataUpdate;
                        //    }
                        //}
                        //else
                        //{
                        //    objResponseMsg.Key = false;
                        //    objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.Message;
                        //}
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "BOM Quantity is Zero (0).";
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "BOM Quantity is not available.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg);
        }

        [HttpPost]
        public ActionResult GetComponentDataById(int headerId)
        {
            try
            {
                var component = db.COP001.Find(headerId);
                BindDropDown(component.FromDate);
                ViewData["Customer"] = Manager.GetCustomerProjectWise(component.Project).Name;
                return PartialView("ComponentFieldsPartial", component);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(ex.Message);
            }
        }

        [HttpPost]
        public ActionResult GetQualityProjectsByCOP(string Project, string BU, string search = "")
        {
            var plngrp = db.COM001.Where(x => x.t_cprj == Project).Select(x => x.t_plgr).FirstOrDefault();
            List<ddlValue> lstQualityProject = (from a in db.COM001
                                                join b in db.QMS010 on a.t_cprj equals b.Project
                                                where a.t_plgr == plngrp && b.Location == objClsLoginInfo.Location && (search == "" || b.QualityProject.ToLower().Contains(search.ToLower()))
                                                select new ddlValue { id = b.QualityProject, text = b.QualityProject }).Distinct().ToList();//&& i.CreatedBy.Equals(username,StringComparison.OrdinalIgnoreCase)

            //List<ddlValue> lstQualityProject = db.QMS010.Where(i => (search == "" || i.QualityProject.ToLower().Contains(search.ToLower()))
            //                                                     && i.Project == Project
            //                                                     && i.Location == objClsLoginInfo.Location
            //                                                   //&& i.BU == BU
            //                                                   ).Select(i => new ddlValue { id = i.QualityProject, text = i.QualityProject }).Distinct().ToList();//&& i.CreatedBy.Equals(username,StringComparison.OrdinalIgnoreCase)
            return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
        }

        public void BindDropDown(DateTime? fromDate)
        {
            ViewData["DrpBU"] = new string[] { clsImplementationEnum.BU.RPV.GetStringValue(),
                                               clsImplementationEnum.BU.HTE.GetStringValue() };

            ViewData["DrpCategory"] = new string[] { clsImplementationEnum.COPCategory.LessThan14.GetStringValue(),
                                                     //clsImplementationEnum.COPCategory.GreterThan4LessThan14.GetStringValue(),
                                                     clsImplementationEnum.COPCategory.GreaterThan14.GetStringValue() };

            ViewData["DrpProcss"] = new string[] { clsImplementationEnum.Process.ESSC.GetStringValue(),
                                                   clsImplementationEnum.Process.NonESSC.GetStringValue() };

            ViewData["DrpTypeofComponent"] = new string[] { clsImplementationEnum.TypeofComponent.BlindFlange.GetStringValue(), clsImplementationEnum.TypeofComponent.Pipe.GetStringValue(),
            clsImplementationEnum.TypeofComponent.NozzleForging.GetStringValue(),clsImplementationEnum.TypeofComponent.Elbow.GetStringValue(),clsImplementationEnum.TypeofComponent.Flange.GetStringValue()};

            ViewData["DrpMOC"] = new string[] { clsImplementationEnum.MOC.CS.GetStringValue(),
            clsImplementationEnum.MOC.LAS.GetStringValue(),clsImplementationEnum.MOC.SS.GetStringValue()};

            ViewData["DrpWPForID"] = new string[] { clsImplementationEnum.WeldProcessForCOP.FCFCAW.GetStringValue(),
            clsImplementationEnum.WeldProcessForCOP.SLGTAW.GetStringValue(),clsImplementationEnum.WeldProcessForCOP.ESESSC.GetStringValue(),
            clsImplementationEnum.WeldProcessForCOP.ELBFCAW.GetStringValue(),clsImplementationEnum.WeldProcessForCOP.PCGTAW.GetStringValue()};

            ViewData["DrpWPForFace"] = new string[] { clsImplementationEnum.WeldProcessForCOP.SMSMAW.GetStringValue(),
            clsImplementationEnum.WeldProcessForCOP.MEGTAW.GetStringValue(),clsImplementationEnum.WeldProcessForCOP.MESESSC.GetStringValue()};

            ViewData["DrpWPForGroove"] = new string[] { clsImplementationEnum.WeldProcessForCOP.SMSMAW.GetStringValue(),
            clsImplementationEnum.WeldProcessForCOP.MEGTAW.GetStringValue()};

            ViewData["DrpWPForBack"] = new string[] { clsImplementationEnum.WeldProcessForCOP.FCFCAW.GetStringValue(),
            clsImplementationEnum.WeldProcessForCOP.MEGGTAW.GetStringValue(),clsImplementationEnum.WeldProcessForCOP.MESESSC.GetStringValue(),
            clsImplementationEnum.WeldProcessForCOP.SMSMAW.GetStringValue()};

            ViewData["CommonStatus"] = new string[] { clsImplementationEnum.COP_CommonStatus.NA.GetStringValue(), clsImplementationEnum.COP_CommonStatus.NS.GetStringValue(),
            clsImplementationEnum.COP_CommonStatus.IP.GetStringValue(),clsImplementationEnum.COP_CommonStatus.Co.GetStringValue(),clsImplementationEnum.COP_CommonStatus.Yes.GetStringValue()};

            ViewData["DrpComponentAssembly"] = new string[] { clsImplementationEnum.ComponentAssembly.Component.GetStringValue(),
                                                             clsImplementationEnum.ComponentAssembly.Assembly.GetStringValue() };


            ViewData["DrpDispatch"] = new string[] { clsImplementationEnum.Dispatch.Yes.GetStringValue(),
                                                    clsImplementationEnum.Dispatch.No.GetStringValue()};

            ViewData["DrpOverlay"] = new string[] { clsImplementationEnum.Overlay.Yes.GetStringValue(), clsImplementationEnum.Overlay.No.GetStringValue() };

            DateTime now = fromDate.HasValue ? fromDate.Value : DateTime.Now;
            var startDate = new DateTime(now.Year, now.Month, 1);
            var endDate = startDate.AddMonths(36).AddDays(-1);
            var Pipelined = new List<CurrentMonthWeek>() { };
            int month = startDate.Month;
            int i = 1;
            foreach (WeekRange wr in GetWeekRange(startDate, endDate))
            {
                if (month != wr.Start.Month)
                {
                    i = 1;
                    month = wr.Start.Month;
                }
                var week = new CurrentMonthWeek()
                {
                    WeekNo = Convert.ToString(i),
                    Date = "Week " + i + " [" + wr.Start.Date.ToShortDateString() + " to " + wr.End.ToShortDateString() + "]",
                    FromDate = wr.Start.Date.ToShortDateString(),
                    ToDate = wr.End.Date.ToShortDateString(),

                };
                Pipelined.Add(week);
                i++;
            }
            ViewData["DrpPipelined"] = Pipelined.ToArray();

        }

        public ActionResult UpdateComponentData(int headerId, string columnName, string value)
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                db.SP_COMMON_TABLE_UPDATE("COP001", headerId, "HeaderId", columnName, value, objClsLoginInfo.UserName);
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg);
        }

        public ActionResult GetContractorDetailsByProjectCode(string projectCode)
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var contractor = db.COM005.Where(i => i.t_sprj == projectCode).FirstOrDefault();
                var customer = Manager.GetCustomerProjectWise(projectCode);
                objResponseMsg.Key = true;
                return Json(new { Response = objResponseMsg, Contractor = contractor, Customer = customer });

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(new { Response = objResponseMsg });
        }

        public JsonResult GetItemIdForProject(string project, string ItemNo = "")
        {
            var itemDetails = GetListItemIdEnt(project, ItemNo);
            return Json(itemDetails, JsonRequestBehavior.AllowGet);
        }

        public List<SP_COP_GET_LIST_ITEM_Result> GetListItemIdEnt(string project, string ItemNo = "")
        {
            return db.SP_COP_GET_LIST_ITEM(project, ItemNo).ToList();
        }

        public JsonResult GetPartForItemProject(string project, string item, string term)
        {
            var partDetails = db.SP_COP_GET_PART_DESCRIPTION_PROJECT(project, item, term).ToList();
            return Json(partDetails, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPlannedReceiptDate(string project, string item)
        {
            var plannedReceiptData = db.SP_COP_GET_PlannedReceiptDate(item, project).ToList();
            return Json(plannedReceiptData, JsonRequestBehavior.AllowGet);
        }

        public IEnumerable<WeekRange> GetWeekRange(DateTime dtStart, DateTime dtEnd)
        {
            DateTime fWeekStart, dt, fWeekEnd;
            int wkCnt = 1;

            if (dtStart.DayOfWeek != DayOfWeek.Sunday)
            {
                fWeekStart = dtStart.AddDays(7 - (int)dtStart.DayOfWeek);
                fWeekEnd = fWeekStart.AddDays(-1);
                IEnumerable<WeekRange> ranges = getMonthRange(new WeekRange(dtStart, fWeekEnd, dtStart.Month, wkCnt++));
                foreach (WeekRange wr in ranges)
                {
                    yield return wr;
                }
                wkCnt = ranges.Last().WeekNo + 1;

            }
            else
            {
                fWeekStart = dtStart;
            }


            for (dt = fWeekStart.AddDays(6); dt <= dtEnd; dt = dt.AddDays(7))
            {


                IEnumerable<WeekRange> ranges = getMonthRange(new WeekRange(fWeekStart, dt, fWeekStart.Month, wkCnt++));
                foreach (WeekRange wr in ranges)
                {
                    yield return wr;
                }
                wkCnt = ranges.Last().WeekNo + 1;
                fWeekStart = dt.AddDays(1);


            }

            if (dt > dtEnd)
            {

                IEnumerable<WeekRange> ranges = getMonthRange(new WeekRange(fWeekStart, dtEnd, dtEnd.Month, wkCnt++));
                foreach (WeekRange wr in ranges)
                {
                    yield return wr;
                }
                wkCnt = ranges.Last().WeekNo + 1;

            }

        }

        public IEnumerable<WeekRange> getMonthRange(WeekRange weekRange)
        {

            List<WeekRange> ranges = new List<WeekRange>();

            if (weekRange.Start.Month != weekRange.End.Month)
            {
                DateTime lastDayOfMonth = new DateTime(weekRange.Start.Year, weekRange.Start.Month, 1).AddMonths(1).AddDays(-1);
                ranges.Add(new WeekRange(weekRange.Start, lastDayOfMonth, weekRange.Start.Month, weekRange.WeekNo));
                ranges.Add(new WeekRange(lastDayOfMonth.AddDays(1), weekRange.End, weekRange.End.Month, weekRange.WeekNo + 1));

            }
            else
            {
                ranges.Add(weekRange);
            }

            return ranges;

        }

        public JsonResult UpdateIMRStatus()
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {

                //var POData = db.COP001.Where(x => x.POSeq == null && x.ReceiptNo == null && x.ReceiptSeq == null).GroupBy(l => l.PONo)
                var POData = db.COP001.GroupBy(l => new { l.PONo, l.POLine })
                            .Select(x => new
                            {
                                PONo = x.FirstOrDefault().PONo,
                                POLine = x.FirstOrDefault().POLine,
                                Qty = x.Count(),
                            }).Where(x => x.PONo != null && x.POLine != null).ToList();

                var listPOQty = db.SP_COP_Update_IMR_Status("", "").ToList();
                if (POData.Any())
                {
                    foreach (var data in POData)
                    {
                        //var POQty = db.SP_COP_Update_IMR_Status(data.PONo, Convert.ToString(data.POLine)).ToList();
                        var POQty = listPOQty.Where(i => i.PONo == data.PONo && i.POLine == data.POLine).ToList();
                        if (POQty.Any())
                        {
                            foreach (var poQtyData in POQty)
                            {
                                if (data.Qty > poQtyData.ApprovedQty)
                                {
                                    int qty = Convert.ToInt32(poQtyData.ApprovedQty);
                                    db.COP001.Where(x => x.PONo == poQtyData.PONo && x.POLine == poQtyData.POLine
                                     && x.POSeq != poQtyData.POSeq && x.ReceiptSeq != poQtyData.ReceiptSeq).OrderBy(x => x.SequenceNo).Take(qty).ToList()
                                    .ForEach(a =>
                                    {
                                        a.POSeq = poQtyData.POSeq;
                                        a.ReceiptSeq = poQtyData.ReceiptSeq;
                                        a.ReceiptNo = poQtyData.ReceiptNo;
                                        a.IMRStatus = "IMR OK";
                                        a.EditedBy = objClsLoginInfo.UserName;
                                        a.EditedOn = DateTime.Now;
                                    });
                                    //db.SaveChanges();
                                }
                                else if (poQtyData.ApprovedQty >= data.Qty)
                                {
                                    db.COP001.Where(x => x.PONo == poQtyData.PONo && x.POLine == poQtyData.POLine
                                    && x.POSeq != poQtyData.POSeq && x.ReceiptSeq != poQtyData.ReceiptSeq).OrderBy(x => x.SequenceNo).ToList()
                                      .ForEach(a =>
                                      {
                                          a.POSeq = poQtyData.POSeq; a.ReceiptSeq = poQtyData.ReceiptSeq; a.ReceiptNo = poQtyData.ReceiptNo;
                                          a.IMRStatus = "IMR OK"; a.EditedBy = objClsLoginInfo.UserName; a.EditedOn = DateTime.Now;
                                      });
                                    //db.SaveChanges();
                                }
                            }
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "Status has updated";
                        }
                    }
                    db.SaveChanges();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Record found!";
                }


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg);
        }

        public JsonResult UpdateDeselectAll()
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string yes = clsImplementationEnum.Overlay.Yes.GetStringValue().ToString();
                var CompleteData = db.COP001.Where(x => x.DISP == yes).ToList();

                if (CompleteData.Count > 0)
                {
                    CompleteData.ForEach(u =>
                    {
                        u.WIP = false;
                        u.IsPlanned = false;
                    });
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "SuccessFully DeSelected All";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Record found!";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg);
        }

        public JsonResult btnUpdateSeamStatus()
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var ff = db.SP_IS_SEAM_FULLY_READY_TO_OFFER();

                if (ff > 0)
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Status has Updated";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Record found!";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg);
        }

        public ActionResult UpdateAttachmentUpload(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();

            try
            {
                //var sourcePath = "COP001/" + HeaderId;
                //var existing = (new clsFileUpload()).GetDocuments(sourcePath);

                var objCOP001 = db.COP001.Where(i => i.HeaderId == HeaderId).FirstOrDefault();

                Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                //if (existing.Count() > 0)
                if (_objFUC.CheckAnyDocumentsExits("COP001", HeaderId))
                {
                    objCOP001.HasAttachment = true;
                    objResponseMsg.Value = "true";
                }
                else
                {
                    objCOP001.HasAttachment = false;
                    objResponseMsg.Value = "false";
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType)
        {
            var objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                var lst = db.SP_COP_GET_INDEX_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Data Found";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                var dtPlannedReceipt = new DateTime();
                var data = (from uc in lst
                            select new
                            {
                                Project = uc.Project,
                                QualityProject = uc.QualityProject,
                                Customer = uc.Customer,
                                BU = uc.BU,
                                ItemId = uc.ItemId,
                                Description = uc.Description,
                                Mark = uc.Mark,
                                TypeOfComponent = uc.TypeOfComponent,
                                WIP = (uc.WIP ? "Yes" : "No"),
                                Planned = (uc.IsPlanned ? "Yes" : "No"),
                                Overlay = (uc.Overlay.HasValue
                                             ? (uc.Overlay == true
                                                  ? "Yes"
                                                  : "No")
                                             : ""),
                                MOC = uc.MOC,
                                ID_BlankDia = Convert.ToString(uc.ID_BlankDia),
                                Category = Convert.ToString(uc.Category),
                                Length = Convert.ToString(uc.Length),
                                ID_Weld_Height = Convert.ToString(uc.ID_Weld_Height),
                                GF_OD = Convert.ToString(uc.GF_OD),
                                GF_Weld_Height = Convert.ToString(uc.GF_Weld_Height),
                                BF_OD = Convert.ToString(uc.BF_OD),
                                BF_Weld_Height = Convert.ToString(uc.BF_Weld_Height),
                                Groove_On_GF = Convert.ToString(uc.Groove_On_GF),
                                ID_Weld_KG = Convert.ToString(uc.ID_Weld_KG),
                                WP_For_ID = Convert.ToString(uc.WP_For_ID),
                                Face_Weld_KG = Convert.ToString(uc.Face_Weld_KG),
                                WP_For_Face = Convert.ToString(uc.WP_For_Face),
                                Groove_Weld_KG = Convert.ToString(uc.Groove_Weld_KG),
                                WP_For_Groove = Convert.ToString(uc.WP_For_Groove),
                                Back_Face_Weld_KG = Convert.ToString(uc.Back_Face_Weld_KG),
                                WP_For_BackFace = Convert.ToString(uc.WP_For_BackFace),
                                Total_Weld_KG = Convert.ToString(uc.Total_Weld_KG),
                                Part = uc.Part,
                                //Description = uc.Description,
                                //Process = uc.Procss,
                                Comp_Assly = uc.Comp_Assly,
                                //TypeOfComponent = uc.TypeOfComponent,
                                PlannedReceipt = (DateTime.TryParse(uc.PlannedReceipt + "", out dtPlannedReceipt) ? dtPlannedReceipt.ToString("dd/MM/yyyy") : uc.PlannedReceipt),
                                IMRStatus = uc.IMRStatus,
                                SeamStatus = uc.SeamStatus,
                                RequestedOn = uc.RequestedOn.HasValue ? uc.RequestedOn.Value.ToString("dd/MM/yyyy") : "",
                                CycleTime = Convert.ToString(uc.CycleTime),
                                //Week = uc.FromDate.HasValue ? "Week " + Convert.ToString(uc.WeekNo) + " [" + uc.FromDate.Value.ToShortDateString() + " to " + uc.ToDate.Value.ToShortDateString() + "]" : "",
                                Start = uc.Start,
                                BM_NDT = uc.BM_NDT,
                                ID_OL = uc.BL_OL,
                                //BL_PT = uc.BL_PT,
                                BF_OL = uc.OL,
                                Corner_OL = uc.GF_BL_PT,
                                GF_OL = uc.GF_OL,
                                M_Cing = uc.M_Cing,
                                NDT = uc.NDT,
                                ICS = uc.ICS,
                                Dispatch = uc.DISP,
                                DispatchDate = uc.DispatchDate.HasValue ? uc.DispatchDate.Value.ToString("dd/MM/yyyy") : "",
                                DeliveryLocation = uc.deliveryLocation,
                                PWHT = uc.PWHT,
                                AfterPWHTDispatch = uc.AfterPWHTDispatch,
                                Remarks = uc.Remarks,
                                MachiningLocation = uc.MachiningLocation,
                                MCStartDate = uc.MCDatetime.HasValue ? uc.MCDatetime.Value.ToString("dd/MM/yyyy") : "",
                                MCingDays = uc.MCingDays,
                                //HasAttachment = uc.HasAttachment ? "Yes" : "No"
                                Machine = uc.MachineCode,
                                DeleteRemark = uc.DeleteRemarks
                            }).ToList();

                strFileName = Helper.GenerateExcel(data, objClsLoginInfo.UserName);
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdateDetails(int id, string columnName, string columnValue)
        {
            var objResponseMsg = new ResponceMsgWithObject();
            try
            {
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    if (columnName.ToLower() == "weekno")
                    {
                        columnValue = columnValue.Length > 0 ? columnValue.Substring(5, 1) : "";
                    }
                    else if (columnName.ToLower() == "fromdate" || columnName.ToLower() == "todate")
                    {
                        if (!string.IsNullOrEmpty(columnValue))
                        {
                            var splitDate = columnValue.Split('/');
                            columnValue = splitDate[2] + "/" + splitDate[1] + "/" + splitDate[0];
                        }
                    }
                    else if (columnName.ToLower() == "mark")
                    {
                        columnValue = columnValue.ToUpper();
                    }
                    else if (columnName.ToLower() == "m_cing")
                    {
                        if (columnValue == "NS")
                        {
                            Mcinglogic(id);
                        }
                    }
                    else if (columnName.ToLower() == "requestedon")
                    {
                        if (!string.IsNullOrEmpty(columnValue))
                        {
                            var splitDate = columnValue.Split('/');
                            columnValue = splitDate[2] + "/" + splitDate[1] + "/" + splitDate[0];
                            db.SP_COMMON_TABLE_UPDATE("COP001", id, "HeaderId", columnName, columnValue, objClsLoginInfo.UserName);
                            Mcinglogic(id);
                        }
                    }
                    else if (columnName.ToLower() == "part")
                    {
                        db.SP_COMMON_TABLE_UPDATE("COP001", id, "HeaderId", "IsPartUpdated", "1", objClsLoginInfo.UserName);
                    }
                    else if (columnName.ToLower() == "dispatchdate")
                    {
                        db.SP_COMMON_TABLE_UPDATE("COP001", id, "HeaderId", columnName, columnValue, objClsLoginInfo.UserName);
                        Mcinglogic(id);
                    }

                    db.SP_COMMON_TABLE_UPDATE("COP001", id, "HeaderId", columnName, columnValue, objClsLoginInfo.UserName);
                    var objsp = db.SP_COP_FORMULA_UPDATE(id, columnName, columnValue, objClsLoginInfo.UserName).FirstOrDefault();

                    objResponseMsg.objFormula = objsp;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();

                }
                else
                {
                    //if (columnName.ToLower() == "remarks" || columnName == "QualityProject")
                    //{
                    db.SP_COMMON_TABLE_UPDATE("COP001", id, "HeaderId", columnName, columnValue, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                    //}
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public void Mcinglogic(int headerid)
        {
            var objcop001 = db.COP001.Where(x => x.HeaderId == headerid).FirstOrDefault();
            if (objcop001 != null)
            {
                if (objcop001.DispatchDate != null)
                {
                    objcop001.MCDatetime = Convert.ToDateTime(objcop001.DispatchDate).AddDays(-5);
                }
                else
                {
                    if (objcop001.RequestedOn != null)
                    {
                        objcop001.MCDatetime = Convert.ToDateTime(objcop001.RequestedOn).AddDays(-5);
                    }
                    else
                    {
                        objcop001.MCDatetime = null;
                    }
                }
                db.SaveChanges();
            }
        }

        [NonAction]
        public static string GenerateCheckbox(int rowId, string columnName, bool columnValue = false, bool isDisabled = false)
        {
            string htmlControl = "";
            string inputID = columnName + "" + rowId.ToString();
            bool inputValue = columnValue;

            htmlControl = "<input type=\"checkbox\" id=\"" + inputID + "\"" + (inputValue ? " checked " : "") + " name=\"" + inputID + "\" colname=\"" + columnName + "\"" + (isDisabled ? " disabled = disabled" : "") + "/>";
            return htmlControl;
        }

        public static string MultiSelectDropdown(List<SelectItemList> list, int rowID, string selectedValue, bool Disabled = false, string OnChangeEvent = "", string OnBlurEvent = "", string ColumnName = "")
        {
            string multipleSelect = string.Empty;
            string[] arrayVal = { };

            try
            {
                if (!string.IsNullOrWhiteSpace(selectedValue))
                {
                    arrayVal = selectedValue.Split(',');
                }
                // var repeatedVal = arrayVal.GroupBy(x => x).ToList();

                multipleSelect += "<select data-name='ddlmultiple' data-oldvalue='" + selectedValue + "' name='ddlmultiple" + rowID + "' id='ddlmultiple" + rowID + "' data-lineid='" + rowID + "' multiple='multiple'  style='width: 100 % ' colname='" + ColumnName + "' class='form-control multiselect-drodown' " + (Disabled ? "disabled " : "") + (OnChangeEvent != string.Empty ? "onchange='" + OnChangeEvent + "'" : "") + (OnBlurEvent != string.Empty ? " onblur='" + OnBlurEvent + "'" : "") + " >";
                foreach (var val in arrayVal)
                {
                    if (val != null)
                    {
                        var item = list.FirstOrDefault(w => w.id == val);
                        if (item != null)
                            multipleSelect += "<option value='" + item.id + "' selected" + " >" + item.text + "</option>";
                    }
                }
                foreach (var item in list)
                {
                    if (arrayVal.Contains(item.id + ""))
                        continue;

                    multipleSelect += "<option value='" + item.id + "'  " + ((arrayVal.Contains(item.id)) ? " selected" : "") + " >" + item.text + "</option>";
                    //foreach (var val in repeatedVal)
                    //{
                    //    if (item.id == val.Key)
                    //    {
                    //        for (int i = 0; i < val.Count() - 1; i++)
                    //        {
                    //            multipleSelect += "<option value='" + item.id + "' data-new='true' selected" + " >" + item.text + "</option>";
                    //        }
                    //    }
                    //}
                }
                multipleSelect += "</select>";
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return multipleSelect;
        }

        public static string MultiSelectDropdownForMachine(List<SelectItemList> list, int rowID, string selectedValue, bool Disabled = false, string OnChangeEvent = "", string OnBlurEvent = "", string ColumnName = "")
        {
            string multipleSelect = string.Empty;
            string[] arrayVal = { };

            try
            {
                if (!string.IsNullOrWhiteSpace(selectedValue))
                {
                    arrayVal = selectedValue.Split(',');
                }
                // var repeatedVal = arrayVal.GroupBy(x => x).ToList();

                multipleSelect += "<select data-name='ddlMachineCode' data-oldvalue='" + selectedValue + "' name='ddlMachineCode" + rowID + "' id='ddlMachineCode" + rowID + "' data-lineid='" + rowID + "' multiple='multiple'  style='width: 100 % ' colname='" + ColumnName + "' class='form-control multiselect-drodown' " + (Disabled ? "disabled " : "") + (OnChangeEvent != string.Empty ? "onchange='" + OnChangeEvent + "'" : "") + (OnBlurEvent != string.Empty ? " onblur='" + OnBlurEvent + "'" : "") + " >";
                foreach (var val in arrayVal)
                {
                    if (val != null)
                    {
                        var item = list.FirstOrDefault(w => w.id == val);
                        if (item != null)
                            multipleSelect += "<option value='" + item.id + "' selected" + " >" + item.text + "</option>";
                    }
                }
                foreach (var item in list)
                {
                    if (arrayVal.Contains(item.id + ""))
                        continue;

                    multipleSelect += "<option value='" + item.id + "'  " + ((arrayVal.Contains(item.id)) ? " selected" : "") + " >" + item.text + "</option>";
                    //foreach (var val in repeatedVal)
                    //{
                    //    if (item.id == val.Key)
                    //    {
                    //        for (int i = 0; i < val.Count() - 1; i++)
                    //        {
                    //            multipleSelect += "<option value='" + item.id + "' data-new='true' selected" + " >" + item.text + "</option>";
                    //        }
                    //    }
                    //}
                }
                multipleSelect += "</select>";
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return multipleSelect;
        }
        public string GetCOPDashboardLink()
        {
            var PowerBIReportURL = db.CONFIG.Where(i => i.Key == "PowerBIReportURL").FirstOrDefault().Value;
            PowerBIReportURL += "Power%20BI%20Reports/IEMQS/COP/COPDashboard";
            return PowerBIReportURL;
        }

        [HttpPost]
        public ActionResult GetQRcode(int headerId)
        {
            var objResponseMsg = new QrResponse();
            objResponseMsg.Key = false;

            try
            {
                var QRCodeURL = db.CONFIG.Where(i => i.Key == "QRCodeURL").FirstOrDefault().Value;

                if (string.IsNullOrEmpty(QRCodeURL))
                {
                    objResponseMsg.Value = "QRCode URL Is Not Maintained";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                var objcop001 = db.COP001.Where(x => x.HeaderId == headerId).Select(x => new { QProject = x.QualityProject, Item = x.ItemId, Mark = x.Mark, Part = x.Part }).FirstOrDefault();

                if (objcop001 != null)
                {
                    if (string.IsNullOrEmpty(objcop001.QProject))
                    {
                        objResponseMsg.Value = "Quality Project Is Not Maintained";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }

                    if (string.IsNullOrEmpty(objcop001.Item))
                    {
                        objResponseMsg.Value = "Item Is Not Maintained";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }

                    if (string.IsNullOrEmpty(objcop001.Mark))
                    {
                        objResponseMsg.Value = "Mark Is Not Maintained";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }

                    if (string.IsNullOrEmpty(objcop001.Part))
                    {
                        objResponseMsg.Value = "Part Is Not Maintained";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }

                    objResponseMsg.QRURL = QRCodeURL;
                    objResponseMsg.QProject = objcop001.QProject;
                    objResponseMsg.Item = objcop001.Item;
                    objResponseMsg.Mark = objcop001.Mark;
                    objResponseMsg.Part = objcop001.Part;
                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #region Machine Camera Details
        public ActionResult MachineCamera()
        {
            ViewBag.IsDisplay = false;
            return View();
        }

        [HttpPost]
        public ActionResult LoadMachineCameraGridData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                strWhereCondition += "1=1 ";

                #endregion

                //search Condition 

                string[] columnName = { "MachineCode", "MachineName", "CameraURL" };
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                var lstResult = db.SP_COP_GET_MACHINE_CAMERA_DETAILS(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();
                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.Id),
                                uc.MachineCode,
                                uc.MachineName,
                                uc.CameraURL,
                                ("<span class='editable'><a id='btnDelete' name='btnDelete' title='Delete' class='blue action' onclick='RemoveMachineCamera( "+ uc.Id+")'>"+
                                        "<i class='fa fa-trash'></i></span>")
                          }).ToList();

                // data.Insert(0, null);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetInlineEditableRowMachineCamera(int id, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();

                if (id == 0)
                {
                    var QPitems = new SelectList(new List<string>());
                    int newRecordId = 0;
                    var newRecord = new[] {
                                        newRecordId.ToString(),
                                        Helper.GenerateTextbox(-1, "MachineCode","", "",false, "","10","form-control"),
                                        Helper.GenerateTextbox(-1, "MachineName","", "",false, "","100","form-control"),
                                        Helper.GenerateTextbox(-1, "CameraURL","", "",false, "","500","form-control"),
                                        Helper.GenerateGridButtonNew(newRecordId, "Save", "Save Rocord", "fa fa-save", "AddMachineCamera()" ),
                                    };
                    data.Add(newRecord);
                }
                else
                {
                    var lstResult = db.SP_COP_GET_MACHINE_CAMERA_DETAILS(1, 0, "", "Id = " + id).Take(1).ToList();
                    if (isReadOnly)
                    {
                        data = (from uc in lstResult
                                select new[]
                               {
                                Convert.ToString(uc.Id),
                                uc.MachineCode,
                                uc.MachineName,
                                uc.CameraURL,
                                ""
                          }).ToList();
                    }
                    else
                    {

                        data = (from uc in lstResult
                                select new[] {
                                        uc.Id.ToString(),
                                        Helper.GenerateTextbox(uc.Id, "MachineCode",Convert.ToString(uc.MachineCode), "UpdateData(this, "+ uc.Id+",true)",true, "","10","form-control editable"),
                                        Helper.GenerateTextbox(uc.Id, "MachineName",Convert.ToString(uc.MachineName), "UpdateData(this, "+ uc.Id+",true)", false, "", "100","form-control editable"),
                                        Helper.GenerateTextbox(uc.Id, "CameraURL",Convert.ToString(uc.CameraURL), "UpdateData(this, "+ uc.Id+",false)",false, "","500","form-control editable"),
                                        ("<span class='editable'><a id='Cancel' name='Cancel' title='Cancel' class='blue action' onclick='ReloadGrid()'>"+
                                        "<i class='fa fa-close'></i></span>"),
                          }).ToList();
                    }
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SaveMachineCamera(COP002 objCOP002)
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (db.COP002.Any(i => i.MachineCode.ToLower() == objCOP002.MachineCode.ToLower() && i.Id != objCOP002.Id))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Machine Code Aready Exist!";
                    objResponseMsg.IsInformation = true;
                    return Json(objResponseMsg);
                }

                if (objCOP002.Id == 0)
                {
                    objCOP002.CreatedBy = objClsLoginInfo.UserName;
                    objCOP002.CreatedOn = DateTime.Now;

                    db.COP002.Add(objCOP002);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Machine And Camera Details Save Successfully!";
                }
                else
                {
                    objCOP002.EditedBy = objClsLoginInfo.UserName;
                    objCOP002.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Machine And Camera Details Updated Successfully!";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateMachineCameraDetails(int id, string columnName, string columnValue)
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (columnName == "MachineName" && columnValue.Trim() == "")
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Machine Name Should Not Be Blank!";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                db.SP_COMMON_TABLE_UPDATE("COP002", id, "Id", columnName, columnValue, objClsLoginInfo.UserName);
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RemoveMachineCamera(int id)
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (!db.COP002.Any(i => i.Id == id))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Record Not Exist!";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                //Here check logic for camera used in COP001 table

                var objCOP002 = db.COP002.Where(i => i.Id == id).FirstOrDefault();
                db.COP002.Remove(objCOP002);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Machine Camera Detail Removed Successfully!";

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }

    public class QrResponse : clsHelper.ResponseMsgWithStatus
    {
        public string QRURL { get; set; }
        public string QProject { get; set; }
        public string Item { get; set; }
        public string Mark { get; set; }
        public string Part { get; set; }
    }

    #region Classes

    public class ResponceMsgWithObject : clsHelper.ResponseMsgWithStatus
    {
        public SP_COP_FORMULA_UPDATE_Result objFormula;
    }

    #endregion

    public struct WeekRange
    {
        public DateTime Start;
        public DateTime End;
        public int MM;
        public int WeekNo;

        public WeekRange(DateTime _start, DateTime _end, int _mm, int _weekNo)
        {
            Start = _start;
            End = _end;
            MM = _mm;
            WeekNo = _weekNo;
        }
    }

    public partial class resCOP001 : clsHelper.ResponseMsgWithStatus
    {
        public COP001 objcop { get; set; }
        public string D_DispatchDate { get; set; }
        public string D_RequestedOn { get; set; }
        public string D_MCDatetime { get; set; }
        public List<SelectItemList> maclocations { get; set; }
        public List<SelectItemList> machines { get; set; }
    }


    public class CurrentMonthWeek
    {
        public string WeekNo { get; set; }
        public string Date { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }
}