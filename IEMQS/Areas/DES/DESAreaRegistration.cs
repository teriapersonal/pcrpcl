﻿using System.Web.Mvc;

namespace IEMQS.Areas.DES
{
    public class DESAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "DES";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "DES_default",
                "DES/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}