﻿using IEMQS.Areas.DES.Models;
using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using IEMQS.DESServices;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.DES.Controllers
{
    [IEMQSImplementation.clsBase.SessionExpireFilter]
    [Models.EncryptedActionParameter]
    public class ProjectController : Controller
    {
        IProjectService _ProjectService;
        ICommonService _CommonService;
        IProductService _ProductService;
        IGlobalJEPTemplateService _GlobalJEPTemplateService;

        public ProjectController(IProjectService ProjectService, ICommonService CommonService, IProductService ProductService, IGlobalJEPTemplateService GlobalJEPTemplateService)
        {
            _ProjectService = ProjectService;
            _CommonService = CommonService;
            _ProductService = ProductService;
            _GlobalJEPTemplateService = GlobalJEPTemplateService;
        }

        // GET: DES/Project
        [HttpGet]
        public ActionResult Index()
        {
            int? count = CommonService.objClsLoginInfo.ListRoles.Where(x => x == IEMQSImplementation.clsImplementationEnum.UserRoleName.PMG1.ToString() || x == IEMQSImplementation.clsImplementationEnum.UserRoleName.PMG2.ToString() || x == IEMQSImplementation.clsImplementationEnum.UserRoleName.PMG3.ToString() || x == IEMQSImplementation.clsImplementationEnum.UserRoleName.PLNG1.ToString() || x == IEMQSImplementation.clsImplementationEnum.UserRoleName.PLNG2.ToString() || x == IEMQSImplementation.clsImplementationEnum.UserRoleName.PLNG3.ToString()).Count();
            if (count > 0)
            {
                return View();
            }
            else
            {
                TempData["Error"] = "You are not authorized to access this page";
                return RedirectToAction("Index", "Dashboard");
            }

        }

        [HttpPost]
        public ActionResult Index(DataTableParamModel parm)
        {
            string PSNo = CommonService.objClsLoginInfo.UserName;
            int recordsTotal = 0;
            var list = _ProjectService.GetProjectList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, PSNo, out recordsTotal);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpGet]
        public ActionResult Edit(string ProjectNo)
        {
            var model = _ProjectService.GetProjectByProjectNo(ProjectNo);
            ViewBag.codestamplist = new SelectList(_CommonService.GetCodeStamp(), "Text", "Text");

            ViewBag.lstReqReg = new SelectList(_CommonService.GetLookupData(LookupType.ApplicableLocalRegulation.ToString()), "Lookup", "Lookup");
            ViewBag.ProjectPMGList = new SelectList(_CommonService.GetUserByRole("PMG1,PMG2"), "t_psno", "t_name");
            //model.DesignCode = _ProjectService.GetDesignCodeByProjectNo(ProjectNo, "DesignCode", model.BUCode, CommonService.objClsLoginInfo.Location);
            return PartialView("Edit", model);
        }

        [HttpPost]
        public ActionResult Edit(ProjectModel model)
        {
            model = _ProjectService.EditProject(model);
            if (!string.IsNullOrEmpty(model.Project))
                return Json(new { Status = true, Msg = "Project updated successfully" }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateProjectStatus(string Project, string Status)
        {
            string errorMsg = "";
            bool result = _ProjectService.UpdateProjectStatus(Project, Status, out errorMsg);
            if (result)
            {
                if (Status == ObjectStatus.InProcess.ToString())
                {
                    return Json(new { result = true, Msg = "Project activated successfully" }, JsonRequestBehavior.AllowGet);
                }
                else if (Status == ObjectStatus.Created.ToString())
                {
                    return Json(new { result = true, Msg = "Project Created successfully" }, JsonRequestBehavior.AllowGet);
                }
                else if (Status == ObjectStatus.Completed.ToString())
                {
                    return Json(new { result = true, Msg = "Project Completed successfully" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { result = true, Msg = "Project updated successfully" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { result = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { result = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult Details(string ProjectNo)
        {
            if (!string.IsNullOrEmpty(ProjectNo))
            {
                var loginUser = CommonService.objClsLoginInfo;
                //ProjectFolderModel ProjectFolderList = new ProjectFolderModel();
                var model = _ProjectService.GetProjectByProjectNo(ProjectNo);
                model.DesignCode = _ProjectService.GetDesignCodeByProjectNo(ProjectNo, "DesignCode", model.BUCode, CommonService.objClsLoginInfo.Location);
                if (model != null && model.Project != null)
                {
                    if (model.Status == ObjectStatus.Created.ToString())
                    {
                        TempData["Error"] = "Project is created.";
                        return RedirectToAction("Index", "Dashboard", new { area = "DES" });
                    }
                    else
                    {
                        if (model.Project != null)
                        {

                            FillProjectFolderSession(model.Project);
                        }
                    }

                    var ProjectFolderList = Models.SessionMgtModel.ProjectFolderList;
                    if (ProjectFolderList == null)
                    {
                        TempData["Error"] = "Project details is not found";
                        return RedirectToAction("Index", "Dashboard", new { area = "DES" });
                    }
                    //Models.SessionMgtModel.ProjectFolderList = ProjectFolderList;
                    _ProjectService.AddRecentProject(CommonService.objClsLoginInfo.UserName, ProjectNo);

                    if (TempData["DINId"] != null)
                    {
                        string qstring = CommonService.Encrypt("Id=" + TempData["DINId"].ToString());
                        TempData["DINId"] = null;
                        return RedirectToAction("Detail", "DIN", new { q = qstring });
                    }
                    if (TempData["DOCId"] != null)
                    {
                        string qstring = CommonService.Encrypt("Id=" + TempData["DOCId"].ToString());
                        TempData["DOCId"] = null;
                        return RedirectToAction("Detail", "DOC", new { q = qstring });
                    }
                    if (TempData["PartId"] != null)
                    {
                        string qstring = CommonService.Encrypt("ItemId=" + TempData["PartId"].ToString());
                        TempData["PartId"] = null;
                        return RedirectToAction("Detail", "Part", new { q = qstring });
                    }
                    if (TempData["JEPId"] != null)
                    {
                        string qstring = CommonService.Encrypt("JEPId=" + TempData["JEPId"].ToString());
                        TempData["JEPId"] = null;
                        return RedirectToAction("Detail", "JEP", new { q = qstring });
                    }
                    if (TempData["DCRId"] != null)
                    {
                        string qstring = CommonService.Encrypt("Id=" + TempData["DCRId"].ToString());
                        TempData["DCRId"] = null;
                        return RedirectToAction("Detail", "DCR", new { q = qstring });
                    }
                    if (TempData["jep-customerfeedbackId"] != null)
                    {
                        string qstring = CommonService.Encrypt("CFId=" + TempData["jep-customerfeedbackId"].ToString() + "&Id=" + TempData["jepidCF"].ToString());
                        TempData["jep-customerfeedbackId"] = null;
                        string JEPId = TempData["jepidCF"].ToString();
                        TempData["jepidCF"] = null;

                        return RedirectToAction("GetCustomerFeedback", "JEP", new { q = qstring });
                    }
                    if (TempData["ROCId"] != null)
                    {
                        string qstring = CommonService.Encrypt("rocId=" + TempData["ROCId"].ToString());
                        TempData["ROCId"] = null;
                        return RedirectToAction("Detail", "ROC", new { q = qstring });
                    }
                    if (TempData["ROC_Attached_Id"] != null)
                    {
                        string qstring = CommonService.Encrypt("rocId=" + TempData["ROC_Attached_Id"].ToString());
                        TempData["ROC_Attached_Id"] = null;
                        return RedirectToAction("Detail", "ROC", new { q = qstring });
                    }

                    return View(model);
                }
            }

            TempData["Error"] = "Project is not exist.";
            return RedirectToAction("Index", "Dashboard", new { area = "DES" });

        }

        [HttpPost]
        public ActionResult Details(DataTableParamModel parm, string ProjectNo, string ContractNo, string BUCode)
        {
            int recordsTotal = 0;
            var list = _ProjectService.GetProjectAccessList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal, ProjectNo, ContractNo, BUCode);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }


        [HttpGet]
        public ActionResult _partialCreateNewFolderProject(string ProjectNo, Int64 ParentProjectFolderId = 0, bool EditModeOnFlag = false)
        {
            FolderListModel model = new FolderListModel();
            model = _ProjectService.GetCategoryActionByProjectNoParentId(ProjectNo, ParentProjectFolderId, EditModeOnFlag);
            return PartialView("_partialCreateNewFolderProject", model);
        }

        [HttpPost]
        public ActionResult _partialCreateNewFolderProject(FolderListModel model)
        {
            string errorMsg = "";
            Int64 status = _ProjectService.AddEditProjectFolder(model, out errorMsg);
            if (status > 0)
            {
                var loginUser = CommonService.objClsLoginInfo;
                ProjectFolderModel ProjectFolderList = new ProjectFolderModel();
                ProjectFolderList = _ProjectService.ProjectFolderList(model.ProjectNo, loginUser.UserName, loginUser.Department);

                FillProjectFolderSession(model.ProjectNo);

                if (model.ProjectFolderId > 0)
                    return Json(new { Status = true, Msg = "Folder updated successfully" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = true, Msg = "New folder added successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult IsAlreadyExist(string FolderName, long? ParentProjectFolderId, string ProjectNo)
        {
            return Json(!_ProjectService.IsAlreadyExist(FolderName, ParentProjectFolderId, ProjectNo), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult DeleteFolder(Int64 Id)
        {
            string errorMsg = "";
            Int64 status = _ProjectService.DeleteFolder(Id, out errorMsg);
            if (status > 0)
            {
                var _ProjectFolderList = Models.SessionMgtModel.ProjectFolderList;
                var model = _ProjectService.GetProjectByProjectNo(_ProjectFolderList.ProjectNo);
                var loginUser = CommonService.objClsLoginInfo;
                _ProjectFolderList.FolderList = _ProjectService.ProjectFolderList(_ProjectFolderList.ProjectNo, loginUser.UserName, loginUser.Department).FolderList;

                FillProjectFolderSession(_ProjectFolderList.ProjectNo);
                //Models.SessionMgtModel.ProjectFolderList = _ProjectFolderList;
                return Json(new { Status = true, Msg = "Folder deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ProjectSearch(string Status, string Key)
        {
            List<string> list = _ProjectService.ProjectSearch(Status, Key);

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Folder(Int64 Id)
        {
            var _ProjectFolderList = Models.SessionMgtModel.ProjectFolderList;
            var topFolder = CommonService.fnFolderRoleGroup(_ProjectFolderList, Id);
            bool doAction = false;
            if (_ProjectFolderList != null)
            {
                if (_ProjectFolderList.FolderList != null)
                {
                    if (topFolder != null && topFolder.IsViewFolder == true && topFolder.IsWriteFolder == true)
                    {
                        doAction = true;
                    }
                }

            }
            else
            {
                return RedirectToAction("Index", "Dashboard", new { area = "DES" });
            }

            var _GetFolder = _ProjectService.GetFolder(Id, _ProjectFolderList.BUId,CommonService.objClsLoginInfo.ListRoles, Models.SessionMgtModel.ProjectFolderList.ProjectNo);
            if (_ProjectFolderList.IsInprocess && doAction)
            {
                _ProjectFolderList.ListAction = _GetFolder.ListAction;
            }
            else
            {
                _ProjectFolderList.ListAction = new List<CategoryActionModel>();
            }
            _ProjectFolderList.ListCategory = _GetFolder.ListCategory;
            _ProjectFolderList.FolderId = Id;
            _ProjectFolderList.RoleGroup = topFolder.RoleGroups;
            Models.SessionMgtModel.ProjectFolderList = _ProjectFolderList;

            ViewBag.objectNames = new SelectList(_CommonService.GetObjectName(), "Text", "Text");
            ViewBag.objectStatus = new SelectList(_CommonService.GetObjectStatus(), "Text", "Text");
            return View();
        }

        [HttpPost]
        public ActionResult Folder(DataTableParamModel parm, string searchObjectType, string searchObjectStatus, string searchObjectName)
        {          
            var Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            var FolderId = Models.SessionMgtModel.ProjectFolderList.FolderId;
            string Roles = CommonService.objClsLoginInfo.UserRoles;
            if(searchObjectType=="")
            {
                searchObjectType = null;
            }
            if (searchObjectStatus == "")
            {
                searchObjectStatus = null;
            }
            if (searchObjectName == "")
            {
                searchObjectName = null;
            }
            int recordsTotal = 0;
            var list = _ProjectService.GetAllObjectsList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, Project, searchObjectType, searchObjectStatus, searchObjectName, FolderId, Roles, out recordsTotal);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        public ActionResult SendRequestMail(Int64 Id)
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/Areas/DES/HTMLMailTemplate/AccessRequestTemplate.html")))
            {
                body = reader.ReadToEnd();
            }
            var MailToAdd = "darpan.patel@rigelnetworks.com";
            var Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;

            body = body.Replace("[ApprovarName]", MailToAdd);
            body = body.Replace("[Project]", Project);
            body = body.Replace("[Name]", "L&TTeam");

            ProjectUserFolderAuthorizeModel _ProjectUserFolderAuthorizeModel = new ProjectUserFolderAuthorizeModel();
            _ProjectUserFolderAuthorizeModel.EmailText = body;
            _ProjectUserFolderAuthorizeModel.PSNumber = CommonService.objClsLoginInfo.UserName;
            _ProjectUserFolderAuthorizeModel.ProjectFolderId = Id;
            _ProjectUserFolderAuthorizeModel.Project = Project;
            _ProjectUserFolderAuthorizeModel.Location = CommonService.objClsLoginInfo.Location;
            _ProjectUserFolderAuthorizeModel.FunctionId = CommonService.objClsLoginInfo.Department;
            _ProjectUserFolderAuthorizeModel.BUID = Models.SessionMgtModel.ProjectFolderList.BUId;
            // var status = _CommonService.SendMail(MailToAdd, "", "", "Access Request Mail", body, "", null, false);

            //if (status)
            if (true)
            {
                var statusid = _ProjectService.AddProjectUserFolderAuthorize(_ProjectUserFolderAuthorizeModel);
                if (statusid > 0)
                    return Json(new { InfoMsg = false, Status = true, Msg = "Access request sent successfully" }, JsonRequestBehavior.AllowGet);
                else
                {
                    return Json(new { InfoMsg = true, Status = true, Msg = "Access request already sent" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { InfoMsg = false, Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        #region MAINTAIN PROJECT DETAILS
        /// <summary>
        /// Maintain Project Details | page load event
        /// </summary>
        /// <returns></returns>
        public ActionResult JEPProject()
        {
            ViewBag.ListDesignGroup = new SelectList(_CommonService.GetLookupData(LookupType.DesignGroup.ToString()), "Lookup", "Lookup");
            ViewBag.ProjectPMGList = new SelectList(_CommonService.GetUserByRole("PMG1,PMG2"), "t_psno", "t_name");
            var model = _ProjectService.GetJEPProjectByProjectNo(SessionMgtModel.ProjectFolderList.ProjectNo);
            ViewBag.codestamplist = new SelectList(_CommonService.GetCodeStamp(), "Text", "Text");
            return View(model);
        }

        /// <summary>
        /// Maintain Project Details | Post medthod for data save
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult JEPProject(ProjectModel model)
        {
            string status = _ProjectService.EditJEPProject(model);
            TempData["Success"] = "Project updated successfully";
            return RedirectToAction("JEPProject");
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult IsAlreadyExistDrawingSeriesNo(string DrawingSeriesNo)
        {
            var ProjectFolderList = Models.SessionMgtModel.ProjectFolderList;
            return Json(!_ProjectService.IsAlreadyExistDrawingSeriesNo(DrawingSeriesNo, ProjectFolderList.ProjectNo), JsonRequestBehavior.AllowGet);
        }
        #endregion

        public void FillProjectFolderSession(string ProjectNo)
        {
            var ProjectFolderList = Models.SessionMgtModel.ProjectFolderList;
            var model = _ProjectService.GetProjectByProjectNo(ProjectNo);
            if (model != null && model.Project != null)
            {
                bool Isinprocess = false;
                if (model.Status == ObjectStatus.InProcess.ToString())
                {
                    Isinprocess = true;
                }

                var loginUser = CommonService.objClsLoginInfo;
                ProjectFolderList = _ProjectService.ProjectFolderList(ProjectNo, loginUser.UserName, loginUser.Department);
                ProjectFolderList.IsInprocess = Isinprocess;
                bool IsProjectRight = false;
                if (ProjectFolderList != null && ProjectFolderList.FolderList != null && ProjectFolderList.FolderList.Count > 0)
                {
                    if (ProjectFolderList.FolderList.Select(x => x.IsProjectRight).FirstOrDefault() == true)
                        IsProjectRight = true;
                }
                ProjectFolderList.IsProjectRight = IsProjectRight;

                var _GetFolder = _ProjectService.GetFolder(0, ProjectFolderList.BUId, CommonService.objClsLoginInfo.ListRoles, ProjectNo);
                //ProjectFolderList.ListAction = _GetFolder.ListAction;
                ProjectFolderList.ListCategory = _GetFolder.ListCategory;

                Models.SessionMgtModel.ProjectFolderList = ProjectFolderList;
            }
        }
    }
}