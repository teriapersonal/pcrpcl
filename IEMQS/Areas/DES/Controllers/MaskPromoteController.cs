﻿using IEMQS.Areas.DES.Models;
using IEMQS.DESCore;
using IEMQS.DESServices;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.DES.Controllers
{
    [IEMQSImplementation.clsBase.SessionExpireFilter]
    [Models.EncryptedActionParameter]
    public class MaskPromoteController : Controller
    {
        IProjectService _ProjectService;
        IDOCService _DOCService;
        IDOCFileService _DOCMappingService;
        ICommonService _CommonService;
        IDINService _DINService;
        IPartService _PartService;
        IDCRService _DCRService;
        // GET: DES/DOC
        public MaskPromoteController(IDOCService DOCService, IDOCFileService DOCMappingService, ICommonService CommonService, IProjectService ProjectService, IDINService DINService, IPartService PartService, IDCRService DCRService)
        {
            _ProjectService = ProjectService;
            _DOCService = DOCService;
            _DOCMappingService = DOCMappingService;
            _CommonService = CommonService;
            _DINService = DINService;
            _PartService = PartService;
            _DCRService = DCRService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        #region PENDING DOC

        [HttpGet]
        public ActionResult GetPendingDoc()
        {
            var psno = CommonService.objClsLoginInfo.UserName;
            string Roles = CommonService.objClsLoginInfo.UserRoles;
            ViewBag.GetPolicySteps = _DOCService.GetMassPromoteUserPolicy(psno, Roles, null);

            if (ViewBag.GetPolicySteps.Count > 0)
            {
                if (ViewBag.GetPolicySteps[0].PolicyOrderID == 1)
                {
                    ViewBag.IsFirstStep = ViewBag.GetPolicySteps[0].PolicyOrderID;
                }
            }
            return View();
        }
        //Fill Gird
        [HttpPost]
        public ActionResult GetPendingDoc(DataTableParamModel parm, string RoleGroup, string Role)
        {
            int recordsTotal = 0;
            var psno = CommonService.objClsLoginInfo.UserName;
            string Roles = CommonService.objClsLoginInfo.UserRoles;
            var Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            var list = _DOCService.GetPendingDocumentList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, psno, Roles, out recordsTotal, Project);


            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpPost]  //On Check box change Status
        public JsonResult MASKLifeCycleDOC(GetListOfDocumentId DocIds)
        {
            List<string> errorMsg = new List<string>();
            List<string> infoMsg = new List<string>();
            List<string> armMsg = new List<string>();
            var psno = CommonService.objClsLoginInfo.UserName;
            string Roles = CommonService.objClsLoginInfo.UserRoles;
            var data = _DOCService.GetMassPromoteUserPolicy(psno, Roles, DocIds.BtnText.Trim());
            var Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            Int64? status = 0;
            if (data != null)
            {
                foreach (var item in data)
                {
                    if (DocIds.DocumentId.Contains(item.DocumentId))
                    {
                        string emsg, imsg,amsg;

                        status = _DOCMappingService.UpdateDOCPolicy(item.DocumentLifeCycleId, Project, out emsg, out imsg,out amsg);
                        if (!string.IsNullOrEmpty(emsg))
                            errorMsg.Add(emsg);
                        if (!string.IsNullOrEmpty(imsg))
                            infoMsg.Add(imsg);
                        if (!string.IsNullOrEmpty(amsg))
                            armMsg.Add(amsg);
                    }
                }
                if (errorMsg != null && errorMsg.Count > 0)
                {
                    TempData["Error"] = MvcHtmlString.Create(string.Join("<br />", errorMsg));
                }
                if (infoMsg != null && infoMsg.Count > 0)
                {
                    TempData["Success"] = MvcHtmlString.Create(string.Join("<br />", infoMsg));
                }
                if (armMsg != null && armMsg.Count > 0)
                {
                    TempData["Info"] = MvcHtmlString.Create(string.Join("<br />", armMsg));
                }
                return Json(new { Status = true, Msg = "Mass " + DocIds.BtnText + " successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { Status = false, Msg = "Data not found" }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]  //On Check box change Status
        public JsonResult ReturnMASKDOC(GetListOfDocumentId DocIds)
        {
            string errorMsg = "";
            var UserName = new clsManager().GetUserNameFromPsNo(CommonService.objClsLoginInfo.UserName);
            var Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            Int64? status = 0;
            foreach (var item in DocIds.DocumentId)
            {
                status = _DOCMappingService.ReviseDOCPolicy(DocIds.BtnText, item, Project, out errorMsg);
            }
            if (status != null)
            {
                return Json(new { Status = true, Msg = "Document return successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region PENDING DIN

        [HttpGet]
        public ActionResult GetPendingDIN()
        {
            DINModel model = new DINModel();
            model.CurrentPsNo = CommonService.objClsLoginInfo.UserName;
            return View(model);
        }

        // Fill Gird
        [HttpPost]
        public ActionResult GetPendingDIN(DataTableParamModel parm)
        {
            int recordsTotal = 0;
            string psno = CommonService.objClsLoginInfo.UserName;
            string Roles = CommonService.objClsLoginInfo.UserRoles;
            var Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            var list = _DINService.GetPendingDINList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, psno, Roles, out recordsTotal, Project);


            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }
        #endregion

        #region PENDING PART

        [HttpGet]
        public ActionResult GetPendingPart()
        {
            var psno = CommonService.objClsLoginInfo.UserName;
            string Roles = CommonService.objClsLoginInfo.UserRoles;
            ViewBag.GetPolicySteps = _PartService.GetMassPromoteUserPolicy(psno, Roles, null);

            if (ViewBag.GetPolicySteps.Count > 0)
            {
                if (ViewBag.GetPolicySteps[0].PolicyOrderID == 1)
                {
                    ViewBag.IsFirstStep = ViewBag.GetPolicySteps[0].PolicyOrderID;
                }
            }
            return View();
        }

        //Fill Gird
        [HttpPost]
        public ActionResult GetPendingPart(DataTableParamModel parm)
        {
            var psno = CommonService.objClsLoginInfo.UserName;
            string Roles = CommonService.objClsLoginInfo.UserRoles;
            //var getRelatedPolicyStep = _PartService.GetMassPromoteUserPolicy(psno, Roles, null).ToList();


            int recordsTotal = 0;
            var Project = SessionMgtModel.ProjectFolderList.ProjectNo;
            var list = _PartService.GetPendingPartList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, psno, Roles, Project, out recordsTotal);

            //var getAllStep = list.Select(x => x.PolicyStep).ToList();

                
            //for (int i = 0; i < getRelatedPolicyStep.Count; i++)
            //{
            //    if (!getAllStep.Contains(getRelatedPolicyStep[i].PolicyStep))
            //    {
            //        getRelatedPolicyStep.Remove(getRelatedPolicyStep[i]);

            //        i--;
            //    }
            //}

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });


        }

        [HttpPost]  //On Check box change Status
        public JsonResult MASKLifeCyclePart(GetListOfItmeID ItemIds)
        {
            List<string> errorMsg = new List<string>();
            List<string> infoMsg = new List<string>();
            var psno = CommonService.objClsLoginInfo.UserName;
            var Project = SessionMgtModel.ProjectFolderList.ProjectNo;
            bool status= _PartService.MassPromote(Project, ItemIds, psno,  out errorMsg, out infoMsg);
            //var data = _PartService.GetMassPromoteUserPolicy(psno, Roles, ItemIds.BtnText.Trim());//.Where(x=> ItemIds.ItemId.Contains(x.ItemId)).ToList();
            //Int64? status = 0;
            if (status)
            {
                //foreach (var item in data)
                //{
                //    if (ItemIds.ItemId.Contains(item.ItemId))
                //    {
                //        string emsg, imsg;

                //        status = _PartService.UpdateItemPolicy(item.PartLifeCycleId, Project, out emsg, out imsg);
                //        if (!string.IsNullOrEmpty(emsg))
                //            errorMsg.Add(emsg);
                //        if (!string.IsNullOrEmpty(imsg))
                //            infoMsg.Add(imsg);

                //    }
                //}
                if (errorMsg != null && errorMsg.Count > 0)
                {
                    TempData["Info"] = MvcHtmlString.Create(string.Join("<br />", errorMsg));
                }
                if (infoMsg != null && infoMsg.Count > 0)
                {
                    TempData["Success"] = MvcHtmlString.Create(string.Join("<br />", infoMsg));
                }
                return Json(new { Status = true, Msg = "Mass" + ItemIds.BtnText + " successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { Status = false, Msg = "Data not found" }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]  //On Check box change Status
        public JsonResult ReturnMASKPart(GetListOfItmeID ItemIds)
        {
            string errorMsg = "";
            string Roles = CommonService.objClsLoginInfo.UserRoles;
            Int64? status = 0;
            var Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            foreach (var item in ItemIds.ItemId)
            {
                status = _PartService.ReviseItemPolicy(item, Project, out errorMsg);

            }
            if (status != null)
            {
                return Json(new { Status = true, Msg = "Document return successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region PENDING DCR

        [HttpGet]
        public ActionResult GetPendingDCR()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetPendingDCR(DataTableParamModel parm)
        {
            int recordsTotal = 0;
            var Roles = CommonService.objClsLoginInfo.UserRoles;
            var Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            string psno = CommonService.objClsLoginInfo.UserName;
            var list = _DCRService.GetPendingDCRList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal, Roles, Project,psno);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        #endregion
    }
}