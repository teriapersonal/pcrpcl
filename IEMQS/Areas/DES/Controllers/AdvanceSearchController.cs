﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQS.DESCore;
using IEMQS.DESServices;
using IEMQS.Areas.DES.Models;

namespace IEMQS.Areas.DES.Controllers
{
    [IEMQSImplementation.clsBase.SessionExpireFilter]
    [Models.EncryptedActionParameter]
    public class AdvanceSearchController : Controller
    {
        ISolrDocumentIndexService _SolrDocumentIndexService;
        ICommonService _CommonService;
        IProjectService _ProjectService;
        IDCRService _DCRService;
        public AdvanceSearchController(ISolrDocumentIndexService solrdocumentindexservice, ICommonService commonService,IProjectService projectService,IDCRService dCRService)
        {
            _SolrDocumentIndexService = solrdocumentindexservice;
            _CommonService = commonService;
            _ProjectService = projectService;
            _DCRService = dCRService;
        }
        // GET: DES/AdvanceSearch
        public ActionResult Index(string filterid, string customername1, string BUId1, string dept, string ObjectId1, string StatusId, string name1, string rev, string Originator1, string Owner, string keyword, string FProjectNo, string SubObject)
        {
            string projectno = Models.SessionMgtModel.ProjectFolderList.ProjectNo;

            AdvanceSearchModel model = new AdvanceSearchModel();
            if (string.IsNullOrEmpty(filterid) || filterid == "0")
            {

                model.CustomerName = customername1;
                model.BUId = (string.IsNullOrEmpty(BUId1) || BUId1 == "0") ? 0 : Convert.ToInt64(BUId1);
                model.Department = dept;
                model.ObjectId = (string.IsNullOrEmpty(ObjectId1) || ObjectId1 == "0") ? 0 : Convert.ToInt64(ObjectId1);
                model.PolicyStepId = (string.IsNullOrEmpty(StatusId) || StatusId == "0") ? 0 : Convert.ToInt64(StatusId);
                model.Name = name1;
                model.Revision = rev;
                model.Originator = Originator1;
                model.OwnerName = Owner;
                model.SearchText = keyword;
                model.CurrentLocationIp = CommonService.GetUseIPConfig.Ip;
                model.FilterName = filterid;
                model.ProjectNumber = FProjectNo;
                model.SubObjectId = SubObject;
            }
            else
            {
                List<AdvanceSearchModel> filterlist = GetSavedFilter(filterid);

                model.CustomerName = filterlist[0].CustomerName;
                model.BUId = (string.IsNullOrEmpty(filterlist[0].BU) || filterlist[0].BU == "0") ? 0 : Convert.ToInt64(filterlist[0].BU);
                model.Department = filterlist[0].Department;
                model.ObjectId = (filterlist[0].ObjectId == 0) ? 0 : filterlist[0].ObjectId;
                model.PolicyStepId = (string.IsNullOrEmpty(filterlist[0].Status) || filterlist[0].Status == "0") ? 0 : Convert.ToInt64(filterlist[0].Status);
                model.Name = filterlist[0].ObjectName;
                model.Revision = filterlist[0].Revision;
                model.Originator = filterlist[0].Originator;
                model.OwnerName = filterlist[0].OwnerName;
                model.SearchText = filterlist[0].SearchText;
                model.CurrentLocationIp = CommonService.GetUseIPConfig.Ip;
                model.FilterName = filterid;
                model.ProjectNumber = FProjectNo;
                model.SubObjectId = filterlist[0].SubObjectId;

                
            }

            var ObjName = _ProjectService.GetObjectNameByObjectId(model.ObjectId);
            if (!string.IsNullOrEmpty(ObjName) && (ObjName.Trim().ToLower() == "part" || ObjName.Trim().ToLower() == "doc"))
            {
                if (ObjName.Trim().ToLower() == "part")
                {
                    List<SelectListItem> SubObjectList = new List<SelectListItem>();
                    SubObjectList.Insert(0, new SelectListItem() { Value = "", Text = "Select Sub Object Type" });
                    SubObjectList.Insert(1, new SelectListItem { Value = ObjectName.Part.ToString(), Text = ObjectName.Part.ToString() });
                    SubObjectList.Insert(2, new SelectListItem { Value = ObjectName.Plate_Part.ToString().Replace("_", " "), Text = ObjectName.Plate_Part.ToString().Replace("_", " ") });
                    SubObjectList.Insert(3, new SelectListItem { Value = ObjectName.Clad_Part.ToString().Replace("_", " "), Text = ObjectName.Clad_Part.ToString().Replace("_", " ") });
                    SubObjectList.Insert(4, new SelectListItem { Value = ObjectName.Jigfix.ToString().Replace("_", " "), Text = ObjectName.Jigfix.ToString().Replace("_", " ") });
                    ViewBag.SubObjectList = SubObjectList;
                }
                else if (ObjName.Trim().ToLower() == "doc")
                {
                    List<SelectListItem> SubObjectList = _CommonService.GetLookupData(LookupType.DOCType.ToString()).Select(x => new SelectListItem { Value = x.Lookup, Text = x.Lookup }).ToList();
                    SubObjectList.Insert(0, new SelectListItem() { Value = "", Text = "Select Sub Object Type" });
                    ViewBag.SubObjectList = SubObjectList;
                }
                else
                {
                    List<SelectListItem> SubObjectList = new List<SelectListItem>();
                    SubObjectList.Insert(0, new SelectListItem() { Value = "", Text = "Select Sub Object Type" });
                    ViewBag.SubObjectList = SubObjectList;
                }
            }
            else
            {
                List<SelectListItem> SubObjectList = new List<SelectListItem>();
                SubObjectList.Insert(0, new SelectListItem() { Value = "", Text = "Select Sub Object Type" });
                ViewBag.SubObjectList = SubObjectList;
            }

            List<SelectListItem> bulist = new List<SelectListItem>();
            bulist = _SolrDocumentIndexService.GetBUList(projectno).Select(x => new SelectListItem { Value = x.BUId.ToString(), Text = x.BU }).ToList();
            bulist.Insert(0, new SelectListItem() { Value = "0", Text = "Select BU" });
            ViewBag.BUList = bulist;

            List<SelectListItem> filtervalues = new List<SelectListItem>();
            filtervalues = _SolrDocumentIndexService.BindFilterdata(Convert.ToInt64(CommonService.objClsLoginInfo.UserName)).Select(x => new SelectListItem { Value = x.PK_SearchFilterId.ToString(), Text = x.FilterName }).ToList();
            filtervalues.Insert(0, new SelectListItem() { Value = "0", Text = "Select Filter" });
            filtervalues.Insert(1, new SelectListItem() { Value = "00", Text = "Save this Filter" });
            ViewBag.FilterData = filtervalues;

            //Bind Object drop down accordingly BU selection
            if (model.BUId == null || model.BUId == 0)
            {
                List<SelectListItem> objectList = new List<SelectListItem>();
                //objectList = _SolrDocumentIndexService.GetObjectList(projectno).Select(x => new SelectListItem { Value = x.ObjectId.ToString(), Text = x.ObjectName }).ToList();
                objectList.Insert(0, new SelectListItem() { Value = "0", Text = "Select Object Type" });
                ViewBag.objectList = objectList;

            }
            else
            {
                List<SelectListItem> objectList = new List<SelectListItem>();
                objectList = _SolrDocumentIndexService.GetObjectListBasedonBuId(model.BUId.ToString()).Select(x => new SelectListItem { Value = x.ObjectId.ToString(), Text = x.ObjectName }).ToList();
                objectList.Insert(0, new SelectListItem() { Value = "0", Text = "Select Object Type" });
                ViewBag.objectList = objectList;
            }

            //Bind Status drop down accordingly status
            if (model.ObjectId == null || model.ObjectId == 0)
            {
                List<SelectListItem> StatusList = new List<SelectListItem>();
                //StatusList = _SolrDocumentIndexService.GetStatusList(projectno).Select(x => new SelectListItem { Value = x.PolicyStepId.ToString(), Text = x.StepName }).ToList();
                StatusList.Insert(0, new SelectListItem() { Value = "0", Text = "Select Status" });
                ViewBag.StatusList = StatusList;
            }
            else
            {
                List<SelectListItem> StatusList = new List<SelectListItem>();
                StatusList = _SolrDocumentIndexService.GetStatusListAsperObjectId(model.ObjectId.ToString()).Select(x => new SelectListItem { Value = x.PolicyStepid.ToString(), Text = x.StepName }).ToList();
                StatusList.Insert(0, new SelectListItem() { Value = "0", Text = "Select Status" });
                ViewBag.StatusList = StatusList;
            }

            return View(model);

        }

        [HttpPost]
        public ActionResult GetAdvanceSerchDoc(DataTableParamModel parm, string customername, string BUId, string dept, string ObjectId, string StatusId, string objName, string rev, string Originator, string Owner, string keyword, string FProjectNo, string SubObject)
        {
            int recordsTotal = 0;
            string searchQuery = string.Empty;

            if (!string.IsNullOrEmpty(customername))
            {
                customername = GetSearchParameterFormat("Customer:", customername, false);

                searchQuery += customername;// "Customer:" + customername + " AND ";
            }
            if (!string.IsNullOrEmpty(BUId) && BUId != "0" && BUId != "Select BU")
            {
                BUId = GetSearchParameterFormat("BU:", BUId, true);

                searchQuery += BUId;// "BU:" + BUId + " AND ";
            }
            if (!string.IsNullOrEmpty(dept))
            {
                dept = GetSearchParameterFormat("Department:", dept, false);

                searchQuery += dept;// "Department:" + dept + " AND ";
            }
            if (!string.IsNullOrEmpty(ObjectId) && ObjectId != "0" && ObjectId != "Select Object Type")
            {
                ObjectId = GetSearchParameterFormat("Type:", ObjectId, false);

                searchQuery += ObjectId;// "Type:" + ObjectId + " AND ";
            }
            if (!string.IsNullOrEmpty(SubObject))
            {
                SubObject = GetSearchParameterFormat("SubObject:", SubObject, false);

                searchQuery += SubObject;// "Type:" + ObjectId + " AND ";
            }
            if (!string.IsNullOrEmpty(StatusId) && StatusId != "0" && StatusId != "Select Status")
            {
                StatusId = GetSearchParameterFormat("Status:", StatusId, false);
                searchQuery += StatusId;// "Status:" + StatusId + " AND ";
            }
            if (!string.IsNullOrEmpty(objName))
            {
                objName = GetSearchParameterFormat("Name:", objName, false);
                searchQuery += objName;// "Name:" + name + " AND ";
            }
            if (!string.IsNullOrEmpty(rev))
            {
                rev = GetSearchParameterFormat("Revision:", rev, false);
                searchQuery += rev;// "Revision:" + rev + " AND ";
            }
            if (!string.IsNullOrEmpty(Originator))
            {
                Originator = GetSearchParameterFormat("Originator:", Originator, false);
                searchQuery += Originator;// "Originator:" + Originator + " AND ";
            }
            if (!string.IsNullOrEmpty(Owner))
            {
                Owner = GetSearchParameterFormat("OwnerName:", Owner, false);
                searchQuery += Owner;// "OwnerName:" + Owner + " AND ";
            }
            if (!string.IsNullOrEmpty(FProjectNo))
            {
                FProjectNo = GetSearchParameterFormat("ProjectNumber:", FProjectNo, false);
                searchQuery += FProjectNo;// "OwnerName:" + Owner + " AND ";
            }
            if (!string.IsNullOrEmpty(keyword))
            {
                var OrFilter = string.Empty;
                OrFilter += "(";
                //searchQuery += "contentsearch:" + keyword + " AND ";

                OrFilter += GetSearchParameterFormat("Customer:", keyword, " OR ");
                OrFilter += GetSearchParameterFormat("BU:", keyword, " OR ");
                OrFilter += GetSearchParameterFormat("Department:", keyword, " OR ");
                OrFilter += GetSearchParameterFormat("Type:", keyword, " OR ");
                OrFilter += GetSearchParameterFormat("SubObject:", keyword, " OR ");
                OrFilter += GetSearchParameterFormat("Status:", keyword, " OR ");
                OrFilter += GetSearchParameterFormat("Name:", keyword, " OR ");
                OrFilter += GetSearchParameterFormat("Revision:", keyword, " OR ");
                OrFilter += GetSearchParameterFormat("Originator:", keyword, " OR ");
                OrFilter += GetSearchParameterFormat("OwnerName:", keyword, " OR ");
                OrFilter += GetSearchParameterFormat("ProjectNumber:", keyword, " OR ");
                OrFilter += GetSearchParameterFormat("Descripion:", keyword, " OR ");
                //OrFilter += GetSearchParameterFormat("contentsearch:", keyword, " OR "); //
                var contentsearch = keyword;
                ReplaceSpecialCharacterForSolr(ref contentsearch);
                OrFilter += " contentsearch:" + contentsearch;
                //OrFilter = OrFilter.Substring(0, OrFilter.LastIndexOf("OR")).Trim();
                searchQuery += OrFilter.Length > 0 ? OrFilter + ") AND " : " AND ";
            }

            if (searchQuery.Contains(" AND "))
                searchQuery = searchQuery.Substring(0, searchQuery.LastIndexOf(" AND ")).Trim();

            SolrDocumentIndexService solrDocumentIndexService = new SolrDocumentIndexService();

            List<DocumentIndexModel> searchresult = new List<DocumentIndexModel>();
            searchresult = solrDocumentIndexService.Search(searchQuery, parm.iDisplayStart, parm.iDisplayLength,parm.iSortCol_0,parm.sSortDir_0, out recordsTotal);
            List<string> storeType = new List<string>() { "DOC", "DCR", "JEP-CustomerFeedback", "ROC-Comment-Attached", "ROC" };
            solrDocumentIndexService.AddSearchResult(searchresult.Where(s => storeType.Contains(s.Type)).ToList());
            //var list = _SolrDocumentIndexService.GetAdvanceSerchDoc(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal, DocMapID);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = searchresult
            });
        }
        private static string GetSearchParameterFormat(string column, string SearchParameter, bool IsGlobalSearch = true)
        {
            return GetSearchParameterFormat(column, SearchParameter, " AND ", IsGlobalSearch);
        }
        private static string GetSearchParameterFormat(string column, string SearchParameter, string DefaultConditionAdd = " AND ", bool IsGlobalSearch = true)
        {
            string[] temp = null;
            if (!string.IsNullOrEmpty(SearchParameter))
            {
                if (IsGlobalSearch)
                {
                    temp = SearchParameter.Split(' ');
                }
                else
                {
                    temp = new string[1];
                    temp[0] = SearchParameter;
                }
                if (temp.Length > 0)
                {
                    if (IsGlobalSearch)
                    {
                        SearchParameter = column + "(";
                        foreach (string worditerate in temp)
                        {
                            var searchWord = worditerate;

                            if (!string.IsNullOrEmpty(worditerate) && worditerate != "-" && worditerate != "/")
                            {
                                ReplaceSpecialCharacterForSolr(ref searchWord);

                                SearchParameter += (DefaultConditionAdd.Trim().ToLower() == "or" ? "*" : "") + searchWord + "*" + " OR ";

                            }
                        }
                        SearchParameter = SearchParameter.Substring(0, SearchParameter.LastIndexOf("OR")).Trim();
                        SearchParameter += ")";
                    }
                    else
                    {
                        var searchWord = SearchParameter;
                        ReplaceSpecialCharacterForSolr(ref searchWord);
                        SearchParameter = column + "\"" + searchWord + "\"";
                    }
                }
            }
            return SearchParameter + DefaultConditionAdd;
        }

        public static void ReplaceSpecialCharacterForSolr(ref string searchWord)
        {
            string[] specialchar = { "+", "-", "&&", "||", "!", "(", ")", "{", "}", "[", "]", "^", "~", "*", "?", ":", "/" };
            foreach (var sc in specialchar)
            {
                searchWord = searchWord.Replace(sc, "\\" + sc);
            }
        }

        public ActionResult ShowFilelogs()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetFilelogs(DataTableParamModel parm)
        {
            int recordsTotal = 0;
            //SolrDocumentIndexService solrDocumentIndexService = new SolrDocumentIndexService();
            //if (!string.IsNullOrEmpty(SearchText))
            //    SearchText = "doctext:" + SearchText;

            //string DocMapID = solrDocumentIndexService.Search(SearchText, 1, 10);

            var list = _SolrDocumentIndexService.GetFilelogStatus(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal);

            var v = CommonService.objClsLoginInfo;
            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpPost]
        public ActionResult GetMostSearchedDocuments(DataTableParamModel parm)
        {
            int recordsTotal = 0;
            //SolrDocumentIndexService solrDocumentIndexService = new SolrDocumentIndexService();
            //if (!string.IsNullOrEmpty(SearchText))
            //    SearchText = "doctext:" + SearchText;

            //string DocMapID = solrDocumentIndexService.Search(SearchText, 1, 10);

            var list = _SolrDocumentIndexService.GetMostSearchedDocuments(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal);

            var v = CommonService.objClsLoginInfo;
            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }
        [HttpPost]
        public ActionResult GetFrequentSearchResult(DataTableParamModel parm)
        {
            int recordsTotal = 0;
            //SolrDocumentIndexService solrDocumentIndexService = new SolrDocumentIndexService();
            //if (!string.IsNullOrEmpty(SearchText))
            //    SearchText = "doctext:" + SearchText;

            //string DocMapID = solrDocumentIndexService.Search(SearchText, 1, 10);

            var list = _SolrDocumentIndexService.GetFilelogStatus(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal);

            var v = CommonService.objClsLoginInfo;
            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        public JsonResult GetExistingCustomerName(string term)
        {
            List<string> advanceSearches = new List<string>();
            advanceSearches = _SolrDocumentIndexService.GetCustomerNameasperTerm(term);

            return Json(advanceSearches, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetBUList()
        {
            string projectno = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            List<AdvanceSearchModel> bulist = new List<AdvanceSearchModel>();
            bulist = _SolrDocumentIndexService.GetBUList(projectno);

            return Json(bulist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetObjectList()
        {
            string projectno = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            List<AdvanceSearchModel> bulist = new List<AdvanceSearchModel>();
            bulist = _SolrDocumentIndexService.GetObjectList(projectno);

            return Json(bulist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStatusList()
        {
            string projectno = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            List<AdvanceSearchModel> bulist = new List<AdvanceSearchModel>();
            bulist = _SolrDocumentIndexService.GetStatusList(projectno);

            return Json(bulist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetOwnerName(string term)
        {
            List<string> bulist = new List<string>();
            bulist = _SolrDocumentIndexService.GetOwnerName(term);

            return Json(bulist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDepartmentList(string term)
        {
            List<string> department = new List<string>();
            department = _SolrDocumentIndexService.GetDepartmentlist(term);

            return Json(department, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetProjectNameList(string term)
        {
            List<string> department = new List<string>();
            department = _SolrDocumentIndexService.GetProjectListName(term);

            return Json(department, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStatusListasperObjectId(string ObjectId)
        {
            List<DESCore.Data.SP_DES_GET_StatusAsPerObjectId_Result> bulist = new List<DESCore.Data.SP_DES_GET_StatusAsPerObjectId_Result>();
            bulist = _SolrDocumentIndexService.GetStatusListAsperObjectId(ObjectId);

            return Json(bulist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetObjectListBasedonBuId(string BUId)
        {
            List<AdvanceSearchModel> bulist = new List<AdvanceSearchModel>();
            bulist = _SolrDocumentIndexService.GetObjectListBasedonBuId(BUId);

            return Json(bulist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSubObjectListBasedonObjectType(string ObjectType)
        {
            int i = 0;
            List<AdvanceSearchModel> subObjectList = new List<AdvanceSearchModel>();
            if (ObjectType.ToLower() == "part")
            {
                subObjectList.Insert(i++, new AdvanceSearchModel { SubObjectId = ObjectName.Part.ToString() });
                subObjectList.Insert(i++, new AdvanceSearchModel { SubObjectId = ObjectName.Plate_Part.ToString().Replace("_", " ") });
                subObjectList.Insert(i++, new AdvanceSearchModel { SubObjectId = ObjectName.Clad_Part.ToString().Replace("_", " ") });
                subObjectList.Insert(i++, new AdvanceSearchModel { SubObjectId = ObjectName.Jigfix.ToString().Replace("_", " ") });
            }
            else if (ObjectType.ToLower() == "doc")
            {
                subObjectList = _CommonService.GetLookupData(LookupType.DOCType.ToString()).Select(x => new AdvanceSearchModel { SubObjectId = x.Lookup }).ToList();
            }

            return Json(subObjectList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveFilterData(string CustomerName, string BUPBU, string Department, string Object, string Status, string Name, string Revision, string Originator, string Owner, string SearchKeyword, string SearchName, string FProjectNo, int? FilterId, string SubObject)
        {
            string message = _SolrDocumentIndexService.SaveFilterData(CustomerName, BUPBU, Department, Object, Status, Name, Revision, Originator, Owner, SearchKeyword, SearchName, FProjectNo, FilterId, SubObject);
            return Content(message);
        }

        [HttpPost]
        public ActionResult DeleteFilterData(string filterid)
        {
            string message = _SolrDocumentIndexService.DeleteFilterData(Convert.ToInt64(filterid));
            return Content(message);
        }

        [HttpGet]
        public JsonResult GetSavedFilterdata(string filterId)
        {
            List<AdvanceSearchModel> filterlist = GetSavedFilter(filterId);

            return Json(filterlist, JsonRequestBehavior.AllowGet);
        }

        private List<AdvanceSearchModel> GetSavedFilter(string filterId)
        {
            List<AdvanceSearchModel> filterlist = new List<AdvanceSearchModel>();
            filterlist = _SolrDocumentIndexService.FetchSavedFilterData(Convert.ToInt64(filterId), Convert.ToInt64(CommonService.objClsLoginInfo.UserName)).Select(s => new AdvanceSearchModel
            {
                CustomerName = s.CustomerName,
                BU = s.BUPBU,
                Department = s.Department,
                ObjectId = Convert.ToInt64(s.Object),
                ObjID = s.Object,
                ObjectName = s.ObjectName,
                Status = s.Status,
                Revision = s.Revision,
                Originator = s.Originator,
                OwnerName = s.Owner,
                SearchText = s.SearchKeyword,
                ProjectNumber = s.ProjectNumber,
                FilterId = s.PK_SearchFilterId.ToString(),
                SubObjectId = s.SubObject
            }).ToList();
            return filterlist;
        }

        [HttpGet]
        public JsonResult GetFilterData()
        {
            List<SelectListItem> filtervalues = new List<SelectListItem>();
            filtervalues = _SolrDocumentIndexService.BindFilterdata(Convert.ToInt64(CommonService.objClsLoginInfo.UserName)).Select(x => new SelectListItem { Value = x.PK_SearchFilterId.ToString(), Text = x.FilterName }).ToList();
            filtervalues.Insert(0, new SelectListItem() { Value = "0", Text = "Select Filter" });
            filtervalues.Insert(1, new SelectListItem() { Value = "00", Text = "Save this Filter" });


            return Json(filtervalues, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetFilterDataForOuter()
        {
            List<SelectListItem> filtervalues = new List<SelectListItem>();
            filtervalues = _SolrDocumentIndexService.BindFilterdata(Convert.ToInt64(CommonService.objClsLoginInfo.UserName)).Select(x => new SelectListItem { Value = x.PK_SearchFilterId.ToString(), Text = x.FilterName }).ToList();
            filtervalues.Insert(0, new SelectListItem() { Value = "0", Text = "Select Filter" });
            //filtervalues.Insert(1, new SelectListItem() { Value = "00", Text = "Save this Filter" });


            return Json(filtervalues, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDCRIdByDCRSupportedDocId(Int64 Id)
        {
            var objDCR = _DCRService.GetDCRIdByDCRSupportedDocId(Id);
            return Json(new { DCRId= (objDCR!=null? objDCR.DCRId:0), DCRNo = (objDCR != null ? objDCR.DCRNo : "") });
        }
    }
}