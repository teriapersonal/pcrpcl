﻿using IEMQS.DESCore;
using IEMQS.DESServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.DES.Controllers
{
    [IEMQSImplementation.clsBase.SessionExpireFilter]
    [Models.EncryptedActionParameter]
    public class ItemGroupController : Controller
    {
        IItemGroupService _IItemGroupService;
        public ItemGroupController(IItemGroupService ItemGroupService)
        {
            _IItemGroupService = ItemGroupService;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(DataTableParamModel parm)
        {
            int recordsTotal = 0;
            var list = _IItemGroupService.GetItemGroupList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }
    }
}