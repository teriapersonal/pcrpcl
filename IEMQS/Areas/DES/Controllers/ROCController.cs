﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQS.DESCore;
using IEMQS.DESServices;

namespace IEMQS.Areas.DES.Controllers
{
    [Models.ProjectFolderAuthorization]
    [IEMQSImplementation.clsBase.SessionExpireFilter]
    [Models.EncryptedActionParameter]
    public class ROCController : Controller
    {
        IROCService _ROCService;
        IAgencyService _AgencyService;
        public ROCController(IROCService ROCService, IAgencyService AgencyService)
        {
            _ROCService = ROCService;
            _AgencyService = AgencyService;
        }
        // GET: DES/ROC
        public ActionResult Index()
        {
            ROCModel _ROCModel = new ROCModel();
            _ROCModel.CurrentPsNo = CommonService.objClsLoginInfo.UserName;
            return View(_ROCModel);
        }

        [HttpPost]
        public JsonResult BindDocRevision(string DocNo)
        {
            List<DOCRevModel> objDOCRevModel = new List<DOCRevModel>();
            objDOCRevModel = _ROCService.BindDocRevision(DocNo);
            return Json(objDOCRevModel, JsonRequestBehavior.AllowGet);
        }

        #region ROC
        public JsonResult Add(long documentId, long AgencyId, string Project)
        {
            string errorMsg = "";
            Int64 status = _ROCService.Add(documentId, AgencyId, Project, out errorMsg);

            if (status > 0)
            {
                return Json(new { Status = true, Msg = "ROC added successfully", ROCId = CommonService.Encrypt("rocID=" + status) }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                {
                    return Json(new { Status = false, Msg = errorMsg, IsRevised = true }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
                }

            }
        }

        public ActionResult Edit(long rocId, string Rev)
        {
            ROCModel _ROCModel = new ROCModel();
            IpLocation ipLocation = CommonService.GetUseIPConfig;
            bool Edit = true;
            _ROCModel.IsView = false;
            string errorMsg = "";

            //if (Rev=="true")
            //{
            //    Int64? folderId = Models.SessionMgtModel.ProjectFolderList.FolderId;
            //    _ROCModel.Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo; // Get PRoject No. For generat  Doc No.
            //    _ROCModel.FolderId = folderId;
            //    _ROCModel.CurrentLocation = CommonService.GetUseIPConfig.Location;
            //    if (RevPaths != null)
            //    {
            //        foreach (var item in RevPaths)
            //        {
            //            _ROCModel.newPathAfterRevise.Add(item);
            //        }
            //    }
            //    Int64? NewCreatedROCId  = _ROCService.AddRevisedData(_ROCModel);

            //    _ROCModel = _ROCService.Edit(NewCreatedROCId??0, Rev, Edit);
            //}
            //else
            //{
            _ROCModel = _ROCService.Edit(rocId, Rev, Edit);
            // }
            _ROCModel.CurrentLocationIp = ipLocation.Ip;
            _ROCModel.FilePath = ipLocation.File_Path;
            ViewBag.AgencyList = new SelectList(_AgencyService.GetAllAgency(Models.SessionMgtModel.ProjectFolderList.BUId), "AgencyId", "AgencyName");
            _ROCService.UpdateROCStatusCheckin(rocId, out errorMsg);
            return View(_ROCModel);
        }

        public ActionResult Detail(long rocId, string Rev)
        {
            ROCModel _ROCModel = new ROCModel();
            bool Edit = false;
            _ROCModel = _ROCService.Edit(rocId, Rev, Edit);
            _ROCModel.IsView = true;
            _ROCModel.CurrentLocationIp = CommonService.GetUseIPConfig.Ip;
            ViewBag.AgencyList = new SelectList(_AgencyService.GetAllAgency(Models.SessionMgtModel.ProjectFolderList.BUId), "AgencyId", "AgencyName");
            return View(_ROCModel);
        }

        [HttpPost]
        public ActionResult GetROCList(DataTableParamModel parm, Int64? DocumentId, string Project)
        {
            if (Project == null)
            {
                Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo; // Get PRoject No. For generat  Doc No.
            }
            ViewBag.CurrentProject = Project;
            int recordsTotal = 0;
            var Roles = CommonService.objClsLoginInfo.UserRoles;
            var list = _ROCService.GetROCList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal, DocumentId, Models.SessionMgtModel.ProjectFolderList.FolderId, Project,Roles);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpPost]
        public ActionResult ShowROCPopup(long documentId, string docProject)
        {
            ROCModel _ROCModel = new ROCModel();
            _ROCModel.DocumentId = documentId;
            _ROCModel.Project = docProject;
            ViewBag.AgencyList = new SelectList(_AgencyService.GetAllAgency(Models.SessionMgtModel.ProjectFolderList.BUId), "AgencyId", "AgencyName");
            return PartialView("_ROCList", _ROCModel);
        }
        #endregion


        #region ROC Comment


        public JsonResult GetSrNo(long ROCId, int DocumentRevision)
        {

            double? SrNo = _ROCService.GetSrNo(ROCId, DocumentRevision);
            if (SrNo > 0)
            {
                return Json(new { Status = true, SrNo = SrNo }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult GetROCCommentsList(DataTableParamModel parm, Int64 ROCId)
        {
            int recordsTotal = 0;
            var list = _ROCService.GetROCCommentsList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal, ROCId);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpPost]
        public ActionResult AddROCComment(ROCCommentModel model)
        {


            Int64 status = _ROCService.AddROCComment(model);
            if (status > 0)
            {
                model.ROCCommentsId = status;
                if (model.ROCCommentsId > 0)
                {

                    return Json(new { Status = true, Msg = "ROC Comments added successfully" }, JsonRequestBehavior.AllowGet);
                }

            }

            return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult EditROCComment(ROCModel model, string btnsubmit)
        {
            string errorMsg = "";
            string successMsg = "";
            bool status = false;

            if (model.IsRevised == true)
            {
                //Int64? folderId = Models.SessionMgtModel.ProjectFolderList.FolderId;
                //model.FolderId = folderId;
                //Int64? rocId = _ROCService.AddRevisedData(model);
            }

            if (model.IsCommentChanges)
            {
                status = _ROCService.EditROCComment(model);
                successMsg = "ROC Comments updated successfully";
            }
            if (btnsubmit == "submit")
            {
                //submit code with validation 
                status = _ROCService.UpdateROCStatusSubmit(model.ROCId, out errorMsg);
                successMsg = "ROC Submitted successfully";
            }
            else if (btnsubmit == "SaveDraft")
            {
                //save as dreft
                //bool status = _ROCService.UpdateAgencySaveAsDraft(ROCId, AgencyId);
                status = _ROCService.UpdateROCStatusCheckin(model.ROCId, out errorMsg);
                successMsg = "Save As Draft successfully";
            }

            if (status)
            {
                TempData["Success"] = successMsg;
                return Json(new {Status = status, Msg = successMsg, SubmitType = btnsubmit }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }


        }


        [HttpPost]
        public JsonResult DeleteROCComment(Int64 ROCCommentId)
        {
            string errorMsg = "";

            Int64 status = _ROCService.DeleteROCComment(ROCCommentId, out errorMsg);
            if (status > 0)
            {
                return Json(new { Status = true, Msg = "ROC Comment deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion


        #region ROC Comments Attach File Upload
        [HttpPost]//ROC Comments Attach File Add/Edit
        public JsonResult CreateROCCommentsAttach(ROCCommentsAttachModel model)
        {
            string errorMsg = "";
            IpLocation ipLocation = CommonService.GetUseIPConfig;
            model.FileLocation = ipLocation.Location;

            Int64 status = _ROCService.AddEditCreateROCCommentsAttach(model, out errorMsg);
            if (status > 0)
            {


                return Json(new { Status = true, Msg = "File added successfully" }, JsonRequestBehavior.AllowGet);


            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]  //Get ROC Comments Attach List For Grid
        public ActionResult GetROCCommentsAttachList(DataTableParamModel parm, Int64 ROCCommentsId,
        bool? IsAgencyComment = null, bool? IsLandTComment = null, bool? IsAgencyResponse = null)
        {
            int recordsTotal = 0;
            var list = _ROCService.GetROCCommentsAttachList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal, ROCCommentsId, IsAgencyComment, IsLandTComment, IsAgencyResponse);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        // ROC Comments Attach Delete
        public JsonResult DeleteROCCommentsAttach(Int64 Id)
        {
            string errorMsg = "";
            Int64 status = _ROCService.DeleteROCCommentsAttach(Id, out errorMsg);
            if (status > 0)
            {
                return Json(new { Status = true, Msg = "File deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region ROC Commented Document File Upload
        [HttpPost]//ROC Commented Document File Add/Edit
        public JsonResult CreateCommentedDocument(ROCCommentedDocModel model)
        {
            IpLocation ipLocation = CommonService.GetUseIPConfig;
            model.FileLocation = ipLocation.Location;

            string errorMsg = "";
            Int64 status = _ROCService.AddEditCommentedDocument(model, out errorMsg);

            if (status > 0)
            {
                return Json(new { Status = true, Msg = "File added successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]  //Get ROC Commented Document List For Grid
        public ActionResult GetROCCommentedDocumentList(DataTableParamModel parm, Int64 ROCId)
        {
            var Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            int recordsTotal = 0;
            var list = _ROCService.GetROCCommentedDocumentList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal, ROCId, Project);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        // ROC Comments Attach Delete
        public JsonResult DeleteROCCommentedDocument(Int64 Id)
        {
            string errorMsg = "";
            Int64 status = _ROCService.DeleteROCCommentedDocument(Id, out errorMsg);
            if (status > 0)
            {
                return Json(new { Status = true, Msg = "File deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetAllAttachmentPath(ROCComment obj)
        {
            string errorMsg = "";
            List<ROCFileAttachModel> data = _ROCService.GetAllAttachmentPath(obj.ROCCommentId);
            if (data != null)
            {
                return Json(new { Status = true, AttachFiles = data }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult GetDataOfROCForRev(Int64? RocId)
        {
            string errorMsg = "";

            //string a = CommonService.Decrypt(rocId);
            //var b = a.Remove(0, 6);
            //Int64? RocId = Convert.ToInt64(b);
            var data = _ROCService.GetAllAttachmentPathForRev(RocId);
            var Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            var CurrentLocationIp = CommonService.GetUseIPConfig.Ip;
            var CurrentLocationPath = CommonService.GetUseIPConfig.File_Path;
            if (data != null)
            {
                return Json(new { Status = true, AttachFiles = data, AtProject = Project, Ip = CurrentLocationIp ,Path= CurrentLocationPath }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        [HttpPost]
        public JsonResult NewRevision(ROCModel _ROCModel)
        {

            Int64? folderId = Models.SessionMgtModel.ProjectFolderList.FolderId;
            _ROCModel.Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo; // Get PRoject No. For generat  Doc No.
            _ROCModel.FolderId = folderId;
            _ROCModel.CurrentLocation = CommonService.GetUseIPConfig.Location;
            //string a = CommonService.Decrypt(_ROCModel.EncryptedROCId);
            //var b = a.Remove(0, 6);
            //_ROCModel.ROCId = Convert.ToInt64(b);
            //if (_ROCModel.newPathAfterRevise != null)
            //{
            //    foreach (var item in _ROCModel.newPathAfterRevise)
            //    {
            //        _ROCModel.newPathAfterRevise.Add(item);
            //    }
            //}
            Int64? NewCreatedROCId = _ROCService.AddRevisedData(_ROCModel);

            if (NewCreatedROCId != null)
            {
                return Json(new { Status = true, NewROCId = CommonService.Encrypt("rocID=" + NewCreatedROCId) }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }

        }



        [HttpPost]//Document File Mapping Add/Edit
        public ActionResult GetAllVersion(ROCModel rocMapping)
        {
            ViewBag.RocRefNo = rocMapping.ROCRefNo;
            return PartialView("_partialVersionByROC");

        }

        [HttpPost]
        public ActionResult GetAllROCVersions(DataTableParamModel parm, string rocRefNo)
        {
            string errorMsg = "";
            int recordsTotal = 0;
            string Roles = CommonService.objClsLoginInfo.UserRoles;
            var list = _ROCService.GetROCListByROCID(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal, rocRefNo);
            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }



        [HttpPost]
        public ActionResult GetAllHistory(ROCModel rocData)
        {
            ViewBag.ROCId = rocData.ROCId;
            return PartialView("_partialROCHistory");
        }

        [HttpPost]
        public ActionResult GetAllROCHistory(DataTableParamModel parm, Int64? ROCId)
        {
            string errorMsg = "";
            int recordsTotal = 0;
            var list = _ROCService.GetROCHistory(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal, ROCId);
            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpGet]
        public void ExportToExcel(Int64? ROCId)
        {
            _ROCService.ExportToExcel(ROCId);
        }


        [HttpPost]  // After Submit, CreatedBy/Date will Update
        public JsonResult UpdateROCCheckOut(int ROCId)
        {
            var Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            string errorMsg = "";

            var psno = CommonService.objClsLoginInfo.UserName;
            if (ROCId != null)
            {
                bool IsChekIn = _ROCService.UpdateIsCheckInROC(ROCId);
                return Json(new { Status = true, Msg = "ROC has been release from edit" }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult GetJEPData(int ROCId)
        {
            var data = _ROCService.GetJEPData(ROCId);
            if (data.Count > 0)
            {

                return Json(new { Data = data, Status = true, Msg = "ROC Comments added successfully" }, JsonRequestBehavior.AllowGet);

            }
            else
            {

                return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }



    }
}