﻿using IEMQS.Areas.DES.Models;
using IEMQS.DESCore;
using IEMQS.DESServices;
using IEMQSImplementation;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.DES.Controllers
{
    [Models.ProjectFolderAuthorization]
    [IEMQSImplementation.clsBase.SessionExpireFilter]
    [Models.EncryptedActionParameter]
    public class DINController : Controller
    {
        ICommonService _CommonService;
        IDINService _DINService;
        IDepartmentGroupService _DepartmentGroupService;

        public DINController(ICommonService CommonService, IDINService DINService)
        {
            _CommonService = CommonService;
            _DINService = DINService;
        }
        // GET: DES/DIN
        public ActionResult Index()
        {
            DINModel model = new DINModel();
            model.CurrentPsNo = CommonService.objClsLoginInfo.UserName;
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(DataTableParamModel parm)
        {
            Int64? folderId = Models.SessionMgtModel.ProjectFolderList.FolderId;
            string Roles = CommonService.objClsLoginInfo.UserRoles;
            string project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            int recordsTotal = 0;
            var list = _DINService.GetDINList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, folderId, Roles, project, out recordsTotal);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpGet]
        public ActionResult Add()
        {
            if (Models.SessionMgtModel.ProjectFolderList.FolderId > 0)
            {
                DINModel model = new DINModel();
                model.Department = CommonService.objClsLoginInfo.Department;
                model.RoleGroup = Models.SessionMgtModel.ProjectFolderList.RoleGroup;
                // ViewBag.YesOrNoListNuclearASME = new SelectList(_CommonService.GetYesOrNo(), "Text", "Text");
                ViewBag.DistributionGroup = new SelectList(_DINService.GetDistributionGroup(), "DINGroupId", "GroupName");
                string getProjectNo = SessionMgtModel.ProjectFolderList.ProjectNo;
                ViewBag.ProjectNumber = getProjectNo;
                ProjectModel getDataOfProject = _DINService.GetProjectDetails(SessionMgtModel.ProjectFolderList.ProjectNo);
                ViewBag.Contract = getDataOfProject.Contract;
                ViewBag.ProjectDescription = getDataOfProject.ProjectDescription;
                ViewBag.Customer = _DINService.GetCustomerName(getDataOfProject.Customer);
                model.ASMETag = getDataOfProject.CodeStamp;
                model.CustomerForDINReport = getDataOfProject.CustomerForDINReport;
                model.FolderId = Models.SessionMgtModel.ProjectFolderList.FolderId;
                var Roles = CommonService.objClsLoginInfo.UserRoles;

                ViewBag.GetPolicySteps = _DINService.GetDINUserLevel(model.RoleGroup, Roles, 0, model.Department);
                ViewBag.GetLifeCycle = _DINService.GetPolicySteps(ObjectName.DIN.ToString(), Models.SessionMgtModel.ProjectFolderList.BUId, null);

                // var checkIssueNo = _DINService.GenerateIssueNo(getProjectNo);
                ViewBag.NextIssueNo = 1;

                ViewBag.NextDINNo = _DINService.GenerateDINNo(getProjectNo);
                ViewBag.GetContractProjectList = new SelectList(_DINService.GetContractProjectList(getProjectNo), "Value", "Text");
                return View(model);
            }
            else
            {
                TempData["Error"] = "Select folder";
                return RedirectToAction("Details", "Project", new { ProjectNo = Models.SessionMgtModel.ProjectFolderList.ProjectNo });
            }
        }

        [HttpPost] // DIN Add/Edit
        public ActionResult AddEditDIN(DINModel model)
        {
            string errorMsg = "";
            model.FolderId = Models.SessionMgtModel.ProjectFolderList.FolderId;
            model.BUId = Models.SessionMgtModel.ProjectFolderList.BUId;
            model.ProjectNo = SessionMgtModel.ProjectFolderList.ProjectNo;
            //bool flag = _DINService.EditProjectDetails(SessionMgtModel.ProjectFolderList.ProjectNo, model.CustomerForDINReport);
            Int64? status = _DINService.AddEdit(model, out errorMsg);
            if (status > 0)
            {

                if (model.DINId > 0)
                    return Json(new { status, Status = true, Msg = "DIN updated successfully" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { status, Status = true, Msg = "DIN added successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new
                    {
                        Status = false,
                        Msg = errorMsg
                    }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetJEPDocList(DataTableParamModel parm, Int64 DINId)
        {
            int recordsTotal = 0;
            var list = _DINService.GetJEPDocList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, SessionMgtModel.ProjectFolderList.ProjectNo, DINId, out recordsTotal);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpPost]
        public ActionResult GetGlobalJEPDocList(DataTableParamModel parm, string Project, string DocumentNo, Int64 DINId)
        {
            int recordsTotal = 0;
            var list = _DINService.GetGlobalJEPDocList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal, Project == null ? "" : Project, DocumentNo == null ? "" : DocumentNo, SessionMgtModel.ProjectFolderList.ProjectNo, DINId);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpPost]
        public ActionResult AddDINDoc(DINRefDocumentModel model)
        {
            string errorMsg = "";
            Int64 status = 0;
            foreach (var item in model.DocumentIdList.ToList())
            {

                model.DocumentId = item.DocId;
                status = _DINService.AddDinRefDocument(model, out errorMsg);
            }
            if (status > 0)
            {
                if (model.DINRefDocumentId > 0)
                    return Json(new { Status = true, Msg = "Document updated successfully" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = true, Msg = "Document added successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetJepDocumentDetails(DataTableParamModel parm, Int64? DINId)
        {
            int recordsTotal = 0;
            string Roles = CommonService.objClsLoginInfo.UserRoles;
            var list = _DINService.GetJepDocumentDetails(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, DINId, Roles, out recordsTotal);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }


        [HttpPost]
        public JsonResult UpdateGenralRemark(DINRefDocumentModel model)
        {
            string errorMsg = "";
            Int64 status = _DINService.UpdateGenralRemark(model);
            if (status > 0)
            {
                return Json(new { Status = true, Msg = "Remarks updated successfully" }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DeleteDINRefDocument(Int64 Id)
        {
            string errorMsg = "";
            Int64 status = _DINService.DeleteDINRefDocument(Id, out errorMsg);
            if (status > 0)
            {
                return Json(new { Status = true, Msg = "Reference document deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult UpdateGird(DINRefDocumentModel DocDetail)
        {
            string errorMsg = "";
            string infoMsg = "";
            bool status = _DINService.UpdateGird(DocDetail, out errorMsg, out infoMsg);
            if (status)
            {
                if (!string.IsNullOrEmpty(errorMsg))
                {
                    return Json(new { Status = true, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var InfoMsgFlag = "true";
                    return Json(new { InfoMSGS = InfoMsgFlag, Status = true, Msg = infoMsg }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DeleteDIN(Int64 DINId)
        {
            string errorMsg = "";
            Int64 status = _DINService.DeleteDIN(DINId, out errorMsg);
            if (status > 0)
            {
                return Json(new { Status = true, Msg = "DIN deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Edit(Int64? Id)
        {
            DINModel model = _DINService.GetDocumentByID(Id);
            //ViewBag.YesOrNoListNuclearASME = new SelectList(_CommonService.GetYesOrNo(), "Text", "Text");
            ViewBag.DistributionGroup = new SelectList(_DINService.GetDistributionGroup(), "DINGroupId", "GroupName");
            model.FolderId = Models.SessionMgtModel.ProjectFolderList.FolderId;
            string getProjectNo = SessionMgtModel.ProjectFolderList.ProjectNo;
            ViewBag.ProjectNumber = getProjectNo;
            ProjectModel getDataOfProject = _DINService.GetProjectDetails(SessionMgtModel.ProjectFolderList.ProjectNo);
            ViewBag.YesOrNoListASMETag = getDataOfProject.ASMETag;
            ViewBag.Contract = getDataOfProject.Contract;
            //model.CustomerForDINReport = getDataOfProject.CustomerForDINReport;
            ViewBag.ProjectDescription = getDataOfProject.ProjectDescription;
            ViewBag.Customer = _DINService.GetCustomerName(getDataOfProject.Customer);
            var Roles = CommonService.objClsLoginInfo.UserRoles;
            ViewBag.GetPolicySteps = _DINService.GetDINUserLevel(model.RoleGroup, Roles, 0, model.Department);
            if (model.Status == ObjectStatus.Completed.ToString())
            {
                ViewBag.RelaseDate = model.EditedOn;
                ViewBag.RelaseBy = new clsManager().GetUserNameFromPsNo(model.EditedBy);
            }
            ViewBag.GetLifeCycle = _DINService.GetPolicySteps(ObjectName.DIN.ToString(), Models.SessionMgtModel.ProjectFolderList.BUId, Id);
            ViewBag.GetContractProjectList = new SelectList(_DINService.GetContractProjectList(getProjectNo), "Value", "Text");
            bool HasPSNo = true;
            bool IsChekIn = _DINService.UpdateIsCheckInDIN(Id, HasPSNo);
            return View("Add", model);
        }

        [HttpPost] // DIN Add/Edit
        public JsonResult UpdatePolicy(DINModel model)
        {
            string errorMsg = "";

            var Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            if (model.DINId != null)
            {
                bool IsHaveAnyDocument = _DINService.IsContainDocument(model.DINId);
                if (IsHaveAnyDocument != true)
                {
                    TempData["Info"] = "Please add atleast one Document";
                    return Json(new { Status = false, IsInfo = true, Msg = "Please add atleast one Document" }, JsonRequestBehavior.AllowGet);
                }
            }
            Int64? status = _DINService.AddStatus(model, Project, out errorMsg);
            if (status > 0)
            {
                string rurl = "/DES/DIN";
                if (Models.SessionMgtModel.ProjectFolderList.FolderId != null)
                {
                    rurl = "/DES/Project/Folder?q=" + CommonService.Encrypt("Id=" + Models.SessionMgtModel.ProjectFolderList.FolderId);
                }
                TempData["Success"] = "DIN distributed successfully";
                return Json(new { status, Status = true, Msg = "DIN distributed successfully", RedirectAction = rurl }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                {
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
                }
            }

        }


        public ActionResult Detail(Int64 Id)
        {
            DINModel model = _DINService.GetDocumentByID(Id);
            model.CurrentPsNo = CommonService.objClsLoginInfo.UserName;
            // ViewBag.YesOrNoListNuclearASME = new SelectList(_CommonService.GetYesOrNo(), "Text", "Text");
            ViewBag.DistributionGroup = new SelectList(_DINService.GetDistributionGroup(), "DINGroupId", "GroupName");
            string getProjectNo = SessionMgtModel.ProjectFolderList.ProjectNo;
            //ProjectModel getDataOfProject = _DINService.GetProjectDetails(SessionMgtModel.ProjectFolderList.ProjectNo);
            ViewBag.ProjectNumber = model.ProjectNo;
            ViewBag.Contract = model.ContractNo;
            ViewBag.ProjectDescription = model.ProjectDescription;
            ViewBag.Customer = model.Customer;
            ViewBag.YesOrNoListASMETag = model.ASMETag;
            ViewBag.CustomerForDINReport = model.CustomerForDINReport;
            //model.CustomerForDINReport = getDataOfProject.CustomerForDINReport;
            model.Department = CommonService.objClsLoginInfo.Department;
            model.BUId = Models.SessionMgtModel.ProjectFolderList.BUId;
            var currentUser = CommonService.objClsLoginInfo.UserName;
            if (model.Status != ObjectStatus.Draft.ToString())
            {
                model.DINAckID = _DINService.IsAllowACkTOUser(Id, model.Department, currentUser);
                if (model.DINAckID > 0)
                {
                    var Roles = CommonService.objClsLoginInfo.UserRoles;
                    ViewBag.GetPolicySteps = _DINService.GetDINUserLevel(model.RoleGroup, Roles, Id, model.Department);
                }
            }
            if (model.Status == ObjectStatus.Completed.ToString())
            {
                ViewBag.RelaseDate = model.EditedOn;
                ViewBag.RelaseBy = new clsManager().GetUserNameFromPsNo(model.EditedBy);
                if (model.EditedBy != null)
                {
                    ViewBag.RelaseByPSNO = model.EditedBy;
                }
            }
            ViewBag.GetLifeCycle = _DINService.GetPolicySteps(ObjectName.DIN.ToString(), Models.SessionMgtModel.ProjectFolderList.BUId, Id);
            // var checkIssueNo = _DINService.GenerateIssueNo(getProjectNo);
            // ViewBag.NextIssueNo = checkIssueNo;
            // model.ACKIUrl = "/DES/DIN/AcknowledgeDIN?q=" + CommonService.Encrypt("DinId=" + model.DINId);
            return View(model);
        }

        [HttpPost]
        public JsonResult CheckDINNo(string DINNoGenratorPrefix, string DINNo, Int64? DINId, string ProjectNo, int IssueNo)
        {
            return Json(!_DINService.CheckDINNo(DINNoGenratorPrefix + DINNo, DINId, ProjectNo, IssueNo), JsonRequestBehavior.AllowGet);
        }

        public ActionResult AcknowledgeDIN(Int64? DinId)
        {

            DINModel model = _DINService.GetDocumentByID(DinId);
            var currentUser = CommonService.objClsLoginInfo.UserName;

            model.DINAckID = _DINService.IsAllowACkTOUser(DinId, model.Department, currentUser);
            if (model.DINAckID > 0)
            {
                var Roles = CommonService.objClsLoginInfo.UserRoles;
                ViewBag.GetPolicySteps = _DINService.GetDINUserLevel(model.RoleGroup, Roles, DinId, model.Department);
                if (ViewBag.GetPolicySteps.Count > 0)
                {
                    if (ViewBag.GetPolicySteps[0].PolicyOrderID == 1)
                    {
                        ViewBag.IsFirstStep = ViewBag.GetPolicySteps[0].PolicyOrderID;
                    }
                }
            }
            ViewBag.GetLifeCycle = _DINService.GetPolicySteps(ObjectName.DIN.ToString(), Models.SessionMgtModel.ProjectFolderList.BUId, DinId);

            ViewBag.DistributionGroup = new SelectList(_DINService.GetDistributionGroup(), "DINGroupId", "GroupName");

            return View(model);
        }

        [HttpPost]
        public JsonResult AddAcknowledgeNote(DINModel AckData)
        {
            string errorMsg = "";
            Int64 status = _DINService.AddAcknowledgeNote(AckData.DINId, AckData.DINAcknowledgeNote);
            if (status > 0)
            {
                return Json(new { Status = true, Msg = "Acknowledge Note updated successfully" }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult BackToReview(Int64 Id)
        {
            string errorMsg = "";
            string Qstring = string.Empty;
            Int64? status = _DINService.ReviseDINPolicy(Id, out errorMsg);

            if (status > 0)
            {
                TempData["Success"] = "Review Has Been Applied.";
                Qstring = CommonService.Encrypt("Id=" + status);
            }

            return RedirectToAction("Detail", new { q = Qstring });
        }

        public ActionResult DINLifeCycle(Int64 Id, Int64 DINAckID, Int64 DINID)
        {
            string errorMsg = "";
            string infoMsg = "";
            string Qstring = string.Empty;
            Int64? status = _DINService.UpdateDINPolicy(Id, DINID, out errorMsg, out infoMsg);
            var Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;

            if (status == 0 && !string.IsNullOrEmpty(errorMsg))
            {
                TempData["Info"] = errorMsg;
            }
            else if (status > 0 && !string.IsNullOrEmpty(infoMsg))
            {
                TempData["Success"] = infoMsg;
                Int64? AckStatus = _DINService.UpdateAckStatus(DINAckID, DINID, Project, out errorMsg);
            }

            if (status != null)
            {
                Qstring = CommonService.Encrypt("Id=" + status);
            }

            return RedirectToAction("Detail", new { q = Qstring });

        }


        [HttpPost]
        public ActionResult GetAllHistory(DINModel dinData)
        {
            ViewBag.DINId = dinData.DINId;
            ViewBag.DINNo = dinData.DINNo;
            return PartialView("_partialDINHistory");
        }

        [HttpPost]
        public ActionResult GetAllDINHistory(DataTableParamModel parm, string DINNo)
        {
            string errorMsg = "";
            int recordsTotal = 0;
            var list = _DINService.GetDINHistory(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, DINNo, out recordsTotal);
            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        //[HttpPost]
        //public JsonResult AddDINACK(DINAck AckModel)
        //{
        //    var Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
        //    string errorMsg = "";
        //    Int64? status = _DINService.AddAck(AckModel, Project, out errorMsg);
        //    if (status > 0)
        //    {
        //        return Json(new { Status = true, Msg = "Created Acknowledgement for DIN" }, JsonRequestBehavior.AllowGet);

        //    }
        //    else
        //    {
        //        if (!string.IsNullOrEmpty(errorMsg))
        //            return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
        //        else
        //            return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
        //    }
        //}

        [HttpPost]
        public ActionResult GetAllACK(DINModel dinData)
        {

            ViewBag.DINId = dinData.DINId;
            return PartialView("_partialDINViewACK");
        }

        [HttpPost]
        public ActionResult GetAllDINACK(DataTableParamModel parm, Int64? DINId)
        {
            string errorMsg = "";
            int recordsTotal = 0;
            var list = _DINService.ViewACKData(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, DINId, out recordsTotal);
            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpPost]
        public ActionResult GetDatabyGroupId(DINModel dbGroupId)
        {

            ViewBag.DistributionGroupId = dbGroupId.DistributionGroupId;
            return PartialView("_partialDINSelectedGroup");
            //var status = _DINService.getDataByGroupID(dbGroupId.DistributionGroupId);

            //DINGroupModel model = new DINGroupModel();
            //string dept = string.Empty;

            //return View();
        }

        [HttpPost]
        public JsonResult GetDINDataByGroupID(DINModel model)
        {
            string errorMsg = "";
            var data = _DINService.getDataByGroupID(model.DistributionGroupId);
            if (data != null)
            {
                return Json(new { data, Status = true, Msg = "Remarks updated successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult CreateDINRev(DINModel model)
        {
            string errorMsg = "";
            Int64? BuId = Models.SessionMgtModel.ProjectFolderList.BUId;
            string getProjectNo = SessionMgtModel.ProjectFolderList.ProjectNo;

            int NextGenratedNo = _DINService.GenerateDINNo(getProjectNo);
            string NewGenrateDINNo = "DIN-" + getProjectNo + "-" + NextGenratedNo;
            string Department = CommonService.objClsLoginInfo.Department;
            //var checkIssueNo = _DINService.GenerateIssueNo(getProjectNo);
            //int NewGenratedIssueNo = checkIssueNo;

            var data = _DINService.DinRevision(model.DINId, BuId, NewGenrateDINNo, Department);
            if (data != null)
            {
                string rurl = "/DES/DIN/Edit?q=" + CommonService.Encrypt("Id=" + data);
                TempData["Success"] = "New Issue has been created successfully";
                return Json(new { rurl, data, Status = true, Msg = "New Issue has been created successfully" }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }




        //// Fill Gird
        //[HttpPost]
        //public ActionResult GetPendingDIN(DataTableParamModel parm)
        //{
        //    int recordsTotal = 0;
        //    string psno = CommonService.objClsLoginInfo.UserName;
        //    string Roles = CommonService.objClsLoginInfo.UserRoles;

        //    var list = _DINService.GetPendingDINList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, psno, Roles, out recordsTotal);


        //    return Json(new
        //    {
        //        Echo = parm.sEcho,
        //        iTotalRecord = recordsTotal,
        //        iTotalDisplayRecords = recordsTotal,
        //        data = list
        //    });
        //}


        [HttpPost]
        public JsonResult SaveAsDraft(DINModel model)
        {
            string errorMsg = "";
            string infoMsg = "";
            bool status = _DINService.DINSaveAsDraft(model.DINId);
            if (status)
            {
                var rurl = "/DES/DIN/";
                TempData["Success"] = "DIN has been created";
                return Json(new { RedirectAction = rurl, Status = true, Msg = "DIN has been created" }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                {
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]  // After Submit, CreatedBy/Date will Update
        public JsonResult UpdateDINCheckOut(Int64? DINId)
        {
            var Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            string errorMsg = "";

            var psno = CommonService.objClsLoginInfo.UserName;
            if (DINId != null)
            {
                bool HasPSNo = false;
                bool IsChekIn = _DINService.UpdateIsCheckInDIN(DINId, HasPSNo);
                return Json(new { Status = true, Msg = "DIN has been release from edit" }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]

        public ActionResult GetDistributeDOCData(DINModel model)
        {
            string errorMsg = "";
            List<DINDistributeDocument> DINDISData = _DINService.GetDistributeDOCData(model.DINId);

            if (DINDISData != null)
            {
                if (DINDISData.Count > 0)
                {
                    var status = DINDISData.GroupBy(c => c.JEPDocumentDetailsId, (key, c) => c.ToList()).ToList();
                    return Json(new { Data = status, Status = true, Msg = "Save as draft scucessfully." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status = true, Msg = "Please add atleast one document." }, JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }

        }


        [HttpPost]

        public ActionResult AddDocDistribute(DINDistributeDocument model)
        {
            string errorMsg = "";
            bool IsDistributeDataAdded = _DINService.AddDocDistribute(model);

            if (IsDistributeDataAdded != false)
            {
                return Json(new { Status = true, Msg = "Save as draft scucessfully." }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]

        public ActionResult AddDocElectronicDistribute(DINDistributeDocument model)
        {
            string errorMsg = "";
            bool IsDistributeDataAdded = _DINService.AddDocElectronicDistribute(model);

            if (IsDistributeDataAdded != false)
            {
                return Json(new { Status = true, Msg = "Save as draft scucessfully." }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }

        }


        [HttpPost]
        public ActionResult IsDocumentComplete(DINModel dINModel)
        {
            string errorMsg = "";
            string Roles = CommonService.objClsLoginInfo.UserRoles;
            string Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            var CurrentUser = CommonService.objClsLoginInfo.UserName;
            string IsDistributeDataAdded = _DINService.IsDocumentComplete(dINModel.DocumentNo, dINModel.DINId, Project, CurrentUser, Roles);

            if (IsDistributeDataAdded != null)
            {
                return Json(new { Status = true, RedirectAction = IsDistributeDataAdded }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }

        }



        [HttpPost]
        public ActionResult DocumentData(DINModel dINModel)
        {
            string errorMsg = "";
            string Roles = CommonService.objClsLoginInfo.UserRoles;
            string Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            var CurrentUser = CommonService.objClsLoginInfo.UserName;
            string IsDistributeDataAdded = _DINService.DocumentData(dINModel.DocumentNo, dINModel.DINId, Project, CurrentUser, Roles);

            if (IsDistributeDataAdded != null)
            {
                return Json(new { Status = true, RedirectAction = IsDistributeDataAdded }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }

        }
  
    }
}