﻿using IEMQS.DESCore;
using IEMQS.DESServices;
using Newtonsoft.Json;
using System;
using System.Web.Mvc;

namespace IEMQS.Areas.DES.Controllers
{
    [Models.ProjectFolderAuthorization]
    [IEMQSImplementation.clsBase.SessionExpireFilter]
    [Models.EncryptedActionParameter]
    public class PartTypeController : Controller
    {
        IPartTypeService _PartTypeService;
        ICommonService _CommonService;
        public PartTypeController(IPartTypeService PartTypeService, ICommonService CommonService)
        {
            _PartTypeService = PartTypeService;
            _CommonService = CommonService;
        }

        // GET: DES/PartType
        public ActionResult Index()
        {
            ViewBag.ProdList = JsonConvert.SerializeObject(_PartTypeService.GetProductFormList());
            ViewBag.ItemTypeList = JsonConvert.SerializeObject(_CommonService.GetDefaultItemType());
            return View();
        }

        #region Part Type

        [HttpPost]
        public ActionResult GetPartTypeList(DataTableParamModel parm)
        {
            int recordsTotal = 0;
            var list = _PartTypeService.GetPartTypeList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpPost]
        public ActionResult AddPartType(PartTypeModel model)
        {
            string errorMsg = "";
            Int64 status = _PartTypeService.AddEdit(model, out errorMsg);
            if (status > 0)
            {
                if (model.ItemTypeId > 0)
                    return Json(new { Status = true, Msg = "Part type updated successfully" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = true, Msg = "Part type added successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult DeletePartType(Int64 Id)
        {
            string errorMsg = "";
            Int64 status = _PartTypeService.DeletePartType(Id, out errorMsg);
            if (status > 0)
            {
                return Json(new { Status = true, Msg = "Part type deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }
        
        #endregion

    }
}