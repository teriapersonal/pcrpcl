﻿using IEMQS.DESCore;
using IEMQS.DESServices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQSImplementation.Models;

namespace IEMQS.Areas.DES.Controllers
{
    [IEMQSImplementation.clsBase.SessionExpireFilter]
    public class DashboardController : Controller
    {
        IProjectService _ProjectService;
        ICommonService _CommonService;

        public DashboardController(IProjectService ProjectService, ICommonService CommonService)
        {
            _ProjectService = ProjectService;
            _CommonService = CommonService;
        }

        // GET: DES/Dashboard
        public ActionResult Index()
        {
            //_CommonService.DeleteFileFromFCS();
            var psno = CommonService.objClsLoginInfo.UserName;
            //var type = _CommonService.GetLookupData(types);
            Notifications.NotificationComponent NC = new Notifications.NotificationComponent();
            var currentTime = DateTime.Now;
            //HttpContext.CurrentHandler.Session["LastUpdated"] = currentTime;
            NC.RegisterNotification(currentTime);
            ViewBag.FavList = _ProjectService.GetAllFavoriteProject(psno);
            ViewBag.RecentList = _ProjectService.GetAllRecentProject(psno);
            //ViewBag.ReportList = _CommonService.GetLookupData("type").Where(Type=Report);
            ViewBag.ReportList = _CommonService.GetLookupData(LookupType.Report.ToString());
            //CommonModel(LookupModel);
            Models.SessionMgtModel.ProjectFolderList = null;
            //ViewBag.IsDESLive = "false";
            //using (var db = new IEMQSEntitiesContext())
            //{
            //    bool IsDESLive = Convert.ToBoolean(db.Database.SqlQuery<string>("SELECT dbo.FN_COMMON_GET_CONFIG_VALUE('IsDESLive')").FirstOrDefault());
            //    ViewBag.IsDESLive = IsDESLive;
            //}
            return View();
        }

        #region My Profile
        public ActionResult MyProfile()
        {
            MyProfileModel model = new MyProfileModel();
            var v = CommonService.objClsLoginInfo;

            model.Emailid = v.Emailid;
            model.FullName = v.UserName;
            model.Designation = v.Designation;
            //v.ListRoles
            //model.CompanyName = v.CompanyName;
            //model.PhoneNo = v.PhoneNo;
            //model.WorkNo = v.WorkNo;
            //model.Address = v.Address;
            //model.City = v.City;
            //model.State = v.State;
            //model.PostCode = v.PostCode;

            return View(model);
        }

        #endregion

        #region Favorite Projects

        [HttpPost]
        public ActionResult AddFavoriteProject(FavoriteProjectModel model)
        {
            string errorMsg = "";
            Int64 status = _ProjectService.AddEdit(model, out errorMsg);
            if (status > 0)
            {
                if (!model.IsFavorite)
                    return Json(new { Status = true, Msg = "Favourite project remove successfully" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = true, Msg = "Favourite project added successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetFavoriteList(DataTableParamModel parm)
        {
            int recordsTotal = 0;
            var list = _ProjectService.GetFavoriteList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpGet]
        public ActionResult FindObject(string Id, string Type)
        {
            FindObjectModel objFindObjectModel = new FindObjectModel();
            objFindObjectModel = _ProjectService.FindObject(Id, Type);

            if (objFindObjectModel != null && objFindObjectModel.PKId > 0)
                if (Type.ToLower() == ObjectName.DIN.ToString().ToLower())
                {
                    TempData["DINId"] = objFindObjectModel.PKId;

                }
                else if (Type.ToLower() == ObjectName.DOC.ToString().ToLower())
                {
                    TempData["DOCId"] = objFindObjectModel.PKId;
                }
                else if (Type.ToLower() == ObjectName.Part.ToString().ToLower())
                {
                    TempData["PartId"] = objFindObjectModel.PKId;
                }
                else if (Type.ToLower() == ObjectName.JEP.ToString().ToLower())
                {
                    TempData["JEPId"] = objFindObjectModel.PKId;
                }
                else if (Type.ToLower() == ObjectName.DCR.ToString().ToLower())
                {
                    TempData["DCRId"] = objFindObjectModel.PKId;
                }
                else if (Type.ToLower() == "jep-customerfeedback")
                {
                    TempData["jep-customerfeedbackId"] = objFindObjectModel.PKId;
                    TempData["jepidCF"] = objFindObjectModel.JEPId;
                }
                else if (Type.ToLower() == "roc")
                {
                    TempData["ROCId"] = objFindObjectModel.PKId;
                }
                else if (Type.ToLower() == "roc comment")
                {
                    TempData["ROCId"] = objFindObjectModel.PKId;
                }
                else if (Type.ToLower() == "roc-comment-attached")
                {
                    TempData["ROC_Attached_Id"] = objFindObjectModel.PKId;
                }

            if (!objFindObjectModel.IsExist)
            {
                TempData["Info"] = Type + " is not exist";
            }
            return RedirectToAction("Details", "Project", new { ProjectNo = objFindObjectModel.ProjectNo });
        }
        #endregion
    }
}