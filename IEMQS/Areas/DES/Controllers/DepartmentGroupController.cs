﻿using IEMQS.DESCore;
using IEMQS.DESServices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;

namespace IEMQS.Areas.DES.Controllers
{
    [clsBase.SessionExpireFilter]
    public class DepartmentGroupController : Controller
    {

        IDepartmentGroupService _DepartmentGroupService;
        IProjectService _ProjectService;

        public DepartmentGroupController(IDepartmentGroupService DepartmentGroupService, IProjectService ProjectService)
        {
            _DepartmentGroupService = DepartmentGroupService;
            _ProjectService = ProjectService;
        }

        // GET: DES/DepartmentGroup
        public ActionResult Index()
        {
            return View();
        }


        #region 

        [HttpPost]
        public ActionResult GetDINGroupList(DataTableParamModel parm)
        {
            int recordsTotal = 0;
            var list = _DepartmentGroupService.GetDepartmentGroupList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpPost]
        public ActionResult GetTypeList()
        {
            var list = _DepartmentGroupService.GetTypeList();

            return Json(new
            {
                data = list
            });
        }

        //[HttpPost]
        //public ActionResult GetPSNumberList(string dept)
        //{
        //    var list = _DepartmentGroupService.GetPSNumberList(dept);

        //    return Json(new
        //    {
        //        data = list
        //    });
        //}

        [HttpGet]
        public ActionResult AddGroup(Int64? Id, string dept, string PsNo)
        {
            DINGroupModel model = new DINGroupModel();

            ViewBag.GroupList = new SelectList(_DepartmentGroupService.GetAllDepartmentGroup(), "DINGroupId", "GroupName");
            //ViewBag.ProjectList = new SelectList(_DepartmentGroupService.GetAllProject(), "Project", "Project");

            if (Id > 0)
            {
                model = _DepartmentGroupService.GetDepartmentGroupbyId(Id.Value);
                if (model == null)
                    model = new DINGroupModel();

            }
            if (model != null && (model.DepartmentGroupList == null || model.DepartmentGroupList.Count() == 0))
            {
                model.DepartmentGroupList = new List<DINGroupDepartmentModel> {
                    new DINGroupDepartmentModel
                    {
                         FunctionList = _DepartmentGroupService.GetTypeList().Select(x => new SelectListItem
                            {
                                Text = x.t_desc,
                                Value = x.t_dimx,
                            }).ToList(),
                        PSNumberList = new List<SelectListItem>(),
                //_DepartmentGroupService.GetPSNumberList(dept).Select(x => new SelectListItem
                // {
                //     Text = x.t_name,
                //    Value = x.t_psno,
                //   }).ToList(),
                     },
                };
            }

            return PartialView("_partialAdd_CopyGroupView", model);
        }

        public ActionResult AddDeptPerson(int TNo, string dept) //, string dept
        {
            DINGroupModel model = new DINGroupModel();
            ViewBag.FunctionList = new SelectList(_DepartmentGroupService.GetTypeList(), "t_dimx", "t_desc");
            //ViewBag.PSNumberList = new SelectList(_DepartmentGroupService.GetPSNumberList(dept), "t_psno", "t_name");

            TempData["NoTask"] = TNo;
            return PartialView("_partialDept_PersonView", model);
        }

        [HttpPost]
        public ActionResult AddGroup(DINGroupModel model)
        {
            string errorMsg = "";
            string infoMsg = "";
            Int64 status = _DepartmentGroupService.AddEdit(model, out errorMsg, out infoMsg);
            if (status > 0)
            {
                if (model.DINGroupId > 0)
                    return Json(new { Status = true, Msg = "Department group updated successfully" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = true, Msg = "Department group added successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(infoMsg))
                {
                    var infoMsgs = "true";
                    return Json(new { Info = infoMsgs, Status = false, Msg = infoMsg }, JsonRequestBehavior.AllowGet);
                }
                else if (!string.IsNullOrEmpty(errorMsg))
                {
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpGet]
        public ActionResult DeleteDINGroup(Int64 Id)
        {
            string errorMsg = "";
            Int64 status = _DepartmentGroupService.DeleteDepartmentGroup(Id, out errorMsg);
            if (status > 0)
            {
                return Json(new { Status = true, Msg = "Department group deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult CheckGroupNameAlreadyExist(string GroupName, Int64? DINGroupId)
        {
            return Json(!_DepartmentGroupService.CheckGroupNameAlreadyExist(GroupName, DINGroupId), JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //public JsonResult ProjectByGroupList(string Project)
        //{
        //    var list = _DepartmentGroupService.GetAllProjectByGroup(Project);
        //    return Json(new { list }, JsonRequestBehavior.AllowGet);
        //    //return Json(_DepartmentGroupService.GetAllProjectByGroup(Project), JsonRequestBehavior.AllowGet);
        //}

        public ActionResult CopyProjectGroupList(Int64 Id)
        {
            DINGroupModel model = new DINGroupModel();
            model = _DepartmentGroupService.GetDepartmentGroupbyId(Id);
            return PartialView("_partialDept_PersonView", model);
            //return View(); _partialDept_PersonView
        }

        [HttpGet]
        public JsonResult FunctionByPSNumberList(string DeptId) //string DeptId
        {
            var list = _DepartmentGroupService.GetAllFunctionByPSNo(DeptId);
            return Json(new { list }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetPsnoNameByNameOrDept(string q, string DeptId) //string DeptId
        {
            var list = _DepartmentGroupService.GetALLPSNOByNameandDepartment(q, DeptId).Select(x => new { id = x.t_psno, text = x.t_namb }).ToList();
            return Json(new { list }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetDeptIdByPsNo(string psno) //string DeptId
        {
            if (!string.IsNullOrEmpty(psno))
            {
                string[] number = psno.Split(',');
                string deptid = CommonService.GetDeptIdByPSNo(number[0]);
                return Content(deptid);
            }
            return Content("0");
        }

        #endregion
    }
}