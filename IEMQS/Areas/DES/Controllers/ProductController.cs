﻿using IEMQS.DESCore;
using IEMQS.DESServices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;

namespace IEMQS.Areas.DES.Controllers
{
    [IEMQSImplementation.clsBase.SessionExpireFilter]
    public class ProductController : Controller
    {
        IProductService _ProductService;
        IBUService _BUService;

        public ProductController(IProductService ProductService, IBUService BUService)
        {
            _ProductService = ProductService;
            _BUService = BUService;            
        }

        // GET: DES/Product
        public ActionResult Index()
        {
            ViewBag.BuList = JsonConvert.SerializeObject(_BUService.GetAllBU());
            return View();
        }

        #region Maintain Product

        [HttpPost]
        public ActionResult GetProductList(DataTableParamModel parm)
        {
            int recordsTotal = 0;
            var list = _ProductService.GetProductList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpPost]
        public ActionResult AddProduct(ProductModel model)
        {
            string errorMsg = "";
            Int64 status = _ProductService.AddEdit(model, out errorMsg);
            if (status > 0)
            {
                if (model.ProductId > 0)
                    return Json(new { Status = true, Msg = "Product updated successfully" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = true, Msg = "Product added successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult DeleteProduct(Int64 Id)
        {
            string errorMsg = "";
            Int64 status = _ProductService.DeleteProduct(Id, out errorMsg);
            if (status > 0)
            {
                return Json(new { Status = true, Msg = "Product deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

    }
}