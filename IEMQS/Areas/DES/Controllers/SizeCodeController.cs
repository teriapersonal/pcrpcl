﻿using IEMQS.DESCore;
using IEMQS.DESServices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;

namespace IEMQS.Areas.DES.Controllers
{
    [IEMQSImplementation.clsBase.SessionExpireFilter]
    public class SizeCodeController : Controller
    {
        ISizeCodeService _SizeCodeService;

        public SizeCodeController(ISizeCodeService SizeCodeService)
        {
            _SizeCodeService = SizeCodeService;
        }

        // GET: DES/SizeCode
        public ActionResult Index()
        {
            return View();
        }

        #region Product Form Code

        [HttpPost]
        public ActionResult GetSizeCodeList(DataTableParamModel parm)
        {
            int recordsTotal = 0;
            var list = _SizeCodeService.GetSizeCodeList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpPost]
        public ActionResult AddSizeCode(SizeCodeModel model)
        {
            string errorMsg = "";
            string status = _SizeCodeService.AddEdit(model, out errorMsg);
            if (status != null)
            {
                if (!string.IsNullOrEmpty(model.SizeCode))               
                    return Json(new { Status = true, Msg = errorMsg }, JsonRequestBehavior.AllowGet);                                              
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);               
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult DeleteSizeCode(string Id)
        {
            string errorMsg = "";
            Int64 status = _SizeCodeService.DeleteSizeCode(Id, out errorMsg);
            if (status > 0)
            {
                return Json(new { Status = true, Msg = "Size code deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

    }
}