﻿using IEMQS.DESCore;
using IEMQS.DESServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.DES.Controllers
{
    //[Models.ProjectFolderAuthorization]
    [IEMQSImplementation.clsBase.SessionExpireFilter]
    [Models.EncryptedActionParameter]

    public class DESHBOMController : Controller
    {
        IHBOMService _HBOMService;
        List<PowerViewModel> listPowerviewtempList = null;

        public DESHBOMController(IHBOMService HBOMService)
        {
            _HBOMService = HBOMService;
        }

        // GET: DES/DESHBOM
        public ActionResult Index(string ProjectNo)
        {           
            PartPowerViewModel obj = new PartPowerViewModel();
            if (ProjectNo != null)
            {
                obj.ProjectNo = ProjectNo;
                Int64? ItemId = _HBOMService.GetItemIdByProject(ProjectNo);
                if (ItemId > 0)
                {
                    obj.ItemId = ItemId;
                    obj.QString = CommonService.Encrypt("ItemId=" + ItemId);
                    List<PowerViewModel> objPowerviewList = new List<PowerViewModel>();
                    objPowerviewList = _HBOMService.GetHBOMPowerViewList(ProjectNo, ItemId);

                    var MainProjectDtl = objPowerviewList.Where(i => !(i.ParentItemId > 0)).First();
                    if (MainProjectDtl != null)
                    {
                        listPowerviewtempList = new List<PowerViewModel>();
                        listPowerviewtempList.Add(new PowerViewModel
                        {
                            BOMId = MainProjectDtl.BOMId,
                            ItemId = MainProjectDtl.ItemId,
                            ParentItemId = MainProjectDtl.ParentItemId,
                            ItemKey = MainProjectDtl.ItemKey,
                            Type = MainProjectDtl.Type,
                            Rev = MainProjectDtl.Rev,
                            policy = MainProjectDtl.policy,
                            FindNumber = MainProjectDtl.FindNumber,
                            ExtFindNumber = MainProjectDtl.ExtFindNumber,
                            ItemName = MainProjectDtl.ItemName,
                            Quantity = MainProjectDtl.Quantity,
                            Length = MainProjectDtl.Length,
                            Width = MainProjectDtl.Width,
                            NumberOfPieces = MainProjectDtl.NumberOfPieces,
                            UOM = MainProjectDtl.UOM,
                            Material = MainProjectDtl.Material,
                            State = MainProjectDtl.State,
                            ItemGroup = MainProjectDtl.ItemGroup,
                        });

                        GetHBOMChildItems(objPowerviewList, MainProjectDtl.ItemId);
                        obj.PowerViewModelList = listPowerviewtempList;
                    }
                }
            }
            return View(obj);
        }

        public void GetHBOMChildItems(List<PowerViewModel> objPowerviewList, Int64? ParentId)
        {
            var listChilds = objPowerviewList.Where(i => i.ParentItemId == ParentId).ToList();
            foreach (var item in listChilds)
            {
                listPowerviewtempList.Add(new PowerViewModel
                {
                    BOMId = item.BOMId,
                    ItemId = item.ItemId,
                    ParentItemId = item.ParentItemId,
                    ItemKey = item.ItemKey,
                    Type = item.Type,
                    Rev = item.Rev,
                    policy = item.policy,
                    FindNumber = item.FindNumber,
                    ExtFindNumber = item.ExtFindNumber,
                    ItemName = item.ItemName,
                    Quantity = item.Quantity,
                    Length = item.Length,
                    Width = item.Width,
                    NumberOfPieces = item.NumberOfPieces,
                    UOM = item.UOM,
                    Material = item.Material,
                    State = item.State,
                    ItemGroup = item.ItemGroup,
                });
                GetHBOMChildItems(objPowerviewList, item.ItemId);
            }
        }

        public ActionResult HBOMTreeView(string ProjectNo)
        {
            PartTreeViewModel obj = new PartTreeViewModel();
            Int64? ItemId = _HBOMService.GetItemIdByProject(ProjectNo);
            //obj.ItemId = ItemId;
            obj.ProjectNo = ProjectNo;
            //obj.QString = CommonService.Encrypt("ItemId=" + ItemId);
            obj.TreeViewModelList = _HBOMService.GetHBOMTreeViewList(ProjectNo, ItemId);
            return View(obj);
        }
    }
}