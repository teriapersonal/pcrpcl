﻿using IEMQS.DESCore;
using IEMQS.DESServices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.DES.Controllers
{
    public class ARMSetController : Controller
    {
        IARMService _ARMService;

        // GET: DES/ARM
        public ARMSetController(IARMService ARMService)
        {
            _ARMService = ARMService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            ARMModel model = new ARMModel();
            ViewBag.ARMCode = JsonConvert.SerializeObject(_ARMService.GetAllArmCode());
            var RoleGroup = _ARMService.GetRoleGroup(CommonService.objClsLoginInfo.UserName);
            model.IsITRoleGroup = RoleGroup;
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(DataTableParamModel parm)
        {
            int recordsTotal = 0;
            var list = _ARMService.GetARMSet(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpPost]
        public ActionResult AddEdit(ARMModel model)
        {
            string errorMsg = "";
            string status = _ARMService.AddEdit(model, out errorMsg);
            if (status != null)
            {
                if (!string.IsNullOrEmpty(model.Arms))
                    return Json(new { Status = true, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult DeleteArmSet(string Arms)
        {
            string errorMsg = "";
            Int64 status = _ARMService.DeleteArmSet(Arms, out errorMsg);
            if (status > 0)
            {
                return Json(new { Status = true, Msg = "ARMSet deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult ARMCode()
        {
            ARMModel model = new ARMModel();
            IpLocation ipLocation = new IpLocation();
            model.CurrentLocationIp = ipLocation.Ip;
            model.CurrentLocationSavedPath = ipLocation.File_Path;
            return View(model);
        }

        [HttpPost]
        public ActionResult ARMCode(DataTableParamModel parm)
        {
            int recordsTotal = 0;
            var list = _ARMService.GetARMCode(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpPost]
        public ActionResult AddEditARMCode(ARMCodeModal model)
        {
            string errorMsg = "";
            Int64 status = _ARMService.AddEditARMCode(model, out errorMsg);
            if (status > 0)
            {
                if (model.ARMCodeId > 0)
                    return Json(new { Status = true, Msg = "Armcode updated successfully", Objectlist = _ARMService.GetAllArmCode() }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = true, Msg = "Armcode added successfully", Objectlist = _ARMService.GetAllArmCode() }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult DeleteArmCode(Int64 ARMCodeId)
        {
            string errorMsg = "";
            Int64 status = _ARMService.DeleteArmCode(ARMCodeId, out errorMsg);
            if (status > 0)
            {
                return Json(new { Status = true, Msg = "ARMcode deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult UpdateArmSetStatus(string Arms, bool Status)
        {
            bool status = _ARMService.UpdateArmSetStatus(Arms, Status);
            if (status)
            {
                if (Status)
                    return Json(new { Status = true, Msg = "Status activated successfully" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = true, Msg = "Status deactivated successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]//Document File Mapping Add/Edit
        public JsonResult CreateARMDocMapping(ARMCodeModal model)
        {
            string errorMsg = "";
            IpLocation ipLocation = CommonService.GetUseIPConfig;
            Int64? status = _ARMService.AddARMCodeFile(model, ipLocation.Location);
            if (status > 0)
            {
                return Json(new { Status = true, Msg = "Document uploaded successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult GetARMCodeAttacheFile(DataTableParamModel parm, Int64? ARMCodeId)
        {
            int recordsTotal = 0;
            var list = _ARMService.GetARMFileByArmCodeId(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, ARMCodeId, out recordsTotal);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpGet]
        public JsonResult DeleteARMAttachFile(Int64 Id)
        {
            string errorMsg = "";
            Int64 status = _ARMService.DeleteARMAttach(Id, out errorMsg);
            if (status > 0)
            {
                return Json(new { Status = true, Msg = "File deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}