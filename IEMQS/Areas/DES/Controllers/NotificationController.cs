﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQS.DESCore;
using IEMQS.DESServices;

namespace IEMQS.Areas.DES.Controllers
{
    public class NotificationController : Controller
    {
        INotificationService _notificationService;
        public NotificationController(INotificationService notificationService)
        {
            _notificationService = notificationService;
        }

        // GET: DES/Notification
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetNotifications()
        {
            var notificationRegisterTime = Session["LastUpdated"] != null ? Convert.ToDateTime(Session["LastUpdated"]) : DateTime.Now;
            var list = _notificationService.GetNotificationContent(notificationRegisterTime);
            //update session here for get only new added contacts (notification)
            Session["LastUpdate"] = DateTime.Now;
            return new JsonResult { Data = list, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult GetCountofNotifications()
        {
            string totCount = _notificationService.GettotalUnreadNotificationasperPSno(CommonService.objClsLoginInfo.UserName);
            //update session here for get only new added contacts (notification)
            Session["LastUpdate"] = DateTime.Now;
            return new JsonResult { Data = totCount, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult MarkNotificationasReadasperPSNo()
        {
            string Message = _notificationService.MarkNotificationasRead(CommonService.objClsLoginInfo.UserName);
            return new JsonResult { Data = Message, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        public ActionResult GetAllNotificationsByPsNo(DataTableParamModel parm)
        {
            int recordsTotal = 0;
            var list = _notificationService.GetAllNotificationByPSNo(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, CommonService.objClsLoginInfo.UserName, out recordsTotal);

            
            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        public ActionResult NotificationList()
        {
            NotificationModel notificationModel = new NotificationModel();
            return View(notificationModel);
        }
    }
}