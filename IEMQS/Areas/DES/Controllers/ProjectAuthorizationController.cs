﻿using IEMQS.DESCore;
using IEMQS.DESServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.DES.Controllers
{
    [IEMQSImplementation.clsBase.SessionExpireFilter]
    [Models.EncryptedActionParameter]
    public class ProjectAuthorizationController : Controller
    {

        IProjectAuthorizationService _IProjectAuthorizationService;
        ICommonService _ICommonService;
        public ProjectAuthorizationController(IProjectAuthorizationService ProjectAuthorizationService, ICommonService CommonService)
        {
            _IProjectAuthorizationService = ProjectAuthorizationService;
            _ICommonService = CommonService;
        }
        // GET: DES/ProjectAuthorization
        public ActionResult Index()
        {
            //var logininfo = CommonService.objClsLoginInfo;
            //var userLevel = _ICommonService.GetUserLevel(logininfo.Department, logininfo.GetUserRole());
            //if (userLevel <= 2)
            //{
            //    return View();
            //}
            //else
            //{
            //    TempData["Error"] = "You are not authorize person.";
            //    return Redirect(Request.UrlReferrer.ToString());
            //}
            return View();
        }

        [HttpPost]
        public ActionResult GetProjectAuthorizationList(DataTableParamModel parm)
        {
            int recordsTotal = 0;
            var list = _IProjectAuthorizationService.GetProjectAuthorizationList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal, Models.SessionMgtModel.ProjectFolderList.ProjectNo, CommonService.objClsLoginInfo.Department, CommonService.objClsLoginInfo.UserRoles);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }



        [HttpPost]
        public ActionResult Approve(Int64 Id)
        {

            Int64 status = _IProjectAuthorizationService.Approve(Id, CommonService.objClsLoginInfo.UserName);
            if (status > 0)
            {
                return Json(new { Status = true, Msg = "Access request accepted successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult Reject(Int64 Id)
        {

            Int64 status = _IProjectAuthorizationService.Reject(Id);
            if (status > 0)
            {
                return Json(new { Status = true, Msg = "Access request rejected successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }


    }
}