﻿using IEMQS.Areas.DES.Models;
using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using IEMQS.DESServices;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.DES.Controllers
{
    [Models.ProjectFolderAuthorization]
    [IEMQSImplementation.clsBase.SessionExpireFilter]
    [Models.EncryptedActionParameter]
    public class DCRController : Controller
    {
        IDCRService _DCRService;
        ICommonService _commonService;
        IDepartmentGroupService _DepartmentGroupService;
        public DCRController(IDCRService DCRService, ICommonService commonService, IDepartmentGroupService DepartmentGroupService)
        {
            _DCRService = DCRService;
            _commonService = commonService;
            _DepartmentGroupService = DepartmentGroupService;
        }

        #region DCR - Yash Shah

        [HttpGet]
        public ActionResult Index()
        {
            DCRModel model = new DCRModel();
            model.Project = Models.SessionMgtModel.ProjectFolderList.ProjectNo;
            model.Identical = _DCRService.Identical(model.Project);      
            model.CurrentUser = CommonService.objClsLoginInfo.UserName;
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(DataTableParamModel parm)
        {
            int recordsTotal = 0;
            var Roles = CommonService.objClsLoginInfo.UserRoles;

            var list = _DCRService.GetDCRList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal, SessionMgtModel.ProjectFolderList.ProjectNo, Roles);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpGet]
        public ActionResult Add()
        {
            if (Models.SessionMgtModel.ProjectFolderList.FolderId > 0)
            {
                var model = new DCRModel();
                IpLocation ipLocation = CommonService.GetUseIPConfig;
                model.Project = SessionMgtModel.ProjectFolderList.ProjectNo;
                model.Department = CommonService.objClsLoginInfo.Department;
                model.RoleGroup = Models.SessionMgtModel.ProjectFolderList.RoleGroup;
                model.FolderId = Models.SessionMgtModel.ProjectFolderList.FolderId;
                model.CurrentLocationIp = ipLocation.Ip;
                model.FilePath = ipLocation.File_Path;
                model.OriginatedDate = DateTime.Now;
                model.Originator = model.Owner = CommonService.objClsLoginInfo.Name;
                model.NatureOfChange = 106;
                model.DCRNo = _DCRService.GenerateDCRNumber(model.Project);
                ViewBag.ResponsibleDesignEnggList = new SelectList(_commonService.GetUserByRole("ENGG3"), "t_psno", "t_name");
                ViewBag.SeniorDesignEnggList = new SelectList(_commonService.GetUserByRole("ENGG1,ENGG2"), "t_psno", "t_name");
                ViewBag.CategoryOfChangeList = new SelectList(_commonService.GetLookupData(LookupType.CategoryOfChanges.ToString()), "Id", "Lookup");
                ViewBag.NatureOfChangeList = new SelectList(_commonService.GetLookupData(LookupType.NatureOfChanges.ToString()), "Id", "Lookup");
                ViewBag.GetContractProjectList = new SelectList(_DCRService.GetContractProjectList(model.Project), "Value", "Text");
                ViewBag.GetContractProjectListWiseDOC = new SelectList(_DCRService.GetAllDOCByProject(model.Project), "Value", "Text");
                ViewBag.DistributionGroup = new SelectList(_DCRService.GetDistributionGroup(), "DINGroupId", "GroupName");
                return View(model);
            }
            else
            {
                TempData["Error"] = "Select folder";
                return RedirectToAction("Details", "Project", new { ProjectNo = Models.SessionMgtModel.ProjectFolderList.ProjectNo });
            }
        }

        [HttpPost]
        public JsonResult GetDOCByProject(DCRModel model)
        {
            return Json(_DCRService.GetAllDOCByProject(model.Project), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Add(DCRModel model)
        {
            model.BUId = Models.SessionMgtModel.ProjectFolderList.BUId;
            string status = _DCRService.AddDCR(model);
            if (status != null)
            {
                var data = status.Split('|').ToArray();
                var DCRId = Convert.ToInt64(data[0]);
                var DCRNo = data[1];

                if (model.DCRId > 0)
                {
                    TempData["Success"] = "DCR Updated successfully";
                    return Json(new { model, Status = true, Msg = "DCR Updated successfully" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    model.DCRId = DCRId;
                    model.DCRNo = DCRNo;
                    TempData["Success"] = "DCR added successfully";
                    return Json(new { model, Status = true, Msg = "DCR added successfully" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
                return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddDetailDCR(DCRModel model)
        {
            model.BUId = Models.SessionMgtModel.ProjectFolderList.BUId;
            string status = _DCRService.AddDetailDCR(model);
            //if (status != null)s
            //{
            //    var data = status.Split('|').ToArray();
            //    var DCRId = Convert.ToInt64(data[0]);
            //    var DCRNo = data[1];

            if (model.DCRId > 0)
            {
                TempData["Success"] = "DCR Updated successfully";
                return Json(new { model, Status = true, Msg = "DCR Updated successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }





        [HttpPost]
        public ActionResult GetJEPDocList(DataTableParamModel parm, Int64? DCRId)
        {
            int recordsTotal = 0;
            var list = _DCRService.GetJEPDocList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, SessionMgtModel.ProjectFolderList.ProjectNo, out recordsTotal, DCRId);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpPost]
        public ActionResult AddDCRdoc(DCRDocumentModel model)
        {
            Int64 status = _DCRService.AddEdit(model);
            if (status > 0)
            {
                if (model.DCRAffectedDocumentId > 0)
                {
                    return Json(new { model, Status = true, Msg = "JEP Document updated successfully" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    model.DCRAffectedDocumentId = status;
                    return Json(new { model, Status = true, Msg = "JEP Document added successfully" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
                return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetJEPGlobalDocList(DataTableParamModel parm, string DocumentNo, Int64? DCRId, string Project)
        {
            int recordsTotal = 0;
            var list = _DCRService.GetJEPGlobalDocList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal, Project, DocumentNo, DCRId, SessionMgtModel.ProjectFolderList.ProjectNo);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpPost]
        public ActionResult GetAffectedDCRDoc(DataTableParamModel parm, Int64? DCRId, bool IsInCorporate)
        {
            int recordsTotal = 0;
            var list = _DCRService.GetAffectedDCRDoc(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal, DCRId);

            if (IsInCorporate)
            {
                list = list.Where(x => x.IsPartialSelect == null).ToList();
            }

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpGet]
        public ActionResult DeleteAffectedDocument(Int64 Id)
        {
            string errorMsg = "";
            bool Status = _DCRService.DeleteAffectedDocument(Id, out errorMsg);
            if (Status)
                return Json(new { Status, Msg = "Document deleted successfully" }, JsonRequestBehavior.AllowGet);
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UploadDCRSupportDocs(DCRDocumentMappingModel model)
        {
            string errorMsg = "";

            Int64 status = _DCRService.UploadDCRSupportDocs(model, out errorMsg);
            if (status > 0)
            {
                model.DCRSupportedDocId = status.ToString();
                return Json(new { model, Status = true, Msg = "File uploaded successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDCRSupportedDocumentList(DataTableParamModel parm, Int64? DCRId)
        {
            int recordsTotal = 0;
            var list = _DCRService.GetDCRSupportedDocumentList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal, DCRId);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpPost]
        public ActionResult GetPartList(DataTableParamModel parm, Int64? DCRId)
        {
            int recordsTotal = 0;
            var list = _DCRService.GetPartList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal, DCRId, SessionMgtModel.ProjectFolderList.ProjectNo);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpPost]
        public ActionResult AddPart(DCRPartModel model)
        {
            Int64 status = _DCRService.AddPart(model);
            if (status > 0)
            {
                model.DCRAffectedPartsId = status;
                return Json(new { model, Status = true, Msg = "Part added successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDCRAffectedPart(DataTableParamModel parm, Int64? DCRId, bool IsInCorporate)
        {
            int recordsTotal = 0;
            var list = _DCRService.GetDCRAffectedPart(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal, DCRId);

            if (IsInCorporate)
            {
                list = list.Where(x => x.IsPartialSelect == null).ToList();
            }

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpGet]
        public ActionResult Edit(Int64? Id)
        {
            var Project = SessionMgtModel.ProjectFolderList.ProjectNo;
            //ViewBag.AddDCRName = new SelectList(_DCRService.GetDCRName(), "Value", "Text");
            ViewBag.ResponsibleDesignEnggList = new SelectList(_commonService.GetUserByRole("ENGG3"), "t_psno", "t_name");
            ViewBag.SeniorDesignEnggList = new SelectList(_commonService.GetUserByRole("ENGG1,ENGG2"), "t_psno", "t_name");
            ViewBag.CategoryOfChangeList = new SelectList(_commonService.GetLookupData(LookupType.CategoryOfChanges.ToString()), "Id", "Lookup");
            ViewBag.NatureOfChangeList = new SelectList(_commonService.GetLookupData(LookupType.NatureOfChanges.ToString()), "Id", "Lookup");
            ViewBag.GetLifeCycle = _DCRService.GetPolicySteps(ObjectName.DCR.ToString(), SessionMgtModel.ProjectFolderList.BUId, Id);
            ViewBag.GetContractProjectList = new SelectList(_DCRService.GetContractProjectList(Project), "Value", "Text");
            ViewBag.GetContractProjectListWiseDOC = new SelectList(_DCRService.GetAllDOCByProject(Project), "Value", "Text");
            ViewBag.DistributionGroup = new SelectList(_DCRService.GetDistributionGroup(), "DINGroupId", "GroupName");
            DCRModel model = new DCRModel();
            IpLocation ipLocation = CommonService.GetUseIPConfig;
            model.DCRId = Id;
            if (Id > 0)
            {
                model = _DCRService.GetDCRById(model);
                if (model == null)
                    model = new DCRModel();

                model.ModifiedDate = DateTime.Now;
                model.Project = Project;
                model.Department = CommonService.objClsLoginInfo.Department;
                model.RoleGroup = Models.SessionMgtModel.ProjectFolderList.RoleGroup;
                model.FolderId = Models.SessionMgtModel.ProjectFolderList.FolderId;
                model.CurrentLocationIp = ipLocation.Ip;
                model.FilePath = ipLocation.File_Path;
                model.OriginatedDate = DateTime.Now;
                model.Originator = model.Owner = CommonService.objClsLoginInfo.Name;
            }
            return View("Add", model);
        }

        [HttpPost]
        public ActionResult UpdateDCRStatus(Int64? DCRId, string status)
        {
            string strStatus = status;
            var Status = _DCRService.UpdateDCRStatus(DCRId, status);
            if (Status)
            {
                if (strStatus == ObjectStatus.Cancelled.ToString())
                {
                    return Json(new { Status, MSG = "DCR cancelled Successfully" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status, MSG = "DCR submitted Successfully" }, JsonRequestBehavior.AllowGet);
                }
            }

            else
                return Json(new { Status, MSG = "Something went wrong Try again later" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Delete(Int64? Id)
        {
            string errorMsg = "";
            bool Status = _DCRService.Delete(Id);
            if (Status)
                return Json(new { Status, Msg = "DCR deleted successfully" }, JsonRequestBehavior.AllowGet);
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult DeleteAffectedPart(Int64? Id)
        {
            bool Status = _DCRService.DeleteAffectedPart(Id);
            if (Status)
                return Json(new { Status, Msg = "Part deleted successfully" }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { Status, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Review(Int64? Id)
        {
            ViewBag.ResponsibleDesignEnggList = new SelectList(_commonService.GetUserByRole("ENGG3"), "t_psno", "t_name");
            ViewBag.SeniorDesignEnggList = new SelectList(_commonService.GetUserByRole("ENGG1,ENGG2"), "t_psno", "t_name");
            ViewBag.CategoryOfChangeList = new SelectList(_commonService.GetLookupData(LookupType.CategoryOfChanges.ToString()), "Id", "Lookup");
            ViewBag.NatureOfChangeList = new SelectList(_commonService.GetLookupData(LookupType.NatureOfChanges.ToString()), "Id", "Lookup");

            DCRModel model = new DCRModel();
            model.DCRId = Id;
            if (Id > 0)
            {
                model = _DCRService.GetDCRById(model);
                if (model == null)
                    model = new DCRModel();
            }
            model.Page = ReviewOrEvaluate.Review.ToString();
            return PartialView("Detail", model);
        }

        [HttpGet]
        public ActionResult Evaluate(Int64? Id)
        {
            ViewBag.ResponsibleDesignEnggList = new SelectList(_commonService.GetUserByRole("ENGG3"), "t_psno", "t_name");
            ViewBag.SeniorDesignEnggList = new SelectList(_commonService.GetUserByRole("ENGG1,ENGG2"), "t_psno", "t_name");
            ViewBag.CategoryOfChangeList = new SelectList(_commonService.GetLookupData(LookupType.CategoryOfChanges.ToString()), "Id", "Lookup");
            ViewBag.NatureOfChangeList = new SelectList(_commonService.GetLookupData(LookupType.NatureOfChanges.ToString()), "Id", "Lookup");

            DCRModel model = new DCRModel();
            model.DCRId = Id;
            if (Id > 0)
            {
                model = _DCRService.GetDCRById(model);
                if (model == null)
                    model = new DCRModel();
            }
            model.Page = ReviewOrEvaluate.Evaluate.ToString();
            return PartialView("Detail", model);
        }

        [HttpGet]
        public ActionResult Detail(Int64? Id, string dept)
        {
           // ViewBag.AddDCRName = new SelectList(_DCRService.GetDCRName(), "Value", "Text");
            ViewBag.ResponsibleDesignEnggList = new SelectList(_commonService.GetUserByRole("ENGG3"), "t_psno", "t_name");
            ViewBag.SeniorDesignEnggList = new SelectList(_commonService.GetUserByRole("ENGG1,ENGG2"), "t_psno", "t_name");
            ViewBag.CategoryOfChangeList = new SelectList(_commonService.GetLookupData(LookupType.CategoryOfChanges.ToString()), "Id", "Lookup");
            ViewBag.NatureOfChangeList = new SelectList(_commonService.GetLookupData(LookupType.NatureOfChanges.ToString()), "Id", "Lookup");
            ViewBag.GetLifeCycle = _DCRService.GetPolicySteps(ObjectName.DCR.ToString(), SessionMgtModel.ProjectFolderList.BUId, Id);
            IpLocation ipLocation = CommonService.GetUseIPConfig;
            DCRModel model = new DCRModel();
            model.DCRId = Id;
            model = _DCRService.GetDCRById(model);
            model.CurrentLocationIp = ipLocation.Ip;
            model.FilePath = ipLocation.File_Path;
            model.CurrentUser = CommonService.objClsLoginInfo.UserName;
            model.PolicyStepForView = _DCRService.PolicyStepStatus(Id);
            ViewBag.GetContractProjectList = new SelectList(_DCRService.GetContractProjectList(model.Project), "Value", "Text");
            ViewBag.GetContractProjectListWiseDOC = new SelectList(_DCRService.GetAllDOCByProject(model.Project), "Value", "Text");
            ViewBag.DistributionGroup = new SelectList(_DCRService.GetDistributionGroup(), "DINGroupId", "GroupName");
            if (model == null)
                model = new DCRModel();
            else
            {
                model.ResponsibleDesignEngineerText = model.ResponsibleDesignEngineer + " - " + new clsManager().GetUserNameFromPsNo(model.ResponsibleDesignEngineer);
                model.ResponsibleSeniorDesignEngineerText = model.ResponsibleSeniorDesignEngineer + " - " + new clsManager().GetUserNameFromPsNo(model.ResponsibleSeniorDesignEngineer);

                if (model != null && (model.DepartmentGroupList == null || model.DepartmentGroupList.Count() == 0))
                {
                    model.DepartmentGroupList = new List<DCREvaluateCommentModel> {
                    new DCREvaluateCommentModel
                    {
                         FunctionList = _DepartmentGroupService.GetTypeList().Select(x => new SelectListItem
                            {
                                Text = x.t_desc,
                                Value = x.t_dimx,
                            }).ToList(),
                         PSNumberList = new List<SelectListItem>(),
                        //PSNumberList = _DepartmentGroupService.GetPSNumberList(dept).Select(x => new SelectListItem
                        //    {
                        //        Text = x.t_psno+" - "+x.t_name,
                        //        Value = x.t_psno,
                        //    }).ToList(),
                     },
                };
                }

                if (model.ResponsibleDesignEngineer == CommonService.objClsLoginInfo.UserName)
                {
                    if ((!model.IsReviewed && !model.IsEvaluated) || model.IsReviewed)
                    {
                        model.Page = ReviewOrEvaluate.Review.ToString();
                    }
                }
                else if (model.ResponsibleSeniorDesignEngineer == CommonService.objClsLoginInfo.UserName)
                {
                    model.Page = ReviewOrEvaluate.Evaluate.ToString();
                }
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Detail(DCRModel model, string button)
        {
            if (model.DCRReviewModel != null)
            {
                if (model.DCRReviewModel.Remark != null)
                {
                    model.Department = CommonService.objClsLoginInfo.Department;
                    model.RoleGroup = Models.SessionMgtModel.ProjectFolderList.RoleGroup;

                    var getData = _DCRService.GetDCRById(model);
                    var flag = false;
                    if (model.Page == DCRLifeCyclePolicyStep.Review.ToString())
                    {
                        if (getData.ResponsibleDesignEngineer == CommonService.objClsLoginInfo.UserName)
                        {
                            flag = true;
                        }
                    }
                    else if (model.Page == DCRLifeCyclePolicyStep.Evaluate.ToString() || model.Page == DCRLifeCyclePolicyStep.Release.ToString())
                    {
                        if (getData.ResponsibleSeniorDesignEngineer == CommonService.objClsLoginInfo.UserName)
                        {
                            flag = true;
                        }
                    }

                    if (flag)
                    {
                        if (button == "Accept")
                            model.Status = ReviewOrEvaluate.Accepted.ToString();
                        else if (button == "Reject")
                            model.Status = ReviewOrEvaluate.Rejected.ToString();
                        else if (button == "Return")
                            model.Status = ReviewOrEvaluate.Return.ToString();

                        string result = _DCRService.AddReviewOrEvaluate(model);

                        if (result == ReviewOrEvaluate.Review.ToString())
                        {
                            TempData["Success"] = "DCR " + model.Status + " successfully";
                            return Json(new { Remark = true, Status = true, Msg = "DCR " + model.Status + " successfully" }, JsonRequestBehavior.AllowGet);
                        }
                        else if (result == ReviewOrEvaluate.Evaluate.ToString())
                        {
                            TempData["Success"] = "DCR " + model.Status + " successfully";
                            return Json(new { Remark = true, Status = true, Msg = "DCR " + model.Status + " successfully" }, JsonRequestBehavior.AllowGet);
                        }
                        else if (result == ObjectStatus.Release.ToString())
                        {
                            TempData["Success"] = "DCR " + model.Status + " successfully";
                            return Json(new { Remark = true, Status = true, Msg = "DCR " + model.Status + " successfully" }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { Status = false, Msg = "Something went wrong try again later" }, JsonRequestBehavior.AllowGet);
                        }                       
                    }
                    else
                    {
                        TempData["Info"] = "You are not authorized person";
                        return Json(new { Status = true, Msg = "You are not authorized person" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else if (model.DCREvaluateModel != null)
                {
                    if (model.DCREvaluateModel.Remark != null)
                    {
                        model.Department = CommonService.objClsLoginInfo.Department;
                        model.RoleGroup = Models.SessionMgtModel.ProjectFolderList.RoleGroup;

                        var getData = _DCRService.GetDCRById(model);
                        var flag = false;
                        if (model.Page == DCRLifeCyclePolicyStep.Review.ToString())
                        {
                            if (getData.ResponsibleDesignEngineer == CommonService.objClsLoginInfo.UserName)
                            {
                                flag = true;
                            }
                        }
                        else if (model.Page == DCRLifeCyclePolicyStep.Evaluate.ToString() || model.Page == DCRLifeCyclePolicyStep.Release.ToString())
                        {
                            if (getData.ResponsibleSeniorDesignEngineer == CommonService.objClsLoginInfo.UserName)
                            {
                                flag = true;
                            }
                        }

                        if (flag)
                        {
                            if (button == "Accept")
                                model.Status = ReviewOrEvaluate.Accepted.ToString();
                            else if (button == "Reject")
                                model.Status = ReviewOrEvaluate.Rejected.ToString();
                            else if (button == "Return")
                                model.Status = ReviewOrEvaluate.Return.ToString();

                            string result = _DCRService.AddReviewOrEvaluate(model);

                            if (result == ReviewOrEvaluate.Review.ToString())
                            {
                                TempData["Success"] = "DCR " + model.Status + " successfully";
                                return Json(new { Remark = true, Status = true, Msg = "DCR " + model.Status + " successfully" }, JsonRequestBehavior.AllowGet);
                            }
                            else if (result == ReviewOrEvaluate.Evaluate.ToString())
                            {
                                TempData["Success"] = "DCR " + model.Status + " successfully";
                                return Json(new { Remark = true, Status = true, Msg = "DCR " + model.Status + " successfully" }, JsonRequestBehavior.AllowGet);
                            }
                            else if (result == ObjectStatus.Release.ToString())
                            {
                                TempData["Success"] = "DCR " + model.Status + " successfully";
                                return Json(new { Remark = true, Status = true, Msg = "DCR " + model.Status + " successfully" }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { Status = false, Msg = "Something went wrong try again later" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            TempData["Info"] = "You are not authorized person";
                            return Json(new { Status = true, Msg = "You are not authorized person" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new { Remark = false, Msg = "Remark is Required" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { Remark = false, Msg = "Remark is Required" }, JsonRequestBehavior.AllowGet);
                }
            }
            else if (model.DCREvaluateModel != null)
            {
                if (model.DCREvaluateModel.Remark != null)
                {
                    model.Department = CommonService.objClsLoginInfo.Department;
                    model.RoleGroup = Models.SessionMgtModel.ProjectFolderList.RoleGroup;

                    var getData = _DCRService.GetDCRById(model);
                    var flag = false;
                    if (model.Page == DCRLifeCyclePolicyStep.Review.ToString())
                    {
                        if (getData.ResponsibleDesignEngineer == CommonService.objClsLoginInfo.UserName)
                        {
                            flag = true;
                        }
                    }
                    else if (model.Page == DCRLifeCyclePolicyStep.Evaluate.ToString() || model.Page == DCRLifeCyclePolicyStep.Release.ToString())
                    {
                        if (getData.ResponsibleSeniorDesignEngineer == CommonService.objClsLoginInfo.UserName)
                        {
                            flag = true;
                        }
                    }

                    if (flag)
                    {
                        if (button == "Accept")
                            model.Status = ReviewOrEvaluate.Accepted.ToString();
                        else if (button == "Reject")
                            model.Status = ReviewOrEvaluate.Rejected.ToString();
                        else if (button == "Return")
                            model.Status = ReviewOrEvaluate.Return.ToString();

                        string result = _DCRService.AddReviewOrEvaluate(model);

                        if (result == ReviewOrEvaluate.Review.ToString())
                        {
                            TempData["Success"] = "DCR " + model.Status + " successfully";
                            return Json(new { Remark = true, Status = true, Msg = "DCR " + model.Status + " successfully" }, JsonRequestBehavior.AllowGet);
                        }
                        else if (result == ReviewOrEvaluate.Evaluate.ToString())
                        {
                            TempData["Success"] = "DCR " + model.Status + " successfully";
                            return Json(new { Remark = true, Status = true, Msg = "DCR " + model.Status + " successfully" }, JsonRequestBehavior.AllowGet);
                        }
                        else if (result == ObjectStatus.Release.ToString())
                        {
                            TempData["Success"] = "DCR " + model.Status + " successfully";
                            return Json(new { Remark = true, Status = true, Msg = "DCR " + model.Status + " successfully" }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { Status = false, Msg = "Something went wrong try again later" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        TempData["Info"] = "You are not authorized person";
                        return Json(new { Status = true, Msg = "You are not authorized person" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { Remark = false, Msg = "Remark is Required" }, JsonRequestBehavior.AllowGet);
                }
            }
            else if (model.DCRReleaseModel != null)
            {
                if (model.DCRReleaseModel.Remark != null)
                {
                    model.Department = CommonService.objClsLoginInfo.Department;
                    model.RoleGroup = Models.SessionMgtModel.ProjectFolderList.RoleGroup;

                    var getData = _DCRService.GetDCRById(model);
                    var flag = false;
                    if (model.Page == DCRLifeCyclePolicyStep.Review.ToString())
                    {
                        if (getData.ResponsibleDesignEngineer == CommonService.objClsLoginInfo.UserName)
                        {
                            flag = true;
                        }
                    }
                    else if (model.Page == DCRLifeCyclePolicyStep.Evaluate.ToString() || model.Page == DCRLifeCyclePolicyStep.Release.ToString())
                    {
                        if (getData.ResponsibleSeniorDesignEngineer == CommonService.objClsLoginInfo.UserName)
                        {
                            flag = true;
                        }
                    }

                    if (flag)
                    {
                        if (button == "Accept")
                            model.Status = ReviewOrEvaluate.Accepted.ToString();
                        else if (button == "Reject")
                            model.Status = ReviewOrEvaluate.Rejected.ToString();
                        else if (button == "Return")
                            model.Status = ReviewOrEvaluate.Return.ToString();

                        string result = _DCRService.AddReviewOrEvaluate(model);

                        if (result == ReviewOrEvaluate.Review.ToString())
                        {
                            TempData["Success"] = "DCR " + model.Status + " successfully";
                            return Json(new { Remark = true, Status = true, Msg = "DCR " + model.Status + " successfully" }, JsonRequestBehavior.AllowGet);
                        }
                        else if (result == ReviewOrEvaluate.Evaluate.ToString())
                        {
                            TempData["Success"] = "DCR " + model.Status + " successfully";
                            return Json(new { Remark = true, Status = true, Msg = "DCR " + model.Status + " successfully" }, JsonRequestBehavior.AllowGet);
                        }
                        else if (result == ObjectStatus.Release.ToString())
                        {
                            TempData["Success"] = "DCR " + model.Status + " successfully";
                            return Json(new { Remark = true, Status = true, Msg = "DCR " + model.Status + " successfully" }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { Status = false, Msg = "Something went wrong try again later" }, JsonRequestBehavior.AllowGet);
                        }                        
                    }
                    else
                    {
                        TempData["Info"] = "You are not authorized person";
                        return Json(new { Status = true, Msg = "You are not authorized person" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {                    
                    return Json(new { Remark = false, Msg = "Remark is Required" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { Remark = false, Msg = "Remark is Required" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AddDeptPerson(int TNo, string dept) //, string dept
        {
            DCRModel model = new DCRModel();
            ViewBag.FunctionList = new SelectList(_DepartmentGroupService.GetTypeList(), "t_dimx", "t_desc");
            //ViewBag.PSNumberList = new SelectList(_DepartmentGroupService.GetPSNumberList(dept), "t_psno", "t_name");
            TempData["NoTask"] = TNo;
            return PartialView("_partialDeptPerson", model);
        }

        [HttpPost]
        public ActionResult GetDeptPersonGrid(DataTableParamModel parm, Int64? DCRId, Int64? DCREvaluatId)
        {
            int recordsTotal = 0;
            var list = _DCRService.GetDeptPersonGridByDCRId(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal, DCRId, DCREvaluatId);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        [HttpPost]
        public ActionResult UpdateApproval(DCRModel model, string button)
        {
            var getData = _DCRService.GetDCRCommentPersonModel(model);
            var flag = false;
            if (model.Page == ReviewOrEvaluate.Approval.ToString())
            {
                if (getData.DepartmentGroupList != null && getData.DepartmentGroupList.Count() > 0)
                {
                    var itemcount = getData.DepartmentGroupList.Where(vv => vv.PSNoList.Contains(CommonService.objClsLoginInfo.UserName)).Count();
                    if (itemcount > 0)
                    {
                        flag = true;
                    }
                }
            }

            if (flag)
            {
                if (button == "Accept")
                {
                    model.DCREvaluateCommentModel.IsApproved = true;
                    model.DCREvaluateCommentModel.IsCompleted = true;
                }
                else if (button == "Reject")
                {
                    model.DCREvaluateCommentModel.IsApproved = false;
                    model.DCREvaluateCommentModel.IsCompleted = true;
                }

                if (model.DCREvaluateCommentModel.Remark != null)
                {
                    var Status = _DCRService.UpdateApproval(model.DCREvaluateCommentModel);
                    if (Status)
                    {
                        TempData["Success"] = "DCR submitted Successfully";
                        return Json(new { Status, model.DCREvaluateCommentModel.Remark, Msg = "DCR submitted Successfully" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { Status, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    //TempData["Info"] = "Remark is Required";
                    return Json(new { model.DCREvaluateCommentModel.Remark, Msg = "Remark is Required" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                TempData["Info"] = "You are not authorized person";
                return Json(new { Status = true, Msg = "You are not authorized person" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ReleasedProcess(DCRDocumentModel DCRDocumentModel, DCRPartModel DCRPartModel, DCRModel DCRModel)
        {
            var getData = _DCRService.GetDCRById(DCRModel);
            var flag = false;
            if (DCRModel.Page == DCRLifeCyclePolicyStep.InCorporate.ToString())
            {
                if (getData.ResponsibleSeniorDesignEngineer == CommonService.objClsLoginInfo.UserName)
                {
                    flag = true;
                }
            }
            if (flag)
            {
                //if (DCRModel.ReleasedRemark != null)
                //{
                    var result = _DCRService.ReleasedProcess(DCRDocumentModel, DCRPartModel, DCRModel, SessionMgtModel.ProjectFolderList.ProjectNo);
                    //if (result.Count > 0)
                    return Json(new { result, status = true, Msg = "DCR incorporated Successfully" }, JsonRequestBehavior.AllowGet);
                    //else
                    //    return Json(new { result, status = true, Msg = "DCR rejected" }, JsonRequestBehavior.AllowGet);
                //}
                //else
                //{
                //    TempData["Info"] = "Remark is required";
                //    return Json(new { ReleasedRemark = false, Msg = "Remark is required" }, JsonRequestBehavior.AllowGet);
                //} 
            }
            else
            {
                TempData["Info"] = "You are not authorized person";
                return Json(new { status = false, Msg = "You are not authorized person" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetAllHistory(DataTableParamModel parm, Int64? DCRId)
        {
            int recordsTotal = 0;
            var list = _DCRService.GetDCRHistory(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal, DCRId);

            return Json(new
            {
                Echo = parm.sEcho,
                iTotalRecord = recordsTotal,
                iTotalDisplayRecords = recordsTotal,
                data = list
            });
        }

        public JsonResult DeleteDocFile(Int64 Id)
        {
            string errorMsg = "";
            Int64 status = _DCRService.DeleteDocFile(Id, out errorMsg);
            if (status > 0)
            {
                return Json(new { Status = true, Msg = "Document deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult GetDatabyGroupId(DCRModel dbGroupId)
        {

            ViewBag.DistributionGroupId = dbGroupId.DistributionGroupId;
            return PartialView("_partialDCRSelectedGroup");
            //var status = _DINService.getDataByGroupID(dbGroupId.DistributionGroupId);

            //DINGroupModel model = new DINGroupModel();
            //string dept = string.Empty;

            //return View();
        }

        [HttpPost]
        public JsonResult GetDCRDataByGroupID(DCRModel model)
        {
            string errorMsg = "";
            var data = _DCRService.getDataByGroupID(model.DistributionGroupId);
            if (data != null)
            {
                return Json(new { data, Status = true, Msg = "Remarks updated successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    return Json(new { Status = false, Msg = errorMsg }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        //[HttpPost]
        //public ActionResult GetPendingDCR(DataTableParamModel parm)
        //{
        //    int recordsTotal = 0;
        //    var Roles = CommonService.objClsLoginInfo.UserRoles;

        //    var list = _DCRService.GetPendingDCRList(parm.sSearch, parm.iDisplayStart, parm.iDisplayLength, parm.iSortCol_0, parm.sSortDir_0, out recordsTotal, Roles);

        //    return Json(new
        //    {
        //        Echo = parm.sEcho,
        //        iTotalRecord = recordsTotal,
        //        iTotalDisplayRecords = recordsTotal,
        //        data = list
        //    });
        //}


        //[HttpPost]
        //public ActionResult GetDCRName(int DCRNameId)
        //{
        //    string status = _DCRService.GenerateDCRNumber(SessionMgtModel.ProjectFolderList.ProjectNo,DCRNameId);
        //    if (status != null)
        //    {

        //        return Json(new { DCRName= status, Status = true, Msg = "JEP Document added successfully" }, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
        //    }
        //}

        [HttpPost]
        public ActionResult GetDCRReasonForChange(string DCRNo,string ReasonForChange)
        {    

            string status = _DCRService.GetDCRReasonForChange(DCRNo, ReasonForChange);
            if (status != null)
            {
                return Json(new { ReasonForChanges = status, Status = true, Msg = "Reason For Change updated successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetDCRChangeFrom(string DCRNo, string ChangeFrom)
        {
            string status = _DCRService.GetDCRChangeFrom(DCRNo, ChangeFrom);
            if (status != null)
            {
                return Json(new { ChangeFroms = status, Status = true, Msg = "Change From updated successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetDCRChangeTo(string DCRNo, string ChangeTo)
        {
            string status = _DCRService.GetDCRChangeTo(DCRNo, ChangeTo);
            if (status != null)
            {
                return Json(new { ChangeTos = status, Status = true, Msg = "Change To updated successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion
    }
}
