﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using IEMQS.Areas.MSW.Controllers;
using System.Globalization;

namespace IEMQS.Areas.KOM.Controllers
{

    public class ApproveKOMController : clsBase
    {
        // GET: KOM/Maintain
        public ActionResult Index()
        {
            return View();
        }

        [SessionExpireFilter]
        public ActionResult MKOMIndex()
        {

            return View();
        }

        [SessionExpireFilter]
        public ActionResult KOMDetails(int Id = 0, string Funcation = "", string Stage = "")
        {
            KOM001 objKOM001 = new KOM001();
            KOM002 objKOM002 = new KOM002();

            var user = objClsLoginInfo.UserName;

            var lstProjectDesc = (from a in db.COM001
                                  select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).ToList();
            ViewBag.Project = new SelectList(lstProjectDesc, "projectCode", "projectDescription");

            var project = (from a in db.KOM001
                           where a.HeaderId == Id
                           select a.Project).FirstOrDefault();
            ViewBag.Customer = Manager.GetCustomerCodeAndNameByProject(project);

            ViewBag.Funcation = Funcation;
            //List<string> lstFuncationLeader = db.COM003.Select(i => i.t_psno + " - " + i.t_name).ToList();
            //ViewBag.lstFuncationLeader = lstFuncationLeader.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            //List<string> lstBuddy = db.COM003.Where(i => i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).ToList();
            //ViewBag.lstBuddy = lstBuddy.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            if (Id > 0)
            {
                objKOM001 = db.KOM001.Where(x => x.HeaderId == Id).FirstOrDefault();
                objKOM002 = db.KOM002.Where(x => x.HeaderId == Id && x.Functions == Funcation).FirstOrDefault();


                ViewBag.LineStatus = objKOM002.Status;
                var ApproveBy = objKOM002.ApprovedBy;
                ViewBag.ApproveBy = db.COM003.Where(i => i.t_psno == ApproveBy && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
                ViewBag.Heading = "Fullkit for Project KOM - ";
                ViewBag.MainHeading = "Kick of Meeting Detail";

                if (Stage.Contains("MKOM"))
                {
                    ViewBag.Heading = "Full Kit for Project MKOM - ";
                    ViewBag.MainHeading = "Manufacturing Kick of Meeting";
                }

                if (ViewBag.LineStatus.Contains("MKOM") && Stage == "KOM")
                {
                    ViewBag.LineStatus = "KOM Approved";
                }

                ViewBag.ZeroDate = Convert.ToDateTime(objKOM001.ZeroDate).ToString("dd/MM/yyyy");
                DateTime temp = Convert.ToDateTime(objKOM001.ZeroDate);
                objKOM001.KOMPlannedDate = temp.AddDays(21);
            }

            return View(objKOM001);
        }



        [SessionExpireFilter]
        public ActionResult AddHeader(int Id = 0)
        {
            KOM001 objKOM001 = new KOM001();
            var user = objClsLoginInfo.UserName;

            var lstProjectDesc = (from a in db.COM001
                                  select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).ToList();
            ViewBag.Project = new SelectList(lstProjectDesc, "projectCode", "projectDescription");

            var project = (from a in db.KOM001
                           where a.HeaderId == Id
                           select a.Project).FirstOrDefault();
            ViewBag.Customer = Manager.GetCustomerCodeAndNameByProject(project);

            List<string> lstFuncationLeader = db.COM003.Where(i => i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).ToList();
            ViewBag.lstFuncationLeader = lstFuncationLeader.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            List<string> lstBuddy = db.COM003.Where(i => i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).ToList();
            ViewBag.lstBuddy = lstBuddy.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            if (Id > 0)
            {
                objKOM001 = db.KOM001.Where(x => x.HeaderId == Id).FirstOrDefault();
                //if (objHTC001.CreatedBy != objClsLoginInfo.UserName)
                //{
                //    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                //}
            }


            return View(objKOM001);
        }


        [SessionExpireFilter]
        public ActionResult KOMLineDetails(int Id = 0, string Funcation = "")
        {

            KOM001 objKOM001 = new KOM001();
            KOM002 objKOM002 = new KOM002();

            var user = objClsLoginInfo.UserName;

            var lstProjectDesc = (from a in db.COM001
                                  select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).ToList();
            ViewBag.Project = new SelectList(lstProjectDesc, "projectCode", "projectDescription");

            var project = (from a in db.KOM001
                           where a.HeaderId == Id
                           select a.Project).FirstOrDefault();
            ViewBag.Customer = Manager.GetCustomerCodeAndNameByProject(project);

            ViewBag.Funcation = Funcation;
            //List<string> lstFuncationLeader = db.COM003.Select(i => i.t_psno + " - " + i.t_name).ToList();
            //ViewBag.lstFuncationLeader = lstFuncationLeader.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            //List<string> lstBuddy = db.COM003.Where(i => i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).ToList();
            //ViewBag.lstBuddy = lstBuddy.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            if (Id > 0)
            {
                objKOM001 = db.KOM001.Where(x => x.HeaderId == Id).FirstOrDefault();
                objKOM002 = db.KOM002.Where(x => x.HeaderId == Id && x.Functions == Funcation).FirstOrDefault();

                ViewBag.LineStatus = objKOM002.Status;
                var ApproveBy = objKOM002.ApprovedBy;
                ViewBag.ApproveBy = db.COM003.Where(i => i.t_psno == ApproveBy && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
                ViewBag.Heading = "Fullkit for Project KOM - ";
                if (ViewBag.LineStatus.Contains("MKOM"))
                {
                    ViewBag.Heading = "Full Kit for Project MKOM - ";
                }
                //if (objHTC001.CreatedBy != objClsLoginInfo.UserName)
                //{
                //    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                //}
            }

            return View(objKOM001);
        }


        [HttpPost]
        public ActionResult SaveKOMLineDetails(FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int newRowIndex = 0;
                KOM001 objKOM001 = new KOM001();
                KOM002 objKOM002 = new KOM002();
                KOM003 objKOM003 = new KOM003();

                int headerId = Convert.ToInt32(fc["HeaderId" + newRowIndex]);
                objKOM001 = db.KOM001.Where(x => x.HeaderId == headerId).FirstOrDefault();

                objKOM003.HeaderId = headerId;
                objKOM003.Project = objKOM001.Project;
                objKOM003.Stage = fc["Stage" + newRowIndex].ToString();
                objKOM003.Functions = fc["Functions" + newRowIndex].ToString();
                objKOM003.Activity = fc["Activity" + newRowIndex].ToString();
                objKOM003.FullKitDocumentList = fc["FullKitDocumentList" + newRowIndex].ToString();
                objKOM003.Output = fc["Output" + newRowIndex].ToString();
                objKOM003.Checked = Convert.ToBoolean(fc["Checked" + newRowIndex]);
                objKOM003.Remarks = Convert.ToString(fc["Remarks" + newRowIndex]);
                objKOM003.CreatedBy = objClsLoginInfo.UserName;
                objKOM003.CreatedDate = DateTime.Now;

                //objKOM003.SectionDescription = fc["SectionDescription" + newRowIndex];
                //objKOM003.FurnaceCharge = fc["FurnaceCharge" + newRowIndex];
                //objKOM003.Height = string.IsNullOrWhiteSpace(fc["Height" + newRowIndex].ToString()) ? 0 : Convert.ToInt32(fc["Height" + newRowIndex]);
                //objKOM003.Width = string.IsNullOrWhiteSpace(fc["Width" + newRowIndex].ToString()) ? 0 : Convert.ToInt32(fc["Width" + newRowIndex]); //Convert.ToInt32(fc["txtWidth" + newRowIndex]);
                //objKOM003.Length = string.IsNullOrWhiteSpace(fc["Length" + newRowIndex].ToString()) ? 0 : Convert.ToInt32(fc["Length" + newRowIndex]); //Convert.ToInt32(fc["txtLength" + newRowIndex]);
                //objKOM003.Weight = string.IsNullOrWhiteSpace(fc["Weight" + newRowIndex].ToString()) ? 0 : Convert.ToInt32(fc["Weight" + newRowIndex]);//Convert.ToInt32(fc["txtWeight" + newRowIndex]);
                //objKOM003.Furnace = fc["Furnace" + newRowIndex].ToString();
                //objKOM003.CreatedBy = objClsLoginInfo.UserName;
                //objKOM003.CreatedOn = DateTime.Now;
                db.KOM003.Add(objKOM003);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public ActionResult GetHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetHeaderGridDataPartial");
        }

        [HttpPost]
        public ActionResult GetMKOMHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetMKOMHeaderGridDataPartial");
        }

        //[HttpPost]
        //public ActionResult GetMKOMHeaderGridDataPartial(string status)
        //{
        //    ViewBag.Status = status;
        //    return PartialView("_GetMKOMHeaderGridDataPartial");
        //}

        //[HttpPost]
        //public ActionResult GetKOMHeaderGridDataPartial(string status)
        //{
        //    ViewBag.Status = status;
        //    return PartialView("_GetKOMHeaderGridDataPartial");
        //}


        //[HttpPost]
        //public JsonResult LoadHeaderData(JQueryDataTableParamModel param)
        //{

        //    try
        //    {
        //        var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
        //        var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
        //        int StartIndex = param.iDisplayStart + 1;
        //        int EndIndex = param.iDisplayStart + param.iDisplayLength;
        //        var user = objClsLoginInfo.UserName;



        //        string strWhere = string.Empty;
        //        if (param.CTQCompileStatus.ToUpper() == "PENDING")
        //        {
        //            strWhere += "1=1 and kom1.Status in ('" + clsImplementationEnum.KOMStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.KOMStatus.KOMApproveTOC.GetStringValue() + "')";
        //        }
        //        else
        //        {
        //            strWhere += "1=1";
        //        }

        //        // strWhere += "1=1";
        //        if (!string.IsNullOrWhiteSpace(param.sSearch))
        //        {
        //            strWhere += " and (kom1.Project like '%" + param.sSearch + "%' or kom1.Equipment like '%" + param.sSearch + "%'   or kom1.Customer like '%" + param.sSearch + "%')";
        //        }
        //        // strWhere += Manager.MakeDefaultWhere(user, "ctq1.BU", "ctq1.Location");
        //        var lstResult = db.SP_KOM_GETHEADER
        //                        (
        //                        StartIndex, EndIndex, "", strWhere
        //                        ).ToList();

        //        var data = (from uc in lstResult
        //                    select new[]
        //                   {
        //                   Convert.ToString(uc.Project),
        //                     Convert.ToString(uc.Customer),
        //                   Convert.ToString(uc.Equipment),
        //                         Convert.ToString(uc.ZeroDate),
        //                    Convert.ToString(uc.Status),
        //                   Convert.ToString(uc.HeaderId),
        //                   Convert.ToString(uc.HeaderId)
        //                   }).ToList();

        //        return Json(new
        //        {
        //            sEcho = Convert.ToInt32(param.sEcho),
        //            iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
        //            iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
        //            aaData = data
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

        //        return Json(new
        //        {
        //            sEcho = param.sEcho,
        //            iTotalRecords = "0",
        //            iTotalDisplayRecords = "0",
        //            aaData = ""
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //}

        [HttpPost]
        public JsonResult LoadKOMHeaderData(JQueryDataTableParamModel param)
        {

            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;

                var RoleResult = db.SP_KOM_GETUserFuncationRole
                             (
                              "", " a.[Employee] = '" + user + "'"
                             ).ToList();
                List<string> lst = RoleResult.Select(x => x.Role).ToList();
                string CurrentRole = string.Join(",", lst);
                if (string.IsNullOrWhiteSpace(CurrentRole))
                    CurrentRole = "' '";
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string strSortOrder = string.Empty;
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and kom2.Status in ('" + clsImplementationEnum.KOMStatus.KOMSent.GetStringValue() + "'  ) ";
                }
                else
                {
                    strWhere += "1=1 ";
                }


                strWhere += " and ( kom2.FunctionalLead  = '" + user + "' or  kom2.Buddy  = '" + user + "' or   kom2.ApprovedBy  = '" + user + "' ) ";
                strWhere += " and  kom2.Functions in (" + CurrentRole + ") ";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (kom1.Project like '%" + param.sSearch + "%' or kom1.Equipment like '%" + param.sSearch + "%'   or kom1.Customer like '%" + param.sSearch + "%' or kom2.Functions like '%" + param.sSearch + "%' or kom2.Status like '%" + param.sSearch + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                // strWhere += Manager.MakeDefaultWhere(user, "ctq1.BU", "ctq1.Location");
                var lstResult = db.SP_KOM_GET_FuncationHEADER
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                uc.Contract,
                                uc.Project,
                                uc.Customer,
                                uc.Equipment,
                                Convert.ToDateTime(uc.ZeroDate).ToString("dd/MM/yyyy"),
                                Convert.ToString( uc.Status.Replace("MKOM Draft","KOM Approved").Replace("MKOM Sent for Approval","KOM Approved").Replace("MKOM Approved","KOM Approved")),
                                uc.Functions,
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }




        [HttpPost]
        public JsonResult LoadMKOMHeaderData(JQueryDataTableParamModel param)
        {

            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string strSortOrder = string.Empty;
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                List<string> lst = db.SP_KOM_GETUserFuncationRole("", " a.[Employee] = '" + user + "'")
                                            .Select(x => x.Role).ToList();

                string CurrentRole = string.Join(",", lst);

                if (string.IsNullOrWhiteSpace(CurrentRole))
                    CurrentRole = "' '";
                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and kom2.Status in ('" + clsImplementationEnum.KOMStatus.MKOMSent.GetStringValue() + "'  ) ";
                }
                else
                {
                    strWhere += "1=1 and kom2.Status in ('" + clsImplementationEnum.KOMStatus.MKOMSent.GetStringValue() + "' , '" + clsImplementationEnum.KOMStatus.MKOMDraft.GetStringValue() + "', '" + clsImplementationEnum.KOMStatus.MKOMApprove.GetStringValue() + "'  ) ";
                }

                strWhere += " and ( kom2.FunctionalLead  = '" + user + "' or  kom2.Buddy  = '" + user + "' or   kom2.MKOMApprovedBy  = '" + user + "') ";
                strWhere += " and  kom2.Functions in (" + CurrentRole + ") ";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (kom1.Project like '%" + param.sSearch + "%' or kom1.Equipment like '%" + param.sSearch + "%'   or kom1.Customer like '%" + param.sSearch + "%' or kom2.Functions like '%" + param.sSearch + "%' or kom2.Status like '%" + param.sSearch + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                // strWhere += Manager.MakeDefaultWhere(user, "ctq1.BU", "ctq1.Location");
                var lstResult = db.SP_KOM_GET_FuncationHEADER(StartIndex, EndIndex, strSortOrder, strWhere).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                uc.Contract,
                                uc.Project,
                                uc.Customer,
                                uc.Equipment,
                                Convert.ToDateTime(uc.ZeroDate).ToString("dd/MM/yyyy"),
                                uc.Status,
                                uc.Functions,
                                uc.HeaderId.ToString(),
                                uc.HeaderId.ToString()
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult verifyApprover(string approver)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                approver = approver.Split('-')[0].Trim();
                var Exists = db.COM003.Any(x => x.t_psno == approver && x.t_actv == 1);
                if (Exists == false)
                {
                    objResponseMsg.Key = false;
                }
                else
                {
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult GetHeaderData(string project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                project = project.Split('-')[0].Trim();
                var ExistsPrject = db.COM001.Any(x => x.t_cprj == project);
                if (ExistsPrject == false)
                {
                    objResponseMsg.Key = false;
                }
                else
                {
                    string customer = Manager.GetCustomerCodeAndNameByProject(project);

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = customer;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult getCodeValue(string project, string TOCapprover, string PMGapprover, string contract)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                objResponseMsg.projdesc = db.COM001.Where(i => i.t_cprj == project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objResponseMsg.appdesc = db.COM003.Where(i => i.t_psno == TOCapprover && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
                objResponseMsg.appdesc = objResponseMsg.appdesc + '|' + db.COM003.Where(i => i.t_psno == PMGapprover && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
                objResponseMsg.contractDesc = db.COM004.Where(i => i.t_cono == contract).Select(i => i.t_cono + " - " + i.t_desc).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateDetails(int lineId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string tableName = "KOM002";
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    db.SP_COMMON_LINES_UPDATE(lineId, columnName, columnValue, tableName, objClsLoginInfo.UserName);
                    //ReviseData(headerId);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult UpdateKOMDetails(int lineId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string tableName = "KOM003";
                if (!string.IsNullOrEmpty(columnName))
                {
                    bool IsUpdate = columnName == "Remarks" ? true : (!string.IsNullOrEmpty(columnValue) ? true : false);
                    if (IsUpdate)
                    {
                        db.SP_COMMON_LINES_UPDATE(lineId, columnName, columnValue, tableName, objClsLoginInfo.UserName);
                        //ReviseData(headerId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult LoadKOMLineData(JQueryDataTableParamModel param)
        {

            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                int headerid = Convert.ToInt32(param.Headerid);
                string headerstatus = param.MTStatus;
                string Funcation = param.Department;
                string strWhere = string.Empty;
                string Stage = "KOM";
                string CheckStatus = "KOM Sent for Approval";
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string strSortOrder = string.Empty;
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                var objKOM001 = db.KOM001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                if (headerstatus.Contains("MKOM"))
                {
                    Stage = "MKOM";
                    CheckStatus = "MKOM Sent for Approval";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (Activity like '%" + param.sSearch
                         + "%' or FullKitDocumentList like '%" + param.sSearch
                         + "%' or Output like '%" + param.sSearch
                          + "%' or Remarks like '%" + param.sSearch
                         + "%')";

                }
                else
                {
                    strWhere = "1=1 ";
                }
                if (Funcation == "MFG")
                {
                    if (objKOM001.CheckListFor == "YARD")
                    {
                        Funcation = "MFG_YARD";
                    }
                    else
                    { Funcation = "MFG_SHOP"; }
                }

                strWhere += " and Stage = '" + Stage + "' and Functions = '" + Funcation + "'  ";
                var lstResult = db.SP_KOM_GET_FuncationLINEDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, headerid, strWhere
                                ).ToList();

                List<SelectListItem> BoolenList = new List<SelectListItem>() { new SelectListItem { Text = "Yes", Value = "True" }, new SelectListItem { Text = "No", Value = "False" } };
                int newRecordId = 0;
                var newRecord = new[] {
                                        //Helper.GenerateHidden(newRecordId, "HeaderId", param.CTQHeaderId),
                                     
                                          "",
                                        GenerateTextboxFor(newRecordId, "Activity"),
                                         GenerateTextboxFor(newRecordId, "FullKitDocumentList"),
                                          GenerateTextboxFor(newRecordId, "Output"),
                                         Helper.GenerateDropdown(newRecordId, "Checked", new SelectList(BoolenList, "Value", "Text"), " "),
                                           GenerateTextboxFor(newRecordId, "Remarks"),
                                           Helper.GenerateGridButton(newRecordId, "Add", "Add Rocord", "fa fa-plus", "SaveKOMLineDetails();" ),
                                            Helper.GenerateHidden(newRecordId, "Functions",Convert.ToString(Funcation)),
                                           Helper.GenerateHidden(newRecordId, "Stage",Stage),
                                        Helper.GenerateHidden(newRecordId, "HeaderId",Convert.ToString(param.Headerid)),
                                        "",

                                        };

                var data = (from uc in lstResult
                            select new[]
                            {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.Activity),
                                Convert.ToString(uc.FullKitDocumentList),
                                Convert.ToString(uc.Output),
                               // Convert.ToString(uc.Checked),
                               //// Helper.GenerateDropdown(headerid, "Checked", new SelectList(BoolenList, "Value", "Text", Convert.ToString(uc.Checked)),"",""),
                               // Convert.ToString(uc.Remarks),
                               // Helper.GenerateGridButton(uc.LineId, "","Attachments","fa fa-paperclip", "showAttachments(this)"),
                                Helper.GenerateDropdown(headerid, "Checked", new SelectList(BoolenList, "Value", "Text", Convert.ToString(uc.Checked)),"","UpdateData(this, "+ uc.LineId +");",(string.Equals(headerstatus,CheckStatus,StringComparison.OrdinalIgnoreCase))?false:true),
                               GenerateTextboxFor(headerid, "Remarks", Convert.ToString(uc.Remarks),"UpdateData(this, "+ uc.LineId +");","checkStatus("+ uc.HeaderId +");",(string.Equals(headerstatus,CheckStatus,StringComparison.OrdinalIgnoreCase))?false:true),
                              
                                 // Helper.GenerateGridButton(headerid, "", "Delete Record", "fa fa-trash-o", "DeleteRecord("+ uc.LineId +");")
                                 //+"   "+Helper.GenerateGridButton(uc.LineId, "","Attachments","glyphicon glyphicon-paperclip", "showAttachments(this)"),

                                HTMLActionString(headerid,"","Delete","Delete Record","fa fa-trash-o","DeleteRecord("+ uc.LineId +");" ,  (string.Equals(Convert.ToString(uc.CreatedBy),"0",StringComparison.OrdinalIgnoreCase))?true:false)
                                  +HTMLActionString(uc.LineId,"","Attachments","Attachments","fa fa-paperclip","showAttachments(" + uc.HeaderId + "," + uc.LineId+")" , false , GetAttachment(uc.HeaderId, uc.LineId , "False")),

                                Convert.ToString(uc.Functions),
                             Convert.ToString(uc.Stage) ,
                              Convert.ToString(uc.HeaderId),
                             Convert.ToString(uc.LineId) ,

                            }).ToList();

                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }
        [NonAction]
        public static string getDocuments(string folderPath)
        {
            string result = "";
            var Files = (new clsFileUpload()).GetDocuments(folderPath);
            foreach (var file in Files)
            {
                result += "<input type=\"hidden\" id=\"" + file.Name + "\" value=\"URL:" + file.URL + "\" class=\"attach\" />";
                if (!string.IsNullOrWhiteSpace(file.Comments))
                    result += "<input type=\"text\" disabled data-toggle=\"tooltip\" placeholder=\"Comments\" id=\"_" + file.Name + "\" class=\"form-control comment\" onchange=\"$(this).attr('value', $(this).val())\" maxlength=\"100\" value=\"" + file.Comments + "\" title=\"" + file.Comments + "\" />";
                else
                    result += "<input type=\"text\" disabled  placeholder=\"Comments\" id=\"_" + file.Name + "\" class=\"form-control comment\" onchange=\"$(this).attr('value', $(this).val())\" maxlength=\"100\" value=\"\" />";
            }
            return result;
        }
        [NonAction]
        public static string ToBase64(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        [NonAction]
        public static byte[] FromBase64(string base64EncodedData)
        {
            base64EncodedData = base64EncodedData.Substring(base64EncodedData.IndexOf("base64,") + 7);
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return base64EncodedBytes;
        }

        [NonAction]
        public static void ManageDocuments(string folderPath, bool hasAttachments, Dictionary<string, string> Attach, string Uploader)
        {
            if (hasAttachments)
            {
                //var existing = clsUpload.getDocs(folderPath);
                var existing = (new clsFileUpload()).GetDocuments(folderPath);
                var toDelete = new Dictionary<string, string>();
                foreach (var item in existing)
                {
                    //if (Attach.Where(q => q.Key.Equals(item.Name)).Count() <= 0)
                    if (Attach.Where(uc => uc.Key.Equals(item.Name)).Count() <= 0)
                        toDelete.Add(item.Name, item.URL);
                    else
                    {
                        //if (Attach.Where(q => q.Key.Equals(item.Name)).FirstOrDefault().Value.Substring(0, 3) != "URL")
                        if (Attach.Where(uc => uc.Key.Equals(item.Name)).FirstOrDefault().Value.Substring(0, 3) != "URL")
                            toDelete.Add(item.Name, item.URL);
                    }
                }
                foreach (var item in toDelete)
                    clsUpload.DeleteFile(folderPath, item.Key);
                var toUpload = Attach.Where(uc => !string.IsNullOrWhiteSpace(uc.Value)).Where(uc => (uc.Value.Length <= 3) || (uc.Value.Substring(0, 3) != "URL")).ToList();

                foreach (var attch in toUpload)
                {
                    try
                    {
                        var base64Data = attch.Value;
                        var dataBytes = FromBase64(attch.Value);
                        var cmnt = Attach.Any(uc => uc.Key.Equals("_" + attch.Key));
                        var x = Attach.Where(uc => uc.Key.Equals("_" + attch.Key)).FirstOrDefault();
                        string comment = (!cmnt) ? null : Attach.Where(q => q.Key.Equals("_" + attch.Key)).FirstOrDefault().Value;
                        clsUpload.Upload(attch.Key, dataBytes, folderPath, Uploader, comment);
                    }
                    catch (Exception e) { Elmah.ErrorSignal.FromCurrentContext().Raise(e); }
                }
            }
            else
            {
                //var existing = clsUpload.getDocs(folderPath);
                var existing = (new clsFileUpload()).GetDocuments(folderPath);
                foreach (var item in existing)
                    clsUpload.DeleteFile(folderPath, item.Name);
            }
        }

        [HttpPost]
        public ActionResult DeleteKOMLine(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                KOM003 objKOM003 = db.KOM003.Where(x => x.LineId == Id).FirstOrDefault();
                db.KOM003.Remove(objKOM003);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public string getDocumentsArray(string folderPath)
        {
            string result = "";
            //var Files = clsUpload.getDocs(folderPath);
            var Files = (new clsFileUpload()).GetDocuments(folderPath);
            foreach (var file in Files)
            {
                result += "<input type=\"hidden\" id=\"" + file.Name + "\" value=\"URL:" + file.URL + "\" class=\"attach\" />";
                if (!string.IsNullOrWhiteSpace(file.Comments))
                    result += "<input type=\"text\" disabled data-toggle=\"tooltip\" placeholder=\"Comments\" id=\"_" + file.Name + "\" class=\"form-control comment\" onchange=\"$(this).attr('value', $(this).val())\" maxlength=\"100\" value=\"" + file.Comments + "\" title=\"" + file.Comments + "\" />";
                else
                    result += "<input type=\"text\" disabled  placeholder=\"Comments\" id=\"_" + file.Name + "\" class=\"form-control comment\" onchange=\"$(this).attr('value', $(this).val())\" maxlength=\"100\" value=\"\" />";

            }
            return result;
        }


        [HttpPost]
        public ActionResult SaveAttachments(bool hasAttachments, Dictionary<string, string> Attach, int LineId)
        {

            var objResult = new clsHelper.ResponseMsg();
            try
            {
                //if (db.LNC002.Any(q => q.LineId == LineId))
                if (db.KOM003.Any(uc => uc.LineId == LineId))
                {
                    var objKOM003 = db.KOM003.Where(uc => uc.LineId == LineId).FirstOrDefault();
                    var folderPath = "KOM003/" + objKOM003.LineId.ToString();
                    ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                    objResult.Key = true;
                    objResult.Value = "Saved Successfully.";
                }
                else
                {
                    objResult.Key = false;
                    objResult.Value = "Line not found.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResult.Key = false;
                objResult.Value = "Error while saving attachments";
            }
            return Json(objResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ApprovalKOMFuncation(int headerid, string Status, string Funcation)
        {
            string MainStatus;
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                if (headerid > 0)
                {
                    int HeaderId = headerid;
                    KOM002 objKOM002 = db.KOM002.Where(x => x.HeaderId == headerid && x.Functions == Funcation).FirstOrDefault();
                    if (Status.Contains("MKOM"))
                    {
                        Status = "MKOM Approved";
                        MainStatus = "MKOM Sent for Approval to PMG";
                        objKOM002.MKOMApprovedBy = objClsLoginInfo.UserName;
                        objKOM002.MKOMApprovedOn = DateTime.Now;
                        if (Funcation == "MFG" || Funcation == "PE")
                        {
                            objKOM002.ApprovedBy = objClsLoginInfo.UserName;
                            objKOM002.ApprovedOn = DateTime.Now;
                        }
                    }
                    else
                    {
                        Status = "KOM Approved";
                        MainStatus = "KOM Sent for Approval to PMG";
                        objKOM002.ApprovedBy = objClsLoginInfo.UserName;
                        objKOM002.ApprovedOn = DateTime.Now;
                    }
                    objKOM002.Status = Status;

                    db.SaveChanges();

                    KOM001 objKOM001 = db.KOM001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                    objKOM001.Status = MainStatus;
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CTQMessages.Update.ToString();
                    objResponseMsg.status = objKOM002.Status;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public static string GenerateTextboxFor(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control";

            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            htmlControl = "<input type='text' " + (isReadOnly ? "disabled" : "") + " id='" + inputID + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + "  />";

            return htmlControl;
        }

        public string GenerateAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false)
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' hdElement='" + hdElementId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + (disabled ? "disabled" : "") + " />";

            return strAutoComplete;
        }

        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", bool isDisable = false, string IsAtteched = "False")
        {
            //string htmlControl = "";
            //string inputID = buttonName + "" + rowId.ToString();
            //string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            //if (!string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
            //{
            //    if (string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue()))
            //    {
            //        htmlControl = "";// "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            //    }
            //    else
            //    {
            //        htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            //    }
            //}

            //return htmlControl;

            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (isDisable)
            {
                if (IsAtteched == "False")
                {
                    htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;pointer-events:none; opacity:0.3;margin-left:5px;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";
                }
                else
                {
                    htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;pointer-events:none; opacity:0.3;margin-left:5px;color:#32CD32' Title='" + buttonTooltip + "' class='" + className + "' ></i>";
                }
            }
            else
            {
                if (IsAtteched == "False")
                {
                    htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;margin-left:5px;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }
                else
                {
                    htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;margin-left:5px;color:#32CD32' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }

            }
            return htmlControl;
        }

        public string GetAttachment(int headerId, int lineId, string attachment)
        {
            var folderPath = "KOM003/" + headerId + "/" + lineId;
            //var existing = clsUpload.getDocs(folderPath);
            //var existing = (new clsFileUpload()).GetDocuments(folderPath);
            Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
            if (!_objFUC.CheckAnyDocumentsExits(folderPath,lineId))
            {
                attachment = "False";
            }
            else
            {
                attachment = "True";
            }
            return attachment;
        }
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "", int? HeaderId = 0, int? latestRv = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_KOM_GET_FuncationHEADER(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Contract = li.Contract,
                                      Project = li.Project,
                                      Customer = li.Customer,
                                      Status = li.Status,
                                      Equipment = li.Equipment,
                                      Functions = li.Functions,
                                      ZeroDate = li.ZeroDate == null || li.ZeroDate.Value == DateTime.MinValue ? "NA" : li.ZeroDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_KOM_GET_FuncationLINEDETAILS(1, int.MaxValue, strSortOrder, HeaderId, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      RowNo = li.ROW_NO,
                                      Activities = li.Activity,
                                      FullkitDocumentList = li.FullKitDocumentList,
                                      Output = li.Output,
                                      CheckedReviewed = li.Checked,
                                      Remarks = li.Remarks,

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

    }



}