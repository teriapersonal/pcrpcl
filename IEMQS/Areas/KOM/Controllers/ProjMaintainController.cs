﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using IEMQS.Areas.MSW.Controllers;
using System.Globalization;

namespace IEMQS.Areas.KOM.Controllers
{
    public class ProjMaintainController : clsBase
    {
        // GET: KOM/ProjMaintain
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }


        [SessionExpireFilter]
        public ActionResult MKOMIndex()
        {
            return View();
        }

        [SessionExpireFilter]
        public ActionResult KOMProjHeaderDetails()
        {

            return View();
        }

        [SessionExpireFilter]
        public ActionResult MKOMProjHeaderDetails()
        {

            return View();
        }


        [SessionExpireFilter]
        public ActionResult AddHeader(int Id = 0, string Stage = "")
        {
            KOM001 objKOM001 = new KOM001();

            var user = objClsLoginInfo.UserName;
            var name = objClsLoginInfo.Name;

            ViewBag.CurrUser = user + " - " + name;
            var lstProjectDesc = (from a in db.COM001
                                  select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).ToList();
            ViewBag.Project = new SelectList(lstProjectDesc, "projectCode", "projectDescription");

            var project = (from a in db.KOM001
                           where a.HeaderId == Id
                           select a.Project).FirstOrDefault();
            ViewBag.Customer = Manager.GetCustomerCodeAndNameByProject(project);

            var lstResult = db.SP_KOM_GETApproverName("", "1=1").ToList();

            List<string> lst = lstResult.Select(x => x.Name).ToList();

            ViewBag.lstFuncationLeader = lst.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            ViewBag.lstBuddy = lst.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            if (Id > 0)
            {
                objKOM001 = db.KOM001.Where(x => x.HeaderId == Id).FirstOrDefault();

                ViewBag.ZeroDate = Convert.ToDateTime(objKOM001.ZeroDate).ToString("yyyy/MM/dd");
                ViewBag.LineStatus = objKOM001.Status;
                ViewBag.MainHeading = "Kick of Meeting Detail";

                if (ViewBag.LineStatus == "KOM Completed" && Stage == "KOM")
                {
                    ViewBag.LineStatus = "KOM Completed.";

                }
                if (ViewBag.LineStatus.Contains("MKOM") && Stage == "KOM")
                {
                    ViewBag.LineStatus = "KOM Completed.";
                }

                if (ViewBag.LineStatus.Contains("MKOM"))
                {
                    ViewBag.MainHeading = "Manufacturing Kick of Meeting";
                }

                if (ViewBag.LineStatus == "KOM Completed" && Stage == "MKOM")
                {
                    ViewBag.MainHeading = "Manufacturing Kick of Meeting";
                }
            }
            else
            {
                ViewBag.LineStatus = "-";
                ViewBag.MainHeading = "Kick of Meeting Detail";
            }

            ViewBag.Stage = Stage;
            ViewBag.UserRole = GetUserAccessRights().UserRole;
            return View(objKOM001);
        }

        public UserRoleAccessDetails GetUserAccessRights()
        {
            UserRoleAccessDetails objUserRoleAccessDetails = new UserRoleAccessDetails();

            try
            {
                var role = (from a in db.ATH001
                            join b in db.ATH004 on a.Role equals b.Id
                            where a.Employee.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                            select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();

                if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.PMG3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Initiator.GetStringValue();
                }
                else if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG2.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.PMG2.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Approver.GetStringValue();
                }
            }
            catch
            {

            }
            return objUserRoleAccessDetails;
        }


        [HttpPost]
        public ActionResult GetEmployeeResult(string term, string Dept, string Person)
        {
            //  List<Employee> lstEmployee = getEmployeeAutocompletebyDept(term, Dept).ToList();
            //    return Json(lstEmployee, JsonRequestBehavior.AllowGet);
            var role = "";

            if (Dept == "WE")
            { role = "Role = '" + clsImplementationEnum.UserRoleName.WE2.GetStringValue() + "'"; }
            else if (Dept == "MCC")
            { role = "Role = '" + clsImplementationEnum.UserRoleName.MCC2.GetStringValue() + "'"; }
            else if (Dept == "PCC")
            { role = "Role = '" + clsImplementationEnum.UserRoleName.PLNG2.GetStringValue() + "'"; }
            else if (Dept == "NDE")
            { role = "Role = '" + clsImplementationEnum.UserRoleName.NDE2.GetStringValue() + "'"; }
            else if (Dept == "QA/QC")
            { role = "Role in ( '" + clsImplementationEnum.UserRoleName.QA2.GetStringValue() + "' , '" + clsImplementationEnum.UserRoleName.QC2.GetStringValue() + "' ) "; }
            else if (Dept == "PMG")
            { role = "Role = '" + clsImplementationEnum.UserRoleName.PMG2.GetStringValue() + "'"; }
            else if (Dept == "DCC")
            { role = "Role = '" + clsImplementationEnum.UserRoleName.ENGG2.GetStringValue() + "'"; }
            else if (Dept == "MFG")
            { role = "Role = '" + clsImplementationEnum.UserRoleName.MFG2.GetStringValue() + "'"; }
            else if (Dept == "PE")
            { role = "Role = '" + clsImplementationEnum.UserRoleName.PE2.GetStringValue() + "'"; }
            string condition;
            if (Person == "")
            {
                condition = " where  ( upper(Name) like '%" + term.ToUpper() + "%' )";

            }
            else
            {
                condition = " where upper(Name) != '" + Person.ToUpper() + "' and ( upper(Name) like '%" + term.ToUpper() + "%' )";

            }
            //   string condition = " where ( upper(Name) like '%" + term.ToUpper() + "%' )   ";
            //if (Person != "")
            //{
            //    condition += "and upper(Name) != '"+ Person.ToUpper() + "'";
            //}
            var lstResult = db.SP_KOM_GETApproverName
                         (
                          condition, role
                         ).ToList();

            List<CategoryData> lst = lstResult.Select(x => new CategoryData { Code = x.Name }).ToList();

            //  ViewBag.lstBuddy = lst.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();
            return Json(lst, JsonRequestBehavior.AllowGet);
        }


        [SessionExpireFilter]
        public ActionResult ProjLineDetails(int Id = 0, string Stage = "")
        {
            KOM001 objKOM001 = new KOM001();
            KOM002 objKOM002 = new KOM002();
            var user = objClsLoginInfo.UserName;

            var lstProjectDesc = (from a in db.COM001
                                  select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).ToList();
            ViewBag.Project = new SelectList(lstProjectDesc, "projectCode", "projectDescription");

            var project = (from a in db.KOM001
                           where a.HeaderId == Id
                           select a.Project).FirstOrDefault();
            ViewBag.Customer = Manager.GetCustomerCodeAndNameByProject(project);

            var lstResult = db.SP_KOM_GETApproverName
                          (
                           "", "1=1"
                          ).ToList();

            List<string> lst = lstResult.Select(x => x.Name).ToList();

            //List<string> lstFuncationLeader = db.COM003.Select(i => i.t_psno + " - " + i.t_name).ToList();
            ViewBag.lstFuncationLeader = lst.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            //List<string> lstBuddy = db.COM003.Where(i => i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).ToList();
            ViewBag.lstBuddy = lst.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();


            if (Id > 0)
            {
                objKOM001 = db.KOM001.Where(x => x.HeaderId == Id).FirstOrDefault();


                DateTime temp = Convert.ToDateTime(objKOM001.ZeroDate);
                objKOM001.KOMPlannedDate = temp.AddDays(21);



                ViewBag.LineStatus = objKOM001.Status;
                ViewBag.Heading = "Project Level Full Kit For KOM";
                ViewBag.MainHeading = "Kick of Meeting Detail";

                if (ViewBag.LineStatus.Contains("MKOM") && Stage == "KOM")
                {
                    ViewBag.LineStatus = "KOM Completed";

                }

                if (ViewBag.LineStatus.Contains("MKOM"))
                {
                    ViewBag.Heading = "Project Level Full Kit For MKOM";
                    ViewBag.MainHeading = "Manufacturing Kick of Meeting";
                    objKOM001.KOMPlannedDate = temp.AddDays(135);
                }
                ViewBag.ZeroDate = Convert.ToDateTime(objKOM001.ZeroDate).ToString("dd/MM/yyyy");
            }

            return View(objKOM001);
        }

        [HttpPost]
        public ActionResult GetHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetHeaderGridDataPartial");
        }


        [HttpPost]
        public ActionResult GetMKOMHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetMKOMHeaderGridDataPartial");
        }

        [HttpPost]
        public ActionResult GetKOMProjHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetKOMProjHeaderGridDataPartial");
        }

        [HttpPost]
        public ActionResult GetMKOMProjHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetMKOMProjHeaderGridDataPartial");
        }

        [HttpPost]
        public JsonResult LoadHeaderData(JQueryDataTableParamModel param)
        {

            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string strSortOrder = string.Empty;
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);


                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and kom1.Status in ('" + clsImplementationEnum.KOMStatus.Draft.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "1=1";
                }

                // strWhere += "1=1";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (kom1.Project like '%" + param.sSearch + "%' or kom1.Equipment like '%" + param.sSearch + "%'   or kom1.Customer like '%" + param.sSearch + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }


                // strWhere += Manager.MakeDefaultWhere(user, "ctq1.BU", "ctq1.Location");
                var lstResult = db.SP_KOM_GETHEADER(StartIndex, EndIndex, strSortOrder, strWhere).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                uc.Contract,
                                uc.Project,
                                uc.Customer,
                                uc.Equipment,
                                Convert.ToDateTime(uc.ZeroDate).ToString("dd/MM/yyyy"),
                                Convert.ToString( uc.Status.Replace("MKOM Approved by TOC","KOM Completed").Replace("MKOM Approved by PMG","KOM Completed").Replace("MKOM Sent for Approval to PMG","KOM Completed").Replace("MKOM Completed","KOM Completed").Replace("MKOM","KOM Completed")),
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult LoadMKOMHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;
                string strSortOrder = string.Empty;
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += string.Format("1=1 and kom1.Status in ('{0}')", clsImplementationEnum.KOMStatus.KOMCompleted.GetStringValue());
                }
                else
                {
                    strWhere += string.Format("1=1 and kom1.Status in ('{0}','{1}','{2}','{3}','{4}','{5}')", clsImplementationEnum.KOMStatus.MKOM.GetStringValue(), clsImplementationEnum.KOMStatus.MKOMSentPMG.GetStringValue(), clsImplementationEnum.KOMStatus.MKOMApprovePMG.GetStringValue(), clsImplementationEnum.KOMStatus.MKOMApproveTOC.GetStringValue(), clsImplementationEnum.KOMStatus.KOMCompleted.GetStringValue(), clsImplementationEnum.KOMStatus.MKOMCompleted.GetStringValue());
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += string.Format(" AND (kom1.Project LIKE '%{0}%' OR kom1.Equipment LIKE '%{0}%' OR kom1.Customer LIKE '%{0}%')", param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_KOM_GETHEADER(StartIndex, EndIndex, strSortOrder, strWhere).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                uc.Contract,
                                uc.Project,
                                uc.Customer,
                                uc.Equipment,
                                Convert.ToDateTime(uc.ZeroDate).ToString("dd/MM/yyyy"),
                                uc.Status,
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult LoadKOMProjHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;
                string strWhere = string.Empty;
                string strSortOrder = string.Empty;
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and kom1.Status in ('" + clsImplementationEnum.KOMStatus.KOMSentPMG.GetStringValue() + "' )";
                }
                else
                {
                    strWhere += "1=1  and kom1.Status not in ('" + clsImplementationEnum.KOMStatus.Draft.GetStringValue() + "'  , '" + clsImplementationEnum.KOMStatus.KOM.GetStringValue() + "')";
                    //and kom1.Status in ('" + clsImplementationEnum.KOMStatus.KOMSentPMG.GetStringValue() + "' ,'" + clsImplementationEnum.KOMStatus.KOMApprovePMG.GetStringValue() + "' ,'" + clsImplementationEnum.KOMStatus.KOMApproveTOC.GetStringValue() + "' , '" + clsImplementationEnum.KOMStatus.MKOMSentPMG.GetStringValue() + "' ,'" + clsImplementationEnum.KOMStatus.MKOMApprovePMG.GetStringValue() + "' ,'" + clsImplementationEnum.KOMStatus.MKOMApproveTOC.GetStringValue() + "')";
                }
                strWhere += " and ([KOMPMG] = '" + user + "'  ) ";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (kom1.Project like '%" + param.sSearch + "%' or kom1.Equipment like '%" + param.sSearch + "%'   or kom1.Customer like '%" + param.sSearch + "%'  or kom1.Status like '%" + param.sSearch + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_KOM_GETHEADER(StartIndex, EndIndex, strSortOrder, strWhere).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                uc.Contract,
                                uc.Project,
                                uc.Customer,
                                uc.Equipment,
                                Convert.ToDateTime(uc.ZeroDate).ToString("dd/MM/yyyy"),
                                Convert.ToString( uc.Status.Replace("MKOM Approved by TOC","KOM Completed").Replace("MKOM Approved by PMG","KOM Completed").Replace("MKOM Sent for Approval to PMG","KOM Completed").Replace("MKOM Completed","KOM Completed").Replace("MKOM","KOM Completed")),
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult LoadMKOMProjHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;
                string strWhere = string.Empty;
                string strSortOrder = string.Empty;
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and kom1.Status in ('" + clsImplementationEnum.KOMStatus.MKOMSentPMG.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "1=1 and kom1.Status in ('" + clsImplementationEnum.KOMStatus.MKOMSentPMG.GetStringValue() + "' ,'" + clsImplementationEnum.KOMStatus.MKOMApprovePMG.GetStringValue() + "' ,'" + clsImplementationEnum.KOMStatus.MKOMApproveTOC.GetStringValue() + "' ,'" + clsImplementationEnum.KOMStatus.MKOMCompleted.GetStringValue() + "')";
                }
                strWhere += " and ( [MKOMPMG] = '" + user + "'  ) ";
                // strWhere += "1=1";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (kom1.Project like '%" + param.sSearch + "%' or kom1.Equipment like '%" + param.sSearch + "%'   or kom1.Customer like '%" + param.sSearch + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                // strWhere += Manager.MakeDefaultWhere(user, "ctq1.BU", "ctq1.Location");
                var lstResult = db.SP_KOM_GETHEADER(StartIndex, EndIndex, strSortOrder, strWhere).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                uc.Contract,
                                uc.Project,
                                uc.Customer,
                                uc.Equipment,
                                Convert.ToDateTime(uc.ZeroDate).ToString("dd/MM/yyyy"),
                                uc.Status,
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult LoadLineData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                int headerid = Convert.ToInt32(param.Headerid);
                string headerstatus = param.MTStatus;
                string strWhere = string.Empty;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (Functions like '%" + param.sSearch
                        + "%' or FunctionalLead like '%" + param.sSearch
                        + "%' or Buddy like '%" + param.sSearch
                        + "%')";
                }
                else
                {
                    strWhere = "1=1";
                }
                var lstResult = db.SP_KOM_GET_LINEDETAILS
                                (
                                StartIndex, EndIndex, "", headerid, strWhere
                                ).ToList();
                int newRecordId = 0;
                var newRecord = new[] {
                                        //Helper.GenerateHidden(newRecordId, "HeaderId", param.CTQHeaderId),
                                     
                                     "",
                                        GenerateTextboxFor(newRecordId, "Functions"),
                                       GenerateAutoComplete(newRecordId,"FunctionalLead","","",false,"","FunctionalLead"),
                                       GenerateAutoComplete(newRecordId,"Buddy","","",false,"","Buddy"),
                                        Helper.GenerateHidden(newRecordId, "HeaderId",Convert.ToString(param.Headerid)),
                                        "",

                                        };

                var data = (from uc in lstResult
                            select new[]
                            {
                                Convert.ToString(uc.ROW_NO),
                            //    Convert.ToString(uc.Functions),
                              GenerateAutoComplete(uc.LineId, "Functions", Convert.ToString(uc.Functions),"UpdateData(this, "+ uc.LineId +");",false,"","Functions",true),

                          // GenerateTextboxFor(headerid, "Functions", Convert.ToString(uc.Functions),"UpdateData(this, "+ uc.LineId +");","checkStatus("+ uc.HeaderId +");",(string.Equals(headerstatus,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false)),
                              GenerateAutoComplete(uc.LineId, "FunctionalLead", Convert.ToString(uc.FunctionalLead),"UpdateData(this, "+ uc.LineId +");",false,"","FunctionalLead",(string.Equals(headerstatus,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false)),
                             GenerateAutoComplete(uc.LineId, "Buddy", Convert.ToString(uc.Buddy),"UpdateData(this, "+ uc.LineId +");",false,"","Buddy",(string.Equals(headerstatus,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false)),

                            Convert.ToString(uc.HeaderId),
                             Convert.ToString(uc.LineId) ,
                           }).ToList();

                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult LoadProjLineData(JQueryDataTableParamModel param)
        {

            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                int headerid = Convert.ToInt32(param.Headerid);
                string headerstatus = param.MTStatus;
                string strWhere = string.Empty;

                if (headerstatus.Contains("MKOM"))
                {
                    if (!string.IsNullOrWhiteSpace(param.sSearch))
                    {
                        strWhere = " (Project like '%" + param.sSearch
                            + "%' or Functions like '%" + param.sSearch
                            + "%' or MKOMApprovedBy like '%" + param.sSearch
                            + "%' or MKOMPMGComment like '%" + param.sSearch
                            + "%' or FunctionalLead like '%" + param.sSearch
                            + "%')";
                    }
                    else
                    {
                        strWhere = "1=1";
                    }
                    var lstResult = db.SP_MKOM_GET_ProjLINEDETAILS
                                    (
                                    StartIndex, EndIndex, "", headerid, strWhere
                                    ).ToList();


                    int newRecordId = 0;
                    var newRecord = new[] {
                                        //Helper.GenerateHidden(newRecordId, "HeaderId", param.CTQHeaderId),
                                     
                                     "",
                                     GenerateTextboxFor(newRecordId, "Functions"),
                                     GenerateTextboxFor(newRecordId, "Responsible"),
                                     GenerateTextboxFor(newRecordId, "FunctionalLead"),
                                     GenerateTextboxFor(newRecordId, "MKOMApprovedBy"),
                                     GenerateTextboxFor(newRecordId, "MKOMApprovedOn"),
                                     GenerateTextboxFor(newRecordId, "PMGComments"),
                                     Helper.GenerateHidden(newRecordId, "HeaderId",Convert.ToString(param.Headerid)),
                                     "",
                                     "",
                                    };

                    var data = (from uc in lstResult
                                select new[]
                                {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.Functions),
                                Convert.ToString(uc.Responsible),
                                Convert.ToString(uc.FunctionalLead),
                                Convert.ToString(uc.MKOMApprovedBy),
                                Convert.ToString(uc.MKOMApprovedOn),
                                GenerateTextboxFor(headerid, "MKOMPMGComment", Convert.ToString(uc.MKOMPMGComment),"UpdateData(this, "+ uc.LineId +");","checkStatus("+ uc.HeaderId +");",(string.Equals(headerstatus,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false)),

                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.LineId) ,
                                HTMLActionStringForView(headerid,uc.Functions,"MKOM", Convert.ToString(uc.MKOMApprovedBy)),
                           }).ToList();

                    data.Insert(0, newRecord);

                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                        iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                        aaData = data
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(param.sSearch))
                    {
                        strWhere = " (Project like '%" + param.sSearch
                            + "%' or Functions like '%" + param.sSearch
                            + "%' or ApprovedBy like '%" + param.sSearch
                            + "%' or PMGComments like '%" + param.sSearch
                            + "%' or FunctionalLead like '%" + param.sSearch
                            + "%')";
                    }
                    else
                    {
                        strWhere = "1=1";
                    }
                    var lstResult = db.SP_KOM_GET_ProjLINEDETAILS(StartIndex, EndIndex, "", headerid, strWhere).ToList();


                    int newRecordId = 0;
                    var newRecord = new[] {
                                        //Helper.GenerateHidden(newRecordId, "HeaderId", param.CTQHeaderId),
                                     
                                     "",
                                     GenerateTextboxFor(newRecordId, "Functions"),
                                     GenerateTextboxFor(newRecordId, "Responsible"),
                                     GenerateTextboxFor(newRecordId, "FunctionalLead"),
                                     GenerateTextboxFor(newRecordId, "ApprovedBy"),
                                     GenerateTextboxFor(newRecordId, "ApprovedOn"),
                                     GenerateTextboxFor(newRecordId, "PMGComments"),
                                     Helper.GenerateHidden(newRecordId, "HeaderId",Convert.ToString(param.Headerid)),
                                     "",
                                     "",
                                 };

                    var data = (from uc in lstResult
                                select new[]
                                {
                                    Convert.ToString(uc.ROW_NO),
                                    Convert.ToString(uc.Functions),
                                    Convert.ToString(uc.Responsible),
                                    Convert.ToString(uc.FunctionalLead),
                                    Convert.ToString(uc.ApprovedBy),
                                    Convert.ToString(uc.ApprovedOn),
                                    GenerateTextboxFor(headerid, "PMGComments", Convert.ToString(uc.PMGComments),"UpdateData(this, "+ uc.LineId +");","checkStatus("+ uc.HeaderId +");",(string.Equals(headerstatus,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false)),
                                    Convert.ToString(uc.HeaderId),
                                    Convert.ToString(uc.LineId) ,
                                    HTMLActionStringForView(headerid,uc.Functions,"KOM",  Convert.ToString(uc.ApprovedBy)),
                           }).ToList();
                    data.Insert(0, newRecord);

                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                        iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                        aaData = data
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult verifyApprover(string approver)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                approver = approver.Split('-')[0].Trim();
                var Exists = db.COM003.Any(x => x.t_psno == approver && x.t_actv == 1);
                if (Exists == false)
                {
                    objResponseMsg.Key = false;
                }
                else
                {
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetHeaderData(string project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                project = project.Split('-')[0].Trim();
                var ExistsPrject = db.COM001.Any(x => x.t_cprj == project);
                if (ExistsPrject == false)
                {
                    objResponseMsg.Key = false;
                }
                else
                {
                    string customer = Manager.GetCustomerCodeAndNameByProject(project);

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = customer;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult GetZeroDate(string project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                project = project.Split('-')[0].Trim();
                var ZeroDate = (from cm004 in db.COM004
                                join cm005 in db.COM005 on cm004.t_cono equals cm005.t_cono
                                join cm006 in db.COM006 on cm004.t_ofbp equals cm006.t_bpid
                                where cm005.t_sprj == project
                                select cm004.t_efdt).FirstOrDefault().ToString("dd/MM/yyyy");

                objResponseMsg.Key = true;
                objResponseMsg.Value = ZeroDate;

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult InitiateKOM(int headerid, string Stage)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                KOM001 objKOM001 = db.KOM001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                var isvalid = db.KOM002.Where(x => x.HeaderId == headerid && (x.FunctionalLead == "" || x.FunctionalLead == null)).Any(x => x.Project == objKOM001.Project);
                if (isvalid == false)
                {
                    if (headerid > 0)
                    {
                        string Project;
                        int HeaderId = headerid;
                        objKOM001.Status = "KOM";
                        Project = objKOM001.Project;
                        db.SaveChanges();
                        db.SP_KOM_Initiate(HeaderId, Project, Stage);

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.KOMMessage.Update.ToString();
                        objResponseMsg.status = objKOM001.Status;

                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.KOMMessage.FLine.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult InitiateMKOM(int headerid, string Stage, string TOC)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                KOM001 objKOM001 = db.KOM001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                var isvalid = db.KOM002.Where(x => x.HeaderId == headerid && (x.FunctionalLead == "" || x.FunctionalLead == null)).Any(x => x.Project == objKOM001.Project);
                if (isvalid == false)
                {
                    if (headerid > 0)
                    {
                        string Project;
                        int HeaderId = headerid;
                        objKOM001.Status = "MKOM";
                        objKOM001.MKOMTOCManager = TOC;
                        Project = objKOM001.Project;
                        db.SaveChanges();
                        db.SP_MKOM_Initiate(HeaderId, Project, Stage);

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.KOMMessage.Update.ToString();
                        objResponseMsg.status = objKOM001.Status;

                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.KOMMessage.FLine.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ApprovePMG(int headerid, string remark)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                KOM001 objKOM001 = db.KOM001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                //var isvalid = db.KOM002.Where(x => x.HeaderId == headerid && (x.PMGComments == "" || x.PMGComments == null)).Any(x => x.Project == objKOM001.Project);
                //if (isvalid == false)
                //{
                var isvalid = db.KOM002.Where(x => x.HeaderId == headerid && x.Status != "KOM Approved").Any(x => x.Project == objKOM001.Project);
                //}
                if (isvalid == false)
                {
                    if (headerid > 0)
                    {

                        int HeaderId = headerid;

                        objKOM001.Status = "KOM Approved by PMG";

                        //try
                        //{
                        //    DateTime KOMPlannedDate = Convert.ToDateTime(objKOM001.ZeroDate);
                        //    objKOM001.KOMPlannedDate = KOMPlannedDate.AddDays(21);
                        //}
                        //catch {

                        //    objKOM001.KOMPlannedDate = DateTime.Now.AddDays(21);
                        //}
                        objKOM001.KOMPMGDate = DateTime.Now;
                        objKOM001.KOMPMGRemarks = @remark;


                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.KOMMessage.Update.ToString();
                        objResponseMsg.status = objKOM001.Status;

                        var folderPath = "KOM001/" + objKOM001.HeaderId + "_KOM";
                        objResponseMsg.appdesc = folderPath;
                        //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);

                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.KOMMessage.SentForApprovalValid.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult MKOMApprovePMG(int headerid, string remark)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                KOM001 objKOM001 = db.KOM001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                //Remarks should not be mandatory in all sessions. Obs Id#18424
                //var isvalid = db.KOM002.Where(x => x.HeaderId == headerid && (x.MKOMPMGComment == "" || x.MKOMPMGComment == null)).Any(x => x.Project == objKOM001.Project);
                //if (isvalid == false)
                //{
                var isvalid = db.KOM002.Where(x => x.HeaderId == headerid && (x.Status != "MKOM Approved" && x.Status != "KOM Approved")).Any(x => x.Project == objKOM001.Project);
                //}
                if (isvalid == false)
                {
                    if (headerid > 0)
                    {

                        int HeaderId = headerid;
                        objKOM001.Status = "MKOM Approved by PMG";



                        //try
                        //{
                        //    DateTime MKOMPlannedDate = Convert.ToDateTime(objKOM001.ZeroDate);
                        //    objKOM001.MKOMPlannedDate = MKOMPlannedDate.AddDays(135);
                        //}
                        //catch
                        //{

                        //    objKOM001.MKOMPlannedDate = DateTime.Now.AddDays(21);
                        //}
                        objKOM001.MKOMPMGDate = DateTime.Now;
                        objKOM001.MKOMPMGRemarks = @remark;


                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.KOMMessage.Update.ToString();
                        objResponseMsg.status = objKOM001.Status;

                        var folderPath = "KOM001/" + objKOM001.HeaderId + "_MKOM";
                        objResponseMsg.appdesc = folderPath;
                        //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);

                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.KOMMessage.SentForApprovalValid.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ComplatePMG(int headerid)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                KOM001 objKOM001 = db.KOM001.Where(x => x.HeaderId == headerid).FirstOrDefault();

                if (headerid > 0)
                {
                    int HeaderId = headerid;
                    objKOM001.Status = "KOM Completed";
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.KOMMessage.Update.ToString();
                    objResponseMsg.status = objKOM001.Status;
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public JsonResult MKOMComplatePMG(int headerid)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                KOM001 objKOM001 = db.KOM001.Where(x => x.HeaderId == headerid).FirstOrDefault();

                if (headerid > 0)
                {
                    int HeaderId = headerid;
                    objKOM001.Status = "MKOM Completed";
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.KOMMessage.Update.ToString();
                    objResponseMsg.status = objKOM001.Status;
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveKOMHeader(KOM001 kom001, FormCollection fc)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                if (!string.IsNullOrWhiteSpace(fc["hfHeaderId"].ToString()) && Convert.ToInt32(fc["hfHeaderId"].ToString()) > 0)
                {
                    int hid = Convert.ToInt32(fc["hfHeaderId"].ToString());
                    string project = fc["hfProject"].ToString();
                    string IProject = fc["IProject"].ToString();

                    if (!string.IsNullOrWhiteSpace(IProject))
                    {
                        List<string> pList = IProject.Split(',').ToList();
                        if (pList.Any(x => x.ToString() == project))
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Primary Project can not be used as Identical Project";
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }
                    }

                    objResponseMsg = CheckProjectInKOM(hid, project, IProject, false);
                    if (!objResponseMsg.Key)
                    {
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }

                    KOM001 objKOM001 = db.KOM001.Where(x => x.HeaderId == hid).FirstOrDefault();
                    objKOM001.Project = fc["hfProject"].ToString();
                    //objKOM001.Customer = fc["txtCust"].ToString().Split('-')[0];
                    //objKOM001.Equipment = fc["txtEquipment"].ToString();

                    if (string.IsNullOrWhiteSpace(fc["ZeroDate"].ToString()))
                        objKOM001.ZeroDate = null;
                    else
                        objKOM001.ZeroDate = DateTime.ParseExact(fc["ZeroDate"].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);  // changed for 18365

                    objKOM001.Status = "Draft";
                    objKOM001.KOMTOCManager = fc["ddlTOCApprover"].ToString().Split('-')[0].Trim();
                    //objKOM001.KOMPMG = fc["ddlPMGApprover"].ToString().Split('-')[0].Trim();
                    objKOM001.IProject = fc["IProject"].ToString();
                    objKOM001.ContractNo = fc["hdnContractNo"].ToString(); // changed for 18365
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CTQMessages.Update.ToString();
                    objResponseMsg.status = objKOM001.Status;

                }
                else
                {
                    string project = fc["ddlProject"].ToString().Split('-')[0].Trim();
                    string IProject = fc["IProject"].ToString();

                    //validation : if project or identical project should be used only once as primary project or identical project.
                    if (!string.IsNullOrWhiteSpace(IProject))
                    {
                        List<string> pList = IProject.Split(',').ToList();
                        if (pList.Any(x => x.ToString() == project))
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Primary Project can not be used as Identical Project";
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }
                    }

                    objResponseMsg = CheckProjectInKOM(0, project, IProject, false);
                    if (!objResponseMsg.Key)
                    {
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }

                    var isvalid = db.KOM001.Any(x => x.Project == project);
                    if (isvalid == false)
                    {
                        KOM001 objkom001 = new KOM001();
                        objkom001.Project = project;
                        objkom001.KOMTOCManager = fc["ddlTOCApprover"].ToString().Split('-')[0].Trim();
                        objkom001.KOMPMG = fc["ddlPMGApprover"].ToString().Split('-')[0].Trim();
                        objkom001.MKOMPMG = fc["ddlPMGApprover"].ToString().Split('-')[0].Trim();
                        objkom001.Customer = fc["txtCust"].ToString().Split('-')[0];
                        objkom001.Equipment = fc["txtEquipment"].ToString();
                        objkom001.IProject = IProject;

                        if (string.IsNullOrWhiteSpace(fc["ZeroDate"].ToString()))
                            objkom001.ZeroDate = DateTime.Now;
                        else
                            objkom001.ZeroDate = DateTime.ParseExact(fc["ZeroDate"].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);

                        objkom001.ContractNo = fc["hdnContractNo"].ToString(); // changed for 18365
                        objkom001.Status = "Draft";
                        //objkom001.CreatedBy = objClsLoginInfo.UserName;
                        //objkom001.CreatedOn = DateTime.Now;                        
                        db.KOM001.Add(objkom001);
                        db.SaveChanges();


                        objkom001 = db.KOM001.Where(x => x.Project == project).FirstOrDefault();
                        var objHeaderId = objkom001.HeaderId;

                        KOM002 objkom002 = new KOM002();
                        objkom002.HeaderId = objHeaderId;
                        objkom002.Project = project;
                        objkom002.Functions = "DCC";

                        db.KOM002.Add(objkom002);
                        db.SaveChanges();

                        objkom002.HeaderId = objHeaderId;
                        objkom002.Project = project;
                        objkom002.Functions = "PMG";

                        db.KOM002.Add(objkom002);
                        db.SaveChanges();

                        objkom002.HeaderId = objHeaderId;
                        objkom002.Project = project;
                        objkom002.Functions = "QA/QC";

                        db.KOM002.Add(objkom002);
                        db.SaveChanges();

                        objkom002.HeaderId = objHeaderId;
                        objkom002.Project = project;
                        objkom002.Functions = "NDE";

                        db.KOM002.Add(objkom002);
                        db.SaveChanges();

                        objkom002.HeaderId = objHeaderId;
                        objkom002.Project = project;
                        objkom002.Functions = "PCC";

                        db.KOM002.Add(objkom002);
                        db.SaveChanges();

                        objkom002.HeaderId = objHeaderId;
                        objkom002.Project = project;
                        objkom002.Functions = "MCC";

                        db.KOM002.Add(objkom002);
                        db.SaveChanges();

                        objkom002.HeaderId = objHeaderId;
                        objkom002.Project = project;
                        objkom002.Functions = "WE";

                        db.KOM002.Add(objkom002);
                        db.SaveChanges();

                        objkom002.HeaderId = objHeaderId;
                        objkom002.Project = project;
                        objkom002.Functions = "MFG";

                        db.KOM002.Add(objkom002);
                        db.SaveChanges();

                        objkom002.HeaderId = objHeaderId;
                        objkom002.Project = project;
                        objkom002.Functions = "PE";// clsImplementationEnum.UserRoleName.ProductionEngineering.GetStringValue();

                        db.KOM002.Add(objkom002);
                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CTQMessages.Insert;
                        objResponseMsg.status = objkom001.Status;
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.Message;
                    }
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateDetails(int lineId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string tableName = "KOM002";
                if (!string.IsNullOrEmpty(columnName)) // && !string.IsNullOrEmpty(columnValue)
                {
                    db.SP_COMMON_LINES_UPDATE(lineId, columnName, columnValue, tableName, objClsLoginInfo.UserName);
                    if (columnName.ToLower() == "functionallead")
                        db.SP_COMMON_LINES_UPDATE(lineId, "ApprovedBy", columnValue, tableName, objClsLoginInfo.UserName);
                    //ReviseData(headerId);
                    if (!string.IsNullOrEmpty(columnValue))
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateProjDetails(int lineId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string tableName = "KOM002";
                if (!string.IsNullOrEmpty(columnName))
                {
                    bool IsUpdate = (columnName == "PMGComments" || columnName == "MKOMPMGComment") ? true : (!string.IsNullOrEmpty(columnValue) ? true : false);
                    if (IsUpdate)
                    {
                        db.SP_COMMON_LINES_UPDATE(lineId, columnName, columnValue, tableName, objClsLoginInfo.UserName);
                        //ReviseData(headerId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult getHeaderId(string project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                project = project.Split('-')[0].Trim();
                int Headerid = (from a in db.KOM001
                                where a.Project == project
                                select a.HeaderId).FirstOrDefault();
                string status = (from a in db.KOM001
                                 where a.Project == project
                                 select a.Status).FirstOrDefault();
                objResponseMsg.Key = true;
                objResponseMsg.Value = Headerid.ToString() + "|" + status;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult getCodeValue(string project, string TOCapprover, string PMGapprover, string contract)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {

                objResponseMsg.projdesc = db.COM001.Where(i => i.t_cprj == project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objResponseMsg.appdesc = db.COM003.Where(i => i.t_psno == TOCapprover && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
                objResponseMsg.appdesc = objResponseMsg.appdesc + '|' + db.COM003.Where(i => i.t_psno == PMGapprover && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
                objResponseMsg.contractDesc = db.COM004.Where(i => i.t_cono == contract).Select(i => i.t_cono + " - " + i.t_desc).FirstOrDefault();

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public static string GenerateTextboxFor(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control";

            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            htmlControl = "<input type='text' " + (isReadOnly ? "disabled" : "") + " id='" + inputID + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + "  />";

            return htmlControl;
        }

        public string GenerateAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false)
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' hdElement='" + hdElementId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + (disabled ? "disabled" : "") + " />";

            return strAutoComplete;
        }

        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (!string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
            {
                if (string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue()))
                {
                    htmlControl = "";// "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }
                else
                {
                    htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }
            }

            return htmlControl;
        }

        public string HTMLActionStringForView(int rowId, string Funcation, string Stage, string isapprove)
        {
            string htmlControl = "";
            string inputID = WebsiteURL + "/KOM/Maintain/KOMLineDetails?Id=" + rowId + "&Funcation=" + Funcation + "&Stage=" + Stage + "&ReadOnly=1";
            string Icon = "<i style = 'margin-left:5px;' class='fa fa-eye'></i>";

            if (isapprove == null)
            {
                htmlControl = "<a title='View' style = 'pointer-events:none; opacity:0.3'  href= '" + inputID + "' target='_blank'>" + Icon + "</a>";
            }
            else
            {
                htmlControl = "<a  title='View'  href= '" + inputID + "' target='_blank'>" + Icon + "</a>";
            }


            return htmlControl;
        }
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "", int? HeaderId = 0, int? latestRv = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_KOM_GETHEADER(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Contract = li.Contract,
                                      Project = li.Project,
                                      Customer = li.Customer,
                                      Status = li.Status,
                                      Equipment = li.Equipment,
                                      //Functions = li.Functions,
                                      ZeroDate = li.ZeroDate == null || li.ZeroDate.Value == DateTime.MinValue ? "NA" : li.ZeroDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_KOM_GET_FuncationLINEDETAILS(1, int.MaxValue, strSortOrder, HeaderId, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      RowNo = li.ROW_NO,
                                      Activities = li.Activity,
                                      FullkitDocumentList = li.FullKitDocumentList,
                                      Output = li.Output,
                                      CheckedReviewed = li.Checked,
                                      Remarks = li.Remarks,

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        #region Other Projects

        public JsonResult GetOtherProjects(string HeaderId, string ContractNo = "")
        {
            try
            {
                var objCOM001List = db.COM001.ToList();

                List<Projects> lstProjects = new List<Projects>();

                var objAccessProjects = db.fn_GetContractProjectAccessForEmployee(objClsLoginInfo.UserName, objClsLoginInfo.Location).ToList();
                if (!string.IsNullOrWhiteSpace(ContractNo))
                {
                    objAccessProjects = objAccessProjects.Where(x => x.ContractNo == ContractNo).ToList();
                }

                //var objAccessProjects1 = db.fn_GetProjectAccessForEmployee(objClsLoginInfo.UserName, objClsLoginInfo.Location).ToList();
                lstProjects = (from c1 in objAccessProjects
                               select new Projects
                               {
                                   Value = c1.ProjectNo,
                                   projectCode = c1.ProjectNo,
                                   projectDescription = c1.ProjectNo + " - " + c1.ProjectName,
                                   Text = c1.ProjectNo + " - " + c1.ProjectName
                               }).Distinct().ToList();

                //skip projects used in KOM001
                //List<string> objKOM001ProjectList = db.KOM001.Select(u => u.Project).ToList();
                //lstProjects = (from u in lstProjects
                //               where !objKOM001ProjectList.Contains(u.projectCode) // && !objIProjectList.Contains(u.projectCode)
                //               select u).ToList();

                if (!string.IsNullOrWhiteSpace(HeaderId) && Convert.ToInt32(HeaderId) > 0)
                {
                    int id = Convert.ToInt32(HeaderId);
                    var IProject = db.KOM001.Where(u => u.HeaderId == id).Select(u => u.IProject).FirstOrDefault();
                    if (!string.IsNullOrWhiteSpace(IProject))
                    {
                        var list = IProject.Split(',').ToList();
                        foreach (var item in list)
                        {
                            if (!lstProjects.Any(u => u.projectCode.Trim() == item.Trim().ToString()))
                            {
                                var projdesc = objCOM001List.Where(u => u.t_cprj.Trim() == item.Trim()).Select(u => u.t_dsca).FirstOrDefault();
                                lstProjects.Add(new Projects()
                                {
                                    Value = item.Trim(),
                                    projectCode = item.Trim(),
                                    projectDescription = item.Trim() + " - " + projdesc,
                                    Text = item.Trim() + " - " + projdesc
                                });
                            }
                        }
                    }
                }

                //List<string> objIProjectList = new List<string>();
                //foreach (var obj in objKOM001List)
                //{
                //    if (!string.IsNullOrWhiteSpace(obj.IProject))
                //    {
                //        objIProjectList.AddRange(obj.IProject.Split(',').ToList());
                //    }
                //}

                var finalList = (from u in lstProjects
                                 select new { id = u.projectCode, text = u.projectDescription }).Distinct().ToList();

                return Json(finalList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region TaskID-18037 & 17994 -Hardik Create view page for KOM/MKOM, user only can see the page detail of page.

        [SessionExpireFilter]
        public ViewResult DisplayKOM(string Project)
        {
            ViewBag.chkProject = Project;
            return View();
        }

        [SessionExpireFilter]
        public ViewResult DisplayMKOM(string Project)
        {
            ViewBag.chkProject = Project;
            return View();
        }

        [HttpPost]
        public JsonResult LoadKOMProjData(JQueryDataTableParamModel param, string Project)
        {
            try
            {
                if (Project != null && Project != "")
                {
                    var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                    int sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                    int from = param.iDisplayStart + 1;
                    int to = param.iDisplayStart + param.iDisplayLength;
                    string user = objClsLoginInfo.UserName,
                            sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]),
                            query = "1=1 ",
                            strSortOrder = string.Empty,
                            sortDirection = Convert.ToString(Request["sSortDir_0"]);
                    query += " and Project='" + Project.ToString() + "'";
                    if (!string.IsNullOrWhiteSpace(param.sSearch))
                    {
                        query += string.Format("AND (kom1.Project LIKE '%{0}%' OR kom1.Equipment LIKE '%{0}%') OR kom1.Customer LIKE '%{0}%' OR kom1.Status LIKE '%{0}%'", param.sSearch);
                    }
                    else
                    {
                        query += Manager.MakeDatatableForSearch(param.SearchFilter);
                    }

                    if (!string.IsNullOrWhiteSpace(sortColumnName))
                    {
                        strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                    }

                    var data = db.SP_KOM_GETHEADER(from, to, strSortOrder, query).Select(x => new
                    {
                        x.Contract,
                        x.Project,
                        x.TotalCount,
                        x.Customer,
                        x.Equipment,
                        ZeroDate = Convert.ToDateTime(x.ZeroDate).ToString("dd/MM/yyyy"),
                        Status = Convert.ToString(x.Status.Replace("MKOM Approved by TOC", "KOM Completed").Replace("MKOM Approved by PMG", "KOM Completed").Replace("MKOM Sent for Approval to PMG", "KOM Completed").Replace("MKOM Completed", "KOM Completed").Replace("MKOM", "KOM Completed")),
                        x.HeaderId
                    }).ToList();


                    data = data.Where(x => x.Project.Contains(Project)).ToList();

                    int? count = (data.Count > 0 && data.FirstOrDefault().TotalCount > 0) ? data.FirstOrDefault().TotalCount : 0;
                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = count,
                        iTotalDisplayRecords = count,
                        aaData = data
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                    int sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                    int from = param.iDisplayStart + 1;
                    int to = param.iDisplayStart + param.iDisplayLength;
                    string user = objClsLoginInfo.UserName,
                            sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]),
                            query = "1=1 ",
                            strSortOrder = string.Empty,
                            sortDirection = Convert.ToString(Request["sSortDir_0"]);

                    if (!string.IsNullOrWhiteSpace(param.sSearch))
                    {
                        query += string.Format("AND (kom1.Project LIKE '%{0}%' OR kom1.Equipment LIKE '%{0}%') OR kom1.Customer LIKE '%{0}%' OR kom1.Status LIKE '%{0}%'", param.sSearch);
                    }
                    else
                    {
                        query += Manager.MakeDatatableForSearch(param.SearchFilter);
                    }

                    if (!string.IsNullOrWhiteSpace(sortColumnName))
                    {
                        strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                    }

                    var data = db.SP_KOM_GETHEADER(from, to, strSortOrder, query).Select(x => new
                    {
                        x.Contract,
                        x.Project,
                        x.TotalCount,
                        x.Customer,
                        x.Equipment,
                        ZeroDate = Convert.ToDateTime(x.ZeroDate).ToString("dd/MM/yyyy"),
                        Status = Convert.ToString(x.Status.Replace("MKOM Approved by TOC", "KOM Completed").Replace("MKOM Approved by PMG", "KOM Completed").Replace("MKOM Sent for Approval to PMG", "KOM Completed").Replace("MKOM Completed", "KOM Completed").Replace("MKOM", "KOM Completed")),
                        x.HeaderId
                    }).ToList();

                    int? count = (data.Count > 0 && data.FirstOrDefault().TotalCount > 0) ? data.FirstOrDefault().TotalCount : 0;
                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = count,
                        iTotalDisplayRecords = count,
                        aaData = data
                    }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadMKOMProjData(JQueryDataTableParamModel param, string Project)
        {
            try
            {
                if (Project != null && Project != "")
                {
                    var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                    var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                    string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                    int from = param.iDisplayStart + 1;
                    int to = param.iDisplayStart + param.iDisplayLength;
                    string user = objClsLoginInfo.UserName,
                        query = string.Empty,
                        orderBy = string.Empty,
                        sortDirection = Convert.ToString(Request["sSortDir_0"]);

                    query = string.Format("1=1 and kom1.Status in ('{0}','{1}','{2}','{3}','{4}','{5}')",
                        clsImplementationEnum.KOMStatus.MKOM.GetStringValue(),
                        clsImplementationEnum.KOMStatus.MKOMSentPMG.GetStringValue(),
                        clsImplementationEnum.KOMStatus.MKOMApprovePMG.GetStringValue(),
                        clsImplementationEnum.KOMStatus.MKOMApproveTOC.GetStringValue(),
                        clsImplementationEnum.KOMStatus.KOMCompleted.GetStringValue(),
                        clsImplementationEnum.KOMStatus.MKOMCompleted.GetStringValue());
                    query += " and Project='" + Project.ToString() + "'";

                    if (!string.IsNullOrWhiteSpace(param.sSearch))
                    {
                        query += string.Format(" AND (kom1.Project LIKE '%{0}%' OR kom1.Equipment LIKE '%{0}%' OR kom1.Customer LIKE '%{0}%')", param.sSearch);
                    }
                    else
                    {
                        query += Manager.MakeDatatableForSearch(param.SearchFilter);
                    }

                    if (!string.IsNullOrWhiteSpace(sortColumnName))
                    {
                        orderBy = " Order By " + sortColumnName + " " + sortDirection + " ";
                    }

                    var data = db.SP_KOM_GETHEADER(from, to, orderBy, query)
                                            .Select(x => new
                                            {
                                                x.Contract,
                                                x.Project,
                                                x.TotalCount,
                                                x.Customer,
                                                x.Equipment,
                                                ZeroDate = Convert.ToDateTime(x.ZeroDate).ToString("dd/MM/yyyy"),
                                                x.Status,
                                                x.HeaderId,
                                            }).ToList();
                    data = data.Where(x => x.Project.Contains(Project)).ToList();
                    Nullable<int> count = (data.Count > 0 && data.FirstOrDefault().TotalCount > 0) ? data.FirstOrDefault().TotalCount : 0;

                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = count,
                        iTotalDisplayRecords = count,
                        aaData = data
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                    var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                    string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                    int from = param.iDisplayStart + 1;
                    int to = param.iDisplayStart + param.iDisplayLength;
                    string user = objClsLoginInfo.UserName,
                        query = string.Empty,
                        orderBy = string.Empty,
                        sortDirection = Convert.ToString(Request["sSortDir_0"]);

                    query = string.Format("1=1 and kom1.Status in ('{0}','{1}','{2}','{3}','{4}','{5}')",
                        clsImplementationEnum.KOMStatus.MKOM.GetStringValue(),
                        clsImplementationEnum.KOMStatus.MKOMSentPMG.GetStringValue(),
                        clsImplementationEnum.KOMStatus.MKOMApprovePMG.GetStringValue(),
                        clsImplementationEnum.KOMStatus.MKOMApproveTOC.GetStringValue(),
                        clsImplementationEnum.KOMStatus.KOMCompleted.GetStringValue(),
                        clsImplementationEnum.KOMStatus.MKOMCompleted.GetStringValue());

                    if (!string.IsNullOrWhiteSpace(param.sSearch))
                    {
                        query += string.Format(" AND (kom1.Project LIKE '%{0}%' OR kom1.Equipment LIKE '%{0}%' OR kom1.Customer LIKE '%{0}%')", param.sSearch);
                    }
                    else
                    {
                        query += Manager.MakeDatatableForSearch(param.SearchFilter);
                    }

                    if (!string.IsNullOrWhiteSpace(sortColumnName))
                    {
                        orderBy = " Order By " + sortColumnName + " " + sortDirection + " ";
                    }

                    var data = db.SP_KOM_GETHEADER(from, to, orderBy, query)
                                            .Select(x => new
                                            {
                                                x.Contract,
                                                x.Project,
                                                x.TotalCount,
                                                x.Customer,
                                                x.Equipment,
                                                ZeroDate = Convert.ToDateTime(x.ZeroDate).ToString("dd/MM/yyyy"),
                                                x.Status,
                                                x.HeaderId,
                                            }).ToList();
                    Nullable<int> count = (data.Count > 0 && data.FirstOrDefault().TotalCount > 0) ? data.FirstOrDefault().TotalCount : 0;

                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = count,
                        iTotalDisplayRecords = count,
                        aaData = data
                    }, JsonRequestBehavior.AllowGet);
                }
                   
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ViewResult DisplayKOMProjLineDetails(int id = 0, string Stage = "",string Project = "")
        {
            KOM001 objKOM001 = new KOM001();
            KOM002 objKOM002 = new KOM002();
            var user = objClsLoginInfo.UserName;

            ViewBag.Project = new SelectList(db.COM001
                        .Select(x => new Projects
                        {
                            projectCode = x.t_cprj,
                            projectDescription = x.t_cprj + " - " + x.t_dsca
                        }).ToList(), "projectCode", "projectDescription");


            ViewBag.Customer = Manager.GetCustomerCodeAndNameByProject(db.KOM001
                                .Where(x => x.HeaderId == id)
                                .Select(x => x.Project)
                                .FirstOrDefault());

            List<SelectListItem> lstResult = db.SP_KOM_GETApproverName("", "1=1")
                .Select(x => new SelectListItem
                {
                    Text = x.Name.ToString(),
                    Value = x.Name.ToString()
                }).ToList();

            ViewBag.lstFuncationLeader = lstResult;
            ViewBag.lstBuddy = lstResult;

            if (id > 0)
            {
                objKOM001 = db.KOM001.FirstOrDefault(x => x.HeaderId == id);

                DateTime temp = Convert.ToDateTime(objKOM001.ZeroDate);
                objKOM001.KOMPlannedDate = temp.AddDays(21);

                ViewBag.LineStatus = objKOM001.Status;
                ViewBag.Heading = "Project Level Full Kit For KOM";
                ViewBag.MainHeading = "Kick of Meeting Detail";

                if (objKOM001.Status.Contains("MKOM") && Stage == "KOM")
                {
                    ViewBag.LineStatus = "KOM Completed";
                }

                if (ViewBag.LineStatus.Contains("MKOM"))
                {
                    ViewBag.Heading = "Project Level Full Kit For MKOM";
                    ViewBag.MainHeading = "Manufacturing Kick of Meeting";
                    objKOM001.KOMPlannedDate = temp.AddDays(135);
                }
                ViewBag.ZeroDate = Convert.ToDateTime(objKOM001.ZeroDate).ToString("dd/MM/yyyy");
            }
            ViewBag.chkProject = Project;
            return View(objKOM001);
        }

        [HttpPost]
        public JsonResult DisplayProjLineData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int from = param.iDisplayStart + 1;
                int to = param.iDisplayStart + param.iDisplayLength;
                int headerid = Convert.ToInt32(param.Headerid);
                string headerstatus = param.MTStatus;
                string query = string.Empty;

                if (headerstatus.Contains("MKOM"))
                {
                    if (!string.IsNullOrWhiteSpace(param.sSearch))
                    {
                        query = string.Format(" (Project like '%{0}%' OR Functions like '%{0}%' OR MKOMApprovedBy like '%{0}%' OR MKOMPMGComment like '%{0}%' OR FunctionalLead like '%{0}%' )", param.sSearch);
                    }
                    else
                    {
                        query = "1=1";
                    }

                    var data = db.SP_MKOM_GET_ProjLINEDETAILS(from, to, "", headerid, query)
                                .Select(x => new
                                {
                                    x.ROW_NO,
                                    x.TotalCount,
                                    x.Functions,
                                    x.Responsible,
                                    x.FunctionalLead,
                                    ApprovedBy = x.MKOMApprovedBy,
                                    ApprovedOn = Convert.ToString(x.MKOMApprovedOn),
                                    PMGComments = x.MKOMPMGComment,
                                    x.HeaderId,
                                    x.LineId,
                                    Link = HTMLActionStringForView(headerid, x.Functions, "MKOM", Convert.ToString(x.MKOMApprovedBy)),
                                }).ToList();
                    int? count = data.Count > 0 ? data.FirstOrDefault().TotalCount : 0;

                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = count,
                        iTotalDisplayRecords = count,
                        aaData = data
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(param.sSearch))
                    {
                        query = string.Format(" (Project like '%{0}%' OR Functions like '%{0}%' " +
                                            "OR ApprovedBy like '%{0}%' OR PMGComments like '%{0}%' " +
                                            "OR FunctionalLead like '%{0}%')", param.sSearch);
                    }
                    else
                    {
                        query = "1=1";
                    }

                    var data = db.SP_KOM_GET_ProjLINEDETAILS(from, to, "", headerid, query)
                        .Select(x => new
                        {
                            ROW_NO = Convert.ToString(x.ROW_NO),
                            TotalCount = x.TotalCount,
                            Functions = Convert.ToString(x.Functions),
                            Responsible = Convert.ToString(x.Responsible),
                            FunctionalLead = Convert.ToString(x.FunctionalLead),
                            ApprovedBy = Convert.ToString(x.ApprovedBy),
                            ApprovedOn = Convert.ToString(x.ApprovedOn),
                            PMGComments = Convert.ToString(x.PMGComments),
                            HeaderId = Convert.ToString(x.HeaderId),
                            LineId = Convert.ToString(x.LineId),
                            Link = HTMLActionStringForView(headerid, x.Functions, "KOM", Convert.ToString(x.ApprovedBy)),
                        }).ToList();

                    int? count = data.Count > 0 ? data.FirstOrDefault().TotalCount : 0;

                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = count,
                        iTotalDisplayRecords = count,
                        aaData = data
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }


        #endregion

        #region Copy
        public ActionResult GetContractList(string term)
        {
            var list = db.fn_GetContractAccessForEmployee(objClsLoginInfo.UserName, objClsLoginInfo.Location).ToList();

            if (!string.IsNullOrEmpty(term))
            {
                list = (from u in list where u.ContractNoName.Trim().ToLower().Contains(term.Trim().ToLower()) select u).ToList();
            }
            var list1 = (from u in list select new { Value = u.ContractNo, Text = u.ContractNoName }).OrderBy(u => u.Value).ToList();
            return Json(list1, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetProjectByContract(string term, string ContractNo)
        {
            var list = db.fn_GetContractProjectAccessForEmployee(objClsLoginInfo.UserName, objClsLoginInfo.Location).Where(x => x.ContractNo == ContractNo).ToList();

            if (!string.IsNullOrEmpty(term))
            {
                list = (from u in list where u.ProjectNoName.Trim().ToLower().Contains(term.Trim().ToLower()) select u).ToList();
            }
            var list1 = (from u in list select new { Value = u.ProjectNo, Text = u.ProjectNoName }).OrderBy(u => u.Value).ToList();
            return Json(list1, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult LoadCopyProjectPartial(string Project, string ProjectDesc)
        {
            ViewBag.Project = Project;
            ViewBag.ProjectDesc = ProjectDesc;

            var temp = ((from cm004 in db.COM004
                         join cm005 in db.COM005 on cm004.t_cono equals cm005.t_cono
                         where cm005.t_sprj == Project
                         select cm004).FirstOrDefault());

            ViewBag.Contract = temp.t_cono;
            ViewBag.ContractDesc = temp.t_cono + "-" + temp.t_desc;

            return PartialView("_CopyProjectPartial");
        }

        [HttpPost]
        public ActionResult CopyProject(FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int HeaderId = fc["HeaderId"] != "" ? Convert.ToInt32(fc["HeaderId"].ToString()) : 0;

                string ContractNo = fc["ContractNoCopy"].ToString().Trim();
                string SourceProject = fc["SourceProject"].ToString().Trim();
                string DestinationProject = fc["DestinationProject"].ToString().Trim();
                string DestinationProjectDesc = fc["txtDestinationProject"].ToString().Trim();
                string IProjectCopy = fc["IProjectCopy"].ToString().Trim();

                ResponceMsgWithStatus objResponse = CheckProjectInKOM(0, DestinationProject, IProjectCopy, true);
                if (!objResponse.Key)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = objResponse.Value;
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                //copy header                     
                KOM001 objSourceKOM001 = db.KOM001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                if (objSourceKOM001 != null)
                {
                    KOM001 objKOM001 = new KOM001();
                    objKOM001.Project = DestinationProject;
                    objKOM001.KOMPMG = objSourceKOM001.KOMPMG;
                    objKOM001.MKOMPMG = objSourceKOM001.MKOMPMG;
                    objKOM001.KOMTOCManager = objSourceKOM001.KOMTOCManager;
                    objKOM001.Status = "Draft";
                    objKOM001.Equipment = DestinationProjectDesc.Split('-')[1];

                    string customer = Manager.GetCustomerCodeAndNameByProject(DestinationProject);
                    if (!string.IsNullOrWhiteSpace(customer))
                        objKOM001.Customer = customer.Split('-')[0];

                    objKOM001.ZeroDate = DateTime.Now;
                    objKOM001.IProject = IProjectCopy;
                    objKOM001.ContractNo = ContractNo;

                    db.KOM001.Add(objKOM001);
                    db.SaveChanges();
                    int NewHeaderId = objKOM001.HeaderId;

                    //copy lines
                    List<KOM002> objKOM002List = new List<KOM002>();
                    var list = db.KOM002.Where(u => u.HeaderId == HeaderId).ToList();
                    list.ForEach(u =>
                    {
                        objKOM002List.Add(new KOM002
                        {
                            HeaderId = NewHeaderId,
                            Project = DestinationProject,
                            Functions = u.Functions,
                            FunctionalLead = u.FunctionalLead,
                            Buddy = u.Buddy,
                            ApprovedBy = u.ApprovedBy,
                            Status = (u.Functions == "MFG" || u.Functions == "PE") ? "KOM Approved" : "Draft",
                            PMGComments = (u.Functions == "MFG" || u.Functions == "PE") ? "OK" : "",
                            CreatedBy = objClsLoginInfo.UserName,
                            CreatedOn = DateTime.Now
                        });
                    });

                    if (objKOM002List.Count > 0)
                    {
                        db.KOM002.AddRange(objKOM002List);
                        db.SaveChanges();
                    }

                    //List<KOM003> objKOM003List = new List<KOM003>();
                    //var list1 = db.KOM003.Where(u => u.HeaderId == HeaderId).ToList();
                    //list1.ForEach(u =>
                    //{
                    //    objKOM003List.Add(new KOM003
                    //    {
                    //        HeaderId = NewHeaderId,
                    //        Project = DestinationProject,
                    //        Stage = u.Stage,
                    //        Functions = u.Functions,
                    //        Activity = u.Activity,
                    //        FullKitDocumentList = u.FullKitDocumentList,
                    //        Output = u.Output,
                    //        Checked = u.Checked,
                    //        CreatedBy = objClsLoginInfo.UserName,
                    //        CreatedDate = DateTime.Now
                    //    });
                    //});

                    //if (objKOM003List.Count > 0)
                    //{
                    //    db.KOM003.AddRange(objKOM003List);
                    //    db.SaveChanges();
                    //}

                    var oldFolderPath = "KOM001//" + HeaderId + "_KOM_INITIATOR";
                    var newfolderPath = "KOM001//" + NewHeaderId + "_KOM_INITIATOR";
                    Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                    //(new clsFileUpload()).CopyFolderContentsAsync(oldFolderPath, newfolderPath);
                    _objFUC.CopyDataOnFCSServerAsync(oldFolderPath, newfolderPath, oldFolderPath, HeaderId, newfolderPath, NewHeaderId, DESServices.CommonService.GetUseIPConfig, DESServices.CommonService.objClsLoginInfo.UserName, 0);

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.CopyRecord.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Source project does not exist";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        public ResponceMsgWithStatus CheckProjectInKOM(int HeaderId, string PrimaryProject, string IProjects, bool IsCopy)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            objResponseMsg.Key = true;

            try
            {
                var objKOMList = db.KOM001.ToList();
                var objPrimary = objKOMList.Where(x => x.Project.ToLower() == PrimaryProject.ToLower()).FirstOrDefault();
                if (objPrimary != null)
                {
                    if (objPrimary.HeaderId != HeaderId)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = (IsCopy ? "Destination" : "") + " Project already exist as KOM primary project";
                        return objResponseMsg;
                    }
                }
                else
                {
                    string query = "select Project from KOM001 WHERE (','+IProject+',' LIKE '%," + PrimaryProject + ",%')";
                    var strPrimaryProject = db.Database.SqlQuery<string>(query).FirstOrDefault();
                    if (!string.IsNullOrWhiteSpace(strPrimaryProject))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = string.Format((IsCopy ? "Destination" : "") + " Project already exist as identical project in KOM primary project : {0}", strPrimaryProject).Trim();
                        return objResponseMsg;
                    }
                }

                if (!string.IsNullOrWhiteSpace(IProjects))
                {
                    //1,2,3
                    string[] arrDestinationIdenticalProject = IProjects.Split(',');
                    foreach (var destproj in arrDestinationIdenticalProject)
                    {
                        var objPrimary1 = objKOMList.Where(x => x.Project.ToLower() == destproj.ToLower()).FirstOrDefault();
                        if (objPrimary1 != null)
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = string.Format((IsCopy ? "Destination" : "") + " Identical Project {0} already exist as KOM primary project", destproj).Trim();
                            return objResponseMsg;
                        }
                        else
                        {
                            string query = "select Project from KOM001 WHERE (','+IProject+',' LIKE '%," + destproj + ",%') and HeaderId != " + HeaderId;
                            var strPrimaryProject = db.Database.SqlQuery<string>(query).FirstOrDefault();
                            if (!string.IsNullOrWhiteSpace(strPrimaryProject))
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = string.Format((IsCopy ? "Destination" : "") + " Identical Project {0} already exist as identical project in KOM primary project : {1}", destproj, strPrimaryProject).Trim();
                                return objResponseMsg;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return objResponseMsg;
        }

        [HttpPost]
        public JsonResult ProjectsByContract(string term, string contract)
        {
            var list = new clsManager()
                        .GetProjectsByContract(contract, objClsLoginInfo.UserName, objClsLoginInfo.Location)
                        .Where(x => x.ProjectNoName.Contains(term))
                        .ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}
