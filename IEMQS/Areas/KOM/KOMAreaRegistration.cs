﻿using System.Web.Mvc;

namespace IEMQS.Areas.KOM
{
    public class KOMAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "KOM";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "KOM_default",
                "KOM/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}