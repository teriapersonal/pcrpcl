﻿using System.Web.Mvc;

namespace IEMQS.Areas.PQR
{
    public class PQRAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "PQR";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "PQR_default",
                "PQR/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}