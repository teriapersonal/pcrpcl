﻿using IEMQS.Areas.PDIN.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Collections;
using System.Web.Script.Serialization;
using System.Net;
using System.Net.Mail;
using IEMQS.Areas.Utility.Models;
using IEMQSImplementation.PQR;
using IEMQSImplementation.Models;
using Microsoft.Reporting.Map.WebForms.VirtualEarth;
using static IEMQSImplementation.clsHelper;
using IEMQS.Areas.TEMP.Controllers;
using System.IO;
using OfficeOpenXml;
using System.Data;
using System.Drawing;

namespace IEMQS.Areas.PQR.Controllers
{
    public class MaintainPQRController : clsBase
    {
        // GET: PQR/PQR
        PQREntitiesContext PQRDB = new PQREntitiesContext();

        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }

        [SessionExpireFilter]
        public ActionResult Maintain()
        {
            var objclsLoginInfo = (clsLoginInfo)System.Web.HttpContext.Current.Session[clsImplementationMessage.Session.LoginInfo];
            if (objclsLoginInfo != null)
            {
                if (objclsLoginInfo.listMenuResult != null)
                {
                    int Cnt = objclsLoginInfo.listMenuResult.Where(x => x.Process == "Maintain PQR").ToList().Count();
                    if (Cnt == 0)
                        return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                }
            }
            ViewBag.PQRRole = "Maintain";
            ViewBag.Title = "Maintain PQR";
            return View("Index");
        }
        [SessionExpireFilter]
        public ActionResult Approve()
        {
            var objclsLoginInfo = (clsLoginInfo)System.Web.HttpContext.Current.Session[clsImplementationMessage.Session.LoginInfo];
            if (objclsLoginInfo != null)
            {
                if (objclsLoginInfo.listMenuResult != null)
                {
                    int Cnt = objclsLoginInfo.listMenuResult.Where(x => x.Process == "Approve PQR").ToList().Count();
                    if (Cnt == 0)
                        return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                }
            }

            ViewBag.PQRRole = "Approve";
            ViewBag.Title = "Approve PQR";
            return View("Index");
        }
        [SessionExpireFilter]

        public PartialViewResult LoadIndexGridDataPartial(string Status, string PQRRole)
        {
            ViewBag.Status = Status;
            ViewBag.PQRRole = PQRRole;
            var role = (from a in db.ATH001
                        join b in db.ATH004 on a.Role equals b.Id
                        where a.Employee.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                        select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();

            //if (role.Where(i => i.RoleDesc.Equals("we3", StringComparison.OrdinalIgnoreCase)).ToList().Any())
            //{
            //    ViewBag.PQRRole = "Maintain";
            //}
            //else if (role.Where(i => i.RoleDesc.Equals("we2", StringComparison.OrdinalIgnoreCase)).ToList().Any() ||
            //    role.Where(i => i.RoleDesc.Equals("we1", StringComparison.OrdinalIgnoreCase)).ToList().Any())
            //{
            //    ViewBag.PQRRole = "Approve";
            //}
            return PartialView("_LoadIndexGridDataPartial");
        }

        public ActionResult GetIndexGridData(JQueryDataTableParamModel param, string Status, string PQRRole)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                string whereCondition = " 1=1 ";
                string[] columnName = { "PQRNo", "PQRFile", "JointypeDesc", "ParentMetal1Desc", "GrNo1", "ParentMetal2Desc", "GrNo2", "ThickNess", "WeldingProcessDesc", "Consumable", "PWHTTemp", "PWHTTime", "DesignCode", "TPIAgency", "StepCooling", "Remarks", "FinalFile", "BackUpFile", };
                ViewBag.PQRRole = PQRRole;

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    if (param.SearchFilter != null)
                    {
                        List<SearchFilterPQR> json_serializer = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SearchFilterPQR>>(param.SearchFilter);

                        foreach (var item in json_serializer)
                        {
                            if (item.ColumnName == "ParentMetal1")
                            {
                                string[] splitValue = item.Value.Split(new[] { "-" }, StringSplitOptions.None);
                                item.Value = splitValue[0].Trim();
                            }

                            if (item.ColumnName == "ParentMetal2")
                            {
                                string[] splitValue = item.Value.Split(new[] { "-" }, StringSplitOptions.None);
                                item.Value = splitValue[0].Trim();
                            }
                        }
                        var searchFilter = new JavaScriptSerializer().Serialize(json_serializer);

                        whereCondition += Manager.MakeDatatableForSearch(searchFilter);
                    }
                    //whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                else
                {
                    strSortOrder = (Status.ToUpper() == "ALL") ? " ORDER BY PQRNo ASC " : " ORDER BY EditedOn DESC ";
                }

                if (Status.ToUpper() == "PENDING")
                {
                    if (ViewBag.PQRRole == "Maintain")
                        whereCondition += " and Status = '" + clsImplementationEnum.PQRStatus.Draft.GetStringValue() + "'";
                    else
                        whereCondition += " and Status = '" + clsImplementationEnum.PQRStatus.SendToApproval.GetStringValue() + "'";
                }
                else if (Status.ToUpper() == "ALL")
                {
                    if (ViewBag.PQRRole != "Maintain")
                        whereCondition += " and Status in ('" + clsImplementationEnum.PQRStatus.SendToApproval.GetStringValue() + "', '" + clsImplementationEnum.PQRStatus.Approved.GetStringValue() + "')";
                }

                var lstLines = PQRDB.SP_PQR_GET_PQRDETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();
                var res = (from c in lstLines
                           select new[] {
                                        Convert.ToString(c.HeaderId),
                                        Convert.ToString(c.HeaderId),
                                        Convert.ToString(c.PQRNo),
                                       // c.Status == clsImplementationEnum.PQRStatus.Approved.GetStringValue() ? this.HTMLActionString(Convert.ToInt32(c.HeaderId),"PQRFile","PQR File","fa fa-paperclip","OpenPQRFileModal(" + c.HeaderId + ",1);",false , GetAttachment(Convert.ToInt32(c.HeaderId), 1, "False")) : "",
                                        c.Status == clsImplementationEnum.PQRStatus.Approved.GetStringValue() ? this.HTMLActionString(Convert.ToInt32(c.HeaderId),"PQRFile","PQR File","fa fa-paperclip","OpenPQRFileModal(" + c.HeaderId + ",1);",false , c.PQRFile == true ? "True" : "False") : "",
                                        Convert.ToString(c.JointypeDesc),
                                        Convert.ToString(c.ParentMetal1Desc),
                                        Convert.ToString(c.GrNo1),
                                        Convert.ToString(c.ParentMetal2Desc),
                                        Convert.ToString(c.GrNo2),
                                        Convert.ToString(c.ThickNess),
                                        Convert.ToString(c.WeldingProcess),
                                        Convert.ToString(c.Consumable),
                                        Convert.ToString(c.PWHTTemp),
                                        Convert.ToString(c.PWHTTime),
                                        Convert.ToString(c.StepCooling),
                                        Convert.ToString(c.DesignCodeDesc),
                                        Convert.ToString(c.TPIAgencyDesc),
                                        Convert.ToString(c.Remarks),
                                        Convert.ToString(c.Status),
                                        c.Status == clsImplementationEnum.PQRStatus.Approved.GetStringValue() ? this.HTMLActionString(Convert.ToInt32(c.HeaderId),"FinalFormat","Final Format","fa fa-paperclip","OpenPQRFileModal(" + c.HeaderId + ",2);",false , c.FinalFile == true ? "True" : "False") : "",
                                        c.Status == clsImplementationEnum.PQRStatus.Approved.GetStringValue() ? this.HTMLActionString(Convert.ToInt32(c.HeaderId),"BackupDoc","Backup Doc","fa fa-paperclip","OpenPQRFileModal(" + c.HeaderId + ",3);",false , c.BackUpFile == true ? "True" : "False") : "",
                                        Convert.ToString(c.CreatedBy),
                                        Convert.ToString(c.CreatedOn.ToString("dd/MM/yyyy")),
                                         "<nobr>" + Helper.GenerateActionIcon(Convert.ToInt32(c.HeaderId), "Edit", "Edit Record", "fa fa-pencil-square-o", "EnabledEditLine(" + c.HeaderId + ");","", Status.ToUpper() == "ALL" && c.Status != "Draft" && ViewBag.PQRRole == "Maintain" ? true : false,false, Status.ToUpper() == "ALL" && c.Status != "Draft" && ViewBag.PQRRole == "Maintain" ? "pointer-events:none;" : null) +
                                         Helper.GenerateActionIcon(Convert.ToInt32(c.HeaderId), "Update", "Update Record", "fa fa-floppy-o", "EditLine(" + c.HeaderId + ")","",false,false,"display:none") +
                                         Helper.GenerateActionIcon(Convert.ToInt32(c.HeaderId), "Cancel", "Cancel Edit", "fa fa-ban", "CancelEditLine(" + c.HeaderId + ")","",false,false,"display:none") +
                                         "<i title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PQR/MaintainPQR/ShowTimeline?HeaderId=" + Convert.ToInt32(c.HeaderId) + "')  class='iconspace fa fa-clock-o' ></i>"+
                                         Helper.GenerateActionIcon(Convert.ToInt32(c.HeaderId), "Delete", "Delete Record", "fa fa-trash-o", "DeleteLine(" + c.HeaderId + ")","", IsDeleteDisable(c.Status,ViewBag.PQRRole) ? true : false, false,  IsDeleteDisable(c.Status,ViewBag.PQRRole) ? "pointer-events:none;" : null)
                                        + "</nobr>"
                                        //Helper.GenerateActionIcon(Convert.ToInt32(c.HeaderId),"Edit","Edit Record","fa fa-pencil-square-o","EnabledEditLine(" + c.HeaderId + ");","", Status.ToUpper() == "ALL" && c.Status != "Draft" && ViewBag.PQRRole == "Maintain" ? true : false) + Helper.HTMLActionString(Convert.ToInt32(c.HeaderId),"Update","Update Record","fa fa-floppy-o","EditLine(" + c.HeaderId + ");","",false,"display:none") + " " + Helper.HTMLActionString(Convert.ToInt32(c.HeaderId),"Cancel","Cancel Edit","fa fa-ban","CancelEditLine(" + c.HeaderId + "); ","",false,"display:none") + " " + Helper.HTMLActionString(Convert.ToInt32(c.HeaderId),"Delete","Delete Record","fa fa-trash-o","DeleteLine("+ c.HeaderId +");"),
                          }).ToList();

                return Json(new
                {
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""

                }, JsonRequestBehavior.AllowGet);
            }
        }

        private bool IsDeleteDisable(string HeaderStatus, string Role)
        {
            bool result = true;
            if (HeaderStatus == clsImplementationEnum.PQRStatus.Draft.GetStringValue())
                result = false;
            if (HeaderStatus == clsImplementationEnum.PQRStatus.SendToApproval.GetStringValue() && Role == "Maintain")
                result = true;
            else if (HeaderStatus == clsImplementationEnum.PQRStatus.SendToApproval.GetStringValue() && Role == "Approve")
                result = false;
            else if (HeaderStatus == clsImplementationEnum.PQRStatus.Approved.GetStringValue())
                result = true;

            return result;
        }

        public JsonResult GetInlineEditableRow(int id, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();
                var result = new List<string>();
                List<DropdownModel> lstConsumabledata = GetConsumableddlist();
                List<DropdownModel> lstWPdata = GetWPddlist(78);

                List<DropdownModel> lstStepCooling = new List<DropdownModel>() {
                    new DropdownModel(){ Code = "NA",Description="NA"},
                    new DropdownModel(){ Code="Yes",Description="Yes"},
                    new DropdownModel(){Code="No",Description="No"}
                };

                if (id == 0)
                {
                    int newRecordId = 0;
                    var newRecord = new[] {
                                    clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                    Helper.GenerateHidden(newRecordId, "HeaderId", Convert.ToString(id)),
                                    "","",
                                    Helper.HTMLAutoComplete(newRecordId,"txtJointType","","",  false,"","JointType") +""+Helper.GenerateHidden(newRecordId, "JointType"),
                                    Helper.HTMLAutoComplete(newRecordId,"txtParentMetal1","","",  false,"","ParentMetal1") +""+Helper.GenerateHidden(newRecordId, "ParentMetal1"),
                                    //Helper.HTMLAutoComplete(newRecordId,"txtGrNo1","","",  false,"","GrNo1",false,"5") +""+Helper.GenerateHidden(newRecordId, "GrNo1", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtGrNo1","","",  false,"","GrNo1",false,"2","","","numeric") +""+Helper.GenerateHidden(newRecordId, "GrNo1", Convert.ToString(newRecordId)),
                                    //Helper.GenerateHTMLTextbox(newRecordId, "txtThickNess",  "", "",false, "", false,"5","","","numeric") +""+Helper.GenerateHidden(newRecordId, "GrNo1", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtParentMetal2","","",  false,"","ParentMetal2",false)+""+Helper.GenerateHidden(newRecordId, "ParentMetal2", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtGrNo2","","",  false,"","GrNo2",false,"2","","","numeric")+""+Helper.GenerateHidden(newRecordId, "GrNo2", Convert.ToString(newRecordId)),
                                    //Helper.HTMLAutoComplete(newRecordId,"txtGrNo2","","",  false,"","GrNo2",false,"5")+""+Helper.GenerateHidden(newRecordId, "GrNo2", Convert.ToString(newRecordId)),
                                    //Helper.GenerateHTMLTextbox(newRecordId, "txtGrNo1",  "", "",false, "", false,"5","","","numeric"),
                                    Helper.GenerateHTMLTextbox(newRecordId, "txtThickNess",  "", "",false, "", false,"5","","","numeric"),
                       //Helper.HTMLAutoComplete(newRecordId,"txtThickNess","","",  false,"","ThickNess",false)+""+Helper.GenerateHidden(newRecordId, "ThickNess", Convert.ToString(newRecordId)),
                                    MultiSelectDropdown(newRecordId,lstWPdata,"", false, "","txtWeldingProcess"),
                                    MultiSelectDropdown(newRecordId,lstConsumabledata,"", false, "","txtConsumable"),                      
                                    //Helper.HTMLAutoComplete(newRecordId,"txtPWHTTemp","","", false,"","PWHTTemp",false,"10")+""+Helper.GenerateHidden(newRecordId, "PWHTTemp", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtPWHTTemp","","", false,"","PWHTTemp",false,"50")+""+Helper.GenerateHidden(newRecordId, "PWHTTemp", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtPWHTTime","","",  false,"","PWHTTime",false,"20")+""+Helper.GenerateHidden(newRecordId, "PWHTTime", Convert.ToString(newRecordId)),
                                    Helper.GenerateDropdown(newRecordId,"ddlStepCooling",new SelectList(lstStepCooling,"Code","Description"),"","",false,""),
                                    Helper.HTMLAutoComplete(newRecordId,"txtDesignCode","","", false,"","DesignCode",false)+""+Helper.GenerateHidden(newRecordId, "DesignCode", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtTPIAgency","","",  false,"","TPIAgency",false)+""+Helper.GenerateHidden(newRecordId, "TPIAgency", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtRemarks","","", false,"","Remarks",false,"100")+""+Helper.GenerateHidden(newRecordId, "Remarks", Convert.ToString(newRecordId)),
                                    "",
                                    "",
                                    "","","",
                                     Helper.HTMLActionString(newRecordId,"Add","Add New Record","fa fa-plus","SaveNewRecord();"),
                    };
                    data.Add(newRecord);
                }
                else
                {
                    var lstLines = PQRDB.SP_PQR_GET_PQRDETAILS(1, 0, "", "HeaderId = " + id).Take(1).ToList();

                    data = (from c in lstLines
                            select new[] {
                                    clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//Convert.ToString(c.ROW_NO),
                                    Helper.GenerateHidden(Convert.ToInt32(c.HeaderId), "HeaderId", Convert.ToString(id)),
                                    Helper.GenerateHidden(Convert.ToInt32(c.HeaderId), "PQRNo", Convert.ToString(id)),
                                    c.Status == clsImplementationEnum.PQRStatus.Approved.GetStringValue() ? this.HTMLActionString(Convert.ToInt32(c.HeaderId),"PQRFile","PQR File","fa fa-paperclip","OpenPQRFileModal(" + c.HeaderId + ",1);",false , c.PQRFile == true ? "True" : "False") : "",
                                    isReadOnly ? c.JointType : Helper.HTMLAutoComplete(Convert.ToInt32(c.HeaderId), "txtJointType", c.JointypeDesc,"", false,"","JointType",false)+""+Helper.GenerateHidden(Convert.ToInt32(c.HeaderId), "JointType",Convert.ToString(c.JointType)),
                                    isReadOnly ? c.ParentMetal1 : Helper.HTMLAutoComplete(Convert.ToInt32(c.HeaderId), "txtParentMetal1", c.ParentMetal1Desc,"", false,"","ParentMetal1",false)+""+Helper.GenerateHidden(Convert.ToInt32(c.HeaderId), "ParentMetal1",Convert.ToString(c.ParentMetal1)),
                                    isReadOnly ? c.GrNo1 : Helper.HTMLAutoComplete(Convert.ToInt32(c.HeaderId), "txtGrNo1",WebUtility.HtmlEncode(c.GrNo1),"", false,"","GrNo1",false,"2")+""+Helper.GenerateHidden(Convert.ToInt32(c.HeaderId), "GrNo1",Convert.ToString(c.GrNo1)),
                                    isReadOnly ? c.ParentMetal2 : Helper.HTMLAutoComplete(Convert.ToInt32(c.HeaderId), "txtParentMetal2",c.ParentMetal2Desc,"", false,"","ParentMetal2",false)+""+Helper.GenerateHidden(Convert.ToInt32(c.HeaderId), "ParentMetal2",Convert.ToString(c.ParentMetal2)),
                                    isReadOnly ? c.GrNo2 : Helper.HTMLAutoComplete(Convert.ToInt32(c.HeaderId), "txtGrNo2",c.GrNo2,"", false,"","GrNo2",false,"2")+""+Helper.GenerateHidden(Convert.ToInt32(c.HeaderId), "GrNo2",Convert.ToString(c.GrNo2)),
                                    isReadOnly ? Convert.ToString(c.ThickNess) : Helper.HTMLAutoComplete(Convert.ToInt32(c.HeaderId), "txtThickNess", Convert.ToString(c.ThickNess),"", false,"","ThickNess",false,"5")+""+Helper.GenerateHidden(Convert.ToInt32(c.HeaderId), "ThickNess",Convert.ToString(c.ThickNess)),
                                    isReadOnly ? c.WeldingProcess : MultiSelectDropdown(Convert.ToInt32(c.HeaderId),lstWPdata, c.WeldingProcess, false, "","txtWeldingProcess"),
                                    isReadOnly ? c.Consumable : MultiSelectDropdown(Convert.ToInt32(c.HeaderId),lstConsumabledata, c.Consumable, false, "","txtConsumable"),
                                    isReadOnly ? Convert.ToString(c.PWHTTemp) : Helper.HTMLAutoComplete(Convert.ToInt32(c.HeaderId), "txtPWHTTemp", Convert.ToString(c.PWHTTemp),"", false,"","PWHTTemp",false,"50")+""+Helper.GenerateHidden(Convert.ToInt32(c.HeaderId), "PWHTTemp",Convert.ToString(c.PWHTTemp)),
                                    isReadOnly ? Convert.ToString(c.PWHTTime) : Helper.HTMLAutoComplete(Convert.ToInt32(c.HeaderId), "txtPWHTTime", Convert.ToString(c.PWHTTime),"", false,"","PWHTTime",false,"20")+""+Helper.GenerateHidden(Convert.ToInt32(c.HeaderId), "PWHTTime",Convert.ToString(c.PWHTTime)),
                                    isReadOnly ? c.StepCooling : Helper.GenerateDropdown(Convert.ToInt32(c.HeaderId),"ddlStepCooling",new SelectList(lstStepCooling,"Code","Description",c.StepCooling),"","",false,""),
                                    isReadOnly ? c.DesignCode : Helper.HTMLAutoComplete(Convert.ToInt32(c.HeaderId), "txtDesignCode",c.DesignCodeDesc,"", false,"","DesignCode",false)+""+Helper.GenerateHidden(Convert.ToInt32(c.HeaderId), "DesignCode",Convert.ToString(c.DesignCode)),
                                    isReadOnly ? c.TPIAgency : Helper.HTMLAutoComplete(Convert.ToInt32(c.HeaderId), "txtTPIAgency",c.TPIAgencyDesc,"", false,"","TPIAgency",false)+""+Helper.GenerateHidden(Convert.ToInt32(c.HeaderId), "TPIAgency",Convert.ToString(c.TPIAgency)),
                                    isReadOnly ? c.Remarks : Helper.HTMLAutoComplete(Convert.ToInt32(c.HeaderId), "txtRemarks",WebUtility.HtmlEncode(c.Remarks),"", false,"","Remarks",false,"100")+""+Helper.GenerateHidden(Convert.ToInt32(c.HeaderId), "Remarks",Convert.ToString(c.Remarks)),
                                    Helper.GenerateHidden(Convert.ToInt32(c.HeaderId), "Status", Convert.ToString(id)),
                                    c.Status == clsImplementationEnum.PQRStatus.Approved.GetStringValue() ?this.HTMLActionString(Convert.ToInt32(c.HeaderId),"FinalFormat","Final Format","fa fa-paperclip","OpenPQRFileModal(" + c.HeaderId + ",2);",false , c.FinalFile == true ? "True" : "False") : "",
                                    c.Status == clsImplementationEnum.PQRStatus.Approved.GetStringValue() ?this.HTMLActionString(Convert.ToInt32(c.HeaderId),"BackupDoc","Backup Doc","fa fa-paperclip","OpenPQRFileModal(" + c.HeaderId + ",3);",false , c.BackUpFile == true ? "True" : "False") : "",
                                    c.CreatedBy,
                                    Convert.ToDateTime(c.CreatedOn).ToString("dd/MM/yyyy"),
                                    Helper.HTMLActionString(Convert.ToInt32(c.HeaderId),"Edit","Edit Record","fa fa-pencil-square-o","EnabledEditLine(" + c.HeaderId + ");") +
                                    Helper.HTMLActionString(Convert.ToInt32(c.HeaderId),"Update","Update Record","fa fa-floppy-o","EditLine(" + c.HeaderId + ");","",false,"display:none") + " " +
                                    Helper.HTMLActionString(Convert.ToInt32(c.HeaderId),"Cancel","Cancel Edit","fa fa-ban","CancelEditLine(" + c.HeaderId + "); ","",false,"display:none") + " " +
                                    Helper.HTMLActionString(Convert.ToInt32(c.HeaderId),"Delete","Delete Record","fa fa-trash-o","DeleteLine("+ c.HeaderId +");"),
                                }).ToList();
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [NonAction]
        public static string MultiSelectDropdown(int rowId, List<DropdownModel> list, string selectedValue, bool Disabled = false, string OnChangeEvent = "", string ColumnName = "")
        {
            string multipleSelect = string.Empty;
            string[] arrayVal = { };

            try
            {
                if (!string.IsNullOrWhiteSpace(selectedValue))
                {
                    arrayVal = selectedValue.Split(',');
                }

                string inputID = ColumnName + "" + rowId.ToString();
                multipleSelect += "<select name=\"" + inputID + "\" id=\"" + inputID + "\" multiple=\"multiple\"  style=\"width: 100 % \" colname=\"" + ColumnName + "\" class=\"form-control\" " + (Disabled ? "disabled" : "") + (OnChangeEvent != string.Empty ? " onchange=\"" + OnChangeEvent + "\"" : "") + " oldvalue=\"" + selectedValue + "\" >";
                foreach (var item in list)
                {
                    multipleSelect += "<option value=\"" + item.Code + "\" " + ((arrayVal.Contains(item.Code)) ? " selected" : "") + " >" + item.Description + "</option>";
                }
                foreach (var item in arrayVal)
                {
                    if (!list.Any(i => i.Code == item))
                    {
                        multipleSelect += "<option value=\"" + item + "\" selected>" + item + "</option>";
                    }
                }
                multipleSelect += "</select>";
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return multipleSelect;
        }

        public string HTMLActionString(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", bool isDisable = false, string IsAtteched = "False")
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (isDisable)
            {
                if (IsAtteched == "False")
                {
                    htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;pointer-events:none; opacity:0.3;margin-left:5px;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";
                }
                else
                {
                    htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;pointer-events:none; opacity:0.3;margin-left:5px;color:#32CD32' Title='" + buttonTooltip + "' class='" + className + "' ></i>";
                }
            }
            else
            {
                if (IsAtteched == "False")
                {
                    htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;margin-left:5px;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }
                else
                {
                    htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;margin-left:5px;color:#32CD32' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }

            }
            return htmlControl;
        }


        public string GetAttachment(int headerId, int folderId, string attachment)
        {
            var vCount = db.FCS001.Where(w => w.TableId == headerId && w.TableName == "PQR001" && w.ViewerId == folderId).Select(s => s.Document_name);
            if (vCount.Count() > 0)
                return "True";
            else
                return "False";
        }

        public ActionResult SaveData(FormCollection fc, int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (fc != null)
                {
                    string JointType = fc["txtJointType" + Id];
                    JointType = JointType.Split('-')[0].Trim();
                    string ParentMetal1 = fc["txtParentMetal1" + Id];
                    ParentMetal1 = ParentMetal1.Split('-')[0].Trim();
                    string GrNo1 = fc["txtGrNo1" + Id];
                    string ParentMetal2 = fc["txtParentMetal2" + Id];
                    ParentMetal2 = ParentMetal2.Split('-')[0].Trim();
                    string GrNo2 = fc["txtGrNo2" + Id];
                    string ThickNess = fc["txtThickNess" + Id];
                    string WeldingProcess = fc["txtWeldingProcess" + Id];
                    string WeldingProcessNew = fc["txtWeldingProcessNew" + Id];
                    string Consumable = fc["txtConsumable" + Id];
                    string ConsumableNew = fc["txtConsumableNew" + Id];
                    string PWHTTemp = fc["txtPWHTTemp" + Id];
                    string PWHTTime = fc["txtPWHTTime" + Id];
                    string DesignCode = fc["txtDesignCode" + Id];
                    DesignCode = DesignCode.Split('-')[0].Trim();
                    string TPIAgency = fc["txtTPIAgency" + Id];
                    TPIAgency = TPIAgency.Split('-')[0].Trim();
                    string stepCooling = fc["ddlStepCooling" + Id];
                    string Remarks = fc["txtRemarks" + Id];
                    double? thick = null;
                    //string[] data = Consumable.Split(',').ToArray();
                    //string strwp = string.Empty;
                    //foreach (var item in data) 
                    //{
                    //    strwp = strwp + '+' + item;
                    //}
                    //strwp.Substring(0, strwp.Length - 1);
                    //strwp = strwp.Slice(0, -1);
                    if (ThickNess != "")
                        thick = Convert.ToDouble(ThickNess);

                    PQR001 objPQR001 = null;

                    if (Id > 0)
                    {
                        objPQR001 = PQRDB.PQR001.Where(x => x.HeaderId == Id).FirstOrDefault();
                        if (objPQR001 != null)
                        {
                            objPQR001.JointType = JointType;
                            objPQR001.ParentMetal1 = ParentMetal1;
                            objPQR001.ParentMetal2 = ParentMetal2;
                            objPQR001.WeldingProcess = WeldingProcessNew;
                            objPQR001.GrNo1 = GrNo1;
                            objPQR001.GrNo2 = GrNo2;
                            objPQR001.Consumable = ConsumableNew;
                            objPQR001.DesignCode = DesignCode;
                            objPQR001.TPIAgency = TPIAgency;
                            objPQR001.PWHTTemp = PWHTTemp;
                            objPQR001.PWHTTime = PWHTTime;
                            objPQR001.ThickNess = thick;
                            objPQR001.StepCooling = stepCooling;
                            objPQR001.Remarks = Remarks;
                            objPQR001.EditedBy = objClsLoginInfo.UserName;
                            objPQR001.EditedOn = DateTime.Now;

                            PQRDB.SaveChanges();

                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
                        }

                    }
                    else
                    {
                        objPQR001 = new PQR001();
                        objPQR001.HeaderId = Convert.ToInt32(8);
                        objPQR001.JointType = JointType;
                        objPQR001.ParentMetal1 = ParentMetal1;
                        objPQR001.ParentMetal2 = ParentMetal2;
                        objPQR001.WeldingProcess = WeldingProcessNew;
                        objPQR001.GrNo1 = GrNo1;
                        objPQR001.GrNo2 = GrNo2;
                        objPQR001.Consumable = ConsumableNew;
                        //objPQR001.Consumable = Consumable;
                        objPQR001.DesignCode = DesignCode;
                        objPQR001.TPIAgency = TPIAgency;
                        objPQR001.PWHTTemp = PWHTTemp;
                        objPQR001.PWHTTime = PWHTTime;
                        objPQR001.ThickNess = thick;
                        objPQR001.StepCooling = stepCooling;
                        objPQR001.Remarks = Remarks;
                        objPQR001.Status = "Draft";
                        objPQR001.PQRFile = false;
                        objPQR001.FinalFile = false;
                        objPQR001.BackUpFile = false;
                        objPQR001.Location = objClsLoginInfo.Location;
                        objPQR001.CreatedBy = objClsLoginInfo.UserName;
                        objPQR001.CreatedOn = DateTime.Now;

                        PQRDB.PQR001.Add(objPQR001);

                        PQRDB.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SubmitPQR(string strHeader)
        {
            var objResponseMsg = new ResponceMsgWithObject();
            try
            {
                if (!string.IsNullOrEmpty(strHeader))
                {
                    List<string> submittedPQR = new List<string>();
                    List<string> pendingPQR = new List<string>();
                    int[] headerIds = Array.ConvertAll(strHeader.Split(','), s => int.Parse(s));
                    foreach (int headerId in headerIds)
                    {
                        var PQR001 = PQRDB.PQR001.FirstOrDefault(x => x.HeaderId == headerId);
                        if (SendForApproval(headerId))
                            submittedPQR.Add(PQR001.JointType);  //Temporary code
                        else
                            pendingPQR.Add(PQR001.JointType);
                    }
                    objResponseMsg.data = new
                    {
                        submittedPQR = (submittedPQR.Count > 0 ? objResponseMsg.data = "PQR " + String.Join(",", submittedPQR) + " submitted successfully" : ""),
                        pendingPQR = (pendingPQR.Count > 0 ? objResponseMsg.data = "Fail to Submit PQR:" + String.Join(",", pendingPQR) + "." : "")
                    };
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "PQR has been sucessfully submitted for approval";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please select atleast one record sending for approval";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApprovePQR(string strHeader)
        {
            var objResponseMsg = new ResponceMsgWithObject();
            try
            {
                if (!string.IsNullOrEmpty(strHeader))
                {
                    List<string> approvedPQR = new List<string>();
                    List<string> pendingPQR = new List<string>();
                    int[] headerIds = Array.ConvertAll(strHeader.Split(','), s => int.Parse(s));
                    foreach (int headerId in headerIds)
                    {
                        var PQR001 = PQRDB.PQR001.FirstOrDefault(x => x.HeaderId == headerId);
                        if (ApproveByUser(headerId))
                            approvedPQR.Add(PQR001.JointType);
                        else
                            pendingPQR.Add(PQR001.JointType);
                    }
                    objResponseMsg.data = new
                    {
                        approvedPQR = (approvedPQR.Count > 0 ? objResponseMsg.data = "PQR " + String.Join(",", approvedPQR) + " Approved successfully" : ""),
                        pendingPQR = (pendingPQR.Count > 0 ? objResponseMsg.data = "Fail to Approve PQR:" + String.Join(",", pendingPQR) + "." : "")
                    };
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "PQR has been sucessfully approved.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please select atleast one record for approve";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        private bool ApproveByUser(int headerId)
        {
            var lineStatus = clsImplementationEnum.PQRStatus.SendToApproval.GetStringValue();
            var objPQR001 = PQRDB.PQR001.FirstOrDefault(i => i.HeaderId == headerId && i.Status == lineStatus);
            if (objPQR001 != null)
            {
                objPQR001.Status = clsImplementationEnum.PQRStatus.Approved.GetStringValue();
                objPQR001.ApprovedBy = objClsLoginInfo.UserName;
                objPQR001.ApprovedOn = DateTime.Now;

                long lastpqrno = 0;
                var temp = PQRDB.PQR001.Where(x => x.Status == "Approved").OrderByDescending(y => y.PQRNo).FirstOrDefault();
                if (temp != null)
                    lastpqrno = Convert.ToInt64(temp.PQRNo) + 1;
                else
                    lastpqrno = 1;

                objPQR001.PQRNo = lastpqrno;

                PQRDB.SaveChanges();

                #region Send Notification
                //(new clsManager()).SendNotificationByUserPSNumber(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(),objPBP002.SubmittedBy, "PBP: " + objPBP002.Procedure + " is approved", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/PBP/MaintainPBP/Index?type="+objPBP002.Product);
                #endregion

                return true;
            }
            return false;
        }

        private bool SendForApproval(int headerId)
        {
            var objPQR = PQRDB.PQR001.FirstOrDefault(i => i.HeaderId == headerId);
            var lineStatus = new string[] { clsImplementationEnum.PQRStatus.Draft.GetStringValue() };
            if (objPQR != null && lineStatus.Contains(objPQR.Status))
            {
                objPQR.Status = clsImplementationEnum.PQRStatus.SendToApproval.GetStringValue();
                objPQR.SubmittedBy = objClsLoginInfo.UserName;
                objPQR.SubmittedOn = DateTime.Now;

                PQRDB.SaveChanges();

                #region Send Notification
                clsManager objManager = new clsManager();
                string[] psnolist = objManager.getEmployeeFromRole(clsImplementationEnum.UserRoleName.WE2.GetStringValue(), objClsLoginInfo.Location).Select(x => x.psnum).ToArray();
                string[] psnolist1 = objManager.getEmployeeFromRole(clsImplementationEnum.UserRoleName.WE1.GetStringValue(), objClsLoginInfo.Location).Select(x => x.psnum).ToArray();
                string strPSNoList = string.Join(",", psnolist);
                string strPSNoList1 = string.Join(",", psnolist1);

                if (!string.IsNullOrEmpty(strPSNoList1))
                    strPSNoList = strPSNoList + "," + strPSNoList1;

                (new clsManager()).SendNotification(null, "", "", "", "PQR has been submitted, please review and approve.", clsImplementationEnum.NotificationType.Information.GetStringValue(), "", strPSNoList);

                #endregion

                return true;
            }
            return false;
        }
        [HttpPost]
        public ActionResult DeleteData(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PQR001 objPQR001 = PQRDB.PQR001.Where(x => x.HeaderId == Id).FirstOrDefault();
                if (objPQR001 != null)
                {
                    PQRDB.PQR001.Remove(objPQR001);
                    PQRDB.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Details not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateFileInfo(int Id, int ViewerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            int FileCnt = 0;
            try
            {
                var vCount = db.FCS001.Where(w => w.TableId == Id && w.TableName == "PQR001" && w.ViewerId == ViewerId).Select(s => s.Document_name);
                bool fileexist = false;
                FileCnt = vCount.Count();
                if (vCount.Count() > 0)
                    fileexist = true;
                else
                    fileexist = false;
                var objPQR = PQRDB.PQR001.FirstOrDefault(i => i.HeaderId == Id);
                if (objPQR != null)
                {
                    if (ViewerId == 1)
                        objPQR.PQRFile = fileexist;
                    if (ViewerId == 2)
                        objPQR.FinalFile = fileexist;
                    if (ViewerId == 3)
                        objPQR.BackUpFile = fileexist;
                    PQRDB.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.DataUpdate.ToString();
                }
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            // return Json(objResponseMsg, FileCnt = vCount.Count(), JsonRequestBehavior.AllowGet);
            return Json(new { Result = "OK", Data = objResponseMsg, FileCnt = FileCnt }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ShowTimeline(int HeaderId)
        {
            TimelineViewModel model = new TimelineViewModel();

            if (HeaderId > 0)
            {
                PQR001 objPQR001 = PQRDB.PQR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                model.CreatedBy = Manager.GetUserNameFromPsNo(objPQR001.CreatedBy);
                model.CreatedOn = objPQR001.CreatedOn;

                model.EditedBy = Manager.GetUserNameFromPsNo(objPQR001.EditedBy);
                model.EditedOn = objPQR001.EditedOn;

                model.SubmittedBy = Manager.GetUserNameFromPsNo(objPQR001.SubmittedBy);
                model.SubmittedOn = objPQR001.SubmittedOn;

                model.ApprovedBy = Manager.GetUserNameFromPsNo(objPQR001.ApprovedBy);
                model.ApprovedOn = objPQR001.ApprovedOn;

            }
            else
            {
                PQR001 objPQR001 = PQRDB.PQR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objPQR001.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPQR001.CreatedBy) : null;
                model.CreatedOn = objPQR001.CreatedOn;
                model.EditedBy = objPQR001.EditedBy != null ? Manager.GetUserNameFromPsNo(objPQR001.EditedBy) : null;
                model.EditedOn = objPQR001.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress2.cshtml", model);
        }

        #region DropDown Data
        [HttpPost]
        public ActionResult GetDropDataResult(int Category, string term)
        {
            var lstLength = new List<Projects>();

            if (!string.IsNullOrWhiteSpace(term))
            {
                lstLength = db.Database.SqlQuery<Projects>("Select distinct Code as Value, Description as Text from [dbo].[GLB002] Where Code like '%" + term + "%' and Category = " + Category + " and Location = '" + objClsLoginInfo.Location + "' and IsActive = 1 order by Code").ToList();
            }
            else
            {
                string _qry = "Select distinct Code as Value, Description as Text from [dbo].[GLB002] Where  Category = " + Category + " and Location = '" + objClsLoginInfo.Location + "' and IsActive = 1 order by Code";
                lstLength = db.Database.SqlQuery<Projects>(_qry).ToList();
            }

            return Json(lstLength, JsonRequestBehavior.AllowGet);
        }


        private List<DropdownModel> GetConsumableddlist()
        {
            var lstLength = new List<DropdownModel>();
            lstLength = db.Database.SqlQuery<DropdownModel>("Select distinct AWSClass as Code,AWSClass as Description from [dbo].[WPS002] Where Location = '" + objClsLoginInfo.Location + "' order by AWSClass").ToList();
            return lstLength;
        }

        private List<DropdownModel> GetWPddlist(int Category)
        {
            var lstLength = new List<DropdownModel>();
            lstLength = db.Database.SqlQuery<DropdownModel>("Select distinct Code as Code , Description as Description from [dbo].[GLB002] Where Category = " + Category + " and Location = '" + objClsLoginInfo.Location + "' order by Code").ToList();
            return lstLength;
        }

        #endregion

        #region Export/Import Code
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string m = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            clsHelper.ResponceMsgWithFileName objexport = new clsHelper.ResponceMsgWithFileName();
            try
            {
                string strFileName = string.Empty;
                var lst = PQRDB.SP_PQR_GET_PQRDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                objexport = GeneratePQRExcel(lst, m);
                strFileName = objexport.FileName;

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        public clsHelper.ResponceMsgWithFileName GeneratePQRExcel(List<SP_PQR_GET_PQRDETAILS_Result> lst, string DownLoadTemplate)
        {
            clsHelper.ResponceMsgWithFileName objResponseMsg = new clsHelper.ResponceMsgWithFileName();
            try
            {
                string name = DownLoadTemplate == "y" ? "Import_Template" : objClsLoginInfo.UserName;
                MemoryStream ms = new MemoryStream();
                string path = "~/Resources/PQR/PQR Export Template.xlsx";
                //if (lst.Count == 0 && DownLoadTemplate == "y")
                //{
                //    path = "~/Resources/PQR/PQR Template.xlsx";
                //}

                using (FileStream fs = System.IO.File.OpenRead(Server.MapPath(path)))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();
                        int i = 2;
                        if (lst.Count > 0)
                        {
                            foreach (var item in lst)
                            {
                                excelWorksheet.Cells[i, 1].Value = item.JointType;
                                excelWorksheet.Cells[i, 2].Value = item.ParentMetal1;
                                excelWorksheet.Cells[i, 3].Value = item.GrNo1;
                                excelWorksheet.Cells[i, 4].Value = item.ParentMetal2;
                                excelWorksheet.Cells[i, 5].Value = item.GrNo2;
                                excelWorksheet.Cells[i, 6].Value = item.ThickNess;
                                excelWorksheet.Cells[i, 7].Value = item.WeldingProcess;
                                excelWorksheet.Cells[i, 8].Value = item.Consumable;
                                excelWorksheet.Cells[i, 9].Value = item.PWHTTemp;
                                excelWorksheet.Cells[i, 10].Value = item.PWHTTime;
                                excelWorksheet.Cells[i, 11].Value = item.DesignCode;
                                excelWorksheet.Cells[i, 12].Value = item.TPIAgency;
                                excelWorksheet.Cells[i, 13].Value = item.StepCooling;
                                excelWorksheet.Cells[i, 14].Value = item.Remarks;
                                i++;
                            }
                        }
                        excelPackage.SaveAs(ms);
                    }
                }

                ms.Position = 0;

                string fileName;
                string excelFilePath = Helper.ExcelFilePath(name, out fileName);

                Byte[] bin = ms.ToArray();
                System.IO.File.WriteAllBytes(excelFilePath, bin);

                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error";
                objResponseMsg.FileName = fileName;
                return objResponseMsg;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }

        [SessionExpireFilter]
        public ActionResult ImportExcelPQR(HttpPostedFileBase upload)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            if (upload != null)
            {
                if (Path.GetExtension(upload.FileName).ToLower() == ".xlsx" || Path.GetExtension(upload.FileName).ToLower() == ".xls")
                {
                    ExcelPackage package = new ExcelPackage(upload.InputStream);
                    ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();

                    DataTable dtHeaderExcel = new DataTable();
                    //List<CategoryData> errors = new List<CategoryData>();
                    bool isError;

                    //ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();
                    //foreach (var firstRowCell in excelWorksheet.Cells[1, 1, 1, excelWorksheet.Dimension.End.Column])
                    if (excelWorksheet.Dimension.End.Column != 14)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Excel is not as per template, please check columns and try again!";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    DataSet ds = PQRtoCheckValidation(package, out isError);
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Record Found!";
                    }
                    else if (!isError)
                    {
                        try
                        {
                            foreach (DataRow item in ds.Tables[0].Rows)
                            {
                                string Joint = item.Field<string>("JOINT");
                                string PNo1 = item.Field<string>("P.No.1");
                                string GNo1 = item.Field<string>("Gr.No.1");
                                string PNo2 = item.Field<string>("P.No.2");
                                string GNo2 = item.Field<string>("Gr.No.2");
                                string ThickNess = item.Field<string>("ThickNess");
                                string WP = item.Field<string>("WELDING PROCESS");
                                string Consumable = item.Field<string>("Consumable");
                                string PWHTTemp = item.Field<string>("PWHTTemp");
                                string PWHTTime = item.Field<string>("PWHTTime");
                                string Code = item.Field<string>("Code");
                                string TPI = item.Field<string>("TPI");
                                string StepCooling = item.Field<string>("StepCooling");
                                string Remarks = item.Field<string>("REMARKS");
                                PQR001 objPQR001 = new PQR001();
                                objPQR001.Location = objClsLoginInfo.Location;
                                objPQR001.JointType = Joint;
                                objPQR001.ParentMetal1 = PNo1;
                                objPQR001.GrNo1 = GNo1;
                                objPQR001.ParentMetal2 = PNo2;
                                objPQR001.GrNo2 = GNo2;
                                objPQR001.WeldingProcess = WP;
                                objPQR001.Consumable = Consumable;
                                objPQR001.DesignCode = Code;
                                objPQR001.TPIAgency = TPI;
                                objPQR001.ThickNess = Convert.ToDouble(ThickNess);
                                objPQR001.PWHTTemp = PWHTTemp;
                                objPQR001.PWHTTime = PWHTTime;
                                objPQR001.Remarks = Remarks;
                                objPQR001.StepCooling = StepCooling;
                                objPQR001.Status = "Draft";
                                objPQR001.CreatedBy = objClsLoginInfo.UserName;
                                objPQR001.CreatedOn = DateTime.Now;
                                PQRDB.PQR001.Add(objPQR001);
                            }
                            PQRDB.SaveChanges();

                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "PQR added successfully";
                        }
                        catch (Exception ex)
                        {
                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = ex.Message;
                        }
                    }
                    else
                    {
                        objResponseMsg = ErrorPQRExportToExcel(ds);
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Excel file is not valid";
                }
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please enter valid Project no.";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public DataSet PQRtoCheckValidation(ExcelPackage package, out bool isError)
        {
            ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();
            DataTable dtHeaderExcel = new DataTable();
            isError = false;
            int notesIndex = 0;
            try
            {
                #region Read Lines data from excel 
                foreach (var firstRowCell in excelWorksheet.Cells[1, 1, 1, excelWorksheet.Dimension.End.Column])
                {
                    dtHeaderExcel.Columns.Add(firstRowCell.Text);
                }

                for (var rowNumber = 2; rowNumber <= excelWorksheet.Dimension.End.Row; rowNumber++)
                {
                    var row = excelWorksheet.Cells[rowNumber, 1, rowNumber, excelWorksheet.Dimension.End.Column];
                    var newRow = dtHeaderExcel.NewRow();
                    if (excelWorksheet.Cells[rowNumber, 1].Value != null)
                    {
                        foreach (var cell in row)
                        {
                            newRow[cell.Start.Column - 1] = cell.Text;
                        }
                    }
                    else
                    {
                        notesIndex = rowNumber + 1;
                        break;
                    }

                    dtHeaderExcel.Rows.Add(newRow);
                }
                #endregion

                #region Validation for Lines
                dtHeaderExcel.Columns.Add("JointErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("PNo1ErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("GrNo1ErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("PNo2ErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("GrNo2ErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("PTempErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("PTimeErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("ThickNessErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("WeldingProcessErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("ConsumableErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("DesignCodeErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("TPIErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("StepCoolingErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("RemarksErrorMsg", typeof(string));

                var lstLoc = Manager.GetUserwiseLocation(objClsLoginInfo.UserName);
                var lstWeldingProcess = Manager.GetSubCatagorywithoutBULocation("Welding Process");
                var lstConsumabledata = GetConsumableddlist();
                var lstJoint = Manager.GetSubCatagorywithoutBULocation("Joint Type");
                var lstParentMetal = Manager.GetSubCatagorywithoutBULocation("Parent Metal");
                var lstCode = Manager.GetSubCatagorywithoutBULocation("DesignCode");
                var lstTPI = Manager.GetSubCatagorywithoutBULocation("TPI Agency");

                foreach (DataRow item in dtHeaderExcel.Rows)
                {
                    string Joint = item.Field<string>("JOINT");
                    string PNo1 = item.Field<string>("P.No.1");
                    string GNo1 = item.Field<string>("Gr.No.1");
                    string PNo2 = item.Field<string>("P.No.2");
                    string GNo2 = item.Field<string>("Gr.No.2");
                    string ThickNess = item.Field<string>("ThickNess");
                    string WP = item.Field<string>("WELDING PROCESS");
                    string Consumable = item.Field<string>("Consumable");
                    string PWHTTemp = item.Field<string>("PWHTTemp");
                    string PWHTTime = item.Field<string>("PWHTTime");
                    string Code = item.Field<string>("Code");
                    string TPI = item.Field<string>("TPI");
                    string StepCooling = item.Field<string>("StepCooling");
                    string Remarks = item.Field<string>("REMARKS");
                    string errorMessage = string.Empty;

                    if (string.IsNullOrEmpty(Joint)) { item["JointErrorMsg"] = "Joint is required"; isError = true; }
                    else if (Joint.Length > 15) { item["JointErrorMsg"] = "Joint maxlength exceeded"; isError = true; }
                    if (!string.IsNullOrEmpty(Joint))
                    {
                        if ((Joint = lstJoint.Where(w => w.CategoryDescription.Trim().ToLower() == Joint.Trim().ToLower() || w.Value.Trim().ToLower() == Joint.Trim().ToLower()).Select(s => s.Value).FirstOrDefault()) == null)
                        {
                            item["JointErrorMsg"] = "Joint is Invalid"; isError = true;
                            isError = true;
                        }
                        else
                            item["JOINT"] = Joint;
                    }

                    if (string.IsNullOrEmpty(PNo1)) { item["PNo1ErrorMsg"] = "P.No.1 is required"; isError = true; }
                    else if (PNo1.Length > 15) { item["PNo1ErrorMsg"] = "P.No.1 maxlength exceeded"; isError = true; }
                    if (!string.IsNullOrEmpty(PNo1))
                    {
                        if ((PNo1 = lstParentMetal.Where(w => w.CategoryDescription.Trim().ToLower() == PNo1.Trim().ToLower() || w.Value.Trim().ToLower() == PNo1.Trim().ToLower()).Select(s => s.Value).FirstOrDefault()) == null)
                        {
                            item["PNo1ErrorMsg"] = "P.No.1 is Invalid"; isError = true;
                            isError = true;
                        }
                        else
                            item["P.No.1"] = PNo1;
                    }

                    if (string.IsNullOrEmpty(PNo2)) { item["PNo2ErrorMsg"] = "P.No.2 is required"; isError = true; }
                    else if (PNo2.Length > 15) { item["PNo2ErrorMsg"] = "P.No.2 maxlength exceeded"; isError = true; }
                    if (!string.IsNullOrEmpty(PNo2))
                    {
                        if ((PNo2 = lstParentMetal.Where(w => w.CategoryDescription.Trim().ToLower() == PNo2.Trim().ToLower() || w.Value.Trim().ToLower() == PNo2.Trim().ToLower()).Select(s => s.Value).FirstOrDefault()) == null)
                        {
                            item["PNo2ErrorMsg"] = "P.No.2 is Invalid"; isError = true;
                            isError = true;
                        }
                        else
                            item["P.No.2"] = PNo2;
                    }

                    double temp;
                    if (!string.IsNullOrEmpty(ThickNess))
                    {
                        if (!Double.TryParse(ThickNess, out temp))
                        {
                            item["ThickNessErrorMsg"] = "ThickNess invalid format";
                            isError = true;
                        }

                        if (ThickNess.Length > 5)
                        {
                            item["ThickNessErrorMsg"] = "Thickness maxlength exceeded";
                            isError = true;
                        }

                    }
                    if (GNo1.Length > 2) { item["GrNo1ErrorMsg"] = "Gr.No1 maxlength exceeded"; isError = true; }
                    int a;
                    if (!string.IsNullOrEmpty(GNo1))
                    {
                        if (!int.TryParse(GNo1, out a))
                        {
                            item["GrNo1ErrorMsg"] = "Gr.No1 invalid format";
                            isError = true;
                        }
                    }

                    if (GNo2.Length > 2) { item["GrNo2ErrorMsg"] = "Gr.No2 maxlength exceeded"; isError = true; }
                    int b;
                    if (!string.IsNullOrEmpty(GNo2))
                    {
                        if (!int.TryParse(GNo2, out b))
                        {
                            item["GrNo2ErrorMsg"] = "Gr.No2 invalid format";
                            isError = true;
                        }
                    }
                    if (PWHTTemp.Length > 50) { item["PTempErrorMsg"] = "PWHTTemp maxlength exceeded"; isError = true; }
                    if (PWHTTime.Length > 20) { item["PTimeErrorMsg"] = "PWHTTime maxlength exceeded"; isError = true; }

                    if (string.IsNullOrEmpty(WP)) { item["WeldingProcessErrorMsg"] = "Welding Process is required"; isError = true; }
                    else if (WP.Length > 200) { item["WeldingProcessErrorMsg"] = "Welding Process maxlength exceeded"; isError = true; }
                    if (!string.IsNullOrEmpty(WP))
                    {
                        string[] strarry = WP.Split('+').ToArray();
                        string strwp = string.Empty;
                        foreach (var arrobj in strarry)
                        {
                            if ((WP = lstWeldingProcess.Where(w => w.CategoryDescription.Trim().ToLower() == arrobj.Trim().ToLower() || w.Value.Trim().ToLower() == arrobj.Trim().ToLower()).Select(s => s.Value).FirstOrDefault()) == null)
                            {
                                item["WeldingProcessErrorMsg"] = "Welding Process is Invalid"; isError = true;
                                isError = true;
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(strwp))
                                    strwp = arrobj;
                                else
                                    strwp = strwp + '+' + arrobj;
                            }
                        }
                        item["WELDING PROCESS"] = strwp;
                    }

                    if (string.IsNullOrEmpty(Consumable)) { item["ConsumableErrorMsg"] = "Consumable is required"; isError = true; }
                    else if (Consumable.Length > 200) { item["ConsumableErrorMsg"] = "Consumable maxlength exceeded"; isError = true; }
                    if (!string.IsNullOrEmpty(Consumable))
                    {
                        string strtype = Consumable;
                        string[] strarry = Consumable.Split('+').ToArray();
                        foreach (var arrobj in strarry)
                        {
                            if ((Consumable = lstConsumabledata.Where(w => w.Code.Trim().ToLower() == arrobj.Trim().ToLower() || w.Description.Trim().ToLower() == arrobj.Trim().ToLower()).Select(s => s.Code).FirstOrDefault()) == null)
                            {
                                item["ConsumableErrorMsg"] = "Consumable Type is Invalid"; isError = true;
                                isError = true;
                            }
                            else
                            {
                                item["Consumable"] = strtype;
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(Code))
                    {
                        if ((Code = lstCode.Where(w => w.CategoryDescription.Trim().ToLower() == Code.Trim().ToLower() || w.Value.Trim().ToLower() == Code.Trim().ToLower()).Select(s => s.Value).FirstOrDefault()) == null)
                        {
                            item["DesignCodeErrorMsg"] = "Design Code is Invalid"; isError = true;
                            isError = true;
                        }
                        else
                            item["Code"] = Code;
                    }
                    if (!string.IsNullOrEmpty(TPI))
                    {
                        if ((TPI = lstTPI.Where(w => w.CategoryDescription.Trim().ToLower() == TPI.Trim().ToLower() || w.Value.Trim().ToLower() == TPI.Trim().ToLower()).Select(s => s.Value).FirstOrDefault()) == null)
                        {
                            item["TPIErrorMsg"] = "TPI is Invalid"; isError = true;
                            isError = true;
                        }
                        else
                            item["TPI"] = TPI;
                    }

                    if (!string.IsNullOrEmpty(StepCooling))
                    {
                        if (StepCooling == "NA" || StepCooling == "Yes" || StepCooling == "No")
                        {
                            item["StepCooling"] = StepCooling;
                        }
                        else
                        {
                            item["StepCoolingErrorMsg"] = "StepCooling is Invalid"; isError = true;
                            isError = true;
                        }
                    }

                    if (!string.IsNullOrEmpty(Remarks) && Remarks.Length > 100) { item["RemarksErrorMsg"] = "Remarks maxlength exceeded"; isError = true; }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                isError = true;
                //throw ex;
                dtHeaderExcel.Rows[0]["RemarksErrorMsg"] = "EXCEPTION:" + ex.Message;
            }
            DataSet ds = new DataSet();
            ds.Tables.Add(dtHeaderExcel);
            return ds;
        }

        public clsHelper.ResponceMsgWithFileName ErrorPQRExportToExcel(DataSet ds)
        {
            clsHelper.ResponceMsgWithFileName objResponseMsg = new clsHelper.ResponceMsgWithFileName();
            try
            {
                MemoryStream ms = new MemoryStream();
                using (FileStream fs = System.IO.File.OpenRead(Server.MapPath("~/Resources/PQR/PQR Error Template.xlsx")))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();
                        int i = 2;
                        foreach (DataRow item in ds.Tables[0].Rows)
                        {
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("JointErrorMsg"))) { excelWorksheet.Cells[i, 1].Value = item.Field<string>(0) + " (Note: " + item.Field<string>("JointErrorMsg") + " )"; excelWorksheet.Cells[i, 1].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 1].Value = item.Field<string>(0); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("PNo1ErrorMsg"))) { excelWorksheet.Cells[i, 2].Value = item.Field<string>(1) + " (Note: " + item.Field<string>("PNo1ErrorMsg") + " )"; excelWorksheet.Cells[i, 2].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 2].Value = item.Field<string>(1); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("GrNo1ErrorMsg"))) { excelWorksheet.Cells[i, 3].Value = item.Field<string>(2) + " (Note: " + item.Field<string>("GrNo1ErrorMsg") + " )"; excelWorksheet.Cells[i, 3].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 3].Value = item.Field<string>(2); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("PNo2ErrorMsg"))) { excelWorksheet.Cells[i, 4].Value = item.Field<string>(3) + " (Note: " + item.Field<string>("PNo2ErrorMsg") + " )"; excelWorksheet.Cells[i, 4].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 4].Value = item.Field<string>(3); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("GrNo2ErrorMsg"))) { excelWorksheet.Cells[i, 5].Value = item.Field<string>(4) + " (Note: " + item.Field<string>("GrNo2ErrorMsg") + " )"; excelWorksheet.Cells[i, 5].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 5].Value = item.Field<string>(4); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("ThickNessErrorMsg"))) { excelWorksheet.Cells[i, 6].Value = item.Field<string>(5) + " (Note: " + item.Field<string>("ThickNessErrorMsg") + " )"; excelWorksheet.Cells[i, 6].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 6].Value = item.Field<string>(5); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("WeldingProcessErrorMsg"))) { excelWorksheet.Cells[i, 7].Value = item.Field<string>(6) + " (Note: " + item.Field<string>("WeldingProcessErrorMsg") + " )"; excelWorksheet.Cells[i, 7].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 7].Value = item.Field<string>(6); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("ConsumableErrorMsg"))) { excelWorksheet.Cells[i, 8].Value = item.Field<string>(7) + " (Note: " + item.Field<string>("ConsumableErrorMsg") + " )"; excelWorksheet.Cells[i, 8].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 8].Value = item.Field<string>(7); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("PTempErrorMsg"))) { excelWorksheet.Cells[i, 9].Value = item.Field<string>(8) + " (Note: " + item.Field<string>("PTempErrorMsg") + " )"; excelWorksheet.Cells[i, 9].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 9].Value = item.Field<string>(8); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("PTimeErrorMsg"))) { excelWorksheet.Cells[i, 10].Value = item.Field<string>(9) + " (Note: " + item.Field<string>("PTimeErrorMsg") + " )"; excelWorksheet.Cells[i, 10].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 10].Value = item.Field<string>(9); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("DesignCodeErrorMsg"))) { excelWorksheet.Cells[i, 11].Value = item.Field<string>(10) + " (Note: " + item.Field<string>("DesignCodeErrorMsg") + " )"; excelWorksheet.Cells[i, 11].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 11].Value = item.Field<string>(10); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("TPIErrorMsg"))) { excelWorksheet.Cells[i, 12].Value = item.Field<string>(11) + " (Note: " + item.Field<string>("TPIErrorMsg") + " )"; excelWorksheet.Cells[i, 12].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 12].Value = item.Field<string>(11); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("StepCoolingErrorMsg"))) { excelWorksheet.Cells[i, 13].Value = item.Field<string>(12) + " (Note: " + item.Field<string>("StepCoolingErrorMsg") + " )"; excelWorksheet.Cells[i, 13].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 13].Value = item.Field<string>(12); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("RemarksErrorMsg"))) { excelWorksheet.Cells[i, 14].Value = item.Field<string>(13) + " (Note: " + item.Field<string>("RemarksErrorMsg") + " )"; excelWorksheet.Cells[i, 14].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 14].Value = item.Field<string>(13); }
                            i++;
                        }
                        excelPackage.SaveAs(ms);
                    }
                }

                ms.Position = 0;

                string fileName;
                string excelFilePath = Helper.ExcelFilePath(objClsLoginInfo.UserName, out fileName);

                Byte[] bin = ms.ToArray();
                System.IO.File.WriteAllBytes(excelFilePath, bin);

                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error";
                objResponseMsg.FileName = fileName;
                return objResponseMsg;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }
        #endregion

        public class SearchFilterPQR
        {
            public string ColumnName { get; set; }
            public string Value { get; set; }
            public string FilterType { get; set; }
            public string DataType { get; set; }
        }
    }
}