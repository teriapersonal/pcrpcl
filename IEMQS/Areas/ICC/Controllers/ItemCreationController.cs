﻿using IEMQS.Areas.ICC.Models;
using IEMQS.Areas.Utility.Models;
using IEMQS.ItemCreationService;
using IEMQS.Models;
using IEMQS.UpdateItemStockService;
using IEMQSImplementation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;


namespace IEMQS.Areas.ICC.Controllers
{
    [SessionExpireFilter]
    public class ItemCreationController : clsBase
    {
        #region Index Page

        //[AllowAnonymous]
        //[UserPermissions]
        public ActionResult Index()
        {
            ViewBag.Title = "Item Code Creation";
            ViewBag.user = clsImplementationEnum.ICCindex.mdm.GetStringValue();
            return View();
        }

        [HttpPost]
        public ActionResult LoadDataGridPartial(string status, string user)
        {
            ViewBag.user = user;
            //ViewBag.Status = status;
            //return PartialView("_GetIndexDataPartial");
            ViewBag.Status = status;
            if (status.ToLower() == "updaterequestapproval" || status.ToLower() == "checkupdaterequest")
            {
                return PartialView("_GetItemUpdateRequestDataPartial");
            }
            else
            {
                return PartialView("_GetIndexDataPartial");
            }
        }

        [HttpPost]
        public ActionResult LoadItemApprovalGridPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetItemUpdateRequestDataPartial");
        }

        [HttpPost]
        public JsonResult LoadDataGridData(JQueryDataTableParamModel param)

        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                strWhereCondition += "1=1 ";

                #endregion
                if (param.Status.ToLower() == "pending")
                {
                    strWhereCondition += " and Status in ('" + clsImplementationEnum.ICCStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.ICCStatus.Not_Created.GetStringValue() + "')";
                }
                else if (param.Status.ToLower() == "all")
                {
                    strWhereCondition += " and Status in ('" + clsImplementationEnum.ICCStatus.Created.GetStringValue() + "')";
                }
                else if (param.Status.ToLower() == "updaterequest" || param.Status.ToLower() == "checkupdaterequest")
                {
                    strWhereCondition += " and  (icc003.RequestStatus = '" + clsImplementationEnum.ICCStatus.Approved.GetStringValue() + "' or icc003.RequestStatus='" +
                        clsImplementationEnum.ICCStatus.Return + "' or icc002.Status = '" +
                        clsImplementationEnum.ICCStatus.Created + "' or icc002.Status = '" +
                        clsImplementationEnum.ICCStatus.Submitted + "' or icc002.Status = '" +
                        clsImplementationEnum.ICCStatus.Return + "' )";
                }
                //search Condition 

                string[] columnName = { "CreatedOn", "ItemCode", "LongDescription", "MaterialCategory", "MaterialGroup", "DistinctItem", "UOM", "Status" };
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var lstResult = db.SP_ICC_GET_ITEM_CREATION_INDEX_DATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = new List<string[]>();
                if (param.Status.ToLower() == "checkupdaterequest")
                {
                    data = (from uc in lstResult
                            select new[]
                           {
                           // Convert.ToString(uc.ROW_NO),
                            Convert.ToString(uc.ItemCode),
                            Convert.ToString(uc.MaterialCategory),
                            Convert.ToString(uc.MaterialGroup),
                            Convert.ToString(uc.DistinctItem),
                            Convert.ToString(uc.UOM),
                            uc.RequestStatus,
                            Convert.ToString(uc.HeaderId)
                          }).ToList();
                }
                else
                {
                    data = (from uc in lstResult
                            select new[]
                           {
                           // Convert.ToString(uc.ROW_NO),
                            uc.CreatedOn.HasValue ? uc.CreatedOn.Value.ToString("dd/MM/yyyy") : "",
                            Convert.ToString(uc.ItemCode),
                            (uc.LongDescription.Length>15? uc.LongDescription.Substring(0,15)+"..." :Convert.ToString(uc.LongDescription)),
                            Convert.ToString(uc.MaterialCategory),
                            Convert.ToString(uc.MaterialGroup),
                            Convert.ToString(uc.DistinctItem),
                            Convert.ToString(uc.UOM),
                            Convert.ToString(uc.Status),
                            Convert.ToString(uc.HeaderId)
                          }).ToList();
                }
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult LoadItemDataGridData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                strWhereCondition += "1=1 ";
                #endregion
                if (param.Status.ToLower() == "pending")
                {
                    strWhereCondition += " and Status in ('" + clsImplementationEnum.ICCStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.ICCStatus.Not_Created.GetStringValue() + "')";
                }
                else if (param.Status.ToLower() == "all")
                {
                    strWhereCondition += " and Status in ('" + clsImplementationEnum.ICCStatus.Created.GetStringValue() + "')";
                }
                //search Condition 

                string[] columnName = { "CreatedOn", "ItemCode", "LongDescription", "MaterialCategory", "MaterialGroup", "DistinctItem", "UOM", "Status" };
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var lstResult = db.SP_ICC_GET_ITEM_CREATION_INDEX_DATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();
                string u = param.Roles;
                var data = new List<string[]>();

                data = (from uc in lstResult
                        select new[]
                       {
                           // Convert.ToString(uc.ROW_NO),
                            uc.CreatedOn.HasValue ? uc.CreatedOn.Value.ToString("dd/MM/yyyy") : "",
                            Convert.ToString(uc.ItemCode),
                            (uc.LongDescription.Length>15? uc.LongDescription.Substring(0,15)+"..." :Convert.ToString(uc.LongDescription)),
                            Convert.ToString(uc.MaterialCategory),
                            Convert.ToString(uc.MaterialGroup),
                            Convert.ToString(uc.DistinctItem),
                            Convert.ToString(uc.UOM),
                            Convert.ToString(uc.Status),
                              "<nobr><center>"+
                                         Helper.GenerateActionIcon(uc.HeaderId, "View", "View Detail", "fa fa-eye", "",WebsiteURL +"/ICC/ItemCreation/Details/"+uc.HeaderId+"?u="+u ,false)
                               + "</center></nobr>",
                          }).ToList();
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadItemUpdateRequestGridData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                strWhereCondition += "1=1 ";
                string u = param.Roles;
                #endregion

                if (param.Status.ToLower() == "updaterequestapproval")
                {
                    strWhereCondition += " and icc003.RequestStatus in ('" + clsImplementationEnum.ICCStatus.Submitted.GetStringValue() + "','" + clsImplementationEnum.ICCStatus.Return.GetStringValue() + "')";
                }
                if (param.Status.ToLower() == "checkupdaterequest")
                {
                    strWhereCondition += " and  (icc003.RequestStatus = '" + clsImplementationEnum.ICCStatus.Approved.GetStringValue() + "' or icc003.RequestStatus='" +
                    clsImplementationEnum.ICCStatus.Return + "' or icc002.Status = '" +
                    clsImplementationEnum.ICCStatus.Created + "' or icc002.Status = '" +
                    clsImplementationEnum.ICCStatus.Submitted + "' or icc002.Status = '" +
                    clsImplementationEnum.ICCStatus.Return + "' ) and icc003.CreatedBy='" + objClsLoginInfo.UserName + "'";
                }
                //search Condition 

                string[] columnName = { "ItemCode", "LongDescription", "MaterialCategory", "MaterialGroup", "DistinctItem", "UOM", "RequestStatus" };
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var lstResult = db.SP_ICC_UPDATE_ITEM_CREATION_INDEX_DATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                           // Convert.ToString(uc.ROW_NO),
                            uc.CreatedOn.HasValue ? uc.CreatedOn.Value.ToString("dd/MM/yyyy") : "",
                            uc.ItemCode,
                            (uc.LongDescription.Length>15? uc.LongDescription.Substring(0,15)+"..." :Convert.ToString(uc.LongDescription)),
                            uc.MaterialCategory,
                            uc.MaterialGroup,
                            uc.DistinctItem,
                            uc.UOM,
                            uc.RequestStatus,
                            Convert.ToString(uc.HeaderId),
                             "<nobr><center>"+
                                         Helper.GenerateActionIcon(uc.RequestId, "View", "View Detail", "fa fa-eye", "",WebsiteURL +"/ICC/ItemCreation/UpdateRequestPending/"+uc.RequestId+"?u="+u ,false)
                                       +  Helper.GenerateActionIcon(uc.RequestId, "Timeline", "Timeline Detail", "fa fa-clock-o", "ShowTimeline('/ICC/ItemCreation/ShowTimeline?RequestId="+ uc.RequestId +"')","",false)
                               + "</center></nobr>",
                          }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Add header

        public ActionResult Details(int? id)
        {
            if (id > 0)
            {
                var objICC002 = db.ICC002.FirstOrDefault(x => x.HeaderId == id);
                if (objICC002.Status != clsImplementationEnum.ICCStatus.Not_Created.GetStringValue() && objICC002.Status != clsImplementationEnum.ICCStatus.Draft.GetStringValue())
                {
                    ViewBag.Status = objICC002.Status;
                }
                ViewBag.Action = "edit";
                List<string> lstNatureOfItem = clsImplementationEnum.getICC_NatureOfItem().ToList();
                ViewBag.NatureOfItem = lstNatureOfItem.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

                UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
                ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

                var objICC003 = db.ICC003.FirstOrDefault(x => x.HeaderId == id);
                if (objICC003 != null)
                {
                    ViewBag.UpdateDetails = objICC003.ChangeDetails;
                }
                ViewBag.ItemCodeRequestor = objICC002.ItemCodeRequestor;
                ViewBag.Title = "Item Master Management";
                return View(objICC002);
            }
            return RedirectToAction("Index");
        }

        public UserRoleAccessDetails GetUserAccessRights()
        {
            UserRoleAccessDetails objUserRoleAccessDetails = new UserRoleAccessDetails();

            try
            {
                var role = (from a in db.ATH001
                            join b in db.ATH004 on a.Role equals b.Id
                            where a.Employee.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                            select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();

                if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.MDM3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.MDM3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Initiator.GetStringValue();
                }
            }
            catch
            {

            }
            return objUserRoleAccessDetails;
        }

        public ActionResult Update(int? id)
        {
            if (id > 0)
            {
                var objICC002 = db.ICC002.FirstOrDefault(x => x.HeaderId == id);
                if (objICC002.Status == clsImplementationEnum.ICCStatus.Created.GetStringValue())
                {
                    ViewBag.Action = "update";
                    List<string> lstNatureOfItem = clsImplementationEnum.getICC_NatureOfItem().ToList();
                    ViewBag.NatureOfItem = lstNatureOfItem.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

                    ViewBag.Title = "Item Master Management";
                    return View("Details", objICC002);
                }
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        public ActionResult BindICCddl(string ddlType, string selectedVal)
        {
            List<SelectListItem> lstdropdown = new List<SelectListItem>();
            string selectedval = string.Empty;
            switch (ddlType)
            {
                case "ItemType":
                    if (selectedVal == clsImplementationEnum.ICC_NatureOfItem.Job.GetStringValue())
                    {
                        List<string> lstEnum = clsImplementationEnum.getICC_ItemType_Job().ToList();
                        lstdropdown = lstEnum.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();
                    }
                    if (selectedVal == clsImplementationEnum.ICC_NatureOfItem.Non_Job.GetStringValue())
                    {
                        List<string> lstEnum = clsImplementationEnum.getICC_ItemType_Non_Job().ToList();
                        lstdropdown = lstEnum.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();
                    }

                    if (selectedVal == clsImplementationEnum.ICC_NatureOfItem.MPI.GetStringValue())
                    {
                        List<string> lstEnum = clsImplementationEnum.getICC_ItemType_MPI().ToList();
                        lstdropdown = lstEnum.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();
                    }
                    break;
                case "ItemSubType":
                    if (selectedVal == clsImplementationEnum.ICC_ItemType_Non_Job.AutoReorder.GetStringValue())
                    {
                        List<string> lstEnum = clsImplementationEnum.getICC_ItemSubType_AutoReorder().ToList();
                        lstdropdown = lstEnum.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();
                    }
                    if (selectedVal == clsImplementationEnum.ICC_ItemType_Non_Job.ManualPurchase.GetStringValue())
                    {
                        List<string> lstEnum = clsImplementationEnum.getICC_ItemSubType__ManualPurchase_CART().ToList();
                        lstdropdown = lstEnum.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();
                    }

                    if (selectedVal == clsImplementationEnum.ICC_ItemType_MPI.Welding_Consumable.GetStringValue())
                    {
                        List<string> lstEnum = clsImplementationEnum.getICC_ItemSubType__Welding_Consumable().ToList();
                        lstdropdown = lstEnum.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();
                    }

                    if (selectedVal == clsImplementationEnum.ICC_ItemType_MPI.Machine_Shop.GetStringValue())
                    {
                        List<string> lstEnum = clsImplementationEnum.getICC_ItemSubType__Machine_Shop().ToList();
                        lstdropdown = lstEnum.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();
                    }
                    break;
            }
            if (lstdropdown.Count() == 1)
            {
                selectedval = lstdropdown.Select(x => x.Value).FirstOrDefault();
            }
            var objData = new
            {
                count = lstdropdown.Count,
                lstdropdown = lstdropdown,
                selectedval = selectedval
            };

            return Json(objData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveHeader(ICC002 icc002)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (icc002.HeaderId > 0)
                {
                    var objICC002 = db.ICC002.FirstOrDefault(o => o.HeaderId == icc002.HeaderId);

                    // if (objICC002.Status == clsImplementationEnum.ICCStatus.Not_Created.GetStringValue() || objICC002.Status == clsImplementationEnum.ICCStatus.Draft.GetStringValue())
                    if (objICC002 != null)
                    {
                        objICC002.ShortDescription = icc002.ShortDescription;
                        objICC002.NatureOfItem = icc002.NatureOfItem;
                        objICC002.ItemType = icc002.ItemType;
                        objICC002.ItemSubType = icc002.ItemSubType;

                        objICC002.SafetyLevel_HZW = icc002.SafetyLevel_HZW;
                        objICC002.SafetyLevel_PEW = icc002.SafetyLevel_PEW;
                        objICC002.SafetyLevel_RNW = icc002.SafetyLevel_RNW;
                        objICC002.ReorderLevel_HZW = icc002.ReorderLevel_HZW;
                        objICC002.ReorderLevel_PEW = icc002.ReorderLevel_PEW;
                        objICC002.ReorderLevel_RNW = icc002.ReorderLevel_RNW;
                        objICC002.MaxStock_HZW = icc002.MaxStock_HZW;
                        objICC002.MaxStock_PEW = icc002.MaxStock_PEW;
                        objICC002.MaxStock_RNW = icc002.MaxStock_RNW;
                        objICC002.FixedOrderQty_HZW = icc002.FixedOrderQty_HZW;
                        objICC002.FixedOrderQty_PEW = icc002.FixedOrderQty_PEW;
                        objICC002.FixedOrderQty_RNW = icc002.FixedOrderQty_RNW;
                        objICC002.ReStockLevel_HZW = icc002.ReStockLevel_HZW;
                        objICC002.ReStockLevel_PEW = icc002.ReStockLevel_PEW;
                        objICC002.ReStockLevel_RNW = icc002.ReStockLevel_RNW;

                        objICC002.ItemCodeRequestor = icc002.ItemCodeRequestor;
                        objICC002.Status = clsImplementationEnum.ICCStatus.Draft.GetStringValue();
                        objICC002.EditedBy = objClsLoginInfo.UserName;
                        objICC002.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Item updated successfully";

                        var log = new ICC002_Log()
                        {
                            ActionBy = objClsLoginInfo.UserName,
                            ActionOn = DateTime.Now,
                            Action = clsImplementationEnum.ICCStatus.Draft.GetStringValue(),
                            ItemCode = objICC002.ItemCode,
                            Comments = "Saving Item as draft"
                        };
                        MaintainLog_ICC002(log);
                    }
                    else
                    {

                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.AlreadySubmitted;

                    }
                    objResponseMsg.Status = objICC002.Status;
                    objResponseMsg.HeaderId = objICC002.HeaderId;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// ERP LN Push Service is remaining 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SubmittoLN(int id)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                ICC002 objICC002 = db.ICC002.Where(o => o.HeaderId == id).FirstOrDefault();
                if (objICC002.Status == clsImplementationEnum.ICCStatus.Not_Created.GetStringValue() || objICC002.Status == clsImplementationEnum.ICCStatus.Draft.GetStringValue())
                {
                    string LNUOM = string.Empty;
                    clsHelper.ResponseMsg objLNResponse = PushToLN(objICC002, ref LNUOM);
                    if (objLNResponse.Key)
                    {
                        objICC002.LNUOM = LNUOM;
                        objICC002.Status = clsImplementationEnum.ICCStatus.Created.GetStringValue();
                        objICC002.LNSubmittedBy = objClsLoginInfo.UserName;
                        objICC002.LNSubmittedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Item has been submitted to LN successfully";
                        objResponseMsg.Status = objICC002.Status;

                        var log = new ICC002_Log()
                        {
                            ActionBy = objClsLoginInfo.UserName,
                            ActionOn = DateTime.Now,
                            Action = clsImplementationEnum.ICCStatus.Created.GetStringValue(),
                            ItemCode = objICC002.ItemCode,
                            Comments = "Item Created and Push into LN"
                        };
                        MaintainLog_ICC002(log);

                        if (objICC002.ItemSubType != null && objICC002.ItemSubType.Trim().ToLower() == clsImplementationEnum.ICC_ItemSubType_AutoReorder.FOS.GetStringValue().ToLower())
                        {
                            //call service to update stock details
                            clsHelper.ResponseMsg objStockResponse = UpdateStockDetailsInLN(objICC002);
                            if (!objStockResponse.Key)
                            {
                                objResponseMsg.Key = true;
                                objResponseMsg.Value = "Item has been submitted to LN successfully. but some errors occured in updating Stock Details into LN : " + objStockResponse.Value;
                                objResponseMsg.Status = objICC002.Status;
                            }
                        }

                        // email configuration
                        MAIL001 objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.ICC.ItemCreatedPushToLN).FirstOrDefault();
                        if (objTemplateMaster != null)
                        {
                            //below is commentted as discussed with Jatin Baraiya sir on 25/02/2019. ItemCodeRequestor will not be the PSNo.
                            //string emails = Manager.GetUserEmailByRole(clsImplementationEnum.UserRoleName.MDM3.GetStringValue()) + "," + Manager.GetMailIdFromPsNo(objICC002.ItemCodeRequestor);

                            string emails = Manager.GetUserEmailByRole(clsImplementationEnum.UserRoleName.MDM3.GetStringValue());
                            List<string> uniques = emails.Split(',').Distinct().ToList();
                            emails = string.Join(",", uniques);

                            Hashtable _ht = new Hashtable();
                            EmailSend _objEmail = new EmailSend();
                            _objEmail.MailToAdd = emails; // ALL MDM Role User, Item Code Requestor
                            _ht["[ItemCode]"] = objICC002.ItemCode;
                            _ht["[ItemLongDescription]"] = objICC002.LongDescription;
                            _ht["[MaterialCategory]"] = objICC002.MaterialCategory;
                            _ht["[MaterialGroup]"] = objICC002.MaterialGroup;
                            _ht["[DistinctItem]"] = objICC002.DistinctItem;
                            _ht["[UOM]"] = objICC002.UOM;
                            _ht["[NatureOfItem]"] = objICC002.NatureOfItem;
                            _ht["[ItemType]"] = objICC002.ItemType;
                            _ht["[ItemSubType]"] = objICC002.ItemSubType;
                            _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = objLNResponse.Value;
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.StatusChanged;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponseMsg PushToLN(ICC002 objICC002, ref string LNUoM)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            objResponseMsg.Key = false;

            string itemgroup = string.Empty;
            var objICC001 = db.ICC001.Where(x => x.MaterialCategory.Equals(objICC002.MaterialCategory, StringComparison.OrdinalIgnoreCase) &&
                                                 x.MaterialGroup.Equals(objICC002.MaterialGroup, StringComparison.OrdinalIgnoreCase) &&
                                                 x.UOM.Equals(objICC002.UOM, StringComparison.OrdinalIgnoreCase) &&
                                                 x.DistinctItem.Equals(objICC002.DistinctItem, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
            if (objICC001 != null)
            {
                itemgroup = !string.IsNullOrWhiteSpace(objICC001.ERPLNItemGroup) ? objICC001.ERPLNItemGroup : "";
            }

            //fetch LN UOM based on Code Gen UOM from ICC005           
            var objICC005 = db.ICC005.Where(x => x.CodeGenUOM.Equals(objICC002.UOM, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
            if (objICC005 != null)
            {
                LNUoM = !string.IsNullOrWhiteSpace(objICC005.LNUOM) ? objICC005.LNUOM : "";
            }

            if (string.IsNullOrWhiteSpace(LNUoM))
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "ERP LN UOM is not maintained against CodeGen UOM. Please maintain it.";
                return objResponseMsg;
            }

            ItemDataBdeService serviceObj = new ItemDataBdeService();
            try
            {
                createItemRequestTypeItemDataBdeText P1createItemRequestTypeItemDataBdeText = new createItemRequestTypeItemDataBdeText();
                createItemResponseType P1createResponse = new createItemResponseType();
                createItemRequestType P1createRequest = new createItemRequestType();
                createItemRequestTypeControlArea P1controlArea = new createItemRequestTypeControlArea();
                createItemRequestTypeItemDataBde P1dataArea = new createItemRequestTypeItemDataBde();

                P1controlArea.processingScope = IEMQS.ItemCreationService.processingScope.request;
                P1dataArea.item = new string(' ', 9) + objICC002.ItemCode;
                P1dataArea.itemdsca = objICC002.ShortDescription;
                P1dataArea.itemgroup = itemgroup;
                P1dataArea.itemtype = tckitm_mandatory.purchase;
                P1dataArea.itemtypeSpecified = true;
                P1dataArea.inventoryunit = LNUoM;
                P1dataArea.productclass = string.Empty;
                P1dataArea.producttype = objICC002.ItemSubType;
                P1dataArea.productline = string.Empty;
                P1dataArea.purchaseunit = string.Empty;
                P1dataArea.purchasepriceunit = string.Empty;
                P1dataArea.purchasepriceSpecified = false;
                P1dataArea.salesunit = string.Empty;
                P1dataArea.salespriceunit = string.Empty;

                P1createItemRequestTypeItemDataBdeText.Value = objICC002.LongDescription;
                P1dataArea.text = new createItemRequestTypeItemDataBdeText[1];
                P1dataArea.text[0] = P1createItemRequestTypeItemDataBdeText;

                P1createRequest.ControlArea = P1controlArea;
                P1createRequest.DataArea = new createItemRequestTypeItemDataBde[1];
                P1createRequest.DataArea[0] = P1dataArea;

                P1createResponse = serviceObj.createItem(P1createRequest);

                if (P1createResponse.DataArea[0].Response.StartsWith("0"))
                {
                    //success
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "success";
                }
                else
                {
                    //error
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = P1createResponse.DataArea[0].Response.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message.ToString();
            }
            finally
            {
                serviceObj = null;
            }
            return objResponseMsg;
        }

        public clsHelper.ResponseMsg UpdateStockDetailsInLN(ICC002 objICC002)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            objResponseMsg.Key = false;

            string HaziraWarehouse = string.Empty;
            string PowaiWarehouse = string.Empty;
            string RanoliWarehouse = string.Empty;
            List<ICC004> objICC004List = db.ICC004.ToList();
            if (objICC004List != null && objICC004List.Count > 0)
            {
                var obj1 = (from u in objICC004List where u.Location.ToUpper() == "HZW" select u).FirstOrDefault();
                if (obj1 != null)
                    HaziraWarehouse = obj1.Warehouse;

                var obj2 = (from u in objICC004List where u.Location.ToUpper() == "PEW" select u).FirstOrDefault();
                if (obj2 != null)
                    PowaiWarehouse = obj2.Warehouse;

                var obj3 = (from u in objICC004List where u.Location.ToUpper() == "RNW" select u).FirstOrDefault();
                if (obj3 != null)
                    RanoliWarehouse = obj3.Warehouse;
            }

            IEMQS.UpdateItemStockService.ItemCreationService serviceObj = new IEMQS.UpdateItemStockService.ItemCreationService();
            try
            {
                itemWarehouseDataUpdateRequestTypeItemCreationItemWarehouseData P1createItemRequestTypeItemDataBdeText = new itemWarehouseDataUpdateRequestTypeItemCreationItemWarehouseData();
                itemWarehouseDataUpdateResponseType P1createResponse = new itemWarehouseDataUpdateResponseType();
                itemWarehouseDataUpdateRequestType P1createRequest = new itemWarehouseDataUpdateRequestType();
                itemWarehouseDataUpdateRequestTypeControlArea P1controlArea = new itemWarehouseDataUpdateRequestTypeControlArea();
                itemWarehouseDataUpdateRequestTypeItemCreation P1dataArea = new itemWarehouseDataUpdateRequestTypeItemCreation();

                P1controlArea.processingScope = IEMQS.UpdateItemStockService.processingScope.request;
                P1dataArea.item = new string(' ', 9) + objICC002.ItemCode;

                P1dataArea.ItemWarehouseData = new itemWarehouseDataUpdateRequestTypeItemCreationItemWarehouseData[3];

                {
                    P1createItemRequestTypeItemDataBdeText = new itemWarehouseDataUpdateRequestTypeItemCreationItemWarehouseData();
                    P1createItemRequestTypeItemDataBdeText.warehouse = HaziraWarehouse;
                    P1createItemRequestTypeItemDataBdeText.safetyLevel = objICC002.SafetyLevel_HZW != null ? Convert.ToDecimal(objICC002.SafetyLevel_HZW) : 0;
                    P1createItemRequestTypeItemDataBdeText.safetyLevelSpecified = objICC002.SafetyLevel_HZW != null ? true : false;

                    P1createItemRequestTypeItemDataBdeText.reorderLevel = objICC002.ReorderLevel_HZW != null ? Convert.ToDecimal(objICC002.ReorderLevel_HZW) : 0;
                    P1createItemRequestTypeItemDataBdeText.reorderLevelSpecified = objICC002.ReorderLevel_HZW != null ? true : false;

                    P1createItemRequestTypeItemDataBdeText.maximumStock = objICC002.MaxStock_HZW != null ? Convert.ToDecimal(objICC002.MaxStock_HZW) : 0;
                    P1createItemRequestTypeItemDataBdeText.maximumStockSpecified = objICC002.MaxStock_HZW != null ? true : false;

                    P1createItemRequestTypeItemDataBdeText.fixedOrderQnty = objICC002.FixedOrderQty_HZW != null ? Convert.ToDecimal(objICC002.FixedOrderQty_HZW) : 0;
                    P1createItemRequestTypeItemDataBdeText.fixedOrderQntySpecified = objICC002.FixedOrderQty_HZW != null ? true : false;

                    P1createItemRequestTypeItemDataBdeText.restockLevel = objICC002.ReStockLevel_HZW != null ? Convert.ToDecimal(objICC002.ReStockLevel_HZW) : 0;
                    P1createItemRequestTypeItemDataBdeText.restockLevelSpecified = objICC002.ReStockLevel_HZW != null ? true : false;

                    P1dataArea.ItemWarehouseData[0] = P1createItemRequestTypeItemDataBdeText;
                }

                {
                    P1createItemRequestTypeItemDataBdeText = new itemWarehouseDataUpdateRequestTypeItemCreationItemWarehouseData();
                    P1createItemRequestTypeItemDataBdeText.warehouse = PowaiWarehouse;
                    P1createItemRequestTypeItemDataBdeText.safetyLevel = objICC002.SafetyLevel_PEW != null ? Convert.ToDecimal(objICC002.SafetyLevel_PEW) : 0;
                    P1createItemRequestTypeItemDataBdeText.safetyLevelSpecified = objICC002.SafetyLevel_PEW != null ? true : false;

                    P1createItemRequestTypeItemDataBdeText.reorderLevel = objICC002.ReorderLevel_PEW != null ? Convert.ToDecimal(objICC002.ReorderLevel_PEW) : 0;
                    P1createItemRequestTypeItemDataBdeText.reorderLevelSpecified = objICC002.ReorderLevel_PEW != null ? true : false;

                    P1createItemRequestTypeItemDataBdeText.maximumStock = objICC002.MaxStock_PEW != null ? Convert.ToDecimal(objICC002.MaxStock_PEW) : 0;
                    P1createItemRequestTypeItemDataBdeText.maximumStockSpecified = objICC002.MaxStock_PEW != null ? true : false;

                    P1createItemRequestTypeItemDataBdeText.fixedOrderQnty = objICC002.FixedOrderQty_PEW != null ? Convert.ToDecimal(objICC002.FixedOrderQty_PEW) : 0;
                    P1createItemRequestTypeItemDataBdeText.fixedOrderQntySpecified = objICC002.FixedOrderQty_PEW != null ? true : false;

                    P1createItemRequestTypeItemDataBdeText.restockLevel = objICC002.ReStockLevel_PEW != null ? Convert.ToDecimal(objICC002.ReStockLevel_PEW) : 0;
                    P1createItemRequestTypeItemDataBdeText.restockLevelSpecified = objICC002.ReStockLevel_PEW != null ? true : false;

                    P1dataArea.ItemWarehouseData[1] = P1createItemRequestTypeItemDataBdeText;
                }

                {
                    P1createItemRequestTypeItemDataBdeText = new itemWarehouseDataUpdateRequestTypeItemCreationItemWarehouseData();
                    P1createItemRequestTypeItemDataBdeText.warehouse = RanoliWarehouse;
                    P1createItemRequestTypeItemDataBdeText.safetyLevel = objICC002.SafetyLevel_RNW != null ? Convert.ToDecimal(objICC002.SafetyLevel_RNW) : 0;
                    P1createItemRequestTypeItemDataBdeText.safetyLevelSpecified = objICC002.SafetyLevel_RNW != null ? true : false;

                    P1createItemRequestTypeItemDataBdeText.reorderLevel = objICC002.ReorderLevel_RNW != null ? Convert.ToDecimal(objICC002.ReorderLevel_RNW) : 0;
                    P1createItemRequestTypeItemDataBdeText.reorderLevelSpecified = objICC002.ReorderLevel_RNW != null ? true : false;

                    P1createItemRequestTypeItemDataBdeText.maximumStock = objICC002.MaxStock_RNW != null ? Convert.ToDecimal(objICC002.MaxStock_RNW) : 0;
                    P1createItemRequestTypeItemDataBdeText.maximumStockSpecified = objICC002.MaxStock_RNW != null ? true : false;

                    P1createItemRequestTypeItemDataBdeText.fixedOrderQnty = objICC002.FixedOrderQty_RNW != null ? Convert.ToDecimal(objICC002.FixedOrderQty_RNW) : 0;
                    P1createItemRequestTypeItemDataBdeText.fixedOrderQntySpecified = objICC002.FixedOrderQty_RNW != null ? true : false;

                    P1createItemRequestTypeItemDataBdeText.restockLevel = objICC002.ReStockLevel_RNW != null ? Convert.ToDecimal(objICC002.ReStockLevel_RNW) : 0;
                    P1createItemRequestTypeItemDataBdeText.restockLevelSpecified = objICC002.ReStockLevel_RNW != null ? true : false;

                    P1dataArea.ItemWarehouseData[2] = P1createItemRequestTypeItemDataBdeText;
                }

                P1createRequest.ControlArea = P1controlArea;
                P1createRequest.DataArea = new itemWarehouseDataUpdateRequestTypeItemCreation[1];
                P1createRequest.DataArea[0] = P1dataArea;

                P1createResponse = serviceObj.itemWarehouseDataUpdate(P1createRequest);

                if (P1createResponse.DataArea[0].response.StartsWith("0"))
                {
                    //success
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "success";
                }
                else
                {
                    //error
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = P1createResponse.DataArea[0].response.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message.ToString();
            }
            finally
            {
                serviceObj = null;
            }
            return objResponseMsg;
        }

        #endregion

        #region Update Request

        public ActionResult UpdateRequest()
        {
            ViewBag.Title = "Item Code Creation";
            ViewBag.Status = "updateRequest";
            ViewBag.user = clsImplementationEnum.ICCindex.bu.GetStringValue();
            return View();
        }

        [HttpPost]
        public ActionResult SubmitUpdateRequest(int headerId, string changeDetails)
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (headerId > 0)
                {
                    //var ICC003 = db.ICC003.FirstOrDefault(o => o.HeaderId.Equals(headerId));
                    var objICC003 = new ICC003()
                    {
                        ChangeDetails = changeDetails,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now,
                        HeaderId = headerId,
                        RequestedBy = objClsLoginInfo.UserName,
                        RequestedOn = DateTime.Now,
                        RequestStatus = clsImplementationEnum.ICCStatus.Submitted.GetStringValue()
                    };
                    db.ICC003.Add(objICC003);
                    db.SaveChanges();
                    var ICC003 = db.ICC003.FirstOrDefault(o => o.HeaderId == objICC003.HeaderId);

                    //email configuration
                    MAIL001 objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.ICC.ItemCreation).FirstOrDefault();
                    if (objTemplateMaster != null)
                    {
                        Hashtable _ht = new Hashtable();
                        EmailSend _objEmail = new EmailSend();
                        _objEmail.MailToAdd = Manager.GetUserEmailByRole(clsImplementationEnum.UserRoleName.MDM3.GetStringValue()); // Get ALL MDM User Email Ids
                        _objEmail.MailCc = ICC003.ICC002.CreatedBy; // requestor id
                        _ht["[ItemCode]"] = ICC003.ICC002.ItemCode;
                        _ht["[ItemLongDescription]"] = ICC003.ICC002.LongDescription;
                        _ht["[MaterialCategory]"] = ICC003.ICC002.MaterialCategory;
                        _ht["[MaterialGroup]"] = ICC003.ICC002.MaterialGroup;
                        _ht["[DistinctItem]"] = ICC003.ICC002.DistinctItem;
                        _ht["[UOM]"] = ICC003.ICC002.UOM;
                        _ht["[ChangeDetails]"] = ICC003.ChangeDetails;
                        _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Your Request has successfuly submitted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ICC003ViewModel GetICC003Data(int? id)
        {
            ICC003ViewModel objICC003ViewModel = new ICC003ViewModel();
            var objICC003 = db.ICC003.Where(x => x.RequestId == id).FirstOrDefault();
            if (objICC003 != null)
            {
                var objICC002 = db.ICC002.FirstOrDefault(x => x.HeaderId == objICC003.HeaderId);
                objICC003ViewModel = new ICC003ViewModel()
                {
                    ApprovedBy = objICC003?.ApprovedBy,
                    ApprovedOn = objICC003?.ApprovedOn,
                    ChangeDetails = objICC003?.ChangeDetails,
                    DistinctItem = objICC002.DistinctItem,
                    HeaderId = objICC002.HeaderId,
                    ItemCode = objICC002.ItemCode,
                    ItemCodeRequestor = objICC002.ItemCodeRequestor,
                    ItemSubType = objICC002.ItemSubType,
                    ItemType = objICC002.ItemType,
                    LongDescription = objICC002.LongDescription,
                    MaterialCategory = objICC002.MaterialCategory,
                    MaterialGroup = objICC002.MaterialGroup,
                    SafetyLevel_HZW = objICC002.SafetyLevel_HZW,
                    SafetyLevel_PEW = objICC002.SafetyLevel_PEW,
                    SafetyLevel_RNW = objICC002.SafetyLevel_RNW,
                    ReorderLevel_HZW = objICC002.ReorderLevel_HZW,
                    ReorderLevel_PEW = objICC002.ReorderLevel_PEW,
                    ReorderLevel_RNW = objICC002.ReorderLevel_RNW,
                    MaxStock_HZW = objICC002.MaxStock_HZW,
                    MaxStock_PEW = objICC002.MaxStock_PEW,
                    MaxStock_RNW = objICC002.MaxStock_RNW,
                    FixedOrderQty_HZW = objICC002.FixedOrderQty_HZW,
                    FixedOrderQty_PEW = objICC002.FixedOrderQty_PEW,
                    FixedOrderQty_RNW = objICC002.FixedOrderQty_RNW,
                    ReStockLevel_HZW = objICC002.ReStockLevel_HZW,
                    ReStockLevel_PEW = objICC002.ReStockLevel_PEW,
                    ReStockLevel_RNW = objICC002.ReStockLevel_RNW,
                    Status = objICC002.Status,
                    NatureOfItem = objICC002.NatureOfItem,
                    RequestedBy = objICC003?.RequestedBy,
                    RequestedOn = objICC003?.RequestedOn,
                    RequestId = objICC003 != null ? objICC003.RequestId : 0,
                    RequestStatus = objICC003?.RequestStatus,
                    ReturnedBy = objICC003?.ReturnedBy,
                    ReturnedOn = objICC003?.ReturnedOn,
                    ReturnRemarks = objICC003?.ReturnRemarks,
                    ShortDescription = objICC002.ShortDescription,
                    UOM = objICC002.UOM,
                };
            }
            return objICC003ViewModel;
        }

        public ActionResult UpdateRequestPending(int? id, string u)
        {
            if (id > 0)
            {
                var data = db.ICC003.Where(x => x.RequestId == id).FirstOrDefault();
                ViewBag.Status = data.RequestStatus;
                ViewBag.Action = "update";
                ViewBag.Title = "Item Update Request Pending";
                ViewBag.Redirect = u;
                List<string> lstNatureOfItem = clsImplementationEnum.getICC_NatureOfItem().ToList();
                //ViewBag.ItemCodeRequestor = Manager.GetPsidandDescription(data.ICC002.ItemCodeRequestor);
                ViewBag.ItemCodeRequestor = data.ICC002.ItemCodeRequestor;
                ViewBag.NatureOfItem = lstNatureOfItem.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();
                return View(GetICC003Data(id));
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult ActionByMDM(int requestId, string remarks, string status)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string message = string.Empty;
                ICC003 objICC003 = db.ICC003.Where(x => x.RequestId == requestId).FirstOrDefault();
                if (objICC003.RequestStatus == clsImplementationEnum.ICCStatus.Submitted.GetStringValue())
                {
                    if (status == clsImplementationEnum.ICCStatus.Approved.GetStringValue())
                    {
                        objICC003.RequestStatus = clsImplementationEnum.ICCStatus.Approved.GetStringValue();
                        objICC003.ApprovedBy = objClsLoginInfo.UserName;
                        objICC003.ApprovedOn = DateTime.Now;
                        objICC003.ReturnRemarks = remarks;
                        db.SaveChanges();
                        message = "Item update request has been approved.";
                    }
                    if (status == clsImplementationEnum.ICCStatus.Return.GetStringValue())
                    {
                        objICC003.RequestStatus = clsImplementationEnum.ICCStatus.Return.GetStringValue();
                        objICC003.ReturnedBy = objClsLoginInfo.UserName;
                        objICC003.ReturnedOn = DateTime.Now;
                        objICC003.ReturnRemarks = remarks;
                        db.SaveChanges();
                        message = "Item update request has been returned.";
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = message;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.StatusChanged;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Report
        public ActionResult Report()
        {
            return View();
        }
        #endregion

        #region Export to Excel

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                var lst = db.SP_ICC_GET_ITEM_CREATION_INDEX_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Data Found";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                switch ((clsImplementationEnum.GridType)Enum.Parse(typeof(clsImplementationEnum.GridType), gridType))
                {
                    case clsImplementationEnum.GridType.GetItemUpdateCreated:
                        var itemUpdateCreatedList = (from uc in lst
                                                     select new
                                                     {
                                                         CreatedOn = uc.CreatedOn.HasValue ? uc.CreatedOn.Value.ToString("dd/MM/yyyy") : "",
                                                         ItemCode = Convert.ToString(uc.ItemCode),
                                                         LongDescription = (uc.LongDescription.Length > 15 ? uc.LongDescription.Substring(0, 15) + "..." : Convert.ToString(uc.LongDescription)),
                                                         MaterialCategory = Convert.ToString(uc.MaterialCategory),
                                                         MaterialGroup = Convert.ToString(uc.MaterialGroup),
                                                         DistinctItem = Convert.ToString(uc.DistinctItem),
                                                         UOM = Convert.ToString(uc.UOM),
                                                         Status = Convert.ToString(uc.Status),
                                                         RequestStatus = uc.RequestStatus
                                                     }).ToList();

                        strFileName = Helper.GenerateExcel(itemUpdateCreatedList, objClsLoginInfo.UserName);
                        break;
                    case clsImplementationEnum.GridType.GetItemUpdateResquest:
                        var itemUpdateResquestList = (from uc in lst
                                                      select new
                                                      {
                                                          CreatedOn = uc.CreatedOn.HasValue ? uc.CreatedOn.Value.ToString("dd/MM/yyyy") : "",
                                                          ItemCode = Convert.ToString(uc.ItemCode),
                                                          LongDescription = (uc.LongDescription.Length > 15 ? uc.LongDescription.Substring(0, 15) + "..." : Convert.ToString(uc.LongDescription)),
                                                          MaterialCategory = Convert.ToString(uc.MaterialCategory),
                                                          MaterialGroup = Convert.ToString(uc.MaterialGroup),
                                                          DistinctItem = Convert.ToString(uc.DistinctItem),
                                                          UOM = Convert.ToString(uc.UOM),
                                                          Status = Convert.ToString(uc.Status),
                                                          //RequestStatus = uc.RequestStatus
                                                      }).ToList();

                        strFileName = Helper.GenerateExcel(itemUpdateResquestList, objClsLoginInfo.UserName);
                        break;
                    case clsImplementationEnum.GridType.GetItemUpdateApproval:
                        var itemUpdateApprovalList = (from uc in lst
                                                      where uc.RequestStatus != "" && uc.RequestStatus != null && (uc.RequestStatus.Contains(clsImplementationEnum.ICCStatus.Submitted.GetStringValue()) ||
                                                       uc.RequestStatus.Contains(clsImplementationEnum.ICCStatus.Return.GetStringValue()))
                                                      select new
                                                      {
                                                          CreatedOn = uc.CreatedOn.HasValue ? uc.CreatedOn.Value.ToString("dd/MM/yyyy") : "",
                                                          ItemCode = Convert.ToString(uc.ItemCode),
                                                          LongDescription = (uc.LongDescription.Length > 15 ? uc.LongDescription.Substring(0, 15) + "..." : Convert.ToString(uc.LongDescription)),
                                                          MaterialCategory = Convert.ToString(uc.MaterialCategory),
                                                          MaterialGroup = Convert.ToString(uc.MaterialGroup),
                                                          DistinctItem = Convert.ToString(uc.DistinctItem),
                                                          UOM = Convert.ToString(uc.UOM),
                                                          RequestStatus = uc.RequestStatus
                                                      }).ToList();

                        strFileName = Helper.GenerateExcel(itemUpdateApprovalList, objClsLoginInfo.UserName);
                        break;
                    case clsImplementationEnum.GridType.GetItemNotCreated:
                        var itemNotCreatedList = (from uc in lst
                                                  where uc.Status.Equals(clsImplementationEnum.ICCStatus.Draft.GetStringValue()) ||
                                                  uc.Status.Equals(clsImplementationEnum.ICCStatus.Not_Created.GetStringValue())
                                                  select new
                                                  {
                                                      CreatedOn = uc.CreatedOn.HasValue ? uc.CreatedOn.Value.ToString("dd/MM/yyyy") : "",
                                                      ItemCode = Convert.ToString(uc.ItemCode),
                                                      LongDescription = (uc.LongDescription.Length > 15 ? uc.LongDescription.Substring(0, 15) + "..." : Convert.ToString(uc.LongDescription)),
                                                      MaterialCategory = Convert.ToString(uc.MaterialCategory),
                                                      MaterialGroup = Convert.ToString(uc.MaterialGroup),
                                                      DistinctItem = Convert.ToString(uc.DistinctItem),
                                                      UOM = Convert.ToString(uc.UOM),
                                                      Status = Convert.ToString(uc.Status),
                                                      //RequestStatus = uc.RequestStatus
                                                  }).ToList();

                        strFileName = Helper.GenerateExcel(itemNotCreatedList, objClsLoginInfo.UserName);
                        break;
                    case clsImplementationEnum.GridType.GetAllItem:
                        var allItemList = (from uc in lst
                                           select new
                                           {
                                               CreatedOn = uc.CreatedOn.HasValue ? uc.CreatedOn.Value.ToString("dd/MM/yyyy") : "",
                                               ItemCode = Convert.ToString(uc.ItemCode),
                                               LongDescription = (uc.LongDescription.Length > 15 ? uc.LongDescription.Substring(0, 15) + "..." : Convert.ToString(uc.LongDescription)),
                                               MaterialCategory = Convert.ToString(uc.MaterialCategory),
                                               MaterialGroup = Convert.ToString(uc.MaterialGroup),
                                               DistinctItem = Convert.ToString(uc.DistinctItem),
                                               UOM = Convert.ToString(uc.UOM),
                                               Status = Convert.ToString(uc.Status)
                                           }).ToList();

                        strFileName = Helper.GenerateExcel(allItemList, objClsLoginInfo.UserName);
                        break;
                }


                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region TimeLine
        public ActionResult ShowTimeline(int RequestId)
        {
            var model = new TimelineViewModel();
            model.Title = "ICC-UpdateRequest";
            model.TimelineTitle = "ICC Timeline";

            if (RequestId > 0)
            {
                var ObjICC003 = db.ICC003.FirstOrDefault(x => x.RequestId.Equals(RequestId));
                var objICC002 = db.ICC002.Where(x => x.HeaderId == ObjICC003.HeaderId).FirstOrDefault();
                if (objICC002 != null)
                {
                    model.CreatedBy = objICC002.CreatedBy != null ? Manager.GetUserNameFromPsNo(objICC002.CreatedBy) : null;
                    model.CreatedOn = objICC002.CreatedOn;
                    model.EditedBy = objICC002.EditedBy != null ? Manager.GetUserNameFromPsNo(objICC002.EditedBy) : null;
                    model.EditedOn = objICC002.EditedOn;
                    model.LNSubmittedBy = objICC002.LNSubmittedBy != null ? Manager.GetUserNameFromPsNo(objICC002.LNSubmittedBy) : null;
                    model.LNSubmittedOn = objICC002.LNSubmittedOn;
                }
                if (ObjICC003 != null)
                {
                    model.RequestGeneratedBy = ObjICC003.RequestedBy != null ? Manager.GetUserNameFromPsNo(ObjICC003.RequestedBy) : null;
                    model.RequestGeneratedBy = ObjICC003.RequestedBy != null ? Manager.GetUserNameFromPsNo(ObjICC003.RequestedBy) : null;
                    model.RequestGeneratedOn = ObjICC003.RequestedOn;
                    model.ReturnedBy = ObjICC003.ReturnedBy != null ? Manager.GetUserNameFromPsNo(ObjICC003.ReturnedBy) : null;
                    model.ReturnedOn = ObjICC003.ReturnedOn;
                    model.ApprovedBy = ObjICC003.ApprovedBy != null ? Manager.GetUserNameFromPsNo(ObjICC003.ApprovedBy) : null; ;
                    model.ApprovedOn = ObjICC003.ApprovedOn;
                }

            }
            TempData.Keep("Title");
            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        #endregion

        #region Timeline History

        public ActionResult GetTimelineHistoryPopup(string ItemCode)
        {
            ViewBag.ItemCode = ItemCode;
            return PartialView("_GetTimelineGridDataPartial");
        }

        [HttpPost]
        public JsonResult LoadTimelineDataGrid(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = " 1=1 ";

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                string _urlform = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                //Search Condition 
                if (!string.IsNullOrWhiteSpace(param.Status))
                {
                    strWhereCondition += " AND ItemCode = '" + param.Status + "'";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = { "Action", "[dbo].[GET_USERNAME_BY_PSNO](ActionBy)" };
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }

                var lstResult = db.SP_ICC_TIMELINE_DATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.Action),
                            Convert.ToString(uc.ActionBy),
                            (uc.ActionOn.HasValue ? uc.ActionOn.Value.ToString("dd/MM/yyyy hh:mm tt") :  "")
                          }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion
        #region LOG
        public void MaintainLog_ICC002(ICC002_Log icc002_log)
        {
            db.ICC002_Log.Add(icc002_log);
            db.SaveChanges();
        }
        #endregion
    }
}