﻿using System.Web.Http;
using System.Web.Mvc;

namespace IEMQS.Areas.ICC
{
    public class ICCAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "ICC";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            //API Routing
            context.Routes.MapHttpRoute(
                    name: "ICC_api_action",
                    routeTemplate: "ICC/api/{controller}/{action}"
                );

            context.Routes.MapHttpRoute(
                    name: "ICC_api_default",
                    routeTemplate: "ICC/api/{controller}"
                );
            // Normal Routing
            context.MapRoute(
                "ICC_default",
                "ICC/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}