﻿using System.Web.Mvc;

namespace IEMQS.Areas.SFM
{
    public class SFMAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SFM";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SFM_default",
                "SFM/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}