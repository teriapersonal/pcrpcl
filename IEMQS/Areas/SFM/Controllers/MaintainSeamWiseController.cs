﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQS.Models;
using IEMQSImplementation;
using IEMQSImplementation.SFM;
using static IEMQSImplementation.clsImplementationEnum;

namespace IEMQS.Areas.SFM.Controllers
{
    public class MaintainSeamWiseController : clsBase
    {
        // GET: SFM/MaintainSeamWise
        SFMEntitiesContext dbSFM = new SFMEntitiesContext();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetSeamAndStageDataGridPartial(int ShellId, string Type = "")
        {
            ViewBag.ShellId = ShellId;
            ViewBag.Type = Type;
            return PartialView("_GetSFMSeamAndStageDataGridPartial");
        }

        public ActionResult GetUIDselection(int ShellId, string Type = "", string Role = "")
        {
            //ViewBag.ShellId = ShellId;
            ViewBag.ShellId = ShellId;
            ViewBag.Type = Type;
            ViewBag.Role = Role;
            if (Role == "")
                ViewBag.UIDDisable = "disabled";
            else
                ViewBag.UIDDisable = "";
            var ProjDetails = (from a in dbSFM.SFM001
                               where a.ShellId == ShellId
                               select new
                               {
                                   Project = a.Project,
                                   PositionNo = a.PositionNo,
                                   ActualStartDate = a.ActualStartDate,
                                   ActualEndDate = a.ActualEndDate,
                                   PlannedStartDate = a.PlannedStartDate,
                                   PlannedEndDate = a.PlannedEndDate,
                                   UID = a.UID
                               }).Distinct().ToList();
            ViewBag.Project = ProjDetails[0].Project;
            ViewBag.PositionNo = ProjDetails[0].PositionNo;
            ViewBag.UID = ProjDetails[0].UID;
            ViewBag.ActualStartDate = (ProjDetails[0].ActualStartDate.HasValue ? ProjDetails[0].ActualStartDate.Value.ToString("dd/MM/yyyy") : ""); ;
            ViewBag.ActualEndDate = (ProjDetails[0].ActualEndDate.HasValue ? ProjDetails[0].ActualEndDate.Value.ToString("dd/MM/yyyy") : ""); ;
            ViewBag.PlannedStartDate = (ProjDetails[0].PlannedStartDate.HasValue ? ProjDetails[0].PlannedStartDate.Value.ToString("dd/MM/yyyy") : ""); ;
            ViewBag.PlannedEndDate = (ProjDetails[0].PlannedEndDate.HasValue ? ProjDetails[0].PlannedEndDate.Value.ToString("dd/MM/yyyy") : "");
            return PartialView("_SaveDetailsWithUIDPartial");
        }

        public ActionResult LoadSeamAndStageList(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string status = param.Status;
                int ShellId = Convert.ToInt32(param.Headerid);
                string whereCondition = "1=1 ";

                string[] columnName = { "q40.StageCode", "q2.StageDesc", "qms.SeamNo" };

                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                //var objFKM102 = db.FKM102.Where(i => i.NodeId == NodeId).FirstOrDefault();
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                List<SP_SFM_GET_SEAM_AND_STAGE_LIST_Result> lstPam = new List<SP_SFM_GET_SEAM_AND_STAGE_LIST_Result>();
                List<SP_SFM_GET_SEAM_AND_STAGE_LIST_Result> lstPam2 = new List<SP_SFM_GET_SEAM_AND_STAGE_LIST_Result>();
                if (!string.IsNullOrEmpty(param.Title))
                {
                    if (param.Title == "SFM")
                    {

                        var lstsfm = dbSFM.SFM001.Where(x => x.ShellId == ShellId).Select(x => new { Project = x.Project, QualityProject = x.QualityProject, PositionNo = x.PositionNo,SeamNo=x.SeamNo }).FirstOrDefault();
                        lstPam = dbSFM.SP_SFM_GET_SEAM_AND_STAGE_LIST(StartIndex, EndIndex, strSortOrder, whereCondition, lstsfm.Project, (lstsfm.QualityProject != null ? lstsfm.QualityProject : ""), lstsfm.PositionNo).ToList();
                        lstPam2= lstPam.Where(y => y.SeamNo == lstsfm.SeamNo && y.Project == lstsfm.Project).ToList();
                    }
                }
                //else
                //{
                //    var lstcop = db.COP003.Where(x => x.RefHeaderId == headerid).Select(x => new { Project = x.Project, QualityProject = x.QualityProject, Part = x.PartNos }).FirstOrDefault();
                //    lstPam = db.SP_COP_GET_SEAM_AND_STAGE_LIST(StartIndex, EndIndex, strSortOrder, whereCondition, lstcop.Project, (lstcop.QualityProject != null ? lstcop.QualityProject : ""), lstcop.Part).ToList();
                //}


                if (lstPam2.Count == 0)
                {
                    return Json(new { sEcho = param.sEcho, iTotalDisplayRecords = "0", iTotalRecords = "0", aaData = new string[] { } }, JsonRequestBehavior.AllowGet);
                }

                var lstSeamAndStage = new List<SeamAndStageEnt>();

                #region New UI
                foreach (var item in lstPam2.Select(i => i.SeamNo).Distinct())
                {
                    lstSeamAndStage.Add(new SeamAndStageEnt { SeamNo = item, StageDesc = GetAllSeamWiseStagesInTable(item, lstPam2) });
                }
                var lstSFM = lstPam2.GroupBy(l => new { l.Project })
                          .Select(x => new
                          {
                              Project = x.FirstOrDefault().Project,
                              Position = x.FirstOrDefault().Position,
                              GROUP_NO = x.FirstOrDefault().GROUP_NO,
                              InspectionStatus = x.FirstOrDefault().InspectionStatus,
                              SeamNo = x.FirstOrDefault().SeamNo,
                              StageCode = x.FirstOrDefault().StageCode,
                              StageDesc = x.FirstOrDefault().StageDesc,
                              TotalCount = x.Count()
                          }).ToList();

                int ? totalRecords = lstSFM.Select(i => i.TotalCount).FirstOrDefault();
                var res = (from h in lstSeamAndStage
                           select new[] {
                                       Convert.ToString(h.SeamNo),
                                       Convert.ToString(h.StageDesc),
                           }).ToList();
                #endregion

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string GetAllSeamWiseStagesInTable(string SeamNo, List<SP_SFM_GET_SEAM_AND_STAGE_LIST_Result> lstPam2)
        {
            string StageTable = "";
            try
            {
                var strClearStatus = SeamListInspectionStatus.Cleared.GetStringValue();
                var listStages = lstPam2.Where(i => i.SeamNo == SeamNo);
                //StageTable = "<table class='tblstages'><tr>";
                foreach (var item in listStages)
                {
                    var tdClass = item.InspectionStatus == strClearStatus ? "partseamstage" : "";
                    //StageTable += "<div class='" + tdClass + "' style='padding:10px;display:inline-block;margin-right:10px;'>" + item.StageCode + "-" + item.StageDesc + "</div>";
                    StageTable += "<div class='" + tdClass + "' style='padding:10px;display:inline-block;margin-right:10px;' title=" + item.StageCode + "-" + item.StageDesc + ">" + item.StageDesc + "</div>";
                }
                //StageTable += "</tr></table>";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return StageTable;
        }

        public JsonResult GetTaskUIDDetails(string Project, string UID = "")
        {
            var itemDetails = GetUIDDetails(Project, UID);
            return Json(itemDetails, JsonRequestBehavior.AllowGet);
        }

        public List<GET_DATES_BY_TASKUNIQUEID> GetUIDDetails(string Project, string UID)
        {
            //Project = "S040411";
            SqlConnection objConn = new SqlConnection(Convert.ToString(ConfigurationManager.ConnectionStrings["LNConcerto"]));
            List<GET_DATES_BY_TASKUNIQUEID> lstUIDDetails = new List<GET_DATES_BY_TASKUNIQUEID>();
            try
            {

                objConn.Open();
                SqlCommand objCmd = new SqlCommand();
                objCmd.Connection = objConn;
                objCmd.CommandType = CommandType.Text;
                SqlDataAdapter dataAdapt = new SqlDataAdapter();
                DataTable dataTable = new DataTable();
                //objCmd.CommandText = "select a.TASKUNIQUEID,a.contact, a.NAME [TaskName] from PROJ_TASK a with(nolock)  where a.contact in ('" + objESP113.TaskManager.Replace(",", "','") + "') and contact is not null";
                objCmd.CommandText = "select a.ActualStart,a.ActualFinish,a.EXPECTED_START_DATE,a.EXPECTED_END_DATE from PROJ_TASK a with(nolock)  where a.TASKUNIQUEID in (" + UID + ") and a.PROJECTNAME='" + Project + "'";
                dataAdapt.SelectCommand = objCmd;
                dataAdapt.Fill(dataTable);

                if (dataTable != null && dataTable.Rows.Count > 0)
                {
                    lstUIDDetails = (from DataRow row in dataTable.Rows
                                     select new GET_DATES_BY_TASKUNIQUEID
                                     {
                                         ACTUALSTART = Convert.ToDateTime(row["ACTUALSTART"].ToString()).ToString("dd/MM/yyyy").ToString(),
                                         ACTUALFINISH = Convert.ToDateTime(row["ACTUALFINISH"].ToString()).ToString("dd/MM/yyyy").ToString(),
                                         PLANNEDSTARTDATE = Convert.ToDateTime(row["EXPECTED_START_DATE"].ToString()).ToString("dd/MM/yyyy").ToString(),
                                         PLANNEDENDDATE = Convert.ToDateTime(row["EXPECTED_END_DATE"].ToString()).ToString("dd/MM/yyyy").ToString()
                                     }).ToList();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            finally
            {
                if (objConn.State == ConnectionState.Open)
                    objConn.Close();
            }
            return lstUIDDetails;
        }

        [HttpPost]
        public ActionResult SaveDatesWithUID(FormCollection fc)
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var sfm001 = new SFM001();
                int ShellId = Convert.ToInt32(fc["ShellId"]);
                SFM001 dbsfm = dbSFM.SFM001.Where(x => x.ShellId == ShellId).FirstOrDefault();
                dbsfm.UID = fc["UID"];
                dbsfm.PlannedEndDate = Convert.ToDateTime(fc["PlannedEnd"]);
                dbsfm.PlannedStartDate = Convert.ToDateTime(fc["PlannedStart"]);
                dbsfm.ActualStartDate = Convert.ToDateTime(fc["ActualStart"]);
                dbsfm.ActualEndDate = Convert.ToDateTime(fc["ActualFinish"]);
                dbsfm.ShellId = ShellId;
                dbSFM.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg);
        }

        public class SeamAndStageEnt : SP_SFM_GET_SEAM_AND_STAGE_LIST_Result
        {
            public bool IsParent { get; set; }
            public bool IsClear { get; set; }
        }

        public partial class GET_DATES_BY_TASKUNIQUEID
        {
            public string Project { get; set; }
            public double TASKUNIQUEID { get; set; }
            public string ACTUALSTART { get; set; }

            public string TaskUID { get; set; }
            public string ACTUALFINISH { get; set; }

            public string PLANNEDSTARTDATE { get; set; }

            public string PLANNEDENDDATE { get; set; }
        }

    }
}