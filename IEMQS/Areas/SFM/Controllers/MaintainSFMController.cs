﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using IEMQSImplementation.SFM;
using IEMQS.Areas.Utility.Controllers;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using static IEMQS.Areas.SFM.Controllers.MaintainSeamWiseController;

namespace IEMQS.Areas.SFM.Controllers
{
    public class MaintainSFMController : clsBase
    {
        SFMEntitiesContext dbSFM = new SFMEntitiesContext();
        //ESPEntitiesContext dbESP = new ESPEntitiesContext();
        // GET: SFM/MaintainSFM
        // GET: COP/MaintainComponentOverlay
        [UserPermissions, SessionExpireFilter]
        public ActionResult IndexJP()
        {

            ViewBag.Title = "Maintain Shell Fabrication Plan";
            BindDropDown(null);
            var objSFM001 = new SFM001();

            ViewBag.Role = "JP";
            return View("Index", objSFM001);
        }

        [UserPermissions, SessionExpireFilter]
        public ActionResult IndexFSP()
        {

            ViewBag.Title = "Maintain Shell Fabrication Schedule";
            BindDropDown(null);
            var objSFM001 = new SFM001();
            ViewBag.Role = "FSP";
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;
            if (ViewBag.Role == "FSP" && ViewBag.UserRole != "PROD3")
            {
                ViewBag.IsDisplay = false;
            }
            if (ViewBag.UserRole == "PROD3")
            {
                ViewBag.IsDisplay = true;
            }
            return View("Index", objSFM001);
        }

        [UserPermissions, SessionExpireFilter]
        public ActionResult Index()
        {

            ViewBag.Title = "Display Shell Fabrication";
            BindDropDown(null);
            var objSFM001 = new SFM001();
            ViewBag.IsDisplay = true;
            return View("Index", objSFM001);
        }

        [HttpPost]
        public ActionResult LoadDataGridPartial(string status, string role, bool isDisplay)
        {
            ViewBag.Status = status;
            ViewBag.Role = role;
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;
            ViewBag.IsDisplay = isDisplay;
            //ViewBag.MachineCode = db.COP002.Select(x => new { label = x.MachineCode + "-" + x.MachineName, id = x.MachineCode, value = x.CameraURL }).Distinct().ToList();

            BindDropDown(null);
            return PartialView("_GetSFMGridDataPartial");
        }

        public string MakeDatatableForSFMSearch(string jsonSearchFilter)
        {
            string strWhereCondition = string.Empty;
            try
            {
                List<SearchFilter> searchflt = new JavaScriptSerializer().Deserialize<List<SearchFilter>>(jsonSearchFilter);
                foreach (var item in searchflt)
                {
                    if (!string.IsNullOrWhiteSpace(item.Value) && item.Value != "false")
                    {
                        strWhereCondition += Manager.MakeDatatableSearchWithMultipleCondition(item.FilterType, item.ColumnName, item.Value, item.DataType);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
            return strWhereCondition;
        }

        [HttpPost]
        public ActionResult LoadDataGridData(JQueryDataTableParamModel param, string dispatchStatus, string Role)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                strWhereCondition += "1=1 ";

                #endregion

                //search Condition 

                string[] columnName = { "Project","BU","PositionNo","ShellMark","PlannedReceipt", "WorkCenter", "CsWep", "LsWep", "EdgeBreak","PCRNO","PCLNumber","PCLStatus","Length",
                    "Width","Thk","Weight","MOC","RollingType","RollingMachine","SeamNo","SeamStatus",
                    "PlannedStartDate","PlannedEndDate","ActualStartDate","ActualEndDate","EdgeBracking",
                    "Rolling","Setup","Welding","NDT","Dispatch","DispatchDate","DeliveryLocation","cm006.t_nama" };
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhereCondition += MakeDatatableForSFMSearch(param.SearchFilter);
                }

                if (!string.IsNullOrEmpty(dispatchStatus))
                {
                    strWhereCondition += " AND Dispatch = '" + dispatchStatus + "'";
                }

                var lstResult = dbSFM.SP_SFM_GET_INDEX_DATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();
                var dtPlannedReceipt = new DateTime();
                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ShellId),
                                uc.Project,
                                uc.QualityProject,
                                uc.Customer,
                                uc.BU,
                                uc.PositionNo,
                                uc.ShellMark,
                                uc.PlannedReceipt,
                                uc.WorkCenter,
                                uc.CsWep,
                                uc.LsWep,
                                uc.EdgeBreak,
                                uc.PCRNO,
                                uc.PCLNumber,
                                uc.PCLStatus,
                                //GenerateCheckbox(uc.ShellId,"IsPlanned",uc.IsPlanned,true),
                                //uc.MOC,
                                Convert.ToString(uc.Length),
                                Convert.ToString(uc.Width),
                                Convert.ToString(uc.Thk),
                               Convert.ToString(uc.Weight),
                               uc.MOC,
                                uc.RollingType,
                                uc.RollingMachine,
                                uc.SeamNo,
                                uc.SeamStatus,
                                //GenerateCheckbox(uc.ShellId,"IsPlanned",Convert.ToBoolean(uc.IsPlanned),true),
                                //(DateTime.TryParse(uc.PlannedReceipt+"",out dtPlannedReceipt) ? dtPlannedReceipt.ToString("dd/MM/yyyy") : uc.PlannedReceipt),

                                uc.PlannedStartDate.HasValue? uc.PlannedStartDate.Value.ToString("dd/MM/yyyy"):"",
                                uc.PlannedEndDate.HasValue? uc.PlannedEndDate.Value.ToString("dd/MM/yyyy"):"",
                                uc.ActualStartDate.HasValue? uc.ActualStartDate.Value.ToString("dd/MM/yyyy"):"",
                                uc.ActualEndDate.HasValue? uc.ActualEndDate.Value.ToString("dd/MM/yyyy"):"",

                                uc.CS_WEP_M_C,
                                uc.EdgeBracking,
                                uc.L_S_WEP_M_C,
                                uc.Rolling,
                                uc.Setup,
                                uc.Welding,
                                uc.NDT,
                                uc.Dispatch,
                                uc.DispatchDate.HasValue? uc.DispatchDate.Value.ToString("dd/MM/yyyy"):"",
                                uc.DeliveryLocation,
                                (Role =="JP")? "<span>" +
                                 "<a id='View" + uc.ShellId + "' name='View' title='View' class='blue action' onclick='ViewRecord(" + uc.ShellId + ")'>"+
                                        "<i class='fa fa-eye'></i></a> &nbsp &nbsp"+
                                         "<a id='UIDSelection" + uc.ShellId + "' name='UIDSelection' title=\"UID\" class=\"blue action\" onclick=\"UIDSelection(this," + uc.ShellId +")\">"+
                                        "<i class=\"iconclass fa fa-bars\"></i></a> &nbsp &nbsp"+
                                         "<a id='Seam" + uc.ShellId + "' name='Seam' title=\"Seam\" class=\"blue action\" onclick=\"Seamlist(this," + uc.ShellId +")\">"+
                                        "<i class=\"iconclass fa fa-th\"></i></a>" +
                                "</span>" : (Role =="FSP")? "<span>" +
                                 "<a id='View" + uc.ShellId + "' name='View' title='View' class='blue action' onclick='ViewRecord(" + uc.ShellId + ")'>"+
                                        "<i class='fa fa-eye'></i></a> &nbsp &nbsp"+
                                         "<a id='UIDSelection" + uc.ShellId + "' name='UIDSelection' title=\"UID\" class=\"blue action\" onclick=\"UIDSelection(this," + uc.ShellId +")\">"+
                                        "<i class=\"iconclass fa fa-bars\"></i></a> &nbsp &nbsp"+
                                         "<a id='Seam" + uc.ShellId + "' name='Seam' title=\"Seam\" class=\"blue action\" onclick=\"Seamlist(this," + uc.ShellId +")\">"+
                                        "<i class=\"iconclass fa fa-th\"></i></a>" +
                                "</span>" :"<span>" +
                                 "<a id='View" + uc.ShellId + "' name='View' title='View' class='blue action' onclick='ViewRecord(" + uc.ShellId + ")'>"+
                                        "<i class='fa fa-eye'></i></a> &nbsp &nbsp"+
                                          "<a id='UIDSelection" + uc.ShellId + "' name='UIDSelection' title=\"UID\" class=\"blue action\" onclick=\"UIDSelection(this," + uc.ShellId +")\">"+
                                        "<i class=\"iconclass fa fa-bars\"></i></a> &nbsp &nbsp"+
                                         "<a id='Seam" + uc.ShellId + "' name='Seam' title=\"Seam\" class=\"blue action\" onclick=\"Seamlist(this," + uc.ShellId +")\">"+
                                        "<i class=\"iconclass fa fa-th\"></i></a>" +
                                "</span>",

                          }).ToList();
                // data.Insert(0, null);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult GetSeamNosP(string Project, string PositionNo, string QualityProject, string search = "")
        {

            List<ddlValue> lstSeamNo = (from a in db.QMS012
                                        join b in db.QMS011 on a.Project equals b.Project
                                        where a.SeamCategory == "LW" && b.Project == Project && a.QualityProject == QualityProject &&
                                        ((a.Position.Contains("," + PositionNo) && a.Position.Contains(PositionNo + ",") && a.Position == PositionNo)
                                        || (a.Position.Contains("," + PositionNo) && a.Position.Contains(PositionNo + ","))
                                        || (a.Position.Contains(PositionNo + ",") && a.Position == PositionNo)
                                        || (a.Position.Contains("," + PositionNo) && a.Position == PositionNo)
                                        || (a.Position == PositionNo || a.Position.Contains("," + PositionNo) || a.Position.Contains(PositionNo + ",")))
                                        select new ddlValue { id = a.SeamNo, text = a.SeamNo }).Distinct().ToList();//&& i.CreatedBy.Equals(username,StringComparison.OrdinalIgnoreCase)

            //List<ddlValue> lstQualityProject = db.QMS010.Where(i => (search == "" || i.QualityProject.ToLower().Contains(search.ToLower()))
            //                                                     && i.Project == Project
            //                                                     && i.Location == objClsLoginInfo.Location
            //                                                   //&& i.BU == BU
            //                                                   ).Select(i => new ddlValue { id = i.QualityProject, text = i.QualityProject }).Distinct().ToList();//&& i.CreatedBy.Equals(username,StringComparison.OrdinalIgnoreCase)
            return Json(lstSeamNo, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult GetPCRNO(string Project, string PositionNo, string search = "")
        //{

        //    List<ddlValue> lstSeamNo = (from a in db.QMS012
        //                                join b in db.QMS011 on a.Project equals b.Project
        //                                where a.SeamCategory == "LW" && b.Project == Project && a.Position.Contains(PositionNo + ",")
        //                                select new ddlValue { id = a.SeamNo, text = a.SeamNo }).Distinct().ToList();//&& i.CreatedBy.Equals(username,StringComparison.OrdinalIgnoreCase)

        //    //List<ddlValue> lstQualityProject = db.QMS010.Where(i => (search == "" || i.QualityProject.ToLower().Contains(search.ToLower()))
        //    //                                                     && i.Project == Project
        //    //                                                     && i.Location == objClsLoginInfo.Location
        //    //                                                   //&& i.BU == BU
        //    //                                                   ).Select(i => new ddlValue { id = i.QualityProject, text = i.QualityProject }).Distinct().ToList();//&& i.CreatedBy.Equals(username,StringComparison.OrdinalIgnoreCase)
        //    return Json(lstSeamNo, JsonRequestBehavior.AllowGet);
        //}

        public ActionResult GetPCRNO(string Project, string PositionNo)
        {
            //Project = "S040411";
            //SqlConnection objConn = new SqlConnection(Convert.ToString(ConfigurationManager.ConnectionStrings["LNConcerto2"]));
            var LNLinkedServer = Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.LNLinkedServer.GetStringValue());
            string LNCompanyId = Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.LNCompanyId.GetStringValue());
            //SqlConnection objConn = new SqlConnection(LNLinkedServer);
            //List<GET_PCRDetails> lstPCRNODetails = new List<GET_PCRDetails>();
            //List<ddlValue> lstPCRNODetails = new List<ddlValue>();
            var lstPCRNODetails = new List<GET_PCRDetails>();
            try
            {

                //objConn.Open();
                //SqlCommand objCmd = new SqlCommand();
                //objCmd.Connection = objConn;
                //objCmd.CommandType = CommandType.Text;
                //SqlDataAdapter dataAdapt = new SqlDataAdapter();
                //DataTable dataTable = new DataTable();
                ////objCmd.CommandText = "select a.TASKUNIQUEID,a.contact, a.NAME [TaskName] from PROJ_TASK a with(nolock)  where a.contact in ('" + objESP113.TaskManager.Replace(",", "','") + "') and contact is not null";
                //objCmd.CommandText = "select a.t_pcrn from tltsfc506175 a where a.t_cprj ='" + Project + "' and a.t_sern='" + PositionNo + "'";
                //dataAdapt.SelectCommand = objCmd;
                //dataAdapt.Fill(dataTable);
                String term = "";
                //if (dataTable != null && dataTable.Rows.Count > 0)
                //{
                //    lstPCRNODetails = (from DataRow row in dataTable.Rows
                //                       select new ddlValue { id = row["t_pcrn"].ToString(), text = row["t_pcrn"].ToString() }).Distinct().ToList();
                //}
                //lstPO = db.Database.SqlQuery<SelectItemList>("Select t_pcrn from tltsfc506175 from " + LNLinkedServer + ".dbo.tltsfc506175" + LNCompanyId + " Where t_cprj like ('%" + term + "%') order by t_cprj").Take(20).ToList()
                //lstPO = db.Database.SqlQuery<ddlValue>("Select t_pcrn as Value from " + LNLinkedServer +".dbo.tltsfc506175" + " Where t_cprj IN ('" + Project + "') AND t_sern IN ('" + PositionNo + "') order by t_cprj ").Take(20).ToList();
                var PRCNos = db.Database.SqlQuery<GET_PCRDetails>(@"SELECT  cast(t_pcrn as varchar(10)) as id,cast(t_pcrn as varchar(10)) as text from " + LNLinkedServer + ".dbo.tltsfc506175  where t_cprj = '" + Project + "' and t_sern='" + PositionNo + "' and isnull(t_pcrn,'')!=''").ToList();
                lstPCRNODetails = (from r in PRCNos
                                   select new GET_PCRDetails { id = r.id, text = r.text }).Distinct().ToList();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            finally
            {
                //if (objConn.State == ConnectionState.Open)
                //    objConn.Close();
            }
            return Json(lstPCRNODetails, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetPCLDetails(string PCR)
        {
            var itemDetails = GetPCLValues(PCR);
            return Json(itemDetails, JsonRequestBehavior.AllowGet);
        }
        public List<GET_PCRDetails> GetPCLValues(string PCR)
        {
            //Project = "S040411";
            //SqlConnection objConn = new SqlConnection(Convert.ToString(ConfigurationManager.ConnectionStrings["LNConcerto2"]));
            var LNLinkedServer = Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.LNLinkedServer.GetStringValue());
            List<GET_PCRDetails> lstPCLNODetails = new List<GET_PCRDetails>();
            var lstPCRNODetails = new List<GET_PCRDetails>();
            try
            {


                //objCmd.CommandText = "select a.TASKUNIQUEID,a.contact, a.NAME [TaskName] from PROJ_TASK a with(nolock)  where a.contact in ('" + objESP113.TaskManager.Replace(",", "','") + "') and contact is not null";
                //lstPCRNODetails = db.Database.SqlQuery<GET_PCRDetails>("select top 1 b.t_pcln,c.t_stat from tltsfc506175 a left outer join tltsfc503175 b on a.t_pcrn=b.t_pcrn left outer join tltsfc504175 c on b.t_pcln=c.t_pcln where a.t_pcrn ='" + PCR + "'";

                lstPCRNODetails = db.Database.SqlQuery<GET_PCRDetails>(@"SELECT top 1 cast(b.t_pcln as varchar(25)) as PCLNO,cast(c.t_stat as varchar(10)) as PCLstatus  from " + LNLinkedServer + ".dbo.tltsfc506175 a left outer join " + LNLinkedServer + ".dbo.tltsfc503175 b on a.t_pcrn=b.t_pcrn left outer join " + LNLinkedServer + ".dbo.tltsfc504175 c on b.t_pcln=c.t_pcln where a.t_pcrn ='" + PCR + "' and b.t_pcln!='' and b.t_pcln is not null").ToList();

                //var q1= (@"Select distinct cast(b.t_pcln as varchar(10)) as PCLNO,cast(b.t_stat as varchar(10)) as PCLstatus from " + LNLinkedServer + ".dbo.tltsfc506175 a left join " + LNLinkedServer + ".dbo.tltsfc503175 b on a.t_pcrn = b.t_pcrn left join " + LNLinkedServer + " Where child.t_orno='" + Convert.ToString(txtPONo) + "'").ToList();
                if (lstPCRNODetails != null && lstPCRNODetails.Count > 0)
                {
                    lstPCLNODetails = (from r in lstPCRNODetails
                                       select new GET_PCRDetails
                                       {
                                           PCLNO = r.PCLNO,
                                           PCLstatus = (Convert.ToInt32(r.PCLstatus) == 1 ? "PCL Released"
                                                        : Convert.ToInt32(r.PCLstatus) == 2 ? "Confirmed by PMG"
                                                        : Convert.ToInt32(r.PCLstatus) == 3 ? "Returned by PMG"
                                                        : Convert.ToInt32(r.PCLstatus) == 4 ? "Approved by QA"
                                                        : Convert.ToInt32(r.PCLstatus) == 5 ? "Rejected by QA"
                                                        : Convert.ToInt32(r.PCLstatus) == 6 ? "Confirmed by SFC"
                                                        : Convert.ToInt32(r.PCLstatus) == 8 ? "Plate Issued"
                                                        : Convert.ToInt32(r.PCLstatus) == 9 ? "Plate Received"
                                                        : Convert.ToInt32(r.PCLstatus) == 10 ? "Plate/Pipe Marked"
                                                        : Convert.ToInt32(r.PCLstatus) == 11 ? "Approved by QC"
                                                        : Convert.ToInt32(r.PCLstatus) == 12 ? "Rejected by QC"
                                                        : Convert.ToInt32(r.PCLstatus) == 13 ? "Plate/Pipe Cut"
                                                        : Convert.ToInt32(r.PCLstatus) == 14 ? "To be Returned"
                                                        : Convert.ToInt32(r.PCLstatus) == 15 ? "Plate Returned"
                                                        : Convert.ToInt32(r.PCLstatus) == 16 ? "PCL Cancelled"
                                                        : Convert.ToInt32(r.PCLstatus) == 17 ? "PCL Closed" : "Confirmed by PMG")
                                       }).Distinct().ToList();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return lstPCLNODetails;
        }

        //public JsonResult UpdateSFMValues()
        //{
        //    var itemDetails = UpdateSFM();
        //    return Json(itemDetails, JsonRequestBehavior.AllowGet);
        //}

        public JsonResult UpdateStatus()
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            var LNLinkedServer = Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.LNLinkedServer.GetStringValue());
            //SqlConnection objConn = new SqlConnection(Convert.ToString(ConfigurationManager.ConnectionStrings["LNConcerto2"]));
            SqlConnection objConn2 = new SqlConnection(Convert.ToString(ConfigurationManager.ConnectionStrings["LNConcerto"]));
            List<GET_SFMDetails> lstPCLNODetails = new List<GET_SFMDetails>();
            List<GET_PODate> lstPODetails = new List<GET_PODate>();
            List<GET_DATES_BY_TASKUNIQUEID> lstUIDDetails = new List<GET_DATES_BY_TASKUNIQUEID>();
            try
            {

                objConn2.Open();

                SqlCommand objCmd2 = new SqlCommand();
                SqlCommand objCmd3 = new SqlCommand();

                objCmd3.Connection = objConn2;
                objCmd3.CommandType = CommandType.Text;
                SqlDataAdapter dataAdapt = new SqlDataAdapter();
                DataTable dataTable = new DataTable();
                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                DataTable dt = new DataTable();
                SqlDataAdapter dataAdapter2 = new SqlDataAdapter();
                DataTable dt2 = new DataTable();
                var PCRData = dbSFM.SFM001.GroupBy(l => new { l.Project, l.PositionNo, l.PCRNO })
                            .Select(x => new
                            {
                                Project = x.FirstOrDefault().Project,
                                PositionNo = x.FirstOrDefault().PositionNo,
                                PCRNO = x.FirstOrDefault().PCRNO,
                                Dispatch = x.FirstOrDefault().Dispatch,
                            }).Where(x => x.Project != null && x.PositionNo != null && x.PCRNO != null && x.Dispatch != "Yes").ToList();

                var PODate = dbSFM.SFM001.GroupBy(l => new { l.Project, l.PositionNo })
                            .Select(x => new
                            {
                                Project = x.FirstOrDefault().Project,
                                PositionNo = x.FirstOrDefault().PositionNo,
                                PlannedReceipt = x.FirstOrDefault().PlannedReceipt,
                                Dispatch = x.FirstOrDefault().Dispatch,
                            }).Where(x => x.Project != null && x.PositionNo != null && x.PlannedReceipt != null && x.Dispatch != "Yes").ToList();

                var UIDDetails = dbSFM.SFM001.GroupBy(l => new { l.Project, l.PositionNo })
                           .Select(x => new
                           {
                               Project = x.FirstOrDefault().Project,
                               PositionNo = x.FirstOrDefault().PositionNo,
                               UID = x.FirstOrDefault().UID,
                               Dispatch = x.FirstOrDefault().Dispatch,
                           }).Where(x => x.Project != null && x.PositionNo != null && x.UID != null && x.Dispatch != "Yes").ToList();
                var MOCDetails = dbSFM.SFM001.GroupBy(l => new { l.Project, l.PositionNo })
                          .Select(x => new
                          {
                              Project = x.FirstOrDefault().Project,
                              PositionNo = x.FirstOrDefault().PositionNo,
                              MOC = x.FirstOrDefault().MOC,
                              Length = x.FirstOrDefault().Length,
                              Thk = x.FirstOrDefault().Thk,
                              Width = x.FirstOrDefault().Width,
                              Weight = x.FirstOrDefault().Weight,
                              Dispatch = x.FirstOrDefault().Dispatch,
                          }).Where(x => x.Project != null && x.PositionNo != null && x.Weight != null && x.Length != null && x.Width != null && x.Thk != null && x.MOC != null && x.Dispatch != "Yes").ToList();
                if (PCRData.Any())
                {
                    foreach (var data in PCRData)
                    {
                        // objCmd.CommandText = "select Top 1 a.t_cprj, a.t_sern, b.t_pcln,c.t_stat,a.t_pcrn from tltsfc506175 a left outer join tltsfc503175 b on a.t_pcrn=b.t_pcrn left outer join tltsfc504175 c on b.t_pcln=c.t_pcln where a.t_cprj ='" + data.Project + "' and a.t_sern='" + data.PositionNo + "' and a.t_pcrn ='" + data.PCRNO + "'";
                        var lstPCRNODetails = db.Database.SqlQuery<GET_PCRDetails>(@"SELECT top 1 cast(a.t_cprj as varchar(10)) as Project,cast(a.t_sern as varchar(10)) as PositionNo, cast(b.t_pcln as varchar(25)) as PCLNO,cast(c.t_stat as varchar(10)) as PCLstatus,a.t_pcrn as PCRNO  from " + LNLinkedServer + ".dbo.tltsfc506175 a left outer join " + LNLinkedServer + ".dbo.tltsfc503175 b on a.t_pcrn=b.t_pcrn left outer join " + LNLinkedServer + ".dbo.tltsfc504175 c on b.t_pcln=c.t_pcln where a.t_cprj ='" + data.Project + "' and a.t_sern='" + data.PositionNo + "' and a.t_pcrn ='" + data.PCRNO + "' and b.t_pcln!='' and  b.t_pcln is not null").ToList();
                        if (lstPCRNODetails != null && lstPCRNODetails.Count > 0)
                        {
                            lstPCLNODetails = (from a in lstPCRNODetails
                                               select new GET_SFMDetails
                                               {
                                                   Project = a.Project,
                                                   PositionNo = a.PositionNo,
                                                   PCRNO = a.PCRNO,
                                                   PCLNO = a.PCLNO,
                                                   PCLstatus = (Convert.ToInt32(a.PCLstatus) == 1 ? "PCL Released"
                                                        : Convert.ToInt32(a.PCLstatus) == 2 ? "Confirmed by PMG"
                                                        : Convert.ToInt32(a.PCLstatus) == 3 ? "Returned by PMG"
                                                        : Convert.ToInt32(a.PCLstatus) == 4 ? "Approved by QA"
                                                        : Convert.ToInt32(a.PCLstatus) == 5 ? "Rejected by QA"
                                                        : Convert.ToInt32(a.PCLstatus) == 6 ? "Confirmed by SFC"
                                                        : Convert.ToInt32(a.PCLstatus) == 8 ? "Plate Issued"
                                                        : Convert.ToInt32(a.PCLstatus) == 9 ? "Plate Received"
                                                        : Convert.ToInt32(a.PCLstatus) == 10 ? "Plate/Pipe Marked"
                                                        : Convert.ToInt32(a.PCLstatus) == 11 ? "Approved by QC"
                                                        : Convert.ToInt32(a.PCLstatus) == 12 ? "Rejected by QC"
                                                        : Convert.ToInt32(a.PCLstatus) == 13 ? "Plate/Pipe Cut"
                                                        : Convert.ToInt32(a.PCLstatus) == 14 ? "To be Returned"
                                                        : Convert.ToInt32(a.PCLstatus) == 15 ? "Plate Returned"
                                                        : Convert.ToInt32(a.PCLstatus) == 16 ? "PCL Cancelled"
                                                        : Convert.ToInt32(a.PCLstatus) == 17 ? "PCL Closed" : "Confirmed by PMG")
                                               }).Distinct().ToList();
                            if (lstPCLNODetails.Any())
                            {
                                foreach (var pcl in lstPCLNODetails)
                                {
                                    dbSFM.SFM001.Where(x => x.Project == pcl.Project && x.PositionNo == pcl.PositionNo
                                     && x.PCRNO == pcl.PCRNO).OrderBy(x => x.ShellId).ToList()
                                    .ForEach(a =>
                                    {
                                        a.PCLNumber = pcl.PCLNO;
                                        a.PCLStatus = pcl.PCLstatus;
                                        a.EditedBy = objClsLoginInfo.UserName;
                                        a.EditedOn = DateTime.Now;
                                    });
                                    objResponseMsg.Key = true;
                                    objResponseMsg.Value = "Status has updated";
                                    dbSFM.SaveChanges();
                                }
                            }
                        }
                    }

                }
                if (PODate.Any())
                {
                    foreach (var data in PODate)
                    {

                        var PLDetails = db.Database.SqlQuery<GET_PODate>(@"SELECT top 1 convert(varchar, c.t_ddta, 23) as PlannedReceiptDate,cast(a.t_cprj as varchar(10)) as Project,cast(a.t_sern as varchar(10)) as PositionNo  from " + LNLinkedServer + ".dbo.tltsfc506175 a left outer join " + LNLinkedServer + ".dbo.ttdpur500175 b on a.t_cprj=b.t_cprj left outer join " + LNLinkedServer + ".dbo.ttdpur401175 c on b.t_bobj=c.t_orno and a.t_sitm=c.t_item  where a.t_cprj ='" + data.Project + "' and a.t_sern='" + data.PositionNo + "'and c.t_ddta is not null").ToList();

                        if (PLDetails != null && PLDetails.Count > 0)
                        {
                            lstPODetails = (from a in PLDetails
                                            select new GET_PODate
                                            {
                                                Project = a.Project,
                                                PositionNo = a.PositionNo,
                                                PlannedReceiptDate = Convert.ToDateTime(a.PlannedReceiptDate).ToString("dd/MM/yyyy").ToString(),
                                            }).Distinct().ToList();
                            if (lstPODetails.Any())
                            {
                                foreach (var po in lstPODetails)
                                {
                                    dbSFM.SFM001.Where(x => x.Project == po.Project && x.PositionNo == po.PositionNo).OrderBy(x => x.ShellId).ToList()
                                    .ForEach(a =>
                                    {
                                        a.PlannedReceipt = po.PlannedReceiptDate;
                                        a.EditedBy = objClsLoginInfo.UserName;
                                        a.EditedOn = DateTime.Now;
                                    });
                                    objResponseMsg.Key = true;
                                    objResponseMsg.Value = "Status has updated";
                                    dbSFM.SaveChanges();
                                }
                            }
                        }
                        else
                        {
                            //item = "140";
                            //objCmd2.CommandText = "select top 1 e.t_ddta,a.t_cprj,a.t_sern from tltsfc506175 a inner join ttpctm110175 b on a.t_cprj=b.t_cprj inner join ttpctm110175 c on b.t_cono=c.t_cono left outer join ttdpur500175 d on d.t_cprj=c.t_cprj left outer join ttdpur401175 e on e.t_orno=d.t_bobj and a.t_sitm=e.t_item where a.t_cprj ='" + data.Project + "' and t_sern='" + data.PositionNo + "' and e.t_ddta is not null";
                            //dataAdapter.SelectCommand = objCmd2;
                            //dataAdapter.Fill(dt);
                            var PLDetails2 = db.Database.SqlQuery<GET_PODate>(@"SELECT top 1 convert(varchar, e.t_ddta, 23) as PlannedReceiptDate,cast(e.t_ddta as varchar(10)) as PlannedReceiptDate,cast(e.t_ddta as varchar(10)) as PlannedReceiptDate  from " + LNLinkedServer + ".dbo.tltsfc506175 a inner join " + LNLinkedServer +
                                           ".dbo.ttpctm110175 b on a.t_cprj=b.t_cprj inner join " + LNLinkedServer +
                                           ".dbo.ttpctm110175 c on b.t_cono=c.t_cono left outer join " + LNLinkedServer + " .dbo.ttdpur500175 d on d.t_cprj=c.t_cprj left outer join " + LNLinkedServer + ".dbo.ttdpur401175 e on e.t_orno=d.t_bobj and a.t_sitm=e.t_item where a.t_cprj ='" + data.Project + "' and t_sern='" + data.PositionNo + "' and e.t_ddta is not null").ToList();
                            if (PLDetails2 != null && PLDetails2.Count > 0)
                            {
                                lstPODetails = (from c in PLDetails2
                                                select new GET_PODate
                                                {
                                                    Project = c.Project,
                                                    PositionNo = c.PositionNo,
                                                    PlannedReceiptDate = Convert.ToDateTime(c.PlannedReceiptDate).ToString("dd/MM/yyyy").ToString(),
                                                }).Distinct().ToList();
                                if (lstPODetails.Any())
                                {
                                    foreach (var po in lstPODetails)
                                    {
                                        dbSFM.SFM001.Where(x => x.Project == po.Project && x.PositionNo == po.PositionNo).OrderBy(x => x.ShellId).ToList()
                                        .ForEach(a =>
                                        {
                                            a.PlannedReceipt = po.PlannedReceiptDate;
                                            a.EditedBy = objClsLoginInfo.UserName;
                                            a.EditedOn = DateTime.Now;
                                        });
                                        objResponseMsg.Key = true;
                                        objResponseMsg.Value = "Status has updated";
                                        dbSFM.SaveChanges();
                                    }
                                }
                            }
                        }
                    }
                }
                if (UIDDetails.Any())
                {
                    foreach (var item in UIDDetails)
                    {
                        objCmd3.CommandText = "select a.ActualStart,a.ActualFinish,a.EXPECTED_START_DATE,a.EXPECTED_END_DATE,a.TASKUNIQUEID,a.PROJECTNAME from PROJ_TASK a with(nolock)  where a.TASKUNIQUEID in (" + item.UID + ") and a.PROJECTNAME='" + item.Project + "'";
                        dataAdapter2.SelectCommand = objCmd3;
                        dataAdapter2.Fill(dt2);
                        if (dt2 != null && dt2.Rows.Count > 0)
                        {
                            lstUIDDetails = (from DataRow row in dataTable.Rows
                                             select new GET_DATES_BY_TASKUNIQUEID
                                             {
                                                 TaskUID = row["TASKUNIQUEID"].ToString(),
                                                 Project = row["Project"].ToString(),
                                                 ACTUALSTART = Convert.ToDateTime(row["ACTUALSTART"].ToString()).ToString("dd/MM/yyyy").ToString(),
                                                 ACTUALFINISH = Convert.ToDateTime(row["ACTUALFINISH"].ToString()).ToString("dd/MM/yyyy").ToString(),
                                                 PLANNEDSTARTDATE = Convert.ToDateTime(row["EXPECTED_START_DATE"].ToString()).ToString("dd/MM/yyyy").ToString(),
                                                 PLANNEDENDDATE = Convert.ToDateTime(row["EXPECTED_END_DATE"].ToString()).ToString("dd/MM/yyyy").ToString()
                                             }).Distinct().ToList();
                            if (lstUIDDetails.Any())
                            {
                                foreach (var uids in lstUIDDetails)
                                {
                                    dbSFM.SFM001.Where(x => x.Project == uids.Project
                                     && x.UID == uids.TaskUID).OrderBy(x => x.ShellId).ToList()
                                    .ForEach(a =>
                                    {
                                        a.PlannedEndDate = Convert.ToDateTime(uids.PLANNEDENDDATE);
                                        a.ActualStartDate = Convert.ToDateTime(uids.ACTUALSTART);
                                        a.PlannedEndDate = Convert.ToDateTime(uids.PLANNEDENDDATE);
                                        a.PlannedStartDate = Convert.ToDateTime(uids.PLANNEDSTARTDATE);
                                        a.EditedBy = objClsLoginInfo.UserName;
                                        a.EditedOn = DateTime.Now;
                                    });
                                    objResponseMsg.Key = true;
                                    objResponseMsg.Value = "Status has updated";
                                    dbSFM.SaveChanges();
                                }
                            }
                        }
                    }

                }
                if (MOCDetails.Any())
                {
                    foreach (var mocdet in MOCDetails)
                    {
                        var itemdet = dbSFM.SP_SFM_GET_LIST_ITEM(mocdet.Project, mocdet.PositionNo).ToList();
                        var itemDetails = itemdet.Where(y => y.FindNo == mocdet.PositionNo && y.Project == mocdet.Project).ToList();
                        if (itemDetails.Any())
                        {
                            foreach (var mocitems in itemDetails)
                            {
                                dbSFM.SFM001.Where(x => x.Project == mocitems.Project
                                 && x.PositionNo == mocitems.FindNo).OrderBy(x => x.ShellId).ToList()
                                .ForEach(a =>
                                {
                                    a.MOC = mocitems.MaterialCode;
                                    a.Length = mocitems.Length;
                                    a.Thk = mocitems.Thickness;
                                    a.Width = mocitems.Width;
                                    a.Weight = Convert.ToDecimal(mocitems.Weight);
                                    a.EditedBy = objClsLoginInfo.UserName;
                                    a.EditedOn = DateTime.Now;
                                });
                                objResponseMsg.Key = true;
                                objResponseMsg.Value = "Status has updated";
                                dbSFM.SaveChanges();
                            }
                        }
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Record found!";
                }
                //objCmd.CommandText = "select a.TASKUNIQUEID,a.contact, a.NAME [TaskName] from PROJ_TASK a with(nolock)  where a.contact in ('" + objESP113.TaskManager.Replace(",", "','") + "') and contact is not null";




            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return Json(objResponseMsg);
        }

        //public JsonResult UpdateSeamStatus()
        //{

        //}
        public JsonResult GetSFMPlannedReceiptDate(string project, string item)
        {
            var itemDetails = PlannedReceiptDate(project, item);
            return Json(itemDetails, JsonRequestBehavior.AllowGet);
        }

        public List<GET_PODate> PlannedReceiptDate(string project, string item)
        {
            //Project = "S040411";
            var LNLinkedServer = Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.LNLinkedServer.GetStringValue());
            List<GET_PODate> lstPODate = new List<GET_PODate>();

            try
            {


                //objCmd.CommandText = "select top 1 c.t_ddta from tltsfc506175 a left outer join ttdpur500175 b on a.t_cprj=b.t_cprj left outer join ttdpur401175 c on b.t_bobj=c.t_orno and a.t_sitm=c.t_item where a.t_cprj ='" + project + "' and a.t_sern='" + item + "' and c.t_ddta is not null";
                var PLDetails = db.Database.SqlQuery<GET_PODate>(@"SELECT top 1 convert(varchar, c.t_ddta, 23) as PlannedReceiptDate  from " + LNLinkedServer + ".dbo.tltsfc506175 a left outer join " + LNLinkedServer + ".dbo.ttdpur500175 b on a.t_cprj=b.t_cprj left outer join " + LNLinkedServer + ".dbo.ttdpur401175 c on b.t_bobj=c.t_orno and a.t_sitm=c.t_item  where a.t_cprj ='" + project + "' and a.t_sern='" + item + "'and c.t_ddta is not null").ToList();

                if (PLDetails != null && PLDetails.Count > 0)
                {
                    lstPODate = (from a in PLDetails
                                 select new GET_PODate
                                 {
                                     PlannedReceiptDate = Convert.ToDateTime(a.PlannedReceiptDate).ToString("dd/MM/yyyy").ToString(),
                                 }).Distinct().ToList();
                }
                else
                {
                    //item = "140";
                    //var PlDetails = "select top 1 e.t_ddta from tltsfc506175 a inner join ttpctm110175 b on a.t_cprj=b.t_cprj inner join ttpctm110175 c on b.t_cono=c.t_cono left outer join ttdpur500175 d on d.t_cprj=c.t_cprj left outer join ttdpur401175 e on e.t_orno=d.t_bobj and a.t_sitm=e.t_item where a.t_cprj ='" + project + "' and t_sern='" + item + "' and e.t_ddta is not null";
                    var PLDetails2 = db.Database.SqlQuery<GET_PODate>(@"SELECT top 1 convert(varchar, e.t_ddta, 23) as PlannedReceiptDate  from " + LNLinkedServer + ".dbo.tltsfc506175 a inner join " + LNLinkedServer +
                                          ".dbo.ttpctm110175 b on a.t_cprj=b.t_cprj inner join " + LNLinkedServer +
                                          ".dbo.ttpctm110175 c on b.t_cono=c.t_cono left outer join " + LNLinkedServer + " .dbo.ttdpur500175 d on d.t_cprj=c.t_cprj left outer join " + LNLinkedServer + ".dbo.ttdpur401175 e on e.t_orno=d.t_bobj and a.t_sitm=e.t_item where a.t_cprj ='" + project + "' and t_sern='" + item + "' and e.t_ddta is not null").ToList();
                    if (PLDetails2 != null && PLDetails2.Count > 0)
                    {
                        lstPODate = (from a in PLDetails2
                                     select new GET_PODate
                                     {
                                         PlannedReceiptDate = Convert.ToDateTime(a.PlannedReceiptDate).ToString("dd/MM/yyyy").ToString(),
                                     }).Distinct().ToList();
                    }
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return lstPODate;
        }


        public ActionResult GetQualityProjectsBySFM(string Project, string BU, string search = "")
        {
            var plngrp = db.COM001.Where(x => x.t_cprj == Project).Select(x => x.t_plgr).FirstOrDefault();
            List<ddlValue> lstQualityProject = (from a in db.COM001
                                                join b in db.QMS010 on a.t_cprj equals b.Project
                                                where a.t_plgr == plngrp && b.Location == objClsLoginInfo.Location && (search == "" || b.QualityProject.ToLower().Contains(search.ToLower()))
                                                select new ddlValue { id = b.QualityProject, text = b.QualityProject }).Distinct().ToList();//&& i.CreatedBy.Equals(username,StringComparison.OrdinalIgnoreCase)


            return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateWeldKGDetails(string Project, string columnValue)
        {
            var WeldData = (from a in db.WKG002
                            where a.Project == Project && a.SeamNo == columnValue
                            select new
                            {
                                a.TotalWeldKg
                            }).Distinct().ToList();
            return Json(WeldData, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetSetupWeldingvalue(string Project, string columnValue, string SeamNo, string QualityProject)
        {
            //Project = "S040005";
            //columnValue = "AW7";
            var WeldingData = (from a in db.QMS040
                               where a.Project == Project && a.SeamNo == SeamNo && a.QualityProject == QualityProject && a.StageCode == "N5AVIC" && a.InspectionStatus != ""
                               select new
                               {
                                   Status = a.InspectionStatus == "Ready to Offer" ? "NS" : a.InspectionStatus == "Cleared" ? "CO" : "IP"
                               }).Distinct().ToList();

            var SetupData = (from a in db.QMS040
                             where a.Project == Project && a.SeamNo == SeamNo && a.QualityProject == QualityProject && a.StageCode == "N1RSETUP" && a.InspectionStatus != ""
                             select new
                             {
                                 Status = a.InspectionStatus == "Ready to Offer" ? "NS" : a.InspectionStatus == "Cleared" ? "CO" : "IP"
                             }).Distinct().ToList();

            var NDT = (from a in db.QMS040
                       join b in db.QMS002 on a.StageCode equals b.StageCode
                       where a.Project == Project && a.SeamNo == SeamNo && a.QualityProject == QualityProject && b.PrePostWeldHeadTreatment == "Pre-Heat Treatment" && a.InspectionStatus == "Cleared"
                       select new
                       {
                           Status = a.InspectionStatus == "Cleared" ? "CO" : ""
                       }).Distinct().ToList();
            return Json(new { WeldingData, SetupData, NDT }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateSeamStatus()
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var SeamStatusDetails = (from a in dbSFM.SFM001
                                         where a.Project != null && a.QualityProject != null && a.PositionNo != null && a.SeamNo != null && a.Setup != null
                                         && a.Welding != null && a.NDT != null && a.Dispatch != "Yes"
                                         select new
                                         {
                                             Project = a.Project,
                                             PositionNo = a.PositionNo,
                                             QualityProject = a.QualityProject,
                                             SeamNo = a.SeamNo,
                                             Rolling = a.Rolling,
                                             Setup = a.Setup,
                                             Welding = a.Welding,
                                             NDT = a.NDT,
                                             Dispatch = a.Dispatch,
                                         }).ToList();
                if (SeamStatusDetails.Any())
                {
                    foreach (var item in SeamStatusDetails)
                    {
                        if (item.Rolling == "CO" && item.Setup != "CO")
                        {
                            dbSFM.SFM001.Where(x => x.Project == item.Project
                                     && x.PositionNo == item.SeamNo && x.QualityProject == item.QualityProject).OrderBy(x => x.ShellId).ToList()
                                    .ForEach(a =>
                                    {
                                        a.Setup = "IP";
                                        a.EditedBy = objClsLoginInfo.UserName;
                                        a.EditedOn = DateTime.Now;
                                    });
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "Status has updated";
                            dbSFM.SaveChanges();
                        }
                        else if (item.Rolling != "CO" && item.Setup != "CO")
                        {
                            var SetupData = (from a in db.QMS040
                                             where a.Project == item.Project && a.SeamNo == item.SeamNo && a.QualityProject == item.QualityProject && a.StageCode == "N1RSETUP" && a.InspectionStatus != ""
                                             select new
                                             {
                                                 Status = a.InspectionStatus == "Ready to Offer" ? "NS" : a.InspectionStatus == "Cleared" ? "CO" : "IP",
                                                 Project = a.Project,
                                                 SeamNo = a.SeamNo,
                                                 QualityProject = a.QualityProject
                                             }).Distinct().ToList();
                            if (SetupData.Any())
                            {
                                foreach (var s1 in SetupData)
                                {
                                    dbSFM.SFM001.Where(x => x.Project == s1.Project
                                    && x.SeamNo == s1.SeamNo && x.QualityProject == s1.QualityProject).OrderBy(x => x.ShellId).ToList()
                                   .ForEach(a =>
                                   {
                                       a.Setup = s1.Status;
                                       a.EditedBy = objClsLoginInfo.UserName;
                                       a.EditedOn = DateTime.Now;
                                   });
                                    objResponseMsg.Key = true;
                                    objResponseMsg.Value = "Status has updated";
                                    dbSFM.SaveChanges();
                                }
                            }
                        }
                        if (item.Welding != "CO" && item.Setup == "CO")
                        {
                            dbSFM.SFM001.Where(x => x.Project == item.Project
                                     && x.PositionNo == item.SeamNo && x.QualityProject == item.QualityProject).OrderBy(x => x.ShellId).ToList()
                                    .ForEach(a =>
                                    {
                                        a.Welding = "IP";
                                        a.EditedBy = objClsLoginInfo.UserName;
                                        a.EditedOn = DateTime.Now;
                                    });
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "Status has updated";
                            dbSFM.SaveChanges();
                        }
                        else if (item.Setup != "CO" && item.Welding != "CO")
                        {
                            var WeldingData = (from a in db.QMS040
                                               where a.Project == item.Project && a.SeamNo == item.SeamNo && a.QualityProject == item.QualityProject && a.StageCode == "N5AVIC" && a.InspectionStatus != ""
                                               select new
                                               {
                                                   Status = a.InspectionStatus == "Ready to Offer" ? "NS" : a.InspectionStatus == "Cleared" ? "CO" : "IP",
                                                   Project = a.Project,
                                                   SeamNo = a.SeamNo,
                                                   QualityProject = a.QualityProject
                                               }).Distinct().ToList();
                            if (WeldingData.Any())
                            {
                                foreach (var s1 in WeldingData)
                                {
                                    dbSFM.SFM001.Where(x => x.Project == s1.Project
                                    && x.SeamNo == s1.SeamNo && x.QualityProject == s1.QualityProject).OrderBy(x => x.ShellId).ToList()
                                   .ForEach(a =>
                                   {
                                       a.Welding = s1.Status;
                                       a.EditedBy = objClsLoginInfo.UserName;
                                       a.EditedOn = DateTime.Now;
                                   });
                                    objResponseMsg.Key = true;
                                    objResponseMsg.Value = "Status has updated";
                                    dbSFM.SaveChanges();
                                }
                            }
                        }
                        if (item.NDT != "CO")
                        {
                            string NDTStatus = "";
                            var NDT = (from a in db.QMS040
                                       join b in db.QMS002 on a.StageCode equals b.StageCode
                                       where a.Project == item.Project && a.SeamNo == item.SeamNo && a.QualityProject == item.QualityProject && b.PrePostWeldHeadTreatment == "Pre-Heat Treatment" && a.InspectionStatus == "Cleared"
                                       select new
                                       {
                                           Status = a.InspectionStatus == "Cleared" ? "CO" : "",
                                           Project = a.Project,
                                           SeamNo = a.SeamNo,
                                           QualityProject = a.QualityProject
                                       }).Distinct().ToList();
                            if (NDT.Any())
                            {
                                foreach (var s1 in NDT)
                                {
                                    dbSFM.SFM001.Where(x => x.Project == s1.Project
                                    && x.SeamNo == s1.SeamNo && x.QualityProject == s1.QualityProject).OrderBy(x => x.ShellId).ToList()
                                   .ForEach(a =>
                                   {
                                       a.NDT = s1.Status;
                                       a.EditedBy = objClsLoginInfo.UserName;
                                       a.EditedOn = DateTime.Now;
                                   });
                                    objResponseMsg.Key = true;
                                    objResponseMsg.Value = "Status has updated";
                                    dbSFM.SaveChanges();
                                }
                            }
                            else if (item.Welding == "CO")
                            {
                                dbSFM.SFM001.Where(x => x.Project == item.Project
                                     && x.PositionNo == item.SeamNo && x.QualityProject == item.QualityProject).OrderBy(x => x.ShellId).ToList()
                                    .ForEach(a =>
                                    {
                                        a.NDT = "IP";
                                        a.EditedBy = objClsLoginInfo.UserName;
                                        a.EditedOn = DateTime.Now;
                                    });
                                objResponseMsg.Key = true;
                                objResponseMsg.Value = "Status has updated";
                                dbSFM.SaveChanges();
                            }

                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "No Record found!";
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(objResponseMsg);
        }


        //public ActionResult GetWeldingvalue(string Project, string columnValue)
        //{
        //    var WeldData = (from a in db.QMS040
        //                    where a.Project == Project && a.SeamNo == columnValue && a.StageCode == "N5AVICWE" && a.InspectionStatus!=""
        //                    select new
        //                    {
        //                        Status = a.InspectionStatus == "Ready to Offer" ? "NS" : a.InspectionStatus == "Cleared" ? "CO" : "Other"
        //                    }).Distinct().ToList();
        //    return Json(WeldData, JsonRequestBehavior.AllowGet);
        //}

        public ActionResult GetFormDataPartial(int ShellId, string Role, string UserRole)
        {
            ViewBag.ShellId = ShellId;
            ViewData["getWorkCenterEnum"] = clsImplementationEnum.getWorkCenterEnum();
            ViewData["getCsWepEnum"] = clsImplementationEnum.getCsWepEnum();
            ViewData["getLsWepEnum"] = clsImplementationEnum.getLsWepEnum();
            ViewData["getEdgeBreakEnum"] = clsImplementationEnum.getEdgeBreakEnum();
            var sfmdata = dbSFM.SFM001.Where(x => x.ShellId == ShellId).FirstOrDefault();
            ViewBag.DispatchStatus = sfmdata.Dispatch;
            ViewBag.Role = Role;
            if (Role == "FSP")
                ViewBag.UserRole = UserRole;
            BindDropDown(null);
            return PartialView("_FormDataPartial");
        }

        public JsonResult GetDetails(int ShellId)
        {
            var objResponseMsg = new resSFM001();
            try
            {
                var sfmdata = dbSFM.SFM001.Where(x => x.ShellId == ShellId).FirstOrDefault();
                var customer = Manager.GetCustomerProjectWise(sfmdata.Project);
                if (sfmdata != null)
                {

                    //objResponseMsg.maclocations = items;
                    //objResponseMsg.machines = MachineCode;                  
                    objResponseMsg.D_DispatchDate = (sfmdata.DispatchDate.HasValue ? sfmdata.DispatchDate.Value.ToString("yyyy-MM-dd") : "");
                    objResponseMsg.ActualStartDate = (sfmdata.ActualStartDate.HasValue ? sfmdata.ActualStartDate.Value.ToString("yyyy-MM-dd") : "");
                    objResponseMsg.ActualEndDate = (sfmdata.ActualEndDate.HasValue ? sfmdata.ActualEndDate.Value.ToString("yyyy-MM-dd") : "");
                    objResponseMsg.PlannedStartDate = (sfmdata.PlannedStartDate.HasValue ? sfmdata.PlannedStartDate.Value.ToString("yyyy-MM-dd") : "");
                    objResponseMsg.PlannedEndDate = (sfmdata.PlannedEndDate.HasValue ? sfmdata.PlannedEndDate.Value.ToString("yyyy-MM-dd") : "");
                    objResponseMsg.CustomerName = customer.Name;
                    if (sfmdata.PlannedReceipt != "" && sfmdata.PlannedReceipt != null)
                    {
                        DateTime PlannedReceipt = Convert.ToDateTime(sfmdata.PlannedReceipt);
                        objResponseMsg.PlannedReceipt = (sfmdata.PlannedReceipt != null && sfmdata.PlannedReceipt != "" ? PlannedReceipt.ToString("yyyy-MM-dd") : "");
                    }
                    else
                        objResponseMsg.PlannedReceipt = "";
                    objResponseMsg.objsfm = sfmdata;
                    objResponseMsg.Key = true;
                    ViewBag.Dispatchstatus = sfmdata.Dispatch;
                    objResponseMsg.Value = "SuccessFully DeSelected All";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Record found!";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg);
        }

        [HttpPost]
        public ActionResult UpdateSFMDetails(SFM001 sfm001)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            List<SFM001> listSFM001 = null;
            try
            {
                listSFM001 = new List<SFM001>();
                SFM001 dbsfm = dbSFM.SFM001.Where(x => x.ShellId == sfm001.ShellId).FirstOrDefault();
                //updatedCustomer.ActualEndDate = sfm001.ActualEndDate;
                //updatedCustomer.ActualStartDate = sfm001.ActualStartDate;

                dbsfm.L_S_WEP_M_C = (sfm001.L_S_WEP_M_C == "-1") ? "" : sfm001.L_S_WEP_M_C;

                dbsfm.IsPlanned = sfm001.IsPlanned;
                dbsfm.CS_WEP_M_C = (sfm001.CS_WEP_M_C == "-1") ? "" : sfm001.CS_WEP_M_C;
                dbsfm.Rolling = (sfm001.Rolling == "-1") ? "" : sfm001.Rolling;
                dbsfm.Setup = sfm001.Setup;
                dbsfm.Welding = sfm001.Welding;
                dbsfm.NDT = sfm001.NDT;
                dbsfm.PlannedStartDate = sfm001.PlannedStartDate;
                dbsfm.PlannedEndDate = sfm001.PlannedEndDate;
                dbsfm.PlannedReceipt = sfm001.PlannedReceipt;
                dbsfm.EdgeBracking = (sfm001.EdgeBracking == "-1") ? "" : sfm001.EdgeBracking;
                dbsfm.Dispatch = (sfm001.Dispatch == "-1") ? "" : sfm001.Dispatch;
                dbsfm.DispatchDate = sfm001.DispatchDate;
                dbsfm.DeliveryLocation = sfm001.DeliveryLocation;
                dbsfm.RollingType = (sfm001.RollingType == "-1") ? "" : sfm001.RollingType;
                dbsfm.RollingMachine = (sfm001.RollingMachine == "-1") ? "" : sfm001.RollingMachine;
                dbsfm.WorkCenter = (sfm001.WorkCenter == "-1") ? "" : sfm001.WorkCenter;
                dbsfm.CsWep = (sfm001.CsWep == "-1") ? "" : sfm001.CsWep;
                dbsfm.LsWep = (sfm001.LsWep == "-1") ? "" : sfm001.LsWep;
                dbsfm.EdgeBreak = (sfm001.EdgeBreak == "-1") ? "" : sfm001.EdgeBreak;
                dbsfm.Weld_KG = sfm001.Weld_KG;
                if (sfm001.PCRNO != "null") dbsfm.PCRNO = sfm001.PCRNO;
                dbsfm.PCLStatus = sfm001.PCLStatus;
                dbsfm.PCLNumber = sfm001.PCLNumber;
                dbsfm.ShellId = sfm001.ShellId;
                dbsfm.EditedOn = DateTime.Now;
                dbsfm.EditedBy = objClsLoginInfo.UserName;
                if (sfm001.SeamNo != "null")
                    dbsfm.SeamNo = sfm001.SeamNo;
                dbsfm.ShellMark = sfm001.ShellMark;
                dbSFM.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
                //var isExist = false;

                //if (sfm001.SeamNo != "null")
                //{

                //    if (dbsfm.PositionNo != null && dbsfm.Project != null && (dbsfm.SeamNo == null || dbsfm.SeamNo == null || dbsfm.SeamNo == sfm001.SeamNo))
                //    {
                //        dbsfm.SeamNo = sfm001.SeamNo;
                //        dbSFM.SaveChanges();
                //        objResponseMsg.Key = true;
                //        objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
                //    }
                //    else
                //    {
                //        isExist = true;
                //        // if you are uncommetting line 497 to 564 code then commetted below 3 line code.
                //        objResponseMsg.Key = false;
                //        objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.Message + " " + "for this Seam No";
                //    }
                //}
                //else
                //{
                //    dbSFM.SaveChanges();
                //    objResponseMsg.Key = true;
                //    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
                //}
                return Json(objResponseMsg);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg);
        }

        [HttpPost]
        public JsonResult GetInlineEditableRow(int id, string role, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();
                var result = new string[] { clsImplementationEnum.MachineLocation.VTLWest.GetStringValue()
                    , clsImplementationEnum.MachineLocation.TitanVTL.GetStringValue() , clsImplementationEnum.MachineLocation.KVTL.GetStringValue()
                    , clsImplementationEnum.MachineLocation.MKVTL.GetStringValue(), clsImplementationEnum.MachineLocation.L50.GetStringValue()
                    , clsImplementationEnum.MachineLocation.L45CNC.GetStringValue(), clsImplementationEnum.MachineLocation.L50Manual.GetStringValue()
                    , clsImplementationEnum.MachineLocation.Lathe.GetStringValue(), clsImplementationEnum.MachineLocation.Outsourced.GetStringValue()
                    , clsImplementationEnum.MachineLocation.NOTREQUIRED.GetStringValue()};


                if (id == 0)
                {
                    var QPitems = new SelectList(new List<string>());
                    int newRecordId = 0;
                    var newRecord = new[] {
                                        "0",
                                        Helper.HTMLAutoComplete(newRecordId,"Project","","",false,"","",false,"","","Project","form-control"),
                                        Helper.GenerateDropdownWithSelected(newRecordId,"QualityProject",QPitems,"","form-control"),
                                        Helper.GenerateTextbox(newRecordId, "Customer","", "",true, "","","form-control"),
                                        Helper.HTMLAutoComplete(newRecordId, "BU",  "", "BU_Blur(this)", false, "", "",false,"","","BU","form-control"),
                                        Helper.HTMLAutoComplete(newRecordId,"ItemId","","",false,"","",false,"","","Position No","form-control"),
                                        Helper.HTMLAutoComplete(newRecordId, "ShellMark",   "", "", false, "", "",false,"","","ShellMark","form-control"),
                                        //Helper.HTMLAutoComplete(newRecordId, "PlannedReceipt","","",false,"","",false,"","","PlannedReceipt","form-control"),
                                        Helper.GenerateTextbox(newRecordId, "PlannedReceipt","", "",false, "","","form-control"),
                                        Helper.GenerateDropdownWithSelected(newRecordId,"WorkCenter",clsImplementationEnum.getWorkCenterEnum(),"",""),
                                        Helper.GenerateDropdownWithSelected(newRecordId,"CsWep",clsImplementationEnum.getCsWepEnum(),"",""),
                                        Helper.GenerateDropdownWithSelected(newRecordId,"LsWep",clsImplementationEnum.getLsWepEnum(),"",""),
                                        Helper.GenerateDropdownWithSelected(newRecordId,"EdgeBreak",clsImplementationEnum.getEdgeBreakEnum(),"",""),
                                         //Helper.GenerateTextbox(newRecordId,"PlannedReceipt"),
                                        //GenerateCheckbox(newRecordId,"WIP",false),
                                        //GenerateCheckbox(newRecordId,"IsPlanned",false),
                                        Helper.GenerateTextbox(newRecordId, "PCRNO","","",true,"","","form-control"),
                                        Helper.GenerateTextbox(newRecordId, "PCLNumber","","",true,"","","form-control"),
                                        Helper.GenerateTextbox(newRecordId, "PCLStatus","","",true,"","","form-control"),
                                        Helper.GenerateTextbox(newRecordId, "Length","","",true,"","","form-control"),
                                         Helper.GenerateTextbox(newRecordId, "Width","","",true,"","","form-control"),
                                          Helper.GenerateTextbox(newRecordId, "Thk","","",true,"","","form-control"),
                                           Helper.GenerateTextbox(newRecordId, "Weight","","",true,"","","form-control"),
                                        Helper.GenerateTextbox(newRecordId,"MOC", "","",true,"","","form-control"),
                                        Helper.GenerateTextbox(newRecordId,"RollingType", "","",true,"","","form-control"),
                                        Helper.GenerateTextbox(newRecordId, "RollingMachine","","",true,"","","form-control"),
                                        Helper.GenerateTextbox(newRecordId,"SeamNo","","",true,"","","form-control"),

                                        Helper.GenerateTextbox(newRecordId, "SeamStatus","","",true,"","","form-control"),
                                       GenerateCheckbox(newRecordId,"IsPlanned",true),

                                        Helper.GenerateTextbox(newRecordId, "PlannedStartDate", "","",true,"","","form-control"),
                                        Helper.GenerateTextbox(newRecordId, "PlannedEndDate", "","",true,"","","form-control"),
                                        Helper.GenerateTextbox(newRecordId, "ActualStartDate", "","",true,"","","form-control"),
                                         Helper.GenerateTextbox(newRecordId, "ActualEndDate","","",true,"","","form-control"),
                                        Helper.GenerateTextbox(newRecordId,"CS_WEP_M_C", "","",true,"","","form-control"),
                                        Helper.GenerateTextbox(newRecordId, "EdgeBracking","","",true,"","","form-control"),
                                        Helper.GenerateTextbox(newRecordId, "Rolling","","",true,"","","form-control"),
                                         Helper.GenerateTextbox(newRecordId, "Setup","","",true,"","","form-control"),
                                          Helper.GenerateTextbox(newRecordId, "Welding","","",true,"","","form-control"),
                                           Helper.GenerateTextbox(newRecordId, "NDT","","",true,"","","form-control"),
                                         Helper.GenerateTextbox(newRecordId, "Dispatch","","",true,"","","form-control"),
                                          Helper.GenerateTextbox(newRecordId,"DispatchDate","","",true,"","","form-control"),
                                             Helper.GenerateTextbox(newRecordId, "DeliveryLocation", "","",true,"","","form-control"),
                                        Helper.GenerateGridButtonNew(newRecordId, "Save", "Save Rocord", "fa fa-save", "AddComponent()" ),
                                    };
                    data.Add(newRecord);
                }
                else
                {
                    var lstResult = dbSFM.SP_SFM_GET_INDEX_DATA(1, 0, "", "ShellId = " + id).Take(1).ToList();
                    var dtPlannedReceipt = new DateTime();
                    isReadOnly = true;
                    if (isReadOnly)
                    {
                        data = (from uc in lstResult
                                select new[]
                               {
                               Convert.ToString(uc.ShellId),
                                uc.Project,
                                uc.Customer,
                                uc.BU,
                                uc.PositionNo,
                                uc.ShellMark,
                                uc.PlannedReceipt,
                                uc.WorkCenter,
                                uc.CsWep,
                                uc.LsWep,
                                uc.EdgeBreak,
                                uc.PCRNO,
                                uc.PCLNumber,
                                uc.PCLStatus,
                                GenerateCheckbox(uc.ShellId,"IsPlanned",Convert.ToBoolean(uc.IsPlanned),true),
                                uc.MOC,
                                Convert.ToString(uc.Thk),
                                Convert.ToString(uc.Width),
                                Convert.ToString(uc.Length),

                               Convert.ToString(uc.Weight),
                                uc.RollingType,
                                uc.RollingMachine,
                                uc.SeamNo,
                                uc.SeamStatus,
                                (DateTime.TryParse(uc.PlannedReceipt+"",out dtPlannedReceipt) ? dtPlannedReceipt.ToString("dd/MM/yyyy") : uc.PlannedReceipt),

                                uc.PlannedStartDate.HasValue? uc.PlannedStartDate.Value.ToString("dd/MM/yyyy"):"",
                                uc.PlannedEndDate.HasValue? uc.PlannedEndDate.Value.ToString("dd/MM/yyyy"):"",
                                uc.ActualStartDate.HasValue? uc.ActualStartDate.Value.ToString("dd/MM/yyyy"):"",
                                uc.ActualEndDate.HasValue? uc.ActualEndDate.Value.ToString("dd/MM/yyyy"):"",
                                uc.DispatchDate.HasValue? uc.DispatchDate.Value.ToString("dd/MM/yyyy"):"",
                                uc.CS_WEP_M_C,
                                uc.EdgeBracking,
                                uc.L_S_WEP_M_C,
                                uc.Rolling,
                                uc.Setup,
                                uc.Welding,
                                uc.NDT,
                                uc.Dispatch,
                                uc.NDT,
                                uc.DeliveryLocation,
                                "<span>" +"<a id='Cancel' name='Cancel' title='Cancel' class='blue action' onclick='ReloadGrid()'>"+
                                        "<i class='fa fa-close'></i></a> &nbsp &nbsp" +
                                 "<a id='View" + uc.ShellId + "' name='View' title='View' class='blue action' onclick='ViewRecord(" + uc.ShellId + ")'>"+
                                        "<i class='fa fa-eye'></i></a> &nbsp &nbsp"+
                                         "<a id='UIDSelection" + uc.ShellId + "' name='UIDSelection' title=\"UID\" class=\"blue action\" onclick=\"UIDSelection(this," + uc.ShellId +")\">"+
                                        "<i class=\"iconclass fa fa-bars\"></i></a> &nbsp &nbsp"+
                                         "<a id='Seam" + uc.ShellId + "' name='Seam' title=\"Seam\" class=\"blue action\" onclick=\"Seamlist(this," + uc.ShellId +")\">"+
                                        "<i class=\"iconclass fa fa-th\"></i></a>" +
                                "</span>"
                          }).ToList();
                    }
                    else
                    {
                        if (role == "JP")
                        {
                            data = (from uc in lstResult
                                    select new[] {
                                        uc.ShellId.ToString(),
                                        Helper.HTMLAutoComplete(uc.ShellId,"Project",uc.Project,"UpdateData(this, "+ uc.ShellId+",true)",false,"","",true,"","","Project","form-control editable"),
                                        Helper.GenerateDropdown(uc.ShellId,"QualityProject",new SelectList(new List<SelectListItem>() { new SelectListItem { Text = uc.QualityProject, Value = uc.QualityProject } },"Value", "Text",uc.QualityProject),"","UpdateData(this, "+ uc.ShellId+")"),
                                        Helper.GenerateTextbox(uc.ShellId, "Customer",uc.Customer,"UpdateData(this, "+ uc.ShellId+",true)",true, "","","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.ShellId, "BU",uc.BU,"UpdateData(this, "+ uc.ShellId +",true)", false, "", "",false,"","","BU","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.ShellId,"ItemId",uc.PositionNo,"UpdateData(this, "+ uc.ShellId+",true)",false,"","",true,"","","Position No","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.ShellId, "ShellMark", uc.ShellMark, "UpdateData(this, "+ uc.ShellId+",true)",false,"","",true,"","","ShellMark","form-control editable"),

                                        Helper.HTMLAutoComplete(uc.ShellId, "PlannedReceipt",uc.PlannedReceipt,"UpdateData(this, "+ uc.ShellId+",true)",false,"","",false,"","","PlannedReceipt","form-control editable"),
                                        Helper.GenerateDropdownWithSelected(uc.ShellId,"WorkCenter",clsImplementationEnum.getWorkCenterEnum(),"",uc.WorkCenter),
                                        Helper.GenerateDropdownWithSelected(uc.ShellId,"CsWep",clsImplementationEnum.getCsWepEnum(),"",uc.CsWep),
                                        Helper.GenerateDropdownWithSelected(uc.ShellId,"LsWep",clsImplementationEnum.getLsWepEnum(),"",uc.LsWep),
                                        Helper.GenerateDropdownWithSelected(uc.ShellId,"EdgeBreak",clsImplementationEnum.getEdgeBreakEnum(),"",uc.EdgeBreak),
                                        //Helper.GenerateTextbox(uc.ShellId, "PlannedReceipt",uc.PlannedReceipt,"UpdateData(this, "+ uc.ShellId+",true)",false, "","","form-control editable"),
                                        Helper.HTMLAutoComplete(uc.ShellId,"PCRNO",  uc.PCRNO,"UpdateData(this, "+ uc.ShellId+",true)", false, "", "",false,"","","PCRNO","form-control editable"),

                                        Helper.HTMLAutoComplete(uc.ShellId, "PCLNumber",  uc.PCLNumber,"UpdateData(this, "+ uc.ShellId+",true)", false, "", "",false,"","","PCLNumber","form-control editable"),
                                         Helper.HTMLAutoComplete(uc.ShellId, "PCLStatus",  uc.PCLStatus,"UpdateData(this, "+ uc.ShellId+",true)", false, "", "",false,"","","PCLStatus","form-control editable"),

                                        Helper.GenerateTextbox(uc.ShellId,"Length",uc.Length.HasValue ? Convert.ToString(uc.Length.Value) : "" ,"UpdateData(this, "+ uc.ShellId+",true)" , false, "", "","form-control editable numeric",false,"","Length"),
                                        Helper.GenerateTextbox(uc.ShellId,"Width",uc.Width.HasValue ? Convert.ToString(uc.Width.Value) : "" ,"UpdateData(this, "+ uc.ShellId+",true)" , false, "", "","form-control editable numeric",false,"","Width"),
                                        Helper.GenerateTextbox(uc.ShellId,"Thk",uc.Thk.HasValue ? Convert.ToString(uc.Thk.Value) : "" ,"UpdateData(this, "+ uc.ShellId+",true)" , false, "", "","form-control editable numeric",false,"","Thk"),
                                        Helper.GenerateTextbox(uc.ShellId,"Weight",uc.Weight.HasValue ? Convert.ToString(uc.Weight.Value) : "" ,"UpdateData(this, "+ uc.ShellId+",true)" , false, "", "","form-control editable numeric",false,"","Weight"),
                                         Helper.HTMLAutoComplete(uc.ShellId, "MOC",  uc.MOC,"UpdateData(this, "+ uc.ShellId+",true)", false, "", "",false,"","","MOC","form-control editable"),
                                           Helper.HTMLAutoComplete(uc.ShellId, "RollingType",  uc.RollingType,"UpdateData(this, "+ uc.ShellId+",true)", false, "", "",false,"","","RollingType","form-control editable"),
                                            Helper.HTMLAutoComplete(uc.ShellId, "SeamNo",  uc.SeamNo,"UpdateData(this, "+ uc.ShellId+",true)", false, "", "",false,"","","SeamNo","form-control editable"),
                                       Helper.GenerateTextbox(uc.ShellId,"SeamStatus",uc.SeamStatus,"UpdateData(this, "+ uc.ShellId+",true)" , false, "", "","form-control editable numeric",false,"","SeamStatus"),
                                       Helper.GenerateCheckboxWithEvent(uc.ShellId,"IsPlanned",Convert.ToBoolean(uc.IsPlanned),"IsPlannedChanged(this,\"IsPlanned\", "+ uc.ShellId+")",true),
                                        Helper.GenerateTextbox(uc.ShellId,"PlannedStartDate",uc.PlannedStartDate.HasValue? uc.PlannedStartDate.Value.ToString("yyyy-MM-dd"): "","UpdateData(this, "+ uc.ShellId+",false)", false, "", "","form-control editable",false),
                                         Helper.GenerateTextbox(uc.ShellId,"PlannedEndDate",uc.PlannedEndDate.HasValue? uc.PlannedEndDate.Value.ToString("yyyy-MM-dd"): "","UpdateData(this, "+ uc.ShellId+",false)", false, "", "","form-control editable",false),
                                          Helper.GenerateTextbox(uc.ShellId,"ActualStartDate",uc.ActualStartDate.HasValue? uc.ActualStartDate.Value.ToString("yyyy-MM-dd"): "","UpdateData(this, "+ uc.ShellId+",false)", false, "", "","form-control editable",false),
                                           Helper.GenerateTextbox(uc.ShellId,"ActualEndDate",uc.ActualEndDate.HasValue? uc.ActualEndDate.Value.ToString("yyyy-MM-dd"): "","UpdateData(this, "+ uc.ShellId+",false)", false, "", "","form-control editable",false),
                                           Helper.GenerateTextbox(uc.ShellId, "CS_WEP_M_C",uc.CS_WEP_M_C,"UpdateData(this, "+ uc.ShellId+",true)",true, "","","form-control editable"),
                                           Helper.GenerateTextbox(uc.ShellId, "EdgeBracking",uc.EdgeBracking,"UpdateData(this, "+ uc.ShellId+",true)",true, "","","form-control editable"),
                                           Helper.GenerateTextbox(uc.ShellId, "Rolling",uc.Rolling,"UpdateData(this, "+ uc.ShellId+",true)",true, "","","form-control editable"),
                                           Helper.GenerateTextbox(uc.ShellId, "Setup",uc.Setup,"UpdateData(this, "+ uc.ShellId+",true)",true, "","","form-control editable"),
                                           Helper.GenerateTextbox(uc.ShellId, "Welding",uc.Welding,"UpdateData(this, "+ uc.ShellId+",true)",true, "","","form-control editable"),
                                           Helper.GenerateTextbox(uc.ShellId, "NDT",uc.NDT,"UpdateData(this, "+ uc.ShellId+",true)",true, "","","form-control editable"),
                                            Helper.GenerateTextbox(uc.ShellId, "Dispatch",uc.Dispatch,"UpdateData(this, "+ uc.ShellId+",true)",true, "","","form-control editable"),
                                           Helper.GenerateTextbox(uc.ShellId,"DispatchDate",uc.DispatchDate.HasValue? uc.DispatchDate.Value.ToString("yyyy-MM-dd"): "","UpdateData(this, "+ uc.ShellId+",false)", false, "", "","form-control editable",false),
                                           Helper.GenerateTextbox(uc.ShellId, "DeliveryLocation",uc.DeliveryLocation,"UpdateData(this, "+ uc.ShellId+",true)",true, "","","form-control editable"),
                                        "<span class='editable'><a id='Cancel' name='Cancel' title='Cancel' class='blue action' onclick='ReloadGrid()'>"+
                                        "<i class='fa fa-close'></i></a> &nbsp &nbsp <a id='View" + uc.ShellId + "' name='View' title='View' class='blue action "+(uc.IsPlanned == true ? "save" : "") + "' onclick='ViewRecord(" + uc.ShellId + ")'>"+
                                        "<i class='fa fa-eye'></i></a> &nbsp &nbsp<a id='Attachment" + uc.ShellId + "' name='Attachment' title='Attachment' class='blue action "+(uc.IsPlanned == true ? "save" : "") + "' onclick='ViewAttachment(" + uc.ShellId + ")'>"+
                                        "<i class='fa fa-paperclip'></i></a> &nbsp &nbsp" +
                                       "<a id='CameraURL" + uc.ShellId + "' name='CameraURL' title=\"URL\" class=\"blue action\" onclick=\"RedirectCameraURL(this," + uc.ShellId + ")\">"+
                                        "<i class=\"iconclass fa fa-camera\"></i></a>&nbsp &nbsp"+
                                        "<a id='QRcode" + uc.ShellId + "' name='QRcode' title=\"QRcode\" class=\"blue action\" onclick=\"ViewQRcode(this," + uc.ShellId +")\">"+
                                        "<i class=\"iconclass fa fa-qrcode\"></i></a>&nbsp &nbsp"+
                                        "<a id='Seam" + uc.ShellId + "' name='Seam' title=\"Seam\" class=\"blue action\" onclick=\"Seamlist(this," + uc.ShellId +")\">"+
                                        "<i class=\"iconclass fa fa-th\"></i></a>" +
                                        "</span>",
                          }).ToList();
                        }
                        else
                        {
                            data = null;
                        }
                    }
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveNewRecord(FormCollection fc)
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var sfm001 = new SFM001();

                sfm001.Project = fc["Project" + 0];
                sfm001.BU = fc["BU" + 0];
                sfm001.WorkCenter = fc["WorkCenter" + 0];
                sfm001.CsWep = fc["CsWep" + 0];
                sfm001.LsWep = fc["LsWep" + 0];
                sfm001.EdgeBreak = fc["EdgeBreak" + 0];

                sfm001.PositionNo = fc["ItemId" + 0];
                sfm001.ShellMark = fc["ShellMark" + 0];

                //cop001.Overlay = fc["Overlay" + 0] == null ? false : (fc["Overlay" + 0].ToString().ToLower() == "no" ? false : true);// Convert.ToBoolean(fc["Overlay" + 0]);
                sfm001.MOC = fc["MOC" + 0];
                sfm001.QualityProject = fc["QualityProject" + 0];
                sfm001.IsPlanned = false;
                sfm001.Length = Convert.ToDouble(fc["Length" + 0]);
                sfm001.Width = Convert.ToDouble(fc["Width" + 0]);
                sfm001.Weight = Convert.ToDecimal(fc["Weight" + 0]);
                sfm001.Thk = Convert.ToDouble(fc["Thk" + 0]);
                sfm001.PlannedReceipt = fc["PlannedReceipt" + 0];
                sfm001.Dispatch = "No";

                string ItemNo = (sfm001.BU == "RPV") ? "5" : (sfm001.BU == "HTE") ? "1" : "All";

                var itemdet = GetListItemIdEnt(sfm001.Project, ItemNo);
                var itemDetails = itemdet.Where(y => y.FindNo == sfm001.PositionNo && y.Project == sfm001.Project).ToList();
                //if (fc["IsPlanned" + 0] == null) { sfm001.IsPlanned = null; }
                //else { sfm001.IsPlanned = (fc["IsPlanned" + 0].ToString().ToLower() == "on" ? true : false); }
                //sfm001.Length = Convert.ToDouble(fc["Length" + 0]);
                //sfm001.PCRNO = fc["PCRNO" + 0];
                //sfm001.PCLNumber = fc["PCLNumber" + 0];
                //sfm001.PCLStatus = fc["PCLStatus" + 0];
                //sfm001.RollingType = fc["RollingType" + 0];
                //sfm001.RollingMachine = fc["RollingMachine" + 0];

                //sfm001.SeamNo = fc["SeamNo" + 0];
                //sfm001.SeamStatus = fc["SeamStatus" + 0];
                //sfm001.CS_WEP_M_C = fc["CS_WEP_M_C" + 0];
                //sfm001.EdgeBracking = fc["EdgeBracking" + 0];
                //sfm001.L_S_WEP_M_C = fc["L_S_WEP_M_C" + 0];

                //sfm001.Rolling = fc["Rolling" + 0];
                //sfm001.Setup = fc["Setup" + 0];
                //sfm001.Welding = fc["Welding" + 0];
                //sfm001.NDT = fc["NDT" + 0];
                //sfm001.Dispatch = fc["Dispatch" + 0];

                //sfm001.DeliveryLocation = fc["DeliveryLocation" + 0];
                //sfm001.PlannedStartDate = (!string.IsNullOrEmpty(fc["PlannedStartDate" + 0]) ? Convert.ToDateTime(fc["PlannedStartDate" + 0]) : (DateTime?)null);
                //sfm001.PlannedEndDate = (!string.IsNullOrEmpty(fc["PlannedEndDate" + 0]) ? Convert.ToDateTime(fc["PlannedEndDate" + 0]) : (DateTime?)null);
                //sfm001.ActualStartDate = (!string.IsNullOrEmpty(fc["ActualStartDate" + 0]) ? Convert.ToDateTime(fc["ActualStartDate" + 0]) : (DateTime?)null);
                //sfm001.ActualEndDate = (!string.IsNullOrEmpty(fc["ActualEndDate" + 0]) ? Convert.ToDateTime(fc["ActualEndDate" + 0]) : (DateTime?)null);
                //if (!string.IsNullOrWhiteSpace(fc["DispatchDate" + 0]))
                //{
                //    sfm001.DispatchDate = Convert.ToDateTime(fc["DispatchDate" + 0]);
                //}
                sfm001.CreatedOn = DateTime.Now;
                sfm001.CreatedBy = objClsLoginInfo.UserName;

                var ProjectDetcount = (from a in dbSFM.SFM001
                                       where a.Project == sfm001.Project && a.PositionNo == sfm001.PositionNo
                                       select new
                                       {
                                           Project = a.Project
                                       }).ToList();

                // make changes from here 

                var isExist = false;
                if ((itemDetails.Count == 0) || (ProjectDetcount.Count() >= Convert.ToInt32(itemDetails[0].NoOfPieces)))
                {
                    isExist = true;
                    // if you are uncommetting line 497 to 564 code then commetted below 3 line code.
                    objResponseMsg.Key = false;
                    if (itemDetails.Count == 0)
                        objResponseMsg.Value = "No of pieces doesn't exists for this project with position no";
                    else
                        objResponseMsg.Value = "Entries already exists or no of pieces is zero";
                    return Json(objResponseMsg);
                }
                else if (dbSFM.SFM001.Where(x => x.Project == sfm001.Project &&
                                    x.PositionNo == sfm001.PositionNo).Any())
                {
                    isExist = true;
                    //&& x.ShellMark == sfm001.ShellMark
                    // if you are uncommetting line 497 to 564 code then commetted below 3 line code.
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.Message;
                    return Json(objResponseMsg);
                }
                else
                {
                    for (int i = 0; i < Convert.ToInt32(itemDetails[0].NoOfPieces); i++)
                    {
                        objResponseMsg.Key = true;
                        dbSFM.SFM001.Add(sfm001);
                        dbSFM.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg);
        }

        public void BindDropDown(DateTime? fromDate)
        {
            ViewData["DrpBU"] = new string[] { "RPV", "HTE", "NU", "MRU", "LTPC", "Other" };
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType)
        {
            var objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                var lst = dbSFM.SP_SFM_GET_INDEX_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Data Found";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                var dtPlannedReceipt = new DateTime();
                var data = (from uc in lst
                            select new
                            {
                                Project = uc.Project,
                                QualityProject = uc.QualityProject,
                                Customer = uc.Customer,
                                BU = uc.BU,
                                PositionNo = uc.PositionNo,
                                ShellMark = uc.ShellMark,
                                PlannedReceipt = uc.PlannedReceipt,
                                WorkCenter = uc.WorkCenter,
                                CSWEP = uc.CsWep,
                                LSWEP = uc.LsWep,
                                EdgeBreak = uc.EdgeBreak,
                                PCRNO = uc.PCRNO,
                                PCLNumber = uc.PCLNumber,
                                PCLStatus = uc.PCLStatus,
                                Planned = (Convert.ToBoolean(uc.IsPlanned) ? "Yes" : "No"),
                                //GenerateCheckbox(uc.ShellId,"IsPlanned",uc.IsPlanned,true),
                                //uc.MOC,
                                Length = Convert.ToString(uc.Length),
                                Width = Convert.ToString(uc.Width),
                                Thk = Convert.ToString(uc.Thk),
                                Weight = Convert.ToString(uc.Weight),
                                MOC = uc.MOC,
                                RollingType = uc.RollingType,
                                RollingMachine = uc.RollingMachine,
                                SeamNo = uc.SeamNo,
                                SeamStatus = uc.SeamStatus,
                                //GenerateCheckbox(uc.ShellId,"IsPlanned",Convert.ToBoolean(uc.IsPlanned),true),
                                //(DateTime.TryParse(uc.PlannedReceipt+"",out dtPlannedReceipt) ? dtPlannedReceipt.ToString("dd/MM/yyyy") : uc.PlannedReceipt),

                                PlannedStartDate = uc.PlannedStartDate.HasValue ? uc.PlannedStartDate.Value.ToString("dd/MM/yyyy") : "",
                                PlannedEndDate = uc.PlannedEndDate.HasValue ? uc.PlannedEndDate.Value.ToString("dd/MM/yyyy") : "",
                                ActualStartDate = uc.ActualStartDate.HasValue ? uc.ActualStartDate.Value.ToString("dd/MM/yyyy") : "",
                                ActualEndDate = uc.ActualEndDate.HasValue ? uc.ActualEndDate.Value.ToString("dd/MM/yyyy") : "",

                                CS_WEP_M_C = uc.CS_WEP_M_C,
                                EdgeBracking = uc.EdgeBracking,
                                L_S_WEP_M_C = uc.L_S_WEP_M_C,
                                Rolling = uc.Rolling,
                                Setup = uc.Setup,
                                Welding = uc.Welding,
                                NDT = uc.NDT,
                                Dispatch = uc.Dispatch,
                                DispatchDate = uc.DispatchDate.HasValue ? uc.DispatchDate.Value.ToString("dd/MM/yyyy") : "",
                                DeliveryLocation = uc.DeliveryLocation,
                            }).ToList();

                strFileName = Helper.GenerateExcel(data, objClsLoginInfo.UserName);
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetContractorDetailsByProjectCode(string projectCode)
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var contractor = db.COM005.Where(i => i.t_sprj == projectCode).FirstOrDefault();
                var customer = Manager.GetCustomerProjectWise(projectCode);
                objResponseMsg.Key = true;
                return Json(new { Response = objResponseMsg, Contractor = contractor, Customer = customer });

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(new { Response = objResponseMsg });
        }
        public JsonResult GetItemIdForProject(string project, string ItemNo = "")
        {
            var itemDetails = GetListItemIdEnt(project, ItemNo);
            return Json(itemDetails, JsonRequestBehavior.AllowGet);
        }

        public List<SP_SFM_GET_LIST_ITEM_Result> GetListItemIdEnt(string project, string ItemNo = "")
        {
            return dbSFM.SP_SFM_GET_LIST_ITEM(project, ItemNo).ToList();
        }

        public JsonResult GetPositionByBU(string project, string BU = "", string ItemNo = "")
        {
            if (BU == "RPV") ItemNo = "5";
            else if (BU == "HTE") ItemNo = "1";
            else ItemNo = "All";
            var itemDetails = GetListItemIdEnt(project, ItemNo);
            return Json(itemDetails, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetPartForItemProject(string project, string item, string term)
        {
            var partDetails = db.SP_COP_GET_PART_DESCRIPTION_PROJECT(project, item, term).ToList();
            return Json(partDetails, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPlannedReceiptDate(string project, string item)
        {
            var plannedReceiptData = db.SP_COP_GET_PlannedReceiptDate(item, project).ToList();
            return Json(plannedReceiptData, JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        public static string GenerateCheckbox(int rowId, string columnName, bool columnValue = false, bool isDisabled = false)
        {
            string htmlControl = "";
            string inputID = columnName + "" + rowId.ToString();
            bool inputValue = columnValue;

            htmlControl = "<input type=\"checkbox\" id=\"" + inputID + "\"" + (inputValue ? " checked " : "") + " name=\"" + inputID + "\" colname=\"" + columnName + "\"" + (isDisabled ? " disabled = disabled" : "") + "/>";
            return htmlControl;
        }
    }



    public partial class resSFM001 : clsHelper.ResponseMsgWithStatus
    {
        public SFM001 objsfm { get; set; }
        public string D_DispatchDate { get; set; }
        public string ActualStartDate { get; set; }
        public string ActualEndDate { get; set; }
        public string PlannedStartDate { get; set; }
        public string PlannedEndDate { get; set; }
        public string PlannedReceipt { get; set; }
        public string CustomerName { get; set; }

    }

    public class CurrentMonthWeek
    {
        public string WeekNo { get; set; }
        public string Date { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }

    public partial class GET_PCRDetails
    {
        public string PCLNO { get; set; }
        public string PCLstatus { get; set; }
        public string PCRNO { get; set; }
        public string Project { get; set; }
        public string PositionNo { get; set; }
        public string id { get; set; }
        public string text { get; set; }
    }

    public partial class GET_PODate
    {
        public string PlannedReceiptDate { get; set; }
        public string Project { get; set; }
        public string PositionNo { get; set; }
    }

    public partial class GET_SFMDetails
    {
        public string Project { get; set; }
        public string PositionNo { get; set; }
        public string PCRNO { get; set; }
        public string PCLNO { get; set; }
        public string PCLstatus { get; set; }

        public string PlannedReceiptDate { get; set; }
    }
}