﻿using IEMQS.Areas.IPI.Controllers;
using IEMQS.Areas.IPI.Models;
using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace IEMQS.Areas.LTFPS.Controllers
{
    public class OfferInspectionController : clsBase
    {

        [SessionExpireFilter]
        public ActionResult Index(string tab = "")
        {
            ViewBag.Tab = tab;
            return View();
        }

        [HttpPost]
        public ActionResult LoadOfferReOfferInspectionDataPartial(string status)
        {
            if (status.ToLower() == "reoffer")
            {
                ViewBag.Status = "Pending";
                return PartialView("_LoadReOfferDataPartial");
            }
            else if (status.ToLower() == "returnnderequest")
            {
                ViewBag.Status = "ReturnNDERequest";
                return PartialView("_LoadReturnNDERequestDataPartial");
            }
            else
            {
                ViewBag.Status = status;
                return PartialView("_LoadOfferDataPartial");
            }
        }

        [HttpPost]
        public JsonResult LoadOfferData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string _urlFrom = "all";
                string strWhere = string.Empty;

                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and InspectionStatus in('" + clsImplementationEnum.PartlistOfferInspectionStatus.READY_TO_OFFER.GetStringValue() + "')";
                    _urlFrom = "offer";
                }
                else
                {
                    strWhere += "1=1";
                }

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "ltf002.BU", "ltf002.Location");
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "QualityProject",
                                                "ltrim(rtrim(ltf002.BU))+' - '+bcom2.t_desc",
                                                "LTFPSNo",
                                                "LTFPSRevNo",
                                                "OperationNo",
                                                "OperationDescription",
                                                "SeamNo",
                                                "PartNo",
                                                "TestType",
                                                "Stage",
                                                "InspectionStatus",
                                                "ltrim(rtrim(ltf002.Location))+' - '+lcom2.t_desc"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var lstResult = db.SP_FETCH_LTFPSOFFERINSPECTION_HEADERS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.QualityProject),
                               Convert.ToString(uc.BU),
                               Convert.ToString(uc.LTFPSNo),
                               Convert.ToString(uc.LTFPSRevNo),
                               Convert.ToString(uc.OperationNo),
                               Convert.ToString(uc.OperationDescription),
                               Convert.ToString(uc.SeamNo),
                               Convert.ToString(uc.PartNo),
                               Convert.ToString(uc.TestType),
                               Convert.ToString(uc.Stage),
                               Convert.ToString(uc.InspectionStatus),
                               Convert.ToString(uc.Location),
                               "<center style=\"white-space:nowrap;\"><a title=\"View Details\" class='' href='" + WebsiteURL + "/LTFPS/OfferInspection/ViewDetails?LineID="+Convert.ToInt32(uc.LineId)+"&from="+ _urlFrom +"'><i style='margin-left:5px;' class='fa fa-eye'></i></a>"+(!string.IsNullOrWhiteSpace(uc.InspectionStatus) && uc.InspectionStatus == clsImplementationEnum.PartlistOfferInspectionStatus.READY_TO_OFFER.GetStringValue() ? "<a title=\"Request Offer\" class='' onclick=\"RequestOffer("+uc.LineId+",'"+uc.TestFor+"')\" ><i style='margin-left:5px;cursor:pointer' class='fa fa-paper-plane'  title=\"Request Offer\"></i></a>":"<a title=\"Request Offer\" ><i style='margin-left:5px;cursor:pointer;opacity:0.3;' class='fa fa-paper-plane'  title=\"Request Offer\"></i></a>")+Manager.generateLTFPSGridButtons(uc.LTFPSNo,uc.QualityProject,uc.Project,uc.BU,uc.Location,!string.IsNullOrWhiteSpace(uc.SeamNo)?uc.SeamNo:uc.PartNo,uc.Stage, Convert.ToString(uc.OperationNo),!string.IsNullOrWhiteSpace(uc.SeamNo)?"Seam Details":"Part Details",Convert.ToString(uc.isNDEStage),!string.IsNullOrWhiteSpace(uc.SeamNo)?"true":"false",!string.IsNullOrWhiteSpace(uc.SeamNo)?"Seam Details":"Part Details",!string.IsNullOrWhiteSpace(uc.SeamNo)?"Seam Request Details":"Part Request Details")+"</center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadReOfferData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string _urlFrom = "all";
                string strWhere = string.Empty;

                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and (tm.TestResult in('') or tm.TestResult is null) and tm.OfferedBy is null and tm.OfferedOn is null ";
                    _urlFrom = "reoffer";
                }
                else
                {
                    strWhere += "1=1 ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "QualityProject",
                                                "dbo.GET_BULOCATIONDEPARTMENTDESC_BY_CODE(tm.BU)",
                                                "OperationNo",
                                                "OperationDescription",
                                                "SeamNo",
                                                "PartNo",
                                                "TestType",
                                                "StageCode",
                                                "IterationNo",
                                                "RequestNo",
                                                "RequestNoSequence",
                                                "TestResult",
                                                "LTFPSNo"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "tm.BU", "tm.Location");
                var lstResult = db.SP_FETCH_LTFPS_RE_OFFERINSPECTION_HEADERS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.QualityProject),
                               Convert.ToString(uc.BU),
                               Convert.ToString(uc.LTFPSNo),
                               Convert.ToString(uc.OperationNo),
                                Convert.ToString(uc.OperationDescription),
                               Convert.ToString(uc.SeamNo),
                               Convert.ToString(uc.PartNo),
                               Convert.ToString(uc.TestType),
                               Convert.ToString(uc.StageCode),
                               Convert.ToString(uc.IterationNo),
                               Convert.ToString(uc.RequestNo),
                               Convert.ToString(uc.RequestNoSequence),
                               Convert.ToString(uc.TestResult),
                               "<center style=\"white-space: nowrap;\" class=\"clsDisplayInlineEle\">"+GetReOfferActionLink(uc.TableName,uc.RequestId,_urlFrom) + Manager.generateLTFPSGridButtons(uc.LTFPSNo,uc.QualityProject,uc.Project,uc.BU,uc.Location,!string.IsNullOrWhiteSpace(uc.SeamNo)?uc.SeamNo:uc.PartNo,uc.StageCode, Convert.ToString(uc.OperationNo),!string.IsNullOrWhiteSpace(uc.SeamNo)?"Seam Details":"Part Details",Convert.ToString(false),!string.IsNullOrWhiteSpace(uc.SeamNo)?"true":"false",!string.IsNullOrWhiteSpace(uc.SeamNo)?"Seam Details":"Part Details",!string.IsNullOrWhiteSpace(uc.SeamNo)?"Seam Request Details":"Part Request Details")+"</center>"
                              // "<center style=\"white-space: nowrap;\"><a title=\"View Details\" class='' href='/LTFPS/OfferInspection/ReOfferedDetails?RequestId="+Convert.ToInt32(uc.RequestId)+"&from="+ _urlFrom +"'><i style='margin-left:5px;' class='fa fa-eye'></i></a></center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string GetReOfferActionLink(string FromTable, int RequestId, string from = "")
        {
            string strAction = string.Empty;

            if (FromTable.ToUpper() == "PART" || FromTable.ToUpper() == "ASSEMBLY")
            {
                strAction += "<a title=\"View Details\" class='' href='" + WebsiteURL + "/IPI/PartlistOfferInspection/ViewReOfferedDetails?RequestId=" + Convert.ToInt32(RequestId) + "&from=" + from + "'><i class='fa fa-eye'></i></a>";
            }
            else
            {
                strAction += "<a href=\"" + WebsiteURL + "/IPI/MaintainSeamListOfferInspection/SendReOfferView/" + RequestId + "\" Title=\"View Details\" ><i style=\"margin - left:5px;\" class=\"fa fa-eye\"></i></a>";
            }
            return strAction;
        }

        [SessionExpireFilter]
        public ActionResult ViewDetails(int LineID = 0, string from = "")
        {
            ViewBag.fromURL = from;
            ViewBag.IsWeldLengthRequired = false;
            ViewBag.IsOverlay = false;
            ViewBag.SeamLength = 0;
            if (LineID > 0)
            {
                LTF002 objLTF002 = db.LTF002.Where(x => x.LineId == LineID).FirstOrDefault();
                if (objLTF002 != null)
                {
                    bool IsSTAGEDEPARTMENTNDE = db.QMS002.Where(x => x.Location == objLTF002.Location && x.BU == objLTF002.BU && x.StageCode == objLTF002.Stage && x.StageDepartment == "NDE").Any();

                    if (objLTF002.TestFor.ToLower() == "seam")
                    {
                        ViewBag.IsOverlay = Manager.IsOverlaySeam(objLTF002.QualityProject, objLTF002.SeamNo);
                        ViewBag.IsWeldLengthRequired = (!Manager.IsNDERequestConfirmationRequiredOnFirstInteration(objLTF002.Project, objLTF002.BU, objLTF002.Location) && Manager.IsNDERequest(objLTF002.Stage, objLTF002.BU, objLTF002.Location));

                        QMS012 objQMS012 = db.QMS012.FirstOrDefault(c => c.QualityProject == objLTF002.QualityProject && c.SeamNo == objLTF002.SeamNo);
                        if (objQMS012 != null)
                        {
                            ViewBag.SeamLength = objQMS012.SeamLengthArea;
                        }

                    }

                    string stageCodeDesc = db.Database.SqlQuery<string>("select  dbo.GET_IPI_STAGECODE_DESCRIPTION('" + objLTF002.Stage + "','" + objLTF002.BU + "','" + objLTF002.Location + "')").FirstOrDefault();

                    var projectDetails = db.COM001.Where(x => x.t_cprj == objLTF002.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                    ViewBag.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                    var BUDescription = (from a in db.COM002
                                         where a.t_dtyp == 2 && a.t_dimx == objLTF002.BU
                                         select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
                    objLTF002.BU = Convert.ToString(BUDescription.BUDesc);
                    objLTF002.Stage = stageCodeDesc;
                    var locDescription = (from a in db.COM002
                                          where a.t_dtyp == 1 && a.t_dimx == objLTF002.Location
                                          select new { Location = a.t_dimx + "-" + a.t_desc }).FirstOrDefault();
                    objLTF002.Location = locDescription.Location;
                    objLTF002.CreatedBy = Manager.GetPsidandDescription(objLTF002.CreatedBy);
                    // ViewBag.fromURL = Convert.ToString(objLTF002.UOM);  // used UOM for from URL

                    #region Attached Protocol by SHOP user 
                    if (objLTF002.TestFor.ToLower() == "part" || objLTF002.TestFor.ToLower() == "assembly")
                    {
                        if (IsSTAGEDEPARTMENTNDE)
                        {
                            //NDE
                            ViewBag.folderPath = "QMS065_LTFPS//" + objLTF002.LineId + "//" + 1 + "//" + 1;

                        }
                        else
                        {
                            //Non NDE
                            ViewBag.folderPath = "QMS055_LTFPS//" + objLTF002.LineId + "//" + 1 + "//" + 1;
                        }
                    }
                    else
                    {
                        if (IsSTAGEDEPARTMENTNDE)
                        {
                            ViewBag.folderPath = "QMS060_LTFPS//" + objLTF002.LineId + "//" + 1 + "//" + 1;
                        }
                        else
                        {
                            ViewBag.folderPath = "QMS050_LTFPS//" + objLTF002.LineId + "//" + 1 + "//" + 1;
                        }
                    }
                    ViewBag.TableId = objLTF002.LineId;

                    #endregion

                    return View(objLTF002);
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
            }

        }

        [HttpPost]
        public ActionResult CaptureWeldersForLTFPSBeforeOffer(int ltfpsHeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            List<string> _weldingProcess = new List<string>();
            string approvedString = clsImplementationEnum.QualityIdStageStatus.APPROVED.GetStringValue();
            List<WPS025> lstWPS025 = new List<WPS025>();
            LTF002 objLTF002 = db.LTF002.FirstOrDefault(x => x.LineId == ltfpsHeaderId);
            List<QMS061> lstWelderList = db.QMS061.Where(x => x.LTFPSHeaderId == ltfpsHeaderId).ToList();
            QMS031 objQMS031 = db.QMS031.Where(c => c.QualityProject == objLTF002.QualityProject && c.Project == objLTF002.Project && c.BU == objLTF002.BU && c.Location == objLTF002.Location && c.SeamNo == objLTF002.SeamNo && c.StageCode == objLTF002.Stage && c.StageStatus == approvedString).FirstOrDefault();

            List<string> lstAssigedWeldwers = lstWelderList.Select(y => y.welderstamp).ToList();

            lstWPS025 = db.WPS025.Where(x => x.BU == objLTF002.BU && x.QualityProject == objLTF002.QualityProject && x.Project == objLTF002.Project && x.SeamNo == objLTF002.SeamNo).OrderBy(o => o.PrintedOn).ToList();

            if (lstWPS025.Count > 0)
            {
                List<QMS061> lstQMS061 = new List<QMS061>();
                List<string> lstWelders = new List<string>();
                int count = 1;
                //if (lstWelderList.Count > 0)
                //{
                //    count = lstWelderList.Count + 1;
                //}
                foreach (var wps25 in lstWPS025)
                {
                    if (!lstWelders.Contains(wps25.WelderStamp) && !lstWelderList.Select(y => y.welderstamp).Contains(wps25.WelderStamp))
                    {
                        QMS061 objQMS061 = new QMS061
                        {
                            HeaderId = null,
                            RefRequestId = null,
                            LTFPSHeaderId = Convert.ToInt32(objLTF002.LineId),
                            QualityProject = objLTF002.QualityProject,
                            Project = objLTF002.Project,
                            BU = objLTF002.BU,
                            Location = objLTF002.Location,
                            SeamNo = objLTF002.SeamNo,
                            StageCode = objLTF002.Stage,
                            StageSequence = (objQMS031 != null ? objQMS031.StageSequance : 0),
                            IterationNo = null,
                            RequestNo = null,
                            RequestNoSequence = null,
                            welderstamp = wps25.WelderStamp,
                            WeldingProcess = wps25.WeldingProcess,
                            CreatedBy = objClsLoginInfo.UserName,
                            CreatedOn = DateTime.Now
                        };
                        lstQMS061.Add(objQMS061);
                        lstWelders.Add(wps25.WelderStamp);
                        _weldingProcess.Add(wps25.WeldingProcess);
                        count++;
                    }
                    else
                    {
                        if (lstWelderList.Select(y => y.welderstamp).Contains(wps25.WelderStamp))
                        {
                            QMS061 existingWelder = lstWelderList.FirstOrDefault(x => x.welderstamp == wps25.WelderStamp);
                            if (existingWelder != null && !existingWelder.WeldingProcess.Split('+').Contains(wps25.WeldingProcess))
                                existingWelder.WeldingProcess = existingWelder.WeldingProcess + "+" + wps25.WeldingProcess;
                        }
                        else if (lstQMS061.Select(y => y.welderstamp).Contains(wps25.WelderStamp))
                        {
                            lstQMS061.Where(w => w.welderstamp == wps25.WelderStamp && !w.WeldingProcess.Split('+').Contains(wps25.WeldingProcess)).ToList().ForEach(x => x.WeldingProcess = (x.WeldingProcess + "+" + wps25.WeldingProcess));
                        }

                        _weldingProcess.Add(wps25.WeldingProcess);

                    }
                }
                db.QMS061.AddRange(lstQMS061);
                db.SaveChanges();

                objResponseMsg.Remarks = string.Join("+", _weldingProcess.Distinct());
                objResponseMsg.Status = string.Join(",", lstWelders);
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Welders Captured Successfully";
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please add Welders in Welder wise Parameter Slip";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetOfferPartial(int LineID)
        {
            LTF002 objLTF002 = db.LTF002.Where(x => x.LineId == LineID).FirstOrDefault();
            LTFSOfferModel objLTFSOfferModel = new LTFSOfferModel();
            string TestFor = objLTF002.TestFor.ToLower();
            ViewBag.TestFor = TestFor;

            List<GLB002> lstGLB002 = Manager.GetSubCatagories("Shop", objLTF002.BU, objLTF002.Location).ToList();
            List<BULocWiseCategoryModel> lstBULocWiseCategoryModel = lstGLB002.Select(x => new BULocWiseCategoryModel { CatDesc = (x.Code + "-" + x.Description), CatID = x.Code }).ToList();
            ViewBag.lstSubCatagories = new JavaScriptSerializer().Serialize(lstBULocWiseCategoryModel);

            List<GLB002> lstGLB002SurfaceCondition = Manager.GetSubCatagories("Surface Condition", objLTF002.BU, objLTF002.Location).ToList();
            List<BULocWiseCategoryModel> lstBULocWiseSurfaceCondition = lstGLB002SurfaceCondition.Select(x => new BULocWiseCategoryModel { CatDesc = (x.Description), CatID = x.Code }).ToList();
            ViewBag.lstSurfaceCondition = new JavaScriptSerializer().Serialize(lstBULocWiseSurfaceCondition);

            //QMS055 objQMS055 = db.QMS055.Where(x=>x.HeaderId)
            var IsProtocolApplicable = objLTF002.DocumentRequired;
            if (IsProtocolApplicable)
                ViewBag.ProtocolApplicable = "Yes";
            else
                ViewBag.ProtocolApplicable = "No";
            bool IsSTAGEDEPARTMENTNDE = db.QMS002.Where(x => x.Location == objLTF002.Location && x.BU == objLTF002.BU && x.StageCode == objLTF002.Stage && x.StageDepartment == "NDE").Any();
            ViewBag.IsSTAGEDEPARTMENTNDE = IsSTAGEDEPARTMENTNDE;
            #region Attachment 
            if (TestFor.ToLower() == "part" || TestFor.ToLower() == "assembly")
            {
                if (IsSTAGEDEPARTMENTNDE)
                {
                    //NDE
                    objLTFSOfferModel.folderPath = "QMS065_LTFPS//" + objLTF002.LineId + "//" + 1 + "//" + 1;
                }
                else
                {
                    //Non NDE
                    objLTFSOfferModel.folderPath = "QMS055_LTFPS//" + objLTF002.LineId + "//" + 1 + "//" + 1;
                }
            }
            else
            {
                if (IsSTAGEDEPARTMENTNDE)
                {
                    objLTFSOfferModel.folderPath = "QMS060_LTFPS//" + objLTF002.LineId + "//" + 1 + "//" + 1;
                }
                else
                {
                    objLTFSOfferModel.folderPath = "QMS050_LTFPS//" + objLTF002.LineId + "//" + 1 + "//" + 1;
                }
            }
            ViewBag.TableId = objLTF002.LineId;
            if (objLTF002.DocumentRequired)
            {
                objLTFSOfferModel.ISDocRequired = true;
            }
            #endregion
            NDEModels objNDEModels = new NDEModels();
            QMS002 objQMS002 = db.QMS002.Where(x => x.Location == objLTF002.Location && x.BU == objLTF002.BU && x.StageCode == objLTF002.Stage).FirstOrDefault();
            ViewBag.SurfaceCondition = objQMS002 != null ? objNDEModels.GetCategory("Surface Condition", objQMS002.DefaultSurfaceCondition, objQMS002.BU, objQMS002.Location, true).CategoryDescription : string.Empty;
            ViewBag.SurfaceConditionCode = objQMS002 != null ? objQMS002.DefaultSurfaceCondition : string.Empty;
            return PartialView("_GetOfferPartial", objLTFSOfferModel);
        }

        [HttpPost]
        public ActionResult SaveOffer(string TestFor, LTFSOfferModel objLTFSOfferModel, int OfferedQuantity = 0)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            clsHelper.ResponseMsg objResponse = new clsHelper.ResponseMsg();
            try
            {
                string ShopLocation = objLTFSOfferModel.ShopLocation;
                string ShopRemark = objLTFSOfferModel.ShopRemark;
                string Shop = objLTFSOfferModel.Shop;
                int LineId = objLTFSOfferModel.LineID;
                string surfaceCondition = objLTFSOfferModel.surfaceCondition;
                int Iteration = 1;
                int? weldThicknessReinforcement = objLTFSOfferModel.weldThicknessReinforcement.HasValue ? objLTFSOfferModel.weldThicknessReinforcement.Value : 0;

                objResponse = Manager.IsApplicableForOffer(LineId, TestFor, true);
                if (!objResponse.Key)
                {
                    return Json(objResponse);
                }

                if (TestFor.ToLower() == "part" || TestFor.ToLower() == "assembly")
                {
                    #region Part Offer
                    LTF002 objLTF002 = db.LTF002.Where(x => x.LineId == LineId).FirstOrDefault();
                    QMS036 objQMS036 = db.QMS036.Where(x =>
                                                            x.QualityProject == objLTF002.QualityProject &&
                                                            x.Project == objLTF002.Project &&
                                                            x.BU == objLTF002.BU &&
                                                            x.Location == objLTF002.Location &&
                                                            x.PartNo == objLTF002.PartNo &&
                                                            //x.ParentPartNo == objQMS045.ParentPartNo &&
                                                            //x.ChildPartNo == objQMS045.ChildPartNo &&
                                                            x.StageCode == objLTF002.Stage).FirstOrDefault();

                    if (objLTF002 != null)
                    {

                        QMS002 objQMS002 = db.QMS002.Where(x => x.StageDepartment.ToUpper() == "NDE" && x.StageCode == objLTF002.Stage && x.BU == objLTF002.BU && x.Location == objLTF002.Location).FirstOrDefault();

                        if (objQMS002 != null)
                        {
                            #region NDE Entry

                            long RequestNo = 0;
                            int ReqSeqNo = 0;

                            //RequestNo = db.Database.SqlQuery<long>("SELECT dbo.FN_GENERATE_MAX_IPI_SEAMNDEREQUESTNO('QMS065')").FirstOrDefault();

                            ObjectParameter outParm = new ObjectParameter("result", typeof(long));
                            var SPResult = db.SP_IPI_GET_NEXT_REQUEST_NO(objLTF002.BU, objLTF002.Location, outParm);
                            RequestNo = (long)outParm.Value;

                            ReqSeqNo = 1;


                            //int fNo = Convert.ToInt32(objLTF002.PartNo);
                            var result = Manager.GetHBOMData(objLTF002.Project).Where(x => x.FindNo == objLTF002.PartNo).FirstOrDefault();

                            //var result = db.VW_IPI_GETHBOMLIST.Where(x => x.Project == objLTF002.Project && x.FindNo == objLTF002.PartNo).FirstOrDefault();

                            string partNo = Convert.ToString(objLTF002.PartNo);

                            QMS065 objQMS065 = db.QMS065.Add(new QMS065
                            {
                                HeaderId = null,
                                LTFPSHeaderId = LineId,
                                QualityProject = objLTF002.QualityProject,
                                Project = objLTF002.Project,
                                BU = objLTF002.BU,
                                Location = objLTF002.Location,
                                PartAssemblyFindNumber = objLTF002.PartNo,
                                PartAssemblyChildItemCode = result.Part,
                                ChildItemCodeDesc = result.Description,
                                StageCode = objLTF002.Stage,
                                StageSequence = objLTF002.StageSequence,
                                IterationNo = Iteration,
                                RequestNo = RequestNo,
                                RequestNoSequence = ReqSeqNo,
                                RequestStatus = clsImplementationEnum.NDESeamRequestStatus.NotAttended.GetStringValue(),
                                ManufacturingCode = db.QMS010.Where(x => x.QualityProject == objLTF002.QualityProject).Select(x => x.ManufacturingCode).FirstOrDefault(),
                                TPIAgency1 = objQMS036 != null ? objQMS036.FirstTPIAgency : null,
                                TPIAgency1Intervention = objQMS036 != null ? objQMS036.FirstTPIIntervention : null,
                                TPIAgency2 = objQMS036 != null ? objQMS036.SecondTPIAgency : null,
                                TPIAgency2Intervention = objQMS036 != null ? objQMS036.SecondTPIIntervention : null,
                                TPIAgency3 = objQMS036 != null ? objQMS036.ThirdTPIAgency : null,
                                TPIAgency3Intervention = objQMS036 != null ? objQMS036.ThirdTPIIntervention : null,
                                TPIAgency4 = objQMS036 != null ? objQMS036.FourthTPIAgency : null,
                                TPIAgency4Intervention = objQMS036 != null ? objQMS036.FourthTPIIntervention : null,
                                TPIAgency5 = objQMS036 != null ? objQMS036.FifthTPIAgency : null,
                                TPIAgency5Intervention = objQMS036 != null ? objQMS036.FifthTPIIntervention : null,
                                TPIAgency6 = objQMS036 != null ? objQMS036.SixthTPIAgency : null,
                                TPIAgency6Intervention = objQMS036 != null ? objQMS036.SixthTPIIntervention : null,
                                Shop = Shop,
                                ShopLocation = ShopLocation,
                                ShopRemark = ShopRemark,
                                OfferedQuantity = OfferedQuantity,
                                ProductionDrawingNo = null,
                                CreatedBy = objClsLoginInfo.UserName,
                                CreatedOn = DateTime.Now,
                                OfferedBy = objClsLoginInfo.UserName,
                                OfferedOn = DateTime.Now
                            });
                            objLTF002.RequestNo = RequestNo;
                            objLTF002.EditedBy = objClsLoginInfo.UserName;
                            objLTF002.EditedOn = DateTime.Now;
                            objLTF002.IterationNo = Iteration;
                            db.SaveChanges();

                            objLTF002.InspectionStatus = clsImplementationEnum.PartlistOfferInspectionStatus.UNDER_INSPECTION.GetStringValue();

                            db.SaveChanges();

                            #region Send Notification
                            (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), objLTF002.Project, objLTF002.BU, objLTF002.Location, "Stage: " + objLTF002.Stage + " of Part: " + objLTF002.PartNo + " & Project No: " + objLTF002.QualityProject + " has been offered for Inspection", clsImplementationEnum.NotificationType.Information.GetStringValue());
                            #endregion

                            #endregion
                        }
                        else
                        {
                            long RequestNo = 0;
                            int ReqSeqNo = 0;

                            #region Non NDE

                            //RequestNo = db.Database.SqlQuery<long>("SELECT dbo.FN_GENERATE_MAX_IPI_SEAMNDEREQUESTNO('QMS055')").FirstOrDefault();

                            ObjectParameter outParm = new ObjectParameter("result", typeof(long));
                            var SPResult = db.SP_IPI_GET_NEXT_REQUEST_NO(objLTF002.BU, objLTF002.Location, outParm);
                            RequestNo = (long)outParm.Value;

                            ReqSeqNo = 1;

                            //int fNo = Convert.ToInt32(objLTF002.PartNo);
                            var result = Manager.GetHBOMData(objLTF002.Project).Where(x => x.FindNo == objLTF002.PartNo).FirstOrDefault();
                            //var result = db.VW_IPI_GETHBOMLIST.Where(x => x.Project == objLTF002.Project && x.FindNo == objLTF002.PartNo).FirstOrDefault();
                            QMS055 objQMS055 = db.QMS055.Add(new QMS055
                            {
                                LTFPSHeaderId = LineId,
                                QualityProject = objLTF002.QualityProject,
                                Project = objLTF002.Project,
                                BU = objLTF002.BU,
                                Location = objLTF002.Location,
                                PartNo = objLTF002.PartNo,
                                ParentPartNo = result.ParentPart,
                                ChildPartNo = result.Part,
                                StageCode = objLTF002.Stage,
                                StageSequence = objLTF002.StageSequence,
                                IterationNo = Iteration,
                                RequestNo = RequestNo,
                                RequestNoSequence = ReqSeqNo,
                                OfferedQuantity = OfferedQuantity,
                                ShopLocation = ShopLocation,
                                ShopRemark = ShopRemark,
                                Shop = Shop,
                                OfferedBy = objClsLoginInfo.UserName,
                                OfferedOn = DateTime.Now,
                                CreatedBy = objClsLoginInfo.UserName,
                                CreatedOn = DateTime.Now,
                                refHeaderId = null,
                                TPIAgency1 = objQMS036 != null ? objQMS036.FirstTPIAgency : null,
                                TPIAgency1Intervention = objQMS036 != null ? objQMS036.FirstTPIIntervention : null,
                                TPIAgency2 = objQMS036 != null ? objQMS036.SecondTPIAgency : null,
                                TPIAgency2Intervention = objQMS036 != null ? objQMS036.SecondTPIIntervention : null,
                                TPIAgency3 = objQMS036 != null ? objQMS036.ThirdTPIAgency : null,
                                TPIAgency3Intervention = objQMS036 != null ? objQMS036.ThirdTPIIntervention : null,
                                TPIAgency4 = objQMS036 != null ? objQMS036.FourthTPIAgency : null,
                                TPIAgency4Intervention = objQMS036 != null ? objQMS036.FourthTPIIntervention : null,
                                TPIAgency5 = objQMS036 != null ? objQMS036.FifthTPIAgency : null,
                                TPIAgency5Intervention = objQMS036 != null ? objQMS036.FifthTPIIntervention : null,
                                TPIAgency6 = objQMS036 != null ? objQMS036.SixthTPIAgency : null,
                                TPIAgency6Intervention = objQMS036 != null ? objQMS036.SixthTPIIntervention : null,
                            });

                            objLTF002.EditedBy = objClsLoginInfo.UserName;
                            objLTF002.EditedOn = DateTime.Now;
                            objLTF002.RequestNo = RequestNo;
                            objLTF002.IterationNo = Iteration;
                            db.SaveChanges();

                            objLTF002.InspectionStatus = clsImplementationEnum.PartlistOfferInspectionStatus.UNDER_INSPECTION.GetStringValue();

                            db.SaveChanges();

                            #region Send Notification
                            (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), objLTF002.Project, objLTF002.BU, objLTF002.Location, "Stage: " + objLTF002.Stage + " of Part: " + objLTF002.PartNo + " & Project No: " + objLTF002.QualityProject + " has been offered for Inspection", clsImplementationEnum.NotificationType.Information.GetStringValue());
                            #endregion

                            #endregion
                        }

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Part offered Successfully";

                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Part not available.";
                    }
                    #endregion
                }
                else
                {
                    #region Seam Offer

                    var objLTF002 = db.LTF002.Where(x => x.LineId == LineId).FirstOrDefault();
                    QMS002 objQMS002 = db.QMS002.Where(x => x.StageDepartment.ToUpper() == "NDE" && x.StageCode == objLTF002.Stage && x.BU == objLTF002.BU && x.Location == objLTF002.Location).FirstOrDefault();

                    string response = db.SP_LTFPS_GENREATESEAMTESTREQUEST(LineId, objClsLoginInfo.UserName, Shop, ShopLocation, ShopRemark, surfaceCondition, weldThicknessReinforcement).FirstOrDefault();
                    if (response == "false")
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Data Not Available";
                        return Json(objResponseMsg);
                    }
                    #region Send Notification
                    if (objQMS002 != null)
                    {
                        //QMS060 objQMS060 = db.QMS060.Where(x=>x.re);
                        //if (objQMS060 != null)
                        //{
                        //    (new clsManager()).SendNotification("NDT3", iter.Project, iter.BU, iter.Location, "Stage: " + iter.StageCode + " of Seam: " + iter.SeamNo + " & Project No: " + iter.QualityProject + " has been offered for Inspection", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/SeamListNDEInspection/QCSeamTestDetails/" + objQMS060.RequestId);
                        //}
                    }
                    else
                    {
                        //QMS050 objQMS050 = iter.QMS050.OrderByDescending(o => o.CreatedOn).FirstOrDefault();
                        //if (objQMS050 != null)
                        //{
                        //    (new clsManager()).SendNotification("QC3", iter.Project, iter.BU, iter.Location, "Stage: " + iter.StageCode + " of Seam: " + iter.SeamNo + " & Project No: " + iter.QualityProject + " has been offered for Inspection", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/MaintainSeamListOfferInspection/ApproveSeamListInspection/" + objQMS050.RequestId);
                        //}
                    }
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = response;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }

            return Json(objResponseMsg);
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_FETCH_LTFPSOFFERINSPECTION_HEADERS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      li.QualityProject,
                                      li.BU,
                                      li.LTFPSNo,
                                      li.LTFPSRevNo,
                                      li.OperationNo,
                                      li.SeamNo,
                                      li.PartNo,
                                      li.TestType,
                                      li.Stage,
                                      li.InspectionStatus,
                                      li.Location,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else
                {
                    var lst = db.SP_FETCH_LTFPS_RE_OFFERINSPECTION_HEADERS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      li.QualityProject,
                                      li.BU,
                                      li.LTFPSNo,
                                      li.OperationNo,
                                      li.SeamNo,
                                      li.PartNo,
                                      li.StageCode,
                                      li.IterationNo,
                                      li.RequestNo,
                                      li.RequestNoSequence,
                                      li.TestResult,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);

                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult LoadLTFPSFilteredDataPartial(FilterParaModel filterparamodel, string isFromSeam = "")
        {
            ViewBag.isFromSeam = isFromSeam;
            return PartialView("_LoadLTFPSFilteredDataPartial", filterparamodel);
        }

        [HttpPost]
        public JsonResult LoadFilteredLTFPSData(JQueryDataTableParamModel param, string qualityProject, string project, string bu, string location, string partNo, string LTFPSNo)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;

                strWhere = "1=1";
                strWhere += string.Format(" and ltf002.QualityProject in('{0}') and ltf002.Project in('{1}') and ltf002.BU in('{2}') and ltf002.Location in('{3}') and (ltf002.SeamNo in('{4}') or ltf002.PartNo in('{4}')) and ltf002.LTFPSNo in('{5}')", qualityProject, project, bu, location, partNo, LTFPSNo);

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "OperationNo",
                                                "Stage",
                                                "InspectionStatus",
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                // strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms045.BU", "qms045.Location");
                var lstResult = db.SP_FETCH_LTFPSOFFERINSPECTION_HEADERS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.Stage),
                               Convert.ToString(uc.OperationNo),
                               Convert.ToString(uc.InspectionStatus),
                               Convert.ToString(uc.LNTStatus),
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.isNDEStage),
                               Convert.ToString(uc.Stage),
                               Convert.ToString(uc.OperationNo)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = 0,
                    iTotalDisplayRecords = 0,
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult LoadLTFPSReqFilteredDataPartial(RequestFilterParaModel requestfilterparamodel)
        {
            if (!string.IsNullOrWhiteSpace(requestfilterparamodel.StageCode))
            {
                requestfilterparamodel.StageCode = requestfilterparamodel.StageCode.Split('-')[0].Trim();
                string StageDesc = db.QMS002.Where(x => x.StageCode == requestfilterparamodel.StageCode).Select(x => x.StageDesc).FirstOrDefault();
                string strStageDesc = requestfilterparamodel.StageCode;
                if (!string.IsNullOrWhiteSpace(StageDesc))
                    strStageDesc += " - " + StageDesc;
                requestfilterparamodel.StageDesc = strStageDesc;
            }
            if (requestfilterparamodel.isNDEStage.ToLower() == "true")
            {
                return PartialView("_LoadNDEFilteredDataPartial", requestfilterparamodel);
            }
            else
            {
                return PartialView("_LoadNonNDEFilteredDataPartial", requestfilterparamodel);
            }
        }

        [HttpPost]
        public JsonResult LoadFilteredReqData(JQueryDataTableParamModel param, string qualityProject, string project, string bu, string location, string partNo, string stageCode, int stageSeq)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[0]) + "," + Convert.ToString(Request["sColumns"].Split(',')[1]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                strWhere = "1=1";
                strWhere += string.Format(" and tm.QualityProject in('{0}') and tm.Project in('{1}') and tm.BU in('{2}') and tm.Location in('{3}') and (tm.PartNo in('{4}') or tm.SeamNo in('{4}')) and (tm.StageCode in('{5}') or tm.StageCode is null) ", qualityProject, project, bu, location, partNo, stageCode);



                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "QualityProject",
                                                "TestResult",
                                                "Shop",
                                                "ShopLocation",
                                                "ShopRemark"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_FETCH_LTFPS_RE_OFFERINSPECTION_HEADERS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.RequestNo),
                               Convert.ToString(uc.RequestNoSequence),
                               Convert.ToString(uc.IterationNo),
                               Convert.ToString(uc.TestResult),
                               Convert.ToString(uc.Shop),
                               Convert.ToString(uc.ShopLocation),
                               Convert.ToString(uc.ShopRemark),
                               Convert.ToString(uc.OfferedBy),
                               Convert.ToString(uc.OfferedOn),
                               Convert.ToString(uc.OfferedtoCustomerBy),
                               Convert.ToString(uc.TPIAgency1Result),
                               Convert.ToString(uc.TPIAgency2Result),
                               Convert.ToString(uc.TPIAgency3Result),
                               Convert.ToString(uc.TPIAgency4Result),
                               Convert.ToString(uc.TPIAgency5Result),
                               Convert.ToString(uc.TPIAgency6Result),
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = 0,
                    iTotalDisplayRecords = 0,
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadFilteredLTFPSNDEReqData(JQueryDataTableParamModel param, string qualityProject, string project, string bu, string location, string partNo, string stageCode, int stageSeq)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[0]) + "," + Convert.ToString(Request["sColumns"].Split(',')[1]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                strWhere = "1=1";
                strWhere += string.Format(" and tm.QualityProject in('{0}') and tm.Project in('{1}') and tm.BU in('{2}') and tm.Location in('{3}') and (tm.PartAssemblyFindNumber in('{4}') or tm.SeamNo in('{4}')) and tm.StageCode in('{5}') ", qualityProject, project, bu, location, partNo, stageCode);


                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "tm.Project+' - '+com1.t_dsca",
                                                "QualityProject",
                                                "Shop",
                                                "ShopLocation",
                                                "ShopRemark"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_LTFPS_FETCH_REQUESTCONFIRMATION_HEADERS
                                (
                                StartIndex, EndIndex, "", strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.RequestNo),
                               Convert.ToString(uc.RequestNoSequence),
                               Convert.ToString(uc.IterationNo),
                               Convert.ToString(uc.RequestStatus),
                               Convert.ToString(uc.TestResult),
                               Convert.ToString(uc.ManufacturingCode),
                               Convert.ToString(uc.Shop),
                               Convert.ToString(uc.ShopLocation),
                               Convert.ToString(uc.ShopRemark),
                               Convert.ToString(uc.NDETechniqueNo),
                               Convert.ToString(uc.NDETechniqueRevisionNo),
                               Convert.ToString(uc.OfferedQuantity),
                               Convert.ToString(uc.ProductionDrawingNo),
                               Convert.ToString(uc.ReturnRemark),
                               Convert.ToString(uc.TPIAgency1),
                               Convert.ToString(uc.TPIAgency1Intervention),
                               Convert.ToString(uc.TPIAgency2),
                               Convert.ToString(uc.TPIAgency2Intervention),
                               Convert.ToString(uc.TPIAgency3),
                               Convert.ToString(uc.TPIAgency3Intervention),
                               Convert.ToString(uc.TPIAgency4),
                               Convert.ToString(uc.TPIAgency4Intervention),
                               Convert.ToString(uc.TPIAgency5),
                               Convert.ToString(uc.TPIAgency5Intervention),
                               Convert.ToString(uc.TPIAgency6),
                               Convert.ToString(uc.TPIAgency6Intervention),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn),
                               Convert.ToString(uc.RequestGeneratedBy),
                               Convert.ToString(uc.RequestGeneratedOn),
                               Convert.ToString(uc.ConfirmedBy),
                               Convert.ToString(uc.ConfirmedOn),
                               Convert.ToString(uc.OfferedBy),
                               Convert.ToString(uc.OfferedOn),
                               Convert.ToString(uc.InspectedBy),
                               Convert.ToString(uc.InspectedOn),
                               Convert.ToString(uc.ReturnedBy),
                               Convert.ToString(uc.ReturnedOn),
                               Convert.ToString(uc.RejectedBy),
                               Convert.ToString(uc.RejectedOn)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = 0,
                    iTotalDisplayRecords = 0,
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult CheckSeamPartOffered(int LineID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            clsHelper.ResponseMsg objResponseICLMsg = new clsHelper.ResponseMsg();
            try
            {

                (new MaintainSeamListOfferInspectionController()).CopyWelderWeldLengthFromPreviousNDEStage(LineID, true);


                LTF002 objLTF002 = db.LTF002.Where(x => x.LineId == LineID).FirstOrDefault();
                objResponseMsg.Key = true;
                if (objLTF002 != null)
                {
                    var InspectionStatus = clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue();
                    if (objLTF002.TestFor.Equals("Seam", StringComparison.OrdinalIgnoreCase))
                    {
                        var lstOperations = db.LTF002.Where(x => x.HeaderId == objLTF002.HeaderId && x.LineId != objLTF002.LineId && x.TestFor.Equals(objLTF002.TestFor) && x.SeamNo.Equals(objLTF002.SeamNo) && x.OperationNo < objLTF002.OperationNo && (!x.InspectionStatus.Equals(InspectionStatus) && x.TPI.ToUpper() != "X" && x.Agency.ToUpper() != "X")).ToList();
                        if (lstOperations.Any())
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Please release operation " + lstOperations[0].OperationNo + " of LTFPS " + lstOperations[0].LTFPSNo;
                        }
                        objResponseICLMsg = CheckPreviousStageSeqOffered(objLTF002.QualityProject, objLTF002.BU, objLTF002.Location, objLTF002.SeamNo, objLTF002.Stage, objLTF002.StageSequence, objLTF002.LTFPSNo);
                        if (!objResponseICLMsg.Key)
                        {
                            objResponseMsg.Key = objResponseICLMsg.Key;
                            objResponseMsg.Value = objResponseICLMsg.Value;
                        }

                    }
                    else if (objLTF002.TestFor.Equals("Part", StringComparison.OrdinalIgnoreCase) || objLTF002.TestFor.Equals("Assembly", StringComparison.OrdinalIgnoreCase))
                    {
                        var lstOperations = db.LTF002.Where(x => x.HeaderId == objLTF002.HeaderId && x.LineId != objLTF002.LineId && (objLTF002.TestFor.Equals("Part", StringComparison.OrdinalIgnoreCase) || objLTF002.TestFor.Equals("Assembly", StringComparison.OrdinalIgnoreCase)) && x.PartNo.Equals(objLTF002.PartNo) && x.OperationNo <= objLTF002.OperationNo && (!x.InspectionStatus.Equals(InspectionStatus) && x.TPI.ToUpper() != "X" && x.Agency.ToUpper() != "X")).ToList();
                        if (lstOperations.Any())
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Please release operation " + lstOperations[0].OperationNo + " of LTFPS " + lstOperations[0].LTFPSNo;
                        }
                    }

                    #region Check For Maintain Weld Length
                    if (objResponseMsg.Key && Manager.IsWelderRequiredInNDEStageOffer(objLTF002.BU, objLTF002.Location, objLTF002.Stage) && objLTF002.TestFor.Equals("Seam", StringComparison.OrdinalIgnoreCase) && (!Manager.IsNDERequestConfirmationRequiredOnFirstInteration(objLTF002.Project, objLTF002.BU, objLTF002.Location) && Manager.IsNDERequest(objLTF002.Stage, objLTF002.BU, objLTF002.Location)))
                    {

                        List<QMS061> lstQMS061 = db.QMS061.Where(x => x.LTFPSHeaderId == objLTF002.LineId).ToList();
                        bool isOverlay = Manager.IsOverlaySeam(objLTF002.QualityProject, objLTF002.SeamNo);
                        string message = string.Empty;

                        //decimal decValue = 0;
                        //QMS012 objQMS012 = db.QMS012.FirstOrDefault(c => c.QualityProject == objLTF002.QualityProject && c.SeamNo == objLTF002.SeamNo);
                        //if (objQMS012.SeamLengthArea != null)
                        //    decValue = Convert.ToDecimal(objQMS012.SeamLengthArea);

                        decimal? sum = 0;
                        if (isOverlay)
                        {
                            sum = lstQMS061.Sum(x => x.Weldarea);
                            if (sum <= 0)
                            {
                                message = "Total weld area should be greater than 0.";
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = message;
                            }
                            else
                            {
                                objResponseMsg.Key = true;
                                objResponseMsg.Value = message;
                            }
                        }
                        else
                        {
                            sum = lstQMS061.Sum(x => x.Weldlength);
                            if (sum <= 0)
                            {
                                message = "Total weld length should be greater than 0.";
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = message;
                            }
                            else
                            {
                                objResponseMsg.Key = true;
                                objResponseMsg.Value = message;
                            }
                        }
                    }
                    #endregion

                    #region Check for additional welder exists in token/parameter slip 

                    if (objResponseMsg.Key)
                    {
                        List<QMS061> lstQMS061 = db.QMS061.Where(x => x.LTFPSHeaderId == objLTF002.LineId).ToList();
                        if (lstQMS061.Count > 0)
                        {
                            List<WPS025> lstWPS025 = db.WPS025.Where(x => x.QualityProject == objLTF002.QualityProject && x.Project == objLTF002.Project && x.SeamNo == objLTF002.SeamNo).ToList();

                            List<string> lstProcess = new List<string>();
                            foreach (var weldingProcess in lstQMS061.Select(s => s.WeldingProcess).Distinct().ToList())
                            {
                                foreach (var wp in weldingProcess.Split('+'))
                                {
                                    if (!string.IsNullOrWhiteSpace(wp))
                                    {
                                        lstProcess.Add(wp);
                                    }
                                }
                            }

                            if (lstQMS061.Select(s => s.welderstamp).Distinct().Count() != lstWPS025.Select(s => s.WelderStamp).Distinct().Count() || lstProcess.Distinct().Count() != lstWPS025.Select(s => s.WeldingProcess).Distinct().Count())
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = "Please Maintain Weld Length";
                            }
                        }
                    }

                    #endregion
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please Try Again";
                }

                return Json(objResponseMsg);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error Occures Please try again";
                return Json(objResponseMsg);
            }
        }

        [HttpPost]
        public clsHelper.ResponseMsg CheckPreviousStageSeqOffered(string qproject, string bu, string loc, string seamno, string stage, int? seq, string ltfsno)
        {
            clsHelper.ResponseMsg obj = new clsHelper.ResponseMsg();
            obj.Key = true;
            List<string> lstNotCleared = new List<string>();
            try
            {
                var InspectionStatus = clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue();
                var lstQMS031 = db.QMS031.Where(x => x.QualityProject == qproject && x.BU == bu && x.Location == loc && x.SeamNo == seamno && x.StageSequance < seq).ToList();
                foreach (var item in lstQMS031)
                {
                    var lstOperations = db.LTF002.Where(x => x.TestFor.Equals("Seam") && x.QualityProject == qproject && x.BU == bu && x.Location == loc &&
                                                             x.SeamNo.Equals(seamno) && x.Stage == item.StageCode && x.StageSequence == item.StageSequance &&
                                                             (!x.InspectionStatus.Equals(InspectionStatus) && x.TPI.ToUpper() != "X" && x.Agency.ToUpper() != "X")).ToList();
                    if (lstOperations.Any())
                    {
                        lstNotCleared.Add(item.StageCode + " of Sequence " + item.StageSequance);
                    }
                }

                if (lstNotCleared.Count > 0)
                {
                    obj.Key = false;
                    obj.Value = "You cannot offer this stage, because Stages : " + string.Join(" , ", lstNotCleared.Distinct()) + " not cleared yet.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

            }
            return obj;
        }
    }

}