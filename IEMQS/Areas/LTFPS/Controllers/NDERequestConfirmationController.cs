﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.LTFPS.Controllers
{
    public class NDERequestConfirmationController : clsBase
    {
        [SessionExpireFilter]
        public ActionResult Index(string tab="")
        {
            ViewBag.Tab = tab;
            return View();
        }

        public ActionResult GetRequestConfirmDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetRequestConfirmDataPartial");
        }

        public ActionResult LoadRequestConfirmationDataTable(JQueryDataTableParamModel param, string status)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1 ";
                string _urlFrom = "all";
                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and tm.RequestNo IS NOT NULL and tm.RequestGeneratedBy IS NOT NULL and tm.ConfirmedBy IS NULL and (tm.RequestStatus='Not Attended' or tm.RequestStatus='' or tm.RequestStatus is null) ";
                    _urlFrom = "confirmation";
                }
                else if (status.ToUpper() == "REGENERATE")
                {
                    whereCondition += " and dbo.fun_LTFPS_SPOTCLEAR_BY_REQUESTNO(tm.LTFPSHeaderId) = 0 and ISNULL(tm.IsPartiallyOffered, 0) = 0 and tm.RequestStatus = '" + clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue() + "' ";
                    _urlFrom = "regenerate";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = {
                                                "QualityProject",
                                                "dbo.GET_PROJECTDESC_BY_CODE(tm.Project)",
                                                "SeamNo",
                                                "PartAssemblyFindNumber",
                                                "PartAssemblyChildItemCode",
                                                "StageCode",
                                                "RequestStatus",
                                                "Jointtype",
                                                "Shop",
                                                "ShopLocation",
                                                "LTFPSNo",
                                                "OperationNo"
                                           };
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = string.Empty; ;
                sortDirection = Convert.ToString(Request["sSortDir_0"]); // asc or desc
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstHeader = db.SP_LTFPS_FETCH_REQUESTCONFIRMATION_HEADERS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                var res = from h in lstHeader
                          select new[] {
                           Convert.ToString(h.QualityProject),
                           Convert.ToString(h.Project),
                           Convert.ToString(h.LTFPSNo),
                           Convert.ToString(h.OperationNo),
                           Convert.ToString(h.SeamNo),
                           Convert.ToString(h.PartAssemblyFindNumber),//Position No.
                           Convert.ToString(h.PartAssemblyChildItemCode),//Part No.
                           Convert.ToString(h.OfferedQuantity),
                           Convert.ToString(h.StageCode),
                           Convert.ToString(h.StageSequence),
                           Convert.ToString(h.IterationNo),
                           Convert.ToString(h.RequestNo),
                           Convert.ToString(h.RequestNoSequence),
                           Convert.ToString(h.RequestStatus),
                           Convert.ToString(h.SeamLength),
                           Convert.ToString(h.OverlayArea),
                           Convert.ToString(h.OverlayThickness),
                           Convert.ToString(h.Jointtype),
                           Convert.ToString(h.ManufacturingCode),
                           Convert.ToString(h.Part1Position),
                           Convert.ToString(h.Part1thickness),
                           Convert.ToString(h.Part1Material),
                           Convert.ToString(h.Part2Position),
                           Convert.ToString(h.Part2thickness),
                           Convert.ToString(h.Part2Material),
                           Convert.ToString(h.Part3Position),
                           Convert.ToString(h.Part3thickness),
                           Convert.ToString(h.Part3Material),
                           Convert.ToString(h.AcceptanceStandard),
                           Convert.ToString(h.ApplicableSpecification),
                           Convert.ToString(h.InspectionExtent),
                           Convert.ToString(h.WeldingEngineeringRemarks),
                           Convert.ToString(h.WeldingProcess),
                           Convert.ToString(h.SeamStatus),
                           Convert.ToString(h.Shop),
                           Convert.ToString(h.ShopLocation),
                           Convert.ToString(h.ShopRemark),
                           Convert.ToString(h.SurfaceCondition),
                           Convert.ToString(h.WeldThicknessReinforcement),
                           Convert.ToString(h.SpotGenerationtype),
                           Convert.ToString(h.SpotLength),
                           Convert.ToString(h.PrintSummary),
                           Convert.ToString(h.NDETechniqueNo),
                           Convert.ToString(h.NDETechniqueRevisionNo),
                          GetActionLink(h.Tablename,h.RequestId,_urlFrom)+"&nbsp"+(string.IsNullOrWhiteSpace(h.ConfirmedBy)?"<i style=\"cursor:pointer;margin-left:10px;\" title=\"Confirm Request\" onclick=\"ConfirmRequest("+h.RequestId+",'"+h.Tablename+"')\" class=\"fa fa-check-square-o\" aria-hidden=\"true\"></i>":"<i style=\"cursor:pointer;margin-left:10px;opacity: 0.3;\" title=\"Confirm Request\"  class=\"fa fa-check-square-o\" aria-hidden=\"true\"></i>")+ Manager.generateLTFPSGridButtons(h.LTFPSNo,h.QualityProject,h.Project,h.BU,h.Location,!string.IsNullOrWhiteSpace(h.SeamNo)?h.SeamNo:h.PartAssemblyFindNumber,h.StageCode, Convert.ToString(h.OperationNo),!string.IsNullOrWhiteSpace(h.SeamNo)?"Seam Details":"Part Details",Convert.ToString(true),!string.IsNullOrWhiteSpace(h.SeamNo)?"true":"false",!string.IsNullOrWhiteSpace(h.SeamNo)?"Seam Details":"Part Details",!string.IsNullOrWhiteSpace(h.SeamNo)?"Seam Request Details":"Part Request Details")

                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string GetActionLink(string FromTable, int RequestId, string from = "")
        {
            string strAction = string.Empty;

            if (FromTable.ToUpper() == "PART" || FromTable.ToUpper() == "ASSEMBLY")
            {
                strAction += "<center style=\"white-space: nowrap;\" class=\"clsDisplayInlineEle\"><a title=\"View Details\" class='' href='"+ WebsiteURL + "/IPI/PartlistNDEInspection/ShopOfferDetails?RequestId=" + Convert.ToInt32(RequestId) + "&from="+from+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a></center>";
            }
            else
            {
                strAction += "<center style=\"white-space: nowrap;\" class=\"clsDisplayInlineEle\"><a   href=\"" + WebsiteURL + "/IPI/SeamListNDEInspection/ShopSeamTestDetails/" + RequestId + "/?from=" + from + "\" Title=\"View Details\" ><i style=\"margin - left:5px;\" class=\"fa fa-eye\"></i></a></center>";
            }

            return strAction;
        }

    }
}