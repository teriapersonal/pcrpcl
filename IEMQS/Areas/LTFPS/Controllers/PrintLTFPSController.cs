﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using Microsoft.Office.Interop.Excel;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace IEMQS.Areas.LTFPS.Controllers
{
    public class PrintLTFPSController : clsBase
    {
        // GET: LTFPS/PrintLTFPS
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult GetQualityProjectList(string term)
        {
            List<ddlValue> objList = null;
            var lstlocation = Manager.GetUserwiseLocation(objClsLoginInfo.UserName);
            if (!string.IsNullOrWhiteSpace(term))
            {
                objList = (from a in db.LTF001
                           where lstlocation.Contains(a.Location) && a.QualityProject.Trim().ToLower().Contains(term.Trim().ToLower())
                           select new ddlValue { Text = a.QualityProject + " (" + a.Location.Trim() + ")", Value = a.QualityProject, id = a.QualityProject + "#" + a.Location.Trim() }).Distinct().ToList();
            }
            else
            {
                objList = (from a in db.LTF001
                           where lstlocation.Contains(a.Location)
                           select new ddlValue { Text = a.QualityProject + " (" + a.Location.Trim() + ")", Value = a.QualityProject, id = a.QualityProject + "#" + a.Location.Trim() }).Distinct().Take(10).ToList();
            }
            return Json(objList, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetLTFPSNoList(string qualityProject, string term)
        {
            List<ddlValue> objList = null;
            var lstlocation = Manager.GetUserwiseLocation(objClsLoginInfo.UserName);
            if (!string.IsNullOrWhiteSpace(qualityProject))
            {
                if (!string.IsNullOrWhiteSpace(term))
                {
                    objList = (from a in db.LTF001
                               where a.QualityProject.Trim().ToLower() == qualityProject.Trim().ToLower()
                               && a.LTFPSNo.ToLower().Contains(term.ToLower())
                               select new ddlValue { Text = a.LTFPSNo, Value = a.LTFPSNo, id = a.LTFPSNo }).Distinct().ToList();
                }
                else
                {
                    objList = (from a in db.LTF001
                               where a.QualityProject.Trim().ToLower() == qualityProject.Trim().ToLower()
                               select new ddlValue { Text = a.LTFPSNo, Value = a.LTFPSNo, id = a.LTFPSNo }).Distinct().ToList();
                }
            }

            return Json(objList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadLTFPSGridDataPartial(string QualityProject, string LTFPSNo)
        {
            ViewBag.QualityProject = QualityProject;
            ViewBag.LTFPSNo = LTFPSNo;
            return PartialView("_LoadLTFPSGridDataPartial");
        }
        public ActionResult LoadLTFPSListGridData(JQueryDataTableParamModel param, string LTFPSNo)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                // string _urlFrom = "all";

                string strWhere = "1=1 ";
                if (param.Project != null)
                {
                    strWhere += "and (tm.QualityProject='" + param.Project + "')";
                }
                if (LTFPSNo != null)
                {
                    strWhere += "and (tm.LTFPSNo='" + LTFPSNo + "')";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "tm.OperationNo",
                                                "tm.OperationDescription",
                                                "tm.TestResult",
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                // strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "tm.BU", "tm.Location");
                var lstResult = db.SP_LTFPS_GET_LTFPS_DATA
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();
                //string folderpath = "QMS050_LTFPS/";
                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.OperationNo),
                               Convert.ToString(uc.OperationDescription),
                               Convert.ToString(uc.TestResult),
                               //Convert.ToString(uc.LTFPSHeaderId),
                               //Convert.ToString(uc.IterationNo),
                               //Convert.ToString(uc.RequestNoSequence),                               
                               ("&nbsp;&nbsp;<a target=\"_blank\" onclick=\"showOtherFiles("+ uc.LTFPSHeaderId +","+ uc.IterationNo + ","+uc.RequestNoSequence+")\" Title=\"View LTFPS Details\" ><i style=\"margin - left:5px;\" class=\"fa fa-eye\"></i></a>")
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string GenerateExcelSeamList<T>(List<T> query, string userName, string QualityProject, string LTFPSNo)
        {
            //try
            {
                using (ExcelPackage excelPackage = new ExcelPackage())
                {
                    ExcelWorksheet ws = excelPackage.Workbook.Worksheets.Add("Sheet1");
                    //Added by Dharmesh Vasani for Change Excel Column Title
                    //get our column headings
                    var t = typeof(T);
                    var Headings = t.GetProperties();
                    string xmlData = HttpContext.Server.MapPath("~/ExcelSetting.xml");
                    DataSet ds = new DataSet();//Using dataset to read xml file  
                    ds.ReadXml(xmlData);
                    var DataColumns = new List<ExcelDateCoumn>();
                    DataColumns = (from rows in ds.Tables[0].AsEnumerable()
                                   select new ExcelDateCoumn
                                   {
                                       Name = (rows[0].ToString()).Trim()
                                   }).ToList();

                    List<string> lstDateCoumnName = DataColumns.Select(x => x.Name.ToUpper()).ToList();
                    //Workbook workbook = new Workbook();
                    //Worksheet worksheet = workbook.Worksheets.Add();                  
                    //CellIndex A1Cell = new CellIndex(0, 0);
                    //worksheet.Cells[A1Cell].Merge();
                    int j = 0, k = 0;
                    ws.Cells[1, j + 1].Value = "Quality Project : " + QualityProject;
                    ws.Cells["A1:C1"].Merge = true;
                    ws.Cells["A1:C1"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    ws.Cells["A1:C1"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    ws.Cells["A1:C1"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    ws.Cells["A1:C1"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    ws.Cells[2, k + 1].Value = "LTFPS no : " + LTFPSNo;
                    ws.Cells["A2:C2"].Merge = true;
                    ws.Cells["A2:C2"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    ws.Cells["A2:C2"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    ws.Cells["A2:C2"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    ws.Cells["A2:C2"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    // ws.Cells["A2:C2"].Style.Border.Right.Style = ExcelBorderStyle.Thick;
                    for (int i = 0; i < Headings.Count(); i++)
                    {
                        ws.Cells[3, i + 1].Value = Headings[i].Name;

                    }

                    //populate our Data
                    if (query.Count() > 0)
                    {
                        ws.Cells["A4"].LoadFromCollection(query);

                    }

                    //Format the header                    
                    using (ExcelRange rng = ws.Cells[3, 1, 3, Headings.Count()])
                    {
                        rng.Style.Font.Bold = true;
                        rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid; //Set Pattern for the background to Solid
                        rng.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(79, 129, 189)); //Set color to dark blue
                        rng.Style.Font.Color.SetColor(System.Drawing.Color.White);
                        rng.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    }

                    //End
                    string fileName;
                    string excelFilePath = Helper.ExcelFilePath(userName, out fileName);

                    ws.Cells.AutoFitColumns();

                    Byte[] bin = excelPackage.GetAsByteArray();
                    System.IO.File.WriteAllBytes(excelFilePath, bin);

                    return fileName;
                }
            }
        }


        public ActionResult GenerateExcelLTFPSList(string strSortOrder, string QualityProject, string LTFPSNo, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                string strWhere = "1=1 ";
                if (QualityProject != null)
                {
                    strWhere += "and (tm.QualityProject='" + QualityProject + "')";
                }
                if (LTFPSNo != null)
                {
                    strWhere += "and (tm.LTFPSNo='" + LTFPSNo + "')";
                }

                //   var lst = db.SP_IPI_GET_SECTION_WISE_SEAM_LIST_DATA(Project, objClsLoginInfo.Location, 1, int.MaxValue, strSortOrder, whereCondition).ToList();
                var lst = db.SP_LTFPS_GET_LTFPS_DATA
                                 (
                                 1, int.MaxValue, strSortOrder, strWhere
                                 ).ToList();





                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No data available";
                }

                var newlst = (from li in lst
                              select new
                              {
                                  OPNNo = li.OperationNo,
                                  OperationDescription = li.OperationDescription,
                                  TestResult = li.TestResult,
                              }).ToList();

                strFileName = GenerateExcelSeamList(newlst, objClsLoginInfo.UserName, QualityProject, LTFPSNo);

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
    }
}