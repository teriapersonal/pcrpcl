﻿using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQS.Areas.NDE.Models;
using System.Data.Entity;
using IEMQS.PLMBOMService;
using System.Data.Entity.Validation;
using static IEMQSImplementation.clsImplementationEnum;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using IEMQS.FKMSAllocationService;
using System.Threading.Tasks;
using IEMQS.Areas.Utility.Controllers;
using System.Globalization;
using System.Xml.Linq;
using System.Text;
using System.Data.Entity.Infrastructure;
using System.Text.RegularExpressions;

namespace IEMQS.Areas.FKM.Controllers
{
    public class MaintainFKMSController : clsBase
    {
        string IsShowFixture = ConfigurationManager.AppSettings["IsShowFRModule"] != null ? ConfigurationManager.AppSettings["IsShowFRModule"].ToString() : "1";
        GeneralController generalMethod = new GeneralController();
        //List<SP_FKMS_GET_PARENTNODE_BY_PROJECT_Result> parentnode_by_pr = null;
        List<SP_FKMS_HBOM_FOR_PROJECT_Result> projectresult = null;
        List<FKM202> listFKM202 = null;
        List<TreeviewEnt> listTreeviewEnt = null;
        List<FKM202> listDeleteTree = null;
        //List<FKM108> lstAllocation = null;
        //List<FKM104> listFKM104 = null;
        List<ParentChildNodeEnt> objParentsclrFKM202 = null;
        List<FKM104> ComparelistFKM104 = null;
        List<FKM104> lstFKM104 = null;
        List<SP_FKMS_HBOM_FOR_PROJECT_Result> lstgetbom = null;
        List<SP_FKMS_HBOM_FOR_PROJECT_Result> lstgetAllbom = null;
        //List<FKM104> Compare2listFKM104 = null;
        List<TreeviewEnt> ComparelistTreeviewEnt = null;
        string PartIcon = "fa fa-cogs";
        string _defaultPolicy = "Part-CRR";
        string _defaultItemGroup = "MMFG00";
        string _defaultPartType = "Part";
        string _defaultParentPartType = "Part";
        string _defaultChildPartType = "Part";
        string _defaultPartAction = "ADD";
        string _defaultIsTopPart = "AutoRevise";
        string _defaultpartQuantity = "1";
        string _defaultLength = "";
        string _defaultWidth = "";
        string _defaultNumberOfPieces = "1";
        string allocateInsert = "Insert";
        string allocateDelete = "Delete";
        string JIGFIXElementNo = "J9999";
        string clspsno = "";
        HedPLMBaaNProjectCreationWebServiceService plmWebService = new HedPLMBaaNProjectCreationWebServiceService();
        int Decrement100Cnt = 0;
        int StartFindNo = 9901;
        List<MergeNodeEnt> listMergeNodeEnt = null;
        string copyDestiProject = "";
        string ASM = NodeTypes.ASM.GetStringValue().ToLower();
        string TJF = NodeTypes.TJF.GetStringValue().ToLower();
        string PLT = NodeTypes.PLT.GetStringValue().ToLower();
        bool IsAnyChangesForCompareBOM = false;

        IEMQSEntitiesContext context = null;

        #region H-BOM Template
        public ActionResult GetSaveHBOMTemplate(int id,string TemplateType = "B")
        {
            ViewBag.lstProduct = db.PBP001.Select(s => new SelectListItem() { Value = s.ProductCode, Text = s.Product }).ToList();
            ViewBag.UserLocationCode = objClsLoginInfo.Location;
            ViewBag.UserLocation = objClsLoginInfo.LocationName;
            ViewBag.TemplateType = TemplateType;
            return View("_GetSaveHBOMTemplate");
        }
        public ActionResult LoadHBOMTemplateData(JQueryDataTableParamModel param, string TemplateType)
        {
            try
            {
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Request["sSortDir_0"];

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                var IsDisplayOnly = !string.IsNullOrEmpty(Request["IsDisplayOnly"]) ? Convert.ToBoolean(Request["IsDisplayOnly"].ToString()) : false;
                string whereCondition = "TemplateType= '"+ TemplateType +"'";

                string indextype = param.Department;
                if (string.IsNullOrWhiteSpace(indextype))
                {
                    indextype = clsImplementationEnum.WPPIndexType.maintain.GetStringValue();
                }

                string[] columnName = { "TemplateName", "Product", "ProcessLicensor", "Location+ '-' + bcom2.t_desc", "CreatedBy" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstPam = db.SP_FKM_GET_HBOM_TEMPLATE(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                int totalRecords = lstPam.Count();// lstPam.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from h in lstPam
                           select new[] {
                               Convert.ToString(h.Id),
                               h.Location,
                               h.Product,
                               h.ProcessLicensor,
                               h.TemplateName,
                               //h.CreatedBy,
                               ""//"<center>"+ Helper.GenerateActionIcon(h.Id, "View", "Override", "fa fa-eye", "","",!IsDisplayOnly )  +"</center>"
                    }).ToList();
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords,
                    iTotalRecords = totalRecords,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = new string[] {}
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult GetAllProcessLicensor(string term)
        {
            List<SelectListItem> lstProcessLicensor = new List<SelectListItem>();
            lstProcessLicensor = (from p3 in db.PBP003
                                    where term == null || (p3.ProcessLicensor.Contains(term))
                                    select new SelectListItem() { Value = p3.Id.ToString(), Text = p3.ProcessLicensor }).ToList();
            
            return Json(lstProcessLicensor, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetTemplateName(string term, string TemplateType, string Location, string Product, string ProcessLicensor)
        {
            List<SelectListItem> lstTemplateName = new List<SelectListItem>();
            lstTemplateName = (from p3 in db.FKM251
                                    where p3.TemplateType == TemplateType && p3.Location == Location && p3.Product == Product && p3.ProcessLicensor == ProcessLicensor && (term == null || p3.TemplateName.Contains(term))
                                    select new SelectListItem() { Value = p3.Id.ToString(), Text = p3.TemplateName }).ToList();
            
            return Json(lstTemplateName, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SaveHBOMTemplate(FKM251 model, int HeaderId)
        {
            FKM251 objFKM251 = null;
            var objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var PlannerStructure = "";
                if (model.TemplateType == "B")
                {
                    var objFKM202 = db.FKM202.FirstOrDefault(w => w.HeaderId == HeaderId && w.PlannerStructure != null);
                    if (objFKM202 == null)
                    {
                        objResponseMsg.Value = "Template not found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    PlannerStructure = objFKM202.PlannerStructure;
                }
                else if (model.TemplateType == "K")
                {
                    var objFKM242 = db.FKM242.FirstOrDefault(w => w.HeaderId == HeaderId && w.PlannerStructure != null);
                    if (objFKM242 == null)
                    {
                        objResponseMsg.Value = "Template not found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    PlannerStructure = objFKM242.PlannerStructure;
                }
               
                if (model.Id > 0)
                {
                    objFKM251 = db.FKM251.FirstOrDefault(x => x.Id == model.Id);
                    objFKM251.TemplateContent = PlannerStructure;
                    objFKM251.EditedBy = objClsLoginInfo.UserName;
                    objFKM251.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                }
                else
                {
                    objFKM251 = new FKM251();
                    objFKM251.TemplateName = model.TemplateName;
                    objFKM251.Location = model.Location;
                    objFKM251.Product = model.Product;
                    objFKM251.ProcessLicensor = model.ProcessLicensor;
                    objFKM251.TemplateType = model.TemplateType;
                    objFKM251.TemplateContent = PlannerStructure;
                    objFKM251.CreatedBy = objClsLoginInfo.UserName;
                    objFKM251.CreatedOn = DateTime.Now;
                    db.FKM251.Add(objFKM251);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ValidateHBOMTemplate(FKM251 model)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objFKM251 = db.FKM251.FirstOrDefault(x => x.TemplateName == model.TemplateName && x.Location == model.Location && x.Product == model.Product && x.ProcessLicensor == model.ProcessLicensor && x.TemplateType == model.TemplateType);
                if (objFKM251 != null)
                    objResponseMsg.Value = objFKM251.Id.ToString();
                else
                    objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Utility
        [HttpPost]
        public JsonResult GetProjectDetails(string project, string sourceProject, int TemplateId = 0, bool IsCopy = true, bool IsGetFileUrl = false)
        {
            projectDetails prjDetails = new projectDetails();

            try
            {
                List<FKM203> objFKM203 = db.FKM203.ToList();
                int NodeTypeEquId = objFKM203.Where(x => x.NodeType.ToLower() == clsImplementationEnum.FKMSNodeType.Equipment.GetStringValue().ToLower()).FirstOrDefault().Id;
                int NodeTypeAssId = objFKM203.Where(x => x.NodeType.ToLower() == clsImplementationEnum.FKMSNodeType.Assembly.GetStringValue().ToLower()).FirstOrDefault().Id;

                /*List<FKM202> objDestiFKM202 = db.FKM202.Where(x => x.Project == project).ToList();
                bool isProjectExist = objDestiFKM202.Where(x => x.NodeTypeId == NodeTypeAssId).Any();
                if (isProjectExist)
                {
                    prjDetails.Key = false;
                    prjDetails.Value = string.Format(clsImplementationMessage.CommonMessages.ProjectExist.ToString(), project);
                    return Json(prjDetails, JsonRequestBehavior.AllowGet);
                }*/

                string query1 = "select d.t_bpid as [Customer], d.t_bpid +'-' + d.t_nama as [CustomerDescription], REPLACE(CONVERT( VARCHAR, c.t_efdt, 105 ),'-','/') as [ZeroDate], REPLACE(CONVERT( VARCHAR, e.t_ccdd, 105 ),'-','/') as [CDD] from " + LNLinkedServer + ".dbo.ttpctm110175 a left join " + LNLinkedServer + ".dbo.ttppdm600175 b on a.t_cprj = b.t_cprj left join " + LNLinkedServer + ".dbo.ttpctm100175 c on a.t_cono = c.t_cono left join " + LNLinkedServer + ".dbo.ttccom100175 d on c.t_ofbp = d.t_bpid left join " + LNLinkedServer + ".dbo.tltctm100175 e on c.t_cono = e.t_cono where a.t_cprj = '" + project + "'";
                prjDetails = db.Database.SqlQuery<projectDetails>(query1).FirstOrDefault();
                string query = "select ltrim(case a.t_item when '''' then a.t_nitm else a.t_item end) [nodeKey], a.t_cprj as [nodeProject], a.t_pono as [nodeFindno], a.t_edes as [nodeName], a.t_qnty as [nodeQty] from " + LNLinkedServer + ".dbo.tltpdm105175 a with(Nolock) join (select bb.t_cono, max(bb.t_amno) t_amno from " + LNLinkedServer + ".dbo.tltpdm105175 bb with(nolock) group by bb.t_cono) b on a.t_cono = b.t_cono and a.t_amno = b.t_amno left join" + LNLinkedServer + ".dbo.tltpdm105175 c with (nolock) on a.t_cono = c.t_cono and b.t_amno = c.t_amno and a.t_cprj = c.t_cprj and c.t_ityp = 2 left join " + LNLinkedServer + ".dbo.ttppdm600175 d with (nolock) on a.t_cprj = d.t_cprj where a.t_ityp in (1) and a.t_cprj ='" + project + "'";// and a.t_mdel = 1";
                prjDetails.destiProjectDetails = db.Database.SqlQuery<destiProjectDetails>(query).ToList();

                if (TemplateId != 0)
                {
                    var objFKM251 = db.FKM251.FirstOrDefault(f => f.Id == TemplateId);
                    var lstNodeData = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Models.NodeData>>(objFKM251.TemplateContent);
                    prjDetails.sourceProjectDetails = lstNodeData.Where(x => x.level == NodeTypeEquId)// && lstNodeName.Contains(x.NodeName))
                                                                .Select(x => new sourceProjectDetails
                                                                {
                                                                    nodeId = x.key,
                                                                    nodeName = x.name,
                                                                    IsPlannerStructure = false
                                                                }).ToList();
                }
                else if (sourceProject != null)
                {
                    //var lstNodeName = db.SP_FKMS_GET_PARENTNODE_BY_PROJECT(sourceProject).Where(w => w.t_mdel == 1).Select(s=>s.t_edes).ToList();
                    prjDetails.sourceProjectDetails = db.FKM202.Where(x => x.Project == sourceProject && x.NodeTypeId == NodeTypeEquId)// && lstNodeName.Contains(x.NodeName))
                                                                .Select(x => new sourceProjectDetails
                                                                {
                                                                    nodeId = x.NodeId,
                                                                    nodeName = x.NodeName,
                                                                    IsPlannerStructure = (db.FKM202.Where(y => y.Project == sourceProject && y.NodeTypeId == NodeTypeEquId && x.PlannerStructure != null).Any() != false ? true : false)
                                                                }).ToList();
                }

                // observation 17624 Copy Project Doesn't want to check Documents
                //if (IsCopy)
                //{
                //    // Task 16106 Product type get from PDN001 table
                //    var productType = db.PDN001.Where(x => x.Project == project).GroupBy(u => u.Product).ToDictionary(g => g.Key, g => g.Max(item => item.IssueNo)).FirstOrDefault();
                //    if (productType.Key != null)
                //    {
                //        prjDetails.ProductType = productType.Key;
                //    }
                //    else
                //    {
                //        prjDetails.Key = false;
                //        prjDetails.Value = string.Format(clsImplementationMessage.CommonMessages.ProjectNotExistInPDN.ToString(), project);
                //        return Json(prjDetails, JsonRequestBehavior.AllowGet);
                //    }
                //}

                if (IsGetFileUrl)
                {
                    var objFKM201 = db.FKM201.FirstOrDefault(f=>f.Project == project);
                    if(objFKM201 != null)
                    {
                        var Files = (new clsFileUpload()).GetDocuments("FKM201/" + objFKM201.HeaderId + "/R" + objFKM201.RevNo);
                        if (Files.Count() > 0)
                        {
                            prjDetails.ImageUrl = Files.FirstOrDefault().URL;
                        }
                    }
                }
                prjDetails.Key = true;
                prjDetails.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                return Json(prjDetails, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                prjDetails.Key = false;
                prjDetails.Value = "Something went wrong..!!";
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(prjDetails, JsonRequestBehavior.AllowGet);
            }
        }

        public void DefaultEntry(int HeaderId, string Project)
        {
            List<FKM202> objFKM202 = new List<FKM202>();
            List<FKM203> objFKM203 = db.FKM203.ToList();
            FKM202 objProject = new FKM202
            {
                HeaderId = HeaderId,
                ParentNodeId = 0,
                NodeTypeId = objFKM203.Where(x => x.NodeType.ToLower() == clsImplementationEnum.FKMSNodeType.Project.GetStringValue().ToLower()).FirstOrDefault().Id,
                NodeName = Project,
                Project = Project
            };
            db.FKM202.Add(objProject);
            db.SaveChanges();
            objFKM202.Add(new FKM202
            {
                HeaderId = HeaderId,
                ParentNodeId = objProject.NodeId,
                NodeTypeId = objFKM203.Where(x => x.NodeType.ToLower() == clsImplementationEnum.FKMSNodeType.LooseItem.GetStringValue().ToLower()).FirstOrDefault().Id,
                NodeName = objFKM203.Where(x => x.NodeType.ToLower() == clsImplementationEnum.FKMSNodeType.LooseItem.GetStringValue().ToLower()).FirstOrDefault().NodeType,
                Project = Project
            });
            objFKM202.Add(new FKM202
            {
                HeaderId = HeaderId,
                ParentNodeId = objProject.NodeId,
                NodeTypeId = objFKM203.Where(x => x.NodeType.ToLower() == clsImplementationEnum.FKMSNodeType.SpareItem.GetStringValue().ToLower()).FirstOrDefault().Id,
                NodeName = objFKM203.Where(x => x.NodeType.ToLower() == clsImplementationEnum.FKMSNodeType.SpareItem.GetStringValue().ToLower()).FirstOrDefault().NodeType,
                Project = Project
            });
            objFKM202.Add(new FKM202
            {
                HeaderId = HeaderId,
                ParentNodeId = objProject.NodeId,
                NodeTypeId = objFKM203.Where(x => x.NodeType.ToLower() == clsImplementationEnum.FKMSNodeType.Template.GetStringValue().ToLower()).FirstOrDefault().Id,
                NodeName = objFKM203.Where(x => x.NodeType.ToLower() == clsImplementationEnum.FKMSNodeType.Template.GetStringValue().ToLower()).FirstOrDefault().NodeType,
                Project = Project
            });
            objFKM202.Add(new FKM202
            {
                HeaderId = HeaderId,
                ParentNodeId = objProject.NodeId,
                NodeTypeId = objFKM203.Where(x => x.NodeType.ToLower() == clsImplementationEnum.FKMSNodeType.Equipment.GetStringValue().ToLower()).FirstOrDefault().Id,
                NodeName = objFKM203.Where(x => x.NodeType.ToLower() == clsImplementationEnum.FKMSNodeType.Equipment.GetStringValue().ToLower()).FirstOrDefault().NodeType,
                Project = Project
            });
            db.FKM202.AddRange(objFKM202);
            db.SaveChanges();
        }

        public void DefaultEntryPLM(int HeaderId, string Project, string CopyFromProject = "", List<copyNodeList> copyValueList = null, string copyStructure = "")
        {
            List<FKM202> objFKM202 = new List<FKM202>();
            List<FKM203> objFKM203 = db.FKM203.ToList();
            var lstNodeData = new List<Models.NodeData>();
            if (CopyFromProject != "" && copyStructure == "")
            {
                int ProjectId = objFKM203.Where(x => x.NodeType.ToLower() == FKMSNodeType.Project.GetStringValue().ToLower()).FirstOrDefault().Id;
                copyStructure = db.FKM202.Where(x => x.Project == CopyFromProject && x.PlannerStructure != null && x.NodeTypeId == ProjectId).Select(x => x.PlannerStructure).FirstOrDefault();
            }
            var lstPLM = db.SP_FKMS_GET_PARENTNODE_BY_PROJECT(Project).ToList();
            if (lstPLM.Count > 0)
            {
                var projectDesc =  db.COM001.FirstOrDefault(f => f.t_cprj == Project);
                FKM202 objProject = new FKM202
                {
                    HeaderId = HeaderId,
                    ParentNodeId = 0,
                    ParentNodeId_Init = 0,
                    NoOfPieces = Convert.ToInt32(_defaultNumberOfPieces),
                    NoOfPieces_Init = Convert.ToInt32(_defaultNumberOfPieces),
                    NodeTypeId = objFKM203.Where(x => x.NodeType.ToLower() == FKMSNodeType.Project.GetStringValue().ToLower()).FirstOrDefault().Id,
                    NodeName = Project + (projectDesc==null?"": "\n"+ projectDesc.t_dsca),// lstPLM.Select(x => x.parentdesc).FirstOrDefault(),
                    Project = Project,
                    ProductType = NodeTypes.ASM.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                    PlannerStructure = copyStructure != string.Empty ? copyStructure : null
                };
                db.FKM202.Add(objProject);
                db.SaveChanges();
                lstNodeData.Add(new Models.NodeData() { key = objProject.NodeId, name = objProject.NodeName, level = objProject.NodeTypeId, type = "Fixed", parent = 0, mdel = 0 });
                var NodeTypeId = objFKM203.FirstOrDefault(x => x.NodeType.ToLower() == FKMSNodeType.Equipment.GetStringValue().ToLower()).Id;
                foreach (var item in lstPLM)
                {
                    objFKM202.Add(new FKM202
                    {
                        HeaderId = HeaderId,
                        ParentNodeId = objProject.NodeId,
                        ParentNodeId_Init = objProject.NodeId,
                        NodeTypeId = NodeTypeId,
                        NodeName = item.t_edes,
                        NodeKey = item.t_item,
                        NoOfPieces = Convert.ToInt32(item.t_qnty),
                        FindNo = Convert.ToString(item.t_pono),
                        NodeKey_Init = item.t_item,
                        NoOfPieces_Init = Convert.ToInt32(item.t_qnty),
                        FindNo_Init = Convert.ToString(item.t_pono),
                        Project = Project,
                        ProductType = NodeTypes.ASM.GetStringValue(),
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                }
                db.FKM202.AddRange(objFKM202);
                db.SaveChanges();
                int i = 0;
                foreach (var item in objFKM202)
                {
                    lstNodeData.Add(new Models.NodeData() { key = item.NodeId, name = item.NodeName, level = item.NodeTypeId, type = "Fixed", parent = objProject.NodeId, mdel = lstPLM[i++].t_mdel });
                }
                if (copyStructure != "" && copyValueList != null)
                {
                    var DefaultLevel = new string[] { FKMSNodeType.Project.GetStringValue(), FKMSNodeType.LooseItem.GetStringValue(), FKMSNodeType.SpareItem.GetStringValue(), FKMSNodeType.Template.GetStringValue(), FKMSNodeType.Equipment.GetStringValue() };
                    var DefaultLevelId = db.FKM203.Where(x => DefaultLevel.Contains(x.NodeType)).Select(x => x.Id).ToList();
                    var lstFromNodeData = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Models.NodeData>>(copyStructure).Where(x=> !DefaultLevelId.Contains(x.level));
                    foreach (var item in lstFromNodeData)
                    {
                        var copyValue = copyValueList.FirstOrDefault(s => s.sourceNodeId == item.parent + "");
                        var objparentFKM202 = copyValue == null ? null : objFKM202.FirstOrDefault(w => w.FindNo == copyValue.destiFindNo);
                        if (objparentFKM202 != null)
                        {
                            item.parent = objparentFKM202.NodeId;
                            lstNodeData.Add(item);
                        }
                        else if (lstNodeData.Any(a => a.key == item.parent))
                            lstNodeData.Add(item);
                    }
                    #region FKM202 Node Updates 
                    int PWHTSrNo = 0, SectionSrNo = 0, GenerateNo = 0, AutoGeneratedFindNo = StartFindNo - 1;
                     foreach (var parent in lstNodeData.Where(w => w.parent == 0 || w.parent == null))
                     {
                         parent.parent = 0;
                         AutoGeneratedFindNo++;
                         AddUpdateFKM202ByNode(ref DefaultLevelId, ref lstNodeData, parent, objProject.HeaderId, objProject.Project, ref PWHTSrNo, ref SectionSrNo, ref GenerateNo, ref AutoGeneratedFindNo);
                     }

                     var lstNodeIDs = lstNodeData.Select(s => s.key);
                     var deletedFKM202 = db.FKM202.Where(w => w.HeaderId == objProject.HeaderId && !lstNodeIDs.Contains(w.NodeId));
                     db.FKM202.RemoveRange(deletedFKM202);
                    #endregion
                }
            }
            var obj_FKM202 = db.FKM202.FirstOrDefault(x => x.HeaderId == HeaderId && x.ParentNodeId == 0);
            obj_FKM202.PlannerStructure = Newtonsoft.Json.JsonConvert.SerializeObject(lstNodeData);
            db.SaveChanges();
        }

        public JsonResult FilterProject(string project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                bool isProjectExist = db.FKM201.Where(x => x.Project == project).Any();
                if (isProjectExist)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = string.Format(clsImplementationMessage.CommonMessages.ProjectExist.ToString(), project);
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var lstPLM = db.SP_FKMS_GET_PARENTNODE_BY_PROJECT(project).ToList();
                    if (lstPLM.Count > 0)
                    {
                        objResponseMsg.Key = true;
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = string.Format(clsImplementationMessage.CommonMessages.ProjectNotExistInLN.ToString(), project);
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }



                    //var lstHBOM = db.SP_FKMS_HBOM_FOR_PROJECT(project).ToList();
                    //if (lstHBOM.Count > 0)
                    //{
                    //    objResponseMsg.Key = false;
                    //    objResponseMsg.Value = string.Format(clsImplementationMessage.CommonMessages.ProjectExistInHBOM.ToString(), project);
                    //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    //}
                    //else
                    //{
                    //    var lstPLM = db.SP_FKMS_GET_PARENTNODE_BY_PROJECT(project).ToList();
                    //    if (lstPLM.Count > 0)
                    //    {
                    //        objResponseMsg.Key = true;
                    //        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    //    }
                    //    else
                    //    {
                    //        objResponseMsg.Key = false;
                    //        objResponseMsg.Value = string.Format(clsImplementationMessage.CommonMessages.ProjectExistInPLM.ToString(), project);
                    //        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    //    }
                    //}
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult CopyFilterProject(string project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var NodeTypeAssembly = clsImplementationEnum.FKMSNodeType.Assembly.GetStringValue();
                var nodeTypeId = db.FKM203.FirstOrDefault(y => y.NodeType == NodeTypeAssembly).Id;
                bool isProjectExist = db.FKM202.Any(x => x.Project == project && x.NodeTypeId == nodeTypeId);
                if (isProjectExist)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = string.Format(clsImplementationMessage.CommonMessages.ProjectExist.ToString(), project);
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (db.SP_FKMS_GET_PARENTNODE_BY_PROJECT(project).Any())
                    {
                        objResponseMsg.Key = true;
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = string.Format(clsImplementationMessage.CommonMessages.ProjectNotExistInLN.ToString(), project);
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public string SetColorFlag(int? nodeId, decimal? qty, string nodeType)
        {
            string ColorFlag = clsImplementationEnum.ColorFlagForFKMS.Red.GetStringValue();
            if (nodeType.ToLower() == clsImplementationEnum.NodeTypes.PLT.GetStringValue().ToLower())
            {
                //var lstFKM108 = db.FKM108.GroupBy(x => new { NodeId = x.NodeId }).Select(y => new { Key = y.Key, Qty = y.Sum(z => z.AllocatedQty) }).ToList();
                //var objFKM108 = lstFKM108.Where(x => x.Key.NodeId == nodeId).FirstOrDefault();
                var objFKM108 = db.FKM108.Where(x => x.NodeId == nodeId).GroupBy(x => new { NodeId = x.NodeId }).Select(y => new { Key = y.Key, Qty = y.Sum(z => z.AllocatedQty) }).FirstOrDefault();

                if (objFKM108 != null && objFKM108.Qty == qty)
                {
                    ColorFlag = clsImplementationEnum.ColorFlagForFKMS.Green.GetStringValue();
                }
                else if (objFKM108 != null && objFKM108.Qty != qty)
                {
                    ColorFlag = clsImplementationEnum.ColorFlagForFKMS.Yellow.GetStringValue();
                }
            }
            else if (nodeType.ToLower() != clsImplementationEnum.NodeTypes.ASM.GetStringValue().ToLower() && nodeType.ToLower() != clsImplementationEnum.NodeTypes.TJF.GetStringValue().ToLower()
                && nodeType.ToLower() != clsImplementationEnum.NodeTypes.PLT.GetStringValue().ToLower())
            {
                //var lstFKM109 = db.FKM109.GroupBy(x => new { NodeId = x.NodeId }).Select(y => new { NodeId = y.Key, Qty = y.Sum(z => z.AllocatedQty) }).ToList();
                //var objFKM109 = lstFKM109.Where(x => Convert.ToInt32(x.NodeId.NodeId) == nodeId).FirstOrDefault();

                var objFKM109 = db.FKM109.Where(x => x.NodeId == nodeId).GroupBy(x => new { NodeId = x.NodeId }).Select(y => new { NodeId = y.Key, Qty = y.Sum(z => z.AllocatedQty) }).FirstOrDefault();

                if (objFKM109 != null && objFKM109.Qty == qty)
                {
                    ColorFlag = clsImplementationEnum.ColorFlagForFKMS.Green.GetStringValue();
                }
                else if (objFKM109 != null && objFKM109.Qty != qty)
                {
                    ColorFlag = clsImplementationEnum.ColorFlagForFKMS.Yellow.GetStringValue();
                }
            }
            return ColorFlag;
        }

        public void listASMColors(int HeaderId)
        {
            //FKM202 objStartMergeNode = null;
            //if (HeaderId != 0)
            //{
            //    objStartMergeNode = db.FKM202.Where(i => i.HeaderId == HeaderId).FirstOrDefault();
            //}
            //objParentsclrFKM202 = FN_GET_ALL_CHILDNODE_FOR_NODE_FKM202(objStartMergeNode.NodeId, 1).ToList();

            //string ASM = clsImplementationEnum.NodeTypes.ASM.GetStringValue().ToLower();
            //string TJF = clsImplementationEnum.NodeTypes.TJF.GetStringValue().ToLower();
            //string YL = clsImplementationEnum.ColorFlagForFKMS.Yellow.GetStringValue();
            //string GR = clsImplementationEnum.ColorFlagForFKMS.Green.GetStringValue();
            //string RE = clsImplementationEnum.ColorFlagForFKMS.Red.GetStringValue();
            //string Relstatus = clsImplementationEnum.KitStatus.Release.GetStringValue();

            //foreach (var item in objParentsclrFKM202)
            //{
            //    if (item.ProductType.ToLower() != ASM.ToLower() && item.ProductType.ToLower() != TJF.ToLower())
            //    {
            //        decimal? nop = Convert.ToDecimal(item.NOP);
            //        string Platecolor = SetColorFlag(item.NodeId, nop, item.ProductType);
            //        item.ASMPartsColor = Platecolor;
            //    }
            //}
            //foreach (var item in objParentsclrFKM202)
            //{
            //    if (objParentsclrFKM202.Any(x => x.ParentNodeId == item.NodeId && 
            //                                    x.ProductType.ToLower() != ASM.ToLower() && 
            //                                    x.ProductType.ToLower() != TJF.ToLower() && 
            //                                    x.ASMPartsColor == YL))
            //    {
            //        item.ASMPartsColor = clsImplementationEnum.ColorFlagForFKMS.Violet.GetStringValue();
            //    }
            //    else if (objParentsclrFKM202.Any(x => x.ParentNodeId == item.NodeId && 
            //                                        x.ProductType.ToLower() != ASM.ToLower() && 
            //                                        x.ProductType.ToLower() != TJF.ToLower() && 
            //                                        x.ASMPartsColor == GR) && 
            //            objParentsclrFKM202.Any(x => x.ParentNodeId == item.NodeId && x.ASMPartsColor == RE))
            //    {
            //        item.ASMPartsColor = clsImplementationEnum.ColorFlagForFKMS.Yellow.GetStringValue();
            //    }
            //    else if (objParentsclrFKM202.Any(x => x.ParentNodeId == item.NodeId && 
            //                                        x.ProductType.ToLower() != ASM.ToLower() && 
            //                                        x.ProductType.ToLower() != TJF.ToLower() && 
            //                                        x.ASMPartsColor != null) && 
            //            !objParentsclrFKM202.Any(x => x.ParentNodeId == item.NodeId && x.ASMPartsColor == GR) && 
            //            !objParentsclrFKM202.Any(x => x.ParentNodeId == item.NodeId && x.ASMPartsColor == YL))
            //    {
            //        item.ASMPartsColor = clsImplementationEnum.ColorFlagForFKMS.Red.GetStringValue();
            //    }
            //    else if (objParentsclrFKM202.Any(x => x.ParentNodeId == item.NodeId &&
            //                                        x.ProductType.ToLower() != ASM.ToLower() &&
            //                                        x.ProductType.ToLower() != TJF.ToLower() &&
            //                                        x.ASMPartsColor != null) && 
            //            !objParentsclrFKM202.Any(x => x.ParentNodeId == item.NodeId && x.ASMPartsColor == RE) &&
            //            !objParentsclrFKM202.Any(x => x.ParentNodeId == item.NodeId && x.ASMPartsColor == YL) &&
            //            objParentsclrFKM202.Any(x => x.ParentNodeId == item.NodeId && item.KitStatus != string.Empty))
            //    {
            //        item.ASMPartsColor = clsImplementationEnum.ColorFlagForFKMS.Green.GetStringValue();
            //    }
            //    else if (objParentsclrFKM202.Any(x => x.ParentNodeId == item.NodeId && 
            //                                        x.ProductType.ToLower() != ASM.ToLower() && 
            //                                        x.ProductType.ToLower() != TJF.ToLower() && 
            //                                        x.ASMPartsColor != null) && 
            //            !objParentsclrFKM202.Any(x => x.ParentNodeId == item.NodeId && x.ASMPartsColor == RE) && 
            //            !objParentsclrFKM202.Any(x => x.ParentNodeId == item.NodeId && x.ASMPartsColor == YL) && 
            //            objParentsclrFKM202.Any(x => x.ParentNodeId == item.NodeId && item.KitStatus == string.Empty))
            //    {
            //        item.ASMPartsColor = clsImplementationEnum.ColorFlagForFKMS.Orange.GetStringValue();
            //    }

            //    if (objParentsclrFKM202.Any(x => x.ProductType.ToLower() == ASM.ToLower() || x.ProductType.ToLower() == TJF.ToLower()))
            //    {
            //        if (!string.IsNullOrEmpty(item.TASKUNIQUEID))
            //        {
            //            if (CheckTaskAvailable(item.TASKUNIQUEID, item.DeleverTo, item.Project))
            //            {
            //                item.ASMPartsColor = clsImplementationEnum.ColorFlagForFKMS.Violet.GetStringValue();
            //            }
            //        }
            //    }
            //}

            SetASMPartsColors(HeaderId);
        }

        #region Parent Child Color Set
        public void SetASMPartsColors(int HeaderId, int ParentNodeId = 0)
        {
            FKM202 objStartMergeNode = null;
            if (HeaderId != 0)
            {
                if (ParentNodeId != 0)
                {
                    objStartMergeNode = db.FKM202.Where(i => i.HeaderId == HeaderId && i.NodeId == ParentNodeId).FirstOrDefault();
                }
                else
                {
                    objStartMergeNode = db.FKM202.Where(i => i.HeaderId == HeaderId).FirstOrDefault();
                }
            }
            objParentsclrFKM202 = FN_GET_ALL_CHILDNODE_FOR_NODE_FKM202(objStartMergeNode.NodeId, 1).ToList();
            var maxLevel = objParentsclrFKM202.Max(i => i.NodeLevel);

            string RedColor = clsImplementationEnum.ColorFlagForFKMS.Red.GetStringValue();
            string YellowColor = clsImplementationEnum.ColorFlagForFKMS.Yellow.GetStringValue();
            string GreenColor = clsImplementationEnum.ColorFlagForFKMS.Green.GetStringValue();
            string BlueColor = clsImplementationEnum.ColorFlagForFKMS.Blue.GetStringValue();
            string strSendToNextKit = clsImplementationEnum.KitStatus.SendToNextKit.GetStringValue();
            string strSendToFullkitArea = clsImplementationEnum.KitStatus.SendToFullkitArea.GetStringValue();

            for (int k = maxLevel; k >= 0; k--)
            {
                var objLevel = objParentsclrFKM202.Where(i => i.NodeLevel == k).ToList();
                foreach (var item in objLevel)
                {
                    if (item.ProductType.ToLower() != ASM.ToLower() && item.ProductType.ToLower() != TJF.ToLower())
                    {
                        decimal? nop = Convert.ToDecimal(item.NOP);
                        string Platecolor = SetColorFlag(item.NodeId, nop, item.ProductType);

                        objParentsclrFKM202.Where(i => i.NodeId == item.NodeId).FirstOrDefault().ASMPartsColor = Platecolor;
                    }
                    else if (item.ProductType.ToLower() == ASM.ToLower() || item.ProductType.ToLower() == TJF.ToLower())
                    {
                        if (item.NodeLevel == maxLevel)
                        {
                            objParentsclrFKM202.Where(i => i.NodeId == item.NodeId).FirstOrDefault().ASMPartsColor = RedColor;
                        }
                        else
                        {
                            if (objParentsclrFKM202.Any(i => i.ParentNodeId == item.NodeId))
                            {
                                var objPreviousLevel = objParentsclrFKM202.Where(i => i.ParentNodeId == item.NodeId).ToList();
                                var IsOnlyASM = objPreviousLevel.Where(i => i.ProductType.ToLower() == ASM || i.ProductType.ToLower() == TJF).Count() == objPreviousLevel.Count();
                                var IsOnlyParts = objPreviousLevel.Where(i => i.ProductType.ToLower() != ASM && i.ProductType.ToLower() != TJF).Count() == objPreviousLevel.Count();
                                if (IsOnlyParts)
                                {
                                    if (objPreviousLevel.Any(x => x.ASMPartsColor == YellowColor))
                                    {
                                        item.ASMPartsColor = clsImplementationEnum.ColorFlagForFKMS.Violet.GetStringValue();
                                    }
                                    else if (objPreviousLevel.Any(x => x.ASMPartsColor == GreenColor) && objPreviousLevel.Any(x => x.ASMPartsColor == RedColor))
                                    {
                                        item.ASMPartsColor = clsImplementationEnum.ColorFlagForFKMS.Yellow.GetStringValue();
                                    }
                                    else if (!objPreviousLevel.Any(x => x.ASMPartsColor == GreenColor) && !objPreviousLevel.Any(x => x.ASMPartsColor == YellowColor))
                                    {
                                        item.ASMPartsColor = clsImplementationEnum.ColorFlagForFKMS.Red.GetStringValue();
                                    }
                                    else if (objPreviousLevel.Any(x => x.ASMPartsColor != null) &&
                                            !objPreviousLevel.Any(x => x.ASMPartsColor == RedColor) &&
                                            !objPreviousLevel.Any(x => x.ASMPartsColor == YellowColor) &&
                                            item.KitStatus == KitStatus.Release.GetStringValue())
                                    {
                                        item.ASMPartsColor = clsImplementationEnum.ColorFlagForFKMS.Green.GetStringValue();
                                    }
                                    else if (objPreviousLevel.Any(x => x.ASMPartsColor != null) &&
                                            !objPreviousLevel.Any(x => x.ASMPartsColor == RedColor) &&
                                            !objPreviousLevel.Any(x => x.ASMPartsColor == YellowColor) &&
                                            (item.KitStatus == KitStatus.ReadyForParent.GetStringValue() ||
                                             item.KitStatus == KitStatus.SendToNextKit.GetStringValue() ||
                                             item.KitStatus == KitStatus.SendToFullkitArea.GetStringValue() ||
                                             item.KitStatus == KitStatus.RequestedByParentKit.GetStringValue() ||
                                             item.KitStatus == KitStatus.Accept.GetStringValue()))
                                    {
                                        item.ASMPartsColor = clsImplementationEnum.ColorFlagForFKMS.Blue.GetStringValue();
                                        if (item.KitStatus == clsImplementationEnum.KitStatus.ReadyForParent.GetStringValue() &&
                                            item.DeleverStatus == clsImplementationEnum.FKMSMaterialDeliveryStatus.Delivered.GetStringValue())
                                        {
                                            SendToNextKit(item.NodeId.Value);
                                        }
                                    }
                                    else if (objPreviousLevel.Any(x => x.ASMPartsColor != null) &&
                                            !objPreviousLevel.Any(x => x.ASMPartsColor == RedColor) &&
                                            !objPreviousLevel.Any(x => x.ASMPartsColor == YellowColor) &&
                                            objPreviousLevel.Any(x => x.KitStatus == string.Empty))
                                    {
                                        if (UpdateKitStatus(item.NodeId.Value).Key)
                                        {
                                            item.ASMPartsColor = clsImplementationEnum.ColorFlagForFKMS.Green.GetStringValue();
                                        }
                                        else
                                        {
                                            item.ASMPartsColor = clsImplementationEnum.ColorFlagForFKMS.Orange.GetStringValue();
                                        }
                                    }
                                    else if (objPreviousLevel.Where(x => x.ASMPartsColor == RedColor).Count() == objPreviousLevel.Count())
                                    {
                                        item.ASMPartsColor = clsImplementationEnum.ColorFlagForFKMS.Red.GetStringValue();
                                    }
                                }
                                else if (IsOnlyASM)
                                {
                                    if (objPreviousLevel.Where(i => i.ASMPartsColor == BlueColor && (i.KitStatus == strSendToNextKit || i.KitStatus == strSendToFullkitArea)).Count() == objPreviousLevel.Count && item.KitStatus == string.Empty)
                                    {
                                        if (UpdateKitStatus(item.NodeId.Value).Key)
                                        {
                                            item.ASMPartsColor = clsImplementationEnum.ColorFlagForFKMS.Green.GetStringValue();
                                        }
                                        else
                                        {
                                            item.ASMPartsColor = clsImplementationEnum.ColorFlagForFKMS.Orange.GetStringValue();
                                        }
                                    }
                                    else if (objPreviousLevel.Where(i => i.ASMPartsColor == BlueColor).Count() == objPreviousLevel.Count &&
                                        item.KitStatus == KitStatus.Release.GetStringValue())
                                    {
                                        item.ASMPartsColor = clsImplementationEnum.ColorFlagForFKMS.Green.GetStringValue();
                                    }
                                    else if (objPreviousLevel.Where(i => i.ASMPartsColor == BlueColor).Count() == objPreviousLevel.Count &&
                                            (item.KitStatus == KitStatus.ReadyForParent.GetStringValue() ||
                                             item.KitStatus == KitStatus.SendToNextKit.GetStringValue() ||
                                             item.KitStatus == KitStatus.SendToFullkitArea.GetStringValue() ||
                                             item.KitStatus == KitStatus.RequestedByParentKit.GetStringValue() ||
                                             item.KitStatus == KitStatus.Accept.GetStringValue()))
                                    {
                                        item.ASMPartsColor = clsImplementationEnum.ColorFlagForFKMS.Blue.GetStringValue();
                                        if (item.KitStatus == clsImplementationEnum.KitStatus.ReadyForParent.GetStringValue() &&
                                            item.DeleverStatus == clsImplementationEnum.FKMSMaterialDeliveryStatus.Delivered.GetStringValue())
                                        {
                                            SendToNextKit(item.NodeId.Value);
                                        }
                                    }
                                    else
                                    {
                                        item.ASMPartsColor = clsImplementationEnum.ColorFlagForFKMS.Red.GetStringValue();
                                    }
                                }
                                else
                                {
                                    var ASMCnt = objPreviousLevel.Where(i => i.ProductType.ToLower() == ASM || i.ProductType.ToLower() == TJF).Count();
                                    var PartCnt = objPreviousLevel.Where(i => i.ProductType.ToLower() != ASM && i.ProductType.ToLower() != TJF).Count();
                                    var BlueASMCnt = objPreviousLevel.Where(i => (i.ProductType.ToLower() == ASM || i.ProductType.ToLower() == TJF) && i.ASMPartsColor == BlueColor).Count();
                                    var GreenPartCnt = objPreviousLevel.Where(i => i.ProductType.ToLower() != ASM && i.ProductType.ToLower() != TJF && i.ASMPartsColor == GreenColor).Count();
                                    var RedASMCnt = objPreviousLevel.Where(i => (i.ProductType.ToLower() == ASM || i.ProductType.ToLower() == TJF) && i.ASMPartsColor == RedColor).Count();
                                    var RedPartCnt = objPreviousLevel.Where(i => i.ProductType.ToLower() != ASM && i.ProductType.ToLower() != TJF && i.ASMPartsColor == RedColor).Count();
                                    var NonBlueASMCnt = objPreviousLevel.Where(i => (i.ProductType.ToLower() == ASM || i.ProductType.ToLower() == TJF) && i.ASMPartsColor != BlueColor).Count();
                                    var IsAnyNonASMBlue = NonBlueASMCnt > 0;
                                    var IsAnyASMBlue = BlueASMCnt > 0;
                                    var IsAnyPartYellow = objPreviousLevel.Any(i => (i.ProductType.ToLower() != ASM && i.ProductType.ToLower() != TJF) && i.ASMPartsColor == YellowColor);
                                    #region Original Logic
                                    //if ((ASMCnt == BlueASMCnt && PartCnt == RedPartCnt))
                                    //{
                                    //    item.ASMPartsColor = ColorFlagForFKMS.Yellow.GetStringValue();
                                    //}
                                    //else if (ASMCnt == NonBlueASMCnt && PartCnt == RedPartCnt)
                                    //{
                                    //    item.ASMPartsColor = ColorFlagForFKMS.Red.GetStringValue();
                                    //}
                                    //else if ((IsAnyNonASMBlue || IsAnyASMBlue) && PartCnt == RedPartCnt)
                                    //{
                                    //    item.ASMPartsColor = ColorFlagForFKMS.Yellow.GetStringValue();
                                    //}
                                    //else if (ASMCnt == BlueASMCnt && PartCnt == GreenPartCnt)
                                    //{
                                    //    item.ASMPartsColor = ColorFlagForFKMS.Orange.GetStringValue();
                                    //}
                                    //else if (ASMCnt == NonBlueASMCnt && PartCnt == GreenPartCnt)
                                    //{
                                    //    item.ASMPartsColor = ColorFlagForFKMS.Yellow.GetStringValue();
                                    //}
                                    //else if ((IsAnyNonASMBlue || IsAnyASMBlue) && PartCnt == GreenPartCnt)
                                    //{
                                    //    item.ASMPartsColor = ColorFlagForFKMS.Yellow.GetStringValue();
                                    //}
                                    //else if (IsAnyPartYellow)
                                    //{
                                    //    item.ASMPartsColor = ColorFlagForFKMS.Violet.GetStringValue();
                                    //}
                                    //else if ((RedPartCnt > 0 && GreenPartCnt > 0) && (ASMCnt == BlueASMCnt))
                                    //{
                                    //    item.ASMPartsColor = ColorFlagForFKMS.Yellow.GetStringValue();
                                    //}
                                    //else if ((RedPartCnt > 0 && GreenPartCnt > 0) && (ASMCnt == NonBlueASMCnt))
                                    //{
                                    //    item.ASMPartsColor = ColorFlagForFKMS.Yellow.GetStringValue();
                                    //}
                                    //else if ((RedPartCnt > 0 && GreenPartCnt > 0) && (IsAnyNonASMBlue || IsAnyASMBlue))
                                    //{
                                    //    item.ASMPartsColor = ColorFlagForFKMS.Yellow.GetStringValue();
                                    //}
                                    #endregion

                                    #region Opimize Logic
                                    if (item.KitStatus == KitStatus.Release.GetStringValue())
                                    {
                                        item.ASMPartsColor = ColorFlagForFKMS.Green.GetStringValue();
                                    }
                                    else if (IsAnyPartYellow)
                                    {
                                        item.ASMPartsColor = ColorFlagForFKMS.Violet.GetStringValue();
                                    }
                                    else if (ASMCnt == NonBlueASMCnt && PartCnt == RedPartCnt)
                                    {
                                        item.ASMPartsColor = ColorFlagForFKMS.Red.GetStringValue();
                                    }
                                    else if (ASMCnt == BlueASMCnt && PartCnt == GreenPartCnt)
                                    {
                                        if (UpdateKitStatus(item.NodeId.Value).Key)
                                        {
                                            item.ASMPartsColor = clsImplementationEnum.ColorFlagForFKMS.Green.GetStringValue();
                                        }
                                        else
                                        {
                                            item.ASMPartsColor = ColorFlagForFKMS.Orange.GetStringValue();
                                        }
                                    }
                                    else if (((ASMCnt == BlueASMCnt && PartCnt == RedPartCnt))
                                            || ((IsAnyNonASMBlue || IsAnyASMBlue) && PartCnt == RedPartCnt)
                                            || (ASMCnt == NonBlueASMCnt && PartCnt == GreenPartCnt)
                                            || ((IsAnyNonASMBlue || IsAnyASMBlue) && PartCnt == GreenPartCnt)
                                            || ((RedPartCnt > 0 && GreenPartCnt > 0) && (ASMCnt == BlueASMCnt))
                                            || ((RedPartCnt > 0 && GreenPartCnt > 0) && (ASMCnt == NonBlueASMCnt))
                                            || ((RedPartCnt > 0 && GreenPartCnt > 0) && (IsAnyNonASMBlue || IsAnyASMBlue))
                                        )
                                    {
                                        item.ASMPartsColor = ColorFlagForFKMS.Yellow.GetStringValue();
                                    }

                                    #endregion
                                }
                            }
                            else
                            {
                                objParentsclrFKM202.Where(i => i.NodeId == item.NodeId).FirstOrDefault().ASMPartsColor = RedColor;
                            }
                        }
                    }
                }
            }

            var lstNodes = objParentsclrFKM202.Select(i => i.NodeId).Distinct().ToList();
            var objFKM202 = db.FKM202.Where(i => lstNodes.Contains(i.NodeId)).ToList();
            objFKM202.ForEach(i =>
            {
                i.NodeColor = objParentsclrFKM202.Where(x => x.NodeId == i.NodeId).FirstOrDefault().ASMPartsColor;
            });

            db.SaveChanges();

            if (IsShowFixture == "1")
                SetFixtureCompleteStatus(objParentsclrFKM202);
        }
        #endregion

        public string PLTSetColorFlag(int? nodeId)
        {
            string ColorFlag = clsImplementationEnum.ColorFlagForFKMS.Default.GetStringValue();
            if (objParentsclrFKM202.Any(x => x.NodeId == nodeId))
            {
                var color = objParentsclrFKM202.Where(x => x.NodeId == nodeId).Select(x => x.ASMPartsColor).FirstOrDefault();
                if (color != null)
                {
                    ColorFlag = color;
                }
            }
            return ColorFlag;
        }

        public string BorderLeftColorFlag(int? nodeId, string findno, string Color)
        {
            string ColorFlag = string.Empty;

            //string Color = PLTSetColorFlag(nodeId);
            ColorFlag = "<div style='display:inline-flex;'>";
            if (Color == clsImplementationEnum.ColorFlagForFKMS.Green.GetStringValue())
            {
                ColorFlag += "<div class='borderleftsuccess'></div>";
            }
            else if (Color == clsImplementationEnum.ColorFlagForFKMS.Yellow.GetStringValue())
            {
                ColorFlag += "<div class='borderleftwarning'></div>";
            }
            else if (Color == clsImplementationEnum.ColorFlagForFKMS.Red.GetStringValue())
            {
                ColorFlag += "<div class='borderleftdanger'></div>";
            }
            else if (Color == clsImplementationEnum.ColorFlagForFKMS.Violet.GetStringValue())
            {
                ColorFlag += "<div class='borderleftvoilet'></div>";
            }
            else if (Color == clsImplementationEnum.ColorFlagForFKMS.Grey.GetStringValue())
            {
                ColorFlag += "<div class='borderleftgray'></div>";
            }
            else if (Color == clsImplementationEnum.ColorFlagForFKMS.Orange.GetStringValue())
            {
                ColorFlag += "<div class='borderleftorange'></div>";
            }
            else if (Color == clsImplementationEnum.ColorFlagForFKMS.Blue.GetStringValue())
            {
                ColorFlag += "<div class='borderleftblue'></div>";
            }
            else if (Color == clsImplementationEnum.ColorFlagForFKMS.Default.GetStringValue())
            {
                ColorFlag += "";
            }
            ColorFlag += "<div>&nbsp;" + findno + "</div></div>";
            return ColorFlag;
        }

        public UserRoleAccessDetails GetUserAccessRights()
        {
            UserRoleAccessDetails objUserRoleAccessDetails = new UserRoleAccessDetails();

            try
            {
                var role = (from a in db.ATH001
                            join b in db.ATH004 on a.Role equals b.Id
                            where a.Employee.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                            select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();

                if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.PLNG3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Initiator.GetStringValue();
                }
                else if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PLNG2.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.PLNG2.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Approver.GetStringValue();
                }
                else if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PLNG1.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.PLNG1.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Approver.GetStringValue();
                }
            }
            catch
            {

            }
            return objUserRoleAccessDetails;
        }

        public List<FKM202> GetTreeForSelectedNode(List<FKM202> lstTree)
        {
            List<FKM202> objTree = new List<FKM202>();

            lstTree.ForEach(x =>
                objTree.Add(new FKM202
                {
                    NodeId = x.NodeId,
                    HeaderId = x.HeaderId,
                    ParentNodeId = x.ParentNodeId,
                    NodeTypeId = x.NodeTypeId,
                    CreatedBy = x.CreatedBy,
                    CreatedOn = x.CreatedOn,
                    EditedBy = x.EditedBy,
                    EditedOn = x.EditedOn,
                    NodeName = x.NodeName,
                    Project = x.Project,
                    FindNo = x.FindNo,
                    PartLevel = x.PartLevel,
                    Description = x.Description,
                    ItemGroup = x.ItemGroup,
                    RevisionNo = x.RevisionNo,
                    MaterialSpecification = x.MaterialSpecification,
                    MaterialDescription = x.MaterialDescription,
                    MaterialCode = x.MaterialCode,
                    ProductType = x.ProductType,
                    Length = x.Length,
                    Width = x.Width,
                    NoOfPieces = x.NoOfPieces,
                    Weight = x.Weight,
                    Thickness = x.Thickness,
                    UOM = x.UOM,
                    RequiredDate = x.RequiredDate,
                    NodeKey = x.NodeKey,
                    IsInsertedInPLM = x.IsInsertedInPLM,
                    PLMError = x.PLMError,
                    DeleverTo = x.DeleverTo,
                    DeleverStatus = x.DeleverStatus,
                    FindNo_Init = x.FindNo_Init,
                    NodeKey_Init = x.NodeKey_Init,
                    NoOfPieces_Init = x.NoOfPieces_Init,
                    ParentNodeId_Init = x.ParentNodeId_Init,
                    TASKUNIQUEID = x.TASKUNIQUEID,
                    KitStatus = x.KitStatus,
                    MaterialStatus = x.MaterialStatus,
                    PlannerStructure = x.PlannerStructure
                })
            );

            return objTree;
        }

        public bool InsertInToPLM(int HeaderId, int NodeId, int AutoGeneratedFindNo, FKM202 chkequipmentid, int assid, string projid, string status, int iscopy = 0)
        {
            bool key = false;
            var objFKM202Main = db.FKM202.FirstOrDefault(f => f.NodeId == NodeId);
            var lstNodeData = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Models.NodeData>>(objFKM202Main.PlannerStructure);//.OrderBy(o=> o.name).ToList();

            var DefaultLevel = new string[] { clsImplementationEnum.FKMSNodeType.Project.GetStringValue(), clsImplementationEnum.FKMSNodeType.LooseItem.GetStringValue(), clsImplementationEnum.FKMSNodeType.SpareItem.GetStringValue(), clsImplementationEnum.FKMSNodeType.Template.GetStringValue(), clsImplementationEnum.FKMSNodeType.Equipment.GetStringValue() };
            var DefaultLevelId = db.FKM203.Where(x => DefaultLevel.Contains(x.NodeType)).Select(x => x.Id).ToList();
            int GenerateNo = 0, PWHTSrNo =0, SectionSrNo = 0;
            List<FKMSBOMEnt> listFKMSBOMEnt = new List<FKMSBOMEnt>();

            foreach (var project in lstNodeData.Where(w => w.parent == 0))
            {
                foreach (var Equipment in lstNodeData.Where(w => w.parent == project.key)) {
                    List<FKMSBOMEnt> lstFKMSBOMEntByEquipment = new List<FKMSBOMEnt>();
                    GetFKMSBOMEntByNodeData(lstNodeData, Equipment, assid, projid, ref PWHTSrNo,ref SectionSrNo, ref GenerateNo, ref AutoGeneratedFindNo, ref lstFKMSBOMEntByEquipment);
                    listFKMSBOMEnt = listFKMSBOMEnt.Concat(lstFKMSBOMEntByEquipment).ToList();
                }
            }

            if (listFKMSBOMEnt.Count > 0)
            {
                context = new IEMQSEntitiesContext();
                using (DbContextTransaction dbTran = context.Database.BeginTransaction())
                {
                    try
                    {
                        if (status.ToLower() == clsImplementationEnum.CTQStatus.DRAFT.GetStringValue().ToLower() || status.ToLower() == clsImplementationEnum.CTQStatus.Returned.GetStringValue().ToLower())
                        {
                            var assem = clsImplementationEnum.FKMSNodeType.Assembly.GetStringValue().ToLower();
                            int AssemblyId = context.FKM203.Where(x => x.NodeType.ToLower() == assem).FirstOrDefault().Id;
                            var delFKM202 = context.FKM202.Where(i => i.HeaderId == HeaderId && i.NodeTypeId == AssemblyId).ToList();

                            if (delFKM202.Count() > 0)
                            {
                                foreach (var item in delFKM202)
                                {
                                    context.Entry(item).State = EntityState.Deleted;
                                    InsertNodesInFKM125(context, item, "InsertInToPLM"); //Task Id:- 20875
                                }
                                context.FKM202.RemoveRange(delFKM202);
                                context.SaveChanges();
                            }
                        }

                        #region Insert Nodes and Copy Async function

                        //Task<int> copyPLMtask1 = LongRunningPLMAllocationAsync(listFKMSBOMEnt, HeaderId, status, iscopy);
                        foreach (var bom in listFKMSBOMEnt)
                        {
                            FKM202 objFKM202 = null;
                            if (status.ToLower() == clsImplementationEnum.CTQStatus.Approved.GetStringValue().ToLower())
                            {
                                InsertPartAndBOMInPLM(bom, HeaderId, status, objFKM202, iscopy);
                            }
                            else
                            {
                                InsertNodeInFKMS(bom, HeaderId, status, objFKM202, iscopy);
                            }
                        }
                        #endregion
                        dbTran.Commit();
                    }
                    catch (DbEntityValidationException dex)
                    {
                        Elmah.ErrorSignal.FromCurrentContext().Raise(dex);
                        dbTran.Rollback();
                        throw dex;
                    }
                    catch (Exception ex)
                    {
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        dbTran.Rollback();
                        throw ex;
                    }
                }

                key = !db.FKM202.Any(i => i.HeaderId == HeaderId && i.IsInsertedInPLM != null && i.IsInsertedInPLM == false);
            }
            return key;
        }
        
        private void GetFKMSBOMEntByNodeData(List<Models.NodeData> lstNodeData, Models.NodeData objNodeData,int assid,string projid, ref int PWHTSrNo, ref int SectionSrNo, ref int GenerateNo, ref int AutoGeneratedFindNo,ref List<FKMSBOMEnt> lstFKMSBOMEnt)
        {
            var parentFKMSBOMEnt = lstFKMSBOMEnt.LastOrDefault();
            foreach (var item in lstNodeData.Where(w=>w.parent == objNodeData.key))
            {
                FKMSBOMEnt bomEnt = null;
                FKMSPartEnt partSectionEnt = null;
                AutoGeneratedFindNo = GetFindNo(AutoGeneratedFindNo);

                var fkm203 = db.FKM203.FirstOrDefault(f=>f.Id == item.level);
                if(fkm203.NodeType == FKMSNodeType.PWHTSection.GetStringValue())
                {
                    partSectionEnt = GeneratePartDetails(FKMSNodeType.PWHTSection, FKMSNodeType.PWHTSection, assid, projid,  0, "", ++PWHTSrNo);
                }
                if (fkm203.NodeType == "Section")
                {
                    partSectionEnt = GeneratePartDetails(FKMSNodeType.Section, FKMSNodeType.Section, assid, projid, 0, "", ++SectionSrNo);
                }
                if (fkm203.NodeType == FKMSNodeType.Assembly.GetStringValue())
                {
                    partSectionEnt = GeneratePartDetails(FKMSNodeType.Assembly, FKMSNodeType.Assembly, assid, projid, ++GenerateNo, item.name);
                }
                if (fkm203.NodeType == "SubAssembly")// FKMSNodeType.SubAssembly.GetStringValue())
                {
                    #region SubAssembly
                    if (item.name == "Internal assly")//FKMSNodeType.InternalAssembly.GetStringValue())
                    {
                        partSectionEnt = GeneratePartDetails(FKMSNodeType.InternalAssembly, FKMSNodeType.InternalAssembly, assid, projid, ++GenerateNo, objNodeData.name);
                    }
                    else if (item.name == "External assly")//FKMSNodeType.ExternalAssembly.GetStringValue())
                    {
                        partSectionEnt = GeneratePartDetails(FKMSNodeType.ExternalAssembly, FKMSNodeType.ExternalAssembly, assid, projid, ++GenerateNo, objNodeData.name);
                    }
                    else if (item.name == "Nozzle assly")// FKMSNodeType.NozzelAssembly.GetStringValue())
                    {
                        partSectionEnt = GeneratePartDetails(FKMSNodeType.NozzelAssembly, FKMSNodeType.NozzelAssembly, assid, projid, ++GenerateNo, objNodeData.name);
                    }
                    else
                    {
                        partSectionEnt = GeneratePartDetails(FKMSNodeType.SubAssembly, FKMSNodeType.SubAssembly, assid, projid, ++GenerateNo, item.name);
                    }
                    #endregion
                }

                #region Section
                if (partSectionEnt != null)
                {
                    var parentNodeName = "";
                    if (parentFKMSBOMEnt != null)
                        parentNodeName = parentFKMSBOMEnt.childPartName;
                    else
                        parentNodeName = db.FKM202.Where(x => x.NodeId == item.parent).Select(x => x.NodeKey).FirstOrDefault();

                    bomEnt = GenerateBOMDetails(partSectionEnt, parentNodeName, AutoGeneratedFindNo);
                    //if (partSectionEnt != null) { listFKMSPartEnt.Add(partSectionEnt); }
                    if (bomEnt != null) { lstFKMSBOMEnt.Add(bomEnt); }
                    AutoGeneratedFindNo = AutoGeneratedFindNo + 1;
                }
                #endregion
                GetFKMSBOMEntByNodeData(lstNodeData, item, assid, projid, ref PWHTSrNo, ref SectionSrNo, ref GenerateNo, ref AutoGeneratedFindNo, ref lstFKMSBOMEnt);
            }
        }

        public async Task<int> LongRunningPLMAllocationAsync(List<FKMSBOMEnt> listFKMSBOMEnt, int HeaderId, string status, int iscopy = 0)
        {
            await Task.Run(() => PLMAllocationAsync(listFKMSBOMEnt, HeaderId, status, iscopy));
            return 1;
        }

        public void PLMAllocationAsync(List<FKMSBOMEnt> listFKMSBOMEnt, int HeaderId, string status, int iscopy = 0)
        {
            foreach (var bom in listFKMSBOMEnt)
            {
                FKM202 objFKM202 = null;
                if (status.ToLower() == clsImplementationEnum.CTQStatus.DRAFT.GetStringValue().ToLower())
                {
                    InsertNodeInFKMS(bom, HeaderId, status, objFKM202, iscopy);
                }
                else
                {
                    InsertPartAndBOMInPLM(bom, HeaderId, status, objFKM202, iscopy);
                }
            }
        }

        public string GetDocumentDetailsLink(string DocNo, string DocType)
        {
            string linkUrl = string.Empty;

            if (DocType == clsImplementationEnum.FKMSDocumentType.DIN.GetStringValue())
            {
                var objPDN002 = db.PDN002.Where(x => x.DocumentNo == DocNo).FirstOrDefault();
                var url = Manager.GenerateLink(objPDN002.Plan, objPDN002.DocumentNo, Convert.ToInt32(objPDN002.RefId), false, true);
                linkUrl += url;
            }
            else
            {
                var objHTR001 = db.HTR001.Where(x => x.HTRNo == DocNo).FirstOrDefault();
                linkUrl += WebsiteURL + "/HTR/Maintain/AddHeader?HeaderID=" + objHTR001.HeaderId;
            }
            return linkUrl;
        }

        public string checkdocument(string project, string userrole, string docno, string doctype, bool ForPrint = false)
        {
            string link = string.Empty;
            bool isapprover = false;
            List<PDN005> ChildsDept = null;
            if (userrole == UserRoleName.PLNG2.GetStringValue() || userrole == UserRoleName.PLNG1.GetStringValue())
            {
                isapprover = true;
            }
            ChildsDept = db.PDN005.Where(i => i.Employee == objClsLoginInfo.UserName).ToList();

            var ApprovedStatus = clsImplementationEnum.CommonStatus.Approved.GetStringValue();

            var CurrentPDINNo = db.PDN001.Where(i => i.Project == project).Max(i => i.IssueNo);

            List<PDINDocumentRevNoEnt> listPDINDocumentRevNoEnt = (from t in db.PDN002
                                                                   group t by new { t.Project, t.DocumentNo, t.Status, t.Plan }
                                                           into grp
                                                                   where grp.Key.Project == project && grp.Key.Status == ApprovedStatus
                                                                   select new PDINDocumentRevNoEnt
                                                                   {
                                                                       Plan = grp.Key.Plan,
                                                                       Project = grp.Key.Project,
                                                                       DocumentNo = grp.Key.DocumentNo,
                                                                       RevNo = grp.Max(t => t.RevNo)
                                                                   }
                                        ).ToList();

            var objPDN002CurentIssueDtl = (from p1 in db.PDN001
                                           join p2 in db.PDN002 on p1.HeaderId equals p2.HeaderId
                                           where p1.Project.Trim() == project.Trim() && p1.IssueNo == p2.IssueNo
                                           && p1.IssueNo == CurrentPDINNo
                                           select new { p2.LineId, p2.IsApplicable, p2.Status, p2.DocumentNo, p2.RevNo, p2.TargetDate, p2.IssueStatus, p2.Department }
                                 ).ToList();

            var objLastApprovedRevDtl = listPDINDocumentRevNoEnt.Where(i => i.DocumentNo == docno).FirstOrDefault();

            var objPDN002 = (from p1 in db.PDN001
                             join p2 in db.PDN002 on p1.HeaderId equals p2.HeaderId
                             join c3 in db.COM003 on p1.IssueBy equals c3.t_psno
                             where p1.Project.Trim() == project.Trim() && c3.t_actv == 1
                             select new { p1.Project, p1.IssueNo, p1.IssueBy, p1.IssueDate, p2.DocumentNo, p2.Description, p2.RevNo, c3.t_init, p2.Department, p2.RefHistoryId, p2.Plan, p1.Status, DocumentStatus = p2.Status, RefId = p2.RefId }
                                  ).ToList();

            var objLine = objPDN002CurentIssueDtl.Where(i => i.DocumentNo == docno).FirstOrDefault();

            bool Applicable = ((objLine == null || objLine.IsApplicable == null) ? true : Convert.ToBoolean(objLine.IsApplicable));
            if (userrole == UserRoleName.SHOP.GetStringValue() && doctype == clsImplementationEnum.FKMSDocumentType.DIN.GetStringValue())
            {

                var arrayDepartmenttemp = new string[0];
                if (!string.IsNullOrEmpty(objLine.Department))
                    arrayDepartmenttemp = objLine.Department.Split(',');

                if (objLastApprovedRevDtl != null && (arrayDepartmenttemp.Contains(objClsLoginInfo.Department) || ChildsDept.Any(i => arrayDepartmenttemp.Contains(i.ChildDeptCode))))
                {
                    var refHistoryId = objPDN002.Where(i => i.RevNo == objLastApprovedRevDtl.RevNo && i.DocumentNo == docno.ToString() && i.DocumentStatus == ApprovedStatus).FirstOrDefault().RefHistoryId;
                    var RefId = objPDN002.Where(i => i.RevNo == objLastApprovedRevDtl.RevNo && i.DocumentNo == docno.ToString() && i.DocumentStatus == ApprovedStatus).FirstOrDefault().RefId;
                    link += "<a onclick=\"ShopUserAction('" + objLastApprovedRevDtl.Plan + "'," + refHistoryId + "," + objLastApprovedRevDtl.RevNo + ",'" + docno + "'," + RefId + ")\">" + docno + "</a>";// + " - R" + objLine?.RevNo.ToString();                       
                }
                else
                {
                    link += docno;
                }
            }
            else
            {
                //If Function called from Print PDIN then no need to display link. Display only text
                if (ForPrint == true)
                {
                    link += docno + " - R" + objLine?.RevNo.ToString();
                }
                else
                {
                    //If not shop user and Document is Applicable then set link for Document No other wise display only Document No
                    string DocumentwithRev = docno + " - R" + objLine?.RevNo;
                    if (objLastApprovedRevDtl != null)
                    {
                        if (objLastApprovedRevDtl.Plan == clsImplementationEnum.PlanList.Learning_Capture.GetStringValue())
                            DocumentwithRev = docno;
                        var RefId = objPDN002.Where(i => i.RevNo == objLastApprovedRevDtl.RevNo && i.DocumentNo == docno.ToString() && i.DocumentStatus == ApprovedStatus).FirstOrDefault().RefId;

                        link += (Applicable ? Manager.GenerateLink(objLastApprovedRevDtl.Plan, DocumentwithRev, Convert.ToInt32(RefId), isapprover, false, true) : docno);
                    }
                    else
                    {
                        link += DocumentwithRev;
                    }
                }
            }
            return link;
        }

        public string checkdocumentsummery(string project, string docno, string doctype)
        {
            string link = string.Empty;
            bool isapprover = false;

            var ApprovedStatus = clsImplementationEnum.CommonStatus.Approved.GetStringValue();

            var CurrentPDINNo = 0;
            if (db.PDN001.Any(i => i.Project == project))
            {
                CurrentPDINNo = db.PDN001.Where(i => i.Project == project).Max(i => i.IssueNo);
            }

            List<PDINDocumentRevNoEnt> listPDINDocumentRevNoEnt = (from t in db.PDN002
                                                                   group t by new { t.Project, t.DocumentNo, t.Status, t.Plan }
                                                           into grp
                                                                   where grp.Key.Project == project && grp.Key.Status == ApprovedStatus
                                                                   select new PDINDocumentRevNoEnt
                                                                   {
                                                                       Plan = grp.Key.Plan,
                                                                       Project = grp.Key.Project,
                                                                       DocumentNo = grp.Key.DocumentNo,
                                                                       RevNo = grp.Max(t => t.RevNo)
                                                                   }
                                        ).ToList();

            var objPDN002CurentIssueDtl = (from p1 in db.PDN001
                                           join p2 in db.PDN002 on p1.HeaderId equals p2.HeaderId
                                           where p1.Project.Trim() == project.Trim() && p1.IssueNo == p2.IssueNo
                                           && p1.IssueNo == CurrentPDINNo
                                           select new { p2.LineId, p2.IsApplicable, p2.Status, p2.DocumentNo, p2.RevNo, p2.TargetDate, p2.IssueStatus, p2.Department }
                                 ).ToList();

            var objLastApprovedRevDtl = listPDINDocumentRevNoEnt.Where(i => i.DocumentNo == docno).FirstOrDefault();

            var objPDN002 = (from p1 in db.PDN001
                             join p2 in db.PDN002 on p1.HeaderId equals p2.HeaderId
                             join c3 in db.COM003 on p1.IssueBy equals c3.t_psno
                             where p1.Project.Trim() == project.Trim() && c3.t_actv == 1
                             select new { p1.Project, p1.IssueNo, p1.IssueBy, p1.IssueDate, p2.DocumentNo, p2.Description, p2.RevNo, c3.t_init, p2.Department, p2.RefHistoryId, p2.Plan, p1.Status, DocumentStatus = p2.Status, RefId = p2.RefId }
                                  ).ToList();

            var objLine = objPDN002CurentIssueDtl.Where(i => i.DocumentNo == docno).FirstOrDefault();

            bool Applicable = ((objLine == null || objLine.IsApplicable == null) ? true : Convert.ToBoolean(objLine.IsApplicable));
            if (doctype == clsImplementationEnum.FKMSDocumentType.DIN.GetStringValue())
            {
                var arrayDepartmenttemp = new string[0];
                if (!string.IsNullOrEmpty(objLine.Department))
                    arrayDepartmenttemp = objLine.Department.Split(',');

                if (objLastApprovedRevDtl != null)
                {
                    var refHistoryId = objPDN002.Where(i => i.RevNo == objLastApprovedRevDtl.RevNo && i.DocumentNo == docno.ToString() && i.DocumentStatus == ApprovedStatus).FirstOrDefault().RefHistoryId;
                    var RefId = objPDN002.Where(i => i.RevNo == objLastApprovedRevDtl.RevNo && i.DocumentNo == docno.ToString() && i.DocumentStatus == ApprovedStatus).FirstOrDefault().RefId;
                    link += "<a onclick=\"ShopUserAction('" + objLastApprovedRevDtl.Plan + "'," + refHistoryId + "," + objLastApprovedRevDtl.RevNo + ",'" + docno + "'," + RefId + ")\">" + docno + "</a>";// + " - R" + objLine?.RevNo.ToString();                       
                }
                else
                {
                    link += docno;
                }
            }
            else
            {
                //If not shop user and Document is Applicable then set link for Document No other wise display only Document No
                string DocumentwithRev = docno + " - R" + objLine?.RevNo;
                if (objLastApprovedRevDtl != null)
                {
                    if (objLastApprovedRevDtl.Plan == clsImplementationEnum.PlanList.Learning_Capture.GetStringValue())
                        DocumentwithRev = docno;
                    var RefId = objPDN002.Where(i => i.RevNo == objLastApprovedRevDtl.RevNo && i.DocumentNo == docno.ToString() && i.DocumentStatus == ApprovedStatus).FirstOrDefault().RefId;

                    link += (Applicable ? Manager.GenerateLink(objLastApprovedRevDtl.Plan, DocumentwithRev, Convert.ToInt32(RefId), isapprover, false, true) : docno);
                }
                else if (DocumentwithRev == " - R")
                {
                }
                else
                {
                    link += DocumentwithRev;
                }
            }
            return link;
        }

        #endregion

        // GET: FKMS/MaintainFKMS
        #region Maintain FKMS
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Index()
        {
            ViewBag.IsDisplayOnly = false;
            ViewBag.Title = "Maintain H-BOM Template";
            ViewBag.IndexType = clsImplementationEnum.WPPIndexType.maintain.GetStringValue();
            return View();
        }

        [SessionExpireFilter, UserPermissions, AllowAnonymous] 
        public ActionResult ApproverIndex()
        {
            ViewBag.IsDisplayOnly = false;
            ViewBag.Title = "Approve H-BOM Template";
            ViewBag.IndexType = clsImplementationEnum.WPPIndexType.approve.GetStringValue();
            return View("Index");
        }

        [SessionExpireFilter, UserPermissions]
        public ActionResult DisplayFKMS()
        {
            ViewBag.IsDisplayOnly = true;
            return View("Index");
        }

        [HttpPost]
        public ActionResult GetHeaderGridDataPartial(string status, string title, string indextype, bool IsDisplayOnly = false)
        {
            ViewBag.IsDisplayOnly = IsDisplayOnly;
            ViewBag.Status = status;
            ViewBag.GridTitle = title;
            ViewBag.IndexDataFor = indextype;

            return PartialView("_GetHeaderGridDataPartial");
            // for get_index_by_role_header_details
            //return PartialView("_GetIndexByRoleHeaderDetailsPartial");
        }

        public ActionResult LoadFKMSHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string status = param.Status;
                var IsDisplayOnly = !string.IsNullOrEmpty(Request["IsDisplayOnly"]) ? Convert.ToBoolean(Request["IsDisplayOnly"].ToString()) : false;
                string whereCondition = "1=1";

                string indextype = param.Department;
                if (string.IsNullOrWhiteSpace(indextype))
                {
                    indextype = clsImplementationEnum.WPPIndexType.maintain.GetStringValue();
                }

                if (status.ToLower() == "pending")
                {
                    if (indextype == clsImplementationEnum.WPPIndexType.maintain.GetStringValue())
                    {
                        whereCondition += " and status in ('" + clsImplementationEnum.PAMStatus.Returned.GetStringValue() + "','" + clsImplementationEnum.PAMStatus.Draft.GetStringValue() + "')";
                    }
                    else
                    {
                        whereCondition += " and status in ('" + clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue() + "')";
                    }
                }

                string[] columnName = { "Project", "Customer", "ZeroDate", "CDD", "Status", "RevNo", "CreatedBy", "CONVERT(nvarchar(20),CreatedOn,103)" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstPam = db.SP_FKM_GETHEADERDETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from h in lstPam
                           select new[] {
                               Convert.ToString(h.HeaderId),
                               Convert.ToString(h.Project),
                               Convert.ToString(h.Customer),
                               Convert.ToString(h.ZeroDate),
                               Convert.ToString(h.CDD),
                               Convert.ToString(h.RevNo),
                               Convert.ToString(h.Status),
                               Convert.ToString(h.CreatedBy),
                               Convert.ToString(h.CreatedOn),
                               "<center>"+ (IsDisplayOnly ? Helper.GenerateActionIcon(h.HeaderId, "View", "View Detail", "fa fa-eye", "", WebsiteURL + "/FKM/MaintainFKMS/FKMSDisplayDetails/"+h.HeaderId +"?urlForm="+indextype,false) : Helper.GenerateActionIcon(h.HeaderId, "View", "View Detail", "fa fa-eye", "", WebsiteURL + "/FKM/MaintainFKMS/FKMSDetails/"+h.HeaderId +"?urlForm="+indextype,false)) +"</center>"
                    }).ToList();
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]
        public ActionResult Details(int id = 0, string urlForm = "")
        {
            var objFKM201 = new FKM201();
            ViewBag.IsDisplayOnly = false;
            ViewBag.path = ConfigurationManager.AppSettings["File_Upload_URL"];
            List<FKM203> objFKM203 = db.FKM203.ToList();
            UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;
            ViewBag.HeaderId = id;

            if (id > 0)
            {
                var lstFKM202 = db.FKM202.Where(x => x.HeaderId == id).ToList();
                objFKM201 = db.FKM201.FirstOrDefault(x => x.HeaderId == id);
                ViewBag.ZeroDate = objFKM201.ZeroDate.Value.ToShortDateString();
                ViewBag.IsCopy = lstFKM202.Any(x => x.PlannerStructure != null) != false ? "true" : "false";
                ViewBag.CDD = objFKM201.CDD.Value.ToShortDateString();
                // Task 16106 Product type get from PDN001 table
                ViewBag.ProductType = objFKM201.ProductType != null ? objFKM201.ProductType : "";
                var Files = (new clsFileUpload()).GetDocuments("FKM201/" + objFKM201.HeaderId + "/R" + objFKM201.RevNo);
                if (Files.Any())
                {
                    var filename = Files.FirstOrDefault().Name;
                    ViewBag.fileName = (filename.Length > 10) ? filename.Substring(0, 10) + "..." : filename;
                    ViewBag.fullFileName = filename;
                    ViewBag.image = Files.FirstOrDefault().URL;
                }
                // observation 16019 Default SOB Status Update Project Load
                UpdateReadyForKitStatusForAllNode(objFKM201.Project);

                //ViewBag.EquipmentId = db.FKM202.Where(x => x.HeaderId == objFKM201.HeaderId && x.NodeName.ToLower() == "BORL-Site Service".ToLower()).FirstOrDefault().NodeId;
                ViewBag.Project = Manager.GetProjectAndDescription(objFKM201.Project);
                ViewBag.Customer = db.Database.SqlQuery<string>(@"select d.t_bpid +'-' + d.t_nama as [Customer] from " + LNLinkedServer + ".dbo.ttccom100175 d where d.t_bpid = '" + objFKM201.Customer + "'").FirstOrDefault();
                if (!String.IsNullOrWhiteSpace(objFKM201.ApprovedBy))
                    ViewBag.ApprovedBy = objFKM201.ApprovedBy + " - " + Manager.GetUserNameFromPsNo(objFKM201.ApprovedBy);
                //int SectionId = objFKM203.Where(x => x.NodeType.ToLower() == clsImplementationEnum.FKMSNodeType.Section.GetStringValue().ToLower()).FirstOrDefault().Id;
                int ProjectId = objFKM203.FirstOrDefault(x => x.NodeType.ToLower() == clsImplementationEnum.FKMSNodeType.Project.GetStringValue().ToLower()).Id;
                var ProjectNode = lstFKM202.FirstOrDefault(x => x.NodeTypeId == ProjectId && x.PlannerStructure != null);
                if (ProjectNode != null)
                    ViewBag.NodeData = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Models.NodeData>>(ProjectNode.PlannerStructure);
                else
                    ViewBag.NodeData = new List<Models.NodeData>();

                ViewBag.lstFKM202 = lstFKM202.Select(s => new { s.NodeId, s.IsInsertedInPLM, s.PLMError, s.FindNo }).ToList();

                //ViewBag.IsTreeGenerated = (objFKM201.Status != clsImplementationEnum.CTQStatus.Approved.GetStringValue() && ProjectNode != null) ? "true" : "false";
                ViewBag.IsRevert = lstFKM202.Any(x => x.NodeTypeId == ProjectId && x.PlannerStructure != null) ? "true" : "false";

                //objFKM201 = db.FKM201.FirstOrDefault(f => f.HeaderId == id);
                if (urlForm == clsImplementationEnum.WPPIndexType.approve.GetStringValue())
                {
                    //if (objFKM201.ApprovedBy != objClsLoginInfo.UserName && objClsLoginInfo.GetUserRoleList().Contains("PLNG2"))
                    // 16562 observation Access denied error
                    if ((!objClsLoginInfo.GetUserRoleList().Contains("PLNG1")) && (!objClsLoginInfo.GetUserRoleList().Contains("PLNG2")))
                        return new RedirectResult("~/Authentication/Authenticate/AccessDenied");
                    if (!(objFKM201.ApprovedBy != objClsLoginInfo.UserName))
                        ViewBag.buttonname = "approve";
                    ViewBag.backbutton = "backbutton";
                }
                else
                    ViewBag.buttonname = "maintain";

                ViewBag.NodeLevel = objFKM203.Select(s => new { s.Id, s.NodeType, s.NodeLevel });
            }
            else
            {
                ViewBag.buttonname = "maintain";
                objFKM201.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
            }

            return View("Details", objFKM201);
        }

        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        // for get_index_by_role_header_details
        //public ActionResult FKMSDetails(int? id, string urlForm = "", int nodeid = 0)
        public ActionResult FKMSDetails(int? id, string urlForm = "")
        {
            return Details(id==null? 0:id.Value , urlForm);
        }

        [SessionExpireFilter, AllowAnonymous] //  UserPermissions,
        public ActionResult FKMSDisplayDetails(int? id, string urlForm = "")
        {
            var viewData = Details(id == null ? 0 : id.Value, urlForm);
            ViewBag.IsDisplayOnly = true;
            return viewData;
        }

        [HttpPost]
        public ActionResult SaveHeader(FKM201 model, bool isDeleteEnabled, string PlannerStructure)
        {
            FKM201 objFKM201 = null;
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (model.HeaderId > 0)
                {
                    var DefaultLevel = new string[] { clsImplementationEnum.FKMSNodeType.Project.GetStringValue(), clsImplementationEnum.FKMSNodeType.LooseItem.GetStringValue(), clsImplementationEnum.FKMSNodeType.SpareItem.GetStringValue(), clsImplementationEnum.FKMSNodeType.Template.GetStringValue(), clsImplementationEnum.FKMSNodeType.Equipment.GetStringValue() };
                    var DefaultLevelId = db.FKM203.Where(x => DefaultLevel.Contains(x.NodeType)).Select(x => x.Id).ToList();
                    int PWHTSrNo = 0, SectionSrNo = 0, GenerateNo = 0, AutoGeneratedFindNo = StartFindNo-1;

                    objFKM201 = db.FKM201.Where(x => x.HeaderId == model.HeaderId).FirstOrDefault();
                    objFKM201.Project = model.Project;
                    objFKM201.Customer = model.Customer;
                    objFKM201.ZeroDate = model.ZeroDate;
                    objFKM201.CDD = model.CDD;
                    objFKM201.Status = model.Status;
                    objFKM201.ProductType = model.ProductType;
                    objFKM201.RevNo = model.RevNo;
                    if (!string.IsNullOrWhiteSpace(model.ApprovedBy))
                        objFKM201.ApprovedBy = model.ApprovedBy.Split('-')[0].Trim();
                    objFKM201.EditedBy = objClsLoginInfo.UserName;
                    objFKM201.EditedOn = DateTime.Now;
                    #region FKM202 Node Updates 
                    var lstNodeData = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Models.NodeData>>(PlannerStructure);//.OrderBy(o=> o.name).ToList();
                    foreach (var parent in lstNodeData.Where(w=>w.parent == 0|| w.parent == null))
                    {
                        parent.parent = 0;
                        AutoGeneratedFindNo++;
                        AddUpdateFKM202ByNode(ref DefaultLevelId, ref lstNodeData, parent, model.HeaderId, model.Project, ref PWHTSrNo, ref SectionSrNo, ref GenerateNo, ref AutoGeneratedFindNo);
                    }
                    
                    var lstNodeIDs = lstNodeData.Select(s => s.key);
                    var deletedFKM202 = db.FKM202.Where(w => w.HeaderId == model.HeaderId && !lstNodeIDs.Contains(w.NodeId));
                    db.FKM202.RemoveRange(deletedFKM202);
                    var objFKM203 = db.FKM203.FirstOrDefault(f => f.NodeLevel == 0);
                    var objFKM202 = db.FKM202.FirstOrDefault(f => f.HeaderId == model.HeaderId && f.NodeTypeId == objFKM203.Id);
                    objFKM202.PlannerStructure = Newtonsoft.Json.JsonConvert.SerializeObject(lstNodeData);
                    #endregion

                    db.SaveChanges();
                    //InsertInToPLM(model.HeaderId, objFKM202.NodeId, AutoGeneratedFindNo, null, assid, model.Project, model.Status,0);

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                    objResponseMsg.HeaderId = objFKM201.HeaderId;
                    //objResponseMsg.RequestId = db.FKM202.Where(x => x.HeaderId == objFKM201.HeaderId && x.NodeName.ToLower() == "Equipment".ToLower()).FirstOrDefault().NodeId;
                    objResponseMsg.RevNo = objFKM201.RevNo.ToString();
                }
                else
                {
                    objFKM201 = new FKM201();
                    objFKM201.Project = model.Project;
                    objFKM201.Customer = model.Customer;
                    objFKM201.ZeroDate = model.ZeroDate;
                    objFKM201.CDD = model.CDD;
                    objFKM201.RevNo = 0;
                    objFKM201.ProductType = model.ProductType;
                    if (!string.IsNullOrWhiteSpace(model.ApprovedBy))
                        objFKM201.ApprovedBy = model.ApprovedBy.Split('-')[0].Trim();
                    objFKM201.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                    objFKM201.CreatedBy = objClsLoginInfo.UserName;
                    objFKM201.CreatedOn = DateTime.Now;
                    objFKM201.PlannerPsno = objClsLoginInfo.UserName;

                    db.FKM201.Add(objFKM201);
                    db.SaveChanges();
                    
                    #region Add Default Entry FKM202
                    if (objFKM201.HeaderId > 0)
                    {
                        //DefaultEntry(objFKM201.HeaderId, objFKM201.Project);
                        DefaultEntryPLM(objFKM201.HeaderId, objFKM201.Project);
                    }
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                    objResponseMsg.HeaderId = objFKM201.HeaderId;
                    //objResponseMsg.RequestId = db.FKM202.Where(x => x.HeaderId == objFKM201.HeaderId && x.NodeName.ToLower() == "Equipment".ToLower()).FirstOrDefault().NodeId;
                    objResponseMsg.RevNo = objFKM201.RevNo.ToString();
                }
                if (isDeleteEnabled)
                {
                    var Files = (new clsFileUpload()).GetDocuments("FKM201/" + objFKM201.HeaderId + "/R" + objFKM201.RevNo);
                    foreach (var item in Files)
                    {
                        (new clsFileUpload()).DeleteFile("FKM201/" + objFKM201.HeaderId + "/R" + objFKM201.RevNo, item.Name);
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        private void AddUpdateFKM202ByNode(ref List<int> DefaultLevelId, ref List<Models.NodeData> lstNodeData, Models.NodeData objNodeData, int HeaderId, string Project,ref int PWHTSrNo, ref int SectionSrNo, ref int GenerateNo, ref int AutoGeneratedFindNo)
        {
            AutoGeneratedFindNo = GetFindNo(AutoGeneratedFindNo);
            FKMSPartEnt partSectionEnt = null;
            foreach (var item in lstNodeData.Where(w => w.parent == objNodeData.key))
            {
                var findNo = AutoGeneratedFindNo.ToString();
                ++AutoGeneratedFindNo;
                FKM202 objFKM202 = db.FKM202.FirstOrDefault(f => f.HeaderId == HeaderId && f.NodeId == item.key);
                var IsNew = objFKM202 == null;
                if (!IsNew && DefaultLevelId.Contains(objNodeData.level))
                {
                    AddUpdateFKM202ByNode(ref DefaultLevelId, ref lstNodeData, item, HeaderId, Project, ref PWHTSrNo, ref SectionSrNo, ref GenerateNo, ref AutoGeneratedFindNo);
                    continue;
                }
                #region Get PartName
                var fkm203 = db.FKM203.FirstOrDefault(f => f.Id == item.level);
                if (fkm203.NodeType == FKMSNodeType.PWHTSection.GetStringValue())
                {
                    partSectionEnt = GeneratePartDetails(FKMSNodeType.PWHTSection, FKMSNodeType.PWHTSection, 0, Project, 0, "", ++PWHTSrNo);
                }
                if (fkm203.NodeType == "Section")
                {
                    partSectionEnt = GeneratePartDetails(FKMSNodeType.Section, FKMSNodeType.Section, 0, Project, 0, "", ++SectionSrNo);
                }
                if (fkm203.NodeType == FKMSNodeType.Assembly.GetStringValue())
                {
                    partSectionEnt = GeneratePartDetails(FKMSNodeType.Assembly, FKMSNodeType.Assembly, 0, Project, ++GenerateNo, item.name);
                }
                if (fkm203.NodeType == "SubAssembly")// FKMSNodeType.SubAssembly.GetStringValue())
                {
                    #region SubAssembly
                    if (item.name == "Internal assly")//FKMSNodeType.InternalAssembly.GetStringValue())
                    {
                        partSectionEnt = GeneratePartDetails(FKMSNodeType.InternalAssembly, FKMSNodeType.InternalAssembly, 0, Project, ++GenerateNo, objNodeData.name);
                    }
                    else if (item.name == "External assly")//FKMSNodeType.ExternalAssembly.GetStringValue())
                    {
                        partSectionEnt = GeneratePartDetails(FKMSNodeType.ExternalAssembly, FKMSNodeType.ExternalAssembly, 0, Project, ++GenerateNo, objNodeData.name);
                    }
                    else if (item.name == "Nozzle assly")// FKMSNodeType.NozzelAssembly.GetStringValue())
                    {
                        partSectionEnt = GeneratePartDetails(FKMSNodeType.NozzelAssembly, FKMSNodeType.NozzelAssembly, 0, Project, ++GenerateNo, objNodeData.name);
                    }
                    else
                    {
                        partSectionEnt = GeneratePartDetails(FKMSNodeType.SubAssembly, FKMSNodeType.SubAssembly, 0, Project, ++GenerateNo, item.name);
                    }
                    #endregion
                }
                #endregion
                int? ParentNodeId = 0; 
                if (item.parent > 0)
                    ParentNodeId = item.parent; 
                //else if (objNodeData.parent != null)
                //    ParentNodeId = AddUpdateFKM202ByNode(ref lstNodeData, lstNodeData.FirstOrDefault(f => f.key == objNodeData.parent), HeaderId, Project, ref PWHTSrNo, ref GenerateNo, ref AutoGeneratedFindNo).NodeId;


                if (IsNew)
                {
                    objFKM202 = new FKM202();
                    objFKM202.HeaderId = HeaderId;
                    //objFKM202.NodeTypeId = item.level;// bomPart.part.NodeTypeId;
                    objFKM202.CreatedBy = objClsLoginInfo.UserName;
                    objFKM202.CreatedOn = DateTime.Now;
                    objFKM202.NodeName = item.name;// bomPart.description;
                    objFKM202.Project = Project;
                    objFKM202.ProductType = clsImplementationEnum.NodeTypes.ASM.GetStringValue();
                    //objFKM202.ParentNodeId = objFKM202.ParentNodeId_Init = item.parent;
                    //objFKM202.NodeKey_Init = partSectionEnt.partName;// Keyfindno;
                    objFKM202.NoOfPieces = objFKM202.NoOfPieces_Init = Convert.ToInt32(_defaultNumberOfPieces);
                    objFKM202.IsPWHT = partSectionEnt.IsPWHT;
                    //objFKM202.FindNo_Init = AutoGeneratedFindNo.ToString();
                    //objFKM202.ParentNodeId_Init = ParentNodeId;
                    objFKM202.NodeTypeId = item.level;// NodeTypeId;
                }
                else
                {
                    objFKM202.EditedBy = objClsLoginInfo.UserName;
                    objFKM202.EditedOn = DateTime.Now;
                }
                objFKM202.ParentNodeId = objFKM202.ParentNodeId_Init = ParentNodeId;
                objFKM202.NodeKey = objFKM202.NodeKey_Init = partSectionEnt.partName;
                objFKM202.FindNo = objFKM202.FindNo_Init = findNo;

                objFKM202.NodeName = item.name;
                if (IsNew)
                    db.FKM202.Add(objFKM202);
                db.SaveChanges();
                if (IsNew)
                {
                    foreach (var item2 in lstNodeData.Where(w => w.parent == item.key))
                    {
                        item2.parent = objFKM202.NodeId;
                        //AddUpdateFKM202ByNode(ref nodeData, item2, HeaderId, Project, ref PWHTSrNo, ref GenerateNo, ref AutoGeneratedFindNo);
                    }
                    item.key = objFKM202.NodeId;
                }
                AddUpdateFKM202ByNode(ref DefaultLevelId, ref lstNodeData, item, HeaderId, Project, ref PWHTSrNo, ref SectionSrNo, ref GenerateNo, ref AutoGeneratedFindNo);
            }
        }

        [HttpPost]
        public ActionResult SendForApprove(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            FKM201 objFKM201 = new FKM201();
            try
            {
                if (headerId > 0)
                {
                    objFKM201 = db.FKM201.Where(i => i.HeaderId == headerId).FirstOrDefault();

                    objFKM201.Status = clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue();
                    objFKM201.SubmittedBy = objClsLoginInfo.UserName;
                    objFKM201.SubmittedOn = DateTime.Now;
                    objFKM201.ReturnRemark = null;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "fullkit successfully Send For Approval";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg);
        }

        [HttpPost]
        public ActionResult ApproveFKMS(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            FKM201 objFKM201 = new FKM201();
            try
            {
                if (headerId > 0)
                {
                    FKM202 objFKM202 = db.FKM202.Where(x => x.HeaderId == headerId && x.PlannerStructure != null).FirstOrDefault();
                    if (objFKM202 != null)
                    {
                        objFKM201 = db.FKM201.Where(i => i.HeaderId == headerId).FirstOrDefault();
                        objFKM201.Status = clsImplementationEnum.CTQStatus.Approved.GetStringValue();
                        objFKM201.ApprovedBy = objClsLoginInfo.UserName;
                        objFKM201.ApprovedOn = DateTime.Now;

                        #region Insert Into PLM
                        string assemblyType = clsImplementationEnum.FKMSNodeType.Assembly.GetStringValue();
                        var assid = db.FKM203.Where(x => x.NodeType.ToLower() == assemblyType.ToLower()).Select(x => x.Id).FirstOrDefault();
                        bool isSuccess = InsertInToPLM(headerId, objFKM202.NodeId, StartFindNo, objFKM202, assid, objFKM201.Project, objFKM201.Status, 1);
                        #endregion

                        //if (isSuccess)
                        //{
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "fullkit successfully approved";
                        //}
                        //else
                        //{
                        //    objResponseMsg.Key = false;
                        //    objResponseMsg.Value = "Something went wrong into PLM insertion...!";
                        //}
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data found...!";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg);
        }

        [HttpPost]
        public ActionResult ReturnFKMS(int headerId, string remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            FKM201 objFKM201 = new FKM201();
            try
            {
                if (headerId > 0)
                {
                    objFKM201 = db.FKM201.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    objFKM201.ReturnRemark = remarks;
                    objFKM201.Status = clsImplementationEnum.CTQStatus.Returned.GetStringValue();
                    objFKM201.EditedBy = objClsLoginInfo.UserName;
                    objFKM201.EditedOn = DateTime.Now;

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "fullkit successfully return";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg);
        }

        public ActionResult CreateNewNode(FKM202 objFKM202, int ParentNodeTypeId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (objFKM202.NodeId == 0)
                {

                    int Keyfindno = Manager.GetMaxFindNo(Convert.ToString(objFKM202.HeaderId));
                    FKM202 newFKM202 = new FKM202();
                    newFKM202.HeaderId = objFKM202.HeaderId;
                    newFKM202.CreatedBy = objClsLoginInfo.UserName;
                    newFKM202.CreatedOn = DateTime.Now;
                    newFKM202.NodeName = objFKM202.NodeName;
                    newFKM202.Project = objFKM202.Project;
                    newFKM202.ParentNodeId = objFKM202.ParentNodeId;
                    newFKM202.ParentNodeId_Init = objFKM202.ParentNodeId;
                    newFKM202.NoOfPieces = Convert.ToInt32(_defaultNumberOfPieces);
                    newFKM202.NoOfPieces_Init = Convert.ToInt32(_defaultNumberOfPieces);
                    newFKM202.FindNo = Convert.ToString(Keyfindno);
                    newFKM202.FindNo_Init = Convert.ToString(Keyfindno);
                    newFKM202.NodeKey = Convert.ToString(Keyfindno);
                    newFKM202.NodeKey_Init = Convert.ToString(Keyfindno);
                    newFKM202.ProductType = clsImplementationEnum.NodeTypes.ASM.GetStringValue();
                    var objParentNodeTypeId = db.FKM203.Where(i => i.Id == ParentNodeTypeId).FirstOrDefault();
                    int NodeTypeId = 7;
                    if (objParentNodeTypeId != null)
                    {
                        var objNodelevel = db.FKM203.Where(i => i.NodeLevel > objParentNodeTypeId.NodeLevel).FirstOrDefault();
                        NodeTypeId = objNodelevel != null ? objNodelevel.Id : 7;
                    }
                    newFKM202.NodeTypeId = NodeTypeId;

                    db.FKM202.Add(newFKM202);
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = newFKM202.NodeId;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateNewNode(int ParentNodeId, string NodeText)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (ParentNodeId != 0 && NodeText != null)
                {
                    var updateFKM202 = db.FKM202.Where(x => x.NodeId == ParentNodeId).FirstOrDefault();
                    //NodeText = NodeText.Replace("(QTY :" + updateFKM202.NoOfPieces_Init + "," + (updateFKM202.UOM != null ? updateFKM202.UOM : "-") + ")", "");

                    updateFKM202.EditedBy = objClsLoginInfo.UserName;
                    updateFKM202.EditedOn = DateTime.Now;
                    updateFKM202.NodeName = NodeText;
                    db.Entry(updateFKM202).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetTreeNodeName(int NodeId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (NodeId != 0)
                {
                    var updateFKM202 = db.FKM202.Where(x => x.NodeId == NodeId).FirstOrDefault();
                    objResponseMsg.Key = true;
                    objResponseMsg.item = updateFKM202.NodeName;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteNewNode(int DeleteNodeid, int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (DeleteNodeid != 0)
                {
                    listFKM202 = db.FKM202.Where(i => i.HeaderId == HeaderId).ToList();
                    listDeleteTree = new List<FKM202>();
                    var listParentNode = listFKM202.Where(i => i.NodeId == DeleteNodeid).FirstOrDefault();

                    var listChildNode = listFKM202.Where(x => x.ParentNodeId == DeleteNodeid).ToList();

                    if (listParentNode != null)
                    {
                        objResponseMsg.HeaderId = listParentNode.ParentNodeId != null ? Convert.ToInt32(listParentNode.ParentNodeId) : 0;
                        foreach (var item in listChildNode)
                        {
                            item.ParentNodeId = listParentNode.ParentNodeId;
                            item.ParentNodeId_Init = listParentNode.ParentNodeId_Init;
                            db.SaveChanges();
                        }

                        InsertNodesInFKM125(db, listParentNode, "DeleteNewNode"); //Task Id:- 20875

                        db.FKM202.Remove(listParentNode);
                        db.SaveChanges();
                    }

                    // child level list all delete method
                    //foreach (var item in listParentNode)
                    //{
                    //    FKM202 mainTree = new FKM202();
                    //    mainTree = GetDeleteAllNodesStructure(item, mainTree);
                    //    listDeleteTree.Add(mainTree);
                    //}

                    //db.FKM202.RemoveRange(listDeleteTree);
                    //db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult MomentDND(int Child)
        {
            momentDND prjmomentDND = new momentDND();
            try
            {
                var objFKM202 = db.FKM202.Where(x => x.NodeId == Child).FirstOrDefault();
                if (objFKM202 != null && objFKM202.NoOfPieces.HasValue)
                {
                    var parent = db.FKM202.Where(x => x.NodeId == objFKM202.ParentNodeId).FirstOrDefault();

                    prjmomentDND.Key = true;
                    prjmomentDND.Value = objFKM202.NoOfPieces.Value;
                    if (parent != null)
                    {
                        prjmomentDND.ParentQty = parent.NoOfPieces.HasValue ? parent.NoOfPieces.Value.ToString() : "0";
                    }
                    prjmomentDND.ProductType = objFKM202.ProductType;
                    prjmomentDND.Qty = objFKM202.NoOfPieces.Value.ToString();
                    return Json(prjmomentDND, JsonRequestBehavior.AllowGet);
                }
                prjmomentDND.Key = false;
                prjmomentDND.Value = 0;
                prjmomentDND.Qty = "0";
                prjmomentDND.ProductType = "";
                return Json(prjmomentDND, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DNDNewNode(int ParentNodeId, int ChildNodeId, int IsNOPs)
        {
            List<CopyNodeDtl> listCopyNodeDtl = null;
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (ParentNodeId != 0 && ChildNodeId != 0)
                {
                    var dndFKM202 = db.FKM202.Where(x => x.NodeId == ChildNodeId).FirstOrDefault();

                    if (dndFKM202 != null)
                    {
                        decimal IsNOP = Convert.ToDecimal(IsNOPs);
                        if (dndFKM202.ProductType != null && dndFKM202.ProductType.ToLower() != ASM && dndFKM202.ProductType.ToLower() != TJF)
                        {
                            Deallocation(ChildNodeId, objClsLoginInfo.UserName, objClsLoginInfo.Location);
                        }
                        if (IsNOP == dndFKM202.NoOfPieces)
                        {
                            dndFKM202.ParentNodeId = ParentNodeId;
                            dndFKM202.EditedBy = objClsLoginInfo.UserName;
                            dndFKM202.EditedOn = DateTime.Now;
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                        }
                        else if (IsNOP < dndFKM202.NoOfPieces)
                        {
                            listFKM202 = db.FKM202.Where(i => i.HeaderId == dndFKM202.HeaderId).ToList();

                            FKM202 parentfkm202 = new FKM202();
                            int parentid = ParentNodeId;
                            parentfkm202 = InsertDND(ref listCopyNodeDtl, dndFKM202, dndFKM202.HeaderId, parentfkm202, parentid, IsNOP);

                            FKM202 mainFKM202 = new FKM202();
                            int objparentid = parentfkm202.NodeId;
                            mainFKM202 = SetNodesFKM202(ref listCopyNodeDtl, dndFKM202, mainFKM202, dndFKM202.HeaderId, objparentid);

                            dndFKM202.NoOfPieces = dndFKM202.NoOfPieces - IsNOP;
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult _DynamicType(int headerid)
        {
            string assemblyType = clsImplementationEnum.FKMSNodeType.Equipment.GetStringValue();
            var assid = db.FKM203.Where(x => x.NodeType.ToLower() == assemblyType.ToLower()).Select(x => x.Id).FirstOrDefault();
            ViewBag.IsInitList = db.FKM202.Where(x => x.HeaderId == headerid && x.NodeTypeId == assid)
            .Select(x => new sourceProjectDetails
            {
                nodeName = x.NodeName,
                nodeId = x.NodeId
            }).ToList();

            return PartialView("_DynamicTypePartial");
        }

        [HttpPost]
        public ActionResult _PartMaterialTracking(int headerid, int nodeid)
        {
            ViewBag.HeaderId = headerid;
            ViewBag.Nodeid = nodeid;
            return PartialView("_PartMaterialTracking");
        }

        [HttpPost]
        public ActionResult LoadPartMaterialTracking(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                int Headerid = Convert.ToInt32(param.Headerid);
                int Nodeid = Convert.ToInt32(param.Nodeid);

                var mainproject = db.FKM201.Where(x => x.HeaderId == Headerid).Select(x => x.Project).FirstOrDefault();

                if (!string.IsNullOrEmpty(mainproject))
                {
                    string whereCondition = "1=1";

                    if (!string.IsNullOrWhiteSpace(param.sSearch))
                    {
                        string[] columnName = { "Project", "Element", "ElementDesc", "PartNo", "ItemID", "ItemDesc", "CompType", "Quantity", "Unit", "PCLNo", "PCRStatus", "PCLNo", "PCLStatus", "PlannerStatus", "ShopStatus", "StoreStatus", "OutboundKey", "OutboundQty", "POOrderQty", "POBalance", "Qtytobereceived", "UnderInspection", "ClearedInventory", "IssuedQty" };
                        whereCondition += columnName.MakeDatatableSearchCondition(param.SearchFilter);
                    }
                    else
                        whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);

                    string strSortOrder = string.Empty;
                    if (!string.IsNullOrWhiteSpace(sortColumnName))
                    {
                        strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                    }

                    var lstPam = db.SP_FKMS_GET_PROJECT_WISE_MATERIAL_TRACKING(mainproject, Nodeid, StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                    int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                    var res = (from h in lstPam
                               select new[] {
                               Convert.ToString(h.Project),
                               Convert.ToString(h.Element),
                               Convert.ToString(h.ElementDesc),
                               Convert.ToString(h.PartNo),
                               Convert.ToString(h.ItemID),
                               Convert.ToString(h.ItemDesc),
                               Convert.ToString(h.CompType),
                               Convert.ToString(h.Quantity),
                               Convert.ToString(h.Unit),
                               Convert.ToString(h.PCRNo),
                               //Convert.ToString(h.PCRStatus),
                               Convert.ToString(h.PCLNo),
                               //Convert.ToString(h.PCLStatus),
                               Convert.ToString(h.PlannerStatus),
                               //Convert.ToString(h.ShopStatus),
                               //Convert.ToString(h.StoreStatus),
                               Convert.ToString(h.OutboundKey),
                               Convert.ToString(h.OutboundQty),
                               Convert.ToString(h.POOrderQty),
                               Convert.ToString(h.POBalance),
                               Convert.ToString(h.Qtytobereceived),
                               Convert.ToString(h.UnderInspection),
                               Convert.ToString(h.ClearedInventory),
                               Convert.ToString(h.IssuedQty)
                    }).ToList();

                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = "0",
                        iTotalRecords = "0",
                        aaData = new string[0]
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = string.Empty,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = new string[0]
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GenerateExcel2(string whereCondition, string strSortOrder, string HeaderId, string NodeId, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int Hid = Convert.ToInt32(HeaderId);
                int Nid = Convert.ToInt32(NodeId);

                var mainproject = db.FKM201.Where(x => x.HeaderId == Hid).Select(x => x.Project).FirstOrDefault();

                string strFileName = string.Empty;
                var lst = db.SP_FKMS_GET_PROJECT_WISE_MATERIAL_TRACKING(mainproject, Nid, 1, int.MaxValue, strSortOrder, whereCondition).ToList();
                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Data Found";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                var newlst = (from li in lst
                              select new
                              {
                                  Project = li.Project,
                                  Element = li.Element,
                                  ElementDesc = li.ElementDesc,
                                  PartNo = li.PartNo,
                                  ItemID = li.ItemID,
                                  ItemDesc = li.ItemDesc,
                                  CompType = li.CompType,
                                  Quantity = li.Quantity,
                                  Unit = li.Unit,
                                  PCRNo = li.PCRNo,
                                  //PCRStatus = li.PCRStatus,
                                  PCLNo = li.PCLNo,
                                  //PCLStatus = li.PCLStatus,
                                  PlannerStatus = li.PlannerStatus,
                                  //ShopStatus = li.ShopStatus,
                                  //StoreStatus = li.StoreStatus,
                                  OutboundKey = li.OutboundKey,
                                  OutboundQty = li.OutboundQty,
                                  POOrderQty = li.POOrderQty,
                                  POBalance = li.POBalance,
                                  Qtytobereceived = li.Qtytobereceived,
                                  UnderInspection = li.UnderInspection,
                                  ClearedInventory = li.ClearedInventory,
                                  IssuedQty = li.IssuedQty,
                              }).ToList();

                strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetKitDetails(int NodeId)
        {
            FKM202 objFKM202 = null;
            KitDetails objResponseMsg = new KitDetails();
            objResponseMsg.releaseKey = 0;
            try
            {
                objFKM202 = db.FKM202.Where(x => x.NodeId == NodeId).FirstOrDefault();
                //var objFKM107 = db.FKM107.Where(x => x.NodeId == NodeId).ToList();
                //var objPDN002 = db.PDN002.Where(x => x.Project == objFKM202.Project).ToList();
                //string objmax = string.Empty;
                //int objmaxno = 0;
                //if (objPDN002.Count() > 0)
                //{
                //    objmax = objPDN002.Max(x => x.IssueNo).ToString();
                //    objmaxno = Convert.ToInt32(objmax);
                //}                

                if (objFKM202 != null)
                {
                    #region Update SOB Key Status
                    if (!string.IsNullOrEmpty(objFKM202.ProductType) &&
                        (objFKM202.ProductType.ToLower() == ASM || objFKM202.ProductType.ToLower() == TJF))
                    {
                        UpdateSOBStatusForDelivered(NodeId);
                    }
                    #endregion

                    //listASMColors(objFKM202.HeaderId);

                    //if (objParentsclrFKM202.Any(x => x.NodeId == NodeId))
                    //{
                    string ReleaseKitclr = clsImplementationEnum.ColorFlagForFKMS.Orange.GetStringValue();
                    string Relstatus = clsImplementationEnum.KitStatus.Release.GetStringValue();
                    string Reqstatus = clsImplementationEnum.KitStatus.Request.GetStringValue();
                    string DocStatus = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue();
                    string Reqformaterialstatus = clsImplementationEnum.FKMSMaterialDeliveryStatus.RequestGenerated.GetStringValue();
                    string ReqNotGenerated = clsImplementationEnum.FKMSMaterialDeliveryStatus.Requestnotgenerated.GetStringValue();

                    if (objFKM202.KitStatus == Relstatus)
                    {
                        objResponseMsg.releaseKey = 2;
                        objResponseMsg.DeliverStatus = objFKM202.DeleverStatus;
                        if (objFKM202.MaterialStatus == Reqstatus)
                        {
                            objResponseMsg.releaseKey = 3;
                            objResponseMsg.MaterialStatus = ReqNotGenerated;

                            if (objFKM202.MaterialStatus == Reqstatus)
                            {
                                objResponseMsg.MaterialStatus = Reqformaterialstatus;
                            }
                        }
                    }
                    else
                    {
                        //var color = objParentsclrFKM202.Where(x => x.NodeId == NodeId).Select(x => x.ASMPartsColor).FirstOrDefault();
                        var color = objFKM202.NodeColor;
                        if (color != null && color == ReleaseKitclr)
                        {
                            if (IsSubmitAnyMaterialAndDocuments(objFKM202.NodeId, objFKM202.Project))
                            {
                                objResponseMsg.releaseKey = 1;
                            }

                            //if (db.FKM106.Any(x => x.NodeId == objFKM202.NodeId))
                            //{
                            //    if (objFKM107.Any(x => x.NodeId == objFKM202.NodeId))
                            //    {
                            //        var data = (from pdn in objPDN002
                            //                    join fkm in objFKM107 on pdn.DocumentNo equals fkm.DocumentNo into t
                            //                    from fkm107 in t
                            //                    where pdn.IssueNo == objmaxno
                            //                    orderby pdn.LineId
                            //                    select new
                            //                    {
                            //                        IssueStatus = pdn.IssueStatus,
                            //                    })
                            //                    .ToList();

                            //        if (!data.Any(x => x.IssueStatus.ToLower() == DocStatus.ToLower()))
                            //        {
                            //            objResponseMsg.releaseKey = 1;
                            //        }
                            //    }
                            //}
                        }
                    }
                    //}

                    var items = db.FN_GET_LOCATIONWISE_DEPARTMENT_PDIN("", objFKM202.DeleverTo, -1).Select(x => new { text = x.t_dimx + "-" + x.t_desc, deliverid = x.t_dimx }).FirstOrDefault();
                    objResponseMsg.KitDept = items != null ? items.text : "";
                    objResponseMsg.DeliverId = items != null ? items.deliverid : "";
                    objResponseMsg.DeliverTo = !string.IsNullOrEmpty(objFKM202.DeleverTo) ? objFKM202.DeleverTo : "";

                    /*  SqlConnection objConn = new SqlConnection(Convert.ToString(ConfigurationManager.ConnectionStrings["LNConcerto"]));
                      try
                      {
                          objConn.Open();
                          SqlCommand objCmd = new SqlCommand();
                          objCmd.Connection = objConn;
                          objCmd.CommandType = CommandType.Text;
                          objCmd.Parameters.AddWithValue("@projNo", objFKM202.Project.Trim());
                          SqlDataAdapter dataAdapt = new SqlDataAdapter();
                          DataTable dataTable = new DataTable();
                          int tempTASKUNIQUEID;
                          int.TryParse(objFKM202.TASKUNIQUEID, out tempTASKUNIQUEID);
                          if (tempTASKUNIQUEID > 0)
                          {
                              objCmd.Parameters.AddWithValue("@TASKUNIQUEID", objFKM202.TASKUNIQUEID);
                              objCmd.CommandText = "select a.NAME [TaskName], a.TASKUNIQUEID [TId] from PROJ_TASK a with(nolock) " +
                                  "join PROJECT b with(nolock) on a.PROJECTNAME = b.PROJECTNAME where a.PROJECTNAME = @projNo AND a.TASKUNIQUEID = @TASKUNIQUEID " +
                                  " and a.SUMMARY <> 1 and a.TASK_TYPE = 0 order by a.TASKID";
                              dataAdapt.SelectCommand = objCmd;
                              dataAdapt.Fill(dataTable);

                              if (dataTable != null && dataTable.Rows.Count > 0)
                              {
                                  var ldstDrawingNo = (from DataRow row in dataTable.Rows select new { text = Convert.ToString(row["TaskName"]) }).FirstOrDefault();
                                  objResponseMsg.UniqueId = ldstDrawingNo != null ? ldstDrawingNo.text : "";
                                  var lsTasktNo = (from DataRow row in dataTable.Rows select new { text = Convert.ToString(row["TId"]) }).FirstOrDefault();
                                  objResponseMsg.TaskId = lsTasktNo != null ? lsTasktNo.text : "";
                                  //objResponseMsg.TaskId = ldstDrawingNo != null ? ldstDrawingNo.text : "";
                              }
                          }

                      }
                      catch (Exception ex)
                      {
                          Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                      }
                      finally
                      {
                          if (objConn.State == ConnectionState.Open)
                              objConn.Close();
                      }*/
                    //Observation 15487 on 04-07-2018
                    if (objFKM202 != null && objFKM202.ProductType != null && objFKM202.ProductType.ToLower() == clsImplementationEnum.NodeTypes.ASM.GetStringValue().ToLower())
                    {
                        objResponseMsg.IsSOBGenerated = db.FKM112.Where(x => x.NodeId == NodeId).Any();
                    }
                    else
                    {
                        objResponseMsg.IsSOBGenerated = false;
                    }

                    objResponseMsg.TaskId = objFKM202.TASKUNIQUEID;
                    objResponseMsg.Key = true;
                    objResponseMsg.NodeId = NodeId;
                    objResponseMsg.KitName = objFKM202.NodeName;

                    objResponseMsg.KitNumber = objFKM202.NodeKey + "-" + objFKM202.NodeId;
                    objResponseMsg.KitIsAssembly = (objFKM202.ProductType != null && objFKM202.ProductType.ToLower() == clsImplementationEnum.NodeTypes.ASM.GetStringValue().ToLower() || objFKM202.ProductType != null && objFKM202.ProductType.ToLower() == clsImplementationEnum.NodeTypes.TJF.GetStringValue().ToLower() ? true : false);

                    if (objResponseMsg.KitIsAssembly)
                    {
                        if (!IsKitRequested(objFKM202))
                        {
                            objResponseMsg.IsAllowToChangeDeliverToAndDeAllocate = true;
                        }
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Kit details not available.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SetDeliverDep(int NodeId, string ProjectCode)
        {
            FKM202 objFKM202 = null;
            KitDetails objResponseMsg = new KitDetails();
            try
            {
                string ErrorMessageHtml = string.Empty;
                bool IsUpdate = true;
                string UpdateTree = "0";
                List<FKM202> ErrorList = new List<FKM202>();

                objFKM202 = db.FKM202.Where(x => x.NodeId == NodeId).FirstOrDefault();
                if (objFKM202 != null)
                {
                    //Task ID# 18308 - When Change Deliver location at that time de-allocate all node
                    if (objFKM202.DeleverTo != ProjectCode)
                    {
                        #region De Allocation

                        var NonPLTNodeList = (from u in db.FKM202
                                              where u.Project == objFKM202.Project && u.ParentNodeId == objFKM202.NodeId
                                              && !string.IsNullOrEmpty(u.ProductType) && u.ProductType.ToLower() != ASM && u.ProductType.ToLower() != TJF && u.ProductType.ToLower() != PLT
                                              select u).ToList();

                        foreach (var item in NonPLTNodeList)
                        {
                            try
                            {
                                RemoveNonPlateDetails(item, ref IsUpdate, ref UpdateTree, ref ErrorList);
                            }
                            catch (Exception ex)
                            {
                                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                            }
                        }

                        #endregion
                    }

                    if (IsUpdate)
                    {
                        objFKM202.DeleverTo = ProjectCode;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.KitDept = objFKM202.DeleverTo;
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = GetHtmlStringForDeAllocationError(ErrorList);
                    }
                }
                else
                { 
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Kit Department not available.";
                }
                objResponseMsg.UniqueId = UpdateTree;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public void RemoveNonPlateDetails(FKM202 item, ref bool IsUpdate, ref string UpdateTree, ref List<FKM202> ErrorList)
        {
            string psno = objClsLoginInfo.UserName;
            string location = objClsLoginInfo.Location;
            //bool IsUpdate = true;
            var allocatedFKM109List = db.FKM109.Where(u => u.NodeId == item.NodeId).ToList();
            foreach (var objAllocatedFKM109 in allocatedFKM109List)
            {
                if (objAllocatedFKM109.AllocatedQty > 0)
                {
                    string ErrorMessage = string.Empty;
                    bool IsDeallocationSuccess = NPLTDeallocation(objAllocatedFKM109, psno, location, db, ref ErrorMessage);
                    if (IsDeallocationSuccess)
                    {
                        if (db.FKM109.Any(x => x.Id == objAllocatedFKM109.Id))
                        {
                            db.FKM109.Remove(objAllocatedFKM109);
                            db.SaveChanges();
                        }
                    }
                    else
                    {
                        IsUpdate = false;
                        if (ErrorList != null)
                        {
                            FKM202 objError = new FKM202();
                            objError.NodeId = item.NodeId;
                            objError.NodeName = item.NodeName;
                            objError.NodeKey = item.NodeKey;
                            objError.NodeColor = item.NodeColor;
                            objError.FindNo = item.FindNo;
                            objError.PLMError = ErrorMessage;
                            ErrorList.Add(objError);
                        }
                    }
                }
                else
                {
                    db.FKM109.Remove(objAllocatedFKM109);
                    db.SaveChanges();
                }
                UpdateTree = "1";
            }
            //return IsUpdate;
        }

        [NonAction]
        public string GetHtmlStringForDeAllocationError(List<FKM202> objFKM202List)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("<strong>Error occured in deallocation of below items : </strong>");
            sb.Append("<br/><br/>");

            sb.Append("<table id=\"tblError\" class=\"table table-bordered\">");
            sb.Append("<thead style='background-color:#b1eaef'><tr>");

            sb.Append("<th class=\"min-phone-l\">Kit Number</th>" +
            "<th class=\"min-phone-l\">Kit Name</th>" +
            "<th class=\"min-phone-l\">Error Message</th>");

            sb.Append("</tr></thead>");

            sb.Append("<tbody>");

            foreach (var item in objFKM202List)
            {
                string KitNumber = item.NodeName;
                string KitName = item.FindNo + "-" + item.NodeKey + "-" + item.NodeId;
                string NodeColor = item.NodeColor;

                sb.Append("<tr>");

                sb.Append("<td width=\"25%\">" + GetKitNumberWithColor(KitNumber, NodeColor) + "</td>");
                sb.Append("<td width=\"30%\">" + KitName + "</td>");
                sb.Append("<td width=\"45%\">" + item.PLMError + "</td>");

                sb.Append("</tr>");
            }

            sb.Append("</tbody>");
            sb.Append("</table>");

            return sb.ToString();
        }

        [HttpPost]
        public ActionResult SetTask(int NodeId, string ProjectCode)
        {
            FKM202 objFKM202 = null;
            //int ProjectTaskId = Convert.ToInt32(ProjectCode);
            KitDetails objResponseMsg = new KitDetails();
            try
            {
                objFKM202 = db.FKM202.Where(x => x.NodeId == NodeId).FirstOrDefault();
                if (objFKM202 != null)
                {
                    objFKM202.TASKUNIQUEID = ProjectCode;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.UniqueId = ProjectCode;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Kit Task Unique Id not available.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SetMaterialStatus(int NodeId)
        {
            KitDetails objResponseMsg = new KitDetails();
            FKM112 objfkm112 = new FKM112();
            FKM202 objFKM202 = null;
            string sobstatus = string.Empty;
            objResponseMsg.DeliverStatus = FKMSMaterialDeliveryStatus.Requestnotgenerated.GetStringValue();
            try
            {
                var fkm202Kit = db.FKM202.Where(u => u.NodeId == NodeId).FirstOrDefault();
                if (fkm202Kit != null)
                {
                    //If any non plate then generate SOB Key
                    if (db.FKM202.Any(i => i.ParentNodeId == NodeId && i.ProductType.ToLower() != ASM && i.ProductType.ToLower() != TJF && i.ProductType.ToLower() != PLT))
                    {
                        objfkm112.Project = fkm202Kit.Project;
                        objfkm112.NodeId = fkm202Kit.NodeId;
                        objfkm112.FindNo = fkm202Kit.FindNo;
                        objfkm112.NodeKey = fkm202Kit.NodeKey;
                        objfkm112.CreatedBy = objClsLoginInfo.UserName;
                        objfkm112.CreatedOn = DateTime.Now;
                        objfkm112.FullKitNo = fkm202Kit.NodeKey + "-" + fkm202Kit.NodeId; //NodeKey + NodeId 

                        FKSOBService serviceObj = new FKSOBService();

                        sfcoffertostoreResponseType P3createResponse = new sfcoffertostoreResponseType();
                        sfcoffertostoreRequestType P3createRequest = new sfcoffertostoreRequestType();
                        sfcoffertostoreRequestTypeControlArea P3controlArea = new sfcoffertostoreRequestTypeControlArea();
                        sfcoffertostoreRequestTypeFKSOB P3dataArea = new sfcoffertostoreRequestTypeFKSOB();

                        P3controlArea.processingScope = processingScope.request;
                        P3dataArea.quantitySpecified = true;

                        P3dataArea.project = "";
                        P3dataArea.location = "";
                        P3dataArea.quantity = 0;
                        P3dataArea.item = "";
                        P3dataArea.element = "";
                        P3dataArea.warehouse = "";
                        P3dataArea.workcenter = "";
                        P3dataArea.fullkitNo = objfkm112.FullKitNo.Trim();
                        P3dataArea.sobkey = "";
                        P3dataArea.logname = objClsLoginInfo.UserName;

                        try
                        {
                            P3createRequest.ControlArea = P3controlArea;
                            P3createRequest.DataArea = new sfcoffertostoreRequestTypeFKSOB[1];
                            P3createRequest.DataArea[0] = P3dataArea;

                            P3createResponse = serviceObj.sfcoffertostore(P3createRequest);

                            if (P3createResponse.InformationArea == null)
                            {
                                objfkm112.SOBKey = P3createResponse.DataArea[0].sobkey;
                                if (objfkm112.SOBKey != null)
                                {
                                    sobstatus = CheckSOBStatus(objfkm112.SOBKey);
                                    objfkm112.Status = sobstatus;

                                    if (sobstatus != string.Empty)
                                    {
                                        objFKM202 = db.FKM202.Where(x => x.NodeId == NodeId).FirstOrDefault();
                                        if (objFKM202 != null)
                                        {
                                            objFKM202.MaterialStatus = KitStatus.Request.GetStringValue();
                                            db.SaveChanges();
                                            objResponseMsg.Key = true;
                                            objResponseMsg.Value = FKMSMaterialDeliveryStatus.RequestGenerated.GetStringValue();
                                        }
                                    }
                                }
                            }
                            else
                            {
                                objfkm112.ErrorMsg = P3createResponse.InformationArea[0].messageText.ToString();
                            }
                            SendNotificationToFKM3(NodeId);
                            SendNotificationToFKM3ForFR(NodeId, fkm202Kit.Project);
                        }
                        catch (Exception ex)
                        {
                            objfkm112.ErrorMsg = ex.Message.ToString();
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = FKMSMaterialDeliveryStatus.Requestnotgenerated.GetStringValue();
                        }
                        finally
                        {
                            db.FKM112.Add(objfkm112);
                            db.SaveChanges();
                        }
                        UpdateReadyForKitStatusForAllNode(fkm202Kit.Project, fkm202Kit.NodeId);
                    }
                    else
                    {
                        SendNotificationToFKM3(NodeId);
                        SendNotificationToFKM3ForFR(NodeId, fkm202Kit.Project);
                        var objDepartment = db.COM002.Where(i => i.t_dimx == fkm202Kit.DeleverTo).FirstOrDefault();
                        fkm202Kit.DeleverStatus = FKMSMaterialDeliveryStatus.Delivered.GetStringValue();
                        fkm202Kit.MaterialStatus = FKMSMaterialDeliveryStatus.MaterialReceived.GetStringValue();
                        fkm202Kit.KitLocation = objDepartment != null ? objDepartment.t_dimx + "-" + objDepartment.t_desc : "";
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = FKMSMaterialDeliveryStatus.RequestGenerated.GetStringValue();
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Not available.";
                    objResponseMsg.DeliverStatus = FKMSMaterialDeliveryStatus.Requestnotgenerated.GetStringValue();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        private void SendNotificationToFKM3(int NodeId)
        {
            var listEmployeeForNotification = Manager.GetDepartmentRoleWiseEmployee(objClsLoginInfo.Location, "", UserRoleName.FKM3.GetStringValue());
            var objASMList = db.FKM202.Where(i => i.ParentNodeId == NodeId && (i.ProductType.ToLower() == ASM || i.ProductType.ToLower() == TJF)).ToList();
            foreach (var item in objASMList)
            {
                if (item.KitStatus == KitStatus.SendToFullkitArea.GetStringValue())
                {
                    item.KitStatus = KitStatus.RequestedByParentKit.GetStringValue();
                    if (listEmployeeForNotification != null)
                    {
                        var FKM3Emp = listEmployeeForNotification.Select(i => i.psno).ToList();
                        var psno = string.Join(",", FKM3Emp);
                        (new clsManager()).SendNotification(UserRoleName.FKM3.GetStringValue(), "", "", objClsLoginInfo.Location, "Please send " + item.NodeKey + " to parent Kit!", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/FKM/MaintainFKMS/Details/" + item.HeaderId + "?urlForm=m", psno);
                    }
                }
            }
            db.SaveChanges();
        }


        private void SendNotificationToFKM3ForFR(int NodeId, string Project)
        {
            if (IsShowFixture == "1")
            {
                string KitNumber = string.Empty;
                string FixtureNos = string.Empty;

                var objFKM202 = db.FKM202.Where(u => u.NodeId == NodeId && u.Project == Project).FirstOrDefault();
                if (objFKM202 != null)
                {
                    KitNumber = objFKM202.FindNo + "-" + objFKM202.NodeKey + "-" + objFKM202.NodeId;
                }

                var listEmployeeForNotification = Manager.GetDepartmentRoleWiseEmployee(objClsLoginInfo.Location, "", UserRoleName.FKM3.GetStringValue());
                var objFKM119List = db.FKM119.Where(x => x.NodeId == NodeId).ToList();
                foreach (var item in objFKM119List)
                {
                    item.FullkitAreaStatus = FullkitAreaStatus.RequestedForKit.GetStringValue();
                    item.RequestedBy = objClsLoginInfo.UserName;
                    item.RequestedOn = DateTime.Now;
                    FixtureNos += item.FixtureNo + ", ";
                }
                FixtureNos = FixtureNos.Trim().TrimEnd(',');

                if (listEmployeeForNotification != null && listEmployeeForNotification.Count > 0)
                {
                    var FKM3Emp = listEmployeeForNotification.Select(i => i.psno).ToList();
                    var psno = string.Join(",", FKM3Emp);


                    string message = string.Format("Fixture No: {0} is requested for Kit Number: {1} for Project No: {2} By {3}", FixtureNos, KitNumber, Project, objClsLoginInfo.Name);

                    (new clsManager()).SendNotification(UserRoleName.FKM3.GetStringValue(), "", "", objClsLoginInfo.Location, message, clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(), "/FKM/MaintainFKMS/PendingRequest", psno);
                }

                db.SaveChanges();
            }
        }


        [HttpPost]
        public ActionResult SetKitStatus(int NodeId)
        {
            KitDetails objResponseMsg = new KitDetails();
            try
            {
                //if (!IsSubmitAnyMaterialAndDocuments(objFKM202.NodeId, objFKM202.Project))
                //{
                //    objResponseMsg.Key = false;
                //    objResponseMsg.Value = "Please First Submit Material Details and Document List!";
                //    objResponseMsg.releaseKey = 0;
                //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                //}
                //if (IsShowFixture == "1")
                //{
                //    if (!IsAllFixtureAllocatedAndCompleted(objFKM202.NodeId))
                //    {
                //        objResponseMsg.Key = false;
                //        objResponseMsg.Value = "All attached fixtures should be allocated and completed!";
                //        objResponseMsg.releaseKey = 0;
                //        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                //    }
                //}
                objResponseMsg = UpdateKitStatus(NodeId);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public bool CheckDocumentAndFixtureStatus(int NodeId, string Project)
        {
            bool IsValid = true;
            try
            {
                if (!IsSubmitAnyMaterialAndDocuments(NodeId, Project))
                {
                    IsValid = false;
                }
                if (IsShowFixture == "1")
                {
                    if (!IsAllFixtureAllocatedAndCompleted(NodeId))
                    {
                        IsValid = false;
                    }
                }
            }
            catch (Exception)
            {
                IsValid = false;
            }
            return IsValid;
        }

        public KitDetails UpdateKitStatus(int NodeId)
        {
            FKM202 objFKM202 = null;
            KitDetails objResponseMsg = new KitDetails();
            try
            {
                objFKM202 = db.FKM202.Where(x => x.NodeId == NodeId).FirstOrDefault();
                if (objFKM202 != null && !string.IsNullOrEmpty(objFKM202.DeleverTo) && CheckDocumentAndFixtureStatus(NodeId, objFKM202.Project))
                {
                    objFKM202.KitStatus = clsImplementationEnum.KitStatus.Release.GetStringValue();
                    objFKM202.DeleverStatus = clsImplementationEnum.FKMSMaterialDeliveryStatus.Requestnotgenerated.GetStringValue();
                    objFKM202.MaterialStatus = clsImplementationEnum.FKMSMaterialDeliveryStatus.Requestnotgenerated.GetStringValue();
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Relesed Kit For Shop.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Not available.";
                    objResponseMsg.DeliverStatus = clsImplementationEnum.FKMSMaterialDeliveryStatus.Requestnotgenerated.GetStringValue();
                }
            }
            catch (Exception ex)
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
                throw;
            }
            return objResponseMsg;
        }

        #region Fetch Seam Data
        [HttpPost]
        public ActionResult FetchSeamData(int NodeId, string project)
        {
            ViewBag.Nodeid = NodeId;
            ViewBag.project = project;
            return PartialView("_SeamDetailsPartial");
        }

        [HttpPost]
        public ActionResult LoadSeamListbyNodeData(JQueryDataTableParamModel param)
        {
            try
            {
                var NodeId = param.Nodeid;
                var project = param.Project;

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";

                string[] columnName = { "SeamNo", "Position" };
                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstPam = db.SP_FKMS_GET_SEAM_DATA(StartIndex, EndIndex, strSortOrder, whereCondition, project, NodeId).ToList();
                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from h in lstPam
                           select new[] {
                               Convert.ToString(h.ROW_NO),
                               Convert.ToString(h.SeamNo),
                               Convert.ToString(h.Position),
                    }).ToList();

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadSeamwiseStagesGridData(JQueryDataTableParamModel param)
        {
            try
            {
                NDEModels objNDEModels = new NDEModels();
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                strWhere += "1=1 and SeamNo='" + param.TravelerNo + "'";

                string[] columnName = { "StageCode", "StageSequance", "AcceptanceStandard", "ApplicableSpecification", "InspectionExtent", "StageStatus", "Remarks", "LineId", };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);

                string strStatus = param.MTStatus;
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {

                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_IPI_SEAM_ICL_LINE_DETAILS
                                (
                               StartIndex,
                               EndIndex,
                               strSortOrder,
                               strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                            {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.StageCode),
                                Convert.ToString(uc.StageSequance),
                                Convert.ToString(uc.AcceptanceStandard),
                                Convert.ToString(uc.ApplicableSpecification),
                                Convert.ToString(uc.InspectionExtent),
                                Convert.ToString(uc.Remarks),
                                Convert.ToString(uc.StageStatus),
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region   Fetch Part Data
        [HttpPost]
        public ActionResult LoadPartlistFilteredDataPartial(string project, string parentpartno)
        {
            ViewBag.project = project;
            ViewBag.parentpartno = parentpartno;
            return PartialView("_PartDetailsPartial");
        }

        [HttpPost]
        public JsonResult LoadFilteredPartlistData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string partNo = param.Title;
                string project = param.Project;

                string strWhere = string.Empty;

                strWhere = "1=1";
                strWhere += string.Format(" and qms045.Project in('{0}')  and PartNo in('{1}')", project, partNo);

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "InspectionStatus",
                                                "[L&TInspectionResult]",
                                                 "(dbo.GET_IPI_STAGECODE_DESCRIPTION(qms055.StageCode,ltrim(rtrim(qms055.BU)),ltrim(rtrim(qms055.Location))))",
                                                "StageSequence",
                                                //"IterationNo",
                                                "OfferedQuantity",
                                                "BalanceQuantity"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_FETCH_PARTLISTOFFERINSPECTION_HEADERS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.StageCodeDesc),
                               Convert.ToString(uc.StageSequence),
                               Convert.ToString(uc.IterationNo),
                               Convert.ToString(uc.InspectionStatus),
                               Convert.ToString(uc.Quantity),
                               Convert.ToString(uc.BalanceQuantity),
                               Convert.ToString(uc.LNTInspectorResult),
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.isNDEStage),
                               Convert.ToString(uc.StageCode),
                               Convert.ToString(uc.StageSequence)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = 0,
                    iTotalDisplayRecords = 0,
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult LoadPartdetailsNDEReqData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[0]) + "," + Convert.ToString(Request["sColumns"].Split(',')[1]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                strWhere = "1=1";
                string project = param.Project, partNo = param.Title, stageCode = param.StageCode, stageSeq = param.Sequenceno;

                strWhere += string.Format(" and qms065.Project in('{0}')  and qms065.PartAssemblyFindNumber in('{1}') and qms065.StageCode in('{2}') and qms065.StageSequence in({3})", project, partNo, stageCode, stageSeq);


                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "qms065.Project+' - '+com1.t_dsca",
                                                "QualityProject",
                                                "Shop",
                                                "ShopLocation",
                                                "ShopRemark"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                //type : table 65

                var lstResult = db.SP_FETCH_IPI_NDE_PARTLIST_OFFERED_RESULT
                                (
                                StartIndex, EndIndex, "", strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.RequestNo),
                               Convert.ToString(uc.RequestNoSequence),
                               Convert.ToString(uc.IterationNo),
                               Convert.ToString(uc.RequestStatus),
                               Convert.ToString(uc.TestResult),
                               Convert.ToString(uc.ManufacturingCode),
                               Convert.ToString(uc.Shop),
                               Convert.ToString(uc.ShopLocation),
                               Convert.ToString(uc.ShopRemark),
                               Convert.ToString(uc.NDETechniqueNo),
                               Convert.ToString(uc.NDETechniqueRevisionNo),
                               Convert.ToString(uc.OfferedQuantity),
                               Convert.ToString(uc.ProductionDrawingNo),
                               Convert.ToString(uc.ReturnRemark),
                               Convert.ToString(uc.TPIAgency1),
                               Convert.ToString(uc.TPIAgency1Intervention),
                               Convert.ToString(uc.TPIAgency2),
                               Convert.ToString(uc.TPIAgency2Intervention),
                               Convert.ToString(uc.TPIAgency3),
                               Convert.ToString(uc.TPIAgency3Intervention),
                               Convert.ToString(uc.TPIAgency4),
                               Convert.ToString(uc.TPIAgency4Intervention),
                               Convert.ToString(uc.TPIAgency5),
                               Convert.ToString(uc.TPIAgency5Intervention),
                               Convert.ToString(uc.TPIAgency6),
                               Convert.ToString(uc.TPIAgency6Intervention),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn),
                               Convert.ToString(uc.RequestGeneratedBy),
                               Convert.ToString(uc.RequestGeneratedOn),
                               Convert.ToString(uc.ConfirmedBy),
                               Convert.ToString(uc.ConfirmedOn),
                               Convert.ToString(uc.OfferedBy),
                               Convert.ToString(uc.OfferedOn),
                               Convert.ToString(uc.ReturnedBy),
                               Convert.ToString(uc.ReturnedOn),
                               Convert.ToString(uc.RejectedBy),
                               Convert.ToString(uc.RejectedOn)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = 0,
                    iTotalDisplayRecords = 0,
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadPartdetailsReqData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[0]) + "," + Convert.ToString(Request["sColumns"].Split(',')[1]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string project = param.Project, partNo = param.Title, stageCode = param.StageCode, stageSeq = param.Sequenceno;
                string strWhere = string.Empty;
                strWhere = "1=1";
                strWhere += string.Format(" and qms055.Project in('{0}') and qms055.PartNo in('{1}') and qms055.StageCode in('{2}') and qms055.StageSequence in({3})", project, partNo, stageCode, stageSeq);



                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "qms055.Project+' - '+com1.t_dsca",
                                                "QualityProject",
                                                "TestResult",
                                                "Shop",
                                                "ShopLocation",
                                                "ShopRemark"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_FETCH_PARTLISTTESTDETAILS_HEADERS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.RequestNo),
                               Convert.ToString(uc.RequestNoSequence),
                               Convert.ToString(uc.IterationNo),
                               Convert.ToString(uc.TestResult),
                               Convert.ToString(uc.OfferedQuantity),
                               Convert.ToString(uc.ClearedQuantity),
                               Convert.ToString(uc.RejectedQuantity),
                               Convert.ToString(uc.Shop),
                               Convert.ToString(uc.ShopLocation),
                               Convert.ToString(uc.ShopRemark),
                               Convert.ToString(uc.OfferedBy),
                               Convert.ToString(uc.OfferedOn),
                               Convert.ToString(uc.InspectedBy),
                               Convert.ToString(uc.InspectedOn),
                               Convert.ToString(uc.QCRemarks),
                               Convert.ToString(uc.OfferedtoCustomerBy),
                               Convert.ToString(uc.OfferedtoCustomerOn),
                               Convert.ToString(uc.TPIAgency1),
                               Convert.ToString(uc.TPIAgency1Intervention),
                               Convert.ToString(uc.TPIAgency1Result),
                               Convert.ToString(uc.TPIAgency1ResultOn),
                               Convert.ToString(uc.TPIAgency2),
                               Convert.ToString(uc.TPIAgency2Intervention),
                               Convert.ToString(uc.TPIAgency2Result),
                               Convert.ToString(uc.TPIAgency2ResultOn),
                               Convert.ToString(uc.TPIAgency3),
                               Convert.ToString(uc.TPIAgency3Intervention),
                               Convert.ToString(uc.TPIAgency3Result),
                               Convert.ToString(uc.TPIAgency3ResultOn),
                               Convert.ToString(uc.TPIAgency4),
                               Convert.ToString(uc.TPIAgency4Intervention),
                               Convert.ToString(uc.TPIAgency4Result),
                               Convert.ToString(uc.TPIAgency4ResultOn),
                               Convert.ToString(uc.TPIAgency5),
                               Convert.ToString(uc.TPIAgency5Intervention),
                               Convert.ToString(uc.TPIAgency5Result),
                               Convert.ToString(uc.TPIAgency5ResultOn),
                               Convert.ToString(uc.TPIAgency6),
                               Convert.ToString(uc.TPIAgency6Intervention),
                               Convert.ToString(uc.TPIAgency6Result),
                               Convert.ToString(uc.TPIAgency6ResultOn),
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = 0,
                    iTotalDisplayRecords = 0,
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        [HttpPost]
        public ActionResult GetIsAssemblyOrNotCheck(int NodeId)
        {
            FKM202 objFKM202 = null;
            IscheckAsm objResponseMsg = new IscheckAsm();
            try
            {
                objFKM202 = db.FKM202.Where(x => x.NodeId == NodeId).FirstOrDefault();
                if (objFKM202 != null)
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.NodeId = NodeId;
                    objResponseMsg.chktype = (objFKM202.ProductType != null && objFKM202.ProductType.ToLower() == clsImplementationEnum.NodeTypes.ASM.GetStringValue().ToLower() || objFKM202.ProductType != null && objFKM202.ProductType.ToLower() == clsImplementationEnum.NodeTypes.TJF.GetStringValue().ToLower() ? true : false);
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Kit details not available.";
                }

                var lstFKM105 = db.FKM105.ToList();

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult LoadKitListData(JQueryDataTableParamModel param)
        {
            try
            {
                var Headerid = Convert.ToInt32(param.Headerid);
                var NodeId = Convert.ToInt32(param.Nodeid);
                //listASMColors(Headerid);
                var SendToNextKit = KitStatus.SendToNextKit.GetStringValue();
                UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
                var UserRole = objUserRoleAccessDetails.UserRole;

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                var IsDisplayOnly = !string.IsNullOrEmpty(Request["IsDisplayOnly"]) ? Convert.ToBoolean(Request["IsDisplayOnly"].ToString()) : false;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string status = param.CTQCompileStatus;
                var objFKM202 = db.FKM202.Where(i => i.NodeId == NodeId).FirstOrDefault();
                if (objFKM202.ProductType != null && (objFKM202.ProductType.ToLower() == ASM.ToLower() || objFKM202.ProductType.ToLower() == TJF.ToLower()))
                {
                    string whereCondition = "1=1 and ParentNodeId = " + NodeId + " and HeaderId = " + Headerid;

                    string[] columnName = { "FindNo", "ProductType", "NoOfPieces" };

                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                    string strSortOrder = string.Empty;
                    if (!string.IsNullOrWhiteSpace(sortColumnName))
                    {
                        strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                    }

                    var lstPam = db.SP_FKMS_GETKITLIST(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                    int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();
                    var userDepartment = objClsLoginInfo.Department;

                    var res = (from h in lstPam
                               select new[] {
                               Convert.ToString(h.NodeId),
                               Convert.ToString(h.HeaderId),
                               //PLTSetColorFlag(h.NodeId),
                               h.NodeColor,
                               BorderLeftColorFlag(h.NodeId,h.FindNo,h.NodeColor),
                               Convert.ToString(h.ProductType),
                               Convert.ToString(h.TotalBOMQty),
                               Convert.ToString(h.NoOfPieces),
                               !string.IsNullOrEmpty(h.KitLocation) ? h.KitLocation : "",
                               "<center>"
                               + Helper.GenerateActionIcon(h.HeaderId, "View", "View Detail", "fa fa-eye", "IsAssemblyOrNot('" + h.NodeId + "','"+ (h.ProductType!=null && h.ProductType.ToLower() == clsImplementationEnum.NodeTypes.ASM.GetStringValue().ToLower() || h.ProductType!=null && h.ProductType.ToLower() == clsImplementationEnum.NodeTypes.TJF.GetStringValue().ToLower() ? "true" : "false") +"','" + (h.NodeId + "#" + h.HeaderId + "#" + h.NodeTypeId + "#" + h.ParentNodeId) + "','"+ h.TotalBOMQty +"')","javascript:void(0)",false)+""
                               + (IsDisplayOnly ? "" : ((UserRole.ToLower() == clsImplementationEnum.UserRoleName.PLNG2.GetStringValue().ToLower() || UserRole.ToLower() == clsImplementationEnum.UserRoleName.PLNG1.GetStringValue().ToLower() || UserRole.ToLower() == clsImplementationEnum.UserRoleName.PLNG3.GetStringValue().ToLower()) ? (h.ProductType!=null && h.ProductType.ToLower() == clsImplementationEnum.NodeTypes.ASM.GetStringValue().ToLower() ||  h.ProductType!=null && h.ProductType.ToLower() == clsImplementationEnum.NodeTypes.TJF.GetStringValue().ToLower() ? "" : GenerateActionIcon(h.HeaderId, "task", "Allocation", "fa fa-tasks", "", WebsiteURL + "/FKM/MaintainFKMS/AllocationDetails?NodeId=" + h.NodeId,false)) : ""))
                               + Helper.GenerateActionIcon(h.HeaderId, "PartDetails", "Part Detail", "icon-settings", "GetStagesforFindNo('"+h.Project+"','"+h.FindNo+"')","", (h.ProductType.ToLower() != clsImplementationEnum.NodeTypes.ASM.GetStringValue().ToLower() && h.ProductType.ToLower() != clsImplementationEnum.NodeTypes.TJF.GetStringValue().ToLower()) ? false :true)
                               //+ (objFKM202.DeleverTo==userDepartment
                               //     ? (!string.IsNullOrEmpty(h.KitStatus) && h.KitStatus==SendToNextKit ? Helper.GenerateActionIcon(h.HeaderId, "AcceptKit", "Accept Kit", "fa fa-check", "AcceptKitByParentKit("+h.NodeId+")",""):"")
                               //     :"")
                               + (objFKM202.DeleverTo==userDepartment
                                    ? (!string.IsNullOrEmpty(h.KitStatus) && h.KitStatus==SendToNextKit && string.IsNullOrEmpty(objFKM202.KitStatus) ? Helper.GenerateActionIcon(h.NodeId, "Send To Fullkit Area", "Send To Fullkit Area", "fa fa-arrow-right", "LoadFullkitArea("+h.NodeId+")","javascript:void(0)"):"")
                                    :"")
                               + "</center>"
                    }).ToList();

                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                        //whereCondition = whereCondition,
                        //strSortOrder = strSortOrder,
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = "0",
                        iTotalRecords = "0",
                        aaData = new string[0]
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #region Document List Observation 15436 On 29-06-2018
        [HttpGet]
        public JsonResult GetDocumentType()
        {
            //Observation 15518 on 05-07-2018
            List<BULocWiseCategoryModel> lstObjDocumentType = new List<BULocWiseCategoryModel>();
            lstObjDocumentType.Add(new BULocWiseCategoryModel { CatID = clsImplementationEnum.FKMSDocumentType.HTR.GetStringValue(), CatDesc = clsImplementationEnum.FKMSDocumentType.HTR.GetStringValue() });
            lstObjDocumentType.Add(new BULocWiseCategoryModel { CatID = clsImplementationEnum.FKMSDocumentType.DIN.GetStringValue(), CatDesc = clsImplementationEnum.FKMSDocumentType.DIN.GetStringValue() });

            return Json(lstObjDocumentType, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetAllDocuments(string term, string Project, int NodeId, string DocType)
        {
            List<CategoryData> lstObjdocument = new List<CategoryData>();
            if (DocType == clsImplementationEnum.FKMSDocumentType.DIN.GetStringValue())
            {
                var docType = clsImplementationEnum.FKMSDocumentType.DIN.GetStringValue();

                var productType = db.PDN001.Where(x => x.Project == Project).GroupBy(u => u.Product).ToDictionary(g => g.Key, g => g.Max(item => item.IssueNo)).FirstOrDefault();
                if (productType.Key != null)
                {

                }
                else
                {
                    bool IsExist = false;
                    var objPDN001List = db.PDN001.Where(x => !string.IsNullOrEmpty(x.IProject)).ToList();
                    foreach (var obj in objPDN001List)
                    {
                        string[] strList = obj.IProject.Split(',');
                        foreach (var strIProject in strList)
                        {
                            if (strIProject.ToLower() == Project.ToLower())
                            {
                                Project = obj.Project;
                                IsExist = true;
                                break;
                            }
                        }

                        if (IsExist)
                            break;
                    }
                }

                var objPDN002 = db.PDN002.Where(x => x.Project == Project).ToList();
                if (objPDN002 != null && objPDN002.Count > 0)
                {
                    int MaxIssueNo = objPDN002.Max(x => x.IssueNo);
                    var lstPDN002 = db.PDN002.Where(x => x.Project == Project && x.IssueNo == MaxIssueNo).ToList();
                    var lstDocumentNo = db.FKM107.Where(x => x.NodeId == NodeId && x.DocType == docType).Select(x => x.DocumentNo).ToList();
                    if (lstDocumentNo != null && lstDocumentNo.Count > 0)
                    {
                        lstPDN002 = lstPDN002.Where(x => !lstDocumentNo.Contains(x.DocumentNo)).ToList();
                    }

                    lstObjdocument = (from li in lstPDN002
                                      where li.Description.ToLower().Contains(term.ToLower())
                                      select new CategoryData
                                      {
                                          id = li.LineId.ToString(),
                                          text = li.Description,
                                      }).ToList();
                }
            }
            else
            {
                string status = clsImplementationEnum.CTQStatus.Approved.GetStringValue().ToLower();
                string docType = clsImplementationEnum.FKMSDocumentType.HTR.GetStringValue();
                var lstHTR001 = db.HTR001.Where(x => x.Project == Project /*&& x.Status.ToLower() == status*/).ToList();// condition removed as per observation 17589
                if (lstHTR001 != null && lstHTR001.Count > 0)
                {
                    var lstHtrDocumentNo = db.FKM107.Where(x => x.NodeId == NodeId && x.DocType == docType).Select(x => x.DocumentNo).ToList();
                    if (lstHtrDocumentNo != null && lstHtrDocumentNo.Count > 0)
                    {
                        lstHTR001 = lstHTR001.Where(x => !lstHtrDocumentNo.Contains(x.HTRNo)).ToList();
                    }

                    lstObjdocument = (from li in lstHTR001
                                      where li.HTRNo.ToLower().Contains(term.ToLower())
                                      select new CategoryData
                                      {
                                          id = li.HTRNo.ToString(),
                                          text = li.HTRNo,
                                      }).ToList();
                }
            }
            return Json(lstObjdocument, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDocumentList(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string indextype = param.Department;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string KitStatus = clsImplementationEnum.KitStatus.Release.GetStringValue();

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                //bool IsAssemblyOrNot = param.ProductType;
                bool isenable = false;
                int nodeId = Convert.ToInt32(param.Nodeid);

                bool isMaintain = false;
                string userRole = param.Roles;
                if (indextype == "maintain" && userRole == clsImplementationEnum.UserRoleName.PLNG3.ToString())
                {
                    isMaintain = true;
                }
                //if (param.Married)
                //{
                //    if (db.FKM202.Any(x => x.NodeId == nodeId && x.KitStatus == KitStatus))
                //    {
                //        ischek = true;
                //    }
                //}
                var objFKM202 = db.FKM202.Where(x => x.NodeId == nodeId).FirstOrDefault();
                bool IsAssemblyOrNot = (objFKM202.ProductType.ToLower() == ASM.ToLower() || objFKM202.ProductType.ToLower() == TJF.ToLower());
                if (string.IsNullOrEmpty(objFKM202.KitStatus))
                {
                    isenable = true;
                }

                string Project = param.Project;
                var productType = db.PDN001.Where(x => x.Project == Project).GroupBy(u => u.Product).ToDictionary(g => g.Key, g => g.Max(item => item.IssueNo)).FirstOrDefault();
                if (productType.Key != null)
                {

                }
                else
                {
                    bool IsExist = false;
                    var objPDN001List = db.PDN001.Where(x => !string.IsNullOrEmpty(x.IProject)).ToList();
                    foreach (var obj in objPDN001List)
                    {
                        string[] strList = obj.IProject.Split(',');
                        foreach (var strIProject in strList)
                        {
                            if (strIProject.ToLower() == Project.ToLower())
                            {
                                Project = obj.Project;
                                IsExist = true;
                                break;
                            }
                        }

                        if (IsExist)
                            break;
                    }
                }

                var objPDN002 = db.PDN002.Where(x => x.Project == Project).ToList();
                var objHTR001 = db.HTR001.Where(x => x.Project == Project).ToList();

                if ((objPDN002 != null && objPDN002.Count > 0) || (objHTR001 != null && objHTR001.Count > 0) && IsAssemblyOrNot)
                {
                    int MaxIssueNo = 0;
                    //string whereCondition = "1=1 and F7.NodeId = " + nodeId + " and P2.Project = '" + Project + "'"; // Document list was not coming for secondary project. because documents are mainly property of primary project. so project condition skipped.
                    string whereCondition = "1=1 and F7.NodeId = " + nodeId + " and DocType='DIN'";

                    if (objPDN002 != null && objPDN002.Count > 0)
                    {
                        MaxIssueNo = db.PDN002.Where(x => x.Project == Project).Max(x => x.IssueNo);
                        whereCondition += " and P2.IssueNo = " + MaxIssueNo;
                    }

                    //var HTRwhereCondition = "F7.NodeId = " + nodeId + " and H1.Project = '" + Project + /*"' and H1.Status = '" + clsImplementationEnum.CTQStatus.Approved.GetStringValue() +*/ "'";  // condition removed as per observation 17589
                    var HTRwhereCondition = "F7.NodeId = " + nodeId + " and DocType='HTR'"; // Document list was not coming for secondary project. because documents are mainly property of primary project. so project condition skipped.

                    string[] columnName = { "Description", "DocumentNo", "Status", "RevNo" };

                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                    string strSortOrder = string.Empty;
                    if (!string.IsNullOrWhiteSpace(sortColumnName))
                    {
                        strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                    }

                    #region Observation (15436 on 29-06-2018), (15518 on 05-07-2018)
                    var lstPam = db.SP_FKMS_GET_HTR_DOCUMENTLIST(StartIndex, EndIndex, strSortOrder, whereCondition, HTRwhereCondition).ToList();

                    int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                    var res = (from h in lstPam
                               select new[] {
                               Convert.ToString(h.Id),
                               Convert.ToString(h.DocType),
                               Convert.ToString(h.Description),
                               (checkdocument(Project, userRole, h.DocumentNo, h.DocType)),
                               Convert.ToString(h.Status),
                               Convert.ToString("R"+h.RevNo),
                               isenable && IsAssemblyOrNot
                               ? (indextype == "maintain" ? Helper.GenerateActionIcon(h.Id, "Document", "Document Details", "fa fa-eye", "", GetDocumentDetailsLink(h.DocumentNo, h.DocType),false, true,"") : Helper.GenerateActionIcon(h.Id, "Document", "Document Details", "fa fa-eye disabledicon", "", "",false, false,""))
                                + (isMaintain ? Helper.HTMLActionString(h.Id, "Delete", "Delete Materil Details", "fa fa-trash-o", "DeleteDocument('','/FKM/MaintainFKMS/DeleteDocument', {Id:"+h.Id+"}, 'tblDocumentList')", "") : Helper.HTMLActionString(h.Id, "Delete", "Delete Materil Details", "fa fa-trash-o disabledicon", "", ""))
                               : ""
                    }).ToList();


                    int newRecordId = 0;
                    var newRecord = new[] {
                                    "0",
                                    Helper.HTMLAutoComplete(newRecordId, "DocumentType","","", false,"","DocumentType",false,"","","---Type---")+""+Helper.GenerateHidden(newRecordId, "hdnDocumentType", ""),
                                    Helper.HTMLAutoComplete(newRecordId, "Description","","", false,"","Description",true,"","","---Select Documents---")+""+Helper.GenerateHidden(newRecordId, "hdnDescription", ""),
                                    "",
                                    "",
                                    "",
                                    IsAssemblyOrNot ? Helper.HTMLActionString(newRecordId,"Add","Add New Record","fa fa-plus","SaveNewDocument("+nodeId+");") : "",
                                };
                    res.Insert(0, newRecord);

                    #endregion

                    //var lstPam = db.SP_FKMS_GETDOCUMENT(nodeId, StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                    //int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                    //var res = (from h in lstPam
                    //           select new[] {
                    //           IsAssemblyOrNot != false ? h.IsCheck != "False"? GenerateHTMLCheckboxWithEvent(h.LineId,"check",true, "UpdateDocument(this, "+h.LineId+")", true, "clsCheckbox",ischek,"check"):GenerateHTMLCheckboxWithEvent(h.LineId,"check",false, "UpdateDocument(this, "+h.LineId+")", true, "clsCheckbox", ischek,""):GenerateHTMLCheckboxWithEvent(h.LineId,"check",false, "", false, "clsCheckbox"),
                    //           //IsAssemblyOrNot != false ? h.IsCheck != "False"? GenerateHTMLCheckboxWithEvent(h.LineId,"check",true, "UpdateDocument(this, "+h.LineId+")", true, "clsCheckbox"):GenerateHTMLCheckboxWithEvent(h.LineId,"check",false, "UpdateDocument(this, "+h.LineId+")", true, "clsCheckbox"):h.IsCheck != "False"? GenerateHTMLCheckboxWithEvent(h.LineId,"check",true, "UpdateDocument(this, "+h.LineId+")", true, "clsCheckbox"):GenerateHTMLCheckboxWithEvent(h.LineId,"check",false, "UpdateDocument(this, "+h.LineId+")", true, "clsCheckbox"),
                    //           Convert.ToString(h.LineId),
                    //           Convert.ToString(h.Description),
                    //           Convert.ToString(h.DocumentNo),
                    //           Convert.ToString(h.IssueStatus),
                    //           Convert.ToString("R"+h.RevNo),
                    //}).ToList();

                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = "0",
                        iTotalRecords = "0",
                        aaData = new string[0]
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = string.Empty,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = new string[0]
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult SaveNewDocument(string DocumentId, int NodeId, string DocType)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                FKM107 objFKM107 = new FKM107();
                if (DocType == clsImplementationEnum.FKMSDocumentType.DIN.GetStringValue())
                {
                    int docId = Convert.ToInt32(DocumentId);
                    objFKM107.DocumentNo = db.PDN002.Where(x => x.LineId == docId).Select(x => x.DocumentNo).FirstOrDefault();
                }
                else
                {
                    objFKM107.DocumentNo = DocumentId;
                }
                objFKM107.NodeId = NodeId;
                objFKM107.DocType = DocType;
                objFKM107.CreatedBy = objClsLoginInfo.UserName;
                objFKM107.CreatedOn = DateTime.Now;
                db.FKM107.Add(objFKM107);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult DeleteDocument(int Id)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsgWithStatus = new clsHelper.ResponseMsgWithStatus();
            FKM107 objFKM107 = db.FKM107.Where(x => x.Id == Id).FirstOrDefault();
            if (objFKM107 != null)
            {
                db.FKM107.Remove(objFKM107);
                db.SaveChanges();
                objResponseMsgWithStatus.Key = true;
                objResponseMsgWithStatus.Value = "Document deleted successfully.";

            }
            else
            {
                objResponseMsgWithStatus.Key = false;
                objResponseMsgWithStatus.Value = "No data found";
            }

            return Json(objResponseMsgWithStatus, JsonRequestBehavior.AllowGet);
        }
        #endregion
        [NonAction]
        public static string GenerateHTMLCheckboxWithEvent(int rowId, string columnName, bool columnValue = false, string onClickMethod = "", bool isEnabled = true, string ClassName = "", bool isDisabled = false, string check = "")
        {
            string htmlControl = "";
            string inputID = columnName + "" + rowId.ToString();
            string inputValue = columnValue ? "Checked='Checked'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";
            string cssClass = !string.IsNullOrEmpty(ClassName) ? "class='" + ClassName + "'" : "";

            if (isDisabled)
            {
                if (check != string.Empty && check != null)
                {
                    htmlControl = "<input type='checkbox' id='" + inputID + "' " + inputValue + " name='" + inputID + "' colname='" + columnName + "' disabled='disabled " + cssClass + " />";
                }
                else
                {
                    htmlControl = "<input type='checkbox' id='" + inputID + "' name='" + inputID + "' colname='" + columnName + "' disabled='disabled " + cssClass + " />";
                }
            }
            else
            {
                if (isEnabled)
                {
                    htmlControl = "<input type='checkbox' id='" + inputID + "' " + inputValue + " name='" + inputID + "' colname='" + columnName + "' " + onClickEvent + " " + (!isEnabled ? "disabled='disabled'" : "") + " " + cssClass + " />";
                }
                else
                {
                    htmlControl = columnValue ? "" : "";
                }
            }
            return htmlControl;
        }

        [NonAction]
        public static string GenerateActionIcon(int? rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", string onHrefURL = "", bool isDisable = false, bool OpenNewWindow = false, string inputstyle = "")
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? onClickMethod : "";
            string onHref = !string.IsNullOrEmpty(onHrefURL) ? "href='" + onHrefURL + "'" : "";
            string newWindow = (OpenNewWindow ? "target='_blank'" : "");
            string customestyle = !string.IsNullOrEmpty(inputstyle) ? "style='" + inputstyle + "'" : "";
            if (isDisable)
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' Title='" + buttonTooltip + "' class='disabledicon " + className + "' " + customestyle + "></i>";
            }
            else
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' Title='" + buttonTooltip + "' class='iconspace " + className + "' onclick =\"" + onClickEvent + "\"  " + customestyle + " ></i>";
                if (!string.IsNullOrEmpty(onHref))
                {
                    htmlControl = "<a target='_blank' " + onHref + " " + newWindow + " >" + htmlControl + "</a>";
                }
            }
            return htmlControl;
        }

        [NonAction]
        public static string GeneratePartButtonNew(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", string inputstyle = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";
            string customestyle = !string.IsNullOrEmpty(inputstyle) ? "style='" + inputstyle + "'" : "";

            htmlControl = "<a id='" + inputID + "' name='" + inputID + "' class='" + className + "' " + onClickEvent + " " + customestyle + "> " + buttonName + "</a>";

            //htmlControl = "<i  data-modal='' id='" + inputID + "' name='" + inputID + "' style='cursor:Pointer;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";

            return htmlControl;
        }

        #region Enabling Material

        [HttpPost]
        public JsonResult GetAllMaterial(string term, int NodeId)
        {
            List<CategoryData> lstObjMaterial = new List<CategoryData>();
            var lstFKM106 = db.FKM106.Where(x => x.NodeId == NodeId).ToList();
            List<int> materialIds = new List<int>();
            if (lstFKM106 != null && lstFKM106.Count > 0)
            {
                materialIds = lstFKM106.Select(x => x.EnablingMaterialId).ToList();
            }
            var lstMaterial = db.FKM105.Where(x => !materialIds.Contains(x.Id)).ToList();

            lstObjMaterial = (from li in lstMaterial
                              where li.EnablingMaterial.ToLower().Contains(term.ToLower())
                              select new CategoryData
                              {
                                  id = li.Id.ToString(),
                                  text = li.EnablingMaterial,
                              }).ToList();
            return Json(lstObjMaterial, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetMaterialList(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string KitStatus = clsImplementationEnum.KitStatus.Release.GetStringValue();


                bool isenable = false;
                string indextype = param.Department;

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                int nodeId = Convert.ToInt32(param.Headerid);
                var objFKM202 = db.FKM202.Where(x => x.NodeId == nodeId).FirstOrDefault();
                string project = objFKM202.Project;

                bool IsAssemblyOrNot = (objFKM202.ProductType.ToLower() == ASM.ToLower() || objFKM202.ProductType.ToLower() == TJF.ToLower());
                if (string.IsNullOrEmpty(objFKM202.KitStatus))
                {
                    isenable = true;
                }
                bool isMaintain = false;
                string userRole = param.Roles;
                if (indextype == "maintain" && userRole == clsImplementationEnum.UserRoleName.PLNG3.ToString())
                {
                    isMaintain = true;
                }
                //bool IsAssemblyOrNot = param.Married;

                //var objFKM105 = db.FKM105.ToList();

                //if (objFKM105 != null && objFKM105.Count > 0)
                //{
                if (IsAssemblyOrNot)
                {
                    string whereCondition = "1=1 and NodeId=" + nodeId;

                    string[] columnName = { "EnablingMaterialId", "Remarks" };

                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                    string strSortOrder = string.Empty;
                    if (!string.IsNullOrWhiteSpace(sortColumnName))
                    {
                        strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                    }

                    //var lstPam = db.SP_FKMS_GETMATERIAL(nodeId, StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                    var lstPam = db.SP_FKMS_GET_MATERIALLIST(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                    int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                    var lstFKM105 = db.FKM105.ToList();

                    var res = (from h in lstPam
                               select new[] {
                                   isenable ? isMaintain ? (h.IsReady == true ? GenerateHTMLCheckboxWithEvent(Convert.ToInt32(h.Id),"check",true, "UpdateEnablingMaterial(this, "+h.Id+")", true, "clsCheckbox") : GenerateHTMLCheckboxWithEvent(Convert.ToInt32(h.Id),"check",false, "UpdateEnablingMaterial(this,"+h.Id+")", true, "clsCheckbox")) : "" : "",
                                   Convert.ToString(h.Id),
                                   Helper.GenerateHidden(h.Id, "NodeId", Convert.ToString(nodeId)),
                                   Convert.ToString(h.IsReady),
                                   Convert.ToString(lstFKM105.Where(x => x.Id == h.EnablingMaterial).Select(x => x.EnablingMaterial).FirstOrDefault()),
                                   isenable ? (IsAssemblyOrNot == true ? isMaintain ? Helper.GenerateTextArea(h.Id, "Remarks", Convert.ToString(h.Remarks), "UpdateMaterialHeader(this, "+ h.Id +")", false, "", "200",""):Helper.GenerateTextArea(h.Id, "Remarks", Convert.ToString(h.Remarks), "", true, "", "200",""):Helper.GenerateTextArea(h.Id, "Remarks", Convert.ToString(h.Remarks), "", true, "", "200","")) : Helper.GenerateTextArea(h.Id, "Remarks", h.Remarks, "", true, "", "200",""),
                                   isenable && IsAssemblyOrNot ? /*(isMaintain ? Helper.HTMLActionString(h.Id, "Add", "Add Materil Details", "fa fa-plus", "AddAndUpdateDetails("+0+","+ h.Id +",'"+project+"','add')", "") : Helper.HTMLActionString(h.Id, "Add", "Add Materil Details", "fa fa-plus disabledicon", "", "")) + (isMaintain ? Helper.HTMLActionString(h.Id, "View", "View Record", "fa fa-eye", "ViewDetails('"+ h.Id +"')", "") : Helper.HTMLActionString(h.Id, "View", "View Record", "fa fa-eye disabledicon", "", ""))*/ (isMaintain ? Helper.HTMLActionString(h.Id, "Delete", "Delete Materil Details", "fa fa-trash-o", "DeleteDocument('','/FKM/MaintainFKMS/DeleteMaterial', {Id:"+h.Id+"}, 'tblMaterialList')", "") : Helper.HTMLActionString(h.Id, "Delete", "Delete Materil Details", "fa fa-trash-o disabledicon", "", "")) + (isMaintain ? Helper.HTMLActionString(h.Id, "Attachments", "File Attachments", "fa fa-paperclip attachments", "ViewAttachment('"+ h.Id +"','false')", "",false,"cursor:pointer;"+ (Helper.CheckAttachmentUpload("FKM106/"+h.Id) == true ? "color:#32CD32;" : string.Empty) +"") : Helper.HTMLActionString(h.Id, "Attachments", "File Attachments", "fa fa-paperclip attachments", "ViewAttachment('"+ h.Id +"','true')", "",false,"cursor:pointer;"+ (Helper.CheckAttachmentUpload("FKM106/"+h.Id) == true ? "color:#32CD32;" : string.Empty) +"")):""
                    }).ToList();

                    int newRecordId = 0;
                    var newRecord = new[] {
                                    "",
                                    "0",
                                    Helper.GenerateHidden(newRecordId, "NodeId", Convert.ToString(nodeId)),
                                    "false",
                                    Helper.HTMLAutoComplete(newRecordId, "EnablingMaterial","","", false,"","EnablingMaterial",false,"","","---Select Material---")+""+Helper.GenerateHidden(newRecordId, "hdnEnablingMaterial", Convert.ToString(newRecordId)),
                                    Helper.GenerateTextbox(newRecordId,"Remarks","","",false,"","500"),
                                    IsAssemblyOrNot ? Helper.HTMLActionString(newRecordId,"Add","Add New Record","fa fa-plus","SaveNewRecord();") : "",
                                };

                    res.Insert(0, newRecord);

                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = "0",
                        iTotalRecords = "0",
                        aaData = new string[0]
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = string.Empty,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = new string[0]
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetMaterialDetails(string HeaderId)
        {
            ViewBag.HeaderId = HeaderId;
            return PartialView("_MaterialListDataPartial");
        }
        //Code Comment because of remove FKM111 reference from FKM106
        //[HttpPost]
        //public ActionResult LoadMaterialListData(JQueryDataTableParamModel param)
        //{
        //    try
        //    {
        //        var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
        //        var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
        //        int StartIndex = param.iDisplayStart + 1;
        //        int EndIndex = param.iDisplayStart + param.iDisplayLength;
        //        string strWhere = string.Empty;
        //        int HeaderId = Convert.ToInt32(param.CTQHeaderId);

        //        string strSortOrder = string.Empty;
        //        string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
        //        string sortDirection = Convert.ToString(Request["sSortDir_0"]);
        //Code Comment because of remove FKM111 reference from FKM106
        //        strWhere = "1=1 and RefId In (" + HeaderId + ")";
        //        if (!string.IsNullOrWhiteSpace(param.sSearch))
        //        {
        //            strWhere += " and (DescriptionofItem like '%" + param.sSearch +
        //                       "%' or Category like '%" + param.sSearch +
        //                       "%' or Material like '%" + param.sSearch +
        //                       "%' or MaterialType like '%" + param.sSearch +
        //                       "%' or StructuralType like '%" + param.sSearch +
        //                       "%' or PipeNormalBore like '%" + param.sSearch + "%')";
        //        }
        //        if (!string.IsNullOrWhiteSpace(sortColumnName))
        //        {
        //            strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
        //        }
        //        var lstResult = db.SP_FKMS_MATERIAL_DETAILS
        //            (
        //                StartIndex,
        //                EndIndex,
        //                strSortOrder,
        //                strWhere
        //            ).ToList();

        //        var data = (from fx in lstResult
        //                    select new[]
        //                    {
        //                        Helper.HTMLActionString(fx.LineId,"Edit","Edit Record","fa fa-pencil-square-o","AddAndUpdateDetails('" + fx.LineId + "','" + fx.RefId + "','','edit');") + " " + Helper.HTMLActionString(fx.LineId,"Delete","Delete Record","fa fa-trash-o","DeleteLine('"+ fx.LineId+ "','" + fx.RefId +"');"),
        //                        Convert.ToString(fx.ROW_NO),
        //                        Convert.ToInt32(fx.FXRSrNo) > 0 ? "FXR-" + fx.Project.Substring(fx.Project.Length - 4) + "-" + fx.FXRSrNo.ToString().PadLeft(3, '0') : "",
        //                        Convert.ToString(fx.QtyofFixture),
        //                        Convert.ToString(fx.DescriptionofItem),
        //                        Convert.ToString(fx.Category),
        //                        Convert.ToString(fx.Material),
        //                        Convert.ToString(fx.MaterialType),
        //                        Convert.ToString(fx.LengthOD),
        //                        Convert.ToString(fx.WidthOD),
        //                        Convert.ToString(fx.Thickness),
        //                        Convert.ToString(fx.Qty),
        //                        Convert.ToString(fx.Wt),
        //                        Convert.ToString(fx.Area),
        //                        Convert.ToString(fx.Unit),
        //                        Convert.ToString(fx.ReUse ? "Yes" : "No"),
        //                        Convert.ToString(fx.ReqWt),
        //                        Convert.ToString(fx.FixRequiredDate.HasValue ? fx.FixRequiredDate.Value.ToShortDateString() : ""),
        //                        Convert.ToString(fx.MaterialReqDate.HasValue ? fx.MaterialReqDate.Value.ToShortDateString() : ""),
        //                        Convert.ToString(fx.LineId),
        //                        Convert.ToString(fx.RefId),
        //                    }).ToList();
        //        return Json(new
        //        {
        //            sEcho = Convert.ToInt32(param.sEcho),
        //            iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
        //            iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
        //            aaData = data,
        //            strSortOrder = strSortOrder,
        //            whereCondition = strWhere
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        return Json(new
        //        {
        //            sEcho = param.sEcho,
        //            iTotalRecords = "0",
        //            iTotalDisplayRecords = "0",
        //            aaData = ""
        //        }, JsonRequestBehavior.AllowGet);
        //    }

        //}

        [HttpPost]
        public ActionResult SaveNewMaterial(int NodeId, int MaterialId, string Remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                FKM106 objFKM106 = new FKM106();
                objFKM106.EnablingMaterialId = MaterialId;
                objFKM106.Remarks = Remarks;
                objFKM106.NodeId = NodeId;
                objFKM106.IsReady = false;
                objFKM106.CreatedBy = objClsLoginInfo.UserName;
                objFKM106.CreatedOn = DateTime.Now;
                db.FKM106.Add(objFKM106);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        //[HttpPost]
        //public ActionResult AddAndUpdateMaterialDetails(int LineId, int HeaderId, string taskActions)
        //{
        //    FKM111 objFKM111 = null;
        //    string fxrno = string.Empty;
        //    ViewBag.StrAction = taskActions;
        //    if (taskActions == "add")
        //    {
        //        objFKM111 = new FKM111();
        //        objFKM111.RefId = HeaderId;
        //        //comment this line because we need QtuOfFXR blank and user input evey new material calculations
        //        //string QtyFXR = generalMethod.getFXRData(HeaderId.ToString(), LineId.ToString(), "fkms");
        //        //if (QtyFXR != "")
        //        //{ objFKM111.QtyofFixture = Int32.Parse(QtyFXR); }
        //    }
        //    else if (taskActions == "edit")
        //    {
        //        objFKM111 = db.FKM111.Where(x => x.LineId == LineId && x.RefId == HeaderId).FirstOrDefault();
        //        ViewBag.Category = objFKM111.Category;
        //        ViewBag.Material = objFKM111.Material;
        //        ViewBag.MaterialType = objFKM111.MaterialType;
        //        ViewBag.Unit = objFKM111.Unit;
        //        ViewBag.Unit2 = objFKM111.Unit2;
        //        ViewBag.StructuralType = objFKM111.StructuralType;
        //        ViewBag.PipeNormalBore = objFKM111.PipeNormalBore;
        //        ViewBag.PipeSchedule = objFKM111.PipeSchedule;
        //        if (objFKM111.FXRSrNo > 0)
        //        {
        //            ViewBag.AutoPartNo = "FXR-" + objFKM111.Project.Substring(objFKM111.Project.Length - 4) + "-" + objFKM111.FXRSrNo.ToString().PadLeft(3, '0');
        //        }
        //        //string QtyFXR = generalMethod.getFXRData(HeaderId.ToString(), LineId.ToString(), "fkms");
        //        //if (QtyFXR != "")
        //        //{ objFKM111.QtyofFixture = Int32.Parse(QtyFXR); }
        //    }

        //    List<string> lstFXRCategory = clsImplementationEnum.GetFXRCategory().ToList();
        //    ViewBag.lstFXRCategory = lstFXRCategory.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

        //    List<string> lstFXRMaterialType = clsImplementationEnum.GetFXRMaterialType().ToList();
        //    ViewBag.lstFXRMaterialType = lstFXRMaterialType.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

        //    List<string> lstFXRStructuralType = clsImplementationEnum.GetFXRStructuralType().ToList();
        //    ViewBag.lstFXRStructuralType = lstFXRStructuralType.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

        //    List<string> lstBore = db.FXR005.Select(x => x.Bore).Distinct().ToList();
        //    ViewBag.lstBore = lstBore.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

        //    List<string> lstSchedule = db.FXR005.Select(x => x.Schedule).Distinct().ToList();
        //    ViewBag.lstSchedule = lstSchedule.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

        //    return PartialView("_MaterialDetailsPartial", objFKM111);
        //}

        [HttpPost]
        public JsonResult DeleteMaterial(int Id)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsgWithStatus = new clsHelper.ResponseMsgWithStatus();
            FKM106 objFKM106 = db.FKM106.Where(x => x.Id == Id).FirstOrDefault();
            if (objFKM106 != null)
            {

                //delete document before deleting material
                string deletefolderPath = "FKM106/" + objFKM106.Id;
                var lstTVL011RefDoc = (new clsFileUpload()).GetDocuments(deletefolderPath, true).Select(x => x.Name).ToList();
                foreach (var filename in lstTVL011RefDoc)
                {
                    (new clsFileUpload()).DeleteFileAsync(deletefolderPath, filename.ToString());
                }

                db.FKM106.Remove(objFKM106);
                db.SaveChanges();
                objResponseMsgWithStatus.Key = true;
                objResponseMsgWithStatus.Value = "Material deleted successfully.";
            }
            else
            {
                objResponseMsgWithStatus.Key = false;
                objResponseMsgWithStatus.Value = "No data found";
            }

            return Json(objResponseMsgWithStatus, JsonRequestBehavior.AllowGet);
        }
        //Code Comment because of remove FKM111 reference from FKM106
        //[HttpPost]
        //public ActionResult SaveSubCategory(FormCollection fc)
        //{
        //    clsHelper.ResponseMsgWithStatus objResponseMsgWithStatus = new clsHelper.ResponseMsgWithStatus();
        //    try
        //    {
        //        if (fc != null)
        //        {
        //            int headerId = Convert.ToInt32(fc["RefId"]);

        //            FKM111 objFKM111 = null;
        //            if (fc["strAction"] == "add")
        //            {
        //                string Project = fc["Project"];
        //                objFKM111 = new FKM111();
        //                objFKM111.RefId = headerId;
        //                objFKM111.Project = Project;
        //                var MaxFxrNo = db.FKM111.Where(x => x.Project == Project).OrderByDescending(x => x.FXRSrNo).Select(x => x.FXRSrNo).FirstOrDefault();
        //                objFKM111.FXRSrNo = Convert.ToInt32(MaxFxrNo) + 1;
        //            }
        //            else
        //            {
        //                int lineId = Convert.ToInt32(fc["LineId"]);
        //                objFKM111 = db.FKM111.Where(x => x.LineId == lineId && x.RefId == headerId).FirstOrDefault();
        //            }

        //            objFKM111.QtyofFixture = Convert.ToInt32(fc["QtyofFixture"]);
        //            objFKM111.DescriptionofItem = fc["DescriptionofItem"];
        //            objFKM111.Category = fc["Category"];
        //            objFKM111.Material = fc["Material"];
        //            objFKM111.MaterialType = fc["MaterialType"];
        //            objFKM111.LengthOD = fc["LengthOD"] == "" ? Decimal.Parse("0") : Convert.ToDecimal(fc["LengthOD"], CultureInfo.InvariantCulture);
        //            objFKM111.WidthOD = fc["WidthOD"] == "" ? Decimal.Parse("0") : Convert.ToDecimal(fc["WidthOD"], CultureInfo.InvariantCulture);
        //            objFKM111.Thickness = fc["Thickness"] == "" ? Decimal.Parse("0") : Convert.ToDecimal(fc["Thickness"], CultureInfo.InvariantCulture);
        //            objFKM111.Qty = fc["Qty"] == "" ? Decimal.Parse("0") : Convert.ToDecimal(fc["Qty"], CultureInfo.InvariantCulture);
        //            objFKM111.Wt = fc["Wt"] == "" ? Decimal.Parse("0") : Convert.ToDecimal(fc["Wt"], CultureInfo.InvariantCulture);
        //            objFKM111.Area = fc["Area"] == "" ? Decimal.Parse("0") : Convert.ToDecimal(fc["Area"], CultureInfo.InvariantCulture);
        //            objFKM111.Unit = fc["Unit"];
        //            objFKM111.ReqWt = fc["ReqWt"] == "" ? Decimal.Parse("0") : Convert.ToDecimal(fc["ReqWt"], CultureInfo.InvariantCulture);
        //            objFKM111.Unit2 = fc["Unit2"];
        //            string FxrReqDt = fc["FixRequiredDate"] != null ? fc["FixRequiredDate"].ToString() : "";
        //            objFKM111.FixRequiredDate = DateTime.ParseExact(FxrReqDt, @"d/M/yyyy", CultureInfo.InvariantCulture);
        //            string FxrMatDt = fc["MaterialReqDate"] != null ? fc["MaterialReqDate"].ToString() : "";
        //            objFKM111.MaterialReqDate = DateTime.ParseExact(FxrMatDt, @"d/M/yyyy", CultureInfo.InvariantCulture);

        //            objFKM111.StructuralType = fc["StructuralType"] == "" ? null : fc["StructuralType"];
        //            objFKM111.Size = Convert.ToString(fc["Size"]);
        //            objFKM111.PipeNormalBore = fc["PipeNormalBore"] == "" ? null : fc["PipeNormalBore"];
        //            objFKM111.PipeSchedule = fc["PipeSchedule"] == "" ? null : fc["PipeSchedule"];
        //            var tempReUse = Convert.ToString(fc["txtReUse"]);
        //            if (tempReUse == "True")
        //            {
        //                objFKM111.ReUse = true;
        //            }
        //            else
        //            {
        //                objFKM111.ReUse = false;
        //            }


        //            if (fc["strAction"] == "add")
        //            {
        //                objFKM111.CreatedBy = objClsLoginInfo.UserName;
        //                objFKM111.CreatedOn = DateTime.Now;
        //                db.FKM111.Add(objFKM111);
        //                objResponseMsgWithStatus.Value = "Fixture's saved successfully";
        //            }
        //            else
        //            {
        //                //int linesId = Convert.ToInt32(fc["RefId"]);
        //                //int qtyoffxr = Convert.ToInt32(fc["QtyofFixture"]);
        //                //List<FKM111> lstobjFKM111 = db.FKM111.Where(x => x.RefId == headerId).ToList();
        //                //if (lstobjFKM111 != null && lstobjFKM111.Count > 0)
        //                //{
        //                //    lstobjFKM111.ForEach(x => x.QtyofFixture = qtyoffxr);
        //                //    db.Entry(lstobjFKM111).State = System.Data.Entity.EntityState.Modified;
        //                //}

        //                objFKM111.EditedBy = objClsLoginInfo.UserName;
        //                objFKM111.EditedOn = DateTime.Now;
        //                objResponseMsgWithStatus.Value = "Fixture's update successfully";
        //            }

        //            objResponseMsgWithStatus.HeaderId = headerId;
        //            db.SaveChanges();

        //            objResponseMsgWithStatus.Key = true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        objResponseMsgWithStatus.Key = false;
        //        objResponseMsgWithStatus.Value = ex.Message;
        //    }
        //    return Json(objResponseMsgWithStatus);
        //}

        //[HttpPost]
        //public JsonResult DeleteMaterialDetails(int LineId, int HeaderId)
        //{
        //    clsHelper.ResponseMsgWithStatus objResponseMsgWithStatus = new clsHelper.ResponseMsgWithStatus();
        //    FKM111 objFKM111 = db.FKM111.Where(x => x.LineId == LineId && x.RefId == HeaderId).FirstOrDefault();
        //    if (objFKM111 != null)
        //    {
        //        db.FKM111.Remove(objFKM111);
        //        db.SaveChanges();
        //        objResponseMsgWithStatus.Key = true;
        //        objResponseMsgWithStatus.Value = "Material deleted successfully.";
        //    }
        //    else
        //    {
        //        objResponseMsgWithStatus.Key = false;
        //        objResponseMsgWithStatus.Value = "No data found";
        //    }

        //    return Json(objResponseMsgWithStatus, JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        public ActionResult UpdateEnablingMaterial(int MaterialId, bool isChk)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                FKM106 objFKM106 = db.FKM106.Where(x => x.Id == MaterialId).FirstOrDefault();
                if (objFKM106 != null)
                {
                    objFKM106.IsReady = isChk;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Key = false;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Revert Dynamic
        [HttpPost]
        public ActionResult GetRevertDynamicPage(int HeaderId)
        {
            var Node = db.FKM202.Where(x => x.HeaderId == HeaderId && x.PlannerStructure != null).FirstOrDefault();
            if (Node != null)
            {
                string assemblyType = clsImplementationEnum.FKMSNodeType.Equipment.GetStringValue();
                var assid = db.FKM203.Where(x => x.NodeType.ToLower() == assemblyType.ToLower()).Select(x => x.Id).FirstOrDefault();
                ViewBag.IsInitList = db.FKM202.Where(x => x.HeaderId == HeaderId && x.NodeTypeId == assid)
                .Select(x => new sourceProjectDetails
                {
                    nodeName = x.NodeName,
                    nodeId = x.NodeId
                }).ToList();

                ViewBag.objInitParent = FN_GETXMLCOLUMNInitParent(Node.NodeId).ToList();
                ViewBag.objPageParent = FN_GETXMLCOLUMNPageParent(Node.NodeId).ToList();
                ViewBag.objParents = FN_GETXMLCOLUMNParent(Node.NodeId).ToList();
                ViewBag.objChilds = FN_GETXMLCOLUMNChild(Node.NodeId).ToList();
                ViewBag.NodeId = Node.NodeId;
                ViewBag.Nodename = Node.NodeName;
            }
            return PartialView("_RevertDynamicPartial");
        }

        #endregion

        #region SOB Key Details Observation 15487 On 04-07-2018
        [HttpPost]
        public ActionResult GetSOBKeyDetails(int NodeId)
        {
            ViewBag.NodeId = NodeId;
            return PartialView("_SOBKeyDetails");
        }

        [HttpPost]
        public ActionResult LoadSOBKeyListData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                int NodeId = Convert.ToInt32(param.CTQHeaderId);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                strWhere = "1=1 and NodeId = " + NodeId;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (SOBKey like '%" + param.sSearch +
                               "%' or Status like '%" + param.sSearch +
                               "%' or CONVERT(nvarchar(20), CreatedOn, 103) like '%" + param.sSearch +
                               "%' or ErrorMsg like '%" + param.sSearch + "%')";
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_FKMS_GET_SOB_KEY_LIST
                    (
                        StartIndex,
                        EndIndex,
                        strSortOrder,
                        strWhere
                    ).ToList();

                var data = (from fx in lstResult
                            select new[]
                            {
                                Convert.ToString(fx.ROW_NO),
                                Convert.ToString(fx.SOBKey),
                                Convert.ToString(fx.Status),
                                Convert.ToString(fx.ErrorMsg),
                                Convert.ToString(fx.CreatedOn),
                                Convert.ToString(fx.Id),
                            }).ToList();
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }
        #endregion

        #region Project Kit SOB Status Observation 16019 On 06-08-2018
        [HttpPost]
        public ActionResult GetProjectbySOBKeyStatus(string keyStatusProject)
        {
            //int ProjectTaskId = Convert.ToInt32(ProjectCode);
            KitDetails objResponseMsg = new KitDetails();
            try
            {
                var objFKM202 = db.FKM201.Where(x => x.Project == keyStatusProject).FirstOrDefault();
                if (objFKM202 != null)
                {
                    // observation 16019 Default SOB Status Update Project Load
                    UpdateReadyForKitStatusForAllNode(objFKM202.Project);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Project Kit SOB Status Updated";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Project not available.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        [HttpPost]
        public JsonResult GetDepartments(string search)
        {
            dynamic items = null;
            try
            {
                //items = db.FN_GET_LOCATIONWISE_DEPARTMENT("", search).Select(x => new { id = x.t_dimx, text = x.t_dimx + "-" + x.t_desc, }).Take(10).ToList();
                items = db.FN_GET_LOCATIONWISE_DEPARTMENT_PDIN("", search, 1).Select(x => new { id = x.t_dimx, text = x.t_dimx + "-" + x.t_desc, }).ToList();

                return Json(items, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdateMaterialHeader(int emId, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            FKM106 objfkm = new FKM106();
            try
            {
                var objfkm106 = db.FKM106.Where(i => i.Id == emId).FirstOrDefault();

                if (objfkm106 != null && columnValue != string.Empty)
                {
                    objfkm106.Remarks = columnValue;
                    objfkm106.EditedBy = objClsLoginInfo.UserName;
                    objfkm106.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Updated Successfully";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Updated Successfully";
                }
                //else if (objfkm106 != null)
                //{
                //    db.FKM106.Remove(objfkm106);
                //}
                //else
                //{
                //    objfkm.NodeId = nodeId;
                //    objfkm.EnablingMaterialId = emId;
                //    objfkm.Remarks = columnValue;
                //    objfkm.CreatedBy = objClsLoginInfo.UserName;
                //    objfkm.CreatedOn = DateTime.Now;
                //    db.FKM106.Add(objfkm);
                //    objResponseMsg.Key = true;
                //    objResponseMsg.Value = "Inserted Successfully";
                //}
                //db.SaveChanges();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateDocument(int nodeId, int docNo, bool isChk)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            FKM107 objfkm = new FKM107();
            try
            {
                var objdoc = db.PDN002.Where(x => x.LineId == docNo).FirstOrDefault().DocumentNo;

                var objfkm107 = db.FKM107.Where(i => i.NodeId == nodeId && i.DocumentNo == objdoc).FirstOrDefault();

                if (isChk != true)
                {
                    if (objfkm107 != null)
                    {
                        db.FKM107.Remove(objfkm107);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Already Exists";
                    }
                }
                else
                {
                    if (objfkm107 != null)
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Already Exists";
                    }
                    else
                    {
                        objfkm.NodeId = nodeId;
                        objfkm.DocumentNo = objdoc;
                        objfkm.CreatedBy = objClsLoginInfo.UserName;
                        objfkm.CreatedOn = DateTime.Now;
                        db.FKM107.Add(objfkm);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Inserted Successfully";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetPartDetails(int NodeId, string BomQty)
        {
            bool IsPLT = false;
            var isplate = db.FKM202.Where(x => x.NodeId == NodeId).FirstOrDefault().ProductType;
            if (isplate == clsImplementationEnum.NodeTypes.PLT.GetStringValue())
            {
                IsPLT = true;
            }

            PLTPartDetail objPartDetails = new PLTPartDetail();
            FKM202 objFKM202 = db.FKM202.Where(x => x.NodeId == NodeId).FirstOrDefault();

            objPartDetails.Project = objFKM202.Project;
            objPartDetails.Key = objFKM202.NodeKey_Init;
            objPartDetails.Findnumber = objFKM202.FindNo_Init;
            objPartDetails.BomQty = BomQty;

            ViewBag.IsPLT = IsPLT;
            ViewBag.HeaderId = NodeId;
            return PartialView("_GetPartDetails", objPartDetails);
        }

        #region PLT Allocation Manually

        //[SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult AllocationDetails(int? NodeId)
        {
            ViewBag.NodeId = NodeId;
            return View();
        }

        [HttpPost]
        public ActionResult GetPartAllocationDetails(int NodeId)
        {
            var objFKM202 = db.FKM202.Where(x => x.NodeId == NodeId).FirstOrDefault();
            string PLT = clsImplementationEnum.NodeTypes.PLT.GetStringValue().ToLower();
            double chkQty = 0;
            double availableQty = 0;
            if (objFKM202 != null)
            {
                if (objFKM202.ProductType.ToLower().ToString() != PLT)
                {
                    //string query1 = "select t_cono from " + LNLinkedServer + ".dbo.ttpctm110175 where t_cprj= '" + objFKM202.Project + "'";
                    //string contract = db.Database.SqlQuery<string>(query1).FirstOrDefault();
                    //string query = "select ISNULL(sum(t_qhnd), 0) as AvlQty from " + LNLinkedServer + ".dbo.tltlnt500175 where t_cono = '" + contract + "' and LTRIM(RTRIM(t_item)) = '" + objFKM202.NodeKey.Trim() + "'";
                    //availableQty = db.Database.SqlQuery<double>(query).FirstOrDefault();

                    //avl qty query updated as per observation id: 15900
                    List<SP_FKMS_GET_NON_PLT_WAREHOUSE_LIST_Result> componentWarehouselist = GetWarehouseList(objFKM202.Project.Trim(), objFKM202.NodeKey.Trim(), objClsLoginInfo.Location.Trim(), db);
                    if (componentWarehouselist != null)
                        availableQty = Convert.ToDouble(componentWarehouselist.Sum(u => u.finqhnd));

                    //chkQty = Convert.ToDouble(db.FKM109.Where(x => x.Project == objFKM202.Project && x.NodeKey == objFKM202.NodeKey).ToList().Sum(x => x.AllocatedQty));
                    //availableQty = availableQty - chkQty;
                }
                else
                {
                    var listforPLT = db.SP_FKMS_GETPLTALLOCATEDETAILS(objFKM202.Project).ToList();
                    if (listforPLT.Any(x => x.Partno == objFKM202.FindNo))
                    {
                        chkQty = Convert.ToDouble(db.FKM108.Where(x => x.FindNo == objFKM202.FindNo && x.Project == objFKM202.Project).ToList().Sum(x => x.AllocatedQty));
                        availableQty = Convert.ToDouble(listforPLT.Where(x => x.Partno == objFKM202.FindNo).ToList().Sum(i => i.Qty));
                        availableQty = availableQty - chkQty;
                    }
                }
            }

            ViewBag.NodeId = NodeId;
            ViewBag.availableQty = availableQty;
            return PartialView("_GetPartAllocationDetails");
        }


        [HttpPost]
        public ActionResult LoadPartAllowcationData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string PLT = clsImplementationEnum.NodeTypes.PLT.GetStringValue().ToLower();

                UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
                var UserRole = objUserRoleAccessDetails.UserRole;

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                int nodeId = Convert.ToInt32(param.Nodeid);

                var lstpartdetails = db.FKM202.Where(x => x.NodeId == nodeId).FirstOrDefault();

                if (lstpartdetails != null)
                {
                    //listASMColors(lstpartdetails.HeaderId);

                    string whereCondition = "1=1";

                    string[] columnName = { "FindNo", "ParentNodeKey", "PLTQty", "NPLTQty", "NodeKey", "NoOfPieces", "ProductType", "ErrorMsg" };

                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                    string strSortOrder = string.Empty;
                    if (!string.IsNullOrWhiteSpace(sortColumnName))
                    {
                        strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                    }

                    var lstPam = db.SP_FKMS_GETPLTALLOWCATIONDETAILS(lstpartdetails.Project, lstpartdetails.FindNo, lstpartdetails.ProductType, StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                    int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                    var res = (from h in lstPam
                               select new[] {
                               BorderLeftColorFlag(h.NodeId,h.FindNo,h.NodeColor),
                                   //Convert.ToString(h.FindNo),
                                   Convert.ToString(h.Project),
                               Convert.ToString(h.ParentNodeKey),
                               Convert.ToString(h.NodeKey),
                               Convert.ToString(h.NoOfPieces),
                               h.ProductType.ToLower().ToString() != PLT ? Convert.ToString(h.NPLTQty != null ? h.NPLTQty: 0): Convert.ToString(h.PLTQty != null ? h.PLTQty: 0),
                               Convert.ToString(h.ProductType),
                               Convert.ToString(h.ErrorMsg != null? h.ErrorMsg: "-"),
                               //"<center> " + "<div class='btn-group btn-group-xs btn-group-solid'> " + PLTSetColorFlag(h.NodeId) == clsImplementationEnum.ColorFlagForFKMS.Red.GetStringValue() == true ? " <button type='button' class='btn green'> Allocate </button> </div> </center>":PLTSetColorFlag(h.NodeId) == clsImplementationEnum.ColorFlagForFKMS.Green.GetStringValue() ? " <button type='button' class='btn red'> DeAllocate </button>":" <button type='button' class='btn red'> DeAllocate </button> <button type='button' class='btn blue'> ReAllocate </button>" + "</div> </center>"
                               //(PLTSetColorFlag(h.NodeId) == clsImplementationEnum.ColorFlagForFKMS.Red.GetStringValue()) == true ? "<center> <div class='btn-group btn-group-xs btn-group-solid'> "+ GeneratePartButtonNew(h.NodeId, "Allocate", "Allocate", "btn green", "Allocate("+h.NodeId+");" ) +" </div> </center>":(PLTSetColorFlag(h.NodeId) == clsImplementationEnum.ColorFlagForFKMS.Green.GetStringValue())?"<center> <div class='btn-group btn-group-xs btn-group-solid'> "+ GeneratePartButtonNew(h.NodeId, "DeAllocate", "DeAllocate", "btn red", "DeAllocate("+h.NodeId+");" ) +" </div> </center>":(h.ErrorMsg != null && h.NPLTQty == 0)?"<center> <div class='btn-group btn-group-xs btn-group-solid'> "+ GeneratePartButtonNew(h.NodeId, "ReTry", "ReTry", "btn blue", "ReAllocate("+h.NodeId+");" ) +" </div> </center>":"<center> <div class='btn-group btn-group-xs btn-group-solid'> "+ GeneratePartButtonNew(h.NodeId, "DeAllocate", "DeAllocate", "btn red", "DeAllocate("+h.NodeId+");" ) +" "+ GeneratePartButtonNew(h.NodeId, "ReAllocate", "ReAllocate", "btn blue", "ReAllocate("+h.NodeId+");" ) +" </div> </center>"
                               (UserRole.ToLower() == clsImplementationEnum.UserRoleName.PLNG3.GetStringValue().ToLower() ? ((h.NodeColor == clsImplementationEnum.ColorFlagForFKMS.Red.GetStringValue()) == true ? "<center> <div class='btn-group btn-group-xs btn-group-solid'> "+ GeneratePartButtonNew(h.NodeId, "Allocate", "Allocate", "btn green", "Allocate("+h.NodeId+","+ Convert.ToString(h.PLTQty != null ? h.PLTQty: 0) + ");" ) +" </div> </center>":(h.NodeColor == clsImplementationEnum.ColorFlagForFKMS.Green.GetStringValue())?"<center> <div class='btn-group btn-group-xs btn-group-solid'> "+ GeneratePartButtonNew(h.NodeId, "DeAllocate", "DeAllocate", "btn red", "DeAllocate("+h.NodeId+","+ Convert.ToString(h.PLTQty != null ? h.PLTQty: 0) +");" ) +" </div> </center>":(h.ErrorMsg != null && h.NPLTQty == 0)?"<center> <div class='btn-group btn-group-xs btn-group-solid'> "+ GeneratePartButtonNew(h.NodeId, "ReTry", "ReTry", "btn blue", "ReAllocate("+h.NodeId+","+ Convert.ToString(h.PLTQty != null ? h.PLTQty: 0) +");" ) +" </div> </center>":"<center> <div class='btn-group btn-group-xs btn-group-solid'> "+ GeneratePartButtonNew(h.NodeId, "DeAllocate", "DeAllocate", "btn red", "DeAllocate("+h.NodeId+","+ Convert.ToString(h.PLTQty != null ? h.PLTQty: 0) +");" ) +" "+ GeneratePartButtonNew(h.NodeId, "ReAllocate", "ReAllocate", "btn blue", "ReAllocate("+h.NodeId+","+ Convert.ToString(h.PLTQty != null ? h.PLTQty: 0) +");" ) +" </div> </center>") : "")
                               //public static string GenerateGridButtonNew(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
                               //(PLTSetColorFlag(h.NodeId) == clsImplementationEnum.ColorFlagForFKMS.Red.GetStringValue()) == true ? "<center> <div class='btn-group btn-group-xs btn-group-solid'> <input type='button' class='btn green' value='Allocate' /> </div> </center>":(PLTSetColorFlag(h.NodeId) == clsImplementationEnum.ColorFlagForFKMS.Green.GetStringValue())?"<center> <div class='btn-group btn-group-xs btn-group-solid'> <input type='button' class='btn red' value='DeAllocate'/> </div> </center>":"<center> <div class='btn-group btn-group-xs btn-group-solid'> <input type='button' class='btn red' value='DeAllocate' /> <input type='button' class='btn blue' value='ReAllocate' /> </div> </center>"
                                }).ToList();

                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = "0",
                        iTotalRecords = "0",
                        aaData = new string[0]
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = string.Empty,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = new string[0]
                }, JsonRequestBehavior.AllowGet);
            }

        }

        public clsHelper.ResponseMsgWithStatus Deallocation(int nodeid, string psno, string location, decimal reqQty = 0)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            string PLT = clsImplementationEnum.NodeTypes.PLT.GetStringValue().ToLower();
            objResponseMsg.Key = false;
            objResponseMsg.Value = "Records Not Found";
            var ParentNodeId = 0;
            var HeaderId = 0;
            string ErrorMessage = string.Empty;
            try
            {
                if (nodeid != 0)
                {
                    var chkfkm202 = db.FKM202.Where(x => x.NodeId == nodeid).FirstOrDefault();
                    ParentNodeId = chkfkm202.ParentNodeId.HasValue ? chkfkm202.ParentNodeId.Value : 0;
                    HeaderId = chkfkm202.HeaderId;

                    if (chkfkm202.ProductType.ToLower().ToString() != PLT)
                    {
                        if (db.FKM109.Any(i => i.NodeId == nodeid))
                        {
                            var allocatedFKM109List = db.FKM109.Where(u => u.NodeId == nodeid).ToList();

                            foreach (var objAllocatedFKM109 in allocatedFKM109List)
                            {
                                if (objAllocatedFKM109.AllocatedQty > 0)
                                {
                                    bool IsDeallocationSuccess = NPLTDeallocation(objAllocatedFKM109, psno, location, db, ref ErrorMessage);
                                    if (IsDeallocationSuccess)
                                    {
                                        if (db.FKM109.Any(x => x.Id == objAllocatedFKM109.Id))
                                        {
                                            db.FKM109.Remove(objAllocatedFKM109);
                                            db.SaveChanges();
                                        }
                                        objResponseMsg.Key = true;
                                        objResponseMsg.Value = "Successfully Deallocated";
                                    }
                                    else
                                    {
                                        objResponseMsg.Key = false;
                                        objResponseMsg.Value = "Error in deallocate";
                                    }
                                }
                                else
                                {
                                    db.FKM109.Remove(objAllocatedFKM109);
                                    db.SaveChanges();
                                    objResponseMsg.Key = true;
                                    objResponseMsg.Value = "Successfully Deallocated";
                                }
                            }
                        }
                    }
                    else
                    {
                        if (reqQty > 0)
                        {
                            var objFKM108 = db.FKM108.Where(i => i.NodeId == nodeid && i.AllocatedQty == reqQty).FirstOrDefault();
                            if (objFKM108 != null)
                            {
                                db.FKM108.Remove(objFKM108);
                                db.SaveChanges();
                                objResponseMsg.Key = true;
                                objResponseMsg.Value = "Successfully Deallocated";
                            }
                        }
                        else
                        {
                            var listFKM108 = db.FKM108.Where(i => i.NodeId == nodeid).ToList();
                            if (listFKM108.Count() > 0)
                            {
                                db.FKM108.RemoveRange(listFKM108);
                                db.SaveChanges();
                                objResponseMsg.Key = true;
                                objResponseMsg.Value = "Successfully Deallocated";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            finally
            {
                SetASMPartsColors(HeaderId, ParentNodeId);
            }

            return objResponseMsg;
        }

        public clsHelper.ResponseMsgWithStatus allocation(int nodeid, decimal availqty)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            string PLT = clsImplementationEnum.NodeTypes.PLT.GetStringValue().ToLower();
            string element = "";
            string fullKitNo = string.Empty;
            objResponseMsg.Key = false;
            objResponseMsg.Value = "Records Not Found";
            var ParentNodeId = 0;
            var HeaderId = 0;
            try
            {
                if (nodeid != 0)
                {
                    var chkfkm202 = db.FKM202.Where(x => x.NodeId == nodeid).FirstOrDefault();
                    ParentNodeId = chkfkm202.ParentNodeId.HasValue ? chkfkm202.ParentNodeId.Value : 0;
                    HeaderId = chkfkm202.HeaderId;
                    var parentnop = db.FKM202.Where(x => x.NodeId == chkfkm202.ParentNodeId).FirstOrDefault();
                    decimal? allocateQty = (chkfkm202.NoOfPieces != null ? chkfkm202.NoOfPieces : 1) * (parentnop.NoOfPieces != null ? parentnop.NoOfPieces : 1);
                    if (chkfkm202 != null && chkfkm202.NoOfPieces != 0)
                    {
                        if (chkfkm202.ProductType.ToLower().ToString() != PLT)
                        {
                            var fkm202Kit = db.FKM202.Where(u => u.NodeId == chkfkm202.ParentNodeId).FirstOrDefault();
                            if (fkm202Kit != null)
                            {
                                if (!string.IsNullOrEmpty(fkm202Kit.DeleverTo))
                                {
                                    fullKitNo = fkm202Kit.NodeKey + "-" + fkm202Kit.NodeId; //ParentNodeKey + ParentNodeId
                                    var ParentDeptCode = FN_GET_PARENTDEPT(fkm202Kit.DeleverTo);
                                    string workCenter = ParentDeptCode != null ? ParentDeptCode : string.Empty;
                                    //string workCenter = fkm202Kit.DeleverTo != null ? fkm202Kit.DeleverTo : string.Empty;
                                    decimal kitQty = Convert.ToDecimal(fkm202Kit.NoOfPieces != 0 ? fkm202Kit.NoOfPieces : 0);

                                    decimal requiredQty = Convert.ToDecimal(chkfkm202.NoOfPieces != 0 ? chkfkm202.NoOfPieces : 0);
                                    requiredQty = requiredQty * kitQty;

                                    if (availqty >= requiredQty)
                                    {
                                        List<FKM109> objFKM109List = new List<FKM109>();
                                        //List<tltlnt500175> componentWarehouselist = GetWarehouseList(chkfkm202.Project, chkfkm202.NodeKey, db);

                                        //added below as per observation id: 15900
                                        List<SP_FKMS_GET_NON_PLT_WAREHOUSE_LIST_Result> componentWarehouselist = GetWarehouseList(chkfkm202.Project.Trim(), chkfkm202.NodeKey.Trim(), objClsLoginInfo.Location.Trim(), db);

                                        #region WAREHOUSE LIST

                                        if (componentWarehouselist.Count > 0)
                                        {
                                            foreach (var wrh in componentWarehouselist)
                                            {
                                                bool IsError = false;
                                                decimal allocatedQty = 0;
                                                string warehouse = wrh.t_cwar;
                                                decimal t_qhnd = Convert.ToDecimal(wrh.finqhnd);

                                                decimal remainQty = Convert.ToDecimal(requiredQty - t_qhnd); //2 - 5
                                                if (remainQty >= 0)//1=1
                                                {
                                                    allocatedQty = t_qhnd;
                                                    requiredQty = remainQty;
                                                }
                                                else
                                                {
                                                    allocatedQty = requiredQty;
                                                    requiredQty = 0;
                                                }

                                                #region WEB SERVICE CALL

                                                allocationservice objService = new allocationservice();
                                                objService.quantitySpecified = true;
                                                objService.element = !string.IsNullOrEmpty(chkfkm202.Element) ? chkfkm202.Element : "";
                                                objService.location = objClsLoginInfo.Location;
                                                objService.project = chkfkm202.Project;
                                                objService.quantity = allocatedQty;
                                                objService.item = new string(' ', 9) + chkfkm202.NodeKey.Trim();
                                                objService.warehouse = warehouse;
                                                objService.workcenter = workCenter;
                                                objService.fullkitNo = fullKitNo;
                                                objService.logname = objClsLoginInfo.UserName;
                                                objService.budgetLine = (long)Convert.ToDouble(chkfkm202.FindNo);
                                                objService.budgetLineSpecified = true;
                                                objService.nodeId = nodeid;
                                                objService.nodeKey = chkfkm202.NodeKey;
                                                objService.psno = objClsLoginInfo.UserName;
                                                objService.findno = chkfkm202.FindNo;

                                                InvokeAllocationService(objService, ref objFKM109List, ref IsError);

                                                #endregion

                                                if (requiredQty == 0)
                                                    break;
                                            }

                                            if (objFKM109List.Count > 0)
                                            {
                                                db.FKM109.AddRange(objFKM109List);
                                                db.SaveChanges();
                                                if (objFKM109List.Count(i => i.AllocatedQty != 0) == objFKM109List.Count())
                                                {
                                                    var removeErrorEntry = db.FKM109.Where(i => i.NodeId == nodeid && i.AllocatedQty == 0).ToList();
                                                    db.FKM109.RemoveRange(removeErrorEntry);
                                                    db.SaveChanges();
                                                }
                                            }

                                            objResponseMsg.Key = true;
                                            objResponseMsg.Value = "Successfully Allocated";
                                        }
                                        else
                                        {
                                            objResponseMsg.Key = false;
                                            objResponseMsg.Value = "Warehouse not available";
                                        }

                                        #endregion
                                    }
                                    else
                                    {
                                        objResponseMsg.Key = false;
                                        objResponseMsg.Value = "Qty not available";
                                    }
                                }
                                else
                                {
                                    objResponseMsg.Key = false;
                                    objResponseMsg.Value = "Deliver to is required for kit";
                                }
                            }
                        }
                        else
                        {
                            if (availqty >= allocateQty)
                            {
                                //FKM108 add data
                                var listforPLT = db.SP_FKMS_GETPLTALLOCATEDETAILS(chkfkm202.Project).ToList();
                                var lstfkm108 = db.FKM108.Where(x => x.Project == chkfkm202.Project).ToList();
                                List<FKM108> lstFKM108 = new List<FKM108>();
                                List<SP_FKMS_GETPLTALLOCATEDETAILS_Result> lstplt = new List<SP_FKMS_GETPLTALLOCATEDETAILS_Result>();

                                double availableQty = 0;
                                double requiredQty = 0;
                                double allocatedExistQty = 0;
                                double finalAvailableQty = 0;
                                double chkQty = 0;

                                if (listforPLT.Any(x => x.Partno == chkfkm202.FindNo))
                                {
                                    chkQty = Convert.ToDouble(lstfkm108.Where(x => x.FindNo == chkfkm202.FindNo).ToList().Sum(x => x.AllocatedQty));
                                    availableQty = Convert.ToDouble(listforPLT.Where(x => x.Partno == chkfkm202.FindNo).ToList().Sum(i => i.Qty));
                                    availableQty = availableQty - chkQty;
                                }

                                requiredQty = chkfkm202.NoOfPieces != null ? Convert.ToDouble(chkfkm202.NoOfPieces) : 0;
                                var objParent = db.FKM202.Where(i => i.NodeId == chkfkm202.ParentNodeId).FirstOrDefault();
                                if (objParent != null)
                                {
                                    requiredQty = requiredQty * (objParent.NoOfPieces != null ? Convert.ToDouble(objParent.NoOfPieces) : 0);
                                }
                                //if (lstFKM108.Any(x => x.Project == sourceProject && x.NodeKey == item.NodeKey && x.FindNo == item.FindNo))
                                //{
                                //    allocatedExistQty = Convert.ToDouble(lstFKM108.Where(x => x.Project == sourceProject && x.NodeKey == item.NodeKey && x.FindNo == item.FindNo).ToList().Sum(c => c.AllocatedQty));
                                //}
                                finalAvailableQty = availableQty - allocatedExistQty;

                                if (finalAvailableQty >= requiredQty)
                                {
                                    lstplt = listforPLT.Where(x => x.Partno == chkfkm202.FindNo).ToList();
                                    foreach (var lstquantity in lstplt)
                                    {

                                        foreach (var item in lstfkm108)
                                        {
                                            if (item.PCLNo == lstquantity.PCLNo && item.FindNo == lstquantity.Partno)
                                            {
                                                if (chkQty > 0)
                                                {
                                                    var dchkQty = chkQty - Convert.ToDouble(lstquantity.Qty);
                                                    if (dchkQty > 0)
                                                    {
                                                        chkQty = chkQty - Convert.ToDouble(item.AllocatedQty);
                                                        if (item.AllocatedQty == lstquantity.Qty)
                                                        {
                                                            lstquantity.Qty = 0;
                                                        }
                                                        else
                                                        {
                                                            lstquantity.Qty = lstquantity.Qty - Convert.ToDecimal(item.AllocatedQty);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        lstquantity.Qty = lstquantity.Qty - Convert.ToDecimal(item.AllocatedQty);
                                                    }
                                                }
                                            }
                                        }


                                        if (Convert.ToDouble(lstquantity.Qty) > requiredQty)
                                        {
                                            if (requiredQty > 0)
                                            {
                                                if (!lstfkm108.Any(x => x.FindNo == chkfkm202.FindNo && x.NodeId == chkfkm202.NodeId && x.ParentNodeId == chkfkm202.ParentNodeId))
                                                {
                                                    lstFKM108.Add(new FKM108
                                                    {
                                                        Project = lstquantity.Project,
                                                        ParentNodeId = chkfkm202.ParentNodeId,
                                                        FindNo = lstquantity.Partno,
                                                        NodeKey = lstquantity.Part,
                                                        NodeId = chkfkm202.NodeId,
                                                        PCRNo = lstquantity.PCRNo,
                                                        PCRLineNo = lstquantity.PCRLineno,
                                                        PCRLineRevNo = lstquantity.PCRlinerev,
                                                        PCLNo = lstquantity.PCLNo,
                                                        AllocatedQty = Convert.ToDecimal(requiredQty),
                                                        CreatedBy = objClsLoginInfo.UserName,
                                                        CreatedOn = DateTime.Now,
                                                    });
                                                }
                                                lstquantity.Qty = lstquantity.Qty - Convert.ToDecimal(requiredQty);
                                                break;
                                            }
                                        }
                                        else
                                        {
                                            if (finalAvailableQty >= requiredQty)
                                            {
                                                if (lstquantity.Qty > 0)
                                                {
                                                    if (!lstfkm108.Any(x => x.FindNo == chkfkm202.FindNo && x.NodeId == chkfkm202.NodeId && x.ParentNodeId == chkfkm202.ParentNodeId))
                                                    {
                                                        lstFKM108.Add(new FKM108
                                                        {
                                                            Project = lstquantity.Project,
                                                            ParentNodeId = chkfkm202.ParentNodeId,
                                                            FindNo = lstquantity.Partno,
                                                            NodeKey = lstquantity.Part,
                                                            NodeId = chkfkm202.NodeId,
                                                            PCRNo = lstquantity.PCRNo,
                                                            PCRLineNo = lstquantity.PCRLineno,
                                                            PCRLineRevNo = lstquantity.PCRlinerev,
                                                            PCLNo = lstquantity.PCLNo,
                                                            AllocatedQty = Convert.ToDouble(lstquantity.Qty) < requiredQty ? Convert.ToDecimal(lstquantity.Qty) : Convert.ToDecimal(requiredQty),
                                                            CreatedBy = objClsLoginInfo.UserName,
                                                            CreatedOn = DateTime.Now,
                                                        });
                                                    }
                                                    finalAvailableQty = finalAvailableQty - Convert.ToDouble(lstquantity.Qty);
                                                    requiredQty = requiredQty - Convert.ToDouble(lstquantity.Qty);
                                                    lstquantity.Qty = Convert.ToDouble(lstquantity.Qty) > requiredQty ? Convert.ToDecimal(lstquantity.Qty) - Convert.ToDecimal(requiredQty) : 0;

                                                }
                                            }
                                        }
                                    }
                                }


                                if (lstFKM108.Count() > 0)
                                {
                                    db.FKM108.AddRange(lstFKM108);
                                    db.SaveChanges();
                                    objResponseMsg.Key = true;
                                    objResponseMsg.Value = "Successfully Allocated";
                                }


                                //var lstquantity = listforPLT.Where(x => x.Part == chkfkm202.NodeKey && x.Partno == chkfkm202.FindNo).FirstOrDefault();
                                //if (lstquantity != null)
                                //{
                                //    FKM108 objfkm108 = new FKM108();
                                //    objfkm108.Project = lstquantity.Project;
                                //    objfkm108.FindNo = lstquantity.Partno;
                                //    objfkm108.NodeKey = lstquantity.Part;
                                //    objfkm108.NodeId = nodeid;
                                //    objfkm108.PCRNo = lstquantity.PCRNo;
                                //    objfkm108.PCRLineNo = lstquantity.PCRLineno;
                                //    objfkm108.PCRLineRevNo = lstquantity.PCRlinerev;
                                //    objfkm108.PCLNo = lstquantity.PCLNo;
                                //    objfkm108.AllocatedQty = allocateQty;
                                //    objfkm108.CreatedBy = objClsLoginInfo.UserName;
                                //    objfkm108.CreatedOn = DateTime.Now;
                                //    objfkm108.ParentNodeId = chkfkm202.ParentNodeId;

                                //    db.FKM108.Add(objfkm108);
                                //    db.SaveChanges();
                                //    objResponseMsg.Key = true;
                                //    objResponseMsg.Value = "Successfully Allocated";
                                //}
                            }
                            else
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = "Qty not available";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            finally
            {
                SetASMPartsColors(HeaderId, ParentNodeId);
            }
            return objResponseMsg;
        }

        [HttpPost]
        public ActionResult DeAllocateplate(int NodeId, decimal ReqQty)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = Deallocation(NodeId, objClsLoginInfo.UserName, objClsLoginInfo.Location, ReqQty);
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Allocateplate(int NodeId, decimal AvailQty, decimal ReqQty)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = allocation(NodeId, AvailQty);
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReAllocateplate(int NodeId, decimal AvailQty, decimal ReqQty)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (NodeId != 0)
                {
                    var chkfkm202 = db.FKM202.Where(x => x.NodeId == NodeId).FirstOrDefault();

                    if (chkfkm202 != null && chkfkm202.NoOfPieces != 0)
                    {
                        decimal requiredQty = chkfkm202.NoOfPieces != null ? Convert.ToDecimal(chkfkm202.NoOfPieces) : 0;

                        var kit = db.FKM202.Where(u => u.NodeId == chkfkm202.ParentNodeId).FirstOrDefault();
                        decimal KitQty = kit.NoOfPieces != null ? Convert.ToDecimal(kit.NoOfPieces) : 0;

                        requiredQty = requiredQty * KitQty;

                        if (AvailQty >= requiredQty)
                        {
                            objResponseMsg = Deallocation(NodeId, objClsLoginInfo.UserName, objClsLoginInfo.Location, ReqQty);
                            if (objResponseMsg.Key != false)
                            {
                                objResponseMsg = allocation(NodeId, AvailQty);
                            }
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Qty not available";
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Records Not Found";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        [HttpPost]
        public ActionResult LoadFKMSData(JQueryDataTableParamModel param)
        {
            try
            {
                double QtyOnHand = 0.00;
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                int nodeId = Convert.ToInt32(param.Headerid);

                var lstpartdetails = db.FKM202.Where(x => x.NodeId == nodeId).FirstOrDefault();

                if (lstpartdetails != null)
                {
                    string whereCondition = "1=1 and PPOQty > 0"; // Observation 15517 on 04-07-2018

                    //string[] columnName = { "PPONumber", "PPOLine", "PPOLinesequence", "PPOQty", "PONumber", "POLine", "POlineSequence", "POOrderQty", "qtyonhand" };
                    string[] columnName = { "PPONumber", "PPOQty", "PONumber", "POOrderQty" };

                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                    string strSortOrder = string.Empty;
                    if (!string.IsNullOrWhiteSpace(sortColumnName))
                    {
                        strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                    }

                    var lstPam = db.SP_FKMS_GETPARTDETAILS(lstpartdetails.Project, lstpartdetails.NodeKey_Init, StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                    if (lstPam != null && lstPam.Count > 0)
                    {
                        QtyOnHand = Convert.ToDouble(lstPam.Select(x => x.qtyonhand).FirstOrDefault());
                    }
                    int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                    var res = (from h in lstPam
                               select new[] {
                               Convert.ToString(h.PPONumber),
                               Convert.ToString(h.PPOQty),
                               Convert.ToString(h.PONumber),
                               Convert.ToString(h.POOrderQty),
                    }).ToList();

                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                        QtyonHand = QtyOnHand,
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = "0",
                        iTotalRecords = "0",
                        aaData = new string[0]
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = string.Empty,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = new string[0]
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult LoadFKMSPLTData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                int nodeId = Convert.ToInt32(param.Headerid);

                var lstpartdetails = db.FKM202.Where(x => x.NodeId == nodeId).FirstOrDefault();

                if (lstpartdetails != null)
                {
                    string whereCondition = "1=1";

                    string[] columnName = { "PCRNumber", "PCRPosition", "PCRRevision", "PCRStenum", "PCRStatus", "PCLNumber", "PCLStEnum", "PCLStatus", "NumberofPieces" };

                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                    string strSortOrder = string.Empty;
                    if (!string.IsNullOrWhiteSpace(sortColumnName))
                    {
                        strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                    }

                    var lstPam = db.SP_FKMS_GETPLTDETAILS(lstpartdetails.Project, lstpartdetails.FindNo_Init, StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                    int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                    var res = (from h in lstPam
                               select new[] {
                               Convert.ToString(h.PCRNumber),
                               Convert.ToString(h.PCRPosition),
                               Convert.ToString(h.PCRRevision),
                               //Convert.ToString(h.PCRStenum ),
                               Convert.ToString(h.PCRStatus),
                               Convert.ToString(h.PCLNumber),
                               //Convert.ToString(h.PCLStEnum),
                               Convert.ToString(h.PCLStatus),
                               Convert.ToString(h.NumberofPieces)
                    }).ToList();

                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = "0",
                        iTotalRecords = "0",
                        aaData = new string[0]
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = string.Empty,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = new string[0]
                }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult DynamicForm(FormCollection form, string noofSections, string noofPages)
        {
            //List<tableData> logParentList = (List<tableData>)Newtonsoft.Json.JsonConvert.DeserializeObject(form["logLists"], typeof(List<tableData>));
            List<tableData> logInitPageParentList = new List<tableData>();
            List<tableData> logPageParentList = new List<tableData>();
            List<tableData> logParentList = new List<tableData>();
            List<tableData> logChildList = new List<tableData>();

            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            int HeaderId = Convert.ToInt32(form["hidheaderid"]);
            int SectionCount = Convert.ToInt32(form["hidsection"]);
            int Equipmentid = Convert.ToInt32(form["hidequipment"]);

            string sectionType = clsImplementationEnum.FKMSNodeType.Section.GetStringValue();
            string assemblyType = clsImplementationEnum.FKMSNodeType.Assembly.GetStringValue();
            string selfType = clsImplementationEnum.FKMSNodeType.Self.GetStringValue();
            string subassemblyType = clsImplementationEnum.FKMSNodeType.SubAssembly.GetStringValue();
            //int GenerateNo = 0;

            try
            {
                int AutoGeneratedFindNo = StartFindNo;
                var objFKM203 = db.FKM203.ToList();
                var sectid = objFKM203.Where(x => x.NodeType.ToLower() == sectionType.ToLower()).Select(x => x.Id).FirstOrDefault();
                var assid = objFKM203.Where(x => x.NodeType.ToLower() == assemblyType.ToLower()).Select(x => x.Id).FirstOrDefault();
                var selfid = objFKM203.Where(x => x.NodeType.ToLower() == selfType.ToLower()).Select(x => x.Id).FirstOrDefault();
                var subassid = objFKM203.Where(x => x.NodeType.ToLower() == subassemblyType.ToLower()).Select(x => x.Id).FirstOrDefault();
                var objFKM201 = db.FKM201.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                string[] Numeric = form["Numeric"].Split(char.Parse(","));
                string[] Name = form["Name"].Split(char.Parse(","));

                string[] Initpages = form["initpages"].Split(char.Parse(","));
                string[] Initlabel = form["InitLabel"].Split(char.Parse(","));
                string[] Initparentlabel = form["InitparentLabel"].Split(char.Parse(","));
                string[] Initsection = form["Initsection"].Split(char.Parse(","));

                string[] PWHTpages = form["pwhtpages"].Split(char.Parse(","));
                string[] Sectionlabel = form["PWHTLabel"].Split(char.Parse(","));
                string[] Sectionparentlabel = form["PWHTparentLabel"].Split(char.Parse(","));
                string[] Sectionparent = form["PWHTsection"].Split(char.Parse(","));

                string[] Names = form["Label"].Split(char.Parse(","));
                string[] ParentNames = form["parentLabel"].Split(char.Parse(","));
                string[] Sections = form["section"].Split(char.Parse(","));
                string[] Internal = form["Internal"].Split(char.Parse(","));
                string[] External = form["External"].Split(char.Parse(","));
                string[] Nozzle = form["Nozzle"].Split(char.Parse(","));

                for (int i = 0; i < Initlabel.Length; i++)
                {
                    tableData objLog = new tableData();
                    objLog.Initpages = Initpages[i];
                    objLog.Initsection = Initsection[i];
                    objLog.Initlabel = Initlabel[i];
                    objLog.Initparentlabel = Initparentlabel[i];
                    logInitPageParentList.Add(objLog);
                }

                for (int b = 0; b < Names.Length; b++)
                {
                    tableData objLog = new tableData();
                    objLog.ParentName = ParentNames[b];
                    objLog.Name = Names[b];
                    objLog.SectionNo = Sections[b];
                    objLog.Internal = Internal[b];
                    objLog.External = External[b];
                    objLog.Nozzle = Nozzle[b];
                    logChildList.Add(objLog);
                }

                for (int p = 0; p < Sectionlabel.Length; p++)
                {
                    tableData objLog = new tableData();
                    objLog.PWHTpages = PWHTpages[p];
                    objLog.Sectionlabel = Sectionlabel[p];
                    objLog.Sectionparentlabel = Sectionparentlabel[p];
                    objLog.PWHTsec = Sectionparent[p];
                    objLog.PWHTQty = noofPages;
                    logPageParentList.Add(objLog);
                }

                for (int c = 0; c < Name.Length; c++)
                {
                    tableData objLog = new tableData();
                    objLog.AssName = Name[c];
                    objLog.AssQty = Numeric[c];
                    objLog.SecQty = noofSections;
                    logParentList.Add(objLog);
                }

                if (logParentList.Count > 0 && logChildList.Count > 0)
                {
                    string initsectionList = getInitParentXMLFromList(logInitPageParentList);
                    string pagesectionList = getPageParentXMLFromList(logPageParentList);
                    string parentist = getParentXMLFromList(logParentList);
                    string childList = getChildXMLFromList(logChildList);
                    var lstparentnode = db.FKM202.Where(x => x.NodeId == Equipmentid).FirstOrDefault();
                    lstparentnode.PlannerStructure = initsectionList + pagesectionList + parentist + childList;
                    db.SaveChanges();
                }

                var chkequipmentid = db.FKM202.Where(x => x.NodeId == Equipmentid).FirstOrDefault();
                if (chkequipmentid == null)
                {
                    return RedirectToAction("FKMSDetails", new { id = HeaderId, urlForm = clsImplementationEnum.WPPIndexType.maintain.GetStringValue() });
                }
                else
                {
                    //comment this line on 07-09-2018, no need to push into PLM
                    objResponseMsg.Key = InsertInToPLM(HeaderId, chkequipmentid.NodeId, AutoGeneratedFindNo, chkequipmentid, assid, objFKM201.Project, objFKM201.Status);

                    #region Move this code in InsertINToPLM function

                    //List<FKMSPartEnt> listFKMSPartEnt = new List<FKMSPartEnt>();
                    //List<FKMSBOMEnt> listFKMSBOMEnt = new List<FKMSBOMEnt>();
                    //for (int i = 1; i <= SectionCount; i++)
                    //{
                    //    AutoGeneratedFindNo = GetFindNo(AutoGeneratedFindNo);
                    //    FKMSPartEnt partSectionEnt = GeneratePartDetails(clsImplementationEnum.FKMSNodeType.Section, clsImplementationEnum.FKMSNodeType.Section, assid, projid, 0, "", i);
                    //    FKMSBOMEnt bomSectionEnt = GenerateBOMDetails(partSectionEnt, chkequipmentid.NodeKey, AutoGeneratedFindNo);
                    //    if (partSectionEnt != null) { listFKMSPartEnt.Add(partSectionEnt); }
                    //    if (bomSectionEnt != null) { listFKMSBOMEnt.Add(bomSectionEnt); }
                    //    AutoGeneratedFindNo = AutoGeneratedFindNo + 1;

                    //    for (int n = 0; n < Names.Length; n++)
                    //    {
                    //        if (Convert.ToInt32(Sections[n]) == i)
                    //        {
                    //            FKMSBOMEnt bomEnt = null;
                    //            FKMSPartEnt partEnt = null;


                    //            #region Main Assembly        
                    //            AutoGeneratedFindNo = GetFindNo(AutoGeneratedFindNo);
                    //            FKMSPartEnt partMainAssEnt = GeneratePartDetails(clsImplementationEnum.FKMSNodeType.Assembly, clsImplementationEnum.FKMSNodeType.Assembly, assid, projid, ++GenerateNo, Names[n]);
                    //            FKMSBOMEnt bomMainAssEnt = GenerateBOMDetails(partMainAssEnt, bomSectionEnt.childPartName, AutoGeneratedFindNo);
                    //            if (partMainAssEnt != null) { listFKMSPartEnt.Add(partMainAssEnt); }
                    //            if (bomMainAssEnt != null) { listFKMSBOMEnt.Add(bomMainAssEnt); }
                    //            AutoGeneratedFindNo = AutoGeneratedFindNo + 1;
                    //            #endregion

                    //            #region Self Node   
                    //            AutoGeneratedFindNo = GetFindNo(AutoGeneratedFindNo);
                    //            partEnt = GeneratePartDetails(clsImplementationEnum.FKMSNodeType.SubAssembly, clsImplementationEnum.FKMSNodeType.Self, assid, projid, ++GenerateNo, Names[n]);
                    //            bomEnt = GenerateBOMDetails(partEnt, bomMainAssEnt.childPartName, AutoGeneratedFindNo);
                    //            if (partEnt != null) { listFKMSPartEnt.Add(partEnt); }
                    //            if (bomEnt != null) { listFKMSBOMEnt.Add(bomEnt); }
                    //            AutoGeneratedFindNo = AutoGeneratedFindNo + 1;
                    //            #endregion

                    //            #region Internal Node     
                    //            if (Convert.ToBoolean(Internal[n]))
                    //            {
                    //                AutoGeneratedFindNo = GetFindNo(AutoGeneratedFindNo);
                    //                partEnt = GeneratePartDetails(clsImplementationEnum.FKMSNodeType.InternalAssembly, clsImplementationEnum.FKMSNodeType.InternalAssembly, assid, projid, ++GenerateNo, Names[n]);
                    //                bomEnt = GenerateBOMDetails(partEnt, bomMainAssEnt.childPartName, AutoGeneratedFindNo);
                    //                if (partEnt != null) { listFKMSPartEnt.Add(partEnt); }
                    //                if (bomEnt != null) { listFKMSBOMEnt.Add(bomEnt); }
                    //                AutoGeneratedFindNo = AutoGeneratedFindNo + 1;
                    //            }
                    //            #endregion

                    //            #region External Node   
                    //            if (Convert.ToBoolean(External[n]))
                    //            {
                    //                AutoGeneratedFindNo = GetFindNo(AutoGeneratedFindNo);
                    //                partEnt = GeneratePartDetails(clsImplementationEnum.FKMSNodeType.ExternalAssembly, clsImplementationEnum.FKMSNodeType.ExternalAssembly, assid, projid, ++GenerateNo, Names[n]);
                    //                bomEnt = GenerateBOMDetails(partEnt, bomMainAssEnt.childPartName, AutoGeneratedFindNo);
                    //                if (partEnt != null) { listFKMSPartEnt.Add(partEnt); }
                    //                if (bomEnt != null) { listFKMSBOMEnt.Add(bomEnt); }
                    //                AutoGeneratedFindNo = AutoGeneratedFindNo + 1;
                    //            }
                    //            #endregion

                    //            #region Nozzle Node            
                    //            if (Convert.ToBoolean(Nozzle[n]))
                    //            {
                    //                AutoGeneratedFindNo = GetFindNo(AutoGeneratedFindNo);
                    //                partEnt = GeneratePartDetails(clsImplementationEnum.FKMSNodeType.NozzelAssembly, clsImplementationEnum.FKMSNodeType.NozzelAssembly, assid, projid, ++GenerateNo, Names[n]);
                    //                bomEnt = GenerateBOMDetails(partEnt, bomMainAssEnt.childPartName, AutoGeneratedFindNo);
                    //                if (partEnt != null) { listFKMSPartEnt.Add(partEnt); }
                    //                if (bomEnt != null) { listFKMSBOMEnt.Add(bomEnt); }
                    //                AutoGeneratedFindNo = AutoGeneratedFindNo + 1;
                    //            }
                    //            #endregion
                    //        }
                    //    }
                    //}

                    //if (listFKMSBOMEnt.Count > 0)
                    //{
                    //    context = new IEMQSEntitiesContext();
                    //    var assem = clsImplementationEnum.FKMSNodeType.Assembly.GetStringValue().ToLower();
                    //    int AssemblyId = context.FKM203.Where(x => x.NodeType.ToLower() == assem).FirstOrDefault().Id;
                    //    var delFKM202 = context.FKM202.Where(i => i.HeaderId == HeaderId && i.NodeTypeId == AssemblyId).ToList();
                    //    using (DbContextTransaction dbTran = context.Database.BeginTransaction())
                    //    {
                    //        try
                    //        {
                    //            if (delFKM202.Count() > 0)
                    //            {
                    //                foreach (var item in delFKM202)
                    //                {
                    //                    context.Entry(item).State = EntityState.Deleted;
                    //                }
                    //                context.FKM202.RemoveRange(delFKM202);
                    //                context.SaveChanges();
                    //            }

                    //            #region Insert Nodes
                    //            foreach (var bom in listFKMSBOMEnt)
                    //            {
                    //                //InsertPartAndBOMInPLM(bom, HeaderId);
                    //                //test purpose comment
                    //                InsertNodeInFKMS(bom, HeaderId);
                    //            }
                    //            #endregion

                    //            dbTran.Commit();

                    //        }
                    //        catch (DbEntityValidationException dex)
                    //        {
                    //            Elmah.ErrorSignal.FromCurrentContext().Raise(dex);
                    //            dbTran.Rollback();
                    //            throw dex;
                    //        }
                    //        catch (Exception ex)
                    //        {
                    //            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    //            dbTran.Rollback();
                    //            throw ex;
                    //        }
                    //    }

                    //    if (db.FKM202.Any(i => i.HeaderId == HeaderId && i.IsInsertedInPLM != null && i.IsInsertedInPLM == false))
                    //    {
                    //        objResponseMsg.Key = false;
                    //    }
                    //    else
                    //    {
                    //        objResponseMsg.Key = true;
                    //    }
                    //}
                    #endregion

                    #region Old Code
                    //#region Insert Sections
                    //List<FKM202> objSection = new List<FKM202>();                    
                    //for (int i = 1; i <= SectionCount; i++)
                    //{
                    //    FKM202 objFKM202 = new FKM202();
                    //    objFKM202.HeaderId = HeaderId;
                    //    objFKM202.ParentNodeId = Equipmentid;
                    //    objFKM202.NodeTypeId = sectid;
                    //    objFKM202.CreatedBy = objClsLoginInfo.UserName;
                    //    objFKM202.CreatedOn = DateTime.Now;
                    //    objFKM202.NodeName = clsImplementationEnum.FKMSNodeType.Section.GetStringValue() + i;
                    //    objFKM202.Project = projid;
                    //    objFKM202.FindNo = AutoGeneratedFindNo.ToString();
                    //    objSection.Add(objFKM202);

                    //    AutoGeneratedFindNo += 1;                        
                    //}
                    //db.FKM202.AddRange(objSection);
                    //db.SaveChanges();
                    //#endregion

                    //#region Insert Assembly
                    //for (int i = 0; i < Names.Length; i++)
                    //{
                    //    FKM202 objFKM202 = new FKM202();
                    //    objFKM202.HeaderId = HeaderId;
                    //    objFKM202.ParentNodeId = objSection.Where(x => x.HeaderId == HeaderId && x.NodeTypeId == 6 && x.NodeName.ToLower() == (sectionType + Sections[i]).ToLower()).FirstOrDefault().NodeId;
                    //    objFKM202.NodeTypeId = assid;
                    //    objFKM202.NodeName = Names[i] + clsImplementationEnum.FKMSNodeType.Assembly.GetStringValue();
                    //    objFKM202.Project = projid;
                    //    objFKM202.CreatedBy = objClsLoginInfo.UserName;
                    //    objFKM202.CreatedOn = DateTime.Now;
                    //    objFKM202.FindNo = AutoGeneratedFindNo.ToString();
                    //    db.FKM202.Add(objFKM202);
                    //    db.SaveChanges();

                    //    AutoGeneratedFindNo += 1;

                    //    #region Insert Self and Sub Assemblies
                    //    FKM202 objSelf = new FKM202();
                    //    objSelf.HeaderId = HeaderId;
                    //    objSelf.ParentNodeId = objFKM202.NodeId;
                    //    objSelf.NodeTypeId = selfid;
                    //    objSelf.NodeName = Names[i];
                    //    objSelf.Project = projid;
                    //    objSelf.CreatedBy = objClsLoginInfo.UserName;
                    //    objSelf.CreatedOn = DateTime.Now;
                    //    objSelf.FindNo = AutoGeneratedFindNo.ToString();
                    //    db.FKM202.Add(objSelf);

                    //    AutoGeneratedFindNo += 1;

                    //    if (Internal[i].ToLower() == "true")
                    //    {
                    //        FKM202 objNewfkm202 = new FKM202();
                    //        objNewfkm202.HeaderId = HeaderId;
                    //        objNewfkm202.ParentNodeId = objFKM202.NodeId;
                    //        objNewfkm202.NodeTypeId = subassid;
                    //        objNewfkm202.NodeName = clsImplementationEnum.FKMSNodeType.InternalAssembly.GetStringValue() + "-" + Names[i];
                    //        objNewfkm202.Project = projid;
                    //        objNewfkm202.CreatedBy = objClsLoginInfo.UserName;
                    //        objNewfkm202.CreatedOn = DateTime.Now;
                    //        objNewfkm202.FindNo = AutoGeneratedFindNo.ToString();
                    //        db.FKM202.Add(objNewfkm202);
                    //        AutoGeneratedFindNo += 1;
                    //    }
                    //    if (External[i].ToLower() == "true")
                    //    {
                    //        FKM202 objNewfkm202 = new FKM202();
                    //        objNewfkm202.HeaderId = HeaderId;
                    //        objNewfkm202.ParentNodeId = objFKM202.NodeId;
                    //        objNewfkm202.NodeTypeId = subassid;
                    //        objNewfkm202.NodeName = clsImplementationEnum.FKMSNodeType.ExternalAssembly.GetStringValue() + "-" + Names[i];
                    //        objNewfkm202.Project = projid;
                    //        objNewfkm202.CreatedBy = objClsLoginInfo.UserName;
                    //        objNewfkm202.CreatedOn = DateTime.Now;
                    //        objNewfkm202.FindNo = AutoGeneratedFindNo.ToString();
                    //        db.FKM202.Add(objNewfkm202);
                    //        AutoGeneratedFindNo += 1;
                    //    }
                    //    if (Nozzle[i].ToLower() == "true")
                    //    {
                    //        FKM202 objNewfkm202 = new FKM202();
                    //        objNewfkm202.HeaderId = HeaderId;
                    //        objNewfkm202.ParentNodeId = objFKM202.NodeId;
                    //        objNewfkm202.NodeTypeId = subassid;
                    //        objNewfkm202.NodeName = clsImplementationEnum.FKMSNodeType.NozzelAssembly.GetStringValue() + "-" + Names[i];
                    //        objNewfkm202.Project = projid;
                    //        objNewfkm202.CreatedBy = objClsLoginInfo.UserName;
                    //        objNewfkm202.CreatedOn = DateTime.Now;
                    //        objNewfkm202.FindNo = AutoGeneratedFindNo.ToString();
                    //        db.FKM202.Add(objNewfkm202);
                    //        AutoGeneratedFindNo += 1;
                    //    }
                    //    db.SaveChanges();
                    //    #endregion
                    //}
                    //#endregion
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("FKMSDetails", new { HeaderId = HeaderId });
        }

        public string getInitParentXMLFromList(List<tableData> logList)
        {
            var xmlfromLINQ = new XElement("InitParentlist",
            from c in logList
            select new XElement("InitParent",
                   new XElement("Initpages", c.Initpages),
                   new XElement("Initlabel", c.Initlabel),
                   new XElement("Initparentlabel", c.Initparentlabel),
                   new XElement("Initsection", c.Initsection)
                   ));
            return xmlfromLINQ.ToString();
        }

        public string getPageParentXMLFromList(List<tableData> logList)
        {
            var xmlfromLINQ = new XElement("PageParentlist",
            from c in logList
            select new XElement("PageParent",
                   new XElement("PWHTpages", c.PWHTpages),
                   new XElement("Sectionlabel", c.Sectionlabel),
                   new XElement("PWHTsec", c.PWHTsec),
                   new XElement("PWHTQty", c.PWHTQty),
                   new XElement("Sectionparentlabel", c.Sectionparentlabel)));
            return xmlfromLINQ.ToString();
        }

        public string getParentXMLFromList(List<tableData> logList)
        {
            var xmlfromLINQ = new XElement("Parentlist",
            from c in logList
            select new XElement("Parent",
                   new XElement("AssName", c.AssName),
                   new XElement("AssQty", c.AssQty),
                   new XElement("SecQty", c.SecQty)));
            return xmlfromLINQ.ToString();
        }

        public string getChildXMLFromList(List<tableData> logList)
        {
            var xmlfromLINQ = new XElement("Childlist",
            from c in logList
            select new XElement("Child",
                   new XElement("ParentName", c.ParentName),
                   new XElement("Name", c.Name),
                   new XElement("SectionNo", c.SectionNo),
                   new XElement("Internal", c.Internal),
                   new XElement("External", c.External),
                   new XElement("Nozzle", c.Nozzle)));
            return xmlfromLINQ.ToString();
        }

        public FKMSPartEnt GeneratePartDetails(clsImplementationEnum.FKMSNodeType NodeType, clsImplementationEnum.FKMSNodeType GenerateNodeKeyType, int FKMSNodeTypeId, string Project, int GenerateNo, string Name = "", int SrNo = 0)
        {
            FKMSPartEnt partEnt = null;
            try
            {
                partEnt = new FKMSPartEnt();
                if (NodeType == clsImplementationEnum.FKMSNodeType.PWHTSection)
                {
                    partEnt.description = NodeType.GetStringValue() + "-" + SrNo;
                    partEnt.partName = (Project.Length > 7 ? Project.Substring(0, 7) : Project) + "-" + (NodeType.GetStringValue() + SrNo).ToUpper();// + SrNo;
                    partEnt.IsPWHT = true;
                }
                else if (NodeType == clsImplementationEnum.FKMSNodeType.Section)
                {
                    partEnt.description = NodeType.GetStringValue() + "-" + SrNo;
                    partEnt.partName = (Project.Length > 7 ? Project.Substring(0, 7) : Project) + "-" + (NodeType.GetStringValue().Substring(12) + SrNo).ToUpper();// + SrNo;
                    partEnt.IsPWHT = false;
                }
                else
                {
                    if (NodeType == FKMSNodeType.NozzelAssembly)
                    {
                        partEnt.description = Name + " " + "Nozzle Assembly";
                    }
                    else
                    {
                        partEnt.description = Name + " " + NodeType.GetStringValue();
                    }
                    partEnt.partName = (Project.Length > 7 ? Project.Substring(0, 7) : Project) + "-" + GenerateNodeKey(GenerateNo, Name, GenerateNodeKeyType);
                    partEnt.IsPWHT = false;
                }
                partEnt.isTopPart = _defaultIsTopPart;
                partEnt.itemGroup = _defaultItemGroup;
                partEnt.owner = objClsLoginInfo.UserName;
                partEnt.partType = _defaultPartType;
                partEnt.policy = _defaultPolicy;
                partEnt.relatedProjectName = Project;
                partEnt.NodeTypeId = FKMSNodeTypeId;
                //partSectionEnt.findNumber = AutoGeneratedFindNo.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                partEnt = null;
                throw;
            }
            return partEnt;
        }

        public FKMSBOMEnt GenerateBOMDetails(FKMSPartEnt part, string ParentNodeName, int AutoGeneratedFindNo)
        {
            FKMSBOMEnt bomEnt = null;
            try
            {
                if (part != null)
                {
                    bomEnt = new FKMSBOMEnt();
                    bomEnt.parentPartType = _defaultParentPartType;
                    bomEnt.parentPartName = ParentNodeName;
                    bomEnt.action = _defaultPartAction;
                    bomEnt.childPartType = _defaultChildPartType;
                    bomEnt.childPartName = part.partName;
                    bomEnt.findNumber = AutoGeneratedFindNo.ToString();
                    bomEnt.quantity = _defaultpartQuantity;
                    bomEnt.length = _defaultLength;
                    bomEnt.width = _defaultWidth;
                    bomEnt.numberOfPieces = _defaultNumberOfPieces;
                    bomEnt.owner = objClsLoginInfo.UserName;
                    bomEnt.description = part.description;
                    bomEnt.part = part;
                    bomEnt.IsPWHT = part.IsPWHT;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                bomEnt = null;
                throw;
            }
            return bomEnt;
        }

        public FKMSPartEnt GenerateCopyPartDetails(FKM202 objfkm202, string sourceProject, string destProject)
        {
            FKMSPartEnt partEnt = null;
            try
            {
                string key = null;
                if (objfkm202.NodeKey != null)
                {
                    key = objfkm202.NodeKey;
                    var sourceProjectText = sourceProject.Length > 7 ? sourceProject.Substring(0, 7) : sourceProject;
                    var destProjectText = destProject.Length > 7 ? destProject.Substring(0, 7) : destProject;
                    key = key.Replace(sourceProjectText, destProjectText);
                }

                partEnt = new FKMSPartEnt();
                partEnt.description = objfkm202.NodeName != null ? objfkm202.NodeName : "";
                partEnt.partName = key;
                partEnt.isTopPart = _defaultIsTopPart;
                partEnt.itemGroup = _defaultItemGroup;
                partEnt.owner = objClsLoginInfo.UserName;
                partEnt.partType = _defaultPartType;
                partEnt.policy = _defaultPolicy;
                partEnt.relatedProjectName = destProject;
                partEnt.NodeTypeId = 7;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                partEnt = null;
                throw;
            }
            return partEnt;
        }

        public FKMSBOMEnt GenerateCopyBOMDetails(FKMSPartEnt part, string ParentNodeName, string AutoGeneratedFindNo, string sourceProject, string destProject)
        {
            FKMSBOMEnt bomEnt = null;
            try
            {
                if (part != null)
                {
                    string key = null;
                    if (ParentNodeName != null)
                    {
                        key = ParentNodeName;
                        var sourceProjectText = sourceProject.Length > 7 ? sourceProject.Substring(0, 7) : sourceProject;
                        var destProjectText = destProject.Length > 7 ? destProject.Substring(0, 7) : destProject;
                        key = key.Replace(sourceProjectText, destProjectText);
                    }

                    bomEnt = new FKMSBOMEnt();
                    bomEnt.parentPartType = _defaultParentPartType;
                    bomEnt.parentPartName = key;
                    bomEnt.action = _defaultPartAction;
                    bomEnt.childPartType = _defaultChildPartType;
                    bomEnt.childPartName = part.partName;
                    bomEnt.findNumber = AutoGeneratedFindNo;
                    bomEnt.quantity = _defaultpartQuantity;
                    bomEnt.length = _defaultLength;
                    bomEnt.width = _defaultWidth;
                    bomEnt.numberOfPieces = _defaultNumberOfPieces;
                    bomEnt.owner = objClsLoginInfo.UserName;
                    bomEnt.description = part.description != null ? part.description : "";
                    bomEnt.part = part;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                bomEnt = null;
                throw;
            }
            return bomEnt;
        }

        public string GenerateNodeKey(int GenerateNo, string NodeName, clsImplementationEnum.FKMSNodeType Type)
        {
            string NodeKey = (NodeName.Length > 3 ? NodeName.Substring(0, 3) : NodeName).ToUpper();
            if (Type == clsImplementationEnum.FKMSNodeType.Assembly)
            {
                NodeKey += (++GenerateNo).ToString("0000") + "ASM";
            }
            else if (Type == clsImplementationEnum.FKMSNodeType.Self || Type == clsImplementationEnum.FKMSNodeType.SubAssembly)
            {
                NodeKey += (++GenerateNo).ToString("0000") + "SUB-ASM";
            }
            else if (Type == clsImplementationEnum.FKMSNodeType.InternalAssembly)
            {
                NodeKey += (++GenerateNo).ToString("0000") + "ASM-INT-ASM";
            }
            else if (Type == clsImplementationEnum.FKMSNodeType.ExternalAssembly)
            {
                NodeKey += (++GenerateNo).ToString("0000") + "ASM-EXT-ASM";
            }
            else if (Type == clsImplementationEnum.FKMSNodeType.NozzelAssembly)
            {
                NodeKey += (++GenerateNo).ToString("0000") + "ASM-NOZ-ASM";
            }
            return NodeKey;
        }

        public void InsertPartAndBOMInPLM(FKMSBOMEnt bomEnt, int HeaderId, string status, FKM202 objFKM202 = null, int iscopy = 0)
        {
            FKM202 parentNode = null;
            if (objFKM202 != null)
            {
                if (status.ToLower() == clsImplementationEnum.CTQStatus.DRAFT.GetStringValue().ToLower())
                {
                    parentNode = context.FKM202.Where(i => i.NodeId == objFKM202.ParentNodeId).FirstOrDefault();
                }
                else
                {
                    parentNode = db.FKM202.Where(i => i.NodeId == objFKM202.ParentNodeId).FirstOrDefault();
                }
            }

            //var part = bomEnt.part;
            hedplmwebServicesBOMCreationServiceBaaNLNbeanhedPLMPartData plmPart = new hedplmwebServicesBOMCreationServiceBaaNLNbeanhedPLMPartData();
            plmPart.description = bomEnt.part != null && bomEnt.part.description != null ? bomEnt.part.description : objFKM202.NodeName;
            plmPart.isTopPart = bomEnt.part != null && bomEnt.part.isTopPart != null ? bomEnt.part.isTopPart : _defaultIsTopPart;
            plmPart.itemGroup = bomEnt.part != null && bomEnt.part.itemGroup != null ? bomEnt.part.itemGroup : _defaultItemGroup;
            plmPart.owner = bomEnt.part != null && bomEnt.part.owner != null ? bomEnt.part.owner : objClsLoginInfo.UserName;
            plmPart.partName = bomEnt.part != null && bomEnt.part.partName != null ? bomEnt.part.partName : objFKM202.NodeKey;
            plmPart.partType = bomEnt.part != null && bomEnt.part.partType != null ? bomEnt.part.partType : _defaultPartType;
            plmPart.policy = bomEnt.part != null && bomEnt.part.policy != null ? bomEnt.part.policy : _defaultPolicy;
            plmPart.relatedProjectName = bomEnt.part != null && bomEnt.part.relatedProjectName != null ? bomEnt.part.relatedProjectName : objFKM202.Project;
            try
            {
                var partResult = plmWebService.createPart(plmPart);
                if (partResult.key == 1)
                {
                    bomEnt.part.IsInsertedInPLM = false;
                    bomEnt.part.error = partResult.value;
                }
                else
                {
                    bomEnt.part.IsInsertedInPLM = true;
                }

            }
            catch (Exception)
            {

            }
            hedplmwebServicesBOMCreationServiceBaaNLNbeanhedPLMBOMData plmBOM = new hedplmwebServicesBOMCreationServiceBaaNLNbeanhedPLMBOMData();
            plmBOM.action = bomEnt.action != null ? bomEnt.action : _defaultPartAction;
            plmBOM.childPartName = bomEnt.childPartName != null ? bomEnt.childPartName : objFKM202.NodeKey;
            plmBOM.childPartType = bomEnt.childPartType != null ? bomEnt.childPartType : _defaultChildPartType;
            plmBOM.findNumber = bomEnt.findNumber != null ? bomEnt.findNumber : objFKM202.FindNo;
            plmBOM.length = bomEnt.length != null ? bomEnt.length : _defaultLength;
            plmBOM.numberOfPieces = bomEnt.numberOfPieces != null ? bomEnt.numberOfPieces : _defaultNumberOfPieces;
            plmBOM.owner = bomEnt.owner != null ? bomEnt.owner : objClsLoginInfo.UserName;
            plmBOM.parentPartName = bomEnt.parentPartName != null ? bomEnt.parentPartName : parentNode.NodeKey;
            plmBOM.parentPartType = bomEnt.parentPartType != null ? bomEnt.parentPartType : _defaultParentPartType;
            plmBOM.quantity = bomEnt.quantity != null ? bomEnt.quantity : _defaultpartQuantity;
            plmBOM.width = bomEnt.width != null ? bomEnt.width : _defaultWidth;
            try
            {
                var partResult = plmWebService.createBOMAutoRevise(plmBOM);
                if (partResult.key == 1)
                {
                    bomEnt.IsInsertedInPLM = false;
                    bomEnt.error = partResult.value;
                }
                else
                {
                    bomEnt.IsInsertedInPLM = true;
                }
            }
            catch (Exception)
            {
            }

            InsertNodeInFKMS(bomEnt, HeaderId, status, objFKM202, iscopy);
        }

        public void InsertNodeInFKMS(FKMSBOMEnt bomPart, int HeaderId, string status, FKM202 objFKM202 = null, int iscopy = 0)
        {
            if (objFKM202 == null)
            {

                if (iscopy > 0)
                {
                    var chkIsPLM = context.FKM202.Where(x => x.NodeKey == bomPart.childPartName && x.HeaderId == HeaderId).FirstOrDefault();
                    if (chkIsPLM != null)
                    {
                        chkIsPLM.IsInsertedInPLM = bomPart.IsInsertedInPLM;
                        chkIsPLM.PLMError = (bomPart.part.error != null && bomPart.part.error.Length > 0 ? "Part Error : " + bomPart.part.error : "") +
                                              (bomPart.error != null && bomPart.error.Length > 0 ? ",BOM Error : " + bomPart.error : "");
                        context.SaveChanges();

                    }
                }
                else
                {
                    var objParentFKM202 = context.FKM202.Where(i => i.NodeKey == bomPart.parentPartName && i.HeaderId == HeaderId).FirstOrDefault();
                    if (objParentFKM202 != null)
                    {
                        objFKM202 = new FKM202();
                        objFKM202.HeaderId = HeaderId;
                        objFKM202.NodeTypeId = bomPart.part.NodeTypeId;
                        objFKM202.CreatedBy = objClsLoginInfo.UserName;
                        objFKM202.CreatedOn = DateTime.Now;
                        objFKM202.NodeName = bomPart.description;
                        objFKM202.Project = objParentFKM202.Project;
                        objFKM202.ProductType = clsImplementationEnum.NodeTypes.ASM.GetStringValue();
                        objFKM202.ParentNodeId = objParentFKM202.NodeId;
                        objFKM202.ParentNodeId_Init = objParentFKM202.NodeId;
                        objFKM202.NodeKey = bomPart.childPartName;
                        objFKM202.NodeKey_Init = bomPart.childPartName;
                        objFKM202.NoOfPieces = Convert.ToInt32(bomPart.numberOfPieces);
                        objFKM202.NoOfPieces_Init = Convert.ToInt32(bomPart.numberOfPieces);
                        objFKM202.FindNo = bomPart.findNumber;
                        objFKM202.FindNo_Init = bomPart.findNumber;
                        objFKM202.IsPWHT = bomPart.IsPWHT;



                        if (status.ToLower() == clsImplementationEnum.CTQStatus.DRAFT.GetStringValue().ToLower() || status.ToLower() == clsImplementationEnum.CTQStatus.Returned.GetStringValue().ToLower())
                        {
                            context.FKM202.Add(objFKM202);
                            context.SaveChanges();
                        }
                        else
                        {
                            if (bomPart.IsInsertedInPLM && bomPart.part.IsInsertedInPLM)
                            {
                                objFKM202.IsInsertedInPLM = true;
                            }
                            else
                            {
                                objFKM202.IsInsertedInPLM = false;
                            }
                            objFKM202.PLMError = (bomPart.part.error != null && bomPart.part.error.Length > 0 ? "Part Error : " + bomPart.part.error : "") +
                                               (bomPart.error != null && bomPart.error.Length > 0 ? ",BOM Error : " + bomPart.error : "");
                            db.FKM202.Add(objFKM202);
                            db.SaveChanges();
                        }

                    }
                }
            }
            else
            {
                if (bomPart.IsInsertedInPLM && bomPart.part.IsInsertedInPLM)
                {
                    objFKM202.IsInsertedInPLM = true;
                }
                objFKM202.PLMError = (bomPart.part.error != null && bomPart.part.error.Length > 0 ? "Part Error : " + bomPart.part.error : "") +
                                        (bomPart.error != null && bomPart.error.Length > 0 ? ",BOM Error : " + bomPart.error : "");

                context.SaveChanges();
            }
        }

        public FKM104 InsertMainGETHBOMFKMS(FKM202 bomPart, int HeaderId, FKM104 objFKM104 = null, string UserName = "")
        {
            if (UserName == "")
            {
                UserName = objClsLoginInfo.UserName;
            }
            var objParentFKM202 = context.FKM104.Where(i => i.NodeKey == bomPart.NodeKey && i.FindNo == bomPart.FindNo && i.HeaderId == HeaderId).FirstOrDefault();
            if (objParentFKM202 != null)
            {
                //update Records
            }
            else
            {
                objFKM104 = new FKM104();
                objFKM104.HeaderId = HeaderId;
                objFKM104.ParentNodeId = 0;
                objFKM104.NodeTypeId = 1;
                //objFKM104.NodeTypeId = db.FKM203.Where(x => x.NodeType.ToLower() == clsImplementationEnum.FKMSNodeType.Project.GetStringValue().ToLower()).FirstOrDefault().Id;
                objFKM104.CreatedBy = UserName;
                objFKM104.CreatedOn = DateTime.Now;
                objFKM104.NodeName = bomPart.NodeName;
                objFKM104.NodeKey = bomPart.NodeKey;
                objFKM104.Project = bomPart.Project;
                objFKM104.FindNo = bomPart.FindNo;
                objFKM104.ProductType = bomPart.ProductType;
                objFKM104.NoOfPieces = Convert.ToInt32(_defaultNumberOfPieces);
                context.FKM104.Add(objFKM104);
                context.SaveChanges();
            }
            return objFKM104;
        }

        public FKM104 InsertGETHBOMFKMS(SP_FKMS_HBOM_FOR_PROJECT_Result bomPart, int HeaderId, FKM104 objFKM104 = null, int parentid = 0, string UserName = "")
        {
            if (UserName == "")
            {
                UserName = objClsLoginInfo.UserName;
            }
            var objParentFKM104 = context.FKM104.Where(i => i.NodeKey == bomPart.Part && i.FindNo == bomPart.FindNo && i.HeaderId == HeaderId && i.ParentNodeId == parentid).FirstOrDefault();
            if (objParentFKM104 != null)
            {
                //update Records
            }
            else
            {
                objFKM104 = new FKM104();
                objFKM104.HeaderId = HeaderId;
                objFKM104.ParentNodeId = parentid;
                objFKM104.NodeName = bomPart.Description;
                objFKM104.Project = bomPart.project;
                objFKM104.FindNo = bomPart.FindNo;
                objFKM104.PartLevel = bomPart.PartLevel;
                objFKM104.Description = bomPart.Description;
                objFKM104.ItemGroup = bomPart.ItemGroup;
                objFKM104.RevisionNo = bomPart.RevisionNo;
                objFKM104.MaterialSpecification = bomPart.MaterialSpecification;
                objFKM104.MaterialDescription = bomPart.MaterialDescription;
                objFKM104.MaterialCode = bomPart.MaterialCode;
                objFKM104.ProductType = bomPart.ProductType;
                objFKM104.Length = Convert.ToDecimal(bomPart.Length);
                objFKM104.Width = Convert.ToDecimal(bomPart.Width);
                objFKM104.NoOfPieces = Convert.ToDecimal(bomPart.NoOfPieces);
                objFKM104.Weight = Convert.ToDecimal(bomPart.Weight);
                objFKM104.Thickness = Convert.ToDecimal(bomPart.Thickness);
                objFKM104.UOM = bomPart.UOM;
                objFKM104.NodeKey = bomPart.Part;
                objFKM104.CreatedBy = UserName;
                objFKM104.CreatedOn = DateTime.Now;
                objFKM104.Element = bomPart.Element;

                context.FKM104.Add(objFKM104);
                context.SaveChanges();
            }
            return objFKM104;
        }

        public FKM202 InsertDND(ref List<CopyNodeDtl> listCopyNodeDtl, FKM202 dndfkm202, int HeaderId, FKM202 objFKM202 = null, int parentid = 0, decimal? IsNOP = 0, int copy = 0, bool IsChangeNodeKey = true, bool IsCopyDocument = false)
        {
            var objParentFKM202 = db.FKM202.Where(i => i.NodeKey == dndfkm202.NodeKey && i.FindNo == dndfkm202.FindNo && i.HeaderId == HeaderId && i.ParentNodeId == parentid).FirstOrDefault();
            if (objParentFKM202 != null)
            {
                //update Records
            }
            else
            {
                objFKM202 = new FKM202();
                objFKM202.HeaderId = HeaderId;
                objFKM202.ParentNodeId = parentid;
                objFKM202.NodeTypeId = dndfkm202.NodeTypeId;
                objFKM202.CreatedOn = DateTime.Now;
                objFKM202.NodeName = dndfkm202.NodeName;
                objFKM202.FindNo = dndfkm202.FindNo;
                objFKM202.PartLevel = dndfkm202.PartLevel;
                objFKM202.Description = dndfkm202.Description;
                objFKM202.ItemGroup = dndfkm202.ItemGroup;
                objFKM202.RevisionNo = dndfkm202.RevisionNo;
                objFKM202.MaterialSpecification = dndfkm202.MaterialSpecification;
                objFKM202.MaterialDescription = dndfkm202.MaterialDescription;
                objFKM202.MaterialCode = dndfkm202.MaterialCode;
                objFKM202.ProductType = dndfkm202.ProductType;
                objFKM202.Length = dndfkm202.Length;
                objFKM202.Width = dndfkm202.Width;
                if (copy > 0 && copyDestiProject != string.Empty)
                {
                    objFKM202.Project = copyDestiProject;
                    objFKM202.NoOfPieces = dndfkm202.NoOfPieces;
                    objFKM202.CreatedBy = clspsno;
                    string key = null;
                    if (IsChangeNodeKey)
                    {
                        if (dndfkm202.NodeKey != null)
                        {
                            key = dndfkm202.NodeKey;
                            var dndfkm202Project = dndfkm202.Project.Length > 7 ? dndfkm202.Project.Substring(0, 7) : dndfkm202.Project;
                            var objFKM202Project = objFKM202.Project.Length > 7 ? objFKM202.Project.Substring(0, 7) : objFKM202.Project;
                            key = key.Replace(dndfkm202Project, objFKM202Project);
                        }
                        objFKM202.NodeKey = key;
                        objFKM202.NodeKey_Init = key;
                    }
                    else
                    {
                        objFKM202.NodeKey = dndfkm202.NodeKey;
                        objFKM202.NodeKey_Init = dndfkm202.NodeKey_Init;
                    }
                }
                else
                {
                    objFKM202.CreatedBy = objClsLoginInfo.UserName;
                    objFKM202.Project = dndfkm202.Project;
                    objFKM202.NoOfPieces = IsNOP;
                    objFKM202.NodeKey = dndfkm202.NodeKey;
                    objFKM202.NodeKey_Init = dndfkm202.NodeKey_Init;
                    objFKM202.ParentNodeId_Init = dndfkm202.ParentNodeId_Init;
                }
                objFKM202.Weight = dndfkm202.Weight;
                objFKM202.Thickness = dndfkm202.Thickness;
                objFKM202.UOM = dndfkm202.UOM;
                objFKM202.RequiredDate = dndfkm202.RequiredDate;
                objFKM202.IsInsertedInPLM = dndfkm202.IsInsertedInPLM;
                //objFKM202.PLMError = dndfkm202.PLMError;

                if (copy <= 0)
                {
                    objFKM202.DeleverTo = dndfkm202.DeleverTo;
                    objFKM202.TASKUNIQUEID = dndfkm202.TASKUNIQUEID;
                }

                //objFKM202.DeleverStatus = dndfkm202.DeleverStatus;
                objFKM202.FindNo_Init = dndfkm202.FindNo_Init;
                objFKM202.NoOfPieces_Init = dndfkm202.NoOfPieces_Init;

                //objFKM202.KitStatus = dndfkm202.KitStatus;
                //objFKM202.MaterialStatus = dndfkm202.MaterialStatus;
                //objFKM202.PlannerStructure = dndfkm202.PlannerStructure;
                //objFKM202.KitLocation = dndfkm202.KitLocation;
                objFKM202.NodeColor = dndfkm202.NodeColor;
                objFKM202.IsPWHT = dndfkm202.IsPWHT;
                objFKM202.Element = dndfkm202.Element;
                objFKM202.RefCopyNodeId = dndfkm202.NodeId;
                db.FKM202.Add(objFKM202);
                db.SaveChanges();
                if (listCopyNodeDtl != null)
                {
                    listCopyNodeDtl.Add(new CopyNodeDtl { NewNodeId = objFKM202.NodeId, NodeId = dndfkm202.NodeId });
                }
                //is destination project is merged project in PDIN then copy document also.
                if (IsCopyDocument)
                {
                    string din = clsImplementationEnum.FKMSDocumentType.DIN.GetStringValue();
                    if (db.FKM107.Any(x => x.NodeId == dndfkm202.NodeId && x.DocType == din))
                    {
                        List<FKM107> objFKM107List = new List<FKM107>();
                        var doclist = db.FKM107.Where(x => x.NodeId == dndfkm202.NodeId && x.DocType == din).ToList();
                        foreach (var docItem in doclist)
                        {
                            FKM107 objFKM107 = new FKM107();
                            objFKM107.NodeId = objFKM202.NodeId;
                            objFKM107.DocumentNo = docItem.DocumentNo;
                            objFKM107.CreatedBy = objClsLoginInfo.UserName;
                            objFKM107.CreatedOn = DateTime.Now;
                            objFKM107.DocType = docItem.DocType;
                            objFKM107List.Add(objFKM107);
                        }
                        if (objFKM107List.Count > 0)
                        {
                            db.FKM107.AddRange(objFKM107List);
                            db.SaveChanges();
                        }
                    }
                }
            }
            return objFKM202;
        }

        #region Allocation

        [HttpPost]
        public ActionResult PLTAllocate(string sourceProject = "", int ParentNodeId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string objallocatation = string.Empty;
            string objproject = string.Empty;
            string objkitno = string.Empty;
            var AssemblyASM = clsImplementationEnum.NodeTypes.ASM.GetStringValue();
            var AssemblyTJF = clsImplementationEnum.NodeTypes.TJF.GetStringValue();
            var Plate = clsImplementationEnum.NodeTypes.PLT.GetStringValue();
            var listFKM202 = new List<FKM202>();
            List<FKM108> lstFKM108 = null;
            try
            {
                if (sourceProject != string.Empty)
                {
                    listFKM202 = db.FKM202.Where(i => i.ProductType != AssemblyASM && i.ProductType != AssemblyTJF && i.ProductType == Plate && i.Project == sourceProject).ToList();
                }
                else
                {
                    listFKM202 = db.FKM202.Where(i => i.ProductType != AssemblyASM && i.ProductType != AssemblyTJF && i.ProductType == Plate).ToList();
                }
                if (ParentNodeId != 0)
                {
                    listFKM202 = listFKM202.Where(i => i.ProductType != AssemblyASM && i.ProductType != AssemblyTJF && i.ProductType == Plate && i.Project == sourceProject && i.ParentNodeId == ParentNodeId).ToList();
                }

                var lstkitdist = listFKM202.Select(i => new { i.Project }).Distinct().ToList();

                foreach (var Proj in lstkitdist)
                {
                    var lstparentdist = listFKM202.Where(x => x.Project == Proj.Project).Select(x => x.ParentNodeId).Distinct().ToList();
                    var listforPLT = db.SP_FKMS_GETPLTALLOCATEDETAILS(Proj.Project).ToList();
                    lstFKM108 = new List<FKM108>();
                    foreach (var parentId in lstparentdist)
                    {
                        var kit = db.FKM202.Where(x => x.NodeId == parentId).Select(x => new { Project = x.Project, KitNumber = x.NodeKey + "-" + x.NodeId, NodeId = x.NodeId }).FirstOrDefault();
                        //insert FKM110
                        objallocatation = allocateInsert;
                        objproject = kit.Project;
                        objkitno = kit.KitNumber;
                        AllocateNodeInFKMS(objproject, objkitno, Plate, allocateInsert, db);

                        List<SP_FKMS_GETPLTALLOCATEDETAILS_Result> lstplt = new List<SP_FKMS_GETPLTALLOCATEDETAILS_Result>();
                        string Producttype = clsImplementationEnum.NodeTypes.PLT.GetStringValue();
                        var objFKM202 = listFKM202.Where(i => i.ParentNodeId == kit.NodeId).ToList();

                        foreach (var item in objFKM202)
                        {
                            double availableQty = 0;
                            double requiredQty = 0;
                            double allocatedExistQty = 0;
                            double finalAvailableQty = 0;
                            if (listforPLT.Any(x => x.Part == item.NodeKey && x.Partno == item.FindNo))
                            {
                                availableQty = Convert.ToDouble(listforPLT.Where(x => x.Part == item.NodeKey && x.Partno == item.FindNo).ToList().Sum(i => i.Qty));
                            }
                            requiredQty = item.NoOfPieces != null ? Convert.ToDouble(item.NoOfPieces) : 0;
                            var objParent = db.FKM202.Where(i => i.NodeId == item.ParentNodeId).FirstOrDefault();
                            if (objParent != null)
                            {
                                requiredQty = requiredQty * (objParent.NoOfPieces != null ? Convert.ToDouble(objParent.NoOfPieces) : 0);
                            }
                            //if (lstFKM108.Any(x => x.Project == sourceProject && x.NodeKey == item.NodeKey && x.FindNo == item.FindNo))
                            //{
                            //    allocatedExistQty = Convert.ToDouble(lstFKM108.Where(x => x.Project == sourceProject && x.NodeKey == item.NodeKey && x.FindNo == item.FindNo).ToList().Sum(c => c.AllocatedQty));
                            //}
                            finalAvailableQty = availableQty - allocatedExistQty;
                            if (finalAvailableQty >= requiredQty)
                            {
                                lstplt = listforPLT.Where(x => x.Part == item.NodeKey && x.Partno == item.FindNo).ToList();
                                foreach (var lstquantity in lstplt)
                                {
                                    if (Convert.ToDouble(lstquantity.Qty) > requiredQty)
                                    {
                                        lstFKM108.Add(new FKM108
                                        {
                                            Project = lstquantity.Project,
                                            FindNo = lstquantity.Partno,
                                            NodeKey = lstquantity.Part,
                                            NodeId = item.NodeId,
                                            PCRNo = lstquantity.PCRNo,
                                            PCRLineNo = lstquantity.PCRLineno,
                                            PCRLineRevNo = lstquantity.PCRlinerev,
                                            PCLNo = lstquantity.PCLNo,
                                            AllocatedQty = Convert.ToDecimal(requiredQty),
                                            CreatedBy = objClsLoginInfo.UserName,
                                            CreatedOn = DateTime.Now,
                                        });
                                        lstquantity.Qty = lstquantity.Qty - Convert.ToDecimal(requiredQty);
                                        break;
                                    }
                                    else
                                    {
                                        if (finalAvailableQty >= requiredQty)
                                        {
                                            lstFKM108.Add(new FKM108
                                            {
                                                Project = lstquantity.Project,
                                                FindNo = lstquantity.Partno,
                                                NodeKey = lstquantity.Part,
                                                NodeId = item.NodeId,
                                                PCRNo = lstquantity.PCRNo,
                                                PCRLineNo = lstquantity.PCRLineno,
                                                PCRLineRevNo = lstquantity.PCRlinerev,
                                                PCLNo = lstquantity.PCLNo,
                                                AllocatedQty = Convert.ToDouble(lstquantity.Qty) < requiredQty ? Convert.ToDecimal(lstquantity.Qty) : Convert.ToDecimal(requiredQty),
                                                CreatedBy = objClsLoginInfo.UserName,
                                                CreatedOn = DateTime.Now,
                                            });
                                            finalAvailableQty = finalAvailableQty - Convert.ToDouble(lstquantity.Qty);
                                            requiredQty = requiredQty - Convert.ToDouble(lstquantity.Qty);
                                            lstquantity.Qty = Convert.ToDouble(lstquantity.Qty) > requiredQty ? Convert.ToDecimal(lstquantity.Qty) - Convert.ToDecimal(requiredQty) : 0;

                                        }
                                    }
                                }
                            }
                        }
                    }
                    var lstProj = db.FKM108.Where(x => x.Project == Proj.Project).ToList();

                    if (lstFKM108.Count() > lstProj.Count())
                    {
                        db.FKM108.RemoveRange(lstProj);
                        db.FKM108.AddRange(lstFKM108);
                        db.SaveChanges();
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "FKMS Nodes Allocted successfully";

                    //Delete from FKM110
                    objallocatation = allocateDelete;
                    AllocateNodeInFKMS(objproject, objkitno, Plate, allocateDelete, db);
                    objallocatation = string.Empty;
                }
            }
            catch (Exception ex)
            {
                if (!String.IsNullOrEmpty(objallocatation))
                {
                    AllocateNodeInFKMS(objproject, objkitno, Plate, allocateDelete, db);
                }
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region PlannerTreeView
        [HttpPost]
        public ActionResult PlannerLoadTreeViewPartial(int HeaderId)
        {
            ViewBag.HeaderId = HeaderId;
            return PartialView("_ComparePhysicalTreePartial");
        }

        public ActionResult PlannerGetTreeNodes(int HeaderId)
        {
            //List<TreeviewEnt> listTreeviewEnt = new List<TreeviewEnt>();
            try
            {

                //TreeviewEnt treeviewEnt = new TreeviewEnt();
                //treeviewEnt.text = "Parent Node 1";
                //TreeviewEnt chileTreeviewEnt = new TreeviewEnt();
                //chileTreeviewEnt.text = "P1-C1";
                //chileTreeviewEnt.state.opened = true;
                //treeviewEnt.children.Add(chileTreeviewEnt);
                //listTreeviewEnt.Add(treeviewEnt);

                //treeviewEnt = new TreeviewEnt();
                //treeviewEnt.text = "Parent Node 2";
                //chileTreeviewEnt = new TreeviewEnt();
                //chileTreeviewEnt.text = "P2-C1";
                //chileTreeviewEnt.state.opened = true;
                //treeviewEnt.children.Add(chileTreeviewEnt);
                //listTreeviewEnt.Add(treeviewEnt);

                listTreeviewEnt = new List<TreeviewEnt>();
                listFKM202 = db.FKM202.Where(i => i.HeaderId == HeaderId).ToList();
                ComparelistFKM104 = db.FKM104.Where(i => i.HeaderId == HeaderId).ToList();

                var listParentNode = listFKM202.Where(i => i.ParentNodeId_Init == 0).ToList();
                foreach (var item in listParentNode)
                {
                    TreeviewEnt mainTreeviewEnt = new TreeviewEnt();
                    mainTreeviewEnt = PlannerGetNodesStructure(item, mainTreeviewEnt);
                    listTreeviewEnt.Add(mainTreeviewEnt);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(listTreeviewEnt, JsonRequestBehavior.AllowGet);
            }
            return Json(listTreeviewEnt, JsonRequestBehavior.AllowGet);
        }

        public TreeviewEnt PlannerGetNodesStructure(FKM202 objFKM202, TreeviewEnt treeviewEnt)
        {
            treeviewEnt.text = objFKM202.NodeName;
            treeviewEnt.id = objFKM202.NodeId.ToString() + "#" + objFKM202.HeaderId.ToString() + "#" + objFKM202.NodeTypeId.ToString() + "#" + objFKM202.ParentNodeId_Init.ToString();
            treeviewEnt.state.opened = true;

            string Changeiconcolor = "fa fa-folder icon-state-success icon-lg";
            string fadanger = " fa_danger";

            var chkkeyfind = ComparelistFKM104.Where(x => x.NodeKey == objFKM202.NodeKey_Init && x.FindNo == objFKM202.FindNo_Init).FirstOrDefault();
            if (chkkeyfind == null)
            {
                treeviewEnt.icon = Changeiconcolor + fadanger;
            }

            if (objFKM202.IsInsertedInPLM.HasValue && !objFKM202.IsInsertedInPLM.Value)
            {
                treeviewEnt.a_attr.style = "background:#fca7a7;font-weight:bold;";
                treeviewEnt.a_attr.title = "Part Key :" + objFKM202.NodeKey_Init + "\n" + "Quantity :" + objFKM202.NoOfPieces_Init + "\n" + "Material :" + objFKM202.MaterialCode + "-" + objFKM202.MaterialDescription + "\n" + "Find No :" + objFKM202.FindNo_Init + "\n" + objFKM202.PLMError;
                treeviewEnt.a_attr.isinsertedinplm = "false";
            }
            else
            {
                treeviewEnt.a_attr.isinsertedinplm = "true";
                treeviewEnt.a_attr.title = "Part Key :" + objFKM202.NodeKey_Init + "\n" + "Quantity :" + objFKM202.NoOfPieces_Init + "\n" + "Material :" + objFKM202.MaterialCode + "-" + objFKM202.MaterialDescription + "\n" + "Find No :" + objFKM202.FindNo_Init;
            }

            // for NoOfPieces check
            var chkPiece = ComparelistFKM104.Where(x => x.NoOfPieces == objFKM202.NoOfPieces_Init).FirstOrDefault();
            if (chkPiece == null)
            {
                treeviewEnt.icon = Changeiconcolor + fadanger;
            }

            //Parent count check
            var checklistnode = ComparelistFKM104.Where(x => x.FindNo == objFKM202.FindNo_Init).FirstOrDefault();
            if (checklistnode != null)
            {
                //For Update records
                //if (checklistnode.ParentNodeId != objFKM202.ParentNodeId || checklistnode.NodeName != objFKM202.NodeName || checklistnode.CreatedOn != objFKM202.CreatedOn || checklistnode.EditedOn != objFKM202.EditedOn)
                //{
                //    treeviewEnt.icon = Changeiconcolor + fadanger;
                //    //treeviewEnt.a_attr.style = "color:red;";
                //}

                //For Parent Count
                if (ComparelistFKM104.Where(x => x.ParentNodeId == objFKM202.NodeId).Count() != listFKM202.Where(x => x.ParentNodeId == checklistnode.NodeId).Count())
                {
                    treeviewEnt.icon = Changeiconcolor + fadanger;
                    //treeviewEnt.a_attr.style = "color:red;";
                }
            }

            if (objFKM202.ProductType != null)
            {
                if (objFKM202.ProductType.ToLower() != clsImplementationEnum.NodeTypes.ASM.GetStringValue().ToLower() && objFKM202.ProductType.ToLower() != clsImplementationEnum.NodeTypes.TJF.GetStringValue().ToLower())
                {
                    treeviewEnt.icon = PartIcon + fadanger;
                }
            }

            // For Insert Delete Records 
            //if (!ComparelistFKM104.Where(x => x.FindNo == objFKM202.FindNo).Any())
            //{
            //    treeviewEnt.icon = Changeiconcolor;
            //}

            if (listFKM202.Any(i => i.ParentNodeId == objFKM202.NodeId))
            {
                var childs = listFKM202.Where(i => i.ParentNodeId == objFKM202.NodeId).ToList();
                foreach (var item in childs)
                {
                    TreeviewEnt childnode = new TreeviewEnt();
                    childnode = PlannerGetNodesStructure(item, childnode);
                    treeviewEnt.children.Add(childnode);
                }
            }
            return treeviewEnt;
        }
        #endregion

        #region CompareTreeView
        [HttpPost]
        public ActionResult CompareLoadTreeViewPartial(int HeaderId)
        {
            ViewBag.HeaderId = HeaderId;
            return PartialView("_CompareTreePartial");
        }

        public ActionResult ComparePlannerDesignerTreePartial()
        {
            return PartialView("_ComparePlannerDesignerTreePartial");
        }

        public ActionResult CompareGetTreeNodes(int HeaderId)
        {
            //List<TreeviewEnt> listTreeviewEnt = new List<TreeviewEnt>();
            try
            {

                //TreeviewEnt treeviewEnt = new TreeviewEnt();
                //treeviewEnt.text = "Parent Node 1";
                //TreeviewEnt chileTreeviewEnt = new TreeviewEnt();
                //chileTreeviewEnt.text = "P1-C1";
                //chileTreeviewEnt.state.opened = true;
                //treeviewEnt.children.Add(chileTreeviewEnt);
                //listTreeviewEnt.Add(treeviewEnt);

                //treeviewEnt = new TreeviewEnt();
                //treeviewEnt.text = "Parent Node 2";
                //chileTreeviewEnt = new TreeviewEnt();
                //chileTreeviewEnt.text = "P2-C1";
                //chileTreeviewEnt.state.opened = true;
                //treeviewEnt.children.Add(chileTreeviewEnt);
                //listTreeviewEnt.Add(treeviewEnt);

                ComparelistTreeviewEnt = new List<TreeviewEnt>();
                ComparelistFKM104 = db.FKM104.Where(i => i.HeaderId == HeaderId).ToList();

                listFKM202 = db.FKM202.Where(i => i.HeaderId == HeaderId).ToList();
                //var comparelistParentNode = listFKM202.Where(i => i.ParentNodeId == 0).ToList();

                var listParentNode = ComparelistFKM104.Where(i => i.ParentNodeId == 0).ToList();

                //HashSet<string> diffids = new HashSet<string>(listFKM202.Select(s => s.NodeName));


                //var result = ComparelistFKM104.Where(m => !diffids.Contains(m.NodeName)).ToList();

                //var secondNotFirst = ComparelistFKM104.Except(listFKM202).ToList();

                //Compare2listFKM104 = new List<FKM104>();

                //foreach (var list1 in ComparelistFKM104)
                //{
                //    foreach (var list2 in listFKM202)
                //    {
                //        if (list1.NodeName == list2.NodeName && list1.ParentNodeId == list2.ParentNodeId && list1.CreatedOn == list2.CreatedOn && list1.EditedOn == list2.EditedOn)
                //        {
                //            Compare2listFKM104.Add(list1);
                //        }
                //    }
                //}

                //var list = Compare2listFKM104.Where(x => x.ParentNodeId == 0).ToList();


                foreach (var item in listParentNode)
                {
                    TreeviewEnt mainTreeviewEnt = new TreeviewEnt();
                    mainTreeviewEnt = CompareGetNodesStructure(item, mainTreeviewEnt);
                    ComparelistTreeviewEnt.Add(mainTreeviewEnt);
                }





                //foreach (var list1 in ComparelistTreeviewEnt)
                //{
                //    foreach (var list2 in listTreeviewEnt)
                //    {
                //        if (list1.id == list2.id && list1.text == list2.text && list1.CreatedOn == list2.CreatedOn && list1.EditedOn == list2.EditedOn)
                //        {
                //            Comparelist.Add(list1);
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(ComparelistTreeviewEnt, JsonRequestBehavior.AllowGet);
            }
            return Json(ComparelistTreeviewEnt, JsonRequestBehavior.AllowGet);
        }

        public TreeviewEnt CompareGetNodesStructure(FKM104 objFKM104, TreeviewEnt treeviewEnt)
        {
            treeviewEnt.text = objFKM104.NodeName;
            treeviewEnt.id = objFKM104.NodeId.ToString() + "#" + objFKM104.HeaderId.ToString() + "#" + objFKM104.NodeTypeId.ToString() + "#" + objFKM104.ParentNodeId.ToString();
            treeviewEnt.state.opened = true;
            treeviewEnt.a_attr.isinsertedinplm = "true";
            treeviewEnt.a_attr.title = "Part Key :" + objFKM104.NodeKey + "\n" + "Quantity :" + objFKM104.NoOfPieces + "\n" + "Material :" + objFKM104.MaterialCode + "-" + objFKM104.MaterialDescription + "\n" + "Find No :" + objFKM104.FindNo;

            string Changeiconcolor = "fa fa-folder icon-state-success icon-lg";
            string fadanger = " fa_danger";

            var chkkeyfind = listFKM202.Where(x => x.NodeKey_Init == objFKM104.NodeKey && x.FindNo == objFKM104.FindNo).FirstOrDefault();
            if (chkkeyfind == null)
            {
                treeviewEnt.icon = Changeiconcolor + fadanger;
            }

            var chkPiece = listFKM202.Where(x => x.NoOfPieces_Init == objFKM104.NoOfPieces).FirstOrDefault();
            if (chkPiece == null)
            {
                treeviewEnt.icon = Changeiconcolor + fadanger;
            }

            var checklistnode = listFKM202.Where(x => x.FindNo_Init == objFKM104.FindNo).FirstOrDefault();
            if (checklistnode != null)
            {
                //For Update records
                //if (checklistnode.ParentNodeId != objFKM104.ParentNodeId || checklistnode.NodeName != objFKM104.NodeName || checklistnode.CreatedOn != objFKM104.CreatedOn || checklistnode.EditedOn != objFKM104.EditedOn)
                //{
                //    treeviewEnt.icon = Changeiconcolor + fadanger;
                //    //treeviewEnt.a_attr.style = "color:red;";
                //}

                //For Parent Count
                if (ComparelistFKM104.Where(x => x.ParentNodeId == objFKM104.NodeId).Count() != listFKM202.Where(x => x.ParentNodeId_Init == checklistnode.NodeId).Count())
                {
                    treeviewEnt.icon = Changeiconcolor + fadanger;
                    //treeviewEnt.a_attr.style = "color:red;";
                }
            }

            if (objFKM104.ProductType != null)
            {
                if (objFKM104.ProductType.ToLower() != clsImplementationEnum.NodeTypes.ASM.GetStringValue().ToLower() && objFKM104.ProductType.ToLower() != clsImplementationEnum.NodeTypes.TJF.GetStringValue().ToLower())
                {
                    treeviewEnt.icon = PartIcon + fadanger;
                }
            }

            // For Insert Delete Records 
            //if (!listFKM202.Where(x => x.FindNo == objFKM104.FindNo).Any())
            //{
            //    treeviewEnt.icon = Changeiconcolor;
            //}

            if (ComparelistFKM104.Any(i => i.ParentNodeId == objFKM104.NodeId))
            {
                var childs = ComparelistFKM104.Where(i => i.ParentNodeId == objFKM104.NodeId).ToList();
                foreach (var item in childs)
                {
                    TreeviewEnt childnode = new TreeviewEnt();
                    childnode = CompareGetNodesStructure(item, childnode);
                    treeviewEnt.children.Add(childnode);
                }
            }
            return treeviewEnt;
        }

        public ActionResult GetComparePlannerDesignerTree(int HeaderId, int? NodeId)
        {
            listMergeNodeEnt = new List<MergeNodeEnt>();
            List<TreeviewEnt> listTreeviewEntPlanner = null;
            try
            {

                listMergeNodeEnt = GetPlannerDesignerNodeList(HeaderId, NodeId);
                #region Planner
                var listPlanner = listMergeNodeEnt.Where(i => i.IsPlanner == true).ToList();
                var listParentNode = listPlanner.Where(i => i.ParentNodeId == 0).ToList();
                listTreeviewEnt = new List<TreeviewEnt>();
                foreach (var item in listParentNode)
                {
                    TreeviewEnt mainTreeviewEnt = new TreeviewEnt();
                    mainTreeviewEnt = GetNodesStructureForMerge(item, mainTreeviewEnt, FKMSNodeGenerateFor.Planner);
                    listTreeviewEnt.Add(mainTreeviewEnt);
                }
                listTreeviewEntPlanner = listTreeviewEnt;
                #endregion

                #region  Designer
                var listDesigner = listMergeNodeEnt.Where(i => i.IsDesigner == true).ToList();
                var listParentNodeDesigner = listDesigner.Where(i => i.ParentNodeId == 0).ToList();
                listTreeviewEnt = new List<TreeviewEnt>();
                foreach (var item in listParentNodeDesigner)
                {
                    TreeviewEnt mainTreeviewEnt = new TreeviewEnt();
                    mainTreeviewEnt = GetNodesStructureForMerge(item, mainTreeviewEnt, FKMSNodeGenerateFor.Designer);
                    listTreeviewEnt.Add(mainTreeviewEnt);
                }
                #endregion

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(ComparelistTreeviewEnt, JsonRequestBehavior.AllowGet);
            }
            return Json(new
            {
                PlannerData = listTreeviewEntPlanner,
                DesignerData = listTreeviewEnt
            }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Accept
        [HttpPost]
        public ActionResult AcceptTreeView(int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var Project = db.FKM201.Where(i => i.HeaderId == HeaderId).FirstOrDefault().Project;
                listMergeNodeEnt = GetPlannerDesignerNodeList(HeaderId, null);
                var listNewDesigner = listMergeNodeEnt.Where(i => i.IsPlanner == false && i.IsDesigner == true).ToList();

                foreach (var item in listNewDesigner)
                {
                    FKM202 objFKM202 = new FKM202();

                    var listparent = db.FKM202.Where(i => (i.FindNo_Init == null ? "" : i.FindNo_Init) == item.ParentFindNo_Init && (i.NodeKey_Init == null ? "" : i.NodeKey_Init) == item.ParentNodeKey_Init && i.Project == Project).ToList();
                    foreach (var parent in listparent)
                    {
                        var SOBStatus = GetAndCheckNodeSOBKeyGenerate(parent.NodeId);
                        if (!SOBStatus.IsIssuedByStore)
                        {
                            var objFKM104 = db.FKM104.Where(i => i.NodeId == item.NodeId).FirstOrDefault();
                            if (!db.FKM202.Any(i => (i.FindNo_Init == null ? "" : i.FindNo_Init) == item.NodeFindNo && (i.NodeKey_Init == null ? "" : i.NodeKey_Init) == item.NodeKey && i.ParentNodeId_Init == parent.NodeId && i.Project == Project))
                            {
                                objFKM202.HeaderId = objFKM104.HeaderId;
                                objFKM202.ParentNodeId = parent.NodeId;
                                objFKM202.NodeTypeId = objFKM104.NodeTypeId;
                                objFKM202.CreatedBy = objClsLoginInfo.UserName;
                                objFKM202.CreatedOn = DateTime.Now;
                                objFKM202.NodeName = objFKM104.NodeName;
                                objFKM202.Project = objFKM104.Project;
                                objFKM202.FindNo = objFKM104.FindNo;
                                objFKM202.PartLevel = objFKM104.PartLevel;
                                objFKM202.Description = objFKM104.Description;
                                objFKM202.ItemGroup = objFKM104.ItemGroup;
                                objFKM202.RevisionNo = objFKM104.RevisionNo;
                                objFKM202.MaterialSpecification = objFKM104.MaterialSpecification;
                                objFKM202.MaterialDescription = objFKM104.MaterialDescription;
                                objFKM202.MaterialCode = objFKM104.MaterialCode;
                                objFKM202.ProductType = objFKM104.ProductType;
                                objFKM202.Length = objFKM104.Length;
                                objFKM202.Width = objFKM104.Width;
                                objFKM202.NoOfPieces = objFKM104.NoOfPieces;
                                objFKM202.Weight = objFKM104.Weight;
                                objFKM202.Thickness = objFKM104.Thickness;
                                objFKM202.UOM = objFKM104.UOM;
                                objFKM202.RequiredDate = objFKM104.RequiredDate;
                                objFKM202.NodeKey = objFKM104.NodeKey;
                                //objFKM202.IsInsertedInPLM = true;                            
                                //objFKM202.DeleverTo = objFKM104.DeleverTo;
                                //objFKM202.DeleverStatus = objFKM104.DeleverStatus;
                                objFKM202.FindNo_Init = objFKM104.FindNo;
                                objFKM202.NodeKey_Init = objFKM104.NodeKey;
                                objFKM202.NoOfPieces_Init = objFKM104.NoOfPieces;
                                objFKM202.ParentNodeId_Init = parent.NodeId;
                                objFKM202.Element = parent.Element;

                                db.FKM202.Add(objFKM202);
                                db.SaveChanges();
                            }
                        }
                    }
                }

                var listUpdatePlannerDesigner = listMergeNodeEnt.Where(i => i.IsPlanner == true && i.IsDesigner == true).ToList();
                foreach (var item in listUpdatePlannerDesigner)
                {
                    FKM202 objFKM202 = new FKM202();

                    var listparent = db.FKM202.Where(i => (i.FindNo_Init == null ? "" : i.FindNo_Init) == item.ParentFindNo_Init && (i.NodeKey_Init == null ? "" : i.NodeKey_Init) == item.ParentNodeKey_Init && i.Project == Project).ToList();
                    foreach (var parent in listparent)
                    {
                        var objFKM104 = db.FKM104.Where(i => i.NodeId == item.DesignerNodeId).FirstOrDefault();
                        if (db.FKM202.Any(i => i.FindNo_Init == (item.NodeFindNo == "" ? null : item.NodeFindNo) && i.NodeKey_Init == (item.NodeKey == "" ? null : item.NodeKey) && i.ParentNodeId_Init == parent.NodeId && i.Project == Project))
                        {
                            objFKM202 = db.FKM202.Where(i => i.FindNo_Init == (item.NodeFindNo == "" ? null : item.NodeFindNo) && i.NodeKey_Init == (item.NodeKey == "" ? null : item.NodeKey) && i.ParentNodeId_Init == parent.NodeId && i.Project == Project).FirstOrDefault();
                            if (objFKM202 != null && objFKM104 != null)
                            {
                                if (objFKM202.NoOfPieces_Init != objFKM104.NoOfPieces)
                                {
                                    var SOBStatus = GetAndCheckNodeSOBKeyGenerate(objFKM202.ParentNodeId.Value);
                                    if (!SOBStatus.IsIssuedByStore)
                                    {
                                        UpdateDesignerDataToPlannerData(objFKM104, objFKM202);

                                        var objFKM202Parent = db.FKM202.Where(i => i.NodeId == objFKM202.ParentNodeId.Value).FirstOrDefault();
                                        objFKM202Parent.KitStatus = null;
                                        objFKM202Parent.DeleverStatus = null;
                                        objFKM202Parent.MaterialStatus = null;
                                        objFKM202Parent.NodeColor = ColorFlagForFKMS.Red.GetStringValue();
                                    }
                                }
                                else
                                {
                                    UpdateDesignerDataToPlannerData(objFKM104, objFKM202);
                                }
                            }
                        }
                    }
                }
                //if (listUpdatePlannerDesigner.Count > 0)
                //{
                //    db.SaveChanges();
                //}

                //Remove Nodes from planner which node is move to other parent by designer but SOB not generated yet.
                var listRemoveNodes = listMergeNodeEnt.Where(i => i.IsPlanner == true && i.IsDesigner == false && i.NodeFindNo != "").Where(i => Convert.ToInt32(i.NodeFindNo) < 10000).ToList();
                foreach (var item in listRemoveNodes)
                {
                    var IsUpdate = true;
                    var lstRemoveFindNo = db.FKM202.Where(i => i.HeaderId == item.HeaderId && i.FindNo_Init == item.NodeFindNo && i.NodeKey_Init == item.NodeKey && i.ParentNodeId_Init == item.ParentNodeId_Init && i.Project == Project).ToList();
                    foreach (var removeitem in lstRemoveFindNo)
                    {
                        var SOBStatus = GetAndCheckNodeSOBKeyGenerate(removeitem.ParentNodeId.Value);
                        if (!SOBStatus.IsIssuedByStore)
                        {
                            InsertNodesInFKM125(db, removeitem, "AcceptTreeView"); //Task Id:- 20875
                            try
                            {
                                var UpdateTree = "0";
                                List<FKM202> ErrorList = null;
                                RemoveNonPlateDetails(removeitem, ref IsUpdate, ref UpdateTree, ref ErrorList);
                            }
                            catch (Exception ex)
                            {
                                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                            }

                            var listFKM108 = db.FKM108.Where(i => i.NodeId == removeitem.NodeId).ToList();
                            db.FKM108.RemoveRange(listFKM108);

                            var listFKM106 = db.FKM106.Where(i => i.NodeId == removeitem.NodeId).ToList();
                            db.FKM106.RemoveRange(listFKM106);

                            db.FKM202.Remove(removeitem);
                        }
                    }
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                objResponseMsg.HeaderId = HeaderId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            finally
            {
                context = null;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet); ;
        }

        public SOBKeyStatusEnt GetAndCheckNodeSOBKeyGenerate(int ParentNodeId)
        {
            SOBKeyStatusEnt ent = new SOBKeyStatusEnt();
            try
            {
                var objFKM202 = db.FKM202.Where(i => i.NodeId == ParentNodeId).FirstOrDefault();
                var objFKM112 = db.FKM112.Where(i => i.NodeId == ParentNodeId && !string.IsNullOrEmpty(i.SOBKey)).FirstOrDefault();
                if (objFKM112 != null)
                {
                    ent.IsSOBKeyGenerated = true;
                    ent.SOBKeyStatus = objFKM112.Status;
                    if (!string.IsNullOrEmpty(objFKM112.Status) && objFKM112.Status.ToLower() == ("Issued By Store").ToLower())
                    {
                        ent.IsIssuedByStore = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return ent;
        }

        public void UpdateDesignerDataToPlannerData(FKM104 objFKM104, FKM202 objFKM202)
        {
            objFKM202.NodeName = objFKM104.NodeName;
            objFKM202.PartLevel = objFKM104.PartLevel;
            objFKM202.Description = objFKM104.Description;
            objFKM202.ItemGroup = objFKM104.ItemGroup;
            objFKM202.RevisionNo = objFKM104.RevisionNo;
            objFKM202.MaterialSpecification = objFKM104.MaterialSpecification;
            objFKM202.MaterialDescription = objFKM104.MaterialDescription;
            objFKM202.MaterialCode = objFKM104.MaterialCode;
            objFKM202.ProductType = objFKM104.ProductType;
            objFKM202.Length = objFKM104.Length;
            objFKM202.Width = objFKM104.Width;
            objFKM202.NoOfPieces = objFKM104.NoOfPieces;
            objFKM202.Weight = objFKM104.Weight;
            objFKM202.Thickness = objFKM104.Thickness;
            objFKM202.UOM = objFKM104.UOM;
            objFKM202.RequiredDate = objFKM104.RequiredDate;
            objFKM202.NoOfPieces_Init = objFKM104.NoOfPieces;
            objFKM202.EditedBy = objClsLoginInfo.UserName;
            objFKM202.EditedOn = DateTime.Now;
            objFKM202.Element = objFKM104.Element;
        }
        #endregion

        #region Filter On Tree Task 15623 On 11-07-2018
        [HttpPost]
        public ActionResult GetNodeWiseFilter(int HeaderId)
        {
            ViewBag.HeaderId = HeaderId;
            List<FKM202> lstFKM202 = null;

            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                lstFKM202 = db.FKM202.Where(x => x.HeaderId == HeaderId && !string.IsNullOrEmpty(x.DeleverTo)).ToList();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString(); ;
            }
            return PartialView("_NodeWiseFilterPartial", lstFKM202);
        }
        #endregion

        #region Refresh
        [HttpPost]
        public ActionResult GETHBOMLIST(int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                context = new IEMQSEntitiesContext();
                lstFKM104 = new List<FKM104>();

                var objFKM201 = db.FKM201.Where(i => i.HeaderId == HeaderId).FirstOrDefault();
                lstgetAllbom = db.SP_FKMS_HBOM_FOR_PROJECT(objFKM201.Project).ToList();
                var Status = InsertHBOMData(objFKM201, objClsLoginInfo.UserName, true);
                if (Status)
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                    objResponseMsg.HeaderId = HeaderId;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                    objResponseMsg.HeaderId = HeaderId;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            finally
            {
                context = null;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public FKM104 SetNodesFKM104(SP_FKMS_HBOM_FOR_PROJECT_Result objgetbomlist, FKM104 mainfkm104, int HeaderId, int objparentid, string UserName = "")
        {
            if (UserName == "")
            {
                UserName = objClsLoginInfo.UserName;
            }
            mainfkm104.Project = objgetbomlist.project;
            mainfkm104.NodeName = objgetbomlist.Description;

            if (lstgetbom.Any(i => i.ParentPart == objgetbomlist.Part && i.project == objgetbomlist.project))
            {
                var childs = lstgetbom.Where(i => i.ParentPart == objgetbomlist.Part && i.project == objgetbomlist.project).ToList();
                foreach (var item in childs)
                {
                    if (item.Element != JIGFIXElementNo)
                    {
                        FKM104 parentfkm104 = new FKM104();
                        parentfkm104 = InsertGETHBOMFKMS(item, HeaderId, parentfkm104, objparentid, UserName);

                        FKM104 mainFKM104 = new FKM104();
                        int objparentchildid = parentfkm104.NodeId;
                        mainfkm104 = SetNodesFKM104(item, mainFKM104, HeaderId, objparentchildid, UserName);
                        lstFKM104.Add(mainFKM104);
                    }
                }
            }
            return mainfkm104;
        }
        #endregion

        #region Copy
        [HttpPost]
        public ActionResult GetCopyDetails(string copyProject, string TemplateType = "B")
        {
            ViewBag.CopyProject = copyProject;
            ViewBag.lstProduct = db.PBP001.Select(s => new SelectListItem() { Value = s.ProductCode, Text = s.Product }).ToList();
            ViewBag.UserLocationCode = objClsLoginInfo.Location;
            ViewBag.UserLocation = objClsLoginInfo.LocationName;
            ViewBag.TemplateType = TemplateType;
            return PartialView("_LoadFKMSCopyPartial");
        }

        [HttpPost]
        public ActionResult GetProjectResult(string term, string location = "")
        {
            List<Projects> lstProjects = new List<Projects>();
            var objCopyproject = FN_GET_ALL_COPYPROJECTS_FOR_NODE_FKM202().ToList();
            var objAccessProjects = db.fn_GetProjectAccessForEmployee(objClsLoginInfo.UserName, location).ToList();
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstProjects = (from cp in objCopyproject
                               where objAccessProjects.Contains(cp.Project) && cp.Project.Contains(term)
                               // 11999A Project have Description Null 
                               //where objAccessProjects.Contains(cp.Project) && (cp.Project.ToLower().Contains(term.ToLower()) || cp.ProjDesc.ToLower().Contains(term.ToLower()))
                               select new Projects
                               {
                                   Value = cp.Project,
                                   projectCode = cp.Project,
                                   projectDescription = cp.Project + " - " + cp.ProjDesc,
                                   Text = cp.Project + " - " + cp.ProjDesc
                               }
                              ).Distinct().ToList();
            }
            else
            {
                lstProjects = (from cp in objCopyproject
                               where objAccessProjects.Contains(cp.Project)
                               select new Projects
                               {
                                   Value = cp.Project,
                                   projectCode = cp.Project,
                                   projectDescription = cp.Project + " - " + cp.ProjDesc,
                                   Text = cp.Project + " - " + cp.ProjDesc
                               }
                              ).Distinct().Take(10).ToList();
            }
            return Json(lstProjects, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetSourceProjectResult(string term, string location = "")
        {
            List<Projects> lstProjects = new List<Projects>();
            var objAccessProjects = db.fn_GetProjectAccessForEmployee(objClsLoginInfo.UserName, location).ToList();
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstProjects = (from fkm in db.FKM201
                               join com in db.COM001 on fkm.Project equals com.t_cprj
                               where objAccessProjects.Contains(fkm.Project) && fkm.Project.Contains(term)
                               // 11999A Project have Description Null 
                               //where objAccessProjects.Contains(cp.Project) && (cp.Project.ToLower().Contains(term.ToLower()) || cp.ProjDesc.ToLower().Contains(term.ToLower()))
                               select new Projects
                               {
                                   Value = fkm.Project,
                                   projectCode = fkm.Project,
                                   projectDescription = fkm.Project + " - " + com.t_dsca,
                                   Text = fkm.Project + " - " + com.t_dsca
                               }
                              ).Distinct().ToList();
            }
            else
            {
                lstProjects = (from fkm in db.FKM201
                               join com in db.COM001 on fkm.Project equals com.t_cprj
                               where objAccessProjects.Contains(fkm.Project)
                               select new Projects
                               {
                                   Value = fkm.Project,
                                   projectCode = fkm.Project,
                                   projectDescription = fkm.Project + " - " + com.t_dsca,
                                   Text = fkm.Project + " - " + com.t_dsca
                               }
                              ).Distinct().Take(10).ToList();
            }
            return Json(lstProjects, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult copyDetails(copyData copyData, List<copyNodeList> copyValueList, int TemplateId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                bool IsPrimaryProjectInDIN = false;
                bool IsSecondaryProjectInDIN = false;

                List<FKM201> objMainFKM201 = db.FKM201.ToList();
                var TemplateContent = "";
                if (TemplateId > 0)
                {
                    TemplateContent = db.FKM251.FirstOrDefault(f => f.Id == TemplateId).TemplateContent;
                    var lstNodeData = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Models.NodeData>>(TemplateContent);
                    var NodeId = lstNodeData.FirstOrDefault().key;
                    var objFKM202 = db.FKM202.FirstOrDefault(f => f.NodeId == NodeId);
                    copyData.sourceProject = objFKM202 == null? null: objFKM202.Project;
                }
                var objFKM201 = objMainFKM201.FirstOrDefault(i => i.Project == copyData.sourceProject);
                if (objFKM201 != null)
                {
                    List<FKM203> objFKM203 = db.FKM203.ToList();
                    FKM201 newObjFKM201 = new FKM201();
                    int NodeTypeAssId = objFKM203.Where(x => x.NodeType.ToLower() == clsImplementationEnum.FKMSNodeType.Assembly.GetStringValue().ToLower()).FirstOrDefault().Id;
                    var copyfkm202 = db.FKM202.Where(x => x.Project == copyData.destProject).ToList();
                    if (copyfkm202.Count() > 0)
                    {
                        db.FKM202.RemoveRange(copyfkm202);
                        /*if (copyfkm202.Any(x => x.NodeTypeId == NodeTypeAssId))
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Data already exists";
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }*/
                        newObjFKM201 = objMainFKM201.Where(x => x.Project == copyData.destProject).FirstOrDefault();
                    }
                    else
                    {
                        //check destination project is in PDIN primary or identical project
                        var productType = db.PDN001.Where(x => x.Project == copyData.destProject).GroupBy(u => u.Product).ToDictionary(g => g.Key, g => g.Max(item => item.IssueNo)).FirstOrDefault();
                        if (productType.Key != null)
                        {
                            IsPrimaryProjectInDIN = true;
                        }
                        else
                        {
                            var objPDN001List = db.PDN001.Where(x => x.Project.ToLower() == copyData.sourceProject.ToLower() && !string.IsNullOrEmpty(x.IProject)).ToList();
                            foreach (var obj in objPDN001List)
                            {
                                string[] strList = obj.IProject.Split(',');
                                foreach (var strIProject in strList)
                                {
                                    if (strIProject.ToLower() == copyData.destProject.ToLower())
                                    {
                                        IsSecondaryProjectInDIN = true;
                                        break;
                                    }
                                }
                            }
                        }

                        if (!IsPrimaryProjectInDIN && !IsSecondaryProjectInDIN)
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Destination Project does not exist in merged projects of source project and also does not exist as primary project in Planning Module";
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }

                        newObjFKM201.Project = copyData.destProject;
                        newObjFKM201.ZeroDate = Convert.ToDateTime(copyData.zeroDate);
                        newObjFKM201.CDD = Convert.ToDateTime(copyData.cdd);
                        newObjFKM201.Customer = copyData.customer;
                        newObjFKM201.RevNo = 0;
                        newObjFKM201.ApprovedBy = objFKM201.ApprovedBy;
                        newObjFKM201.ProductType = objFKM201.ProductType;
                        newObjFKM201.Status = clsImplementationEnum.LTFPSHeaderStatus.Draft.GetStringValue();
                        newObjFKM201.CreatedBy = objClsLoginInfo.UserName;
                        newObjFKM201.CreatedOn = DateTime.Now;
                        newObjFKM201.IsCopyInProgress = false;
                        newObjFKM201.PlannerPsno = objFKM201.PlannerPsno;
                        db.FKM201.Add(newObjFKM201);
                        db.SaveChanges();
                    }

                    #region Add Default Entry FKM202
                    if (newObjFKM201.HeaderId > 0)
                    {
                        DefaultEntryPLM(newObjFKM201.HeaderId, newObjFKM201.Project, copyData.sourceProject, copyValueList, TemplateContent);
                    }
                    foreach (var item in copyValueList.Where(i => i.destiFindNo != "00"))
                    {
                        var objFKM202Update = db.FKM202.Where(i => i.Project == copyData.destProject && i.FindNo == item.destiFindNo).FirstOrDefault();
                        objFKM202Update.RefCopyNodeId = Convert.ToInt32(item.sourceNodeId);
                    }
                    db.SaveChanges();
                    #endregion

                    #region Image copy
                    var folderPath = "FKM201/" + objFKM201.HeaderId + "/R" + objFKM201.RevNo;
                    var destinationPath = "FKM201/" + newObjFKM201.HeaderId + "/R" + objFKM201.RevNo;

                    var existing = (new clsFileUpload()).GetDocuments(folderPath);

                    if (existing.Any())
                    {
                        (new clsFileUpload()).CopyFolderContentsAsync(folderPath, destinationPath);
                    }
                    #endregion

                    var headerId = newObjFKM201.HeaderId;
                    clspsno = objClsLoginInfo.UserName;

                    //Task<int> copyPLMtask1 = LongRunningCopyPLMAllocationAsync(copyData, copyValueList, headerId, newObjFKM201.Project, newObjFKM201.Status, IsPrimaryProjectInDIN, IsSecondaryProjectInDIN);

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "FKMS copied successfully";

                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Record not found";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #region CopyDetails Async

        public async Task<int> LongRunningCopyPLMAllocationAsync(copyData copyData, List<copyNodeList> copyValueList, int headerId, string project, string Status, bool IsPrimaryProjectInDIN, bool IsSecondaryProjectInDIN)
        {
            await Task.Run(() => CopyPLMAllocationAsync(copyData, copyValueList, headerId, project, Status, IsPrimaryProjectInDIN, IsSecondaryProjectInDIN));
            return 1;
        }

        public void CopyPLMAllocationAsync(copyData copyData, List<copyNodeList> copyValueList, int headerId, string project, string Status, bool IsPrimaryProjectInDIN = false, bool IsSecondaryProjectInDIN = false)
        {
            List<CopyNodeDtl> listCopyNodeDtl = new List<CopyNodeDtl>();

            listFKM202 = db.FKM202.Where(x => x.Project == copyData.sourceProject).ToList().OrderBy(x => x.NodeId).ToList();

            var lstdestfkm202 = db.FKM202.Where(x => x.Project == copyData.destProject).ToList();

            if (listFKM202.Count > 0)
            {
                foreach (var item in copyValueList)
                {
                    int sourceNodeId = Convert.ToInt32(item.sourceNodeId);
                    var sourcenode = db.FKM202.Where(x => x.Project == copyData.sourceProject && x.NodeId == sourceNodeId).FirstOrDefault();

                    var destinode = lstdestfkm202.Where(x => x.FindNo == item.destiFindNo).FirstOrDefault();
                    if (destinode != null)
                    {
                        //destinode.PlannerStructure = sourcenode.PlannerStructure;
                        //db.SaveChanges();

                        var childs = listFKM202.Where(i => i.ParentNodeId == sourceNodeId).ToList().OrderBy(i => i.ParentNodeId_Init).ThenBy(x => x.NodeId).ToList();
                        foreach (var childitem in childs)
                        {
                            //if secondary project then only copy item. PLT/NPLT
                            bool Copy = true;
                            bool IsChangeNodeKey = true;
                            bool IsCopyDocument = false;

                            string ASM = NodeTypes.ASM.GetStringValue().ToLower();
                            string TJF = NodeTypes.ASM.GetStringValue().ToLower();
                            if (!string.IsNullOrWhiteSpace(childitem.ProductType) && childitem.ProductType.ToLower() != ASM && childitem.ProductType.ToLower() != TJF)
                            {
                                if (IsPrimaryProjectInDIN)
                                {
                                    Copy = false;
                                }
                                else if (IsSecondaryProjectInDIN)
                                {
                                    Copy = true;
                                    IsChangeNodeKey = false;
                                }
                            }

                            if (IsSecondaryProjectInDIN)
                                IsCopyDocument = true;

                            if (Copy)
                            {
                                FKM202 parentfkm202 = new FKM202();
                                copyDestiProject = copyData.destProject;
                                parentfkm202 = InsertDND(ref listCopyNodeDtl, childitem, headerId, parentfkm202, destinode.NodeId, 0, 1, IsChangeNodeKey, IsCopyDocument);

                                FKM202 mainFKM202 = new FKM202();
                                int objparentid = parentfkm202.NodeId;
                                mainFKM202 = SetNodesFKM202(ref listCopyNodeDtl, childitem, mainFKM202, headerId, objparentid, 1, IsPrimaryProjectInDIN, IsSecondaryProjectInDIN);
                            }
                        }
                    }
                }
            }
            //Update Parent Node Id Init 
            if (listCopyNodeDtl.Count > 0)
            {
                foreach (var item in listCopyNodeDtl)
                {
                    var objParentInit = db.FKM202.Where(i => i.NodeId == item.NodeId).FirstOrDefault().ParentNodeId_Init;
                    var ActualParentInit = db.FKM202.Where(i => i.RefCopyNodeId == objParentInit && i.Project == copyData.destProject).FirstOrDefault().NodeId;
                    var objFKM202 = db.FKM202.Where(i => i.NodeId == item.NewNodeId).FirstOrDefault();
                    objFKM202.ParentNodeId_Init = ActualParentInit;
                }
                db.SaveChanges();
            }

            #region  -- (Observation 16615 14/09/2018 Plm Push data commiteed)
            //Observation Id#17652
            if (IsPrimaryProjectInDIN)
            {
                var objFKM202 = lstdestfkm202.Where(x => x.HeaderId == headerId && x.PlannerStructure != null).FirstOrDefault();
                if (objFKM202 != null)
                {
                    #region Insert Into PLM

                    string assemblyType = clsImplementationEnum.FKMSNodeType.Assembly.GetStringValue();
                    var assid = db.FKM203.Where(x => x.NodeType.ToLower() == assemblyType.ToLower()).Select(x => x.Id).FirstOrDefault();
                    bool isSuccess = InsertInToPLM(headerId, objFKM202.NodeId, StartFindNo, objFKM202, assid, project, Status, 1);

                    #endregion
                }
            }

            #endregion

            #region --(Observation 18309 04/12/2018 IsCheckCopy Progress)

            var objfkm201 = db.FKM201.Where(x => x.HeaderId == headerId).FirstOrDefault();
            if (objfkm201 != null)
            {
                objfkm201.IsCopyInProgress = false;
                db.SaveChanges();
            }

            #endregion
        }

        #endregion

        //public FKM202 SetNodesFKM202(FKM202 objgetbomlist, FKM202 mainfkm202, int HeaderId, int objparentid)
        //{
        //    mainfkm202.Project = objgetbomlist.Project;
        //    mainfkm202.NodeName = objgetbomlist.Description;

        //    if (listFKM202.Any(i => i.ParentNodeId == objgetbomlist.NodeId))
        //    {
        //        var childs = listFKM202.Where(i => i.ParentNodeId == objgetbomlist.NodeId).ToList();
        //        foreach (var item in childs)
        //        {
        //            FKM202 parentfkm202 = new FKM202();
        //            parentfkm202 = InsertDND(item, HeaderId, parentfkm202, objparentid);

        //            FKM202 mainFKM202 = new FKM202();
        //            int objparentchildid = parentfkm202.NodeId;
        //            mainfkm202 = SetNodesFKM202(item, mainFKM202, HeaderId, objparentchildid);
        //            listCopyFKM202.Add(mainFKM202);
        //        }
        //    }
        //    return mainfkm202;
        //}

        #endregion

        #region TreeView
        [HttpPost]
        public ActionResult LoadTreeViewPartial(int HeaderId, string userRole, string actionform, string status, bool IsDisplayOnly = false)
        {
            ViewBag.IsDisplayOnly = IsDisplayOnly;
            ViewBag.HeaderId = HeaderId;
            ViewBag.UserRole = userRole;
            ViewBag.buttonname = actionform;
            ViewBag.Status = status;
            ViewBag.IsCompare = db.FKM104.Where(x => x.HeaderId == HeaderId).Any();
            return PartialView("_TreeViewPartial");
        }

        public ActionResult GetTreeNodes(int HeaderId, int NodeId)
        {
            try
            {
                listTreeviewEnt = new List<TreeviewEnt>();
                listASMColors(HeaderId);

                if (NodeId > 0)
                {
//                  listFKM202 = db.SP_FKMS_GET_NODETREE(NodeId, "").ToList();
                }
                else
                {
                    listFKM202 = db.FKM202.Where(i => i.HeaderId == HeaderId).ToList();
                }

                var listParentNode = listFKM202.Where(i => i.ParentNodeId == 0).ToList();
                foreach (var item in listParentNode)
                {
                    TreeviewEnt mainTreeviewEnt = new TreeviewEnt();
                    mainTreeviewEnt = GetNodesStructure(item, mainTreeviewEnt);
                    listTreeviewEnt.Add(mainTreeviewEnt);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(listTreeviewEnt, JsonRequestBehavior.AllowGet);
            }
            return Json(listTreeviewEnt, JsonRequestBehavior.AllowGet);
        }

        //for PLM TreeView Data
        public TreeviewEnt GetNodesStructureProjects(SP_FKMS_HBOM_FOR_PROJECT_Result objFKMprojects, TreeviewEnt treeviewEnt)
        {
            treeviewEnt.text = objFKMprojects.Description;
            treeviewEnt.id = objFKMprojects.PartLevel.ToString() + "#" + objFKMprojects.FindNo.ToString();
            treeviewEnt.state.opened = true;
            treeviewEnt.a_attr.ondblclick = "Rename()";
            if (projectresult.Any(i => i.ParentPart == objFKMprojects.Part))
            {
                var childs = projectresult.Where(i => i.ParentPart == objFKMprojects.Part).ToList();
                foreach (var item in childs)
                {
                    TreeviewEnt childnode = new TreeviewEnt();
                    childnode = GetNodesStructureProjects(item, childnode);
                    treeviewEnt.children.Add(childnode);
                }
            }
            return treeviewEnt;
        }

        public TreeviewEnt GetNodesStructure(FKM202 objFKM202, TreeviewEnt treeviewEnt)
        {
            string iconColor = string.Empty;
            treeviewEnt.text = string.Empty;
            string Changeiconcolor = "fa fa-folder icon-state-success icon-lg";
            string uom = objFKM202.UOM != null ? objFKM202.UOM : "-";
            //treeviewEnt.text = objFKM202.NodeName + "(QTY :" + objFKM202.NoOfPieces_Init + "," + uom + ")";
            treeviewEnt.id = objFKM202.NodeId.ToString() + "#" + objFKM202.HeaderId.ToString() + "#" + objFKM202.NodeTypeId.ToString() + "#" + objFKM202.ParentNodeId.ToString();
            treeviewEnt.li_attr.scrollid = objFKM202.NodeId.ToString();
            treeviewEnt.li_attr.kstatus = objFKM202.KitStatus != null ? objFKM202.KitStatus.ToString() : "";
            treeviewEnt.li_attr.dstatus = objFKM202.DeleverStatus != null ? objFKM202.DeleverStatus.ToString() : "";
            treeviewEnt.li_attr.mstatus = objFKM202.MaterialStatus != null ? objFKM202.MaterialStatus.ToString() : "";
            treeviewEnt.li_attr.FullkitArea = objFKM202.FullkitArea != null ? objFKM202.FullkitArea.ToString() : "";
            treeviewEnt.li_attr.qty = !string.IsNullOrEmpty(objFKM202.NoOfPieces.ToString()) ? objFKM202.NoOfPieces.ToString() : "";
            treeviewEnt.a_attr.scrollid = objFKM202.NodeId.ToString() + "_anchor";
            treeviewEnt.state.opened = true;
            //if (objFKM202.NodeTypeId == 1) //For Project No Node
            //{
            //    treeviewEnt.state.selected = true;
            //}
            //treeviewEnt.type = "file";
            treeviewEnt.a_attr.ondblclick = "Rename()";

            if (objFKM202.IsInsertedInPLM.HasValue && !objFKM202.IsInsertedInPLM.Value)
            {
                treeviewEnt.a_attr.style = "background:#fca7a7;font-weight:bold;";
                treeviewEnt.a_attr.title = "Part Key :" + objFKM202.NodeKey + "\n" + "Quantity :" + objFKM202.NoOfPieces + "\n" + "Material :" + objFKM202.MaterialCode + "-" + objFKM202.MaterialDescription + "\n" + "Find No :" + objFKM202.FindNo + "\n" + "Node Id :" + objFKM202.NodeId + "\n" + "Product Type :" + objFKM202.ProductType + "\n" + objFKM202.PLMError;
                treeviewEnt.a_attr.isinsertedinplm = "false";
            }
            else
            {
                treeviewEnt.a_attr.isinsertedinplm = "true";
                treeviewEnt.a_attr.title = "Part Key :" + objFKM202.NodeKey + "\n" + "Quantity :" + objFKM202.NoOfPieces + "\n" + "Material :" + objFKM202.MaterialCode + "-" + objFKM202.MaterialDescription + "\n" + "Find No :" + objFKM202.FindNo + "\n" + "Node Id :" + objFKM202.NodeId + "\n" + "Product Type :" + objFKM202.ProductType + "\n" + "Kit Status :" + objFKM202.KitStatus + "\n" + "Delivery Status :" + objFKM202.DeleverStatus + "\n" + "Material Status :" + objFKM202.MaterialStatus;
            }
            List<FKM203> objEquipFKM203 = db.FKM203.ToList();
            var EquipId = objEquipFKM203.Where(x => x.Id == objFKM202.NodeTypeId).FirstOrDefault();

            if (objFKM202.ProductType != null)
            {
                if (objFKM202.ProductType.ToLower() != clsImplementationEnum.NodeTypes.ASM.GetStringValue().ToLower() && objFKM202.ProductType.ToLower() != clsImplementationEnum.NodeTypes.TJF.GetStringValue().ToLower())
                {
                    string ColorFlag = SetColorFlag(objFKM202.NodeId, objFKM202.NoOfPieces, objFKM202.ProductType);
                    if (ColorFlag == clsImplementationEnum.ColorFlagForFKMS.Green.GetStringValue())
                    {
                        treeviewEnt.text += "<i class='fa fa-circle icon-stack-1x fa-success'></i>";
                    }
                    else if (ColorFlag == clsImplementationEnum.ColorFlagForFKMS.Yellow.GetStringValue())
                    {
                        treeviewEnt.text += "<i class='fa fa-circle icon-stack-1x fa-warning'></i>";
                    }
                    else if (ColorFlag == clsImplementationEnum.ColorFlagForFKMS.Red.GetStringValue())
                    {
                        treeviewEnt.text += "<i class='fa fa-circle icon-stack-1x fa-danger'></i>";
                    }
                    treeviewEnt.icon = PartIcon + iconColor;
                    iconColor = string.Empty;
                    treeviewEnt.li_attr.asmtype = clsImplementationEnum.NodeTypes.PLT.GetStringValue();
                    treeviewEnt.a_attr.color = ColorFlag;

                    var parentobjFKM202 = listFKM202.Where(i => i.NodeId == objFKM202.ParentNodeId).FirstOrDefault();
                    treeviewEnt.li_attr.IsRequested = IsKitRequested(parentobjFKM202);
                }
                else
                {
                    //string ColorFlag = PLTSetColorFlag(objFKM202.NodeId);
                    string ColorFlag = objFKM202.NodeColor;
                    if (ColorFlag == clsImplementationEnum.ColorFlagForFKMS.Green.GetStringValue())
                    {
                        treeviewEnt.text += "<i class='fa fa-circle icon-stack-1x fa-success'></i>";
                    }
                    else if (ColorFlag == clsImplementationEnum.ColorFlagForFKMS.Yellow.GetStringValue())
                    {
                        treeviewEnt.text += "<i class='fa fa-circle icon-stack-1x fa-warning'></i>";
                    }
                    else if (ColorFlag == clsImplementationEnum.ColorFlagForFKMS.Red.GetStringValue())
                    {
                        treeviewEnt.text += "<i class='fa fa-circle icon-stack-1x fa-danger'></i>";
                    }
                    else if (ColorFlag == clsImplementationEnum.ColorFlagForFKMS.Violet.GetStringValue())
                    {
                        treeviewEnt.text += "<i class='fa fa-circle icon-stack-1x fa-voilet'></i>";
                    }
                    else if (ColorFlag == clsImplementationEnum.ColorFlagForFKMS.Grey.GetStringValue())
                    {
                        treeviewEnt.text += "<i class='fa fa-circle icon-stack-1x fa-gray'></i>";
                    }
                    else if (ColorFlag == clsImplementationEnum.ColorFlagForFKMS.Orange.GetStringValue())
                    {
                        treeviewEnt.text += "<i class='fa fa-circle icon-stack-1x fa-orange'></i>";
                    }
                    else if (ColorFlag == clsImplementationEnum.ColorFlagForFKMS.Blue.GetStringValue())
                    {
                        treeviewEnt.text += "<i class='fa fa-circle icon-stack-1x fa-blue'></i>";
                    }
                    else if (ColorFlag == clsImplementationEnum.ColorFlagForFKMS.Default.GetStringValue())
                    {
                        treeviewEnt.text += "";
                    }
                    treeviewEnt.icon += Changeiconcolor + iconColor;
                    iconColor = string.Empty;
                    treeviewEnt.li_attr.asmtype = clsImplementationEnum.NodeTypes.ASM.GetStringValue();
                    treeviewEnt.a_attr.color = ColorFlag;
                    treeviewEnt.li_attr.IsRequested = IsKitRequested(objFKM202);
                }
            }
            // observaiton findno change 17899
            treeviewEnt.text += (objFKM202.FindNo != null ? objFKM202.FindNo + "-" : "") + objFKM202.NodeName + "<span title='FindNo:" + objFKM202.FindNo + "'></span>";//+ "(QTY :" + objFKM202.NoOfPieces_Init + "," + uom + ") ";


            var chkasm = listFKM202.Where(x => x.NodeKey_Init == x.FindNo_Init && x.NodeId == objFKM202.NodeId).FirstOrDefault();
            if (chkasm != null)
            {
                treeviewEnt.li_attr.isplannerasm = 1;
            }
            else
            {
                treeviewEnt.li_attr.isplannerasm = 0;
            }

            var lstLevelId = listFKM202.Where(x => x.NodeTypeId == objFKM202.NodeTypeId).FirstOrDefault();
            if (lstLevelId != null)
            {
                treeviewEnt.li_attr.Level = EquipId != null ? EquipId.NodeLevel : 0;
            }

            if (listFKM202.Any(i => i.ParentNodeId == objFKM202.NodeId))
            {
                var childs = listFKM202.Where(i => i.ParentNodeId == objFKM202.NodeId).ToList();
                foreach (var item in childs)
                {
                    TreeviewEnt childnode = new TreeviewEnt();
                    childnode = GetNodesStructure(item, childnode);
                    treeviewEnt.children.Add(childnode);
                }
            }
            return treeviewEnt;
        }

        public bool IsKitRequested(FKM202 objFKM202)
        {
            if (objFKM202.MaterialStatus == KitStatus.Request.GetStringValue() || objFKM202.MaterialStatus == FKMSMaterialDeliveryStatus.MaterialReceived.GetStringValue())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public FKM202 SetNodesFKM202(ref List<CopyNodeDtl> listCopyNodeDtl, FKM202 dndlst, FKM202 mainfkm202, int HeaderId, int objparentid, int copy = 0, bool IsPrimaryProjectInDIN = false, bool IsSecondaryProjectInDIN = false)
        {
            if (listFKM202.Any(i => i.ParentNodeId == dndlst.NodeId))
            {
                var childs = listFKM202.Where(i => i.ParentNodeId == dndlst.NodeId).OrderBy(i => i.ParentNodeId_Init).ThenBy(x => x.NodeId).ToList();
                foreach (var item in childs)
                {
                    //if secondary project then only copy item. PLT/NPLT
                    bool Copy = true;
                    bool IsChangeNodeKey = true;
                    bool IsCopyDocument = false;

                    if (copy > 0)
                    {
                        string ASM = NodeTypes.ASM.GetStringValue().ToLower();
                        string TJF = NodeTypes.ASM.GetStringValue().ToLower();
                        if (!string.IsNullOrWhiteSpace(item.ProductType) && item.ProductType.ToLower() != ASM && item.ProductType.ToLower() != TJF)
                        {
                            if (IsPrimaryProjectInDIN)
                            {
                                Copy = false;
                            }
                            else if (IsSecondaryProjectInDIN)
                            {
                                Copy = true;
                                IsChangeNodeKey = false;
                            }
                        }

                        if (IsSecondaryProjectInDIN)
                            IsCopyDocument = true;
                    }

                    if (Copy)
                    {
                        FKM202 parentfkm202 = new FKM202();
                        parentfkm202 = InsertDND(ref listCopyNodeDtl, item, HeaderId, parentfkm202, objparentid, item.NoOfPieces, copy, IsChangeNodeKey, IsCopyDocument);

                        FKM202 mainFKM202 = new FKM202();
                        int objparentchildid = parentfkm202.NodeId;
                        mainfkm202 = SetNodesFKM202(ref listCopyNodeDtl, item, mainFKM202, HeaderId, objparentchildid, copy, IsPrimaryProjectInDIN, IsSecondaryProjectInDIN);
                    }
                }
            }
            return mainfkm202;
        }

        public FKM202 GetDeleteAllNodesStructure(FKM202 objFKM202, FKM202 deletetreeview)
        {
            deletetreeview = objFKM202;
            if (listFKM202.Any(i => i.ParentNodeId == objFKM202.NodeId))
            {
                var childNodes = listFKM202.Where(i => i.ParentNodeId == objFKM202.NodeId).ToList();
                foreach (var item in childNodes)
                {
                    FKM202 mainTree = new FKM202();
                    mainTree = GetDeleteAllNodesStructure(item, mainTree);
                    listDeleteTree.Add(mainTree);
                }
            }
            return deletetreeview;
        }
        #endregion

        #region MergeTree View
        public ActionResult GetMergeTreeViewPartial(int NodeId)
        {
            ViewBag.NodeId = NodeId;
            return PartialView("_MergeTreePartial");
        }

        public ActionResult MergeTreeNodes(int HeaderId, int? NodeId)
        {
            listMergeNodeEnt = new List<MergeNodeEnt>();
            try
            {

                listMergeNodeEnt = GetPlannerDesignerNodeList(HeaderId, NodeId);
                listTreeviewEnt = new List<TreeviewEnt>();
                var listParentNode = listMergeNodeEnt.Where(i => i.ParentNodeId == 0).ToList();
                foreach (var item in listParentNode)
                {
                    TreeviewEnt mainTreeviewEnt = new TreeviewEnt();
                    mainTreeviewEnt = GetNodesStructureForMerge(item, mainTreeviewEnt);
                    listTreeviewEnt.Add(mainTreeviewEnt);
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(ComparelistTreeviewEnt, JsonRequestBehavior.AllowGet);
            }
            return Json(listTreeviewEnt, JsonRequestBehavior.AllowGet);
        }

        public List<MergeNodeEnt> GetPlannerDesignerNodeList(int HeaderId, int? NodeId)
        {
            FKM202 objStartMergeNode = null;
            listMergeNodeEnt = new List<MergeNodeEnt>();
            int Id = 0;
            try
            {
                if (!NodeId.HasValue)
                {
                    objStartMergeNode = db.FKM202.Where(i => i.HeaderId == HeaderId).FirstOrDefault();
                }
                else
                {
                    objStartMergeNode = db.FKM202.Where(i => i.NodeId == NodeId).FirstOrDefault();
                }
                NodeId = objStartMergeNode.NodeId;

                if (NodeId.HasValue)
                {
                    #region Parents For FKM202
                    List<ParentChildNodeEnt> objParentsFKM202 = FN_GET_ALL_PARENTNODE_FOR_NODE_FKM202(objStartMergeNode.NodeId, 0).ToList();
                    objParentsFKM202.ForEach(i =>
                    {
                        ////var parent = objParentsFKM202.Where(x => x.NodeId == i.ParentNodeId).FirstOrDefault();
                        //var parent = objParentsFKM202.Where(x => x.NodeId == i.ParentNodeId_Init).FirstOrDefault();

                        listMergeNodeEnt.Add(new MergeNodeEnt
                        {
                            NodeKey = i.NodeKey,
                            NodeFindNo = i.FindNo,
                            ParentFindNo_Init = i.ParentFindNo_Init,
                            ParentFindNo = i.ParentFindNo,
                            Qty = i.NoOfPieces != "0" ? i.NoOfPieces : "0",
                            NodeName = i.NodeName,
                            IsPlanner = true,
                            NodeId = i.NodeId.HasValue ? i.NodeId.Value : 0,
                            ParentNodeId = i.ParentNodeId.HasValue ? i.ParentNodeId.Value : 0,
                            ParentNodeId_Init = i.ParentNodeId_Init.HasValue ? i.ParentNodeId_Init.Value : 0,
                            IsCompare = false,
                            ParentNodeKey_Init = i.ParentNodeKey_Init,
                            ParentNodeKey = i.ParentNodeKey,
                            Id = ++Id,
                            ProductType = i.ProductType,
                            IsDesigner = false,
                            QtyDesigner = "",
                            HeaderId = HeaderId,
                            NodeTypeId = 0,
                            Material = i.Material,
                            UOM = i.UOM,
                            Project = i.Project
                        });
                    });
                    #endregion

                    #region Childs For FKM202
                    List<ParentChildNodeEnt> objChildsFKM202 = FN_GET_ALL_CHILDNODE_FOR_NODE_FKM202(objStartMergeNode.NodeId, 1).ToList();
                    objChildsFKM202.ForEach(i =>
                    {
                        ////var parent = listMergeNodeEnt.Where(x => x.NodeId == i.ParentNodeId && x.IsPlanner == true).FirstOrDefault();
                        //var parent = listMergeNodeEnt.Where(x => x.NodeId == i.ParentNodeId_Init && x.IsPlanner == true).FirstOrDefault();
                        //if (parent == null)
                        //{
                        //    parent = (from fkm202 in db.FKM202
                        //              where fkm202.NodeId == i.ParentNodeId_Init
                        //              select new MergeNodeEnt { NodeFindNo = fkm202.FindNo_Init, NodeKey = fkm202.NodeKey_Init, }
                        //            ).FirstOrDefault();
                        //}
                        listMergeNodeEnt.Add(new MergeNodeEnt
                        {
                            NodeKey = i.NodeKey,
                            NodeFindNo = i.FindNo,
                            //ParentFindNo_Init = parent != null ? parent.NodeFindNo : "",
                            ParentFindNo_Init = i.ParentFindNo_Init,
                            ParentFindNo = i.ParentFindNo,
                            Qty = i.NoOfPieces != "0" ? i.NoOfPieces : "0",
                            NodeName = i.NodeName,
                            IsPlanner = true,
                            NodeId = i.NodeId.HasValue ? i.NodeId.Value : 0,
                            ParentNodeId = i.ParentNodeId.HasValue ? i.ParentNodeId.Value : 0,
                            ParentNodeId_Init = i.ParentNodeId_Init.HasValue ? i.ParentNodeId_Init.Value : 0,
                            IsCompare = true,
                            //ParentNodeKey_Init = parent != null ? parent.NodeKey : "",
                            ParentNodeKey_Init = i.ParentNodeKey_Init,
                            ParentNodeKey = i.ParentNodeKey,
                            Id = ++Id,
                            ProductType = i.ProductType,
                            IsDesigner = false,
                            QtyDesigner = "",
                            HeaderId = HeaderId,
                            NodeTypeId = 0,
                            Material = i.Material,
                            UOM = i.UOM,
                            Project = i.Project
                        });
                    });
                    #endregion

                    var IsExist = false;
                    var objFKM104 = db.FKM104.Where(i => i.FindNo.ToLower() == objStartMergeNode.FindNo.ToLower() && i.HeaderId == objStartMergeNode.HeaderId && i.NodeKey.ToLower() == objStartMergeNode.NodeKey.ToLower()).ToList();
                    foreach (var fkm104 in objFKM104)
                    {
                        #region Parents For FKM104
                        List<ParentChildNodeEnt> objParentsFKM104 = FN_GET_ALL_PARENTNODE_FOR_NODE_FKM104(fkm104.NodeId, 0).ToList();
                        objParentsFKM104.ForEach(i =>
                        {
                            IsExist = false;
                            var parent = objParentsFKM104.Where(x => x.NodeId == i.ParentNodeId).FirstOrDefault();
                            if (parent != null)
                            {
                                if (listMergeNodeEnt.Any(x => x.NodeFindNo.ToLower() == i.FindNo.ToLower() && x.ParentFindNo_Init.ToLower() == parent.FindNo.ToLower() && x.NodeKey.ToLower() == i.NodeKey.ToLower()))
                                {
                                    IsExist = true;
                                    listMergeNodeEnt.ForEach(x =>
                                    {
                                        if (x.NodeFindNo.ToLower() == i.FindNo.ToLower() && x.ParentFindNo_Init.ToLower() == parent.FindNo.ToLower() && x.NodeKey.ToLower() == i.NodeKey.ToLower())
                                        {
                                            x.IsDesigner = true;
                                            x.QtyDesigner = i.NoOfPieces != "0" ? i.NoOfPieces : "0";
                                            x.DesignerNodeId = i.NodeId.HasValue ? i.NodeId.Value : 0;
                                            x.DesignerParentNodeId = i.ParentNodeId.HasValue ? i.ParentNodeId.Value : 0;
                                        }
                                    });
                                }
                            }
                            else if (parent == null && i.ParentNodeId == 0)
                            {
                                if (listMergeNodeEnt.Any(x => x.NodeFindNo.ToLower() == i.FindNo.ToLower() && x.ParentNodeId == i.ParentNodeId))
                                {
                                    IsExist = true;
                                    listMergeNodeEnt.ForEach(x =>
                                    {
                                        if (x.NodeFindNo.ToLower() == i.FindNo.ToLower() && x.ParentNodeId == i.ParentNodeId)
                                        {
                                            x.IsDesigner = true;
                                            x.QtyDesigner = i.NoOfPieces != "0" ? i.NoOfPieces : "0";
                                            x.DesignerNodeId = i.NodeId.HasValue ? i.NodeId.Value : 0;
                                            x.DesignerParentNodeId = i.ParentNodeId.HasValue ? i.ParentNodeId.Value : 0;
                                        }
                                    });
                                }
                            }
                            if (!IsExist)
                            {
                                listMergeNodeEnt.Add(new MergeNodeEnt
                                {
                                    NodeKey = i.NodeKey,
                                    NodeFindNo = i.FindNo,
                                    ParentFindNo_Init = parent != null ? parent.FindNo : "",
                                    ParentFindNo = parent != null ? parent.FindNo : "",
                                    Qty = i.NoOfPieces != "0" ? i.NoOfPieces : "0",
                                    NodeName = i.NodeName,
                                    IsPlanner = false,
                                    NodeId = i.NodeId.HasValue ? i.NodeId.Value : 0,
                                    ParentNodeId = i.ParentNodeId.HasValue ? i.ParentNodeId.Value : 0,
                                    IsCompare = false,
                                    ParentNodeKey_Init = parent != null ? parent.NodeKey : "",
                                    ParentNodeKey = parent != null ? parent.NodeKey : "",
                                    Id = ++Id,
                                    ProductType = i.ProductType,
                                    IsDesigner = true,
                                    QtyDesigner = i.NoOfPieces != "0" ? i.NoOfPieces : "0",
                                    HeaderId = HeaderId,
                                    NodeTypeId = 0,
                                    Material = i.Material,
                                    UOM = i.UOM,
                                    DesignerNodeId = i.NodeId.HasValue ? i.NodeId.Value : 0,
                                    DesignerParentNodeId = i.ParentNodeId.HasValue ? i.ParentNodeId.Value : 0,
                                    Project = i.Project,
                                });
                            }
                        });
                        #endregion

                        #region Childs For FKM104
                        List<ParentChildNodeEnt> objChildsFKM104 = FN_GET_ALL_CHILDNODE_FOR_NODE_FKM104(fkm104.NodeId, 1).ToList();
                        objChildsFKM104.ForEach(i =>
                        {
                            IsExist = false;
                            if (i.NodeId == fkm104.NodeId)
                            {
                                var parent = objParentsFKM104.Where(x => x.NodeId == i.ParentNodeId).FirstOrDefault();
                                if (parent != null)
                                {
                                    if (listMergeNodeEnt.Any(x => x.NodeFindNo.ToLower() == i.FindNo.ToLower() && x.ParentFindNo_Init.ToLower() == parent.FindNo.ToLower()))
                                    {
                                        IsExist = true;
                                        listMergeNodeEnt.ForEach(x =>
                                        {
                                            if (x.NodeFindNo.ToLower() == i.FindNo.ToLower() && x.ParentFindNo_Init.ToLower() == parent.FindNo.ToLower())
                                            {
                                                x.IsDesigner = true;
                                                x.QtyDesigner = i.NoOfPieces != "0" ? i.NoOfPieces : "0";
                                                x.DesignerNodeId = i.NodeId.HasValue ? i.NodeId.Value : 0;
                                                x.DesignerParentNodeId = i.ParentNodeId.HasValue ? i.ParentNodeId.Value : 0;
                                            }
                                        });
                                    }
                                }
                                else if (parent == null && i.ParentNodeId == 0)
                                {
                                    if (listMergeNodeEnt.Any(x => x.NodeFindNo.ToLower() == i.FindNo.ToLower() && x.ParentNodeId == i.ParentNodeId))
                                    {
                                        IsExist = true;
                                        listMergeNodeEnt.ForEach(x =>
                                        {
                                            if (x.NodeFindNo.ToLower() == i.FindNo.ToLower() && x.ParentNodeId == i.ParentNodeId)
                                            {
                                                x.IsDesigner = true;
                                                x.QtyDesigner = i.NoOfPieces != "0" ? i.NoOfPieces : "0";
                                                x.DesignerNodeId = i.NodeId.HasValue ? i.NodeId.Value : 0;
                                                x.DesignerParentNodeId = i.ParentNodeId.HasValue ? i.ParentNodeId.Value : 0;
                                            }
                                        });
                                    }
                                }
                                if (!IsExist)
                                {
                                    listMergeNodeEnt.Add(new MergeNodeEnt
                                    {
                                        NodeKey = i.NodeKey,
                                        NodeFindNo = i.FindNo,
                                        ParentFindNo_Init = parent != null ? parent.FindNo : "",
                                        ParentFindNo = parent != null ? parent.FindNo : "",
                                        Qty = i.NoOfPieces != "0" ? i.NoOfPieces : "0",
                                        NodeName = i.NodeName,
                                        IsPlanner = false,
                                        NodeId = i.NodeId.HasValue ? i.NodeId.Value : 0,
                                        ParentNodeId = i.ParentNodeId.HasValue ? i.ParentNodeId.Value : 0,
                                        ParentNodeId_Init = i.ParentNodeId.HasValue ? i.ParentNodeId.Value : 0,
                                        IsCompare = true,
                                        ParentNodeKey_Init = parent != null ? parent.NodeKey : "",
                                        ParentNodeKey = parent != null ? parent.NodeKey : "",
                                        Id = ++Id,
                                        ProductType = i.ProductType,
                                        IsDesigner = true,
                                        QtyDesigner = i.NoOfPieces != "0" ? i.NoOfPieces : "0",
                                        HeaderId = HeaderId,
                                        NodeTypeId = 0,
                                        Material = i.Material,
                                        UOM = i.UOM,
                                        DesignerNodeId = i.NodeId.HasValue ? i.NodeId.Value : 0,
                                        DesignerParentNodeId = i.ParentNodeId.HasValue ? i.ParentNodeId.Value : 0,
                                        Project = i.Project,
                                    });
                                }
                            }
                            else
                            {
                                var parent = objChildsFKM104.Where(x => x.NodeId == i.ParentNodeId).First();
                                if (parent != null)
                                {
                                    if (listMergeNodeEnt.Any(x => x.NodeFindNo.ToLower() == i.FindNo.ToLower() && x.ParentFindNo_Init.ToLower() == parent.FindNo.ToLower() && x.NodeKey.ToLower() == i.NodeKey.ToLower() && x.ParentNodeKey_Init.ToLower() == i.ParentNodeKey.ToLower()))
                                    {
                                        IsExist = true;
                                        listMergeNodeEnt.ForEach(x =>
                                        {
                                            if (x.NodeFindNo.ToLower() == i.FindNo.ToLower() && x.ParentFindNo_Init.ToLower() == parent.FindNo.ToLower() && x.NodeKey.ToLower() == i.NodeKey.ToLower() && x.ParentNodeKey_Init.ToLower() == i.ParentNodeKey.ToLower())
                                            {
                                                x.IsDesigner = true;
                                                x.QtyDesigner = i.NoOfPieces != "0" ? i.NoOfPieces : "0";
                                                x.DesignerNodeId = i.NodeId.HasValue ? i.NodeId.Value : 0;
                                                x.DesignerParentNodeId = i.ParentNodeId.HasValue ? i.ParentNodeId.Value : 0;
                                            }
                                        });
                                    }
                                }
                                if (!IsExist)
                                {
                                    listMergeNodeEnt.Add(new MergeNodeEnt
                                    {
                                        NodeKey = i.NodeKey,
                                        NodeFindNo = i.FindNo,
                                        ParentFindNo_Init = parent != null ? parent.FindNo : "",
                                        ParentFindNo = parent != null ? parent.FindNo : "",
                                        Qty = i.NoOfPieces != "0" ? i.NoOfPieces : "0",
                                        NodeName = i.NodeName,
                                        IsPlanner = false,
                                        NodeId = i.NodeId.HasValue ? i.NodeId.Value : 0,
                                        ParentNodeId = i.ParentNodeId.HasValue ? i.ParentNodeId.Value : 0,
                                        ParentNodeId_Init = i.ParentNodeId.HasValue ? i.ParentNodeId.Value : 0,
                                        IsCompare = true,
                                        ParentNodeKey_Init = parent != null ? parent.NodeKey : "",
                                        ParentNodeKey = parent != null ? parent.NodeKey : "",
                                        Id = ++Id,
                                        ProductType = i.ProductType,
                                        IsDesigner = true,
                                        QtyDesigner = i.NoOfPieces != "0" ? i.NoOfPieces : "0",
                                        HeaderId = HeaderId,
                                        NodeTypeId = 0,
                                        Material = i.Material,
                                        UOM = i.UOM,
                                        DesignerNodeId = i.NodeId.HasValue ? i.NodeId.Value : 0,
                                        DesignerParentNodeId = i.ParentNodeId.HasValue ? i.ParentNodeId.Value : 0,
                                        Project = i.Project,
                                    });
                                }
                            }
                        });
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return listMergeNodeEnt;
        }

        public TreeviewEnt GetNodesStructureForMerge(MergeNodeEnt objFKM202, TreeviewEnt treeviewEnt, FKMSNodeGenerateFor NodeFor = FKMSNodeGenerateFor.MergePlannerAndDesigner)
        {
            string difference = string.Empty;
            string Changeiconcolor = "fa fa-folder icon-state-success icon-lg";
            string fadanger = " fa_danger";
            string uom = objFKM202.UOM != "" ? objFKM202.UOM : "-";
            treeviewEnt.text = objFKM202.NodeName;
            treeviewEnt.id = objFKM202.NodeId.ToString() + "#" + objFKM202.HeaderId.ToString() + "#" + objFKM202.NodeTypeId.ToString() + "#" + objFKM202.ParentNodeId.ToString();
            treeviewEnt.state.opened = true;
            treeviewEnt.a_attr.title = "Part Key : " + objFKM202.NodeKey + "\n" + "Find No : " + objFKM202.NodeFindNo + "\n" + "Parent Find No : " + objFKM202.ParentFindNo_Init + "\n" + "Parent PartKey : " + objFKM202.ParentNodeKey_Init + "\n" + "Material : " + objFKM202.Material;

            if (objFKM202.ProductType != null)
            {
                if (objFKM202.ProductType.ToLower() != clsImplementationEnum.NodeTypes.ASM.GetStringValue().ToLower() && objFKM202.ProductType.ToLower() != clsImplementationEnum.NodeTypes.TJF.GetStringValue().ToLower())
                {
                    treeviewEnt.icon = PartIcon;
                }
                else
                {
                    treeviewEnt.icon = Changeiconcolor;
                }
            }
            else
            {
                treeviewEnt.icon = Changeiconcolor;
            }
            if (NodeFor == FKMSNodeGenerateFor.Planner)
            {
                //treeviewEnt.text += " (QTY :" + objFKM202.Qty + "," + uom + ")";
                treeviewEnt.a_attr.title += "\n" + "Quantity : " + objFKM202.Qty;
            }
            else if (NodeFor == FKMSNodeGenerateFor.Designer)
            {
                //treeviewEnt.text += " (QTY :" + objFKM202.QtyDesigner + "," + uom + ")";
                treeviewEnt.a_attr.title += "\n" + "Quantity : " + objFKM202.QtyDesigner;
            }
            if (objFKM202.IsCompare)
            {
                if (objFKM202.IsPlanner)
                {
                    //if (!listMergeNodeEnt.Any(i => i.ParentFindNo == objFKM202.NodeFindNo && i.ParentNodeKey == objFKM202.NodeKey && i.IsDesigner == true))
                    if (!objFKM202.IsDesigner)
                    {
                        var isExistInDesignerAnyWhere = false;
                        /*
                        If Node Generate for Merge and node is move to other parent node check node exist any where in planner tree with Init parent node relation.
                        If any records found then not consider as New Added Node.
                        */
                        if (NodeFor == FKMSNodeGenerateFor.MergePlannerAndDesigner)
                        {
                            var allnodes = db.FKM104.Where(i => i.FindNo == objFKM202.NodeFindNo && i.NodeKey == objFKM202.NodeKey && i.Project == objFKM202.Project).ToList();
                            allnodes.ForEach(i =>
                            {
                                var parentdtl = db.FKM104.Where(x => x.NodeId == i.ParentNodeId).ToList();
                                if (parentdtl.Any(x => x.FindNo == objFKM202.ParentFindNo_Init && x.NodeKey == objFKM202.ParentNodeKey_Init))
                                {
                                    isExistInDesignerAnyWhere = true;
                                }
                            });
                        }

                        if (NodeFor == FKMSNodeGenerateFor.MergePlannerAndDesigner)
                        {
                            //If Record not exist then consider as new added node.
                            if (!isExistInDesignerAnyWhere)
                            {
                                treeviewEnt.a_attr.style = "background:#a3fcff;font-weight:bold;";
                                difference += "New Part/Assembly Added.";
                            }
                        }
                        else
                        {
                            difference += "New Part/Assembly Added.";
                        }
                    }
                    else
                    {
                        if (objFKM202.Qty != objFKM202.QtyDesigner)
                        {
                            treeviewEnt.icon += (NodeFor == FKMSNodeGenerateFor.MergePlannerAndDesigner ? " qtydiff" : fadanger);
                            difference += (difference.Length > 0 ? " " : "") + "No Of Qty(Original Qty : " + objFKM202.Qty + ", Designer Qty : " + objFKM202.QtyDesigner + ").";
                        }

                        if (objFKM202.IsDesigner && (NodeFor == FKMSNodeGenerateFor.Planner || NodeFor == FKMSNodeGenerateFor.Designer))
                        {
                            var NoOfChildPlanner = listMergeNodeEnt.Where(i => i.ParentFindNo_Init == objFKM202.NodeFindNo && i.ParentNodeKey_Init == objFKM202.NodeKey && i.IsPlanner == true).Count();
                            var NoOfChildDesigner = listMergeNodeEnt.Where(i => i.ParentFindNo_Init == objFKM202.NodeFindNo && i.ParentNodeKey_Init == objFKM202.NodeKey && i.IsDesigner == true).Count();
                            if (NoOfChildPlanner != NoOfChildDesigner)
                            {
                                treeviewEnt.icon += fadanger;
                                difference += (difference.Length > 0 ? " " : "") + "No Of Parts/Assembly.";
                            }
                        }
                        if (difference.Length > 0)
                        {
                            treeviewEnt.a_attr.title += "\n" + "Diffrence : " + difference;
                        }
                    }
                }
                else if (objFKM202.IsDesigner)
                {
                    //if (!listMergeNodeEnt.Any(i => i.ParentFindNo == objFKM202.NodeFindNo && i.ParentNodeKey == objFKM202.NodeKey && i.IsPlanner == true))
                    if (!objFKM202.IsPlanner)
                    {
                        var isExistInPlannerAnyWhere = false;
                        /*
                        If Node Generate for Merge and node is move to other parent node check node exist any where in planner tree with Init parent node relation.
                        If any records found then not consider as New Added Node.
                        */
                        if (NodeFor == FKMSNodeGenerateFor.MergePlannerAndDesigner)
                        {
                            var allnodes = db.FKM202.Where(i => i.FindNo_Init == objFKM202.NodeFindNo && i.NodeKey_Init == objFKM202.NodeKey && i.Project == objFKM202.Project).ToList();
                            allnodes.ForEach(i =>
                            {
                                var parentdtl = db.FKM202.Where(x => x.NodeId == i.ParentNodeId_Init).ToList();
                                if (parentdtl.Any(x => x.FindNo_Init == objFKM202.ParentFindNo_Init && x.NodeKey_Init == objFKM202.ParentNodeKey_Init))
                                {
                                    isExistInPlannerAnyWhere = true;
                                }
                            });
                        }
                        if (NodeFor == FKMSNodeGenerateFor.Designer)
                        {
                            treeviewEnt.icon += fadanger;
                        }
                        if (NodeFor == FKMSNodeGenerateFor.MergePlannerAndDesigner)
                        {
                            //If Record not exist then consider as new added node.
                            if (!isExistInPlannerAnyWhere)
                            {
                                treeviewEnt.a_attr.style = "background:#d2ffd2;font-weight:bold;";
                                difference += "New Part/Assembly Added.";
                            }
                        }
                        else
                        {
                            difference += "New Part/Assembly Added.";
                        }
                    }
                    else
                    {
                        if (objFKM202.Qty != objFKM202.QtyDesigner)
                        {
                            treeviewEnt.icon += (NodeFor == FKMSNodeGenerateFor.MergePlannerAndDesigner ? " qtydiff" : fadanger);
                            difference += (difference.Length > 0 ? " " : "") + "No Of Qty(Original Qty : " + objFKM202.Qty + ", Designer Qty : " + objFKM202.QtyDesigner + ").";
                        }

                        if (objFKM202.IsPlanner && (NodeFor == FKMSNodeGenerateFor.Planner || NodeFor == FKMSNodeGenerateFor.Designer))
                        {
                            var NoOfChildPlanner = listMergeNodeEnt.Where(i => i.ParentFindNo_Init == objFKM202.NodeFindNo && i.ParentNodeKey_Init == objFKM202.NodeKey && i.IsPlanner == true).Count();
                            var NoOfChildDesigner = listMergeNodeEnt.Where(i => i.ParentFindNo_Init == objFKM202.NodeFindNo && i.ParentNodeKey_Init == objFKM202.NodeKey && i.IsDesigner == true).Count();
                            if (NoOfChildPlanner != NoOfChildDesigner)
                            {
                                treeviewEnt.icon += fadanger;
                                difference += (difference.Length > 0 ? " " : "") + "No Of Parts/Assembly.";
                            }
                        }
                    }
                    if (difference.Length > 0)
                    {
                        treeviewEnt.a_attr.title += "\n" + "Diffrence : " + difference;
                    }
                }
            }
            if (NodeFor == FKMSNodeGenerateFor.MergePlannerAndDesigner)
            {
                if (listMergeNodeEnt.Any(i => i.ParentFindNo == objFKM202.NodeFindNo && i.ParentNodeKey == objFKM202.NodeKey && i.Id != objFKM202.Id))
                {
                    //var childs = listMergeNodeEnt.Where(i => i.ParentFindNo == objFKM202.NodeFindNo && i.NodeFindNo != objFKM202.NodeFindNo).ToList();
                    var childs = listMergeNodeEnt.Where(i => i.ParentFindNo == objFKM202.NodeFindNo && i.ParentNodeKey == objFKM202.NodeKey && i.Id != objFKM202.Id).ToList();
                    foreach (var item in childs)
                    {
                        TreeviewEnt childnode = new TreeviewEnt();
                        childnode = GetNodesStructureForMerge(item, childnode);
                        treeviewEnt.children.Add(childnode);
                    }
                }
            }
            else if (NodeFor == FKMSNodeGenerateFor.Planner)
            {
                if (listMergeNodeEnt.Any(i => i.ParentNodeId == objFKM202.NodeId && i.IsPlanner == true))
                {
                    //var childs = listMergeNodeEnt.Where(i => i.ParentFindNo == objFKM202.NodeFindNo && i.NodeFindNo != objFKM202.NodeFindNo).ToList();
                    var childs = listMergeNodeEnt.Where(i => i.ParentNodeId == objFKM202.NodeId && i.IsPlanner == true).ToList();
                    foreach (var item in childs)
                    {
                        TreeviewEnt childnode = new TreeviewEnt();
                        childnode = GetNodesStructureForMerge(item, childnode, NodeFor);
                        treeviewEnt.children.Add(childnode);
                    }
                }
            }
            else if (NodeFor == FKMSNodeGenerateFor.Designer)
            {
                //if (listMergeNodeEnt.Any(i => i.DesignerParentNodeId == objFKM202.DesignerNodeId && i.IsDesigner == true))
                if (listMergeNodeEnt.Any(i => i.DesignerParentNodeId == objFKM202.DesignerNodeId))
                {
                    //var childs = listMergeNodeEnt.Where(i => i.ParentFindNo == objFKM202.NodeFindNo && i.NodeFindNo != objFKM202.NodeFindNo).ToList();
                    //var childs = listMergeNodeEnt.Where(i => i.DesignerParentNodeId == objFKM202.DesignerNodeId && i.IsDesigner == true).ToList();
                    var childs = listMergeNodeEnt.Where(i => i.DesignerParentNodeId == objFKM202.DesignerNodeId).ToList();
                    foreach (var item in childs)
                    {
                        TreeviewEnt childnode = new TreeviewEnt();
                        childnode = GetNodesStructureForMerge(item, childnode, NodeFor);
                        treeviewEnt.children.Add(childnode);
                    }
                }
            }
            return treeviewEnt;
        }

        public List<copyProject> FN_GET_ALL_COPYPROJECTS_FOR_NODE_FKM202()
        {
            return db.Database.SqlQuery<copyProject>("SELECT * from dbo.FN_GET_ALL_COPYPROJECTS_FOR_NODE_FKM202()").ToList();
        }
        public List<Projects> FN_GET_PROJECTS_RESULT(string TaskManager)
        {
            return db.Database.SqlQuery<Projects>("SELECT * from dbo.FN_GET_PROJECTS_RESULT('" + TaskManager + "')").ToList();
        }
        public List<ParentList> FN_GETXMLCOLUMNParent(int NodeId)
        {
            return db.Database.SqlQuery<ParentList>("SELECT * from dbo.FN_GETXMLCOLUMNParent(" + NodeId + ")").ToList();
        }
        public List<InitParentList> FN_GETXMLCOLUMNInitParent(int NodeId)
        {
            return db.Database.SqlQuery<InitParentList>("SELECT * from dbo.FN_GETXMLCOLUMNInitParent(" + NodeId + ")").ToList();
        }
        public List<PageParentList> FN_GETXMLCOLUMNPageParent(int NodeId)
        {
            return db.Database.SqlQuery<PageParentList>("SELECT * from dbo.FN_GETXMLCOLUMNPageParent(" + NodeId + ")").ToList();
        }
        public List<ChildList> FN_GETXMLCOLUMNChild(int NodeId)
        {
            return db.Database.SqlQuery<ChildList>("SELECT * from dbo.FN_GETXMLCOLUMNChild(" + NodeId + ")").ToList();
        }
        //public List<ChildList> FN_GETXMLCOLUMNChild(int NodeId)
        //{
        //    return db.Database.SqlQuery<ChildList>("SELECT a.*,b.AssQty from dbo.FN_GETXMLCOLUMNChild(" + NodeId + ") a join dbo.FN_GETXMLCOLUMNParent("+ NodeId + ") b on a.ParentName = b.AssName").ToList();
        //}
        public List<ParentChildNodeEnt> FN_GET_ALL_PARENTNODE_FOR_NODE_FKM202(int NodeId, int IncludeSelf)
        {
            return db.Database.SqlQuery<ParentChildNodeEnt>("SELECT * from dbo.FN_GET_ALL_PARENTNODE_FOR_NODE_FKM202(" + NodeId + "," + IncludeSelf + ")").ToList();
        }
        public List<ParentChildNodeEnt> FN_GET_ALL_CHILDNODE_FOR_NODE_FKM202(int NodeId, int IncludeSelf)
        {
            return db.Database.SqlQuery<ParentChildNodeEnt>("SELECT * from dbo.FN_GET_ALL_CHILDNODE_FOR_NODE_FKM202(" + NodeId + "," + IncludeSelf + ")").ToList();
        }
        public List<ParentChildNodeEnt> FN_GET_ALL_PARENTNODE_FOR_NODE_FKM104(int NodeId, int IncludeSelf)
        {
            return db.Database.SqlQuery<ParentChildNodeEnt>("SELECT * from dbo.FN_GET_ALL_PARENTNODE_FOR_NODE_FKM104(" + NodeId + "," + IncludeSelf + ")").ToList();
        }
        public List<ParentChildNodeEnt> FN_GET_ALL_CHILDNODE_FOR_NODE_FKM104(int NodeId, int IncludeSelf)
        {
            return db.Database.SqlQuery<ParentChildNodeEnt>("SELECT * from dbo.FN_GET_ALL_CHILDNODE_FOR_NODE_FKM104(" + NodeId + "," + IncludeSelf + ")").ToList();
        }
        public string FN_GET_PARENTDEPT(string DeptCode)
        {
            return db.Database.SqlQuery<string>("SELECT dbo.FN_GET_PARENTDEPT('" + DeptCode + "')").FirstOrDefault();
        }

        #endregion

        public ActionResult LoadNodeNotLinkInPLM(JQueryDataTableParamModel param, int HeaderId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Request["sSortDir_0"];

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string status = param.CTQCompileStatus;

                string whereCondition = " HeaderId=" + HeaderId + " and IsInsertedInPLM is not null and IsInsertedInPLM=0";

                string[] columnName = { "NodeName", "Description", "PLMError", "NodeKey" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstPam = db.SP_FKM_GET_NODE_NOT_INSERT_IN_PLM(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from h in lstPam
                           select new[] {
                               Convert.ToString(h.NodeId),
                               Convert.ToString(h.NodeKey),
                               Convert.ToString(h.NodeName),
                               Convert.ToString(h.PLMError)
                    }).ToList();
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetNodeNotExistInPLMGridDataPartial()
        {
            return PartialView("_GetNodeNotExistInPLMGridDataPartial");
        }

        public ActionResult ResendPartsForSaveInPLM(int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                context = new IEMQSEntitiesContext();
                var objFKM201 = db.FKM201.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                var listNotLinkInPLN = db.FKM202.Where(i => i.HeaderId == HeaderId && i.IsInsertedInPLM.HasValue && i.IsInsertedInPLM.Value == false).ToList();
                foreach (var part in listNotLinkInPLN)
                {
                    var bomEnt = new FKMSBOMEnt();
                    InsertPartAndBOMInPLM(bomEnt, HeaderId, objFKM201.Status, part);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public int GetFindNo(int FindNo)
        {
            var FindNoMinusStartNo = FindNo - StartFindNo;
            if (FindNoMinusStartNo == 99 && Decrement100Cnt == 0)
            {
                Decrement100Cnt = Decrement100Cnt + 1;
                FindNo = StartFindNo - 100;
                StartFindNo = StartFindNo - 100;
            }
            else if (FindNoMinusStartNo == 100 && Decrement100Cnt > 0)
            {
                Decrement100Cnt = Decrement100Cnt + 1;
                FindNo = StartFindNo - 100;
                StartFindNo = StartFindNo - 100;
            }
            //else
            //{
            //    FindNo = FindNo + 1;
            //}

            return FindNo;
        }

        public ActionResult CheckProjectAllocationInProgress(string sourceProject = "", int ParentNodeId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                objResponseMsg.Key = true;
                if (sourceProject != "")
                {
                    if (db.FKM110.Any(i => i.Project == sourceProject))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Allocation already is in progress. Please Try Again After Sometime.";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FKMSAllocation(string sourceProject = "", int ParentNodeId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                FKMSAllocationExecute(sourceProject, ParentNodeId);

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Allocation process is started. it will take time.";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public void FKMSAllocationExecute(string sourceProject = "", int ParentNodeId = 0)
        {
            var psno = objClsLoginInfo != null && !string.IsNullOrEmpty(objClsLoginInfo.UserName) ? objClsLoginInfo.UserName : "";
            var location = objClsLoginInfo != null && !string.IsNullOrEmpty(objClsLoginInfo.Location) ? objClsLoginInfo.Location : "";

            Task<int> task1 = LongRunningNPLTAllocationAsync(sourceProject, ParentNodeId, psno, location);
            Task<int> task2 = LongRunningPLTAllocationAsync(sourceProject, ParentNodeId, psno);
        }

        #region NPLT Allocation
        public async Task<int> LongRunningNPLTAllocationAsync(string sourceProject, int ParentNodeId, string psno, string location)
        {
            await Task.Run(() => NPLTAllocationAsync(sourceProject, ParentNodeId, psno, location));
            return 1;
        }

        public void NPLTAllocationAsync(string sourceProject, int ParentNodeId, string psno, string location)
        {
            IEMQSEntitiesContext db1 = new IEMQSEntitiesContext();
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            FKSOBService serviceObj = new FKSOBService();
            string result = string.Empty;
            string project = string.Empty;
            string fullKitNo = string.Empty;
            string ASM = clsImplementationEnum.NodeTypes.ASM.GetStringValue().ToLower();
            string TJF = clsImplementationEnum.NodeTypes.TJF.GetStringValue().ToLower();
            string PLT = clsImplementationEnum.NodeTypes.PLT.GetStringValue().ToLower();
            string NPLT = clsImplementationEnum.AllocateNodeTypes.NPLT.ToString();
            string PROJ_NPLT = clsImplementationEnum.AllocateNodeTypes.PROJ_NPLT.ToString();

            try
            {
                AllocateProjectInFKMS(sourceProject, PROJ_NPLT, allocateInsert, db1);

                //all fkm202 data
                var objFKM202List = db1.FKM202.ToList();

                var objFKM202FilteredList = (from u in db1.FKM202
                                             where u.ProductType.ToLower().ToString() != ASM && u.ProductType.ToLower().ToString() != TJF
                                                    && u.ProductType.ToLower().ToString() != PLT && !string.IsNullOrEmpty(u.ProductType)
                                             select u).ToList();

                var objFKM201PlannerDtl = (from fkm1 in db1.FKM201
                                           join c3 in db1.COM003 on fkm1.PlannerPsno equals c3.t_psno
                                           select new { Project = fkm1.Project, PlannerPsno = fkm1.PlannerPsno, Location = c3.t_loca }
                                          ).ToList();

                if (sourceProject != string.Empty)
                {
                    objFKM202FilteredList = (from u in objFKM202FilteredList where u.Project.Trim() == sourceProject.Trim() select u).ToList();
                }

                if (ParentNodeId != 0)
                {
                    objFKM202FilteredList = (from u in objFKM202FilteredList where u.ParentNodeId == ParentNodeId select u).ToList();
                }

                //get all distinct kit list
                var objFKM202KitList = (from u in objFKM202FilteredList
                                        where !string.IsNullOrEmpty(u.ProductType.ToString())
                                        orderby u.Project, u.ParentNodeId
                                        select new { u.Project, u.ParentNodeId }).Distinct().ToList();

                foreach (var objFKM202Kit in objFKM202KitList)
                {
                    try
                    {
                        var PlannerPsno = psno;
                        var PlannerLocation = location;
                        if (PlannerPsno == string.Empty || PlannerLocation == string.Empty)
                        {
                            var objPlannerDtl = objFKM201PlannerDtl.Where(i => i.Project == objFKM202Kit.Project).FirstOrDefault();
                            if (objPlannerDtl != null)
                            {
                                PlannerPsno = objPlannerDtl.PlannerPsno;
                                PlannerLocation = objPlannerDtl.Location;
                            }
                        }

                        var fkm202Kit = (from u in objFKM202List
                                         where u.NodeId == objFKM202Kit.ParentNodeId && !string.IsNullOrEmpty(u.DeleverTo)
                                         select u).FirstOrDefault();

                        if (fkm202Kit != null)
                        {
                            //find all component for each kit and allocate to LN.
                            var objFKM202CompList = (from u in objFKM202FilteredList
                                                     where u.ParentNodeId == objFKM202Kit.ParentNodeId && u.Project == objFKM202Kit.Project
                                                     select u).ToList();

                            if (objFKM202CompList != null && objFKM202CompList.Count > 0)
                            {
                                #region KIT DETAILS

                                project = fkm202Kit.Project;
                                fullKitNo = fkm202Kit.NodeKey + "-" + fkm202Kit.NodeId; //NodeKey + NodeId  
                                var ParentDeptCode = FN_GET_PARENTDEPT(fkm202Kit.DeleverTo);
                                string workCenter = ParentDeptCode != null ? ParentDeptCode : string.Empty;
                                //string workCenter = fkm202Kit.DeleverTo != null ? fkm202Kit.DeleverTo : string.Empty;
                                decimal kitQty = fkm202Kit.NoOfPieces != null ? Convert.ToDecimal(fkm202Kit.NoOfPieces) : 0;
                                string element = "";

                                #endregion

                                #region INSERT RECORD INTO FKM110

                                AllocateNodeInFKMS(project, fullKitNo, NPLT, allocateInsert, db1);

                                #endregion
                                try
                                {
                                    #region NPLT/COMPONENT LOOP

                                    foreach (var objFKM202Comp in objFKM202CompList)
                                    {
                                        string findno = objFKM202Comp.FindNo;
                                        element = !string.IsNullOrEmpty(objFKM202Comp.Element) ? objFKM202Comp.Element : "";
                                        int nodeid = objFKM202Comp.NodeId;
                                        string nodeKey = objFKM202Comp.NodeKey != null ? objFKM202Comp.NodeKey : "";
                                        decimal requiredQty = objFKM202Comp.NoOfPieces != null ? Convert.ToDecimal(objFKM202Comp.NoOfPieces) : 0;

                                        requiredQty = requiredQty * kitQty;

                                        #region CHECK ALLOCATION DONE ALREADY IF YES THEN UPDATE QTY ACCORDINGLY

                                        bool IsDeallocationSuccess = true;
                                        string ErrorMessage = string.Empty;
                                        if (db1.FKM109.Any(u => u.NodeId == nodeid))
                                        {
                                            string strTotalAllocatedQty = db1.FKM109.Where(u => u.NodeId == nodeid).Sum(u => u.AllocatedQty).ToString();
                                            decimal totalAllocatedQty = strTotalAllocatedQty != "" ? Convert.ToDecimal(strTotalAllocatedQty) : 0;
                                            if (requiredQty < totalAllocatedQty)//2<5
                                            {
                                                #region DEALLOCATE LOGIC HERE

                                                //deallocate totalAllocatedQty one by one from warehouse
                                                var allocatedFKM109List = db1.FKM109.Where(u => u.NodeId == nodeid).ToList();
                                                if (allocatedFKM109List != null)
                                                {
                                                    foreach (var objAllocatedFKM109 in allocatedFKM109List)
                                                    {
                                                        if (objAllocatedFKM109.AllocatedQty > 0)
                                                        {
                                                            IsDeallocationSuccess = NPLTDeallocation(objAllocatedFKM109, PlannerPsno.Trim(), PlannerLocation.Trim(), db1, ref ErrorMessage);
                                                            if (IsDeallocationSuccess)
                                                            {
                                                                if (db1.FKM109.Any(x => x.Id == objAllocatedFKM109.Id))
                                                                {
                                                                    db1.FKM109.Remove(objAllocatedFKM109);
                                                                    db1.SaveChanges();
                                                                }
                                                            }
                                                            else
                                                            {
                                                                break;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            db1.FKM109.Remove(objAllocatedFKM109);
                                                            db1.SaveChanges();
                                                        }
                                                    }
                                                }

                                                #endregion
                                            }
                                            else if (requiredQty > totalAllocatedQty)//5>2
                                            {
                                                requiredQty = requiredQty - totalAllocatedQty;
                                            }
                                            else
                                            {
                                                requiredQty = 0;
                                            }
                                        }

                                        #endregion

                                        #region ALLOCATION PROCESS

                                        if (requiredQty > 0 && IsDeallocationSuccess)//&& CheckQytAvailable(project, nodeKey, requiredQty, db1)
                                        {
                                            //get warehouse list and sum qty and check if qty available 
                                            double avlQty = 0;
                                            List<SP_FKMS_GET_NON_PLT_WAREHOUSE_LIST_Result> componentWarehouselist = GetWarehouseList(project.Trim(), nodeKey.Trim(), PlannerLocation.Trim(), db1);
                                            if (componentWarehouselist != null)
                                                avlQty = Convert.ToDouble(componentWarehouselist.Sum(u => u.finqhnd));

                                            if (Convert.ToDecimal(avlQty) >= requiredQty)
                                            {
                                                List<FKM109> objFKM109List = new List<FKM109>();
                                                //List<tltlnt500175> componentWarehouselist = GetWarehouseList(project, nodeKey, db1);

                                                #region WAREHOUSE LIST

                                                foreach (var wrh in componentWarehouselist)
                                                {
                                                    bool IsError = false;
                                                    decimal allocatedQty = 0;
                                                    string warehouse = wrh.t_cwar;
                                                    decimal t_qhnd = Convert.ToDecimal(wrh.finqhnd);

                                                    decimal remainQty = Convert.ToDecimal(requiredQty - t_qhnd); //2 - 5
                                                    if (remainQty >= 0)//1=1
                                                    {
                                                        allocatedQty = t_qhnd;
                                                        requiredQty = remainQty;
                                                    }
                                                    else
                                                    {
                                                        allocatedQty = requiredQty;
                                                        requiredQty = 0;
                                                    }

                                                    #region WEB SERVICE CALL

                                                    allocationservice objService = new allocationservice();
                                                    objService.quantitySpecified = true;
                                                    objService.element = element;
                                                    objService.location = PlannerLocation;
                                                    objService.project = project;
                                                    objService.quantity = allocatedQty;
                                                    objService.item = new string(' ', 9) + nodeKey.Trim();
                                                    objService.warehouse = warehouse;
                                                    objService.workcenter = workCenter;
                                                    objService.fullkitNo = fullKitNo;
                                                    objService.logname = PlannerPsno.Trim();
                                                    objService.budgetLine = (long)Convert.ToDouble(findno);
                                                    objService.budgetLineSpecified = true;
                                                    objService.nodeId = nodeid;
                                                    objService.nodeKey = nodeKey;
                                                    objService.psno = PlannerPsno.Trim();
                                                    objService.findno = findno;

                                                    InvokeAllocationService(objService, ref objFKM109List, ref IsError);

                                                    #endregion

                                                    if (requiredQty == 0)
                                                        break;
                                                }

                                                #endregion

                                                if (objFKM109List.Count > 0)
                                                {
                                                    db1.FKM109.AddRange(objFKM109List);
                                                    db1.SaveChanges();
                                                }
                                            }
                                        }

                                        #endregion
                                    }

                                    #endregion

                                }
                                catch (Exception)
                                {

                                }
                                #region DELETE RECORD FROM FKM110

                                AllocateNodeInFKMS(project, fullKitNo, NPLT, allocateDelete, db1);

                                #endregion
                            }
                        }
                    }
                    catch (Exception)
                    {

                    }

                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = "FKMS nodes allocated successfully";
                AllocateProjectInFKMS(sourceProject, PROJ_NPLT, allocateDelete, db1);
            }
            catch (Exception ex)
            {
                #region DELETE RECORD FROM FKM110

                if (project != "" && fullKitNo != "")
                {
                    AllocateNodeInFKMS(project, fullKitNo, NPLT, allocateDelete, db1);
                }

                #endregion

                AllocateProjectInFKMS(sourceProject, PROJ_NPLT, allocateDelete, db1);

                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            finally
            {
                db1 = null;
                serviceObj = null;
            }
        }

        public bool NPLTDeallocation(FKM109 objFKM109, string psno, string location, IEMQSEntitiesContext db, ref string ErrorMessage)
        {
            bool IsSuccess = true;
            ErrorMessage = "";

            try
            {
                deallocationservice objDeallocation = new deallocationservice();
                objDeallocation.quantitySpecified = true;
                objDeallocation.project = objFKM109.Project;
                objDeallocation.location = location;
                objDeallocation.item = objFKM109.NodeKey.Trim();
                objDeallocation.quantity = objFKM109.AllocatedQty != null ? Convert.ToDecimal(objFKM109.AllocatedQty) : 0;
                objDeallocation.warehouse = objFKM109.Warehouse;
                objDeallocation.workcenter = GetKitWorkCenter(objFKM109.NodeId, db);
                objDeallocation.fullkitNo = objFKM109.FullKitNo;
                objDeallocation.logname = psno;

                IsSuccess = InvokeDeallocationService(objDeallocation, objFKM109.Id, db, ref ErrorMessage);
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                ErrorMessage = ex.Message;
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return IsSuccess;
        }

        public string CheckSOBStatus(string sobkey)
        {
            string result = string.Empty;
            try
            {
                if (sobkey != null)
                {
                    string query = "select b.t_desc " +
                                    //"select a.t_okey, max(a.t_trst) t_trst, b.t_desc " +
                                    "from " + LNLinkedServer + ".dbo.tltlnt505175 a with(nolock)join" +
                                    LNLinkedServer + ".dbo.uvwGetEnumDesc b with(nolock) on a.t_trst = b.t_cnst and b.tableno = 'ltlnt505' and b.domain = 'ltlnt.trst'" +
                                    "where a.t_okey = '" + sobkey + "' group by a.t_okey, b.t_desc";
                    var avlQty = db.Database.SqlQuery<string>(query).FirstOrDefault();
                    if (avlQty != null)
                    {
                        result = avlQty;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //result = ex.InnerException.Message;
            }
            return result;
        }

        //public bool CheckQytAvailable(string project, string item, decimal? reqQty, IEMQSEntitiesContext db1)
        //{
        //    bool result = false;
        //    try
        //    {
        //        if (reqQty > 0)
        //        {
        //            string query1 = "select t_cono from " + LNLinkedServer + ".dbo.ttpctm110175 where t_cprj= '" + project + "'";
        //            string contract = db1.Database.SqlQuery<string>(query1).FirstOrDefault();
        //            string query = "select ISNULL(sum(t_qhnd), 0) as AvlQty from " + LNLinkedServer + ".dbo.tltlnt500175 where t_cono = '" + contract + "' and LTRIM(RTRIM(t_item)) = '" + item.Trim() + "'";

        //            double avlQty = db1.Database.SqlQuery<double>(query).FirstOrDefault();
        //            if (Convert.ToDecimal(avlQty) > 0)
        //            {
        //                if (Convert.ToDecimal(avlQty) >= reqQty)
        //                {
        //                    result = true;
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        result = false;
        //    }
        //    return result;
        //}

        public bool CheckTaskAvailable(string task, string deleverto, string project)
        {
            bool result = false;
            if (!string.IsNullOrEmpty(task))
            {
                SqlConnection objConn = new SqlConnection(Convert.ToString(ConfigurationManager.ConnectionStrings["LNConcerto"]));
                List<TaskAutoCompleteModel> lstDrawingNo = new List<TaskAutoCompleteModel>();
                try
                {
                    objConn.Open();
                    SqlCommand objCmd = new SqlCommand();
                    objCmd.Connection = objConn;
                    objCmd.CommandType = CommandType.Text;
                    SqlDataAdapter dataAdapt = new SqlDataAdapter();
                    DataTable dataTable = new DataTable();
                    //objCmd.CommandText = "select a.TASKUNIQUEID,a.contact, a.NAME [TaskName] from PROJ_TASK a with(nolock)  where a.contact in ('" + objFKM113.TaskManager.Replace(",", "','") + "') and contact is not null";
                    objCmd.CommandText = "select a.TASKUNIQUEID,a.contact, a.NAME [TaskName] from PROJ_TASK a with(nolock)  where a.TASKUNIQUEID in (" + task + ") and a.PROJECTNAME='" + project + "' and contact is not null";
                    dataAdapt.SelectCommand = objCmd;
                    dataAdapt.Fill(dataTable);
                    if (dataTable != null && dataTable.Rows.Count > 0)
                    {
                        lstDrawingNo = (from DataRow row in dataTable.Rows
                                        select new TaskAutoCompleteModel
                                        {
                                            text = Convert.ToString(row["contact"]),
                                            id = Convert.ToString(row["TASKUNIQUEID"])
                                        }).ToList();

                        if (!string.IsNullOrEmpty(deleverto))
                        {
                            //like h-mo, d-di, dd-kji values get from fkm113
                            string taskitems = db.FKM113.Where(f => f.DeleverTo == deleverto).Select(x => x.TaskManager).FirstOrDefault();

                            if (!string.IsNullOrEmpty(taskitems))
                            {
                                List<string> intobj = taskitems.Split(',').ToList();
                                foreach (var item in lstDrawingNo)
                                {
                                    if (!intobj.Any(x => x.Contains(item.text)))
                                    {
                                        result = true;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                result = true;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    result = false;
                }
                finally
                {
                    if (objConn.State == ConnectionState.Open)
                        objConn.Close();
                }
            }
            return result;
        }

        public List<SP_FKMS_GET_NON_PLT_WAREHOUSE_LIST_Result> GetWarehouseList(string project, string item, string location, IEMQSEntitiesContext db1)
        {
            List<SP_FKMS_GET_NON_PLT_WAREHOUSE_LIST_Result> list = new List<SP_FKMS_GET_NON_PLT_WAREHOUSE_LIST_Result>();
            try
            {
                string query1 = "select t_cono from " + LNLinkedServer + ".dbo.ttpctm110175 where t_cprj= '" + project + "'";
                string contract = db1.Database.SqlQuery<string>(query1).FirstOrDefault();
                list = db1.SP_FKMS_GET_NON_PLT_WAREHOUSE_LIST(contract, item, location).ToList();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return list;
        }

        public void InvokeAllocationService(allocationservice obj, ref List<FKM109> objFKM109List, ref bool IsError)
        {
            FKM109 objFKM109 = new FKM109();
            try
            {
                FKSOBService serviceObj = new FKSOBService();

                planner1sendrequestResponseType P1createResponse = new planner1sendrequestResponseType();
                planner1sendrequestRequestType P1createRequest = new planner1sendrequestRequestType();
                planner1sendrequestRequestTypeControlArea P1controlArea = new planner1sendrequestRequestTypeControlArea();
                planner1sendrequestRequestTypeFKSOB P1dataArea = new planner1sendrequestRequestTypeFKSOB();

                P1controlArea.processingScope = processingScope.request;
                P1dataArea.quantitySpecified = obj.quantitySpecified;
                P1dataArea.element = obj.element;
                P1dataArea.location = obj.location;
                P1dataArea.project = obj.project;
                P1dataArea.quantity = obj.quantity;
                P1dataArea.item = obj.item;
                P1dataArea.warehouse = obj.warehouse;
                P1dataArea.workcenter = obj.workcenter;
                P1dataArea.fullkitNo = obj.fullkitNo;
                P1dataArea.logname = obj.psno;
                P1dataArea.budgetLine = obj.budgetLine;
                P1dataArea.budgetLineSpecified = obj.budgetLineSpecified;

                P1createRequest.ControlArea = P1controlArea;
                P1createRequest.DataArea = new planner1sendrequestRequestTypeFKSOB[1];
                P1createRequest.DataArea[0] = P1dataArea;

                P1createResponse = serviceObj.planner1sendrequest(P1createRequest);
                if (P1createResponse.InformationArea == null)
                {
                    objFKM109.AllocatedQty = obj.quantity;
                }
                else
                {
                    objFKM109.AllocatedQty = 0;
                    objFKM109.ErrorMsg = P1createResponse.InformationArea[0].messageText.ToString();
                }
            }
            catch (Exception ex)
            {
                IsError = true;
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                objFKM109.AllocatedQty = 0;
                objFKM109.ErrorMsg = ex.Message.ToString();

                #region As discussed with satish on 19/07/2018. If exception occurs, no need to deallocate already allocated component.

                //if (objFKM109List.Count > 0)
                //{
                //    for (int i = objFKM109List.Count - 1; i >= 0; i--)
                //    {
                //        #region DEALLOCATE LOGIC HERE

                //        bool IsSuccess1 = true;
                //        if (objFKM109List[i].AllocatedQty > 0)
                //        {
                //            deallocationservice objDeallocation = new deallocationservice();
                //            objDeallocation.quantitySpecified=true;
                //            objDeallocation.project = objFKM109List[i].Project;
                //            objDeallocation.location = obj.location;
                //            objDeallocation.item = objFKM109List[i].NodeKey;
                //            objDeallocation.quantity = objFKM109List[i].AllocatedQty != null ? Convert.ToDecimal(objFKM109List[i].AllocatedQty) : 0;
                //            objDeallocation.warehouse = objFKM109List[i].Warehouse;
                //            objDeallocation.workcenter = GetKitWorkCenter(objFKM109List[i].NodeId);
                //            objDeallocation.fullkitNo = objFKM109List[i].FullKitNo;
                //            objDeallocation.logname = obj.psno;

                //            IsSuccess = InvokeDeallocationService(objDeallocation);
                //        }

                //        //remove from objFKM109List
                //        if (IsSuccess)
                //            objFKM109List.Remove(objFKM109List[i]);

                //        #endregion
                //    }
                //}

                #endregion
            }
            finally
            {
                objFKM109.Project = obj.project;
                objFKM109.NodeId = obj.nodeId;
                objFKM109.FindNo = obj.findno;
                objFKM109.NodeKey = obj.nodeKey;
                objFKM109.FullKitNo = obj.fullkitNo;
                objFKM109.Warehouse = obj.warehouse;
                objFKM109.CreatedBy = obj.psno;
                objFKM109.CreatedOn = DateTime.Now;
                objFKM109List.Add(objFKM109);
            }
        }

        public bool InvokeDeallocationService(deallocationservice obj, int Id, IEMQSEntitiesContext db, ref string ErrorMessage)
        {
            bool IsSuccess = true;
            ErrorMessage = "";

            try
            {
                FKSOBService serviceObj = new FKSOBService();

                sfcReturnInventoryResponseType sfcCreateResponse = new sfcReturnInventoryResponseType();
                sfcReturnInventoryRequestType sfcCreateRequest = new sfcReturnInventoryRequestType();
                sfcReturnInventoryRequestTypeControlArea sfcControlArea = new sfcReturnInventoryRequestTypeControlArea();
                sfcReturnInventoryRequestTypeFKSOB sfcDataArea = new sfcReturnInventoryRequestTypeFKSOB();

                sfcControlArea.processingScope = processingScope.request;
                sfcDataArea.quantitySpecified = obj.quantitySpecified;
                sfcDataArea.project = obj.project;
                sfcDataArea.location = obj.location;
                sfcDataArea.item = new string(' ', 9) + obj.item;
                sfcDataArea.quantity = obj.quantity;
                sfcDataArea.warehouse = obj.warehouse;
                sfcDataArea.workcenter = obj.workcenter;
                sfcDataArea.fullkitNo = obj.fullkitNo;
                sfcDataArea.logname = obj.logname;

                sfcCreateRequest.ControlArea = sfcControlArea;
                sfcCreateRequest.DataArea = new sfcReturnInventoryRequestTypeFKSOB[1];
                sfcCreateRequest.DataArea[0] = sfcDataArea;

                sfcCreateResponse = serviceObj.sfcReturnInventory(sfcCreateRequest);

                if (sfcCreateResponse.InformationArea == null)
                {
                    IsSuccess = true;
                }
                else
                {
                    IsSuccess = false;
                    ErrorMessage = sfcCreateResponse.InformationArea[0].messageText.ToString();
                }
            }
            catch (Exception ex)
            {
                var RejectByShopMessage = "Record not found to process Return Inventory.";
                if (ex.Message.ToLower() == RejectByShopMessage.ToLower())
                {
                    var objFKM109 = db.FKM109.Where(i => i.Id == Id).FirstOrDefault();
                    if (objFKM109 != null)
                    {
                        db.FKM109.Remove(objFKM109);
                        db.SaveChanges();
                    }
                }
                IsSuccess = false;
                ErrorMessage = ex.Message;
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return IsSuccess;
        }

        public string GetKitWorkCenter(int? componentNodeId, IEMQSEntitiesContext db)
        {
            string workCenter = string.Empty;
            if (componentNodeId != null)
            {
                var obj1 = db.FKM202.Where(u => u.NodeId == componentNodeId).FirstOrDefault();
                if (obj1 != null)
                {
                    var obj2 = db.FKM202.Where(u => u.NodeId == obj1.ParentNodeId).FirstOrDefault();
                    var ParentDeptCode = FN_GET_PARENTDEPT(obj2.DeleverTo);
                    workCenter = ParentDeptCode != null ? ParentDeptCode : string.Empty;
                    //string workCenter = fkm202Kit.DeleverTo != null ? fkm202Kit.DeleverTo : string.Empty;
                }
            }
            return workCenter;
        }

        #endregion

        #region PLT Allocation

        public async Task<int> LongRunningPLTAllocationAsync(string sourceProject, int ParentNodeId, string psno = "")
        {
            await Task.Run(() => PLTAllocationAsync(sourceProject, ParentNodeId, psno));
            return 1;
        }

        public void PLTAllocationAsync(string sourceProject, int ParentNodeId, string psno = "")
        {
            IEMQSEntitiesContext db1 = new IEMQSEntitiesContext();
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string objallocatation = string.Empty;
            string objproject = string.Empty;
            string objkitno = string.Empty;
            var AssemblyASM = clsImplementationEnum.NodeTypes.ASM.GetStringValue();
            var AssemblyTJF = clsImplementationEnum.NodeTypes.TJF.GetStringValue();
            var Plate = clsImplementationEnum.NodeTypes.PLT.GetStringValue();
            string PROJ_PLT = clsImplementationEnum.AllocateNodeTypes.PROJ_PLT.ToString();

            var listFKM202 = new List<FKM202>();
            try
            {
                AllocateProjectInFKMS(sourceProject, PROJ_PLT, allocateInsert, db1);

                var lstfkm202 = db1.FKM202.ToList();
                if (sourceProject != string.Empty)
                {
                    listFKM202 = lstfkm202.Where(i => i.ProductType != AssemblyASM && i.ProductType != AssemblyTJF && i.ProductType == Plate && i.Project == sourceProject).ToList();
                }
                else
                {
                    listFKM202 = lstfkm202.Where(i => i.ProductType != AssemblyASM && i.ProductType != AssemblyTJF && i.ProductType == Plate).ToList();
                }
                if (ParentNodeId != 0)
                {
                    listFKM202 = listFKM202.Where(i => i.ProductType != AssemblyASM && i.ProductType != AssemblyTJF && i.ProductType == Plate && i.Project == sourceProject && i.ParentNodeId == ParentNodeId).ToList();
                }

                var lstkitdist = listFKM202.Select(i => new { i.Project }).Distinct().ToList();

                var objFKM201PlannerDtl = (from fkm1 in db1.FKM201
                                               //join c3 in db1.COM003 on fkm1.PlannerPsno equals c3.t_psno
                                           select new { Project = fkm1.Project, PlannerPsno = fkm1.PlannerPsno }
                                         ).ToList();


                foreach (var Proj in lstkitdist)
                {
                    try
                    {
                        if (psno == "")
                        {
                            var objPlannerDtl = objFKM201PlannerDtl.Where(x => x.Project == Proj.Project).FirstOrDefault();
                            if (objPlannerDtl != null) { psno = objPlannerDtl.PlannerPsno; }
                        }
                        var lstparentdist = listFKM202.Where(x => x.Project == Proj.Project).Select(x => x.ParentNodeId).Distinct().ToList();
                        var listforPLT = db1.SP_FKMS_GETPLTALLOCATEDETAILS(Proj.Project).ToList();
                        var lstfkm108 = db1.FKM108.Where(x => x.Project == Proj.Project).ToList();
                        foreach (var parentId in lstparentdist)
                        {
                            var kit = lstfkm202.Where(x => x.NodeId == parentId).Select(x => new { Project = x.Project, KitNumber = x.NodeKey + "-" + x.NodeId, NodeId = x.NodeId }).FirstOrDefault();
                            //insert FKM110
                            objallocatation = allocateInsert;
                            objproject = kit.Project;
                            objkitno = kit.KitNumber;
                            AllocateNodeInFKMS(objproject, objkitno, Plate, allocateInsert, db1);


                            List<FKM108> lstFKM108 = new List<FKM108>();
                            List<SP_FKMS_GETPLTALLOCATEDETAILS_Result> lstplt = new List<SP_FKMS_GETPLTALLOCATEDETAILS_Result>();
                            string Producttype = clsImplementationEnum.NodeTypes.PLT.GetStringValue();
                            var objFKM202 = listFKM202.Where(i => i.ParentNodeId == kit.NodeId).ToList();

                            foreach (var item in objFKM202)
                            {
                                double availableQty = 0;
                                double requiredQty = 0;
                                double allocatedExistQty = 0;
                                double finalAvailableQty = 0;
                                double chkQty = 0;


                                if (listforPLT.Any(x => x.Partno == item.FindNo))
                                {
                                    chkQty = Convert.ToDouble(lstfkm108.Where(x => x.FindNo == item.FindNo).ToList().Sum(x => x.AllocatedQty));
                                    availableQty = Convert.ToDouble(listforPLT.Where(x => x.Partno == item.FindNo).ToList().Sum(i => i.Qty));
                                    availableQty = availableQty - chkQty;
                                }

                                requiredQty = item.NoOfPieces != null ? Convert.ToDouble(item.NoOfPieces) : 0;
                                var objParent = db1.FKM202.Where(i => i.NodeId == item.ParentNodeId).FirstOrDefault();
                                if (objParent != null)
                                {
                                    requiredQty = requiredQty * (objParent.NoOfPieces != null ? Convert.ToDouble(objParent.NoOfPieces) : 0);
                                }
                                //if (lstFKM108.Any(x => x.Project == sourceProject && x.NodeKey == item.NodeKey && x.FindNo == item.FindNo))
                                //{
                                //    allocatedExistQty = Convert.ToDouble(lstFKM108.Where(x => x.Project == sourceProject && x.NodeKey == item.NodeKey && x.FindNo == item.FindNo).ToList().Sum(c => c.AllocatedQty));
                                //}
                                finalAvailableQty = availableQty - allocatedExistQty;

                                if (finalAvailableQty >= requiredQty)
                                {
                                    lstplt = listforPLT.Where(x => x.Partno == item.FindNo).ToList();
                                    foreach (var lstquantity in lstplt)
                                    {
                                        if (Convert.ToDouble(lstquantity.Qty) > requiredQty)
                                        {
                                            if (requiredQty > 0)
                                            {
                                                if (!lstfkm108.Any(x => x.FindNo == item.FindNo && x.NodeId == item.NodeId && x.ParentNodeId == item.ParentNodeId))
                                                {
                                                    lstFKM108.Add(new FKM108
                                                    {
                                                        Project = lstquantity.Project,
                                                        ParentNodeId = item.ParentNodeId,
                                                        FindNo = lstquantity.Partno,
                                                        NodeKey = lstquantity.Part,
                                                        NodeId = item.NodeId,
                                                        PCRNo = lstquantity.PCRNo,
                                                        PCRLineNo = lstquantity.PCRLineno,
                                                        PCRLineRevNo = lstquantity.PCRlinerev,
                                                        PCLNo = lstquantity.PCLNo,
                                                        AllocatedQty = Convert.ToDecimal(requiredQty),
                                                        CreatedBy = psno,
                                                        CreatedOn = DateTime.Now,
                                                    });
                                                }
                                                lstquantity.Qty = lstquantity.Qty - Convert.ToDecimal(requiredQty);
                                                break;
                                            }
                                        }
                                        else
                                        {
                                            if (finalAvailableQty >= requiredQty)
                                            {
                                                if (lstquantity.Qty > 0)
                                                {
                                                    if (!lstfkm108.Any(x => x.FindNo == item.FindNo && x.NodeId == item.NodeId && x.ParentNodeId == item.ParentNodeId))
                                                    {
                                                        lstFKM108.Add(new FKM108
                                                        {
                                                            Project = lstquantity.Project,
                                                            ParentNodeId = item.ParentNodeId,
                                                            FindNo = lstquantity.Partno,
                                                            NodeKey = lstquantity.Part,
                                                            NodeId = item.NodeId,
                                                            PCRNo = lstquantity.PCRNo,
                                                            PCRLineNo = lstquantity.PCRLineno,
                                                            PCRLineRevNo = lstquantity.PCRlinerev,
                                                            PCLNo = lstquantity.PCLNo,
                                                            AllocatedQty = Convert.ToDouble(lstquantity.Qty) < requiredQty ? Convert.ToDecimal(lstquantity.Qty) : Convert.ToDecimal(requiredQty),
                                                            CreatedBy = psno,
                                                            CreatedOn = DateTime.Now,
                                                        });
                                                    }
                                                    finalAvailableQty = finalAvailableQty - Convert.ToDouble(lstquantity.Qty);
                                                    requiredQty = requiredQty - Convert.ToDouble(lstquantity.Qty);
                                                    lstquantity.Qty = Convert.ToDouble(lstquantity.Qty) > requiredQty ? Convert.ToDecimal(lstquantity.Qty) - Convert.ToDecimal(requiredQty) : 0;

                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            //var lstProj = db.FKM108.Where(x => x.Project == Proj.Project).ToList();
                            //var lstProj = (from f8 in db.FKM108
                            //               join f2 in db.FKM202 on f8.NodeId equals f2.NodeId
                            //               where f2.ParentNodeId == parentId
                            //               select f8
                            //            ).ToList();
                            //if (lstProj.Count() > 0)
                            //{
                            //    // or EntityState.Modified for edit etc.
                            //    foreach (var item in lstProj)
                            //    {
                            //        db1.Entry(item).State = EntityState.Deleted;
                            //    }
                            //    // db1.FKM108.RemoveRange(lstProj);
                            //}
                            if (lstFKM108.Count() > 0)
                            {
                                db1.FKM108.AddRange(lstFKM108);
                            }
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "FKMS Nodes Allocted successfully";


                            //Delete from FKM110
                            objallocatation = allocateDelete;
                            AllocateNodeInFKMS(objproject, objkitno, Plate, allocateDelete, db1);
                            objallocatation = string.Empty;
                        }
                    }
                    catch (Exception)
                    {
                    }

                }
                AllocateProjectInFKMS(sourceProject, PROJ_PLT, allocateDelete, db1);
            }
            catch (Exception ex)
            {
                if (!String.IsNullOrEmpty(objallocatation))
                {
                    AllocateNodeInFKMS(objproject, objkitno, Plate, allocateDelete, db1);
                }

                AllocateProjectInFKMS(sourceProject, PROJ_PLT, allocateDelete, db1);

                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            finally
            {
                db1 = null;
            }
        }

        #endregion

        #region Part Split 

        public ActionResult PartSplit(int NodeId, int ParentNodeId, decimal NoOfPieces = 0, bool IsCheckExist = false)
        {
            clsHelper.ResponceMsgWithNodeExist objResponseMsg = new clsHelper.ResponceMsgWithNodeExist();
            try
            {
                if (ParentNodeId != 0 && NodeId != 0)
                {
                    var objFKM202 = db.FKM202.Where(i => i.NodeId == NodeId).FirstOrDefault();
                    var objParent = db.FKM202.Where(i => i.NodeId == objFKM202.ParentNodeId).FirstOrDefault();
                    decimal TotalNoOfPieces = (objFKM202.NoOfPieces.HasValue ? objFKM202.NoOfPieces.Value : 0) * (objParent.NoOfPieces.HasValue ? objParent.NoOfPieces.Value : 0);
                    if (objFKM202 != null && TotalNoOfPieces > 0)
                    {
                        if (TotalNoOfPieces == NoOfPieces)
                        {
                            if (db.FKM202.Any(i => i.ParentNodeId == ParentNodeId && i.FindNo == objFKM202.FindNo && i.NodeKey == objFKM202.NodeKey))
                            {
                                if (IsCheckExist)
                                {
                                    objResponseMsg.Key = true;
                                    objResponseMsg.IsNodeExist = true;
                                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                                }
                                var objFKM202Update = db.FKM202.Where(i => i.ParentNodeId == ParentNodeId && i.FindNo == objFKM202.FindNo && i.NodeKey == objFKM202.NodeKey).FirstOrDefault();
                                objFKM202.NoOfPieces = TotalNoOfPieces + objFKM202Update.NoOfPieces;

                                InsertNodesInFKM125(db, objFKM202Update, "PartSplit"); //Task Id:- 20875
                                db.FKM202.Remove(objFKM202Update);

                            }
                            objFKM202.ParentNodeId = ParentNodeId;
                            db.SaveChanges();
                        }
                        else
                        {
                            var SplitQty = NoOfPieces / objParent.NoOfPieces;
                            objFKM202.NoOfPieces = objFKM202.NoOfPieces - SplitQty;
                            if (db.FKM202.Any(i => i.ParentNodeId == ParentNodeId && i.FindNo == objFKM202.FindNo && i.NodeKey == objFKM202.NodeKey))
                            {
                                if (IsCheckExist)
                                {
                                    objResponseMsg.Key = true;
                                    objResponseMsg.IsNodeExist = true;
                                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                                }
                                var objFKM202Update = db.FKM202.Where(i => i.ParentNodeId == ParentNodeId && i.FindNo == objFKM202.FindNo && i.NodeKey == objFKM202.NodeKey).FirstOrDefault();
                                objFKM202Update.NoOfPieces = objFKM202Update.NoOfPieces + NoOfPieces;
                            }
                            else
                            {
                                var objFKM202New = new FKM202();
                                objFKM202New.HeaderId = objFKM202.HeaderId;
                                objFKM202New.ParentNodeId = ParentNodeId;
                                objFKM202New.NodeTypeId = objFKM202.NodeTypeId;
                                objFKM202New.CreatedBy = objClsLoginInfo.UserName;
                                objFKM202New.CreatedOn = DateTime.Now;
                                objFKM202New.NodeName = objFKM202.NodeName;
                                objFKM202New.Project = objFKM202.Project;
                                objFKM202New.FindNo = objFKM202.FindNo;
                                objFKM202New.PartLevel = objFKM202.PartLevel;
                                objFKM202New.Description = objFKM202.Description;
                                objFKM202New.ItemGroup = objFKM202.ItemGroup;
                                objFKM202New.RevisionNo = objFKM202.RevisionNo;
                                objFKM202New.MaterialSpecification = objFKM202.MaterialSpecification;
                                objFKM202New.MaterialDescription = objFKM202.MaterialDescription;
                                objFKM202New.MaterialCode = objFKM202.MaterialCode;
                                objFKM202New.ProductType = objFKM202.ProductType;
                                objFKM202New.Length = objFKM202.Length;
                                objFKM202New.Width = objFKM202.Width;
                                objFKM202New.NoOfPieces = NoOfPieces;
                                objFKM202New.Weight = objFKM202.Weight;
                                objFKM202New.Thickness = objFKM202.Thickness;
                                objFKM202New.UOM = objFKM202.UOM;
                                objFKM202New.RequiredDate = objFKM202.RequiredDate;
                                objFKM202New.NodeKey = objFKM202.NodeKey;
                                objFKM202New.IsInsertedInPLM = objFKM202.IsInsertedInPLM;
                                objFKM202New.PLMError = objFKM202.PLMError;
                                objFKM202New.DeleverTo = objFKM202.DeleverTo;
                                objFKM202New.DeleverStatus = objFKM202.DeleverStatus;
                                objFKM202New.FindNo_Init = objFKM202.FindNo_Init;
                                objFKM202New.NodeKey_Init = objFKM202.NodeKey_Init;
                                objFKM202New.NoOfPieces_Init = objFKM202.NoOfPieces_Init;
                                objFKM202New.ParentNodeId_Init = objFKM202.ParentNodeId_Init;
                                objFKM202New.TASKUNIQUEID = objFKM202.TASKUNIQUEID;
                                objFKM202New.KitStatus = objFKM202.KitStatus;
                                objFKM202New.MaterialStatus = objFKM202.MaterialStatus;

                                db.FKM202.Add(objFKM202New);
                            }
                            db.SaveChanges();
                        }
                        if (objFKM202.ProductType != null && objFKM202.ProductType.ToLower() != ASM && objFKM202.ProductType.ToLower() != TJF)
                        {
                            Deallocation(objFKM202.NodeId, objClsLoginInfo.UserName, objClsLoginInfo.Location);
                        }
                    }
                }
                objResponseMsg.Key = true;
                objResponseMsg.IsNodeExist = false;
                objResponseMsg.Value = "Part move successfully!";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public void AllocateProjectInFKMS(string Project, string Type, string ActionType, IEMQSEntitiesContext db1)
        {
            try
            {
                if (ActionType == allocateInsert)
                {
                    var objParentFKM202 = db1.FKM110.Where(x => x.Project == Project && x.NodeType == Type).FirstOrDefault();
                    if (objParentFKM202 == null)
                    {
                        FKM110 objFKM110 = new FKM110();
                        objFKM110.Project = Project;
                        objFKM110.NodeType = Type;
                        objFKM110.FullKitNo = string.Empty;
                        objFKM110.CreatedOn = DateTime.Now;
                        db1.FKM110.Add(objFKM110);
                        db1.SaveChanges();
                    }
                }
                else if (ActionType == allocateDelete)
                {
                    var objParentFKM202 = db1.FKM110.Where(x => x.Project == Project && x.NodeType == Type).FirstOrDefault();
                    if (objParentFKM202 != null)
                    {
                        db1.FKM110.Remove(objParentFKM202);
                        db1.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }

        public void AllocateNodeInFKMS(string Project, string kitno, string PlateType, string ActionType, IEMQSEntitiesContext db1)
        {
            try
            {
                if (ActionType == allocateInsert)
                {
                    var objParentFKM202 = db1.FKM110.Where(x => x.Project == Project && x.NodeType == PlateType && x.FullKitNo == kitno).FirstOrDefault();
                    if (objParentFKM202 == null)
                    {
                        FKM110 objFKM110 = new FKM110();
                        objFKM110.Project = Project;
                        objFKM110.NodeType = PlateType;
                        objFKM110.FullKitNo = kitno;
                        objFKM110.CreatedOn = DateTime.Now;
                        db1.FKM110.Add(objFKM110);
                        db1.SaveChanges();
                    }
                }
                else if (ActionType == allocateDelete)
                {
                    var objParentFKM202 = db1.FKM110.Where(x => x.Project == Project && x.NodeType == PlateType && x.FullKitNo == kitno).FirstOrDefault();
                    if (objParentFKM202 != null)
                    {
                        db1.FKM110.Remove(objParentFKM202);
                        db1.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }

        #region Maintain Shop Task TaskManager
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult TaskManager()
        {
            return View();
        }
        public ActionResult GetTaskManagerGridDataPartial()
        {
            ///Utility/General/Get_Task_Unique_Id           
            ViewBag.lstTask = GetTaskManagerList();
            return PartialView("_LoadTaskManagerDataGridPartial");
        }

        public List<TaskAutoCompleteModel> GetTaskManagerList()
        {
            SqlConnection objConn = new SqlConnection(Convert.ToString(ConfigurationManager.ConnectionStrings["LNConcerto"]));
            List<TaskAutoCompleteModel> lstDrawingNo = new List<TaskAutoCompleteModel>();
            try
            {

                objConn.Open();
                SqlCommand objCmd = new SqlCommand();
                objCmd.Connection = objConn;
                objCmd.CommandType = CommandType.Text;
                objCmd.Parameters.AddWithValue("@LocationPrefix", objClsLoginInfo.Location[0]);
                SqlDataAdapter dataAdapt = new SqlDataAdapter();
                DataTable dataTable = new DataTable();

                objCmd.CommandText = "select distinct USER_NAME from RES_USERS a  " +
                    " WHERE left(contact,1) = @LocationPrefix order by a.USER_NAME";
                dataAdapt.SelectCommand = objCmd;
                dataAdapt.Fill(dataTable);

                if (dataTable != null && dataTable.Rows.Count > 0)
                {
                    lstDrawingNo = (from DataRow row in dataTable.Rows
                                    select new TaskAutoCompleteModel
                                    {
                                        text = Convert.ToString(row["USER_NAME"]),
                                        id = Convert.ToString(row["USER_NAME"])
                                    }).ToList();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            finally
            {
                if (objConn.State == ConnectionState.Open)
                    objConn.Close();
            }
            return lstDrawingNo;
        }

        public ActionResult loadTaskManagerDataTable(JQueryDataTableParamModel param, string ForApprove)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";
                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                string _urlform = string.Empty;
                string indextype = param.Department;
                #endregion
                string[] columnName = { "DeleverTo", "com002.t_desc", "TaskManager" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                var lstMCR = db.SP_FKMS_TASK_MANAGER(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstMCR.Select(i => i.TotalCount).FirstOrDefault();

                int newRecordId = 0;
                var newRecord = new[] {
                                    "0",
                                    Helper.GenerateHTMLTextbox(newRecordId,"txtDeleverTo","","",false,"",false)+
                                    Helper.GenerateHidden(newRecordId,"DeleverTo"),
                                    Helper.MultiSelectDropdown(new List<SelectItemList>(),newRecordId,"",false,"","","TaskManager"),
                                    Helper.GenerateGridButton(newRecordId, "Add", "Add Rocord", "fa fa-plus", "SaveRecord(0);" ),
                                };
                var res = (from h in lstMCR
                           select new[] {
                                    Convert.ToString(h.Id),
                                    h.DeleverTo,
                                    Helper.MultiSelectDropdown(new List<SelectItemList>(),h.Id,h.TaskManager,false,"UpdateData(this, "+ h.Id+",true);","","TaskManager"),
                                    Helper.GenerateGridButton(h.Id, "Delete", "Delete Record", "fa fa-trash", "DeleteRecord("+h.Id+");" ), // + Helper.GenerateGridButton(newRecordId, "Edit", "Edit Rocord", "fa fa-plus", "SaveRecord("+h.LineId+");" )
                                    }).ToList();

                res.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveTaskManager(FKM113 model)
        {
            FKM113 objFKM113 = null;
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (model.Id > 0)
                {
                    objFKM113 = db.FKM113.FirstOrDefault(x => x.Id == model.Id);
                    objFKM113.TaskManager = model.TaskManager;
                    objFKM113.EditedBy = objClsLoginInfo.UserName;
                    objFKM113.EditedOn = DateTime.Now;

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                    objResponseMsg.HeaderId = objFKM113.Id;
                }
                else
                {
                    objFKM113 = new FKM113();
                    objFKM113.TaskManager = model.TaskManager;
                    objFKM113.DeleverTo = model.DeleverTo;
                    objFKM113.CreatedBy = objClsLoginInfo.UserName;
                    objFKM113.CreatedOn = DateTime.Now;

                    db.FKM113.Add(objFKM113);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                    objResponseMsg.HeaderId = objFKM113.Id;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateTaskMasterData(int rowId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                columnValue = columnValue == null ? null : columnValue.Replace("'", "''");
                db.SP_COMMON_TABLE_UPDATE("FKM113", rowId, "Id", columnName, columnValue, objClsLoginInfo.UserName);
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteTaskManager(int Id = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objFKM113 = db.FKM113.FirstOrDefault(f => f.Id == Id);

                if (objFKM113 != null)
                {
                    db.FKM113.Remove(objFKM113);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Record Deleted Successfully";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Record not found or already deleted";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult IsShopExistInTaskManager(string DeleverTo)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objFKM113 = db.FKM113.FirstOrDefault(f => f.DeleverTo == DeleverTo);

                if (objFKM113 != null)
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "This shop already added!";
                }
                else
                {
                    objResponseMsg.Key = false;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Maintain Fullkit Area

        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult FullkitArea()
        {
            return View();
        }

        public ActionResult GetFullkitAreaGridDataPartial()
        {
            return PartialView("_LoadFullKitAreaDataGridPartial");
        }

        public ActionResult loadFullkitDataTable(JQueryDataTableParamModel param)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";
                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                string _urlform = string.Empty;
                string indextype = param.Department;
                #endregion
                string[] columnName = { "FullKitArea", "Location" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                var lstKit = db.SP_FKMS_Fullkit_Area(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstKit.Select(i => i.TotalCount).FirstOrDefault();

                int newRecordId = 0;
                var newRecord = new[] {
                                    "0",
                                    Helper.GenerateHTMLTextbox(newRecordId,"FullKitArea","","SaveRecord("+ newRecordId +")",false,"",false,"20"),
                                    GenerateHTMLCheckboxWithEvent(newRecordId,"check",true, "", true, "clsCheckbox"),
                                    Helper.GenerateGridButton(newRecordId, "Add", "Add Rocord", "fa fa-plus", "SaveRecord(0);" ),
                                };
                var res = (from h in lstKit
                           select new[] {
                                    Convert.ToString(h.Id),
                                    h.FullKitArea,
                                    h.IsActive != "false"? GenerateHTMLCheckboxWithEvent(Convert.ToInt32(h.Id),"check",true, "UpdateisActive(this, "+h.Id+")", true, "clsCheckbox") : GenerateHTMLCheckboxWithEvent(Convert.ToInt32(h.Id),"check",false, "UpdateisActive(this,"+h.Id+")", true, "clsCheckbox"),
                                    Helper.GenerateGridButton(h.Id, "Delete", "Delete Record", "fa fa-trash", "DeleteRecord("+h.Id+");" ), 
                                    // + Helper.GenerateGridButton(newRecordId, "Edit", "Edit Rocord", "fa fa-plus", "SaveRecord("+h.LineId+");" )
                                    }).ToList();

                res.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveFullkit(FKM117 model)
        {
            FKM117 objFKM117 = null;
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (model.Id > 0)
                {
                    if (!db.FKM117.Any(x => x.Id != model.Id && x.FullKitArea == model.FullKitArea))
                    {
                        objFKM117 = db.FKM117.FirstOrDefault(x => x.Id == model.Id);
                        objFKM117.IsActive = model.IsActive;
                        objFKM117.EditedBy = objClsLoginInfo.UserName;
                        objFKM117.EditedOn = DateTime.Now;

                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                    }
                }
                else
                {
                    if (!db.FKM117.Any(x => x.FullKitArea == model.FullKitArea))
                    {
                        objFKM117 = new FKM117();
                        objFKM117.FullKitArea = model.FullKitArea;
                        objFKM117.IsActive = model.IsActive;
                        objFKM117.Location = objClsLoginInfo.Location;
                        objFKM117.CreatedBy = objClsLoginInfo.UserName;
                        objFKM117.CreatedOn = DateTime.Now;

                        db.FKM117.Add(objFKM117);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteFullkit(int Id = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objFKM117 = db.FKM117.FirstOrDefault(f => f.Id == Id);

                if (objFKM117 != null)
                {
                    db.FKM117.Remove(objFKM117);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Record Deleted Successfully";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Record not found or already deleted";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        public ActionResult ReadyForParent(int NodeId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                objResponseMsg.Key = UpdateReadyForParentStatusByNodeId(NodeId);
                objResponseMsg.Value = "Kit is ready for parent.";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public bool UpdateReadyForParentStatusByNodeId(int NodeId)
        {
            bool Status = false;
            try
            {
                var objFKM202 = db.FKM202.Where(i => i.NodeId == NodeId).FirstOrDefault();
                objFKM202.KitStatus = KitStatus.ReadyForParent.GetStringValue();
                objFKM202.EditedBy = objClsLoginInfo.UserName;
                objFKM202.EditedOn = DateTime.Now;
                db.SaveChanges();
                Status = true;
            }
            catch (Exception)
            {
                throw;
            }
            return Status;
        }
        public ActionResult SendToNextKit(int NodeId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objFKM202 = db.FKM202.Where(i => i.NodeId == NodeId).FirstOrDefault();
                var objParentNode = db.FKM202.Where(i => i.NodeId == objFKM202.ParentNodeId).FirstOrDefault();
                var objDepartment = db.COM002.Where(i => i.t_dimx == objParentNode.DeleverTo).FirstOrDefault();

                objFKM202.KitStatus = KitStatus.SendToNextKit.GetStringValue();
                objFKM202.EditedBy = objClsLoginInfo.UserName;
                objFKM202.EditedOn = DateTime.Now;
                objFKM202.KitLocation = objDepartment != null ? objDepartment.t_dimx + "-" + objDepartment.t_desc : "";
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Kit is send to next kit.";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AcceptKitByParentKit(int NodeId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objFKM202 = db.FKM202.Where(i => i.NodeId == NodeId).FirstOrDefault();
                var objParentNode = db.FKM202.Where(i => i.NodeId == objFKM202.ParentNodeId).FirstOrDefault();
                var objDepartment = db.COM002.Where(i => i.t_dimx == objParentNode.DeleverTo).FirstOrDefault();
                objFKM202.KitStatus = KitStatus.Accept.GetStringValue();
                objFKM202.EditedBy = objClsLoginInfo.UserName;
                objFKM202.EditedOn = DateTime.Now;
                objFKM202.KitLocation = objDepartment != null ? objDepartment.t_dimx + "-" + objDepartment.t_desc : "";
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Kit is send to next kit.";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public bool IsSubmitAnyMaterialAndDocuments(int NodeId, string Project)
        {
            bool IsSubmited = true;
            string DocReleasedStatus = clsImplementationEnum.PlanningDinStatus.RELEASED.GetStringValue();
            string DocAprrovedStatus = clsImplementationEnum.HTRStatus.Release.GetStringValue();
            string din = clsImplementationEnum.FKMSDocumentType.DIN.GetStringValue();
            string htr = clsImplementationEnum.FKMSDocumentType.HTR.GetStringValue();
            var objFKM107 = db.FKM107.Where(x => x.NodeId == NodeId).ToList();
            var objPDN002 = db.PDN002.Where(x => x.Project == Project).ToList();
            string objmax = string.Empty;
            int objmaxno = 0;
            if (objPDN002.Count() > 0)
            {
                objmax = objPDN002.Max(x => x.IssueNo).ToString();
                objmaxno = Convert.ToInt32(objmax);
            }

            if (db.FKM106.Any(x => x.NodeId == NodeId && x.IsReady != true))
            {
                IsSubmited = false;
                return IsSubmited;
            }
            if (objFKM107.Any(x => x.NodeId == NodeId))
            {
                var data = (from pdn in objPDN002
                            join fkm in objFKM107 on pdn.DocumentNo equals fkm.DocumentNo into t
                            from fkm107 in t
                            where pdn.IssueNo == objmaxno
                            orderby pdn.LineId
                            select new
                            {
                                IssueStatus = pdn.IssueStatus,
                                PType = fkm107.DocType
                            })
                            .ToList();

                if ((data.Any(x => x.PType == din && x.IssueStatus.ToLower() != DocReleasedStatus.ToLower())) || (data.Any(x => x.PType == htr && x.IssueStatus.ToLower() != DocAprrovedStatus.ToLower())))
                {
                    IsSubmited = false;
                }
            }

            return IsSubmited;
        }
        public void UpdateSOBStatusForDelivered(int KitNodeId)
        {
            int SOBIssueCnt = 0;
            try
            {
                //var listComponent = db.FKM202.Where(i => i.ParentNodeId == KitNodeId && i.ProductType != null && i.ProductType.ToLower() != ASM && i.ProductType.ToLower() != TJF && i.ProductType.ToLower() != PLT).ToList();
                //if (listComponent.Count > 0)
                //{
                //    foreach (var item in listComponent)
                //    {
                if (db.FKM112.Any(i => i.NodeId == KitNodeId && !string.IsNullOrEmpty(i.SOBKey) && i.Status != "Issued By Store"))
                {
                    var lstSOBkeys = db.FKM112.Where(i => i.NodeId == KitNodeId && !string.IsNullOrEmpty(i.SOBKey)).ToList();
                    foreach (var key in lstSOBkeys)
                    {
                        if (key.Status != "Issued By Store")
                        {
                            key.Status = CheckSOBStatus(key.SOBKey);
                        }
                    }
                    var objFKM202 = db.FKM202.Where(i => i.NodeId == KitNodeId).FirstOrDefault();
                    if (lstSOBkeys.Count == lstSOBkeys.Where(i => i.NodeId == KitNodeId && !string.IsNullOrEmpty(i.SOBKey) && i.Status == "Issued By Store").Count())
                    {
                        objFKM202.DeleverStatus = FKMSMaterialDeliveryStatus.Delivered.GetStringValue();
                        var objDepartment = db.COM002.Where(i => i.t_dimx == objFKM202.DeleverTo).FirstOrDefault();
                        objFKM202.KitLocation = objDepartment != null ? objDepartment.t_dimx + "-" + objDepartment.t_desc : "";
                    }
                    db.SaveChanges();
                }
                //    }
                //}

                //if (listComponent.Count == SOBIssueCnt)
                //{
                //    objFKM202.DeleverStatus = FKMSMaterialDeliveryStatus.Delivered.GetStringValue();
                //}
                //db.SaveChanges();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public void UpdateReadyForKitStatusForAllNode(string Project, int NodeId = 0)
        {
            FKM202 objFKM202 = null;
            if (NodeId == 0)
            {
                objFKM202 = db.FKM202.Where(i => i.Project == Project && i.ParentNodeId == 0).FirstOrDefault();
            }
            else
            {
                objFKM202 = db.FKM202.Where(i => i.NodeId == NodeId).FirstOrDefault();
            }
            var objAllChildNodes = FN_GET_ALL_CHILDNODE_FOR_NODE_FKM202(objFKM202.NodeId, 0).ToList();

            var strClearStatus = SeamListInspectionStatus.Cleared.GetStringValue();
            var strRelease = KitStatus.Release.GetStringValue();
            var strDelivered = FKMSMaterialDeliveryStatus.Delivered.GetStringValue();

            #region  observation 16019 Update SOB Key Status
            var lstfkm202 = db.FKM202.Where(x => x.Project == Project && (x.ProductType.ToLower() == ASM || objFKM202.ProductType.ToLower() == TJF)).ToList();
            foreach (var nodeslist in lstfkm202)
            {
                UpdateSOBStatusForDelivered(nodeslist.NodeId);
            }
            #endregion

            foreach (var childs in objAllChildNodes)
            {
                var childFKM202 = db.FKM202.Where(i => i.NodeId == childs.NodeId).FirstOrDefault();
                if (childFKM202.DeleverStatus != null && string.Compare(childFKM202.DeleverStatus, strDelivered, true) == 0 &&
                    childFKM202.KitStatus != null && string.Compare(childFKM202.KitStatus, strRelease, true) == 0)
                {
                    var objSeamList = db.SP_FKMS_GET_SEAM_AND_STAGE_LIST(0, 0, "", "", childs.Project, childs.NodeId.ToString()).ToList();
                    var objPartList = db.SP_FKMS_GET_PART_AND_STAGE_LIST(0, 0, "", "", childs.NodeId.ToString()).ToList();
                    var IsReadyForParentKit = true;

                    if (childs.isPWHT)
                    {
                        //N8 initial Stagecode must be clear
                        if (objSeamList.Any(i => !i.StageCode.StartsWith("N8") && i.InspectionStatus != strClearStatus) ||
                            objSeamList.Any(i => i.StageCode.StartsWith("N8") && i.InspectionStatus != strClearStatus) ||
                            objPartList.Any(i => !i.StageCode.StartsWith("N8") && i.InspectionStatus != strClearStatus) ||
                            objPartList.Any(i => i.StageCode.StartsWith("N8") && i.InspectionStatus != strClearStatus))
                        {
                            IsReadyForParentKit = false;
                        }
                    }
                    else if (FN_GET_ALL_CHILDNODE_FOR_NODE_FKM202(childs.NodeId.Value, 0).Any(i => i.isPWHT))
                    {
                        // all stage code must be clear
                        if (objSeamList.Any(i => i.InspectionStatus != strClearStatus) || objPartList.Any(i => i.InspectionStatus != strClearStatus))
                        {
                            IsReadyForParentKit = false;
                        }
                    }
                    else
                    {
                        //No need to check N8 initial stagecode for seam clear
                        if (objSeamList.Any(i => (!i.StageCode.StartsWith("N8"))
                                                && i.InspectionStatus != strClearStatus) ||
                            objPartList.Any(i => (!i.StageCode.StartsWith("N8"))
                                                && i.InspectionStatus != strClearStatus))
                        {
                            IsReadyForParentKit = false;
                        }
                    }


                    if (IsReadyForParentKit)
                    {
                        UpdateReadyForParentStatusByNodeId(childFKM202.NodeId);
                    }
                }

            }


        }
        public ActionResult GetSeamAndStageDataGridPartial(int NodeId, string Project)
        {
            ViewBag.NodeId = NodeId;
            ViewBag.Project = Project;
            return PartialView("_GetSeamAndStageDataGridPartial");
        }
        public ActionResult LoadSeamAndStageList(JQueryDataTableParamModel param, int NodeId, string Project)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string status = param.Status;

                string whereCondition = "1=1 ";

                string[] columnName = { "q40.StageCode", "q2.StageDesc", "qms.SeamNo" };

                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                //var objFKM202 = db.FKM202.Where(i => i.NodeId == NodeId).FirstOrDefault();
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstPam = db.SP_FKMS_GET_SEAM_AND_STAGE_LIST(StartIndex, EndIndex, strSortOrder, whereCondition, Project, NodeId.ToString()).ToList();
                var lstSeamAndStage = new List<SeamAndStageEnt>();
                #region Old UI Logic
                //var GroupNo = 0;
                //var strClearStatus = SeamListInspectionStatus.Cleared.GetStringValue();
                //foreach (var item in lstPam)
                //{
                //    if (GroupNo != item.GROUP_NO)
                //    {
                //        GroupNo = (int)item.GROUP_NO.Value;
                //        lstSeamAndStage.Add(new SeamAndStageEnt
                //        {
                //            GROUP_NO = GroupNo,
                //            InspectionStatus = "",
                //            IsParent = true,
                //            Position = "",
                //            Project = "",
                //            SeamNo = "",
                //            StageCode = item.StageCode,
                //            StageDesc = item.StageDesc,
                //            TotalCount = item.TotalCount
                //        });
                //    }
                //    lstSeamAndStage.Add(new SeamAndStageEnt
                //    {
                //        GROUP_NO = GroupNo,
                //        InspectionStatus = item.InspectionStatus,
                //        IsParent = false,
                //        Position = item.Position,
                //        Project = item.Project,
                //        SeamNo = item.SeamNo,
                //        StageCode = item.StageCode,
                //        StageDesc = item.StageDesc,
                //        TotalCount = item.TotalCount,
                //        IsClear = item.InspectionStatus == strClearStatus
                //    });
                //    var objParent = lstSeamAndStage.Where(i => i.GROUP_NO == GroupNo && i.IsParent == true).FirstOrDefault();
                //    objParent.IsClear = !lstSeamAndStage.Any(i => i.GROUP_NO == GroupNo && i.IsParent != true && !i.IsClear);
                //}
                //int? totalRecords = lstSeamAndStage.Select(i => i.TotalCount).FirstOrDefault();
                //var res = (from h in lstSeamAndStage
                //           select new[] {
                //                       h.IsParent ? "<nobr><img class='plusminus' onclick='ExpandCollapsChild("+ h.GROUP_NO +", this)' src='"+WebsiteURL+"/Images/details_open.png' />"+Convert.ToString(h.GROUP_NO) + "</nobr>" : "<span class='"+ h.GROUP_NO +" child' ></span>",
                //                       h.IsParent?Convert.ToString(h.StageCode) +"-"+Convert.ToString(h.StageDesc):"",
                //                       Convert.ToString(h.SeamNo),
                //                       Convert.ToString(h.IsClear)
                //            }).ToList();
                #endregion

                #region New UI
                foreach (var item in lstPam.Select(i => i.SeamNo).Distinct())
                {
                    lstSeamAndStage.Add(new SeamAndStageEnt { SeamNo = item, StageDesc = GetAllSeamWiseStagesInTable(item, lstPam) });
                }

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();
                var res = (from h in lstSeamAndStage
                           select new[] {
                                       Convert.ToString(h.SeamNo),
                                       Convert.ToString(h.StageDesc),
                           }).ToList();
                #endregion

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string GetAllSeamWiseStagesInTable(string SeamNo, List<SP_FKMS_GET_SEAM_AND_STAGE_LIST_Result> lstPam)
        {
            string StageTable = "";
            try
            {
                var strClearStatus = SeamListInspectionStatus.Cleared.GetStringValue();
                var listStages = lstPam.Where(i => i.SeamNo == SeamNo);
                //StageTable = "<table class='tblstages'><tr>";
                foreach (var item in listStages)
                {
                    var tdClass = item.InspectionStatus == strClearStatus ? "partseamstage" : "";
                    //StageTable += "<div class='" + tdClass + "' style='padding:10px;display:inline-block;margin-right:10px;'>" + item.StageCode + "-" + item.StageDesc + "</div>";
                    StageTable += "<div class='" + tdClass + "' style='padding:10px;display:inline-block;margin-right:10px;' title=" + item.StageCode + "-" + item.StageDesc + ">" + item.StageDesc + "</div>";
                }
                //StageTable += "</tr></table>";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return StageTable;
        }
        public string GetAllPartWiseStagesInTable(string PartNo, List<SP_FKMS_GET_PART_AND_STAGE_LIST_Result> lstPam)
        {
            string StageTable = "";
            try
            {
                var strClearStatus = SeamListInspectionStatus.Cleared.GetStringValue();
                var listStages = lstPam.Where(i => i.PartNo == PartNo);
                //StageTable = "<table class='tblstages'><tr>";
                foreach (var item in listStages)
                {
                    var tdClass = item.InspectionStatus == strClearStatus ? "partseamstage" : "";
                    //StageTable += "<div class='" + tdClass + " no-warp' style='padding:10px;display:inline-block;margin-right:10px;' >" + item.StageCode + "-" + item.StageDesc + "</div>";
                    StageTable += "<div class='" + tdClass + " no-warp' style='padding:10px;display:inline-block;margin-right:10px;' title=" + item.StageCode + "-" + item.StageDesc + " >" + item.StageDesc + "</div>";
                }
                //StageTable += "</tr></table>";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return StageTable;
        }

        public ActionResult GetPartAndStageDataGridPartial(int NodeId)
        {
            ViewBag.NodeId = NodeId;
            return PartialView("_GetPartAndStageDataGridPartial");
        }
        public ActionResult LoadPartAndStageList(JQueryDataTableParamModel param, int NodeId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string status = param.Status;

                string whereCondition = "1=1";

                string[] columnName = { "q45.StageCode", "q2.StageDesc", "childs.FindNo" };

                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                //var objFKM202 = db.FKM202.Where(i => i.NodeId == NodeId).FirstOrDefault();
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstPam = db.SP_FKMS_GET_PART_AND_STAGE_LIST(StartIndex, EndIndex, strSortOrder, whereCondition, NodeId.ToString()).ToList();
                var lstPartAndStage = new List<PartAndStageEnt>();
                #region Old UI Logic
                //var GroupNo = 0;
                //var strClearStatus = SeamListInspectionStatus.Cleared.GetStringValue();
                //foreach (var item in lstPam)
                //{
                //    if (GroupNo != item.GROUP_NO)
                //    {
                //        GroupNo = (int)item.GROUP_NO.Value;
                //        lstPartAndStage.Add(new PartAndStageEnt
                //        {
                //            GROUP_NO = GroupNo,
                //            InspectionStatus = "",
                //            IsParent = true,
                //            PartNo = "",
                //            StageCode = item.StageCode,
                //            StageDesc = item.StageDesc,
                //            TotalCount = item.TotalCount
                //        });
                //    }
                //    lstPartAndStage.Add(new PartAndStageEnt
                //    {
                //        GROUP_NO = GroupNo,
                //        InspectionStatus = item.InspectionStatus,
                //        IsParent = false,
                //        PartNo = item.PartNo,
                //        StageCode = item.StageCode,
                //        StageDesc = item.StageDesc,
                //        TotalCount = item.TotalCount,
                //        IsClear = item.InspectionStatus == strClearStatus
                //    });
                //    var objParent = lstPartAndStage.Where(i => i.GROUP_NO == GroupNo && i.IsParent == true).FirstOrDefault();
                //    objParent.IsClear = !lstPartAndStage.Any(i => i.GROUP_NO == GroupNo && i.IsParent != true && !i.IsClear);
                //}
                //int? totalRecords = lstPartAndStage.Select(i => i.TotalCount).FirstOrDefault();
                //var res = (from h in lstPartAndStage
                //           select new[] {
                //                       h.IsParent ? "<nobr><img class='plusminus' onclick='ExpandCollapsChild("+ h.GROUP_NO +", this)' src='"+WebsiteURL+"/Images/details_open.png' />"+Convert.ToString(h.GROUP_NO) + "</nobr>" : "<span class='"+ h.GROUP_NO +" child' ></span>",
                //                       h.IsParent?Convert.ToString(h.StageCode) +"-"+Convert.ToString(h.StageDesc):"",
                //                       Convert.ToString(h.PartNo),
                //                       Convert.ToString(h.IsClear)
                //            }).ToList();
                #endregion

                #region New UI Logic
                foreach (var item in lstPam.Select(i => i.PartNo).Distinct())
                {
                    lstPartAndStage.Add(new PartAndStageEnt { PartNo = item, StageDesc = GetAllPartWiseStagesInTable(item, lstPam) });
                }

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();
                var res = (from h in lstPartAndStage
                           select new[] {
                                       Convert.ToString(h.PartNo),
                                       Convert.ToString(h.StageDesc),
                           }).ToList();
                #endregion

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetFullkitAreaList(string term)
        {
            try
            {
                if (term != null && term != "")
                {
                    var items = (from f17 in db.FKM117
                                 where f17.IsActive == true && f17.FullKitArea.Contains(term)
                                 select new AutoCompleteModel { Value = f17.FullKitArea, Text = f17.FullKitArea }
                                ).ToList();

                    return Json(items, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var items = (from f17 in db.FKM117
                                 where f17.IsActive == true
                                 select new AutoCompleteModel { Value = f17.FullKitArea, Text = f17.FullKitArea }
                                ).Take(10).ToList();
                    return Json(items, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveFullkitArea(int NodeId, string FullkitArea)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objFKM202 = db.FKM202.Where(i => i.NodeId == NodeId).FirstOrDefault();

                objFKM202.FullkitArea = FullkitArea;
                objFKM202.KitLocation = FullkitArea;
                objFKM202.EditedBy = objClsLoginInfo.UserName;
                objFKM202.EditedOn = DateTime.Now;
                objFKM202.KitStatus = KitStatus.SendToFullkitArea.GetStringValue();
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Kit is send to fullkit area.";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FKMSPLTAndNPLTAutoAllocation()
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                FKMSAllocationExecute();
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Allocation process is started. it will take time.";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InsertAllBOMData(string project = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string UnSuccessProjects = "";
            string SuccessProjects = "";
            try
            {
                context = new IEMQSEntitiesContext();
                var strApproved = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
                var listFKM201 = db.FKM201.Where(i => i.Status != null && i.Status == strApproved && (!i.IsCopyInProgress.HasValue || (i.IsCopyInProgress.HasValue && !i.IsCopyInProgress.Value))).ToList();
                ((IObjectContextAdapter)this.db).ObjectContext.CommandTimeout = 1800;
                if (!string.IsNullOrEmpty(project) && !string.IsNullOrWhiteSpace(project))
                {
                    lstgetAllbom = db.SP_FKMS_HBOM_FOR_PROJECT(project).ToList();
                    listFKM201 = listFKM201.Where(i => i.Project == project).ToList();
                }
                else
                {
                    lstgetAllbom = db.SP_FKMS_HBOM_FOR_PROJECT("").ToList();
                }
                var Projects = string.Join(",", listFKM201.Select(i => i.Project).Distinct().ToList());
                foreach (var objFKM201 in listFKM201)
                {
                    lstFKM104 = new List<FKM104>();
                    try
                    {
                        var Status = InsertHBOMData(objFKM201, objFKM201.CreatedBy);
                        if (Status)
                        {
                            if (SuccessProjects.Length == 0) { SuccessProjects = objFKM201.Project; }
                            else { SuccessProjects += "," + objFKM201.Project; }
                        }
                    }
                    catch (Exception ex)
                    {
                        if (UnSuccessProjects.Length == 0) { UnSuccessProjects = objFKM201.Project; }
                        else { UnSuccessProjects += "," + objFKM201.Project; }
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    }
                }

                var objFKM124 = new FKM124();
                objFKM124.Projects = Projects;
                objFKM124.SuccessProjects = SuccessProjects;
                objFKM124.UnsuccessProjects = UnSuccessProjects;
                objFKM124.SynchDate = DateTime.Now;
                db.FKM124.Add(objFKM124);
                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = "BOM Data Inserted Successfully." + (UnSuccessProjects.Length > 0 ? " Unsuccessfull Attempts For " + UnSuccessProjects + " projects." : "");
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            finally
            {
                context = null;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public bool InsertHBOMData(FKM201 objFKM201, string UserName, bool IsReturnFalseForNoRecord = false)
        {
            bool Status = false;
            string projectname = "";
            int HeaderId = 0;
            try
            {
                projectname = objFKM201.Project;
                HeaderId = objFKM201.HeaderId;

                var mainparent = context.FKM202.Where(x => x.Project == projectname && x.ParentNodeId == 0).FirstOrDefault();
                lstgetbom = lstgetAllbom.Where(i => i.project == projectname).ToList();
                if (lstgetbom.Count() > 0)
                {
                    var Parentname = lstgetbom.OrderBy(x => x.PartLevel).FirstOrDefault().ParentPart;
                    var checkfkm104data = context.FKM104.Where(x => x.Project == projectname).ToList();
                    using (DbContextTransaction dbTran = context.Database.BeginTransaction())
                    {

                        try
                        {
                            if (checkfkm104data.Count() > 0)
                            {
                                foreach (var item in checkfkm104data)
                                {
                                    context.Entry(item).State = EntityState.Deleted;
                                }
                                context.FKM104.RemoveRange(checkfkm104data);
                                context.SaveChanges();
                            }

                            FKM104 mainFKM104entry = new FKM104();
                            mainFKM104entry = InsertMainGETHBOMFKMS(mainparent, HeaderId, null, UserName);

                            var childs = lstgetbom.Where(i => i.ParentPart == Parentname).ToList();
                            foreach (var item in childs)
                            {
                                if (item.Element != JIGFIXElementNo)
                                {
                                    FKM104 parentfkm104 = new FKM104();
                                    int parentid = mainFKM104entry.NodeId;
                                    parentfkm104 = InsertGETHBOMFKMS(item, HeaderId, parentfkm104, parentid, UserName);

                                    FKM104 mainFKM104 = new FKM104();
                                    int objparentid = parentfkm104.NodeId;
                                    mainFKM104 = SetNodesFKM104(item, mainFKM104, HeaderId, objparentid, UserName);
                                    lstFKM104.Add(mainFKM104);
                                }
                            }
                            dbTran.Commit();
                        }
                        catch (DbEntityValidationException dex)
                        {
                            Elmah.ErrorSignal.FromCurrentContext().Raise(dex);
                            dbTran.Rollback();
                            throw dex;
                        }
                        catch (Exception ex)
                        {
                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                            dbTran.Rollback();
                            throw ex;
                        }
                    }
                }
                if (IsReturnFalseForNoRecord && lstgetbom.Count() <= 0)
                {
                    Status = false;
                }
                else
                {
                    Status = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                Status = false;
                throw ex;
            }
            return Status;
        }
        #endregion

        #region Maintain H-Kit
        [SessionExpireFilter, AllowAnonymous]//UserPermissions,
        public ActionResult HKitIndex()
        {
            ViewBag.IsDisplayOnly = false;
            ViewBag.Title = "Maintain H-Kit";
            ViewBag.IndexType = clsImplementationEnum.WPPIndexType.maintain.GetStringValue();
            return View();
        }

        [SessionExpireFilter, AllowAnonymous] //UserPermissions, 
        public ActionResult ApproverHKitIndex()
        {
            ViewBag.IsDisplayOnly = false;
            ViewBag.Title = "Approve H-Kit";
            ViewBag.IndexType = clsImplementationEnum.WPPIndexType.approve.GetStringValue();
            return View("HKitIndex");
        }

        [HttpPost]
        public ActionResult GetHKitHeaderGridDataPartial(string status, string title, string indextype, bool IsDisplayOnly = false)
        {
            ViewBag.IsDisplayOnly = IsDisplayOnly;
            ViewBag.Status = status;
            ViewBag.GridTitle = title;
            ViewBag.IndexDataFor = indextype;

            return PartialView("_GetHKitHeaderGridDataPartial"); 
        }

        public ActionResult LoadHKitHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string status = param.Status;
                var IsDisplayOnly = !string.IsNullOrEmpty(Request["IsDisplayOnly"]) ? Convert.ToBoolean(Request["IsDisplayOnly"].ToString()) : false;
                string whereCondition = "1=1";

                string indextype = param.Department;
                if (string.IsNullOrWhiteSpace(indextype))
                {
                    indextype = clsImplementationEnum.WPPIndexType.maintain.GetStringValue();
                }
                if (status.ToLower() == "pending")
                {
                    if (indextype == clsImplementationEnum.WPPIndexType.maintain.GetStringValue())
                    {
                        whereCondition += " and status in ('" + clsImplementationEnum.PAMStatus.Returned.GetStringValue() + "','" + clsImplementationEnum.PAMStatus.Draft.GetStringValue() + "')";
                    }
                    else
                    {
                        whereCondition += " and status in ('" + clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue() + "')";
                    }
                }

                string[] columnName = { "Project", "Customer", "ZeroDate", "CDD", "CreatedBy", "CONVERT(nvarchar(20),CreatedOn,103)" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstPam = db.SP_FKM_GET_HKIT_HEADERDETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from h in lstPam
                           select new[] {
                               Convert.ToString(h.HeaderId),
                               Convert.ToString(h.Project),
                               Convert.ToString(h.Customer),
                               Convert.ToString(h.ZeroDate),
                               Convert.ToString(h.CDD),
                               Convert.ToString(h.CreatedBy),
                               Convert.ToString(h.CreatedOn),
                               "<center>"+ (IsDisplayOnly ? Helper.GenerateActionIcon(h.HeaderId, "View", "View Detail", "fa fa-eye", "", WebsiteURL + "/FKM/MaintainFKMS/HKitDetails/"+h.HeaderId +"?urlForm="+indextype,false) : Helper.GenerateActionIcon(h.HeaderId, "View", "View Detail", "fa fa-eye", "", WebsiteURL + "/FKM/MaintainFKMS/HKitDetails/"+h.HeaderId +"?urlForm="+indextype,false)) +"</center>"
                    }).ToList();
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult LoadHKitTemplate(string Project)
        {
            var objResponseMsg = new clsHelper.ResponceMsgWithObject();
            try
            {

                var lstHKit = db.SP_FKM_GET_HKIT_LIST("c.Project = '" + Project + "' and c.ProductType not in ('FRG','ASM')");
                var nodeFolderTypeId = db.FKM203.FirstOrDefault(f => f.NodeLevel == 5).Id;
                var nodePartTypeId = db.FKM203.FirstOrDefault(f => f.NodeLevel == 0).Id;
                //var FolderTypes = new string[] { "FRG", "ASM" };
                var lstParentNodeData = new List<Models.HKitNodeData>();
                var lstNodeData = new List<Models.HKitNodeData>();
                //var lstNodeData =  lstHKit.Select(s => new Models.NodeData { key = Convert.ToInt32(s.CHildFindNo), name = s.CHildDescription, parent = Convert.ToInt32(s.ParentFindNo), level = nodeFolderTypeId }).ToList();
                var lstFKM242 = db.FKM242.Where(w => w.Project == Project).Select(s => s.FindNo +s.NodeKey + s.ParentFindNo + s.ParentNodeKey).ToList();
                foreach (var item in lstHKit)
                {
                    if (!lstParentNodeData.Any(a => a.key + "" == item.ParentFindNo))
                        lstParentNodeData.Add(new Models.HKitNodeData { key = Convert.ToInt32(item.ParentFindNo), findno = item.ParentFindNo, part = item.ParentPart, name = item.ParentDescription, parent = 0, level = nodeFolderTypeId, selectable = false });
                    lstNodeData.Add(new Models.HKitNodeData { type = "Part", key = Convert.ToInt32(item.ParentFindNo + item.CHildFindNo), name = item.CHildDescription, producttype = item.ChildProductType, findno = item.CHildFindNo, part = item.ChildPart, parent = Convert.ToInt32(item.ParentFindNo), parentfindno = item.ParentFindNo, parentpart = item.ParentPart, level = nodePartTypeId, selectable = !lstFKM242.Contains(item.CHildFindNo + item.ChildPart + item.ParentFindNo + item.ParentPart) });
                }
                if (lstNodeData.Any())
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.data = lstParentNodeData.Concat(lstNodeData);
                }
                else
                {
                    objResponseMsg.Value = string.Format(clsImplementationMessage.CommonMessages.Notavailable.ToString());
                }
            }
            catch (Exception ex)
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error;

                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public ActionResult HKitDetails(int id = 0, string urlForm = "")
        {
            var objFKM241 = new FKM241();
            ViewBag.IsDisplayOnly = urlForm == clsImplementationEnum.PBPIndexType.display.GetStringValue();
            ViewBag.path = ConfigurationManager.AppSettings["File_Upload_URL"];
            List<FKM203> objFKM203 = db.FKM203.ToList();
            UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;
            ViewBag.HeaderId = id;

            if (id > 0)
            {
                var lstFKM242 = db.FKM242.Where(x => x.HeaderId == id).ToList();
                objFKM241 = db.FKM241.FirstOrDefault(x => x.HeaderId == id);
                ViewBag.ZeroDate = objFKM241.ZeroDate.Value.ToShortDateString();
                //ViewBag.IsCopy = lstFKM242.Any(x => x.PlannerStructure != null) != false ? "true" : "false";
                ViewBag.CDD = objFKM241.CDD.Value.ToShortDateString();
                // Task 16106 Product type get from PDN001 table
                ViewBag.ProductType = objFKM241.ProductType != null ? objFKM241.ProductType : "";
                var Files = (new clsFileUpload()).GetDocuments("FKM241/" + objFKM241.HeaderId + "/R" + objFKM241.RevNo);
                if (Files.Any())
                {
                    var filename = Files.FirstOrDefault().Name;
                    ViewBag.fileName = (filename.Length > 10) ? filename.Substring(0, 10) + "..." : filename;
                    ViewBag.fullFileName = filename;
                    ViewBag.image = Files.FirstOrDefault().URL;
                }
                // observation 16019 Default SOB Status Update Project Load
                //UpdateReadyForKitStatusForAllNode(objFKM241.Project);

                //ViewBag.EquipmentId = db.FKM202.Where(x => x.HeaderId == objFKM201.HeaderId && x.NodeName.ToLower() == "BORL-Site Service".ToLower()).FirstOrDefault().NodeId;
                ViewBag.Project = Manager.GetProjectAndDescription(objFKM241.Project);
                ViewBag.Customer = db.Database.SqlQuery<string>(@"select d.t_bpid +'-' + d.t_nama as [Customer] from " + LNLinkedServer + ".dbo.ttccom100175 d where d.t_bpid = '" + objFKM241.Customer + "'").FirstOrDefault();
                if (!String.IsNullOrWhiteSpace(objFKM241.ApprovedBy))
                    ViewBag.ApprovedBy = objFKM241.ApprovedBy + " - " + Manager.GetUserNameFromPsNo(objFKM241.ApprovedBy);
                //int SectionId = objFKM203.Where(x => x.NodeType.ToLower() == clsImplementationEnum.FKMSNodeType.Section.GetStringValue().ToLower()).FirstOrDefault().Id;
                var ProjectNode = lstFKM242.FirstOrDefault(x => x.PlannerStructure != null);
                if (ProjectNode != null)
                    ViewBag.NodeData = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Models.HKitNodeData>>(ProjectNode.PlannerStructure);
                else
                    ViewBag.NodeData = new List<Models.HKitNodeData>();
                ViewBag.lstFKM242 = lstFKM242.Select(s => new { s.NodeId, s.IsInsertedInPLM, s.PLMError, s.FindNo, s.Weight, s.NoOfPieces, s.ItemGroup, s.NodeKey, s.Description }).ToList();

                //ViewBag.IsTreeGenerated = (objFKM201.Status != clsImplementationEnum.CTQStatus.Approved.GetStringValue() && ProjectNode != null) ? "true" : "false";
                ViewBag.IsRevert = lstFKM242.Any(x => x.PlannerStructure != null) ? "true" : "false";

                //objFKM201 = db.FKM201.FirstOrDefault(f => f.HeaderId == id);
                if (urlForm == clsImplementationEnum.WPPIndexType.approve.GetStringValue())
                {
                    //if (objFKM201.ApprovedBy != objClsLoginInfo.UserName && objClsLoginInfo.GetUserRoleList().Contains("PLNG2"))
                    // 16562 observation Access denied error
                    if ((!objClsLoginInfo.GetUserRoleList().Contains("PLNG1")) && (!objClsLoginInfo.GetUserRoleList().Contains("PLNG2")))
                        return new RedirectResult("~/Authentication/Authenticate/AccessDenied");
                    if (!(objFKM241.ApprovedBy != objClsLoginInfo.UserName))
                        ViewBag.buttonname = "approve";
                    ViewBag.backbutton = "backbutton";
                }
                else
                    ViewBag.buttonname = "maintain";

                ViewBag.NodeLevel = objFKM203.Select(s => new { s.Id, s.NodeType, s.NodeLevel });
            }
            else
            {
                ViewBag.buttonname = "maintain";
                objFKM241.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
            }

            return View("HKitDetails", objFKM241);
        }

        [SessionExpireFilter] //UserPermissions
        public ActionResult DisplayHKit()
        {
            ViewBag.IsDisplayOnly = true;
            ViewBag.Title = "Display H-Kit";
            ViewBag.IndexType = clsImplementationEnum.PBPIndexType.display.GetStringValue();
            return View("HKitIndex");
        }

        public JsonResult HKitFilterProject(string project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                bool isProjectExist = db.FKM241.Where(x => x.Project == project).Any();
                if (isProjectExist)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = string.Format(clsImplementationMessage.CommonMessages.ProjectExist.ToString(), project);
                }
                else
                {
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SaveHKitHeader(FKM241 model, bool isDeleteEnabled, string PlannerStructure)
        {
            FKM241 objFKM241 = null;
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (model.HeaderId > 0)
                {
                    var DefaultLevel = new string[] { clsImplementationEnum.FKMSNodeType.Project.GetStringValue(), clsImplementationEnum.FKMSNodeType.LooseItem.GetStringValue(), clsImplementationEnum.FKMSNodeType.SpareItem.GetStringValue(), clsImplementationEnum.FKMSNodeType.Template.GetStringValue(), clsImplementationEnum.FKMSNodeType.Equipment.GetStringValue() };
                    var DefaultLevelId = db.FKM203.Where(x => DefaultLevel.Contains(x.NodeType)).Select(x => x.Id).ToList();
                    int PWHTSrNo = 0, SectionSrNo = 0, GenerateNo = 0;

                    objFKM241 = db.FKM241.Where(x => x.HeaderId == model.HeaderId).FirstOrDefault();
                    objFKM241.Project = model.Project;
                    objFKM241.Customer = model.Customer;
                    objFKM241.ZeroDate = model.ZeroDate;
                    objFKM241.CDD = model.CDD;
                    objFKM241.Status = model.Status;
                    objFKM241.ProductType = model.ProductType;
                    objFKM241.RevNo = model.RevNo;
                    if (!string.IsNullOrWhiteSpace(model.ApprovedBy))
                        objFKM241.ApprovedBy = model.ApprovedBy.Split('-')[0].Trim();
                    objFKM241.EditedBy = objClsLoginInfo.UserName;
                    objFKM241.EditedOn = DateTime.Now;
                    #region FKM242 Node Updates 
                    var lstNodeData = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Models.HKitNodeData>>(PlannerStructure);//.OrderBy(o=> o.name).ToList();
                    var nonParent = new Models.HKitNodeData() { key = 0, name = "", level = DefaultLevelId[0] };
                    AddUpdateFKM242ByNode(ref DefaultLevelId, ref lstNodeData, nonParent, model.HeaderId, model.Project, ref PWHTSrNo, ref SectionSrNo, ref GenerateNo);

                    var lstNodeIDs = lstNodeData.Select(s => s.key);
                    var deletedFKM242 = db.FKM242.Where(w => w.HeaderId == model.HeaderId && !lstNodeIDs.Contains(w.NodeId));
                    db.FKM242.RemoveRange(deletedFKM242);

                    var objFKM242 = db.FKM242.FirstOrDefault(f => f.HeaderId == model.HeaderId && lstNodeIDs.Contains(f.NodeId));
                    if (objFKM242 != null)
                        objFKM242.PlannerStructure = Newtonsoft.Json.JsonConvert.SerializeObject(lstNodeData);
                    #endregion

                    db.SaveChanges();
                    //InsertInToPLM(model.HeaderId, objFKM202.NodeId, AutoGeneratedFindNo, null, assid, model.Project, model.Status,0);

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                    objResponseMsg.HeaderId = objFKM241.HeaderId;
                    //objResponseMsg.RequestId = db.FKM202.Where(x => x.HeaderId == objFKM201.HeaderId && x.NodeName.ToLower() == "Equipment".ToLower()).FirstOrDefault().NodeId;
                    objResponseMsg.RevNo = objFKM241.RevNo.ToString();
                }
                else
                {
                    objFKM241 = new FKM241();
                    objFKM241.Project = model.Project;
                    objFKM241.Customer = model.Customer;
                    objFKM241.ZeroDate = model.ZeroDate;
                    objFKM241.CDD = model.CDD;
                    objFKM241.RevNo = 0;
                    objFKM241.ProductType = model.ProductType;
                    if (!string.IsNullOrWhiteSpace(model.ApprovedBy))
                        objFKM241.ApprovedBy = model.ApprovedBy.Split('-')[0].Trim();
                    objFKM241.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                    objFKM241.CreatedBy = objClsLoginInfo.UserName;
                    objFKM241.CreatedOn = DateTime.Now;
                    objFKM241.PlannerPsno = objClsLoginInfo.UserName;

                    db.FKM241.Add(objFKM241);
                    db.SaveChanges();
                    if (isDeleteEnabled == false)
                    {
                        var objFKM201 = db.FKM201.FirstOrDefault(f => f.Project == objFKM241.Project);
                        if (objFKM201 != null)
                        {
                            (new clsFileUpload()).CopyFolderContents("FKM201/" + objFKM201.HeaderId + "/R" + objFKM201.RevNo, "FKM241/" + objFKM241.HeaderId + "/R" + objFKM241.RevNo);
                        }
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                    objResponseMsg.HeaderId = objFKM241.HeaderId;
                    //objResponseMsg.RequestId = db.FKM202.Where(x => x.HeaderId == objFKM201.HeaderId && x.NodeName.ToLower() == "Equipment".ToLower()).FirstOrDefault().NodeId;
                    objResponseMsg.RevNo = objFKM241.RevNo.ToString();
                }
                if (isDeleteEnabled)
                {
                    var Files = (new clsFileUpload()).GetDocuments("FKM241/" + objFKM241.HeaderId + "/R" + objFKM241.RevNo);
                    foreach (var item in Files)
                    {
                        (new clsFileUpload()).DeleteFile("FKM241/" + objFKM241.HeaderId + "/R" + objFKM241.RevNo, item.Name);
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        private void AddUpdateFKM242ByNode(ref List<int> DefaultLevelId, ref List<Models.HKitNodeData> lstNodeData, Models.HKitNodeData objNodeData, int HeaderId, string Project, ref int PWHTSrNo, ref int SectionSrNo, ref int GenerateNo)
        {
            var partName = "";
            foreach (var item in lstNodeData.Where(w => w.parent == objNodeData.key))
            {
                FKM242 objFKM242 = db.FKM242.FirstOrDefault(f => f.HeaderId == HeaderId && f.NodeId == item.key);
                var IsNew = objFKM242 == null;
                #region Get PartName
                var fkm203 = db.FKM203.FirstOrDefault(f => f.Id == item.level);
                if (fkm203.NodeType == "SubAssembly" || fkm203.NodeType == "Self")// FKMSNodeType.SubAssembly.GetStringValue())
                {
                    partName = (Project.Length > 7 ? Project.Substring(0, 7) : Project) + "-" + GenerateNodeKey(GenerateNo, item.name, FKMSNodeType.Self);
                }
                else
                {
                    partName = item.part;
                }
                #endregion
                int? ParentNodeId = 0;
                if (item.parent > 0)
                    ParentNodeId = item.parent;
                //else if (objNodeData.parent != null)
                //    ParentNodeId = AddUpdateFKM202ByNode(ref lstNodeData, lstNodeData.FirstOrDefault(f => f.key == objNodeData.parent), HeaderId, Project, ref PWHTSrNo, ref GenerateNo, ref AutoGeneratedFindNo).NodeId;

                if (IsNew)
                {
                    objFKM242 = new FKM242();
                    objFKM242.HeaderId = HeaderId;
                    //objFKM202.NodeTypeId = item.level;// bomPart.part.NodeTypeId;
                    objFKM242.CreatedBy = objClsLoginInfo.UserName;
                    objFKM242.CreatedOn = DateTime.Now;
                    //objFKM242.NodeName = bomPart.description;
                    objFKM242.Project = Project;
                    objFKM242.ProductType = item.producttype;// clsImplementationEnum.NodeTypes.ASM.GetStringValue();
                    objFKM242.ParentFindNo = item.parentfindno;
                    objFKM242.ParentNodeKey = item.parentpart;
                    //objFKM202.ParentNodeId = objFKM202.ParentNodeId_Init = item.parent;
                    //objFKM202.NodeKey_Init = partSectionEnt.partName;// Keyfindno;
                    objFKM242.NoOfPieces = objFKM242.NoOfPieces_Init = Convert.ToInt32(_defaultNumberOfPieces);
                    objFKM242.IsPWHT = false;//partSectionEnt.IsPWHT;
                    //objFKM202.FindNo_Init = AutoGeneratedFindNo.ToString();
                    //objFKM202.ParentNodeId_Init = ParentNodeId;
                    objFKM242.NodeTypeId = item.level;// NodeTypeId;
                }
                else
                {
                    objFKM242.EditedBy = objClsLoginInfo.UserName;
                    objFKM242.EditedOn = DateTime.Now;
                }
                objFKM242.ParentNodeId = objFKM242.ParentNodeId_Init = ParentNodeId;
                objFKM242.NodeKey = objFKM242.NodeKey_Init = partName;
                objFKM242.FindNo = objFKM242.FindNo_Init = item.findno;
                objFKM242.NodeName = item.name;

                if (IsNew)
                    db.FKM242.Add(objFKM242);
                db.SaveChanges();
                if (IsNew)
                {
                    foreach (var item2 in lstNodeData.Where(w => w.parent == item.key))
                    {
                        item2.parent = objFKM242.NodeId;
                        //AddUpdateFKM202ByNode(ref nodeData, item2, HeaderId, Project, ref PWHTSrNo, ref GenerateNo, ref AutoGeneratedFindNo);
                    }
                    item.key = objFKM242.NodeId;
                }
                AddUpdateFKM242ByNode(ref DefaultLevelId, ref lstNodeData, item, HeaderId, Project, ref PWHTSrNo, ref SectionSrNo, ref GenerateNo);
            }
        }


        [HttpPost]
        public ActionResult HKitSendForApprove(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            FKM241 objFKM241 = new FKM241();
            try
            {
                if (headerId > 0)
                {
                    objFKM241 = db.FKM241.Find(headerId);

                    objFKM241.Status = clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue();
                    objFKM241.SubmittedBy = objClsLoginInfo.UserName;
                    objFKM241.SubmittedOn = DateTime.Now;
                    objFKM241.ReturnRemark = null;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "H-Kit successfully Send For Approval";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg);
        }

        [HttpPost]
        public ActionResult ApproveHKit(int headerId)
        {
            var objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (headerId > 0)
                {
                    /*var objFKM242 = db.FKM242.Where(x => x.HeaderId == headerId && x.PlannerStructure != null).FirstOrDefault();
                    if (objFKM242 != null)
                    {*/
                        var objFKM241 = db.FKM241.Find(headerId);
                        objFKM241.Status = clsImplementationEnum.CTQStatus.Approved.GetStringValue();
                        objFKM241.ApprovedBy = objClsLoginInfo.UserName;
                        objFKM241.ApprovedOn = DateTime.Now;

                        #region Insert Into PLM
                        //string assemblyType = clsImplementationEnum.FKMSNodeType.Assembly.GetStringValue();
                        //var assid = db.FKM203.Where(x => x.NodeType.ToLower() == assemblyType.ToLower()).Select(x => x.Id).FirstOrDefault();
                        //bool isSuccess = InsertInToPLM(headerId, objFKM242.NodeId, StartFindNo, objFKM242, assid, objFKM241.Project, objFKM241.Status, 1);
                        #endregion

                        //if (isSuccess)
                        //{
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "fullkit successfully approved";
                        //}
                        //else
                        //{
                        //    objResponseMsg.Key = false;
                        //    objResponseMsg.Value = "Something went wrong into PLM insertion...!";
                        //}
                    /*}
                    else
                    {
                        objResponseMsg.Value = "No data found...!";
                    }*/
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg);
        }

        [HttpPost]
        public ActionResult ReturnHKit(int headerId, string remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (headerId > 0)
                {
                    var objFKM241 = db.FKM241.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    objFKM241.ReturnRemark = remarks;
                    objFKM241.Status = clsImplementationEnum.CTQStatus.Returned.GetStringValue();
                    objFKM241.EditedBy = objClsLoginInfo.UserName;
                    objFKM241.EditedOn = DateTime.Now;

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "fullkit successfully return";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg);
        }

        #region Copy
        [HttpPost]
        public ActionResult GetHKitCopyDetails(string copyProject, string TemplateType = "K")
        {
            ViewBag.CopyProject = copyProject;
            ViewBag.lstProduct = db.PBP001.Select(s => new SelectListItem() { Value = s.ProductCode, Text = s.Product }).ToList();
            ViewBag.UserLocationCode = objClsLoginInfo.Location;
            ViewBag.UserLocation = objClsLoginInfo.LocationName;
            ViewBag.TemplateType = TemplateType;
            return PartialView("_LoadHKitCopyPartial");
        }

        [HttpPost]
        public ActionResult GetHKitProjectResult(string term, string location = "")
        {
            List<Projects> lstProjects = new List<Projects>();
            //var objCopyproject = FN_GET_ALL_COPYPROJECTS_FOR_NODE_FKM202().ToList();
            var objAccessProjects = db.fn_GetProjectAccessForEmployee(objClsLoginInfo.UserName, location).ToList();
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstProjects = (from cp in db.COM001
                               where objAccessProjects.Contains(cp.t_cprj) && cp.t_cprj.Contains(term)
                               // 11999A Project have Description Null 
                               //where objAccessProjects.Contains(cp.Project) && (cp.Project.ToLower().Contains(term.ToLower()) || cp.ProjDesc.ToLower().Contains(term.ToLower()))
                               select new Projects
                               {
                                   Value = cp.t_cprj,
                                   projectCode = cp.t_cprj,
                                   projectDescription = cp.t_cprj + " - " + cp.t_dsca,
                                   Text = cp.t_cprj + " - " + cp.t_dsca
                               }
                              ).Distinct().ToList();
            }
            else
            {
                lstProjects = (from cp in db.COM001
                               where objAccessProjects.Contains(cp.t_cprj)
                               select new Projects
                               {
                                   Value = cp.t_cprj,
                                   projectCode = cp.t_cprj,
                                   projectDescription = cp.t_cprj + " - " + cp.t_dsca,
                                   Text = cp.t_cprj + " - " + cp.t_dsca
                               }
                              ).Distinct().Take(10).ToList();
            }
            return Json(lstProjects, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetHKitSourceProjectResult(string term, string location = "")
        {
            List<Projects> lstProjects = new List<Projects>();
            var objAccessProjects = db.fn_GetProjectAccessForEmployee(objClsLoginInfo.UserName, location).ToList();
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstProjects = (from fkm in db.FKM241
                               join com in db.COM001 on fkm.Project equals com.t_cprj
                               where objAccessProjects.Contains(fkm.Project) && fkm.Project.Contains(term)
                               // 11999A Project have Description Null 
                               //where objAccessProjects.Contains(cp.Project) && (cp.Project.ToLower().Contains(term.ToLower()) || cp.ProjDesc.ToLower().Contains(term.ToLower()))
                               select new Projects
                               {
                                   Value = fkm.Project,
                                   projectCode = fkm.Project,
                                   projectDescription = fkm.Project + " - " + com.t_dsca,
                                   Text = fkm.Project + " - " + com.t_dsca
                               }
                              ).Distinct().ToList();
            }
            else
            {
                lstProjects = (from fkm in db.FKM241
                               join com in db.COM001 on fkm.Project equals com.t_cprj
                               where objAccessProjects.Contains(fkm.Project)
                               select new Projects
                               {
                                   Value = fkm.Project,
                                   projectCode = fkm.Project,
                                   projectDescription = fkm.Project + " - " + com.t_dsca,
                                   Text = fkm.Project + " - " + com.t_dsca
                               }
                              ).Distinct().Take(10).ToList();
            }
            return Json(lstProjects, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult HKitCopyDetails(copyData copyData, List<copyNodeList> copyValueList, int TemplateId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<FKM241> objMainFKM241 = db.FKM241.ToList();
                var TemplateContent = "";
                
                var objFKM241 = objMainFKM241.FirstOrDefault(i => i.Project == copyData.sourceProject);
                if (objFKM241 != null)
                {
                    if (TemplateId > 0)
                    {
                        TemplateContent = db.FKM251.FirstOrDefault(f => f.Id == TemplateId).TemplateContent;
                        var lstNodeData = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Models.NodeData>>(TemplateContent);
                        var NodeId = lstNodeData.FirstOrDefault().key;
                        var objFKM202 = db.FKM242.FirstOrDefault(f => f.NodeId == NodeId);
                        copyData.sourceProject = objFKM202 == null ? null : objFKM202.Project;
                    }
                    else
                    {
                        var srcfkm242 = db.FKM242.FirstOrDefault(x => x.Project == copyData.sourceProject && x.PlannerStructure != null);
                        TemplateContent = srcfkm242 == null ? "" : srcfkm242.PlannerStructure;
                    }
                    var objFKM203 = db.FKM203.ToList();
                    var newObjFKM241 = objMainFKM241.FirstOrDefault(x => x.Project == copyData.destProject);
                    int NodeTypeAssId = objFKM203.Where(x => x.NodeType.ToLower() == clsImplementationEnum.FKMSNodeType.Assembly.GetStringValue().ToLower()).FirstOrDefault().Id;
                    if (newObjFKM241 != null)
                    {
                        var copyfkm242 = db.FKM242.Where(x => x.Project == copyData.destProject).ToList();
                        db.FKM242.RemoveRange(copyfkm242);
                        db.SaveChanges();
                        /*if (copyfkm202.Any(x => x.NodeTypeId == NodeTypeAssId))
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Data already exists";
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }*/
                    }
                    else
                    { 
                        newObjFKM241.Project = copyData.destProject;
                        newObjFKM241.ZeroDate = Convert.ToDateTime(copyData.zeroDate);
                        newObjFKM241.CDD = Convert.ToDateTime(copyData.cdd);
                        newObjFKM241.Customer = copyData.customer;
                        newObjFKM241.RevNo = 0;
                        newObjFKM241.ApprovedBy = objFKM241.ApprovedBy;
                        newObjFKM241.ProductType = objFKM241.ProductType;
                        newObjFKM241.Status = clsImplementationEnum.LTFPSHeaderStatus.Draft.GetStringValue();
                        newObjFKM241.CreatedBy = objClsLoginInfo.UserName;
                        newObjFKM241.CreatedOn = DateTime.Now;
                        newObjFKM241.IsCopyInProgress = false;
                        newObjFKM241.PlannerPsno = objFKM241.PlannerPsno;
                        db.FKM241.Add(newObjFKM241);
                        db.SaveChanges();
                    }

                    #region Add Default Entry FKM202
                    if (newObjFKM241.HeaderId > 0)
                    {
                        #region FKM242 Node Updates 
                        var DefaultLevel = new string[] { clsImplementationEnum.FKMSNodeType.Project.GetStringValue(), clsImplementationEnum.FKMSNodeType.LooseItem.GetStringValue(), clsImplementationEnum.FKMSNodeType.SpareItem.GetStringValue(), clsImplementationEnum.FKMSNodeType.Template.GetStringValue(), clsImplementationEnum.FKMSNodeType.Equipment.GetStringValue() };
                        var DefaultLevelId = db.FKM203.Where(x => DefaultLevel.Contains(x.NodeType)).Select(x => x.Id).ToList();
                        int PWHTSrNo = 0, SectionSrNo = 0, GenerateNo = 0;

                        var lstNodeData = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Models.HKitNodeData>>(TemplateContent);//.OrderBy(o=> o.name).ToList();
                        var nonParent = new Models.HKitNodeData() { key = 0, name = "", level = DefaultLevelId[0] };
                        lstNodeData = lstNodeData.Where(w => w.type == "Label").ToList(); // only kit can copy
                        AddUpdateFKM242ByNode(ref DefaultLevelId, ref lstNodeData, nonParent, newObjFKM241.HeaderId, newObjFKM241.Project, ref PWHTSrNo, ref SectionSrNo, ref GenerateNo);

                        var lstNodeIDs = lstNodeData.Select(s => s.key);
                        var deletedFKM242 = db.FKM242.Where(w => w.HeaderId == newObjFKM241.HeaderId && !lstNodeIDs.Contains(w.NodeId));
                        db.FKM242.RemoveRange(deletedFKM242);
                        db.SaveChanges();

                        var objFKM242 = db.FKM242.FirstOrDefault(f => f.HeaderId == newObjFKM241.HeaderId && lstNodeIDs.Contains(f.NodeId));
                        if (objFKM242 != null)
                            objFKM242.PlannerStructure = Newtonsoft.Json.JsonConvert.SerializeObject(lstNodeData);
                        #endregion
                    }
                    db.SaveChanges();
                    #endregion

                    #region Image copy
                    var folderPath = "FKM201/" + objFKM241.HeaderId + "/R" + objFKM241.RevNo;
                    var destinationPath = "FKM201/" + newObjFKM241.HeaderId + "/R" + objFKM241.RevNo;

                    var existing = (new clsFileUpload()).GetDocuments(folderPath);

                    if (existing.Any())
                    {
                        (new clsFileUpload()).CopyFolderContentsAsync(folderPath, destinationPath);
                    }
                    #endregion

                    var headerId = newObjFKM241.HeaderId;
                    clspsno = objClsLoginInfo.UserName;

                    //Task<int> copyPLMtask1 = LongRunningCopyPLMAllocationAsync(copyData, copyValueList, headerId, newObjFKM201.Project, newObjFKM201.Status, IsPrimaryProjectInDIN, IsSecondaryProjectInDIN);

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "FKMS copied successfully";

                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Record not found";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        public JsonResult HKitCopyFilterProject(string project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                bool isProjectExist = db.FKM242.Any(x => x.Project == project);
                if (isProjectExist)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = string.Format(clsImplementationMessage.CommonMessages.ProjectExist.ToString(), project);
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    //if (db.SP_FKMS_GET_PARENTNODE_BY_PROJECT(project).Any())
                    //{
                    objResponseMsg.Key = true;
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    //}
                    //else
                    //{
                    //    objResponseMsg.Key = false;
                    //    objResponseMsg.Value = string.Format(clsImplementationMessage.CommonMessages.ProjectNotExistInLN.ToString(), project);
                    //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    //}
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #endregion

        #region FR Details

        public ActionResult LoadFixtureDataTable(JQueryDataTableParamModel param, string Project, int NodeId, string UserRole)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string status = param.Status;

                string whereCondition = " 1=1 and Project='" + Project + "' and NodeId= " + NodeId;

                string[] columnName = { "FixtureNo", "FixtureName", "AllocatedQty", "RequestedQty", "CompletedQty" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                //var objFKM202 = db.FKM202.Where(i => i.NodeId == NodeId).FirstOrDefault();
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstFRLines = db.SP_FKMS_FR_GET_ALLOCATION_DETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstFRLines.Select(i => i.TotalCount).FirstOrDefault();

                var objFKM118List = db.FKM118.ToList();

                var data = new List<string[]>();
                foreach (var h in lstFRLines)
                {
                    string action = "<center>";

                    //access for all user roles
                    action += Helper.GenerateActionIcon(h.RefLineId, "Attchment", "View Attachment", "fa fa-paperclip", "FixtureDocumentAttachment('" + Project + "'," + h.NodeId + "," + h.RefLineId + ")", "", false, false, "cursor:pointer;");
                    action += Helper.GenerateActionIcon(h.RefLineId, "ItemDetail", "View Item Detail", "fa fa-eye", "FixtureItemDetails(" + h.RefLineId + ");", "", false, false, "display:inline");
                    action += Helper.GenerateActionIcon(h.RefLineId, "AllocationDetail", "Allocation Detail", "fa fa-tasks", "GetFixtureAllocatedInKit(" + h.RefLineId + ");", "", false, false, "display:inline");

                    if (UserRole == UserRoleName.PLNG3.GetStringValue())
                    {
                        action += Helper.GenerateActionIcon(h.RefLineId, "UpdateOrder", "Update Order", "fa fa-sort", "LoadFixtureReOrderGridDataPartial(" + h.RefLineId + ");", "", false, false, "display:inline");

                        if (h.NodeColor != ColorFlagForFKMS.Green.GetStringValue() && h.NodeColor != ColorFlagForFKMS.Blue.GetStringValue())
                            action += Helper.GenerateActionIcon(h.RefLineId, "DeAllocate", "DeAllocate", "fa fa-trash", "DeAllocateFixture(" + h.NodeId + ", " + h.RefLineId + "," + h.AllocatedQty + "," + h.RequestedQty + ");", "", false, false, "display:inline");
                        else
                            action += Helper.GenerateActionIcon(h.RefLineId, "DeAllocate", "De-Allocate", "fa fa-trash", "", "", true, false, "display:inline");
                    }
                    else
                    {
                        action += Helper.GenerateActionIcon(h.RefLineId, "UpdateOrder", "Update Order", "fa fa-sort", "", "", true, false, "display:inline");
                        action += Helper.GenerateActionIcon(h.RefLineId, "DeAllocate", "De-Allocate", "fa fa-trash", "", "", true, false, "display:inline");
                    }

                    if (UserRole == UserRoleName.SHOP.GetStringValue())
                    {
                        if (h.CompletedQty != null && h.CompletedQty.Value > 0)
                        {
                            if (!CheckFixtureSendToFullkitArea(h.Project, h.NodeId, h.RefLineId))
                                action += Helper.GenerateActionIcon(h.RefLineId, "SendToFullkitArea", "Send To Fullkit Area", "fa fa-send", "LoadSendToFullKitArea(" + h.NodeId + "," + h.RefLineId + ");", "", false, false);
                            else
                                action += Helper.GenerateActionIcon(h.RefLineId, "SendToFullkitArea", "Send To Fullkit Area", "fa fa-send", "", "", true, false);
                        }
                        else
                            action += Helper.GenerateActionIcon(h.RefLineId, "SendToFullkitArea", "Send To Fullkit Area", "fa fa-send", "", "", true, false);
                    }
                    else
                    {
                        action += Helper.GenerateActionIcon(h.RefLineId, "SendToFullkitArea", "Send To Fullkit Area", "fa fa-send", "", "", true, false);
                    }

                    action += "</center>";

                    data.Add(new[] {
                                       Helper.GenerateHidden(h.NodeId, "NodeId", Convert.ToString(h.NodeId)),
                                       Helper.GenerateHidden(Convert.ToInt32(h.ROW_NO),"RefLineId",Convert.ToString(h.RefLineId)),
                                       Convert.ToString(h.FixtureNo),
                                       Convert.ToString(h.FixtureName),
                                       "<span title='"+h.Remarks+"'>"+Convert.ToString(h.AllocatedQty)+"</span>",
                                       Convert.ToString(h.RequestedQty),
                                       Convert.ToString(h.CompletedQty),
                                       GetFixtureLocation(h.NodeId,h.RefLineId),
                                       action
                    });
                }

                //var res = (from h in lstFRLines
                //           select new[] {
                //                       Helper.GenerateHidden(h.NodeId, "NodeId", Convert.ToString(h.NodeId)),
                //                       Helper.GenerateHidden(Convert.ToInt32(h.ROW_NO),"RefLineId",Convert.ToString(h.RefLineId)),
                //                       Convert.ToString(h.FixtureNo),
                //                       Convert.ToString(h.FixtureName),
                //                       "<span title='"+h.Remarks+"'>"+Convert.ToString(h.AllocatedQty)+"</span>",
                //                       Convert.ToString(h.RequestedQty),
                //                       Convert.ToString(h.CompletedQty),
                //                       GetFixtureLocation(h.NodeId,h.RefLineId),
                //                       Helper.GenerateActionIcon(h.RefLineId, "AllocationDetail", "Allocation Detail", "fa fa-tasks", "GetFixtureAllocatedInKit("+h.RefLineId+");","",false,false,"display:inline")+(UserRole==UserRoleName.PLNG3.GetStringValue() ? ((Helper.GenerateActionIcon(h.RefLineId, "UpdateOrder", "Update Order", "fa fa-sort", "LoadFixtureReOrderGridDataPartial("+h.RefLineId+");","",false,false,"display:inline"))+(h.CompletedQty.Value==0 ? Helper.GenerateActionIcon(h.RefLineId, "DeAllocate", "DeAllocate", "fa fa-trash", "DeAllocateFixture("+h.NodeId+", "+h.RefLineId+","+h.AllocatedQty+","+h.RequestedQty+");"):Helper.GenerateActionIcon(h.RefLineId, "DeAllocate", "De-Allocate", "fa fa-trash", "","",true))):Helper.GenerateActionIcon(h.RefLineId, "UpdateOrder", "Update Order", "fa fa-sort", "","",true,false,"display:inline")+Helper.GenerateActionIcon(h.RefLineId, "DeAllocate", "De-Allocate", "fa fa-trash", "","",true))                                       
                //           }).ToList();

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = data,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string GetFixtureLocation(int NodeId, int RefLineId)
        {
            List<string> locationList = new List<string>();
            string location = string.Empty;

            var list = (from a in db.FKM119
                        join b in db.FKM118 on a.RefId equals b.Id
                        where a.NodeId == NodeId && a.RefLineId == RefLineId
                        select b).ToList();

            foreach (var item in list)
            {
                if (!string.IsNullOrWhiteSpace(item.FullkitArea))
                {
                    if (!locationList.Any(x => x.ToString() == item.FullkitArea))
                        locationList.Add(item.FullkitArea);
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(item.KitLocation))
                    {
                        if (!locationList.Any(x => x.ToString() == item.KitLocation))
                            locationList.Add(item.KitLocation);
                    }
                }
            }

            if (locationList.Count > 0)
                location = string.Join(", ", locationList);

            return location;
        }

        public ActionResult AddFixturePartial(string Project)
        {
            List<BULocWiseCategoryModel> finalList = new List<BULocWiseCategoryModel>();
            if (!string.IsNullOrWhiteSpace(Project))
            {
                //check if project is in merged project in Planning DIN, then load fixture of primary project.
                var productType = db.PDN001.Where(x => x.Project == Project).GroupBy(u => u.Product).ToDictionary(g => g.Key, g => g.Max(item => item.IssueNo)).FirstOrDefault();
                if (productType.Key != null)
                {
                    //primary project
                }
                else
                {
                    bool IsExist = false;
                    var objPDN001List = db.PDN001.Where(x => !string.IsNullOrEmpty(x.IProject)).ToList();
                    foreach (var obj in objPDN001List)
                    {
                        string[] strList = obj.IProject.Split(',');
                        foreach (var strIProject in strList)
                        {
                            if (strIProject.ToLower() == Project.ToLower())
                            {
                                Project = obj.Project;
                                IsExist = true;
                                break;
                            }
                        }

                        if (IsExist)
                            break;
                    }
                }

                string ApprovedStatus = FRStatus.Approved.GetStringValue();

                List<FKM111> objFKM111List = new List<FKM111>();
                objFKM111List = (from u in db.FKM111
                                 where u.Project == Project && u.Status == ApprovedStatus && !u.ReUse && u.ParentId == 0
                                 orderby u.FXRSrNo ascending
                                 select u).ToList();

                var objFKM111List1 = (from a in db.FKM111
                                      join b in db.FKM111 on a.RefFixtureReuse equals b.LineId
                                      where a.Project == Project && a.Status == ApprovedStatus && a.ReUse && a.ParentId == 0 && b.ParentId == 0
                                      orderby b.FXRSrNo ascending
                                      select b).ToList();

                if (objFKM111List1.Count > 0)
                    objFKM111List.AddRange(objFKM111List1);

                objFKM111List = objFKM111List.Distinct().ToList();

                clsManager objManager = new clsManager();
                foreach (var item in objFKM111List)
                {
                    finalList.Add(new BULocWiseCategoryModel() { CatID = item.LineId.ToString(), CatDesc = objManager.GetFixtureNo(item.Project, item.FXRSrNo) + " - " + item.FixtureName });
                }
            }
            ViewBag.lstFixtures = finalList;
            return PartialView("_AddFixturePartial");
        }

        [HttpPost]
        public ActionResult GetKitData(int NodeId)
        {
            FKM202 objFKM202 = null;
            KitDetails objResponseMsg = new KitDetails();

            bool Key = false;
            string Value = "";
            string ProductType = "";

            try
            {
                objFKM202 = db.FKM202.Where(x => x.NodeId == NodeId).FirstOrDefault();
                if (objFKM202 != null)
                {
                    Key = true;
                    Value = objFKM202.NodeColor;
                    ProductType = objFKM202.ProductType;
                }
                else
                {
                    Key = false;
                    Value = "Kit details not available.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                Key = false;
                Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(new
            {
                Key = Key,
                Value = Value,
                ProductType = ProductType
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetRequiredQtyList(string term, string RefLineId)
        {
            List<AutoCompleteModel> ddlList = new List<AutoCompleteModel>();
            if (!string.IsNullOrEmpty(RefLineId))
            {
                int reflineId = Convert.ToInt32(RefLineId);
                int TotalQtyOfFixture = GetFixtureTotalQty(reflineId);

                for (int i = 1; i <= TotalQtyOfFixture; i++)
                {
                    ddlList.Add(new AutoCompleteModel() { Value = i.ToString(), Text = i.ToString() });
                }

                if (!string.IsNullOrEmpty(term))
                {
                    ddlList = (from u in ddlList where u.Text.Trim().ToLower().Contains(term.Trim().ToLower()) select u).ToList();
                }
            }
            return Json(ddlList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetFixtureQty(string RefLineId)
        {
            string strTotalQtyOfFixture = "";
            string strAvlQty = "";
            if (!string.IsNullOrEmpty(RefLineId))
            {
                int reflineId = Convert.ToInt32(RefLineId);

                int TotalQtyOfFixture = GetFixtureTotalQty(reflineId);
                int AvlQty = GetFixtureAvailableQty(reflineId);

                strTotalQtyOfFixture = TotalQtyOfFixture.ToString();
                strAvlQty = AvlQty.ToString();
            }
            return Json(new
            {
                TotalQty = strTotalQtyOfFixture,
                AvlQty = strAvlQty
            }, JsonRequestBehavior.AllowGet);
        }

        public int GetFixtureAvailableQty(int RefLineId)
        {
            int TotalQtyOfFixture = GetFixtureTotalQty(RefLineId);

            string AllocatedStatus = FRAllocationStatus.Allocated.GetStringValue();
            int UsedQty = db.FKM119.Where(x => x.RefLineId == RefLineId && x.Status == AllocatedStatus).ToList().Count();

            int AvlQty = 0;
            if (TotalQtyOfFixture >= UsedQty)
                AvlQty = TotalQtyOfFixture - UsedQty;

            return AvlQty;
        }

        public int GetFixtureTotalQty(int RefLineId)
        {
            int TotalQtyOfFixture = 0;
            var objFKM111Fix = db.FKM111.Where(x => x.LineId == RefLineId).FirstOrDefault();
            if (objFKM111Fix.QtyofFixture != null)
            {
                TotalQtyOfFixture = Convert.ToInt32(objFKM111Fix.QtyofFixture);
            }
            else
            {
                var objFKM111 = db.FKM111.Where(x => x.ParentId == RefLineId).FirstOrDefault();
                TotalQtyOfFixture = objFKM111 != null ? Convert.ToInt32(objFKM111.QtyofFixture) : 0;
            }
            return TotalQtyOfFixture;
        }

        [HttpPost]
        public ActionResult AllocateFixture(FormCollection fc)
        {
            string Project = fc["Project"].ToString();
            int NodeId = Convert.ToInt32(fc["NodeId"].ToString());
            int RefLineId = Convert.ToInt32(fc["FixtureName"].ToString());
            int RequiredQty = Convert.ToInt32(fc["RequiredQty"].ToString());
            int AvailableQty = GetFixtureAvailableQty(RefLineId);

            clsHelper.ResponseMsgWCS objResponseMsg = new clsHelper.ResponseMsgWCS();
            try
            {
                var objFKM202 = db.FKM202.Where(x => x.NodeId == NodeId && x.Project == Project).FirstOrDefault();
                if (objFKM202 != null)
                {
                    if (objFKM202.NodeColor != ColorFlagForFKMS.Blue.GetStringValue() && objFKM202.NodeColor != ColorFlagForFKMS.Green.GetStringValue())
                    {
                        if (!db.FKM119.Any(u => u.NodeId == NodeId && u.RefLineId == RefLineId && u.Project == Project))
                        {
                            if (AvailableQty >= RequiredQty)//4>2
                            {
                                List<FKM119> objFKM119List = FixtureAllocation(Project, NodeId, RefLineId, RequiredQty);

                                if (objFKM119List.Count > 0)
                                {
                                    db.FKM119.AddRange(objFKM119List);
                                    db.SaveChanges();
                                }
                                objResponseMsg.Key = true;
                                objResponseMsg.Value = clsImplementationMessage.FRMessage.AllocateSuccess.ToString();
                            }
                            else
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = "Qty not available. Please try again.";
                            }
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Kit is " + (objFKM202.NodeColor == ColorFlagForFKMS.Blue.GetStringValue() ? "Blue" : "Green") + ". you can not add fixture to it.";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public List<FKM119> FixtureAllocation(string Project, int NodeId, int RefLineId, int RequiredQty)
        {
            List<FKM119> objFKM119List = new List<FKM119>();

            //fixture list with bifurcated single qty                        
            var objFKM118List = db.FKM118.Where(u => u.RefLineId == RefLineId).OrderBy(u => u.FixtureNo).ToList();//3

            string AllocatedStatus = FRAllocationStatus.Allocated.GetStringValue();
            var objFKM119AllocatedList = db.FKM119.Where(u => u.RefLineId == RefLineId && u.Status == AllocatedStatus).Select(u => u.FixtureNo).ToList();//3                       

            if (objFKM119AllocatedList.Count > 0)
                objFKM118List = (from u in objFKM118List
                                 where !objFKM119AllocatedList.Contains(u.FixtureNo)
                                 select u).ToList();

            if (objFKM118List.Count < RequiredQty)
            {
                return objFKM119List;
            }

            var newSortedList = (from u in objFKM118List
                                 select new
                                 {
                                     Id = u.Id,
                                     RefLineId = u.RefLineId,
                                     FixtureNo = u.FixtureNo,
                                     CreatedBy = u.CreatedBy,
                                     CreatedOn = u.CreatedOn,
                                     SrNo = Convert.ToInt32(u.FixtureNo.Split('-')[u.FixtureNo.Split('-').Length - 1])
                                 }
                                 ).ToList();

            newSortedList = newSortedList.OrderBy(x => x.SrNo).ToList();

            for (int i = 0; i < RequiredQty; i++)
            {
                FKM119 objFKM119 = new FKM119();
                objFKM119.Project = Project;
                objFKM119.NodeId = NodeId;
                objFKM119.RefLineId = RefLineId;
                objFKM119.RefId = newSortedList[i].Id;
                objFKM119.FixtureNo = newSortedList[i].FixtureNo;
                objFKM119.Status = FRAllocationStatus.Allocated.GetStringValue();
                objFKM119.CreatedBy = objClsLoginInfo.UserName;
                objFKM119.CreatedOn = DateTime.Now;
                objFKM119List.Add(objFKM119);
            }

            return objFKM119List;
        }

        public ActionResult LoadAddAllocatedFixturePartial(string RefLineId, int AutoSelectQty = 0)
        {
            ViewBag.RefLineId = RefLineId;
            ViewBag.AutoSelectQty = AutoSelectQty;
            return PartialView("_AddAllocatedFixturePartial");
        }

        [HttpPost]
        public ActionResult GetAllocatedFixtureDetails(int RefLineId, int AutoSelectQty)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();

            try
            {
                string allocatedStatus = FRAllocationStatus.Allocated.GetStringValue();
                var objFKM119AllocatedList = (from u in db.FKM119
                                              where u.RefLineId == RefLineId && u.Status == allocatedStatus
                                              select u).ToList();
                var objFKM119DistinctList = (from a in db.FKM119
                                             join b in db.FKM202 on a.NodeId equals b.NodeId
                                             where a.RefLineId == RefLineId && a.Status == allocatedStatus
                                             orderby b.NodeKey
                                             select new { a.NodeId, a.RefLineId, b.NodeName, b.NodeKey, b.FindNo }).Distinct().ToList();

                //string sortCondition = "order by NodeKey asc";
                //string whereCondition = " 1=1 and Status='" + allocatedStatus + "' and RefLineId= " + RefLineId;

                //var list = db.SP_FKMS_FR_GET_ALLOCATION_DETAILS(1, int.MaxValue, sortCondition, whereCondition).ToList();

                if (objFKM119DistinctList.Count > 0)
                {
                    int tempQty = AutoSelectQty;
                    int i = 1;
                    StringBuilder sb = new StringBuilder();

                    foreach (var item in objFKM119DistinctList)
                    {
                        int AllocatedQty = (from u in objFKM119AllocatedList
                                            where u.NodeId == item.NodeId && u.RefLineId == item.RefLineId
                                            select u).Count();

                        sb.Append("<tr>");

                        sb.Append("<td class=\"hide\">");
                        sb.Append("<input type = \"hidden\" class=\"nodeid\" id=\"hdNodeId" + i + "\" value=\"" + item.NodeId.ToString() + "\"/>");
                        sb.Append("</td>");

                        sb.Append("<td>");
                        sb.Append(item.NodeName);
                        sb.Append("</td>");

                        sb.Append("<td>");
                        sb.Append(item.FindNo + "-" + item.NodeKey + "-" + item.NodeId);
                        sb.Append("</td>");

                        sb.Append("<td>");
                        sb.Append(AllocatedQty);
                        sb.Append("</td>");

                        sb.Append("<td>");
                        if (AutoSelectQty > 0)
                        {
                            if (tempQty > 0)
                            {
                                if (tempQty >= Convert.ToInt32(AllocatedQty))//2>1
                                {
                                    sb.Append("<input type = \"text\" class=\"form-control frqty\" readonly=\"readonly\" id=\"txtFRQty" + i + "\" value=\"" + AllocatedQty + "\" />");
                                    tempQty = tempQty - Convert.ToInt32(AllocatedQty);
                                }
                                else
                                {
                                    sb.Append("<input type = \"text\" class=\"form-control frqty\" readonly=\"readonly\" id=\"txtFRQty" + i + "\" value=\"" + tempQty + "\" />");
                                    tempQty = 0;
                                }
                            }
                            else
                            {
                                sb.Append("<input type = \"text\" class=\"form-control frqty\" readonly=\"readonly\" id=\"txtFRQty" + i + "\" />");
                            }
                        }
                        else
                        {
                            sb.Append("<input type = \"text\" class=\"form-control frqty\" id=\"txtFRQty" + i + "\" />");
                        }
                        sb.Append("</td>");

                        sb.Append("</tr>");
                        i++;
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = sb.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RequestFixture(string Project, int NodeId, int RefLineId, int AdditionalQty, List<FRTemp> detailList)
        {
            clsHelper.ResponseMsgWCS objResponseMsg = new clsHelper.ResponseMsgWCS();

            try
            {
                string allocatedStatus = FRAllocationStatus.Allocated.GetStringValue();
                string requestedStatus = FRAllocationStatus.Requested.GetStringValue();

                if (AdditionalQty > 0)
                {
                    //allocate fixture which qty available
                    List<FKM119> objFKM119List = FixtureAllocation(Project, NodeId, RefLineId, AdditionalQty);

                    if (objFKM119List.Count > 0)
                    {
                        db.FKM119.AddRange(objFKM119List);
                        db.SaveChanges();
                    }
                }

                foreach (var item in detailList)
                {

                    var objFKM119AllocatedList = db.FKM119.Where(u => u.NodeId == item.NodeId && u.RefLineId == RefLineId && u.Status == allocatedStatus).OrderBy(x => x.FixtureNo).ToList();

                    var newSortedList = (from u in objFKM119AllocatedList
                                         select new
                                         {
                                             Id = u.Id,
                                             RefLineId = u.RefLineId,
                                             RefId = u.RefId,
                                             FixtureNo = u.FixtureNo,
                                             CreatedBy = u.CreatedBy,
                                             CreatedOn = u.CreatedOn,
                                             SrNo = Convert.ToInt32(u.FixtureNo.Split('-')[u.FixtureNo.Split('-').Length - 1])
                                         }
                                ).ToList();

                    newSortedList = newSortedList.OrderBy(x => x.SrNo).ToList();

                    for (int i = 0; i < item.ReqQty; i++)
                    {
                        bool IsFound = false;
                        int RefId = 0;
                        string FixtureNo = "";

                        foreach (var objfkm119 in newSortedList)
                        {
                            string FRNo = objfkm119.FixtureNo;
                            if (!db.FKM119.Any(x => x.Status == requestedStatus && x.FixtureNo == FRNo))
                            {
                                IsFound = true;
                                RefId = objfkm119.RefId;
                                FixtureNo = objfkm119.FixtureNo;
                                break;
                            }
                        }

                        if (!IsFound)
                        {
                            string refIds = string.Join(",", objFKM119AllocatedList.Select(u => u.RefId).ToList());
                            string query = "select top 1 RefLineId,RefId,FixtureNo,COUNT(*) as ReqQty from FKM119 WHERE Status = '" + requestedStatus
                                + "' and RefLineId = " + RefLineId + " and RefId in (" + refIds + ") group by RefLineId,RefId,FixtureNo order by COUNT(*) ASC,CAST(RIGHT(FixtureNo, CHARINDEX('-',REVERSE(FixtureNo))-1) AS INT) ASC";
                            var objTemp = db.Database.SqlQuery<FRTemp>(query).FirstOrDefault();
                            RefId = objTemp.RefId;
                            FixtureNo = objTemp.FixtureNo;
                        }

                        int SeqNo = 1;
                        var maxNo = db.FKM119.Where(x => x.RefLineId == RefLineId && x.RefId == RefId && x.Status == requestedStatus).Max(x => x.SeqNo);
                        if (maxNo != null)
                            SeqNo = Convert.ToInt32(maxNo) + 1;

                        FKM119 objFKM119 = new FKM119();
                        objFKM119.Project = Project;
                        objFKM119.NodeId = NodeId;
                        objFKM119.RefLineId = RefLineId;
                        objFKM119.RefId = RefId;
                        objFKM119.FixtureNo = FixtureNo;
                        objFKM119.Status = FRAllocationStatus.Requested.GetStringValue();
                        objFKM119.CreatedBy = objClsLoginInfo.UserName;
                        objFKM119.CreatedOn = DateTime.Now;
                        objFKM119.SeqNo = SeqNo;
                        db.FKM119.Add(objFKM119);
                        db.SaveChanges();
                    }
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.FRMessage.AllocateSuccess.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public void SetFixtureCompleteStatus(List<ParentChildNodeEnt> objParentsclrFKM202)
        {
            string blueColor = ColorFlagForFKMS.Blue.GetStringValue();
            objParentsclrFKM202 = objParentsclrFKM202.Where(u => u.ASMPartsColor == blueColor).ToList();
            foreach (var item in objParentsclrFKM202)
            {
                UpdateFixtureCompleteStatus(item.NodeId.Value);
            }
        }

        public void UpdateFixtureCompleteStatus(int NodeId)
        {
            try
            {
                string allocatedStatus = FRAllocationStatus.Allocated.GetStringValue();
                string requestedStatus = FRAllocationStatus.Requested.GetStringValue();
                string completedStatus = FRAllocationStatus.Completed.GetStringValue();

                List<FKM119> objRequestedList = db.FKM119.Where(u => u.NodeId != NodeId && u.Status == requestedStatus).ToList();
                if (db.FKM119.Any(u => u.NodeId == NodeId && u.Status != completedStatus))
                {
                    var objFKM119List = db.FKM119.Where(u => u.NodeId == NodeId && u.Status != completedStatus).ToList();
                    foreach (var objfkm119 in objFKM119List)
                    {
                        //As per observation id#18462. Once shop user send fixture to fullkit area, fixture will be allocated to other kits.
                        //if (objfkm119.Status == allocatedStatus)
                        //{
                        //    if (objRequestedList.Any(u => u.NodeId != NodeId && u.RefLineId == objfkm119.RefLineId && u.RefId == objfkm119.RefId && u.Status == requestedStatus))
                        //    {
                        //        var objTemp = objRequestedList.Where(u => u.NodeId != NodeId && u.RefLineId == objfkm119.RefLineId && u.RefId == objfkm119.RefId && u.Status == requestedStatus).OrderBy(x => x.SeqNo).ThenBy(x => x.CreatedOn).FirstOrDefault();
                        //        objRequestedList.Where(x => x.Id == objTemp.Id).ToList().ForEach(i =>
                        //        {
                        //            i.SeqNo = null;
                        //            i.Status = allocatedStatus;
                        //            i.EditedBy = objClsLoginInfo.UserName;
                        //            i.EditedOn = DateTime.Now;
                        //        });
                        //    }
                        //}

                        objfkm119.Status = completedStatus;
                        objfkm119.EditedBy = objClsLoginInfo.UserName;
                        objfkm119.EditedOn = DateTime.Now;
                    }
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }

        [HttpPost]
        public ActionResult DeAllocateFixture(int NodeId, int RefLineId, int DeAllocateQty)
        {
            clsHelper.ResponseMsgWCS objResponseMsg = new clsHelper.ResponseMsgWCS();

            try
            {
                string allocatedStatus = FRAllocationStatus.Allocated.GetStringValue();
                string requestedStatus = FRAllocationStatus.Requested.GetStringValue();

                List<FKM119> objUnAssignedList = new List<FKM119>();

                List<FKM119> objRequestedList = db.FKM119.Where(u => u.NodeId != NodeId && u.RefLineId == RefLineId && u.Status == requestedStatus).ToList();

                //var objFKM119List = db.FKM119.Where(u => u.NodeId == NodeId && u.RefLineId == RefLineId && (u.Status == allocatedStatus || u.Status == requestedStatus)).OrderByDescending(u => u.Status).ThenByDescending(u => u.FixtureNo).Take(DeAllocateQty).ToList();
                var objFKM119List = db.FKM119.Where(u => u.NodeId == NodeId && u.RefLineId == RefLineId && (u.Status == allocatedStatus || u.Status == requestedStatus)).OrderByDescending(u => u.Status).ToList();
                var newSortedList = (from u in objFKM119List
                                     select new
                                     {
                                         Id = u.Id,
                                         RefLineId = u.RefLineId,
                                         RefId = u.RefId,
                                         FixtureNo = u.FixtureNo,
                                         CreatedBy = u.CreatedBy,
                                         CreatedOn = u.CreatedOn,
                                         Status = u.Status,
                                         SrNo = Convert.ToInt32(u.FixtureNo.Split('-')[u.FixtureNo.Split('-').Length - 1])
                                     }
                                ).ToList();

                newSortedList = newSortedList.OrderByDescending(u => u.Status).ThenByDescending(x => x.SrNo).Take(DeAllocateQty).ToList();

                foreach (var item in newSortedList)
                {
                    if (item.Status == allocatedStatus)
                    {
                        if (objRequestedList.Any(u => u.RefId == item.RefId && u.Status == requestedStatus))
                        {
                            var objTemp = objRequestedList.Where(u => u.RefId == item.RefId && u.Status == requestedStatus).OrderBy(x => x.SeqNo).ThenBy(x => x.CreatedOn).FirstOrDefault();
                            objRequestedList.Where(x => x.Id == objTemp.Id).ToList().ForEach(i =>
                            {
                                i.SeqNo = null;
                                i.Status = FRAllocationStatus.Allocated.GetStringValue();
                                i.EditedBy = objClsLoginInfo.UserName;
                                i.EditedOn = DateTime.Now;
                            });
                        }
                        else
                        {
                            objUnAssignedList.Add(new FKM119
                            {
                                Id = item.Id,
                                RefLineId = item.RefLineId,
                                RefId = item.RefId,
                                FixtureNo = item.FixtureNo,
                                CreatedBy = item.CreatedBy,
                                CreatedOn = item.CreatedOn,
                                Status = item.Status,
                            });
                        }
                    }
                }

                foreach (var item in objUnAssignedList)
                {
                    if (objRequestedList.Any(u => u.Status == requestedStatus))
                    {
                        var objTemp = objRequestedList.Where(u => u.Status == requestedStatus).OrderBy(x => x.SeqNo).ThenBy(x => x.CreatedOn).FirstOrDefault();
                        objRequestedList.Where(x => x.Id == objTemp.Id).ToList().ForEach(i =>
                        {
                            i.RefId = item.RefId;
                            i.FixtureNo = item.FixtureNo;
                            i.Remarks = clsImplementationMessage.FRMessage.AutoAllocateRemarks.ToString();
                            i.Status = FRAllocationStatus.Allocated.GetStringValue();
                            i.SeqNo = null;
                            i.EditedBy = objClsLoginInfo.UserName;
                            i.EditedOn = DateTime.Now;
                        });
                    }
                }

                if (newSortedList.Count > 0)
                {
                    var lstIds = newSortedList.Select(i => i.Id).Distinct().ToList();
                    var objFKM119 = db.FKM119.Where(i => lstIds.Contains(i.Id)).ToList();
                    if (objFKM119.Count > 0)
                    {
                        db.FKM119.RemoveRange(objFKM119);
                        db.SaveChanges();
                    }
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.FRMessage.DeAllocateSuccess.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadFixtureAllocationInKitGridDataPartial(int RefLineId)
        {
            ViewBag.RefLineId = RefLineId;
            return PartialView("_FixtureAllocationInKitGridDataPartial");
        }

        public ActionResult LoadFixtureAllocationInKitDataTable(JQueryDataTableParamModel param, int RefLineId)
        {
            try
            {

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = " RefLineId=" + RefLineId;

                string[] columnName = { "ProjectDesc", "FixtureNo", "FindNo", "NodeName", "NodeKey", "Status" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstPam = db.SP_FKMS_FR_GET_FIXTURE_ALLOCATION_IN_FKMS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from h in lstPam
                           select new[] {
                               h.ProjectDesc,
                               GetKitNumberWithColor(h.FindNo+"-"+h.NodeKey+"-"+h.NodeId,h.NodeColor),
                               h.NodeName,
                               h.FixtureNo,
                               h.Status
                    }).ToList();
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public static string GetKitNumberWithColor(string KitNumber, string ColorFlag)
        {
            string colorText = "";
            if (ColorFlag == clsImplementationEnum.ColorFlagForFKMS.Green.GetStringValue())
            {
                colorText = "<i class='fa fa-circle fa-success'></i>";
            }
            else if (ColorFlag == clsImplementationEnum.ColorFlagForFKMS.Yellow.GetStringValue())
            {
                colorText = "<i class='fa fa-circle fa-warning'></i>";
            }
            else if (ColorFlag == clsImplementationEnum.ColorFlagForFKMS.Red.GetStringValue())
            {
                colorText = "<i class='fa fa-circle fa-danger'></i>";
            }
            else if (ColorFlag == clsImplementationEnum.ColorFlagForFKMS.Violet.GetStringValue())
            {
                colorText = "<i class='fa fa-circle fa-voilet'></i>";
            }
            else if (ColorFlag == clsImplementationEnum.ColorFlagForFKMS.Grey.GetStringValue())
            {
                colorText = "<i class='fa fa-circle fa-gray'></i>";
            }
            else if (ColorFlag == clsImplementationEnum.ColorFlagForFKMS.Orange.GetStringValue())
            {
                colorText = "<i class='fa fa-circle fa-orange'></i>";
            }
            else if (ColorFlag == clsImplementationEnum.ColorFlagForFKMS.Blue.GetStringValue())
            {
                colorText = "<i class='fa fa-circle fa-blue'></i>";
            }
            else if (ColorFlag == clsImplementationEnum.ColorFlagForFKMS.Default.GetStringValue())
            {
                colorText = "";
            }

            KitNumber = colorText + " " + KitNumber;
            return KitNumber;
        }

        public ActionResult LoadFixtureReOrderGridDataPartial(int RefLineId)
        {
            ViewBag.RefLineId = RefLineId;
            return PartialView("_FixtureReOrderGridDataPartial");
        }

        public ActionResult LoadFixtureReOrderGridDataTable(int RefLineId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();

            try
            {
                string allocatedStatus = FRAllocationStatus.Allocated.GetStringValue();
                string requestedStatus = FRAllocationStatus.Requested.GetStringValue();

                string htmlContent = "";
                int tableId = 0;

                var objFKM202List = db.FKM202.ToList();
                var objFKM119List = db.FKM119.Where(x => x.RefLineId == RefLineId && x.Status == requestedStatus).ToList();

                var objFKM118List = db.FKM118.Where(x => x.RefLineId == RefLineId).OrderBy(x => x.FixtureNo).ToList();//qty loop
                foreach (var objFKM118 in objFKM118List)
                {
                    tableId++;
                    if (objFKM119List != null)
                    {
                        var filteredFKM119List = objFKM119List.Where(x => x.RefId == objFKM118.Id).OrderBy(x => x.SeqNo).ThenBy(x => x.CreatedOn).ToList();
                        htmlContent += GetHtmlStringForReOrder(tableId, objFKM118.FixtureNo, filteredFKM119List, objFKM202List);
                    }
                    else
                    {
                        htmlContent += GetHtmlStringForReOrder(tableId, objFKM118.FixtureNo, null, objFKM202List);
                    }
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = htmlContent;
                objResponseMsg.QtyofFixture = tableId.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        public string GetHtmlStringForReOrder(int tableId, string FixtureNo, List<FKM119> list, List<FKM202> objFKM202List)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("<strong>Fixture No: <span>" + FixtureNo + "</span></strong>");
            sb.Append("<br/><br/>");

            sb.Append("<table id=\"tblReOrder" + tableId + "\" class=\"table table-bordered\">");
            sb.Append("<thead style='background-color:#b1eaef'><tr>");

            sb.Append("<th class=\"hide\">Id</th><th class=\"min-phone-l\">Project</th>" +
            "<th class=\"min-phone-l\">Kit Number</th>" +
            "<th class=\"min-phone-l\">Kit Name</th>" +
            "<th class=\"min-phone-l\">Status</th>");

            sb.Append("</tr></thead>");

            sb.Append("<tbody id=\"tbody" + tableId + "\">");

            string KitNumber = "", KitName = "", NodeColor = "";
            int i = 1;
            if (list != null && list.Count > 0)
            {
                foreach (var item in list)
                {
                    var objFKM202 = objFKM202List.Where(x => x.NodeId == item.NodeId).FirstOrDefault();
                    if (objFKM202 != null)
                    {
                        KitName = objFKM202.NodeName;
                        KitNumber = objFKM202.FindNo + "-" + objFKM202.NodeKey + "-" + objFKM202.NodeId;
                        NodeColor = objFKM202.NodeColor;
                    }

                    sb.Append("<tr>");

                    sb.Append("<td class=\"hide\"><input type=\"hidden\" class=\"Id\" id=\"" + item.Id + "\" value=\"" + item.Id + "\"/></td>");
                    sb.Append("<td width=\"15%\">" + item.Project + "</td>");
                    sb.Append("<td width=\"43%\">" + GetKitNumberWithColor(KitNumber, NodeColor) + "</td>");
                    sb.Append("<td width=\"30%\">" + KitName + "</td>");
                    sb.Append("<td width=\"12%\">" + item.Status + "</td>");

                    sb.Append("</tr>");

                    i++;
                }
            }
            else
            {
                sb.Append("<tr>");
                sb.Append("<td colspan=\"4\">No record found</td>");
                sb.Append("</tr>");
            }

            sb.Append("</tbody>");

            sb.Append("</table>");

            sb.Append("<br/>");

            return sb.ToString();
        }

        [HttpPost]
        public ActionResult UpdateFixtureSeqNo(List<FKM119> DataList)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();

            try
            {
                var objFKM119List = db.FKM119.ToList();
                objFKM119List.ForEach(i =>
                {
                    if (DataList.Any(x => x.Id == i.Id))
                    {
                        i.SeqNo = DataList.Where(x => x.Id == i.Id).FirstOrDefault().SeqNo;
                    }
                });
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public bool IsAllFixtureAllocatedAndCompleted(int NodeId)
        {
            bool result = true;

            string requestedStatus = FRAllocationStatus.Requested.GetStringValue();
            string FRMaterialDeliveryCompletedStatus = clsImplementationEnum.FRMaterialDeliveryStatus.Completed.GetStringValue();
            string Confirmed = clsImplementationEnum.FullkitAreaStatus.Confirmed.GetStringValue();

            if (db.FKM119.Any(x => x.NodeId == NodeId && x.Status == requestedStatus))
            {
                result = false;
            }
            else
            {
                var count = (from a in db.FKM119
                             join b in db.FKM118 on a.RefId equals b.Id
                             where a.NodeId == NodeId && (b.DeliverStatus != FRMaterialDeliveryCompletedStatus || string.IsNullOrEmpty(b.FullkitArea) || b.FullkitAreaStatus != Confirmed)
                             select a).Count();

                if (count > 0)
                    result = false;
            }
            return result;
        }

        [HttpPost]
        public ActionResult GetFixtureAttachmentFiles(string FolderPath, string Project, int NodeId, int RefLineId, bool includeFromSubfolders = false, string Type = "", string orderby = "")
        {
            string folderPath = "";
            List<SharePointFile> Files = new List<SharePointFile>();

            var objFKM111 = db.FKM111.Where(x => x.LineId == RefLineId).FirstOrDefault();
            if (objFKM111 != null)
            {
                folderPath = "FKM111/" + objFKM111.RefHeaderId + "/" + RefLineId + "/R" + objFKM111.RevNo;
                var tempFiles = (new clsFileUpload("")).GetDocuments(folderPath, includeFromSubfolders, orderby).ToList();
                if (tempFiles.Count > 0)
                {
                    Files.AddRange(tempFiles);
                }

                var objFKM119List = db.FKM119.Where(x => x.Project == Project && x.NodeId == NodeId && x.RefLineId == RefLineId).ToList();
                if (objFKM119List.Count > 0)
                {
                    foreach (var item in objFKM119List)
                    {
                        folderPath = "FKM118/" + objFKM111.RefHeaderId + "/" + RefLineId + "/" + item.RefId;
                        var tempFiles1 = (new clsFileUpload("")).GetDocuments(folderPath, includeFromSubfolders, orderby).ToList();
                        if (tempFiles1.Count > 0)
                        {
                            Files.AddRange(tempFiles1);
                        }
                    }
                }
            }
            return Json(Files.ToArray(), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Pending Request

        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult PendingRequest()
        {
            return View("PendingRequest");
        }

        [HttpPost]
        public ActionResult GetFRPendingRequestDataGridPartial(string tab)
        {
            ViewBag.UserRole = GetUserAccessRights().UserRole;
            return PartialView(tab == "Pending" ? "_LoadPendingFRRequestDataGridPartial" : "_LoadConfirmPendingDataGridPartial");
        }

        public ActionResult LoadFRPendingRequesData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1 and FullkitAreaStatus='" + FullkitAreaStatus.RequestedForKit.GetStringValue() + "'";

                string[] columnName = { "Project", "NodeName", "NodeKey", "FindNo", "FixtureNo", "RequestedBy" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstPam = db.SP_FKMS_GET_KIT_FR_REQUEST_LIST(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from h in lstPam
                           select new[] {
                              "",
                              h.IsParentRecord == 1 ? "<nobr><img onclick='ExpandCollapsChild("+ h.NodeId +", this)' src='"+WebsiteURL+"/Images/details_open.png' />"+Convert.ToString(h.GROUP_NO) + "</nobr>" : "<span class='"+ h.NodeId +" child' ></span>",
                              h.IsParentRecord == 1 ? Convert.ToString(h.Project) : "",
                              h.IsParentRecord == 1 ? Convert.ToString(h.FindNo + "-" + h.NodeKey + "-" + h.NodeId): "",
                              Convert.ToString(h.FixtureNo),
                              h.IsParentRecord == 1 ? Convert.ToString(h.RequestedBy) : "",
                              h.IsParentRecord == 1 ? (h.RequestedOn!=null? Convert.ToDateTime(h.RequestedOn).ToString("dd/MM/yyyy") : "" ) : "",
                              h.IsParentRecord.ToString(),
                              h.NodeId.ToString()
                    }).ToList();
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult LoadConfirmationPandingFRData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? from = param.iDisplayStart + 1;
                int? to = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1 AND FullkitAreaStatus='" + FullkitAreaStatus.PendingConfirmation.GetStringValue() + "'";
                // string whereCondition = "1=1";

                string[] columnName = { "FullKitArea", "FixtureNo", "RequestedBy" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                string orderBy = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    orderBy = " ORDER BY " + sortColumnName + " " + sortDirection + " ";
                }

                var lstPam = db.SP_FKMS_FR_GET_PENDING_CONFIRMATION_LIST(from, to, orderBy, whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from h in lstPam
                           select new[] {
                              "",
                              Convert.ToString(h.ROW_NO),
                              h.FixtureNo,
                              h.FullKitArea,
                              Convert.ToString(h.RequestedBy),
                              h.RequestedOn!=null ? Convert.ToDateTime(h.RequestedOn).ToString("dd/MM/yyyy") : "",
                              Convert.ToString(h.Id),
                              Convert.ToString(h.RefLineId),
                    }).ToList();

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords ?? 0,
                    iTotalRecords = totalRecords ?? 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = orderBy,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SendFixtureToKit(List<FKM119> list)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var objCOM002List = db.COM002.Where(x => x.t_dtyp == 3).ToList();
                foreach (var item in list)
                {
                    var objFKM202 = db.FKM202.Where(x => x.NodeId == item.NodeId).FirstOrDefault();
                    if (objFKM202 != null)
                    {
                        var objFKM119List = db.FKM119.Where(x => x.NodeId == item.NodeId).ToList();
                        objFKM119List.ForEach(x =>
                        {
                            x.FullkitAreaStatus = FullkitAreaStatus.SendToKit.GetStringValue();
                            x.EditedBy = objClsLoginInfo.UserName;
                            x.EditedOn = DateTime.Now;
                        });

                        List<string> RefIdList = objFKM119List.Select(x => x.RefId.ToString()).ToList();
                        var objFKM118List = (from u in db.FKM118
                                             where RefIdList.Contains(u.Id.ToString())
                                             select u).ToList();

                        var objDepartment = objCOM002List.Where(i => i.t_dimx == objFKM202.DeleverTo && i.t_dtyp == 3).FirstOrDefault();
                        string KitLocation = "";
                        if (objDepartment != null)
                            KitLocation = objDepartment != null ? objDepartment.t_dimx + "-" + objDepartment.t_desc : "";

                        objFKM118List.ForEach(x =>
                        {
                            x.FullkitArea = "";
                            x.KitLocation = KitLocation;
                            x.EditedBy = objClsLoginInfo.UserName;
                            x.EditedOn = DateTime.Now;
                        });
                    }
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Fixture send to kit successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ConfirmFixture(List<int> list)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string allocatedStatus = FRAllocationStatus.Allocated.GetStringValue();
                string requestedStatus = FRAllocationStatus.Requested.GetStringValue();
                string completedStatus = FRAllocationStatus.Completed.GetStringValue();

                if (list.Count > 0)
                {
                    db.FKM118.Where(x => list.Contains(x.Id))
                        .ToList()
                        .ForEach(x =>
                        {
                            x.FullkitAreaStatus = FullkitAreaStatus.Confirmed.GetStringValue();
                            x.EditedBy = objClsLoginInfo.UserName;
                            x.EditedOn = DateTime.Now;
                        });

                    List<FKM119> objRequestedList = db.FKM119.Where(u => u.Status == requestedStatus).ToList();
                    foreach (var item in list)
                    {
                        if (!db.FKM119.Any(x => x.RefId == item && x.Status == allocatedStatus))
                        {
                            if (objRequestedList.Any(u => u.RefId == item && u.Status == requestedStatus))
                            {
                                var objTemp = objRequestedList.Where(u => u.RefId == item && u.Status == requestedStatus).OrderBy(x => x.SeqNo)
                                        .ThenBy(x => x.CreatedOn)
                                        .FirstOrDefault();

                                objRequestedList.Where(x => x.Id == objTemp.Id).ToList()
                                     .ForEach(i =>
                                     {
                                         i.SeqNo = null;
                                         i.Status = allocatedStatus;
                                         i.EditedBy = objClsLoginInfo.UserName;
                                         i.EditedOn = DateTime.Now;
                                     });
                            }
                        }
                    }

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Fixture(s) confirmed successfully";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please select atleast one fixture item!";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult LoadFixtureSendToFullKitAreaPartial(string Project, int NodeId, int RefLineId)
        {
            ViewBag.Project = Project;
            ViewBag.NodeId = NodeId;
            ViewBag.RefLineId = RefLineId;

            var list = (from a in db.FKM119
                        join b in db.FKM118 on a.RefId equals b.Id
                        where a.Project == Project && a.NodeId == NodeId && a.RefLineId == RefLineId && string.IsNullOrEmpty(b.FullkitArea)
                        select b).ToList();

            ViewBag.FixtureList = list;

            string location = objClsLoginInfo.Location;
            ViewBag.FullkitAreaList = db.FKM117.Where(x => x.Location == location && x.IsActive).ToList();

            return PartialView("_FixtureSendToFullKitAreaPartial");
        }

        public bool CheckFixtureSendToFullkitArea(string Project, int NodeId, int RefLineId)
        {
            var list = (from a in db.FKM119
                        join b in db.FKM118 on a.RefId equals b.Id
                        where a.Project == Project && a.NodeId == NodeId && a.RefLineId == RefLineId && string.IsNullOrEmpty(b.FullkitArea)
                        select b).ToList();

            if (list.Count() > 0)
                return false;
            else
                return true;
        }

        [HttpPost]
        public ActionResult SendToFullkitArea(string Project, int NodeId, int RefLineId, List<FKM118> fixtureList)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string allocatedStatus = FRAllocationStatus.Allocated.GetStringValue();
                string requestedStatus = FRAllocationStatus.Requested.GetStringValue();
                string completedStatus = FRAllocationStatus.Completed.GetStringValue();

                // List<FKM119> objRequestedList = db.FKM119.Where(u => u.Project == Project && u.NodeId != NodeId && u.Status == requestedStatus).ToList();
                foreach (var item in fixtureList)
                {
                    db.FKM118.Where(x => x.Id == item.Id).ToList().ForEach(i =>
                    {
                        i.FullkitArea = item.FullkitArea;
                        i.FullkitAreaStatus = FullkitAreaStatus.PendingConfirmation.GetStringValue();
                        i.RequestedBy = objClsLoginInfo.UserName;
                        i.RequestedOn = DateTime.Now;
                        i.KitLocation = "";
                        i.EditedBy = objClsLoginInfo.UserName;
                        i.EditedOn = DateTime.Now;
                    });


                    //As per observation id#18462. Once shop user send fixture to fullkit area, fixture will be allocated to other kits.
                    // As per observation id#19114. Once fullkit area manager confirmed fixture, then fixture will be allocated to other kits.

                    //if (objRequestedList.Any(u => u.NodeId != NodeId && u.RefLineId == RefLineId && u.RefId == item.Id && u.Status == requestedStatus))
                    //{
                    //    var objTemp = objRequestedList.Where(u => u.NodeId != NodeId && u.RefLineId == RefLineId && u.RefId == item.Id && u.Status == requestedStatus).OrderBy(x => x.SeqNo).ThenBy(x => x.CreatedOn).FirstOrDefault();
                    //    objRequestedList.Where(x => x.Id == objTemp.Id).ToList().ForEach(i =>
                    //    {
                    //        i.SeqNo = null;
                    //        i.Status = allocatedStatus;
                    //        i.EditedBy = objClsLoginInfo.UserName;
                    //        i.EditedOn = DateTime.Now;
                    //    });
                    //}
                }

                List<int> Ids = fixtureList.Select(x => x.Id).ToList();
                var fixtures = db.FKM118.Where(x => Ids.Contains(x.Id)).Select(x => x.FixtureNo);
                SendNotificationToFKM3ForFR(string.Join(",", fixtures));

                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Fixture send to fullkit area successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        private void SendNotificationToFKM3ForFR(string fixtureNo)
        {
            var listEmployeeForNotification = Manager.GetDepartmentRoleWiseEmployee(objClsLoginInfo.Location, "", UserRoleName.FKM3.GetStringValue());

            if (listEmployeeForNotification != null && listEmployeeForNotification.Count > 0)
            {
                var FKM3Emp = listEmployeeForNotification.Select(i => i.psno).ToList();
                var psno = string.Join(",", FKM3Emp);

                string message = $"Fixture No : {fixtureNo} has been submitted to Fullkit Area for your confirmation";
                (new clsManager()).SendNotification(UserRoleName.FKM3.GetStringValue(), "", "", objClsLoginInfo.Location, message, NotificationType.ActionRequired.GetStringValue(), "/FKM/MaintainFKMS/PendingRequest", psno);
            }
        }

        #endregion

        #region FKMS Summary

        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult FKMSSummary()
        {
            List<string> KitStatusList = clsImplementationEnum.GetListOfDescription<KitStatus>();
            ViewBag.KitStatusList = KitStatusList.AsEnumerable().Select(x => new { CatID = x.ToString(), CatDesc = x.ToString() }).ToList();
            TempData.Clear();
            return View("FKMSSummary");
        }

        [HttpPost]
        public ActionResult GetDeliverToSearchFilter(string term, string Project)
        {
            List<ddlValue> lstDeliverTo = null;
            if (!string.IsNullOrWhiteSpace(Project))
            {
                if (TempData["Project"] != null && TempData["Project"].ToString().Trim().ToLower() != Project.ToString().Trim().ToLower())
                {
                    TempData["DeliverTo"] = null;
                }

                if (TempData["DeliverTo"] == null)
                {
                    lstDeliverTo = (from a in db.FKM202
                                    join b in db.COM002 on a.DeleverTo equals b.t_dimx
                                    where a.Project.Equals(Project, StringComparison.OrdinalIgnoreCase) && !string.IsNullOrEmpty(a.DeleverTo) && b.t_dtyp == 3
                                    select new ddlValue { Value = b.t_dimx, Text = b.t_dimx + " - " + b.t_desc }).Distinct().ToList();
                    TempData["DeliverTo"] = lstDeliverTo;
                }
                else
                {
                    lstDeliverTo = (List<ddlValue>)TempData["DeliverTo"];
                }

                if (!string.IsNullOrWhiteSpace(term))
                {
                    lstDeliverTo = (from u in lstDeliverTo
                                    where u.Text.Contains(term)
                                    select u).Distinct().ToList();
                }

                TempData["Project"] = Project;
                TempData.Keep("Project");
                TempData.Keep("DeliverTo");
            }
            return Json(lstDeliverTo, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetTaskManagerSearchFilter(string term, string Project)
        {
            List<TaskAutoCompleteModel> objTaskManagerList = new List<TaskAutoCompleteModel>();
            if (!string.IsNullOrWhiteSpace(Project))
            {
                var tempList = (from a in db.FKM202
                                join b in db.FKM113 on a.DeleverTo equals b.DeleverTo
                                where a.Project.Equals(Project, StringComparison.OrdinalIgnoreCase) && !string.IsNullOrEmpty(a.DeleverTo) && !string.IsNullOrEmpty(b.TaskManager)
                                select b.TaskManager).Distinct().ToList();

                string str = string.Join(",", tempList);
                if (!string.IsNullOrWhiteSpace(str))
                {
                    List<string> strList = str.Split(',').Distinct().ToList();
                    objTaskManagerList = (from a in strList
                                          select new TaskAutoCompleteModel
                                          {
                                              id = a.ToString(),
                                              text = a.ToString()
                                          }).ToList();
                }
            }
            else
            {
                objTaskManagerList = GetTaskManagerList();
            }

            if (!string.IsNullOrWhiteSpace(term))
            {
                objTaskManagerList = (from u in objTaskManagerList
                                      where u.text.Trim().ToLower().Contains(term.Trim().ToLower()) || u.id.Trim().ToLower().Contains(term.Trim().ToLower())
                                      orderby u.text
                                      select u).Distinct().ToList();
            }
            else
            {
                objTaskManagerList = (from u in objTaskManagerList orderby u.text select u).Distinct().Take(10).ToList();
            }

            var finalList = (from u in objTaskManagerList select new ddlValue { Text = u.text, Value = u.id }).ToList();
            return Json(finalList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetProjectsResult(string term, string Task)
        {
            List<Projects> lstProjects = new List<Projects>();
            lstProjects = FN_GET_PROJECTS_RESULT(Task);

            if (!string.IsNullOrWhiteSpace(term))
            {
                lstProjects = lstProjects.Where(x => x.projectCode.Trim().ToLower().Contains(term.Trim().ToLower()) || x.projectDescription.Trim().ToLower().Contains(term.Trim().ToLower())).OrderBy(x => x.projectCode).ToList();
            }
            else
            {
                lstProjects = lstProjects.OrderBy(x => x.projectCode).Take(10).ToList();
            }
            return Json(lstProjects, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetDeliverTo(string term)
        {
            List<ddlValue> lstDeliverTo = null;
            try
            {
                if (term == null)
                    term = "";

                lstDeliverTo = db.FN_GET_LOCATIONWISE_DEPARTMENT_PDIN("", term, 1).Select(x => new ddlValue { Value = x.t_dimx, Text = x.t_dimx + "-" + x.t_desc, }).ToList();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(lstDeliverTo, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter, AllowAnonymous]
        public ActionResult LoadFKMSSummaryGridDataPartial(string Project)
        {
            ViewBag.Project = Project;
            return PartialView("_LoadFKMSSummaryGridDataPartial");
        }

        [HttpPost]
        public ActionResult LoadFKMSSummaryGridData(JQueryDataTableParamModel param, string Project, string TaskManager, string DeliverTo, string KitStatus)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = string.Empty;
                string PDINCondition = string.Empty;

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                whereCondition = "1=1 ";

                if (!string.IsNullOrWhiteSpace(Project))
                    whereCondition += " and Project='" + Project + "' ";

                if (!string.IsNullOrWhiteSpace(TaskManager))
                    whereCondition += " and ','+TaskManager+',' like '%," + TaskManager + ",%'";

                //if (!string.IsNullOrWhiteSpace(DeliverTo))
                //    whereCondition += " and DeleverTo=" + DeliverTo;

                if (!string.IsNullOrWhiteSpace(KitStatus))
                    whereCondition += " and KitStatus='" + KitStatus + "' ";

                string[] columnName = { "Project", "KitDescription", "KitStatus", "KitDeliverTo", "TASKUNIQUEID", "MaterialType", "SubComponentType", "PartNo", "BOMQty", "PartDescription", "DocumentNo" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var objPDN002 = db.PDN002.Where(x => x.Project == Project).ToList();
                if (objPDN002 != null && objPDN002.Count > 0)
                {
                    int MaxIssueNo = db.PDN002.Where(x => x.Project == Project).Max(x => x.IssueNo);
                    PDINCondition += " and P2.IssueNo = " + MaxIssueNo;
                }

                var lstResult = db.SP_FKMS_GET_SUMMARY_DATA(StartIndex, EndIndex, strSortOrder, whereCondition, Project, PDINCondition).ToList();
                var data = (from u in lstResult
                            select new[]
                            {
                                Convert.ToString(u.RowNo),//0
                                Convert.ToString(u.KitGroupNo),//1
                                Convert.ToString(u.TempNodeGroupNo),//2
                                Convert.ToString(u.TempMaterialTypeGroupNo),//3
                                Convert.ToString(u.MaterialTypeGroupNo),//4
                                u.Project,//5
                                u.TempNodeGroupNo.Value == 1 ? GetKitNumberWithColor(u.KitDescription, u.NodeColor) : "",//6
                                u.TempNodeGroupNo.Value == 1 ? u.KitStatus : "", //7
                                (u.TempNodeGroupNo.Value == 1 && !string.IsNullOrWhiteSpace(u.DeleverTo)) ? u.kitDeliverTo : "",  //8
                                (u.TempNodeGroupNo.Value == 1 && !string.IsNullOrWhiteSpace(u.TASKUNIQUEID)) ? GetTaskDescription(u.Project, u.TASKUNIQUEID) : "", //9
                                u.TempNodeGroupNo.Value == 1 ? u.Project : "",//10
                                u.TempMaterialTypeGroupNo.Value == 1 ? u.MaterialType: "", //11
                                u.SubComponentType,  //12
                                u.MaterialTypeGroupNo == 1 && u.SubComponentType != null && u.SubComponentType != NodeTypes.ASM.GetStringValue() && u.SubComponentType != NodeTypes.TJF.GetStringValue() ? GetPartNoWithLink(u.PartNo,u.SubComponentNodeId) : u.PartNo,//13
                                Convert.ToString(u.BOMQty), //14
                                u.PartDescription,  //15
                                u.MaterialTypeGroupNo == 3 ? (checkdocumentsummery(u.Project, u.DocumentNo, u.DocType)) : "", //16
                                //Helper.GenerateHidden(Convert.ToInt32(u.RowNo), "NodeId", Convert.ToString(u.NodeId))+(u.SubComponentType != null && u.SubComponentType != NodeTypes.ASM.GetStringValue() && u.SubComponentType != NodeTypes.TJF.GetStringValue() ? GenerateActionIcon(Convert.ToInt32(u.RowNo), "task", "Allocation", "fa fa-tasks", "", WebsiteURL + "/FKM/MaintainFKMS/AllocationDetails?NodeId=" + u.SubComponentNodeId,false) : GenerateActionIcon(Convert.ToInt32(u.RowNo), "task", "Allocation", "fa fa-tasks", "", "",true))+(u.TempNodeGroupNo.Value == 1 ? Helper.HTMLActionString(Convert.ToInt32(u.RowNo),"Edit","Edit Record","fa fa-pencil-square-o","EnabledEditLine(" + u.RowNo + ");") + Helper.HTMLActionString(Convert.ToInt32(u.RowNo),"Update","Update Record","fa fa-floppy-o","EditLine(" + u.RowNo + ",'"+u.Project+"');","",false,"display:none") + " " + Helper.HTMLActionString(Convert.ToInt32(u.RowNo),"Cancel","Cancel Edit","fa fa-ban","CancelEditLine(" + Convert.ToInt32(u.RowNo) + "); ","",false,"display:none") : ""),
                                RenderSummaryGridActionButtons(Convert.ToInt32(u.RowNo),u.TempNodeGroupNo.Value,u.MaterialTypeGroupNo,u.Project,u.NodeId,u.SubComponentType,u.SubComponentNodeId), //17
                            }).ToList();

                int TotalCount = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount.Value > 0 ? lstResult.FirstOrDefault().TotalCount.Value : 0);

                TempData["SearchData"] = lstResult;

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = TotalCount,
                    iTotalDisplayRecords = TotalCount,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string RenderSummaryGridActionButtons(int RowNo, long TempNodeGroupNo, int MaterialTypeGroupNo, string Project, int NodeId, string SubComponentType, int? SubComponentNodeId)
        {
            string result = "";
            result = Helper.GenerateHidden(RowNo, "NodeId", Convert.ToString(NodeId));
            var objFKM202 = db.FKM202.Where(x => x.NodeId == NodeId).FirstOrDefault();
            if (objFKM202 != null)
            {
                if (MaterialTypeGroupNo == 1 && SubComponentType != null && SubComponentType != NodeTypes.ASM.GetStringValue() && SubComponentType != NodeTypes.TJF.GetStringValue())
                {
                    result += GenerateActionIcon(RowNo, "task", "Allocation", "fa fa-tasks", "", WebsiteURL + "/FKM/MaintainFKMS/AllocationDetails?NodeId=" + SubComponentNodeId, false);
                }
                else
                {
                    result += GenerateActionIcon(RowNo, "task", "Allocation", "fa fa-tasks", "", "", true);
                }

                if (TempNodeGroupNo == 1 && objFKM202.KitStatus != KitStatus.Release.GetStringValue() && objFKM202.MaterialStatus != KitStatus.Request.GetStringValue() && objFKM202.MaterialStatus != FKMSMaterialDeliveryStatus.MaterialReceived.GetStringValue())
                {
                    result += Helper.HTMLActionString(Convert.ToInt32(RowNo), "Edit", "Edit Record", "fa fa-pencil-square-o", "EnabledEditLine(" + RowNo + ");") +
                              Helper.HTMLActionString(Convert.ToInt32(RowNo), "Update", "Update Record", "fa fa-floppy-o", "EditLine(" + RowNo + ",'" + Project + "');", "", false, "display:none")
                              + " " + Helper.HTMLActionString(Convert.ToInt32(RowNo), "Cancel", "Cancel Edit", "fa fa-ban", "CancelEditLine(" + Convert.ToInt32(RowNo) + "); ", "", false, "display:none");
                }
                else
                {
                    result += Helper.HTMLActionString(Convert.ToInt32(RowNo), "Edit", "Edit Record", "fa fa-pencil-square-o", "EnabledEditLine(" + RowNo + ");", "", true);
                }
            }
            return result;
        }

        public ActionResult LoadFKMSSummaryPartDataPartial(int NodeId)
        {
            SingleWindowPartDetails model = new SingleWindowPartDetails();
            model.NodeId = NodeId;
            var objFKM202 = db.FKM202.Where(x => x.NodeId == NodeId).FirstOrDefault();
            if (objFKM202 != null)
            {
                model.FindNo = objFKM202.FindNo;
                model.NodeKey = objFKM202.NodeKey;
                model.ProductType = objFKM202.ProductType;
                model.NodeName = objFKM202.NodeName;
                model.BOMQty = db.FKM202.Where(x => x.Project == objFKM202.Project && x.FindNo == objFKM202.FindNo && x.NoOfPieces != null).Sum(x => x.NoOfPieces).ToString();

                if (objFKM202.ProductType == NodeTypes.PLT.GetStringValue())
                {
                    model.AllocatedQty = db.FKM108.Where(x => x.NodeId == NodeId && x.Project == objFKM202.Project && x.FindNo == objFKM202.FindNo && x.AllocatedQty != null).Sum(x => x.AllocatedQty).ToString();
                }
                else
                {
                    model.AllocatedQty = db.FKM109.Where(x => x.NodeId == NodeId && x.Project == objFKM202.Project && x.FindNo == objFKM202.FindNo && x.AllocatedQty != null).Sum(x => x.AllocatedQty).ToString();
                }

                if (objFKM202.ParentNodeId != null)
                {
                    var objFKM202Parent = db.FKM202.Where(x => x.NodeId == objFKM202.ParentNodeId).FirstOrDefault();
                    if (objFKM202Parent != null)
                    {
                        var objFKM112List = db.FKM112.Where(x => x.NodeId == objFKM202.ParentNodeId && !string.IsNullOrEmpty(x.SOBKey)).Select(x => x.SOBKey).ToList();
                        if (objFKM112List != null && objFKM112List.Count > 0)
                            model.MaterialRequest = string.Join(", ", objFKM112List);

                        model.MaterialLocation = objFKM202Parent.KitLocation;

                        if (objFKM202Parent.DeleverStatus == FKMSMaterialDeliveryStatus.Delivered.GetStringValue())
                        {
                            model.MaterialIssued = GetKitNumberWithColor("", ColorFlagForFKMS.Green.GetStringValue());
                        }
                        else
                        {
                            if (db.FKM112.Any(i => i.NodeId == objFKM202.ParentNodeId && !string.IsNullOrEmpty(i.SOBKey)))
                            {
                                var lstSOBkeys = db.FKM112.Where(i => i.NodeId == objFKM202.ParentNodeId && !string.IsNullOrEmpty(i.SOBKey)).ToList();
                                foreach (var key in lstSOBkeys)
                                {
                                    if (key.Status != "Issued By Store")
                                    {
                                        key.Status = CheckSOBStatus(key.SOBKey);
                                    }
                                }
                                if (lstSOBkeys.Count == lstSOBkeys.Where(i => i.NodeId == objFKM202.ParentNodeId && !string.IsNullOrEmpty(i.SOBKey) && i.Status == "Issued By Store").Count())
                                {
                                    model.MaterialIssued = GetKitNumberWithColor("", ColorFlagForFKMS.Green.GetStringValue());
                                }
                                else
                                {
                                    model.MaterialIssued = GetKitNumberWithColor("", ColorFlagForFKMS.Red.GetStringValue());
                                }
                            }
                            else
                            {
                                model.MaterialIssued = GetKitNumberWithColor("", ColorFlagForFKMS.Red.GetStringValue());
                            }
                        }
                    }
                }

                string whereCondition = "1=1 and NodeId = " + NodeId;
                var lstResult = db.SP_FKMS_GET_SINGLE_WINDOW_PART_DETAILS(objFKM202.Project, objFKM202.FindNo, objFKM202.NodeKey, 1, Int32.MaxValue, "", whereCondition).ToList();
                if (lstResult != null && lstResult.Count > 0)
                {
                    model.PPONumber = string.Join(", ", lstResult.Where(x => x.PPONumber != null && x.PPONumber.Trim() != "").Select(x => x.PPONumber).Distinct().ToList());
                    model.PONumber = string.Join(", ", lstResult.Where(x => x.PONumber != null && x.PONumber.Trim() != "").Select(x => x.PONumber).Distinct().ToList());
                    model.PPOQty = lstResult.Where(x => x.PPOQty != null).Sum(x => x.PPOQty).ToString();
                    model.POOrderQty = lstResult.Where(x => x.POOrderQty != null).Sum(x => x.POOrderQty).ToString();

                    if (objFKM202.ProductType == NodeTypes.PLT.GetStringValue())
                    {
                        model.PCRNumber = string.Join(", ", lstResult.Where(x => x.PCRNumber != null && x.PCRNumber.Trim() != "").Select(x => x.PCRNumber).Distinct().ToList());
                        model.PCLStatus = string.Join(", ", lstResult.Where(x => x.PCLStatus != null && x.PCLStatus.Trim() != "").Select(x => x.PCLStatus).Distinct().ToList());

                        foreach (var item in lstResult)
                        {
                            if (!string.IsNullOrWhiteSpace(item.PCLNumber) && !model.PCLNumber.Contains(item.PCLNumber))
                            {
                                if (model.PCLNumber.Length == 0)
                                {
                                    model.PCLNumber = item.PCLNumber + " - " + item.PCLQty;
                                }
                                else
                                {
                                    model.PCLNumber += "<br/>" + item.PCLNumber + " - " + item.PCLQty;
                                }
                            }
                        }
                    }

                    string OrderNoList = string.Join("','", lstResult.Where(x => x.PONumber != null && x.PONumber.Trim() != "").Select(x => x.PONumber.Replace("'", "''")).Distinct().ToList());
                    string PONoList = string.Join(",", lstResult.Where(x => x.PONo != null).Select(x => x.PONo).Distinct().ToList());
                    if (!string.IsNullOrWhiteSpace(OrderNoList) && !string.IsNullOrWhiteSpace(PONoList))
                    {
                        string query1 = "select t_rcno as MaterialReceipt,t_qapr as IMRClearedQty from " + LNLinkedServer + ".dbo.twhinh312175 where t_orno in ('" + OrderNoList + "') and t_pono in (" + PONoList + ")";//t_pono
                        List<SingleWindowIMRData> objIMRList = db.Database.SqlQuery<SingleWindowIMRData>(query1).ToList();
                        if (objIMRList != null && objIMRList.Count > 0)
                        {
                            model.MaterialReceipt = string.Join(", ", objIMRList.Where(x => x.MaterialReceipt != null && x.MaterialReceipt.Trim() != "").Select(x => x.MaterialReceipt).Distinct().ToList());
                            model.IMRClearedQty = objIMRList.Sum(x => x.IMRClearedQty).ToString();
                        }
                    }
                }

                if (string.IsNullOrWhiteSpace(model.AllocatedQty)) { model.AllocatedQty = "0"; }
                if (string.IsNullOrWhiteSpace(model.PPOQty)) { model.PPOQty = "0"; }
                if (string.IsNullOrWhiteSpace(model.POOrderQty)) { model.POOrderQty = "0"; }
                if (string.IsNullOrWhiteSpace(model.IMRClearedQty)) { model.IMRClearedQty = "0"; }

                if (objFKM202.ProductType == NodeTypes.PLT.GetStringValue())
                    return PartialView("_LoadFKMSSummaryPLTDataPartial", model);
            }
            return PartialView("_LoadFKMSSummaryPartDataPartial", model);
        }

        public string GetPartNoWithLink(string PartNo, int? NodeId)
        {
            string onClickEvent = "onclick='ShowNodeDetails(" + NodeId + "," + PartNo + ")'";
            string customestyle = "style='cursor:pointer;color:blue;text-decoration:underline'";

            PartNo = "<span title='View Part Details' " + onClickEvent + " " + customestyle + ">" + PartNo + "</span>";
            return PartNo;
        }

        [HttpPost]
        public JsonResult GetInlineEditableRow(int id, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();

                if (TempData["SearchData"] != null)
                {
                    var lstResult = (List<SP_FKMS_GET_SUMMARY_DATA_Result>)TempData["SearchData"];
                    var linedata = lstResult.Where(x => x.RowNo == id).ToList();

                    data = (from u in linedata
                            select new[] {
                                SRE_CMD.Avoid.GetStringValue(),//0
                                SRE_CMD.Avoid.GetStringValue(),//1
                                SRE_CMD.Avoid.GetStringValue(),//2
                                SRE_CMD.Avoid.GetStringValue(),//3
                                SRE_CMD.Avoid.GetStringValue(),//4
                                SRE_CMD.Avoid.GetStringValue(),//5
                                SRE_CMD.Avoid.GetStringValue(),//6
                                SRE_CMD.Avoid.GetStringValue(), //7
                                isReadOnly ? (u.TempNodeGroupNo.Value == 1 ? u.kitDeliverTo : "") : Helper.HTMLAutoComplete(id,"txtDeliverTo",u.kitDeliverTo,"",  false,"","DeliverTo",false) +""+Helper.GenerateHidden(id, "DeliverTo", u.DeleverTo),  //8
                                SRE_CMD.Avoid.GetStringValue(), //9
                                SRE_CMD.Avoid.GetStringValue(), //10
                                u.TempMaterialTypeGroupNo.Value == 1 ? u.MaterialType: "",//11
                                u.SubComponentType,//12
                                u.PartNo,//13
                                Convert.ToString(u.BOMQty),//14
                                u.PartDescription,//15
                                SRE_CMD.Avoid.GetStringValue(), //16
                                //Helper.GenerateHidden(Convert.ToInt32(u.RowNo), "NodeId", Convert.ToString(u.NodeId))+(u.SubComponentType != null && u.SubComponentType != NodeTypes.ASM.GetStringValue() && u.SubComponentType != NodeTypes.TJF.GetStringValue() ? GenerateActionIcon(Convert.ToInt32(u.RowNo), "task", "Allocation", "fa fa-tasks", "", WebsiteURL + "/FKM/MaintainFKMS/AllocationDetails?NodeId=" + u.SubComponentNodeId,false) : GenerateActionIcon(Convert.ToInt32(u.RowNo), "task", "Allocation", "fa fa-tasks", "", "",true))+(u.TempNodeGroupNo.Value == 1 ? Helper.HTMLActionString(Convert.ToInt32(u.RowNo),"Edit","Edit Record","fa fa-pencil-square-o","EnabledEditLine(" + u.RowNo + ");") + Helper.HTMLActionString(Convert.ToInt32(u.RowNo),"Update","Update Record","fa fa-floppy-o","EditLine(" + u.RowNo + ",'"+u.Project+"');","",false,"display:none") + " " + Helper.HTMLActionString(Convert.ToInt32(u.RowNo),"Cancel","Cancel Edit","fa fa-ban","CancelEditLine(" + Convert.ToInt32(u.RowNo) + "); ","",false,"display:none") : ""),
                                RenderSummaryGridActionButtons(Convert.ToInt32(u.RowNo),u.TempNodeGroupNo.Value,u.MaterialTypeGroupNo,u.Project,u.NodeId,u.SubComponentType,u.SubComponentNodeId),
                        }).ToList();

                    TempData.Keep("SearchData");
                }

                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveSummaryData(FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (fc != null)
                {
                    int RowNo = Convert.ToInt32(fc["RowNo"].ToString());
                    string WhereCondition = Convert.ToString(fc["WhereCondition"]);
                    string SortCondition = Convert.ToString(fc["SortCondition"]);
                    string Project = Convert.ToString(fc["Project"]);
                    string PartDescription = fc["PartDescription" + RowNo];

                    if (TempData["SearchData"] != null)
                    {
                        var lstResult = (List<SP_FKMS_GET_SUMMARY_DATA_Result>)TempData["SearchData"];
                        var linedata = lstResult.Where(x => x.RowNo == RowNo).ToList();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();

                        TempData.Keep("SearchData");
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        public string GetTaskDescription(string Project, string TASKUIDs)
        {
            List<ddlValue> objTaskList = new List<ddlValue>();
            string result = "";
            if (!string.IsNullOrWhiteSpace(TASKUIDs))
            {
                //if (TempData["TaskManager"] == null)
                //{
                //    var taskList = (from u in db.FKM202
                //                    where u.Project.Equals(Project, StringComparison.OrdinalIgnoreCase) && !string.IsNullOrEmpty(u.TASKUNIQUEID)
                //                    select u.TASKUNIQUEID).Distinct().ToList();

                //    var strTaskIds = string.Join(",", taskList);

                //    objTaskList = GetLNConcertoTaskList(Project, strTaskIds);
                //    TempData["TaskManager"] = objTaskList;
                //}
                //else
                //{
                //    objTaskList = (List<ddlValue>)TempData["TaskManager"];
                //}

                //List<string> list = TASKUIDs.Split(',').ToList();
                //var finalList = (from u in objTaskList
                //                 where list.Contains(u.Value)
                //                 select u).Distinct().ToList();
                var finalList = GetLNConcertoTaskList(Project, TASKUIDs);
                result = string.Join(", ", finalList.Select(x => x.Value + " - " + x.Text).ToList());
                //TempData.Keep("TaskManager");
            }
            return result.Trim();
        }

        [HttpPost]
        public List<ddlValue> GetLNConcertoTaskList(string Projects, string TaskIds)
        {
            SqlConnection objConn = new SqlConnection(Convert.ToString(ConfigurationManager.ConnectionStrings["LNConcerto"]));
            List<ddlValue> objTaskList = new List<ddlValue>();
            if (!string.IsNullOrEmpty(Projects))
            {
                try
                {
                    objConn.Open();
                    SqlCommand objCmd = new SqlCommand();
                    objCmd.Connection = objConn;
                    objCmd.CommandType = CommandType.Text;
                    SqlDataAdapter dataAdapt = new SqlDataAdapter();
                    DataTable dataTable = new DataTable();
                    objCmd.CommandText = "select DISTINCT a.TASKUNIQUEID, a.NAME as [TaskName] from PROJ_TASK a with(nolock) " +
                               "join PROJECT b with(nolock) on a.PROJECTNAME = b.PROJECTNAME where a.PROJECTNAME IN ('" + Projects + "') " +
                               " AND a.TASKUNIQUEID in (" + TaskIds + ") AND a.SUMMARY <> 1 AND a.TASK_TYPE = 0 order by a.TASKUNIQUEID";
                    dataAdapt.SelectCommand = objCmd;
                    dataAdapt.Fill(dataTable);
                    if (dataTable != null && dataTable.Rows.Count > 0)
                    {
                        objTaskList = (from DataRow row in dataTable.Rows
                                       select new ddlValue
                                       {
                                           Text = Convert.ToString(row["TaskName"]),
                                           Value = Convert.ToString(row["TASKUNIQUEID"])
                                       }).ToList();
                    }
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                }
                finally
                {
                    if (objConn.State == ConnectionState.Open)
                        objConn.Close();
                }
            }
            return objTaskList;
        }

        [HttpPost]
        public List<ddlValue> GetlstLNConcertoTask(string TaskIds)
        {
            SqlConnection objConn = new SqlConnection(Convert.ToString(ConfigurationManager.ConnectionStrings["LNConcerto"]));
            List<ddlValue> objTaskList = new List<ddlValue>();

            try
            {
                objConn.Open();
                SqlCommand objCmd = new SqlCommand();
                objCmd.Connection = objConn;
                objCmd.CommandType = CommandType.Text;
                SqlDataAdapter dataAdapt = new SqlDataAdapter();
                DataTable dataTable = new DataTable();
                objCmd.CommandText = "select distinct a.TASKUNIQUEID, a.NAME as [TaskName] from PROJ_TASK a with(nolock) " +
                           "join PROJECT b with(nolock) on a.PROJECTNAME = b.PROJECTNAME where " +
                           "a.TASKUNIQUEID in (" + TaskIds + ") AND a.SUMMARY <> 1 AND a.TASK_TYPE = 0 order by a.TASKUNIQUEID";
                dataAdapt.SelectCommand = objCmd;
                dataAdapt.Fill(dataTable);
                if (dataTable != null && dataTable.Rows.Count > 0)
                {
                    objTaskList = (from DataRow row in dataTable.Rows
                                   select new ddlValue
                                   {
                                       Text = Convert.ToString(row["TaskName"]),
                                       Value = Convert.ToString(row["TASKUNIQUEID"])
                                   }).ToList();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            finally
            {
                if (objConn.State == ConnectionState.Open)
                    objConn.Close();
            }
            return objTaskList;
        }

        #endregion

        #region FKMS Summary SCR Details

        public ActionResult LoadFKMSSummarySCRDataPartial(int NodeId)
        {
            ViewBag.NodeId = NodeId;
            return PartialView("_LoadFKMSSummarySCRDataPartial");
        }

        public ActionResult LoadFKMSSummarySCRData(JQueryDataTableParamModel param, int NodeId)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;

                string whereCondition = " 1=1 and NodeId = " + NodeId;
                var objFKM202 = db.FKM202.Where(x => x.NodeId == NodeId).FirstOrDefault();
                if (objFKM202 != null)
                {
                    whereCondition += " and Project = '" + objFKM202.Project + "'";
                }

                string[] columnName = { "FindNo", "SCRNo", "SCRSendingLine", "SCRReceivableLine", "SCRPONo", "MaterialShiftedToVendor", "ReceiptofMaterial" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                else
                {
                    strSortOrder = " Order By SCRNo asc ";
                }

                var lstLines = db.SP_FKMS_GET_SCR_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from u in lstLines
                           select new[] {
                                        Convert.ToString(u.Id),
                                        u.FindNo,
                                        u.SCRNo,
                                        //u.SCRRevNo,
                                        u.SCRReceivableLine,
                                        u.SCRSendingLine,
                                        u.SCRPONo,
                                        //u.SCRPORevNo,
                                        u.MaterialShiftedToVendor,
                                        u.ReceiptofMaterial,
                                        Helper.HTMLActionString(u.Id,"EditSCR","Edit Record","fa fa-pencil-square-o","EnabledEditSCR(" + u.Id + ");") + Helper.HTMLActionString(u.Id,"UpdateSCR","Update Record","fa fa-floppy-o","EditSCR(" + u.Id + ");","",false,"display:none") + " " + Helper.HTMLActionString(u.Id,"CancelSCR","Cancel Edit","fa fa-ban","CancelEditSCR(" + u.Id + "); ","",false,"display:none") + " " + Helper.HTMLActionString(u.Id,"DeleteSCR","Delete Record","fa fa-trash-o","DeleteSCR("+ u.Id +");"),
                          }).ToList();

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""

                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetSCRNoData(string term)
        {
            string WhereCondition = " where t_stat = 6";
            if (!string.IsNullOrWhiteSpace(term))
            {
                WhereCondition += " and UPPER(ISNULL(t_scrn,'''')) like '%" + term.ToUpper() + "%'";
            }

            var objSCRNo = db.SP_FKMS_GET_SINGLE_WINDOW_SCR_DATA_FROM_LN(term, "", WhereCondition).ToList();
            var lstPOLines = (from a in objSCRNo
                              select new { Value = a.SCRNo, Text = a.SCRNo }).Distinct().ToList();
            return Json(lstPOLines, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSendingLineData(string term, string SCRNo)
        {
            string WhereCondition = " where t_scrn = '" + SCRNo + "' and t_ipos is not null and t_ipos <>'' ";
            if (!string.IsNullOrWhiteSpace(term))
            {
                WhereCondition += " and UPPER(ISNULL(t_ipos,'''')) like '%" + term.ToUpper() + "%'";
            }

            var objSCRNo = db.SP_FKMS_GET_SINGLE_WINDOW_SCR_DATA_FROM_LN(term, "get_sending_line", WhereCondition).ToList();
            var lstPOLines = (from a in objSCRNo
                              select new { Value = a.Seq, Text = a.Pos }).Distinct().ToList();
            return Json(lstPOLines, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetReceivableLineData(string term, string SCRNo)
        {
            string WhereCondition = " where t_scrn = '" + SCRNo + "' and t_rpos is not null and t_rpos <>'' ";
            if (!string.IsNullOrWhiteSpace(term))
            {
                WhereCondition += " and UPPER(ISNULL(t_rpos,'''')) like '%" + term.ToUpper() + "%'";
            }

            var objSCRNo = db.SP_FKMS_GET_SINGLE_WINDOW_SCR_DATA_FROM_LN(term, "get_receivable_line", WhereCondition).ToList();
            var lstPOLines = (from a in objSCRNo
                              select new { Value = a.Seq, Text = a.Pos }).Distinct().ToList();
            return Json(lstPOLines, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSCRPurchaseOrderData(string term, string SCRNo, string ReceivablePos)
        {
            string WhereCondition = " where t_scrn = '" + SCRNo + "' and t_rpos='" + ReceivablePos + "' and t_orno is not null and t_orno <>'' ";
            if (!string.IsNullOrWhiteSpace(term))
            {
                WhereCondition += " and UPPER(ISNULL(t_orno,'''')) like '%" + term.ToUpper() + "%'";
            }

            var objSCRNo = db.SP_FKMS_GET_SINGLE_WINDOW_SCR_DATA_FROM_LN(term, "get_po", WhereCondition).ToList();
            var lstPOLines = (from a in objSCRNo
                              select new { Value = a.OrderNo, Text = a.OrderNo }).Distinct().ToList();
            return Json(lstPOLines, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetInlineEditableRowSCR(int id, string FindNo, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();
                var result = new List<string>();

                if (id == 0)
                {
                    int newRecordId = 0;
                    var newRecord = new[] {
                                        SRE_CMD.Avoid.GetStringValue(),
                                        FindNo,
                                        Helper.HTMLAutoComplete(newRecordId,"txtSCRNo","","",  false,"","SCRNo",false) +""+Helper.GenerateHidden(newRecordId, "SCRNo",""),
                                        //Helper.GenerateNumericTextbox(newRecordId, "txtSCRRevNo", "", "",true, "", "100"),                                        
                                        Helper.HTMLAutoComplete(newRecordId,"txtSCRReceivableLine","","",  false,"","SCRReceivableLine",false) +""+Helper.GenerateHidden(newRecordId, "SCRReceivableLine", ""),
                                        Helper.HTMLAutoComplete(newRecordId,"txtSCRSendingLine","","",  false,"","SCRSendingLine",false) +""+Helper.GenerateHidden(newRecordId, "SCRSendingLine", ""),
                                        Helper.HTMLAutoComplete(newRecordId,"txtSCRPONo","","",  false,"","SCRPONo",false) +""+Helper.GenerateHidden(newRecordId, "SCRPONo", ""),                        
                                        //Helper.GenerateNumericTextbox(newRecordId, "txtSCRPORevNo", "", "",false, "", "100"),                                        
                                        Helper.GenerateLabelFor(newRecordId,"txtMaterialShiftedToVendor","","",""),
                                        Helper.GenerateLabelFor(newRecordId,"txtReceiptofMaterial","","",""),
                                        Helper.HTMLActionString(newRecordId,"AddSCR","Add New Record","fa fa-plus","SaveNewRecord();"),
                    };
                    data.Add(newRecord);
                }
                else
                {
                    var lstLines = db.SP_FKMS_GET_SCR_DATA(1, 0, "", "Id = " + id).Take(1).ToList();

                    data = (from u in lstLines
                            select new[] {
                                    SRE_CMD.Avoid.GetStringValue(),
                                    u.FindNo,
                                    isReadOnly ? u.SCRNo : Helper.HTMLAutoComplete(u.Id, "txtSCRNo",u.SCRNo,"", false,"","SCRNo",false)+""+Helper.GenerateHidden(u.Id, "SCRNo",Convert.ToString(u.SCRNo)),
                                    //isReadOnly ? u.SCRRevNo : Helper.GenerateNumericTextbox(u.Id, "txtSCRRevNo", u.SCRRevNo, "",true, "", "100"),
                                    isReadOnly ? u.SCRReceivableLine : Helper.HTMLAutoComplete(u.Id, "txtSCRReceivableLine",u.SCRReceivableLine,"", false,"","SCRReceivableLine",false)+""+Helper.GenerateHidden(u.Id, "SCRReceivableLine",Convert.ToString(u.SCRReceivableLineSeq)),
                                    isReadOnly ? u.SCRSendingLine : Helper.HTMLAutoComplete(u.Id, "txtSCRSendingLine",u.SCRSendingLine,"", false,"","SCRSendingLine",false)+""+Helper.GenerateHidden(u.Id, "SCRSendingLine",Convert.ToString(u.SCRSendingLineSeq)),
                                    isReadOnly ? u.SCRPONo : Helper.HTMLAutoComplete(u.Id, "txtSCRPONo",u.SCRPONo,"", false,"","SCRPONo",false)+""+Helper.GenerateHidden(u.Id, "SCRPONo",Convert.ToString(u.SCRPONo)),
                                    //isReadOnly ? u.SCRPORevNo : Helper.GenerateNumericTextbox(u.Id, "txtSCRPORevNo", u.SCRPORevNo, "",false, "", "100"),
                                    Helper.GenerateLabelFor(u.Id,"txtMaterialShiftedToVendor",u.MaterialShiftedToVendor,"",""),
                                    Helper.GenerateLabelFor(u.Id,"txtReceiptofMaterial",u.ReceiptofMaterial,"",""),
                                    Helper.HTMLActionString(u.Id,"EditSCR","Edit Record","fa fa-pencil-square-o","EnabledEditSCR(" + u.Id + ");") + Helper.HTMLActionString(u.Id,"UpdateSCR","Update Record","fa fa-floppy-o","EditSCR(" + u.Id + ");","",false,"display:none") + " " + Helper.HTMLActionString(u.Id,"CancelSCR","Cancel Edit","fa fa-ban","CancelEditSCR(" + u.Id + "); ","",false,"display:none") + " " + Helper.HTMLActionString(u.Id,"DeleteSCR","Delete Record","fa fa-trash-o","DeleteSCR("+ u.Id +");"),
                        }).ToList();
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveSCR(FormCollection fc, int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (fc != null)
                {
                    int NodeId = Convert.ToInt32(fc["NodeId"]);

                    string Project = string.Empty;
                    var objFKM202 = db.FKM202.Where(x => x.NodeId == NodeId).FirstOrDefault();
                    if (objFKM202 != null)
                        Project = objFKM202.Project;

                    string FindNo = fc["FindNo"].ToString();

                    string SCRNo = fc["txtSCRNo" + Id].ToString();
                    //string SCRRevNo = fc["txtSCRRevNo" + Id].ToString();
                    string SCRSendingLine = fc["txtSCRSendingLine" + Id].ToString();
                    string SCRSendingLineSeq = fc["SCRSendingLine" + Id].ToString();

                    string SCRReceivableLine = fc["txtSCRReceivableLine" + Id].ToString();
                    string SCRReceivableLineSeq = fc["SCRReceivableLine" + Id].ToString();

                    string SCRPONo = fc["txtSCRPONo" + Id].ToString();
                    //string SCRPORevNo = fc["txtSCRPORevNo" + Id].ToString();
                    string MaterialShiftedToVendor = fc["txtMaterialShiftedToVendor" + Id].ToString();
                    string ReceiptofMaterial = fc["txtReceiptofMaterial" + Id].ToString();

                    FKM126 objFKM126 = null;
                    if (Id > 0)
                    {
                        if (!db.FKM126.Any(x => x.SCRNo == SCRNo && x.NodeId == NodeId && x.Id != Id))
                        {
                            objFKM126 = db.FKM126.Where(x => x.Id == Id).FirstOrDefault();
                            if (objFKM126 != null)
                            {
                                objFKM126.SCRNo = SCRNo;
                                //objFKM126.SCRRevNo = SCRRevNo;
                                objFKM126.SCRSendingLine = SCRSendingLine;
                                objFKM126.SCRSendingLineSeq = SCRSendingLineSeq;
                                objFKM126.SCRReceivableLine = SCRReceivableLine;
                                objFKM126.SCRReceivableLineSeq = SCRReceivableLineSeq;
                                objFKM126.SCRPONo = SCRPONo;
                                //objFKM126.SCRPORevNo = SCRPORevNo;
                                objFKM126.MaterialShiftedToVendor = MaterialShiftedToVendor;
                                objFKM126.ReceiptofMaterial = ReceiptofMaterial;

                                objFKM126.EditedBy = objClsLoginInfo.UserName;
                                objFKM126.EditedOn = DateTime.Now;

                                db.SaveChanges();
                                objResponseMsg.Key = true;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                            }
                            else
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
                            }
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                        }
                    }
                    else
                    {
                        if (!db.FKM126.Any(x => x.SCRNo == SCRNo && x.NodeId == NodeId))
                        {
                            objFKM126 = new FKM126();
                            objFKM126.Project = Project;
                            objFKM126.NodeId = NodeId;
                            objFKM126.FindNo = FindNo;
                            objFKM126.SCRNo = SCRNo;
                            //objFKM126.SCRRevNo = SCRRevNo;
                            objFKM126.SCRSendingLine = SCRSendingLine;
                            objFKM126.SCRSendingLineSeq = SCRSendingLineSeq;
                            objFKM126.SCRReceivableLine = SCRReceivableLine;
                            objFKM126.SCRReceivableLineSeq = SCRReceivableLineSeq;
                            objFKM126.SCRPONo = SCRPONo;
                            //objFKM126.SCRPORevNo = SCRPORevNo;
                            objFKM126.MaterialShiftedToVendor = MaterialShiftedToVendor;
                            objFKM126.ReceiptofMaterial = ReceiptofMaterial;

                            objFKM126.CreatedBy = objClsLoginInfo.UserName;
                            objFKM126.CreatedOn = DateTime.Now;

                            db.FKM126.Add(objFKM126);

                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult DeleteSCR(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                FKM126 objFKM126 = db.FKM126.Where(x => x.Id == Id).FirstOrDefault();
                if (objFKM126 != null)
                {
                    db.FKM126.Remove(objFKM126);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Details not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetMaterialShiftedToVendor(string SCRNo, int NodeId)
        {
            string res = "";
            try
            {
                FKM202 objFKM202 = db.FKM202.Where(x => x.NodeId == NodeId).FirstOrDefault();
                if (objFKM202 != null)
                {
                    string query1 = "select count(*) from " + LNLinkedServer + ".dbo.tltsfc408175 where LTRIM(RTRIM(t_scrn))='" + SCRNo +
                                    "' and LTRIM(RTRIM(t_item))='" + objFKM202.NodeKey + "'";
                    int count = db.Database.SqlQuery<int>(query1).FirstOrDefault();
                    if (count > 0)
                        res = "Yes";
                    else
                        res = "No";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        #endregion

        public void InsertNodesInFKM125(IEMQSEntitiesContext context, FKM202 objFKM202, string ActionPerformFrom)
        {
            FKM125 objFKM125 = new FKM125();
            objFKM125.NodeId = objFKM202.NodeId;
            objFKM125.HeaderId = objFKM202.HeaderId;
            objFKM125.ParentNodeId = objFKM202.ParentNodeId;
            objFKM125.NodeTypeId = objFKM202.NodeTypeId;
            objFKM125.CreatedBy = objFKM202.CreatedBy;
            objFKM125.CreatedOn = objFKM202.CreatedOn;
            objFKM125.EditedBy = objFKM202.EditedBy;
            objFKM125.EditedOn = objFKM202.EditedOn;
            objFKM125.NodeName = objFKM202.NodeName;
            objFKM125.Project = objFKM202.Project;
            objFKM125.FindNo = objFKM202.FindNo;
            objFKM125.PartLevel = objFKM202.PartLevel;
            objFKM125.Description = objFKM202.Description;
            objFKM125.ItemGroup = objFKM202.ItemGroup;
            objFKM125.RevisionNo = objFKM202.RevisionNo;
            objFKM125.MaterialSpecification = objFKM202.MaterialSpecification;
            objFKM125.MaterialDescription = objFKM202.MaterialDescription;
            objFKM125.MaterialCode = objFKM202.MaterialCode;
            objFKM125.ProductType = objFKM202.ProductType;
            objFKM125.Length = objFKM202.Length;
            objFKM125.Width = objFKM202.Width;
            objFKM125.NoOfPieces = objFKM202.NoOfPieces;
            objFKM125.Weight = objFKM202.Weight;
            objFKM125.Thickness = objFKM202.Thickness;
            objFKM125.UOM = objFKM202.UOM;
            objFKM125.RequiredDate = objFKM202.RequiredDate;
            objFKM125.NodeKey = objFKM202.NodeKey;
            objFKM125.IsInsertedInPLM = objFKM202.IsInsertedInPLM;
            objFKM125.PLMError = objFKM202.PLMError;
            objFKM125.DeleverTo = objFKM202.DeleverTo;
            objFKM125.DeleverStatus = objFKM202.DeleverStatus;
            objFKM125.FindNo_Init = objFKM202.FindNo_Init;
            objFKM125.NodeKey_Init = objFKM202.NodeKey_Init;
            objFKM125.NoOfPieces_Init = objFKM202.NoOfPieces_Init;
            objFKM125.ParentNodeId_Init = objFKM202.ParentNodeId_Init;
            objFKM125.TASKUNIQUEID = objFKM202.TASKUNIQUEID;
            objFKM125.KitStatus = objFKM202.KitStatus;
            objFKM125.MaterialStatus = objFKM202.MaterialStatus;
            objFKM125.PlannerStructure = objFKM202.PlannerStructure;
            objFKM125.KitLocation = objFKM202.KitLocation;
            objFKM125.NodeColor = objFKM202.NodeColor;
            objFKM125.IsPWHT = objFKM202.IsPWHT;
            objFKM125.FullkitArea = objFKM202.FullkitArea;
            objFKM125.FullkitAreaAccepted = objFKM202.FullkitAreaAccepted;
            objFKM125.Element = objFKM202.Element;
            objFKM125.RefCopyNodeId = objFKM202.RefCopyNodeId;
            objFKM125.ActionPerformOn = DateTime.Now;
            objFKM125.ActionPerformFrom = ActionPerformFrom;
            context.FKM125.Add(objFKM125);
        }

        public JsonResult ExportSeamWiseStageList(string whereCondition, string strSortOrder, int NodeId, string Project)
        {
            List<SeamPartWiseStageListEnt> listSeamWiseStageSeamListEnt = new List<SeamPartWiseStageListEnt>();
            try
            {
                var lstPam = db.SP_FKMS_GET_SEAM_AND_STAGE_LIST(0, 0, strSortOrder, whereCondition, Project, NodeId.ToString()).ToList();
                foreach (var SeamNo in lstPam.Select(i => i.SeamNo).Distinct())
                {
                    List<SeamPartWiseStageStageListEnt> listSeamPartWiseStageStageListEnt = new List<SeamPartWiseStageStageListEnt>();
                    foreach (var stages in lstPam.Where(i => i.SeamNo == SeamNo))
                    {
                        listSeamPartWiseStageStageListEnt.Add(new SeamPartWiseStageStageListEnt
                        {
                            InspectionStatus = stages.InspectionStatus,
                            StageCode = stages.StageCode,
                            StageDesc = stages.StageDesc,
                        });
                    }
                    listSeamWiseStageSeamListEnt.Add(new SeamPartWiseStageListEnt { SeamNoPartNo = SeamNo, listSeamPartWiseStageStageListEnt = listSeamPartWiseStageStageListEnt });
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(listSeamWiseStageSeamListEnt);
        }

        public JsonResult ExportPartWiseStageList(string whereCondition, string strSortOrder, int NodeId, string Project)
        {
            List<SeamPartWiseStageListEnt> listSeamWiseStageSeamListEnt = new List<SeamPartWiseStageListEnt>();
            try
            {
                var lstPam = db.SP_FKMS_GET_PART_AND_STAGE_LIST(0, 0, strSortOrder, whereCondition, NodeId.ToString()).ToList();
                foreach (var PartNo in lstPam.Select(i => i.PartNo).Distinct())
                {
                    List<SeamPartWiseStageStageListEnt> listSeamPartWiseStageStageListEnt = new List<SeamPartWiseStageStageListEnt>();
                    foreach (var stages in lstPam.Where(i => i.PartNo == PartNo))
                    {
                        listSeamPartWiseStageStageListEnt.Add(new SeamPartWiseStageStageListEnt
                        {
                            InspectionStatus = stages.InspectionStatus,
                            StageCode = stages.StageCode,
                            StageDesc = stages.StageDesc,
                        });
                    }
                    listSeamWiseStageSeamListEnt.Add(new SeamPartWiseStageListEnt { SeamNoPartNo = PartNo, listSeamPartWiseStageStageListEnt = listSeamPartWiseStageStageListEnt });
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(listSeamWiseStageSeamListEnt);
        }

        private int GetNodeTypeInLevel(int NodeTypeId)
        {
            var nodetype = db.FKM203.FirstOrDefault(f => f.Id == NodeTypeId);
            return nodetype != null ? nodetype.NodeLevel : 5;
        }
    }

    #region Used Entities Class

    public class SeamPartWiseStageListEnt
    {
        public SeamPartWiseStageListEnt() { }
        public string SeamNoPartNo { get; set; }
        public List<SeamPartWiseStageStageListEnt> listSeamPartWiseStageStageListEnt { get; set; }
    }

    public class SeamPartWiseStageStageListEnt
    {
        public SeamPartWiseStageStageListEnt() { }
        public string StageCode { get; set; }
        public string StageDesc { get; set; }
        public string InspectionStatus { get; set; }
    }

    public class FRTemp
    {
        public int NodeId { get; set; }
        public int RefLineId { get; set; }
        public int ReqQty { get; set; }
        public string FixtureNo { get; set; }
        public int RefId { get; set; }
    }

    public class allocationservice : planner1sendrequestRequestTypeFKSOB
    {
        public string psno { get; set; }
        public string findno { get; set; }
        public int nodeId { get; set; }
        public string nodeKey { get; set; }
    }
    public class deallocationservice : sfcReturnInventoryRequestTypeFKSOB
    {
        public string psno { get; set; }
        public string findno { get; set; }
        public int nodeId { get; set; }
        public string nodeKey { get; set; }
    }

    public class tltlnt500175
    {
        public string t_cprj { get; set; }
        public string t_item { get; set; }
        public string t_cwar { get; set; }
        public string t_cono { get; set; }
        public double t_qhnd { get; set; }
        public double t_qofr { get; set; }
        public string t_user { get; set; }
        public string t_uuid { get; set; }
    }

    public class TreeviewEnt
    {
        public TreeviewEnt()
        {
            state = new TreeViewStateEnt();
            children = new List<TreeviewEnt>();
            li_attr = new li_attr();
            a_attr = new a_attr();
        }
        public string id { get; set; }
        public string text { get; set; }
        public string icon { get; set; }
        public string type { get; set; }
        public TreeViewStateEnt state { get; set; }
        public List<TreeviewEnt> children { get; set; }
        public li_attr li_attr { get; set; }
        public a_attr a_attr { get; set; }
    }

    public class li_attr
    {
        public string type { get; set; }
        public int Level { get; set; }
        public string asmtype { get; set; }
        public int isplannerasm { get; set; }
        public string scrollid { get; set; }
        public string qty { get; set; }
        public string dstatus { get; set; }
        public string kstatus { get; set; }
        public string mstatus { get; set; }
        public string FullkitArea { get; set; }
        public bool FullkitAreaAccepted { get; set; }
        public bool IsRequested { get; set; }
    }

    public class a_attr
    {
        public a_attr()
        {
            title = string.Empty;
        }
        public string style { get; set; }
        public string title { get; set; }
        public string ondblclick { get; set; }
        public string isinsertedinplm { get; set; }

        public string scrollid { get; set; }
        public string color { get; set; }
    }

    public class TreeViewStateEnt
    {
        public bool opened { get; set; }
        public bool disabled { get; set; }
        public bool selected { get; set; }
    }
    public class projectDetails
    {
        public bool Key { get; set; }
        public string Value { get; set; }
        public string Customer { get; set; }
        public string CustomerDescription { get; set; }
        public string ZeroDate { get; set; }
        public string CDD { get; set; }
        public string ProductType { get; set; }
        public string ImageUrl { get; set; }
        public int RevNo { get; set; }
        public List<destiProjectDetails> destiProjectDetails { get; set; }
        public List<sourceProjectDetails> sourceProjectDetails { get; set; }
    }

    public class destiProjectDetails
    {
        public string nodeProject { get; set; }
        public string nodeName { get; set; }
        public string nodeKey { get; set; }
        public double nodeQty { get; set; }
        public int nodeFindno { get; set; }
    }

    public class sourceProjectDetails
    {
        public int nodeId { get; set; }
        public string nodeName { get; set; }
        public bool IsPlannerStructure { get; set; }
    }

    public class tableData
    {
        public string SectionNo { get; set; }
        public string ParentName { get; set; }
        public string Name { get; set; }
        public string Internal { get; set; }
        public string External { get; set; }
        public string Nozzle { get; set; }
        public string AssName { get; set; }
        public string AssQty { get; set; }
        public string SecQty { get; set; }

        public string PWHTsec { get; set; }
        public string PWHTQty { get; set; }
        public string PWHTpages { get; set; }
        public string Sectionlabel { get; set; }
        public string Sectionparentlabel { get; set; }

        public string Initpages { get; set; }
        public string Initlabel { get; set; }
        public string Initparentlabel { get; set; }
        public string Initsection { get; set; }

    }

    public class copyNodeList
    {
        public string sourceNodeId { get; set; }
        public string destiFindNo { get; set; }
    }

    public class copyData
    {
        public string sourceProject { get; set; }
        public string destProject { get; set; }
        public string zeroDate { get; set; }
        public string cdd { get; set; }
        public string customer { get; set; }
    }

    public class allocationDetails
    {
        public string Project { get; set; }
        public string KitNumber { get; set; }
        public int NodeId { get; set; }
    }

    public class momentDND
    {
        public bool Key { get; set; }
        public decimal? Value { get; set; }
        public string ProductType { get; set; }
        public string ParentQty { get; set; }
        public string Qty { get; set; }
    }

    public class KitDetails
    {
        public bool Key { get; set; }
        public int releaseKey { get; set; }
        public string Value { get; set; }
        public int NodeId { get; set; }
        public string KitName { get; set; }
        public string KitNumber { get; set; }
        public string KitDept { get; set; }
        public bool KitIsAssembly { get; set; }
        public string UniqueId { get; set; }
        public string DeliverId { get; set; }
        public string TaskId { get; set; }
        public string DeliverStatus { get; set; }
        public string MaterialStatus { get; set; }
        public bool IsSOBGenerated { get; set; }
        public bool IsReadyForParentKit { get; set; }
        public string DeliverTo { get; set; }
        public bool IsAllowToChangeDeliverToAndDeAllocate { get; set; }
    }

    public class IscheckAsm
    {
        public bool Key { get; set; }
        public string Value { get; set; }
        public bool chktype { get; set; }
        public int NodeId { get; set; }
    }

    public class PLTPartDetail
    {
        public string Project { get; set; }
        public string Key { get; set; }
        public string Findnumber { get; set; }
        public string BomQty { get; set; }
    }

    public class PartDetails
    {
        public string KitNumber { get; set; }
        public string KitName { get; set; }
        public string PartNumber { get; set; }
        public string ProductType { get; set; }
        public string ItemID { get; set; }
        public string ItemName { get; set; }
        public string PPONumber { get; set; }
        public string PONumber { get; set; }
        public string PCRNumber { get; set; }
        public string PCRStatus { get; set; }
        public string PCLNumber { get; set; }
        public string PCLStatus { get; set; }
        public string PlannerStatus { get; set; }
        public string QAStatus { get; set; }
        public string ShopStatus { get; set; }
        public int OutboundKey { get; set; }
        public int OutboundQty { get; set; }
        public int TotalReqQty { get; set; }
    }

    public class FKMSPartEnt
    {
        public string description { get; set; }
        public string isTopPart { get; set; }
        public string itemGroup { get; set; }
        public string owner { get; set; }
        public string partName { get; set; }
        public string partType { get; set; }
        public string policy { get; set; }
        public string relatedProjectName { get; set; }
        //public string findNumber { get; set; } 
        public bool IsInsertedInPLM { get; set; }
        public string error { get; set; }
        public int NodeTypeId { get; set; }
        public bool IsPWHT { get; set; }
    }
    public class FKMSBOMEnt
    {
        public FKMSBOMEnt()
        {
            part = new FKMSPartEnt();
        }
        public string action { get; set; }
        public string childPartName { get; set; }
        public string childPartType { get; set; }
        public string findNumber { get; set; }
        public string length { get; set; }
        public string numberOfPieces { get; set; }
        public string owner { get; set; }
        public string parentPartName { get; set; }
        public string parentPartType { get; set; }
        public string quantity { get; set; }
        public string width { get; set; }
        public string description { get; set; } //Set Extra For used description while save in FKM202
        public bool IsInsertedInPLM { get; set; }
        public string error { get; set; }
        public bool IsPWHT { get; set; }
        public FKMSPartEnt part { get; set; }
    }
    public class SeamAndStageEnt : SP_FKMS_GET_SEAM_AND_STAGE_LIST_Result
    {
        public bool IsParent { get; set; }
        public bool IsClear { get; set; }
    }
    public class PartAndStageEnt : SP_FKMS_GET_PART_AND_STAGE_LIST_Result
    {
        public bool IsParent { get; set; }
        public bool IsClear { get; set; }
    }

    public class PDINDocumentRevNoEnt
    {
        public string Project { get; set; }
        public string DocumentNo { get; set; }
        public int? RevNo { get; set; }
        public string Plan { get; set; }
    }

    #region Merge&Compare&Accept
    public class MergeNodeEnt
    {
        public MergeNodeEnt()
        {

        }
        public int HeaderId { get; set; }
        public int NodeTypeId { get; set; }
        public int Id { get; set; }
        public string NodeKey { get; set; }
        public string NodeFindNo { get; set; }
        public string Qty { get; set; }
        public string QtyDesigner { get; set; }
        public string ParentFindNo { get; set; }
        public string ParentNodeKey { get; set; }
        public string ParentFindNo_Init { get; set; }
        public string ParentNodeKey_Init { get; set; }
        public string NodeName { get; set; }
        public bool IsPlanner { get; set; }
        public bool IsDesigner { get; set; }
        public int NodeId { get; set; }
        public int DesignerNodeId { get; set; }
        public int DesignerParentNodeId { get; set; }
        public int ParentNodeId { get; set; }
        public int? ParentNodeId_Init { get; set; }
        public bool IsCompare { get; set; }
        public string ProductType { get; set; }
        public string Material { get; set; }
        public string UOM { get; set; }
        public string Project { get; set; }
    }
    public class ParentChildNodeEnt
    {
        public ParentChildNodeEnt() { }

        public int? NodeId { get; set; }
        public int? ParentNodeId { get; set; }
        public int? ParentNodeId_Init { get; set; }
        public string FindNo { get; set; }
        public string NodeKey { get; set; }
        public string NodeName { get; set; }
        public string NoOfPieces { get; set; }
        public string ProductType { get; set; }
        public string Material { get; set; }
        public string UOM { get; set; }
        public string ASMPartsColor { get; set; }
        public string NOP { get; set; }
        public string KitStatus { get; set; }
        public string ParentNodeKey_Init { get; set; }
        public string ParentFindNo_Init { get; set; }
        public string ParentNodeKey { get; set; }
        public string ParentFindNo { get; set; }
        public string DeleverTo { get; set; }
        public string TASKUNIQUEID { get; set; }
        public string Project { get; set; }
        public string DeleverStatus { get; set; }
        public int NodeLevel { get; set; }
        public bool isPWHT { get; set; }
    }

    public class ParentList
    {
        public int Id { get; set; }
        public int AssQty { get; set; }
        public int SecQty { get; set; }
        public string AssName { get; set; }
    }

    public class PageParentList
    {
        public int Id { get; set; }
        public int PWHTQty { get; set; }
        public int PWHTpages { get; set; }
        public int PWHTsec { get; set; }
        public string Sectionlabel { get; set; }
        public string Sectionparentlabel { get; set; }
    }

    public class InitParentList
    {
        public int Id { get; set; }
        public int Initpages { get; set; }
        public int Initsection { get; set; }
        public string Initparentlabel { get; set; }
        public string Initlabel { get; set; }
    }

    public class ChildList
    {
        public int Id { get; set; }
        public string ParentName { get; set; }
        public string Name { get; set; }
        public int SectionNo { get; set; }
        public string Internals { get; set; }
        public string Externals { get; set; }
        public string Nozzles { get; set; }
        public int AssQty { get; set; }
    }

    public class copyProject
    {
        public string Project { get; set; }
        public string ProjDesc { get; set; }
    }
    #endregion

    public class CopyNodeDtl
    {
        public int NewNodeId { get; set; }
        public int NodeId { get; set; }
    }

    public class SOBKeyStatusEnt
    {
        public bool IsSOBKeyGenerated { get; set; }
        public bool IsIssuedByStore { get; set; }
        public string SOBKeyStatus { get; set; }
        public string KitStatus { get; set; }
    }

    public class SingleWindowPartDetails
    {
        public SingleWindowPartDetails()
        {
            this.PPONumber = string.Empty;
            this.PONumber = string.Empty;
            this.PCRNumber = string.Empty;
            this.PCLNumber = string.Empty;
            this.PCLStatus = string.Empty;
            this.MaterialRequest = string.Empty;
            this.MaterialLocation = string.Empty;
            this.MaterialReceipt = string.Empty;
            this.IMRClearedQty = "0";
            this.AllocatedQty = "0";
            this.PPOQty = "0";
            this.POOrderQty = "0";
        }

        public int NodeId { get; set; }
        public string NodeName { get; set; }
        public string FindNo { get; set; }
        public string ProductType { get; set; }
        public string NodeKey { get; set; }
        public string BOMQty { get; set; }
        public string PPONumber { get; set; }
        public string PPOQty { get; set; }
        public string PONumber { get; set; }
        public string POOrderQty { get; set; }
        public string PCRNumber { get; set; }
        public string PCRStatus { get; set; }
        public string PCRPosition { get; set; }
        public string PCRRevision { get; set; }
        public string PCRStenum { get; set; }
        public string PCLNumber { get; set; }
        public string PCLStEnum { get; set; }
        public string PCLStatus { get; set; }
        public string MaterialReceipt { get; set; }
        public string IMRClearedQty { get; set; }
        public string AllocatedQty { get; set; }
        public string MaterialRequest { get; set; }
        public string MaterialLocation { get; set; }
        public string MaterialIssued { get; set; }
        public string Type { get; set; }
    }

    public class SingleWindowIMRData
    {
        public string MaterialReceipt { get; set; }
        public double IMRClearedQty { get; set; }
    }

    #endregion
}