﻿using IEMQS.Areas.PDIN.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Collections;
using System.Web.Script.Serialization;
using System.Net;
using System.Net.Mail;
using IEMQS.Areas.Utility.Models;
using IEMQSImplementation.CBL;
using static IEMQSImplementation.clsHelper;
using IEMQSImplementation.Models;

namespace IEMQS.Areas.CBL.Controllers
{
    public class MaintainCBLController : clsBase
    {
        // GET: CBL/MaintainCBL
        CBLEntitiesContext CBLDB = new CBLEntitiesContext();

        public ActionResult Index()
        {
            return View();
        }

        [SessionExpireFilter]
        public ActionResult Maintain()
        {
            var objclsLoginInfo = (clsLoginInfo)System.Web.HttpContext.Current.Session[clsImplementationMessage.Session.LoginInfo];
            if (objclsLoginInfo != null)
            {
                if (objclsLoginInfo.listMenuResult != null)
                {
                    int Cnt = objclsLoginInfo.listMenuResult.Where(x => x.Process == "Maintain CBL").ToList().Count();
                    if (Cnt == 0)
                        return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                }
            }
            ViewBag.IsDisplayOnly = false;
            ViewBag.Role = "Maintain";
            ViewBag.Title = "Maintain CBL";
            return View("Index");
        }
        [SessionExpireFilter]
        public ActionResult Approve()
        {
            var objclsLoginInfo = (clsLoginInfo)System.Web.HttpContext.Current.Session[clsImplementationMessage.Session.LoginInfo];
            if (objclsLoginInfo != null)
            {
                if (objclsLoginInfo.listMenuResult != null)
                {
                    int Cnt = objclsLoginInfo.listMenuResult.Where(x => x.Process == "Approve CBL").ToList().Count();
                    if (Cnt == 0)
                        return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                }
            }
            ViewBag.IsDisplayOnly = true;
            ViewBag.Role = "Approve";
            ViewBag.Title = "Approve CBL";
            return View("Index");
        }
        [SessionExpireFilter]
        public ActionResult Display()
        {
            var objclsLoginInfo = (clsLoginInfo)System.Web.HttpContext.Current.Session[clsImplementationMessage.Session.LoginInfo];
            if (objclsLoginInfo != null)
            {
                if (objclsLoginInfo.listMenuResult != null)
                {
                    int Cnt = objclsLoginInfo.listMenuResult.Where(x => x.Process == "Display CBL").ToList().Count();
                    if (Cnt == 0)
                        return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                }
            }
            ViewBag.IsDisplayOnly = true;
            ViewBag.Role = "Display";
            ViewBag.Title = "Display CBL";
            return View("Index");
        }

        [HttpPost]
        public ActionResult GetGridDataPartial(string status, string role, bool IsDisplayOnly = false)
        {
            ViewBag.IsDisplayOnly = IsDisplayOnly;
            ViewBag.Status = status;
            ViewBag.Role = role;

            return PartialView("_GetGridDataPartial");
        }
        public ActionResult LoadCBLHeaderData(JQueryDataTableParamModel param)
        {
            try
            {

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string status = param.Status;
                string role = param.Roles;

                var IsDisplayOnly = !string.IsNullOrEmpty(Request["IsDisplayOnly"]) ? Convert.ToBoolean(Request["IsDisplayOnly"].ToString()) : false;
                string whereCondition = "1=1 and islatest = 1";

                if (role == "maintain")
                {
                    if (status == "pending")
                    {
                        whereCondition += " and status in ('" + clsImplementationEnum.CBLStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.CBLStatus.Returned.GetStringValue() + "')";
                    }
                    else
                    {
                        whereCondition += " and status in ('" + clsImplementationEnum.CBLStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.CBLStatus.SendToApproval.GetStringValue() + "','" + clsImplementationEnum.CBLStatus.Approved.GetStringValue() + "','" + clsImplementationEnum.CBLStatus.Returned.GetStringValue() + "')";
                    }
                }
                else if (role == "display")
                {
                    whereCondition += " and status in ('" + clsImplementationEnum.CBLStatus.Approved.GetStringValue() + "')";
                    // whereCondition += " and status in ('" + clsImplementationEnum.CBLStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.CBLStatus.SendToApproval.GetStringValue() + "','" + clsImplementationEnum.CBLStatus.Approved.GetStringValue() + "','" + clsImplementationEnum.CBLStatus.Returned.GetStringValue() + "')";

                }
                else if (role == "approve")
                {
                    if (status == "pending")
                    {
                        whereCondition += " and status in ('" + clsImplementationEnum.CBLStatus.SendToApproval.GetStringValue() + "')";
                    }
                    else
                    {
                        whereCondition += " and status in ('" + clsImplementationEnum.CBLStatus.SendToApproval.GetStringValue() + "','" + clsImplementationEnum.CBLStatus.Approved.GetStringValue() + "','" + clsImplementationEnum.CBLStatus.Returned.GetStringValue() + "')";
                    }
                }

                string[] columnName = { "QualityProject", "Project", "ProjectDesc", "BU", "BUDesc", "Location", "LocationDesc", "Status", "RevNo" };

                if (!string.IsNullOrEmpty(param.sSearch))
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                {
                    if (!string.IsNullOrWhiteSpace(param.SearchFilter))
                        whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstPam = CBLDB.SP_CBL_GetHeader_Data1(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from h in lstPam
                           select new[] {
                               Convert.ToString(h.HeaderId),
                               Convert.ToString(h.HeaderId),
                               Convert.ToString(h.QualityProject),
                               Convert.ToString(h.ProjectDesc),
                               Convert.ToString(h.BUDesc),
                               Convert.ToString(h.LocationDesc),
                               Convert.ToString(h.Status),
                               Convert.ToString("R"+h.RevNo),
                               //Convert.ToString(h.CreatedBy),
                              // h.CreatedOn.ToShortDateString(),
                               (role != "display" ?
                               "<center>"+ (Helper.GenerateActionIcon(Convert.ToInt32(h.HeaderId), "View", "View Detail", "fa fa-eye", "", WebsiteURL + "/CBL/MaintainCBL/CBLDetail?id="+h.HeaderId+"&role="+role ,false))
                               +
                               (h.RevNo > 0 ? HTMLHistoryString(Convert.ToInt32(h.HeaderId),h.Status,"History","History","iconspace fa fa-history","GetHistoryDetails(\""+ h.QualityProject.Trim() +"\",\""+ h.Project.Trim() +"\",\""+ h.BU.Trim() +"\",\""+h.Location+"\");") + "" : "<i title='History' style = '' class='disabledicon fa fa-history'></i>")
                               +"<i title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/CBL/MaintainCBL/ShowTimeline?HeaderId=" + Convert.ToInt32(h.HeaderId) + "')  class='iconspace fa fa-clock-o' ></i>"
                               +Helper.GenerateActionIcon(Convert.ToInt32(h.HeaderId), "Print", "Print Detail", "fa fa-print","printReport("+h.HeaderId+")","" ,false)
                               +(role == "maintain"? Helper.GenerateActionIcon(Convert.ToInt32(h.HeaderId), "Delete", "Delete Record", "fa fa-trash-o", "DeleteHeader(" + h.HeaderId + ",'" + status + "')","", IsDeleteDisable(h.Status) ? true : false, false,  IsDeleteDisable(h.Status) ? "pointer-events:none;" : null):"")
                               +"</center>" : Helper.GenerateActionIcon(Convert.ToInt32(h.HeaderId), "Print", "Print Detail", "fa fa-print","printReport("+h.HeaderId+")","" ,false))


                    }).ToList();

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        private bool IsDeleteDisable(string HeaderStatus)
        {
            bool result = true;
            if (HeaderStatus == clsImplementationEnum.CBLStatus.Draft.GetStringValue())
                result = false;
            return result;
        }

        [HttpPost]
        public ActionResult ApproveCBL(string strHeader)
        {
            var objResponseMsg = new ResponceMsgWithObject();
            try
            {
                if (!string.IsNullOrEmpty(strHeader))
                {
                    List<string> approvedCBL = new List<string>();
                    List<string> pendingCBL = new List<string>();
                    int[] headerIds = Array.ConvertAll(strHeader.Split(','), s => int.Parse(s));
                    foreach (int headerId in headerIds)
                    {
                        var CBL001 = CBLDB.CBL001.FirstOrDefault(x => x.HeaderId == headerId);
                        if (ApproveByUser(headerId))
                            approvedCBL.Add(CBL001.QualityProject);
                        else
                            pendingCBL.Add(CBL001.QualityProject);
                    }
                    objResponseMsg.data = new
                    {
                        approvedCBL = (approvedCBL.Count > 0 ? objResponseMsg.data = "CBL " + String.Join(",", approvedCBL) + " Approved successfully" : ""),
                        pendingCBL = (pendingCBL.Count > 0 ? objResponseMsg.data = "Fail to Approve CBL:" + String.Join(",", pendingCBL) + "." : "")
                    };
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "CBL has been sucessfully approved.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please select atleast one record for approve";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        private bool ApproveByUser(int headerId)
        {
            var lineStatus = clsImplementationEnum.CBLStatus.SendToApproval.GetStringValue();
            var objCBL001 = CBLDB.CBL001.FirstOrDefault(i => i.HeaderId == headerId && i.Status == lineStatus);
            if (objCBL001 != null)
            {
                objCBL001.Status = clsImplementationEnum.CBLStatus.Approved.GetStringValue();
                objCBL001.ApprovedBy = objClsLoginInfo.UserName;
                objCBL001.ApprovedOn = DateTime.Now;
                CBLDB.SaveChanges();

                #region Log Entry
                CBL001_Log objLog = new CBL001_Log();
                objLog.HeaderId = objCBL001.HeaderId;
                objLog.QualityProject = objCBL001.QualityProject;
                objLog.Project = objCBL001.Project;
                objLog.BU = objCBL001.BU;
                objLog.Location = objCBL001.Location;
                objLog.RevNo = objCBL001.RevNo;
                objLog.CreatedBy = objCBL001.CreatedBy;
                objLog.CreatedOn = objCBL001.CreatedOn;
                objLog.ApprovedBy = objCBL001.ApprovedBy;
                objLog.ApprovedOn = objCBL001.ApprovedOn;
                objLog.ReturnedBy = objCBL001.ReturnedBy;
                objLog.ReturnedOn = objCBL001.ReturnedOn;
                objLog.Remark = objCBL001.Remark;
                objLog.EditedBy = objCBL001.EditedBy;
                objLog.EditedOn = objCBL001.EditedOn;
                objLog.SubmittedBy = objCBL001.SubmittedBy;
                objLog.SubmittedOn = objCBL001.SubmittedOn;
                objLog.Status = objCBL001.Status;
                CBLDB.CBL001_Log.Add(objLog);
                CBLDB.SaveChanges();

                List<CBL002> ObjLinelist = new List<CBL002>();
                ObjLinelist = CBLDB.CBL002.Where(x => x.HeaderId == objCBL001.HeaderId).ToList();
                foreach (var item in ObjLinelist)
                {
                    CBL002_Log objlineLog = new CBL002_Log();
                    objlineLog.LineId = item.LineId;
                    objlineLog.HeaderId = item.HeaderId;
                    objlineLog.WeldingProcess = item.WeldingProcess;
                    objlineLog.ConsumableType = item.ConsumableType;
                    objlineLog.AWSClass = item.AWSClass;
                    objlineLog.SizeMM = item.SizeMM;
                    objlineLog.BatchNo = item.BatchNo;
                    objlineLog.BrandName = item.BrandName;
                    objlineLog.Manufacturer = item.Manufacturer;
                    objlineLog.CreatedBy = item.CreatedBy;
                    objlineLog.CreatedOn = item.CreatedOn;
                    objlineLog.EditedBy = item.EditedBy;
                    objlineLog.EditedOn = item.EditedOn;
                    CBLDB.CBL002_Log.Add(objlineLog);
                    CBLDB.SaveChanges();
                }
                //Identical Project log
                List<CBL003> ObjIPlist = new List<CBL003>();
                ObjIPlist = CBLDB.CBL003.Where(x => x.HeaderId == objCBL001.HeaderId).ToList();
                foreach (var item in ObjIPlist)
                {
                    CBL003_Log objIPLog = new CBL003_Log();
                    objIPLog.LineId = item.LineId;
                    objIPLog.HeaderId = item.HeaderId;
                    objIPLog.QualityProject = item.QualityProject;
                    objIPLog.IQualityProject = item.IQualityProject;
                    objIPLog.BU = item.BU;
                    objIPLog.Location = item.Location;
                    objIPLog.CreatedBy = item.CreatedBy;
                    objIPLog.CreatedOn = item.CreatedOn;
                    objIPLog.EditedBy = item.EditedBy;
                    objIPLog.EditedOn = item.EditedOn;
                    CBLDB.CBL003_Log.Add(objIPLog);
                    CBLDB.SaveChanges();
                }

                #endregion

                return true;
            }
            return false;
        }

        [HttpPost]
        public ActionResult GetHistoryView(string QualityProject, string Project, string BU, string Location, string Role)
        {
            ViewBag.QualityProject = QualityProject;
            ViewBag.Project = Project;
            ViewBag.Location = Location;
            ViewBag.BU = BU;
            ViewBag.Role = Role;
            return PartialView("_CBLHistoryGrid");
        }

        [HttpPost]
        public JsonResult LoadHeaderHistoryData(JQueryDataTableParamModel param)
        {
            try
            {

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string QualityProject = param.QualityProject;
                string Project = param.Project;
                string BU = param.BU;
                string Location = param.Location;

                string role = param.Roles.ToLower();
                var IsDisplayOnly = !string.IsNullOrEmpty(Request["IsDisplayOnly"]) ? Convert.ToBoolean(Request["IsDisplayOnly"].ToString()) : false;
                string whereCondition = "1=1";

                whereCondition += " and QualityProject = '" + QualityProject + "' and Project ='" + Project + "' and BU ='" + BU + "' and Location = '" + Location + "' and RevNo != (select Max(RevNo) FROM [CBL001] where QualityProject = '" + QualityProject + "' and Project ='" + Project + "' and BU ='" + BU + "' and Location = '" + Location + "')";


                string[] columnName = { "QualityProject", "Project", "BU", "Location", "Status", "RevNo" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstPam = CBLDB.SP_CBL_GETHEADER_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from h in lstPam
                           select new[] {
                               Convert.ToString(h.HeaderId),
                               Convert.ToString(h.QualityProject),
                               Convert.ToString(h.ProjectDesc),
                               Convert.ToString(h.BUDesc),
                               Convert.ToString(h.LocationDesc),
                               Convert.ToString(h.Status),
                               Convert.ToString("R"+h.RevNo),
                                "<center>"+ (Helper.GenerateActionIcon(Convert.ToInt32(h.HeaderId), "View", "View Detail", "fa fa-eye", "", WebsiteURL + "/CBL/MaintainCBL/CBLDetail?id="+h.HeaderId+"&role="+role ,false))
                                +"<i title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/CBL/MaintainCBL/ShowTimeline?HeaderId=" + Convert.ToInt32(h.HeaderId) + "')  class='iconspace fa fa-clock-o' ></i>"
                                +Helper.GenerateActionIcon(Convert.ToInt32(h.HeaderId), "Print", "Print Detail", "fa fa-print","printReport("+h.HeaderId+")","" ,false)
                               +"</center>"
                    }).ToList();
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                });
            }
        }

        public ActionResult ShowTimeline(int HeaderId)
        {
            TimelineViewModel model = new TimelineViewModel();

            if (HeaderId > 0)
            {
                CBL001 objCBL001 = CBLDB.CBL001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                model.CreatedBy = Manager.GetUserNameFromPsNo(objCBL001.CreatedBy);
                model.CreatedOn = objCBL001.CreatedOn;

                model.EditedBy = Manager.GetUserNameFromPsNo(objCBL001.EditedBy);
                model.EditedOn = objCBL001.EditedOn;

                model.SubmittedBy = Manager.GetUserNameFromPsNo(objCBL001.SubmittedBy);
                model.SubmittedOn = objCBL001.SubmittedOn;

                model.ReturnedBy = Manager.GetUserNameFromPsNo(objCBL001.ReturnedBy);
                model.ReturnedOn = objCBL001.ReturnedOn;

                model.ApprovedBy = Manager.GetUserNameFromPsNo(objCBL001.ApprovedBy);
                model.ApprovedOn = objCBL001.ApprovedOn;

            }
            else
            {
                CBL001 objCBL001 = CBLDB.CBL001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objCBL001.CreatedBy != null ? Manager.GetUserNameFromPsNo(objCBL001.CreatedBy) : null;
                model.CreatedOn = objCBL001.CreatedOn;
                model.EditedBy = objCBL001.EditedBy != null ? Manager.GetUserNameFromPsNo(objCBL001.EditedBy) : null;
                model.EditedOn = objCBL001.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress2.cshtml", model);
        }

        public ActionResult CBLDetail(int? id, string role)
        {
            CBL001 objCBL001 = new CBL001();
            ViewBag.HeaderId = id;
            ViewBag.Role = role;
            ViewBag.IsDisplayOnly = false;
            ViewBag.CanCreateRevision = false;

            if (id > 0)
            {
                objCBL001 = CBLDB.CBL001.Where(x => x.HeaderId == id).FirstOrDefault();
                ViewBag.Project = db.COM001.Where(i => i.t_cprj.Equals(objCBL001.Project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + "-" + i.t_dsca).FirstOrDefault();
                ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objCBL001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                ViewBag.WeldingProcess = Manager.GetSubCatagories("Welding Process", objCBL001.BU, objCBL001.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
                ViewBag.consumableType = Manager.GetSubCatagories("Consumable Type", objCBL001.BU, objCBL001.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
                int maxRevNo = CBLDB.CBL001.Where(x => x.QualityProject == objCBL001.QualityProject && x.Project == objCBL001.Project && x.Location == objCBL001.Location).Select(x => x.RevNo).Max();
                if (objCBL001.RevNo == maxRevNo)
                    ViewBag.CanCreateRevision = true;
                ViewBag.maxRevNo = "R" + maxRevNo;
            }
            else
            {
                objCBL001.HeaderId = 0;
                objCBL001.Status = clsImplementationEnum.CBLStatus.Draft.GetStringValue();
                objCBL001.RevNo = 0;
                ViewBag.maxRevNo = "";
                ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objClsLoginInfo.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            }


            return View(objCBL001);
        }

        [HttpPost]
        public ActionResult GetLineData(JQueryDataTableParamModel param)
        {
            try
            {
                List<SP_CBL_GetLineData_Result> lstResult = new List<SP_CBL_GetLineData_Result>();
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var HeaderId = Convert.ToInt32(param.Headerid);
                ViewBag.Role = param.Roles;

                CBL001 objCBL001 = CBLDB.CBL001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                string whereCondition = string.Empty;
                whereCondition += " HeaderId=" + HeaderId + " ";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                string[] columnName = { "WeldingProcess", "ConsumableType", "AWSClass", "SizeMM", "BrandName", "BatchNo", "Manufacturer" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }

                if (string.IsNullOrWhiteSpace(sortColumnName))
                    lstResult = CBLDB.SP_CBL_GetLineData(StartIndex, EndIndex, strSortOrder, whereCondition).ToList().OrderBy(x => x.AWSClass).ToList();
                else
                    lstResult = CBLDB.SP_CBL_GetLineData(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                    Helper.GenerateHidden(newRecordId, "HeaderId", Convert.ToString(HeaderId)),
                                    "","0",
                                    Helper.HTMLAutoComplete(newRecordId,"txtWeldingProcess","","",false,"","WeldingProcess")+""+Helper.GenerateHidden(newRecordId,"WeldingProcess"),
                                    Helper.HTMLAutoComplete(newRecordId,"txtconsType","","",false,"","ConsumableType")+""+Helper.GenerateHidden(newRecordId,"ConsumableType"),
                                    Helper.HTMLAutoComplete(newRecordId,"txtAWSClass","","",false,"","AWSClass")+""+Helper.GenerateHidden(newRecordId,"AWSClass"),
                                    Helper.GenerateTextbox(newRecordId, "SizeMM", "", "", true, "", "10"),
                                    Helper.GenerateTextbox(newRecordId, "BrandName", "", "", false, "", "20"),
                                    Helper.GenerateTextbox(newRecordId, "BatchNo", "", "", false, "", "20"),
                                    Helper.GenerateTextbox(newRecordId, "Manufacturer", "", "", false, "", "25"),
                                    Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveLineData();")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                                Helper.GenerateHidden(Convert.ToInt32(uc.LineId), "HeaderId", Convert.ToString(uc.HeaderId)),
                                Helper.GenerateHidden(Convert.ToInt32(uc.LineId), "LineId", Convert.ToString(uc.LineId)),
                                Convert.ToString(uc.ROW_NO),
                               // GenerateAutoCompleteonBlur(Convert.ToInt32(uc.LineId),"txtWeldingProcess",getCategoryDesc("Welding Process",objCBL001.BU,objCBL001.Location,uc.WeldingProcess),"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");",IsDisable(objCBL001.Status,ViewBag.Role) ? true:false,IsDisable(objCBL001.Status,ViewBag.Role) ? "pointer-events:none;" : null,"WeldingProcess",false)+""+Helper.GenerateHidden(Convert.ToInt32(uc.LineId),"WeldingProcess",uc.WeldingProcess),
                                //GenerateAutoCompleteonBlur(Convert.ToInt32(uc.LineId),"txtconsType",getCategoryDesc("Consumable Type",objCBL001.BU,objCBL001.Location,uc.ConsumableType),"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");",IsDisable(objCBL001.Status,ViewBag.Role) ? true:false,IsDisable(objCBL001.Status,ViewBag.Role) ? "pointer-events:none;" : null,"ConsumableType",false)+""+Helper.GenerateHidden(Convert.ToInt32(uc.LineId),"ConsumableType",uc.ConsumableType),
                                //public string GenerateAutoCompleteonBlur(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string maxlength = "")
                                GenerateAutoCompleteonBlur(Convert.ToInt32(uc.LineId),"txtWeldingProcess",uc.WPDesc,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");",IsDisable(objCBL001.Status,ViewBag.Role) ? true:false,IsDisable(objCBL001.Status,ViewBag.Role) ? "pointer-events:none;" : null,"WeldingProcess",IsDisableESWorSAW(uc.WeldingProcess,uc.ConsumableType))+""+Helper.GenerateHidden(Convert.ToInt32(uc.LineId),"WeldingProcess",uc.WeldingProcess),
                                GenerateAutoCompleteonBlur(Convert.ToInt32(uc.LineId),"txtconsType",uc.CTDesc,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");",IsDisable(objCBL001.Status,ViewBag.Role) ? true:false,IsDisable(objCBL001.Status,ViewBag.Role) ? "pointer-events:none;" : null,"ConsumableType",IsDisableESWorSAW(uc.WeldingProcess,uc.ConsumableType))+""+Helper.GenerateHidden(Convert.ToInt32(uc.LineId),"ConsumableType",uc.ConsumableType),
                                GenerateAutoCompleteonBlur(Convert.ToInt32(uc.LineId),"txtAWSClass",uc.AWSClass,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");", IsDisable(objCBL001.Status,ViewBag.Role) ? true:false,IsDisable(objCBL001.Status,ViewBag.Role) ? "pointer-events:none;" : null,"AWSClass",false,"")+""+Helper.GenerateHidden(Convert.ToInt32(uc.LineId),"AWSClass",uc.AWSClass),
                                GenerateTextbox(Convert.ToInt32(uc.LineId), "SizeMM", uc.SizeMM, "UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","","", true, "", "10"),
                                GenerateTextbox(Convert.ToInt32(uc.LineId), "BrandName", uc.BrandName, "UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","","", IsDisable(objCBL001.Status,ViewBag.Role) ? true: false, "", "20"),
                                GenerateTextbox(Convert.ToInt32(uc.LineId), "BatchNo", uc.BatchNo, "UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");", "","",IsDisable(objCBL001.Status,ViewBag.Role) ? true: false, "", "20"),
                                GenerateTextbox(Convert.ToInt32(uc.LineId), "Manufacturer", uc.Manufacturer, "UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","","", IsDisable(objCBL001.Status,ViewBag.Role) ? true: false, "", "25"),
                                Helper.GenerateActionIcon(Convert.ToInt32(uc.LineId), "Copy Line", "Copy Line", "fa fa-files-o", "CopyLine("+ uc.LineId +")","",IsDisable(objCBL001.Status,ViewBag.Role) ? true : false, false, IsDisable(objCBL001.Status,ViewBag.Role) ? "pointer-events:none;" : null ) + " " +
                                Helper.GenerateActionIcon(Convert.ToInt32(uc.LineId), "Delete", "Delete Line", "fa fa-trash-o", "DeleteLine("+ uc.LineId +")","",IsDisable(objCBL001.Status,ViewBag.Role) ? true : false, false, IsDisable(objCBL001.Status,ViewBag.Role) ? "pointer-events:none;" : null )
                                }).ToList();
                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        private bool IsDisable(string HeaderStatus, string Role)
        {
            bool result = true;
            if ((HeaderStatus == clsImplementationEnum.CBLStatus.Draft.GetStringValue() || HeaderStatus == clsImplementationEnum.CBLStatus.Returned.GetStringValue()) && Role.ToLower() == "maintain")
                result = false;
            else
                result = true;

            return result;
        }

        private bool IsDisableESWorSAW(string weldingProcess, string consumableType)
        {
            bool result = false;

            if (weldingProcess.Equals("ESW"))
            {
                result = consumableType.Equals("Strip") || consumableType.Equals("Flux") ? true : false;
            }
            else if (weldingProcess.Equals("SAW"))
            {
                result = consumableType.Equals("Wire") || consumableType.Equals("Flux") ? true : false;
            }

            return result;
        }

        public string getCategoryDesc(string Key, string BU, string loc, string code)
        {
            string catDesc = (from glb002 in db.GLB002
                              join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                              where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && BU.Contains(glb002.BU) && loc.Equals(glb002.Location) && code.Equals(glb002.Code)
                              select glb002.Description).FirstOrDefault();
            if (string.IsNullOrEmpty(catDesc))
                return code;
            return catDesc;
        }

        #region Database Insertion/Updation
        [SessionExpireFilter, HttpPost]
        public ActionResult SaveNewRecord(CBL001 model)
        {
            CBL001 objCBL001 = null;
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string project = model.Project.Split('-')[0];
                string Location = model.Location.Split('-')[0];
                string BU = model.BU.Split('-')[0];
                string action = Convert.ToString(Request["Action"]);
                string IdenticalProject = Convert.ToString(Request["IdenticalProject"]);
                if (model.HeaderId > 0)
                {
                    objCBL001 = CBLDB.CBL001.Where(x => x.HeaderId == model.HeaderId).FirstOrDefault();
                    if (action == "Submit" || action == "SaveAndDraft")
                    {
                        objCBL001.QualityProject = model.QualityProject;
                        objCBL001.Project = project;
                        objCBL001.BU = BU;
                        objCBL001.Location = Location;

                        #region Identical Project in CBL003
                        List<CBL003> DBlistCBL003 = CBLDB.CBL003.Where(x => x.HeaderId == objCBL001.HeaderId).ToList();
                        if (DBlistCBL003 != null && DBlistCBL003.Count > 0)
                        {
                            CBLDB.CBL003.RemoveRange(DBlistCBL003);
                        }
                        CBLDB.SaveChanges();
                        if (!string.IsNullOrEmpty(IdenticalProject))
                        {
                            string[] strarray = IdenticalProject.Split(',').ToArray();
                            List<CBL003> listCBL003 = new List<CBL003>();
                            foreach (var item in strarray)
                            {
                                CBL003 objCBL003 = new CBL003();
                                objCBL003.HeaderId = objCBL001.HeaderId;
                                objCBL003.BU = objCBL001.BU;
                                objCBL003.Location = objCBL001.Location;
                                objCBL003.QualityProject = objCBL001.QualityProject;
                                objCBL003.IQualityProject = item;
                                objCBL003.CreatedOn = DateTime.Now;
                                objCBL003.CreatedBy = objClsLoginInfo.UserName;
                                listCBL003.Add(objCBL003);
                            }
                            if (listCBL003.Count > 0)
                            {
                                CBLDB.CBL003.AddRange(listCBL003);
                            }
                        }
                        CBLDB.SaveChanges();
                        #endregion

                        if (action == "Submit")
                        {
                            objCBL001.Status = clsImplementationEnum.CBLStatus.SendToApproval.GetStringValue();
                            objCBL001.SubmittedBy = objClsLoginInfo.UserName;
                            objCBL001.SubmittedOn = DateTime.Now;
                        }
                        objCBL001.EditedBy = objClsLoginInfo.UserName;
                        objCBL001.EditedOn = DateTime.Now;
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.RecordSave.ToString();
                    }
                    else if (action == "Approve")
                    {
                        objCBL001.Remark = model.Remark;
                        objCBL001.Status = clsImplementationEnum.CBLStatus.Approved.GetStringValue();
                        objCBL001.ApprovedBy = objClsLoginInfo.UserName;
                        objCBL001.ApprovedOn = DateTime.Now;
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Approve.ToString();
                    }
                    else if (action == "Return")
                    {
                        objCBL001.Remark = model.Remark;
                        objCBL001.Status = clsImplementationEnum.CBLStatus.Returned.GetStringValue();
                        objCBL001.ReturnedBy = objClsLoginInfo.UserName;
                        objCBL001.ReturnedOn = DateTime.Now;
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Return.ToString();
                    }
                    else if (action == "Revision")
                    {
                        var Revision = CBLDB.CBL001.Where(x => x.HeaderId == model.HeaderId).Select(x => x.RevNo).Max();
                        objCBL001.RevNo = Revision + 1;
                        objCBL001.Status = "Draft";
                        objCBL001.CreatedOn = DateTime.Now;
                        objCBL001.CreatedBy = objClsLoginInfo.UserName;
                        objCBL001.EditedBy = objClsLoginInfo.UserName;
                        objCBL001.EditedOn = DateTime.Now;
                        objCBL001.ApprovedBy = null;
                        objCBL001.ApprovedOn = null;
                        objCBL001.ReturnedBy = null;
                        objCBL001.ReturnedOn = null;
                        objCBL001.SubmittedBy = null;
                        objCBL001.SubmittedOn = null;
                        CBLDB.CBL001.Add(objCBL001);

                        List<CBL003> IPList = CBLDB.CBL003.Where(x => x.HeaderId == model.HeaderId).ToList();
                        foreach (var item in IPList)
                        {
                            CBL003 newIPobj = new CBL003();
                            newIPobj.HeaderId = objCBL001.HeaderId;
                            newIPobj.QualityProject = item.QualityProject;
                            newIPobj.IQualityProject = item.IQualityProject;
                            newIPobj.BU = item.BU;
                            newIPobj.Location = item.Location;
                            newIPobj.CreatedOn = DateTime.Now;
                            newIPobj.CreatedBy = objClsLoginInfo.UserName;
                            CBLDB.CBL003.Add(newIPobj);
                        }

                        List<CBL002> LineList = CBLDB.CBL002.Where(x => x.HeaderId == model.HeaderId).ToList();
                        foreach (var item in LineList)
                        {
                            CBL002 newobj = new CBL002();

                            newobj.HeaderId = objCBL001.HeaderId;
                            newobj.WeldingProcess = item.WeldingProcess;
                            newobj.ConsumableType = item.ConsumableType;
                            newobj.AWSClass = item.AWSClass;
                            newobj.SizeMM = item.SizeMM;
                            newobj.BrandName = item.BrandName;
                            newobj.BatchNo = item.BatchNo;
                            newobj.Manufacturer = item.Manufacturer;
                            newobj.CreatedOn = DateTime.Now;
                            newobj.CreatedBy = objClsLoginInfo.UserName;
                            newobj.RefLinId = item.RefLinId;
                            CBLDB.CBL002.Add(newobj);
                        }
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revision.ToString();
                    }

                    CBLDB.SaveChanges();

                    if (action == "Revision")
                    {
                        List<CBL002> LineList = CBLDB.CBL002.Where(x => x.HeaderId == objCBL001.HeaderId && x.RefLinId != null).OrderBy(o => o.RefLinId).OrderByDescending(o => o.LineId).ToList();

                        if (LineList != null)
                        {
                            foreach (var item in LineList)
                            {
                                var result = CBLDB.CBL002.Where(cbl2 => cbl2.HeaderId == item.HeaderId && cbl2.RefLinId == item.RefLinId);

                                if (result != null)
                                {
                                    foreach (var item1 in result)
                                    {
                                        item1.RefLinId = item.LineId;
                                    }
                                    CBLDB.SaveChanges();
                                }
                            }
                        }
                    }

                    #region Log Entry
                    if (action == "Approve")
                    {
                        CBL001_Log objLog = new CBL001_Log();
                        objLog.HeaderId = objCBL001.HeaderId;
                        objLog.QualityProject = objCBL001.QualityProject;
                        objLog.Project = objCBL001.Project;
                        objLog.BU = objCBL001.BU;
                        objLog.Location = objCBL001.Location;
                        objLog.RevNo = objCBL001.RevNo;
                        objLog.CreatedBy = objCBL001.CreatedBy;
                        objLog.CreatedOn = objCBL001.CreatedOn;
                        objLog.ApprovedBy = objCBL001.ApprovedBy;
                        objLog.ApprovedOn = objCBL001.ApprovedOn;
                        objLog.ReturnedBy = objCBL001.ReturnedBy;
                        objLog.ReturnedOn = objCBL001.ReturnedOn;
                        objLog.Remark = objCBL001.Remark;
                        objLog.EditedBy = objCBL001.EditedBy;
                        objLog.EditedOn = objCBL001.EditedOn;
                        objLog.SubmittedBy = objCBL001.SubmittedBy;
                        objLog.SubmittedOn = objCBL001.SubmittedOn;
                        objLog.Status = objCBL001.Status;
                        CBLDB.CBL001_Log.Add(objLog);
                        CBLDB.SaveChanges();

                        List<CBL002> ObjLinelist = new List<CBL002>();
                        ObjLinelist = CBLDB.CBL002.Where(x => x.HeaderId == objCBL001.HeaderId).ToList();
                        foreach (var item in ObjLinelist)
                        {
                            CBL002_Log objlineLog = new CBL002_Log();
                            objlineLog.LineId = item.LineId;
                            objlineLog.HeaderId = item.HeaderId;
                            objlineLog.WeldingProcess = item.WeldingProcess;
                            objlineLog.ConsumableType = item.ConsumableType;
                            objlineLog.AWSClass = item.AWSClass;
                            objlineLog.SizeMM = item.SizeMM;
                            objlineLog.BatchNo = item.BatchNo;
                            objlineLog.BrandName = item.BrandName;
                            objlineLog.Manufacturer = item.Manufacturer;
                            objlineLog.CreatedBy = item.CreatedBy;
                            objlineLog.CreatedOn = item.CreatedOn;
                            objlineLog.EditedBy = item.EditedBy;
                            objlineLog.EditedOn = item.EditedOn;
                            CBLDB.CBL002_Log.Add(objlineLog);
                            CBLDB.SaveChanges();
                        }

                        List<CBL003> ObjIPlist = new List<CBL003>();
                        ObjIPlist = CBLDB.CBL003.Where(x => x.HeaderId == objCBL001.HeaderId).ToList();
                        foreach (var item in ObjIPlist)
                        {
                            CBL003_Log objIPLog = new CBL003_Log();
                            objIPLog.LineId = item.LineId;
                            objIPLog.HeaderId = item.HeaderId;
                            objIPLog.QualityProject = item.QualityProject;
                            objIPLog.IQualityProject = item.IQualityProject;
                            objIPLog.BU = item.BU;
                            objIPLog.Location = item.Location;
                            objIPLog.CreatedBy = item.CreatedBy;
                            objIPLog.CreatedOn = item.CreatedOn;
                            objIPLog.EditedBy = item.EditedBy;
                            objIPLog.EditedOn = item.EditedOn;
                            CBLDB.CBL003_Log.Add(objIPLog);
                            CBLDB.SaveChanges();
                        }
                    }
                    #endregion

                    #region Send Notification
                    if (action == "Return")
                    {
                        string struser = Manager.GetUserNameFromPsNo(objCBL001.ReturnedBy);
                        (new clsManager()).SendNotification(null, objCBL001.Project, "", "", "CBL: Request for " + objCBL001.QualityProject + " has been Returned by " + struser, clsImplementationEnum.NotificationType.Information.GetStringValue(), "", objCBL001.CreatedBy);
                    }
                    else if (action == "Submit")
                    {
                        clsManager objManager = new clsManager();
                        string[] psnolist = objManager.getEmployeeFromRole(clsImplementationEnum.UserRoleName.WE2.GetStringValue(), objCBL001.Location).Select(x => x.psnum).ToArray();
                        string strPSNoList = string.Join(",", psnolist);
                        (new clsManager()).SendNotification(null, objCBL001.Project, "", "", "CBL: Request for " + objCBL001.QualityProject + " has been submitted, please review and approve.", clsImplementationEnum.NotificationType.Information.GetStringValue(), "", strPSNoList);
                    }
                    #endregion
                    objResponseMsg.HeaderId = Convert.ToInt32(objCBL001.HeaderId);
                }
                else
                {
                    if (CBLDB.CBL001.Any(x => x.Project.ToLower() == project && x.Location == Location && x.BU == BU && x.QualityProject == model.QualityProject))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                        //objResponseMsg.HeaderId = objCBL001.HeaderId;
                    }
                    else
                    {
                        objCBL001 = new CBL001();
                        objCBL001.Project = project;
                        objCBL001.QualityProject = model.QualityProject;
                        objCBL001.Status = model.Status;
                        objCBL001.Location = Location;
                        objCBL001.BU = BU;
                        objCBL001.RevNo = 0;
                        objCBL001.CreatedOn = DateTime.Now;
                        objCBL001.CreatedBy = objClsLoginInfo.UserName;
                        CBLDB.CBL001.Add(objCBL001);
                        CBLDB.SaveChanges();

                        #region Identical Project added in CBL003
                        if (!string.IsNullOrEmpty(IdenticalProject))
                        {
                            string[] strarray = IdenticalProject.Split(',').ToArray();
                            List<CBL003> listCBL003 = new List<CBL003>();
                            foreach (var item in strarray)
                            {
                                CBL003 objCBL003 = new CBL003();
                                objCBL003.HeaderId = objCBL001.HeaderId;
                                objCBL003.BU = objCBL001.BU;
                                objCBL003.Location = objCBL001.Location;
                                objCBL003.QualityProject = objCBL001.QualityProject;
                                objCBL003.IQualityProject = item;
                                objCBL003.CreatedOn = DateTime.Now;
                                objCBL003.CreatedBy = objClsLoginInfo.UserName;
                                listCBL003.Add(objCBL003);
                            }
                            if (listCBL003.Count > 0)
                            {
                                CBLDB.CBL003.AddRange(listCBL003);
                            }
                        }

                        CBLDB.SaveChanges();
                        #endregion

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                        objResponseMsg.HeaderId = Convert.ToInt32(objCBL001.HeaderId);
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg);
        }

        [HttpPost]
        public ActionResult SaveLineDetails(FormCollection fc)
        {
            ResponceMsgWithHeaderId objResponseMsg = new ResponceMsgWithHeaderId();
            CBL002 objCBL002 = new CBL002();
            try
            {
                int newRowIndex = 0;
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId" + newRowIndex]) ? Convert.ToInt32(fc["HeaderId" + newRowIndex]) : 0;
                string strWP = string.Empty;
                string strCType = string.Empty;

                if (refHeaderId > 0)
                {
                    objCBL002.HeaderId = refHeaderId;
                    objCBL002.WeldingProcess = fc["WeldingProcess" + newRowIndex];
                    objCBL002.ConsumableType = fc["ConsumableType" + newRowIndex];
                    objCBL002.AWSClass = fc["AWSClass" + newRowIndex];
                    objCBL002.SizeMM = fc["SizeMM" + newRowIndex];
                    objCBL002.BrandName = fc["BrandName" + newRowIndex];
                    objCBL002.BatchNo = fc["BatchNo" + newRowIndex];
                    objCBL002.Manufacturer = fc["Manufacturer" + newRowIndex];
                    objCBL002.CreatedOn = DateTime.Now;
                    objCBL002.CreatedBy = objClsLoginInfo.UserName;
                    CBLDB.CBL002.Add(objCBL002);
                    CBLDB.SaveChanges();
                    long refId = objCBL002.LineId;

                    if (fc["WeldingProcess" + newRowIndex] == "SAW" && fc["ConsumableType" + newRowIndex] == "Wire")
                    {
                        strWP = "SAW";
                        strCType = "Flux";
                    }
                    else if (fc["WeldingProcess" + newRowIndex] == "SAW" && fc["ConsumableType" + newRowIndex] == "Flux")
                    {
                        strWP = "SAW";
                        strCType = "Wire";
                    }
                    else if (fc["WeldingProcess" + newRowIndex] == "ESW" && fc["ConsumableType" + newRowIndex] == "Flux")
                    {
                        strWP = "ESW";
                        strCType = "Strip";
                    }
                    else if (fc["WeldingProcess" + newRowIndex] == "ESW" && fc["ConsumableType" + newRowIndex] == "Strip")
                    {
                        strWP = "ESW";
                        strCType = "Flux";
                    }

                    if (!string.IsNullOrEmpty(strWP) && !string.IsNullOrEmpty(strCType))
                    {
                        using (var db = new CBLEntitiesContext())
                        {
                            var result = db.CBL002.SingleOrDefault(cbl2 => cbl2.LineId == refId);
                            if (result != null)
                            {
                                result.RefLinId = refId;
                                db.SaveChanges();
                            }
                        }

                        CBL002 Tempobj = new CBL002();
                        Tempobj.HeaderId = refHeaderId;
                        Tempobj.WeldingProcess = strWP;
                        Tempobj.ConsumableType = strCType;
                        Tempobj.AWSClass = fc["AWSClass" + newRowIndex]; ;
                        Tempobj.SizeMM = fc["SizeMM" + newRowIndex];
                        Tempobj.Manufacturer = "";
                        Tempobj.BatchNo = "";
                        Tempobj.BrandName = "";
                        Tempobj.CreatedOn = DateTime.Now;
                        Tempobj.CreatedBy = objClsLoginInfo.UserName;
                        Tempobj.RefLinId = refId;
                        CBLDB.CBL002.Add(Tempobj);
                        CBLDB.SaveChanges();
                    }

                    objResponseMsg.Value = "Added Succesfully";
                    objResponseMsg.Key = true;

                    objResponseMsg.LineId = objCBL002.LineId;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please Try Again";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please Try Again";
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateLine(int lineId, int headerId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            CBL002 objCBL002 = new CBL002();
            try
            {
                if (!string.IsNullOrEmpty(columnName))
                {
                    objCBL002 = CBLDB.CBL002.Where(i => i.LineId == lineId).FirstOrDefault();
                    if ((columnName == "WeldingProcess" || columnName == "ConsumableType" || columnName == "AWSClass" || columnName == "SizeMM" || columnName == "BrandName" ||
                        columnName == "BatchNo" || columnName == "Manufacturer")
                        && string.IsNullOrEmpty(columnValue)) //Not Nullable
                    {
                        objResponseMsg.Value = columnName + " is required!";
                    }
                    else
                    {
                        db.SP_COMMON_LINES_UPDATE(lineId, columnName, columnValue, "CBL002", objClsLoginInfo.UserName);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Updated Successfully";
                    }
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CopyCBLLine(int lineId)
        {
            ResponceMsgWithHeaderId objResponseMsg = new ResponceMsgWithHeaderId();
            try
            {
                CBL002 objCBL002 = CBLDB.CBL002.Where(i => i.LineId == lineId).FirstOrDefault();
                CBL002 newline = new CBL002();
                newline.HeaderId = objCBL002.HeaderId;
                newline.WeldingProcess = objCBL002.WeldingProcess;
                newline.ConsumableType = objCBL002.ConsumableType;
                newline.AWSClass = objCBL002.AWSClass;
                newline.SizeMM = objCBL002.SizeMM;
                newline.BrandName = objCBL002.BrandName;
                newline.BatchNo = objCBL002.BatchNo;
                newline.Manufacturer = objCBL002.Manufacturer;
                newline.CreatedOn = DateTime.Now;
                newline.CreatedBy = objClsLoginInfo.UserName;
                CBLDB.CBL002.Add(newline);
                CBLDB.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Inserted Successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Try Again";
            }

            return Json(objResponseMsg);
        }

        [HttpPost]
        public ActionResult DeleteCBLLine(int lineId)
        {
            ResponceMsgWithHeaderId objResponseMsg = new ResponceMsgWithHeaderId();
            try
            {
                CBL002 objCBL002 = CBLDB.CBL002.Where(i => i.LineId == lineId).FirstOrDefault();

                if (objCBL002 != null)
                {
                    var getRefID = objCBL002.RefLinId;
                    CBL002 cBL002 = CBLDB.CBL002.Where(i => i.RefLinId == getRefID && i.LineId != objCBL002.LineId && i.HeaderId == objCBL002.HeaderId).FirstOrDefault();

                    if (cBL002 != null)
                    {
                        cBL002.RefLinId = null;
                    }

                    CBLDB.CBL002.Remove(objCBL002);
                    CBLDB.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Deleted Successfully";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Try Again";
            }

            return Json(objResponseMsg);
        }

        [HttpPost]
        public ActionResult DeleteHeader(int HeaderId)
        {
            ResponceMsgWithHeaderId objResponseMsg = new ResponceMsgWithHeaderId();
            try
            {
                List<CBL002> objCBL002list = CBLDB.CBL002.Where(i => i.HeaderId == HeaderId).ToList();
                CBLDB.CBL002.RemoveRange(objCBL002list);

                List<CBL003> objCBL003list = CBLDB.CBL003.Where(i => i.HeaderId == HeaderId).ToList();
                CBLDB.CBL003.RemoveRange(objCBL003list);

                CBL001 objCBL001 = CBLDB.CBL001.Where(i => i.HeaderId == HeaderId).FirstOrDefault();
                CBLDB.CBL001.Remove(objCBL001);

                CBLDB.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Deleted Successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Try Again";
            }

            return Json(objResponseMsg);
        }
        #endregion

        #region Drop Down data for AWS Class
        [HttpPost]
        public ActionResult GetAWSList(int HeaderId, string term)
        {
            int RevNo = 0;
            CBL001 obj = CBLDB.CBL001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            RevNo = obj.RevNo;
            if (RevNo == 0)
            {
                if (!string.IsNullOrWhiteSpace(term))
                {
                    var data = CBLDB.uvw_CBL_AWSClassSizeDetails.Where(p => p.AWSClass.ToLower().Contains(term.ToLower())).Select(x => new { Value = x.AWSClass, Description = x.Size }).ToList();
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var data = CBLDB.uvw_CBL_AWSClassSizeDetails.Select(x => new { Value = x.AWSClass, Description = x.Size }).ToList();
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                var lstLength = new List<CategoryData>();
                lstLength = db.Database.SqlQuery<CategoryData>("Select distinct AWSClass as Value, AWSClass as Description from [dbo].[WPS002] Where Location = '" + objClsLoginInfo.Location + "' order by AWSClass").ToList();
                if (!string.IsNullOrWhiteSpace(term))
                {
                    lstLength = lstLength.Where(x => x.Value.ToLower().Contains(term.ToLower())).ToList();
                    return Json(lstLength, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(lstLength, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public ActionResult getSizeMM(string awsclass, int RevNo)
        {
            string size = string.Empty;
            if (RevNo == 0)
            {
                var data = CBLDB.uvw_CBL_AWSClassSizeDetails.Where(x => x.AWSClass == awsclass).Select(x => new { Value = x.AWSClass, Description = x.Size }).ToList();
                return Json(new { size = data[0].Description }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var lstLength = new List<CategoryData>();
                lstLength = db.Database.SqlQuery<CategoryData>("Select distinct AWSClass as Value, ConsumableSize as Description from [dbo].[WPS002] Where Location = '" + objClsLoginInfo.Location + "' order by AWSClass").ToList();
                size = lstLength.Where(x => x.Value == awsclass.Trim()).Select(x => x.Description).FirstOrDefault();
                return Json(new { size = size }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Header Data - Quality Project and Details 
        [HttpPost]
        public ActionResult GetQualityProjectDetails(string term)
        {
            string username = objClsLoginInfo.UserName;
            var lstATHLocation = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.Location).Distinct().ToList();
            var lstCOMLocation = db.COM003.Where(i => i.t_psno.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_loca).Distinct().ToList();

            var lstCOMBU = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.BU).Distinct().ToList();

            lstCOMLocation = lstATHLocation.Union(lstCOMLocation).ToList();

            List<Projects> lstQualityProject = new List<Projects>();
            if (!string.IsNullOrEmpty(term))
            {
                lstQualityProject = db.QMS010.Where(i => i.QualityProject.ToLower().Contains(term.ToLower())
                                                                 && lstCOMLocation.Contains(i.Location)
                                                                 && lstCOMBU.Contains(i.BU)
                                                               ).Select(i => new Projects { Value = i.QualityProject, projectCode = i.QualityProject }).Distinct().ToList();//&& i.CreatedBy.Equals(username,StringComparison.OrdinalIgnoreCase)
            }
            else
            {
                lstQualityProject = db.QMS010.Where(i => lstCOMLocation.Contains(i.Location)
                                                        && lstCOMBU.Contains(i.BU)
                                                        ).Select(i => new Projects { Value = i.QualityProject, projectCode = i.QualityProject }).Take(10).Distinct().ToList();
            }

            return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CheckProjectAsIdenticalProject(string QualityProject)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (CBLDB.CBL001.Any(i => i.QualityProject == QualityProject))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = QualityProject + " is as maintain identical project in " + CBLDB.CBL001.Where(i => i.QualityProject == QualityProject).FirstOrDefault().QualityProject;
                }
                else
                {
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetQualityProjectWiseProjectBUDetail(string qualityProject, int HeaderId = 0)
        {
            ModelQualityId objModelQualityId = new ModelQualityId();
            var project = db.QMS010.Where(i => i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase)).Select(i => i.Project).FirstOrDefault();
            objModelQualityId.Project = db.COM001.Where(i => i.t_cprj.Equals(project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + "-" + i.t_dsca).FirstOrDefault();// project;
            var ProjectWiseBULocation = Manager.getProjectWiseBULocation(project);
            objModelQualityId.QCPBU = ProjectWiseBULocation.QCPBU;
            if (HeaderId > 0)
            {
                objModelQualityId.IdenticalProj = string.Join(",", CBLDB.CBL003.Where(i => i.HeaderId == HeaderId).Select(i => i.IQualityProject).ToList());
            }
            return Json(objModelQualityId, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetIdenticalProjectEdit(string param)
        {
            int header = Convert.ToInt32(param);
            var obj = CBLDB.CBL001.Where(x => x.HeaderId == header).FirstOrDefault();
            string bu = obj.BU;
            string loc = obj.Location;
            param = obj.QualityProject;
            List<ddlValue> identicalProject = new List<ddlValue>();
            identicalProject = db.QMS010.Where(i => !i.QualityProject.Equals(param, StringComparison.OrdinalIgnoreCase) && i.BU == bu && i.Location == loc).Select(i => new ddlValue { id = i.QualityProject, text = i.QualityProject }).Distinct().Take(10).ToList();
            return Json(identicalProject, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetIdenticalProjects(string search, string param, string bu = "", string loc = "")
        {
            List<ddlValue> identicalProject = new List<ddlValue>();
            if (!string.IsNullOrEmpty(search))
            {
                identicalProject = db.QMS010.Where(i => !i.QualityProject.Equals(param, StringComparison.OrdinalIgnoreCase) && i.BU == bu && i.Location == loc && i.QualityProject.ToLower().Contains(search.ToLower())).Select(i => new ddlValue { id = i.QualityProject, text = i.QualityProject }).Distinct().ToList();
            }
            else
            {
                identicalProject = db.QMS010.Where(i => !i.QualityProject.Equals(param, StringComparison.OrdinalIgnoreCase) && i.BU == bu && i.Location == loc).Select(i => new ddlValue { id = i.QualityProject, text = i.QualityProject }).Distinct().Take(10).ToList();
            }
            return Json(identicalProject, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region HTML elements
        public string HTMLHistoryString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";
            htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            return htmlControl;
        }

        [NonAction]
        public string GenerateAutoCompleteonBlur(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string maxlength = "")
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control " + columnName;
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' data-name = '" + rowId + "' hdElement='" + hdElementId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + "  " + MaxCharcters + "  " + (disabled ? "disabled" : "") + " />";

            return strAutoComplete;
        }

        [NonAction]
        public static string GenerateTextbox(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onKeyPressMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string maxlength = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control " + columnName;
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onchange=\"" + onBlurMethod + "\"" : "";
            string onKeypressEvent = !string.IsNullOrEmpty(onKeyPressMethod) ? "onkeypress='" + onKeyPressMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            //htmlControl = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' " + MaxCharcters + " value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + "  " + onKeypressEvent + " "+ onClickEvent +" spara='" + rowId + "' />";
            htmlControl = "<input type=\"text\" " + (isReadOnly ? "readonly=\"readonly\"" : "") + " id=\"" + inputID + "\" " + MaxCharcters + " value=\"" + inputValue + "\" oldvalue=\"" + inputValue + "\" colname=\"" + columnName + "\" name=\"" + inputID + "\" class=\"" + className + "\" style=\"" + inputStyle + "\"  " + onBlurEvent + "  " + onKeypressEvent + " " + onClickEvent + " spara=\"" + rowId + "\" />";

            return htmlControl;
        }


        #endregion

    }

    public class ResponceMsgWithHeaderId : clsHelper.ResponseMsg
    {
        public long HeaderId;
        public long LineId;
        public int? RevNo;
        public string CBLNo;
    }


}