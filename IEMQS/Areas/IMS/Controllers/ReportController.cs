﻿using IEMQS.Areas.Authorization.Controllers;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using OfficeOpenXml;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace IEMQS.Areas.IMS.Controllers
{
    public class ReportController : clsBase
    {
        // GET: IMS/Report
        #region Main Page
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }

        //load tabs wise data
        [SessionExpireFilter]
        [HttpPost]
        public ActionResult LoadTabWisePartial(string tabname)
        {
            ViewBag.tabname = tabname; //name of tab
            switch (tabname)
            {
                //for IMS Details Report
                case "Detail":
                    ViewBag.Title = "Maintain Instrument IMS Report";
                    ViewBag.ReportType = clsImplementationEnum.IMSReport.Instrument.GetStringValue();
                    break;
                //for IMS DPP Report
                case "DPP":
                    ViewBag.Title = "Maintain Instrument Issued IMS Report";
                    ViewBag.ReportType = clsImplementationEnum.IMSReport.Issued.GetStringValue();
                    break;
                //for IMS Summary Report
                case "Summary":
                    ViewBag.Title = "Maintain Instrument Available IMS Report";
                    ViewBag.ReportType = clsImplementationEnum.IMSReport.Available.GetStringValue();
                    break;
                default:
                    ViewBag.Title = "Maintain Instrument IMS Report";
                    ViewBag.ReportType = clsImplementationEnum.IMSReport.Instrument.GetStringValue();
                    break;
            }
            return PartialView("_LoadReports");
        }

        public JsonResult GetLocation(string search, string param)
        {
            try
            {
                int LocationTypeId = Convert.ToInt32(clsImplementationEnum.BLDNumber.LOC.GetStringValue());

                var objLocation = (from a1 in db.ATH001
                                   join c2 in db.COM002 on a1.Location equals c2.t_dimx
                                   where a1.Employee == objClsLoginInfo.UserName && c2.t_dtyp == LocationTypeId
                                   select new { Location = a1.Location, Name = c2.t_desc }
                ).ToList().Select(i => new { Value = i.Location, Text = i.Name }).Distinct();
                //Text = s.Role, Value = s.Id
                return Json(objLocation, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }

    public class ResponceMsg
    {
        public Boolean Key { get; set; }
        public string Value { get; set; }
        public string Project { get; set; }
        public string DesignEngineer { get; set; }
        public string QAEngineer { get; set; }
        public string QCManager { get; set; }
        public string QAManager { get; set; }
        public string PMGManager { get; set; }
    }
}