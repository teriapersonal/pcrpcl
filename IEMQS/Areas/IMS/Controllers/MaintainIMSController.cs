﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using OfficeOpenXml;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.IMS.Controllers
{
    public class MaintainIMSController : clsBase
    {
        // GET: IMS/MaintainIMS
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult Index()
        {
            var lstTestArea = db.COM002.Where(x => x.t_dtyp == 3).Select(i => new CategoryData { Value = i.t_dimx, Code = i.t_dimx, CategoryDescription = i.t_dimx + " - " + i.t_desc }).ToList();
            ViewBag.TestArea = lstTestArea;
            return View();
        }
        [HttpPost, SessionExpireFilter]
        public JsonResult LoadTestAreaGridData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "1=1";

                string[] columnName = { "Location", "TestArea", "CreatedBy", "EditedBy", };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {

                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_IMS_GET_TEST_AREA_DETAILS
                                (
                               StartIndex,
                               EndIndex,
                               strSortOrder,
                               strWhere
                                ).ToList();

                int newRecordId = 0;
                string location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objClsLoginInfo.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                var newRecord = new[] {
                                        Helper.GenerateHidden(newRecordId, "HeaderId", ""),
                                        location+""+Helper.GenerateHidden(newRecordId,"Location",location.Split('-')[0].Trim()),
                                        //GenerateAutoCompleteonBlur(newRecordId,"txtTestArea","","CheckDuplicate(this);",false,"","TestArea")+""+Helper.GenerateHidden(newRecordId,"TestArea"),
                                        GenerateAutoCompleteonBlur(newRecordId,"txtTestArea","","",false,"","TestArea")+""+Helper.GenerateHidden(newRecordId,"TestArea"),
                                        GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveNewStage();" ),
                                        };
                var data = (from uc in lstResult
                            select new[]
                            {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == uc.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault())+""+Helper.GenerateHidden(uc.Id,"Location",uc.Location),
                                GenerateAutoCompleteonBlur(uc.Id,"txtTestArea",getTestAreaDesc(uc.TestArea),"UpdateLineDetails(this, "+ uc.Id +");",false,"","TestArea",(chkTestAreaUsed(uc.Location,uc.TestArea)?true:false))+""+Helper.GenerateHidden(uc.Id,"TestArea",uc.TestArea),
                                "<center>"+(HTMLActionString(uc.Id,"","Delete","Delete Record","fa fa-trash-o","DeleteRecord("+ uc.Id +");",chkTestAreaUsed(uc.Location,uc.TestArea))) +"<a class='' href='javascript:void(0)' onclick=ShowTimeline('/IMS/MaintainIMS/ShowTimelineTestArea?HeaderID="+Convert.ToInt32(uc.Id)+"');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a>"+"</center>",
                           }).ToList();
                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public string getTestAreaDesc(string TestArea)
        {
            if (String.IsNullOrEmpty(TestArea))
                return "";
            else
                return db.COM002.Where(x => x.t_dtyp == 3 && x.t_dimx.Equals(TestArea)).Select(x => x.t_dimx + " - " + x.t_desc).FirstOrDefault();
        }
        [HttpPost]
        public ActionResult UpdateTestAreaData(int headerId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string tableName = string.Empty;
                //tableName = "QMS021";
                if (!string.IsNullOrEmpty(columnName))
                {
                    var dbResult = db.SP_IMS_TEST_AREA_UPDATE(headerId, columnName, columnValue).FirstOrDefault();//SP_IMS_TEST_AREA_UPDATE
                    if (dbResult.Result.ToString() == "IS")
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost, SessionExpireFilter]
        public ActionResult SaveNewTestArea(FormCollection fc)
        {
            int newRowIndex = 0;
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (CheckDuplicateTestArea(fc["Location" + newRowIndex], fc["TestArea" + newRowIndex]))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Test Area with same location already exists";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                IMS001 objIMS001 = new IMS001();
                objIMS001.Location = fc["Location" + newRowIndex].Split('-')[0].Trim();
                objIMS001.TestArea = fc["TestArea" + newRowIndex];
                objIMS001.CreatedBy = objClsLoginInfo.UserName;
                objIMS001.CreatedOn = DateTime.Now;
                db.IMS001.Add(objIMS001);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteTestArea(int HeaderID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                IMS001 objIMS001 = db.IMS001.Where(x => x.Id == HeaderID).FirstOrDefault();
                if (objIMS001 != null)
                {

                    db.IMS001.Remove(objIMS001);

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Test area " + objIMS001.TestArea + " is deleted successfully";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult CheckDuplicateTestAreas(string location, string testarea)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();

            if (CheckDuplicateTestArea(location, testarea))
            {
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Test Area with same location already exists";
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public bool CheckDuplicateTestArea(string location, string testarea)
        {
            return db.IMS001.Any(c => c.Location == location && c.TestArea == testarea);
        }
        public bool chkTestAreaUsed(string location, string testarea)
        {
            return db.IMS002.Any(c => c.Location == location && c.TestArea == testarea);
        }

        public ActionResult ShowTimelineTestArea(int HeaderID)
        {
            TimelineViewModel model = new TimelineViewModel();

            IMS001 objIMS001 = db.IMS001.Where(x => x.Id == HeaderID).FirstOrDefault();
            model.Title = "IMS TestArea";
            model.CreatedBy = objIMS001.CreatedBy != null ? Manager.GetUserNameFromPsNo(objIMS001.CreatedBy) : null;
            model.CreatedOn = objIMS001.CreatedOn;
            model.EditedBy = objIMS001.EditedBy != null ? Manager.GetUserNameFromPsNo(objIMS001.EditedBy) : null;
            model.EditedOn = objIMS001.EditedOn;

            return PartialView("_ShowTimeline", model);
        }

        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult MaintainInstrument()
        {
            return View();
        }

        [HttpPost]
        public JsonResult LoadInstumentHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                strWhere += " 1=1";

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                string[] columnName = { "Location", "InstrumentNumber", "InstrumentDescription", "InstrumentGroup", "TestArea" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                //strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "", "Location");
                var lstResult = db.SP_IMS_GET_INSTRUMENT_HEADER
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.Location),
                            Convert.ToString(uc.InstrumentNumber),
                            Convert.ToString(uc.InstrumentDescription),
                            Convert.ToString(uc.InstrumentGroup),
                            Convert.ToString(getTestAreaDesc(uc.TestArea)),
                            uc.LastCalibrationDate == null ? "" :Convert.ToDateTime(uc.LastCalibrationDate).ToString("dd/MM/yyyy"),
                            uc.CalibrationDueDate == null ? "" :Convert.ToDateTime(uc.CalibrationDueDate).ToString("dd/MM/yyyy"),
                            "<center><a class='' href='"+ WebsiteURL +"/IMS/MaintainIMS/InstrumentDetails?HeaderId="+uc.HeaderId+"'><i style='' class='fa fa-eye'></i></a> "+(HTMLActionString(uc.HeaderId,"","Delete","Delete Record","fa fa-trash-o","DeleteRecord("+ uc.HeaderId +");",chkTestResultMaintained(uc.HeaderId) || uc.Active == "No"))+" "+HTMLActionString(uc.HeaderId,"","CopyInstrument","Copy Instrument","fa fa-files-o","CopyInstrument("+uc.HeaderId+");")+" <a class='' href='javascript:void(0)' onclick=ShowTimeline('/IMS/MaintainIMS/ShowTimelineInstrument?HeaderID="+Convert.ToInt32(uc.HeaderId)+"');><i style='' class='fa fa-clock-o'></i></a></center>",
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult GetCopyDetails(int HeaderID)
        {
            var objIMS002 = db.IMS002.Where(i => i.HeaderId == HeaderID).FirstOrDefault();

            ViewBag.Instrument = objIMS002.InstrumentNumber + " - " + objIMS002.InstrumentDescription;
            ViewBag.Location = db.COM002.Where(x => x.t_dtyp == 1 && x.t_dimx == objIMS002.Location).Select(x => x.t_dimx + " - " + x.t_desc).FirstOrDefault();

            return PartialView("_LoadInstCopyPartial");
        }
        [HttpPost]
        public ActionResult chkCopyDetails(int strheaderID, string toInstrument)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objIMS002 = db.IMS002.Where(i => i.HeaderId == strheaderID).FirstOrDefault();
                var user = objClsLoginInfo.UserName;
                var testArea = db.COM003.Where(x => x.t_psno == user && x.t_actv == 1).Select(x => x.t_depc).FirstOrDefault();
                if (objIMS002 != null)
                {
                    if (db.IMS001.Where(x => x.TestArea == testArea && x.Location == objIMS002.Location).Any())
                    {
                        if (db.IMS002.Any(x => x.InstrumentNumber == toInstrument && x.Location == objIMS002.Location && x.Active == "Yes"))
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "This instrument number already exist for this location";
                        }
                        else
                        {
                            objResponseMsg.Key = true;
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Your department code doesn’t exist in Test Area, Pls maintain";
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Instrument found for copy";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult copyDetails(int strheaderID, string toInstrument, string instrumentDesc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objIMS002 = db.IMS002.Where(i => i.HeaderId == strheaderID).FirstOrDefault();
                var user = objClsLoginInfo.UserName;
                if (objIMS002 != null)
                {
                    IMS002 objDestIMS002 = new IMS002();
                    if (db.IMS002.Any(x => x.InstrumentNumber == toInstrument && x.Location == objIMS002.Location && x.Active == "No"))
                    {
                        var oldRevNo = db.IMS002.Where(x => x.InstrumentNumber == toInstrument && x.Location == objIMS002.Location && x.Active == "No").Max(x => x.RevNo);
                        objDestIMS002.RevNo = oldRevNo + 1;
                    }
                    else
                        objDestIMS002.RevNo = 0;
                    objDestIMS002.Location = objIMS002.Location;
                    objDestIMS002.InstrumentNumber = toInstrument;
                    objDestIMS002.InstrumentDescription = instrumentDesc;
                    objDestIMS002.FromRange = objIMS002.FromRange;
                    objDestIMS002.ToRange = objIMS002.ToRange;
                    objDestIMS002.InstrumentGroup = objIMS002.InstrumentGroup;
                    objDestIMS002.TestArea = db.COM003.Where(x => x.t_psno == user && x.t_actv == 1).Select(x => x.t_depc).FirstOrDefault();
                    objDestIMS002.LeastCount = objIMS002.LeastCount;
                    objDestIMS002.LeastCountUnit = objIMS002.LeastCountUnit;
                    objDestIMS002.CalibrationIntervalType = objIMS002.CalibrationIntervalType;
                    objDestIMS002.CalibrationInterval = objIMS002.CalibrationInterval;
                    objDestIMS002.BlockedforCalibration = "No";
                    objDestIMS002.Active = "Yes";
                    objDestIMS002.CalibrationProgram = objIMS002.CalibrationProgram;
                    objDestIMS002.CreatedBy = objClsLoginInfo.UserName;
                    objDestIMS002.CreatedOn = DateTime.Now;
                    db.IMS002.Add(objDestIMS002);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Instrument copied successfully";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Instrument found for copy";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult InstrumentDetails(int? HeaderId)
        {
            IMS002 objIMS002 = new IMS002();
            var user = objClsLoginInfo.UserName;
            var BU = db.ATH001.Where(x => x.Employee == user && x.Location == objClsLoginInfo.Location).Select(x => x.BU).FirstOrDefault();
            ViewBag.InstrumentGroup = Manager.GetSubCatagories("Instrument Group", BU, objClsLoginInfo.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
            ViewBag.LeastCountUnit = Manager.GetSubCatagories("Least Count Unit", BU, objClsLoginInfo.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
            ViewBag.CalibrationIntervalType = Manager.GetSubCatagories("Calibration Interval Type", BU, objClsLoginInfo.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
            ViewBag.CalibrationProgram = Manager.GetSubCatagories("Calibration Program", BU, objClsLoginInfo.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
            if (HeaderId != null)
            {
                objIMS002 = db.IMS002.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                objIMS002.TestArea = db.COM002.Where(x => x.t_dtyp == 3 && x.t_dimx == objIMS002.TestArea).Select(x => x.t_dimx + " - " + x.t_desc).FirstOrDefault();
                ViewBag.LastCalibrationDate = objIMS002.LastCalibrationDate == null ? "" : Convert.ToDateTime(objIMS002.LastCalibrationDate).ToString("dd/MM/yyyy");
                ViewBag.CalibrationDueDate = objIMS002.CalibrationDueDate == null ? "" : Convert.ToDateTime(objIMS002.CalibrationDueDate).ToString("dd/MM/yyyy");
                ViewBag.txtInstrumentGroup = getCategoryDesc("Instrument Group", BU, objIMS002.Location, objIMS002.InstrumentGroup);
                ViewBag.txtLeastCountUnit = getCategoryDesc("Least Count Unit", BU, objIMS002.Location, objIMS002.LeastCountUnit);
                ViewBag.txtCalibrationIntervalType = getCategoryDesc("Calibration Interval Type", BU, objIMS002.Location, objIMS002.CalibrationIntervalType);
                ViewBag.txtCalibrationProgram = getCategoryDesc("Calibration Program", BU, objIMS002.Location, objIMS002.CalibrationProgram);
                ViewBag.TestResultMaintained = chkTestResultMaintained(Convert.ToInt32(HeaderId));
                objIMS002.Location = db.COM002.Where(x => x.t_dtyp == 1 && x.t_dimx == objIMS002.Location).Select(x => x.t_dimx + " - " + x.t_desc).FirstOrDefault();
            }
            else
            {
                var TestArea = db.COM003.Where(x => x.t_psno == user && x.t_actv == 1).Select(x => x.t_depc).FirstOrDefault();
                objIMS002.TestArea = db.COM002.Where(x => x.t_dtyp == 3 && x.t_dimx == TestArea).Select(x => x.t_dimx + " - " + x.t_desc).FirstOrDefault();



                var location = (from a in db.COM003
                                join b in db.COM002 on a.t_loca equals b.t_dimx
                                where b.t_dtyp == 1 && a.t_actv == 1
                                && a.t_psno.Equals(user, StringComparison.OrdinalIgnoreCase)
                                select b.t_dimx + "-" + b.t_desc).FirstOrDefault();
                objIMS002.Location = location;
            }
            return View(objIMS002);
        }
        [HttpPost]
        public ActionResult SaveInstrumentDetails(FormCollection fc, IMS002 ims002)/*, bool hasAttachments, Dictionary<string, string> Attach)*/
        {
            IMS002 objIMS002 = new IMS002();
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            //string Status = clsImplementationEnum.PTRStatus.Draft.GetStringValue();
            try
            {
                string location = fc["Location"].Split('-')[0].Trim();
                if (ims002.HeaderId > 0)
                {
                    objIMS002 = db.IMS002.Where(m => m.HeaderId == ims002.HeaderId).FirstOrDefault();
                }
                objIMS002.Location = location;
                objIMS002.InstrumentNumber = ims002.InstrumentNumber;
                objIMS002.InstrumentDescription = ims002.InstrumentDescription;
                objIMS002.InstrumentGroup = ims002.InstrumentGroup;
                objIMS002.FromRange = ims002.FromRange;
                objIMS002.ToRange = ims002.ToRange;
                objIMS002.LeastCount = ims002.LeastCount;
                objIMS002.LeastCountUnit = ims002.LeastCountUnit;
                objIMS002.TestArea = ims002.TestArea.Split('-')[0].Trim();
                objIMS002.CalibrationInterval = ims002.CalibrationInterval;
                objIMS002.CalibrationIntervalType = ims002.CalibrationIntervalType;
                objIMS002.LastCalibrationDate = ims002.LastCalibrationDate;
                objIMS002.CalibrationDueDate = ims002.CalibrationDueDate;
                objIMS002.BlockedforCalibration = ims002.BlockedforCalibration;
                objIMS002.Active = ims002.Active;
                objIMS002.CalibrationProgram = ims002.CalibrationProgram;
                if (ims002.HeaderId > 0)
                {
                    objIMS002.EditedBy = objClsLoginInfo.UserName;
                    objIMS002.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Value = "Record Updated Succesfully";
                }
                else
                {
                    if (db.IMS002.Any(x => x.InstrumentNumber == ims002.InstrumentNumber && x.Location == location && x.Active == "No"))
                    {
                        var oldRevNo = db.IMS002.Where(x => x.InstrumentNumber == ims002.InstrumentNumber && x.Location == location && x.Active == "No").Max(x => x.RevNo);
                        objIMS002.RevNo = oldRevNo + 1;
                    }
                    else
                        objIMS002.RevNo = 0;
                    objIMS002.CreatedOn = DateTime.Now;
                    objIMS002.CreatedBy = objClsLoginInfo.UserName;
                    db.IMS002.Add(objIMS002);
                    db.SaveChanges();
                    objResponseMsg.Value = "Record Added Succesfully";
                }
                //var folderPath = "IMS002/" + objIMS002.HeaderId ;
                objResponseMsg.HeaderId = objIMS002.HeaderId;
                //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please Try Again";
                // objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteInstrument(int HeaderID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                IMS002 objIMS002 = db.IMS002.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
                if (objIMS002 != null)
                {
                    if (!chkTestResultMaintained(HeaderID))
                    {
                        db.IMS002.Remove(objIMS002);

                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Instrument " + objIMS002.InstrumentNumber + " is deleted successfully";
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Test Result is maintained for Instrument " + objIMS002.InstrumentNumber + " it cannot be deleted";
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public bool chkTestResultMaintained(int HeaderId)
        {
            return db.IMS003.Any(c => c.HeaderId == HeaderId);
        }

        [HttpPost]
        public ActionResult CheckInstumentNo(string InstrumentNumber)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var location = objClsLoginInfo.Location;
                if (db.IMS002.Where(x => x.InstrumentNumber == InstrumentNumber && x.Location == location && x.Active == "Yes").Any())
                {
                    objResponseMsg.Key = false;
                }
                else
                    objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please try Again";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult CheckTestArea(string TestArea)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var location = objClsLoginInfo.Location;
                var testArea = TestArea.Split('-')[0].Trim();
                if (!db.IMS001.Where(x => x.TestArea == testArea && x.Location == location).Any())
                {
                    objResponseMsg.Key = false;
                }
                else
                    objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please try Again";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost, SessionExpireFilter]
        public JsonResult LoadCalibrationTestResult(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "1=1";
                var HeaderId = Convert.ToInt32(param.Headerid);
                strWhere += "and HeaderId=" + HeaderId + " ";
                string[] columnName = { "CalibrationReportSummary", "Unblock", "Remarks", };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {

                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_IMS_GET_CALIBRATION_TEST_RESULT
                                (
                               StartIndex,
                               EndIndex,
                               strSortOrder,
                               strWhere
                                ).ToList();
                List<SelectListItem> BoolenList = new List<SelectListItem>() { new SelectListItem { Text = "Yes", Value = "Yes" }, new SelectListItem { Text = "No", Value = "No" } };
                int newRecordId = 0;
                string location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objClsLoginInfo.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                var newRecord = new[] {
                                        Helper.GenerateHidden(newRecordId, "HeaderId", Convert.ToString(HeaderId)),
                                        Helper.GenerateTextbox(newRecordId,"LastCalibrationDate"),
                                        "",
                                        Helper.GenerateTextbox(newRecordId,"CalibrationDueDate","","",true),
                                        Helper.GenerateTextbox(newRecordId,"CalibrationReportSummary"),
                                        Helper.GenerateDropdown(newRecordId, "Unblock", new SelectList(BoolenList, "Value", "Text")),
                                        Helper.GenerateTextbox(newRecordId,"Remarks","","",false,"","100"),
                                        GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveNewStage();" ),
                                        };
                //var data = (from uc in lstResult
                //            select new[]
                //            {
                //                //Convert.ToString(uc.ROW_NO),
                //                Helper.GenerateHidden(uc.LineId, "HeaderId", Convert.ToString(uc.HeaderId)),
                //                Helper.GenerateTextbox(uc.LineId, "LastCalibrationDate",Convert.ToDateTime(uc.LastCalibrationDate).ToString("yyyy-MM-dd") , "UpdateLineDetails(this, "+ uc.LineId +");",false),
                //                Helper.GenerateTextbox(uc.LineId, "CalibrationDueDate",uc.CalibrationDueDate == null?"": Convert.ToDateTime(uc.CalibrationDueDate).ToString("dd/MM/yyyy") , "",true),
                //                Helper.GenerateTextbox(uc.LineId,"CalibrationReportSummary",uc.CalibrationReportSummary,"UpdateLineDetails(this, "+ uc.LineId +");"),
                //                Helper.GenerateDropdown(uc.LineId, "Unblock", new SelectList(BoolenList, "Value", "Text", uc.Unblock),"","UpdateLineDetails(this, "+ uc.LineId +");",false),
                //                Helper.GenerateTextbox(uc.LineId,"Remarks",uc.Remarks,"UpdateLineDetails(this, "+ uc.LineId +");",false,"","100"),
                //                (chkInstrumentIssued(uc.Location,uc.InstrumentNumber)? "":HTMLActionString(uc.LineId,"","Delete","Delete Record","fa fa-trash-o","DeleteRecord("+ uc.LineId +");")) //+"<a class='btn btn-xs' href='javascript:void(0)' onclick=ShowTimeline('/IMS/MaintainIMS/ShowTimelineTestArea?HeaderID="+Convert.ToInt32(uc.Id)+"');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a>",
                //           }).ToList();
                var data = (from uc in lstResult
                            select new[]
                            {
                                //Convert.ToString(uc.ROW_NO),
                                Helper.GenerateHidden(uc.LineId, "HeaderId", Convert.ToString(uc.HeaderId)),
                                GenerateOnchangeTextbox(uc.LineId, "LastCalibrationDate",Convert.ToDateTime(uc.LastCalibrationDate).ToString("yyyy-MM-dd") , "UpdateCDateDetails(this, "+ uc.LineId +");",false),
                                Helper.GenerateHidden(uc.LineId, "hdnLastCalibrationDate",uc.CalibrationDueDate == null?"": Convert.ToDateTime(uc.CalibrationDueDate).ToString("dd/MM/yyyy")),
                                Helper.GenerateTextbox(uc.LineId, "CalibrationDueDate",uc.CalibrationDueDate == null?"": Convert.ToDateTime(uc.CalibrationDueDate).ToString("dd/MM/yyyy") , "",true),
                                Helper.GenerateTextbox(uc.LineId,"CalibrationReportSummary",uc.CalibrationReportSummary,"",true),
                                Helper.GenerateDropdown(uc.LineId, "Unblock", new SelectList(BoolenList, "Value", "Text", uc.Unblock),"","",true),
                                Helper.GenerateTextbox(uc.LineId,"Remarks",uc.Remarks,"UpdateLineDetails(this, "+ uc.LineId +");",true,"","100"),
                                "<center>"+Helper.GenerateGridButton(uc.LineId, "","Attachments","fa fa-paperclip", "showAttachments(this,"+uc.LineId+")") +"<a class='' href='javascript:void(0)' onclick=ShowTimeline('/IMS/MaintainIMS/ShowTimelineTestResult?LineID="+Convert.ToInt32(uc.LineId)+"');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a>"+"</center>", //(chkInstrumentIssued(uc.Location,uc.InstrumentNumber)? "":HTMLActionString(uc.LineId,"","Delete","Delete Record","fa fa-trash-o","DeleteRecord("+ uc.LineId +");"))
                           }).ToList();
                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhere,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [NonAction]
        public static byte[] FromBase64(string base64EncodedData)
        {
            base64EncodedData = base64EncodedData.Substring(base64EncodedData.IndexOf("base64,") + 7);
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return base64EncodedBytes;
        }

        [HttpPost]
        public ActionResult CalibrationDate(string rowId, int interval, string columnValue)
        {
            resultdata objResponseMsg = new resultdata();
            try
            {
                int lineId = Convert.ToInt32(rowId);
                DateTime cdate = Convert.ToDateTime(columnValue);
                var updatedata = db.IMS003.Where(x => x.LineId == lineId).FirstOrDefault();
                if (updatedata != null)
                {
                    if (updatedata.LastCalibrationDate != cdate && (updatedata.CalibrationDueDate != null ? updatedata.CalibrationDueDate > cdate : true))
                    {
                        updatedata.LastCalibrationDate = cdate;
                        updatedata.CalibrationDueDate = cdate.AddDays(interval);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.lineId = rowId;
                        objResponseMsg.duedate = Convert.ToString(updatedata.LastCalibrationDate);
                        objResponseMsg.Value = "Last Calibration Date Greater Than Calibration Due Date";
                    }
                }
            }
            catch
            {
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        public static string GenerateOnchangeTextbox(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string maxlength = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control";
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string onchangeEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onchange='" + onBlurMethod + "'" : "";

            htmlControl = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' " + MaxCharcters + " value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onchangeEvent + " spara='" + rowId + "' />";

            return htmlControl;
        }


        //[NonAction]
        //public static void ManageDocuments(string folderPath, bool hasAttachments, Dictionary<string, string> Attach, string Uploader)
        //{
        //    if (hasAttachments)
        //    {
        //        var existing = (new clsFileUpload()).GetDocuments(folderPath);
        //        var toDelete = new Dictionary<string, string>();
        //        foreach (var item in existing)
        //        {
        //            //if (Attach.Where(q => q.Key.Equals(item.Name)).Count() <= 0)
        //            if (Attach.Where(uc => uc.Key.Equals(item.Name)).Count() <= 0)
        //                toDelete.Add(item.Name, item.URL);
        //            else
        //            {
        //                //if (Attach.Where(q => q.Key.Equals(item.Name)).FirstOrDefault().Value.Substring(0, 3) != "URL")
        //                if (Attach.Where(uc => uc.Key.Equals(item.Name)).FirstOrDefault().Value.Substring(0, 3) != "URL")
        //                    toDelete.Add(item.Name, item.URL);
        //            }
        //        }
        //        foreach (var item in toDelete)
        //            (new clsFileUpload()).DeleteFile(folderPath, item.Key);
        //        var toUpload = Attach.Where(uc => !string.IsNullOrWhiteSpace(uc.Value)).Where(uc => (uc.Value.Length <= 3) || (uc.Value.Substring(0, 3) != "URL")).ToList();

        //        foreach (var attch in toUpload)
        //        {
        //            try
        //            {
        //                var base64Data = attch.Value;
        //                var dataBytes = FromBase64(attch.Value);
        //                var cmnt = Attach.Any(uc => uc.Key.Equals("_" + attch.Key));
        //                var x = Attach.Where(uc => uc.Key.Equals("_" + attch.Key)).FirstOrDefault();
        //                string comment = (!cmnt) ? null : Attach.Where(q => q.Key.Equals("_" + attch.Key)).FirstOrDefault().Value;
        //                clsUpload.Upload(attch.Key, dataBytes, folderPath, Uploader, comment);
        //            }
        //            catch (Exception e) { Elmah.ErrorSignal.FromCurrentContext().Raise(e); }
        //        }
        //    }
        //    else
        //    {
        //        var existing = clsUpload.getDocs(folderPath);
        //        foreach (var item in existing)
        //            clsUpload.DeleteFile(folderPath, item.Name);
        //    }
        //}
        //[HttpPost]
        //public ActionResult SaveAttachments(bool hasAttachments, Dictionary<string, string> Attach, int LineId)
        //{

        //    var objResult = new clsHelper.ResponseMsg();
        //    try
        //    {
        //        //if (db.LNC002.Any(q => q.LineId == LineId))
        //        if (db.IMS003.Any(uc => uc.LineId == LineId))
        //        {
        //            var objIMS003 = db.IMS003.Where(uc => uc.LineId == LineId).FirstOrDefault();
        //            //if (objKOM003.CreatedBy == objClsLoginInfo.UserName)
        //            //{
        //            //    var folderPath = "KOM003/" + objKOM003.LineId.ToString();
        //            //    ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
        //            //    objResult.Key = true;
        //            //    objResult.Value = "Saved Successfully.";
        //            //}
        //            //else
        //            //{
        //            //    objResult.Key = false;
        //            //    objResult.Value = "You cannot modify attachments on this line.";
        //            //}
        //            var folderPath = "IMS003/" + objIMS003.LineId.ToString();
        //            ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
        //            objResult.Key = true;
        //            objResult.Value = "Saved Successfully.";
        //        }
        //        else
        //        {
        //            objResult.Key = false;
        //            objResult.Value = "Line not found.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        objResult.Key = false;
        //        objResult.Value = "Error while saving attachments";
        //    }
        //    return Json(objResult, JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        public ActionResult UpdateTestResultData(int lineId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string tableName = string.Empty;
                //tableName = "QMS021";
                if (!string.IsNullOrEmpty(columnName))
                {
                    var dbResult = db.SP_IMS_TEST_RESULT_UPDATE(lineId, columnName, columnValue).FirstOrDefault();
                    if (dbResult.Result.ToString() == "IS")
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost, SessionExpireFilter]
        public ActionResult SaveNewTestResult(FormCollection fc)
        {
            int newRowIndex = 0;
            //clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                //if (CheckDuplicateTestArea(fc["Location" + newRowIndex], fc["TestArea" + newRowIndex]))
                //{
                //    objResponseMsg.Key = false;
                //    objResponseMsg.Value = "Test Area with same location already exists";
                //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                //}
                IMS002 objIMS002 = new IMS002();
                int headerId = Convert.ToInt32(fc["HeaderId" + newRowIndex]);
                objIMS002 = db.IMS002.Where(x => x.HeaderId == headerId).FirstOrDefault();

                IMS003 objIMS003 = new IMS003();
                objIMS003.HeaderId = headerId;
                objIMS003.Location = objIMS002.Location;
                objIMS003.InstrumentNumber = objIMS002.InstrumentNumber;
                string LastCalibrationDate = fc["LastCalibrationDate" + newRowIndex];
                if (!string.IsNullOrWhiteSpace(LastCalibrationDate))
                {
                    objIMS003.LastCalibrationDate = DateTime.ParseExact(LastCalibrationDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                { objIMS003.LastCalibrationDate = null; }
                objIMS002.LastCalibrationDate = objIMS003.LastCalibrationDate;
                if (objIMS002.CalibrationIntervalType == "Days")
                {
                    var a = DateTime.ParseExact(LastCalibrationDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                    objIMS003.CalibrationDueDate = a.AddDays(Convert.ToDouble(objIMS002.CalibrationInterval));
                    objIMS002.CalibrationDueDate = objIMS003.CalibrationDueDate;
                }

                objIMS003.CalibrationReportSummary = fc["CalibrationReportSummary" + newRowIndex];
                objIMS003.Unblock = fc["Unblock" + newRowIndex];
                if (objIMS003.Unblock == "Yes")
                    objIMS002.BlockedforCalibration = "No";
                objIMS003.Remarks = fc["Remarks" + newRowIndex];

                objIMS003.CreatedBy = objClsLoginInfo.UserName;
                objIMS003.CreatedOn = DateTime.Now;
                db.IMS003.Add(objIMS003);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.HeaderId = objIMS002.HeaderId;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteCalibrationResult(int LineID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                IMS003 objIMS003 = db.IMS003.Where(x => x.LineId == LineID).FirstOrDefault();
                if (objIMS003 != null)
                {

                    db.IMS003.Remove(objIMS003);

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Instrument No. " + objIMS003.InstrumentNumber + " calibration result is deleted successfully";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public bool chkInstrumentIssued(string location, string InstrumentNo)
        {
            string status = clsImplementationEnum.IMSStatus.Draft.GetStringValue();
            if (db.IMS011.Any(x => x.InstrumentNumber == InstrumentNo && x.Location == location && !x.Status.Equals(status)))
                return true;
            else
                return false;
        }

        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult MaintainInstrumentIssue()
        {
            return View();
        }
        [HttpPost]
        public ActionResult MaintainInstrumentRequest(string status)
        {
            if (status.ToLower() == "return")
            {
                ViewBag.Status = "Return";
                return PartialView("MaintainReturnRequest");
            }
            else
            {
                ViewBag.role = "Initiator";
                ViewBag.Status = status;
                return PartialView("_IssueRequest");
            }

        }
        [HttpPost]
        public JsonResult LoadInstumentIssueHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                strWhere += " 1=1";

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string userType = param.MTStatus;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += " and Status in('" + clsImplementationEnum.IMSStatus.Draft.GetStringValue() + "')";
                }
                var user = objClsLoginInfo.UserName;
                var userDept = db.COM003.Where(x => x.t_psno == user && x.t_actv == 1).Select(x => x.t_depc).FirstOrDefault();
                if (!userType.Equals("Approver"))
                {
                    strWhere += "and MaintainedbyUsersDepartment = " + userDept;
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                string[] columnName = { "Location", "InstrumentRequestNumber", "MaintainedbyUsersDepartment", "Remarks", "Status" };
                
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                //strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "", "Location");
                var lstResult = db.SP_IMS_GET_INSTRUMENT_ISSUE_HEADER
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.Location),
                            Convert.ToString(uc.InstrumentRequestNumber),
                            Convert.ToString(getTestAreaDesc(uc.MaintainedbyUsersDepartment)),
                            Convert.ToString(uc.Remarks),
                            Convert.ToString(uc.Status),
                            "<center><a class='' href='"+ WebsiteURL +"/IMS/MaintainIMS/InstrumentIssueDetails?HeaderId="+uc.HeaderId+"&uType="+userType+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a>"+"<a class='' href='javascript:void(0)' onclick=ShowTimeline('/IMS/MaintainIMS/ShowTimelineIssueHeader?HeaderID="+Convert.ToInt32(uc.HeaderId)+"');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a> "+(userType=="Approver"?HTMLActionString(uc.HeaderId,"","Close","Close Request","fa fa-times","closeRequest("+ uc.HeaderId +"); ",closeRequest(uc.HeaderId)):"")+"</center>",
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public bool closeRequest(int HeaderId)
        {
            IMS010 objIMS010 = db.IMS010.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            if (objIMS010.IMS011.Any(x => x.Status.Equals(clsImplementationEnum.IMSStatus.IssuedAndAcknowledged.GetStringValue()) || x.Status.Equals(clsImplementationEnum.IMSStatus.Returned.GetStringValue()) || x.Status.Equals(clsImplementationEnum.IMSStatus.Draft.GetStringValue())) || objIMS010.Status.Equals(clsImplementationEnum.IMSStatus.Closed.GetStringValue()))
                return true;
            else
                return false;
        }
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult InstrumentIssueDetails(int? HeaderId, string uType)
        {
            IMS010 objIMS010 = new IMS010();
            var user = objClsLoginInfo.UserName;
            var BU = db.ATH001.Where(x => x.Employee == user && x.Location == objClsLoginInfo.Location).Select(x => x.BU).FirstOrDefault();
            if (HeaderId != null)
            {
                objIMS010 = db.IMS010.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                objIMS010.MaintainedbyUsersDepartment = db.COM002.Where(x => x.t_dtyp == 3 && x.t_dimx == objIMS010.MaintainedbyUsersDepartment).Select(x => x.t_dimx + " - " + x.t_desc).FirstOrDefault();
                ViewBag.InstrumentGroup = Manager.GetSubCatagories("Instrument Group", BU, objIMS010.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
                objIMS010.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objIMS010.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                ViewBag.uType = uType;
                if (objIMS010.IMS011.Any(x => x.Status.Equals(clsImplementationEnum.IMSStatus.Draft.GetStringValue())) && uType == "Initiator")
                    ViewBag.Submit = "true";
                else
                    ViewBag.Submit = "false";
                if (objIMS010.IMS011.Any(x => x.Status.Equals(clsImplementationEnum.IMSStatus.IssuedAndAcknowledged.GetStringValue()) || x.Status.Equals(clsImplementationEnum.IMSStatus.Returned.GetStringValue()) || x.Status.Equals(clsImplementationEnum.IMSStatus.Draft.GetStringValue())) || objIMS010.Status.Equals(clsImplementationEnum.IMSStatus.Closed.GetStringValue()) || uType == "Initiator")
                    ViewBag.Close = "false";
                else
                    ViewBag.Close = "true";
                if (objIMS010.IMS011.Any(x => !(x.Status.Equals(clsImplementationEnum.IMSStatus.Submitted.GetStringValue()))) || !objIMS010.Status.Equals(clsImplementationEnum.IMSStatus.Submitted.GetStringValue()))
                    ViewBag.Retract = "false";
                else
                    ViewBag.Retract = "true";
            }
            else
            {
                var TestArea = db.COM003.Where(x => x.t_psno == user && x.t_actv == 1).Select(x => x.t_depc).FirstOrDefault();
                objIMS010.MaintainedbyUsersDepartment = db.COM002.Where(x => x.t_dtyp == 3 && x.t_dimx == TestArea).Select(x => x.t_dimx + " - " + x.t_desc).FirstOrDefault();
                objIMS010.Status = clsImplementationEnum.IMSStatus.Draft.GetStringValue();


                var location = (from a in db.COM003
                                join b in db.COM002 on a.t_loca equals b.t_dimx
                                where b.t_dtyp == 1 && a.t_actv == 1
                                && a.t_psno.Equals(user, StringComparison.OrdinalIgnoreCase)
                                select b.t_dimx + "-" + b.t_desc).FirstOrDefault();
                objIMS010.Location = location;
            }
            return View(objIMS010);
        }
        [HttpPost]
        public ActionResult SaveInstrumentIssueDetails(FormCollection fc, IMS010 ims010)
        {
            IMS010 objIMS010 = new IMS010();
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            //clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            //string Status = clsImplementationEnum.PTRStatus.Draft.GetStringValue();
            try
            {
                var BU = db.ATH001.Where(x => x.Employee == objClsLoginInfo.UserName && x.Location == objClsLoginInfo.Location).Select(x => x.BU).FirstOrDefault();
                string location = fc["Location"].Split('-')[0].Trim();
                if (ims010.HeaderId > 0)
                {
                    objIMS010 = db.IMS010.Where(m => m.HeaderId == ims010.HeaderId).FirstOrDefault();
                }
                objIMS010.Location = location;
                objIMS010.MaintainedbyUsersDepartment = ims010.MaintainedbyUsersDepartment.Split('-')[0].Trim();
                objIMS010.Remarks = ims010.Remarks;
                if (ims010.HeaderId > 0)
                {
                    objIMS010.EditedBy = objClsLoginInfo.UserName;
                    objIMS010.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.HeaderId = objIMS010.HeaderId;
                    objResponseMsg.Value = "Record Updated Succesfully";
                }
                else
                {
                    ObjectParameter outParm = new ObjectParameter("result", typeof(long));
                    db.SP_IMS_GET_NEXT_INSTRUMENT_REQUEST_NO(BU, objClsLoginInfo.Location, outParm);
                    objIMS010.InstrumentRequestNumber = (long)outParm.Value;
                    objIMS010.Status = clsImplementationEnum.IMSStatus.Draft.GetStringValue();
                    objIMS010.CreatedOn = DateTime.Now;
                    objIMS010.CreatedBy = objClsLoginInfo.UserName;
                    db.IMS010.Add(objIMS010);
                    db.SaveChanges();
                    objResponseMsg.HeaderId = objIMS010.HeaderId;
                    objResponseMsg.Value = "Record Added Succesfully";
                }

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please Try Again";
                // objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult RetractInstrumentIssueRequest(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                IMS010 objIMS010 = db.IMS010.Where(u => u.HeaderId == HeaderId).SingleOrDefault();
                if (objIMS010 != null && !objIMS010.Status.Equals(clsImplementationEnum.IMSStatus.Draft.GetStringValue()))
                {
                    var status = clsImplementationEnum.IMSStatus.IssuedAndAcknowledged.GetStringValue();
                    if (!objIMS010.IMS011.Any(x => x.Status.Equals(status)))
                    {
                        objIMS010.Status = clsImplementationEnum.IMSStatus.Draft.GetStringValue();
                        objIMS010.SubmittedBy = null;
                        objIMS010.SubmittedOn = null;
                        var lstLines = db.IMS011.Where(i => i.HeaderId == objIMS010.HeaderId).ToList();
                        lstLines.ForEach(i => i.Status = clsImplementationEnum.IMSStatus.Draft.GetStringValue());
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Request Retracted successfully";
                        db.SaveChanges();
                    }

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SubmitInstrumentIssueRequest(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                IMS010 objIMS010 = db.IMS010.Where(u => u.HeaderId == HeaderId).SingleOrDefault();
                if (objIMS010 != null)
                {
                    var status = clsImplementationEnum.IMSStatus.Draft.GetStringValue();
                    objIMS010.Status = clsImplementationEnum.IMSStatus.Submitted.GetStringValue();
                    objIMS010.SubmittedBy = objClsLoginInfo.UserName;
                    objIMS010.SubmittedOn = DateTime.Now;
                    db.SaveChanges();
                    var lstLines = db.IMS011.Where(i => i.HeaderId == objIMS010.HeaderId && i.Status.Equals(status)).ToList();
                    lstLines.ForEach(i => i.Status = clsImplementationEnum.IMSStatus.Submitted.GetStringValue());
                    db.SaveChanges();

                    #region Send Notification
                    (new clsManager()).SendNotificationByLocation(clsImplementationEnum.UserRoleName.QA3.GetStringValue(), "", objIMS010.Location, "Instrument Request: " + objIMS010.InstrumentRequestNumber + " has been submitted for your Approval", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IMS/MaintainIMS/ApproveIssueRequest");
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Request submitted successfully";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CloseInstrumentIssueRequest(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();

            try
            {
                IMS010 objIMS010 = db.IMS010.Where(u => u.HeaderId == HeaderId).SingleOrDefault();
                if (objIMS010 != null)
                {
                    objIMS010.Status = clsImplementationEnum.IMSStatus.Closed.GetStringValue();
                    db.SaveChanges();
                    var lstLines = db.IMS011.Where(i => i.HeaderId == objIMS010.HeaderId).ToList();
                    lstLines.ForEach(i => i.Status = clsImplementationEnum.IMSStatus.Closed.GetStringValue());
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Request closed successfully";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult CopyInstrumentIssueRequest(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var user = objClsLoginInfo.UserName;
                IMS010 objIMS010 = db.IMS010.Where(u => u.HeaderId == HeaderId).SingleOrDefault();
                if (objIMS010 != null)
                {
                    IMS010 objDesIMS010 = new IMS010();
                    var BU = db.ATH001.Where(x => x.Employee == objClsLoginInfo.UserName && x.Location == objClsLoginInfo.Location).Select(x => x.BU).FirstOrDefault();
                    ObjectParameter outParm = new ObjectParameter("result", typeof(long));
                    db.SP_IMS_GET_NEXT_INSTRUMENT_REQUEST_NO(BU, objClsLoginInfo.Location, outParm);
                    objDesIMS010.InstrumentRequestNumber = (long)outParm.Value;
                    objDesIMS010.MaintainedbyUsersDepartment = db.COM003.Where(x => x.t_psno == user && x.t_actv == 1).Select(x => x.t_depc).FirstOrDefault();
                    objDesIMS010.Location = objIMS010.Location;
                    objDesIMS010.Remarks = objIMS010.Remarks;
                    objDesIMS010.Status = clsImplementationEnum.IMSStatus.Draft.GetStringValue();
                    objDesIMS010.CreatedOn = DateTime.Now;
                    objDesIMS010.CreatedBy = objClsLoginInfo.UserName;
                    db.IMS010.Add(objDesIMS010);
                    db.SaveChanges();

                    List<IMS011> lstSrcIMS011 = db.IMS011.Where(x => x.HeaderId == HeaderId).ToList();
                    foreach (var srcIMS011 in lstSrcIMS011)
                    {
                        if (db.IMS002.Any(x => x.InstrumentNumber == srcIMS011.InstrumentNumber && x.Location == srcIMS011.Location && x.InstrumentGroup == srcIMS011.InstrumentGroup && x.RevNo == srcIMS011.RevNo && x.Active == "Yes"))
                        {
                            IMS011 objIMS011 = new IMS011();
                            objIMS011.HeaderId = objDesIMS010.HeaderId;
                            objIMS011.Location = objDesIMS010.Location;
                            objIMS011.InstrumentRequestNumber = objDesIMS010.InstrumentRequestNumber;
                            objIMS011.ExpectedReturnDate = srcIMS011.ExpectedReturnDate;
                            objIMS011.InstrumentGroup = srcIMS011.InstrumentGroup;
                            objIMS011.InstrumentNumber = srcIMS011.InstrumentNumber;
                            objIMS011.Status = clsImplementationEnum.IMSStatus.Draft.GetStringValue();
                            objIMS011.RevNo = srcIMS011.RevNo;
                            objIMS011.CreatedOn = DateTime.Now;
                            objIMS011.CreatedBy = objClsLoginInfo.UserName;
                            db.IMS011.Add(objIMS011);
                            db.SaveChanges();
                        }

                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Request No copied to " + objDesIMS010.InstrumentRequestNumber + " ";

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost, SessionExpireFilter]
        public JsonResult LoadIssueLines(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "1=1 ";
                var HeaderId = Convert.ToInt32(param.Headerid);
                var uType = param.MTStatus;
                strWhere += "and ims11.HeaderId=" + HeaderId + " ";
                string[] columnName = { "ims11.InstrumentGroup", "ims11.InstrumentNumber", "ims2.FromRange", "ims2.ToRange", "ims2.LeastCount", "ims2.LeastCountUnit", "ims11.Status" };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {

                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var objIMS010 = db.IMS010.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                var BU = db.ATH001.Where(x => x.Employee == objClsLoginInfo.UserName && x.Location == objIMS010.Location).Select(x => x.BU).FirstOrDefault();

                var lstResult = db.SP_IMS_GET_INSTRUMENT_ISSUE_LINES
                                (
                               StartIndex,
                               EndIndex,
                               strSortOrder,
                               strWhere
                                ).ToList();
                List<SelectListItem> BoolenList = new List<SelectListItem>() { new SelectListItem { Text = "Yes", Value = "Yes" }, new SelectListItem { Text = "No", Value = "No" } };
                int newRecordId = 0;
                string location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objClsLoginInfo.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                var newRecord = new[] {
                                        Helper.GenerateHidden(newRecordId, "HeaderId", Convert.ToString(HeaderId)),
                                        //Helper.GenerateTextbox(newRecordId,"InstrumentGroup"),
                                        GenerateAutoCompleteonBlur(newRecordId,"txtInstrumentGroup","","",false,"","InstrumentGroup")+""+Helper.GenerateHidden(newRecordId,"InstrumentGroup"),
                                        SelectInstrumentNumber(newRecordId,"","",objIMS010.Location,false,"","InstrumentNumber"),
                                        Helper.GenerateTextbox(newRecordId,"FromRange","","",true),
                                        Helper.GenerateTextbox(newRecordId,"ToRange","","",true),
                                        Helper.GenerateTextbox(newRecordId,"LeastCount","","",true),
                                        Helper.GenerateTextbox(newRecordId,"LeastCountUnit","","",true),
                                        Helper.GenerateTextbox(newRecordId,"CalibrationDueDate","","",true),
                                        Helper.GenerateTextbox(newRecordId,"ExpectedReturnDate"),
                                        //Helper.GenerateTextbox(newRecordId,"Status",clsImplementationEnum.IMSStatus.Draft.GetStringValue(),"",true),
                                        clsImplementationEnum.IMSStatus.Draft.GetStringValue(),
                                        GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveNewStage();" ),
                                        };
                var data = (from uc in lstResult
                            select new[]
                            {
                                //Convert.ToString(uc.ROW_NO),
                                Helper.GenerateHidden(uc.LineId, "HeaderId", Convert.ToString(uc.HeaderId)),
                                //Helper.GenerateTextbox(uc.LineId, "InstrumentGroup",uc.InstrumentGroup , "UpdateLineDetails(this, "+ uc.LineId +");",false),
                                //GenerateAutoCompleteonBlur(uc.LineId,"txtInstrumentGroup",getCategoryDesc("Instrument Group",BU,objIMS010.Location,uc.InstrumentGroup),"UpdateLineDetails(this, "+ uc.LineId +");",false,"","InstrumentGroup",(uc.Status.Equals(clsImplementationEnum.IMSStatus.Draft.GetStringValue())?false:true))+""+Helper.GenerateHidden(uc.LineId,"InstrumentGroup",uc.InstrumentGroup),
                                //SelectInstrumentNumber(uc.LineId,uc.InstrumentNumber,uc.InstrumentGroup,objIMS010.Location,(uc.Status.Equals(clsImplementationEnum.IMSStatus.Draft.GetStringValue())?false:true),"UpdateLineDetails(this, "+ uc.LineId +");","InstrumentNumber"),
                                getCategoryDesc("Instrument Group",BU,objIMS010.Location,uc.InstrumentGroup),
                                uc.InstrumentNumber,
                                Helper.GenerateTextbox(uc.LineId,"FromRange",Convert.ToString(uc.FromRange),"",true),
                                Helper.GenerateTextbox(uc.LineId,"ToRange",Convert.ToString(uc.ToRange),"",true),
                                Helper.GenerateTextbox(uc.LineId,"LeastCount",Convert.ToString(uc.LeastCount),"",true),
                                Helper.GenerateTextbox(uc.LineId,"LeastCountUnit",Convert.ToString(uc.LeastCountUnit),"",true),
                                Helper.GenerateTextbox(uc.LineId,"CalibrationDueDate",(uc.CalibrationDueDate!=null && uc.CalibrationDueDate.HasValue? uc.CalibrationDueDate.Value.ToString("dd/MM/yyyy"):""),"",true),
                                (uc.Status.Equals(clsImplementationEnum.IMSStatus.Draft.GetStringValue()) && uType == "Initiator")?Helper.GenerateTextbox(uc.LineId,"ExpectedReturnDate",Convert.ToDateTime(uc.ExpectedReturnDate).ToString("yyyy-MM-dd"),"UpdateLineDetails(this, "+ uc.LineId +");",false):Helper.GenerateTextbox(uc.LineId,"ExpectedReturnDate",Convert.ToDateTime(uc.ExpectedReturnDate).ToString("yyyy-MM-dd"),"UpdateLineDetails(this, "+ uc.LineId +");",true),
                                //Helper.GenerateTextbox(uc.LineId,"Status",uc.Status,"",true),
                                uc.Status,
                                "<center>"+"<i class='fa fa-paperclip' style='padding-right:5px;cursor:pointer;' onclick='ViewAttachment(this,"+uc.IMS3LineId+")'></i>"+((!uc.Status.Equals(clsImplementationEnum.IMSStatus.Draft.GetStringValue()) || uType == "Approver" )? HTMLActionString(uc.LineId,"","Delete","Delete Record","fa fa-trash-o","DeleteRecord("+ uc.LineId +");",true):HTMLActionString(uc.LineId,"","Delete","Delete Record","fa fa-trash-o","DeleteRecord("+ uc.LineId +");"))+"<a class='' href='javascript:void(0)' onclick=ShowTimeline('/IMS/MaintainIMS/ShowTimelineIssueLine?LineID="+Convert.ToInt32(uc.LineId)+"');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a></center>",
                           }).ToList();
                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult UpdateIssueLines(int lineId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string tableName = string.Empty;
                //tableName = "QMS021";
                if (!string.IsNullOrEmpty(columnName))
                {
                    if (columnName == "InstrumentNumber")
                    {
                        IMS011 objIMS011 = db.IMS011.Where(x => x.LineId == lineId).FirstOrDefault();
                        var RevNo = db.IMS002.Where(x => x.InstrumentNumber == columnValue && x.Location == objIMS011.Location && x.InstrumentGroup == objIMS011.InstrumentGroup && x.Active == "Yes").Select(x => x.RevNo).FirstOrDefault();
                        objIMS011.RevNo = RevNo;
                        db.SaveChanges();
                    }
                    else if (columnName == "ExpectedReturnDate")
                    {
                        var CalibrationDueDate = (from i11 in db.IMS011
                                                  join i2 in db.IMS002 on i11.InstrumentNumber equals i2.InstrumentNumber
                                                  where i11.LineId == lineId && i11.InstrumentGroup == i2.InstrumentGroup && i2.RevNo == i11.RevNo
                                                  select i2.CalibrationDueDate
                                                ).FirstOrDefault();

                        if (CalibrationDueDate != null && CalibrationDueDate.HasValue && Convert.ToDateTime(columnValue) > CalibrationDueDate)
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Expected return date must be less than equal to calibration due date!";
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }
                    }
                    var dbResult = db.SP_IMS_INSTRUMENT_ISSUE_LINE_UPDATE(lineId, columnName, columnValue).FirstOrDefault(); //SP_IMS_INSTRUMENT_ISSUE_LINE_UPDATE
                    //if (dbResult.Result.ToString() == "IS")
                    //{
                    //    objResponseMsg.Key = true;
                    //    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                    //}
                    //else
                    //{
                    //    objResponseMsg.Key = false;
                    //    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                    //}
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost, SessionExpireFilter]
        public ActionResult SaveInstrumentIssueLine(FormCollection fc)
        {
            int newRowIndex = 0;
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                //if (CheckDuplicateTestArea(fc["Location" + newRowIndex], fc["TestArea" + newRowIndex]))
                //{
                //    objResponseMsg.Key = false;
                //    objResponseMsg.Value = "Test Area with same location already exists";
                //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                //}
                IMS010 objIMS010 = new IMS010();
                int headerId = Convert.ToInt32(fc["HeaderId" + newRowIndex]);
                objIMS010 = db.IMS010.Where(x => x.HeaderId == headerId).FirstOrDefault();

                IMS011 objIMS011 = new IMS011();
                objIMS011.HeaderId = headerId;
                objIMS011.Location = objIMS010.Location;
                objIMS011.InstrumentRequestNumber = objIMS010.InstrumentRequestNumber;
                objIMS011.InstrumentGroup = fc["InstrumentGroup" + newRowIndex];
                objIMS011.InstrumentNumber = fc["InstrumentNumber" + newRowIndex];
                var RevNo = db.IMS002.Where(x => x.InstrumentNumber == objIMS011.InstrumentNumber && x.Location == objIMS011.Location && x.InstrumentGroup == objIMS011.InstrumentGroup && x.Active == "Yes").Select(x => x.RevNo).FirstOrDefault();
                objIMS011.RevNo = RevNo;
                string ExpectedReturnDate = fc["ExpectedReturnDate" + newRowIndex] != null ? fc["ExpectedReturnDate" + newRowIndex].ToString() : "";
                if (!string.IsNullOrWhiteSpace(ExpectedReturnDate))
                {
                    objIMS011.ExpectedReturnDate = DateTime.ParseExact(ExpectedReturnDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                else
                { objIMS011.ExpectedReturnDate = null; }
                objIMS011.Status = clsImplementationEnum.IMSStatus.Draft.GetStringValue();
                objIMS011.CreatedBy = objClsLoginInfo.UserName;
                objIMS011.CreatedOn = DateTime.Now;

                var objIMS002 = db.IMS002.Where(x => x.Location == objIMS011.Location && x.InstrumentNumber == objIMS011.InstrumentNumber && x.InstrumentGroup == objIMS011.InstrumentGroup && x.Active == "Yes").FirstOrDefault();
                if (objIMS002 == null)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Instrument Found For Save!";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (objIMS002.CalibrationDueDate == null)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Calibration Due Date Not Found!";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    else if (objIMS002.CalibrationDueDate < objIMS011.ExpectedReturnDate)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Expected return date must be less than equal to calibration due date!";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                }

                db.IMS011.Add(objIMS011);
                db.SaveChanges();


                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteIssueLine(int LineID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                IMS011 objIMS011 = db.IMS011.Where(x => x.LineId == LineID).FirstOrDefault();
                if (objIMS011 != null)
                {

                    db.IMS011.Remove(objIMS011);

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Issue line is deleted successfully";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult ApproveIssueRequest()
        {
            return View();
        }
        //[SessionExpireFilter, AllowAnonymous, UserPermissions]
        [HttpPost]
        public ActionResult ProcessIssueRequest(string status)
        {
            if (status.ToLower() == "pending")
            {
                ViewBag.Status = "Pending";
                return PartialView("ProcessIssueRequest");
            }
            else if (status.ToLower() == "receive")
            {
                ViewBag.Status = status;
                return PartialView("ProcessReturnRequest");
            }
            else
            {
                ViewBag.role = "Approver";
                ViewBag.Status = status;
                return PartialView("_IssueRequest");
            }

        }
        [HttpPost]
        public JsonResult LoadIssueRequestData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;
                var location = (from a in db.COM003
                                join b in db.COM002 on a.t_loca equals b.t_dimx
                                where b.t_dtyp == 1 && a.t_actv == 1
                                && a.t_psno.Equals(user, StringComparison.OrdinalIgnoreCase)
                                select b.t_dimx).FirstOrDefault();
                var userType = "Approver";
                string strWhere = string.Empty;
                strWhere = " 1=1 and ims11.Status in('" + clsImplementationEnum.IMSStatus.Submitted.GetStringValue() + "')"; ;
                string cond = string.Empty;
                string[] columnName = { "ims11.InstrumentRequestNumber", "ims11.InstrumentNumber", "ims10.SubmittedBy", "ims11.Location" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {

                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_IMS_GET_REQUEST_LINES
                                (
                               StartIndex,
                               EndIndex,
                               strSortOrder,
                               strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                            {
                             Convert.ToString(uc.LineId),
                             Convert.ToString(uc.InstrumentRequestNumber),
                             Convert.ToString(uc.InstrumentNumber),
                             Convert.ToString(uc.SubmittedBy),
                             Convert.ToString(uc.SubmittedOn),
                             uc.Location,
                             "<center><a class='' href='" +WebsiteURL +"/IMS/MaintainIMS/InstrumentIssueDetails?HeaderId="+uc.HeaderId+"&uType="+userType+"'><i style='' class='fa fa-eye'></i></a>"+"<a class='' href='javascript:void(0)' onclick=ShowTimeline('/IMS/MaintainIMS/ShowTimelineIssueLine?LineID="+Convert.ToInt32(uc.LineId)+"');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a></center>",
                             Convert.ToString(uc.LineId)
                         }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhere,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult IssueRequest(string strLineIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrayLineIds = strLineIds.Split(',').ToArray();
                var status = clsImplementationEnum.IMSStatus.IssuedAndAcknowledged.GetStringValue();
                for (int i = 0; i < arrayLineIds.Length; i++)
                {
                    int lineId = Convert.ToInt32(arrayLineIds[i]);
                    IMS011 objIMS011 = db.IMS011.Where(x => x.LineId == lineId).FirstOrDefault();
                    IMS002 objIMS002 = db.IMS002.Where(x => x.InstrumentNumber == objIMS011.InstrumentNumber && x.InstrumentGroup == objIMS011.InstrumentGroup && x.Location == objIMS011.Location && x.RevNo == objIMS011.RevNo && x.Active == "Yes").FirstOrDefault();
                    if (objIMS002 != null && !db.IMS011.Any(x => x.InstrumentNumber == objIMS011.InstrumentNumber && x.Location == objIMS011.Location && x.RevNo == objIMS011.RevNo && x.Status.Equals(status)))
                    {
                        objIMS011.Status = clsImplementationEnum.IMSStatus.IssuedAndAcknowledged.GetStringValue();
                        objIMS011.IssuedBy = objClsLoginInfo.UserName;
                        objIMS011.IssuedOn = DateTime.Now;
                        db.SaveChanges();
                        IMS010 objIMS010 = db.IMS010.Where(x => x.HeaderId == objIMS011.HeaderId).FirstOrDefault();

                        #region Send Notification
                        (new clsManager()).SendNotificationByLocation("Initiator", objIMS010.SubmittedBy, objIMS011.Location, "Instrument No. " + objIMS011.InstrumentNumber + " of Instrument Request: " + objIMS011.InstrumentRequestNumber + " has been issued to you", clsImplementationEnum.NotificationType.Information.GetStringValue());
                        #endregion

                    }
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Instruments Issued Successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please Try Again";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CloseRequest(string strLineIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrayLineIds = strLineIds.Split(',').ToArray();
                for (int i = 0; i < arrayLineIds.Length; i++)
                {
                    int lineId = Convert.ToInt32(arrayLineIds[i]);
                    IMS011 objIMS011 = db.IMS011.Where(x => x.LineId == lineId).FirstOrDefault();
                    objIMS011.Status = clsImplementationEnum.IMSStatus.Closed.GetStringValue();
                    db.SaveChanges();
                    var recStatus = clsImplementationEnum.IMSStatus.Received.GetStringValue();
                    var closeStatus = clsImplementationEnum.IMSStatus.Closed.GetStringValue();
                    if (!db.IMS011.Any(x => x.HeaderId == objIMS011.HeaderId && (!x.Status.Equals(recStatus) && !x.Status.Equals(closeStatus))))
                    {
                        IMS010 objIMS010 = db.IMS010.Where(x => x.HeaderId == objIMS011.HeaderId).FirstOrDefault();
                        objIMS010.Status = closeStatus;
                        var lstLines = db.IMS011.Where(y => y.HeaderId == objIMS010.HeaderId).ToList();
                        lstLines.ForEach(y => y.Status = closeStatus);
                        db.SaveChanges();
                    }
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Instruments Closed Successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please Try Again";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        //[SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult MaintainReturnRequest()
        {
            return View();
        }

        [HttpPost]
        public JsonResult LoadReturnRequestData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;
                var userType = "Initiator";
                var location = (from a in db.COM003
                                join b in db.COM002 on a.t_loca equals b.t_dimx
                                where b.t_dtyp == 1 && a.t_actv == 1
                                && a.t_psno.Equals(user, StringComparison.OrdinalIgnoreCase)
                                select b.t_dimx).FirstOrDefault();
                string strWhere = string.Empty;
                strWhere = " 1=1 and ims11.Status in('" + clsImplementationEnum.IMSStatus.IssuedAndAcknowledged.GetStringValue() + "')"; ;
                var userDept = db.COM003.Where(x => x.t_psno == user && x.t_actv == 1).Select(x => x.t_depc).FirstOrDefault();
                strWhere += "and ims10.MaintainedbyUsersDepartment = " + userDept;
                string cond = string.Empty;
                string[] columnName = { "ims11.InstrumentRequestNumber", "ims11.InstrumentNumber", "ims10.SubmittedBy", "ims11.Location" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {

                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_IMS_GET_REQUEST_LINES
                                (
                               StartIndex,
                               EndIndex,
                               strSortOrder,
                               strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                            {
                             Convert.ToString(uc.LineId),
                             Convert.ToString(uc.InstrumentRequestNumber),
                             Convert.ToString(uc.InstrumentNumber),
                             Convert.ToString(uc.SubmittedBy),
                             Convert.ToString(uc.SubmittedOn),
                             uc.Location,
                             "<center><a class='' href='"+ WebsiteURL +"/IMS/MaintainIMS/InstrumentIssueDetails?HeaderId="+uc.HeaderId+"&uType="+userType+"'><i style='' class='fa fa-eye'></i></a>"+"<a class='' href='javascript:void(0)' onclick=ShowTimeline('/IMS/MaintainIMS/ShowTimelineIssueLine?LineID="+Convert.ToInt32(uc.LineId)+"');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a></center>",
                             Convert.ToString(uc.LineId)
                         }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult ReturnRequest(string strLineIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrayLineIds = strLineIds.Split(',').ToArray();
                for (int i = 0; i < arrayLineIds.Length; i++)
                {
                    int lineId = Convert.ToInt32(arrayLineIds[i]);
                    IMS011 objIMS011 = db.IMS011.Where(x => x.LineId == lineId).FirstOrDefault();
                    objIMS011.Status = clsImplementationEnum.IMSStatus.Returned.GetStringValue();
                    objIMS011.ReturnedBy = objClsLoginInfo.UserName;
                    objIMS011.ReturnedOn = DateTime.Now;
                    db.SaveChanges();

                    #region Send Notification
                    (new clsManager()).SendNotificationByLocation(clsImplementationEnum.UserRoleName.QA3.GetStringValue(), "", objIMS011.Location, "Instrument No. " + objIMS011.InstrumentNumber + " of Instrument Request: " + objIMS011.InstrumentRequestNumber + " has been Returned", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IMS/MaintainIMS/ApproveIssueRequest");
                    #endregion

                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Instruments Returned Successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please Try Again";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //[SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult ProcessReturnRequest()
        {
            return View();
        }

        [HttpPost]
        public JsonResult LoadReceiveRequestData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;
                var location = (from a in db.COM003
                                join b in db.COM002 on a.t_loca equals b.t_dimx
                                where b.t_dtyp == 1 && a.t_actv == 1
                                && a.t_psno.Equals(user, StringComparison.OrdinalIgnoreCase)
                                select b.t_dimx).FirstOrDefault();
                var userType = "Approver";
                string strWhere = string.Empty;
                strWhere = " 1=1 and ims11.Status in('" + clsImplementationEnum.IMSStatus.Returned.GetStringValue() + "')"; ;
                string cond = string.Empty;
                string[] columnName = { "ims11.InstrumentRequestNumber", "ims11.InstrumentNumber", "ims10.SubmittedBy", "ims11.Location" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {

                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_IMS_GET_REQUEST_LINES
                                (
                               StartIndex,
                               EndIndex,
                               strSortOrder,
                               strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                            {
                             Convert.ToString(uc.LineId),
                             Convert.ToString(uc.InstrumentRequestNumber),
                             Convert.ToString(uc.InstrumentNumber),
                             Convert.ToString(uc.SubmittedBy),
                             Convert.ToString(uc.SubmittedOn),
                             uc.Location,
                             "<center>"+"<a href='javascript:void(0)'><i class='fa fa-paperclip' style='padding-right:5px;' onclick='LoadFileUploadReceive("+uc.LineId+")'></i></a>"+"<a class='' href='"+ WebsiteURL +"/IMS/MaintainIMS/InstrumentIssueDetails?HeaderId="+uc.HeaderId+"&uType="+userType+"'><i style='' class='fa fa-eye'></i></a>"+"<a class='' href='javascript:void(0)' onclick=ShowTimeline('/IMS/MaintainIMS/ShowTimelineIssueLine?LineID="+Convert.ToInt32(uc.LineId)+"');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a></center>",
                             Convert.ToString(uc.LineId)
                         }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult ReceiveRequest(string strLineIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrayLineIds = strLineIds.Split(',').ToArray();
                for (int i = 0; i < arrayLineIds.Length; i++)
                {
                    int lineId = Convert.ToInt32(arrayLineIds[i]);
                    IMS011 objIMS011 = db.IMS011.Where(x => x.LineId == lineId).FirstOrDefault();
                    objIMS011.Status = clsImplementationEnum.IMSStatus.Received.GetStringValue();
                    objIMS011.ReceivedBy = objClsLoginInfo.UserName;
                    objIMS011.ReceivedOn = DateTime.Now;
                    db.SaveChanges();

                    IMS010 objIMS010 = db.IMS010.Where(x => x.HeaderId == objIMS011.HeaderId).FirstOrDefault();

                    #region Send Notification
                    (new clsManager()).SendNotificationByLocation("Initiator", objIMS010.SubmittedBy, objIMS011.Location, "Instrument No. " + objIMS011.InstrumentNumber + " of Instrument Request: " + objIMS011.InstrumentRequestNumber + " has been Received", clsImplementationEnum.NotificationType.Information.GetStringValue());
                    #endregion


                    var recStatus = clsImplementationEnum.IMSStatus.Received.GetStringValue();
                    var closeStatus = clsImplementationEnum.IMSStatus.Closed.GetStringValue();
                    if (!db.IMS011.Any(x => x.HeaderId == objIMS011.HeaderId && (!x.Status.Equals(recStatus) && !x.Status.Equals(closeStatus))))
                    {
                        objIMS010.Status = closeStatus;
                        var lstLines = db.IMS011.Where(y => y.HeaderId == objIMS010.HeaderId).ToList();
                        lstLines.ForEach(y => y.Status = closeStatus);
                        db.SaveChanges();
                    }

                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Instruments Received Successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please Try Again";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult ReCallNotification()
        {
            string location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objClsLoginInfo.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            ViewBag.location = location;
            string status = clsImplementationEnum.IMSStatus.IssuedAndAcknowledged.GetStringValue();
            //var issuedInstumentList = db.IMS011.Where(x => x.Status.Equals(status) && x.Location == objClsLoginInfo.Location).OrderBy(x => x.InstrumentNumber).Select(x=>x.InstrumentNumber).Distinct().ToList();
            var issuedInstumentList = (from a in db.IMS011
                                       join b in db.IMS002
                                       on new { a.InstrumentNumber, a.Location, a.RevNo } equals new { b.InstrumentNumber, b.Location, b.RevNo }
                                       orderby a.InstrumentNumber
                                       where a.Status.Equals(status) && a.Location == objClsLoginInfo.Location
                                       select new { Id = a.InstrumentNumber, Desc = a.InstrumentNumber + " - " + b.InstrumentDescription }
                                       ).Distinct().ToList();
            ViewBag.instrumentList = new SelectList(issuedInstumentList, "Id", "Desc");
            return View();
        }
        [HttpPost, SessionExpireFilter]
        public JsonResult LoadIssuedInstrumentResult(JQueryDataTableParamModel param, string from, string to)
        {
            try
            {
                if (!(string.IsNullOrEmpty(from) || string.IsNullOrEmpty(to)))
                {
                    //var a = Request["from"];
                    //var b = Request["to"];
                    var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                    var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                    int StartIndex = param.iDisplayStart + 1;
                    int EndIndex = param.iDisplayStart + param.iDisplayLength;
                    string status = clsImplementationEnum.IMSStatus.IssuedAndAcknowledged.GetStringValue();
                    string strWhere = "1=1";
                    strWhere += " and ims11.Status = '" + status + "' and ims11.InstrumentNumber between '" + from + "' and '" + to + "' and ims02.Active = 'Yes' and ims11.Location = '" + objClsLoginInfo.Location + "'";
                    string[] columnName = { "ims11.InstrumentNumber", "ims11.IssuedBy", };
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);

                    string strSortOrder = string.Empty;
                    string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                    string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                    if (!string.IsNullOrWhiteSpace(sortColumnName))
                    {

                        strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                    }

                    var lstResult = db.SP_IMS_GET_ISSUED_INSTRUMENTS
                                    (
                                   StartIndex,
                                   EndIndex,
                                   strSortOrder,
                                   strWhere
                                    ).ToList();
                    var data = (from uc in lstResult
                                select new[]
                                {
                                    Convert.ToString(uc.LineId),
                                    uc.InstrumentNumber,
                                    uc.IssuedBy,
                           }).ToList();
                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                        iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                        aaData = data,
                        strSortOrder = strSortOrder,
                        whereCondition = strWhere
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var data = new List<object>();
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalRecords = "0",
                        iTotalDisplayRecords = "0",
                        aaData = data
                    }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SendRecallEmail(string FromInstrumentNumber, string ToInstrumentNumber)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (!(string.IsNullOrEmpty(FromInstrumentNumber) || string.IsNullOrEmpty(ToInstrumentNumber)))
                {
                    var today = DateTime.Now;
                    string status = clsImplementationEnum.IMSStatus.IssuedAndAcknowledged.GetStringValue();
                    var issuedInstumentList = db.IMS011.Where(x => x.Status.Equals(status) && x.Location == objClsLoginInfo.Location && x.InstrumentNumber.CompareTo(FromInstrumentNumber) >= 0 && x.InstrumentNumber.CompareTo(ToInstrumentNumber) <= 0).ToList();
                    foreach (var ims11 in issuedInstumentList)
                    {
                        var ims02 = db.IMS002.Where(x => x.Active == "Yes" && x.InstrumentNumber == ims11.InstrumentNumber && x.Location == ims11.Location && x.RevNo == ims11.RevNo && x.CalibrationDueDate < today).FirstOrDefault();
                        if (ims02 != null)
                        {
                            Hashtable _ht = new Hashtable();
                            EmailSend _objEmail = new EmailSend();
                            MAIL001 objTemplateMaster;
                            _ht["[IssuedTo]"] = Manager.GetUserNameFromPsNo(ims11.IssuedBy);
                            _ht["[IssuedOn]"] = ims11.IssuedOn;
                            _ht["[InstrumentNo]"] = ims11.InstrumentNumber;
                            _ht["[ExpectedReturnDate]"] = ims11.ExpectedReturnDate;
                            _objEmail.MailToAdd = Manager.GetMailIdFromPsNo(ims11.IssuedBy);
                            objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.IMS.RecallNotification).SingleOrDefault();
                            _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "Email sent successfully";
                        }
                        else
                        {
                            if (ims11.ExpectedReturnDate < today)
                            {
                                Hashtable _ht = new Hashtable();
                                EmailSend _objEmail = new EmailSend();
                                MAIL001 objTemplateMaster;
                                _ht["[IssuedTo]"] = Manager.GetUserNameFromPsNo(ims11.IssuedBy);
                                _ht["[IssuedOn]"] = ims11.IssuedOn;
                                _ht["[InstrumentNo]"] = ims11.InstrumentNumber;
                                _ht["[ExpectedReturnDate]"] = ims11.ExpectedReturnDate;
                                _objEmail.MailToAdd = Manager.GetMailIdFromPsNo(ims11.IssuedBy);
                                objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.IMS.RecallNotification).SingleOrDefault();
                                _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                                objResponseMsg.Key = true;
                                objResponseMsg.Value = "Email sent successfully";
                            }
                        }

                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please Select Instrument Number";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public string SelectInstrumentNumber(int rowId, string selectedValue, string Numbergroup, string loc, bool Disabled = false, string OnChangeEvent = "", string ColumnName = "")
        {
            clsManager objManager = new clsManager();
            string htmlOption = "<option value=''> Select Instrument No </option>";

            //List<DropdownModel> lstNoGroup = Manager.GetSubCatagories("Instrument Group", BU, objClsLoginInfo.Location).Select(i => new DropdownModel { Code = i.Code, Description = i.Code + " - " + i.Description }).ToList();
            List<DropdownModel> lstInstNo = (from a in db.IMS002
                                             where a.Location == loc && a.InstrumentGroup == Numbergroup //&& a.Active == "yes"
                                             select new DropdownModel { Code = a.InstrumentNumber, Description = a.InstrumentNumber + " - " + a.InstrumentDescription }).Distinct().ToList();
            foreach (var item in lstInstNo)
            {
                if (item.Code == selectedValue)
                {
                    htmlOption += "<option selected value=" + item.Code + ">" + item.Description + "</option>";
                }
                else
                {
                    htmlOption += "<option value='" + item.Code + "'>" + item.Description + "</option>";
                }
            }
            string inputID = ColumnName + "" + rowId.ToString();

            return "<select id=\"" + inputID + "\" colname=\"" + ColumnName + "\" name=\"" + inputID + "\" " + (Disabled ? " disabled" : "") + (OnChangeEvent != string.Empty ? " onchange=\"" + OnChangeEvent + "\"" : "") + " class='form-control input-sm input-inline'>" + htmlOption + "</select>";

        }
        [HttpPost]
        public ActionResult GetInstrumentNumber(string loc, string InstrumentGroup)
        {
            var ims002 = db.IMS002.Where(x => x.Location == loc && x.InstrumentGroup == InstrumentGroup && x.Active == "Yes" && x.BlockedforCalibration == "No").ToList();
            List<IMS002> instruments = new List<IMS002>();
            var status = clsImplementationEnum.IMSStatus.IssuedAndAcknowledged.GetStringValue();
            foreach (var a in ims002)
            {
                if (!db.IMS011.Any(x => x.Location == a.Location && x.InstrumentNumber == a.InstrumentNumber && x.RevNo == a.RevNo && x.InstrumentGroup == a.InstrumentGroup && x.Status.Equals(status)))
                {
                    instruments.Add(a);
                }
            }
            var list = instruments.Select(x => new BULocWiseCategoryModel { CatDesc = x.InstrumentNumber + " - " + x.InstrumentDescription, CatID = x.InstrumentNumber }).ToList();
            //var lstInstrumentNumbers = (from a in db.IMS002
            //                            where a.Location == loc && a.InstrumentGroup == InstrumentGroup && a.Active == "Yes" && a.BlockedforCalibration == "No"
            //                            select new { InstrumentNo = a.InstrumentNumber, InstrumentDesc = a.InstrumentDescription }).Distinct().ToList();


            //var list = lstInstrumentNumbers.Select(x => new BULocWiseCategoryModel { CatDesc = x.InstrumentNo +" - "+x.InstrumentDesc, CatID = x.InstrumentNo }).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetInstrumentDetails(string loc, string InstrumentNumber, string InstrumentGroup)
        {
            var objCalibrationDueDate = db.IMS002.Where(a => a.Location == loc && a.InstrumentNumber == InstrumentNumber && a.InstrumentGroup == InstrumentGroup && a.Active == "Yes").FirstOrDefault().CalibrationDueDate;
            var CalibrationDueDate = objCalibrationDueDate.HasValue ? objCalibrationDueDate.Value.ToString("dd/MM/yyyy") : "";
            var lstInstrumentNumbers = (from a in db.IMS002
                                        where a.Location == loc && a.InstrumentNumber == InstrumentNumber && a.InstrumentGroup == InstrumentGroup && a.Active == "Yes"
                                        select new { FromRange = a.FromRange, ToRange = a.ToRange, LeastCount = a.LeastCount, LeastCountUnit = a.LeastCountUnit, CalibrationDueDate = CalibrationDueDate }).FirstOrDefault();

            //var list = lstInstrumentNumbers.Select(x => new BULocWiseCategoryModel { CatDesc = x.InstrumentNo + " - " + x.InstrumentDesc, CatID = x.InstrumentNo }).ToList();
            return Json(lstInstrumentNumbers, JsonRequestBehavior.AllowGet);
        }
        public string getCategoryDesc(string Key, string BU, string loc, string code)
        {
            string catDesc = (from glb002 in db.GLB002
                              join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                              where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true && BU.Contains(glb002.BU) && loc.Equals(glb002.Location) && code.Equals(glb002.Code)
                              select glb002.Code + "-" + glb002.Description).FirstOrDefault();
            return catDesc;
        }
        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", bool disabled = false)
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (disabled)
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;opacity: 0.5;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";
            }
            else
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";

            return htmlControl;
        }
        [NonAction]
        public static string GenerateGridButton(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";


            htmlControl = "<a id='" + inputID + "' name='" + inputID + "' class='btn btn-outline btn-circle btn-sm blue' " + onClickEvent + " ><i class='" + className + "'></i> " + buttonName + "</a>";

            //htmlControl = "<i  data-modal='' id='" + inputID + "' name='" + inputID + "' style='cursor:Pointer;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";

            return htmlControl;
        }
        public string GenerateAutoCompleteonBlur(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string maxlength = "")
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onchange='" + onBlurMethod + "'" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' hdElement='" + hdElementId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + "  " + MaxCharcters + "  " + (disabled ? "disabled" : "") + " />";

            return strAutoComplete;
        }

        public ActionResult ShowTimelineInstrument(int HeaderID)
        {
            TimelineViewModel model = new TimelineViewModel();

            IMS002 objIMS002 = db.IMS002.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
            model.Title = "IMS Instrument";
            model.CreatedBy = objIMS002.CreatedBy != null ? Manager.GetUserNameFromPsNo(objIMS002.CreatedBy) : null;
            model.CreatedOn = objIMS002.CreatedOn;
            model.EditedBy = objIMS002.EditedBy != null ? Manager.GetUserNameFromPsNo(objIMS002.EditedBy) : null;
            model.EditedOn = objIMS002.EditedOn;

            return PartialView("_ShowTimeline", model);
        }
        public ActionResult ShowTimelineTestResult(int LineID)
        {
            TimelineViewModel model = new TimelineViewModel();

            IMS003 objIMS003 = db.IMS003.Where(x => x.LineId == LineID).FirstOrDefault();
            model.Title = "IMS TestResult";
            model.CreatedBy = objIMS003.CreatedBy != null ? Manager.GetUserNameFromPsNo(objIMS003.CreatedBy) : null;
            model.CreatedOn = objIMS003.CreatedOn;
            model.EditedBy = objIMS003.EditedBy != null ? Manager.GetUserNameFromPsNo(objIMS003.EditedBy) : null;
            model.EditedOn = objIMS003.EditedOn;

            return PartialView("_ShowTimeline", model);
        }
        public ActionResult ShowTimelineIssueHeader(int HeaderID)
        {
            TimelineViewModel model = new TimelineViewModel();

            IMS010 objIMS010 = db.IMS010.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
            model.Title = "IMS IssueHeader";
            model.CreatedBy = objIMS010.CreatedBy != null ? Manager.GetUserNameFromPsNo(objIMS010.CreatedBy) : null;
            model.CreatedOn = objIMS010.CreatedOn;
            model.EditedBy = objIMS010.EditedBy != null ? Manager.GetUserNameFromPsNo(objIMS010.EditedBy) : null;
            model.EditedOn = objIMS010.EditedOn;
            model.SubmittedBy = objIMS010.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objIMS010.SubmittedBy) : null;
            model.SubmittedOn = objIMS010.SubmittedOn;

            return PartialView("_ShowTimeline", model);
        }
        public ActionResult ShowTimelineIssueLine(int LineID)
        {
            TimelineViewModel model = new TimelineViewModel();

            IMS011 objIMS011 = db.IMS011.Where(x => x.LineId == LineID).FirstOrDefault();
            model.Title = "IMS IssueLine";
            model.CreatedBy = objIMS011.CreatedBy != null ? Manager.GetUserNameFromPsNo(objIMS011.CreatedBy) : null;
            model.CreatedOn = objIMS011.CreatedOn;
            model.EditedBy = objIMS011.EditedBy != null ? Manager.GetUserNameFromPsNo(objIMS011.EditedBy) : null;
            model.EditedOn = objIMS011.EditedOn;
            model.ReceivedBy = objIMS011.ReceivedBy != null ? Manager.GetUserNameFromPsNo(objIMS011.ReceivedBy) : null;
            model.ReceivedOn = objIMS011.ReceivedOn;
            model.ReturnedBy = objIMS011.ReturnedBy != null ? Manager.GetUserNameFromPsNo(objIMS011.ReturnedBy) : null;
            model.ReturnedOn = objIMS011.ReturnedOn;
            model.IssuedBy = objIMS011.IssuedBy != null ? Manager.GetUserNameFromPsNo(objIMS011.IssuedBy) : null;
            model.IssuedOn = objIMS011.IssuedOn;

            return PartialView("_ShowTimeline", model);
        }

        #region Export Excel
        // Export Functionality
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "", int? HeaderId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                //used for View : Index
                if (gridType == clsImplementationEnum.GridType.TestArea.GetStringValue())
                {

                    string location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objClsLoginInfo.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();

                    var lst = db.SP_IMS_GET_TEST_AREA_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      ROW_NO = Convert.ToString(uc.ROW_NO),
                                      Location = Convert.ToString(db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == uc.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault()) + "" + Helper.GenerateHidden(uc.Id, "Location", uc.Location),
                                      TestArea = uc.TestArea
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                //used for View :MaintainInstrumentIssue (return instrument)
                if (gridType == clsImplementationEnum.GridType.MaintainReturnInstrumentIssue.GetStringValue())
                {
                    var lst = db.SP_IMS_GET_REQUEST_LINES(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      InstrumentRequestNumber = Convert.ToString(uc.InstrumentRequestNumber),
                                      InstrumentNumber = Convert.ToString(uc.InstrumentNumber),
                                      SubmittedBy = Convert.ToString(uc.SubmittedBy),
                                      SubmittedOn = Convert.ToString(uc.SubmittedOn),
                                      Location = uc.Location,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                //used for View : ReCallNotification 
                if (gridType == clsImplementationEnum.GridType.RecallNotification.GetStringValue())
                {
                    string location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objClsLoginInfo.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();

                    var lst = db.SP_IMS_GET_ISSUED_INSTRUMENTS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      InstrumentNumber = uc.InstrumentNumber,
                                      IssuedBy = uc.IssuedBy,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                //used for View : ApproveIssueRequest 
                if (gridType == clsImplementationEnum.GridType.ProcessInstrumentIssue.GetStringValue())
                {

                    var lst = db.SP_IMS_GET_INSTRUMENT_ISSUE_HEADER(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Location = Convert.ToString(uc.Location),
                                      InstrumentRequestNumber = Convert.ToString(uc.InstrumentRequestNumber),
                                      MaintainedbyUsersDepartment = Convert.ToString(getTestAreaDesc(uc.MaintainedbyUsersDepartment)),
                                      Remarks = Convert.ToString(uc.Remarks),
                                      Status = Convert.ToString(uc.Status),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                //process issue request
                if (gridType == clsImplementationEnum.GridType.ProcessIssueRequest.GetStringValue())
                {
                    var lst = db.SP_IMS_GET_REQUEST_LINES(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      InstrumentRequestNumber = Convert.ToString(uc.InstrumentRequestNumber),
                                      InstrumentNumber = Convert.ToString(uc.InstrumentNumber),
                                      SubmittedBy = Convert.ToString(uc.SubmittedBy),
                                      SubmittedOn = Convert.ToString(uc.SubmittedOn),
                                      Location = uc.Location,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                //return process issue request
                if (gridType == clsImplementationEnum.GridType.ReturnInstrumentIssue.GetStringValue())
                {
                    var lst = db.SP_IMS_GET_REQUEST_LINES(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      InstrumentRequestNumber = Convert.ToString(uc.InstrumentRequestNumber),
                                      InstrumentNumber = Convert.ToString(uc.InstrumentNumber),
                                      SubmittedBy = Convert.ToString(uc.SubmittedBy),
                                      SubmittedOn = Convert.ToString(uc.SubmittedOn),
                                      Location = uc.Location,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }



                //used for View : MaintainInstrument 
                if (gridType == clsImplementationEnum.GridType.MaintainCalibration.GetStringValue())
                {
                    var lst = db.SP_IMS_GET_INSTRUMENT_HEADER(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Location = Convert.ToString(uc.Location),
                                      InstrumentNumber = Convert.ToString(uc.InstrumentNumber),
                                      InstrumentDescription = Convert.ToString(uc.InstrumentDescription),
                                      InstrumentGroup = Convert.ToString(uc.InstrumentGroup),
                                      TestArea = Convert.ToString(getTestAreaDesc(uc.TestArea)),
                                      LastCalibrationDate = uc.LastCalibrationDate == null ? "" : Convert.ToDateTime(uc.LastCalibrationDate).ToString("dd/MM/yyyy"),
                                      CalibrationDueDate = uc.CalibrationDueDate == null ? "" : Convert.ToDateTime(uc.CalibrationDueDate).ToString("dd/MM/yyyy"),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                //used for View : InstrumentIssueDetails  
                if (gridType == clsImplementationEnum.GridType.InstrumentIssueLine.GetStringValue())
                {
                    var objIMS010 = db.IMS010.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    var BU = db.ATH001.Where(x => x.Employee == objClsLoginInfo.UserName && x.Location == objIMS010.Location).Select(x => x.BU).FirstOrDefault();

                    string location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objClsLoginInfo.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                    var lst = db.SP_IMS_GET_INSTRUMENT_ISSUE_LINES(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      InstrumentGroup = getCategoryDesc("Instrument Group", BU, objIMS010.Location, uc.InstrumentGroup),
                                      InstrumentNumber = uc.InstrumentNumber,
                                      FromRange = Convert.ToString(uc.FromRange),
                                      ToRange = Convert.ToString(uc.ToRange),
                                      LeastCount = Convert.ToString(uc.LeastCount),
                                      LeastCountUnit = Convert.ToString(uc.LeastCountUnit),
                                      ExpectedReturnDate = Convert.ToDateTime(uc.ExpectedReturnDate).ToString("dd/MM/yyyy"),
                                      Status = uc.Status,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                //used for View : InstrumentDetails Calibration Line
                if (gridType == clsImplementationEnum.GridType.InstrumentCalibrationLine.GetStringValue())
                {

                    var lst = db.SP_IMS_GET_CALIBRATION_TEST_RESULT(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      LastCalibrationDate = uc.LastCalibrationDate == null ? "" : Convert.ToDateTime(uc.LastCalibrationDate).ToString("dd/MM/yyyy"),
                                      CalibrationDueDate = uc.CalibrationDueDate == null ? "" : Convert.ToDateTime(uc.CalibrationDueDate).ToString("dd/MM/yyyy"),
                                      CalibrationReportSummary = uc.CalibrationReportSummary,
                                      Unblock = uc.Unblock,
                                      Remarks = uc.Remarks
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                //

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Import Excel
        //Observation 15878 implement by Ajay Chauhan on 31-07-2018
        public FileResult DownloadSample()
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(Server.MapPath("~/Areas/IMS/Views/MaintainIMS/IMSTemplate.xlsx"));
            string fileName = " UploadTemplate.xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public ActionResult ReadExcel()
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            if (Request.Files.Count == 0)
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please select a file first!";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            HttpPostedFileBase upload = Request.Files[0];
            if (Path.GetExtension(upload.FileName) == ".xlsx" || Path.GetExtension(upload.FileName) == ".xls")
            {
                ExcelPackage package = new ExcelPackage(upload.InputStream);
                DataTable dt = Manager.ExcelToDataTable(package);
                return validateAndSave(dt);
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please upload valid excel file!";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult validateAndSave(DataTable dt)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            bool hasError = false;
            var TestArea = string.Empty;
            var Location = string.Empty;

            dt.Columns.Add("CalibrationDueDate", typeof(DateTime));
            dt.Columns.Add("RevNo", typeof(Int32));
            try
            {
                if (!dt.Columns.Contains("Error"))
                {
                    dt.Columns.Add("Error");
                }
                List<string> isActive = new List<string>();
                isActive.Add(clsImplementationEnum.DGSDiagram.Yes.GetStringValue());
                isActive.Add(clsImplementationEnum.DGSDiagram.No.GetStringValue());

                var user = objClsLoginInfo.UserName;
                var lstIMS002 = db.IMS002.ToList();
                var lstIMS001 = db.IMS001.ToList();
                var BU = db.ATH001.Where(x => x.Employee == user && x.Location == objClsLoginInfo.Location).Select(x => x.BU).FirstOrDefault();
                var InstrumentGroup = Manager.GetSubCatagories("Instrument Group", BU, objClsLoginInfo.Location).Select(x => x.Code).ToList();
                var LeastCountUnit = Manager.GetSubCatagories("Least Count Unit", BU, objClsLoginInfo.Location).Select(x => x.Code).ToList();
                var CalibrationIntervalType = Manager.GetSubCatagories("Calibration Interval Type", BU, objClsLoginInfo.Location).Select(x => x.Code).ToList();
                var CalibrationProgram = Manager.GetSubCatagories("Calibration Program", BU, objClsLoginInfo.Location).Select(x => x.Code).ToList();
                TestArea = db.COM003.Where(x => x.t_psno == user && x.t_actv == 1).Select(x => x.t_depc).FirstOrDefault();

                Location = (from a in db.COM003
                            join b in db.COM002 on a.t_loca equals b.t_dimx
                            where b.t_dtyp == 1 && a.t_actv == 1
                            && a.t_psno.Equals(user, StringComparison.OrdinalIgnoreCase)
                            select b.t_dimx).FirstOrDefault();

                foreach (DataRow item in dt.Rows)
                {
                    int fromRange = 0;
                    int toRange = 0;
                    int leastCount = 0;
                    int calibInterval = 0;

                    List<string> errors = new List<string>();
                    if (!lstIMS001.Any(x => x.TestArea == TestArea && x.Location == Location))
                    {
                        errors.Add("Your department code doesn’t exist in Test Area, Pls maintain"); hasError = true;
                    }

                    //Instrument Number
                    if (string.IsNullOrEmpty(item.Field<string>(0)))
                    {
                        errors.Add("Instrument Number is Mandatory"); hasError = true;
                    }
                    else
                    {
                        var isactive = clsImplementationEnum.DGSDiagram.Yes.GetStringValue();
                        var isnotactive = clsImplementationEnum.DGSDiagram.No.GetStringValue();
                        if (lstIMS002.Any(x => x.InstrumentNumber == item.Field<string>(0) && x.Location == Location && x.Active == isactive))
                        {
                            errors.Add("Instrument Number already exist for this location"); hasError = true;
                        }
                        else
                        {
                            //logic for revision number
                            var revNo = 0;
                            if (lstIMS002.Any(x => x.InstrumentNumber == item.Field<string>(0) && x.Location == Location && x.Active == isnotactive))
                            {
                                var objMIS002 = lstIMS002.Where(x => x.InstrumentNumber == item.Field<string>(0) && x.Location == Location && x.Active == isnotactive).FirstOrDefault();
                                revNo = Convert.ToInt32(objMIS002.RevNo) + 1;
                                item.SetField<int>(16, revNo);
                            }
                            else
                            {
                                item.SetField<int>(16, revNo);
                            }
                        }
                    }

                    //Instrument Description
                    if (string.IsNullOrEmpty(item.Field<string>(1)))
                    { errors.Add("Instrument Description is Mandatory"); hasError = true; }

                    //Instrument Group
                    if (string.IsNullOrEmpty(item.Field<string>(2)))
                    {
                        errors.Add("Instrument Group is Mandatory");
                        hasError = true;
                    }
                    else
                    {
                        if (!InstrumentGroup.Contains(item.Field<string>(2)))
                        {
                            errors.Add("Invalid Instrument Group"); hasError = true;
                        }
                    }

                    //From Range
                    if (string.IsNullOrEmpty(item.Field<string>(3)))
                    {
                        errors.Add("From Range is Mandatory"); hasError = true;
                    }
                    else
                    {
                        if (!int.TryParse(item.Field<string>(3), out fromRange))
                        {
                            errors.Add("From Range must be numeric"); hasError = true;
                        }
                    }

                    //To Range
                    if (string.IsNullOrEmpty(item.Field<string>(4)))
                    {
                        errors.Add("To Range is Mandatory"); hasError = true;
                    }
                    else
                    {
                        if (!int.TryParse(item.Field<string>(4), out toRange))
                        {
                            errors.Add("To Range must be numeric"); hasError = true;
                        }
                    }

                    //Lease Count
                    if (string.IsNullOrEmpty(item.Field<string>(5)))
                    {
                        errors.Add("Least Count is Mandatory"); hasError = true;
                    }
                    else
                    {
                        if (!int.TryParse(item.Field<string>(5), out leastCount))
                        {
                            errors.Add("Least Count must be numeric"); hasError = true;
                        }
                    }
                    if (string.IsNullOrEmpty(item.Field<string>(6)))
                    {
                        errors.Add("Least Count Unit is Mandatory"); hasError = true;
                    }
                    else
                    {
                        if (!LeastCountUnit.Contains(item.Field<string>(6)))
                        {
                            errors.Add("Invalid Least Count Unit"); hasError = true;
                        }
                    }
                    if (string.IsNullOrEmpty(item.Field<string>(7)))
                    {
                        errors.Add("Calibration Interval Type is Mandatory"); hasError = true;
                    }
                    else
                    {
                        if (!CalibrationIntervalType.Contains(item.Field<string>(7)))
                        {
                            errors.Add("Invalid Calibration Interval Type"); hasError = true;
                        }
                    }
                    if (string.IsNullOrEmpty(item.Field<string>(8)))
                    {
                        errors.Add("Calibration Interval is Mandatory"); hasError = true;
                    }
                    else
                    {
                        if (!int.TryParse(item.Field<string>(8), out calibInterval) && calibInterval < 0)
                        {
                            errors.Add("Invalid Calibration Interval, it must be numeric"); hasError = true;
                        }
                        else if (int.TryParse(item.Field<string>(8), out calibInterval) && calibInterval < 0)
                        {
                            errors.Add("Calibration Interval must be positive number"); hasError = true;
                        }
                    }

                    if (string.IsNullOrEmpty(item.Field<string>(9)))
                    {
                        errors.Add("Active is Mandatory"); hasError = true;
                    }
                    else
                    {
                        if (!isActive.Contains(item.Field<string>(9)))
                        {
                            errors.Add("Invalid Active Option"); hasError = true;
                        }
                        else
                        {
                            //Line level validation
                            var isActiveYes = item.Field<string>(9);
                            if (isActiveYes == clsImplementationEnum.DGSDiagram.Yes.GetStringValue())
                            {
                                if (string.IsNullOrEmpty(item.Field<string>(11)))
                                {
                                    errors.Add("Last Calibration Date is Mandatory"); hasError = true;
                                }

                                if (string.IsNullOrEmpty(item.Field<string>(13)))
                                { errors.Add("Unblock is Mandatory"); hasError = true; }
                                else
                                {
                                    if (!isActive.Contains(item.Field<string>(13)))
                                    {
                                        errors.Add("Invalid Unblock Option"); hasError = true;
                                    }
                                }
                            }
                        }
                    }

                    if (string.IsNullOrEmpty(item.Field<string>(10)))
                    {
                        errors.Add("Calibration Program is Mandatory"); hasError = true;
                    }
                    else
                    {
                        if (!CalibrationProgram.Contains(item.Field<string>(10)))
                        {
                            errors.Add("Invalid Calibration Program"); hasError = true;
                        }
                    }

                    if (errors.Count > 0)
                    {
                        item.SetField<string>(20, string.Join(",", errors.ToArray()));
                    }
                }

                if (!hasError)
                {
                    List<IMS002> lstObjIMS002 = new List<IMS002>();
                    foreach (DataRow item in dt.Rows)
                    {
                        int fromRange = 0;
                        int toRange = 0;
                        int leastCount = 0;
                        int calibInterval = 0;

                        int.TryParse(item.Field<string>(3), out fromRange);
                        int.TryParse(item.Field<string>(4), out toRange);
                        int.TryParse(item.Field<string>(5), out leastCount);
                        int.TryParse(item.Field<string>(8), out calibInterval);

                        IMS002 objIMS002 = new IMS002();
                        objIMS002.Location = Location;
                        objIMS002.InstrumentNumber = item.Field<string>(0);
                        objIMS002.InstrumentDescription = item.Field<string>(1);
                        objIMS002.InstrumentGroup = item.Field<string>(2);
                        objIMS002.FromRange = fromRange;
                        objIMS002.ToRange = toRange;
                        objIMS002.LeastCount = leastCount;
                        objIMS002.LeastCountUnit = item.Field<string>(6);
                        objIMS002.TestArea = TestArea;
                        objIMS002.CalibrationInterval = calibInterval;
                        objIMS002.CalibrationIntervalType = item.Field<string>(7);
                        objIMS002.BlockedforCalibration = clsImplementationEnum.DGSDiagram.No.GetStringValue();
                        objIMS002.Active = item.Field<string>(9);
                        objIMS002.CalibrationProgram = item.Field<string>(10);
                        objIMS002.RevNo = Convert.ToInt32(item.Field<int>(16));
                        objIMS002.CreatedOn = DateTime.Now;
                        objIMS002.CreatedBy = objClsLoginInfo.UserName;

                        #region Line insert data
                        IMS003 objIMS003 = null;
                        var isActiveYes = item.Field<string>(9);
                        if (isActiveYes == clsImplementationEnum.DGSDiagram.Yes.GetStringValue())
                        {
                            objIMS003 = new IMS003();
                            objIMS003.Location = Location;
                            objIMS003.InstrumentNumber = objIMS002.InstrumentNumber;
                            objIMS003.LastCalibrationDate = Convert.ToDateTime(DateTime.ParseExact(item.Field<string>(11), "dd/MM/yyyy", CultureInfo.InvariantCulture));
                            if (objIMS002.CalibrationIntervalType == "Days")
                            {
                                objIMS003.CalibrationDueDate = objIMS003.LastCalibrationDate.Value.AddDays(Convert.ToInt32(objIMS002.CalibrationInterval));

                                objIMS002.LastCalibrationDate = objIMS003.LastCalibrationDate;
                                objIMS002.CalibrationDueDate = objIMS003.CalibrationDueDate;
                            }
                            objIMS003.CalibrationReportSummary = item.Field<string>(12);
                            objIMS003.Unblock = item.Field<string>(13);
                            objIMS003.Remarks = item.Field<string>(14);
                            objIMS003.CreatedOn = DateTime.Now;
                            objIMS003.CreatedBy = objClsLoginInfo.UserName;
                        }

                        if (objIMS003 != null)
                        {
                            objIMS002.IMS003.Add(objIMS003);
                        }
                        #endregion

                        lstObjIMS002.Add(objIMS002);
                    }

                    db.IMS002.AddRange(lstObjIMS002);
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Data Upload Successfully!";

                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return ErrorExportToExcel(dt);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return null;
            }
        }

        public ActionResult ErrorExportToExcel(DataTable dt)
        {
            clsHelper.ResponceMsgWithFileName objResponseMsg = new clsHelper.ResponceMsgWithFileName();
            if (dt.Columns.Contains("CalibrationDueDate"))
            {
                dt.Columns.Remove("CalibrationDueDate");
            }
            if (dt.Columns.Contains("RevNo"))
            {
                dt.Columns.Remove("RevNo");
            }

            try
            {
                MemoryStream ms = new MemoryStream();
                using (FileStream fs = System.IO.File.OpenRead(Server.MapPath("~/Areas/IMS/Views/MaintainIMS/IMS_Error.xlsx")))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();
                        int i = 2;
                        foreach (DataRow item in dt.Rows)
                        {
                            excelWorksheet.Cells[i, 1].Value = item.Field<string>(0);
                            excelWorksheet.Cells[i, 2].Value = item.Field<string>(1);
                            excelWorksheet.Cells[i, 3].Value = item.Field<string>(2);
                            excelWorksheet.Cells[i, 4].Value = item.Field<string>(3);
                            excelWorksheet.Cells[i, 5].Value = item.Field<string>(4);
                            excelWorksheet.Cells[i, 6].Value = item.Field<string>(5);
                            excelWorksheet.Cells[i, 7].Value = item.Field<string>(6);
                            excelWorksheet.Cells[i, 8].Value = item.Field<string>(7);
                            excelWorksheet.Cells[i, 9].Value = item.Field<string>(8);
                            excelWorksheet.Cells[i, 10].Value = item.Field<string>(9);
                            excelWorksheet.Cells[i, 11].Value = item.Field<string>(10);
                            excelWorksheet.Cells[i, 12].Value = item.Field<string>(11);
                            excelWorksheet.Cells[i, 13].Value = item.Field<string>(12);
                            excelWorksheet.Cells[i, 14].Value = item.Field<string>(13);
                            excelWorksheet.Cells[i, 15].Value = item.Field<string>(14);
                            excelWorksheet.Cells[i, 16].Value = item.Field<string>(15);
                            excelWorksheet.Cells[i, 17].Value = item.Field<string>(16);
                            excelWorksheet.Cells[i, 18].Value = item.Field<string>(17);
                            excelWorksheet.Cells[i, 19].Value = item.Field<string>(18);

                            i++;
                        }
                        excelPackage.SaveAs(ms);
                    }
                }

                ms.Position = 0;
                string fileName;
                string excelFilePath = Helper.ExcelFilePath("ErrorList", out fileName);
                Byte[] bin = ms.ToArray();
                System.IO.File.WriteAllBytes(excelFilePath, bin);

                objResponseMsg.Key = false;
                objResponseMsg.FileName = fileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion
    }

    public class resultdata
    {
        public bool Key { get; set; }
        public string Value { get; set; }
        public string lineId { get; set; }
        public string duedate { get; set; }
    }

}