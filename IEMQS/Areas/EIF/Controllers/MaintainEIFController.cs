﻿using IEMQS.Areas.COP.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using Microsoft.ReportingServices.DataProcessing;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationEnum;

namespace IEMQS.Areas.EIF.Controllers
{
    [SessionExpireFilter]
    public class MaintainEIFController : clsBase
    {
        // GET: EIF/MaintainEIF
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult IndexFSP()
        {
            ViewBag.Title = "Maintain Internal & External Item’s Data";
            ViewBag.Role = "FSP";
            return View("Index");
        }

        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult IndexJP()
        {
            ViewBag.Title = "Maintain Scheduling Details";
            ViewBag.Role = "JP";
            return View("Index");
        }

        [UserPermissions]
        public ActionResult DisplayEIF()
        {
            ViewBag.Title = "Display session";
            ViewBag.IsDisplay = true;
            return View("Index", new EIF001());
        }

        [HttpPost]
        public ActionResult LoadDataGridPartial(string status, string role, bool isDisplay)
        {
            ViewBag.Status = status;
            ViewBag.Role = role;
            ViewBag.IsDisplayy = (status.ToLower() == clsImplementationEnum.Overlay.Yes.GetStringValue().ToLower()) ? true : isDisplay;

            ViewBag.lstMOC = new string[] { clsImplementationEnum.MOC.CS.GetStringValue(), clsImplementationEnum.MOC.LAS.GetStringValue(), clsImplementationEnum.MOC.SS.GetStringValue() };            
            ViewBag.lstyesno = new string[] { clsImplementationEnum.Overlay.Yes.GetStringValue(), clsImplementationEnum.Overlay.No.GetStringValue() };
            ViewBag.lstyesnona = new string[] { clsImplementationEnum.YesNoNA.NA.GetStringValue(), clsImplementationEnum.YesNoNA.Yes.GetStringValue(), clsImplementationEnum.YesNoNA.No.GetStringValue() };
            ViewBag.lstfabtype = new string[] { clsImplementationEnum.FabricationType.Assembly.GetStringValue(), clsImplementationEnum.FabricationType.Component.GetStringValue() };
            ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            ViewBag.BU = new string[] { clsImplementationEnum.BU.HTE.GetStringValue(), clsImplementationEnum.BU.RPV.GetStringValue() };
            return PartialView("_GetGridDataPartial");
        }

        [HttpPost]
        public ActionResult LoadDataGridData(JQueryDataTableParamModel param, string role)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string status = param.Status;
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                strWhereCondition += "1=1 ";

                if (status.ToLower() == clsImplementationEnum.Overlay.No.GetStringValue().ToLower())
                {
                    strWhereCondition += "and Dispatched in ('" + clsImplementationEnum.Overlay.No.GetStringValue() + "') ";
                }
                else
                {
                    strWhereCondition += "and Dispatched in ('" + clsImplementationEnum.Overlay.Yes.GetStringValue() + "') ";
                }

                #endregion

                //search Condition 

                string[] columnName = {
                    "e1.Project"
                    ,"QualityProject"
                    ,"BU"
                    ,"Location"
                    ,"TypeofProduct"
                    ,"Taskmanager"
                    ,"FullkitNumber"                   
                    ,"PositionNumber"
                    ,"f2.NoOfPieces"
                    ,"f9.NodeKey"
                    ,"f2.ProductType"
                    ,"MOC"
                    ,"f2.Thickness"
                    ,"f2.Weight"
                    ,"f2.NoOfPieces"
                    ,"e1.RequiredDate"
                    ,"Duration"                   
                    ,"WorkStarted"
                    ,"FabricationCompleted"
                    ,"NDTcompleted"
                    ,"Dispatched"
                    ,"DispatchDate"
                    ,"FabricationType"
                    ,"MatAvailDate"
                    ,"DateofEntry"
                };
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                var lstResult = db.SP_EIF_GET_INDEX_DATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.HeaderId),
                                uc.Project,
                                uc.QualityProject,
                                uc.BU,
                                uc.Location,
                                uc.TypeofProduct,
                                uc.Taskmanager,
                                uc.FullkitNumber,                               
                                uc.PositionNumber,
                                Convert.ToString(uc.Quantity),
                                uc.ItemId,
                                uc.MaterialType,
                                uc.MOC,
                                Convert.ToString(uc.Thickness),
                                Convert.ToString(uc.Weight),
                                Convert.ToString(uc.NoOfPieces),
                                uc.FabricationType,
                                Convert.ToString(uc.RequiredDate.HasValue ? uc.RequiredDate.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture) : ""),
                                Convert.ToString(uc.Duration),                                
                                Convert.ToString(uc.WorkStarted),
                                Convert.ToString(uc.FabricationCompleted),
                                Convert.ToString(uc.NDTcompleted),
                                Convert.ToString(uc.Dispatched),
                                Convert.ToString(uc.DispatchDate.HasValue? uc.DispatchDate.Value.ToString("dd/MM/yyyy"): ""),
                                Convert.ToString(uc.MatAvailDate),
                                Convert.ToString(uc.DateofEntry.HasValue? uc.DateofEntry.Value.ToString("dd/MM/yyyy"): ""),
                                ((role == "FSP" && status.ToLower() == clsImplementationEnum.Overlay.No.GetStringValue().ToLower()) ? "<span class='editable'><a id='Delete' name='Delete' title='Delete' class='blue action' onclick='DeleteRecord("+uc.HeaderId+")'><i class='fa fa-trash'></i></a></span>&nbsp;&nbsp;<a id='Seam" + uc.HeaderId + "' name='Seam' title='Seam details' class='blue action' onclick='Seamlist(" + uc.HeaderId + ")'><i class='fa fa-th'></i></a>" : "")

                          }).ToList();

                // data.Insert(0, null);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetInlineEditableRow(int id, string role, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();

                if (id == 0)
                {
                    var QPitems = new SelectList(new List<string>());
                    int newRecordId = 0;
                    var newRecord = new[] {
                                        "0",
                                        Helper.GenerateHidden(newRecordId, "Project",  ""),
                                        Helper.HTMLAutoComplete(newRecordId,"QualityProject","","",false,"","",false,"","","QualityProject","form-control"),                                       
                                        Helper.HTMLAutoComplete(newRecordId,"BU","","",false,"","",false,"","","BU","form-control BU"),
                                        Helper.GenerateTextbox(newRecordId, "Location",  "", "", true, "", "","location"),
                                        Helper.HTMLAutoComplete(newRecordId,"TypeofProduct","","",false,"","",false,"","","TypeofProduct","form-control TypeofProduct"),

                                        Helper.HTMLAutoComplete(newRecordId,"Taskmanager","","",false,"","",false,"","","Taskmanager","form-control taskmanager"),
                                        Helper.HTMLAutoComplete(newRecordId,"FullkitNumber","","",false,"","",false,"","","FullkitNumber","form-control fullkitnumber"),                                       
                                        Helper.HTMLAutoComplete(newRecordId,"PositionNumber","","",false,"","",false,"","","PositionNumber","form-control positionno"),
                                        Helper.GenerateTextbox(newRecordId, "Quantity",  "", "", false, "", "","quantity",true),
                                        Helper.GenerateTextbox(newRecordId, "ItemId",  "", "", false, "", "","itemid",true),
                                        Helper.GenerateTextbox(newRecordId, "MaterialType",  "", "", false, "", "","materialtype",true),
                                        Helper.HTMLAutoComplete(newRecordId,"MOC","","",false,"","",false,"","","MOC","form-control moc"),
                                        Helper.GenerateTextbox(newRecordId, "Thickness",  "", "", false, "", "","thickness",true),
                                        Helper.GenerateTextbox(newRecordId, "FabWeight",  "", "", false, "", "","fabweight",true),
                                        Helper.GenerateTextbox(newRecordId, "NoOfPieces",  "", "", false, "", "","noofpieces",true),
                                        Helper.HTMLAutoComplete(newRecordId,"FabricationType","","",false,"","",false,"","","FabricationType","form-control FabricationType"),
                                        Helper.GenerateTextbox(newRecordId, "RequiredDate",  "", "",false,"","","form-control datePicker requireddate",false, "", "RequiredDate"),
                                        Helper.HTMLAutoComplete(newRecordId,"Duration","","",false,"","",false,"","","Duration","form-control Duration"),
                                        Helper.HTMLAutoComplete(newRecordId,"WorkStarted","NA","",false,"","",false,"","","WorkStarted","form-control workstarted"),
                                        Helper.HTMLAutoComplete(newRecordId,"FabricationCompleted","","",false,"","",false,"","","FabricationCompleted","form-control fabricationcompleted"),
                                        Helper.HTMLAutoComplete(newRecordId,"NDTcompleted","","",false,"","",false,"","","NDTcompleted","form-control ndtcompleted"),
                                        Helper.HTMLAutoComplete(newRecordId,"Dispatched","NA","DispatchedChange("+newRecordId+")",false,"","",false,"","","Dispatched","form-control dispatched"),
                                        Helper.GenerateTextbox(newRecordId,"DispatchDate"),
                                        Helper.GenerateTextbox(newRecordId, "MatAvailDate",  "", "",false,"","","form-control datePicker matavaildate",false, "", "Material Availability Date"),
                                        Helper.GenerateTextbox(newRecordId, "DateofEntry", DateTime.Now.ToString("dd/MM/yyyy"),"",true,"","","form-control datePicker dateofentry",false, "", "Date of Entry"),
                                        Helper.GenerateGridButtonNew(newRecordId, "Save", "Save Rocord", "fa fa-save", "AddFabrication()" ),
                                    };
                    data.Add(newRecord);
                }
                else
                {
                    var lstResult = db.SP_EIF_GET_INDEX_DATA(1, 0, "", "e1.HeaderId = " + id).Take(1).ToList();
                    if (isReadOnly)
                    {
                        data = (from uc in lstResult
                                select new[]
                               {
                                Convert.ToString(uc.HeaderId),
                                uc.Project,
                                uc.QualityProject,
                                uc.BU,
                                uc.Location,
                                uc.TypeofProduct,
                                uc.Taskmanager,
                                uc.FullkitNumber,                              
                                uc.PositionNumber,
                                "",//uc.NoOfPieces
                                "",//uc.NodeKey
                                "",//uc.MaterialType
                                uc.MOC,
                                "",//uc.Thickness
                                "",//uc.FabWeight
                                "",//uc.NoOfPieces
                                uc.FabricationType,
                                Convert.ToString(uc.RequiredDate.HasValue ? uc.RequiredDate.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture) : ""),
                                Convert.ToString(uc.Duration),                              
                                Convert.ToString(uc.WorkStarted),
                                Convert.ToString(uc.FabricationCompleted),
                                Convert.ToString(uc.NDTcompleted),
                                Convert.ToString(uc.Dispatched),
                                Convert.ToString(uc.DispatchDate.HasValue? uc.DispatchDate.Value.ToString("dd/MM/yyyy"): ""),
                                Convert.ToString(uc.MatAvailDate),
                                Convert.ToString(uc.DateofEntry.HasValue? uc.DateofEntry.Value.ToString("dd/MM/yyyy"): ""),
                          }).ToList();
                    }
                    else
                    {
                        data = (from uc in lstResult
                                select new[] {
                                         uc.HeaderId.ToString(),
                                        Helper.GenerateHidden(uc.HeaderId, "Project", uc.Project),
                                        Helper.HTMLAutoComplete(uc.HeaderId,"QualityProject",uc.QualityProject,"UpdateData(this, "+ uc.HeaderId+",true)",false,"","",(role == "JP"?true:false),"","","QualityProject","form-control editable"),                                        
                                        Helper.HTMLAutoComplete(uc.HeaderId, "BU",uc.BU,"UpdateData(this, "+ uc.HeaderId+",true)", false, "", "",(role == "JP"?true:false),"","","BU","form-control editable BU"),
                                        Helper.GenerateTextbox(uc.HeaderId, "Location",uc.Location, "", true, "", "","location"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "TypeofProduct",uc.TypeofProduct,"UpdateData(this, "+ uc.HeaderId+",true)", false, "", "",(role == "JP"?true:false),"","","TypeofProduct","form-control editable TypeofProduct"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "Taskmanager",uc.Taskmanager,"UpdateData(this, "+ uc.HeaderId+",true)", false, "", "",(role == "JP"?true:false),"","","Taskmanager","form-control editable Taskmanager"),

                                        Helper.HTMLAutoComplete(uc.HeaderId, "FullkitNumber",uc.FullkitNumber,"UpdateData(this, "+ uc.HeaderId+",true)", false, "", "",(role == "JP"?true:false),"","","FullkitNumber","form-control editable FullkitNumber"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "PositionNumber",uc.PositionNumber,"UpdateData(this, "+ uc.HeaderId+",true)", false, "", "",(role == "JP"?true:false),"","","PositionNumber","form-control editable positionno"),
                                        Helper.GenerateTextbox(uc.HeaderId, "Quantity",  Convert.ToString(uc.Quantity), "", true, "", "","quantity"),
                                        Helper.GenerateTextbox(uc.HeaderId, "ItemId",  Convert.ToString(uc.ItemId), "", true, "", "","itemid"),
                                        Helper.GenerateTextbox(uc.HeaderId, "MaterialType",  Convert.ToString(uc.MaterialType), "", true, "", "","materialtype"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "MOC",uc.MOC,"UpdateData(this, "+ uc.HeaderId+",true)", false, "", "",(role == "JP"?true:false),"","","MOC","form-control editable MOC"),
                                        Helper.GenerateTextbox(uc.HeaderId, "Thickness",  Convert.ToString(uc.Thickness), "", true, "", "","thickness"),
                                        Helper.GenerateTextbox(uc.HeaderId, "FabWeight",  Convert.ToString(uc.Weight), "", true, "", "","fabweight"),
                                        Helper.GenerateTextbox(uc.HeaderId, "NoOfPieces",  Convert.ToString(uc.NoOfPieces), "", true, "", "","noofpieces"),

                                        Helper.HTMLAutoComplete(uc.HeaderId, "FabricationType",uc.FabricationType,"UpdateData(this, "+ uc.HeaderId+",true)", false, "", "",(role == "FSP"?true:false),"","","FabricationType","form-control editable FabricationType"),
                                        Helper.GenerateTextbox(uc.HeaderId, "RequiredDate",uc.RequiredDate.HasValue?uc.RequiredDate.Value.ToString("dd/MM/yyyy"):"","", false, "", "","form-control editable datePicker requireddate",(role == "FSP"?true:false),"UpdateData(this, "+ uc.HeaderId+",true)","RequiredDate"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "Duration",uc.Duration.HasValue? Convert.ToString(uc.Duration.Value):"","UpdateData(this, "+ uc.HeaderId+",true)", false, "", "",(role == "FSP"?true:false),"","","Duration","form-control editable duration"),
                                                                              
                                        Helper.HTMLAutoComplete(uc.HeaderId, "WorkStarted",string.IsNullOrEmpty(uc.WorkStarted)?"NA":uc.WorkStarted,"UpdateData(this, "+ uc.HeaderId+",true)", false, "", "",(role == "JP"?true:false),"","","WorkStarted","form-control editable WorkStarted"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "FabricationCompleted",uc.FabricationCompleted,"UpdateData(this, "+ uc.HeaderId+",true)", false, "", "",(role == "JP"?true:false),"","","FabricationCompleted","form-control editable FabricationCompleted"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "NDTcompleted",uc.NDTcompleted,"UpdateData(this, "+ uc.HeaderId+",true)", false, "", "",(role == "JP"?true:false),"","","NDTcompleted","form-control editable NDTcompleted"),
                                        Helper.HTMLAutoComplete(uc.HeaderId, "Dispatched",uc.Dispatched,"UpdateData(this, "+ uc.HeaderId+",true)", false, "", "",(role == "JP"?true:false),"","","Dispatched","form-control editable Dispatched" ),
                                        Helper.GenerateTextbox(uc.HeaderId,"DispatchDate",uc.DispatchDate.HasValue? uc.DispatchDate.Value.ToString("dd/MM/yyyy"): "","UpdateData(this, "+ uc.HeaderId+",false)", false, "", "","form-control editable",false),
                                        Helper.GenerateTextbox(uc.HeaderId, "MatAvailDate",uc.MatAvailDate.HasValue?uc.MatAvailDate.Value.ToString("dd/MM/yyyy"):"","", false, "", "","form-control editable datePicker matavaildate",false,"UpdateData(this, "+ uc.HeaderId+",true)","Material Availability Date"),
                                        Helper.GenerateTextbox(uc.HeaderId, "DateofEntry",uc.DateofEntry.HasValue?uc.DateofEntry.Value.ToString("dd/MM/yyyy"):"","", false, "", "","form-control editable datePicker dateofentry",false,"UpdateData(this, "+ uc.HeaderId+",true)","Date of Entry"),

                                        (role == "FSP" ? "<span class='editable'><a id='Delete' name='Delete' title='Delete' class='blue action' onclick='DeleteRecord("+uc.HeaderId+")'><i class='fa fa-trash'></i></a></span> &nbsp" : " " ) +
                                        "<span class='editable'><a id='Cancel' name='Cancel' title='Cancel' class='blue action' onclick='ReloadGrid()'><i class='fa fa-close'></i></a></span>",
                          }).ToList();
                    }
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveNewRecord(FormCollection fc)
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var eif001 = new EIF001();

                eif001.QualityProject = fc["QualityProject" + 0];
                eif001.Taskmanager = fc["Taskmanager" + 0];
                eif001.BU = fc["BU" + 0];
                eif001.Project = fc["Project" + 0];
                eif001.TypeofProduct = fc["TypeofProduct" + 0];
                eif001.FullkitNumber = fc["FullkitNumber" + 0];
                eif001.DrawingNo = fc["DrawingNo" + 0];
                eif001.DescriptionofAssembly = fc["DescriptionofAssembly" + 0];               
                eif001.PositionNumber = fc["PositionNumber" + 0];
                eif001.MOC = fc["MOC" + 0];
                eif001.RequiredDate = (!string.IsNullOrEmpty(fc["RequiredDate" + 0]) ? Convert.ToDateTime(fc["RequiredDate" + 0]) : (DateTime?)null);
                eif001.Duration = Convert.ToInt32(!string.IsNullOrEmpty(fc["Duration" + 0]) ? Convert.ToInt32(fc["Duration" + 0]) : 0);               
                eif001.PCRNumber = fc["PCRNumber" + 0];
                eif001.PCLNumber = fc["PCLNumber" + 0];
                eif001.WorkStarted = fc["WorkStarted" + 0];
                eif001.FabricationCompleted = fc["FabricationCompleted" + 0];
                eif001.NDTcompleted = fc["NDTcompleted" + 0];
                eif001.Dispatched = fc["Dispatched" + 0];
                if (!string.IsNullOrWhiteSpace(fc["DispatchDate" + 0]))
                {
                    eif001.DispatchDate = Convert.ToDateTime(fc["DispatchDate" + 0]);
                }
                eif001.FabricationType = fc["FabricationType" + 0];
                if (!string.IsNullOrWhiteSpace(fc["DateofEntry" + 0]))
                {
                    eif001.DateofEntry = Convert.ToDateTime(fc["DateofEntry" + 0]);
                }
                eif001.Location = objClsLoginInfo.Location;
                eif001.CreatedOn = DateTime.Now;
                eif001.CreatedBy = objClsLoginInfo.UserName;

                db.EIF001.Add(eif001);
                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data save successfully.";

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg);
        }

        [HttpPost]
        public ActionResult DeleteRecord(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objEIF001 = db.EIF001.Where(x => x.HeaderId == Id).FirstOrDefault();
                db.EIF001.Remove(objEIF001);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateDetails(int id, string columnName, string columnValue)
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    if (columnName.ToLower() == "weekno")
                    {
                        columnValue = columnValue.Length > 0 ? columnValue.Substring(5, 1) : "";
                    }
                    else if (columnName.ToLower() == "mark")
                    {
                        columnValue = columnValue.ToUpper();
                    }
                    else if (columnName.ToLower() == "requireddate")
                    {
                        if (!string.IsNullOrEmpty(columnValue))
                        {
                            var splitDate = columnValue.Split('/');
                            columnValue = splitDate[2] + "/" + splitDate[1] + "/" + splitDate[0];
                        }
                    }
                    db.SP_COMMON_TABLE_UPDATE("eif001", id, "HeaderId", columnName, columnValue, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();

                }
                else
                {
                    //if (columnName.ToLower() == "remarks" || columnName == "QualityProject")
                    //{
                    db.SP_COMMON_TABLE_UPDATE("eif001", id, "HeaderId", columnName, columnValue, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                    //}
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

     

        public IEnumerable<WeekRange> GetWeekRange(DateTime dtStart, DateTime dtEnd)
        {
            DateTime fWeekStart, dt, fWeekEnd;
            int wkCnt = 1;

            if (dtStart.DayOfWeek != DayOfWeek.Sunday)
            {
                fWeekStart = dtStart.AddDays(7 - (int)dtStart.DayOfWeek);
                fWeekEnd = fWeekStart.AddDays(-1);
                IEnumerable<WeekRange> ranges = getMonthRange(new WeekRange(dtStart, fWeekEnd, dtStart.Month, wkCnt++));
                foreach (WeekRange wr in ranges)
                {
                    yield return wr;
                }
                wkCnt = ranges.Last().WeekNo + 1;

            }
            else
            {
                fWeekStart = dtStart;
            }


            for (dt = fWeekStart.AddDays(6); dt <= dtEnd; dt = dt.AddDays(7))
            {


                IEnumerable<WeekRange> ranges = getMonthRange(new WeekRange(fWeekStart, dt, fWeekStart.Month, wkCnt++));
                foreach (WeekRange wr in ranges)
                {
                    yield return wr;
                }
                wkCnt = ranges.Last().WeekNo + 1;
                fWeekStart = dt.AddDays(1);


            }

            if (dt > dtEnd)
            {

                IEnumerable<WeekRange> ranges = getMonthRange(new WeekRange(fWeekStart, dtEnd, dtEnd.Month, wkCnt++));
                foreach (WeekRange wr in ranges)
                {
                    yield return wr;
                }
                wkCnt = ranges.Last().WeekNo + 1;

            }

        }

        public IEnumerable<WeekRange> getMonthRange(WeekRange weekRange)
        {

            List<WeekRange> ranges = new List<WeekRange>();

            if (weekRange.Start.Month != weekRange.End.Month)
            {
                DateTime lastDayOfMonth = new DateTime(weekRange.Start.Year, weekRange.Start.Month, 1).AddMonths(1).AddDays(-1);
                ranges.Add(new WeekRange(weekRange.Start, lastDayOfMonth, weekRange.Start.Month, weekRange.WeekNo));
                ranges.Add(new WeekRange(lastDayOfMonth.AddDays(1), weekRange.End, weekRange.End.Month, weekRange.WeekNo + 1));

            }
            else
            {
                ranges.Add(weekRange);
            }

            return ranges;

        }

        [HttpPost]
        public ActionResult Get_PCR_No_By_Project(string term, string findno, string project)
        {
            List<TaskAutoCompleteModel> lstlndata = new List<TaskAutoCompleteModel>();
            try
            {
                string query =
                         "SELECT distinct " + (string.IsNullOrEmpty(term) ? "top 10" : "") + " a.t_pcrn as id,a.t_pcrn as text " +
                        "FROM FKM102 as f  left join " + LNLinkedServer + ".dbo.tltsfc503175 a with(nolock) on " +
                        "a.t_cprj = f.Project COLLATE DATABASE_DEFAULT and a.t_prtn = f.FindNo COLLATE DATABASE_DEFAULT " +
                        "WHERE f.Project = '" + project + "' and f.ProductType <> 'ASM' and f.ProductType = 'PLT' " +
                        "and a.t_pcrn is not null and a.t_pcln is not null " +
                        (string.IsNullOrEmpty(term) ? "" : "and a.t_pcln = '" + term + "' ") + "" +
                        "and f.FindNo = " + findno + " ";

                lstlndata = db.Database.SqlQuery<TaskAutoCompleteModel>(query).ToList();

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
            return Json(lstlndata, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Get_PCL_No_By_Project(string term, string findno, string project, string pcrno)
        {
            List<TaskAutoCompleteModel> lstlndata = new List<TaskAutoCompleteModel>();
            try
            {
                string query =
                         "SELECT distinct " + (string.IsNullOrEmpty(term) ? "top 10" : "") + " a.t_pcln as id,a.t_pcln as text " +
                        "FROM FKM102 as f  left join " + LNLinkedServer + ".dbo.tltsfc503175 a with(nolock) on " +
                        "a.t_cprj = f.Project COLLATE DATABASE_DEFAULT and a.t_prtn = f.FindNo COLLATE DATABASE_DEFAULT " +
                        "WHERE f.Project = '" + project + "' and f.ProductType <> 'ASM' and f.ProductType = 'PLT' " +
                        "and a.t_pcrn is not null and a.t_pcln is not null " +
                        (string.IsNullOrEmpty(pcrno) ? " " + (string.IsNullOrEmpty(term) ? "" : "and a.t_pcln = '" + term + "' ") + "" : "and a.t_pcrn = '" + term + "' and a.t_pcrn = '" + pcrno + "' ") +
                        "and f.FindNo = " + findno + " ";

                lstlndata = db.Database.SqlQuery<TaskAutoCompleteModel>(query).ToList();

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
            return Json(lstlndata, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            var objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                var lst = db.SP_EIF_GET_INDEX_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Data Found";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                var data = (from uc in lst
                            select new
                            {
                                Project = uc.Project,
                                QualityProject = uc.QualityProject,
                                BU = uc.BU,
                                Location = uc.Location,
                                TypeofProduct = uc.TypeofProduct,
                                Taskmanager = uc.Taskmanager,
                                FullkitNumber = uc.FullkitNumber,
                                DrawingNo = uc.DrawingNo,
                                DescriptionofAssembly = uc.DescriptionofAssembly,                                
                                PositionNumber = uc.PositionNumber,
                                Quantity = Convert.ToString(uc.Quantity),
                                ItemId = uc.ItemId,
                                MaterialType = uc.MaterialType,
                                MOC = uc.MOC,
                                Thickness = Convert.ToString(uc.Thickness),
                                Weight = Convert.ToString(uc.Weight),
                                NoOfPieces = Convert.ToString(uc.NoOfPieces),
                                FabricationType = uc.FabricationType,
                                RequiredDate = Convert.ToString(uc.RequiredDate.HasValue ? uc.RequiredDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""),
                                Duration = Convert.ToString(uc.Duration),                               
                                PCRNumber = uc.PCRNumber,
                                PCLNumber = uc.PCLNumber,
                                WorkStarted = Convert.ToString(uc.WorkStarted),
                                FabricationCompleted = Convert.ToString(uc.FabricationCompleted),
                                NDTcompleted = Convert.ToString(uc.NDTcompleted),
                                Dispatched = Convert.ToString(uc.Dispatched),
                            }).ToList();

                strFileName = Helper.GenerateExcel(data, objClsLoginInfo.UserName);
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult Get_FKMS_Position_By_FullkitNo(string term, string Project, string FullKitNo)
        {
            try
            {
                string Nodeid = FullKitNo.Split('-').LastOrDefault();
                List<SP_EIF_Get_Position_By_FullkitNo_AND_PROJECT_Result> lstgetAllbom = db.SP_EIF_Get_Position_By_FullkitNo_AND_PROJECT(Project, Nodeid).ToList();

                if (!string.IsNullOrEmpty(Project) && !string.IsNullOrEmpty(Nodeid))
                {

                    //var item = (from a in db.FKM109
                    //            join b in db.FKM102 on a.NodeId equals b.NodeId
                    //            where a.Project.ToLower() == Project.ToLower() && a.FullKitNo == FullKitNo && string.IsNullOrEmpty(a.ErrorMsg)
                    //                && (term == "" || a.FindNo.Trim().Contains(term.Trim()))
                    //            select new
                    //            {
                    //                Value = a.FindNo,
                    //                NodeKey = a.NodeKey,
                    //                Qty = b.NoOfPieces,
                    //                MaterialType = b.ProductType,
                    //                Thickness = b.Thickness,
                    //                NoOfPieces = b.NoOfPieces,
                    //                Weight = b.Weight
                    //            }
                    //      ).Distinct().Take(10).ToList();

                    var item = lstgetAllbom.Where(x => term == "" || x.FindNo.Trim().ToLower().Contains(term.Trim().ToLower())).Select(x => new
                    {
                        Value = x.FindNo,
                        NodeKey = x.NodeKey,
                        Qty = x.NoOfPieces,
                        MaterialType = x.MaterialType,
                        Thickness = x.Thickness,
                        NoOfPieces = x.NoOfPieces,
                        Weight = x.Weight
                    }).Distinct().Take(10).ToList();

                    return Json(item, JsonRequestBehavior.AllowGet);

                    //var item = db.FKM109.Where(x => x.Project.ToLower() == Project.ToLower() && x.FullKitNo == FullKitNo && string.IsNullOrEmpty(x.ErrorMsg)).Select(x => new { Value = x.FindNo, NodeKey = x.NodeKey }).Distinct().ToList();

                }
                else
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult Get_FKMS_FullkitNumber(string term, string Project)
        {
            try
            {
                if (!string.IsNullOrEmpty(Project))
                {
                    string ASM = NodeTypes.ASM.GetStringValue().ToLower();
                    string TJF = NodeTypes.TJF.GetStringValue().ToLower();
                    dynamic item;

                    var parentfkm = db.FKM102
                        .Where(x => x.Project == Project && (x.ProductType.ToLower() == ASM.ToLower() || x.ProductType.ToLower() == TJF.ToLower()))
                        .Select(b => new { FullKitNo = b.FindNo + "-" + b.NodeKey + "-" + b.NodeId, ParentNodeId = b.NodeId }).Distinct().ToList();

                    //var childfkm = db.FKM102
                    //    .Where(x => x.Project == Project && (x.ProductType.ToLower() != ASM.ToLower() || x.ProductType.ToLower() != TJF.ToLower()))
                    //    .Select(x=> new { Nodeid = x.ParentNodeId }).Distinct().ToList();

                    //var Mainlist = (from pfkm in parentfkm join cfkm in childfkm on pfkm.ParentNodeId equals cfkm.Nodeid select pfkm).ToList();

                    if (!string.IsNullOrWhiteSpace(term))
                    {
                        item = parentfkm.Where(x => x.FullKitNo.Trim().ToLower().Contains(term.Trim().ToLower())).Select(x => new { Value = x.FullKitNo }).OrderByDescending(x => x.Value).ToList();
                    }
                    else
                    {
                        item = parentfkm.Where(x => x.FullKitNo.Trim().ToLower().Contains(term.Trim().ToLower())).Select(x => new { Value = x.FullKitNo }).OrderByDescending(x => x.Value).Take(10).ToList();
                    }
                    return Json(item, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetSeamAndStageDataGridPartial(int HeaderId)
        {
            ViewBag.HeaderId = HeaderId;
            return PartialView("_GetSeamAndStageDataGridPartial");
        }

        public ActionResult LoadSeamAndStageList(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string status = param.Status;
                int headerid = Convert.ToInt32(param.Headerid);
                string whereCondition = "1=1 ";

                string[] columnName = { "q40.StageCode", "q2.StageDesc", "qms.SeamNo" };

                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                //var objFKM102 = db.FKM102.Where(i => i.NodeId == NodeId).FirstOrDefault();
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstcop = db.EIF001.Where(x => x.HeaderId == headerid).Select(x => new { Project = x.Project, QualityProject = x.QualityProject, Part = x.PositionNumber }).FirstOrDefault();

                var lstPam = db.SP_COP_GET_SEAM_AND_STAGE_LIST(StartIndex, EndIndex, strSortOrder, whereCondition, lstcop.Project, (lstcop.QualityProject != null ? lstcop.QualityProject : ""), lstcop.Part).ToList();
                var lstSeamAndStage = new List<SeamAndStageEnt>();

                #region New UI
                foreach (var item in lstPam.Select(i => i.SeamNo).Distinct())
                {
                    lstSeamAndStage.Add(new SeamAndStageEnt { SeamNo = item, StageDesc = GetAllSeamWiseStagesInTable(item, lstPam) });
                }

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();
                var res = (from h in lstSeamAndStage
                           select new[] {
                                       Convert.ToString(h.SeamNo),
                                       Convert.ToString(h.StageDesc),
                           }).ToList();
                #endregion

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public string GetAllSeamWiseStagesInTable(string SeamNo, List<SP_COP_GET_SEAM_AND_STAGE_LIST_Result> lstPam)
        {
            string StageTable = "";
            try
            {
                var strClearStatus = SeamListInspectionStatus.Cleared.GetStringValue();
                var listStages = lstPam.Where(i => i.SeamNo == SeamNo);
                //StageTable = "<table class='tblstages'><tr>";
                foreach (var item in listStages)
                {
                    var tdClass = item.InspectionStatus == strClearStatus ? "partseamstage" : "";
                    //StageTable += "<div class='" + tdClass + "' style='padding:10px;display:inline-block;margin-right:10px;'>" + item.StageCode + "-" + item.StageDesc + "</div>";
                    StageTable += "<div class='" + tdClass + "' style='padding:10px;display:inline-block;margin-right:10px;' title=" + item.StageCode + "-" + item.StageDesc + ">" + item.StageDesc + "</div>";
                }
                //StageTable += "</tr></table>";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return StageTable;
        }
    }


}
