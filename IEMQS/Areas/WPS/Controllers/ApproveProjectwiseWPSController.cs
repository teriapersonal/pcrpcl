﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.WPS.Controllers
{
    public class ApproveProjectwiseWPSController : clsBase
    {
        // GET: WPS/ApproveProjectwiseWPS

        [SessionExpireFilter]
        [AllowAnonymous]
        //[UserPermissions]
        #region Index Page
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetHeaderGridDataPartial");
        }

        [HttpPost]
        public JsonResult LoadHeaderData(JQueryDataTableParamModel param)
        {
            ViewBag.Status = param.CTQCompileStatus.ToString();
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += " 1=1 and status in('" + clsImplementationEnum.CommonStatus.SendForApprovel.GetStringValue() + "')";
                }
                else
                {
                    strWhere += " 1=1";
                }
                string[] columnName = { "Project", "QualityProject", "WPSNumber", "Status" };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                strWhere += "and Location='" + objClsLoginInfo.Location + "'";

                #region Sorting                
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstResult = db.SP_WPS_PROJECTWISEWPS_HEADER_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                     Convert.ToString(uc.HeaderId),
                            Convert.ToString(uc.QualityProject),
                            Convert.ToString(uc.Project),
                             Convert.ToString(uc.WPSNumber),
                            Convert.ToString("R" + uc.WPSRevNo),
                            Convert.ToString(uc.Jointtype),
                            Convert.ToString(uc.Status),
                            Convert.ToString(uc.Location),
                               "<center><nobr><a class='iconspace' href='"+WebsiteURL+"/WPS/ApproveProjectwiseWPS/ViewDetail?HeaderID="+uc.HeaderId+ "&Status="+ param.CTQCompileStatus.ToString()+"'><i class='fa fa-eye'></i></a></nobr></center>",
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Header
        [SessionExpireFilter]
        public ActionResult ViewDetail( int headerID, string Status)
        {
            WPS012 objWPS012 = new WPS012();
            ViewBag.Status = Status;
            if (headerID > 0)
            {
                objWPS012 = db.WPS012.Where(x => x.HeaderId == headerID).FirstOrDefault();
                ViewBag.Location = db.COM002.Where(i => i.t_dtyp == 1 && i.t_dimx == objWPS012.Location).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
                ViewBag.QualityProject = objWPS012.QualityProject;
                ViewBag.Project = db.COM001.Where(i => i.t_cprj == objWPS012.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            }
            return View(objWPS012);
        }


        [HttpPost]
        public ActionResult ApproveDetails(int strHeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                WPS012 objWPS012 = db.WPS012.Where(u => u.HeaderId == strHeaderId).SingleOrDefault();
                if (objWPS012 != null)
                {
                    objWPS012.Status = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
                    objWPS012.ApprovedBy = objClsLoginInfo.UserName;
                    objWPS012.ApprovedOn = DateTime.Now;
                    db.SaveChanges();
                    UpdateWPSRev(strHeaderId);
                    var lstWPSrevWPS012 = db.WPS012.Where(u => u.QualityProject == objWPS012.QualityProject && u.Location == objWPS012.Location && u.WPSNumber == objWPS012.WPSNumber && u.HeaderId != strHeaderId).Select(o => o.HeaderId).ToList();
                    if (lstWPSrevWPS012.Count() != 0)
                    {
                        foreach (var item in lstWPSrevWPS012)
                        {
                            WPS012 objSuperseededWPS = db.WPS012.Where(u => u.HeaderId == item).SingleOrDefault();
                            objSuperseededWPS.Status = clsImplementationEnum.CommonStatus.Superseded.ToString();
                            objSuperseededWPS.EditedBy = objClsLoginInfo.UserName;
                            objSuperseededWPS.EditedOn = DateTime.Now;
                            db.SaveChanges();
                        }
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Approve.ToString();


                    #region Get BU and Project From WPSNo                                            
                    var project = new NDEModels().GetQMSProject(objWPS012.QualityProject).Project;
                    var BU = db.COM001.Where(n => n.t_cprj.Equals(project, StringComparison.OrdinalIgnoreCase)).FirstOrDefault().t_entu;
                    #endregion
                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.WE3.GetStringValue(), project, BU, objWPS012.Location, "Project maintained for WPS: " + objWPS012.WPSNumber + " has been Approved", clsImplementationEnum.NotificationType.Information.GetStringValue());
                    #endregion
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApproveByApproverSelected(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
            try
            {
                for (int i = 0; i < arrayHeaderIds.Length; i++)
                {
                    int headerId = Convert.ToInt32(arrayHeaderIds[i]);
                    WPS012 objWPS012 = db.WPS012.Where(u => u.HeaderId == headerId).SingleOrDefault();
                    if (objWPS012 != null)
                    {
                        objWPS012.Status = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
                        objWPS012.ApprovedBy = objClsLoginInfo.UserName;
                        objWPS012.ApprovedOn = DateTime.Now;
                        db.SaveChanges();
                        UpdateWPSRev(headerId);
                        var lstWPSrevWPS012 = db.WPS012.Where(u => u.QualityProject == objWPS012.QualityProject && u.Location == objWPS012.Location && u.WPSNumber == objWPS012.WPSNumber && u.HeaderId != headerId).Select(o => o.HeaderId).ToList();
                        if (lstWPSrevWPS012.Count() != 0)
                        {
                            foreach (var item in lstWPSrevWPS012)
                            {
                                WPS012 objSuperseededWPS = db.WPS012.Where(u => u.HeaderId == item).SingleOrDefault();
                                objSuperseededWPS.Status = clsImplementationEnum.CommonStatus.Superseded.ToString();
                                objSuperseededWPS.EditedBy = objClsLoginInfo.UserName;
                                objSuperseededWPS.EditedOn = DateTime.Now;
                                db.SaveChanges();
                            }
                        }
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Approve.ToString();


                        #region Get BU and Project From WPSNo                                            
                        var project = new NDEModels().GetQMSProject(objWPS012.QualityProject).Project;
                        var BU = db.COM001.Where(n => n.t_cprj.Equals(project, StringComparison.OrdinalIgnoreCase)).FirstOrDefault().t_entu;
                        #endregion
                        #region Send Notification
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.WE3.GetStringValue(), project, BU, objWPS012.Location, "Project maintained for WPS: " + objWPS012.WPSNumber + " has been Approved", clsImplementationEnum.NotificationType.Information.GetStringValue());
                        #endregion

                    }
                    else
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                    }

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        public bool UpdateWPSRev(int HeaderId)
        {
            var objWPS012 = db.WPS012.Where(i => i.HeaderId == HeaderId).FirstOrDefault();
            if (objWPS012 != null)
            {
                var IQualityProjects = db.WPS013.Where(i => i.QualityProject == objWPS012.QualityProject && i.Location == objWPS012.Location).Select(x => x.IQualityProject).ToList();
                IQualityProjects.Add(objWPS012.QualityProject);
                //var wps012 = db.WPS012.Where(i => IQualityProjects.Contains(i.QualityProject) && i.Location == objWPS012.Location).ToList();
                foreach (var QP in IQualityProjects)
                {
                    var objSWP010 = db.SWP010.Where(i => i.QualityProject == QP && i.Location == objWPS012.Location && (i.MainWPSNumber == objWPS012.WPSNumber || i.AlternateWPS1 == objWPS012.WPSNumber || i.AlternateWPS2 == objWPS012.WPSNumber)).ToList();
                    foreach (var swp010 in objSWP010)
                    {
                        /* Commented On 20/11/2019. Observation No : 27291
                        var maxOfferedSequenceNo = (new clsManager()).GetMaxOfferedSequenceNo(swp010.QualityProject, swp010.BU, swp010.Location, swp010.SeamNo);
                        if (maxOfferedSequenceNo == 0 && !IsTokenGenerated(swp010.HeaderId))
                        */
                        //Added On 20/11/2019. Observation No : 27291
                        var IsWeldVisualOffered = (new clsManager()).CheckWeldVisualOfferedForSeam(swp010.QualityProject, swp010.BU, swp010.Location, swp010.SeamNo);
                        if (!IsWeldVisualOffered)
                        {
                            if (swp010.MainWPSNumber == objWPS012.WPSNumber)
                                swp010.MainWPSRevNo = objWPS012.WPSRevNo;
                            else if (swp010.AlternateWPS1 == objWPS012.WPSNumber)
                                swp010.AlternateWPS1RevNo = objWPS012.WPSRevNo;
                            else if (swp010.AlternateWPS2 == objWPS012.WPSNumber)
                                swp010.AlternateWPS2RevNo = objWPS012.WPSRevNo;
                            if (swp010.Status.Equals(clsImplementationEnum.UTCTQStatus.Approved.GetStringValue()))
                            {
                                swp010.Status = clsImplementationEnum.UTCTQStatus.DRAFT.GetStringValue();
                                swp010.RevNo = swp010.RevNo + 1;
                                db.SaveChanges();
                                db.SP_SWP_APPROVE(swp010.HeaderId, objClsLoginInfo.UserName);
                            }
                            else
                            {
                                db.SaveChanges();
                            }                            
                        }
                    }
                }

            }
            return true;
        }

        [HttpPost]
        public JsonResult GetApproveQualityProjectNo(string term)
        {
            db.Configuration.ProxyCreationEnabled = false;

            var res = db.WPS012.Where(c => c.QualityProject.Contains(term));

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult FetchApproveQualityData(string approvequalityproject)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var res = db.WPS012.Where(i => i.QualityProject.Equals(approvequalityproject, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public bool IsSeamOffered(int HeaderId)
        {
            var status = clsImplementationEnum.PartlistOfferInspectionStatus.READY_TO_OFFER.GetStringValue();
            var objSWP010 = db.SWP010.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            //return Manager.IsSeamApplicableForPrintSlip(objSWP010.Project, objSWP010.QualityProject, objSWP010.BU, objSWP010.Location, objSWP010.SeamNo);
            if (db.QMS040.Where(x => x.QualityProject == objSWP010.QualityProject && x.Location == objSWP010.Location && x.BU == objSWP010.BU && x.SeamNo == objSWP010.SeamNo && (x.InspectionStatus != null && x.InspectionStatus != "" && !x.InspectionStatus.Equals(status))).Any())
                return true;
            else
                return false;
        }
        public bool IsTokenGenerated(int HeaderId)
        {
            var status = clsImplementationEnum.PartlistOfferInspectionStatus.READY_TO_OFFER.GetStringValue();
            var objSWP010 = db.SWP010.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            if (db.WPS026.Any(x => x.QualityProject == objSWP010.QualityProject && x.Project == objSWP010.Project && x.Location == objSWP010.Location && x.BU == objSWP010.BU && x.SeamNo == objSWP010.SeamNo))
                return true;
            else
                return false;
        }

        [SessionExpireFilter]
        [HttpPost]
        public ActionResult ApproveHeader(int HeaderID)
        {
            WPS012 objWPS012 = new WPS012();
            if (HeaderID > 0)
            {
                objWPS012 = db.WPS012.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
                objWPS012.Project = db.COM001.Where(i => i.t_cprj == objWPS012.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                ViewBag.QualityProject = objWPS012.QualityProject;
                ViewBag.Project = objWPS012.Project;
                ViewBag.IdenticalProject = string.Join(",", db.WPS013.Where(i => i.HeaderId == HeaderID).Select(i => i.IQualityProject).ToList());
            }
            objWPS012.Location = db.COM002.Where(i => i.t_dtyp == 1 && i.t_dimx == objClsLoginInfo.Location).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
            return View(objWPS012);
        }

        [HttpPost]
        public JsonResult NextApproveWPS(int? headerId, string Status)
        {

            ViewBag.Status = Status;
            string strWhere = string.Empty;
            if (headerId != 0)
            {
                if (Status == "Pending")
                {
                    strWhere += " 1=1 and status in('" + clsImplementationEnum.CommonStatus.SendForApprovel.GetStringValue() + "')";
                }
                else
                {
                    strWhere += " 1=1";
                }
                strWhere += "and Location='" + objClsLoginInfo.Location + "'";
                var lstResult = db.SP_WPS_GET_LOCATION_WISE_QUALITYPROJECT_HEADER(0, 0, "", strWhere).ToList();
                var q = lstResult.Where(i => i.HeaderId > headerId).OrderBy(i => i.HeaderId).FirstOrDefault();


                return Json(q, JsonRequestBehavior.AllowGet);
            }
            return Json(null);

        }

        [HttpPost]
        public JsonResult PrevApproveWPS(int? headerId, string status)
        {

            ViewBag.Status = status;
            string strWhere = string.Empty;
            if (headerId != 0)
            {
                if (status == "Pending")
                {
                    strWhere += " 1=1 and status in('" + clsImplementationEnum.CommonStatus.SendForApprovel.GetStringValue() + "')";
                }
                else
                {
                    strWhere += " 1=1";
                }
                strWhere += "and Location='" + objClsLoginInfo.Location + "'";
                var lstResult = db.SP_WPS_GET_LOCATION_WISE_QUALITYPROJECT_HEADER(0, 0, "", strWhere).ToList();
                var q = lstResult.Where(i => i.HeaderId < headerId).OrderByDescending(i => i.HeaderId).FirstOrDefault();


                return Json(q, JsonRequestBehavior.AllowGet);
            }
            return Json(null);

        }
        [HttpPost]
        public JsonResult LoadQualityProjectWPSHeaderData(JQueryDataTableParamModel param,string CTQCompileStatus)
        {
            ViewBag.Status = CTQCompileStatus;
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += " 1=1 and status in('" + clsImplementationEnum.CommonStatus.SendForApprovel.GetStringValue() + "')";
                }
                else
                {
                    strWhere += " 1=1";
                }
                string[] columnName = { "Project", "QualityProject", "Location" };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                strWhere += "and Location='" + objClsLoginInfo.Location + "'";

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstResult = db.SP_WPS_GET_LOCATION_WISE_QUALITYPROJECT_HEADER
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.QualityProject),
                            Convert.ToString(uc.Project),
                            Convert.ToString(uc.Location),
                             "<center><nobr><a class='iconspace' href='"+WebsiteURL+"/WPS/ApproveProjectwiseWPS/ViewDetail?headerID="+uc.HeaderId+ "&Status="+CTQCompileStatus+"'><i class='fa fa-eye'></i></a></nobr></center>",
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]
        [HttpPost]
        public ActionResult LoadProjectWiseWPSDetails(string status)
        {
            //WPS012 objWPS012 = new WPS012();
            //objWPS012 = db.WPS012.Where(x => x.HeaderId == headerID).FirstOrDefault();
            ViewBag.Status = status;
            return PartialView("_LoadProjectWiseWPSDetails");
        }
        public ActionResult loadProjectWiseWPSDetailsDataTable(JQueryDataTableParamModel param, string location, string qualityProject)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += " 1=1 and status in('" + clsImplementationEnum.CommonStatus.SendForApprovel.GetStringValue() + "')";
                }
                else
                {
                    strWhere += " 1=1";
                }
                strWhere += " and QualityProject='" + qualityProject + "' and location='" + location + "' ";
                string[] columnName = { "Project", "QualityProject", "WPSNumber", "Status" };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstPam = db.SP_WPS_GET_PROJECT_WISE_WPS_DETAILS(StartIndex, EndIndex, strSortOrder, strWhere).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from h in lstPam
                           select new[] {
                                    h.HeaderId.ToString(),
                                    h.HeaderId.ToString(),
                                    //GenerateAutoComplete(h.HeaderId, "WPSNumber",h.WPSNumber,"", "", (h.status.ToLower()==clsImplementationEnum.CommonStatus.DRAFT.GetStringValue().ToLower() ||
                                    //                                                                  h.status.ToLower()==clsImplementationEnum.CommonStatus.Returned.GetStringValue().ToLower()?false:true),"","WPSNumber",false,"autocomplete"),
                                    GenerateAutoComplete(h.HeaderId, "WPSNumber",h.WPSNumber,"", "", true,"","WPSNumber",false,"autocomplete"),
                                    GenerateAutoComplete(h.HeaderId, "WPSRevNo",h.WPSRevNo!=null?h.WPSRevNo.ToString():"","", "", true,"","WPSRevNo"),
                                    GenerateAutoComplete(h.HeaderId, "Jointtype",h.Jointtype,"", "", true,"","Jointtype"),
                                    GenerateAutoComplete(h.HeaderId, "WeldingProcess",h.WeldingProcess,"", "", true,"","WeldingProcess"),
                                    ////GenerateAutoComplete(h.HeaderId, "IQualityProject",h.IQualityProject,"", "", false,"","IQualityProject",false,"autocomplete"),
                                    //h.IQualityProject!=null?h.IQualityProject:string.Empty,
                                    h.status,
                                    HTMLActionString(h.HeaderId,"Timeline","Timeline","fa fa-clock-o iconspace","ShowProjectWiseWPSTimeline(" +h.HeaderId.ToString() + ");")
                                    }).ToList();

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string GenerateAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string cssClass = "", string title = "")
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control " + (cssClass != string.Empty ? cssClass : "");
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick=" + onClickMethod + "" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' data-lineid='" + rowId + "' hdElement='" + hdElement + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + " " + (disabled ? "disabled" : "") + " title='" + title + "' />";

            return strAutoComplete;
        }
        public static string GenerateGridButton(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";


            htmlControl = "<a id='" + inputID + "' name='" + inputID + "' data-lineid='" + rowId + "' class='btn btn-outline btn-circle btn-sm blue' " + onClickEvent + " ><i class='" + className + "'></i> " + buttonName + "</a>";

            //htmlControl = "<i  data-modal='' id='" + inputID + "' name='" + inputID + "' style='cursor:Pointer;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";

            return htmlControl;
        }
        public string HTMLActionString(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";
            htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            return htmlControl;
        }
    }
}
