﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.WPS.Controllers
{
    public class ProjectwiseWPSController : clsBase
    {
        // GET: WPS/ProjectwiseWPS

        [SessionExpireFilter]
        [AllowAnonymous]
        //  [UserPermissions]
        #region Index Page
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetHeaderGridDataPartial");
        }

        [HttpPost]
        public JsonResult NextWPSProjectWise(int? headerId, string status)
        {
            string strWhere = string.Empty;
                      
            if (headerId != 0)
            {
                ViewBag.Status = status;
                if (status == "Pending")
                {
                    strWhere += " 1=1 and status in('" + clsImplementationEnum.CommonStatus.DRAFT.GetStringValue() + "')";
                }
                else
                {
                    strWhere += " 1=1";
                }
                var lstResult = db.SP_WPS_PROJECTWISEWPS_HEADER_DETAILS(0, 0, "", strWhere).ToList();
                var q = lstResult.Where(i => i.HeaderId > headerId).OrderBy(i => i.HeaderId).FirstOrDefault();

                return Json(q, JsonRequestBehavior.AllowGet);
            }
            return Json(null);

        }

        [HttpPost]
        public JsonResult PrevWPSProjectWise(int? headerId, string status)
        {
            string strWhere = string.Empty;

            if (headerId != 0)
            {
                ViewBag.Status = status;
                if (status == "Pending")
                {
                    strWhere += " 1=1 and status in('" + clsImplementationEnum.CommonStatus.DRAFT.GetStringValue() + "')";
                }
                else
                {
                    strWhere += " 1=1";
                }
                var lstResult = db.SP_WPS_PROJECTWISEWPS_HEADER_DETAILS(0, 0, "", strWhere).ToList();
                var q = lstResult.Where(i => i.HeaderId < headerId).OrderByDescending(i => i.HeaderId).FirstOrDefault();

                return Json(q, JsonRequestBehavior.AllowGet);
            }
            return Json(null);

        }

        [HttpPost]
        public JsonResult LoadHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += " 1=1 and status in('" + clsImplementationEnum.CommonStatus.DRAFT.GetStringValue() + "')";
                }
                else
                {
                    strWhere += " 1=1";
                }
                string[] columnName = { "Project", "QualityProject", "WPSNumber", "Status" };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                strWhere += "and Location='" + objClsLoginInfo.Location + "'";

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstResult = db.SP_WPS_PROJECTWISEWPS_HEADER_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.QualityProject),
                            Convert.ToString(uc.Project),
                             Convert.ToString(uc.WPSNumber),
                            Convert.ToString("R" + uc.WPSRevNo),
                             Convert.ToString(uc.Jointtype),
                            Convert.ToString(uc.Status),
                            Convert.ToString(uc.Location),
                              GetActionButton(uc.HeaderId,uc.Status),
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string GetActionButton(int header, string status)
        {
            //if (status == clsImplementationEnum.CommonStatus.DRAFT.GetStringValue())
            //{
            return "<center><nobr><a href='" + WebsiteURL + "/WPS/ProjectwiseWPS/AddHeader?HeaderID=" + header + "' class='iconspace' ><i class='fa fa-eye'></i></a>" +
                    "<a id='btnDelete' name='btnAction' style='cursor:pointer;' Title='Delete Record'  Title='Delete Header' onClick='" + (status.ToLower() == clsImplementationEnum.CommonStatus.DRAFT.GetStringValue().ToLower() ? "DeleteHeader(" + header + ")" : "") + "' class='" + (status.ToLower() == clsImplementationEnum.CommonStatus.DRAFT.GetStringValue().ToLower() ? "iconspance" : "disabledicon") + "'><i class='fa fa-trash-o'></i></a></nobr></center>";
            //}
            //else
            //{
            //    return "<center><a href='/WPS/ProjectwiseWPS/AddHeader?HeaderID=" + header + "'><i style='margin-left:5px;' class='fa fa-eye'></i></a></center>";
            //}
        }
        #endregion

       
        [HttpPost]
        public JsonResult GetQualityProjectNo(string term)
        {
            db.Configuration.ProxyCreationEnabled = false;

            var res = db.WPS012.Where(c => c.QualityProject.Contains(term)).Select(x => new { x.QualityProject, x.HeaderId, x.WPSNumber }).Distinct().ToList();

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult FetchQualityData(string qualityproject)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var res = db.WPS012.Where(i => i.QualityProject.Equals(qualityproject, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        #region Header Details
        [SessionExpireFilter]
        public ActionResult AddHeader(string Status, int headerID = 0)
        {
            ViewBag.Status = Status;
            if (headerID > 0)
            {
                string currentUser = objClsLoginInfo.UserName;
                //WPS012 objWPS012 = db.WPS012.Where(x => x.HeaderId == headerID && x.Location != objClsLoginInfo.UserName).FirstOrDefault();
                //if (objWPS012 == null)
                //{
                //    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                //}
            }
            ViewBag.HeaderID = headerID;
            return View();
        }
        [SessionExpireFilter]
        [HttpPost]
        public ActionResult LoadHeaderForm(int headerID)
        {
            WPS012 objWPS012 = new WPS012();
            if (headerID > 0)
            {
                objWPS012 = db.WPS012.Where(x => x.HeaderId == headerID).FirstOrDefault();
                objWPS012.Project = db.COM001.Where(i => i.t_cprj == objWPS012.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                ViewBag.QualityProject = objWPS012.QualityProject;
                ViewBag.Project = objWPS012.Project;
                ViewBag.IdenticalProject = string.Join(",", db.WPS013.Where(i => i.HeaderId == headerID).Select(i => i.IQualityProject).ToList());

                ViewBag.Action = "headeredit";
            }
            else
            {
                objWPS012.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
            }
            objWPS012.Location = db.COM002.Where(i => i.t_dtyp == 1 && i.t_dimx == objClsLoginInfo.Location).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
          ///  var lstQID17 = db.WPS010_Log.Where(i => i.Location == objClsLoginInfo.Location).Select(i => i.WPSNumber).Distinct().ToList();
            var lstQID17 = db.WPS010_Log.Select(i => i.WPSNumber).Distinct().ToList();

            ViewBag.lstWPS = lstQID17;
            return PartialView("_LoadProjectwiseWPSHeaderForm", objWPS012);
        }

        [HttpPost]
        public ActionResult SaveHeader(WPS012 wps012, FormCollection fc)
        {
            CustomResponceMsg objResponseMsg = new CustomResponceMsg();
            try
            {
                WPS012 objWPS012 = new WPS012();
                bool IsEdited = false;
                if (wps012.HeaderId > 0)
                {
                    objWPS012 = db.WPS012.Where(x => x.HeaderId == wps012.HeaderId).FirstOrDefault();
                    IsEdited = true;
                }
                objWPS012.Location = wps012.Location.Split('-')[0];
                objWPS012.QualityProject = wps012.QualityProject;
                objWPS012.Project = wps012.Project.Split('-')[0];
                objWPS012.WPSNumber = wps012.WPSNumber;
                objWPS012.WPSRevNo = wps012.WPSRevNo;
                objWPS012.Jointtype = wps012.Jointtype;
                objWPS012.WeldingProcess = wps012.WeldingProcess;
                objWPS012.Status = wps012.Status;

                if (IsEdited)
                {
                    objWPS012.EditedBy = objClsLoginInfo.UserName;
                    objWPS012.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    List<WPS013> lstDesWPS013 = db.WPS013.Where(x => x.HeaderId == objWPS012.HeaderId).ToList();
                    if (lstDesWPS013 != null && lstDesWPS013.Count > 0)
                    {
                        db.WPS013.RemoveRange(lstDesWPS013);
                    }
                    db.SaveChanges();
                    if (fc["IdenticalProject"] != null)
                    {
                        WPS013 objWPS013 = new WPS013();
                        var strIQProject = fc["IdenticalProject"].ToString();
                        var strIQualityProject = strIQProject.Split('-')[0];
                        string[] arrayIQualityProject = strIQualityProject.Split(',').ToArray();
                        WPS013 newobjWPS013 = db.WPS013.Where(x => x.HeaderId == objWPS012.HeaderId).FirstOrDefault();
                        List<WPS013> lstWPS013 = new List<WPS013>();
                        for (int i = 0; i < arrayIQualityProject.Length; i++)
                        {
                            WPS013 objWPS013Add = new WPS013();
                            objWPS013Add.HeaderId = objWPS012.HeaderId;
                            objWPS013Add.Project = objWPS012.Project;
                            objWPS013Add.QualityProject = objWPS012.QualityProject;
                            objWPS013Add.Location = objWPS012.Location;
                            objWPS013Add.IQualityProject = arrayIQualityProject[i];
                            objWPS013Add.WPSNumber = objWPS012.WPSNumber;
                            objWPS013Add.CreatedBy = objClsLoginInfo.UserName;
                            objWPS013Add.CreatedOn = DateTime.Now;
                            lstWPS013.Add(objWPS013Add);

                        }
                        if (lstWPS013 != null && lstWPS013.Count > 0)
                        {
                            db.WPS013.AddRange(lstWPS013);
                        }
                        db.SaveChanges();
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                }
                else
                {
                    objWPS012.CreatedBy = objClsLoginInfo.UserName;
                    objWPS012.CreatedOn = DateTime.Now;
                    db.WPS012.Add(objWPS012);
                    db.SaveChanges();
                    if (fc["IdenticalProject"] != null)
                    {
                        WPS013 objWPS013 = new WPS013();
                        var strIQProject = fc["IdenticalProject"].ToString();
                        var strIQualityProject = strIQProject.Split('-')[0];
                        string[] arrayIQualityProject = strIQualityProject.Split(',').ToArray();
                        WPS013 newobjWPS013 = db.WPS013.Where(x => x.HeaderId == objWPS012.HeaderId).FirstOrDefault();
                        List<WPS013> lstWPS013 = new List<WPS013>();
                        for (int i = 0; i < arrayIQualityProject.Length; i++)
                        {
                            WPS013 objWPS013Add = new WPS013();
                            objWPS013Add.HeaderId = objWPS012.HeaderId;
                            objWPS013Add.Project = objWPS012.Project;
                            objWPS013Add.QualityProject = objWPS012.QualityProject;
                            objWPS013Add.Location = objWPS012.Location;
                            objWPS013Add.IQualityProject = arrayIQualityProject[i];
                            objWPS013Add.WPSNumber = objWPS012.WPSNumber;
                            objWPS013Add.CreatedBy = objClsLoginInfo.UserName;
                            objWPS013Add.CreatedOn = DateTime.Now;
                            lstWPS013.Add(objWPS013Add);

                        }
                        if (lstWPS013 != null && lstWPS013.Count > 0)
                        {
                            db.WPS013.AddRange(lstWPS013);
                        }
                        db.SaveChanges();
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();

                }
                objResponseMsg.HeaderID = objWPS012.HeaderId;
                objResponseMsg.Status = objWPS012.Status;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SendForApproval(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                WPS012 objWPS012 = db.WPS012.Where(u => u.HeaderId == HeaderId).SingleOrDefault();
                if (objWPS012 != null)
                {
                    objWPS012.Status = clsImplementationEnum.CommonStatus.SendForApprovel.GetStringValue();
                    objWPS012.SubmittedBy = objClsLoginInfo.UserName;
                    objWPS012.SubmittedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.sentforApprove;

                    #region Get BU and Project From WPSNo                                            
                    var project = new NDEModels().GetQMSProject(objWPS012.QualityProject).Project;
                    var BU = db.COM001.Where(i => i.t_cprj.Equals(project, StringComparison.OrdinalIgnoreCase)).FirstOrDefault().t_entu;
                    #endregion
                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.WE2.GetStringValue(), project, BU, objWPS012.Location, "Project  maintained for WPS: " + objWPS012.WPSNumber + " has been submitted for your Approval", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/WPS/ApproveProjectwiseWPS/ViewDetail?HeaderID=" + objWPS012.HeaderId);
                    #endregion

                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable;
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
                throw ex;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteHeader(int id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                WPS012 objWPS012 = db.WPS012.Where(u => u.HeaderId == id).SingleOrDefault();
                if (objWPS012 != null)
                {

                    List<WPS013> lstDesWPS013 = db.WPS013.Where(x => x.HeaderId == objWPS012.HeaderId).ToList();
                    if (lstDesWPS013 != null && lstDesWPS013.Count > 0)
                    {
                        db.WPS013.RemoveRange(lstDesWPS013);
                    }
                    db.SaveChanges();
                    db.WPS012.Remove(objWPS012);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult getWPSRev(string wpsnumber, string qualityproject,string Location)
        {
            var msg = string.Empty;
            Qualityprojectwiseproject objProjectDataModel = new Qualityprojectwiseproject();
            if (wpsnumber != string.Empty && qualityproject != string.Empty)
            {
                //var wpsrev = db.WPS010_Log.Where(i => i.WPSNumber == wpsnumber).FirstOrDefault().WPSRevNo;
                //var objWPS010_Log= db.WPS010_Log.Where(i => i.WPSNumber == wpsnumber).OrderByDescending(i => i.WPSRevNo).FirstOrDefault();
               // var objWPS010_Log = db.WPS010_Log.Where(i => i.WPSNumber == wpsnumber && i.Location == Location).OrderByDescending(i => i.WPSRevNo).FirstOrDefault();
                var objWPS010_Log = db.WPS010_Log.Where(i => i.WPSNumber == wpsnumber).OrderByDescending(i => i.WPSRevNo).FirstOrDefault();

                objProjectDataModel.rev = objWPS010_Log.WPSRevNo;

                //var wpsJointtype = db.WPS010_Log.Where(i => i.WPSNumber == wpsnumber).FirstOrDefault().Jointtype;                
                objProjectDataModel.wpsJointtype = objWPS010_Log.Jointtype;

                //var strweldingprocess = (from a in db.WPS010_Log
                //                         where a.WPSNumber == wpsnumber
                //                         select new { weldProc = a.WeldingProcess1 + "," + a.WeldingProcess2 + "," + a.WeldingProcess3 + "," + a.WeldingProcess4 }).FirstOrDefault();

                var strweldingprocess = objWPS010_Log.WeldingProcess1 + (objWPS010_Log.WeldingProcess2 != null && objWPS010_Log.WeldingProcess2 != string.Empty ? "," + objWPS010_Log.WeldingProcess2 : string.Empty)
                                                + (objWPS010_Log.WeldingProcess3 != null && objWPS010_Log.WeldingProcess3 != string.Empty ? "," + objWPS010_Log.WeldingProcess3 : string.Empty)
                                                + (objWPS010_Log.WeldingProcess4 != null && objWPS010_Log.WeldingProcess4 != string.Empty ? "," + objWPS010_Log.WeldingProcess4 : string.Empty)
                                        ;
                objProjectDataModel.weldingprocess = strweldingprocess;

                WPS012 objwps012 = db.WPS012.Where(i => i.QualityProject == qualityproject && i.Location == objClsLoginInfo.Location && i.WPSNumber == wpsnumber && i.WPSRevNo == objWPS010_Log.WPSRevNo).FirstOrDefault();
                if (objwps012 != null)
                {
                    objProjectDataModel.Key = false;
                }
                else
                {
                    objProjectDataModel.Key = true;
                }

                var strDraft = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                var strSendForApprovel = clsImplementationEnum.CommonStatus.SendForApprovel.GetStringValue();

                objwps012 = null;
                objwps012 = db.WPS012.Where(i => i.QualityProject == qualityproject && i.Location == objClsLoginInfo.Location && i.WPSNumber == wpsnumber && (i.Status == strDraft || i.Status == strSendForApprovel)).FirstOrDefault();
                if (objwps012 != null)
                {
                    objProjectDataModel.IsAnyPendingWPS = true;
                    objProjectDataModel.PendingWPSRevNo = Convert.ToString(objwps012.WPSRevNo);
                }
                else
                {
                    objProjectDataModel.IsAnyPendingWPS = false;
                }
            }
            else
            {
                objProjectDataModel.Key = true;
            }

            if (wpsnumber == string.Empty)
            {
                msg = "Please Enter WPS Number";
            }
            else if (qualityproject == string.Empty)
            {
                msg = "Please Enter Quality Project";
            }

            return Json(new
            {
                model = objProjectDataModel,
                msg = msg
            }
            , JsonRequestBehavior.AllowGet);
        }

        public class CustomResponceMsg : clsHelper.ResponseMsg
        {
            public string Project;
            public int HeaderID;
            public string Status;
        }

        public class Qualityprojectwiseproject : ProjectDataModel
        {
            public string Project { get; set; }
            public string Code { get; set; }
            public int? rev { get; set; }
            public string wpsJointtype { get; set; }
            public string weldingprocess { get; set; }
            public bool Key { get; set; }

            public bool IsAnyPendingWPS { get; set; }
            public string PendingWPSRevNo { get; set; }
        }

        [HttpPost]
        public ActionResult GetQualityProjectWiseProject(string qualityProject, string location)
        {
            Qualityprojectwiseproject objProjectDataModel = new Qualityprojectwiseproject();
            string project = db.QMS010.Where(a => a.QualityProject == qualityProject).FirstOrDefault().Project;

            var lstProject = (from a in db.COM001
                              where a.t_cprj == project
                              select new Qualityprojectwiseproject { Code = a.t_cprj, Project = a.t_cprj + " - " + a.t_dsca }).FirstOrDefault();

            return Json(lstProject, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetQualityProjectDetails(string term)
        {
            string username = objClsLoginInfo.UserName;
            var lstATHLocation = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.Location).Distinct().ToList();
            var lstCOMLocation = db.COM003.Where(i => i.t_psno.Equals(username, StringComparison.OrdinalIgnoreCase) && i.t_actv == 1).Select(i => i.t_loca).Distinct().ToList();
            List<Projects> lstQualityProject = null;
            lstCOMLocation = lstATHLocation.Union(lstCOMLocation).ToList();
            if (string.IsNullOrWhiteSpace(term))
            {
                lstQualityProject = db.QMS010.Where(i => lstCOMLocation.Contains(i.Location)
                                            ).Select(i => new Projects { Value = i.QualityProject, projectCode = i.QualityProject }).Distinct().Take(10).ToList();//&& i.CreatedBy.Equals(username,StringComparison.OrdinalIgnoreCase)
            }
            else
            {
                lstQualityProject = db.QMS010.Where(i => i.QualityProject.ToLower().Contains(term.ToLower())
                                                               && lstCOMLocation.Contains(i.Location)
                                                               ).Select(i => new Projects { Value = i.QualityProject, projectCode = i.QualityProject }).Distinct().ToList();//&& i.CreatedBy.Equals(username,StringComparison.OrdinalIgnoreCase)
            }
            return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetIdenticalProjects(string search, string param)
        {
            List<ddlValue> identicalProject = db.QMS010.Where(i => !i.QualityProject.Equals(param, StringComparison.OrdinalIgnoreCase) && i.QualityProject.ToLower().Contains(search.ToLower())).Select(i => new ddlValue { id = i.QualityProject, text = i.QualityProject }).Distinct().ToList();
            return Json(identicalProject, JsonRequestBehavior.AllowGet);

        }
        public ActionResult GetAllIdenticalProjects(string param, string Location)
        {
            var ApprovedStatus = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
            var listExistAllIQualityProject = db.WPS013.Select(i => i.IQualityProject).Distinct().ToList();

            List<ddlValue> listidenticalProject = db.QMS010.Where(i => !i.QualityProject.Equals(param, StringComparison.OrdinalIgnoreCase) && !listExistAllIQualityProject.Contains(i.QualityProject)).Select(i => new ddlValue { id = i.QualityProject, text = i.QualityProject }).Distinct().ToList();
            var lisIQualProjForCurrentQualityProject = db.WPS013.Where(i => i.QualityProject == param && i.Location == Location).Select(i => i.IQualityProject).ToList();
            var IdenticalProject = string.Join(",", lisIQualProjForCurrentQualityProject);
            var IsDisabled = (db.WPS012.Where(i => i.QualityProject == param && i.Location == Location && i.Status == ApprovedStatus).Any());
            lisIQualProjForCurrentQualityProject.ForEach(QProject =>
            {
                listidenticalProject.Add(new ddlValue { id = QProject, text = QProject });
            });

            return Json(new { listidenticalProject = listidenticalProject, IdenticalProject = IdenticalProject, IsDisabled = IsDisabled }, JsonRequestBehavior.AllowGet);

        }
        public ActionResult GetIdenticalProjectEdit(string param)
        {
            List<ddlValue> identicalProject = db.WPS013.Where(i => !i.QualityProject.Equals(param, StringComparison.OrdinalIgnoreCase)).Select(i => new ddlValue { id = i.QualityProject, text = i.QualityProject }).Distinct().ToList();
            return Json(identicalProject, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Export Grid Data
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;

                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    List<SP_WPS_GET_LOCATION_WISE_QUALITYPROJECT_HEADER_Result> lst = db.SP_WPS_GET_LOCATION_WISE_QUALITYPROJECT_HEADER(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      Location = li.Location
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);

                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    List<SP_WPS_GET_PROJECT_WISE_WPS_DETAILS_Result> lst = db.SP_WPS_GET_PROJECT_WISE_WPS_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      WPSNumber = li.WPSNumber,
                                      WPSRevNo = "R" + li.WPSRevNo,
                                      Jointtype = li.Jointtype,
                                      WeldingProcess = li.WeldingProcess,
                                      //IdenticalProject = li.IQualityProject,
                                      Status = li.status
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region  New UI Changes
        [HttpPost]
        public JsonResult LoadQualityProjectWPSHeaderData(JQueryDataTableParamModel param)
        {
            ViewBag.Status = param.CTQCompileStatus.ToString();
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += " 1=1 and status in('" + clsImplementationEnum.CommonStatus.DRAFT.GetStringValue() + "')";
                }
                else
                {
                    strWhere += " 1=1";
                }
                string[] columnName = { "Project", "QualityProject", "Location" };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                strWhere += "and Location='" + objClsLoginInfo.Location + "'";

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstResult = db.SP_WPS_GET_LOCATION_WISE_QUALITYPROJECT_HEADER
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.QualityProject),
                            Convert.ToString(uc.Project),
                            Convert.ToString(uc.Location),
                            "<center><nobr><a href=\""+WebsiteURL+"/WPS/ProjectwiseWPS/AddHeader?HeaderID=" + uc.HeaderId  + "&Status="+ param.CTQCompileStatus.ToString() +"\" class=\"iconspace\" ><i class=\"fa fa-eye\"></i></a>"+"</nobr></center>"
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]
        [HttpPost]
        public ActionResult AddEditProjectWiseWPSHeader(int headerID,string Status)
        {

            ViewBag.Status = Status;
            WPS012 objWPS012 = new WPS012();
            if (headerID > 0)
            {
                objWPS012 = db.WPS012.Where(x => x.HeaderId == headerID).FirstOrDefault();
                ViewBag.Location = db.COM002.Where(i => i.t_dtyp == 1 && i.t_dimx == objWPS012.Location).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
                ViewBag.QualityProject = objWPS012.QualityProject;
                ViewBag.Project = db.COM001.Where(i => i.t_cprj == objWPS012.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                //ViewBag.IdenticalProject = string.Join(",", db.WPS013.Where(i => i.QualityProject == objWPS012.QualityProject && i.Location == objWPS012.Location).Select(i => i.IQualityProject).ToList());
            }
            else
            {
                objWPS012.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                objWPS012.Location = objClsLoginInfo.Location;
                ViewBag.Location = db.COM002.Where(i => i.t_dtyp == 1 && i.t_dimx == objClsLoginInfo.Location).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
            }

          //  var lstQID17 = db.WPS010_Log.Where(i => i.Location == objClsLoginInfo.Location).Select(i => i.WPSNumber).Distinct().ToList();
            var lstQID17 = db.WPS010_Log.Select(i => i.WPSNumber).Distinct().ToList();

            ViewBag.lstWPS = lstQID17;
            return PartialView("_AddEditProjectWiseWPSHeader", objWPS012);
        }

        [SessionExpireFilter]
        [HttpPost]
        public ActionResult LoadProjectWiseWPSDetails(string status)
        {
            ViewBag.Status = status;
            return PartialView("_LoadProjectWiseWPSDetails");
        }
        public ActionResult loadProjectWiseWPSDetailsDataTable(JQueryDataTableParamModel param, string location, string qualityProject)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += " 1=1 and status in('" + clsImplementationEnum.CommonStatus.DRAFT.GetStringValue() + "')";
                }
                else
                {
                    strWhere += " 1=1";
                }
                strWhere += " and QualityProject='" + qualityProject + "' and location='" + location + "' ";
                string[] columnName = { "Project", "QualityProject", "WPSNumber", "Status" };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstPam = db.SP_WPS_GET_PROJECT_WISE_WPS_DETAILS(StartIndex, EndIndex, strSortOrder, strWhere).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                int newRecordId = 0;
                var newRecord = new[] {
                                    "",
                                    newRecordId.ToString(),
                                    GenerateAutoComplete(newRecordId, "WPSNumber","","", "", false,"","WPSNumber",false,"autocomplete wpsnumber"),
                                    GenerateAutoComplete(newRecordId, "WPSRevNo","","", "", true,"","WPSRevNo",false,"wpsrevno"),
                                    GenerateAutoComplete(newRecordId, "Jointtype","","", "", true,"","Jointtype",false,"jointtype"),
                                    GenerateAutoComplete(newRecordId, "WeldingProcess","","", "", true,"","WeldingProcess",false,"weldingprocess"),
                                    ////GenerateAutoComplete(newRecordId, "IQualityProject","","", "", false,"","IQualityProject",false,"autocomplete"),
                                    //"",
                                    clsImplementationEnum.CommonStatus.DRAFT.GetStringValue(),
                                    GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-plus", "SaveLine(this);" )
                                };

                var res = (from h in lstPam
                           select new[] {
                                    h.HeaderId.ToString(),
                                    h.HeaderId.ToString(),
                                    //GenerateAutoComplete(h.HeaderId, "WPSNumber",h.WPSNumber,"", "", (h.status.ToLower()==clsImplementationEnum.CommonStatus.DRAFT.GetStringValue().ToLower() ||
                                    //                                                                  h.status.ToLower()==clsImplementationEnum.CommonStatus.Returned.GetStringValue().ToLower()?false:true),"","WPSNumber",false,"autocomplete"),
                                    GenerateAutoComplete(h.HeaderId, "WPSNumber",h.WPSNumber,"", "", true,"","WPSNumber",false,"autocomplete"),
                                    GenerateAutoComplete(h.HeaderId, "WPSRevNo",h.WPSRevNo!=null?"R"+h.WPSRevNo.ToString():"","", "", true,"","WPSRevNo"),
                                    GenerateAutoComplete(h.HeaderId, "Jointtype",h.Jointtype,"", "", true,"","Jointtype"),
                                    GenerateAutoComplete(h.HeaderId, "WeldingProcess",h.WeldingProcess,"", "", true,"","WeldingProcess"),
                                    ////GenerateAutoComplete(h.HeaderId, "IQualityProject",h.IQualityProject,"", "", false,"","IQualityProject",false,"autocomplete"),
                                    //h.IQualityProject!=null?h.IQualityProject:string.Empty,
                                    h.status,
                                    HTMLActionString(h.HeaderId,"Delete","Delete Record","fa fa-trash-o "+(h.status.ToLower()==clsImplementationEnum.CommonStatus.DRAFT.GetStringValue().ToLower()?" iconspace":" disabledicon"),(h.status.ToLower()==clsImplementationEnum.CommonStatus.DRAFT.GetStringValue().ToLower()?"DeleteHeader("+ h.HeaderId+ "); ":""))+
                                    HTMLActionString(h.HeaderId,"Timeline","Timeline","fa fa-clock-o iconspace","ShowProjectWiseWPSTimeline(" +h.HeaderId.ToString() + ");")
                                    }).ToList();

                res.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public string GenerateAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string cssClass = "", string title = "")
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control " + (cssClass != string.Empty ? cssClass : "");
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick=" + onClickMethod + "" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' data-lineid='" + rowId + "' hdElement='" + hdElement + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + " " + (disabled ? "disabled" : "") + " title='" + title + "' />";

            return strAutoComplete;
        }
        public static string GenerateGridButton(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";


            htmlControl = "<a id='" + inputID + "' name='" + inputID + "' data-lineid='" + rowId + "' class='btn btn-outline btn-circle btn-sm blue' " + onClickEvent + " ><i class='" + className + "'></i> " + buttonName + "</a>";

            //htmlControl = "<i  data-modal='' id='" + inputID + "' name='" + inputID + "' style='cursor:Pointer;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";

            return htmlControl;
        }
        public string HTMLActionString(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";
            htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            return htmlControl;
        }
        public ActionResult SaveProjectWiseWPS(WPS012 wps012, string IQualityProject)
        {
            CustomResponceMsg objResponseMsg = new CustomResponceMsg();
            try
            {
                WPS012 objWPS012 = new WPS012();
                if (wps012.HeaderId > 0)
                {
                    objWPS012 = db.WPS012.Where(x => x.HeaderId == wps012.HeaderId).FirstOrDefault();
                }
                else
                {
                    objWPS012.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                    objWPS012.Location = wps012.Location;
                    objWPS012.QualityProject = wps012.QualityProject;
                    objWPS012.Project = wps012.Project;
                    objWPS012.WPSNumber = wps012.WPSNumber;
                    objWPS012.WPSRevNo = wps012.WPSRevNo;
                    objWPS012.Jointtype = wps012.Jointtype;
                    objWPS012.WeldingProcess = wps012.WeldingProcess;
                    objWPS012.CreatedBy = objClsLoginInfo.UserName;
                    objWPS012.CreatedOn = DateTime.Now;
                    db.WPS012.Add(objWPS012);
                    db.SaveChanges();
                    //if (IQualityProject.Length > 0)
                    //{
                    //    string[] arrayIQualityProject = IQualityProject.Split(',').ToArray();
                    //    List<WPS013> lstWPS013 = new List<WPS013>();
                    //    for (int i = 0; i < arrayIQualityProject.Length; i++)
                    //    {
                    //        WPS013 objWPS013Add = new WPS013();
                    //        objWPS013Add.HeaderId = objWPS012.HeaderId;
                    //        objWPS013Add.Project = objWPS012.Project;
                    //        objWPS013Add.QualityProject = objWPS012.QualityProject;
                    //        objWPS013Add.Location = objWPS012.Location;
                    //        objWPS013Add.IQualityProject = arrayIQualityProject[i];
                    //        objWPS013Add.WPSNumber = objWPS012.WPSNumber;
                    //        objWPS013Add.CreatedBy = objClsLoginInfo.UserName;
                    //        objWPS013Add.CreatedOn = DateTime.Now;
                    //        lstWPS013.Add(objWPS013Add);
                    //    }
                    //    if (lstWPS013 != null && lstWPS013.Count > 0)
                    //    {
                    //        db.WPS013.AddRange(lstWPS013);
                    //        db.SaveChanges();
                    //    }
                    //}
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                    objResponseMsg.HeaderID = objWPS012.HeaderId;
                    objResponseMsg.Status = objWPS012.Status;
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SendForApprovalSelected(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
            try
            {
                for (int i = 0; i < arrayHeaderIds.Length; i++)
                {
                    int headerId = Convert.ToInt32(arrayHeaderIds[i]);
                    SendForApproval(headerId);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.sentforApprove;

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateIdenticalProjects(int HeaderId, string IQualityProject)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<string> arrayIQualityProject = IQualityProject.Split(',').ToList();
                var objWPS013 = db.WPS013.Where(i => i.HeaderId == HeaderId && !arrayIQualityProject.Contains(i.IQualityProject)).ToList();
                var objWPS013Existing = db.WPS013.Where(i => i.HeaderId == HeaderId).Select(i => i.IQualityProject).ToList();
                var objWPSNewAdd = arrayIQualityProject.Except(objWPS013Existing).ToList();

                if ((objWPS013 != null && objWPS013.Count > 0) || (objWPSNewAdd != null && objWPSNewAdd.Count > 0))
                {
                    if (objWPS013 != null && objWPS013.Count > 0)
                    {
                        db.WPS013.RemoveRange(objWPS013);
                    }
                    if (objWPSNewAdd != null && objWPSNewAdd.Count > 0)
                    {
                        var objWPS012 = db.WPS012.Where(i => i.HeaderId == HeaderId).FirstOrDefault();
                        List<WPS013> lstWPS013 = new List<WPS013>();
                        foreach (var item in objWPSNewAdd)
                        {
                            WPS013 objWPS013Add = new WPS013();
                            objWPS013Add.HeaderId = objWPS012.HeaderId;
                            objWPS013Add.Project = objWPS012.Project;
                            objWPS013Add.QualityProject = objWPS012.QualityProject;
                            objWPS013Add.Location = objWPS012.Location;
                            objWPS013Add.IQualityProject = item;
                            objWPS013Add.WPSNumber = objWPS012.WPSNumber;
                            objWPS013Add.CreatedBy = objClsLoginInfo.UserName;
                            objWPS013Add.CreatedOn = DateTime.Now;
                            lstWPS013.Add(objWPS013Add);
                        }
                        if (lstWPS013 != null && lstWPS013.Count > 0)
                        {
                            db.WPS013.AddRange(lstWPS013);
                        }
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateIdenticalProjectsHeaders(string QualityProject, string Location, string Project, string IQualityProject)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<string> arrayIQualityProject = IQualityProject.Split(',').ToList();
                var objWPS013 = db.WPS013.Where(i => i.QualityProject == QualityProject && i.Location == Location && !arrayIQualityProject.Contains(i.IQualityProject)).ToList();
                var objWPS013Existing = db.WPS013.Where(i => i.QualityProject == QualityProject && i.Location == Location).Select(i => i.IQualityProject).ToList();
                var objWPSNewAdd = arrayIQualityProject.Except(objWPS013Existing).ToList();

                if ((objWPS013 != null && objWPS013.Count > 0) || (objWPSNewAdd != null && objWPSNewAdd.Count > 0))
                {
                    if (objWPS013 != null && objWPS013.Count > 0)
                    {
                        db.WPS013.RemoveRange(objWPS013);
                    }
                    if (objWPSNewAdd != null && objWPSNewAdd.Count > 0)
                    {
                        List<WPS013> lstWPS013 = new List<WPS013>();
                        foreach (var item in objWPSNewAdd)
                        {
                            WPS013 objWPS013Add = new WPS013();
                            objWPS013Add.HeaderId = 0;
                            objWPS013Add.Project = Project;
                            objWPS013Add.QualityProject = QualityProject;
                            objWPS013Add.Location = Location;
                            objWPS013Add.IQualityProject = item;
                            objWPS013Add.WPSNumber = null;
                            objWPS013Add.CreatedBy = objClsLoginInfo.UserName;
                            objWPS013Add.CreatedOn = DateTime.Now;
                            lstWPS013.Add(objWPS013Add);
                        }
                        if (lstWPS013 != null && lstWPS013.Count > 0)
                        {
                            db.WPS013.AddRange(lstWPS013);
                        }
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ShowTimeline(int HeaderId)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.TimelineTitle = "Project Wise WPS History";
            model.Title = "PWPS";
            WPS012 objPAM006 = db.WPS012.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            model.CreatedBy = Manager.GetUserNameFromPsNo(objPAM006.CreatedBy);
            model.CreatedOn = objPAM006.CreatedOn;
            model.SubmittedBy = Manager.GetUserNameFromPsNo(objPAM006.SubmittedBy);
            model.SubmittedOn = objPAM006.SubmittedOn;
            model.ApprovedBy = Manager.GetUserNameFromPsNo(objPAM006.ApprovedBy);
            model.ApprovedOn = objPAM006.ApprovedOn;

            return PartialView("~/Views/Shared/_TimelineProgress2.cshtml", model);
        }

        public ActionResult CheckProjectAsIdenticalProject(string QualityProject)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (db.WPS013.Any(i => i.IQualityProject == QualityProject))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = QualityProject + " is as maintain identical project in " + db.WPS013.Where(i => i.IQualityProject == QualityProject).FirstOrDefault().QualityProject;
                }
                else
                {
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}