﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQS.Models;
using IEMQS.Areas.NDE.Models;

namespace IEMQS.Areas.WPS.Controllers
{
    public class WPSKeyMasterController : clsBase
    {
        // GET: WPS/WPSKeyMaster
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LoadWPSKeyListDataTable(JQueryDataTableParamModel param)
        {
            try
            {
                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                //whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "", "wps1.Location");
                var lstLoc = string.Join(",", db.ATH001.Where(x => x.Employee == objClsLoginInfo.UserName).Select(x => x.Location).Distinct().ToList());

                whereCondition += Manager.MakeStringInCondition("wps3.Location", lstLoc);

                //whereCondition += " and wps1.Location IN('HZW', 'HZW')";

                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += " and (wps3.Location like '%" + param.sSearch + "%' or wps3.Key1 like '%" + param.sSearch + "%' or wps3.Key2 like '%" + param.sSearch + "%' or wps3.JointType like '%" + param.sSearch + "%' or wps3.CreatedBy like '%" + param.sSearch + "%')";
                }
                else
                {                   
                        whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);                   
                }


                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstHeader = db.SP_WPS_GetWPSKey(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstHeader
                          select new[] {                      
                        Convert.ToString(a.Key1),
                        Convert.ToString(a.Key2),
                        a.JointType,
                        a.CreatedBy,
                        Convert.ToDateTime(a.CreatedOn).ToString("dd/MM/yyyy"),
                         a.Location,
                         a.InUsed.ToString().ToLower(),
                           Convert.ToString(a.Id),
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult WPSKeyMasterPartial(int id, string InUsed)
        {
            WPS003 objWPS003 = new WPS003();
            NDEModels objNDEModels = new NDEModels();
            //var location = (from a in db.COM003
            //                join b in db.COM002 on a.t_loca equals b.t_dimx
            //                where b.t_dtyp == 1 && a.t_psno.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
            //                select b.t_dimx + " - " + b.t_desc).FirstOrDefault();
            string location = string.Empty;
            if (id > 0)
            {
                objWPS003 = db.WPS003.Where(i => i.Id == id).FirstOrDefault();
                location = (from a in db.COM002
                            where a.t_dtyp == 1 && a.t_dimx.Equals(objWPS003.Location, StringComparison.OrdinalIgnoreCase)
                            select a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            }
            else
            {
                location = (from a in db.COM003
                            join b in db.COM002 on a.t_loca equals b.t_dimx
                            where b.t_dtyp == 1 && a.t_psno.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase) && a.t_actv == 1
                            select b.t_dimx + " - " + b.t_desc).FirstOrDefault();
            }

            ViewBag.Location = location;

            ViewBag.ParentMetal = GetSubCatagory("Parent Metal", location.Split('-')[0]).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description });
            //ViewBag.WeldingProcess = GetSubCatagory("Welding Process", location.Split('-')[0]).Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description }).Distinct();
            ViewBag.WeldingProcess = GetSubCatagory("Welding Process", location.Split('-')[0]).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description }).Distinct().ToList();
            var jointtype = GetSubCatagory("Joint Type", location.Split('-')[0]).Select(i => new { i.Code, i.Description }).Distinct();
            var dist = jointtype;
            ViewBag.JointType = jointtype.Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description });
            ViewBag.PWHT = GetSubCatagory("PWHT", location.Split('-')[0]).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description });
            ViewBag.InUsed = InUsed;
            if (id > 0)
            {
                objWPS003 = db.WPS003.Where(i => i.Id == id).FirstOrDefault();
                ViewBag.ParentMetal1Selected = objWPS003.Parentmetal1; // objNDEModels.GetCategory(objWPS003.Parentmetal1).CategoryDescription;
                ViewBag.ParentMetal2Selected = objWPS003.Parentmetal2;// objNDEModels.GetCategory(objWPS003.Parentmetal2).CategoryDescription;
                ViewBag.WeldingProcess1Selected = objNDEModels.GetCategory(objWPS003.WeldingProcess1).CategoryDescription;
                ViewBag.WeldingProcess2Selected = objNDEModels.GetCategory(objWPS003.WeldingProcess2).CategoryDescription;
                ViewBag.WeldingProcess3Selected = objNDEModels.GetCategory(objWPS003.WeldingProcess3).CategoryDescription;
                ViewBag.WeldingProcess4Selected = objNDEModels.GetCategory(objWPS003.WeldingProcess4).CategoryDescription;
                ViewBag.JointTypeSelected = objNDEModels.GetCategory(objWPS003.JointType).CategoryDescription;
                ViewBag.PWHTSelected = objNDEModels.GetCategory(objWPS003.PWHT).CategoryDescription;

            }
            return PartialView("_WPSKeyMasterPartial", objWPS003);
        }

        [HttpPost]
        public ActionResult Save(FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            WPS003 objWPS003 = new WPS003();
            try
            {
                if (fc != null)
                {
                    int id = Convert.ToInt32(fc["Id"]);
                    string location = fc["Location"].Split('-')[0];
                    if (!isWPSKeyExist(location, Convert.ToInt32(fc["Key1"]), Convert.ToInt32(fc["Key2"]), id))
                    {                        
                        if (id > 0)
                        {
                            var objInUsed = db.SP_WPS_GetWPSKey(0, 0, "", "wps3.Id=" + id).FirstOrDefault();
                            if (objInUsed != null && objInUsed.InUsed.HasValue && objInUsed.InUsed.Value)
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = "You Can Not Modify This Key Because Already Used In WPS Number Index Master.";
                                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                            }

                            #region Update WPS Key Existing
                            objWPS003 = db.WPS003.Where(i => i.Id == id).FirstOrDefault();

                            if (isWPSKeyExistInWPSNoIndex(location, objWPS003.Key1, objWPS003.Key2))
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = string.Format(clsImplementationMessage.WPSKeyMaster.RestrictUpdate, objWPS003.Key1, objWPS003.Key2);
                            }
                            else
                            {
                                objWPS003.Parentmetal1 = fc["Parentmetal1"];
                                objWPS003.Parentmetal2 = fc["Parentmetal2"];
                                objWPS003.WeldingProcess1 = fc["WeldingProcess1"];
                                objWPS003.WeldingProcess2 = fc["WeldingProcess2"];
                                objWPS003.WeldingProcess3 = fc["WeldingProcess3"];
                                objWPS003.WeldingProcess4 = fc["WeldingProcess4"];
                                objWPS003.JointType = fc["JointType"];
                                objWPS003.PWHT = fc["PWHT"];
                                objWPS003.Remarks = fc["Remarks"];
                                objWPS003.EditedBy = objClsLoginInfo.UserName;
                                objWPS003.EditedOn = DateTime.Now;

                                db.SaveChanges();
                                objResponseMsg.Key = true;
                                objResponseMsg.Value = clsImplementationMessage.WPSKeyMaster.Save;
                            }
                            #endregion
                        }
                        else
                        {
                            #region Add New WPS Key
                            objWPS003.Location = location;
                            objWPS003.Key1 = Convert.ToInt32(fc["Key1"]);
                            objWPS003.Key2 = Convert.ToInt32(fc["Key2"]);
                            objWPS003.Parentmetal1 = fc["Parentmetal1"];
                            objWPS003.Parentmetal2 = fc["Parentmetal2"];
                            objWPS003.WeldingProcess1 = fc["WeldingProcess1"];
                            objWPS003.WeldingProcess2 = fc["WeldingProcess2"];
                            objWPS003.WeldingProcess3 = fc["WeldingProcess3"];
                            objWPS003.WeldingProcess4 = fc["WeldingProcess4"];
                            objWPS003.JointType = fc["JointType"];
                            objWPS003.PWHT = fc["PWHT"];
                            objWPS003.Remarks = fc["Remarks"];
                            objWPS003.CreatedBy = objClsLoginInfo.UserName;
                            objWPS003.CreatedOn = DateTime.Now;

                            db.WPS003.Add(objWPS003);
                            db.SaveChanges();

                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.WPSKeyMaster.Save;
                            #endregion
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = fc["Key1"].ToString() + "," + fc["Key2"].ToString() + " Key Combination Already Exist";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteWPSKeyMaster(int id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            WPS003 objWPS003 = new WPS003();
            try
            {
                if (id > 0)
                {
                    var objInUsed = db.SP_WPS_GetWPSKey(0, 0, "", "wps3.Id=" + id).FirstOrDefault();
                    if(objInUsed!=null && objInUsed.InUsed.HasValue && objInUsed.InUsed.Value)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "You Can Not Modify This Key Because Already Used In WPS Number Index Master.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    objWPS003 = db.WPS003.Where(i => i.Id == id).FirstOrDefault();
                    if (objWPS003 != null)
                    {
                        if (isWPSKeyExistInWPSNoIndex(objWPS003.Location, objWPS003.Key1, objWPS003.Key2))
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = string.Format(clsImplementationMessage.WPSKeyMaster.RestrictDelete, objWPS003.Key1, objWPS003.Key2);
                        }
                        else
                        {
                            db.WPS003.Remove(objWPS003);
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = string.Format(clsImplementationMessage.WPSKeyMaster.Delete, objWPS003.Key1, objWPS003.Key2);
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Record Doesn't Exist";
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Record Doesn't Exist";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public bool isWPSKeyExist(string location, int key1, int key2, int id = 0)
        {
            bool isWPSKeyExist = false;
            var wps3 = db.WPS003.Where(i => i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && i.Key1 == key1 && i.Key2 == key2 && i.Id != id).ToList();
            if (wps3.Any())
                isWPSKeyExist = true;

            return isWPSKeyExist;
        }

        public bool isWPSKeyExistInWPSNoIndex(string location, int? key1, int? key2)
        {
            bool isWPSKeyExistInWPSNoIndex = false;
            if (db.WPS004.Where(i => i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && i.Key1 == key1 && i.Key2 == key2).Any())
                isWPSKeyExistInWPSNoIndex = true;
            return isWPSKeyExistInWPSNoIndex;
        }

        public List<GLB002> GetSubCatagory(string Key, string strLoc)
        {
            List<GLB002> lstGLB002 = (from glb002 in db.GLB002
                                      join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                                      where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true && strLoc.Equals(glb002.Location)
                                      select glb002).ToList();
            //var category = lstGLB002.Select(i => new { i.Code, i.Description }).Distinct().ToList();
            return lstGLB002;
        }

        #region Export Grid Data
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;

                List<SP_WPS_GetWPSKey_Result> lst = db.SP_WPS_GetWPSKey(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Data Found";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                var newlst = (from li in lst
                              select new
                              {
                                  Key1 = li.Key1,
                                  Key2 = li.Key2,
                                  JointType = li.JointType,                                  
                                  CreatedBy = li.CreatedBy,
                                  CreatedOn = li.CreatedOn,
                                  Location = li.Location
                              }).ToList();

                strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

    }
}