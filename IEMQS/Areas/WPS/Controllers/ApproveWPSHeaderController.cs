﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.WPS.Models;
using IEMQS.Models;
using IEMQSImplementation;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.WPS.Controllers
{
    public class ApproveWPSHeaderController : clsBase
    {
        // GET: WPS/ApproveWPSHeader
        [SessionExpireFilter]
        public ActionResult Index()
        {
            ViewBag.Tab = Request.QueryString["tab"] != null ? Convert.ToString(Request.QueryString["tab"]) : "";
            return View();
        }

        public ActionResult GetApproveWPSHeaderGridDataPartial(string status)
        {
            ViewBag.status = status;
            return PartialView("_GetApproveWPSHeaderGridDataPartial");
        }

        public ActionResult LoadWPSHeaderDataTable(JQueryDataTableParamModel param, string status)
        {
            try
            {
                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                //whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms15.BU", "qms15.Location");
                var lstLoc = string.Join(",", db.ATH001.Where(x => x.Employee == objClsLoginInfo.UserName).Select(x => x.Location).Distinct().ToList());
                whereCondition += Manager.MakeStringInCondition("wps10.Location", lstLoc);

                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and status in('" + clsImplementationEnum.WPSHeaderStatus.SentForApproval.GetStringValue() + "')";
                }
                else
                {
                    whereCondition += " and status in('" + clsImplementationEnum.WPSHeaderStatus.SentForApproval.GetStringValue() + "','" + clsImplementationEnum.WPSHeaderStatus.Approved.GetStringValue() + "')";
                }
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += " and ( lcom2.t_desc like '%" + param.sSearch + "%' or Location like '%" + param.sSearch + "%' or wps10.WPSNumber like '%" + param.sSearch + "%' or WPSRevNo like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%' or wps10.CreatedBy like '%" + param.sSearch + "%')";
                }

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstHeader = db.SP_WPS_GetWPSHeader(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstHeader
                          select new[] {
                        Convert.ToString(a.HeaderId),
                        Convert.ToString(a.HeaderId),
                        a.WPSNumber,
                        a.Jointtype,
                        "<span>R</span>"+Convert.ToString(a.WPSRevNo),
                        a.Status,
                        Convert.ToString(a.CreatedBy),
                        a.CreatedOn!=null?a.CreatedOn.Value.ToString("dd/MM/yyyy"):string.Empty,
                        a.Location,
                        status,
                        a.WeldingProcess1,
                        a.WeldingProcess2,
                        a.WeldingProcess3,
                        a.WeldingProcess4
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public JsonResult GetApproveWPSNo(string term)
        {
            db.Configuration.ProxyCreationEnabled = false;
            //   ViewBag.Status = Status;
            var res = db.WPS010.Where(c => c.WPSNumber.Contains(term));

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult FetchApproveWPSNo(string AppWPSNo)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var res = db.WPS010.Where(i => i.WPSNumber.Equals(AppWPSNo, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public ActionResult ViewWPSHeader(int headerId, string Status)
        {
            ViewBag.Status = Status;
            WPS010 objWPS010 = new WPS010();
            if (headerId > 0)
                objWPS010 = db.WPS010.Where(i => i.HeaderId == headerId).FirstOrDefault();
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("WPS010,WPS011");

            ViewBag.Tab = Request.QueryString["from"] != null ? Convert.ToString(Request.QueryString["from"]) : "";
            return View(objWPS010);
        }
        [HttpPost]
        public JsonResult NextWPSRequest(int? headerId, string status)
        {
            if (headerId != 0)
            {
                //ViewBag.roleId = roleId;
                ViewBag.status = status;
                string whereCondition = "1=1";
                var lstLoc = string.Join(",", db.ATH001.Where(x => x.Employee == objClsLoginInfo.UserName).Select(x => x.Location).Distinct().ToList());
                whereCondition += Manager.MakeStringInCondition("wps10.Location", lstLoc);

                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and status in('" + clsImplementationEnum.WPSHeaderStatus.SentForApproval.GetStringValue() + "')";
                }
                else
                {
                    whereCondition += " and status in('" + clsImplementationEnum.WPSHeaderStatus.SentForApproval.GetStringValue() + "','" + clsImplementationEnum.WPSHeaderStatus.Approved.GetStringValue() + "')";
                }
                var lstHeader = db.SP_WPS_GetWPSHeader(0, 0, "", whereCondition).ToList();
                var q = lstHeader.Where(i => i.HeaderId > headerId).OrderBy(i => i.HeaderId).FirstOrDefault();
                return Json(q, JsonRequestBehavior.AllowGet);
            }
            return Json(null);

        }
        [HttpPost]
        public JsonResult PrevWPSRequest(int? headerId, string status)
        {
            db.Configuration.ProxyCreationEnabled = false;
            if (headerId != 0)
            {
                //viewBag.roleId = roleId;
                ViewBag.status = status;
                string whereCondition = "1=1";
                var lstLoc = string.Join(",", db.ATH001.Where(x => x.Employee == objClsLoginInfo.UserName).Select(x => x.Location).Distinct().ToList());
                whereCondition += Manager.MakeStringInCondition("wps10.Location", lstLoc);

                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and status in('" + clsImplementationEnum.WPSHeaderStatus.SentForApproval.GetStringValue() + "')";
                }
                else
                {
                    whereCondition += " and status in('" + clsImplementationEnum.WPSHeaderStatus.SentForApproval.GetStringValue() + "','" + clsImplementationEnum.WPSHeaderStatus.Approved.GetStringValue() + "')";
                }
                var lstHeader = db.SP_WPS_GetWPSHeader(0, 0, "", whereCondition).ToList();
                var q = lstHeader.Where(i => i.HeaderId < headerId).OrderByDescending(i => i.HeaderId).FirstOrDefault();
                return Json(q, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);

        }

        public ActionResult LoadWPSLinesDataTable(JQueryDataTableParamModel param, int headerId)
        {

            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                //whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "wps10.BU", "wps10.Location");

                var lstLoc = string.Join(",", db.ATH001.Where(x => x.Employee == objClsLoginInfo.UserName).Select(x => x.Location).Distinct().ToList());
                whereCondition += " AND HeaderId=" + headerId;
                //whereCondition += Manager.MakeStringInCondition("wps11.Location", lstLoc);

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " and ( Location like '%" + param.sSearch + "%' or wps11.WPSNumber like '%" + param.sSearch + "%' or WPSRevNo like '%" + param.sSearch + "%' or wps11.CreatedBy like '%" + param.sSearch
                                       + "%' or  WeldingProcess like '%" + param.sSearch + "%' or WeldLayer like '%" + param.sSearch + "%' or AWSClass like '%" + param.sSearch + "%' or FillerMetalSize like '%" + param.sSearch
                                       + "%' or ANumber like '%" + param.sSearch + "%' or SupplementalFiller like '%" + param.sSearch + "%' or FluxType like '%" + param.sSearch + "%' or FillerMetalTradeName like '%" + param.sSearch
                                       + "%' or ReCrushedSlag like '%" + param.sSearch + "%' or SFANo like '%" + param.sSearch + "%' or FillerMetalProductForm like '%" + param.sSearch
                                       + "%' or ElectrodeFluxClass like '%" + param.sSearch + "%' or FluxTradeName like '%" + param.sSearch + "%' or ConsumableInsert like '%" + param.sSearch
                                       + "%' or CurrentType like '%" + param.sSearch + "%' or CurrentPolarity like '%" + param.sSearch + "%' or ConsumableInsert like '%" + param.sSearch + "%')";
                }

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstLines = db.SP_WPS_GETHEADERLINES(StartIndex, EndIndex, "", whereCondition).ToList();

                int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstLines
                          select new[] {
                        Convert.ToString(a.ROW_NO),
                        Convert.ToString(a.HeaderId),
                        Convert.ToString(a.LineId),
                        a.WeldingProcess,
                        a.WeldLayer,
                        a.AWSClass,
                        a.FillerMetalSize,
                        a.ANumber,
                        a.SupplementalFiller,
                        a.FluxType,
                        a.FillerMetalTradeName,
                        a.ReCrushedSlag,
                        a.SFANo,
                        a.FNumber,
                        a.FillerMetalProductForm,
                        a.ElectrodeFluxClass,
                        a.FluxTradeName,
                        a.ConsumableInsert,
                        a.Other,
                        a.CurrentAMP,
                        a.TravelSpeed,
                        a.CurrentType,
                        a.CurrentPolarity,
                        a.Voltage,
                        a.BeadLength,
                        a.HeatInput,
                        a.WireFeedSpeed

                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult LoadPartial(int headerId, string partialName, string jointType)
        {
            WPS010 objWPS010 = new WPS010();
            string filepath = string.Empty;
            var partialname = "_WPSHeader" + partialName;

            var location = (from a in db.COM003
                            join b in db.COM002 on a.t_loca equals b.t_dimx
                            where b.t_dtyp == 1 && a.t_psno.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase) && a.t_actv == 1
                            select b.t_dimx + " - " + b.t_desc).FirstOrDefault();
            ViewBag.Location = location;

            List<string> lstBacking = clsBackingEnum.GetBacking();


            if (headerId > 0)
            {
                objWPS010 = db.WPS010.Where(i => i.HeaderId == headerId).FirstOrDefault();
            }

            filepath += "~/Areas/WPS/Views/ApproveWPSHeader/";
            NDEModels objNDEModels = new NDEModels();
            int key1 = 0, key2 = 0;
            if (!string.IsNullOrEmpty(objWPS010.WPSNumber))
            {
                key1 = Convert.ToInt32(objWPS010.WPSNumber.Split('-')[0]);
                key2 = Convert.ToInt32(objWPS010.WPSNumber.Split('-')[1]);
                var wpsKey = db.WPS003.Where(i => i.Key1 == key1 && i.Key2 == key2 && i.Location == objWPS010.Location).FirstOrDefault();

                objWPS010.ParentMaterial1 = wpsKey.Parentmetal1;
                objWPS010.ParentMaterial2 = wpsKey.Parentmetal2;
            }

            if (!string.IsNullOrEmpty(objWPS010.WeldingProcess1))
            {
                //ViewBag.WeldingProcess1 = objNDEModels.GetCategory(objWPS010.WeldingProcess1).CategoryDescription;
                ViewBag.WeldingProcess1 = objNDEModels.GetCategory(objWPS010.WeldingProcess1).Code;
            }
            if (!string.IsNullOrEmpty(objWPS010.WeldingProcess2))
            {
                //ViewBag.WeldingProcess2 = objNDEModels.GetCategory(objWPS010.WeldingProcess2).CategoryDescription;
                ViewBag.WeldingProcess2 = objNDEModels.GetCategory(objWPS010.WeldingProcess2).Code;
            }
            if (!string.IsNullOrEmpty(objWPS010.WeldingProcess3))
            {
                //ViewBag.WeldingProcess3 = objNDEModels.GetCategory(objWPS010.WeldingProcess3).CategoryDescription;
                ViewBag.WeldingProcess3 = objNDEModels.GetCategory(objWPS010.WeldingProcess3).Code;
            }
            if (!string.IsNullOrEmpty(objWPS010.WeldingProcess4))
            {
                //ViewBag.WeldingProcess4 = objNDEModels.GetCategory(objWPS010.WeldingProcess4).CategoryDescription;
                ViewBag.WeldingProcess4 = objNDEModels.GetCategory(objWPS010.WeldingProcess4).Code;
            }

            #region GrooveF1
            if (!string.IsNullOrEmpty(objWPS010.PositionofGrooveTTSJointWeldingProcess1)) { ViewBag.PositionofGrooveTTSJointWeldingProcess1 = GetCategoryDescription(objWPS010.PositionofGrooveTTSJointWeldingProcess1).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.PositionofGrooveTTSJointWeldingProcess2)) { ViewBag.PositionofGrooveTTSJointWeldingProcess2 = GetCategoryDescription(objWPS010.PositionofGrooveTTSJointWeldingProcess2).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.PositionofGrooveTTSJointWeldingProcess3)) { ViewBag.PositionofGrooveTTSJointWeldingProcess3 = GetCategoryDescription(objWPS010.PositionofGrooveTTSJointWeldingProcess3).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.PositionofGrooveTTSJointWeldingProcess4)) { ViewBag.PositionofGrooveTTSJointWeldingProcess4 = GetCategoryDescription(objWPS010.PositionofGrooveTTSJointWeldingProcess4).CategoryDescription; }

            if (!string.IsNullOrEmpty(objWPS010.WeldingProgressionWeldingProcess1)) { ViewBag.WeldingProgressionWeldingProcess1 = GetCategoryDescription(objWPS010.WeldingProgressionWeldingProcess1).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.WeldingProgressionWeldingProcess2)) { ViewBag.WeldingProgressionWeldingProcess2 = GetCategoryDescription(objWPS010.WeldingProgressionWeldingProcess2).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.WeldingProgressionWeldingProcess3)) { ViewBag.WeldingProgressionWeldingProcess3 = GetCategoryDescription(objWPS010.WeldingProgressionWeldingProcess3).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.WeldingProgressionWeldingProcess4)) { ViewBag.WeldingProgressionWeldingProcess4 = GetCategoryDescription(objWPS010.WeldingProgressionWeldingProcess4).CategoryDescription; }

            if (!string.IsNullOrEmpty(objWPS010.WeldingProgressionSubsequentWeldingProcess1)) { ViewBag.WeldingProgressionSubsequentWeldingProcess1 = GetCategoryDescription(objWPS010.WeldingProgressionSubsequentWeldingProcess1).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.WeldingProgressionSubsequentWeldingProcess2)) { ViewBag.WeldingProgressionSubsequentWeldingProcess2 = GetCategoryDescription(objWPS010.WeldingProgressionSubsequentWeldingProcess2).CategoryDescription; }

            if (!string.IsNullOrEmpty(objWPS010.PositionoffilletWeldingProcess1)) { ViewBag.PositionoffilletWeldingProcess1 = GetCategoryDescription(objWPS010.PositionoffilletWeldingProcess1).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.PositionoffilletWeldingProcess2)) { ViewBag.PositionoffilletWeldingProcess2 = GetCategoryDescription(objWPS010.PositionoffilletWeldingProcess2).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.PositionoffilletWeldingProcess3)) { ViewBag.PositionoffilletWeldingProcess3 = GetCategoryDescription(objWPS010.PositionoffilletWeldingProcess3).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.PositionoffilletWeldingProcess4)) { ViewBag.PositionoffilletWeldingProcess4 = GetCategoryDescription(objWPS010.PositionoffilletWeldingProcess4).CategoryDescription; }
            #endregion
            #region GrooveF2
            if (!string.IsNullOrEmpty(objWPS010.TypeofPWHT)) { ViewBag.TypeofPWHT = GetCategoryDescription(objWPS010.TypeofPWHT).CategoryDescription; }
            #endregion
            #region GrooveF3
            if (!string.IsNullOrEmpty(objWPS010.ShieldingGasGasType1)) { ViewBag.ShieldingGasGasType1 = GetCategoryDescription(objWPS010.ShieldingGasGasType1).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.ShieldingGasGasType2)) { ViewBag.ShieldingGasGasType2 = GetCategoryDescription(objWPS010.ShieldingGasGasType2).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.BackingGasGasType1)) { ViewBag.BackingGasGasType1 = GetCategoryDescription(objWPS010.BackingGasGasType1).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.BackingGasGasType2)) { ViewBag.BackingGasGasType2 = GetCategoryDescription(objWPS010.BackingGasGasType2).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.TrailingGasGasType1)) { ViewBag.TrailingGasGasType1 = GetCategoryDescription(objWPS010.TrailingGasGasType1).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.TrailingGasGasType2)) { ViewBag.TrailingGasGasType2 = GetCategoryDescription(objWPS010.TrailingGasGasType2).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.Tungsten)) { ViewBag.TungstenSelected = GetCategoryDescription(objWPS010.Tungsten).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.Modeofmetaltransfer)) { ViewBag.ModeofmetaltransferSelected = GetCategoryDescription(objWPS010.Modeofmetaltransfer).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.Pulsing)) { ViewBag.PulsingSelected = GetCategoryDescription(objWPS010.Pulsing).CategoryDescription; }
            #endregion

            #region GrooveF4
            if (!string.IsNullOrEmpty(objWPS010.MultiSingleElectrode1)) { ViewBag.MultiSingleElectrode1 = GetCategoryDescription(objWPS010.MultiSingleElectrode1).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.MultiSingleElectrode2)) { ViewBag.MultiSingleElectrode2 = GetCategoryDescription(objWPS010.MultiSingleElectrode2).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.MultiSingleElectrode3)) { ViewBag.MultiSingleElectrode3 = GetCategoryDescription(objWPS010.MultiSingleElectrode3).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.MultiSingleElectrode4)) { ViewBag.MultiSingleElectrode4 = GetCategoryDescription(objWPS010.MultiSingleElectrode4).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.MultipleSinglepassperside1)) { ViewBag.MultipleSinglepassperside1 = GetCategoryDescription(objWPS010.MultipleSinglepassperside1).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.MultipleSinglepassperside2)) { ViewBag.MultipleSinglepassperside2 = GetCategoryDescription(objWPS010.MultipleSinglepassperside2).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.MultipleSinglepassperside3)) { ViewBag.MultipleSinglepassperside3 = GetCategoryDescription(objWPS010.MultipleSinglepassperside3).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.MultipleSinglepassperside4)) { ViewBag.MultipleSinglepassperside4 = GetCategoryDescription(objWPS010.MultipleSinglepassperside4).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.SingleMultipleLayer1)) { ViewBag.SingleMultipleLayer1 = GetCategoryDescription(objWPS010.SingleMultipleLayer1).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.SingleMultipleLayer2)) { ViewBag.SingleMultipleLayer2 = GetCategoryDescription(objWPS010.SingleMultipleLayer2).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.SingleMultipleLayer3)) { ViewBag.SingleMultipleLayer3 = GetCategoryDescription(objWPS010.SingleMultipleLayer3).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.SingleMultipleLayer4)) { ViewBag.SingleMultipleLayer4 = GetCategoryDescription(objWPS010.SingleMultipleLayer4).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.StringWeaveBead1)) { ViewBag.StringWeaveBead1 = GetCategoryDescription(objWPS010.StringWeaveBead1).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.StringWeaveBead2)) { ViewBag.StringWeaveBead2 = GetCategoryDescription(objWPS010.StringWeaveBead2).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.StringWeaveBead3)) { ViewBag.StringWeaveBead3 = GetCategoryDescription(objWPS010.StringWeaveBead3).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.StringWeaveBead4)) { ViewBag.StringWeaveBead4 = GetCategoryDescription(objWPS010.StringWeaveBead4).CategoryDescription; }
            #endregion

            #region GrooveF5
            if (!string.IsNullOrEmpty(objWPS010.Oscillation1)) { ViewBag.Oscillation1 = GetCategoryDescription(objWPS010.Oscillation1).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.Oscillation2)) { ViewBag.Oscillation2 = GetCategoryDescription(objWPS010.Oscillation2).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.Oscillation3)) { ViewBag.Oscillation3 = GetCategoryDescription(objWPS010.Oscillation3).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.Oscillation4)) { ViewBag.Oscillation4 = GetCategoryDescription(objWPS010.Oscillation4).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.Peening)) { ViewBag.PeeningSelected = GetCategoryDescription(objWPS010.Peening).CategoryDescription; }
            #endregion

            if (string.Equals(jointType, "Groove", StringComparison.OrdinalIgnoreCase))
            {
                filepath += "GrooveFillet/_WPSHeader" + partialName + "Partial.cshtml";
            }
            else if (string.Equals(jointType, "Buttering", StringComparison.OrdinalIgnoreCase))
            {
                filepath += "Buttering/_WPSHeader" + partialName + "Partial.cshtml";
            }
            else if (string.Equals(jointType, "TTS", StringComparison.OrdinalIgnoreCase))
            {
                filepath += "TTS/_WPSHeader" + partialName + "Partial.cshtml";
            }
            else if (string.Equals(jointType, "OverLay", StringComparison.OrdinalIgnoreCase))
            {
                if (!string.IsNullOrEmpty(objWPS010.PositionOverlayBarrierWeldingProcess1)) { ViewBag.PositionOverlayBarrierWeldingProcess1 = GetCategoryDescription(objWPS010.PositionOverlayBarrierWeldingProcess1).CategoryDescription; }
                if (!string.IsNullOrEmpty(objWPS010.PositionOverlayBarrierWeldingProcess2)) { ViewBag.PositionOverlayBarrierWeldingProcess2 = GetCategoryDescription(objWPS010.PositionOverlayBarrierWeldingProcess2).CategoryDescription; }
                if (!string.IsNullOrEmpty(objWPS010.PositionOverlaySubsequentWeldingProcess1)) { ViewBag.PositionOverlaySubsequentWeldingProcess1 = GetCategoryDescription(objWPS010.PositionOverlaySubsequentWeldingProcess1).CategoryDescription; }
                if (!string.IsNullOrEmpty(objWPS010.PositionOverlaySubsequentWeldingProcess2)) { ViewBag.PositionOverlaySubsequentWeldingProcess2 = GetCategoryDescription(objWPS010.PositionOverlaySubsequentWeldingProcess2).CategoryDescription; }

                filepath += "OverlayHardfacing/_WPSHeader" + partialName + "Partial.cshtml";
            }
            else
            {
                if (!string.IsNullOrEmpty(objWPS010.WeldingType1))
                {
                    ViewBag.WeldingType1 = GetCategoryDescription(objWPS010.WeldingType1).CategoryDescription;
                }
                if (!string.IsNullOrEmpty(objWPS010.WeldingType2))
                {
                    ViewBag.WeldingType2 = GetCategoryDescription(objWPS010.WeldingType2).CategoryDescription;
                }
                if (!string.IsNullOrEmpty(objWPS010.WeldingType3))
                {
                    ViewBag.WeldingType3 = GetCategoryDescription(objWPS010.WeldingType3).CategoryDescription;
                }
                if (!string.IsNullOrEmpty(objWPS010.WeldingType4))
                {
                    ViewBag.WeldingType4 = GetCategoryDescription(objWPS010.WeldingType4).CategoryDescription;
                }
                filepath += "_ApproveWPSHeader.cshtml";
            }

            return PartialView(filepath, objWPS010);
        }


        [HttpPost]
        public ActionResult ApproveWPSHeader(string strHeaderIds)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            string WPSNo = string.Empty;
            int NoOfApproveWPS = 0;
            try
            {
                if (!string.IsNullOrEmpty(strHeaderIds))
                {
                    int[] headerIds = Array.ConvertAll(strHeaderIds.Split(','), i => Convert.ToInt32(i));
                    foreach (var headerId in headerIds)
                    {
                        var objwps010 = db.WPS010.Where(i => i.HeaderId == headerId).FirstOrDefault();
                        if (objwps010.Status == clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue())
                        {
                            if (WPSNo.Length == 0) { WPSNo = objwps010.WPSNumber.ToString(); }
                            else { WPSNo += "," + objwps010.WPSNumber.ToString(); }
                            db.SP_WPS_APPROVEHEADER(headerId, objClsLoginInfo.UserName);

                            #region Get BU and Project From WPSNo                        
                            var QualityProject = db.WPS004.Where(i => i.WPSNumber == objwps010.WPSNumber).FirstOrDefault().QualityProject;
                            var project = new NDEModels().GetQMSProject(QualityProject).Project;
                            var BU = db.COM001.Where(i => i.t_cprj.Equals(project, StringComparison.OrdinalIgnoreCase)).FirstOrDefault().t_entu;
                            #endregion
                            #region Send Notification
                            (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.WE3.GetStringValue(), project, BU, objwps010.Location, "WPS: " + objwps010.WPSNumber + " has been Approved", clsImplementationEnum.NotificationType.Information.GetStringValue());
                            NoOfApproveWPS++;
                        }
                        #endregion

                    }

                    if (NoOfApproveWPS > 1)
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = string.Format(clsImplementationMessage.WPSHeader.ApprovedMultiple.ToString(), NoOfApproveWPS);
                    }
                    else if (NoOfApproveWPS == 1)
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = string.Format(clsImplementationMessage.WPSHeader.Approved.ToString(), WPSNo);
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.IsInformation = true;
                        if (headerIds.Count() > 1)
                        {
                            objResponseMsg.Value = string.Format("No WPS Found In Send For Approval Status.", WPSNo);
                        }
                        else
                        {
                            objResponseMsg.Value = string.Format("WPS Is Not In Send For Approval Status. So You Can Not Approve WPS.", WPSNo);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GeneratePdf(string strHeaderIds = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            List<byte[]> lstFileBytes = new List<byte[]>();
            string WPSNo = string.Empty;
            WPS010 objWPS010 = new WPS010();
            try
            {
                if (!string.IsNullOrEmpty(strHeaderIds))
                {
                    string reportUrl = "";
                    int[] headerIds = Array.ConvertAll(strHeaderIds.Split(','), i => Convert.ToInt32(i));
                   
                    foreach (var headerId in headerIds)
                    {
                        objWPS010 = db.WPS010.Where(i => i.HeaderId == headerId && i.Status == "Approved").FirstOrDefault();
                        if (objWPS010 != null)
                        {
                            string strReturnType = objWPS010.Jointtype.ToLower().ToString();
                            if (string.Equals(strReturnType, "Buttering", StringComparison.OrdinalIgnoreCase))
                            {
                                reportUrl = "/WPS/WPS Buttering";
                            }
                            else if (string.Equals(strReturnType, "groove/fillet", StringComparison.OrdinalIgnoreCase))
                            {
                                reportUrl = "/WPS/WPS Groove Fillet";
                            }
                            else if (string.Equals(strReturnType, "Overlay", StringComparison.OrdinalIgnoreCase))
                            {
                                reportUrl = "/WPS/WPS Overlay";
                            }
                            else if (string.Equals(strReturnType, "T#TS", StringComparison.OrdinalIgnoreCase))
                            {
                                reportUrl = "/WPS/WPS Tube Tubesheet";
                            }
                            else
                            {
                                reportUrl = string.Empty;
                            }

                            List<SSRSParam> reportWpsParams = new List<SSRSParam>();
                            if (!string.IsNullOrWhiteSpace(reportUrl))
                            {
                                reportWpsParams.Add(new SSRSParam { Param = "HeaderId", Value = headerId.ToString() });
                                reportWpsParams.Add(new SSRSParam("Host", System.Configuration.ConfigurationManager.AppSettings["Applicaiton_URL"]));
                            }
                            
                            byte[] fileBytes = (new Utility.Controllers.GeneralController()).GetPDFBytesfromSSRSreport(reportUrl, reportWpsParams);

                            if (fileBytes != null)
                            {
                                var reader = new iTextSharp.text.pdf.PdfReader(fileBytes);
                                lstFileBytes.Add(fileBytes);
                            }
                        }
                    }
                }
                if (lstFileBytes.Count > 0)
                {
                    string fileName = objWPS010.WPSNumber + " R" + objWPS010.WPSRevNo;
                    //fileName = string.Join("�", fileName.Split(Path.GetInvalidFileNameChars()));
                    fileName = string.Join("", fileName.Split(Path.GetInvalidFileNameChars()));
                    string pdfName = String.Format("{0}.{1}", fileName, "pdf");
                    string OutFile = Server.MapPath("~/Resources/Download/") + pdfName;
                    string outFilePath = MergedPDFFileWithMultipleFileBytes(lstFileBytes, OutFile);

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = outFilePath;
                    objResponseMsg.CheckStageType = pdfName;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No file found.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReturnWPSHeader(int strHeaderId, string remark)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            string WPSNo = string.Empty;
            try
            {
                var objwps010 = db.WPS010.Where(i => i.HeaderId == strHeaderId).FirstOrDefault();
                if (objwps010 != null)
                {
                    if (objwps010.Status != clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.IsInformation = true;
                        objResponseMsg.Value = string.Format("WPS No Is Not In Send For Approval Status. So You Can Not Return This WPS.", WPSNo);
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    WPSNo = objwps010.WPSNumber;
                    objwps010.Status = clsImplementationEnum.QualityIdHeaderStatus.Returned.GetStringValue();
                    objwps010.ReturnedBy = objClsLoginInfo.UserName;
                    objwps010.ReturnedOn = DateTime.Now;
                    objwps010.ReturnRemarks = remark;
                    db.SaveChanges();
                    #region Get BU and Project From WPSNo                        
                    var QualityProject = db.WPS004.Where(i => i.WPSNumber == objwps010.WPSNumber).FirstOrDefault().QualityProject;
                    var project = new NDEModels().GetQMSProject(QualityProject).Project;
                    var BU = db.COM001.Where(i => i.t_cprj.Equals(project, StringComparison.OrdinalIgnoreCase)).FirstOrDefault().t_entu;
                    #endregion

                    #region Send Notification
                    (new clsManager()).SendNotification("", project, BU, objwps010.Location, "WPS: " + objwps010.WPSNumber + " has been Returned", clsImplementationEnum.NotificationType.Information.GetStringValue(), "", objwps010.SubmittedBy);
                    #endregion
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = string.Format("WPS Not Exist!", WPSNo);
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = string.Format(clsImplementationMessage.WPSHeader.Returned.ToString(), WPSNo);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public CategoryData GetCategoryDescription(string categoryCode)
        {
            CategoryData objGLB002 = new CategoryData();

            if (IsCategoryExist(categoryCode))
            {

                if (!string.IsNullOrEmpty(categoryCode))
                {
                    objGLB002 = db.GLB002.Where(i => i.Code.Equals(categoryCode, StringComparison.OrdinalIgnoreCase)).Select(i => new CategoryData { Code = i.Code, CategoryDescription = i.Description }).FirstOrDefault();
                }
            }
            else
            {
                objGLB002.CategoryDescription = categoryCode;
            }

            return objGLB002;
        }
        public bool IsCategoryExist(string categoryCode)
        {
            bool categoryExist = false;

            if (db.GLB002.Where(i => i.Code.Equals(categoryCode, StringComparison.OrdinalIgnoreCase)).Any())
                categoryExist = true;

            return categoryExist;
        }
        private string MergedPDFFileWithMultipleFileBytes(List<byte[]> lstFileBytes, string fileName, string filetype = "pdf")
        {

            try
            {
                using (var doc = new iTextSharp.text.Document())
                using (FileStream stream = new FileStream(fileName, FileMode.Create))
                using (var pdf = new iTextSharp.text.pdf.PdfCopy(doc, stream))
                {
                    doc.Open();
                    foreach (var fileBytes in lstFileBytes)
                    {
                        try
                        {
                            var reader = new iTextSharp.text.pdf.PdfReader(fileBytes);
                            iTextSharp.text.pdf.PdfImportedPage page = null;

                            for (int i = 0; i < reader.NumberOfPages; i++)
                            {
                                page = pdf.GetImportedPage(reader, i + 1);
                                pdf.AddPage(page);
                            }
                            pdf.FreeReader(reader);
                            reader.Close();
                        }
                        catch (Exception ex)
                        {
                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        }
                    }

                    pdf.Close();
                    stream.Close();
                }
            }
            catch (Exception ex)
            {
                System.IO.File.Delete(fileName);
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return fileName;
        }
        public FileResult Download(string filePath, string FileName)
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            string fileName = FileName;
            System.IO.File.Delete(filePath);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Pdf, fileName);
        }

    }
}