﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQS.Models;

namespace IEMQS.Areas.WPS.Controllers
{
    public class FillerMetalMasterController : clsBase
    {
        // GET: WPS/FillerMetalMaster

        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LoadFillerMetalListDataTable(JQueryDataTableParamModel param)
        {
            try
            {
                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                //whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "", "wps1.Location");
                var lstLoc = string.Join(",", db.ATH001.Where(x => x.Employee == objClsLoginInfo.UserName).Select(x => x.Location).Distinct().ToList());

                whereCondition += Manager.MakeStringInCondition("wps2.Location", lstLoc);

                //whereCondition += " and wps1.Location IN('HZW', 'HZW')";

                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += " and ((wps2.Location+'-'+lcom2.t_desc) like '%" + param.sSearch + "%' or wps2.FNumber like '%" + param.sSearch + "%' or wps2.AWSClass like '%" + param.sSearch + "%' or wps2.ConsumableSize like '%" + param.sSearch + "%' or (ecom3.t_psno+'-'+ecom3.t_name) like '%" + param.sSearch + "%')";
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(param.SearchFilter))
                        whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstHeader = db.SP_WPS_GetFillerMetal(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstHeader
                          select new[] {
                        Convert.ToString(a.Id),
                        a.AWSClass,
                        a.FNumber,
                        a.ConsumableSize,
                        a.CreatedBy,
                        Convert.ToDateTime(a.CreatedOn).ToString("dd/MM/yyyy"),
                        a.Location
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult FillerMetalMasterPartial(int? id)
        {
            WPS002 objWPS002 = new WPS002();
            string location = string.Empty;
            if (id > 0)
            {
                objWPS002 = db.WPS002.Where(i => i.Id == id).FirstOrDefault();
                location = (from a in db.COM002
                            where a.t_dtyp == 1 && a.t_dimx.Equals(objWPS002.Location, StringComparison.OrdinalIgnoreCase)
                            select a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            }
            else
            {
                location = (from a in db.COM003
                            join b in db.COM002 on a.t_loca equals b.t_dimx
                            where b.t_dtyp == 1 && a.t_psno.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase) && a.t_actv == 1
                            select b.t_dimx + " - " + b.t_desc).FirstOrDefault();
            }

            ViewBag.Location = location;
            string loc = location.Split('-')[0];
            //ViewBag.AWSClassList = db.WPS001.Where(i => i.Location.Equals(loc, StringComparison.Ordinal)).Select(i => new CategoryData {CategoryDescription= i.AWSClass.ToString(), Value = i.AWSClass.ToString() }).Distinct().ToList();

            return PartialView("_FillerMetalMasterPartial", objWPS002);
        }

        [HttpPost]
        public ActionResult Save(FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            WPS002 objWPS002 = new WPS002();
            try
            {
                if (fc != null)
                {
                    int id = Convert.ToInt32(fc["Id"]);
                    string location = fc["Location"].Split('-')[0];
                    if (!isFillerMetalExist(location, fc["AWSClass"], fc["FNumber"], fc["ConsumableSize"], id))
                    {
                        if (id > 0)
                        {
                            objWPS002 = db.WPS002.Where(i => i.Id == id).FirstOrDefault();

                            objWPS002.SFANo = fc["SFANo"];
                            objWPS002.ANumber = fc["ANumber"];
                            objWPS002.CurrentAMP = fc["CurrentAMP"];
                            objWPS002.Voltage = fc["Voltage"];
                            objWPS002.TravelSpeed = fc["TravelSpeed"];
                            objWPS002.BeadLength = fc["BeadLength"];
                            objWPS002.HeatInput = fc["HeatInput"];
                            objWPS002.EditedBy = objClsLoginInfo.UserName;
                            objWPS002.EditedOn = DateTime.Now;

                            db.SaveChanges();

                            string strDraft = clsImplementationEnum.WPSHeaderStatus.Draft.GetStringValue();
                            var objDraftWPS11 = (from a in db.WPS010
                                                 join b in db.WPS011 on a.HeaderId equals b.HeaderId
                                                 where a.WPSRevNo == 0 && a.Status.ToLower() == strDraft.ToLower() 
                                                    && b.AWSClass.ToLower() == objWPS002.AWSClass.ToLower()
                                                    && b.FillerMetalSize.ToLower() == objWPS002.ConsumableSize.ToLower()
                                                    && b.FNumber.ToLower() == objWPS002.FNumber.ToLower()
                                                    && b.Location.ToLower() == objWPS002.Location.ToLower()
                                                 select b
                                               ).ToList();
                            if (objDraftWPS11.Count > 0)
                            {
                                objDraftWPS11.ForEach(i => {
                                    i.AWSClass = objWPS002.AWSClass;
                                    i.ANumber = objWPS002.ANumber;
                                    i.FillerMetalSize = objWPS002.ConsumableSize;
                                    i.SFANo = objWPS002.SFANo;
                                    i.FNumber = objWPS002.FNumber;
                                    i.CurrentAMP = objWPS002.CurrentAMP;
                                    i.TravelSpeed = objWPS002.TravelSpeed;
                                    i.Voltage = objWPS002.Voltage;
                                    i.BeadLength = objWPS002.BeadLength;
                                    i.HeatInput = objWPS002.HeatInput;
                                });
                                db.SaveChanges();
                            }

                            //var objWPS11 = db.WPS011.Where(i => i.AWSClass.Equals(objWPS002.AWSClass, StringComparison.OrdinalIgnoreCase) && i.Location == objWPS002.Location).Select(i => i.HeaderId).Distinct().ToList();
                            //if (objWPS11.Any())
                            //{
                            //    var lstWPS10 = db.WPS010.Where(i => objWPS11.Contains(i.HeaderId)).ToList();
                            //    string strApproved = clsImplementationEnum.WPSHeaderStatus.Approved.GetStringValue();
                            //    string strDraft = clsImplementationEnum.WPSHeaderStatus.Draft.GetStringValue();
                            //    //var lstApproved = lstWPS10.Where(i => i.Status.Equals(strApproved, StringComparison.OrdinalIgnoreCase)).ToList();
                            //    var lstDraft = lstWPS10.Where(i => i.Status.Equals(strDraft, StringComparison.OrdinalIgnoreCase)).Select(i => i.HeaderId).ToList();
                            //    if (lstDraft.Any())
                            //    {
                            //        foreach (var item in lstDraft)
                            //        {
                            //            var lstWPS011 = db.WPS011.Where(i => i.HeaderId == item).ToList();
                            //            if (lstWPS011.Any())
                            //            {
                            //                lstWPS011.ForEach(i => i.AWSClass = objWPS002.AWSClass);
                            //            }
                            //        }
                            //    }
                            //    db.SaveChanges();
                            //}

                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.FillerMetalMaster.Save;
                        }
                        else
                        {
                            objWPS002.Location = location;
                            objWPS002.AWSClass = fc["AWSClass"];
                            objWPS002.FNumber = fc["FNumber"];
                            objWPS002.ConsumableSize = fc["ConsumableSize"];
                            objWPS002.SFANo = fc["SFANo"];
                            objWPS002.ANumber = fc["ANumber"];
                            objWPS002.CurrentAMP = fc["CurrentAMP"];
                            objWPS002.Voltage = fc["Voltage"];
                            objWPS002.TravelSpeed = fc["TravelSpeed"];
                            objWPS002.BeadLength = fc["BeadLength"];
                            objWPS002.HeatInput = fc["HeatInput"];
                            objWPS002.CreatedBy = objClsLoginInfo.UserName;
                            objWPS002.CreatedOn = DateTime.Now;

                            db.WPS002.Add(objWPS002);
                            db.SaveChanges();

                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.FillerMetalMaster.Save;
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Filler Metal Already Exist";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteFillerMetal(int id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            WPS002 objWPS002 = new WPS002();
            try
            {
                if (id > 0)
                {
                    objWPS002 = db.WPS002.Where(i => i.Id == id).FirstOrDefault();
                    if (isFillerMetalExistInWPS(objWPS002.AWSClass,objWPS002.Location, objWPS002.FNumber, objWPS002.ConsumableSize))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.FillerMetalMaster.RestrictDelete;
                    }
                    else
                    {
                        if (objWPS002 != null)
                        {
                            db.WPS002.Remove(objWPS002);
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.FillerMetalMaster.Delete;
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Record doesn't exist";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public bool isFillerMetalExistInWPS(string awsClass,string Location, string FNumber,string ConsumableSize)
        {
            bool isFillerMetalExistInWPS = false;
            var objWPS11 = db.WPS011.Where(i => i.AWSClass.Equals(awsClass, StringComparison.OrdinalIgnoreCase)
                                            && i.Location.Equals(Location, StringComparison.OrdinalIgnoreCase)
                                            && i.FNumber.Equals(FNumber, StringComparison.OrdinalIgnoreCase)
                                            && i.FillerMetalSize.Equals(ConsumableSize, StringComparison.OrdinalIgnoreCase)).Select(i => i.HeaderId).Distinct().ToList();
            if (objWPS11.Any())
            {
                isFillerMetalExistInWPS = true;
            }
            return isFillerMetalExistInWPS;
        }

        public bool isFillerMetalExist(string location, string awsClass, string fNo, string consumableSize, int id = 0)
        {
            bool isFillerMetalExist = false;
            var lstFillerMetal = db.WPS002.Where(i => i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && i.AWSClass.Equals(awsClass, StringComparison.OrdinalIgnoreCase) &&
                                                  i.FNumber.Equals(fNo, StringComparison.OrdinalIgnoreCase) && i.ConsumableSize.Equals(consumableSize, StringComparison.OrdinalIgnoreCase) && i.Id != id).ToList();

            if (lstFillerMetal.Any())
            {
                isFillerMetalExist = true;
            }

            return isFillerMetalExist;
        }

        #region Export Grid Data
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;

                List<SP_WPS_GetFillerMetal_Result> lst = db.SP_WPS_GetFillerMetal(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Data Found";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                var newlst = (from li in lst
                              select new
                              {
                                  AWSClass = li.AWSClass,
                                  FNumber = li.FNumber,
                                  ConsumableSize = li.ConsumableSize,                                  
                                  CreatedBy = li.CreatedBy,
                                  CreatedOn = li.CreatedOn,
                                  Location = li.Location
                              }).ToList();

                strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

    }
}