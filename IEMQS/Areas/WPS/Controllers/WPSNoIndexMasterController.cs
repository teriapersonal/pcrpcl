﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQS.Models;

namespace IEMQS.Areas.WPS.Controllers
{
    public class WPSNoIndexMasterController : clsBase
    {
        // GET: WPS/WPSNoIndexMaster
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LoadWPSNoIndexListDataTable(JQueryDataTableParamModel param)
        {
            try
            {
                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                //whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "", "wps1.Location");
                var lstLoc = string.Join(",", db.ATH001.Where(x => x.Employee == objClsLoginInfo.UserName).Select(x => x.Location).Distinct().ToList());

                whereCondition += Manager.MakeStringInCondition("wps4.Location", lstLoc);

                //whereCondition += " and wps1.Location IN('HZW', 'HZW')";

                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += " and (wps4.Location like '%" + param.sSearch + "%'" +
                                        " or wps4.Key1 like '%" + param.sSearch + "%'" +
                                        " or wps4.Key2 like '%" + param.sSearch + "%'" +
                                        " or wps4.QualityProject like '%" + param.sSearch + "%'" +
                                        " or wps4.WPSNumber like '%" + param.sSearch + "%'" +
                                        " or wps4.CreatedBy+'-'+ecom3.t_name like '%" + param.sSearch + "%')";
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);

                }

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstHeader = db.SP_WPS_GetWPSNoIndex(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstHeader
                          select new[] {
                        Convert.ToString(a.Key1),
                        Convert.ToString(a.Key2),
                        a.QualityProject,
                        "<a onclick=\"RedirectWPS('"+a.WPSNumber+"','"+a.QualityProject+"',"+a.WPSHeaderId+")\">"+a.WPSNumber+"</a>",
                        a.CreatedBy,
                        Convert.ToDateTime(a.CreatedOn).ToString("dd/MM/yyyy"),
                        a.Location,
                        a.InUsed.ToString().ToLower(),
                         Convert.ToString(a.Id)
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult WPSNoIndexMasterPartial()
        {
            WPS004 objWPS004 = new WPS004();
            var location = (from a in db.COM003
                            join b in db.COM002 on a.t_loca equals b.t_dimx
                            where b.t_dtyp == 1 && a.t_psno.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase) && a.t_actv == 1
                            select b.t_dimx + " - " + b.t_desc).FirstOrDefault();

            ViewBag.Location = location;
            var loc = location.Split('-')[0];
            var lstKey1 = db.WPS003.Where(i => i.Location.Equals(loc, StringComparison.Ordinal)).OrderBy(i => i.Key1).Select(i => i.Key1).Distinct().ToList();
            ViewBag.Key1 = lstKey1.Select(i => new CategoryData { Value = i.Value.ToString() }).Distinct().ToList();
            //ViewBag.Key2 = db.WPS003.Where(i => i.Location.Equals(loc, StringComparison.Ordinal)).Select(i => new CategoryData { Value = i.Key2.ToString() }).ToList();
            return PartialView("_WPSNoIndexMasterPartial", objWPS004);
        }

        [HttpPost]
        public ActionResult Save(FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            WPS004 objWPS004 = new WPS004();
            try
            {
                if (fc != null)
                {
                    int id = Convert.ToInt32(fc["Id"]);
                    string location = fc["Location"].Split('-')[0];
                    if (!isWPSIndexExist(location, Convert.ToInt32(fc["Key1"]), Convert.ToInt32(fc["Key2"]), fc["QualityProject"], id))
                    {
                        if (id > 0)
                        {
                            var objInUsed = db.SP_WPS_GetWPSNoIndex(0, 0, "", "wps4.Id="+id).FirstOrDefault();
                            if (objInUsed != null && objInUsed.InUsed.HasValue && objInUsed.InUsed.Value)
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = "You Can Not Modify This Key Number Index Because Already Used In Maitain WPS.";
                                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                            }
                            #region WPS Number Update
                            if (!isWPSNoExistInWPS(objWPS004.Location, objWPS004.WPSNumber))
                            {
                                objWPS004 = db.WPS004.Where(i => i.Id == id).FirstOrDefault();
                                objWPS004.Key1 = Convert.ToInt32(fc["Key1"]);
                                objWPS004.Key2 = Convert.ToInt32(fc["Key2"]);
                                objWPS004.QualityProject = fc["QualityProject"];
                                objWPS004.WPSNumber = fc["WPSNumber"];
                                objWPS004.EditedBy = objClsLoginInfo.UserName;
                                objWPS004.EditedOn = DateTime.Now;

                                db.SaveChanges();

                                objResponseMsg.Key = true;
                                objResponseMsg.Value = clsImplementationMessage.WPSNoIndexMaster.Save;
                            }
                            else
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = string.Format("WPS : {0} cannot be modified, it is already used to generate WPS", objWPS004.WPSNumber);
                                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                            }
                            #endregion
                        }
                        else
                        {
                            var Key1 = Convert.ToInt32(fc["Key1"]);
                            var Key2 = Convert.ToInt32(fc["Key2"]);
                            var objWPS003 = db.WPS003.Where(i => i.Key1 == Key1 && i.Key2 == Key2 && i.Location == location).FirstOrDefault();
                            if (objWPS003 != null)
                            {
                                if (string.IsNullOrEmpty(objWPS003.JointType) || (string.IsNullOrWhiteSpace(objWPS003.JointType)))
                                {
                                    objResponseMsg.Key = false;
                                    objResponseMsg.Value = string.Format(clsImplementationMessage.WPSNoIndexMaster.JointTypeNotDefine, Key1, Key2, location);
                                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                                }
                                #region Add New WPS Number Index
                                objWPS004.Location = location;
                                objWPS004.Key1 = Convert.ToInt32(fc["Key1"]);
                                objWPS004.Key2 = Convert.ToInt32(fc["Key2"]);
                                objWPS004.QualityProject = fc["QualityProject"];
                                objWPS004.WPSNumber = fc["WPSNumber"];
                                objWPS004.CreatedBy = objClsLoginInfo.UserName;
                                objWPS004.CreatedOn = DateTime.Now;

                                db.WPS004.Add(objWPS004);
                                db.SaveChanges();

                                objResponseMsg.Key = true;
                                objResponseMsg.Value = clsImplementationMessage.WPSNoIndexMaster.Save;
                            }
                            else
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = string.Format(clsImplementationMessage.WPSNoIndexMaster.WPSKeyNoNotExist, Key1, Key2, location);
                                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "WPS No Already Exist";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteWPSNoIndex(int id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            WPS004 objWPS004 = new WPS004();
            try
            {
                if (id > 0)
                {
                    objWPS004 = db.WPS004.Where(i => i.Id == id).FirstOrDefault();
                    if (!isWPSNoExistInWPS(objWPS004.Location, objWPS004.WPSNumber))
                    {
                        if (objWPS004 != null)
                        {
                            db.WPS004.Remove(objWPS004);
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = string.Format(clsImplementationMessage.WPSNoIndexMaster.Delete, objWPS004.WPSNumber);
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Record Doesn't Exist";
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = string.Format(clsImplementationMessage.WPSNoIndexMaster.RestrictDelete, objWPS004.WPSNumber);
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Record Doesn't Exist";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetQualityProjectDetails(string term, string location)
        {
            //string username = objClsLoginInfo.UserName;
            //var lstATHLocation = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.Location).Distinct().ToList();
            //var lstCOMLocation = db.COM003.Where(i => i.t_psno.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_loca).Distinct().ToList();

            //lstCOMLocation = lstATHLocation.Union(lstCOMLocation).ToList();

            List<Projects> lstQualityProject = db.QMS010.Where(i => i.QualityProject.ToLower().Contains(term.ToLower())
                                                               && i.Location.Equals(location, StringComparison.OrdinalIgnoreCase)
                                                               ).Select(i => new Projects { Value = i.QualityProject, projectCode = i.QualityProject }).ToList();//&& i.CreatedBy.Equals(username,StringComparison.OrdinalIgnoreCase)
            return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetKey2(int key1, string location)
        {
            var Key2 = db.WPS003.Where(i => i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) &&
                                                             i.Key1 == key1).OrderBy(i => i.Key2).Select(i => i.Key2).Distinct().ToList();

            //List<CategoryData> lstKey2 = db.WPS003.Where(i => i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) &&
            //                                                 i.Key1 == key1).OrderBy(i => i.Key2).Select(i => new CategoryData { Value = i.Key2.ToString() }).ToList();
            List<CategoryData> lstKey2 = Key2.Select(i => new CategoryData { Value = i.Value.ToString() }).Distinct().ToList();

            return Json(lstKey2, JsonRequestBehavior.AllowGet);
        }

        public bool isWPSNoExistInWPS(string location, string wpsNo)
        {
            bool isWPSNoExistInWPS = false;
            if (db.WPS010.Where(i => i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && i.WPSNumber.Equals(wpsNo, StringComparison.OrdinalIgnoreCase)).Any())
                isWPSNoExistInWPS = true;

            return isWPSNoExistInWPS;
        }

        public bool isWPSIndexExist(string location, int key1, int key2, string qualityProject, int id)
        {
            bool isWPSIndexExist = false;
            if (db.WPS004.Where(i => i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && i.Key1 == key1 && i.Key2 == key2 && i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase) && i.Id != id).Any())
                isWPSIndexExist = true;

            return isWPSIndexExist;
        }

        public ActionResult WPSKeyGridDataPartial()
        {
            return PartialView("_WPSKeyGridDataPartial");
        }
        public ActionResult LoadWPSKeyListDataTable(JQueryDataTableParamModel param)
        {
            try
            {
                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                //whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "", "wps1.Location");
                var lstLoc = string.Join(",", db.ATH001.Where(x => x.Employee == objClsLoginInfo.UserName).Select(x => x.Location).Distinct().ToList());

                whereCondition += Manager.MakeStringInCondition("Location", lstLoc);

                //whereCondition += " and wps1.Location IN('HZW', 'HZW')";

                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += " and ( Key1 like '%" + param.sSearch + "%'" +
                                            " or Key2 like '%" + param.sSearch + "%'" +
                                            " or JointType like '%" + param.sSearch + "%'" +
                                            " or Parentmetal1 like '%" + param.sSearch + "%'" +
                                            " or Parentmetal2 like '%" + param.sSearch + "%'" +
                                            " or WeldingProcess1 like '%" + param.sSearch + "%'" +
                                            " or WeldingProcess2 like '%" + param.sSearch + "%'" +
                                            " or WeldingProcess3 like '%" + param.sSearch + "%'" +
                                            " or WeldingProcess4 like '%" + param.sSearch + "%'" +
                                            " or JointType like '%" + param.sSearch + "%'" +
                                            " or PWHT like '%" + param.sSearch + "%')";
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstHeader = db.SP_WPS_GET_KEY_DETAILS_FOR_NO_INDEX(0, 0, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstHeader
                          select new[] {
                        Convert.ToString(a.Id),
                        "",
                        Convert.ToString(a.Key1),
                        Convert.ToString(a.Key2),
                        a.Parentmetal1,
                        a.Parentmetal2,
                        a.WeldingProcess1,
                        a.WeldingProcess2,
                        a.WeldingProcess3,
                        a.WeldingProcess4,
                        a.JointType,
                        a.PWHT,
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #region Export Grid Data
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    List<SP_WPS_GetWPSNoIndex_Result> lst = db.SP_WPS_GetWPSNoIndex(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Key1 = li.Key1,
                                      Key2 = li.Key2,
                                      QualityProject = li.QualityProject,
                                      WPSNumber = li.WPSNumber,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      Location = li.Location
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.WPSKey.GetStringValue())
                {
                    List<SP_WPS_GET_KEY_DETAILS_FOR_NO_INDEX_Result> lst = db.SP_WPS_GET_KEY_DETAILS_FOR_NO_INDEX(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from a in lst
                                  select new
                                  {
                                      Key1 = Convert.ToString(a.Key1),
                                      Key2 = Convert.ToString(a.Key2),
                                      Parentmetal1 = a.Parentmetal1,
                                      Parentmetal2 = a.Parentmetal2,
                                      WeldingProcess1 = a.WeldingProcess1,
                                      WeldingProcess2 = a.WeldingProcess2,
                                      WeldingProcess3 = a.WeldingProcess3,
                                      WeldingProcess4 = a.WeldingProcess4,
                                      JointType = a.JointType,
                                      PWHT = a.PWHT,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

    }
}