﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQS.Areas.WPS.Models;
using IEMQS.Models;
using IEMQS.Areas.NDE.Models;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Data.Entity.Core.Objects;
using System.Text.RegularExpressions;

namespace IEMQS.Areas.WPS.Controllers
{
    public class MaintainWPSHeaderController : clsBase
    {
        // GET: WPS/MaintainWPSHeader
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Index()
        {
            ViewBag.Tab = Request.QueryString["tab"] != null ? Convert.ToString(Request.QueryString["tab"]) : "";
            return View();
        }

        // GET: WPS/MaintainWPSHeader/DisplayWPS
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult DisplayWPS()
        {
            return View();
        }
        public ActionResult GetWPSHeaderGridDataPartial(string status)
        {
            ViewBag.status = status;
            return PartialView("_GetWPSHeaderGridDataPartial");
        }

        [HttpPost]
        public JsonResult GetMaintainWPSNo(string term)
        {
            db.Configuration.ProxyCreationEnabled = false;

            var res = db.WPS010.Where(c => c.WPSNumber.Contains(term));

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult FetchmaintainWPSNo(string MainWPSNo)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var res = db.WPS010.Where(i => i.WPSNumber.Equals(MainWPSNo, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        #region WPS Number
        #region WPS Header
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult AddWPSHeader(int? headerId, string wpsno, string Status)
        {
            ViewBag.Status = Status;
            WPS010 objWPS010 = new WPS010();
            //var location = (from a in db.COM003
            //                join b in db.COM002 on a.t_loca equals b.t_dimx
            //                where b.t_dtyp == 1 && a.t_psno.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
            //                select b.t_dimx + " - " + b.t_desc).FirstOrDefault();

            string location = string.Empty;
            if (headerId > 0)
            {
                objWPS010 = db.WPS010.Where(i => i.HeaderId == headerId).FirstOrDefault();
                location = (from a in db.COM002
                            where a.t_dtyp == 1 && a.t_dimx.Equals(objWPS010.Location, StringComparison.OrdinalIgnoreCase)
                            select a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            }
            else
            {
                location = (from a in db.COM003
                            join b in db.COM002 on a.t_loca equals b.t_dimx
                            where b.t_dtyp == 1 && a.t_psno.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase) && a.t_actv == 1
                            select b.t_dimx + " - " + b.t_desc).FirstOrDefault();
            }
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("WPS010,WPS011");

            //ViewBag.Backing=


            //var lstWPSNo=db.WPS004.Where(i=>i.)

            if (headerId > 0)
            {
                //objWPS010 = db.WPS010.Where(i => i.HeaderId == headerId).FirstOrDefault();
            }
            else
            {
                objWPS010.WPSRevNo = 0;
                if (!string.IsNullOrEmpty(wpsno))
                {
                    objWPS010.WPSNumber = wpsno;
                    ViewBag.IsFromKeyIndex = "true";
                }
                objWPS010.Status = clsImplementationEnum.WPSHeaderStatus.Draft.GetStringValue();
            }

            ViewBag.Location = location;
            ViewBag.Tab = Request.QueryString["from"] != null ? Convert.ToString(Request.QueryString["from"]) : "";
            return View(objWPS010);
        }

        [HttpPost]
        public JsonResult NextWPSRequest(int? headerId, string status)
        {
            if (headerId != 0)
            {
                //ViewBag.roleId = roleId;
                ViewBag.status = status;
                string whereCondition = "1=1";
                var lstLoc = string.Join(",", db.ATH001.Where(x => x.Employee == objClsLoginInfo.UserName).Select(x => x.Location).Distinct().ToList());

                whereCondition += Manager.MakeStringInCondition("wps10.Location", lstLoc);

                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and status in('" + clsImplementationEnum.WPSHeaderStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.WPSHeaderStatus.Returned.GetStringValue() + "')";
                }
                var lstHeader = db.SP_WPS_GetWPSHeader(0, 0, "", whereCondition).ToList();
                var q = lstHeader.Where(i => i.HeaderId > headerId).OrderBy(i => i.HeaderId).FirstOrDefault();
                return Json(q, JsonRequestBehavior.AllowGet);
            }
            return Json(null);

        }
        [HttpPost]
        public JsonResult PrevWPSRequest(int? headerId, string status)
        {
            db.Configuration.ProxyCreationEnabled = false;
            if (headerId != 0)
            {
                //viewBag.roleId = roleId;
                ViewBag.status = status;
                string whereCondition = "1=1";
                var lstLoc = string.Join(",", db.ATH001.Where(x => x.Employee == objClsLoginInfo.UserName).Select(x => x.Location).Distinct().ToList());

                whereCondition += Manager.MakeStringInCondition("wps10.Location", lstLoc);
                var lstHeader = db.SP_WPS_GetWPSHeader(0, 0, "", whereCondition).ToList();
                var q = lstHeader.Where(i => i.HeaderId < headerId).OrderByDescending(i => i.HeaderId).FirstOrDefault();
                return Json(q, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);

        }


        [HttpPost]
        public ActionResult LoadPartial(int headerId, string partialName, string jointType)
        {
            WPS010 objWPS010 = new WPS010();
            string filepath = string.Empty;
            var partialname = "_WPSHeader" + partialName;

            //var location = (from a in db.COM003
            //                join b in db.COM002 on a.t_loca equals b.t_dimx
            //                where b.t_dtyp == 1 && a.t_psno.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
            //                select b.t_dimx + " - " + b.t_desc).FirstOrDefault();
            string location = string.Empty;
            string BU = string.Empty;
            if (headerId > 0)
            {
                objWPS010 = db.WPS010.Where(i => i.HeaderId == headerId).FirstOrDefault();
                location = (from a in db.COM002
                            where a.t_dtyp == 1 && a.t_dimx.Equals(objWPS010.Location, StringComparison.OrdinalIgnoreCase)
                            select a.t_dimx + " - " + a.t_desc).FirstOrDefault();

                //Notes: If we want to get category base on BU then uncomment below code.this is discuss with parth and satish
                //var QualityProject = db.WPS004.Where(i => i.WPSNumber == objWPS010.WPSNumber).FirstOrDefault().QualityProject;
                //string project = new NDEModels().GetQMSProject(QualityProject).Project;
                //BU = db.COM001.Where(i => i.t_cprj.Equals(project, StringComparison.OrdinalIgnoreCase)).FirstOrDefault().t_entu;

            }
            else
            {
                location = (from a in db.COM003
                            join b in db.COM002 on a.t_loca equals b.t_dimx
                            where b.t_dtyp == 1 && a.t_psno.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase) && a.t_actv == 1
                            select b.t_dimx + " - " + b.t_desc).FirstOrDefault();
            }

            ViewBag.Location = location;
            ViewBag.WeldingType = GetSubCatagoryNew("Welding Type", location.Split('-')[0], BU);
            //ViewBag.WeldingType = GetSubCatagory("Welding Type", location.Split('-')[0]).Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description });
            //ViewBag.WeldingType = GetSubCatagory("Welding Type", location.Split('-')[0]).Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code });
            List<string> lstBacking = clsBackingEnum.GetBacking();
            ViewBag.Backing = lstBacking.Select(x => new CategoryData { Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();
            //ViewBag.JointType = GetSubCatagory("Joint Type", location.Split('-')[0]).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description });
            ViewBag.JointType = GetSubCatagoryNew("Joint Type", location.Split('-')[0], BU);


            #region Category For Groove
            //ViewBag.PositionOfJoint = GetSubCatagory("Position of Joint", location.Split('-')[0]).Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description });
            //ViewBag.WeldingProgression = GetSubCatagory("Welding Progression", location.Split('-')[0]).Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description });
            //ViewBag.PositionOfFillet = GetSubCatagory("Position of Fillet", location.Split('-')[0]).Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description });
            ViewBag.PositionOfJoint = GetSubCatagoryNew("Position of Joint", location.Split('-')[0], BU);
            ViewBag.WeldingProgression = GetSubCatagoryNew("Welding Progression", location.Split('-')[0], BU);
            ViewBag.PositionOfFillet = GetSubCatagoryNew("Position of Fillet", location.Split('-')[0], BU);

            //ViewBag.PWHT = GetSubCatagory("PWHT", location.Split('-')[0]).Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description });
            ViewBag.PWHT = GetSubCatagoryNew("PWHT", location.Split('-')[0], BU);

            //ViewBag.ShieldingGasGasType = GetSubCatagory("Shielding Gas", location.Split('-')[0]).Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description });
            //ViewBag.BackingGasGasType = GetSubCatagory("Backing Gas", location.Split('-')[0]).Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description });
            //ViewBag.TrailingGasGasType = GetSubCatagory("Trailing Gas", location.Split('-')[0]).Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description });
            //ViewBag.Tungsten = GetSubCatagory("Tungsten", location.Split('-')[0]).Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description });
            //ViewBag.Modeofmetaltransfer = GetSubCatagory("Mode of Metal Transfer", location.Split('-')[0]).Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description });
            //ViewBag.Pulsing = GetSubCatagory("Pulsing", location.Split('-')[0]).Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description });

            //ViewBag.MultiSingleElectrode = GetSubCatagory("Electrode", location.Split('-')[0]).Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description });
            //ViewBag.MultipleSinglepassperside = GetSubCatagory("Pass per Side", location.Split('-')[0]).Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description });
            //ViewBag.SingleMultipleLayer = GetSubCatagory("Layer", location.Split('-')[0]).Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description });
            //ViewBag.StringWeaveBead = GetSubCatagory("Bead", location.Split('-')[0]).Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + " -" + i.Description });

            //ViewBag.Oscillation = GetSubCatagory("Oscillation", location.Split('-')[0]).Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description });
            //ViewBag.Peening = GetSubCatagory("Peening", location.Split('-')[0]).Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description });

            ViewBag.ShieldingGasGasType = GetSubCatagoryNew("Shielding Gas", location.Split('-')[0], BU);
            ViewBag.BackingGasGasType = GetSubCatagoryNew("Backing Gas", location.Split('-')[0], BU);
            ViewBag.TrailingGasGasType = GetSubCatagoryNew("Trailing Gas", location.Split('-')[0], BU);
            ViewBag.Tungsten = GetSubCatagoryNew("Tungsten", location.Split('-')[0], BU);
            ViewBag.Modeofmetaltransfer = GetSubCatagoryNew("Mode of Metal Transfer", location.Split('-')[0], BU);
            ViewBag.Pulsing = GetSubCatagoryNew("Pulsing", location.Split('-')[0], BU);

            ViewBag.MultiSingleElectrode = GetSubCatagoryNew("Electrode", location.Split('-')[0], BU);
            ViewBag.MultipleSinglepassperside = GetSubCatagoryNew("Pass per Side", location.Split('-')[0], BU);
            ViewBag.SingleMultipleLayer = GetSubCatagoryNew("Layer", location.Split('-')[0], BU);
            ViewBag.StringWeaveBead = GetSubCatagoryNew("Bead", location.Split('-')[0], BU);

            ViewBag.Oscillation = GetSubCatagoryNew("Oscillation", location.Split('-')[0], BU);
            ViewBag.Peening = GetSubCatagoryNew("Peening", location.Split('-')[0], BU);
            ViewBag.SupplementalDevice = GetSupplementalDevice().ToArray();
            #endregion

            #region Category For Overlay/HardFacing
            //ViewBag.PositionOverlayBarrier = GetSubCatagoryNew("Position Overlay - Barrier", location.Split('-')[0]).Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description });
            //ViewBag.PositionOverlaySubsequent = GetSubCatagoryNew("Position Overlay - Subsequent", location.Split('-')[0]).Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description });
            ViewBag.PositionOverlayBarrier = GetSubCatagoryNew("Position Overlay - Barrier", location.Split('-')[0], BU);
            ViewBag.PositionOverlaySubsequent = GetSubCatagoryNew("Position Overlay - Subsequent", location.Split('-')[0], BU);
            #endregion

            if (headerId > 0)
            {
                //objWPS010 = db.WPS010.Where(i => i.HeaderId == headerId).FirstOrDefault();
                ViewBag.BackingDefault = objWPS010.Backing != string.Empty ? objWPS010.Backing : clsBackingEnum.Backing.No.GetStringValue();
            }
            else
            {
                objWPS010.WPSRevNo = 0;
                ViewBag.BackingDefault = clsBackingEnum.Backing.No.GetStringValue();
            }

            filepath += "~/Areas/WPS/Views/MaintainWPSHeader/";
            NDEModels objNDEModels = new NDEModels();
            int key1 = 0, key2 = 0;
            if (!string.IsNullOrEmpty(objWPS010.WPSNumber))
            {
                key1 = Convert.ToInt32(objWPS010.WPSNumber.Split('-')[0]);
                key2 = Convert.ToInt32(objWPS010.WPSNumber.Split('-')[1]);
                var wpsKey = db.WPS003.Where(i => i.Key1 == key1 && i.Key2 == key2 && i.Location == objWPS010.Location).FirstOrDefault();

                objWPS010.ParentMaterial1 = wpsKey.Parentmetal1;
                objWPS010.ParentMaterial2 = wpsKey.Parentmetal2;
            }

            if (!string.IsNullOrEmpty(objWPS010.WeldingProcess1))
            {
                //ViewBag.WeldingProcess1 = objNDEModels.GetCategory(objWPS010.WeldingProcess1).CategoryDescription;
                ViewBag.WeldingProcess1 = objNDEModels.GetCategory(objWPS010.WeldingProcess1).Code;
            }
            if (!string.IsNullOrEmpty(objWPS010.WeldingProcess2))
            {
                //ViewBag.WeldingProcess2 = objNDEModels.GetCategory(objWPS010.WeldingProcess2).CategoryDescription;
                ViewBag.WeldingProcess2 = objNDEModels.GetCategory(objWPS010.WeldingProcess2).Code;
            }
            if (!string.IsNullOrEmpty(objWPS010.WeldingProcess3))
            {
                //ViewBag.WeldingProcess3 = objNDEModels.GetCategory(objWPS010.WeldingProcess3).CategoryDescription;
                ViewBag.WeldingProcess3 = objNDEModels.GetCategory(objWPS010.WeldingProcess3).Code;
            }
            if (!string.IsNullOrEmpty(objWPS010.WeldingProcess4))
            {
                //ViewBag.WeldingProcess4 = objNDEModels.GetCategory(objWPS010.WeldingProcess4).CategoryDescription;
                ViewBag.WeldingProcess4 = objNDEModels.GetCategory(objWPS010.WeldingProcess4).Code;
            }

            #region GrooveF1
            if (!string.IsNullOrEmpty(objWPS010.PositionofGrooveTTSJointWeldingProcess1)) { ViewBag.PositionofGrooveTTSJointWeldingProcess1 = GetCategoryDescription(objWPS010.PositionofGrooveTTSJointWeldingProcess1).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.PositionofGrooveTTSJointWeldingProcess2)) { ViewBag.PositionofGrooveTTSJointWeldingProcess2 = GetCategoryDescription(objWPS010.PositionofGrooveTTSJointWeldingProcess2).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.PositionofGrooveTTSJointWeldingProcess3)) { ViewBag.PositionofGrooveTTSJointWeldingProcess3 = GetCategoryDescription(objWPS010.PositionofGrooveTTSJointWeldingProcess3).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.PositionofGrooveTTSJointWeldingProcess4)) { ViewBag.PositionofGrooveTTSJointWeldingProcess4 = GetCategoryDescription(objWPS010.PositionofGrooveTTSJointWeldingProcess4).CategoryDescription; }

            if (!string.IsNullOrEmpty(objWPS010.WeldingProgressionWeldingProcess1)) { ViewBag.WeldingProgressionWeldingProcess1 = GetCategoryDescription(objWPS010.WeldingProgressionWeldingProcess1).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.WeldingProgressionWeldingProcess2)) { ViewBag.WeldingProgressionWeldingProcess2 = GetCategoryDescription(objWPS010.WeldingProgressionWeldingProcess2).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.WeldingProgressionWeldingProcess3)) { ViewBag.WeldingProgressionWeldingProcess3 = GetCategoryDescription(objWPS010.WeldingProgressionWeldingProcess3).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.WeldingProgressionWeldingProcess4)) { ViewBag.WeldingProgressionWeldingProcess4 = GetCategoryDescription(objWPS010.WeldingProgressionWeldingProcess4).CategoryDescription; }

            if (!string.IsNullOrEmpty(objWPS010.WeldingProgressionSubsequentWeldingProcess1)) { ViewBag.WeldingProgressionSubsequentWeldingProcess1 = GetCategoryDescription(objWPS010.WeldingProgressionSubsequentWeldingProcess1).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.WeldingProgressionSubsequentWeldingProcess2)) { ViewBag.WeldingProgressionSubsequentWeldingProcess2 = GetCategoryDescription(objWPS010.WeldingProgressionSubsequentWeldingProcess2).CategoryDescription; }

            if (!string.IsNullOrEmpty(objWPS010.PositionoffilletWeldingProcess1)) { ViewBag.PositionoffilletWeldingProcess1 = GetCategoryDescription(objWPS010.PositionoffilletWeldingProcess1).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.PositionoffilletWeldingProcess2)) { ViewBag.PositionoffilletWeldingProcess2 = GetCategoryDescription(objWPS010.PositionoffilletWeldingProcess2).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.PositionoffilletWeldingProcess3)) { ViewBag.PositionoffilletWeldingProcess3 = GetCategoryDescription(objWPS010.PositionoffilletWeldingProcess3).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.PositionoffilletWeldingProcess4)) { ViewBag.PositionoffilletWeldingProcess4 = GetCategoryDescription(objWPS010.PositionoffilletWeldingProcess4).CategoryDescription; }
            #endregion
            #region GrooveF2
            if (!string.IsNullOrEmpty(objWPS010.TypeofPWHT)) { ViewBag.TypeofPWHT = GetCategoryDescription(objWPS010.TypeofPWHT).CategoryDescription; }
            #endregion
            #region GrooveF3
            if (!string.IsNullOrEmpty(objWPS010.ShieldingGasGasType1)) { ViewBag.ShieldingGasGasType1 = GetCategoryDescription(objWPS010.ShieldingGasGasType1).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.ShieldingGasGasType2)) { ViewBag.ShieldingGasGasType2 = GetCategoryDescription(objWPS010.ShieldingGasGasType2).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.BackingGasGasType1)) { ViewBag.BackingGasGasType1 = GetCategoryDescription(objWPS010.BackingGasGasType1).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.BackingGasGasType2)) { ViewBag.BackingGasGasType2 = GetCategoryDescription(objWPS010.BackingGasGasType2).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.TrailingGasGasType1)) { ViewBag.TrailingGasGasType1 = GetCategoryDescription(objWPS010.TrailingGasGasType1).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.TrailingGasGasType2)) { ViewBag.TrailingGasGasType2 = GetCategoryDescription(objWPS010.TrailingGasGasType2).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.Tungsten)) { ViewBag.TungstenSelected = GetCategoryDescription(objWPS010.Tungsten).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.Modeofmetaltransfer)) { ViewBag.ModeofmetaltransferSelected = GetCategoryDescription(objWPS010.Modeofmetaltransfer).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.Pulsing)) { ViewBag.PulsingSelected = GetCategoryDescription(objWPS010.Pulsing).CategoryDescription; }
            #endregion

            #region GrooveF4
            if (!string.IsNullOrEmpty(objWPS010.MultiSingleElectrode1)) { ViewBag.MultiSingleElectrode1 = GetCategoryDescription(objWPS010.MultiSingleElectrode1).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.MultiSingleElectrode2)) { ViewBag.MultiSingleElectrode2 = GetCategoryDescription(objWPS010.MultiSingleElectrode2).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.MultiSingleElectrode3)) { ViewBag.MultiSingleElectrode3 = GetCategoryDescription(objWPS010.MultiSingleElectrode3).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.MultiSingleElectrode4)) { ViewBag.MultiSingleElectrode4 = GetCategoryDescription(objWPS010.MultiSingleElectrode4).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.MultipleSinglepassperside1)) { ViewBag.MultipleSinglepassperside1 = GetCategoryDescription(objWPS010.MultipleSinglepassperside1).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.MultipleSinglepassperside2)) { ViewBag.MultipleSinglepassperside2 = GetCategoryDescription(objWPS010.MultipleSinglepassperside2).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.MultipleSinglepassperside3)) { ViewBag.MultipleSinglepassperside3 = GetCategoryDescription(objWPS010.MultipleSinglepassperside3).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.MultipleSinglepassperside4)) { ViewBag.MultipleSinglepassperside4 = GetCategoryDescription(objWPS010.MultipleSinglepassperside4).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.SingleMultipleLayer1)) { ViewBag.SingleMultipleLayer1 = GetCategoryDescription(objWPS010.SingleMultipleLayer1).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.SingleMultipleLayer2)) { ViewBag.SingleMultipleLayer2 = GetCategoryDescription(objWPS010.SingleMultipleLayer2).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.SingleMultipleLayer3)) { ViewBag.SingleMultipleLayer3 = GetCategoryDescription(objWPS010.SingleMultipleLayer3).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.SingleMultipleLayer4)) { ViewBag.SingleMultipleLayer4 = GetCategoryDescription(objWPS010.SingleMultipleLayer4).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.StringWeaveBead1)) { ViewBag.StringWeaveBead1 = GetCategoryDescription(objWPS010.StringWeaveBead1).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.StringWeaveBead2)) { ViewBag.StringWeaveBead2 = GetCategoryDescription(objWPS010.StringWeaveBead2).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.StringWeaveBead3)) { ViewBag.StringWeaveBead3 = GetCategoryDescription(objWPS010.StringWeaveBead3).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.StringWeaveBead4)) { ViewBag.StringWeaveBead4 = GetCategoryDescription(objWPS010.StringWeaveBead4).CategoryDescription; }
            #endregion

            #region GrooveF5
            if (!string.IsNullOrEmpty(objWPS010.Oscillation1)) { ViewBag.Oscillation1 = GetCategoryDescription(objWPS010.Oscillation1).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.Oscillation2)) { ViewBag.Oscillation2 = GetCategoryDescription(objWPS010.Oscillation2).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.Oscillation3)) { ViewBag.Oscillation3 = GetCategoryDescription(objWPS010.Oscillation3).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.Oscillation4)) { ViewBag.Oscillation4 = GetCategoryDescription(objWPS010.Oscillation4).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.Peening)) { ViewBag.PeeningSelected = GetCategoryDescription(objWPS010.Peening).CategoryDescription; }
            #endregion

            if (string.Equals(jointType, "Groove", StringComparison.OrdinalIgnoreCase))
            {
                filepath += "GrooveFillet/_WPSHeader" + partialName + "Partial.cshtml";
            }
            else if (string.Equals(jointType, "Buttering", StringComparison.OrdinalIgnoreCase))
            {
                filepath += "Buttering/_WPSHeader" + partialName + "Partial.cshtml";
            }
            else if (string.Equals(jointType, "TTS", StringComparison.OrdinalIgnoreCase))
            {
                filepath += "TTS/_WPSHeader" + partialName + "Partial.cshtml";
            }
            else if (string.Equals(jointType, "OverLay", StringComparison.OrdinalIgnoreCase))
            {
                if (!string.IsNullOrEmpty(objWPS010.PositionOverlayBarrierWeldingProcess1)) { ViewBag.PositionOverlayBarrierWeldingProcess1 = GetCategoryDescription(objWPS010.PositionOverlayBarrierWeldingProcess1).CategoryDescription; }
                if (!string.IsNullOrEmpty(objWPS010.PositionOverlayBarrierWeldingProcess2)) { ViewBag.PositionOverlayBarrierWeldingProcess2 = GetCategoryDescription(objWPS010.PositionOverlayBarrierWeldingProcess2).CategoryDescription; }
                if (!string.IsNullOrEmpty(objWPS010.PositionOverlaySubsequentWeldingProcess1)) { ViewBag.PositionOverlaySubsequentWeldingProcess1 = GetCategoryDescription(objWPS010.PositionOverlaySubsequentWeldingProcess1).CategoryDescription; }
                if (!string.IsNullOrEmpty(objWPS010.PositionOverlaySubsequentWeldingProcess2)) { ViewBag.PositionOverlaySubsequentWeldingProcess2 = GetCategoryDescription(objWPS010.PositionOverlaySubsequentWeldingProcess2).CategoryDescription; }

                filepath += "OverlayHardfacing/_WPSHeader" + partialName + "Partial.cshtml";
            }
            else
            {
                if (!string.IsNullOrEmpty(objWPS010.WeldingType1))
                {
                    ViewBag.WeldingType1 = GetCategoryDescription(objWPS010.WeldingType1).CategoryDescription;
                }
                if (!string.IsNullOrEmpty(objWPS010.WeldingType2))
                {
                    ViewBag.WeldingType2 = GetCategoryDescription(objWPS010.WeldingType2).CategoryDescription;
                }
                if (!string.IsNullOrEmpty(objWPS010.WeldingType3))
                {
                    ViewBag.WeldingType3 = GetCategoryDescription(objWPS010.WeldingType3).CategoryDescription;
                }
                if (!string.IsNullOrEmpty(objWPS010.WeldingType4))
                {
                    ViewBag.WeldingType4 = GetCategoryDescription(objWPS010.WeldingType4).CategoryDescription;
                }
                filepath += "_WPSHeader.cshtml";
            }

            return PartialView(filepath, objWPS010);
        }

        public ActionResult loadHeaderDataTable(JQueryDataTableParamModel param, string status)
        {

            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                //whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "wps10.BU", "wps10.Location");

                var lstLoc = string.Join(",", db.ATH001.Where(x => x.Employee == objClsLoginInfo.UserName).Select(x => x.Location).Distinct().ToList());

                whereCondition += Manager.MakeStringInCondition("wps10.Location", lstLoc);

                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and status in('" + clsImplementationEnum.WPSHeaderStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.WPSHeaderStatus.Returned.GetStringValue() + "')";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " and ( lcom2.t_desc like '%" + param.sSearch + "%'" +
                                      " or wps10.Location like '%" + param.sSearch + "%'" +
                                      " or wps10.WPSNumber like '%" + param.sSearch + "%'" +
                                      " or wps10.WPSRevNo like '%" + param.sSearch + "%'" +
                                      " or wps10.Status like '%" + param.sSearch + "%'" +
                                      " or wps10.CreatedBy like '%" + param.sSearch + "%'" +
                                      " or wps10.Jointtype like '%" + param.sSearch + "%')";
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstHeader = db.SP_WPS_GetWPSHeader(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from h in lstHeader
                          select new[] {
                        "",
                        h.WPSNumber,//db.COM001.Where(i=>i.t_cprj==h.Project).Select(i=>i.t_dsca).FirstOrDefault(),//h.Project,
                        h.Jointtype,
                        "<span>R</span>"+Convert.ToString(h.WPSRevNo),//db.COM002.Where(i=>i.t_dtyp==2 && i.t_dimx==h.BU).Select(i=>i.t_desc).FirstOrDefault(),//h.BU,
                        h.Status,
                        Convert.ToString(h.CreatedBy),
                        Convert.ToDateTime(h.CreatedOn).ToString("dd/MM/yyyy"),
                        h.Location,//db.COM002.Where(i=>i.t_dtyp==1 && i.t_dimx==h.Location).Select(i=>i.t_desc).FirstOrDefault(),//h.Location,
                       h.Jointtype!=null? h.Jointtype.ToLower():"",
                       Convert.ToString(h.HeaderId),
                       Convert.ToString(h.WeldingProcess1),
                       Convert.ToString(h.WeldingProcess1),
                       Convert.ToString(h.WeldingProcess1),
                       Convert.ToString(h.WeldingProcess1),
                          };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveWPSHeader(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            WPS010 objWPS010 = new WPS010();
            try
            {
                if (fc != null)
                {
                    int headerId = Convert.ToInt32(fc["HeaderId"]);

                    if (headerId > 0)
                    {
                        objWPS010 = db.WPS010.Where(i => i.HeaderId == headerId).FirstOrDefault();
                        //if (string.IsNullOrEmpty(fc["PQR1"])) { objWPS010.PQR1 = null; } else { objWPS010.PQR1 = Convert.ToInt32(fc["PQR1"]); }
                        //if (string.IsNullOrEmpty(fc["PQR2"])) { objWPS010.PQR2 = null; } else { objWPS010.PQR2 = Convert.ToInt32(fc["PQR2"]); }
                        //if (string.IsNullOrEmpty(fc["PQR3"])) { objWPS010.PQR3 = null; } else { objWPS010.PQR3 = Convert.ToInt32(fc["PQR3"]); }
                        //if (string.IsNullOrEmpty(fc["PQR4"])) { objWPS010.PQR4 = null; } else { objWPS010.PQR4 = Convert.ToInt32(fc["PQR4"]); }
                        //if (string.IsNullOrEmpty(fc["PQR5"])) { objWPS010.PQR5 = null; } else { objWPS010.PQR5 = Convert.ToInt32(fc["PQR5"]); }
                        //if (string.IsNullOrEmpty(fc["PQR6"])) { objWPS010.PQR6 = null; } else { objWPS010.PQR6 = Convert.ToInt32(fc["PQR6"]); }
                        //if (string.IsNullOrEmpty(fc["PQR7"])) { objWPS010.PQR7 = null; } else { objWPS010.PQR7 = Convert.ToInt32(fc["PQR7"]); }

                        if (string.IsNullOrEmpty(fc["PQR1"])) { objWPS010.PQR1 = null; } else { objWPS010.PQR1 = Convert.ToString(fc["PQR1"]); }
                        if (string.IsNullOrEmpty(fc["PQR2"])) { objWPS010.PQR2 = null; } else { objWPS010.PQR2 = Convert.ToString(fc["PQR2"]); }
                        if (string.IsNullOrEmpty(fc["PQR3"])) { objWPS010.PQR3 = null; } else { objWPS010.PQR3 = Convert.ToString(fc["PQR3"]); }
                        if (string.IsNullOrEmpty(fc["PQR4"])) { objWPS010.PQR4 = null; } else { objWPS010.PQR4 = Convert.ToString(fc["PQR4"]); }
                        if (string.IsNullOrEmpty(fc["PQR5"])) { objWPS010.PQR5 = null; } else { objWPS010.PQR5 = Convert.ToString(fc["PQR5"]); }
                        if (string.IsNullOrEmpty(fc["PQR6"])) { objWPS010.PQR6 = null; } else { objWPS010.PQR6 = Convert.ToString(fc["PQR6"]); }
                        if (string.IsNullOrEmpty(fc["PQR7"])) { objWPS010.PQR7 = null; } else { objWPS010.PQR7 = Convert.ToString(fc["PQR7"]); }

                        //objWPS010.PQR1 = string.IsNullOrEmpty(fc["PQR1"]) ? 0 : Convert.ToInt32(fc["PQR1"]);
                        //objWPS010.PQR2 = string.IsNullOrEmpty(fc["PQR2"]) ? 0 : Convert.ToInt32(fc["PQR2"]);
                        //objWPS010.PQR3 = string.IsNullOrEmpty(fc["PQR3"]) ? 0 : Convert.ToInt32(fc["PQR3"]);
                        //objWPS010.PQR4 = string.IsNullOrEmpty(fc["PQR4"]) ? 0 : Convert.ToInt32(fc["PQR4"]);
                        //objWPS010.PQR5 = string.IsNullOrEmpty(fc["PQR5"]) ? 0 : Convert.ToInt32(fc["PQR5"]);
                        //objWPS010.PQR6 = string.IsNullOrEmpty(fc["PQR6"]) ? 0 : Convert.ToInt32(fc["PQR6"]);
                        //objWPS010.PQR7 = string.IsNullOrEmpty(fc["PQR7"]) ? 0 : Convert.ToInt32(fc["PQR7"]);
                        objWPS010.WeldingProcess1 = string.IsNullOrEmpty(fc["WeldingProcess1"]) ? string.Empty : fc["WeldingProcess1"];
                        objWPS010.WeldingProcess2 = string.IsNullOrEmpty(fc["WeldingProcess2"]) ? string.Empty : fc["WeldingProcess2"];
                        objWPS010.WeldingProcess3 = string.IsNullOrEmpty(fc["WeldingProcess3"]) ? string.Empty : fc["WeldingProcess3"];
                        objWPS010.WeldingProcess4 = string.IsNullOrEmpty(fc["WeldingProcess4"]) ? string.Empty : fc["WeldingProcess4"];
                        objWPS010.WeldingType1 = string.IsNullOrEmpty(fc["WeldingType1"]) ? string.Empty : fc["WeldingType1"];
                        objWPS010.WeldingType2 = string.IsNullOrEmpty(fc["WeldingType2"]) ? string.Empty : fc["WeldingType2"];
                        objWPS010.WeldingType3 = string.IsNullOrEmpty(fc["WeldingType3"]) ? string.Empty : fc["WeldingType3"];
                        objWPS010.WeldingType4 = string.IsNullOrEmpty(fc["WeldingType4"]) ? string.Empty : fc["WeldingType4"];
                        objWPS010.Jointtype = fc["Jointtype"];
                        objWPS010.Groovedesign = fc["Groovedesign"];
                        objWPS010.Backing = fc["Backing"];
                        objWPS010.BackingMaterial = fc["BackingMaterial"];
                        objWPS010.Other1 = fc["Other1"];


                        #region  Do Not Consider This
                        //#region GrooveF1
                        //objWPS010.ParentMaterial1 = fc["ParentMaterial1"];
                        //objWPS010.ParentMaterial2 = fc["ParentMaterial2"];
                        //objWPS010.GroupNo1 = fc["GroupNo1"];
                        //objWPS010.GroupNo2 = fc["GroupNo2"];
                        //objWPS010.MaxDepositedWeldMetalThkWeldingProcess1 = fc["MaxDepositedWeldMetalThkWeldingProcess1"];
                        //objWPS010.MaxDepositedWeldMetalThkWeldingProcess2 = fc["MaxDepositedWeldMetalThkWeldingProcess2"];
                        //objWPS010.MaxDepositedWeldMetalThkWeldingProcess3 = fc["MaxDepositedWeldMetalThkWeldingProcess3"];
                        //objWPS010.MaxDepositedWeldMetalThkWeldingProcess4 = fc["MaxDepositedWeldMetalThkWeldingProcess4"];
                        //objWPS010.Specification1Grade1 = fc["Specification1Grade1"];
                        //objWPS010.Specification2Grade2 = fc["Specification2Grade2"];
                        //objWPS010.BaseMaterialgrooveThkRange = fc["BaseMaterialgrooveThkRange"];
                        //objWPS010.BasemetalFilletrange = fc["BasemetalFilletrange"];
                        //objWPS010.MaxPassThk = Convert.ToInt32(fc["MaxPassThk"]);
                        //objWPS010.Other2 = fc["Other2"];
                        //objWPS010.PositionofGrooveTTSJointWeldingProcess1 = fc["PositionofGrooveTTSJointWeldingProcess1"];
                        //objWPS010.PositionofGrooveTTSJointWeldingProcess2 = fc["PositionofGrooveTTSJointWeldingProcess2"];
                        //objWPS010.PositionofGrooveTTSJointWeldingProcess3 = fc["PositionofGrooveTTSJointWeldingProcess3"];
                        //objWPS010.PositionofGrooveTTSJointWeldingProcess4 = fc["PositionofGrooveTTSJointWeldingProcess4"];
                        //objWPS010.WeldingProgressionWeldingProcess1 = fc["WeldingProgressionWeldingProcess1"];
                        //objWPS010.WeldingProgressionWeldingProcess2 = fc["WeldingProgressionWeldingProcess2"];
                        //objWPS010.WeldingProgressionWeldingProcess3 = fc["WeldingProgressionWeldingProcess3"];
                        //objWPS010.WeldingProgressionWeldingProcess4 = fc["WeldingProgressionWeldingProcess4"];
                        //objWPS010.PositionoffilletWeldingProcess1 = fc["PositionoffilletWeldingProcess1"];
                        //objWPS010.PositionoffilletWeldingProcess2 = fc["PositionoffilletWeldingProcess2"];
                        //objWPS010.PositionoffilletWeldingProcess3 = fc["PositionoffilletWeldingProcess3"];
                        //objWPS010.PositionoffilletWeldingProcess4 = fc["PositionoffilletWeldingProcess4"];
                        //#endregion

                        //#region GrooveF2
                        //objWPS010.Thickness1 = fc["Thickness1"];
                        //objWPS010.Thickness = fc["Thickness"];
                        //objWPS010.Thickness3 = fc["Thickness3"];
                        //objWPS010.Thickness4 = fc["Thickness4"];
                        //objWPS010.Thickness5 = fc["Thickness5"];
                        //objWPS010.Thickness6 = fc["Thickness6"];
                        //objWPS010.PreheatTemp1 = Convert.ToDecimal(fc["PreheatTemp1"]);
                        //objWPS010.PreheatTemp2 = Convert.ToDecimal(fc["PreheatTemp2"]);
                        //objWPS010.PreheatTemp3 = Convert.ToDecimal(fc["PreheatTemp3"]);
                        //objWPS010.PreheatTemp4 = Convert.ToDecimal(fc["PreheatTemp4"]);
                        //objWPS010.PreheatTemp5 = Convert.ToDecimal(fc["PreheatTemp5"]);
                        //objWPS010.PreheatTemp6 = Convert.ToDecimal(fc["PreheatTemp6"]);
                        //objWPS010.InterpassTemp = Convert.ToDecimal(fc["InterpassTemp"]);
                        //objWPS010.Temp1 = Convert.ToDecimal(fc["Temp1"]);
                        //objWPS010.Time1 = fc["Time1"];
                        //objWPS010.Temp2 = Convert.ToDecimal(fc["Temp2"]);
                        //objWPS010.Time2 = fc["Time2"];
                        //objWPS010.PHMaintenance = fc["PHMaintenance"];
                        //objWPS010.Other3 = fc["Other3"];
                        //objWPS010.TypeofPWHT = fc["TypeofPWHT"];
                        //objWPS010.PWHTTempRange = fc["PWHTTempRange"];
                        //objWPS010.PWHTTimeRange = fc["PWHTTimeRange"];
                        //objWPS010.RateofHeatingCooling = fc["RateofHeatingCooling"];
                        //objWPS010.MethodofPWHT = fc["MethodofPWHT"];
                        //objWPS010.PWHTNotes = fc["PWHTNotes"];
                        //objWPS010.Other5 = fc["Other5"];
                        //#endregion

                        //#region GrooveF3
                        //objWPS010.ShieldingGasGasType1 = fc["ShieldingGasGasType1"];
                        //objWPS010.BackingGasGasType1 = fc["BackingGasGasType1"];
                        //objWPS010.TrailingGasGasType1 = fc["TrailingGasGasType1"];
                        //objWPS010.ShieldingGasComposition1 = fc["ShieldingGasComposition1"];
                        //objWPS010.BackingGasComposition1 = fc["BackingGasComposition1"];
                        //objWPS010.TrailingGasComposition1 = fc["TrailingGasComposition1"];
                        //objWPS010.ShieldingGasFlowRateLPM1 = fc["ShieldingGasFlowRateLPM1"];
                        //objWPS010.BackingGasFlowRateLPM1 = fc["BackingGasFlowRateLPM1"];
                        //objWPS010.TrailingGasFlowRateLPM1 = fc["TrailingGasFlowRateLPM1"];
                        //objWPS010.ShieldingGasGasType2 = fc["ShieldingGasGasType2"];
                        //objWPS010.BackingGasGasType2 = fc["BackingGasGasType2"];
                        //objWPS010.TrailingGasGasType2 = fc["TrailingGasGasType2"];
                        //objWPS010.ShieldingGasComposition2 = fc["ShieldingGasComposition2"];
                        //objWPS010.BackingGasComposition2 = fc["BackingGasComposition2"];
                        //objWPS010.TrailingGasComposition2 = fc["TrailingGasComposition2"];
                        //objWPS010.ShieldingGasFlowrateLPM2 = fc["ShieldingGasFlowrateLPM2"];
                        //objWPS010.BackingGasFlowrateLPM2 = fc["BackingGasFlowrateLPM2"];
                        //objWPS010.TrailingGasFlowrateLPM2 = fc["TrailingGasFlowrateLPM2"];
                        //objWPS010.Other6 = fc["Other6"];
                        //objWPS010.Tungsten = fc["Tungsten"];
                        //objWPS010.TungstenSize = fc["TungstenSize"];
                        //objWPS010.Modeofmetaltransfer = fc["Modeofmetaltransfer"];
                        //objWPS010.Pulsing = fc["Pulsing"];
                        //#endregion

                        //#region GrooveF4
                        //objWPS010.MultiSingleElectrode1 = fc["MultiSingleElectrode1"];
                        //objWPS010.MultiSingleElectrode2 = fc["MultiSingleElectrode2"];
                        //objWPS010.MultiSingleElectrode3 = fc["MultiSingleElectrode3"];
                        //objWPS010.MultiSingleElectrode4 = fc["MultiSingleElectrode4"];
                        //objWPS010.MultipleSinglepassperside1 = fc["MultipleSinglepassperside1"];
                        //objWPS010.MultipleSinglepassperside2 = fc["MultipleSinglepassperside2"];
                        //objWPS010.MultipleSinglepassperside3 = fc["MultipleSinglepassperside3"];
                        //objWPS010.MultipleSinglepassperside4 = fc["MultipleSinglepassperside4"];
                        //objWPS010.SingleMultipleLayer1 = fc["SingleMultipleLayer1"];
                        //objWPS010.SingleMultipleLayer2 = fc["SingleMultipleLayer2"];
                        //objWPS010.SingleMultipleLayer3 = fc["SingleMultipleLayer3"];
                        //objWPS010.SingleMultipleLayer4 = fc["SingleMultipleLayer4"];
                        //objWPS010.StringWeaveBead1 = fc["StringWeaveBead1"];
                        //objWPS010.StringWeaveBead2 = fc["StringWeaveBead2"];
                        //objWPS010.StringWeaveBead3 = fc["StringWeaveBead3"];
                        //objWPS010.StringWeaveBead4 = fc["StringWeaveBead4"];
                        //objWPS010.ContactTube_WorkDistance1 = fc["ContactTube_WorkDistance1"];
                        //objWPS010.ContactTube_WorkDistance2 = fc["ContactTube_WorkDistance2"];
                        //objWPS010.ContactTube_WorkDistance3 = fc["ContactTube_WorkDistance3"];
                        //objWPS010.ContactTube_WorkDistance4 = fc["ContactTube_WorkDistance4"];
                        //objWPS010.ElectrodeSpacing1 = fc["ElectrodeSpacing1"];
                        //objWPS010.ElectrodeSpacing2 = fc["ElectrodeSpacing2"];
                        //objWPS010.ElectrodeSpacing3 = fc["ElectrodeSpacing3"];
                        //objWPS010.ElectrodeSpacing4 = fc["ElectrodeSpacing4"];
                        //objWPS010.GasCupSize1 = fc["GasCupSize1"];
                        //objWPS010.GasCupSize2 = fc["GasCupSize2"];
                        //objWPS010.GasCupSize3 = fc["GasCupSize3"];
                        //objWPS010.GasCupSize4 = fc["GasCupSize4"];
                        //#endregion

                        //#region GrooveF5
                        //objWPS010.Oscillation1 = fc["Oscillation1"];
                        //objWPS010.Oscillation2 = fc["Oscillation2"];
                        //objWPS010.Oscillation3 = fc["Oscillation3"];
                        //objWPS010.Oscillation4 = fc["Oscillation4"];
                        //objWPS010.OscillationWidth1 = fc["OscillationWidth1"];
                        //objWPS010.OscillationWidth2 = fc["OscillationWidth2"];
                        //objWPS010.OscillationWidth3 = fc["OscillationWidth3"];
                        //objWPS010.OscillationWidth4 = fc["OscillationWidth4"];
                        //objWPS010.OscillationFreq1 = fc["OscillationFreq1"];
                        //objWPS010.OscillationFreq2 = fc["OscillationFreq2"];
                        //objWPS010.OscillationFreq3 = fc["OscillationFreq3"];
                        //objWPS010.OscillationFreq4 = fc["OscillationFreq4"];
                        //objWPS010.Peening = fc["Peening"];
                        //objWPS010.SupplementalDevice = fc["SupplementalDevice"];
                        //objWPS010.ChemicalComposition = fc["ChemicalComposition"];
                        //objWPS010.InitialInterpassCleaning = fc["InitialInterpassCleaning"];
                        //objWPS010.Backgouging = fc["Backgouging"];
                        //objWPS010.Note = fc["Note"];
                        //#endregion 
                        #endregion

                        objWPS010.EditedBy = objClsLoginInfo.UserName;
                        objWPS010.EditedOn = DateTime.Now;

                        if (string.Equals(objWPS010.Status, clsImplementationEnum.WPSHeaderStatus.Approved.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                        {
                            objWPS010.Status = clsImplementationEnum.WPSHeaderStatus.Draft.GetStringValue();
                            if (db.WPS011.Where(i => i.HeaderId == objWPS010.HeaderId).Any())
                            {
                                db.WPS011.Where(i => i.HeaderId == objWPS010.HeaderId).ToList().ForEach(i => i.WPSRevNo = objWPS010.WPSRevNo + 1);
                            }
                            objWPS010.CreatedBy = objClsLoginInfo.UserName;
                            objWPS010.CreatedOn = DateTime.Now;
                            objWPS010.WPSRevNo += 1;
                        }

                        db.SaveChanges();


                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.WPSHeader.Save;
                        objResponseMsg.HeaderId = objWPS010.HeaderId;
                        objResponseMsg.HeaderStatus = objWPS010.Status;
                        objResponseMsg.RevNo = Convert.ToString(objWPS010.WPSRevNo);
                    }
                    else
                    {
                        objWPS010.Location = fc["Location"].Split('-')[0];
                        objWPS010.WPSNumber = fc["WPSNumber"];
                        objWPS010.WPSRevNo = 0;
                        objWPS010.WeldingProcess1 = string.IsNullOrEmpty(fc["WeldingProcess1"]) ? string.Empty : fc["WeldingProcess1"];
                        objWPS010.WeldingProcess2 = string.IsNullOrEmpty(fc["WeldingProcess2"]) ? string.Empty : fc["WeldingProcess2"];
                        objWPS010.WeldingProcess3 = string.IsNullOrEmpty(fc["WeldingProcess3"]) ? string.Empty : fc["WeldingProcess3"];
                        objWPS010.WeldingProcess4 = string.IsNullOrEmpty(fc["WeldingProcess4"]) ? string.Empty : fc["WeldingProcess4"];
                        objWPS010.Jointtype = Convert.ToString(fc["Jointtype"]);
                        objWPS010.Status = clsImplementationEnum.WPSHeaderStatus.Draft.GetStringValue();
                        objWPS010.CreatedBy = objClsLoginInfo.UserName;
                        objWPS010.CreatedOn = DateTime.Now;

                        if (fc["Jointtype"] != null && (fc["Jointtype"].ToString().ToLower() == ("groove/fillet").ToLower() || fc["Jointtype"].ToString().ToLower() == ("t#ts").ToLower()))
                        {
                            objWPS010.Groovedesign = fc["Groovedesign"];
                            objWPS010.BackingMaterial = fc["BackingMaterial"];
                        }
                        objWPS010.PWHTNotes = "See eqpt. Drg./Spec for restriction within above range if any";

                        db.WPS010.Add(objWPS010);
                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.WPSHeader.Save;
                        objResponseMsg.HeaderId = objWPS010.HeaderId;
                        objResponseMsg.HeaderStatus = objWPS010.Status;
                        objResponseMsg.RevNo = Convert.ToString(objWPS010.WPSRevNo);
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveWPSHeaderColumnWiseDynamically(string strColumnWithData)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            WPS010 objWPS010 = new WPS010();

            List<ModelWPSHeader> lstModelWPSHeader;
            JavaScriptSerializer jss = new JavaScriptSerializer();
            try
            {
                if (!string.IsNullOrEmpty(strColumnWithData))
                {
                    string[] AutoCompleteItems = { "WeldingType1", "WeldingType2", "WeldingType3", "WeldingType4", "Backing",
                            "PositionofGrooveTTSJointWeldingProcess1", "PositionofGrooveTTSJointWeldingProcess2", "PositionofGrooveTTSJointWeldingProcess3", "PositionofGrooveTTSJointWeldingProcess4",
                            "WeldingProgressionWeldingProcess1","WeldingProgressionWeldingProcess2","WeldingProgressionWeldingProcess3","WeldingProgressionWeldingProcess4",
                            "PositionoffilletWeldingProcess1","PositionoffilletWeldingProcess2","PositionoffilletWeldingProcess3","PositionoffilletWeldingProcess4",
                            "TypeofPWHT","ShieldingGasGasType1","BackingGasGasType1","TrailingGasGasType1","ShieldingGasGasType2","BackingGasGasType2","TrailingGasGasType2","Tungsten","Modeofmetaltransfer","Pulsing",
                            "MultiSingleElectrode1","MultiSingleElectrode2","MultiSingleElectrode3","MultiSingleElectrode4","MultipleSinglepassperside1","MultipleSinglepassperside2","MultipleSinglepassperside3","MultipleSinglepassperside4",
                            "SingleMultipleLayer1","SingleMultipleLayer2","SingleMultipleLayer3","SingleMultipleLayer4","StringWeaveBead1","StringWeaveBead2","StringWeaveBead3","StringWeaveBead4",
                            "Oscillation1","Oscillation2","Oscillation3","Oscillation4","Peening","PositionOverlayBarrierWeldingProcess1","PositionOverlayBarrierWeldingProcess2",
                            "PositionOverlaySubsequentWeldingProcess1","PositionOverlaySubsequentWeldingProcess2","WeldingProgressionWeldingProcess1","WeldingProgressionWeldingProcess2","WeldingProgressionSubsequentWeldingProcess1","WeldingProgressionSubsequentWeldingProcess2",
                            "SupplementalDevice","ChemicalComposition","InitialInterpassCleaning","Note","ContactTubeWorkDistance1","ContactTubeWorkDistance2","ContactTubeWorkDistance3",
                            "ContactTubeWorkDistance4","ElectrodeSpacing1","ElectrodeSpacing2","ElectrodeSpacing3","ElectrodeSpacing4","GasCupSize1","GasCupSize2","GasCupSize3",
                            "GasCupSize4","OscillationWidth1","OscillationWidth2","OscillationWidth3","OscillationWidth4","OscillationFreq1","OscillationFreq2","OscillationFreq3",
                            "OscillationFreq4","Other6","TungstenSize","ShieldingGasComposition1","BackingGasComposition1","TrailingGasComposition1","ShieldingGasFlowRateLPM1",
                            "BackingGasFlowRateLPM1","TrailingGasFlowRateLPM1","ShieldingGasComposition2","BackingGasComposition2","TrailingGasComposition2","ShieldingGasFlowrateLPM2",
                            "BackingGasFlowrateLPM2","TrailingGasFlowrateLPM2",
                            "PHMaintenance","Other4","PWHTTempRange","PWHTTimeRange","RateofHeatingCooling","MethodofPWHT","PWHTNotes","Other5",
                            "Time2","Time1","Thickness6","Thickness5","Thickness4","Thickness3","Thickness","Thickness","Thickness1","Other3",
                            "WeldingProgressionWeldingProcess1A","WeldingProgressionWeldingProcess2B","MinQualifiedbasemetalthk","MinOverlaythk",
                            "TubesheetThk","TubeThickness","Pitch","TubeExpansionPriorToWelding","TubeOD","Ligament","TubeProjection","Other2",
                            "GroupNo1","GroupNo2","Specification1Grade1","Specification2Grade2","MaxDepositedWeldMetalThkWeldingProcess1","MaxDepositedWeldMetalThkWeldingProcess2",
                            "MaxDepositedWeldMetalThkWeldingProcess3","MaxDepositedWeldMetalThkWeldingProcess4","BaseMaterialgrooveThkRange","BasemetalFilletrange","Backgouging","Other7",
                            "MaxTime1","MaxTime2","Other8"
                    };
                    // old code :(Remove)changes done by pramod as on date:19-11-2019 "PQR1","PQR2","PQR3","PQR4","PQR5","PQR6","PQR7",
                    string[] IntTypeItems = { "MaxPassThk","PreheatTemp1","PreheatTemp2","PreheatTemp3",
                                            "PreheatTemp4","PreheatTemp5","PreheatTemp6","InterpassTemp","Temp1","Temp2","PreheatTempBarrier","InterpassTempBarrier",
                                            "PreheatTempSubsequent","InterpassTempSubsequent","MaxTemp1","MaxTemp2"};

                    lstModelWPSHeader = jss.Deserialize<List<ModelWPSHeader>>(strColumnWithData);
                    //var lstWPSHeader = lstModelWPSHeader.Where(i => !i.ColumnValue.Equals("") && i.ColumnValue != "0" && !i.ColumnName.Equals("HeaderId", StringComparison.OrdinalIgnoreCase) && !i.ColumnName.StartsWith("txt")).ToList();
                    var lstWPSHeader = lstModelWPSHeader.Where(i => !i.ColumnValue.Equals("") && !i.ColumnName.Equals("HeaderId", StringComparison.OrdinalIgnoreCase) && !i.ColumnName.StartsWith("txt")).ToList();

                    var lstAutoCompleteWPSHeader = lstModelWPSHeader.Where(i => i.ColumnValue.Equals("") && AutoCompleteItems.Contains(i.ColumnName)).ToList();
                    if (lstAutoCompleteWPSHeader.Count > 0)
                    {
                        lstWPSHeader.AddRange(lstAutoCompleteWPSHeader);
                    }

                    var lstIntTypeItems = lstModelWPSHeader.Where(i => i.ColumnValue.Equals("") && IntTypeItems.Contains(i.ColumnName)).ToList();
                    if (lstIntTypeItems.Count > 0)
                    {
                        lstIntTypeItems.ForEach(i => { i.ColumnValue = null; });
                        lstWPSHeader.AddRange(lstIntTypeItems);
                    }

                    int headerId = Convert.ToInt32(lstModelWPSHeader.Where(i => i.ColumnName.Equals("HeaderId", StringComparison.OrdinalIgnoreCase)).Select(i => i.ColumnValue).FirstOrDefault());
                    objWPS010 = db.WPS010.Where(i => i.HeaderId == headerId).FirstOrDefault();

                    if (objWPS010.Status == clsImplementationEnum.WPSHeaderStatus.SentForApproval.GetStringValue() || objWPS010.Status == clsImplementationEnum.WPSHeaderStatus.Approved.GetStringValue())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.IsInformation = true;
                        objResponseMsg.Value = "You Can Not Modify WPS Because It's In Send For Approval Or Approved Status.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }

                    if (lstWPSHeader.Any())
                    {
                        foreach (var item in lstWPSHeader)
                        {
                            db.SP_WPS_UPDATE_COLUMNS(headerId, item.ColumnName, item.ColumnValue);
                        }

                        if (string.Equals(objWPS010.Status, clsImplementationEnum.WPSHeaderStatus.Approved.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                        {
                            objWPS010.Status = clsImplementationEnum.WPSHeaderStatus.Draft.GetStringValue();
                            if (db.WPS011.Where(i => i.HeaderId == objWPS010.HeaderId).Any())
                            {
                                db.WPS011.Where(i => i.HeaderId == objWPS010.HeaderId).ToList().ForEach(i => i.WPSRevNo = objWPS010.WPSRevNo + 1);
                            }
                            objWPS010.CreatedBy = objClsLoginInfo.UserName;
                            objWPS010.CreatedOn = DateTime.Now;
                            objWPS010.WPSRevNo += 1;
                            db.SaveChanges();
                        }

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.WPSHeader.Save;
                        objResponseMsg.HeaderId = objWPS010.HeaderId;
                        objResponseMsg.HeaderStatus = objWPS010.Status;
                        objResponseMsg.RevNo = Convert.ToString(objWPS010.WPSRevNo);
                    }
                    else
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.WPSHeader.Save;
                        objResponseMsg.HeaderId = objWPS010.HeaderId;
                        objResponseMsg.HeaderStatus = objWPS010.Status;
                        objResponseMsg.RevNo = Convert.ToString(objWPS010.WPSRevNo);
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Record Updated";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteWPSHeader(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            WPS010 objWPS010 = new WPS010();
            try
            {
                if (headerId > 0)
                {
                    objWPS010 = db.WPS010.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    if (objWPS010 != null)
                    {
                        if ((string.Equals(objWPS010.Status, clsImplementationEnum.WPSHeaderStatus.Draft.GetStringValue(), StringComparison.OrdinalIgnoreCase) || objWPS010.Status == clsImplementationEnum.WPSHeaderStatus.Returned.GetStringValue()) && objWPS010.WPSRevNo == 0)
                        {
                            if (db.WPS011.Where(i => i.HeaderId == headerId).Any())
                            {
                                db.WPS011.RemoveRange(db.WPS011.Where(i => i.HeaderId == headerId).ToList());
                            }
                            db.WPS010.Remove(objWPS010);
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = string.Format(clsImplementationMessage.WPSHeader.Delete.ToString(), objWPS010.WPSNumber);
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = string.Format("WPS:{0} cannot be deleted.", objWPS010.WPSNumber);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReviseWPS(int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            WPS010 objWPS010 = new WPS010();
            try
            {
                if (headerId > 0)
                {
                    objWPS010 = db.WPS010.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    if (objWPS010 != null)
                    {
                        if (string.Equals(objWPS010.Status, clsImplementationEnum.WPSHeaderStatus.Approved.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                        {
                            objWPS010.Status = clsImplementationEnum.WPSHeaderStatus.Draft.GetStringValue();
                            if (db.WPS011.Where(i => i.HeaderId == objWPS010.HeaderId).Any())
                            {
                                db.WPS011.Where(i => i.HeaderId == objWPS010.HeaderId).ToList().ForEach(i => i.WPSRevNo = objWPS010.WPSRevNo + 1);
                            }
                            objWPS010.CreatedBy = objClsLoginInfo.UserName;
                            objWPS010.CreatedOn = DateTime.Now;
                            objWPS010.EditedBy = null;
                            objWPS010.EditedOn = null;
                            objWPS010.ApprovedBy = null;
                            objWPS010.ApprovedOn = null;
                            objWPS010.SubmittedBy = null;
                            objWPS010.SubmittedOn = null;
                            objWPS010.ReturnedBy = null;
                            objWPS010.ReturnedOn = null;

                            objWPS010.WPSRevNo += 1;
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = string.Format("WPS Revised Successfully.", objWPS010.WPSNumber);
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.IsInformation = true;
                            objResponseMsg.Value = string.Format("WPS Is Not In Approved Status So Can't Be Revised WPS.", objWPS010.WPSNumber);
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.IsInformation = true;
                        objResponseMsg.Value = string.Format("WPS Not Exist.", objWPS010.WPSNumber);
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }
        #endregion

        #region WPS Lines
        [HttpPost]
        public ActionResult GetWPSLinesPartial(int headerId, int lineId)
        {
            WPS011 objWPS011 = new WPS011();
            WPS010 objWPS010 = new WPS010();
            NDEModels objNDEModels = new NDEModels();
            string BU = string.Empty;


            if (headerId > 0)
                objWPS010 = db.WPS010.Where(i => i.HeaderId == headerId).FirstOrDefault();

            ////if We add BU for get Category then remove below code comment
            //var QualityProject = db.WPS004.Where(i => i.WPSNumber == objWPS010.WPSNumber).FirstOrDefault().QualityProject;
            //string project = new NDEModels().GetQMSProject(QualityProject).Project;
            //BU = db.COM001.Where(i => i.t_cprj.Equals(project, StringComparison.OrdinalIgnoreCase)).FirstOrDefault().t_entu;


            List<CategoryData> lstWeldingProc = new List<CategoryData>();
            //new Tuple<string, string> (objWPS010.WeldingProcess1,"")
            lstWeldingProc.AddRange(new List<CategoryData> { new CategoryData { Value= objWPS010.WeldingProcess1,CategoryDescription= !string.IsNullOrEmpty(objWPS010.WeldingProcess1) ? GetCategoryDescription(objWPS010.WeldingProcess1).CategoryDescription : string.Empty },
                                                             new CategoryData { Value=objWPS010.WeldingProcess2, CategoryDescription= !string.IsNullOrEmpty(objWPS010.WeldingProcess2)?GetCategoryDescription(objWPS010.WeldingProcess2).CategoryDescription:string.Empty},
                                                             new CategoryData { Value=objWPS010.WeldingProcess3, CategoryDescription= !string.IsNullOrEmpty(objWPS010.WeldingProcess3)?GetCategoryDescription(objWPS010.WeldingProcess3).CategoryDescription:string.Empty },
                                                             new CategoryData { Value=objWPS010.WeldingProcess4, CategoryDescription = !string.IsNullOrEmpty(objWPS010.WeldingProcess4)?GetCategoryDescription(objWPS010.WeldingProcess4).CategoryDescription:string.Empty } });

            ViewBag.Jointtype = objWPS010.Jointtype;

            ViewBag.WeldingProcess = lstWeldingProc.Where(i => i.CategoryDescription != "" && i.Code != "").ToList();

            //if change sequenceno of AWSClass Description then change in GetAWSClassDetail(string awsClass, string location) function also
            ViewBag.AWSClass = db.WPS002.Where(i => i.Location.Equals(objWPS010.Location, StringComparison.OrdinalIgnoreCase)).Select(i => new CategoryData { Value = i.Id.ToString(), CategoryDescription = i.AWSClass + "|" + i.FNumber + "|" + i.SFANo + "|" + i.ANumber + "|" + i.ConsumableSize }).ToList();
            //ViewBag.WeldLayer = GetSubCatagory("Weld Layer", objWPS010.Location, BU).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description });
            //ViewBag.SupplementalFiller = GetSubCatagory("Supplemental Filler", objWPS010.Location, BU).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description });
            //ViewBag.FluxType = GetSubCatagory("Flux Type", objWPS010.Location, BU).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description });
            //ViewBag.RecrushedSlag = GetSubCatagory("Recrushed Slag", objWPS010.Location, BU).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description });
            //ViewBag.FillerMetalProductForm = GetSubCatagory("Filler Metal Product Form", objWPS010.Location, BU).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description });
            //ViewBag.ConsumableInsert = GetSubCatagory("Consumable Insert", objWPS010.Location, BU).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description });
            //ViewBag.CurrentType = GetSubCatagory("Current Type", objWPS010.Location, BU).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description });
            //ViewBag.CurrentPolarity = GetSubCatagory("Current Polarity", objWPS010.Location, BU).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + "-" + i.Description });
            if (objWPS010.Jointtype.ToUpper() == "BUTTERING")
            {
                var glbData = GetSubCatagory("Weld Layer", objWPS010.Location, BU).Where(a => a.Code.ToUpper() == "FIRST LAYER" || a.Code.ToUpper() == "SUBSEQUENT").Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Description });
                ViewBag.WeldLayer = glbData;
            }
            else
            {
                ViewBag.WeldLayer = GetSubCatagory("Weld Layer", objWPS010.Location, BU).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Description });
            }
            ViewBag.SupplementalFiller = GetSubCatagory("Supplemental Filler", objWPS010.Location, BU).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Description });
            ViewBag.FluxType = GetSubCatagory("Flux Type", objWPS010.Location, BU).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Description });
            ViewBag.RecrushedSlag = GetSubCatagory("Recrushed Slag", objWPS010.Location, BU).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Description });
            ViewBag.FillerMetalProductForm = GetSubCatagory("Filler Metal Product Form", objWPS010.Location, BU).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Description });
            ViewBag.ConsumableInsert = GetSubCatagory("Consumable Insert", objWPS010.Location, BU).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Description });
            ViewBag.CurrentType = GetSubCatagory("Current Type", objWPS010.Location, BU).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Description });
            ViewBag.CurrentPolarity = GetSubCatagory("Current Polarity", objWPS010.Location, BU).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Description });

            if (lineId > 0)
            {
                objWPS011 = db.WPS011.Where(i => i.HeaderId == headerId && i.LineId == lineId).FirstOrDefault();
                ViewBag.WeldingProcessSelected = GetCategoryDescription(objWPS011.WeldingProcess).CategoryDescription;
                ViewBag.WeldLayerSelected = GetCategoryDescription(objWPS011.WeldLayer).CategoryDescription;
                ViewBag.SupplementalFillerSelected = GetCategoryDescription(objWPS011.SupplementalFiller).CategoryDescription;
                ViewBag.FluxTypeSelected = GetCategoryDescription(objWPS011.FluxType).CategoryDescription;
                ViewBag.RecrushedSlagSelected = GetCategoryDescription(objWPS011.ReCrushedSlag).CategoryDescription;
                ViewBag.FillerMetalProductFormSelected = GetCategoryDescription(objWPS011.FillerMetalProductForm).CategoryDescription;
                ViewBag.ConsumableInsertSelected = GetCategoryDescription(objWPS011.ConsumableInsert).CategoryDescription;
                ViewBag.CurrentTypeSelected = GetCategoryDescription(objWPS011.CurrentType).CategoryDescription;
                ViewBag.CurrentPolaritySelected = GetCategoryDescription(objWPS011.CurrentPolarity).CategoryDescription;
                ViewBag.FillerMetalTradeName = db.WPS001.Where(i => i.AWSClass.ToLower().Equals(objWPS011.AWSClass.ToLower()) && i.Location.Equals(objWPS010.Location)).Select(i => new CategoryData { Value = i.BrandName, CategoryDescription = i.BrandName }).ToList();
                ViewBag.FluxTradeName = db.WPS001.Where(i => i.Location.Equals(objWPS010.Location) && i.Producttype.ToLower().Equals("flux")).Select(i => new CategoryData { Value = i.BrandName, CategoryDescription = i.BrandName }).ToList();
            }
            else
            {
                objWPS011.HeaderId = headerId;
                objWPS011.WPSRevNo = objWPS010.WPSRevNo;
            }
            return PartialView("_GetWPSLinesPartial", objWPS011);
        }

        public ActionResult LoadWPSLinesDataTable(JQueryDataTableParamModel param, int headerId)
        {

            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                //whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "wps10.BU", "wps10.Location");

                var lstLoc = string.Join(",", db.ATH001.Where(x => x.Employee == objClsLoginInfo.UserName).Select(x => x.Location).Distinct().ToList());
                whereCondition += " AND HeaderId=" + headerId;
                //whereCondition += Manager.MakeStringInCondition("wps11.Location", lstLoc);

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    //whereCondition += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or Location like '%" + param.sSearch + "%' or wps11.WPSNumber like '%" + param.sSearch + "%' or WPSRevNo like '%" + param.sSearch + "%' or wps11.CreatedBy like '%" + param.sSearch + "%')";
                    whereCondition += " and ( Location like '%" + param.sSearch + "%' or wps11.WPSNumber like '%" + param.sSearch + "%' or WPSRevNo like '%" + param.sSearch + "%' or wps11.CreatedBy like '%" + param.sSearch
                                       + "%' or  WeldingProcess like '%" + param.sSearch + "%' or WeldLayer like '%" + param.sSearch + "%' or AWSClass like '%" + param.sSearch + "%' or FillerMetalSize like '%" + param.sSearch
                                       + "%' or ANumber like '%" + param.sSearch + "%' or SupplementalFiller like '%" + param.sSearch + "%' or FluxType like '%" + param.sSearch + "%' or FillerMetalTradeName like '%" + param.sSearch
                                       + "%' or ReCrushedSlag like '%" + param.sSearch + "%' or SFANo like '%" + param.sSearch + "%' or FillerMetalProductForm like '%" + param.sSearch
                                       + "%' or ElectrodeFluxClass like '%" + param.sSearch + "%' or FluxTradeName like '%" + param.sSearch + "%' or ConsumableInsert like '%" + param.sSearch
                                       + "%' or CurrentType like '%" + param.sSearch + "%' or CurrentPolarity like '%" + param.sSearch + "%' or ConsumableInsert like '%" + param.sSearch + "%')";
                }

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion


                var lstLines = db.SP_WPS_GETHEADERLINES(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstLines
                          select new[] {
                        Convert.ToString(a.ROW_NO),
                        Convert.ToString(a.HeaderId),
                        Convert.ToString(a.LineId),
                        a.WeldingProcess,
                        a.WeldLayer,
                        a.AWSClass,
                        a.FillerMetalSize,
                        a.ANumber,
                        a.SupplementalFiller,
                        a.FluxType,
                        GetActionForLine(a.HeaderId,a.LineId),//"Action",
                        a.FillerMetalTradeName,
                        a.ReCrushedSlag,
                        a.SFANo,
                        a.FNumber,
                        a.FillerMetalProductForm,
                        a.ElectrodeFluxClass,
                        a.FluxTradeName,
                        a.ConsumableInsert,
                        a.Other,
                        a.CurrentAMP,
                        a.TravelSpeed,
                        a.CurrentType,
                        a.CurrentPolarity,
                        a.Voltage,
                        a.BeadLength,
                        a.HeatInput,
                        a.WireFeedSpeed

                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveWPSLines(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            WPS010 objWPS010 = new WPS010();
            WPS011 objWPS011 = new WPS011();

            try
            {
                if (fc != null)
                {
                    int headerId = Convert.ToInt32(fc["HeaderId"]);
                    int lineId = Convert.ToInt32(fc["LineId"]);
                    if (headerId > 0)
                        objWPS010 = db.WPS010.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    if (objWPS010 != null && (objWPS010.Status == clsImplementationEnum.WPSHeaderStatus.SentForApproval.GetStringValue() || objWPS010.Status == clsImplementationEnum.WPSHeaderStatus.Approved.GetStringValue()))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.IsInformation = true;
                        objResponseMsg.Value = "WPS Status Is Send For Approval Or Approved Status. You Can Not Perform Any Action.";
                    }
                    else
                    {
                        if (lineId > 0)
                        {
                            #region Update Existing WPS Number Line
                            objWPS011 = db.WPS011.Where(i => i.HeaderId == headerId && i.LineId == lineId).FirstOrDefault();

                            objWPS011.WeldingProcess = fc["WeldingProcess"];
                            objWPS011.WeldLayer = fc["WeldLayer"];
                            objWPS011.AWSClass = fc["AWSClass"];
                            objWPS011.FillerMetalSize = fc["FillerMetalSize"];
                            objWPS011.ANumber = fc["ANumber"];
                            objWPS011.SupplementalFiller = fc["SupplementalFiller"];
                            objWPS011.FluxType = fc["FluxType"];
                            objWPS011.FillerMetalTradeName = fc["FillerMetalTradeName"];
                            objWPS011.ReCrushedSlag = fc["ReCrushedSlag"];
                            objWPS011.SFANo = fc["SFANo"];
                            objWPS011.FNumber = fc["FNumber"];
                            objWPS011.FillerMetalProductForm = fc["FillerMetalProductForm"];
                            objWPS011.ElectrodeFluxClass = fc["ElectrodeFluxClass"];
                            objWPS011.FluxTradeName = fc["FluxTradeName"];
                            objWPS011.ConsumableInsert = fc["ConsumableInsert"];
                            objWPS011.Other = fc["Other"];
                            objWPS011.CurrentAMP = fc["CurrentAMP"];
                            objWPS011.TravelSpeed = fc["TravelSpeed"];
                            objWPS011.CurrentType = fc["CurrentType"];
                            objWPS011.CurrentPolarity = fc["CurrentPolarity"];
                            objWPS011.Voltage = fc["Voltage"];
                            objWPS011.BeadLength = fc["BeadLength"];
                            objWPS011.HeatInput = fc["HeatInput"];
                            objWPS011.WireFeedSpeed = fc["WireFeedSpeed"];
                            objWPS011.EditedBy = objClsLoginInfo.UserName;
                            objWPS011.EditedOn = DateTime.Now;

                            if (string.Equals(objWPS010.Status, clsImplementationEnum.WPSHeaderStatus.Approved.GetStringValue()))
                            {
                                objWPS010.Status = clsImplementationEnum.WPSHeaderStatus.Draft.GetStringValue();
                                db.WPS011.Where(i => i.HeaderId == objWPS010.HeaderId).ToList().ForEach(i => i.WPSRevNo = objWPS010.WPSRevNo + 1);
                                objWPS010.CreatedBy = objClsLoginInfo.UserName;
                                objWPS010.CreatedOn = DateTime.Now;
                                objWPS010.WPSRevNo += 1;
                            }

                            db.SaveChanges();

                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.WPSLine.Save;
                            objResponseMsg.HeaderId = objWPS010.HeaderId;
                            objResponseMsg.HeaderStatus = objWPS010.Status;
                            objResponseMsg.RevNo = Convert.ToString(objWPS010.WPSRevNo);
                            #endregion
                        }
                        else
                        {
                            #region Add Lines To WPS Number
                            if (db.WPS011.Where(i => i.HeaderId == headerId).ToList().Count() < 8)
                            {
                                objWPS011.HeaderId = headerId;
                                objWPS011.Location = objWPS010.Location;
                                objWPS011.WPSNumber = objWPS010.WPSNumber;
                                objWPS011.WPSRevNo = objWPS010.WPSRevNo;
                                objWPS011.WeldingProcess = fc["WeldingProcess"];
                                objWPS011.WeldLayer = fc["WeldLayer"];
                                objWPS011.AWSClass = fc["AWSClass"];
                                objWPS011.FillerMetalSize = fc["FillerMetalSize"];
                                objWPS011.ANumber = fc["ANumber"];
                                objWPS011.SupplementalFiller = fc["SupplementalFiller"];
                                objWPS011.FluxType = fc["FluxType"];
                                objWPS011.FillerMetalTradeName = fc["FillerMetalTradeName"];
                                objWPS011.ReCrushedSlag = fc["ReCrushedSlag"];
                                objWPS011.SFANo = fc["SFANo"];
                                objWPS011.FNumber = fc["FNumber"];
                                objWPS011.FillerMetalProductForm = fc["FillerMetalProductForm"];
                                objWPS011.ElectrodeFluxClass = fc["ElectrodeFluxClass"];
                                objWPS011.FluxTradeName = fc["FluxTradeName"];
                                objWPS011.ConsumableInsert = fc["ConsumableInsert"];
                                objWPS011.Other = fc["Other"];
                                objWPS011.CurrentAMP = fc["CurrentAMP"];
                                objWPS011.TravelSpeed = fc["TravelSpeed"];
                                objWPS011.CurrentType = fc["CurrentType"];
                                objWPS011.CurrentPolarity = fc["CurrentPolarity"];
                                objWPS011.Voltage = fc["Voltage"];
                                objWPS011.BeadLength = fc["BeadLength"];
                                objWPS011.HeatInput = fc["HeatInput"];
                                objWPS011.WireFeedSpeed = fc["WireFeedSpeed"];
                                objWPS011.CreatedBy = objClsLoginInfo.UserName;
                                objWPS011.CreatedOn = DateTime.Now;

                                db.WPS011.Add(objWPS011);

                                if (string.Equals(objWPS010.Status, clsImplementationEnum.WPSHeaderStatus.Approved.GetStringValue()))
                                {
                                    objWPS010.Status = clsImplementationEnum.WPSHeaderStatus.Draft.GetStringValue();
                                    db.WPS011.Where(i => i.HeaderId == objWPS010.HeaderId).ToList().ForEach(i => i.WPSRevNo = objWPS010.WPSRevNo + 1);
                                    objWPS010.CreatedBy = objClsLoginInfo.UserName;
                                    objWPS010.CreatedOn = DateTime.Now;
                                    objWPS010.WPSRevNo += 1;
                                }

                                db.SaveChanges();

                                objResponseMsg.Key = true;
                                objResponseMsg.Value = clsImplementationMessage.WPSLine.Save;
                                objResponseMsg.HeaderId = objWPS010.HeaderId;
                                objResponseMsg.HeaderStatus = objWPS010.Status;
                                objResponseMsg.RevNo = Convert.ToString(objWPS010.WPSRevNo);
                            }
                            else
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = objWPS010.WPSNumber + " is already having 8 Lines.You cannot add more lines";
                            }
                            #endregion

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }


            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteCategory(int lineId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            WPS011 objWPS011 = new WPS011();
            WPS010 objWPS010 = new WPS010();
            try
            {
                if (lineId > 0)
                {
                    objWPS011 = db.WPS011.Where(i => i.LineId == lineId).FirstOrDefault();
                    objWPS010 = db.WPS010.Where(i => i.HeaderId == objWPS011.HeaderId).FirstOrDefault();
                    if (objWPS010 != null && (objWPS010.Status == clsImplementationEnum.WPSHeaderStatus.SentForApproval.GetStringValue() || objWPS010.Status == clsImplementationEnum.WPSHeaderStatus.Approved.GetStringValue()))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.IsInformation = true;
                        objResponseMsg.Value = "WPS Status Is Send For Approval Or Approved Status. You Can Not Perform Any Action.";
                    }
                    else
                    {
                        if (objWPS011 != null)
                        {
                            if (string.Equals(objWPS010.Status, clsImplementationEnum.WPSHeaderStatus.Approved.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                            {
                                objWPS010.Status = clsImplementationEnum.WPSHeaderStatus.Draft.GetStringValue();
                                db.WPS011.Where(i => i.HeaderId == objWPS010.HeaderId).ToList().ForEach(i => i.WPSRevNo = objWPS010.WPSRevNo + 1);
                                objWPS010.CreatedBy = objClsLoginInfo.UserName;
                                objWPS010.CreatedOn = DateTime.Now;
                                objWPS010.WPSRevNo += 1;
                            }
                            db.WPS011.Remove(objWPS011);
                            db.SaveChanges();

                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.WPSLine.Delete;
                            objResponseMsg.HeaderId = objWPS010.HeaderId;
                            objResponseMsg.HeaderStatus = objWPS010.Status;
                            objResponseMsg.RevNo = Convert.ToString(objWPS010.WPSRevNo);
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Line doesn't exist";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        [HttpPost]
        public JsonResult SendForApproval(int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            WPS010 objWPS010 = new WPS010();
            try
            {
                if (headerId > 0)
                {
                    objWPS010 = db.WPS010.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    if (objWPS010.Status == clsImplementationEnum.WPSHeaderStatus.SentForApproval.GetStringValue() || objWPS010.Status == clsImplementationEnum.WPSHeaderStatus.Approved.ToString())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.IsInformation = true;
                        objResponseMsg.Value = "WPS Already Send For Approval Or Approved Status.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var lstLines = db.WPS011.Where(i => i.HeaderId == headerId).ToList();
                    if (lstLines.Any())
                    {
                        objWPS010.Status = clsImplementationEnum.WPSHeaderStatus.SentForApproval.GetStringValue();
                        objWPS010.SubmittedBy = objClsLoginInfo.UserName;
                        objWPS010.SubmittedOn = DateTime.Now;
                        objWPS010.ReturnedBy = null;
                        objWPS010.ReturnedOn = null;
                        objWPS010.ReturnRemarks = null;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = string.Format(clsImplementationMessage.WPSHeader.SentForApproval.ToString(), objWPS010.WPSNumber);

                        #region Get BU and Project From WPSNo                        
                        var QualityProject = db.WPS004.Where(i => i.WPSNumber == objWPS010.WPSNumber).FirstOrDefault().QualityProject;
                        var project = new NDEModels().GetQMSProject(QualityProject).Project;
                        var BU = db.COM001.Where(i => i.t_cprj.Equals(project, StringComparison.OrdinalIgnoreCase)).FirstOrDefault().t_entu;
                        #endregion

                        #region Send Notification
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.WE2.GetStringValue(), project, BU, objWPS010.Location, "WPS: " + objWPS010.WPSNumber + " has been submitted for your Approval", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/WPS/ApproveWPSHeader/ViewWPSHeader?headerId=" + objWPS010.HeaderId.ToString());
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Lines Exists For  WPS:" + objWPS010.WPSNumber + " . Please Add Lines  ";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }


            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SubmitWPSHeader(string strHeaderIds)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            string WPSNumberNotSubmit = string.Empty;
            string WPSNumberSubmit = string.Empty;
            string WPSNumberNotDraft = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(strHeaderIds))
                {
                    int[] headerIds = Array.ConvertAll(strHeaderIds.Split(','), i => Convert.ToInt32(i));
                    foreach (var headerId in headerIds)
                    {
                        var objwps010 = db.WPS010.Where(i => i.HeaderId == headerId).FirstOrDefault();
                        if (objwps010.Status == clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue() ||
                            objwps010.Status == clsImplementationEnum.QualityIdHeaderStatus.Returned.GetStringValue())
                        {
                            var objStatus = GetRequiredFieldsForJointType(headerId);
                            var objValidate = ((clsHelper.ResponseMsg)objStatus.Data);
                            if (objValidate.Key)
                            {
                                var objSendForApproval = SendForApproval(headerId);
                                var objSendForApprovalStatus = ((clsHelper.ResponseMsg)objSendForApproval.Data);
                                if (objSendForApprovalStatus.Key)
                                {
                                    Manager.ConcatString(ref WPSNumberSubmit, objwps010.WPSNumber);
                                }
                            }
                            else
                            {
                                Manager.ConcatString(ref WPSNumberNotSubmit, objwps010.WPSNumber);
                            }
                        }
                        else
                        {
                            Manager.ConcatString(ref WPSNumberNotDraft, objwps010.WPSNumber);
                        }
                    }

                    if (WPSNumberSubmit.Length > 0)
                    {
                        objResponseMsg.Key = true;
                        if (WPSNumberSubmit.Length > 0)
                        {
                            objResponseMsg.Value += WPSNumberSubmit + " Submitted Successfully.";
                        }
                        if (WPSNumberNotSubmit.Length > 0 && (!string.IsNullOrEmpty(objResponseMsg.Value) ? objResponseMsg.Value : "").Length > 0)
                        {
                            objResponseMsg.Value += "Please Fill All Mandatory For WPS Number " + WPSNumberNotSubmit + ".";
                        }
                        if (WPSNumberNotDraft.Length > 0 && (!string.IsNullOrEmpty(objResponseMsg.Value) ? objResponseMsg.Value : "").Length > 0)
                        {
                            objResponseMsg.Value += WPSNumberNotDraft + " Not In Draft/Return Status.";
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.IsInformation = true;
                        if (WPSNumberNotSubmit.Length > 0)
                        {
                            objResponseMsg.Value += "Please Fill All Mandatory For WPS Number " + WPSNumberNotSubmit + ".";
                        }
                        if (WPSNumberNotDraft.Length > 0)
                        {
                            objResponseMsg.Value += WPSNumberNotDraft + " Not In Draft Status.";
                        }
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please Select Atleast One WPS Number For Send For Approval.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Copy WPS Number
        public ActionResult CopyWPSNumberPartial(string location, int HeaderId)
        {
            //var WPSNo = (from a in db.WPS010
            //             where a.HeaderId == HeaderId
            //             select a.WPSNumber
            //           ).FirstOrDefault();
            //var lstWPSNo = db.WPS004.Where(i => i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && i.WPSNumber != WPSNo).ToList();
            var lstWPSNo = (from w4 in db.WPS004
                            join w10 in db.WPS010 on new { w4.WPSNumber, w4.Location } equals new { w10.WPSNumber, w10.Location } into w410
                            from p in w410.DefaultIfEmpty()
                                ///where w4.Location.ToLower() == location.ToLower()
                            select new { WPSNumber = w4.WPSNumber, Location = w4.Location, HeaderId = ((w4.WPSNumber == p.WPSNumber) ? p.HeaderId : 0) }
                          );



            ViewBag.CategoryData = lstWPSNo.Where(i => i.HeaderId == 0).Select(i => new CategoryData { Value = i.WPSNumber, CategoryDescription = i.WPSNumber + " : " + i.Location }).ToList();

            return PartialView("_CopyWPSNumberPartial");
        }

        [HttpPost]
        public ActionResult CopyToDestination(int headerId, string destWPSNo, string Location)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            WPS010 objSrcWPS010 = new WPS010();
            WPS010 objDestWPS010 = new WPS010();

            try
            {
                string currentUser = objClsLoginInfo.UserName;

                var currentLoc = (from a in db.COM003
                                  join b in db.COM002 on a.t_loca equals b.t_dimx
                                  where b.t_dtyp == 1 && a.t_actv == 1
                                  && a.t_psno.Equals(currentUser, StringComparison.OrdinalIgnoreCase)
                                  select b.t_dimx).FirstOrDefault().ToString();

                var destLoc = (from a in db.WPS004
                               where a.WPSNumber == destWPSNo
                               select a.Location).FirstOrDefault().ToString();
                if (headerId > 0)
                {
                    objSrcWPS010 = db.WPS010.Where(i => i.HeaderId == headerId).FirstOrDefault();
                }

                if (!string.IsNullOrEmpty(destWPSNo))
                {
                    objDestWPS010 = db.WPS010.Where(i => i.WPSNumber.Equals(destWPSNo, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(currentLoc, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                    if (objDestWPS010 != null)
                    {
                        if ((string.Equals(objDestWPS010.Status, clsImplementationEnum.WPSHeaderStatus.Draft.GetStringValue()) || string.Equals(objDestWPS010.Status, clsImplementationEnum.WPSHeaderStatus.Returned.GetStringValue())) && objDestWPS010.WPSRevNo == 0)
                        {
                            objResponseMsg = InsertOrUpdateWPSNumber(objSrcWPS010, objDestWPS010, true);
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "You cannot copy to " + destWPSNo + " as it is in " + objDestWPS010.Status + " Staus and WPSRevNo > 0";
                        }
                    }
                    else
                    {

                        objDestWPS010 = new WPS010();
                        //objDestWPS010.Location = destLoc;
                        objDestWPS010.Location = Location;
                        objDestWPS010.WPSNumber = destWPSNo;
                        objDestWPS010.WPSRevNo = 0;
                        objDestWPS010.Status = clsImplementationEnum.WPSHeaderStatus.Draft.GetStringValue();
                        objDestWPS010.CreatedBy = objClsLoginInfo.UserName;
                        objDestWPS010.CreatedOn = DateTime.Now;
                        objDestWPS010.ApprovedBy = null;
                        objDestWPS010.ApprovedOn = null;
                        db.WPS010.Add(objDestWPS010);
                        db.SaveChanges();
                        objResponseMsg = InsertOrUpdateWPSNumber(objSrcWPS010, objDestWPS010, false);
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponseMsg InsertOrUpdateWPSNumber(WPS010 srcWPS010, WPS010 destWPS010, bool isUpdate)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var columnNames = (from t in typeof(WPS010).GetProperties()
                                   where !string.Equals(t.Name, "HeaderId", StringComparison.OrdinalIgnoreCase)
                                   && !string.Equals(t.Name, "Location", StringComparison.OrdinalIgnoreCase)
                                   && !string.Equals(t.Name, "WPSNumber", StringComparison.OrdinalIgnoreCase)
                                   && !string.Equals(t.Name, "WPSRevNo", StringComparison.OrdinalIgnoreCase)
                                   && !string.Equals(t.Name, "Status", StringComparison.OrdinalIgnoreCase)
                                   && !string.Equals(t.Name, "CreatedBy", StringComparison.OrdinalIgnoreCase)
                                   && !string.Equals(t.Name, "CreatedOn", StringComparison.OrdinalIgnoreCase)
                                   && !string.Equals(t.Name, "SubmittedBy", StringComparison.OrdinalIgnoreCase)
                                   && !string.Equals(t.Name, "SubmittedOn", StringComparison.OrdinalIgnoreCase)
                                   && !string.Equals(t.Name, "ApprovedBy", StringComparison.OrdinalIgnoreCase)
                                   && !string.Equals(t.Name, "ApprovedOn", StringComparison.OrdinalIgnoreCase)
                                   && !string.Equals(t.Name, "WPS011", StringComparison.OrdinalIgnoreCase)
                                   select t.Name).ToList();

                if (isUpdate)
                {
                    columnNames = columnNames.Where(i => !string.Equals(i, "createdby", StringComparison.OrdinalIgnoreCase) && !string.Equals(i, "createdon", StringComparison.OrdinalIgnoreCase)).ToList();
                }
                else
                {
                    columnNames = columnNames.Where(i => !string.Equals(i, "editedby", StringComparison.OrdinalIgnoreCase) && !string.Equals(i, "editedon", StringComparison.OrdinalIgnoreCase)).ToList();
                }

                var lstSrcWPSNoLines = db.WPS011.Where(i => i.HeaderId == srcWPS010.HeaderId).ToList();
                if (lstSrcWPSNoLines.Any())
                {
                    var cntSrcLines = db.WPS011.Where(i => i.HeaderId == srcWPS010.HeaderId).ToList().Count();
                    var cntDestLines = db.WPS011.Where(i => i.HeaderId == destWPS010.HeaderId).ToList().Count();
                    if (cntDestLines + cntSrcLines <= 8)
                    {
                        foreach (var item in columnNames)
                        {
                            string strSQLQuery = string.Format("select convert(nvarchar(max) ,{0}) from WPS010 where HeaderId=" + srcWPS010.HeaderId, item);
                            var colvalue = db.Database.SqlQuery<string>(strSQLQuery).FirstOrDefault();
                            db.SP_WPS_UPDATE_COLUMNS(destWPS010.HeaderId, item, colvalue);
                        }

                        db.WPS011.AddRange(lstSrcWPSNoLines.Select(i => new WPS011
                        {
                            HeaderId = destWPS010.HeaderId,
                            Location = destWPS010.Location,
                            WPSNumber = destWPS010.WPSNumber,
                            WPSRevNo = destWPS010.WPSRevNo,
                            WeldingProcess = i.WeldingProcess,
                            WeldLayer = i.WeldLayer,
                            AWSClass = i.AWSClass,
                            FillerMetalSize = i.FillerMetalSize,
                            ANumber = i.ANumber,
                            SupplementalFiller = i.SupplementalFiller,
                            FluxType = i.FluxType,
                            FillerMetalTradeName = i.FillerMetalTradeName,
                            ReCrushedSlag = i.ReCrushedSlag,
                            SFANo = i.SFANo,
                            FNumber = i.FNumber,
                            FillerMetalProductForm = i.FillerMetalProductForm,
                            ElectrodeFluxClass = i.ElectrodeFluxClass,
                            FluxTradeName = i.FluxTradeName,
                            ConsumableInsert = i.ConsumableInsert,
                            Other = i.Other,
                            CurrentAMP = i.CurrentAMP,
                            TravelSpeed = i.TravelSpeed,
                            CurrentType = i.CurrentType,
                            CurrentPolarity = i.CurrentPolarity,
                            Voltage = i.Voltage,
                            BeadLength = i.BeadLength,
                            HeatInput = i.HeatInput,
                            WireFeedSpeed = i.WireFeedSpeed,
                            CreatedBy = objClsLoginInfo.UserName,
                            CreatedOn = DateTime.Now
                        }));

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = string.Format("Data Copied to WPS Number :{0} Successfully", destWPS010.WPSNumber);
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = string.Format("WPS :{0} is already having {1} line(s).Cannot add {2} line(s) as line limit is 8 only", destWPS010.WPSNumber, cntDestLines, cntSrcLines);
                    }
                }


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return objResponseMsg;
        }
        #endregion

        #region WPS Header History
        public ActionResult GetHistoryDetails(int? headerId)
        {
            WPS010 objWPS010 = db.WPS010.Where(i => i.HeaderId == headerId).FirstOrDefault();
            return PartialView("_GetHistoryDetailsPartial", objWPS010);
        }

        public ActionResult ViewHistory(int id)
        {
            WPS010_Log objWPS010 = new WPS010_Log();
            if (id > 0)
                objWPS010 = db.WPS010_Log.Where(i => i.Id == id).FirstOrDefault();
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("WPS010_log,WPS011_log");
            return View(objWPS010);
        }

        [HttpPost]
        public ActionResult LoadWPSheaderHistoryPartial(int id, string partialName, string jointType)
        {
            WPS010_Log objWPS010 = new WPS010_Log();
            string filepath = string.Empty;
            var partialname = "_WPSHeader" + partialName;

            var location = (from a in db.COM003
                            join b in db.COM002 on a.t_loca equals b.t_dimx
                            where b.t_dtyp == 1 && a.t_psno.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase) && a.t_actv == 1
                            select b.t_dimx + " - " + b.t_desc).FirstOrDefault();
            ViewBag.Location = location;

            if (id > 0)
            {
                objWPS010 = db.WPS010_Log.Where(i => i.Id == id).FirstOrDefault();
            }

            filepath += "~/Areas/WPS/Views/MaintainWPSHeader/";
            NDEModels objNDEModels = new NDEModels();
            int key1 = 0, key2 = 0;
            if (!string.IsNullOrEmpty(objWPS010.WPSNumber))
            {
                key1 = Convert.ToInt32(objWPS010.WPSNumber.Split('-')[0]);
                key2 = Convert.ToInt32(objWPS010.WPSNumber.Split('-')[1]);
                var wpsKey = db.WPS003.Where(i => i.Key1 == key1 && i.Key2 == key2).FirstOrDefault();

                objWPS010.ParentMaterial1 = wpsKey.Parentmetal1;
                objWPS010.ParentMaterial2 = wpsKey.Parentmetal2;
            }

            if (!string.IsNullOrEmpty(objWPS010.WeldingProcess1))
            {
                //ViewBag.WeldingProcess1 = objNDEModels.GetCategory(objWPS010.WeldingProcess1).CategoryDescription;
                ViewBag.WeldingProcess1 = objNDEModels.GetCategory(objWPS010.WeldingProcess1).Code;
            }
            if (!string.IsNullOrEmpty(objWPS010.WeldingProcess2))
            {
                //ViewBag.WeldingProcess2 = objNDEModels.GetCategory(objWPS010.WeldingProcess2).CategoryDescription;
                ViewBag.WeldingProcess2 = objNDEModels.GetCategory(objWPS010.WeldingProcess2).Code;
            }
            if (!string.IsNullOrEmpty(objWPS010.WeldingProcess3))
            {
                //ViewBag.WeldingProcess3 = objNDEModels.GetCategory(objWPS010.WeldingProcess3).CategoryDescription;
                ViewBag.WeldingProcess3 = objNDEModels.GetCategory(objWPS010.WeldingProcess3).Code;
            }
            if (!string.IsNullOrEmpty(objWPS010.WeldingProcess4))
            {
                //ViewBag.WeldingProcess4 = objNDEModels.GetCategory(objWPS010.WeldingProcess4).CategoryDescription;
                ViewBag.WeldingProcess4 = objNDEModels.GetCategory(objWPS010.WeldingProcess4).Code;
            }

            #region GrooveF1
            if (!string.IsNullOrEmpty(objWPS010.PositionofGrooveTTSJointWeldingProcess1)) { ViewBag.PositionofGrooveTTSJointWeldingProcess1 = GetCategoryDescription(objWPS010.PositionofGrooveTTSJointWeldingProcess1).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.PositionofGrooveTTSJointWeldingProcess2)) { ViewBag.PositionofGrooveTTSJointWeldingProcess2 = GetCategoryDescription(objWPS010.PositionofGrooveTTSJointWeldingProcess2).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.PositionofGrooveTTSJointWeldingProcess3)) { ViewBag.PositionofGrooveTTSJointWeldingProcess3 = GetCategoryDescription(objWPS010.PositionofGrooveTTSJointWeldingProcess3).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.PositionofGrooveTTSJointWeldingProcess4)) { ViewBag.PositionofGrooveTTSJointWeldingProcess4 = GetCategoryDescription(objWPS010.PositionofGrooveTTSJointWeldingProcess4).CategoryDescription; }

            if (!string.IsNullOrEmpty(objWPS010.WeldingProgressionWeldingProcess1)) { ViewBag.WeldingProgressionWeldingProcess1 = GetCategoryDescription(objWPS010.WeldingProgressionWeldingProcess1).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.WeldingProgressionWeldingProcess2)) { ViewBag.WeldingProgressionWeldingProcess2 = GetCategoryDescription(objWPS010.WeldingProgressionWeldingProcess2).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.WeldingProgressionWeldingProcess3)) { ViewBag.WeldingProgressionWeldingProcess3 = GetCategoryDescription(objWPS010.WeldingProgressionWeldingProcess3).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.WeldingProgressionWeldingProcess4)) { ViewBag.WeldingProgressionWeldingProcess4 = GetCategoryDescription(objWPS010.WeldingProgressionWeldingProcess4).CategoryDescription; }

            if (!string.IsNullOrEmpty(objWPS010.WeldingProgressionSubsequentWeldingProcess1)) { ViewBag.WeldingProgressionSubsequentWeldingProcess1 = GetCategoryDescription(objWPS010.WeldingProgressionSubsequentWeldingProcess1).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.WeldingProgressionSubsequentWeldingProcess2)) { ViewBag.WeldingProgressionSubsequentWeldingProcess2 = GetCategoryDescription(objWPS010.WeldingProgressionSubsequentWeldingProcess2).CategoryDescription; }

            if (!string.IsNullOrEmpty(objWPS010.PositionoffilletWeldingProcess1)) { ViewBag.PositionoffilletWeldingProcess1 = GetCategoryDescription(objWPS010.PositionoffilletWeldingProcess1).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.PositionoffilletWeldingProcess2)) { ViewBag.PositionoffilletWeldingProcess2 = GetCategoryDescription(objWPS010.PositionoffilletWeldingProcess2).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.PositionoffilletWeldingProcess3)) { ViewBag.PositionoffilletWeldingProcess3 = GetCategoryDescription(objWPS010.PositionoffilletWeldingProcess3).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.PositionoffilletWeldingProcess4)) { ViewBag.PositionoffilletWeldingProcess4 = GetCategoryDescription(objWPS010.PositionoffilletWeldingProcess4).CategoryDescription; }
            #endregion
            #region GrooveF2
            if (!string.IsNullOrEmpty(objWPS010.TypeofPWHT)) { ViewBag.TypeofPWHT = GetCategoryDescription(objWPS010.TypeofPWHT).CategoryDescription; }
            #endregion
            #region GrooveF3
            if (!string.IsNullOrEmpty(objWPS010.ShieldingGasGasType1)) { ViewBag.ShieldingGasGasType1 = GetCategoryDescription(objWPS010.ShieldingGasGasType1).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.ShieldingGasGasType2)) { ViewBag.ShieldingGasGasType2 = GetCategoryDescription(objWPS010.ShieldingGasGasType2).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.BackingGasGasType1)) { ViewBag.BackingGasGasType1 = GetCategoryDescription(objWPS010.BackingGasGasType1).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.BackingGasGasType2)) { ViewBag.BackingGasGasType2 = GetCategoryDescription(objWPS010.BackingGasGasType2).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.TrailingGasGasType1)) { ViewBag.TrailingGasGasType1 = GetCategoryDescription(objWPS010.TrailingGasGasType1).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.TrailingGasGasType2)) { ViewBag.TrailingGasGasType2 = GetCategoryDescription(objWPS010.TrailingGasGasType2).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.Tungsten)) { ViewBag.TungstenSelected = GetCategoryDescription(objWPS010.Tungsten).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.Modeofmetaltransfer)) { ViewBag.ModeofmetaltransferSelected = GetCategoryDescription(objWPS010.Modeofmetaltransfer).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.Pulsing)) { ViewBag.PulsingSelected = GetCategoryDescription(objWPS010.Pulsing).CategoryDescription; }
            #endregion

            #region GrooveF4
            if (!string.IsNullOrEmpty(objWPS010.MultiSingleElectrode1)) { ViewBag.MultiSingleElectrode1 = GetCategoryDescription(objWPS010.MultiSingleElectrode1).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.MultiSingleElectrode2)) { ViewBag.MultiSingleElectrode2 = GetCategoryDescription(objWPS010.MultiSingleElectrode2).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.MultiSingleElectrode3)) { ViewBag.MultiSingleElectrode3 = GetCategoryDescription(objWPS010.MultiSingleElectrode3).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.MultiSingleElectrode4)) { ViewBag.MultiSingleElectrode4 = GetCategoryDescription(objWPS010.MultiSingleElectrode4).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.MultipleSinglepassperside1)) { ViewBag.MultipleSinglepassperside1 = GetCategoryDescription(objWPS010.MultipleSinglepassperside1).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.MultipleSinglepassperside2)) { ViewBag.MultipleSinglepassperside2 = GetCategoryDescription(objWPS010.MultipleSinglepassperside2).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.MultipleSinglepassperside3)) { ViewBag.MultipleSinglepassperside3 = GetCategoryDescription(objWPS010.MultipleSinglepassperside3).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.MultipleSinglepassperside4)) { ViewBag.MultipleSinglepassperside4 = GetCategoryDescription(objWPS010.MultipleSinglepassperside4).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.SingleMultipleLayer1)) { ViewBag.SingleMultipleLayer1 = GetCategoryDescription(objWPS010.SingleMultipleLayer1).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.SingleMultipleLayer2)) { ViewBag.SingleMultipleLayer2 = GetCategoryDescription(objWPS010.SingleMultipleLayer2).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.SingleMultipleLayer3)) { ViewBag.SingleMultipleLayer3 = GetCategoryDescription(objWPS010.SingleMultipleLayer3).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.SingleMultipleLayer4)) { ViewBag.SingleMultipleLayer4 = GetCategoryDescription(objWPS010.SingleMultipleLayer4).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.StringWeaveBead1)) { ViewBag.StringWeaveBead1 = GetCategoryDescription(objWPS010.StringWeaveBead1).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.StringWeaveBead2)) { ViewBag.StringWeaveBead2 = GetCategoryDescription(objWPS010.StringWeaveBead2).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.StringWeaveBead3)) { ViewBag.StringWeaveBead3 = GetCategoryDescription(objWPS010.StringWeaveBead3).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.StringWeaveBead4)) { ViewBag.StringWeaveBead4 = GetCategoryDescription(objWPS010.StringWeaveBead4).CategoryDescription; }
            #endregion

            #region GrooveF5
            if (!string.IsNullOrEmpty(objWPS010.Oscillation1)) { ViewBag.Oscillation1 = GetCategoryDescription(objWPS010.Oscillation1).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.Oscillation2)) { ViewBag.Oscillation2 = GetCategoryDescription(objWPS010.Oscillation2).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.Oscillation3)) { ViewBag.Oscillation3 = GetCategoryDescription(objWPS010.Oscillation3).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.Oscillation4)) { ViewBag.Oscillation4 = GetCategoryDescription(objWPS010.Oscillation4).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWPS010.Peening)) { ViewBag.PeeningSelected = GetCategoryDescription(objWPS010.Peening).CategoryDescription; }
            #endregion

            if (string.Equals(jointType, "Groove", StringComparison.OrdinalIgnoreCase))
            {
                filepath += "GrooveFilletHistory/_WPSHeader" + partialName + "Partial.cshtml";
            }
            else if (string.Equals(jointType, "Buttering", StringComparison.OrdinalIgnoreCase))
            {
                filepath += "ButteringHistory/_WPSHeader" + partialName + "Partial.cshtml";
            }
            else if (string.Equals(jointType, "TTS", StringComparison.OrdinalIgnoreCase))
            {
                filepath += "TTSHistory/_WPSHeader" + partialName + "Partial.cshtml";
            }
            else if (string.Equals(jointType, "OverLay", StringComparison.OrdinalIgnoreCase))
            {
                if (!string.IsNullOrEmpty(objWPS010.PositionOverlayBarrierWeldingProcess1)) { ViewBag.PositionOverlayBarrierWeldingProcess1 = GetCategoryDescription(objWPS010.PositionOverlayBarrierWeldingProcess1).CategoryDescription; }
                if (!string.IsNullOrEmpty(objWPS010.PositionOverlayBarrierWeldingProcess2)) { ViewBag.PositionOverlayBarrierWeldingProcess2 = GetCategoryDescription(objWPS010.PositionOverlayBarrierWeldingProcess2).CategoryDescription; }
                if (!string.IsNullOrEmpty(objWPS010.PositionOverlaySubsequentWeldingProcess1)) { ViewBag.PositionOverlaySubsequentWeldingProcess1 = GetCategoryDescription(objWPS010.PositionOverlaySubsequentWeldingProcess1).CategoryDescription; }
                if (!string.IsNullOrEmpty(objWPS010.PositionOverlaySubsequentWeldingProcess2)) { ViewBag.PositionOverlaySubsequentWeldingProcess2 = GetCategoryDescription(objWPS010.PositionOverlaySubsequentWeldingProcess2).CategoryDescription; }

                filepath += "OverlayHardfacingHistory/_WPSHeader" + partialName + "Partial.cshtml";
            }
            else
            {
                if (!string.IsNullOrEmpty(objWPS010.WeldingType1))
                {
                    ViewBag.WeldingType1 = GetCategoryDescription(objWPS010.WeldingType1).CategoryDescription;
                }
                if (!string.IsNullOrEmpty(objWPS010.WeldingType2))
                {
                    ViewBag.WeldingType2 = GetCategoryDescription(objWPS010.WeldingType2).CategoryDescription;
                }
                if (!string.IsNullOrEmpty(objWPS010.WeldingType3))
                {
                    ViewBag.WeldingType3 = GetCategoryDescription(objWPS010.WeldingType3).CategoryDescription;
                }
                if (!string.IsNullOrEmpty(objWPS010.WeldingType4))
                {
                    ViewBag.WeldingType4 = GetCategoryDescription(objWPS010.WeldingType4).CategoryDescription;
                }
                filepath += "_WPSHeaderHistory.cshtml";
            }

            return PartialView(filepath, objWPS010);
        }

        [HttpPost]
        public JsonResult LoadWPSHeaderHistoryDetailData(JQueryDataTableParamModel param, int headerId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";

                //whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qcp1.BU", "qcp1.Location");
                var lstLoc = string.Join(",", db.ATH001.Where(x => x.Employee == objClsLoginInfo.UserName).Select(x => x.Location).Distinct().ToList());
                whereCondition += Manager.MakeStringInCondition("wps10.Location", lstLoc);

                whereCondition += "and wps10.HeaderId = " + headerId;


                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or Location like '%" + param.sSearch + "%' or wps10.WPSNumber like '%" + param.sSearch + "%' or WPSRevNo like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%' or wps10.CreatedBy like '%" + param.sSearch + "%')";
                }

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstResult = db.SP_WPS_WPSHEADERHISTORY(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstResult.Select(i => i.TotalCount).FirstOrDefault();

                var data = (from uc in lstResult
                            select new[]
                           {
                           Convert.ToString(uc.Id),
                           Convert.ToString(uc.WPSNumber),
                           Convert.ToString("R" + uc.WPSRevNo),
                           Convert.ToString(uc.Status),
                           uc.CreatedBy,
                           uc.EditedBy,
                           Convert.ToString(uc.Location),
                           Convert.ToDateTime( uc.CreatedOn).ToString("dd/MM/yyyy"),
                           Convert.ToDateTime( uc.EditedOn).ToString("dd/MM/yyyy"),
                           uc.Location
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadWPSLinesHistoryData(JQueryDataTableParamModel param, int refId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                var totalRecords = new ObjectParameter("totalRecords", typeof(int));
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                //string SortingFields = param.iSortingCols;
                //readonly= "readonly"

                Func<QCP003_Log, string> orderingFunction = (tsk =>
                                        sortColumnIndex == 2 && isLocationSortable ? Convert.ToString(tsk.Location) : "");
                var sortDirection = Request["sSortDir_0"]; // asc or desc
                string whereCondition = "1=1";
                whereCondition += " and wps11.RefId = " + refId;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or Location like '%" + param.sSearch + "%' or wps11.WPSNumber like '%" + param.sSearch + "%' or WPSRevNo like '%" + param.sSearch + "%' or wps11.CreatedBy like '%" + param.sSearch + "%')";
                }
                var lstlines = db.SP_WPS_GETWPSLINESHISTORY(StartIndex, EndIndex, "", whereCondition).ToList();
                int totalRecord = Convert.ToInt32(lstlines.Select(x => x.TotalCount).FirstOrDefault());

                var res = from a in lstlines
                          select new[] {
                        Convert.ToString(a.ROW_NO),
                        Convert.ToString(a.HeaderId),
                        Convert.ToString(a.LineId),
                        a.WeldingProcess,
                        a.WeldLayer,
                        a.AWSClass,
                        a.FillerMetalSize,
                        a.ANumber,
                        a.SupplementalFiller,
                        a.FluxType,
                        //GetActionForLine(a.HeaderId,a.LineId),//"Action",
                        a.FillerMetalTradeName,
                        a.ReCrushedSlag,
                        a.SFANo,
                        a.FNumber,
                        a.FillerMetalProductForm,
                        a.ElectrodeFluxClass,
                        a.FluxTradeName,
                        a.ConsumableInsert,
                        a.Other,
                        a.CurrentAMP,
                        a.TravelSpeed,
                        a.CurrentType,
                        a.CurrentPolarity,
                        a.Voltage,
                        a.BeadLength,
                        a.HeatInput,
                        a.WireFeedSpeed

                    };
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = totalRecord,
                    iTotalDisplayRecords = totalRecord,
                    aaData = res
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region WPS Common Functions
        [HttpPost]
        public ActionResult GetWPSNo(string location, string WPSNo)
        {
            ModelWPS objModelWPS = new ModelWPS();

            var lstWPSNo = db.WPS004.Where(i => i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && i.WPSNumber.Contains(WPSNo)).ToList();
            //if (!string.IsNullOrEmpty(WPSNo))
            //{
            //    var lstWPSHeader = db.WPS010.Where(i => i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && i.WPSNumber.Equals(WPSNo, StringComparison.OrdinalIgnoreCase)).ToList();

            //    if (lstWPSHeader.Any())
            //    {
            //        objModelWPS.isWPSDocGenerated = true;
            //    }
            //}
            objModelWPS.CategoryData = lstWPSNo.Select(i => new CategoryData { Value = i.WPSNumber, CategoryDescription = i.WPSNumber }).ToList();

            return Json(objModelWPS, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult isWPSNoExist(string location, string WPSNo, int headerId)
        {
            ModelWPS objModelWPS = new ModelWPS();
            NDEModels objNDEModels = new NDEModels();
            if (!string.IsNullOrEmpty(WPSNo))
            {
                var lstWPSHeader = db.WPS010.Where(i => i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && i.WPSNumber.ToLower().Contains(WPSNo.ToLower()) && i.HeaderId != headerId).ToList();

                if (lstWPSHeader.Any())
                {
                    objModelWPS.isWPSDocGenerated = true;
                    objModelWPS.HeaderId = lstWPSHeader[0].HeaderId;
                }
            }
            int key1 = Convert.ToInt32(WPSNo.Split('-')[0]), key2 = Convert.ToInt32(WPSNo.Split('-')[1]);

            var objWPS003 = db.WPS003.Where(i => i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && i.Key1 == key1 && i.Key2 == key2).FirstOrDefault();
            if (objWPS003 != null)
            {
                objModelWPS.WeldingProcess1 = objNDEModels.GetCategory(objWPS003.WeldingProcess1).Code;// objWPS003.WeldingProcess1;
                objModelWPS.WeldingProcess2 = objNDEModels.GetCategory(objWPS003.WeldingProcess2).Code;// objWPS003.WeldingProcess2;
                objModelWPS.WeldingProcess3 = objNDEModels.GetCategory(objWPS003.WeldingProcess3).Code;// objWPS003.WeldingProcess3;
                objModelWPS.WeldingProcess4 = objNDEModels.GetCategory(objWPS003.WeldingProcess4).Code;// objWPS003.WeldingProcess4;
                                                                                                       //if (string.Equals(Convert.ToString(objWPS003.JointType), "Overlay/HardFac", StringComparison.OrdinalIgnoreCase))
                                                                                                       //{
                                                                                                       //    objModelWPS.JointType = "Overlay/HardFacing";
                                                                                                       //}
                                                                                                       //else
                                                                                                       //{
                objModelWPS.JointType = !string.IsNullOrEmpty(objWPS003.JointType) ? objWPS003.JointType.Trim() : "";
                //}

            }
            else
            {
                objModelWPS.WeldingProcess1 = string.Empty;
                objModelWPS.WeldingProcess2 = string.Empty;
                objModelWPS.WeldingProcess3 = string.Empty;
                objModelWPS.WeldingProcess4 = string.Empty;
                objModelWPS.JointType = string.Empty;
            }
            return Json(objModelWPS, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRequiredFieldsForJointType(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            WPS010 objWPS010 = new WPS010();
            string strRequiredField = string.Empty;
            List<string> RequiredField = new List<string>();
            List<string> lstComReqField = new List<string>();
            List<string> lstRequiredField = new List<string>();

            lstComReqField.AddRange(new List<string> { "ParentMaterial1", "TypeofPWHT", "Peening" });

            if (headerId > 0)
            {
                objWPS010 = db.WPS010.Where(i => i.HeaderId == headerId).FirstOrDefault();

                if (!string.IsNullOrEmpty(objWPS010.WeldingProcess1))
                {
                    lstComReqField.AddRange(new List<string> { "WeldingType1,MultiSingleElectrode1", "SingleMultipleLayer1", "StringWeaveBead1", "Oscillation1" });
                }
                if (!string.IsNullOrEmpty(objWPS010.WeldingProcess2))
                {
                    lstComReqField.AddRange(new List<string> { "WeldingType2,MultiSingleElectrode2", "SingleMultipleLayer2", "StringWeaveBead2", "Oscillation2" });
                }
                if (!string.IsNullOrEmpty(objWPS010.WeldingProcess3))
                {
                    lstComReqField.AddRange(new List<string> { "WeldingType3,MultiSingleElectrode3", "SingleMultipleLayer3", "StringWeaveBead3", "Oscillation3" });
                }
                if (!string.IsNullOrEmpty(objWPS010.WeldingProcess4))
                {
                    lstComReqField.AddRange(new List<string> { "WeldingType4,MultiSingleElectrode4", "SingleMultipleLayer4", "StringWeaveBead4", "Oscillation4" });
                }

                if (!string.IsNullOrEmpty(objWPS010.Jointtype))
                {
                    if (string.Equals(objWPS010.Jointtype, "groove/fillet", StringComparison.OrdinalIgnoreCase))
                    {
                        //lstRequiredField.AddRange(new List<string> {"Groovedesign","Backing","BackingMaterial","ParentMaterial2","PositionofGrooveTTSJointWeldingProcess1","PositionofGrooveTTSJointWeldingProcess2","PositionofGrooveTTSJointWeldingProcess3","PositionofGrooveTTSJointWeldingProcess4",
                        //                                           "WeldingProgressionWeldingProcess1","WeldingProgressionWeldingProcess2","WeldingProgressionWeldingProcess3","WeldingProgressionWeldingProcess4","PositionoffilletWeldingProcess1","PositionoffilletWeldingProcess2","PositionoffilletWeldingProcess3","PositionoffilletWeldingProcess4",
                        //                                           "MultipleSinglepassperside1","MultipleSinglepassperside2","MultipleSinglepassperside3","MultipleSinglepassperside4"});

                        lstRequiredField.AddRange(new List<string> { "Groovedesign", "Backing", "BackingMaterial", "ParentMaterial2", "TypeofPWHT", "Peening" });


                        if (!string.IsNullOrEmpty(objWPS010.WeldingProcess1))
                        {
                            lstRequiredField.AddRange(new List<string> { "PositionofGrooveTTSJointWeldingProcess1", "WeldingProgressionWeldingProcess1", "PositionoffilletWeldingProcess1", "MultipleSinglepassperside1", "Oscillation1" });
                        }
                        if (!string.IsNullOrEmpty(objWPS010.WeldingProcess2))
                        {
                            lstRequiredField.AddRange(new List<string> { "PositionofGrooveTTSJointWeldingProcess2", "WeldingProgressionWeldingProcess2", "PositionoffilletWeldingProcess2", "MultipleSinglepassperside2", "Oscillation2" });
                        }
                        if (!string.IsNullOrEmpty(objWPS010.WeldingProcess3))
                        {
                            lstRequiredField.AddRange(new List<string> { "PositionofGrooveTTSJointWeldingProcess3", "WeldingProgressionWeldingProcess3", "PositionoffilletWeldingProcess3", "MultipleSinglepassperside3", "Oscillation3" });
                        }
                        if (!string.IsNullOrEmpty(objWPS010.WeldingProcess4))
                        {
                            lstRequiredField.AddRange(new List<string> { "PositionofGrooveTTSJointWeldingProcess4", "WeldingProgressionWeldingProcess4", "PositionoffilletWeldingProcess4", "MultipleSinglepassperside4", "Oscillation4" });
                        }
                    }
                    //else if (string.Equals(objWPS010.Jointtype, "overlay hardfacing", StringComparison.OrdinalIgnoreCase))
                    //else if (string.Equals(objWPS010.Jointtype, "Overlay/HardFacing", StringComparison.OrdinalIgnoreCase))
                    else if (string.Equals(objWPS010.Jointtype, "Overlay", StringComparison.OrdinalIgnoreCase))
                    {
                        //lstRequiredField.AddRange(new List<string> {"PositionOverlayBarrierWeldingProcess1","PositionOverlayBarrierWeldingProcess2","PositionOverlaySubsequentWeldingProcess1","PositionOverlaySubsequentWeldingProcess2",
                        //                                            "WeldingProgressionWeldingProcess1","WeldingProgressionWeldingProcess2"});

                        if (!string.IsNullOrEmpty(objWPS010.WeldingProcess1))
                        {
                            lstRequiredField.AddRange(new List<string> { "PositionOverlayBarrierWeldingProcess1", "PositionOverlaySubsequentWeldingProcess1", "WeldingProgressionWeldingProcess1", "WeldingProgressionSubsequentWeldingProcess1" });
                        }
                        if (!string.IsNullOrEmpty(objWPS010.WeldingProcess2))
                        {
                            lstRequiredField.AddRange(new List<string> { "PositionOverlayBarrierWeldingProcess2", "PositionOverlaySubsequentWeldingProcess2", "WeldingProgressionWeldingProcess2", "WeldingProgressionSubsequentWeldingProcess2" });
                        }
                    }
                    else if (string.Equals(objWPS010.Jointtype, "T#TS", StringComparison.OrdinalIgnoreCase))
                    {
                        //lstRequiredField.AddRange(new List<string> {"Groovedesign","Backing","BackingMaterial","ParentMaterial2","PositionofGrooveTTSJointWeldingProcess1","PositionofGrooveTTSJointWeldingProcess2","PositionofGrooveTTSJointWeldingProcess3","PositionofGrooveTTSJointWeldingProcess4",
                        //                                           "WeldingProgressionWeldingProcess1","WeldingProgressionWeldingProcess2","WeldingProgressionWeldingProcess3","WeldingProgressionWeldingProcess4","PositionoffilletWeldingProcess1","PositionoffilletWeldingProcess2","PositionoffilletWeldingProcess3","PositionoffilletWeldingProcess4",
                        //                                           "MultipleSinglepassperside1","MultipleSinglepassperside2","MultipleSinglepassperside3","MultipleSinglepassperside4"});

                        lstRequiredField.AddRange(new List<string> { "Groovedesign", "Backing", "BackingMaterial", "ParentMaterial2", "TubesheetThk", "TubeThickness", "Pitch", "TubeExpansionPriorToWelding", "TubeOD", "Ligament", "TubeProjection", "Other7", "TypeofPWHT", "Peening" });

                        if (!string.IsNullOrEmpty(objWPS010.WeldingProcess1))
                        {
                            lstRequiredField.AddRange(new List<string> { "PositionofGrooveTTSJointWeldingProcess1", "WeldingProgressionWeldingProcess1", "PositionoffilletWeldingProcess1", "MultipleSinglepassperside1", "Oscillation1" });
                        }
                        if (!string.IsNullOrEmpty(objWPS010.WeldingProcess2))
                        {
                            lstRequiredField.AddRange(new List<string> { "PositionofGrooveTTSJointWeldingProcess2", "WeldingProgressionWeldingProcess2", "PositionoffilletWeldingProcess2", "MultipleSinglepassperside2", "Oscillation2" });
                        }
                        if (!string.IsNullOrEmpty(objWPS010.WeldingProcess3))
                        {
                            lstRequiredField.AddRange(new List<string> { "PositionofGrooveTTSJointWeldingProcess3", "WeldingProgressionWeldingProcess3", "PositionoffilletWeldingProcess3", "MultipleSinglepassperside3", "Oscillation3" });
                        }
                        if (!string.IsNullOrEmpty(objWPS010.WeldingProcess4))
                        {
                            lstRequiredField.AddRange(new List<string> { "PositionofGrooveTTSJointWeldingProcess4", "WeldingProgressionWeldingProcess4", "PositionoffilletWeldingProcess4", "MultipleSinglepassperside4", "Oscillation4" });
                        }
                    }

                    var lstAllRequiredFields = lstComReqField.Union(lstRequiredField).Distinct();
                    strRequiredField = string.Join(",", lstAllRequiredFields);

                    //string strResult = string.Empty;
                    var strResult = new ObjectParameter("ReqColumnNames", typeof(string));
                    db.SP_WPS_RequiredField(headerId, strRequiredField, strResult).ToString();

                    if (!string.IsNullOrEmpty(strResult.Value.ToString()))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = string.Format("{0} are Required Fields Need To Be Filled", strResult.Value.ToString());
                    }
                    else
                    {
                        objResponseMsg.Key = true;
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Joint Type Is Mandatory";
                }
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetAWSClassDetail(string id, string location)
        {
            WPS002 objWPS002 = new WPS002();
            ModelWPSLines objModelWPSLines = new ModelWPSLines();

            if (!string.IsNullOrEmpty(id) && !string.IsNullOrEmpty(location))
            {
                //string AWSClass = awsClass.Split('|')[0];
                //string FNo= awsClass.Split('|')[1];
                //string ANo = awsClass.Split('|')[2];
                //string SFANo = awsClass.Split('|')[3];
                int AWSClassId = Convert.ToInt32(id);
                objWPS002 = db.WPS002.Where(i => i.Id == AWSClassId).FirstOrDefault();
                if (objWPS002 != null)
                {
                    objModelWPSLines.ANo = objWPS002.ANumber;
                    objModelWPSLines.SizeOfFillerMetal = objWPS002.ConsumableSize;
                    objModelWPSLines.SFANo = objWPS002.SFANo;
                    objModelWPSLines.FNo = objWPS002.FNumber;
                    objModelWPSLines.Current = objWPS002.CurrentAMP;
                    objModelWPSLines.TravelSpeed = objWPS002.TravelSpeed;
                    objModelWPSLines.Voltage = objWPS002.Voltage;
                    objModelWPSLines.BeadLength = objWPS002.BeadLength;
                    objModelWPSLines.HeatInput = objWPS002.HeatInput;
                }

                var lstFillerMetalTradeName = db.WPS001.Where(i => i.AWSClass.ToLower().Equals(objWPS002.AWSClass.ToLower()) && i.Location.Equals(location)).Select(i => new CategoryData { Value = i.BrandName, CategoryDescription = i.BrandName }).ToList();
                if (lstFillerMetalTradeName.Any())
                    objModelWPSLines.FillerMetalTradeName = lstFillerMetalTradeName;

                //var lstFluxTradeName = db.WPS001.Where(i => i.AWSClass.ToLower().Equals(objWPS002.AWSClass.ToLower()) && i.Location.Equals(location) && i.Producttype.ToLower().Equals("flux")).Select(i => new CategoryData { Value = i.BrandName, CategoryDescription = i.BrandName }).ToList();
                var lstFluxTradeName = db.WPS001.Where(i => i.Location.Equals(location) && i.Producttype.ToLower().Equals("flux")).Select(i => new CategoryData { Value = i.BrandName, CategoryDescription = i.BrandName }).ToList();
                if (lstFluxTradeName.Any())
                    objModelWPSLines.FluxTradeName = lstFluxTradeName;

            }
            //else
            //{
            //    objModelWPSLines.FillerMetalTradeName = new List<CategoryData>();
            //    objModelWPSLines.FluxTradeName = new List<CategoryData>();
            //}

            return Json(objModelWPSLines, JsonRequestBehavior.AllowGet);
        }

        public string GetActionForLine(int headerId, int lineId)
        {
            string strHtmlAction = string.Empty;
            WPS010 objWPS010 = new WPS010();
            if (headerId > 0)
                objWPS010 = db.WPS010.Where(i => i.HeaderId == headerId).FirstOrDefault();

            if (!string.Equals(objWPS010.Status, clsImplementationEnum.WPSHeaderStatus.SentForApproval.GetStringValue(), StringComparison.OrdinalIgnoreCase) && !string.Equals(objWPS010.Status, clsImplementationEnum.WPSHeaderStatus.Approved.GetStringValue(), StringComparison.OrdinalIgnoreCase))
            {
                strHtmlAction = "<center><i id='btnAddSubLine' name='btnAction style='cursor: pointer;' Title='View Line' class='fa fa-edit iconspace'  onClick='WPSLinesPartial(" + headerId + "," + lineId + ")' ></i><i id='btnDelete' name='btnAction' style='cursor: pointer;' Title='Delete Line' class='fa fa-trash-o iconspace' onclick='DeleteCategory(" + lineId + ")'></i></center>";
            }
            else
            {
                strHtmlAction = "<center><i id='btnAddSubLine' name='btnAction style='cursor: pointer;' Title='View Line' class='fa fa-edit disabledicon' ></i><i id='btnDelete' name='btnAction' style='cursor: pointer;' Title='Delete Line' class='fa fa-trash-o disabledicon'></i></center>";
            }
            //strRes = "<center><a class='btn btn-xs green' href='/CTQ/CompileCTQ/CTQLineDetails?ID=" + Convert.ToString(uc.HeaderId) + "'><i style='margin-left:5px;' class='fa fa-eye'></i></a></center>";
            //var ier = "<center><i id='btnAddSubLine' name='btnAction style='cursor: pointer;'  Title='View Category' class='fa fa-plus'  onClick='WPSLinesPartial('" + headerId + "','" + lineId + "')' ></i></center>";

            return strHtmlAction;
        }

        public List<GLB002> GetSubCatagory(string Key, string strLoc, string BU)
        {
            List<GLB002> lstGLB002 = null;
            if (BU != string.Empty) //Get BU and Location Base Sub Category
            {
                lstGLB002 = (from glb002 in db.GLB002
                             join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                             where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true && strLoc.Equals(glb002.Location) &&
                                   glb002.BU.Equals(BU, StringComparison.OrdinalIgnoreCase)
                             select glb002).ToList();
            }
            else
            {
                lstGLB002 = (from glb002 in db.GLB002
                             join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                             where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true && strLoc.Equals(glb002.Location)
                             select glb002).ToList();
            }
            return lstGLB002;
        }
        public CategoryData GetCategoryDescription(string categoryCode)
        {
            CategoryData objGLB002 = new CategoryData();

            if (IsCategoryExist(categoryCode))
            {

                if (!string.IsNullOrEmpty(categoryCode))
                {
                    objGLB002 = db.GLB002.Where(i => i.Code.Equals(categoryCode, StringComparison.OrdinalIgnoreCase)).Select(i => new CategoryData { Code = i.Code, CategoryDescription = i.Description }).FirstOrDefault();
                }
            }
            else
            {
                objGLB002.CategoryDescription = categoryCode;
            }

            return objGLB002;
        }
        public bool IsCategoryExist(string categoryCode)
        {
            bool categoryExist = false;

            if (db.GLB002.Where(i => i.Code.Equals(categoryCode, StringComparison.OrdinalIgnoreCase)).Any())
                categoryExist = true;

            return categoryExist;
        }

        public IQueryable<CategoryData> GetSubCatagoryNew(string Key, string strLoc, string BU = "", bool OnlyDesciption = true)
        {
            IQueryable<CategoryData> lstGLB002 = null;
            if (BU != string.Empty) //Get BU and Location Base Sub Category
            {
                lstGLB002 = (from glb002 in db.GLB002
                             join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                             where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.BU.Equals(BU, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true && strLoc.Equals(glb002.Location)
                             //select glb002).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + " - " + i.Description }).Distinct();
                             select glb002).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = (OnlyDesciption == true ? i.Description : i.Code + " - " + i.Description) }).Distinct();
            }
            else //Get Location Base Sub Category
            {
                lstGLB002 = (from glb002 in db.GLB002
                             join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                             where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true && strLoc.Equals(glb002.Location)
                             select glb002).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = (OnlyDesciption == true ? i.Description : i.Code + " - " + i.Description) }).Distinct();
            }
            return lstGLB002;
        }

        public static List<string> GetSupplementalDevice()
        {
            List<string> items = GetSupplementalDevice<SupplementalDevice>();
            return items;
        }
        public static List<string> GetSupplementalDevice<T>() where T : struct
        {
            Type t = typeof(T);
            return !t.IsEnum ? null : Enum.GetValues(t).Cast<Enum>().Select(x => x.GetStringValue()).ToList();
        }

        //public string ReplaceNewLine(string oldString)
        //{
        //    string ReplaceString = Regex.Replace(oldString, @"\t|\n|\r", string.Empty);
        //    return ReplaceString;
        //}
        #endregion

        #region Export Grid Data
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    List<SP_WPS_GetWPSHeader_Result> lst = db.SP_WPS_GetWPSHeader(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      WPSNumber = li.WPSNumber,
                                      WPSRevNo = li.WPSRevNo,
                                      Status = li.Status,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      Location = li.Location
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    List<SP_WPS_GETHEADERLINES_Result> lst = db.SP_WPS_GETHEADERLINES(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      WeldingProcess = li.WeldingProcess,
                                      WeldLayer = li.WeldLayer,
                                      AWSClass = li.AWSClass,
                                      FillerMetalSize = li.FillerMetalSize,
                                      ANumber = li.ANumber,
                                      SupplementalFiller = li.SupplementalFiller,
                                      FluxType = li.FluxType,
                                      FillerMetalTradeName = li.FillerMetalTradeName,
                                      ReCrushedSlag = li.ReCrushedSlag,
                                      SFANo = li.SFANo,
                                      FNumber = li.FNumber,
                                      FillerMetalProductForm = li.FillerMetalProductForm,
                                      ElectrodeFluxClass = li.ElectrodeFluxClass,
                                      FluxTradeName = li.FluxTradeName,
                                      ConsumableInsert = li.ConsumableInsert,
                                      Other = li.Other,
                                      CurrentAMP = li.CurrentAMP,
                                      TravelSpeed = li.TravelSpeed,
                                      CurrentType = li.CurrentType,
                                      CurrentPolarity = li.CurrentPolarity,
                                      Voltage = li.Voltage,
                                      BeadLength = li.BeadLength,
                                      HeatInput = li.HeatInput,
                                      WireFeedSpeed = li.WireFeedSpeed
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {
                    List<SP_WPS_WPSHEADERHISTORY_Result> lst = db.SP_WPS_WPSHEADERHISTORY(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      WPSNumber = li.WPSNumber,
                                      WPSRevNo = "R" + li.WPSRevNo,
                                      Status = li.Status,
                                      CreatedBy = li.CreatedBy,
                                      EditedBy = li.EditedBy,
                                      Location = li.Location,
                                      CreatedOn = li.CreatedOn,
                                      EditedOn = li.EditedOn
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        //public ActionResult CreateWPSFromWPSKeyIndex(string WPSNo,string QualityProject)
        //{

        //}

        #region WPS Json
        public ActionResult getWPSNoForAutoComplete(string Loc, string term)
        {
            var lstData = db.WPS010_Log.Where(x => x.Location == Loc && x.WPSNumber.Contains(term))
                .Select(s => new AutoCompleteList { id = s.WPSNumber, value = s.WPSNumber, label = s.WPSNumber }).Distinct().Take(10).ToList();
            return Json(lstData, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult CheckWPSIsSendForApproval(int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var objWPS010 = db.WPS010.Where(i => i.HeaderId == HeaderId).FirstOrDefault();
                if (objWPS010 != null && (objWPS010.Status == clsImplementationEnum.WPSHeaderStatus.SentForApproval.GetStringValue() || objWPS010.Status == clsImplementationEnum.WPSHeaderStatus.Approved.GetStringValue()))
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.IsInformation = true;
                    objResponseMsg.Value = "WPS Status Is Send For Approval Or Approved Status. You Can Not Perform Any Action.";
                }
                else
                {
                    objResponseMsg.Key = false;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
    }

    public enum SupplementalDevice
    {
        [StringValue("Yes")]
        Yes,
        [StringValue("No")]
        No,
        [StringValue("Yes/No*")]
        Yes_No,
        [StringValue("NA")]
        NA
    }
}