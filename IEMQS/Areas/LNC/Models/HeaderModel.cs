﻿using IEMQSImplementation;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.LNC.Models
{
    public class HeaderModel
    {
        public string Project { get; set;}
    }
    public class LNCProjectDetail
    {
        public LNC001 objLNC { get; set; }
        public string Condition { get; set; }
        public string webCDDString { get { return(objLNC.CDD.ToString("dd/MM/yyyy")); } }
    }

    public class LNCDataHelper
    {
        public static List<string> ProductType = new List<string>()
        {
            "EO Reactor" ,
            "Plate Reactor" ,
            "Forge Rector" ,
            "Ammonia Convertor" ,
            "Methonal Convertor" ,
            "Coke Drum" ,
            "FCC RR Package" ,
            "HP Heat Exchangers" ,
            "Feed Water Heater" ,
            "Condenser" ,
            "Gasifier" ,
            "Thick LNG Vessels" ,
            "Columns" ,
            "Urea Equipments" ,
            "Boiler Package" ,
            "Ammonia Convertor Basket" ,
            "Reactor Internls" ,
            "FCC Cyclones" ,
            "Canisters" 
        };

    }
}