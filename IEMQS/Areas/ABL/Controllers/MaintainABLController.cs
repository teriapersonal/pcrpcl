﻿using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using IEMQSImplementation.ABL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQS.CJCLNService;
using System.Globalization;
using Microsoft.Owin.Infrastructure;

namespace IEMQS.Areas.ABL.Controllers
{
    public class MaintainABLController : clsBase
    {
        // GET: ABL/MaintainABL

        ABLEntitiesContext dbAbl = new ABLEntitiesContext();

        //[SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult Index()
        {
            //ABL001 obj = new ABL001();
            //obj.Project = "Test";

            //dbAbl.ABL001.Add(obj);
            //dbAbl.SaveChanges();
            return View();
        }

        //[SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult AddAutoBilling(int? HeaderID)
        {
            ABL001 objABL001 = null;

            int? Id = HeaderID.HasValue ? HeaderID : 0;
            Session["HeaderID"] = 0;
            Session["SubCategory"] = "";
            if (Id > 0)
            {
                objABL001 = dbAbl.ABL001.Where(x => x.HeaderId == Id).FirstOrDefault();
                Session["HeaderID"] = objABL001.HeaderId.ToString();
            }
            else
            {
                objABL001 = new ABL001();
            }
            return View(objABL001);
        }

        public class NDETypeModel
        {
            public string CatID { get; set; }
            public string CatDesc { get; set; }
        }

        //public class SelectABLList
        //{
        //    public Nullable<long> ROW_NO { get; set; }
        //    public Nullable<int> TotalCount { get; set; }
        //    public int HeaderId { get; set; }
        //    public string ProjectNo { get; set; }
        //    public string SeamLengthArea { get; set; }
        //    public string NDEType { get; set; }
        //    public string Stage { get; set; }
        //    public string Itrn { get; set; }
        //    public string Shop{ get; set; }
        //    public string Location { get; set; }
        //    public string Ext { get; set; }
        //    public string ActualNDE { get; set; }
        //    public string NDTRemark { get; set; }
        //    public string OfferDT { get; set; }
        //    public string AttendedDT { get; set; }
        //    public string CalculationAMT { get; set; }
        //    public string Type { get; set; }
        //}

        [HttpPost]
        public ActionResult GetProjectResult(string term)
        {
            var LNLinkedServer = Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.LNLinkedServer.GetStringValue());
            string LNCompanyId = Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.LNCompanyId.GetStringValue());
            var lstProject = new List<SelectItemList>();

            if (!string.IsNullOrWhiteSpace(term))
            {
                lstProject = db.Database.SqlQuery<SelectItemList>("Select distinct child.t_cprj ,child.t_cprj + ':' + parnt.t_dsca as id , child.t_cprj + ':' + parnt.t_dsca as text from " + LNLinkedServer + ".dbo.tltlnt923" + LNCompanyId + " child join " + LNLinkedServer + ".dbo.ttppdm600" + LNCompanyId + " parnt on parnt.t_cprj = child.t_cprj  Where UPPER(child.t_cprj + '-' + parnt.t_dsca) like UPPER('%" + term + "%') order by child.t_cprj ").Take(20).ToList();

            }
            else
            {
                lstProject = db.Database.SqlQuery<SelectItemList>("Select distinct child.t_cprj ,child.t_cprj + ':' + parnt.t_dsca as id , child.t_cprj + ':' + parnt.t_dsca as text from " + LNLinkedServer + ".dbo.tltlnt923" + LNCompanyId + " child join " + LNLinkedServer + ".dbo.ttppdm600" + LNCompanyId + " parnt on parnt.t_cprj = child.t_cprj order by child.t_cprj ").Take(20).ToList();
            }

            return Json(lstProject, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetVendorResult(string term, string NDEType, string SubCategory)
        {
            var lstVendor = new List<SelectItemList>();
            string query = string.Empty;
            if (!string.IsNullOrWhiteSpace(term))
            {
                //query = "select a.Vendor,a.Vendor + ':' + b.t_nama as id , a.Vendor + ':' + b.t_nama as text FROM [IEMQS].[dbo].[ABL003] a left join [IEMQS].[dbo].[COM006] b on a.Vendor = b.t_bpid where [NDEType]= '" + NDEType +
                //    "'  and Vendor like '%" + term + "%' order by Vendor";
                query = "select distinct a.Vendor,a.Vendor + ':' + b.t_nama as id , a.Vendor + ':' + b.t_nama as text FROM [dbo].[ABL003] a left join [dbo].[COM006] b on a.Vendor = b.t_bpid where [NDEType]= '" + NDEType + "' And SubCategory in (select value from dbo.fn_split(  '" + SubCategory + "' , ','))  and Vendor like '%" + term + "%' order by Vendor";
                lstVendor = db.Database.SqlQuery<SelectItemList>(query).Take(20).ToList();
            }
            else
            {
                //query = "Select a.Vendor,a.Vendor + ':' + b.t_nama as id,a.Vendor + ':' + b.t_nama as text FROM [IEMQS].[dbo].[ABL003] a left join [IEMQS].[dbo].[COM006] b on a.Vendor = b.t_bpid where [NDEType]= '" + NDEType + "' order by Vendor";
                query = "Select distinct a.Vendor,a.Vendor + ':' + b.t_nama as id,a.Vendor + ':' + b.t_nama as text FROM [dbo].[ABL003] a left join [dbo].[COM006] b on a.Vendor = b.t_bpid where [NDEType]= '" + NDEType + "' and SubCategory in (select value from dbo.fn_split(  '" + SubCategory + "' , ','))  and Vendor like '%" + term + "%' order by Vendor";
                lstVendor = db.Database.SqlQuery<SelectItemList>(query).Take(20).ToList();
            }

            return Json(lstVendor, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetPOResult(string term, string Vendor, string NDEType, string Subcategory)
        {
            var lstPO = new List<SelectItemList>();
            string query = string.Empty;
            Vendor = Vendor.ToString().Split(':')[0].Trim();
            if (!string.IsNullOrWhiteSpace(term))
            {
                //query = "Select distinct PONo as id  FROM [IEMQS].[dbo].[ABL003] where [Vendor]= '" + Vendor + "' and" +
                //    " Vendor like '%" + term + "%' order by PONo ";
                //query = "Select distinct PONo as id  FROM [dbo].[ABL003] where [Vendor]= '" + Vendor + "' and" +
                //    " Vendor like '%" + term + "%' order by PONo ";

                query = "Select distinct PONo as id  FROM [dbo].[ABL003] where [Vendor]= '" + Vendor + "' and " +
                   "NDEType ='" + NDEType + "'" + "and SubCategory in (select value from dbo.fn_split(  '" + Subcategory + "' , ','))  " +
                   "And  Vendor like '%" + term + "%' order by PONo ";
                lstPO = db.Database.SqlQuery<SelectItemList>(query).Take(20).ToList();
            }
            else
            {
                //query = "Select distinct PONo as id  FROM [IEMQS].[dbo].[ABL003] where [Vendor]= '" + Vendor + "' order by PONo ";
                query = "Select distinct PONo as id  FROM [dbo].[ABL003] where [Vendor]= '" + Vendor + "'" +
                      " And NDEType ='" + NDEType + "'" + "and SubCategory  in (select value from dbo.fn_split(  '" + Subcategory + "' , ','))  order by PONo;";

                lstPO = db.Database.SqlQuery<SelectItemList>(query).Take(20).ToList();
            }

            return Json(lstPO, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetPOLineResult(string term, string Vendor,string NDEType,string SubCategory,string PO)
        {
            var lstPO = new List<SelectItemList>();
            string query = string.Empty;
            Vendor = Vendor.ToString().Split(':')[0].Trim();
            if (!string.IsNullOrWhiteSpace(term))
            {
                //query = "Select distinct cast(POLine as varchar(10)) as id  FROM [IEMQS].[dbo].[ABL003]" +
                //    " where [Vendor]= '" + Vendor + "' and" +
                //    " Vendor like '%" + term + "%' order by cast(POLine as varchar(10)) ";
                query = "Select distinct cast(POLine as varchar(10)) as id  FROM [dbo].[ABL003]" +
                    " where [Vendor]= '" + Vendor + "' and " +
                        "NDEType ='" + NDEType + "'" + "and SubCategory  in (select value from dbo.fn_split(  '" + SubCategory + "' , ','))  " + "and PONo='" + PO + "'"+
                        "and Vendor like '%" + term + "%' order by cast(POLine as varchar(10)) ";
                lstPO = db.Database.SqlQuery<SelectItemList>(query).Take(20).ToList();
            }
            else
            {
                //query = "Select distinct cast(POLine as varchar(10)) as id  FROM [IEMQS].[dbo].[ABL003]" +
                //    " where [Vendor]= '" + Vendor + "' order by cast(POLine as varchar(10)) ";
                query = "Select distinct cast(POLine as varchar(10)) as id  FROM [dbo].[ABL003]" +
                    " where [Vendor]= '" + Vendor + "' order by cast(POLine as varchar(10)) ";
                lstPO = db.Database.SqlQuery<SelectItemList>(query).Take(20).ToList();
            }

            return Json(lstPO, JsonRequestBehavior.AllowGet);
        }

        public List<string> NDEType()
        {
            List<string> items = clsImplementationEnum.GetListOfDescription<clsImplementationEnum.NDEType>();
            return items;
        }

        [HttpPost]
        public ActionResult LoadABLAddFormPartial(int? HeaderID)
        {
            ABL001 objABL001 = null;
            int? Id = HeaderID.HasValue ? HeaderID : 0;
            ViewBag.Project = "";
            ViewBag.TypeNDE = "";
            ViewBag.SubCategory = "";
            ViewBag.Vendor = "";
            ViewBag.PONo = "";
            ViewBag.POLine = 0;
            ViewBag.BillNo = "";
            ViewBag.BillDate = DateTime.Now;
            ViewBag.BillAmount = 0.0;
            ViewBag.BasicAmount = 0.0;
            ViewBag.BriefDescription = "";
            ViewBag.Remark = "";
            ViewBag.CJC = "";
            if (Id != 0)
            {
                objABL001 = dbAbl.ABL001.Where(x => x.HeaderId == Id).FirstOrDefault();
                if (objABL001 != null)
                {
                    if (objABL001.Project != null)
                    {
                        var ProjectDesc = db.COM001.Where(x => x.t_cprj == objABL001.Project).Select(x => x.t_dsca).FirstOrDefault();
                        ViewBag.Project = objABL001.Project + "-" + ProjectDesc;
                    }
                    if (objABL001.NDEType != null)
                    {
                        ViewBag.TypeNDE = objABL001.NDEType;
                    }
                    if (Session["SubCategory"].ToString() != "")
                    {
                        ViewBag.SubCategory = Session["SubCategory"].ToString();
                    }
                    if (objABL001.Vendor != null)
                    {
                        var VendorName = db.COM006.Where(x => x.t_bpid == objABL001.Vendor).Select(x => x.t_nama).FirstOrDefault();
                        ViewBag.Vendor = objABL001.Vendor + "-" + VendorName;
                    }
                    if (objABL001.PONo != null)
                    {
                        ViewBag.PONo = objABL001.PONo;
                    }
                    if (objABL001.POLine != null)
                    {
                        ViewBag.POLine = objABL001.POLine;
                    }
                    if (objABL001.BillNo != null)
                    {
                        ViewBag.BillNo = objABL001.BillNo;
                    }
                    if (objABL001.BillDate != null)
                    {
                        ViewBag.BillDate = objABL001.BillDate;
                    }
                    if (objABL001.BillAmount != null)
                    {
                        ViewBag.BillAmount = objABL001.BillAmount;
                    }
                    if (objABL001.BasicAmount != null)
                    {
                        ViewBag.BasicAmount = objABL001.BasicAmount;
                    }
                    if (objABL001.BriefDescription != null)
                    {
                        ViewBag.BriefDescription = objABL001.BriefDescription;
                    }
                    if (objABL001.Remark != null)
                    {
                        ViewBag.Remark = objABL001.Remark;
                    }
                    if (objABL001.CJCNo != null)
                    {
                        ViewBag.CJC = objABL001.CJCNo;
                    }
                }
            }
            else
            {
                objABL001 = new ABL001();
                List<string> lstNDEtype = NDEType();
                ViewBag.NDEType = lstNDEtype.AsEnumerable().Select(x => new NDETypeModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();
            }
            return PartialView("_ABLAddFormPartial", objABL001);
        }

        [HttpPost]
        public JsonResult LoadABLLineData(JQueryDataTableParamModel param, string NDEType, string NDESubcategory, string Status, int HeaderId, DateTime BillDate,DateTime BillToDate)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();
                string[] arrayCTQStatus = { clsImplementationEnum.CTQStatus.DRAFT.GetStringValue(), clsImplementationEnum.CTQStatus.Returned.GetStringValue() };
                string[] columnName = { "BU", "Project", "SeamNo", "StageSequence", "SeamLength", "NDEType", "Shop", "Location", "ActualNDE", "NDEExecution", "StageCode", "PrintSummary" };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);

                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName);
                //strWhere += " and Project = " + param.Project + " and NDEType = " +NDEType;
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                DateTime lunchDate = (DateTime)dbAbl.ABL005.OrderByDescending(a => a.LaunchDate).FirstOrDefault().LaunchDate;


                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (BillDate == null && BillToDate == null) {
                    strWhere = "(Q60.InspectedOn and Q60.InspectedOn)";
                }
                else {
                    strWhere = "(cast('" +BillDate+ "' as nvarchar(30)) and cast('" + BillToDate + "' as nvarchar(30)) )";
                }
                DataTable dtABLLines = new DataTable();
                var lstResult = dbAbl.SP_GET_ABL_LINES
                                (
                               StartIndex,
                               EndIndex,
                               strSortOrder,
                               strWhere,
                               param.Project.Split(':')[0].Trim(),
                               NDEType,
                               NDESubcategory,
                               Status, BillDate ,BillToDate
                                ).ToList();
                dtABLLines = Helper.ToDataTable(lstResult);
                TempData["FetchABLList"] = dtABLLines;


                DataView dataView = dtABLLines.AsDataView();

                if (!string.IsNullOrWhiteSpace(strSortOrder))
                    dataView.Sort = strSortOrder;

                int? totalRecords = dataView.ToTable().Rows.Count;

                List<DataRow> filteredList = dataView.ToTable().Select().Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();


                var data = (from uc in filteredList
                            select new[]
                          {
                                    Convert.ToString(uc["HeaderId"]),
                                    Convert.ToString(uc["BU"]),
                                    Convert.ToString(uc["ProjectNo"]),
                                    Convert.ToString(uc["SeamNo"]),
                                    Convert.ToString(uc["StageSequence"]),
                                    Convert.ToString(uc["SeamLength"]),
                                    Convert.ToString(uc["NDEType"]),
                                    Convert.ToString(uc["Stage"]),
                                    Convert.ToString(uc["Itrn"]),
                                    Convert.ToString(uc["Shop"]),
                                    Convert.ToString(uc["Location"]),
                                    Convert.ToString(uc["Ext"]),
                                    Convert.ToString(uc["ActualNDE"]),
                                    Convert.ToString(uc["NDEExecution"]),
                                    Convert.ToString(uc["NDTRemarks"]),
                                    Convert.ToString(uc["OfferDT"] == null || Convert.ToDateTime(uc["OfferDT"]) == DateTime.MinValue || Convert.ToString(uc["OfferDT"]) == "01/01/1900 00:00:00" ? "-" : Convert.ToDateTime(uc["OfferDT"]).ToString("dd/MM/yyyy")),
                                    Convert.ToString(uc["AttendedDT"] == null || Convert.ToDateTime(uc["AttendedDT"]) == DateTime.MinValue || Convert.ToString(uc["AttendedDT"]) == "01/01/1900 00:00:00" ? "-" : Convert.ToDateTime(uc["AttendedDT"]).ToString("dd/MM/yyyy")),
                                    Helper.GenerateTextArea(Convert.ToInt32(uc["ROW_NO"]), "BillingRemark"),
                                    Convert.ToString(uc["PrintSummary"]),
                                  
                                    //"",
                            }).ToList();
                //data.Insert(0, newRecord);
                RetenTempDataValue();

                if (HeaderId > 0)
                {
                    #region Insert ABLLines

                    DataTable dtABL = (DataTable)TempData["FetchABLList"];
                    if (dtABL != null)
                    {
                        List<DataRow> dtFinal = (from u in dtABL.Rows.Cast<DataRow>()
                                                 select u).ToList();

                        if (dtFinal != null && dtFinal.Count > 0)
                        {
                            List<ABL002> lstAddABL = new List<ABL002>();
                            foreach (DataRow dtRow in dtFinal)
                            {
                                var CalculationAmount = 0.0;
                                var SeamLength = Convert.ToInt32(dtRow["SeamLength"]);
                                var NDELength = dbAbl.ABL004.Where(x => x.MinimumValue <= SeamLength && x.MaximumValue >= SeamLength).Select(x => x.NDELength).FirstOrDefault();
                                var UnitRate = dbAbl.ABL003.Where(x => x.NDELength == NDELength).Select(x => x.UnitRate).FirstOrDefault();
                                var NdeExecution = Convert.ToString(dtRow["NDEExecution"]);
                                CalculationAmount = Convert.ToDouble(SeamLength * UnitRate);
                                if (NdeExecution.ToUpper() == "Double".ToUpper())
                                {
                                    CalculationAmount = CalculationAmount * 2;
                                }

                                #region Assign Values 
                                var obj1 = dbAbl.ABL001.OrderByDescending(a => a.HeaderId).FirstOrDefault();
                                ABL002 objABL002 = new ABL002();
                                objABL002.HeaderId = (obj1.HeaderId) + 1;
                                objABL002.BU = Convert.ToString(dtRow["BU"]);
                                objABL002.SeamNo = Convert.ToString(dtRow["SeamNo"]);
                                objABL002.StageSequence = Convert.ToInt32(dtRow["StageSequence"]); ;
                                objABL002.Project = obj1.Project;
                                objABL002.QualityProject = Convert.ToString(dtRow["ProjectNo"]);
                                objABL002.SeamLength = Convert.ToInt32(dtRow["SeamLength"]);
                                objABL002.NDEType = Convert.ToString(dtRow["NDEType"]);
                                objABL002.StageCode = Convert.ToString(dtRow["Stage"]);
                                objABL002.Iteration = Convert.ToInt32(dtRow["Itrn"]);
                                objABL002.Shop = Convert.ToString(dtRow["Shop"]);
                                objABL002.Location = Convert.ToString(dtRow["Location"]);
                                objABL002.InspectionExtent = Convert.ToString(dtRow["Ext"]);
                                objABL002.ActualNDE = Convert.ToString(dtRow["ActualNDE"]);
                                //objABL002.NDEExecution = Convert.ToString(dtRow["NDEExecution"]);
                                objABL002.NDTRemarks = Convert.ToString(dtRow["NDTRemarks"]);
                                objABL002.OfferedOn = Convert.ToDateTime(dtRow["OfferDT"]);
                                objABL002.InspectedOn = Convert.ToDateTime(dtRow["AttendedDT"]);
                                objABL002.CalculationAmount = Convert.ToDecimal(CalculationAmount);
                                objABL002.CreatedBy = obj1.CreatedBy;
                                objABL002.CreatedDate = DateTime.Now;
                                lstAddABL.Add(objABL002);

                                #endregion
                            }

                            if (lstAddABL.Count > 0)
                            {
                                dbAbl.ABL002.AddRange(lstAddABL);
                                dbAbl.SaveChanges();
                            }
                        }
                    }
                    #endregion

                    #region showlines
                    try
                    {
                        var isLocationSortable1 = Convert.ToBoolean(Request["bSortable_1"]);
                        var sortColumnIndex1 = Convert.ToInt32(Request["iSortCol_0"]);

                        string sortColumnName1 = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                        string sortDirection1 = Convert.ToString(Request["sSortDir_0"]);

                        int? StartIndex1 = param.iDisplayStart + 1;
                        int? EndIndex1 = param.iDisplayStart + param.iDisplayLength;


                        string strSortOrder1 = string.Empty;
                        var updateWhere1 = "HeaderId=" + HeaderId;
                        string strWhere1 = "HeaderId=" + HeaderId;
                        //string[] arrayCTQStatus = { clsImplementationEnum.CTQStatus.DRAFT.GetStringValue(), clsImplementationEnum.CTQStatus.Returned.GetStringValue() };
                        string[] columnName1 = { "LineId", "HeaderId", "BU", "SeamNo", "StageSequence", "QualityProject", "Project", "CJCNo", "SeamLength", "NDEType", "StageCode", "Iteration", "Shop", "Location", "InspectionExtent", "ActualNDE", "NDTRemarks", "OfferedOn", "InspectedOn", "CalculationAmount", "BillingRemark" };

                        if (!string.IsNullOrWhiteSpace(param.sSearch))
                        {
                            strWhere1 += columnName.MakeDatatableSearchCondition(param.sSearch);
                        }

                        if (!string.IsNullOrWhiteSpace(sortColumnName1))
                        {
                            strSortOrder1 = " Order By " + sortColumnName1 + " " + sortDirection1 + " ";
                        }

                        var lstResult1 = dbAbl.SP_ABL_GET_AUTOBILL_Line(StartIndex1, EndIndex1, strSortOrder1, strWhere1, updateWhere1).ToList();
                        int? totalRecords1 = lstResult1.Select(i => i.TotalCount).FirstOrDefault();
                        var data1 = (from uc in lstResult1
                                     select new[]
                                   {
                                Helper.HTMLActionString(uc.LineId,"Delete","Delete Record","fa fa-trash-o","DeleteLine("+ uc.LineId +");"),
                                Convert.ToString(uc.LineId),
                                    Convert.ToString(uc.HeaderId),
                                    Convert.ToString(uc.BU),
                                    Convert.ToString(uc.SeamNo),
                                    Convert.ToString(uc.StageSequence),
                                    Convert.ToString(uc.QualityProject),
                                    Convert.ToString(uc.Project + " - " + uc.ProjectName),
                                    Convert.ToString(uc.CJCNo),
                                    Convert.ToString(uc.SeamLength),
                                    Convert.ToString(uc.NDEType),
                                    Convert.ToString(uc.StageCode),
                                    Convert.ToString(uc.Iteration),
                                    Convert.ToString(uc.Shop),
                                    Convert.ToString(uc.Location),
                                    Convert.ToString(uc.InspectionExtent),
                                    Convert.ToString(uc.ActualNDE),
                                    Convert.ToString(uc.NDTRemarks),
                                    Convert.ToString(uc.OfferedOn == null || uc.OfferedOn.Value == DateTime.MinValue || Convert.ToString(uc.OfferedOn) == "01/01/1900 00:00:00" ? "-" : uc.OfferedOn.Value.ToString("dd/MM/yyyy",CultureInfo.InvariantCulture)),
                                    Convert.ToString(uc.InspectedOn == null || uc.InspectedOn.Value == DateTime.MinValue || Convert.ToString(uc.InspectedOn) == "01/01/1900 00:00:00" ? "-" : uc.InspectedOn.Value.ToString("dd/MM/yyyy",CultureInfo.InvariantCulture)),
                                    Convert.ToString(uc.CalculationAmount),
                                    Convert.ToString(uc.BillingRemark),
                                "",
                            }).ToList();
                        //data.Insert(0, newRecord);
                        return Json(new
                        {
                            sEcho = param.sEcho,
                            iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                            iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                            aaData = data,
                            strWhere = strWhere,
                            strSortOrder = strSortOrder,
                        }, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception ex)
                    {
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        return Json(new
                        {
                            sEcho = param.sEcho,
                            iTotalRecords = "0",
                            iTotalDisplayRecords = "0",
                            aaData = ""
                        }, JsonRequestBehavior.AllowGet);
                    }
                    #endregion
                }
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult DeleteABLLineData(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                ABL002 objABL002 = dbAbl.ABL002.Where(x => x.LineId == Id).FirstOrDefault();
                if (objABL002 != null)
                {
                    dbAbl.ABL002.Remove(objABL002);
                    dbAbl.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Details not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult LoadABLListDataPartial(string status)
        {
            Session["SubCategory"] = "";
            ViewBag.Status = status;

            return PartialView("_ABLListDataPartial");
        }

        [HttpPost]
        public JsonResult AssignDetail(string project, string NDEType, string Vendor, string Po, string PoLine, string SubCategory)
        {
            try
            {
                ModelState.Clear();
                Session["SubCategory"] = SubCategory;
                //int PoNO = C.(Po);
                int PoLineNO = Convert.ToInt32(PoLine);
                var obj = dbAbl.ABL001.OrderByDescending(a => a.HeaderId).FirstOrDefault();
                int number = 0;
                if (obj != null)
                {
                    string lastId = obj.WorkAssigID.ToString();
                    number = Convert.ToInt32(lastId.Substring(lastId.Length - 4));
                    number++;
                }
                else
                {
                    number = 1;
                }
                string s = number.ToString().PadLeft(4, '0');
                int curYear = (DateTime.Today.Year % 100);
                string lastTwoDig = curYear.ToString();
                string newWorkAssignId = "ABL" + lastTwoDig + s;
                project = project.ToString().Split(':')[0].Trim();
                Vendor = Vendor.ToString().Split(':')[0].Trim();
                ABL001 aBL = new ABL001();

                #region Insert ABLLines

                DataTable dtABL = (DataTable)TempData["FetchABLList"];
                if (dtABL != null)
                {
                    List<DataRow> dtFinal = (from u in dtABL.Rows.Cast<DataRow>()
                                             select u).ToList();

                    if (dtFinal != null && dtFinal.Count > 0)
                    {
                        List<ABL002> lstAddABL = new List<ABL002>();
                        foreach (DataRow dtRow in dtFinal)
                        {
                            var CalculationAmount = 0.0;
                            var SeamLength = Convert.ToInt32(dtRow["SeamLength"]);
                            var NDELength = dbAbl.ABL004.Where(x => x.MinimumValue <= SeamLength && x.MaximumValue >= SeamLength).Select(x => x.NDELength).FirstOrDefault();
                            var UnitRate = dbAbl.ABL003.Where(x => x.NDELength == NDELength && x.NDEType == NDEType && x.SubCategory == SubCategory && x.Vendor == Vendor && x.PONo == Po && x.POLine == PoLineNO).Select(x => x.UnitRate).FirstOrDefault();
                           
                            
                            string seamno = Convert.ToString(dtRow["SeamNo"]);
                            string QualityProject = Convert.ToString(dtRow["QualityProject"]);
                            var unit = db.QMS012.Where(x => x.SeamNo == seamno && x.QualityProject == QualityProject).Select(x => x.Unit).FirstOrDefault();
                            if (unit == "mm")
                            {
                                CalculationAmount = (Convert.ToDouble(SeamLength * UnitRate)) / 1000;
                            }
                            if (unit == "mm2") { 
                                CalculationAmount = (Convert.ToDouble(SeamLength * UnitRate)) / 1000000;
                            }
                            var NdeExecution = Convert.ToString(dtRow["NDEExecution"]);
                            if (NdeExecution.ToUpper() == "Double".ToUpper())
                            {
                                CalculationAmount = CalculationAmount * 2;
                            }

                            #region Assign Values 
                            var obj1 = dbAbl.ABL001.OrderByDescending(a => a.HeaderId).FirstOrDefault();
                            ABL002 objABL002 = new ABL002();
                            if (obj1 != null)
                            {
                                objABL002.HeaderId = (obj1.HeaderId) + 1;
                                objABL002.Project = obj1.Project;
                            }
                            else
                            {
                                objABL002.HeaderId = 1;
                                objABL002.Project = project;
                            }
                            objABL002.BU = Convert.ToString(dtRow["BU"]);
                            objABL002.SeamNo = Convert.ToString(dtRow["SeamNo"]);
                            objABL002.StageSequence = Convert.ToInt32(dtRow["StageSequence"]);
                            objABL002.QualityProject = Convert.ToString(dtRow["ProjectNo"]);
                            objABL002.SeamLength = Convert.ToInt32(dtRow["SeamLength"]);
                            objABL002.NDEType = Convert.ToString(dtRow["NDEType"]);
                            objABL002.StageCode = Convert.ToString(dtRow["Stage"]);
                            objABL002.Iteration = Convert.ToInt32(dtRow["Itrn"]);
                            objABL002.Shop = Convert.ToString(dtRow["Shop"]);
                            objABL002.Location = Convert.ToString(dtRow["Location"]);
                            objABL002.InspectionExtent = Convert.ToString(dtRow["Ext"]);
                            objABL002.ActualNDE = Convert.ToString(dtRow["ActualNDE"]);
                            objABL002.NDTRemarks = Convert.ToString(dtRow["NDTRemarks"]);
                            objABL002.OfferedOn = Convert.ToDateTime(dtRow["OfferDT"]);
                            objABL002.InspectedOn = Convert.ToDateTime(dtRow["AttendedDT"]);
                            objABL002.CalculationAmount = Convert.ToDecimal(CalculationAmount);
                            objABL002.CreatedBy = objClsLoginInfo.UserName;
                            objABL002.CreatedDate = DateTime.Now;
                            lstAddABL.Add(objABL002);

                            #endregion
                        }

                        if (lstAddABL.Count > 0)
                        {
                            aBL.Project = project;
                            aBL.NDEType = NDEType;
                            aBL.WorkAssigID = newWorkAssignId;
                            aBL.Vendor = Vendor;
                            aBL.PONo = Po.ToString().Trim();
                            aBL.POLine = Convert.ToInt32(PoLine);
                            aBL.CreatedBy = objClsLoginInfo.UserName;
                            aBL.CreatedDate = DateTime.Now;
                            dbAbl.ABL001.Add(aBL);
                            dbAbl.SaveChanges();
                            int _HeaderId = aBL.HeaderId;
                            foreach(var item  in lstAddABL)
                            {
                                item.HeaderId = _HeaderId;
                                //dbAbl.ABL002.Add(item);
                               
                            }
                            dbAbl.ABL002.AddRange(lstAddABL);
                            dbAbl.SaveChanges();
                            return Json(new
                            {
                                Key = true,
                                Value = "Successfully Assigned",
                                workAssignId = aBL.WorkAssigID,
                                headerId = aBL.HeaderId
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new
                        {
                            Key = false,
                            Value = "Can Not Assign Work."
                        }, JsonRequestBehavior.AllowGet);
                    }
                }

                else
                {
                    return Json(new
                    {
                        Key = false,
                        Value = "Can Not Assign Work."
                    }, JsonRequestBehavior.AllowGet);
                }
                #endregion
                return null;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    Key = false,
                    Value = "Technical Error"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult SaveData(int HeaderId, string BillNo, DateTime BillDate, int BillAmount, int BasicAmount, string BriefDescription, string Remark)
        {
            try
            {
                if (HeaderId > 0)
                {
                    ABL001 obj = dbAbl.ABL001.OrderByDescending(a => a.HeaderId == HeaderId).FirstOrDefault();
                    obj.BillNo = BillNo;
                    obj.BillDate = BillDate;
                    obj.BillAmount = BillAmount;
                    obj.BasicAmount = BasicAmount;
                    obj.BriefDescription = BriefDescription;
                    obj.Remark = Remark;
                    dbAbl.SaveChanges();
                }
                else
                {
                    ABL001 obj = dbAbl.ABL001.OrderByDescending(a => a.HeaderId).FirstOrDefault();
                    obj.BillNo = BillNo;
                    obj.BillDate = BillDate;
                    obj.BillAmount = BillAmount;
                    obj.BasicAmount = BasicAmount;
                    obj.BriefDescription = BriefDescription;
                    obj.Remark = Remark;
                    dbAbl.SaveChanges();
                }
                return Json(new
                {
                    Key = true,
                    Value = "Successfully Saved",
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    Key = false,
                    Value = "Technical Error"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public PartialViewResult LoadCJCGridDataPartial(string status)
        {
            ViewBag.status = status;
            return PartialView("_LoadCJCGridDataPartial");
        }
        [HttpPost]
        public ActionResult getCJCHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string status = param.Status;
                string strSortOrder = string.Empty;
                string strWhere = "1=1";

                if (status.ToLower() == "pending")
                {
                    strWhere += "and (CJCNo IS NULL or CJCNo = '')";
                }
                else
                {
                    strWhere += "";
                }

                string[] columnName = { "WorkAssignID", "Project", "NDEType", "Vendor", "PONo", "POLine", "CJCNo", "CreatedBy", "CreatedDate" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = dbAbl.SP_ABL_GET_AUTOBILL_HEADER(StartIndex, EndIndex, strSortOrder, strWhere).ToList();
                int? totalRecords = lstResult.Select(i => i.TotalCount).FirstOrDefault();
                var data = (from uc in lstResult
                            select new[]
                          {
                                "<center>"+ (Helper.GenerateActionIcon(uc.HeaderId, "View", "View Detail", "fa fa-eye", "", WebsiteURL + "/ABL/MaintainABL/AddAutoBilling?HeaderID="+uc.HeaderId,false))+"</center>",
                                    Convert.ToString(uc.HeaderId),
                                    Convert.ToString(uc.WorkAssigID),
                                    Convert.ToString(uc.Project + " - " + uc.ProjectName),
                                    Convert.ToString(uc.NDEType),
                                    Convert.ToString(uc.Vendor + " - " + uc.VendorName),
                                    Convert.ToString(uc.PONo),
                                    Convert.ToString(uc.POLine),
                                    Convert.ToString(uc.CJCNo),
                                    Convert.ToString(uc.CreatedBy + " - " + uc.CreatedByName),
                                    Convert.ToString(uc.CreatedDate.Value.ToString("dd/MM/yyyy"))
                            }).ToList();
                //data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = data,
                    strWhere = strWhere,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        public ActionResult GenerateCJCHeader(int HeaderId, string Project, string Vendor, string PONo, string POLine, string BillNo, DateTime BillDate, int BillAmount, int BasicAmount, string BriefDescription, string Remark, int sum)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            clsHelper.ResponseMsg obj = new clsHelper.ResponseMsg();
            clsHelper.ResponseMsg objServiceRes = new clsHelper.ResponseMsg();
            try
            {
                ABL001 objABL001 = dbAbl.ABL001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                var CreatedBy = objABL001.CreatedBy;

                COM003 objCOM003 = db.COM003.Where(x => x.t_psno == CreatedBy).FirstOrDefault();
                //COM003 objCOM003 = db.COM003.Where(x => x.t_psno == "20303768").FirstOrDefault();
                var Dept = objCOM003.t_depc;
                var Location = objCOM003.t_loca;

                int tolerance = (int)dbAbl.ABL005.OrderByDescending(a => a.ID).FirstOrDefault().Tolerance;
                int amount = Math.Abs(Convert.ToInt32(BillAmount - sum));
                if (amount <= tolerance)
                {
                    objServiceRes = GenerateCJCIntoLN(Dept, BriefDescription, BillAmount, BasicAmount, Location, PONo, CreatedBy, Vendor, Remark, POLine, BillNo, BillDate);
                    if (objServiceRes.Key)
                    {
                        objABL001.CJCNo = objServiceRes.Value;
                        dbAbl.SaveChanges();
                        List<ABL002> objABL002 = dbAbl.ABL002.Where(x => x.HeaderId == objABL001.HeaderId).ToList();
                        if (objABL002 != null)
                        {
                            foreach (var item in objABL002)
                            {
                                item.CJCNo = objABL001.CJCNo;
                                dbAbl.SaveChanges();
                            }
                        }
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "CJC : '" + objServiceRes.Value + "' generated successfully";
                        objResponseMsg.Status = objServiceRes.Value;
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = objServiceRes.Value;
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Bill Amount and Total Calculated Amount diffrence exceeds the tolerance limit.";
                }
            }

            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.dataValue = ex.Message;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponseMsg GenerateCJCIntoLN(string Dept, string BriefDescription, int BillAmount, int BasicAmount, string Location, string PONo, string Psno, string Vendor, string Remark, string POLine, string BillNo, DateTime BillDate)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            objResponseMsg.Key = false;

            CjcDataService serviceObj = new CjcDataService();
            try
            {
                generateCJCResponseType P1createResponse = new generateCJCResponseType();
                generateCJCRequestType P1createRequest = new generateCJCRequestType();
                generateCJCRequestTypeControlArea P1controlArea = new generateCJCRequestTypeControlArea();
                generateCJCRequestTypeCjcData P1dataArea = new generateCJCRequestTypeCjcData();


                Vendor = Vendor.ToString().Split(':')[0].Trim();
                string PoWhere = " BP LIKE '%" + Vendor + "%' AND t_orno LIKE '%" + PONo + "%' AND pono='" + POLine + "'";
                var PurOrder = db.SP_COMMON_GET_LN_APPROVED_PO(1, int.MaxValue, "", PoWhere).FirstOrDefault();


                P1controlArea.processingScope = processingScope.request;
                P1dataArea.cjcNo = Location;
                P1dataArea.initiatorDept = Dept;
                P1dataArea.briefDescription = BriefDescription;
                P1dataArea.grossValue = BillAmount != 0 ? Convert.ToDecimal(BillAmount) : 0;
                P1dataArea.location = Location;
                P1dataArea.purchaseOrder = PONo;
                P1dataArea.initiatorsPsno = Psno;
                //P1dataArea.initiatorsPsno = "20303768";
                P1dataArea.supplier = Vendor;
                P1dataArea.Remark = Remark;

                P1dataArea.BillDetail = new generateCJCRequestTypeCjcDataBillDetail[1];
                P1dataArea.BillDetail[0] = new generateCJCRequestTypeCjcDataBillDetail();
                P1dataArea.BillDetail[0].positionNo = POLine;
                P1dataArea.BillDetail[0].billNo = BillNo;
                P1dataArea.BillDetail[0].billDateSpecified = true;
                P1dataArea.BillDetail[0].billDate = BillDate;
                P1dataArea.BillDetail[0].claimedQtySpecified = true;
                P1dataArea.BillDetail[0].claimedQty = Convert.ToDecimal(Math.Round(BillAmount / PurOrder.Price, 4));
                //P1dataArea.BillDetail[0].claimedQty = Convert.ToDecimal(0.1);
                P1dataArea.BillDetail[0].claimedAmountSpecified = true;
                P1dataArea.BillDetail[0].claimedAmount = Convert.ToDecimal(BasicAmount);



                P1createRequest.ControlArea = P1controlArea;
                P1createRequest.DataArea = new generateCJCRequestTypeCjcData[1];
                P1createRequest.DataArea[0] = P1dataArea;

                P1createResponse = serviceObj.generateCJC(P1createRequest);

                if (P1createResponse.DataArea[0].Remark.ToLower().StartsWith("0"))
                {
                    //success
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = P1createResponse.DataArea[0].cjcNo;
                }
                else
                {
                    //error
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = P1createResponse.DataArea[0].Remark.ToString();
                }
            }



            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message.ToString();
            }
            finally
            {
                serviceObj = null;
            }
            return objResponseMsg;
        }

        [HttpPost]
        public ActionResult getCJCLineData(JQueryDataTableParamModel param, int HeaderID)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;


                string strSortOrder = string.Empty;
                var updateWhere = "A02.HeaderId=" + HeaderID;
                string strWhere = "HeaderId=" + HeaderID;
                //string[] arrayCTQStatus = { clsImplementationEnum.CTQStatus.DRAFT.GetStringValue(), clsImplementationEnum.CTQStatus.Returned.GetStringValue() };
                string[] columnName = { "LineId", "HeaderId", "BU", "SeamNo", "StageSequence", "QualityProject", "Project", "CJCNo", "SeamLength", "NDEType", "StageCode", "Iteration", "Shop", "Location", "InspectionExtent", "ActualNDE", "NDEExecution", "NDTRemarks", "OfferedOn", "InspectedOn", "CalculationAmount", "BillingRemark", "Not to be consider" };
                // string[] columnName = {"BU", "SeamNo", "StageSequence", "QualityProject", "Project", "CJCNo", "SeamLength", "NDEType" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = dbAbl.SP_ABL_GET_AUTOBILL_Line(StartIndex, EndIndex, strSortOrder, strWhere, updateWhere).ToList();
                int? totalRecords = lstResult.Select(i => i.TotalCount).FirstOrDefault();
                var data = (from uc in lstResult
                            select new[]
                          {
                               (uc.CJCNo == null ) ?  Helper.HTMLActionString(uc.LineId, "Delete", "Delete Record", "fa fa-trash-o", "DeleteLine(" + uc.LineId + ");") : Helper.HTMLActionString(uc.LineId, "Delete", "Do not Delete Record", "fa fa-trash-o", "","",true,""),
                                    Convert.ToString(uc.LineId),
                                    Convert.ToString(uc.HeaderId),
                                    Convert.ToString(uc.BU),
                                    Convert.ToString(uc.SeamNo),
                                    Convert.ToString(uc.StageSequence),
                                    Convert.ToString(uc.QualityProject),
                                    Convert.ToString(uc.Project + " - " + uc.ProjectName),
                                    Convert.ToString(uc.CJCNo),
                                    Convert.ToString(uc.SeamLength),
                                    Convert.ToString(uc.NDEType),
                                    Convert.ToString(uc.StageCode),
                                    Convert.ToString(uc.Iteration),
                                    Convert.ToString(uc.Shop),
                                    Convert.ToString(uc.Location),
                                    Convert.ToString(uc.InspectionExtent),
                                    Convert.ToString(uc.ActualNDE),
                                    Convert.ToString(uc.NDEExecution),
                                    Convert.ToString(uc.NDTRemarks),
                                    Convert.ToString(uc.OfferedOn == null || uc.OfferedOn.Value == DateTime.MinValue || Convert.ToString(uc.OfferedOn) == "01/01/1900 00:00:00" ? "-" : uc.OfferedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)),
                                    Convert.ToString(uc.InspectedOn == null || uc.InspectedOn.Value == DateTime.MinValue || Convert.ToString(uc.InspectedOn) == "01/01/1900 00:00:00" ? "-" : uc.InspectedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)),
                                    Convert.ToString(uc.CalculationAmount),
                                    Convert.ToString(uc.BillingRemark),
                                    Convert.ToString(uc.NotToBeConsider),
                                "",
                            }).ToList();
                //data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = data,
                    strWhere = strWhere,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        public void RetenTempDataValue()
        {
            TempData.Keep("FetchABLList");
        }


        public ActionResult getNewCJCLineData(JQueryDataTableParamModel param, int HeaderID)
        {
            try
            {
                //  int HeaderID = 49;
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);

                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;
                var updateWhere = "A02.HeaderId=" + HeaderID;
                string strWhere = "HeaderId=" + HeaderID;

                string whereCondition = " 1=1";
                string[] columnName = { "LineId", "HeaderId", "BU", "SeamNo", "StageSequence", "QualityProject", "Project", "CJCNo", "SeamLength", "NDEType", "StageCode", "Iteration", "Shop", "Location", "InspectionExtent", "ActualNDE", "NDEExecution", "NDTRemarks", "OfferedOn", "InspectedOn", "CalculationAmount", "BillingRemark", "Not to be consider" };
                //string[] columnName = { "BU", "SeamNo"/*"StageSequence", "QualityProject", "Project"*/};
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                //else
                //{
                //    strSortOrder = " Order By EditedOn desc ";
                //}

                var lstResult = dbAbl.SP_ABL_GET_AUTOBILL_Line(StartIndex, EndIndex, strSortOrder, strWhere, updateWhere).ToList();
                int? totalRecords = lstResult.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from uc in lstResult
                           select new[] {

                                    Convert.ToString(uc.LineId),
                                    Convert.ToString(uc.HeaderId),
                                    Convert.ToString(uc.BU),
                                    Convert.ToString(uc.SeamNo),
                                    Convert.ToString(uc.StageSequence),
                                    Convert.ToString(uc.QualityProject),
                                    Convert.ToString(uc.Project + " - " + uc.ProjectName),
                                    Convert.ToString(uc.CJCNo),
                                    string.IsNullOrWhiteSpace(uc.CJCNo)?     
                                    Helper.HTMLAutoComplete(uc.LineId,"txtSeamLength",Convert.ToString(uc.SeamLength),"",  false,"","SeamLength",false,"","",""," numberonly") +""+Helper.GenerateHidden(uc.LineId, "SeamLength",Convert.ToString( uc.SeamLength))
                                   :  Convert.ToString(uc.SeamLength),
                               
                                    Convert.ToString(uc.NDEType),
                                    Convert.ToString(uc.StageCode),
                                    Convert.ToString(uc.Iteration),
                                    Convert.ToString(uc.Shop),
                                    Convert.ToString(uc.Location),
                                    Convert.ToString(uc.InspectionExtent),
                                    Convert.ToString(uc.ActualNDE),
                                    Convert.ToString(uc.NDEExecution),
                                    Convert.ToString(uc.NDTRemarks),
                                    Convert.ToString(uc.OfferedOn == null || uc.OfferedOn.Value == DateTime.MinValue || Convert.ToString(uc.OfferedOn) == "01/01/1900 00:00:00" ? "-" : uc.OfferedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)),
                                    Convert.ToString(uc.InspectedOn == null || uc.InspectedOn.Value == DateTime.MinValue || Convert.ToString(uc.InspectedOn) == "01/01/1900 00:00:00" ? "-" : uc.InspectedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)),
                                    //Convert.ToString(uc.CalculationAmount),
                                    Helper.HTMLAutoComplete(uc.LineId,"txtCalculationAmount",Convert.ToString(uc.CalculationAmount),"",  false,"","CalculationAmount",false,"","",""," decimalonly") +""+Helper.GenerateHidden(uc.LineId, "CalculationAmount", Convert.ToString(uc.CalculationAmount)),

                                    Convert.ToString(uc.BillingRemark),
                                    Convert.ToString(uc.NotToBeConsider),
                                     //(uc.CJCNo == null ) ?  Helper.HTMLActionString(uc.LineId, "Delete", "Delete Record", "fa fa-trash-o", "DeleteLine(" + uc.LineId + ");") : Helper.HTMLActionString(uc.LineId, "Delete", "Do not Delete Record", "fa fa-trash-o", "","",true,"") + Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o","EnabledEditLine(" + uc.LineId + ");") + Helper.HTMLActionString(uc.LineId,"Update","Update Record","fa fa-floppy-o","EditLine(" + uc.LineId+ ");","",false,"display:none") + " " + Helper.HTMLActionString(uc.LineId,"Cancel","Cancel Edit","fa fa-ban","CancelEditLine(" + uc.LineId+ "); ","",false,"display:none"),
                                   (uc.CJCNo == null ) ?  Helper.HTMLActionString(uc.LineId, "Delete", "Delete Record", "fa fa-trash-o", "DeleteLine(" + uc.LineId + ");") : Helper.HTMLActionString(uc.LineId, "Delete", "Do not Delete Record", "fa fa-trash-o", "","",true,"") ,
                                "",
                                "",
                          }).ToList();

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""

                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetInlineEditableCGLineRowLength(string NDEType, string CJNO, string Project, int id, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();
                var result = new List<string>();
                string proj = Project.Split('-')[0];
                string bu = db.COM001.Where(i => i.t_cprj == proj).FirstOrDefault().t_entu;

                if (id == 0)
                {
                    int newRecordId = 0;
                    var newRecord = new[] {
                                    clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(), 
                        //Convert.ToString(uc.HeaderId),
                        
                                  
                                    Helper.GenerateHidden(newRecordId, "LineId", Convert.ToString(id)),
                                    //Helper.GenerateHidden(newRecordId, "HeaderId", Convert.ToString(id)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtBU",bu,"",  true,"","BU",false) +""+Helper.GenerateHidden(newRecordId, "BU",bu),
                                    Helper.HTMLAutoComplete(newRecordId,"txtSeamNo","","",  false,"","SeamNo",false) +""+Helper.GenerateHidden(newRecordId, "SeamNo", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtStageSequence","","",  false,"","StageSequence",false) +""+Helper.GenerateHidden(newRecordId, "StageSequence", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtQualityProject","","",  false,"","QualityProject",false) +""+Helper.GenerateHidden(newRecordId, "QualityProject", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtProject",Project,"",  true,"","Project",false) +""+Helper.GenerateHidden(newRecordId, "Project", Convert.ToString(newRecordId)),

                                    Helper.HTMLAutoComplete(newRecordId,"txtCJCNo",CJNO,"",true,"","CJCNo",false) +""+Helper.GenerateHidden(newRecordId, "CJCNo", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtSeamLength","","",  false,"","SeamLength",false) +""+Helper.GenerateHidden(newRecordId, "SeamLength", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtNDEType",NDEType,"",  true,"","NDEType",false) +""+Helper.GenerateHidden(newRecordId, "NDEType", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtStageCode","","",  false,"","StageCode",false) +""+Helper.GenerateHidden(newRecordId, "StageCode", Convert.ToString(newRecordId)),

                                    Helper.HTMLAutoComplete(newRecordId,"txtIteration","","",  false,"","Iteration",false) +""+Helper.GenerateHidden(newRecordId, "Iteration", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtShop","","",  false,"","Shop",false) +""+Helper.GenerateHidden(newRecordId, "Shop", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtLocation","","",  false,"","Location",false) +""+Helper.GenerateHidden(newRecordId, "Location", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtInspectionExtent","","",  false,"","InspectionExtent",false) +""+Helper.GenerateHidden(newRecordId, "InspectionExtent", Convert.ToString(newRecordId)),

                                    Helper.HTMLAutoComplete(newRecordId,"txtActualNDE","","",  false,"","ActualNDE",false) +""+Helper.GenerateHidden(newRecordId, "ActualNDE", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtNDEExecution","","",  false,"","NDEExecution",false) +""+Helper.GenerateHidden(newRecordId, "NDEExecution", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtNDTRemarks","","",  false,"","NDTRemarks",false) +""+Helper.GenerateHidden(newRecordId, "NDTRemarks", Convert.ToString(newRecordId)),

                         Helper.GenerateHTMLTextbox(newRecordId,"txtOfferedOn","","",  false,"",false,"") +""+Helper.GenerateHidden(newRecordId, "OfferedOn", Convert.ToString(newRecordId)),
                                    //Helper.HTMLAutoComplete(newRecordId,"txtOfferedOn","","",  false,"","OfferedOn",false) +""+Helper.GenerateHidden(newRecordId, "OfferedOn", Convert.ToString(newRecordId)),
                          Helper.GenerateHTMLTextbox(newRecordId,"txtInspectedOn","","",  false,"",false,"") +""+Helper.GenerateHidden(newRecordId, "InspectedOn", Convert.ToString(newRecordId)),
                        //Helper.HTMLAutoComplete(newRecordId,"txtInspectedOn","","",  false,"","InspectedOn",false) +""+Helper.GenerateHidden(newRecordId, "InspectedOn", Convert.ToString(newRecordId)),

                                    Helper.HTMLAutoComplete(newRecordId,"txtCalculationAmount","","",  true,"","CalculationAmount",false) +""+Helper.GenerateHidden(newRecordId, "CalculationAmount", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtBillingRemark","","",  false,"","BillingRemark",false) +""+Helper.GenerateHidden(newRecordId, "BillingRemark", Convert.ToString(newRecordId)),
                                    Helper.GenerateCheckbox(newRecordId,"txtNotToBeConsider",false) +""+Helper.GenerateHidden(newRecordId, "NotToBeConsider", Convert.ToString(newRecordId)),
                                    //"",

                                    //"",
                                    Helper.HTMLActionString(newRecordId,"Add","Add New Record","fa fa-plus","SaveNewRecord();"),
                    };
                    data.Add(newRecord);
                }
                else
                {
                    //var lstLines = dbAbl.SP_ABL_GET_AUTOBILL_Line(1, 0, "", "", "").ToList();
                    ////var lstLines = dbAbl.SB_ABL_GET_NDE_SUBCATEGORY(1, 0, "", "Id = " + id).Take(1).ToList();

                    //data = (from c in lstLines
                    //select new[] {
                    //        clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//Convert.ToString(c.ROW_NO),
                    //        Helper.GenerateHidden(c.LineId, "LineId", Convert.ToString(id)),
                    //        isReadOnly ? c.BU : Helper.HTMLAutoComplete(c.LineId, "txtBU",WebUtility.HtmlEncode(c.BU),"", false,"","BU",false)+""+Helper.GenerateHidden(c.LineId, "BU",Convert.ToString(c.BU)),
                    //        isReadOnly ? c.SeamNo : Helper.HTMLAutoComplete(c.LineId, "txtSeamNo",WebUtility.HtmlEncode(c.SubCategory),"", false,"","SeamNo",false)+""+Helper.GenerateHidden(c.LineId, "SeamNo",Convert.ToString(c.SeamNo)),
                    //        isReadOnly ? c.StageSequence : Helper.HTMLAutoComplete(c.LineId, "txtStageSequence",WebUtility.HtmlEncode(c.BU),"", false,"","StageSequence",false)+""+Helper.GenerateHidden(c.LineId, "BU",Convert.ToString(c.StageSequence)),
                    //        isReadOnly ? c.QualityProject : Helper.HTMLAutoComplete(c.LineId, "txtQualityProject",WebUtility.HtmlEncode(c.QualityProject),"", false,"","QualityProject",false)+""+Helper.GenerateHidden(c.LineId, "QualityProject",Convert.ToString(c.QualityProject)),
                    //        isReadOnly ? c.QualityProject : Helper.HTMLAutoComplete(c.LineId, "txtQualityProject",WebUtility.HtmlEncode(c.QualityProject),"", false,"","QualityProject",false)+""+Helper.GenerateHidden(c.LineId, "QualityProject",Convert.ToString(c.QualityProject)),
                    //        isReadOnly ? c.Project : Helper.HTMLAutoComplete(c.LineId, "txtProject",WebUtility.HtmlEncode(c.Project),"", false,"","Project",false)+""+Helper.GenerateHidden(c.LineId, "Project",Convert.ToString(c.Project)),
                    //        isReadOnly ? c.CJCNo : Helper.HTMLAutoComplete(c.LineId, "txtCJCNo",WebUtility.HtmlEncode(c.CJCNo),"", false,"","CJCNo",false)+""+Helper.GenerateHidden(c.LineId, "CJCNo",Convert.ToString(c.CJCNo)),
                    //        isReadOnly ? c.SeamLength : Helper.HTMLAutoComplete(c.LineId, "txtSeamLength",WebUtility.HtmlEncode(c.CSeamLengthJCNo),"", false,"","SeamLength",false)+""+Helper.GenerateHidden(c.LineId, "SeamLength",Convert.ToString(c.SeamLength)),
                    //        isReadOnly ? c.NDEType : Helper.HTMLAutoComplete(c.LineId, "txtNDEType",WebUtility.HtmlEncode(c.CSeamLengthJCNo),"", false,"","NDEType",false)+""+Helper.GenerateHidden(c.LineId, "NDEType",Convert.ToString(c.NDEType)),
                    //        isReadOnly ? c.StageCode : Helper.HTMLAutoComplete(c.LineId, "txtStageCode",WebUtility.HtmlEncode(c.StageCode),"", false,"","StageCode",false)+""+Helper.GenerateHidden(c.LineId, "StageCode",Convert.ToString(c.StageCode)),
                    //        isReadOnly ? c.Iteration : Helper.HTMLAutoComplete(c.LineId, "txtIteration",WebUtility.HtmlEncode(c.StageCode),"", false,"","Iteration",false)+""+Helper.GenerateHidden(c.LineId, "Iteration",Convert.ToString(c.Iteration)),
                    //        isReadOnly ? c.Shop : Helper.HTMLAutoComplete(c.LineId, "txtShop",WebUtility.HtmlEncode(c.Shop),"", false,"","Shop",false)+""+Helper.GenerateHidden(c.LineId, "Shop",Convert.ToString(c.Shop)),
                    //        isReadOnly ? c.Location : Helper.HTMLAutoComplete(c.LineId, "txtLocation",WebUtility.HtmlEncode(c.Location),"", false,"","Location",false)+""+Helper.GenerateHidden(c.LineId, "Location",Convert.ToString(c.Location)),
                    //        isReadOnly ? c.StageCode : Helper.HTMLAutoComplete(c.LineId, "txtStageCode",WebUtility.HtmlEncode(c.StageCode),"", false,"","StageCode",false)+""+Helper.GenerateHidden(c.LineId, "StageCode",Convert.ToString(c.StageCode)),


                    //    Helper.HTMLActionString(c.LineId,"Edit","Edit Record","fa fa-pencil-square-o","EnabledEditLine(" + c.LineId + ");") +
                    //        Helper.HTMLActionString(c.LineId,"Update","Update Record","fa fa-floppy-o","EditLine(" + c.LineId + ");","",false,"display:none") + " " +
                    //        Helper.HTMLActionString(c.LineId,"Cancel","Cancel Edit","fa fa-ban","CancelEditLine(" + c.LineId + "); ","",false,"display:none") + " " +
                    //        Helper.HTMLActionString(c.LineId,"Delete","Delete Record","fa fa-trash-o","DeleteLine("+ c.LineId +");"),
                    //    }).ToList();
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SaveAutoBillingData(FormCollection fc, int Id, int HeaderId, string SubCategory, string Vendor, string PONO, int POLINE)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
               // Session["SubCategory"] = SubCategory;
                int seamLenghData = 0;
                bool status = false;
                var obj = dbAbl.ABL001.OrderByDescending(a => a.HeaderId).FirstOrDefault();
                if (fc != null)
                {
                    string BU = fc["txtBU" + Id].ToString().TrimEnd(',');
                    string SeamNo = fc["txtSeamNo" + Id].ToString().TrimEnd(',');
                    string StageSequence = fc["txtStageSequence" + Id].ToString().TrimEnd(',');
                    string QualityProject = fc["txtQualityProject" + Id].ToString().TrimEnd(',');

                    string Project = fc["txtProject" + Id].ToString().TrimEnd(',');
                    Project = Project.ToString().Split('-')[0].Trim().ToString().TrimEnd(',');
                    string CJCNo = fc["txtCJCNo" + Id].ToString().TrimEnd(',');
                    string SeamLength = fc["txtSeamLength" + Id].ToString().TrimEnd(',');
                    if (SeamLength != "")
                    {
                        seamLenghData = Convert.ToInt32(SeamLength);
                    }
                    string NDEType = fc["txtNDEType" + Id].ToString().TrimEnd(',');
                    if (NDEType != "")
                    {
                        NDEType = NDEType.ToString().Split(',')[0].Trim();
                    }
                    string StageCode = fc["txtStageCode" + Id].ToString().TrimEnd(',');
                    string Iteration = fc["txtIteration" + Id].ToString().TrimEnd(',');
                    string Shop = fc["txtShop" + Id].ToString().TrimEnd(',');
                    string Location = fc["txtLocation" + Id].ToString().TrimEnd(',');
                    string InspectionExtent = fc["txtInspectionExtent" + Id].ToString().TrimEnd(',');

                    string ActualNDE = fc["txtActualNDE" + Id].ToString().TrimEnd(','); ;
                    string NDEExecution = fc["txtNDEExecution" + Id].ToString().TrimEnd(',');
                    string NDTRemarks = fc["txtNDTRemarks" + Id].ToString().TrimEnd(',');
                    string OfferedOn = fc["txtOfferedOn" + Id].ToString().TrimEnd(',');
                    string InspectedOn = fc["txtInspectedOn" + Id].ToString().TrimEnd(',');
                    double CalculationAmount = 0;
                    string BillingRemark = fc["txtBillingRemark" + Id].ToString().TrimEnd(',');
                    string NotToBeConsider = fc["txtNotToBeConsider" + Id];
                    if (NotToBeConsider != null)
                    {
                        var dt = fc["txtNotToBeConsider" + Id].ToString().TrimEnd(',');
                        status = true;
                    }
                    else
                    {
                        status = false;
                    }

                   
                    var NDELength = dbAbl.ABL004.Where(x => x.MinimumValue <= seamLenghData && x.MaximumValue >= seamLenghData).Select(x => x.NDELength).FirstOrDefault();
                    int _Poline = Convert.ToInt32(POLINE);
                    var UnitRate = dbAbl.ABL003.Where(x => x.NDELength == NDELength && x.NDEType == NDEType && x.SubCategory == SubCategory && x.Vendor == obj.Vendor && x.PONo == PONO && x.POLine == _Poline).Select(x => x.UnitRate).FirstOrDefault();
                    CalculationAmount = Convert.ToDouble(seamLenghData * UnitRate);
                    var NdeExecution = NDEExecution;
                    if (NdeExecution.ToUpper() == "Double".ToUpper())
                    {
                        CalculationAmount = CalculationAmount * 2;
                    }
                    ABL002 objABL002 = new ABL002();
                    //int MinimumValue = Convert.ToInt32(MinValue);
                    //int MaximumValue = Convert.ToInt32(MaxValue);

                    if (Id == 0)
                    {
                        var obj1 = dbAbl.ABL001.OrderByDescending(a => a.HeaderId).FirstOrDefault();

                        if (obj1 != null)
                        {
                            objABL002.HeaderId = (obj1.HeaderId);
                            objABL002.Project = obj1.Project;
                        }
                        else
                        {
                            objABL002.HeaderId = 1;
                            objABL002.Project = Project;
                        }
                        objABL002.BU = BU;
                        objABL002.SeamNo = SeamNo;
                       
                        if(StageSequence != "")
                        {
                            objABL002.StageSequence = Convert.ToInt32(StageSequence);
                        }
                        else
                        {
                            objABL002.StageSequence = null;
                        }
                        //objABL002.StageSequence = StageSequence != null ? Convert.ToInt32(StageSequence) : 0;
                        objABL002.QualityProject = QualityProject;
                        objABL002.SeamLength = SeamLength != "" ? Convert.ToInt32(SeamLength) : 0;
                        objABL002.NDEType = NDEType;
                        objABL002.StageCode = StageCode;
                        objABL002.Iteration = Iteration != "" ? Convert.ToInt32(Iteration) : 0;
                        objABL002.Shop = Shop;
                        objABL002.Location = Location;
                        objABL002.InspectionExtent = InspectionExtent;
                        objABL002.ActualNDE = ActualNDE;
                        objABL002.NDEExecution = NDEExecution;
                        objABL002.NDTRemarks = NDTRemarks;
                        if (OfferedOn != "")
                        {
                            objABL002.OfferedOn = Convert.ToDateTime(OfferedOn);
                        }
                        else
                        {
                            objABL002.OfferedOn = null;
                        }

                        if (InspectedOn != "")
                        {
                            objABL002.InspectedOn = Convert.ToDateTime(InspectedOn);
                        }
                        else
                        {
                            objABL002.InspectedOn = null;
                        }
                        // objABL002.InspectedOn = Convert.ToDateTime(InspectedOn);
                        objABL002.CalculationAmount = Iteration != "" ? Convert.ToDecimal(CalculationAmount) : 0 ;
                        objABL002.NotToBeConsider = status;
                        objABL002.CreatedBy = obj.CreatedBy;
                        objABL002.CreatedDate = DateTime.Now;
                        dbAbl.ABL002.Add(objABL002);
                        dbAbl.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                    }
                    else
                    {
                        //    if (!dbAbl.ABL006.Any(x => x.NDEType == NDEType && x.SubCategory == SubCategory && x.ID != Id))
                        //        if (true)
                        //        {
                        //            objABL006 = new ABL006();
                        //            objABL006.ID = Convert.ToInt32(8);
                        //            objABL006.NDEType = NDEType;
                        //            objABL006.SubCategory = SubCategory;
                        //            dbAbl.ABL006.Add(objABL006);

                        //            dbAbl.SaveChanges();
                        //            objResponseMsg.Key = true;
                        //            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                        //        }
                        //        else
                        //        {
                        //            objResponseMsg.Key = false;
                        //            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                        //        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }


        public ActionResult GETNDESubCategoryResult(string param="")
        {
            var lstPO = new List<SelectItemList>();
            string query = string.Empty;
            if (!string.IsNullOrWhiteSpace(param))
            {

                query = "Select SubCategory as id,SubCategory as text from [dbo].[ABL006] Where NDEType like '%" + param + "%' order by SubCategory";
                lstPO = db.Database.SqlQuery<SelectItemList>(query).Distinct().Take(20).ToList();
            }

            return Json(lstPO, JsonRequestBehavior.AllowGet);
        }

        public List<string> NDEExecutionList()
        {
            List<string> items = clsImplementationEnum.GetListOfDescription<clsImplementationEnum.NDEExecution>();
            return items;
        }
    }
}
