﻿using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQS.Areas.Utility.Models;
using static IEMQS.Areas.HTC.Controllers.MaintainHTCController;

namespace IEMQS.Areas.PLN.Controllers
{
    public class MaintainEquipmentController : clsBase
    {
        //modified by nikita on 29/08/2017
        //task assigned by Satish Pawar
        //added method updatepdn002
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadEquipmentListDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_EquipmentListDataPartial");
        }

        [HttpPost]
        public JsonResult LoadEquipmentHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.PlanStatus.DRAFT.GetStringValue() + "','" + clsImplementationEnum.PlanStatus.Returned.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "1=1";
                }
                //strWhere += " and pln011.CreatedBy in('" + objClsLoginInfo.UserName + "')";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "pln011.Project+' - '+com1.t_dsca",
                                                "Document",
                                                "pln011.Customer+' - '+com6.t_nama",
                                                "Status",
                                                "Product",
                                                "ProcessLicensor",
                                                "ProcessPlan",
                                                "JobNo"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstResult = db.SP_FETCH_EQUIPMENTHANDLING_HEADERS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString(uc.Customer),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.CDD),
                               Convert.ToString(uc.ProcessPlan),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn),
                               Convert.ToString(uc.EditedBy),
                               Convert.ToString(uc.EditedOn),
                                    Convert.ToString(uc.SubmittedBy),
                                Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),

                                Convert.ToString(uc.ApprovedBy),
                               Convert.ToString(uc.ApprovedOn),
                               Convert.ToString(uc.ReturnRemark),
                               //Convert.ToString(uc.HeaderId)
                                "<nobr><center>"+"<a class=\"iconspace\" href=\""+WebsiteURL+"/PLN/MaintainEquipment/CreateEquipment?HeaderID=" + uc.HeaderId + "\"><i class=\"fa fa-eye\"></i></a>"+
                                Helper.GenerateActionIcon(uc.HeaderId,"Delete","Delete Record","fa fa-trash-o", "DeleteDocument("+ uc.HeaderId +",'/PLN/MaintainEquipment/DeleteHeader', {headerid:"+uc.HeaderId+"}, 'tblEquipmentHeader')","",  (( uc.RevNo >0 && uc.Status != clsImplementationEnum.CommonStatus.SendForApprovel.GetStringValue()) || ( uc.RevNo == 0 && uc.Status == clsImplementationEnum.CommonStatus.Approved.GetStringValue()) ) ? false:true) +
                                "<i class='iconspace fa fa-clock-o' title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintainEquipment/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')></i>" +
                                (uc.RevNo > 0 ? "<i  title=\"History\" class=\"iconspace fa fa-history\" onClick=\"ViewHistoryForProject(" + uc.HeaderId + ")\"></i>" : "<i  title=\"History\" class=\"disabledicon fa fa-history\" \"></i>") +
                                //"<i title=\"Print Report\" class=\"iconspace fa fa-print\" onClick=\"PrintReport(" + uc.HeaderId + ")\"></i> " +
                                "</center></nobr>"
                               // "<a title='View' href='/PLN/MaintainEquipment/CreateEquipment?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a>"+(uc.RevNo>0 ?"<a title=\"History\" onclick=\"ViewHistoryForProjectPLN('"+uc.HeaderId+"','Initiator','/PLN/MaintainEquipment/GetHistoryDetails','Equipment Handling')\"><i style='margin-left:5px;' class='fa fa-history'></i></a>":"")+"<a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintainEquipment/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')><i style='margin-left:5px;cursor:pointer;' class='fa fa-clock-o'></i></a>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult DeleteHeader(int headerid)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                objResponseMsg = Manager.DeletePDINDocument(headerid, clsImplementationEnum.PlanList.Equipment_Handling_Plan);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ShowHistoryTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();

            model.TimelineTitle = "Equipment Handling TimeLine";
            model.Title = "PDinDoc";
            if (HeaderId > 0)
            {

                PLN011_Log objPLN011_Log = db.PLN011_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN011_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN011_Log.CreatedBy) : null;
                model.CreatedOn = objPLN011_Log.CreatedOn;
                model.EditedBy = objPLN011_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN011_Log.EditedBy) : null;
                model.EditedOn = objPLN011_Log.EditedOn;
                model.SubmittedBy = objPLN011_Log.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objPLN011_Log.SubmittedBy) : null;
                model.SubmittedOn = objPLN011_Log.SubmittedOn;

                model.ApprovedBy = objPLN011_Log.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objPLN011_Log.ApprovedBy) : null;
                model.ApprovedOn = objPLN011_Log.ApprovedOn;

            }
            else
            {

                PLN011_Log objPLN011_Log = db.PLN011_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN011_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN011_Log.CreatedBy) : null;
                model.CreatedOn = objPLN011_Log.CreatedOn;
                model.EditedBy = objPLN011_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN011_Log.EditedBy) : null;
                model.EditedOn = objPLN011_Log.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.Title = "PDinDoc";
            model.TimelineTitle = "Equipment Timeline";

            if (HeaderId > 0)
            {
                PLN011 objPLN011 = db.PLN011.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN011.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN011.CreatedBy) : null;
                model.CreatedOn = objPLN011.CreatedOn;
                model.EditedBy = objPLN011.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN011.EditedBy) : null;
                model.EditedOn = objPLN011.EditedOn;
                model.SubmittedBy = objPLN011.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objPLN011.SubmittedBy) : null;
                model.SubmittedOn = objPLN011.SubmittedOn;
                model.ApprovedBy = objPLN011.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objPLN011.ApprovedBy) : null;
                model.ApprovedOn = objPLN011.ApprovedOn;

            }
            else
            {
                PLN011 objPLN011 = db.PLN011.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN011.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN011.CreatedBy) : null;
                model.CreatedOn = objPLN011.CreatedOn;
                model.EditedBy = objPLN011.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN011.EditedBy) : null;
                model.EditedOn = objPLN011.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }

        [SessionExpireFilter]//, AllowAnonymous, UserPermissions
        public ActionResult CreateEquipment(int HeaderID = 0)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("PLN011");
            if (HeaderID > 0)
            {
                //string currentUser = objClsLoginInfo.UserName;
                //PLN011 objPLN011 = db.PLN011.Where(x => x.HeaderId == HeaderID && x.CreatedBy == currentUser).FirstOrDefault();
                //if (objPLN011 == null)
                //{
                //    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                //}
            }
            ViewBag.HeaderID = HeaderID;
            return View();
        }

        [HttpPost]
        public ActionResult LoadEquipmentCreateFormPartial(int HeaderID = 0)
        {
            //ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("PLN011");
            PLN011 objPLN011 = db.PLN011.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
            if (objPLN011 != null)
            {
                var projectDetails = db.COM001.Where(x => x.t_cprj == objPLN011.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                ViewBag.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string ApproverName = db.COM003.Where(x => x.t_psno == objPLN011.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                if (!string.IsNullOrEmpty(ApproverName))
                {
                    ViewBag.ApproverName = objPLN011.ApprovedBy + " - " + ApproverName;
                }
                string customerName = db.COM006.Where(x => x.t_bpid == objPLN011.Customer).Select(x => x.t_nama).FirstOrDefault();
                ViewBag.customerName = objPLN011.Customer + " - " + customerName;
                string strEquipment = clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments1("PLN011/" + objPLN011.HeaderId + "/" + objPLN011.RevNo));
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strEquipment &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();
                if (latestRecord != null)
                {
                    PLN012 objPLN012 = db.PLN012.Where(x => x.HeaderId == objPLN011.HeaderId).FirstOrDefault();
                    if (objPLN012 != null)
                    {
                        if (latestRecord.RevNo == objPLN012.PlanRevNo)
                        {
                            ViewBag.isLatest = "true";
                        }
                        else
                        {
                            ViewBag.isLatest = "false";
                        }
                    }
                    else
                    {
                        ViewBag.isLatest = "false";
                    }
                }
                else
                {
                    ViewBag.isLatest = "true";
                }

                var PlanningDinID = db.PDN002.Where(x => x.RefId == HeaderID && x.DocumentNo == objPLN011.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;

                ViewBag.IsReviseBtnEnabled = Manager.IsReviseEnabled(PlanningDinID, objPLN011.HeaderId, objPLN011.Document);
                ViewBag.DocMessage = clsImplementationMessage.CommonMessages.DocMessage.ToString();
            }
            else
            {
                objPLN011 = new PLN011();
                objPLN011.ProcessPlan = clsImplementationEnum.PlanList.Equipment_Handling_Plan.GetStringValue();
                ViewBag.isLatest = "true";
                objPLN011.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                objPLN011.RevNo = 0;
            }
            ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            return PartialView("_LoadEquipmentCreateFormPartial", objPLN011);
        }

        [HttpPost]
        public JsonResult LoadCheckListData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                PLN011 objPLN011 = db.PLN011.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                string strEquipment = clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strEquipment &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();

                if (latestRecord == null)
                {
                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = 0,
                        iTotalDisplayRecords = 0,
                        aaData = new string[] { }
                    }, JsonRequestBehavior.AllowGet);
                }
                int? PlanRev = 0;
                if (HeaderId > 0)
                {
                    strWhere += "pln012.HeaderId = " + HeaderId + " and ";
                    PLN012 objPLN012 = db.PLN012.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    if (objPLN012 != null)
                    {
                        PlanRev = objPLN012.PlanRevNo;
                    }
                    else
                    {
                        PlanRev = 0;
                    }
                }
                else
                {
                    PlanRev = latestRecord.RevNo;
                }
                strWhere += "1=1 and pln002.[Plan] in('" + clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue() + "') and pln002.RevNo = " + PlanRev + "";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = { "RelatedTo", "[CheckPoint]" };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                var strSortOrder = string.Empty;
                var lstResult = db.SP_FETCH_EQUIPMENTHANDLING_HEADER_LINE
                                (
                                     HeaderId,
                                     latestRecord.RevNo,
                                     StartIndex,
                                     EndIndex,
                                     strSortOrder,
                                    (!string.IsNullOrWhiteSpace(strWhere) ? strWhere : null)
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.RelatedTo),
                                Convert.ToString(uc.CheckPointDesc),
                                //LineOperation(Convert.ToBoolean(uc.Yes_No),uc.LineId,uc.HeaderId),
                                Manager.CheckListLineOperationYesNo(uc.Yes_No,uc.LineId,uc.HeaderId,false,uc.ActualId),
                                Manager.CheckListLineOperationRemarks(uc.Remarks,uc.LineId,uc.HeaderId,false),
                                Convert.ToString(uc.CreatedBy),
                                Convert.ToString(uc.CreatedOn),
                                Convert.ToString(uc.EditedBy),
                                Convert.ToString(uc.EditedOn),
                                getChecklistDetails(uc.CheckListId.ToString())[0],
                                getChecklistDetails(uc.CheckListId.ToString())[1],
                                getChecklistDetails(uc.CheckListId.ToString())[2],
                                getChecklistDetails(uc.CheckListId.ToString())[3],
                                Convert.ToString(uc.IsLatest),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [NonAction]
        public string[] getChecklistDetails(string lineId)
        {
            string[] checklist = new string[4];
            int LineID = Int32.Parse(lineId);
            var data = (from p2 in db.PLN002
                        join
                            p1 in db.PLN001 on p2.HeaderId equals p1.HeaderId
                        where p2.LineId == LineID
                        select p1).FirstOrDefault();
            if (data != null)
            {
                checklist[0] = Manager.GetUserNameFromPsNo(data.CreatedBy); //prepared by
                checklist[1] = Manager.GetUserNameFromPsNo(data.ApprovedBy); // approved by
                checklist[2] = data.ApprovedOn.ToString(); // approved on
                checklist[3] = data.Document; // document no
            }
            return checklist;
        }

        [HttpPost]
        public ActionResult SaveHeader(PLN011 pln011, string strHeaderId, string strLineId, string strIsLatest)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                int NewHeaderId = 0;
                if (pln011.HeaderId > 0)
                {
                    PLN011 objPLN011 = db.PLN011.Where(x => x.HeaderId == pln011.HeaderId).FirstOrDefault();
                    if (objPLN011 != null)
                    {
                        objPLN011.Product = pln011.Product;
                        objPLN011.ProcessLicensor = pln011.ProcessLicensor;
                        objPLN011.JobNo = pln011.JobNo;
                        objPLN011.ReviseRemark = pln011.ReviseRemark;
                        objPLN011.ApprovedBy = pln011.ApprovedBy;
                        objPLN011.EditedBy = objClsLoginInfo.UserName;
                        objPLN011.EditedOn = DateTime.Now;
                        if (objPLN011.Status == clsImplementationEnum.PlanStatus.Approved.GetStringValue())
                        {
                            objPLN011.RevNo = Convert.ToInt32(objPLN011.RevNo) + 1;
                            objPLN011.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                            objPLN011.ReturnRemark = null;
                            objPLN011.ApprovedOn = null;
                        }                      
                        if (!Convert.ToBoolean(strIsLatest))
                        {
                            List<PLN012> lstPLN012 = db.PLN012.Where(x => x.HeaderId == objPLN011.HeaderId).ToList();
                            if (lstPLN012 != null && lstPLN012.Count > 0)
                            {
                                db.PLN012.RemoveRange(lstPLN012);
                            }
                            string strEquipment = clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue();
                            string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                            var latestRecord = db.PLN001
                                     .Where(x
                                                => x.Plan == strEquipment &&
                                                   x.Status == strStatus
                                            )
                                     .OrderByDescending(x => x.HeaderId)
                                     .Select(x => new { x.HeaderId, x.RevNo })
                                     .FirstOrDefault();
                            List<PLN002> lstPLN002 = db.PLN002.Where(x => x.HeaderId == latestRecord.HeaderId).ToList();
                            if (lstPLN002 != null && lstPLN002.Count > 0)
                            {
                                List<PLN012> NewlstPLN012 = lstPLN002.Select(x => new PLN012()
                                {
                                    LineId = x.LineId,
                                    HeaderId = objPLN011.HeaderId,
                                    Project = objPLN011.Project,
                                    Document = objPLN011.Document,
                                    RevNo = objPLN011.RevNo,
                                    Plan = objPLN011.ProcessPlan,
                                    PlanRevNo = x.RevNo,
                                    CheckListId = x.LineId,
                                    //Yes_No = false,
                                    //Yes_No = clsImplementationEnum.ChecklistYesNo.No.GetStringValue(), as per observation 15003
                                    Yes_No = string.Empty,
                                    CreatedBy = objClsLoginInfo.UserName,
                                    CreatedOn = DateTime.Now
                                }).ToList();
                                db.PLN012.AddRange(NewlstPLN012);
                            }
                        }
                        else
                        {
                            List<PLN012> lstPLN012 = db.PLN012.Where(x => x.HeaderId == objPLN011.HeaderId).ToList();
                            if (lstPLN012.Count > 0)
                            {
                                List<PLN012> lstTruePLN012 = new List<PLN012>();

                                foreach (PLN012 objPLN012 in lstPLN012)
                                {
                                    //objPLN012.Yes_No = (!string.IsNullOrWhiteSpace(strLineId) && Array.ConvertAll(strLineId.Split(',').ToArray(), s => int.Parse(s)).Length > 0 && Array.ConvertAll(strLineId.Split(',').ToArray(), s => int.Parse(s)).Contains(objPLN012.LineId) ? true : false);
                                    objPLN012.EditedBy = objClsLoginInfo.UserName;
                                    objPLN012.EditedOn = DateTime.Now;
                                }
                            }
                        }
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.PlanMessages.Update.ToString();
                        NewHeaderId = objPLN011.HeaderId;
                        objResponseMsg.Status = objPLN011.Status;
                        objResponseMsg.Revision = objPLN011.RevNo;
                        objResponseMsg.HeaderID = NewHeaderId;

                        //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                        Manager.UpdatePDN002(objPLN011.HeaderId, objPLN011.Status, objPLN011.RevNo, objPLN011.Project, objPLN011.Document);


                        //if (hasAttachments)
                        //{
                        var folderPath = "PLN011/" + NewHeaderId + "/" + objPLN011.RevNo;
                        //    var existing = clsUpload.getDocs(folderPath);
                        //    var toDelete = new Dictionary<string, string>();
                        //    foreach (var item in existing)
                        //    {
                        //        if (Attach.Where(q => q.Key.Equals(item.Key)).Count() <= 0)
                        //            toDelete.Add(item.Key, item.Value);
                        //        else
                        //        {
                        //            if (Attach.Where(q => q.Key.Equals(item.Key)).FirstOrDefault().Value.Substring(0, 3) != "URL")
                        //                toDelete.Add(item.Key, item.Value);
                        //        }
                        //    }
                        //    foreach (var item in toDelete)
                        //        clsUpload.DeleteFile(folderPath, item.Key);
                        //    var toUpload = Attach.Where(q => q.Value.Substring(0, 3) != "URL");

                        //    foreach (var attch in toUpload)
                        //    {
                        //        var base64Data = attch.Value;
                        //        var dataBytes = Helper.FromBase64(attch.Value);
                        //        //var dataBytes = Encoding.ASCII.GetBytes(stringData);
                        //        clsUpload.Upload(attch.Key, dataBytes, folderPath);
                        //    }
                        //}
                        //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Process Details not available.";
                    }
                }
                else
                {
                    PLN011 objPLN011 = db.PLN011.Add(new PLN011
                    {
                        Project = pln011.Project,
                        Document = pln011.Document,
                        Customer = pln011.Customer,
                        RevNo = 0,
                        Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                        CDD = Helper.ToNullIfTooEarlyForDb(pln011.CDD),
                        Product = pln011.Product,
                        ProcessLicensor = pln011.ProcessLicensor,
                        ProcessPlan = pln011.ProcessPlan,
                        JobNo = pln011.JobNo,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now,
                        ApprovedBy = pln011.ApprovedBy
                    });
                    db.SaveChanges();
                    NewHeaderId = objPLN011.HeaderId;
                    if (!string.IsNullOrWhiteSpace(strHeaderId))
                    {
                        int planHeaderId = Convert.ToInt32(strHeaderId);
                        List<PLN002> lstPLN002 = db.PLN002.Where(x => x.HeaderId == planHeaderId).ToList();
                        List<PLN012> lstPLN012 = new List<PLN012>();
                        if (lstPLN002.Count > 0)
                        {
                            List<PLN002> lstTruePLN002 = new List<PLN002>();
                            if (!string.IsNullOrWhiteSpace(strLineId))
                            {
                                int[] arrayLineId = Array.ConvertAll(strLineId.Split(',').ToArray(), s => int.Parse(s));
                                lstTruePLN002 = lstPLN002.Where(x => arrayLineId.Contains(x.LineId)).ToList();
                            }
                            foreach (PLN002 objPLN002 in lstPLN002)
                            {
                                PLN012 objPLN012 = new PLN012();
                                objPLN012.HeaderId = objPLN011.HeaderId;
                                objPLN012.Project = objPLN011.Project;
                                objPLN012.Document = objPLN011.Document;
                                objPLN012.RevNo = objPLN011.RevNo;
                                objPLN012.Plan = objPLN011.ProcessPlan;
                                objPLN012.PlanRevNo = objPLN002.RevNo;
                                objPLN012.CheckListId = objPLN002.LineId;
                                //objPLN012.Yes_No = (lstTruePLN002.Where(x => x.LineId == objPLN002.LineId).FirstOrDefault() != null ? true : false);
                                objPLN012.CreatedBy = objClsLoginInfo.UserName;
                                objPLN012.CreatedOn = DateTime.Now;
                                lstPLN012.Add(objPLN012);
                            }
                        }
                        if (lstPLN012 != null && lstPLN012.Count > 0)
                        {
                            db.PLN012.AddRange(lstPLN012);
                            db.SaveChanges();
                        }
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PlanMessages.Insert.ToString();
                    objResponseMsg.Status = objPLN011.Status;
                    objResponseMsg.Revision = objPLN011.RevNo;
                    objResponseMsg.HeaderID = NewHeaderId;

                    //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                    Manager.UpdatePDN002(objPLN011.HeaderId, objPLN011.Status, objPLN011.RevNo, objPLN011.Project, objPLN011.Document);


                    //if (hasAttachments)
                    //{
                    var folderPath = "PLN011/" + NewHeaderId + "/" + objPLN011.RevNo;
                    //    var existing = clsUpload.getDocs(folderPath);
                    //    var toDelete = new Dictionary<string, string>();
                    //    foreach (var item in existing)
                    //    {
                    //        if (Attach.Where(q => q.Key.Equals(item.Key)).Count() <= 0)
                    //            toDelete.Add(item.Key, item.Value);
                    //        else
                    //        {
                    //            if (Attach.Where(q => q.Key.Equals(item.Key)).FirstOrDefault().Value.Substring(0, 3) != "URL")
                    //                toDelete.Add(item.Key, item.Value);
                    //        }
                    //    }
                    //    foreach (var item in toDelete)
                    //        clsUpload.DeleteFile(folderPath, item.Key);
                    //    var toUpload = Attach.Where(q => q.Value.Substring(0, 3) != "URL");
                    //    foreach (var attch in toUpload)
                    //    {
                    //        var base64Data = attch.Value;
                    //        var dataBytes = Helper.FromBase64(attch.Value);
                    //        //var dataBytes = Encoding.ASCII.GetBytes(stringData);
                    //        clsUpload.Upload(attch.Key, dataBytes, folderPath);
                    //    }
                    //}
                    //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveHeaderLine(int LineId, int HeaderId, string ActualId, string YesNo, string Remarks)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                var splitid = ActualId.Split(',');
                if (Convert.ToInt32(splitid[0]) == 0)
                {
                    PLN012 objPLN012 = db.PLN012.Where(x => x.CheckListId == LineId && x.HeaderId == HeaderId).FirstOrDefault();
                    objPLN012.Yes_No = YesNo;
                    objPLN012.Remarks = Remarks;
                    objPLN012.EditedBy = objClsLoginInfo.UserName;
                    objPLN012.EditedOn = DateTime.Now;
                    db.SaveChanges();
                }
                else
                {
                    int ActualLineId = Convert.ToInt32(splitid[0]);
                    PLN012 objPLN012 = db.PLN012.Where(x => x.LineId == ActualLineId && x.HeaderId == HeaderId).FirstOrDefault();
                    objPLN012.Yes_No = YesNo;
                    objPLN012.Remarks = Remarks;
                    objPLN012.EditedBy = objClsLoginInfo.UserName;
                    objPLN012.EditedOn = DateTime.Now;
                    db.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SendForApproval(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrLineStatus = { clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(), clsImplementationEnum.PlanStatus.Returned.GetStringValue() };
                PLN011 objPLN011 = db.PLN011.Where(x => x.HeaderId == HeaderId && arrLineStatus.Contains(x.Status)).FirstOrDefault();
                if (objPLN011 != null)
                {                  
                    objPLN011.Status = clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue();
                    objPLN011.SubmittedBy = objClsLoginInfo.UserName;
                    objPLN011.SubmittedOn = DateTime.Now;

                    //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                    Manager.UpdatePDN002(objPLN011.HeaderId, objPLN011.Status, objPLN011.RevNo, objPLN011.Project, objPLN011.Document);
                    //string unReleasedStatus = clsImplementationEnum.PlanningDinStatus.UNRELEASED.GetStringValue();
                    //PDN002 objPDN002 = db.PDN002.Where(x => x.RefId == objPLN011.HeaderId && x.Status == unReleasedStatus).FirstOrDefault();
                    //if (objPDN002 != null)
                    //{
                    //    objPDN002.RevNo = objPLN011.RevNo;
                    //    objPDN002.Status = objPLN011.Status;
                    //}
                    //--

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Checklist successfully sent for approval.";

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PLNG1.GetStringValue(),
                                                        objPLN011.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objPLN011.Project, clsImplementationEnum.PlanList.Equipment_Handling_Plan.GetStringValue(), objPLN011.RevNo.Value.ToString(), objPLN011.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Equipment_Handling_Plan.GetStringValue(), objPLN011.HeaderId.ToString(), true),
                                                        objPLN011.ApprovedBy);
                    #endregion
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Checklist not available for approval.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult RetractStatus(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PLN011 objPLN011 = db.PLN011.Where(u => u.HeaderId == headerId).SingleOrDefault();
                if (objPLN011 != null)
                {                   
                    objPLN011.Status = clsImplementationEnum.PLCStatus.DRAFT.GetStringValue();
                    objPLN011.EditedBy = objClsLoginInfo.UserName;
                    objPLN011.EditedOn = DateTime.Now;
                    objPLN011.SubmittedOn = null;
                    objPLN011.SubmittedBy = null;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Retract.ToString();
                    Manager.UpdatePDN002(objPLN011.HeaderId, objPLN011.Status, objPLN011.RevNo, objPLN011.Project, objPLN011.Document);
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public string LineOperation(bool flag, int LineId, int HeaderId)
        {
            if (flag)
            {
                return "<label class=\"mt-checkbox mt-checkbox-outline\">" +
                       "<input type=\"checkbox\" name=\"chkYesNo\" checked=\"checked\" sHeaderId=\"" + HeaderId + "\" sLineId=\"" + LineId + "\" />" +
                       "<span></ span >" +
                       "</label>";
            }
            else
            {
                return "<label class=\"mt-checkbox mt-checkbox-outline\">" +
                       "<input type=\"checkbox\" name=\"chkYesNo\" sHeaderId=\"" + HeaderId + "\" sLineId=\"" + LineId + "\" />" +
                       "<span></ span >" +
                       "</label>";
            }
        }


        [HttpPost]
        public ActionResult GetCustomerProjectWise(string projectCode)
        {
            ApproverModel objApproverModel = Manager.GetCustomerProjectWise(projectCode);
            string strCdd = Manager.GetContractWiseCdd(projectCode);
            if (!string.IsNullOrWhiteSpace(strCdd))
            {
                DateTime dtCdd = Convert.ToDateTime(strCdd);
                objApproverModel.Cdd = dtCdd.Date.ToShortDateString();
            }
            return Json(objApproverModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetHistoryDetails(string strRole, string HeaderId)
        {
            ViewBag.Role = strRole;
            ViewBag.HeaderId = HeaderId;
            return PartialView("_GetHistoryDetailsPartial");
        }

        [HttpPost]
        public JsonResult LoadEquipmentHistory(JQueryDataTableParamModel param, string HeaderId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                //if (param.CTQCompileStatus.ToUpper() == "INITIATOR")
                //{
                //    strWhere += "1=1  and pln011.CreatedBy = '" + objClsLoginInfo.UserName + "'";

                //}
                //else
                //{
                //    strWhere += "1=1 and pln011.ApprovedBy = '" + objClsLoginInfo.UserName + "'";
                //}
                if (!string.IsNullOrWhiteSpace(HeaderId))
                {
                    strWhere += " pln011.HeaderId = '" + HeaderId + "'";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "pln011.Project+' - '+com1.t_dsca",
                                                "Document",
                                                "pln011.Customer+' - '+com6.t_nama",
                                                "Status",
                                                "Product",
                                                "ProcessLicensor",
                                                "ProcessPlan",
                                                "JobNo"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                var strSortOrder = string.Empty;
                var lstResult = db.SP_FETCH_EQUIPMENTHANDLING_HEADERS_HISTORY
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString(uc.Customer),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.CDD),
                               Convert.ToString(uc.ProcessPlan),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn),
                               Convert.ToString(uc.EditedBy),
                               Convert.ToString(uc.EditedOn),
                                    Convert.ToString(uc.SubmittedBy),
                                Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),

                                Convert.ToString(uc.ApprovedBy),
                               Convert.ToString(uc.ApprovedOn),
                               Convert.ToString(uc.ReturnRemark),
                               (param.CTQCompileStatus.ToUpper() == "INITIATOR"?"<center><a title='View' href='"+WebsiteURL+"/PLN/MaintainEquipment/ViewHistory?Id="+Convert.ToInt32(uc.Id)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a><a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintainEquipment/ShowHistoryTimeline?HeaderID=" + Convert.ToInt32(uc.Id) + "')><i style = 'margin-left:5px;'  class='fa fa-clock-o'></i></a></center>":"<center><a title='View' href='"+WebsiteURL+"/PLN/ApproveEquipment/ViewHistory?Id="+Convert.ToInt32(uc.Id)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a><a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintainEquipment/ShowHistoryTimeline?HeaderID=" + Convert.ToInt32(uc.Id) + "')><i style = 'margin-left:5px;'  class='fa fa-clock-o'></i></a></center>")
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]//, AllowAnonymous, UserPermissions
        public ActionResult ViewHistory(int Id = 0)
        {
            if (Id > 0)
            {
                //string currentUser = objClsLoginInfo.UserName;
                //PLN011_Log objPLN011_Log = db.PLN011_Log.Where(x => x.Id == Id && x.CreatedBy == currentUser).FirstOrDefault();
                //if (objPLN011_Log == null)
                //{
                //    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                //}
            }
            ViewBag.Id = Id;
            return View();
        }

        [HttpPost]
        public ActionResult LoadEquipmentHistoryFormPartial(int Id = 0)
        {
            PLN011_Log objPLN011_Log = db.PLN011_Log.Where(x => x.Id == Id).FirstOrDefault();
            if (objPLN011_Log != null)
            {
                var projectDetails = db.COM001.Where(x => x.t_cprj == objPLN011_Log.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                objPLN011_Log.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string ApproverName = db.COM003.Where(x => x.t_psno == objPLN011_Log.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                objPLN011_Log.ApprovedBy = objPLN011_Log.ApprovedBy + " - " + ApproverName;
                string customerName = db.COM006.Where(x => x.t_bpid == objPLN011_Log.Customer).Select(x => x.t_nama).FirstOrDefault();
                objPLN011_Log.Customer = objPLN011_Log.Customer + " - " + customerName;
                string strEquipment = clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strEquipment &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();
                if (latestRecord != null)
                {
                    //if (latestRecord.HeaderId == objPLN003.HeaderId)
                    //{

                    //}
                }
                ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments1("PLN011/" + objPLN011_Log.HeaderId + "/" + objPLN011_Log.RevNo));
            }
            else
            {
                objPLN011_Log = new PLN011_Log();
                objPLN011_Log.ProcessPlan = clsImplementationEnum.PlanList.Equipment_Handling_Plan.GetStringValue();
                objPLN011_Log.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                objPLN011_Log.RevNo = 0;
            }
            // ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            //var urlPrefix = "http://" + Request.Url.Authority + "/PLN/MaintainEquipment/ViewHistory?Id=";// AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.LastIndexOf('=') + 1);
            var urlPrefix = WebsiteURL + "/PLN/MaintainEquipment/ViewHistory?Id=";
            if (!objClsLoginInfo.GetUserRoleList().Contains("SHOP"))
            {
                ViewBag.RevPrev = (db.PLN011_Log.Any(q => (q.HeaderId == objPLN011_Log.HeaderId && q.RevNo == (objPLN011_Log.RevNo - 1))) ? urlPrefix + db.PLN011_Log.Where(q => (q.HeaderId == objPLN011_Log.HeaderId && q.RevNo == (objPLN011_Log.RevNo - 1))).FirstOrDefault().Id : null);
                ViewBag.RevNext = (db.PLN011_Log.Any(q => (q.HeaderId == objPLN011_Log.HeaderId && q.RevNo == (objPLN011_Log.RevNo + 1))) ? urlPrefix + db.PLN011_Log.Where(q => (q.HeaderId == objPLN011_Log.HeaderId && q.RevNo == (objPLN011_Log.RevNo + 1))).FirstOrDefault().Id : null);
            }
            return PartialView("_LoadEquipmentHistoryFormPartial", objPLN011_Log);
        }

        [HttpPost]
        public JsonResult LoadCheckListHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                int Id = Convert.ToInt32(param.CTQHeaderId);
                PLN011_Log objPLN011_Log = db.PLN011_Log.Where(x => x.Id == Id).FirstOrDefault();

                strWhere += "1=1 and pln012.RefId = " + objPLN011_Log.Id + "";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = { "RelatedTo", "[CheckPoint]" };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                var strSortOrder = string.Empty;
                var lstResult = db.SP_FETCH_EQUIPMENTHANDLING_HEADER_LINE_HISTORY
                                (
                                     objPLN011_Log.HeaderId,
                                     StartIndex,
                                     EndIndex,
                                     strSortOrder,
                                    (!string.IsNullOrWhiteSpace(strWhere) ? strWhere : null)
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.RelatedTo),
                                Convert.ToString(uc.CheckPointDesc),
                                //LineOperation(Convert.ToBoolean(uc.Yes_No),uc.LineId,uc.HeaderId),
                                Manager.CheckListLineOperationYesNo(uc.Yes_No,uc.LineId,uc.HeaderId,true,uc.ActualId),
                                Manager.CheckListLineOperationRemarks(uc.Remarks,uc.LineId,uc.HeaderId,true),
                                Convert.ToString(uc.CreatedBy),
                                Convert.ToString(uc.CreatedOn),
                                Convert.ToString(uc.EditedBy),
                                Convert.ToString(uc.EditedOn),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public bool checkProjectExist(string projectcode)
        {
            bool Flag = false;
            if (!string.IsNullOrWhiteSpace(projectcode))
            {
                PLN011 objPLN011 = db.PLN011.Where(x => x.Project == projectcode).FirstOrDefault();
                if (objPLN011 != null)
                {
                    Flag = true;
                }
            }
            return Flag;
        }

        #region Inline Grid New Method
        [HttpPost]
        public JsonResult LoadCheckListDataNew(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                PLN011 objPLN011 = db.PLN011.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                string HeaderStatus = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                bool IsDisable = false;
                if (objPLN011 != null)
                {
                    HeaderStatus = objPLN011.Status;
                }
                if (HeaderId == 0 || (HeaderId > 0 && (HeaderStatus.ToLower() == clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue().ToLower() || HeaderStatus.ToLower() == clsImplementationEnum.PlanStatus.Approved.GetStringValue().ToLower())))
                {
                    IsDisable = true;
                }
                string strEquipment = clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strEquipment &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();

                if (latestRecord == null)
                {
                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = 0,
                        iTotalDisplayRecords = 0,
                        aaData = new string[] { }
                    }, JsonRequestBehavior.AllowGet);
                }
                int? PlanRev = 0;
                if (HeaderId > 0)
                {
                    strWhere += "pln012.HeaderId = " + HeaderId + " and ";
                    PLN012 objPLN012 = db.PLN012.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    if (objPLN012 != null)
                    {
                        PlanRev = objPLN012.PlanRevNo;
                    }
                    else
                    {
                        PlanRev = 0;
                    }
                }
                else
                {
                    PlanRev = latestRecord.RevNo;
                }
                strWhere += "1=1 and pln002.[Plan] in('" + clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue() + "') and pln002.RevNo = " + PlanRev + "";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = { "RelatedTo", "[CheckPoint]" };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                var strSortOrder = string.Empty;
                var lstResult = db.SP_FETCH_EQUIPMENTHANDLING_HEADER_LINE
                                (
                                     HeaderId,
                                     latestRecord.RevNo,
                                     StartIndex,
                                     EndIndex,
                                     strSortOrder,
                                    (!string.IsNullOrWhiteSpace(strWhere) ? strWhere : null)
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.RelatedTo),
                                Convert.ToString(uc.CheckPointDesc),
                                //LineOperation(Convert.ToBoolean(uc.Yes_No),uc.LineId,uc.HeaderId),
                               (new PLNCommonFunction()).CheckListLineOperationYesNoNew(uc.Yes_No,uc.LineId,uc.HeaderId,IsDisable,uc.ActualId,"Yes_No","UpdateData(this)"),
                                (new PLNCommonFunction()).CheckListLineOperationRemarksNew(uc.Remarks,uc.LineId,uc.HeaderId,IsDisable,uc.ActualId,"Remarks","UpdateData(this)"),
                                Convert.ToString(uc.CreatedBy),
                                Convert.ToString(uc.CreatedOn),
                                Convert.ToString(uc.EditedBy),
                                Convert.ToString(uc.EditedOn),
                                //getChecklistDetails(uc.CheckListId.ToString())[0],
                                //getChecklistDetails(uc.CheckListId.ToString())[1],
                                //getChecklistDetails(uc.CheckListId.ToString())[2],
                                //getChecklistDetails(uc.CheckListId.ToString())[3],
                                Convert.ToString(uc.IsLatest),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdateData(string id, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var splitid = id.Split(',');
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    db.SP_PLN_UPDATE_CHECKLIST_TABLE_COLUMN("pln012", "LineId", splitid[0], columnName, columnValue);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion


        [HttpPost]
        public ActionResult ReviseHeader(int strHeaderId, string strRemarks)
        {
            CustomResponceMsg objResponseMsg = new CustomResponceMsg();
            try
            {
                PLN011 objPLN011 = db.PLN011.Where(u => u.HeaderId == strHeaderId).SingleOrDefault();
                if (objPLN011 != null)
                {                  
                    objPLN011.RevNo = Convert.ToInt32(objPLN011.RevNo) + 1;
                    objPLN011.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                    objPLN011.ReviseRemark = strRemarks;
                    objPLN011.EditedBy = objClsLoginInfo.UserName;
                    objPLN011.EditedOn = DateTime.Now;
                    objPLN011.ReturnRemark = null;
                    objPLN011.ApprovedOn = null;
                    objPLN011.SubmittedBy = null;
                    objPLN011.SubmittedOn = null;
                    db.SaveChanges();
                    Manager.UpdatePDN002(objPLN011.HeaderId, objPLN011.Status, objPLN011.RevNo, objPLN011.Project, objPLN011.Document);
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderID = objPLN011.HeaderId;
                    objResponseMsg.Status = objPLN011.Status;
                    objResponseMsg.rev = objPLN011.RevNo;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revision;
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #region Export Grid Data
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "", int? HeaderId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;

                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    #region Header
                    var lst = db.SP_FETCH_EQUIPMENTHANDLING_HEADERS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Project = Convert.ToString(uc.Project),
                                      Document = Convert.ToString(uc.Document),
                                      Customer = Convert.ToString(uc.Customer),
                                      Product = Convert.ToString(uc.Product),
                                      ProcessLicensor = Convert.ToString(uc.ProcessLicensor),
                                      RevNo = Convert.ToString("R" + uc.RevNo),
                                      Status = Convert.ToString(uc.Status),
                                      CDD = Convert.ToString(uc.CDD),
                                      ProcessPlan = Convert.ToString(uc.ProcessPlan),
                                      CreatedBy = Convert.ToString(uc.CreatedBy),
                                      CreatedOn = Convert.ToString(uc.CreatedOn),
                                      EditedBy = Convert.ToString(uc.EditedBy),
                                      EditedOn = Convert.ToString(uc.EditedOn),
                                      SubmittedBy = Convert.ToString(uc.SubmittedBy),
                                      SubmittedOn = Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),
                                      ApprovedBy = Convert.ToString(uc.ApprovedBy),
                                      ApprovedOn = Convert.ToString(uc.ApprovedOn),
                                      ReturnRemark = Convert.ToString(uc.ReturnRemark),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                    #endregion
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    #region Lines
                    int? PlanRev = 0;
                    if (HeaderId > 0)
                    {
                        PLN012 objPLN012 = db.PLN012.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                        if (objPLN012 != null)
                        {
                            PlanRev = objPLN012.PlanRevNo;
                        }
                        else
                        {
                            PlanRev = 0;
                        }
                    }
                    else
                    {
                        string strEquipmentHandling = clsImplementationEnum.PlanList.Equipment_Handling_Plan.GetStringValue();
                        string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                        var latestRecord = db.PLN001
                                 .Where(x
                                            => x.Plan == strEquipmentHandling &&
                                               x.Status == strStatus
                                        )
                                 .OrderByDescending(x => x.HeaderId)
                                 .Select(x => new { x.HeaderId, x.RevNo })
                                 .FirstOrDefault();
                        PlanRev = latestRecord.RevNo;
                    }

                    var lst = db.SP_FETCH_EQUIPMENTHANDLING_HEADER_LINE(HeaderId, PlanRev, 1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      RelatedTo = Convert.ToString(uc.RelatedTo),
                                      CheckPointDesc = Convert.ToString(uc.CheckPointDesc),
                                      Yes_No = uc.Yes_No,
                                      Remarks = uc.Remarks,
                                      CreatedBy = Convert.ToString(uc.CreatedBy),
                                      CreatedOn = Convert.ToString(uc.CreatedOn),
                                      EditedBy = Convert.ToString(uc.EditedBy),
                                      //EditedOn = Convert.ToString(uc.EditedOn),
                                      //PreparedBy = getChecklistDetails(uc.CheckListId.ToString())[0],
                                      //CheckedBy = getChecklistDetails(uc.CheckListId.ToString())[1],
                                      //CheckedOn = getChecklistDetails(uc.CheckListId.ToString())[2],
                                      //DocumentNo = getChecklistDetails(uc.CheckListId.ToString())[3]
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                    #endregion
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {
                    #region History Header
                    var lst = db.SP_FETCH_EQUIPMENTHANDLING_HEADERS_HISTORY(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Project = Convert.ToString(uc.Project),
                                      Document = Convert.ToString(uc.Document),
                                      Customer = Convert.ToString(uc.Customer),
                                      Product = Convert.ToString(uc.Product),
                                      ProcessLicensor = Convert.ToString(uc.ProcessLicensor),
                                      RevNo = Convert.ToString("R" + uc.RevNo),
                                      Status = Convert.ToString(uc.Status),
                                      CDD = Convert.ToString(uc.CDD),
                                      ProcessPlan = Convert.ToString(uc.ProcessPlan),
                                      CreatedBy = Convert.ToString(uc.CreatedBy),
                                      CreatedOn = Convert.ToString(uc.CreatedOn),
                                      EditedBy = Convert.ToString(uc.EditedBy),
                                      EditedOn = Convert.ToString(uc.EditedOn),
                                      SubmittedBy = Convert.ToString(uc.SubmittedBy),
                                      SubmittedOn = Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),
                                      ApprovedBy = Convert.ToString(uc.ApprovedBy),
                                      ApprovedOn = Convert.ToString(uc.ApprovedOn),
                                      Remark = Convert.ToString(uc.ReturnRemark),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                    #endregion
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYLINES.GetStringValue())
                {
                    #region History Lines
                    PLN011_Log objPLN011_Log = db.PLN011_Log.Where(x => x.Id == HeaderId).FirstOrDefault();

                    var lst = db.SP_FETCH_EQUIPMENTHANDLING_HEADER_LINE_HISTORY(objPLN011_Log.HeaderId, 1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      RelatedTo = Convert.ToString(uc.RelatedTo),
                                      CheckPointDesc = Convert.ToString(uc.CheckPointDesc),
                                      //LineOperation(Convert.ToBoolean(uc.Yes_No),uc.LineId,uc.HeaderId),
                                      Yes_No = uc.Yes_No,
                                      Remarks = uc.Remarks,
                                      CreatedBy = Convert.ToString(uc.CreatedBy),
                                      CreatedOn = Convert.ToString(uc.CreatedOn),
                                      EditedBy = Convert.ToString(uc.EditedBy),
                                      EditedOn = Convert.ToString(uc.EditedOn),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                    #endregion
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
    public class PLNCommonFunction
    {
        public string CheckListLineOperationYesNoNew(string Yes_No, int LineId, int HeaderId, bool IsDisable, string ActualId, string ColumnName, string OnChangeEvent)
        {
            List<string> lstEncoderType = clsImplementationEnum.getChecklistYesNo().ToList();
            string ddl = string.Empty;
            ddl = "<select class='form-control yesno' id='ddlYesNo"+LineId+"' name='ddlYesNo"+LineId+"' colname='" + ColumnName + "' sHeaderId='" + HeaderId.ToString() + "' sLineId='" + LineId.ToString() + "' sactualid='" + ActualId.ToString() + "' class='remarks' " + (IsDisable == true ? "disabled" : "") + " onchange=" + OnChangeEvent + " >";
            if (string.IsNullOrEmpty(Yes_No))
            {
                ddl += "<option value='' selected>select</option>";
            }
            else
            {
                ddl += "<option value=''>select</option>";
            }
            lstEncoderType.ForEach(x => { ddl += "<option value='" + x.ToString() + "' " + (Yes_No.ToLower() == x.ToString().ToLower() ? " selected " : "") + ">" + x.ToString() + "</option>"; });
            ddl += "</select>";
            return ddl;
        }
        public string CheckListLineOperationRemarksNew(string Remarks, int LineId, int HeaderId, bool IsDisable, string ActualId, string ColumnName, string OnChangeEvent)
        {
            return "<input class='form-control col-md-3 remarks' id='txtRemark" + LineId.ToString() + "' colname='" + ColumnName + "' sactualid='" + ActualId.ToString() + "' maxlength='100' name='txtRemark" + LineId.ToString() + "' value='" + Remarks + "' type='text' " + (IsDisable == true ? "disabled" : "") + " onchange=" + OnChangeEvent + " />";
        }

    }
}