﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQS.Areas.Utility.Models;
using System.Globalization;

namespace IEMQS.Areas.PLN.Controllers
{
    public class MaintainLockingCleatController : clsBase
    {
        // GET: PLN/MaintainLockingCleat

        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        #region Index Page
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetHeaderGridDataPartial");
        }
        public ActionResult ShowHistoryTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();

            model.TimelineTitle = "Locking Cleat TimeLine";
            model.Title = "PDinDoc";
            if (HeaderId > 0)
            {

                PLN019_Log objPLN019_Log = db.PLN019_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN019_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN019_Log.CreatedBy) : null;
                model.CreatedOn = objPLN019_Log.CreatedOn;
                model.EditedBy = objPLN019_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN019_Log.EditedBy) : null;
                model.EditedOn = objPLN019_Log.EditedOn;
                model.SubmittedBy = objPLN019_Log.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objPLN019_Log.SubmittedBy) : null;
                model.SubmittedOn = objPLN019_Log.SubmittedOn;

                model.ApprovedBy = objPLN019_Log.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objPLN019_Log.ApprovedBy) : null;
                model.ApprovedOn = objPLN019_Log.ApprovedOn;

            }
            else
            {

                PLN019_Log objPLN019_Log = db.PLN019_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN019_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN019_Log.CreatedBy) : null;
                model.CreatedOn = objPLN019_Log.CreatedOn;
                model.EditedBy = objPLN019_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN019_Log.EditedBy) : null;
                model.EditedOn = objPLN019_Log.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.Title = "PDinDoc";
            model.TimelineTitle = "Locking Cleat Timeline";

            if (HeaderId > 0)
            {
                PLN019 objPLN019 = db.PLN019.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN019.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN019.CreatedBy) : null;
                model.CreatedOn = objPLN019.CreatedOn;
                model.EditedBy = objPLN019.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN019.EditedBy) : null;
                model.EditedOn = objPLN019.EditedOn;

                // model.SubmittedBy = PMB001_Log.SendToCompiledOn != null ? Manager.GetUserNameFromPsNo(PMB001_Log.SendToCompiledOn) : null;
                //model.SubmittedOn = PMB001_Log.SendToCompiledOn;
                model.SubmittedBy = objPLN019.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objPLN019.SubmittedBy) : null;
                model.SubmittedOn = objPLN019.SubmittedOn;
                //model.CompiledBy = PMB001_Log.CompiledBy != null ? Manager.GetUserNameFromPsNo(PMB001_Log.CompiledBy) : null;
                // model.CompiledOn = PMB001_Log.CompiledOn;
                model.ApprovedBy = objPLN019.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objPLN019.ApprovedBy) : null;
                model.ApprovedOn = objPLN019.ApprovedOn;

            }
            else
            {
                PLN019 objPLN019 = db.PLN019.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN019.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN019.CreatedBy) : null;
                model.CreatedOn = objPLN019.CreatedOn;
                model.EditedBy = objPLN019.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN019.EditedBy) : null;
                model.EditedOn = objPLN019.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        [HttpPost]
        public JsonResult GetHeaderDetailsForPrintReport(int HeaderId)
        {
            var objIMB001 = db.IMB001.Where(i => i.HeaderId == HeaderId).Select(i => new { i.HeaderId, i.Project, i.Document, i.RevNo }).FirstOrDefault();
            return Json(objIMB001, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult LoadHeaderData(JQueryDataTableParamModel param)
        {
            try
            {

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += " 1=1 and status in('" + clsImplementationEnum.TestPlanStatus.Draft.GetStringValue() + "')";
                }
                else
                {
                    strWhere += " 1=1";
                }
                string[] columnName = { "Project", "Document", "Customer", "JointType", "RevNo", "Status" };
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                //strWhere += "and CreatedBy=" + objClsLoginInfo.UserName;
                var lstResult = db.SP_PLN_LOCKINGCLEAT_HEADER_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.Project),
                                Convert.ToString(uc.Document),
                                Convert.ToString(uc.Customer),
                                //Convert.ToString(uc.JointType),
                                Convert.ToString("R" + uc.RevNo),
                                Convert.ToString(uc.Status),
                                Convert.ToString(uc.SubmittedBy),
                                uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                Convert.ToString(uc.ApprovedBy),
                                  uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                "<nobr><center>" +
                                 "<a title='View' class='iconspace' href='"+WebsiteURL +"/PLN/MaintainLockingCleat/AddHeader?HeaderID="+uc.HeaderId+"'><i class='fa fa-eye'></i></a>" +
                                   Helper.GenerateActionIcon(uc.HeaderId,"Delete","Delete Record","fa fa-trash-o", "DeleteDocument("+ uc.HeaderId +",'/PLN/MaintainLockingCleat/DeleteHeader', {headerid:"+uc.HeaderId+"}, 'tblLockingCleatHeader')","",  (( uc.RevNo >0 && uc.Status != clsImplementationEnum.CommonStatus.SendForApprovel.GetStringValue()) || ( uc.RevNo == 0 && uc.Status == clsImplementationEnum.CommonStatus.Approved.GetStringValue()) ) ? false:true) +
                                     "<i title='Show Timeline' class='iconspace fa fa-clock-o' href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintainLockingCleat/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')></i>"+
                                    (uc.RevNo > 0 ? HTMLHistoryString(uc.HeaderId,uc.Status,"History","History","iconspace fa fa-history","GetHistoryDetails(\""+ uc.HeaderId +"\");")  : HTMLHistoryString(uc.HeaderId,uc.Status,"History","History","disabledicon fa fa-history","") )+
                                    "<i style='' title='Print Report' class='iconspace fa fa-print' onClick='PrintReport("+uc.HeaderId+")'></i>"+
                               "</center></nobr>",
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DeleteHeader(int headerid)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                objResponseMsg = Manager.DeletePDINDocument(headerid, clsImplementationEnum.PlanList.Locking_Cleat);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region Header Details
        [SessionExpireFilter]
        public ActionResult AddHeader(int HeaderID = 0)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("PLN019");
            if (HeaderID > 0)
            {
                string currentUser = objClsLoginInfo.UserName;
                PLN019 objPLN019 = db.PLN019.Where(x => x.HeaderId == HeaderID && x.CreatedBy == objClsLoginInfo.UserName).FirstOrDefault();
                //if (objPLN019 == null)
                //{
                //    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                //}
            }

            ViewBag.HeaderID = HeaderID;
            return View();
        }

        [SessionExpireFilter]
        [HttpPost]
        public ActionResult LoadHeaderForm(int headerID)
        {
            PLN019 objPLN019 = new PLN019();

            if (headerID > 0)
            {
                objPLN019 = db.PLN019.Where(x => x.HeaderId == headerID).FirstOrDefault();
                objPLN019.Project = db.COM001.Where(i => i.t_cprj == objPLN019.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                ViewBag.Project = objPLN019.Project;
                if (objPLN019.ApprovedBy != null)
                {
                    string ApproverName = db.COM003.Where(x => x.t_psno == objPLN019.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                    ViewBag.ApproverName = objPLN019.ApprovedBy + " - " + ApproverName;
                }
                else
                {
                    ViewBag.ApproverName = "";
                }

                ViewBag.Customer = db.Database.SqlQuery<string>(@"SELECT  t_bpid +'-'+ t_nama   from " + LNLinkedServer + ".dbo.ttccom100175  where t_bpid = '" + objPLN019.Customer + "'").FirstOrDefault();
                ViewBag.Action = "headeredit";

                var PlanningDinID = db.PDN002.Where(x => x.RefId == headerID && x.DocumentNo == objPLN019.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;

                ViewBag.IsReviseBtnEnabled = Manager.IsReviseEnabled(PlanningDinID, objPLN019.HeaderId, objPLN019.Document);
                ViewBag.DocMessage = clsImplementationMessage.CommonMessages.DocMessage.ToString();
            }
            else
            {
                objPLN019.RevNo = 0;
                objPLN019.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
            }

            return PartialView("_LoadHeaderForm", objPLN019);
        }


        [SessionExpireFilter]
        public ActionResult LoadTabWiseData(int headerID)
        {
            ViewBag.HeaderID = headerID;
            if (headerID > 0)
            {
                string status = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
                int lstLine = db.PLN020.Where(p => p.HeaderId == headerID).Count();
                if (lstLine > 0)
                {
                    int lstStatus = db.PLN020.Where(p => (p.HeaderId == headerID) && (p.LineStatus == status)).Count();
                    if (lstLine == lstStatus)
                    {
                        ViewBag.Trigger = "approvedlockingcleat";
                    }
                }
            }

            return PartialView("_LoadTabWiseDataPartial");
        }

        [SessionExpireFilter]
        public ActionResult LoadLockingCleatData(int headerID)
        {
            PLN019 objPLN019 = new PLN019();

            if (headerID > 0)
            {
                objPLN019 = db.PLN019.Where(x => x.HeaderId == headerID).FirstOrDefault();
                ViewBag.Action = "headeredit";
            }
            return PartialView("_LoadLockingCleatDataPartial", objPLN019);
        }

        [HttpPost]
        public bool checkJointtypeExists(string jointtype, int headerid)
        {
            bool Flag = false;
            if (!string.IsNullOrWhiteSpace(jointtype))
            {
                PLN020 objPLN020 = db.PLN020.Where(x => x.JointType == jointtype && x.HeaderId == headerid).FirstOrDefault();
                if (objPLN020 != null)
                {
                    Flag = true;
                }
            }
            return Flag;
        }

        [HttpPost]
        public bool checkJointdetailsExists(string jointtype, int headerid)
        {
            bool Flag = false;
            if (!string.IsNullOrWhiteSpace(jointtype))
            {
                PLN021 objPLN021 = db.PLN021.Where(x => x.JointDetails == jointtype && x.HeaderId == headerid).FirstOrDefault();
                if (objPLN021 != null)
                {
                    Flag = true;
                }
            }
            return Flag;
        }
        [SessionExpireFilter]
        public ActionResult LoadLoadingDetailsData(int headerID, int lineID)
        {
            PLN020 objPLN020 = new PLN020();
            ViewBag.HeaderId = headerID;
            ViewBag.Material = clsImplementationEnum.GetLockingCleatMaterial().ToArray();
            ViewBag.WeldType = clsImplementationEnum.GetWeldType().ToArray();
            PLN019 objPLN019 = new PLN019();
            objPLN019 = db.PLN019.Where(x => x.HeaderId == headerID).FirstOrDefault();
            if (objPLN019 != null)
            {
                if (lineID > 0)
                {
                    objPLN020 = db.PLN020.Where(x => x.LineId == lineID).FirstOrDefault();
                    ViewBag.Action = "line_edit";
                }
                else
                {
                    string docNum = MakeLCDocNumber(headerID);
                    objPLN020.DocNo = Convert.ToInt32(docNum);
                    objPLN020.LCDocument = "LC" + docNum + "_" + objPLN019.Document;
                }

                ViewBag.HeaderStatus = objPLN019.Status;

            }
            return PartialView("_LoadLoadingDetailsHTML", objPLN020);
        }

        [SessionExpireFilter]
        public ActionResult LoadTackWeldsData(int headerID)
        {
            PLN019 objPLN019 = new PLN019();
            ViewBag.Header = headerID;
            if (headerID > 0)
            {
                objPLN019 = db.PLN019.Where(x => x.HeaderId == headerID).FirstOrDefault();
                ViewBag.Action = "headeredit";
            }

            return PartialView("_LoadTackWeldsDataPartial", objPLN019);
        }

        [SessionExpireFilter]
        public ActionResult LoadTackWeldLineData(int headerID, int lineID)
        {
            PLN021 objPLN021 = new PLN021();
            PLN019 objPLN019 = new PLN019();
            ViewBag.HeaderId = headerID;
            ViewBag.Material = clsImplementationEnum.GetTackWeldMaterial().ToArray();
            ViewBag.WeldType = clsImplementationEnum.GetTackWeldYieldStrength().ToArray();
            objPLN019 = db.PLN019.Where(x => x.HeaderId == headerID).FirstOrDefault();
            if (objPLN019 != null)
            {
                if (lineID > 0)
                {
                    objPLN021 = db.PLN021.Where(x => x.LineId == lineID).FirstOrDefault();
                    ViewBag.Action = "headeredit";
                }
                else
                {
                    ViewBag.Action = "lineadd";
                    string docNum = MakeTWDocNumber(headerID);
                    objPLN021.DocNo = Convert.ToInt32(docNum);
                    objPLN021.TWDocument = "TW" + docNum + "_" + objPLN019.Document;
                }
                ViewBag.HeaderStatus = objPLN019.Status;

            }
            return PartialView("_LoadTackWeldLineDetailsHTML", objPLN021);
        }

        [HttpPost]
        public JsonResult LoadLCLineData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;

                string[] columnName = { "LCDocument", "JointType", "RevNo", "LineStatus", "ReturnRemark" };
                strWhere += "HeaderId=" + Convert.ToInt32(param.Headerid);
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_PLN_LOCKINGCLEAT_LC_LINES_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            //Convert.ToString(uc.Project),
                            Convert.ToString(uc.LCDocument),
                            //Convert.ToString(uc.Customer),
                            Convert.ToString(uc.JointType),
                            Convert.ToString(uc.LineStatus),
                                 Convert.ToString(uc.ReturnRemark),
                            //"<center>" + HTMLActionString(uc.HeaderId,uc.LineStatus,"Edit","Edit Record","fa fa-trash-o","DeleteRecord("+ uc.LineId +");") +"</center>",
                            "<center>"+"<a title='View' onClick = 'ViewLockingCleat(" +uc.HeaderId +"," + uc.LineId +")' ><i style='margin-right:5px;' class='fa fa-eye'></i></a>" +" "+HTMLActionString(uc.LineId, uc.LineStatus, "Delete", "Delete Locking Cleat", "fa fa-trash-o", "DeleteLockingCleat(" + uc.LineId + ");")+""+"</center>",
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadTWLineData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                string[] columnName = { "TWDocument", "JointDetails", "RevNo", "LineStatus", "ReturnRemark" };
                strWhere += "HeaderId=" + Convert.ToInt32(param.Headerid);
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_PLN_LOCKINGCLEAT_TW_LINES_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            //Convert.ToString(uc.Project),
                            Convert.ToString(uc.TWDocument),
                            //Convert.ToString(uc.Customer),
                            Convert.ToString(uc.JointDetails),
                            Convert.ToString(uc.LineStatus),
                               Convert.ToString(uc.ReturnRemark),
                            //"<center>" + HTMLActionString(uc.HeaderId,uc.LineStatus,"Edit","Edit Record","fa fa-trash-o","DeleteRecord("+ uc.LineId +");") +"</center>",
                               "<center>"+"<a title='View' onClick = 'ViewTackWeld(" +uc.HeaderId +"," + uc.LineId +")' ><i style='margin-right:5px;' class='fa fa-eye'></i></a>" +" "+  HTMLActionString(uc.LineId, uc.LineStatus, "Delete", "Delete Tack Weld", "fa fa-trash-o", "DeleteTackWeld(" + uc.LineId + ");")+""+"</center>",
                            //"<center><a class='btn btn-xs green' onClick = 'ViewTackWeld(" +uc.HeaderId +"," + uc.LineId +")' >View<i style='margin-right:5px;' class='fa fa-eye'></i></a></center>"+""+HTMLActionString(uc.LineId, uc.LineStatus, "Delete", "Delete Tack Weld", "fa fa-trash-o", "DeleteTackWeld(" + uc.LineId + ");"),
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }


        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (!string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()) && !string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue()))
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' Title='" + buttonTooltip + "' class='iconspace " + className + "'" + onClickEvent + " ></i>";
            }
            else
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' Title='" + buttonTooltip + "' class='disabledicon " + className + "' ></i>";
            }
            return htmlControl;
        }
        public string HTMLHistoryString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";
            htmlControl = "<i id='" + inputID + "' name='" + inputID + "' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            return htmlControl;
        }
        [HttpPost]
        public ActionResult SaveHeader(PLN019 pln019, FormCollection fc)
        {
            CustomResponceMsg objResponseMsg = new CustomResponceMsg();
            try
            {
                PLN019 objPLN019 = new PLN019();
                bool IsEdited = false;
                if (pln019.HeaderId > 0)
                {
                    objPLN019 = db.PLN019.Where(x => x.HeaderId == pln019.HeaderId).FirstOrDefault();
                    IsEdited = true;
                }
                //objPLN019.Project = pln019.Project.Split('-')[0];
                //objPLN019.Document = pln019.Document;
                //objPLN019.Customer = pln019.Customer.Split('-')[0];
                //objPLN019.RevNo = pln019.RevNo;
                //objPLN019.Status = pln019.Status;
                //objPLN019.CDD = pln019.CDD;
                //objPLN019.JointType = pln019.JointType;
                objPLN019.ApprovedBy = pln019.ApprovedBy;
                objPLN019.ReviseRemark = pln019.ReviseRemark;
                //objPLN019.Load = pln019.Load;
                //objPLN019.FactorofSafety = pln019.FactorofSafety;
                //objPLN019.LoadwithFOS = pln019.LoadwithFOS;
                //objPLN019.NumberofCleats = pln019.NumberofCleats;
                //objPLN019.Loadpercleat_MT = pln019.Loadpercleat_MT;
                //objPLN019.Loadpercleat_N = pln019.Loadpercleat_N;
                //objPLN019.Thickness = pln019.Thickness;
                //objPLN019.Dimention_a = pln019.Dimention_a;
                //objPLN019.Dimention_b = pln019.Dimention_b;
                //objPLN019.Dimention_c = pln019.Dimention_c;
                //objPLN019.Dimention_d = pln019.Dimention_d;
                //objPLN019.Material = pln019.Material;
                //objPLN019.YieldStrength = pln019.YieldStrength;
                //objPLN019.WeldType = pln019.WeldType;
                //objPLN019.SizeofWeld = pln019.SizeofWeld;
                //objPLN019.Throatthickness = pln019.Throatthickness;
                //objPLN019.Lengthofweld = pln019.Lengthofweld;

                if (objPLN019.Status == clsImplementationEnum.CommonStatus.Approved.GetStringValue())
                {
                    objPLN019.RevNo = Convert.ToInt32(objPLN019.RevNo) + 1;
                    objPLN019.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                }
                if (IsEdited)
                {
                    objPLN019.EditedBy = objClsLoginInfo.UserName;
                    objPLN019.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                }
                else
                {
                    objPLN019.CreatedBy = objClsLoginInfo.UserName;
                    objPLN019.CreatedOn = DateTime.Now;
                    db.PLN019.Add(objPLN019);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = objResponseMsg.Value = string.Format(clsImplementationMessage.CommonMessages.Insert.ToString(), objPLN019.Document);

                }
                Manager.UpdatePDN002(objPLN019.HeaderId, objPLN019.Status, objPLN019.RevNo, objPLN019.Project, objPLN019.Document);
                objResponseMsg.HeaderID = objPLN019.HeaderId;
                objResponseMsg.Status = objPLN019.Status;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public ActionResult ReviseHeader(int strHeaderId, string strRemarks)
        {
            CustomResponceMsg objResponseMsg = new CustomResponceMsg();
            try
            {
                string status = clsImplementationEnum.PLCStatus.Approved.GetStringValue();
                PLN019 objPLN019 = db.PLN019.Where(u => u.HeaderId == strHeaderId && u.Status == status).FirstOrDefault();
                if (objPLN019 != null)
                {
                    objPLN019.RevNo = Convert.ToInt32(objPLN019.RevNo) + 1;
                    objPLN019.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                    objPLN019.ReviseRemark = strRemarks;
                    objPLN019.ApprovedOn = null;
                    objPLN019.ReturnRemark = null;
                    objPLN019.EditedBy = objClsLoginInfo.UserName;
                    objPLN019.EditedOn = DateTime.Now;
                    objPLN019.SubmittedBy = null;
                    objPLN019.SubmittedOn = null;
                    db.SaveChanges();
                    Manager.UpdatePDN002(objPLN019.HeaderId, objPLN019.Status, objPLN019.RevNo, objPLN019.Project, objPLN019.Document);
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderID = objPLN019.HeaderId;
                    objResponseMsg.Status = objPLN019.Status;
                    objResponseMsg.rev = objPLN019.RevNo;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revision;
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SendForApproval(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PLN019 objPLN019 = db.PLN019.Where(u => u.HeaderId == HeaderId).SingleOrDefault();
                if (objPLN019 != null)
                {
                    db.SP_PLN_LOCKINGCLEAT_SENDFORAPPROVE(HeaderId, clsImplementationEnum.CommonStatus.SendForApprovel.GetStringValue(), objClsLoginInfo.UserName);
                    db.SaveChanges();
                    //db.SaveChanges();
                    Manager.UpdatePDN002(objPLN019.HeaderId, clsImplementationEnum.CommonStatus.SendForApprovel.GetStringValue(), objPLN019.RevNo, objPLN019.Project, objPLN019.Document);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.sentforApprove;

                    PLN019 objPLN019Update = new PLN019();
                    IEMQSEntitiesContext db1 = new IEMQSEntitiesContext();
                    objPLN019Update = db1.PLN019.Where(u => u.HeaderId == HeaderId).FirstOrDefault();
                    #region Send Notification
                    string rolePLNG3 = clsImplementationEnum.UserRoleName.PLNG3.GetStringValue();
                    (new clsManager()).SendNotification(rolePLNG3,
                                                        objPLN019Update.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objPLN019Update.Project, clsImplementationEnum.PlanList.Locking_Cleat.GetStringValue(), objPLN019Update.RevNo.Value.ToString(), objPLN019Update.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Locking_Cleat.GetStringValue(), objPLN019Update.HeaderId.ToString(), true),
                                                        objPLN019Update.ApprovedBy);
                    #endregion
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveLockingCleat(PLN020 pln020, FormCollection fc)
        {
            CustomResponceMsg objResponseMsg = new CustomResponceMsg();
            try
            {
                PLN020 objPLN020 = new PLN020();
                PLN019 objPLN019 = new PLN019();
                bool IsEditable = false;
                var lstPLN020 = db.PLN020.Where(x => x.HeaderId == pln020.HeaderId && x.JointType == pln020.JointType && x.LineId != pln020.LineId).ToList();
                if (lstPLN020.Count() > 0)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.DuplicateJointType.ToString();
                }
                else
                {
                    if (pln020.LineId > 0)
                    {
                        objPLN020 = db.PLN020.Where(x => x.LineId == pln020.LineId).FirstOrDefault();
                        IsEditable = true;
                    }
                    objPLN019 = db.PLN019.Where(x => x.HeaderId == pln020.HeaderId).FirstOrDefault();
                    objPLN020.JointType = pln020.JointType;
                    objPLN020.LineStatus = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                    objPLN020.Load = pln020.Load;
                    objPLN020.ShellThickness = pln020.ShellThickness;
                    objPLN020.FactorofSafety = pln020.FactorofSafety;
                    objPLN020.LoadwithFOS = pln020.LoadwithFOS;
                    objPLN020.NumberofCleats = pln020.NumberofCleats;
                    objPLN020.Loadpercleat_MT = Convert.ToInt32(pln020.Loadpercleat_MT);
                    objPLN020.Loadpercleat_N = Convert.ToInt32(pln020.Loadpercleat_N);
                    objPLN020.Thickness = pln020.Thickness;
                    objPLN020.Dimention_a = pln020.Dimention_a;
                    objPLN020.Dimention_b = pln020.Dimention_b;
                    objPLN020.Dimention_c = pln020.Dimention_c;
                    objPLN020.Dimention_d = pln020.Dimention_d;
                    objPLN020.Material = pln020.Material;
                    objPLN020.YieldStrength = pln020.YieldStrength;
                    objPLN020.WeldType = pln020.WeldType;
                    objPLN020.SizeofWeld = pln020.SizeofWeld;
                    objPLN020.Throatthickness = pln020.Throatthickness;
                    objPLN020.Lengthofweld = pln020.Lengthofweld;
                    if (objPLN019.Status == clsImplementationEnum.CommonStatus.Approved.GetStringValue())
                    {
                        objPLN019.RevNo = Convert.ToInt32(objPLN019.RevNo) + 1;
                        objPLN019.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                    }
                    if (IsEditable)
                    {
                        objPLN020.EditedBy = objClsLoginInfo.UserName;
                        objPLN020.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                    }
                    else
                    {
                        objPLN020.HeaderId = objPLN019.HeaderId;
                        objPLN020.Project = objPLN019.Project;
                        objPLN020.Document = objPLN019.Document;
                        objPLN020.Customer = objPLN019.Customer;
                        objPLN020.RevNo = objPLN019.RevNo;
                        objPLN020.CDD = objPLN019.CDD;
                        //string docNum = MakeLCDocNumber(objPLN019.HeaderId);
                        objPLN020.DocNo = pln020.DocNo;//Convert.ToInt32(docNum);
                        objPLN020.LCDocument = pln020.LCDocument;
                        objPLN020.CreatedBy = objClsLoginInfo.UserName;
                        objPLN020.CreatedOn = DateTime.Now;
                        db.PLN020.Add(objPLN020);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                    }

                    Manager.UpdatePDN002(objPLN019.HeaderId, objPLN019.Status, objPLN019.RevNo, objPLN019.Project, objPLN019.Document);
                    objResponseMsg.HeaderID = objPLN019.HeaderId;
                    objResponseMsg.Status = objPLN019.Status;
                    objResponseMsg.LineID = objPLN020.LineId;
                    objResponseMsg.rev = objPLN019.RevNo;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        //
        [HttpPost]
        public ActionResult DeleteTackWeld(int lineid)
        {
            CustomResponceMsg objResponseMsg = new CustomResponceMsg();
            try
            {
                PLN021 objPLN021 = new PLN021();
                {
                    objPLN021 = db.PLN021.Where(x => x.LineId == lineid).FirstOrDefault();
                    db.PLN021.Remove(objPLN021);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult DeleteLockingCleat(int lineid)
        {
            CustomResponceMsg objResponseMsg = new CustomResponceMsg();
            try
            {
                PLN020 objPLN020 = new PLN020();
                {
                    objPLN020 = db.PLN020.Where(x => x.LineId == lineid).FirstOrDefault();
                    db.PLN020.Remove(objPLN020);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [SessionExpireFilter]
        [HttpPost]
        public ActionResult SaveTackWeld(PLN021 pln021, FormCollection fc)
        {
            CustomResponceMsg objResponseMsg = new CustomResponceMsg();
            try
            {
                PLN021 objPLN021 = new PLN021();
                PLN019 objPLN019 = new PLN019();
                bool IsEditable = false;
                if (objClsLoginInfo == null)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.SessionExp.ToString();
                }
                else
                {
                    var lstPLN021 = db.PLN021.Where(x => x.HeaderId == pln021.HeaderId && x.JointDetails == pln021.JointDetails && x.LineId != pln021.LineId).ToList();
                    if (lstPLN021.Count() > 0)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Joint details already exists";
                    }
                    else
                    {
                        if (pln021.LineId > 0)
                        {
                            objPLN021 = db.PLN021.Where(x => x.LineId == pln021.LineId).FirstOrDefault();
                            IsEditable = true;
                        }

                        objPLN019 = db.PLN019.Where(x => x.HeaderId == pln021.HeaderId).FirstOrDefault();
                        objPLN021.JointDetails = pln021.JointDetails;
                        objPLN021.WeightofSection = pln021.WeightofSection;
                        objPLN021.Factorofsafety = pln021.Factorofsafety;
                        objPLN021.LoadwithFOS_MT = pln021.LoadwithFOS_MT;
                        objPLN021.LoadwithFOS_N = pln021.LoadwithFOS_N;
                        objPLN021.DistCorrWeightSect = pln021.DistCorrWeightSect;
                        objPLN021.InsideRadiousofweld = pln021.InsideRadiousofweld;
                        objPLN021.Weldsize = pln021.Weldsize;
                        objPLN021.OutsideRadiusofweld = pln021.OutsideRadiusofweld;
                        objPLN021.Numoftacks = pln021.Numoftacks;
                        objPLN021.Materialofweld = pln021.Materialofweld;
                        objPLN021.Yieldstrength = pln021.Yieldstrength;
                        objPLN021.AllowtensileStress = pln021.AllowtensileStress;
                        objPLN021.AllowShearStress = pln021.AllowShearStress;
                        objPLN021.maxTensileLoad = pln021.maxTensileLoad;
                        objPLN021.DirectShearLoad = pln021.DirectShearLoad;
                        objPLN021.EquivalentTensileLoad = pln021.EquivalentTensileLoad;
                        objPLN021.EquivalentShearLoad = pln021.EquivalentShearLoad;
                        objPLN021.WeldLengthDuetoTensileLoad = pln021.WeldLengthDuetoTensileLoad;
                        objPLN021.WeldLengthDuetoShearLoad = pln021.WeldLengthDuetoShearLoad;
                        objPLN021.WeldLenghtRequired = pln021.WeldLenghtRequired;
                        objPLN021.DocNo = pln021.DocNo;
                        objPLN021.WeldLenghtRequired = pln021.WeldLenghtRequired;
                        objPLN021.LineStatus = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();

                        if (objPLN019.Status == clsImplementationEnum.CommonStatus.Approved.GetStringValue())
                        {
                            objPLN019.RevNo = Convert.ToInt32(objPLN019.RevNo) + 1;
                            objPLN019.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                        }
                        if (IsEditable)
                        {
                            objPLN021.EditedBy = objClsLoginInfo.UserName;
                            objPLN021.EditedOn = DateTime.Now;
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                        }
                        else
                        {
                            objPLN021.HeaderId = objPLN019.HeaderId;
                            objPLN021.Project = objPLN019.Project;
                            objPLN021.Document = objPLN019.Document;
                            objPLN021.Customer = objPLN019.Customer;
                            objPLN021.RevNo = objPLN019.RevNo;
                            objPLN021.CDD = objPLN019.CDD;
                            //string docNum = MakeTWDocNumber(objPLN019.HeaderId);
                            objPLN021.TWDocument = pln021.TWDocument;
                            objPLN021.DocNo = pln021.DocNo;//Convert.ToInt32(docNum);
                            objPLN021.CreatedBy = objClsLoginInfo.UserName;
                            objPLN021.CreatedOn = DateTime.Now;
                            db.PLN021.Add(objPLN021);
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                        }
                        db.SaveChanges();
                        //objResponseMsg.Key = true;
                        //objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                        Manager.UpdatePDN002(objPLN019.HeaderId, objPLN019.Status, objPLN019.RevNo, objPLN019.Project, objPLN019.Document);
                        objResponseMsg.HeaderID = objPLN019.HeaderId;
                        objResponseMsg.Status = objPLN019.Status;
                        objResponseMsg.LineID = objPLN021.LineId;
                        objResponseMsg.rev = objPLN019.RevNo;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public string MakeTWDocNumber(int header)
        {
            var lstPLN021 = db.PLN021.Where(x => x.HeaderId == header).ToList();
            string docNumber = string.Empty;

            if (lstPLN021.Count() > 0)
            {
                var twDocNumber = (from a in db.PLN021
                                   where a.HeaderId.Equals(header) //, StringComparison.OrdinalIgnoreCase) && a.Method == method && a.QualityProject.Equals(qms, StringComparison.OrdinalIgnoreCase)
                                   select a).Max(a => a.DocNo);
                docNumber = (twDocNumber + 1).ToString();
            }
            else
            {
                docNumber = "01";
            }

            return docNumber.PadLeft(2, '0');
        }


        public string MakeLCDocNumber(int header)
        {
            var lstPLN020 = db.PLN020.Where(x => x.HeaderId == header).ToList();
            string docNumber = string.Empty;

            if (lstPLN020.Count() > 0)
            {
                var twDocNumber = (from a in db.PLN020
                                   where a.HeaderId.Equals(header)
                                   select a).Max(a => a.DocNo);
                docNumber = (twDocNumber + 1).ToString();
            }
            else
            {
                docNumber = "01";
            }

            return docNumber.PadLeft(2, '0');
        }


        [HttpPost]
        public ActionResult RetractStatus(int id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PLN019 objPLN019 = db.PLN019.Where(u => u.HeaderId == id).SingleOrDefault();
                string status = clsImplementationEnum.PLCStatus.DRAFT.GetStringValue();
                string statusSendforApproval = clsImplementationEnum.PLCStatus.SendForApprovel.GetStringValue();
                List<PLN020> lstPLN020 = db.PLN020.Where(u => u.HeaderId == id && u.LineStatus == statusSendforApproval).ToList();
                List<PLN021> lstPLN021 = db.PLN021.Where(u => u.HeaderId == id && u.LineStatus == statusSendforApproval).ToList();

                if (objPLN019 != null)
                {
                    objPLN019.Status = clsImplementationEnum.PLCStatus.DRAFT.GetStringValue();
                    #region update status for locking cleat and Tack weld
                    if (lstPLN020.Count > 0)
                    {
                        lstPLN020.ForEach(i => i.LineStatus = status);
                    }
                    if (lstPLN021.Count > 0)
                    { lstPLN021.ForEach(i => i.LineStatus = status); }
                    #endregion

                    objPLN019.EditedBy = objClsLoginInfo.UserName;
                    objPLN019.EditedOn = DateTime.Now;
                    objPLN019.SubmittedOn = null;
                    objPLN019.SubmittedBy = null;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Retract.ToString();
                    Manager.UpdatePDN002(objPLN019.HeaderId, objPLN019.Status, objPLN019.RevNo, objPLN019.Project, objPLN019.Document);
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region functionsS
        [HttpPost]
        public ActionResult getCustomer(string projectcode)
        {
            CustomResponceMsg objResponseMsg = new CustomResponceMsg();
            string Project = projectcode.Split('-')[0];
            objResponseMsg.Customer = Manager.GetCustomerCodeAndNameByProject(Project);
            string strCdd = Manager.GetContractWiseCdd(projectcode);
            if (!string.IsNullOrWhiteSpace(strCdd))
            {
                DateTime dtCdd = Convert.ToDateTime(strCdd);
                objResponseMsg.CDD = dtCdd.Date.ToShortDateString();
            }
            objResponseMsg.Project = Project;
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public bool checkProjectExist(string projectcode)
        {
            bool Flag = false;
            if (!string.IsNullOrWhiteSpace(projectcode))
            {
                PLN019 objPLN019 = db.PLN019.Where(x => x.Project == projectcode).FirstOrDefault();
                if (objPLN019 != null)
                {
                    Flag = true;
                }
            }
            return Flag;
        }

        #endregion
        [SessionExpireFilter]
        public ActionResult PrintFormat(int? HeaderId)
        {
            PLN019 objPLN019 = db.PLN019.Where(q => q.HeaderId == HeaderId).FirstOrDefault();
            objPLN019.CreatedBy = Manager.GetUserNameFromPsNo(objPLN019.CreatedBy);
            ViewBag.Project = db.COM001.Where(i => i.t_cprj == objPLN019.Project).Select(i => i.t_dsca).FirstOrDefault();

            return View(objPLN019);
        }

        #region History
        [SessionExpireFilter]
        [HttpPost]
        public ActionResult GetHistoryDetails(int Id)
        {
            PLN019_Log objPLN019Log = new PLN019_Log();
            objPLN019Log = db.PLN019_Log.Where(x => x.HeaderId == Id).FirstOrDefault();
            return PartialView("_LoadHistoryPartial", objPLN019Log);
        }

        [SessionExpireFilter]
        public ActionResult ViewLogDetails(int? Id)
        {
            PLN019_Log objPLN019 = new PLN019_Log();
            if (Id > 0)
            {
                objPLN019 = db.PLN019_Log.Where(i => i.Id == Id).FirstOrDefault();
                objPLN019.Project = db.COM001.Where(i => i.t_cprj == objPLN019.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                ViewBag.Project = objPLN019.Project;
                string ApproverName = db.COM003.Where(x => x.t_psno == objPLN019.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                ViewBag.Customer = db.Database.SqlQuery<string>(@"SELECT  t_bpid +'-'+ t_nama   from " + LNLinkedServer + ".dbo.ttccom100175  where t_bpid = '" + objPLN019.Customer + "'").FirstOrDefault();
                ViewBag.ApproverName = objPLN019.ApprovedBy + " - " + ApproverName;
                var urlPrefix = Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.LastIndexOf('=') + 1);
                if (!objClsLoginInfo.GetUserRoleList().Contains("SHOP"))
                {
                    ViewBag.RevPrev = (db.PLN019_Log.Any(q => (q.HeaderId == objPLN019.HeaderId && q.RevNo == (objPLN019.RevNo - 1))) ? urlPrefix + db.PLN019_Log.Where(q => (q.HeaderId == objPLN019.HeaderId && q.RevNo == (objPLN019.RevNo - 1))).FirstOrDefault().Id : null);
                    ViewBag.RevNext = (db.PLN019_Log.Any(q => (q.HeaderId == objPLN019.HeaderId && q.RevNo == (objPLN019.RevNo + 1))) ? urlPrefix + db.PLN019_Log.Where(q => (q.HeaderId == objPLN019.HeaderId && q.RevNo == (objPLN019.RevNo + 1))).FirstOrDefault().Id : null);
                }
            }
            return View(objPLN019);
        }

        [SessionExpireFilter]
        public ActionResult LoadTabWiseHistoryData(int id)
        {
            ViewBag.HistoryID = id;
            return PartialView("_LoadTabWiseDataHistoryPartial");
        }

        [SessionExpireFilter]
        public ActionResult LoadLockingCleatHistoryData(int id)
        {
            PLN019_Log objPLN019 = new PLN019_Log();

            if (id > 0)
            {
                objPLN019 = db.PLN019_Log.Where(x => x.Id == id).FirstOrDefault();
                ViewBag.Action = "headeredit";
            }

            //PLN019_Log objPLN019 = new PLN019_Log();
            //ViewBag.Material = clsImplementationEnum.GetLockingCleatMaterial().ToArray();
            //ViewBag.WeldType = clsImplementationEnum.GetWeldType().ToArray();
            //if (id > 0)
            //{
            //    objPLN019 = db.PLN019_Log.Where(x => x.Id == id).FirstOrDefault();
            //    ViewBag.Action = "headeredit";
            //}
            return PartialView("_LoadLockingCleatDataHistoryPartial", objPLN019);
        }

        [SessionExpireFilter]
        public ActionResult LoadTackWeldsHistoryData(int id)
        {
            PLN019_Log objPLN019 = new PLN019_Log();
            if (id > 0)
            {
                ViewBag.Header = id;
                objPLN019 = db.PLN019_Log.Where(x => x.Id == id).FirstOrDefault();
                ViewBag.Action = "headeredit";
            }

            return PartialView("_LoadTackWeldsDataHistoryPartial", objPLN019);
        }


        [SessionExpireFilter]
        public ActionResult LoadLoadingHistoryDetailsData(int id, int lineID)
        {
            PLN020_Log objPLN020 = new PLN020_Log();
            ViewBag.HeaderId = id;
            ViewBag.Material = clsImplementationEnum.GetLockingCleatMaterial().ToArray();
            ViewBag.WeldType = clsImplementationEnum.GetWeldType().ToArray();
            PLN019_Log objPLN019 = new PLN019_Log();
            objPLN019 = db.PLN019_Log.Where(x => x.HeaderId == id).FirstOrDefault();
            if (objPLN019 != null)
            {
                if (lineID > 0)
                {
                    objPLN020 = db.PLN020_Log.Where(x => x.LineId == lineID).FirstOrDefault();
                    ViewBag.Action = "line_edit";
                }

                ViewBag.HeaderStatus = objPLN019.Status;

            }
            return PartialView("_LoadLoadingDetailsHistoryHTML", objPLN020);
        }


        [SessionExpireFilter]
        public ActionResult LoadTackWeldHistoryLineData(int id, int lineID)
        {
            PLN021_Log objPLN021 = new PLN021_Log();
            PLN019_Log objPLN019 = new PLN019_Log();
            ViewBag.HeaderId = id;
            ViewBag.Material = clsImplementationEnum.GetTackWeldMaterial().ToArray();
            ViewBag.WeldType = clsImplementationEnum.GetTackWeldYieldStrength().ToArray();
            objPLN019 = db.PLN019_Log.Where(x => x.HeaderId == id).FirstOrDefault();
            if (objPLN019 != null)
            {
                if (lineID > 0)
                {
                    objPLN021 = db.PLN021_Log.Where(x => x.LineId == lineID).FirstOrDefault();
                    ViewBag.Action = "headeredit";
                }
                ViewBag.HeaderStatus = objPLN019.Status;

            }
            return PartialView("_LoadTackWeldLineDetailsHistoryHTML", objPLN021);
        }

        [HttpPost]
        public JsonResult LoadHeaderHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;

                string[] columnName = { "Project", "Customer", "Document", "Status", "RevNo" };
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                
                strWhere += "1=1 and HeaderId=" + param.Headerid;

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_PLN_LOCKINGCLEAT_HEADER_HISTORY_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.Project),
                            Convert.ToString(uc.Document),
                            Convert.ToString(uc.Customer),
                            Convert.ToString("R" +uc.RevNo),
                            Convert.ToString(uc.Status),
                            Convert.ToString(uc.SubmittedBy),
                            Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),
                            Convert.ToString(uc.ApprovedBy),
                            Convert.ToString(Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy")),
                            "<center><a title='View' href='"+WebsiteURL +"/PLN/MaintainLockingCleat/ViewLogDetails?Id="+uc.Id+"'><i style='margin-right:5px;' class='fa fa-eye'></i></a><a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintainLockingCleat/ShowHistoryTimeline?HeaderID=" + Convert.ToInt32(uc.Id) + "')><i style = 'margin-left:5px;'  class='fa fa-clock-o'></i></a></center>",
                             }).ToList();



                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadLCLineHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;

                string[] columnName = { "LCDocument", "JointType", "RevNo", "LineStatus", "ReturnRemark" };
                strWhere += "RefId=" + Convert.ToInt32(param.Headerid);
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }


                var lstResult = db.SP_PLN_LOCKINGCLEAT_LC_LINES_DETAILS_HISTORY
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            //Convert.ToString(uc.Project),
                            Convert.ToString(uc.LCDocument),
                            //Convert.ToString(uc.Customer),
                            Convert.ToString(uc.JointType),
                            Convert.ToString(uc.LineStatus),
                               Convert.ToString(uc.ReturnRemark),
                            //"<center>" + HTMLActionString(uc.HeaderId,uc.LineStatus,"Edit","Edit Record","fa fa-trash-o","DeleteRecord("+ uc.LineId +");") +"</center>",
                            "<center>"+"<a title='View' onClick = 'ViewLockingCleat(" +uc.HeaderId +"," + uc.LineId +")' ><i style='margin-right:5px;' class='fa fa-eye'></i></a></center>",
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadTWLineHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;

                string[] columnName = { "TWDocument", "JointDetails", "RevNo", "LineStatus", "ReturnRemark" };
                strWhere += "RefId=" + Convert.ToInt32(param.Headerid);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var lstResult = db.SP_PLN_LOCKINGCLEAT_TW_LINES_DETAILS_HISTORY
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            //Convert.ToString(uc.Project),
                            Convert.ToString(uc.TWDocument),
                            //Convert.ToString(uc.Customer),
                            Convert.ToString(uc.JointDetails),
                            Convert.ToString(uc.LineStatus),
                               Convert.ToString(uc.ReturnRemark),
                            //"<center>" + HTMLActionString(uc.HeaderId,uc.LineStatus,"Edit","Edit Record","fa fa-trash-o","DeleteRecord("+ uc.LineId +");") +"</center>",
                               "<center>"+"<a title='View' onClick = 'ViewTackWeld(" +uc.HeaderId +"," + uc.LineId +")' ><i style='margin-right:5px;' class='fa fa-eye'></i></a></center>",
                            //"<center><a class='btn btn-xs green' onClick = 'ViewTackWeld(" +uc.HeaderId +"," + uc.LineId +")' >View<i style='margin-right:5px;' class='fa fa-eye'></i></a></center>"+""+HTMLActionString(uc.LineId, uc.LineStatus, "Delete", "Delete Tack Weld", "fa fa-trash-o", "DeleteTackWeld(" + uc.LineId + ");"),
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }


        #endregion

        #region Export Excel
        //Excel funtionality
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {//header grid data
                    var lst = db.SP_PLN_LOCKINGCLEAT_HEADER_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Project = uc.Project,
                                      Document = uc.Document,
                                      Customer = uc.Customer,
                                      JointType = uc.JointType,
                                      RevNo = "R" + uc.RevNo,
                                      Status = uc.Status,
                                      SubmittedBy = uc.SubmittedBy,
                                      SubmittedOn = uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ApprovedBy = uc.ApprovedBy,
                                      ApprovedOn = uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.lockingcleat.GetStringValue())
                {  //locking cleat lines
                    var lst = db.SP_PLN_LOCKINGCLEAT_LC_LINES_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();


                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from uc in lst
                                  select new
                                  {
                                      LCDocument = uc.LCDocument,
                                      JointType = uc.JointType,
                                      LineStatus = uc.LineStatus,
                                      ReturnRemark = uc.ReturnRemark,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.tackWeld.GetStringValue())
                {  //tack weld lines
                    var lst = db.SP_PLN_LOCKINGCLEAT_TW_LINES_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from uc in lst
                                  select new
                                  {
                                      TWDocument = uc.TWDocument,
                                      JointDetails = uc.JointDetails,
                                      LineStatus = uc.LineStatus,
                                      ReturnRemark = uc.ReturnRemark,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {
                    var lst = db.SP_PLN_LOCKINGCLEAT_HEADER_HISTORY_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Project = uc.Project,
                                      Document = uc.Document,
                                      Customer = uc.Customer,
                                      JointType = uc.JointType,
                                      RevNo = "R" + uc.RevNo,
                                      Status = uc.Status,
                                      SubmittedBy = uc.SubmittedBy,
                                      SubmittedOn = uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ApprovedBy = uc.ApprovedBy,
                                      ApprovedOn = uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.historylockingcleat.GetStringValue())
                {
                    var lst = db.SP_PLN_LOCKINGCLEAT_LC_LINES_DETAILS_HISTORY(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      LCDocument = uc.LCDocument,
                                      JointType = uc.JointType,
                                      LineStatus = uc.LineStatus,
                                      ReturnRemark = uc.ReturnRemark,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.historytackWeld.GetStringValue())
                {  //tack weld lines
                    var lst = db.SP_PLN_LOCKINGCLEAT_TW_LINES_DETAILS_HISTORY(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from uc in lst
                                  select new
                                  {
                                      TWDocument = uc.TWDocument,
                                      JointDetails = uc.JointDetails,
                                      LineStatus = uc.LineStatus,
                                      ReturnRemark = uc.ReturnRemark,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        public class CustomResponceMsg : clsHelper.ResponseMsg
        {
            public string Project;
            public string Customer;
            public int HeaderID;
            public int LineID;
            public string Status;
            public string CDD;
            public int? rev;
        }

    }
}