﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.PLN.Controllers
{
    public class MaintainPlanController : clsBase
    {
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult CreatePlan(int HeaderID = 0)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("PLN001");
            if (HeaderID > 0)
            {
                string currentUser = objClsLoginInfo.UserName;
                PLN001 objPLN001 = db.PLN001.Where(x => x.HeaderId == HeaderID && x.CreatedBy == currentUser).FirstOrDefault();
                //if (objPLN001 == null)
                //{
                //    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                //}
            }
            ViewBag.HeaderID = HeaderID;
            return View();
        }

        [HttpPost]
        public ActionResult LoadPlanCreateFormPartial(int HeaderID)
        {

            PLN001 objPLN001 = db.PLN001.Where(x => x.HeaderId == HeaderID).FirstOrDefault();

            if (objPLN001 == null)
            {
                var lstPLN001 = db.PLN001.Select(x => x.Plan).Distinct().ToList();
                List<string> lstPlan = new List<string>()
                {
                    "Section Handling Plan",
                    "Equipment Jacking Plan",
                    "Tank Rotator Plan",
                    "Positioner Plan",
                    "Hydro Test Plan",
                    "Equipment Handling Plan",
                    "Reference Sketch",
                    "Manufacturing Sequence & Schedule for Equipment"
                };//clsImplementationEnum.GetPlanList().ToList();
                if (lstPLN001 != null && lstPLN001.Count > 0)
                {
                    ViewBag.lstPlan = lstPlan.Where(x => !lstPLN001.Contains(x.ToString())).AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();
                }
                else
                {
                    ViewBag.lstPlan = lstPlan.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();
                }
                objPLN001 = new PLN001();
                objPLN001.HeaderId = 0;
                objPLN001.RevNo = 0;
                objPLN001.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
            }
            else
            {
                List<string> lstPlan = new List<string>();
                lstPlan.Add(objPLN001.Plan);
                string ApproverName = db.COM003.Where(x => x.t_psno == objPLN001.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                if (!string.IsNullOrEmpty(ApproverName))
                {
                    ViewBag.ApproverName = objPLN001.ApprovedBy + " - " + ApproverName;
                }
                ViewBag.lstPlan = lstPlan.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();
            }
            return PartialView("_LoadPlanCreateFormPartial", objPLN001);
        }

        [HttpPost]
        public ActionResult LoadPlanListDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_PlanListDataPartial");
        }

        [HttpPost]
        public JsonResult LoadPlanHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "LATEST")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.PlanStatus.DRAFT.GetStringValue() + "','" + clsImplementationEnum.PlanStatus.Returned.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "1=1";
                }
                // strWhere += " and pln001.CreatedBy=('" + objClsLoginInfo.UserName + "')";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = { "[Plan]", "Document", "RevNo", "[Status]", "pln001.CreatedBy +' - '+com003c.t_name", "pln001.EditedBy +' - '+com003e.t_name", "pln001.ApprovedBy +' - '+com003a.t_name" };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var objPLN001LatestRevlist = (from t in db.PLN001
                                              group t by new { t.Document, t.RevNo }
                            into grp
                                              select new
                                              {
                                                  grp.Key.Document,
                                                  RevNo = grp.Max(t => t.RevNo)
                                              }

                            ).ToList().Select(i => new { i.Document, RevNo = Convert.ToInt32(i.RevNo) }).ToList();

                var lstResult = db.SP_FETCH_PLAN_HEADERS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.PlanName),
                               Convert.ToString(uc.Document),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn),
                               Convert.ToString(uc.EditedBy),
                               Convert.ToString(uc.EditedOn),
                                Convert.ToString(uc.ApprovedBy),
                               Convert.ToString(uc.ApprovedOn),
                               Convert.ToString(uc.ReturnRemark),
                               GetButtonsStatusWise(uc.Status,uc.HeaderId,(lstResult.Count >0 && objPLN001LatestRevlist.Where(x=>x.Document==uc.Document).Select(x=>x.RevNo).Max() ==Convert.ToInt32(uc.RevNo) &&  uc.Status == clsImplementationEnum.PlanStatus.Approved.GetStringValue()?true:false))
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string GetButtonsStatusWise(string status, int headerId, bool IsVisisble)
        {
            string strButtons = string.Empty;
            if (status == clsImplementationEnum.PlanStatus.Approved.GetStringValue() && IsVisisble)
            {
                strButtons = "<center><a title=\"Revision\" href=\"" + WebsiteURL + "/PLN/MaintainPlan/CreatePlan?HeaderID=" + headerId + "\"><i style=\"margin-left:5px;\" class=\"fa fa-eye\"></i></a><a onclick=\"CreateRevision(" + headerId + ")\" class=\"btn btn-xs\" href=\"javascript:void(0)\"><i style=\"margin-left:5px;\" class=\"fa fa-share\"></i></a></center>";
            }
            else
            {
                strButtons = "<center><a title=\"View\" href=\"" + WebsiteURL + "/PLN/MaintainPlan/CreatePlan?HeaderID=" + headerId + "\"><i style=\"margin-left:5px;\" class=\"fa fa-eye\"></i></a></center>";
            }
            return strButtons;
        }

        [HttpPost]
        public ActionResult ReviseData(int HeaderId)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            PLN001 objPLN001 = db.PLN001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            if (objPLN001.Status == clsImplementationEnum.PlanStatus.Approved.GetStringValue())
            {
                PLN001 objPLN001New = new PLN001();
                objPLN001New.Plan = objPLN001.Plan;
                objPLN001New.Document = objPLN001.Document;
                objPLN001New.RevNo = Convert.ToInt32(objPLN001.RevNo) + 1;
                objPLN001New.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                objPLN001New.CreatedBy = objClsLoginInfo.UserName;
                objPLN001New.CreatedOn = DateTime.Now;
                objPLN001New.ApprovedBy = objPLN001.ApprovedBy;
                db.PLN001.Add(objPLN001New);
                db.SaveChanges();

                List<PLN002> lstPLN002 = db.PLN002.Where(x => x.HeaderId == objPLN001.HeaderId).ToList();
                List<PLN002> tmplstPLN002 = new List<PLN002>();
                if (lstPLN002 != null && lstPLN002.Count > 0)
                {
                    tmplstPLN002.AddRange(lstPLN002);
                    tmplstPLN002.ForEach(x => { x.EditedBy = null; x.EditedOn = null; x.HeaderId = objPLN001New.HeaderId; x.RevNo = objPLN001New.RevNo; });
                    db.PLN002.AddRange(tmplstPLN002);
                    db.SaveChanges();
                }
                objResponseMsg.HeaderID = objPLN001New.HeaderId;
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.PlanMessages.Insert.ToString();
            }

            return Json(objResponseMsg);
        }

        [HttpPost]
        public ActionResult SaveHeader(PLN001 pln001)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                if (pln001.HeaderId > 0)
                {
                    PLN001 objPLN001 = db.PLN001.Where(x => x.HeaderId == pln001.HeaderId).FirstOrDefault();
                    if (objPLN001.Status == clsImplementationEnum.PlanStatus.Approved.GetStringValue())
                    {
                        objPLN001 = new PLN001();
                        objPLN001.Plan = pln001.Plan;
                        objPLN001.Document = pln001.Document;
                        objPLN001.RevNo = Convert.ToInt32(objPLN001.RevNo) + 1;
                        objPLN001.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                        objPLN001.CreatedBy = objClsLoginInfo.UserName;
                        objPLN001.CreatedOn = DateTime.Now;
                        objPLN001.ApprovedBy = pln001.ApprovedBy;
                        db.PLN001.Add(objPLN001);
                        db.SaveChanges();
                        objResponseMsg.HeaderID = objPLN001.HeaderId;
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.PlanMessages.Insert.ToString();

                    }
                    else
                    {
                        objPLN001.Plan = pln001.Plan;
                        objPLN001.Document = pln001.Document;
                        objPLN001.EditedBy = objClsLoginInfo.UserName;
                        objPLN001.EditedOn = DateTime.Now;
                        objPLN001.ApprovedBy = pln001.ApprovedBy;
                        db.SaveChanges();
                        objResponseMsg.HeaderID = objPLN001.HeaderId;
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.PlanMessages.Update.ToString();

                    }
                }
                else
                {
                    PLN001 objPLN001 = new PLN001();
                    objPLN001.Plan = pln001.Plan;
                    objPLN001.Document = pln001.Document;
                    objPLN001.RevNo = 0;
                    objPLN001.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                    objPLN001.CreatedBy = objClsLoginInfo.UserName;
                    objPLN001.CreatedOn = DateTime.Now;
                    objPLN001.ApprovedBy = pln001.ApprovedBy;
                    db.PLN001.Add(objPLN001);
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objPLN001.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PlanMessages.Insert.ToString();

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetPlanLinesForm(int LineId, int HeaderId)
        {
            PLN001 objPLN001 = db.PLN001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            PLN002 objPLN002 = db.PLN002.Where(x => x.LineId == LineId).FirstOrDefault();
            if (objPLN002 == null)
            {
                objPLN002 = new PLN002();
            }
            objPLN002.HeaderId = objPLN001.HeaderId;
            objPLN002.Plan = objPLN001.Plan;
            objPLN002.Document = objPLN001.Document;
            return PartialView("_GetPlanLinesForm", objPLN002);
        }

        [HttpPost]
        public ActionResult SavePlanLines(PLN002 pln002, FormCollection fc)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                PLN001 objPLN001 = db.PLN001.Where(x => x.HeaderId == pln002.HeaderId).FirstOrDefault();
                if (pln002.LineId > 0)
                {
                    PLN002 objPLN002 = db.PLN002.Where(x => x.LineId == pln002.LineId).FirstOrDefault();
                    objPLN002.RelatedTo = pln002.RelatedTo;
                    objPLN002.CheckPoint = pln002.CheckPoint;
                    objPLN002.EditedBy = objClsLoginInfo.UserName;
                    objPLN002.EditedOn = DateTime.Now;
                }
                else
                {
                    db.PLN002.Add(new PLN002
                    {
                        HeaderId = pln002.HeaderId,
                        Plan = pln002.Plan,
                        Document = pln002.Document,
                        RelatedTo = pln002.RelatedTo,
                        RevNo = objPLN001.RevNo,
                        CheckPoint = pln002.CheckPoint,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now,
                    });
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
                if (pln002.LineId > 0)
                {
                    objResponseMsg.Value = clsImplementationMessage.PlanMessages.Update;
                }
                else
                {
                    objResponseMsg.Value = clsImplementationMessage.PlanMessages.Insert;
                }
                //objResponseMsg.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult LoadPlanHeaderLineData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();
                string[] arrayCTQStatus = { clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(), clsImplementationEnum.PlanStatus.Returned.GetStringValue() };
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                PLN001 objPLN001 = db.PLN001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = { "RelatedTo", "[CheckPoint]", "pln002.CreatedBy +' - '+com003.t_name", "pln002.EditedBy +' - '+com003.t_name" };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }

                #region Sorting
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstResult = db.SP_FETCH_PLAN_HEADERS_LINES
                                (
                               StartIndex,
                               EndIndex,
                               strSortOrder,
                               strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.RelatedTo),
                                Convert.ToString(uc.CheckPointDesc),
                                Convert.ToString(uc.CreatedBy),
                                Convert.ToString(uc.CreatedOn),
                                Convert.ToString(uc.EditedBy),
                                Convert.ToString(uc.EditedOn),
                                LineOperation(objPLN001.Status,uc.LineId,uc.HeaderId),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SendForApproval(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrLineStatus = { clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(), clsImplementationEnum.PlanStatus.Returned.GetStringValue() };
                List<PLN002> lstPLN002 = db.PLN002.Where(x => x.HeaderId == HeaderId).ToList();
                if (lstPLN002 != null && lstPLN002.Count > 0)
                {
                    foreach (var objData in lstPLN002)
                    {
                        PLN002 objPLN002 = lstPLN002.Where(x => x.LineId == objData.LineId).FirstOrDefault();
                        objPLN002.EditedBy = objClsLoginInfo.UserName;
                        objPLN002.EditedOn = DateTime.Now;
                    }
                }
                PLN001 objPLN001 = db.PLN001.Where(x => x.HeaderId == HeaderId && arrLineStatus.Contains(x.Status)).FirstOrDefault();
                if (objPLN001 != null)
                {
                    objPLN001.Status = clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue();
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Lines successfully sent for approval.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Lines not available for approval.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //added by nikita on 31/08/2017
        [HttpPost]
        public ActionResult RetractStatus(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PLN001 objPLN001 = db.PLN001.Where(u => u.HeaderId == headerId).SingleOrDefault();
                if (objPLN001 != null)
                {
                    objPLN001.Status = clsImplementationEnum.PLCStatus.DRAFT.GetStringValue();
                    objPLN001.EditedBy = objClsLoginInfo.UserName;
                    objPLN001.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Retract.ToString();
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteLine(int LineID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PLN002 objPLN002 = db.PLN002.Where(x => x.LineId == LineID).FirstOrDefault();
                if (objPLN002 != null)
                {
                    db.PLN002.Remove(objPLN002);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Line successfully deleted.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Line not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public string LineOperation(string status, int LineId, int HeaderId)
        {
            string strButtons = string.Empty;
            if (status != clsImplementationEnum.PlanStatus.Approved.GetStringValue() && status != clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue())
            {
                strButtons = "<center><i id=\"btnEdit\" name=\"btnAction\" style=\"cursor: pointer;\" Title=\"Edit Record\" class=\"fa fa-pencil - square - o\"  onclick=\"GetPlanLinesForm(" + LineId + "," + HeaderId + ")\" ></i>&nbsp;&nbsp;<i id=\"btnDelete\" name=\"btnAction\" style=\"cursor: pointer;\" Title=\"Delete Record\" class=\"fa fa-trash - o\" onClick=\"DeleteRecord(" + LineId + ")\"></i></center>";
            }
            return strButtons;
        }

        #region Export Excell
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_FETCH_PLAN_HEADERS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      PlanName = li.PlanName,
                                      Document = li.Document,
                                      RevNo = "R" + li.RevNo,
                                      Status = li.Status,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn,
                                      ReturnRemark = li.ReturnRemark,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_FETCH_PLAN_HEADERS_LINES(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from li in lst
                                  select new
                                  {
                                      ROW_NO = li.ROW_NO,
                                      RelatedTo = li.RelatedTo,
                                      CheckPointDesc = li.CheckPointDesc,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }

    public class ResponceMsgWithHeaderID : clsHelper.ResponseMsg
    {
        public int HeaderID;
    }
    public class ResponceMsgWithStatus : clsHelper.ResponseMsg
    {
        public int HeaderID;
        public string Status;
        public int? Revision;
    }
}