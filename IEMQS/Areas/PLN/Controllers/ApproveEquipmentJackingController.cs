﻿using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.PLN.Controllers
{
    public class ApproveEquipmentJackingController : clsBase
    {
        //modified by nikita on 29/8/2017
        //added method UpdatePDN002
        //task assigned by Satish Pawar
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadEquipmentJackingListDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_EquipmentJackingListDataPartial");
        }

        [HttpPost]
        public JsonResult LoadEquipmentJackingHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue() + "') and pln016.ApprovedBy in('" + objClsLoginInfo.UserName + "')";
                }
                else
                {
                    strWhere += "1=1";
                }
                //strWhere += " and pln016.ApprovedBy in('" + objClsLoginInfo.UserName + "')";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "pln016.Project+' - '+com1.t_dsca",
                                                "Document",
                                                "pln016.Customer+' - '+com6.t_nama",
                                                "Status",
                                                "Product",
                                                "ProcessLicensor",
                                                "ProcessPlan",
                                                "JobNo"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstResult = db.SP_FETCH_EQUIPMENTJACKINGPLAN_HEADERS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString(uc.Customer),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.CDD),
                               Convert.ToString(uc.ProcessPlan),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn),
                               Convert.ToString(uc.EditedBy),
                               Convert.ToString(uc.EditedOn),
                                    Convert.ToString(uc.SubmittedBy),
                                Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),

                                Convert.ToString(uc.ApprovedBy),
                               Convert.ToString(uc.ApprovedOn),
                               Convert.ToString(uc.ReturnRemark),
                                Convert.ToString(uc.HeaderId),
                               // "<a title='View' href='/PLN/ApproveEquipmentJacking/ApproveEquipmentJacking?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a>"+(uc.RevNo>0 ?"<a title=\"History\" onclick=\"ViewHistoryForProjectPLN('"+uc.HeaderId+"','Approver','/PLN/MaintainEquipmentJacking/GetHistoryDetails','Equipment Jacking')\"><i style='margin-left:5px;' class='fa fa-history'></i></a>":"")+"<a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintainEquipmentJacking/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')><i style='margin-left:5px;cursor:pointer;' class='fa fa-clock-o'></i></a>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]//, AllowAnonymous, UserPermissions
        public ActionResult ApproveEquipmentJacking(int HeaderID = 0)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("PLN016");
            if (HeaderID > 0)
            {
                string currentUser = objClsLoginInfo.UserName;
                PLN016 objPLN016 = db.PLN016.Where(x => x.HeaderId == HeaderID).FirstOrDefault();               
                var PlanningDinID = db.PDN002.Where(x => x.RefId == HeaderID && x.DocumentNo == objPLN016.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;
            }
            ViewBag.HeaderID = HeaderID;
            return View();
        }

        [HttpPost]
        public ActionResult LoadEquipmentJackingCreateFormPartial(int HeaderID = 0)
        {
            PLN016 objPLN016 = db.PLN016.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
            if (objPLN016 != null)
            {
                if (objPLN016.ApprovedBy != null)
                {
                    if (objPLN016.ApprovedBy.Trim() != objClsLoginInfo.UserName.Trim())
                    {
                        ViewBag.IsDisplayMode = true;
                    }
                    else
                    {
                        ViewBag.IsDisplayMode = false;
                    }
                }
                else
                {
                    ViewBag.IsDisplayMode = true;
                }
                var projectDetails = db.COM001.Where(x => x.t_cprj == objPLN016.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                objPLN016.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string ApproverName = db.COM003.Where(x => x.t_psno == objPLN016.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                objPLN016.ApprovedBy = objPLN016.ApprovedBy + " - " + ApproverName;
                string customerName = db.COM006.Where(x => x.t_bpid == objPLN016.Customer).Select(x => x.t_nama).FirstOrDefault();
                objPLN016.Customer = objPLN016.Customer + " - " + customerName;
                ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments1("PLN016/" + objPLN016.HeaderId + "/" + objPLN016.RevNo));
                string strEquipment = clsImplementationEnum.PlanList.Equipment_Jacking_Plan.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strEquipment &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();
                if (latestRecord != null)
                {
                    PLN012 objPLN012 = db.PLN012.Where(x => x.HeaderId == objPLN016.HeaderId).FirstOrDefault();
                    if (objPLN012 != null)
                    {
                        if (latestRecord.RevNo == objPLN012.PlanRevNo)
                        {
                            ViewBag.isLatest = "true";
                        }
                        else
                        {
                            ViewBag.isLatest = "false";
                        }
                    }
                    else
                    {
                        ViewBag.isLatest = "false";
                    }
                }
                else
                {
                    ViewBag.isLatest = "true";
                }
                var PlanningDinID = db.PDN002.Where(x => x.RefId == HeaderID && x.DocumentNo == objPLN016.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;
                ViewBag.Revoke = Manager.IsRevokeApplicable(PlanningDinID, HeaderID, clsImplementationEnum.PlanList.Equipment_Jacking_Plan.GetStringValue());
            }
            else
            {
                objPLN016 = new PLN016();
                objPLN016.ProcessPlan = clsImplementationEnum.PlanList.Stool_Plan.GetStringValue();
                objPLN016.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                objPLN016.RevNo = 0;
            }
            ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            return PartialView("_LoadEquipmentJackingCreateFormPartial", objPLN016);
        }

        [HttpPost]
        public JsonResult LoadCheckListDataNew(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                PLN016 objPLN016 = db.PLN016.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                string HeaderStatus = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                bool IsDisable = true;
                if (objPLN016 != null)
                {
                    HeaderStatus = objPLN016.Status;
                }
                //if (HeaderId == 0 || (HeaderId > 0 && (HeaderStatus.ToLower() == clsImplementationEnum.PlanStatus.Approved.GetStringValue().ToLower())))
                //{
                //    IsDisable = true;
                //}
                //else if (HeaderId == 0 || (HeaderId > 0 && (HeaderStatus.ToLower() == clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue().ToLower()) && objPLN016.ApprovedBy!=objClsLoginInfo.UserName))
                //{
                //    IsDisable = true;
                //}
                if (HeaderStatus.ToLower() == clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue().ToLower() && objPLN016.ApprovedBy.Trim() == objClsLoginInfo.UserName.Trim())
                {
                    IsDisable = false;
                }
                string strEquipment = clsImplementationEnum.PlanList.Equipment_Jacking_Plan.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strEquipment &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();

                if (latestRecord == null)
                {
                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = 0,
                        iTotalDisplayRecords = 0,
                        aaData = new string[] { }
                    }, JsonRequestBehavior.AllowGet);
                }
                int? PlanRev = 0;
                if (HeaderId > 0)
                {
                    strWhere += "pln022.HeaderId = " + HeaderId + " and ";
                    PLN022 objPLN022 = db.PLN022.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    if (objPLN022 != null)
                    {
                        PlanRev = objPLN022.PlanRevNo;
                    }
                    else
                    {
                        PlanRev = 0;
                    }
                }
                else
                {
                    PlanRev = latestRecord.RevNo;
                }
                strWhere += "1=1 and pln002.[Plan] in('" + clsImplementationEnum.PlanList.Equipment_Jacking_Plan.GetStringValue() + "') and pln002.RevNo = " + PlanRev + "";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = { "RelatedTo", "[CheckPoint]" };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                string strWhere1 = (!string.IsNullOrWhiteSpace(strWhere) ? strWhere : null);

                #region Sorting
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstResult = db.SP_FETCH_EQUIPMENTJACKINGPLAN_HEADER_LINE
                                (
                                     HeaderId,
                                     latestRecord.RevNo,
                                     StartIndex,
                                     EndIndex,
                                     strSortOrder,
                                    strWhere1
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.RelatedTo),
                                Convert.ToString(uc.CheckPointDesc),
                                //LineOperation(Convert.ToBoolean(uc.Yes_No),uc.LineId,uc.HeaderId),
                               (new PLNCommonFunction()).CheckListLineOperationYesNoNew(uc.Yes_No,uc.LineId,uc.HeaderId,IsDisable,uc.ActualId,"Yes_No","UpdateData(this)"),
                                (new PLNCommonFunction()).CheckListLineOperationRemarksNew(uc.Remarks,uc.LineId,uc.HeaderId,IsDisable,uc.ActualId,"Remarks","UpdateData(this)"),
                                Convert.ToString(uc.CreatedBy),
                                Convert.ToString(uc.CreatedOn),
                                Convert.ToString(uc.EditedBy),
                                Convert.ToString(uc.EditedOn),
                                //getChecklistDetails(uc.CheckListId.ToString())[0],
                                //getChecklistDetails(uc.CheckListId.ToString())[1],
                                //getChecklistDetails(uc.CheckListId.ToString())[2],
                                //getChecklistDetails(uc.CheckListId.ToString())[3],
                                Convert.ToString(uc.IsLatest),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere1
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdateData(string id, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var splitid = id.Split(',');
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    db.SP_PLN_UPDATE_CHECKLIST_TABLE_COLUMN("pln022", "LineId", splitid[0], columnName, columnValue);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [NonAction]
        public string[] getChecklistDetails(string lineId)
        {
            string[] checklist = new string[4];
            int LineID = Int32.Parse(lineId);
            var data = (from p2 in db.PLN002
                        join
                            p1 in db.PLN001 on p2.HeaderId equals p1.HeaderId
                        where p2.LineId == LineID
                        select p1).FirstOrDefault();
            if (data != null)
            {
                checklist[0] = data.CreatedBy + "-" + Manager.GetUserNameFromPsNo(data.CreatedBy); //prepared by
                checklist[1] = data.ApprovedBy + "-" + Manager.GetUserNameFromPsNo(data.ApprovedBy); // approved by
                checklist[2] = data.ApprovedOn.Value != null ? data.ApprovedOn.Value.ToString("dd/MM/yyyy") : string.Empty; // approved on
                checklist[3] = data.Document; // document no
            }
            return checklist;
        }

        [HttpPost]
        public ActionResult ReturnEquipmentJacking(int HeaderId, string Remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string Status = clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue().ToUpper();
                PLN016 objPLN016 = db.PLN016.Where(x => x.HeaderId == HeaderId && x.Status.ToUpper() == Status).FirstOrDefault();
                if (objPLN016 != null)
                {
                    objPLN016.Status = clsImplementationEnum.PlanStatus.Returned.GetStringValue();
                    objPLN016.ReturnRemark = Remarks;
                    objPLN016.EditedBy = objClsLoginInfo.UserName;
                    objPLN016.EditedOn = DateTime.Now;




                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Plan successfully returned.";
                    //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                    Manager.UpdatePDN002(objPLN016.HeaderId, objPLN016.Status, objPLN016.RevNo, objPLN016.Project, objPLN016.Document);

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(),
                                                        objPLN016.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objPLN016.Project, clsImplementationEnum.PlanList.Equipment_Jacking_Plan.GetStringValue(), objPLN016.RevNo.Value.ToString(), objPLN016.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Equipment_Jacking_Plan.GetStringValue(), objPLN016.HeaderId.ToString(), false),
                                                        objPLN016.CreatedBy);
                    #endregion
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please add remarks for returning Plan.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApproveByApproverSelected(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
            for (int i = 0; i < arrayHeaderIds.Length; i++)
            {
                int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                objResponseMsg = ApproveHeaderByApprover(HeaderId);
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApproveByApprover(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                objResponseMsg = ApproveHeaderByApprover(HeaderId);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponseMsg ApproveHeaderByApprover(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string Status = clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue().ToUpper();
                PLN016 objPLN016 = db.PLN016.Where(x => x.HeaderId == HeaderId && x.Status.ToUpper() == Status).FirstOrDefault();
                if (objPLN016 != null)
                {
                    objPLN016.Status = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                    objPLN016.ApprovedBy = objClsLoginInfo.UserName;
                    objPLN016.ApprovedOn = DateTime.Now;




                    #region Header Log
                    var lstPLN016_Log = db.PLN016_Log.Where(x => x.HeaderId == objPLN016.HeaderId).ToList();
                    PLN016_Log objPLN016_Log = new PLN016_Log();
                    if (objPLN016_Log != null)
                    {
                        objPLN016_Log = lstPLN016_Log.FirstOrDefault();
                        lstPLN016_Log.ForEach(i => { i.Status = clsImplementationEnum.PlanStatus.Superseded.GetStringValue(); });
                    }

                    objPLN016_Log = db.PLN016_Log.Add(new PLN016_Log
                    {
                        HeaderId = objPLN016.HeaderId,
                        Project = objPLN016.Project,
                        Document = objPLN016.Document,
                        Customer = objPLN016.Customer,
                        RevNo = objPLN016.RevNo,
                        Status = objPLN016.Status,
                        CDD = objPLN016.CDD,
                        Product = objPLN016.Product,
                        ProcessLicensor = objPLN016.ProcessLicensor,
                        ProcessPlan = objPLN016.ProcessPlan,
                        Attachment = objPLN016.Attachment,
                        JobNo = objPLN016.JobNo,
                        CreatedBy = objPLN016.CreatedBy,
                        CreatedOn = objPLN016.CreatedOn,
                        EditedBy = objPLN016.EditedBy,
                        EditedOn = objPLN016.EditedOn,
                        ApprovedBy = objPLN016.ApprovedBy,
                        ApprovedOn = objPLN016.ApprovedOn,
                        ReturnRemark = objPLN016.ReturnRemark,
                        ReviseRemark = objPLN016.ReviseRemark,
                        SubmittedBy = objPLN016.SubmittedBy,
                        SubmittedOn = objPLN016.SubmittedOn
                    });

                    #endregion
                    db.SaveChanges();

                    List<PLN022> lstPLN022 = db.PLN022.Where(x => x.HeaderId == HeaderId).ToList();
                    List<PLN022_Log> lstPLN022_Log = new List<PLN022_Log>();
                    if (lstPLN022 != null && lstPLN022.Count > 0)
                    {
                        lstPLN022_Log = lstPLN022.Select(x => new PLN022_Log()
                        {
                            RefId = objPLN016_Log.Id,
                            LineId = x.LineId,
                            HeaderId = x.HeaderId,
                            Project = x.Project,
                            Document = x.Document,
                            RevNo = x.RevNo,
                            Plan = x.Plan,
                            PlanRevNo = x.PlanRevNo,
                            CheckListId = x.CheckListId,
                            Yes_No = x.Yes_No,
                            Remarks = x.Remarks,
                            CreatedBy = x.CreatedBy,
                            CreatedOn = x.CreatedOn,
                            EditedBy = x.EditedBy,
                            EditedOn = x.EditedOn
                        }).ToList();
                        db.PLN022_Log.AddRange(lstPLN022_Log);
                        db.SaveChanges();
                    }

                    int? currentRevisionNull = db.PLN016.Where(q => q.HeaderId == HeaderId).FirstOrDefault().RevNo;
                    int currentRevision = ((currentRevisionNull == null) ? 0 : currentRevisionNull.Value);
                    //clsUpload.CopyFolderContents("PLN016/" + HeaderId + "/" + currentRevision, "PLN016/" + HeaderId + "/" + (currentRevision + 1));
                    var oldfolderPath = "PLN016//" + HeaderId + "//" + currentRevision;
                    var newfolderPath = "PLN016//" + HeaderId + "//" + (currentRevision + 1);
                    //(new clsFileUpload()).CopyFolderContentsAsync(oldfolderPath, newfolderPath);

                    Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                    _objFUC.CopyDataOnFCSServerAsync(oldfolderPath, newfolderPath, oldfolderPath, HeaderId, newfolderPath, HeaderId, DESServices.CommonService.GetUseIPConfig, DESServices.CommonService.objClsLoginInfo.UserName, 0);

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Plan successfully approved.";
                    //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                    int newId = db.PLN016_Log.Where(q => q.HeaderId == HeaderId).OrderByDescending(q => q.ApprovedOn).FirstOrDefault().Id;
                    Manager.UpdatePDN002(objPLN016.HeaderId, objPLN016.Status, objPLN016.RevNo, objPLN016.Project, objPLN016.Document, newId);

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(),
                                                        objPLN016.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objPLN016.Project, clsImplementationEnum.PlanList.Equipment_Jacking_Plan.GetStringValue(), objPLN016.RevNo.Value.ToString(), objPLN016.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Equipment_Jacking_Plan.GetStringValue(), objPLN016.HeaderId.ToString(), false),
                                                        objPLN016.CreatedBy);
                    #endregion
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Plan not available approved.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return objResponseMsg;
        }

        [SessionExpireFilter]//, AllowAnonymous, UserPermissions
        public ActionResult ViewHistory(int Id = 0)
        {
            if (Id > 0)
            {
                string currentUser = objClsLoginInfo.UserName;
                PLN016_Log objPLN016_Log = db.PLN016_Log.Where(x => x.Id == Id).FirstOrDefault();
                if (objPLN016_Log == null)
                {
                    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                }
            }
            ViewBag.Id = Id;
            return View();
        }

        [HttpPost]
        public ActionResult LoadEquipmentJackingHistoryFormPartial(int Id = 0)
        {
            PLN016_Log objPLN016_Log = db.PLN016_Log.Where(x => x.Id == Id).FirstOrDefault();
            if (objPLN016_Log != null)
            {
                var projectDetails = db.COM001.Where(x => x.t_cprj == objPLN016_Log.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                objPLN016_Log.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string ApproverName = db.COM003.Where(x => x.t_psno == objPLN016_Log.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                objPLN016_Log.ApprovedBy = objPLN016_Log.ApprovedBy + " - " + ApproverName;
                string customerName = db.COM006.Where(x => x.t_bpid == objPLN016_Log.Customer).Select(x => x.t_nama).FirstOrDefault();
                objPLN016_Log.Customer = objPLN016_Log.Customer + " - " + customerName;
                ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments1("PLN016/" + objPLN016_Log.HeaderId + "/" + objPLN016_Log.RevNo));
            }
            else
            {
                objPLN016_Log = new PLN016_Log();
                objPLN016_Log.ProcessPlan = clsImplementationEnum.PlanList.Stool_Plan.GetStringValue();
                objPLN016_Log.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                objPLN016_Log.RevNo = 0;
            }
            // ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            return PartialView("_LoadEquipmentJackingHistoryFormPartial", objPLN016_Log);
        }

        [HttpPost]
        public ActionResult SaveHeader(PLN016 pln016, string strHeaderId, string strLineId, string strIsLatest)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                int NewHeaderId = 0;
                if (pln016.HeaderId > 0)
                {
                    PLN016 objPLN016 = db.PLN016.Where(x => x.HeaderId == pln016.HeaderId).FirstOrDefault();
                    if (objPLN016 != null)
                    {
                        objPLN016.Product = pln016.Product;
                        objPLN016.ProcessLicensor = pln016.ProcessLicensor;
                        objPLN016.JobNo = pln016.JobNo;
                        objPLN016.ApprovedBy = pln016.ApprovedBy.Split('-')[0].ToString().Trim();
                        objPLN016.EditedBy = objClsLoginInfo.UserName;
                        objPLN016.EditedOn = DateTime.Now;
                        if (objPLN016.Status == clsImplementationEnum.PlanStatus.Approved.GetStringValue())
                        {
                            objPLN016.RevNo = Convert.ToInt32(objPLN016.RevNo) + 1;
                            objPLN016.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                        }
                        if (!Convert.ToBoolean(strIsLatest))
                        {
                            List<PLN022> lstPLN022 = db.PLN022.Where(x => x.HeaderId == objPLN016.HeaderId).ToList();
                            if (lstPLN022 != null && lstPLN022.Count > 0)
                            {
                                db.PLN022.RemoveRange(lstPLN022);
                            }
                            string strEquipment = clsImplementationEnum.PlanList.Equipment_Handling_Plan.GetStringValue();
                            string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                            var latestRecord = db.PLN001
                                     .Where(x
                                                => x.Plan == strEquipment &&
                                                   x.Status == strStatus
                                            )
                                     .OrderByDescending(x => x.HeaderId)
                                     .Select(x => new { x.HeaderId, x.RevNo })
                                     .FirstOrDefault();
                            List<PLN002> lstPLN002 = db.PLN002.Where(x => x.HeaderId == latestRecord.HeaderId).ToList();
                            if (lstPLN002 != null && lstPLN002.Count > 0)
                            {
                                List<PLN022> NewlstPLN022 = lstPLN002.Select(x => new PLN022()
                                {
                                    LineId = x.LineId,
                                    HeaderId = objPLN016.HeaderId,
                                    Project = objPLN016.Project,
                                    Document = objPLN016.Document,
                                    RevNo = objPLN016.RevNo,
                                    Plan = objPLN016.ProcessPlan,
                                    PlanRevNo = x.RevNo,
                                    CheckListId = x.LineId,
                                    //Yes_No = false,
                                    Yes_No = clsImplementationEnum.ChecklistYesNo.No.GetStringValue(),
                                    CreatedBy = objClsLoginInfo.UserName,
                                    CreatedOn = DateTime.Now
                                }).ToList();
                                db.PLN022.AddRange(NewlstPLN022);
                            }
                        }
                        else
                        {
                            List<PLN022> lstPLN022 = db.PLN022.Where(x => x.HeaderId == objPLN016.HeaderId).ToList();
                            if (lstPLN022.Count > 0)
                            {
                                List<PLN022> lstTruePLN022 = new List<PLN022>();

                                foreach (PLN022 objPLN022 in lstPLN022)
                                {
                                    //objPLN022.Yes_No = (!string.IsNullOrWhiteSpace(strLineId) && Array.ConvertAll(strLineId.Split(',').ToArray(), s => int.Parse(s)).Length > 0 && Array.ConvertAll(strLineId.Split(',').ToArray(), s => int.Parse(s)).Contains(objPLN022.LineId) ? true : false);                                    
                                    objPLN022.EditedBy = objClsLoginInfo.UserName;
                                    objPLN022.EditedOn = DateTime.Now;
                                }
                            }
                        }
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.PlanMessages.Update.ToString();
                        NewHeaderId = objPLN016.HeaderId;
                        objResponseMsg.Status = objPLN016.Status;
                        objResponseMsg.Revision = objPLN016.RevNo;
                        objResponseMsg.HeaderID = NewHeaderId;

                        //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                        Manager.UpdatePDN002(objPLN016.HeaderId, objPLN016.Status, objPLN016.RevNo, objPLN016.Project, objPLN016.Document);


                        //var folderPath = "PLN016/" + NewHeaderId + "/" + objPLN016.RevNo;

                        //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Process Details not available.";
                    }
                }


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RevokeDocument(int HeaderId, int pdinId, string Remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (Manager.IsRevokeApplicable(pdinId, HeaderId, clsImplementationEnum.PlanList.Equipment_Jacking_Plan.GetStringValue()))
                {
                    objResponseMsg.Key = Manager.RevokePDINDocument(HeaderId, clsImplementationEnum.PlanList.Equipment_Jacking_Plan, Remarks);
                    if (objResponseMsg.Key)
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revoke;
                    }
                    else
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.UnSuccess;
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.pdnRevokeMessage;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #region Export Excell
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "", int? HeaderId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_FETCH_EQUIPMENTJACKINGPLAN_HEADERS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      Document = li.Document,
                                      Customer = li.Customer,
                                      Product = li.Product,
                                      ProcessLicensor = li.ProcessLicensor,
                                      RevNo = "R" + li.RevNo,
                                      Status = li.Status,
                                      CDD = li.CDD,
                                      ProcessPlan = li.ProcessPlan,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn.HasValue ? Convert.ToString(li.CreatedOn) : "",
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn.HasValue ? Convert.ToString(li.EditedOn) : "",
                                      SubmittedBy = li.SubmittedBy,
                                      SubmittedOn = li.SubmittedOn.HasValue ? Convert.ToString(li.SubmittedOn) : "",
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn.HasValue ? Convert.ToString(li.ApprovedOn) : "",
                                      ReturnRemark = li.ReturnRemark,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    int? PlanRev = 0;
                    if (HeaderId > 0)
                    {
                        PLN022 objPLN022 = db.PLN022.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                        if (objPLN022 != null)
                        {
                            PlanRev = objPLN022.PlanRevNo;
                        }
                        else
                        {
                            PlanRev = 0;
                        }
                    }
                    else
                    {

                        string strEquipment = clsImplementationEnum.PlanList.Equipment_Jacking_Plan.GetStringValue();
                        string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                        var latestRecord = db.PLN001
                                 .Where(x
                                            => x.Plan == strEquipment &&
                                               x.Status == strStatus
                                        )
                                 .OrderByDescending(x => x.HeaderId)
                                 .Select(x => new { x.HeaderId, x.RevNo })
                                 .FirstOrDefault();
                        PlanRev = latestRecord.RevNo;
                    }
                    var lst = db.SP_FETCH_EQUIPMENTJACKINGPLAN_HEADER_LINE(HeaderId, PlanRev, 1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from li in lst
                                  select new
                                  {
                                      RelatedTo = li.RelatedTo,
                                      CheckPointDesc = li.CheckPointDesc,
                                      Yes_No = li.Yes_No,
                                      Remarks = li.Remarks,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      PreparedBy = getChecklistDetails(li.CheckListId.ToString())[0],
                                      CheckedBy = getChecklistDetails(li.CheckListId.ToString())[1],
                                      CheckedOn = getChecklistDetails(li.CheckListId.ToString())[2],
                                      DocumentNo = getChecklistDetails(li.CheckListId.ToString())[3],
                                      IsLatest = li.IsLatest
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}