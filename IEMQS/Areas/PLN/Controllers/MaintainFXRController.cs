﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQS.Areas.Utility.Models;
using static IEMQS.Areas.HTC.Controllers.MaintainHTCController;

namespace IEMQS.Areas.PLN.Controllers
{
    public class MaintainFXRController : clsBase
    {
        #region Utility
        public void InsertLinesEntry(FXR001 objFXR001)
        {
            List<FXR004> objFXR004 = db.FXR004.Where(x => x.Product == objFXR001.Product).ToList();
            List<FXR002> objFXR002 = new List<FXR002>();
            if (objFXR004 != null && objFXR004.Count > 0)
            {
                foreach (var item in objFXR004)
                {
                    objFXR002.Add(new FXR002
                    {
                        HeaderId = objFXR001.HeaderId,
                        Project = objFXR001.Project,
                        Document = objFXR001.Document,
                        RevNo = objFXR001.RevNo,
                        FixtureName = item.FixtureName,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now,
                        ParentId = 0,
                        IsManual = false
                    });
                }
                db.FXR002.AddRange(objFXR002);
                db.SaveChanges();
            }
        }
        public string HTMLActionString(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", bool isVisible = true)
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick=" + onClickMethod : "";

            if (isVisible)
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "' " + onClickEvent + " ></i>";
            }
            else
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer; display:none' Title='" + buttonTooltip + "' class='" + className + "' " + onClickEvent + " ></i>";
            }


            htmlControl = "<a>" + htmlControl + "</a>";
            return htmlControl;
        }

        public int FindChildCount(string fixtureName, int HeaderId)
        {
            return db.FXR002.Where(x => x.FixtureName.ToLower() == fixtureName.ToLower() && x.HeaderId == HeaderId).Count();
        }
        #endregion
        // GET: PLN/MaintainFXR
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadFixtureListDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_FixtureListDataPartial");
        }

        [HttpPost]
        public ActionResult LoadFixtureHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;


                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);


                string strWhere = string.Empty;
                strWhere += "1=1";
                //if (param.CTQCompileStatus.ToUpper() == "PENDING")
                //{
                //    strWhere += "1=1 and status in('" + clsImplementationEnum.CTQStatus.DRAFT.GetStringValue() + "','" + clsImplementationEnum.CTQStatus.Returned.GetStringValue() + "')";
                //}
                //else
                //{
                //    strWhere += "1=1";
                //}
                strWhere += " and FXR001.CreatedBy in('" + objClsLoginInfo.UserName + "')";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "FXR001.Project+' - '+com1.t_dsca",
                                                "Document",
                                                "FXR001.Customer+' - '+com6.t_nama",
                                                "Status",
                                                "Product",
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }


                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_FETCH_FIXTURES_HEADER
                         (
                         StartIndex, EndIndex, strSortOrder, strWhere
                         ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString(uc.Customer),
                               Convert.ToString(uc.Product),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.CDD),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn),
                               Convert.ToString(uc.EditedBy),
                               Convert.ToString(uc.EditedOn),
                               Convert.ToString(uc.SubmittedBy),
                               Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),
                               Convert.ToString(uc.ApprovedBy),
                               Convert.ToString(uc.ApprovedOn),
                               Convert.ToString(uc.ReturnRemark),
                               "<nobr>"+"<a title='View' href='"+WebsiteURL+"/PLN/MaintainFXR/CreateFixture?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i class='iconspace fa fa-eye'></i></a> "+
                                 Helper.GenerateActionIcon(uc.HeaderId,"Delete","Delete Record","fa fa-trash-o", "DeleteDocument("+ uc.HeaderId +",'/PLN/MaintainFXR/DeleteHeader', {headerid:"+uc.HeaderId+"}, 'tblFXRHeader')","",  (( uc.RevNo >0 && uc.Status != clsImplementationEnum.CommonStatus.SendForApprovel.GetStringValue()) || ( uc.RevNo == 0 && uc.Status == clsImplementationEnum.CommonStatus.Approved.GetStringValue()) ) ? false:true) +
                               "<a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintainFXR/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')><i style='cursor:pointer;' class='iconspace fa fa-clock-o'></i></a>" + " "
                               + (uc.RevNo > 0 ? HTMLActionString(uc.HeaderId,"History","History Record","iconspace fa fa-history","ViewHistoryData('"+ Convert.ToInt32(uc.HeaderId) +"');") : HTMLActionString(uc.HeaderId, "History","History Record","fa fa-history disabledicon","ViewHistoryData('"+ Convert.ToInt32(uc.HeaderId) +"');")) + " "
                               + HTMLActionString(uc.HeaderId,"Summary","Summary Record","iconspace fa fa-list-alt","ViewProjectSummary('"+ Convert.ToInt32(uc.HeaderId) +"');")+"</nobr>"
                                //"<center><a title='View' href='/PLN/MaintainFXR/CreateFixture?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a>&nbsp;&nbsp;<button class='btn btn-xs' onclick=ViewProjectSummary('"+ Convert.ToInt32(uc.HeaderId) +"')><i style='margin-left:5px;' class='fa fa-list-alt'></i></button>&nbsp;&nbsp<a class='btn btn-xs'  href='javascript:void(O); ' Title='Print' ><i style='margin - left:5px; ' class='glyphicon glyphicon-print'></i></a>"+ uc.RevNo > 0 ? "&nbsp;&nbsp<button class='btn btn-xs' onclick=ViewHistoryData('"+ Convert.ToInt32(uc.HeaderId) +"')><i style='margin-left:5px;' class='fa fa-history'></i></button>" : "" +"</center>"
                               //GetButtonsStatusWise(uc.Status,uc.HeaderId,(lstResult.Count >0 && lstResult.Select(x=>x.RevNo).Max() == uc.RevNo && uc.Status == clsImplementationEnum.PlanStatus.Approved.GetStringValue()?true:false))
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult DeleteHeader(int headerid)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                objResponseMsg = Manager.DeletePDINDocument(headerid, clsImplementationEnum.PlanList.Fixture);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ShowHistoryTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();

            model.TimelineTitle = "FXR TimeLine";
            model.Title = "PDinDoc";
            if (HeaderId > 0)
            {

                FXR001_Log objFXR001_Log = db.FXR001_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objFXR001_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objFXR001_Log.CreatedBy) : null;
                model.CreatedOn = objFXR001_Log.CreatedOn;
                model.EditedBy = objFXR001_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(objFXR001_Log.EditedBy) : null;
                model.EditedOn = objFXR001_Log.EditedOn;
                model.SubmittedBy = objFXR001_Log.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objFXR001_Log.SubmittedBy) : null;
                model.SubmittedOn = objFXR001_Log.SubmittedOn;

                model.ApprovedBy = objFXR001_Log.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objFXR001_Log.ApprovedBy) : null;
                model.ApprovedOn = objFXR001_Log.ApprovedOn;

            }
            else
            {

                FXR001_Log objFXR001_Log = db.FXR001_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objFXR001_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objFXR001_Log.CreatedBy) : null;
                model.CreatedOn = objFXR001_Log.CreatedOn;
                model.EditedBy = objFXR001_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(objFXR001_Log.EditedBy) : null;
                model.EditedOn = objFXR001_Log.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.Title = "PDinDoc";
            model.TimelineTitle = "FXR Timeline";

            if (HeaderId > 0)
            {
                FXR001 objFXR001 = db.FXR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objFXR001.CreatedBy != null ? Manager.GetUserNameFromPsNo(objFXR001.CreatedBy) : null;
                model.CreatedOn = objFXR001.CreatedOn;
                model.EditedBy = objFXR001.EditedBy != null ? Manager.GetUserNameFromPsNo(objFXR001.EditedBy) : null;
                model.EditedOn = objFXR001.EditedOn;

                // model.SubmittedBy = PMB001_Log.SendToCompiledOn != null ? Manager.GetUserNameFromPsNo(PMB001_Log.SendToCompiledOn) : null;
                //model.SubmittedOn = PMB001_Log.SendToCompiledOn;
                model.SubmittedBy = objFXR001.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objFXR001.SubmittedBy) : null;
                model.SubmittedOn = objFXR001.SubmittedOn;
                //model.CompiledBy = PMB001_Log.CompiledBy != null ? Manager.GetUserNameFromPsNo(PMB001_Log.CompiledBy) : null;
                // model.CompiledOn = PMB001_Log.CompiledOn;
                model.ApprovedBy = objFXR001.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objFXR001.ApprovedBy) : null;
                model.ApprovedOn = objFXR001.ApprovedOn;

            }
            else
            {
                FXR001 objFXR001 = db.FXR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objFXR001.CreatedBy != null ? Manager.GetUserNameFromPsNo(objFXR001.CreatedBy) : null;
                model.CreatedOn = objFXR001.CreatedOn;
                model.EditedBy = objFXR001.EditedBy != null ? Manager.GetUserNameFromPsNo(objFXR001.EditedBy) : null;
                model.EditedOn = objFXR001.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }

        #region History
        [HttpPost]
        public ActionResult GetHistoryDetails(int HeaderId)
        {
            ViewBag.HeaderId = HeaderId;
            return PartialView("_GetHistoryDetailsPartial");
        }

        [HttpPost]
        public JsonResult LoadFixtureHistory(JQueryDataTableParamModel param)
        {
            try
            {
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "1=1 and HeaderId = " + HeaderId + " ";
                //if (param.CTQCompileStatus.ToUpper() == "INITIATOR")
                //{
                //    strWhere += "1=1 and FXR001_Log.CreatedBy = '" + objClsLoginInfo.UserName + "'";

                //}
                //else
                //{
                //    strWhere += "1=1 and FXR001_Log.ApprovedBy = '" + objClsLoginInfo.UserName + "'";
                //}
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "FXR001_Log.Project+' - '+com1.t_dsca",
                                                "Document",
                                                "FXR001_Log.Customer+' - '+com6.t_nama",
                                                "Status",
                                                "Product",
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_FETCH_FIXTURES_HEADER_HISTORY
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString(uc.Customer),
                               Convert.ToString(uc.Product),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.CDD),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn),
                               Convert.ToString(uc.EditedBy),
                               Convert.ToString(uc.EditedOn),
                               Convert.ToString(uc.SubmittedBy),
                               Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),
                               Convert.ToString(uc.ApprovedBy),
                               Convert.ToString(uc.ApprovedOn),
                               //Convert.ToString(uc.ReturnRemark),
                               "<center><a title='View' href='"+WebsiteURL+"/PLN/MaintainFXR/ViewHistory?Id="+Convert.ToInt32(uc.Id)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a><a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintainFXR/ShowHistoryTimeline?HeaderID=" + Convert.ToInt32(uc.Id) + "')><i style = 'margin-left:5px;'  class='fa fa-clock-o'></i></a></center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]//, AllowAnonymous, UserPermissions
        public ActionResult ViewHistory(int Id = 0)
        {
            if (Id > 0)
            {
                string currentUser = objClsLoginInfo.UserName;
                //FXR001_Log objFXR001_Log = db.FXR001_Log.Where(x => x.Id == Id && x.CreatedBy == currentUser).FirstOrDefault();
                FXR001_Log objFXR001_Log = db.FXR001_Log.Where(x => x.Id == Id).FirstOrDefault();
                if (objFXR001_Log == null)
                {
                    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                }
            }
            ViewBag.Id = Id;
            return View();
        }

        [HttpPost]
        public ActionResult LoadFXRHistoryFormPartial(int Id = 0)
        {
            FXR001_Log objFXR001_Log = db.FXR001_Log.Where(x => x.Id == Id).FirstOrDefault();
            if (objFXR001_Log != null)
            {
                var projectDetails = db.COM001.Where(x => x.t_cprj == objFXR001_Log.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                ViewBag.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string ApproverName = db.COM003.Where(x => x.t_psno == objFXR001_Log.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                ViewBag.ApproverName = objFXR001_Log.ApprovedBy + " - " + ApproverName;
                string customerName = db.COM006.Where(x => x.t_bpid == objFXR001_Log.Customer).Select(x => x.t_nama).FirstOrDefault();
                ViewBag.customerName = objFXR001_Log.Customer + " - " + customerName;
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();

                var objFXR002_Log = db.FXR002_Log.Where(x => x.HeaderId == objFXR001_Log.HeaderId).ToList();

                decimal RM = Math.Truncate((objFXR002_Log.Select(x => x.Wt ?? 0).Sum() / 1000) * 1000) / 1000;
                decimal EM = Math.Truncate((objFXR002_Log.Select(x => x.ReqWt ?? 0).Sum() / 1000) * 1000) / 1000;
                decimal RUM = Math.Truncate((RM - EM) * 1000) / 1000;
                decimal ROP = RM != 0 ? Math.Truncate((RUM / RM) * 1000) / 1000 : 0;

                ViewBag.ReqMaterial = RM;
                ViewBag.EstimatedMaterial = EM;
                ViewBag.ReuseMaterial = RUM;
                ViewBag.ReusePercentage = ROP;
            }
            else
            {
                objFXR001_Log = new FXR001_Log();
                objFXR001_Log.Status = clsImplementationEnum.FixtureStatus.Draft.GetStringValue();
                objFXR001_Log.RevNo = 0;
            }
            // ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            var urlPrefix = WebsiteURL + "/PLN/MaintainFXR/ViewHistory?Id=";// AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.LastIndexOf('=') + 1);
            if (!objClsLoginInfo.GetUserRoleList().Contains("SHOP"))
            {
                ViewBag.RevPrev = (db.FXR001_Log.Any(q => (q.HeaderId == objFXR001_Log.HeaderId && q.RevNo == (objFXR001_Log.RevNo - 1))) ? urlPrefix + db.FXR001_Log.Where(q => (q.HeaderId == objFXR001_Log.HeaderId && q.RevNo == (objFXR001_Log.RevNo - 1))).FirstOrDefault().Id : null);
                ViewBag.RevNext = (db.FXR001_Log.Any(q => (q.HeaderId == objFXR001_Log.HeaderId && q.RevNo == (objFXR001_Log.RevNo + 1))) ? urlPrefix + db.FXR001_Log.Where(q => (q.HeaderId == objFXR001_Log.HeaderId && q.RevNo == (objFXR001_Log.RevNo + 1))).FirstOrDefault().Id : null);
            }
            return PartialView("_LoadFixtureHistoryFormPartial", objFXR001_Log);
        }

        [HttpPost]
        public ActionResult LoadFixtureListHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                int RefId = Convert.ToInt32(param.CTQHeaderId);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                strWhere = "1=1 and RefId In (" + RefId + ")";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (Project like '%" + param.sSearch +
                               "%' or Document like '%" + param.sSearch +
                               "%' or RevNo like '%" + param.sSearch +
                               "%' or FixtureName like '%" + param.sSearch +
                               "%' or DescriptionofItem like '%" + param.sSearch +
                               "%' or Category like '%" + param.sSearch +
                               "%' or Material like '%" + param.sSearch +
                               "%' or MaterialType like '%" + param.sSearch +
                               "%' or StructuralType like '%" + param.sSearch +
                               "%' or PipeNormalBore like '%" + param.sSearch + "%')";
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_FETCH_FIXTURE_HEADER_LINES_HISTORY
                    (
                        0,
                        StartIndex,
                        EndIndex,
                        strSortOrder,
                        strWhere
                    ).ToList();

                var data = (from fx in lstResult
                            select new[]
                            {
                                "",
                                Convert.ToString(fx.ROW_NO),
                                fx.ParentId == 0 ? "<nobr><img onclick='ExpandCollapsChild("+ fx.LineId +", this)' src='"+WebsiteURL+"/Images/details_close.png' />"+Convert.ToString(fx.GROUP_NO) + "</nobr>" : "<span class='"+ fx.ParentId +" child' ></span>",
                                fx.ParentId == 0 ? Convert.ToString(fx.FixtureName) : "",
                                Convert.ToString(fx.QtyofFixture),
                                Convert.ToString(fx.DescriptionofItem),
                                Convert.ToString(fx.Category),
                                Convert.ToString(fx.Material),
                                Convert.ToString(fx.MaterialType),
                                Convert.ToString(fx.LengthOD),
                                Convert.ToString(fx.WidthOD),
                                Convert.ToString(fx.Thickness),
                                Convert.ToString(fx.Qty),
                                Convert.ToString(fx.Wt),
                                Convert.ToString(fx.Area),
                                Convert.ToString(fx.Unit),
                                Convert.ToString(fx.ReUse ? "Yes" : "No"),
                                Convert.ToString(fx.ReqArea),
                                Convert.ToString(fx.Unit2),
                                Convert.ToString(fx.ReqWt),
                                fx.FixRequiredDate.HasValue ? Convert.ToString(fx.FixRequiredDate.Value.ToShortDateString()) : "",
                                fx.MaterialReqDate.HasValue ? Convert.ToString(fx.MaterialReqDate.Value.ToShortDateString()) : "",
                                Convert.ToString(fx.LineId),
                                Convert.ToString(fx.HeaderId),
                                Convert.ToString(fx.ParentId),
                            }).ToList();
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }
        #endregion

        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult CreateFixture(int HeaderID = 0)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("FXR001");
            FXR001 objFXR001 = db.FXR001.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
            if (HeaderID > 0)
            {
                string currentUser = objClsLoginInfo.UserName;

                if (objFXR001 == null)
                {
                    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                }
                var PlanningDinID = db.PDN002.Where(x => x.RefId == HeaderID && x.DocumentNo == objFXR001.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;

                ViewBag.IsReviseBtnEnabled = Manager.IsReviseEnabled(PlanningDinID, objFXR001.HeaderId, objFXR001.Document);
                ViewBag.DocMessage = clsImplementationMessage.CommonMessages.DocMessage.ToString();
            }

            ViewBag.HeaderID = HeaderID;


            return View();
        }

        [HttpPost]
        public ActionResult LoadFixtureCreateFormPartial(int HeaderID = 0)
        {
            FXR001 objFXR001 = db.FXR001.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
            if (objFXR001 != null)
            {
                var projectDetails = db.COM001.Where(x => x.t_cprj == objFXR001.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                ViewBag.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string ApproverName = db.COM003.Where(x => x.t_psno == objFXR001.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                if (!string.IsNullOrEmpty(ApproverName))
                {
                    ViewBag.ApproverName = objFXR001.ApprovedBy + " - " + ApproverName;
                }
                string customerName = db.COM006.Where(x => x.t_bpid == objFXR001.Customer).Select(x => x.t_nama).FirstOrDefault();
                ViewBag.customerName = objFXR001.Customer + " - " + customerName;
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                var objFXR002 = db.FXR002.Where(x => x.HeaderId == HeaderID).ToList();
                ViewBag.CDD = objFXR001.CDD.Value.ToShortDateString();
                decimal RM = Math.Truncate((objFXR002.Select(x => x.Wt ?? 0).Sum() / 1000) * 1000) / 1000;
                decimal EM = Math.Truncate((objFXR002.Select(x => x.ReqWt ?? 0).Sum() / 1000) * 1000) / 1000;
                decimal RUM = Math.Truncate((RM - EM) * 1000) / 1000;
                decimal ROP = RM != 0 ? Math.Truncate((RUM / RM) * 1000) / 1000 : 0;

                ViewBag.ReqMaterial = RM;
                ViewBag.EstimatedMaterial = EM;
                ViewBag.ReuseMaterial = RUM;
                ViewBag.ReusePercentage = ROP;

                var PlanningDinID = db.PDN002.Where(x => x.RefId == HeaderID && x.DocumentNo == objFXR001.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;

                ViewBag.IsReviseBtnEnabled = Manager.IsReviseEnabled(PlanningDinID, objFXR001.HeaderId, objFXR001.Document);
                ViewBag.DocMessage = clsImplementationMessage.CommonMessages.DocMessage.ToString();
            }
            else
            {
                objFXR001 = new FXR001();
                objFXR001.Status = clsImplementationEnum.FixtureStatus.Draft.GetStringValue();
                objFXR001.RevNo = 0;
            }
            ViewBag.Products = clsImplementationEnum.getProductCategoryFXR().ToArray();

            return PartialView("_LoadFixtureCreateFormPartial", objFXR001);
        }

        [HttpPost]
        public ActionResult GetCustomerProjectWise(string projectCode)
        {
            ApproverModel objApproverModel = Manager.GetCustomerProjectWise(projectCode);
            string strCdd = Manager.GetContractWiseCdd(projectCode);
            if (!string.IsNullOrWhiteSpace(strCdd))
            {
                DateTime dtCdd = Convert.ToDateTime(strCdd);
                objApproverModel.Cdd = dtCdd.Date.ToShortDateString();
            }
            return Json(objApproverModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveHeader(FXR001 fxr001, string strHeaderId, string strLineId, bool hasAttachments, Dictionary<string, string> Attach)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                int NewHeaderId = 0;
                FXR001 objFXR001 = db.FXR001.Where(x => x.Project == fxr001.Project).FirstOrDefault();
                if (objFXR001 != null)
                {
                    objFXR001.BudgetedMaterial = Math.Round(Convert.ToDecimal(fxr001.BudgetedMaterial), 2);
                    objFXR001.Product = fxr001.Product;
                    objFXR001.ReviseRemark = fxr001.ReviseRemark;
                    objFXR001.ApprovedBy = fxr001.ApprovedBy;
                    objFXR001.EditedBy = fxr001.EditedBy;
                    objFXR001.EditedOn = fxr001.EditedOn;
                    if (objFXR001.Status == clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue())
                    {
                        objFXR001.RevNo = Convert.ToInt32(objFXR001.RevNo) + 1;
                        objFXR001.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                        objFXR001.ReturnRemark = null;
                        objFXR001.ApprovedOn = null;
                        Manager.UpdatePDN002(objFXR001.HeaderId, objFXR001.Status, objFXR001.RevNo, objFXR001.Project, objFXR001.Document);
                    }
                    db.SaveChanges();
                    Manager.UpdatePDN002(objFXR001.HeaderId, objFXR001.Status, objFXR001.RevNo, objFXR001.Project, objFXR001.Document);
                    List<FXR002> objFXR002 = db.FXR002.Where(x => x.HeaderId == objFXR001.HeaderId && x.Project == objFXR001.Project).ToList();

                    if (objFXR002.Count == 0)
                    {
                        InsertLinesEntry(objFXR001);
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PlanMessages.Insert.ToString();
                    objResponseMsg.Status = objFXR001.Status;
                    objResponseMsg.Revision = objFXR001.RevNo;
                    objResponseMsg.HeaderID = objFXR001.HeaderId;
                }
                else
                {
                    FXR001 newObjFXR001 = db.FXR001.Add(new FXR001
                    {
                        Project = fxr001.Project,
                        Customer = fxr001.Customer,
                        Document = fxr001.Document,
                        Status = clsImplementationEnum.FixtureStatus.Draft.GetStringValue(),
                        CDD = Helper.ToNullIfTooEarlyForDb(fxr001.CDD),
                        RevNo = 0,
                        Product = fxr001.Product,
                        ApprovedBy = fxr001.ApprovedBy,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now,
                    });
                    db.SaveChanges();
                    Manager.UpdatePDN002(newObjFXR001.HeaderId, newObjFXR001.Status, newObjFXR001.RevNo, newObjFXR001.Project, newObjFXR001.Document);
                    NewHeaderId = newObjFXR001.HeaderId;
                    InsertLinesEntry(newObjFXR001);

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PlanMessages.Insert.ToString();
                    objResponseMsg.Status = newObjFXR001.Status;
                    objResponseMsg.Revision = newObjFXR001.RevNo;
                    objResponseMsg.HeaderID = NewHeaderId;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult LoadFixtureListData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                strWhere = "1=1 and HeaderId In (" + HeaderId + ")";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (Project like '%" + param.sSearch +
                               "%' or Document like '%" + param.sSearch +
                               "%' or RevNo like '%" + param.sSearch +
                               "%' or FixtureName like '%" + param.sSearch +
                               "%' or DescriptionofItem like '%" + param.sSearch +
                               "%' or Category like '%" + param.sSearch +
                               "%' or Material like '%" + param.sSearch +
                               "%' or MaterialType like '%" + param.sSearch +
                               "%' or StructuralType like '%" + param.sSearch +
                               "%' or PipeNormalBore like '%" + param.sSearch + "%')";
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_FETCH_FIXTURE_HEADER_LINES
                    (
                        HeaderId,
                        StartIndex,
                        EndIndex,
                        strSortOrder,
                        strWhere
                    ).ToList();

                var data = (from fx in lstResult
                            select new[]
                            {
                                "",
                                Convert.ToString(fx.ROW_NO),
                                fx.ParentId == 0 ? "<nobr><img onclick='ExpandCollapsChild("+ fx.LineId +", this)' src='"+WebsiteURL+"/Images/details_close.png' />"+Convert.ToString(fx.GROUP_NO) + "</nobr>" : "<span class='"+ fx.ParentId +" child' ></span>",
                                fx.ParentId == 0 ? Convert.ToString(fx.FixtureName) : "",
                                Convert.ToString(fx.QtyofFixture),
                                Convert.ToString(fx.DescriptionofItem),
                                Convert.ToString(fx.Category),
                                Convert.ToString(fx.Material),
                                Convert.ToString(fx.MaterialType),
                                Convert.ToString(fx.LengthOD),
                                Convert.ToString(fx.WidthOD),
                                Convert.ToString(fx.Thickness),
                                Convert.ToString(fx.Qty),
                                Convert.ToString(fx.Wt),
                                Convert.ToString(fx.Area),
                                Convert.ToString(fx.Unit),
                                Convert.ToString(fx.ReUse ? "Yes" : "No"),
                                Convert.ToString(fx.ReqArea),
                                Convert.ToString(fx.Unit2),
                                Convert.ToString(fx.ReqWt),
                                fx.FixRequiredDate.HasValue ? Convert.ToString(fx.FixRequiredDate.Value.ToShortDateString()) : "",
                                fx.MaterialReqDate.HasValue ? Convert.ToString(fx.MaterialReqDate.Value.ToShortDateString()) : "",
                                Convert.ToString(fx.LineId),
                                Convert.ToString(fx.HeaderId),
                                Convert.ToString(fx.ParentId),
                                Convert.ToString(fx.FixtureName),
                                Convert.ToString(fx.IsManual),
                                fx.ParentId == 0 ? FindChildCount(fx.FixtureName, fx.HeaderId) > 1 ? "true" : "false" : "false",
                            }).ToList();
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult GetFXRLinesSubCategroyPartial(int lineId, int headerId, string action)
        {
            FXR002 objFXR002 = null;
            if (lineId > 0 && headerId > 0)
            {
                ViewBag.StrAction = action;
                if (action == "add")
                {
                    objFXR002 = new FXR002();
                    FXR002 masterLine = db.FXR002.Where(x => x.LineId == lineId && x.HeaderId == headerId).FirstOrDefault();
                    if (masterLine != null)
                    {
                        //var QtyFXR = getFXRData(headerId.ToString(), lineId.ToString());
                        //if (QtyFXR.QtyofFixture != "")
                        //{ objFXR002.QtyofFixture = Int32.Parse(QtyFXR.QtyofFixture); }
                        //if (QtyFXR.FixRequiredDate.HasValue)
                        //{ objFXR002.FixRequiredDate = QtyFXR.FixRequiredDate; }
                        //if (QtyFXR.MaterialReqDate.HasValue)
                        //{ objFXR002.MaterialReqDate = QtyFXR.MaterialReqDate; }

                        objFXR002.LineId = masterLine.LineId;
                        objFXR002.HeaderId = masterLine.HeaderId;
                        objFXR002.Project = masterLine.Project;
                        objFXR002.Document = masterLine.Document;
                        objFXR002.RevNo = masterLine.RevNo;
                        objFXR002.FixtureName = masterLine.FixtureName;

                        ViewBag.RevNo = objFXR002.RevNo;
                    }
                }
                else if (action == "edit")
                {
                    objFXR002 = db.FXR002.Where(x => x.LineId == lineId && x.HeaderId == headerId).FirstOrDefault();
                    ViewBag.Category = objFXR002.Category;
                    ViewBag.Material = objFXR002.Material;
                    ViewBag.MaterialType = objFXR002.MaterialType;
                    ViewBag.Unit = objFXR002.Unit;
                    ViewBag.Unit2 = objFXR002.Unit2;
                    ViewBag.StructuralType = objFXR002.StructuralType;
                    ViewBag.PipeNormalBore = objFXR002.PipeNormalBore;
                    ViewBag.PipeSchedule = objFXR002.PipeSchedule;
                    ViewBag.RevNo = objFXR002.RevNo;
                    //string QtyFXR = getFXRData(headerId.ToString(), lineId.ToString());
                    //if (QtyFXR != "")
                    //{ objFXR002.QtyofFixture = Int32.Parse(QtyFXR); }
                }
                var QtyFXR = getFXRData(headerId.ToString(), lineId.ToString());
                if (QtyFXR.QtyofFixture != "")
                { objFXR002.QtyofFixture = Int32.Parse(QtyFXR.QtyofFixture); }
                if (QtyFXR.FixRequiredDate.HasValue)
                { objFXR002.FixRequiredDate = QtyFXR.FixRequiredDate; }
                if (QtyFXR.MaterialReqDate.HasValue)
                { objFXR002.MaterialReqDate = QtyFXR.MaterialReqDate; }

                List<string> lstFXRStatus = clsImplementationEnum.GetFixtureStatus().ToList();
                ViewBag.lstFXRStatus = lstFXRStatus.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

                List<string> lstFXRCategory = clsImplementationEnum.GetFXRCategory().ToList();
                ViewBag.lstFXRCategory = lstFXRCategory.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

                List<string> lstFXRMaterialType = clsImplementationEnum.GetFXRMaterialType().ToList();
                ViewBag.lstFXRMaterialType = lstFXRMaterialType.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

                //List<string> lstFXRUnit = clsImplementationEnum.GetFXRUnit().ToList();
                //ViewBag.lstFXRUnit = lstFXRUnit.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

                //List<string> lstFXRUnit2 = clsImplementationEnum.GetFXRUnit2().ToList();
                //ViewBag.lstFXRUnit2 = lstFXRUnit2.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

                List<string> lstFXRStructuralType = clsImplementationEnum.GetFXRStructuralType().ToList();
                ViewBag.lstFXRStructuralType = lstFXRStructuralType.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

                List<string> lstBore = db.FXR005.Select(x => x.Bore).Distinct().ToList();
                ViewBag.lstBore = lstBore.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

                List<string> lstSchedule = db.FXR005.Select(x => x.Schedule).Distinct().ToList();
                ViewBag.lstSchedule = lstSchedule.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();
            }

            return PartialView("_FXRLinesSubCategoryPartial", objFXR002);
        }

        [HttpGet]
        public ActionResult GetMaterialFromCategory(int Id)
        {
            List<BULocWiseCategoryModel> lstMaterial = new List<BULocWiseCategoryModel>();
            if (Id == 1)
            {
                var data = from q in db.FXR006.ToList()
                           select new { name = q.Type + "-" + q.Size };

                lstMaterial = data.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.name, CatID = x.name }).ToList();
            }
            else if (Id == 2)
            {
                var data = from q in db.FXR005.ToList()
                           select new { name = q.Bore + "-" + q.Schedule };

                lstMaterial = data.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.name, CatID = x.name }).ToList();
            }
            return Json(lstMaterial, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveSubCategory(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsgWithStatus = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (fc != null)
                {
                    int headerId = Convert.ToInt32(fc["HeaderId"]);
                    string project = fc["Project"];

                    FXR001 objFXR001 = db.FXR001.Where(x => x.HeaderId == headerId && x.Project == project).FirstOrDefault();
                    if (objFXR001.Status == clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue())
                    {
                        objFXR001.RevNo = Convert.ToInt32(objFXR001.RevNo) + 1;
                        objFXR001.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                        objFXR001.ReturnRemark = null;
                        objFXR001.ApprovedOn = null;
                        Manager.UpdatePDN002(objFXR001.HeaderId, objFXR001.Status, objFXR001.RevNo, objFXR001.Project, objFXR001.Document);
                    }
                    db.SaveChanges();
                    FXR002 objFXR002 = null;
                    if (fc["strAction"] == "add")
                    {
                        string fixturename = fc["FixtureName"];
                        int parentFXRId = db.FXR002.Where(x => x.HeaderId == headerId && x.FixtureName == fixturename && x.ParentId == 0).Select(x => x.LineId).FirstOrDefault();
                        objFXR002 = new FXR002();
                        objFXR002.ParentId = parentFXRId;
                    }
                    else
                    {
                        int lineId = Convert.ToInt32(fc["LineId"]);
                        objFXR002 = db.FXR002.Where(x => x.LineId == lineId && x.HeaderId == headerId).FirstOrDefault();
                    }
                    var str = fc["RevNo"].ToString();
                    int revNo = Convert.ToInt32(str.Substring(1, str.Length - 1));

                    objFXR002.LineId = Convert.ToInt32(fc["LineId"]);
                    objFXR002.HeaderId = Convert.ToInt32(fc["HeaderId"]);
                    objFXR002.Project = fc["Project"];
                    objFXR002.Document = fc["Document"];
                    objFXR002.RevNo = revNo;
                    objFXR002.FixtureName = fc["FixtureName"];
                    objFXR002.QtyofFixture = Convert.ToInt32(fc["QtyofFixture"]);
                    objFXR002.DescriptionofItem = fc["DescriptionofItem"];
                    objFXR002.Category = fc["Category"];
                    objFXR002.Material = fc["Material"];
                    objFXR002.MaterialType = fc["MaterialType"];
                    objFXR002.LengthOD = fc["LengthOD"] == "" ? Decimal.Parse("0") : Convert.ToDecimal(fc["LengthOD"], CultureInfo.InvariantCulture);
                    objFXR002.WidthOD = fc["WidthOD"] == "" ? Decimal.Parse("0") : Convert.ToDecimal(fc["WidthOD"], CultureInfo.InvariantCulture);
                    objFXR002.Thickness = fc["Thickness"] == "" ? Decimal.Parse("0") : Convert.ToDecimal(fc["Thickness"], CultureInfo.InvariantCulture);
                    objFXR002.Qty = fc["Qty"] == "" ? Decimal.Parse("0") : Convert.ToDecimal(fc["Qty"], CultureInfo.InvariantCulture);
                    objFXR002.Wt = fc["Wt"] == "" ? Decimal.Parse("0") : Convert.ToDecimal(fc["Wt"], CultureInfo.InvariantCulture);
                    objFXR002.Area = fc["Area"] == "" ? Decimal.Parse("0") : Convert.ToDecimal(fc["Area"], CultureInfo.InvariantCulture);
                    objFXR002.Unit = fc["Unit"];
                    objFXR002.ReqWt = fc["ReqWt"] == "" ? Decimal.Parse("0") : Convert.ToDecimal(fc["ReqWt"], CultureInfo.InvariantCulture);
                    objFXR002.Unit2 = fc["Unit2"];
                    string FxrReqDt = fc["FixRequiredDate"] != null ? fc["FixRequiredDate"].ToString() : "";
                    objFXR002.FixRequiredDate = DateTime.ParseExact(FxrReqDt, @"d/M/yyyy", CultureInfo.InvariantCulture);
                    string FxrMatDt = fc["MaterialReqDate"] != null ? fc["MaterialReqDate"].ToString() : "";
                    objFXR002.MaterialReqDate = DateTime.ParseExact(FxrMatDt, @"d/M/yyyy", CultureInfo.InvariantCulture);
                    objFXR002.ReqArea = (fc["ReqArea"] == "" || fc["ReqArea"] == null) ? Decimal.Parse("0") : Convert.ToDecimal(fc["ReqArea"], CultureInfo.InvariantCulture);
                    //if (fc["FixRequiredDate"] == "")
                    //{
                    //    objFXR002.FixRequiredDate = null;
                    //}
                    //else
                    //{
                    //    objFXR002.FixRequiredDate = DateTime.Parse(fc["FixRequiredDate"], CultureInfo.InvariantCulture);
                    //}
                    //if (fc["MaterialReqDate"] == "")
                    //{
                    //    objFXR002.MaterialReqDate = null;
                    //}
                    //else
                    //{
                    //    objFXR002.MaterialReqDate = DateTime.Parse(fc["MaterialReqDate"], CultureInfo.InvariantCulture);
                    //}
                    objFXR002.StructuralType = fc["StructuralType"] == "" ? null : fc["StructuralType"];
                    objFXR002.Size = Convert.ToString(fc["Size"]);
                    objFXR002.PipeNormalBore = fc["PipeNormalBore"] == "" ? null : fc["PipeNormalBore"];
                    objFXR002.PipeSchedule = fc["PipeSchedule"] == "" ? null : fc["PipeSchedule"];
                    var tempReUse = Convert.ToString(fc["txtReUse"]);
                    if (tempReUse == "True")
                    {
                        objFXR002.ReUse = true;
                    }
                    else
                    {
                        objFXR002.ReUse = false;
                    }


                    if (fc["strAction"] == "add")
                    {
                        objFXR002.CreatedBy = objClsLoginInfo.UserName;
                        objFXR002.CreatedOn = DateTime.Now;
                        db.FXR002.Add(objFXR002);
                        objResponseMsgWithStatus.Value = "Fixture's saved successfully";
                    }
                    else
                    {
                        int linesId = Convert.ToInt32(fc["LineId"]);
                        string name = fc["FixtureName"];
                        string proj = fc["Project"];
                        List<FXR002> obj1FXR002 = db.FXR002.Where(x => x.FixtureName == name && x.Project == proj).ToList();
                        foreach (var obj in obj1FXR002)
                        {

                            obj.QtyofFixture = Convert.ToInt32(fc["QtyofFixture"]);
                            db.Entry(obj).State = System.Data.Entity.EntityState.Modified;

                        }

                        objFXR002.EditedBy = objClsLoginInfo.UserName;
                        objFXR002.EditedOn = DateTime.Now;
                        objResponseMsgWithStatus.Value = "Fixture's update successfully";
                    }



                    objResponseMsgWithStatus.HeaderId = Convert.ToInt32(fc["HeaderId"]);
                    db.SaveChanges();

                    objResponseMsgWithStatus.Key = true;
                    objResponseMsgWithStatus.RevNo = Convert.ToString(objFXR001.RevNo);
                    objResponseMsgWithStatus.HeaderStatus = objFXR001.Status;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsgWithStatus.Key = false;
                objResponseMsgWithStatus.Value = ex.Message;
            }
            return Json(objResponseMsgWithStatus);
        }

        [HttpPost]
        public ActionResult SendForApproval(int HeaderId, string Approver)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrLineStatus = { clsImplementationEnum.FixtureStatus.Draft.GetStringValue(), clsImplementationEnum.FixtureStatus.Returned.GetStringValue() };
                List<FXR002> lstFXR002 = db.FXR002.Where(x => x.HeaderId == HeaderId).ToList();
                if (lstFXR002 != null && lstFXR002.Count > 0)
                {
                    foreach (var objData in lstFXR002)
                    {
                        FXR002 objFXR002 = lstFXR002.Where(x => x.LineId == objData.LineId).FirstOrDefault();
                        //objFXR002.Status = clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue();
                        objFXR002.EditedBy = objClsLoginInfo.UserName;
                        objFXR002.EditedOn = DateTime.Now;
                        //objFXR002.SubmittedBy = objClsLoginInfo.UserName;
                        //objFXR002.SubmittedOn = DateTime.Now;
                    }
                }
                FXR001 objFXR001 = db.FXR001.Where(x => x.HeaderId == HeaderId && arrLineStatus.Contains(x.Status)).FirstOrDefault();
                if (objFXR001 != null)
                {
                    objFXR001.Status = clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue();
                    objFXR001.ApprovedBy = Approver;
                    objFXR001.SubmittedBy = objClsLoginInfo.UserName;
                    objFXR001.SubmittedOn = DateTime.Now;
                    //objFXR001.SubmittedBy = objClsLoginInfo.UserName;
                    //objFXR001.SubmittedOn = DateTime.Now;
                    db.SaveChanges();
                    Manager.UpdatePDN002(objFXR001.HeaderId, objFXR001.Status, objFXR001.RevNo, objFXR001.Project, objFXR001.Document);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details successfully sent for approval.";

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PLNG1.GetStringValue(), objFXR001.Project, "", "", Manager.GetPDINDocumentNotificationMsg(objFXR001.Project, clsImplementationEnum.PlanList.Fixture.GetStringValue(), objFXR001.RevNo.Value.ToString(), objFXR001.Status), clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(), Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Fixture.GetStringValue(), objFXR001.HeaderId.ToString(), true), objFXR001.ApprovedBy);
                    #endregion
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details not available for approval.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetProjectSummary(int HeaderId)
        {
            //List<FXR002> objFXR002 = db.FXR002.Where(x => x.HeaderId == HeaderId && x.Category != null).OrderBy(x => x.Category).ToList();
            //List<FXR002> model = null;
            //if (objFXR002.Count > 0)
            //{
            //    model = (from c in objFXR002
            //             group c by new
            //             {
            //                 c.Category,
            //                 c.Material,
            //                 c.MaterialType,
            //                 c.Thickness,
            //                 c.LengthOD,
            //                 c.WidthOD,
            //                 HeaderId
            //             } into gcs
            //             select new FXR002()
            //             {
            //                 Category = gcs.Key.Category,
            //                 Material = gcs.Key.Material,
            //                 MaterialType = gcs.Key.MaterialType,
            //                 Thickness = gcs.Key.Thickness,
            //                 LengthOD = gcs.Key.LengthOD,
            //                 WidthOD = gcs.Key.WidthOD,
            //                 Wt = gcs.Sum(x => x.Wt),
            //                 Qty = gcs.Sum(x => x.Qty),
            //                 HeaderId = HeaderId
            //             }).ToList();

            //    ViewBag.TotalWeight = objFXR002.Sum(x => x.Wt);
            //}

            var list = db.SP_FETCH_FIXTURES_SUMMARY_SHEET(HeaderId).ToList();
            if (list.Count > 0)
            {
                ViewBag.TotalWeight = list.Sum(x => x.Weight);
            }
            ViewBag.HeaderId = HeaderId;
            return PartialView("_ProjectSummary", list);
        }

        [HttpPost]
        public bool checkProjectExist(string projectcode)
        {
            bool Flag = false;
            if (!string.IsNullOrWhiteSpace(projectcode))
            {
                FXR001 objPLN003 = db.FXR001.Where(x => x.Project == projectcode).FirstOrDefault();
                if (objPLN003 != null)
                {
                    Flag = true;
                }
            }
            return Flag;
        }

        [HttpPost]
        public JsonResult checkNoOfFixture(string fixtureName, int lineId, int headerId, string action, string project)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsgWithStatus = new clsHelper.ResponseMsgWithStatus();

            int count = db.FXR002.Where(x => x.FixtureName == fixtureName && x.Project == project).Count();
            if (count >= 10)
            {
                objResponseMsgWithStatus.Key = false;
                objResponseMsgWithStatus.Value = "You can add maximum 10 fixture's";
            }
            else
            {
                objResponseMsgWithStatus.Key = true;
            }
            var data = new { status = objResponseMsgWithStatus, lineId = lineId, headerId = headerId, action = action };
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteFixture(string fixtureName, int lineId, string project)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsgWithStatus = new clsHelper.ResponseMsgWithStatus();

            int count = db.FXR002.Where(x => x.FixtureName == fixtureName && x.Project == project).Count();

            if (count <= 1)
            {
                FXR002 objFXR002 = db.FXR002.Where(x => x.LineId == lineId).FirstOrDefault();
                if (objFXR002 != null)
                {
                    objResponseMsgWithStatus.HeaderId = objFXR002.HeaderId;
                    db.FXR002.Remove(objFXR002);
                    db.SaveChanges();
                    objResponseMsgWithStatus.Key = true;
                    objResponseMsgWithStatus.Value = "Fixture name deleted successfully.";
                }
            }
            else
            {
                FXR002 objFXR002 = db.FXR002.Where(x => x.LineId == lineId).FirstOrDefault();
                if (objFXR002.ParentId == 0)
                {
                    objResponseMsgWithStatus.Key = false;
                    objResponseMsgWithStatus.Value = "You can not delete parent record...!";
                }
                else
                {
                    if (objFXR002 != null)
                    {
                        objResponseMsgWithStatus.HeaderId = objFXR002.HeaderId;
                        db.FXR002.Remove(objFXR002);
                        db.SaveChanges();
                        objResponseMsgWithStatus.Key = true;
                        objResponseMsgWithStatus.Value = "Fixture name deleted successfully.";
                    }
                }
            }
            return Json(objResponseMsgWithStatus, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetDensityValue(string name)
        {
            var density = db.FXR003.Where(x => x.MaterialType == name).FirstOrDefault().Density;
            return Json(density, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetWeightValue(string material, string type)
        {
            var mType = material.Split('-')[0];
            var mSize = material.Split('-')[1];
            decimal weight = 0;
            if (type.ToLower() == "structural")
            {
                weight = Convert.ToDecimal(db.FXR006.Where(x => x.Type == mType && x.Size == mSize).FirstOrDefault().Weight);
            }
            else
            {
                weight = Convert.ToDecimal(db.FXR005.Where(x => x.Bore == mType && x.Schedule == mSize).FirstOrDefault().Weight);
            }

            return Json(weight, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public clsHelper.ResponseMsgWithStatus getFXRData(string headerID, string lineID)
        {
            clsHelper.ResponseMsgWithStatus obj = new clsHelper.ResponseMsgWithStatus();
            int LineID = Int32.Parse(lineID);
            int HeaderID = Int32.Parse(headerID);
            obj.Key = false;
            FXR002 objFXR002 = db.FXR002.Where(x => x.LineId == LineID && x.HeaderId == HeaderID).FirstOrDefault();
            if (objFXR002 != null)
            {
                string fixtureName = objFXR002.FixtureName;
                string project = objFXR002.Project;
                FXR002 obj1FXR002 = db.FXR002.Where(x => x.FixtureName == fixtureName && x.Project == project && x.QtyofFixture != null).FirstOrDefault();
                if (obj1FXR002 != null)
                {
                    obj.Key = true;
                    obj.QtyofFixture = obj1FXR002.QtyofFixture.ToString();
                    obj.FixRequiredDate = obj1FXR002.FixRequiredDate;
                    obj.MaterialReqDate = obj1FXR002.MaterialReqDate;
                }
                else
                {
                    obj.QtyofFixture = "";
                }
            }

            return obj;
        }

        [HttpPost]
        public ActionResult RetractStatus(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                FXR001 objFXR001 = db.FXR001.Where(u => u.HeaderId == HeaderId).FirstOrDefault();
                if (objFXR001 != null)
                {
                    objFXR001.Status = clsImplementationEnum.PLCStatus.DRAFT.GetStringValue();
                    objFXR001.SubmittedOn = null;
                    objFXR001.SubmittedBy = null;
                    objFXR001.EditedBy = objClsLoginInfo.UserName;
                    objFXR001.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Retract.ToString();
                    Manager.UpdatePDN002(objFXR001.HeaderId, objFXR001.Status, objFXR001.RevNo, objFXR001.Project, objFXR001.Document);
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReviseHeader(int strHeaderId, string strRemarks)
        {
            CustomResponceMsg objResponseMsg = new CustomResponceMsg();
            try
            {
                FXR001 objFXR001 = db.FXR001.Where(u => u.HeaderId == strHeaderId).SingleOrDefault();
                if (objFXR001 != null)
                {
                    objFXR001.RevNo = Convert.ToInt32(objFXR001.RevNo) + 1;
                    objFXR001.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                    objFXR001.ReviseRemark = strRemarks;
                    objFXR001.EditedBy = objClsLoginInfo.UserName;
                    objFXR001.EditedOn = DateTime.Now;
                    objFXR001.ReturnRemark = null;
                    objFXR001.ApprovedOn = null;
                    objFXR001.SubmittedBy = null;
                    objFXR001.SubmittedOn = null;
                    db.SaveChanges();
                    List<FXR002> lstFXR002 = db.FXR002.Where(x => x.HeaderId == objFXR001.HeaderId).ToList();
                    if (lstFXR002 != null && lstFXR002.Count > 0)
                    {
                        lstFXR002.ForEach(x =>
                        {
                            x.RevNo = objFXR001.RevNo;
                            x.EditedBy = objClsLoginInfo.UserName;
                            x.EditedOn = DateTime.Now;
                        });
                    }
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderID = objFXR001.HeaderId;
                    objResponseMsg.Status = objFXR001.Status;
                    objResponseMsg.rev = objFXR001.RevNo != null ? Convert.ToInt32(objFXR001.RevNo) : 0;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revision;
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddNewFixture(int strHeaderId, string fixtureName)
        {
            CustomResponceMsg objResponseMsg = new CustomResponceMsg();
            try
            {
                bool result = db.FXR002.Where(x => x.HeaderId == strHeaderId && x.FixtureName.ToLower().Trim() == fixtureName.ToLower().Trim()).Any();

                if (!result)
                {
                    FXR001 objFXR001 = db.FXR001.Where(x => x.HeaderId == strHeaderId).FirstOrDefault();
                    db.FXR002.Add(new FXR002
                    {
                        HeaderId = objFXR001.HeaderId,
                        Project = objFXR001.Project,
                        Document = objFXR001.Document,
                        RevNo = objFXR001.RevNo,
                        FixtureName = fixtureName,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now,
                        ParentId = 0,
                        IsManual = true
                    });

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        //#region Attachments
        [SessionExpireFilter]
        public ActionResult FXRAttachmentPartial(int? id, string revNo, string status = "", string ForApprover = "")
        {
            ViewBag.Status = status;
            ViewBag.RevNo = revNo;
            ViewBag.ForApprover = ForApprover;
            ViewBag.LineId = id;
            return PartialView("_FXRAttachmentPartial");
        }

        [HttpPost]
        public ActionResult SaveFXRAttachment(string id, string revNo, bool hasAttachments, Dictionary<string, string> Attach)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var folderPath = "FXR002/" + revNo + "/" + id;
                Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                objResponseMsg.Key = true;
                objResponseMsg.Value = "File Uploaded Successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #region Export Excell
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_FETCH_FIXTURES_HEADER(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      Document = li.Document,
                                      Customer = li.Customer,
                                      Product = li.Product,
                                      RevNo = "R" + li.RevNo,
                                      Status = li.Status,
                                      CDD = li.CDD,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn.HasValue ? Convert.ToString(li.CreatedOn) : "",
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn.HasValue ? Convert.ToString(li.EditedOn) : "",
                                      SubmittedBy = li.SubmittedBy,
                                      SubmittedOn = li.SubmittedOn.HasValue ? Convert.ToString(li.SubmittedOn) : "",
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn.HasValue ? Convert.ToString(li.ApprovedOn) : "",
                                      ReturnRemark = li.ReturnRemark,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_FETCH_FIXTURE_HEADER_LINES(0, 1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from li in lst
                                  select new
                                  {
                                      FixtureName = li.FixtureName,
                                      QtyofFixture = li.QtyofFixture,
                                      DescriptionofItem = li.DescriptionofItem,
                                      Category = li.Category,
                                      Material = li.Material,
                                      MaterialType = li.MaterialType,
                                      LengthOD = li.LengthOD,
                                      WidthOD = li.WidthOD,
                                      Thickness = li.Thickness,
                                      Qty = li.Qty,
                                      Wt = li.Wt,
                                      Area = li.Area,
                                      Unit = li.Unit,
                                      ReUse = li.ReUse ? "Yes" : "No",
                                      ReqArea = li.ReqArea,
                                      Unit2 = li.Unit2,
                                      ReqWt = li.ReqWt,
                                      FixRequiredDate = li.FixRequiredDate.HasValue ? Convert.ToString(li.FixRequiredDate.Value.ToShortDateString()) : "",
                                      MaterialReqDate = li.MaterialReqDate.HasValue ? Convert.ToString(li.MaterialReqDate.Value.ToShortDateString()) : "",
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {
                    var lst = db.SP_FETCH_FIXTURES_HEADER_HISTORY(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      Document = li.Document,
                                      Customer = li.Customer,
                                      Product = li.Product,
                                      RevNo = "R" + li.RevNo,
                                      Status = li.Status,
                                      CDD = li.CDD,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn.HasValue ? Convert.ToString(li.CreatedOn) : "",
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn.HasValue ? Convert.ToString(li.EditedOn) : "",
                                      SubmittedBy = li.SubmittedBy,
                                      SubmittedOn = li.SubmittedOn.HasValue ? Convert.ToString(li.SubmittedOn) : "",
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn.HasValue ? Convert.ToString(li.ApprovedOn) : "",
                                      ReturnRemark = li.ReturnRemark,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYLINES.GetStringValue())
                {
                    var lst = db.SP_FETCH_FIXTURE_HEADER_LINES_HISTORY(0, 1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from li in lst
                                  select new
                                  {
                                      FixtureName = li.FixtureName,
                                      QtyofFixture = li.QtyofFixture,
                                      DescriptionofItem = li.DescriptionofItem,
                                      Category = li.Category,
                                      Material = li.Material,
                                      MaterialType = li.MaterialType,
                                      LengthOD = li.LengthOD,
                                      WidthOD = li.WidthOD,
                                      Thickness = li.Thickness,
                                      Qty = li.Qty,
                                      Wt = li.Wt,
                                      Area = li.Area,
                                      Unit = li.Unit,
                                      ReUse = li.ReUse ? "Yes" : "No",
                                      ReqArea = li.ReqArea,
                                      Unit2 = li.Unit2,
                                      ReqWt = li.ReqWt,
                                      FixRequiredDate = li.FixRequiredDate.HasValue ? Convert.ToString(li.FixRequiredDate.Value.ToShortDateString()) : "",
                                      MaterialReqDate = li.MaterialReqDate.HasValue ? Convert.ToString(li.MaterialReqDate.Value.ToShortDateString()) : "",
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}