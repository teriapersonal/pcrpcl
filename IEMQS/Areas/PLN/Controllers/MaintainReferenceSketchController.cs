﻿using IEMQS.Areas.MSW.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQS.Areas.Utility.Models;
using System.Globalization;
namespace IEMQS.Areas.PLN.Controllers
{
    public class MaintainReferenceSketchController : clsBase
    {
        //modified by nikita on 29/08/2017
        //task assigned by Satish Pawar
        //added method updatepdn002
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadReferenceSketchListDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_ReferenceSketchListDataPartial");
        }
        public ActionResult ShowHistoryTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();

            model.TimelineTitle = "Reference Sketch TimeLine";
            model.Title = "PDinDoc";
            if (HeaderId > 0)
            {

                PLN003_Log objPLN003_Log = db.PLN003_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN003_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN003_Log.CreatedBy) : null;
                model.CreatedOn = objPLN003_Log.CreatedOn;
                model.EditedBy = objPLN003_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN003_Log.EditedBy) : null;
                model.EditedOn = objPLN003_Log.EditedOn;
                model.SubmittedBy = objPLN003_Log.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objPLN003_Log.SubmittedBy) : null;
                model.SubmittedOn = objPLN003_Log.SubmittedOn;

                model.ApprovedBy = objPLN003_Log.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objPLN003_Log.ApprovedBy) : null;
                model.ApprovedOn = objPLN003_Log.ApprovedOn;

            }
            else
            {

                PLN003_Log objPLN003_Log = db.PLN003_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN003_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN003_Log.CreatedBy) : null;
                model.CreatedOn = objPLN003_Log.CreatedOn;
                model.EditedBy = objPLN003_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN003_Log.EditedBy) : null;
                model.EditedOn = objPLN003_Log.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.Title = "PDinDoc";
            model.TimelineTitle = "Reference Sketch Timeline";

            if (HeaderId > 0)
            {
                PLN003 objPLN003 = db.PLN003.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN003.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN003.CreatedBy) : null;
                model.CreatedOn = objPLN003.CreatedOn;
                model.EditedBy = objPLN003.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN003.EditedBy) : null;
                model.EditedOn = objPLN003.EditedOn;
                model.SubmittedBy = objPLN003.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objPLN003.SubmittedBy) : null;
                model.SubmittedOn = objPLN003.SubmittedOn;
                model.ApprovedBy = objPLN003.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objPLN003.ApprovedBy) : null;
                model.ApprovedOn = objPLN003.ApprovedOn;

            }
            else
            {
                PLN003 objPLN003 = db.PLN003.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN003.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN003.CreatedBy) : null;
                model.CreatedOn = objPLN003.CreatedOn;
                model.EditedBy = objPLN003.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN003.EditedBy) : null;
                model.EditedOn = objPLN003.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        [HttpPost]
        public JsonResult LoadRererenceSketchHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.PlanStatus.DRAFT.GetStringValue() + "','" + clsImplementationEnum.PlanStatus.Returned.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "1=1";
                }
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;


                //strWhere += " and pln003.CreatedBy in('" + objClsLoginInfo.UserName + "')";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "pln003.Project+' - '+com1.t_dsca",
                                                "Document",
                                                "pln003.Customer+' - '+com6.t_nama",
                                                "Status",
                                                "Product",
                                                "ProcessLicensor",
                                                "ProcessPlan",
                                                "JobNo"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }



                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_FETCH_REFERENCESKETCH_HEADERS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString(uc.Customer),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.CDD),
                               Convert.ToString(uc.ProcessPlan),
                               Convert.ToString(uc.CreatedBy),
                               uc.CreatedOn.HasValue ? Convert.ToString(uc.CreatedOn.Value.ToShortDateString()) : "",
                               Convert.ToString(uc.EditedBy),
                               uc.EditedOn.HasValue ? Convert.ToString(uc.EditedOn.Value.ToShortDateString()) : "",
                               Convert.ToString(uc.SubmittedBy),
                               Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),
                               Convert.ToString(uc.ApprovedBy),
                               uc.ApprovedOn.HasValue ? Convert.ToString(uc.ApprovedOn.Value.ToShortDateString()) : "",
                               Convert.ToString(uc.ReturnRemark),
                               "<nobr><center>"+"<a title='View' href='"+WebsiteURL+"/PLN/MaintainReferenceSketch/CreateReferenceSketch?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i style='' class='iconspace fa fa-eye'></i></a>"+
                               Helper.GenerateActionIcon(uc.HeaderId,"Delete","Delete Record","fa fa-trash-o", "DeleteDocument("+ uc.HeaderId +",'/PLN/MaintainReferenceSketch/DeleteHeader', {headerid:"+uc.HeaderId+"}, 'tblRefSketchHeader')","",  (( uc.RevNo >0 && uc.Status != clsImplementationEnum.CommonStatus.SendForApprovel.GetStringValue()) || ( uc.RevNo == 0 && uc.Status == clsImplementationEnum.CommonStatus.Approved.GetStringValue()) ) ? false:true) +
                               (uc.RevNo>0 ?"<a title=\"History\" onclick=\"history('"+uc.HeaderId+"')\"><i style='' class='iconspace fa fa-history'></i></a>":"<a title=\"History\"><i style='' class='disabledicon fa fa-history'></i></a>")+"<a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintainReferenceSketch/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')><i style='' class='iconspace fa fa-clock-o'></i></a>"
                               + "</center></nobr>",
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult DeleteHeader(int headerid)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                objResponseMsg = Manager.DeletePDINDocument(headerid, clsImplementationEnum.PlanList.Reference_Sketch);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "",int? HeaderId=0, int? latestRv=0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_FETCH_REFERENCESKETCH_HEADERS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project=li.Project,
                                      Documents=li.Document,
                                      Customer=li.Customer,
                                      Product=li.Product,
                                      ProcessLicensor=li.ProcessLicensor,
                                      RevNo=li.RevNo,
                                      Status=li.Status,
                                      CDD=li.CDD,
                                      ProcessPlan=li.ProcessPlan,
                                      CreatedBy=li.CreatedBy,
                                      CreatedOn = li.CreatedOn == null || li.CreatedOn.Value == DateTime.MinValue ? "NA" : li.CreatedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      EditedBy =li.EditedBy,
                                      EditedOn=li.EditedOn==null || li.EditedOn.Value==DateTime.MinValue ?"NA" :li.EditedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      SubmittedBy =li.SubmittedBy,
                                      SubmittedOn = li.SubmittedOn == null || li.SubmittedOn.Value == DateTime.MinValue ? "NA" : li.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ApprovedBy =li.ApprovedBy,
                                      ApproveOn = li.ApprovedOn == null || li.ApprovedOn.Value == DateTime.MinValue ? "NA" : li.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ReturnRemark=li.ReturnRemark

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_FETCH_REFERENCESKETCH_HEADER_LINE(HeaderId, latestRv, 1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      RowNo=li.ROW_NO,
                                      RelatedTo = li.RelatedTo,
                                      CheckPointDesc = li.CheckPointDesc,
                                      YesNo=li.Yes_No,
                                      Remarks=li.Remarks,
                                      PreparedBy=li.CreatedBy,
                                      CreatedOn = li.CreatedOn == null || li.CreatedOn.Value == DateTime.MinValue ? "NA" : li.CreatedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      CheckedBy =li.EditedBy,
                                      CheckedOn = li.EditedOn == null || li.EditedOn.Value == DateTime.MinValue ? "NA" : li.EditedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      DocumentNo =li.Document,
                                      
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {  //tack weld lines
                    var lst = db.SP_FETCH_REFERENCESKETCH_HEADERS_HISTORY(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      Documents = li.Document,
                                      Customer = li.Customer,
                                      Product = li.Product,
                                      ProcessLicensor = li.ProcessLicensor,
                                      RevNo = li.RevNo,
                                      Status = li.Status,
                                      CDD = li.CDD,
                                      ProcessPlan = li.ProcessPlan,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn == null || li.CreatedOn.Value == DateTime.MinValue ? "NA" : li.CreatedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn == null || li.EditedOn.Value == DateTime.MinValue ? "NA" : li.EditedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      SubmittedBy = li.SubmittedBy,
                                      SubmittedOn = li.SubmittedOn == null || li.SubmittedOn.Value == DateTime.MinValue ? "NA" : li.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ApprovedBy = li.ApprovedBy,
                                      ApproveOn = li.ApprovedOn == null || li.ApprovedOn.Value == DateTime.MinValue ? "NA" : li.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ReturnRemark = li.ReturnRemark

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYLINES.GetStringValue())
                {
                    var lst = db.SP_FETCH_REFERENCESKETCH_HEADER_LINE_HISTORY(HeaderId,1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      RowNo = li.ROW_NO,
                                      RelatedTo = li.RelatedTo,
                                      CheckPointDesc = li.CheckPointDesc,
                                      YesNo = li.Yes_No,
                                      Remarks = li.Remarks,
                                      PreparedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn == null || li.CreatedOn.Value == DateTime.MinValue ? "NA" : li.CreatedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      CheckedBy = li.EditedBy,
                                      CheckedOn = li.EditedOn == null || li.EditedOn.Value == DateTime.MinValue ? "NA" : li.EditedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      DocumentNo = li.Document,

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult CreateReferenceSketch(int HeaderID = 0)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("PLN003");
            if (HeaderID > 0)
            {
                //string currentUser = objClsLoginInfo.UserName;
                //PLN003 objPLN003 = db.PLN003.Where(x => x.HeaderId == HeaderID && x.CreatedBy == currentUser).FirstOrDefault();
                //if (objPLN003 == null)
                //{
                //    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                //}
            }
            ViewBag.HeaderID = HeaderID;
            return View();
        }

        [HttpPost]
        public ActionResult LoadRefSketchCreateFormPartial(int HeaderID = 0)
        {
            PLN003 objPLN003 = db.PLN003.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
            if (objPLN003 != null)
            {
                var projectDetails = db.COM001.Where(x => x.t_cprj == objPLN003.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                ViewBag.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string ApproverName = db.COM003.Where(x => x.t_psno == objPLN003.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                if (!string.IsNullOrEmpty(ApproverName))
                {
                    ViewBag.ApproverName = objPLN003.ApprovedBy + " - " + ApproverName;
                }
                string customerName = db.COM006.Where(x => x.t_bpid == objPLN003.Customer).Select(x => x.t_nama).FirstOrDefault();
                ViewBag.customerName = objPLN003.Customer + " - " + customerName;
                string strRefSketch = clsImplementationEnum.PlanList.Reference_Sketch.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments1("PLN003/" + objPLN003.HeaderId + "/" + objPLN003.RevNo));
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strRefSketch &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();
                if (latestRecord != null)
                {
                    PLN004 objPLN004 = db.PLN004.Where(x => x.HeaderId == objPLN003.HeaderId).FirstOrDefault();
                    if (objPLN004 != null)
                    {
                        if (latestRecord.RevNo == objPLN004.PlanRevNo)
                        {
                            ViewBag.isLatest = "true";
                        }
                        else
                        {
                            ViewBag.isLatest = "false";
                        }
                    }
                    else
                    {
                        ViewBag.isLatest = "false";
                    }
                }
                else
                {
                    ViewBag.isLatest = "true";
                }

                var PlanningDinID = db.PDN002.Where(x => x.RefId == HeaderID && x.DocumentNo == objPLN003.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;

                ViewBag.IsReviseBtnEnabled = Manager.IsReviseEnabled(PlanningDinID, objPLN003.HeaderId, objPLN003.Document);
                ViewBag.DocMessage = clsImplementationMessage.CommonMessages.DocMessage.ToString();
            }
            else
            {
                objPLN003 = new PLN003();
                objPLN003.ProcessPlan = clsImplementationEnum.PlanList.Reference_Sketch.GetStringValue();
                ViewBag.isLatest = "true";
                objPLN003.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                objPLN003.RevNo = 0;
            }
            ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            return PartialView("_LoadRefSketchCreateFormPartial", objPLN003);
        }

        [HttpPost]
        public JsonResult LoadCheckListData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                PLN003 objPLN001 = db.PLN003.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                string strRefSketch = clsImplementationEnum.PlanList.Reference_Sketch.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strRefSketch &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();

                if (latestRecord == null)
                {
                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = 0,
                        iTotalDisplayRecords = 0,
                        aaData = new string[] { }
                    }, JsonRequestBehavior.AllowGet);
                }
                int? PlanRev = 0;
                if (HeaderId > 0)
                {
                    strWhere += "pln004.HeaderId = " + HeaderId + " and ";
                    PLN004 objPLN004 = db.PLN004.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    if (objPLN004 != null)
                    {
                        PlanRev = objPLN004.PlanRevNo;
                    }
                    else
                    {
                        PlanRev = 0;
                    }
                }
                else
                {
                    PlanRev = latestRecord.RevNo;
                }
                strWhere += "1=1 and pln002.[Plan] in('" + clsImplementationEnum.PlanList.Reference_Sketch.GetStringValue() + "') and pln002.RevNo = " + PlanRev + "";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = { "RelatedTo", "[CheckPoint]" };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                
               var lstResult = db.SP_FETCH_REFERENCESKETCH_HEADER_LINE
                                (
                                     HeaderId,
                                     latestRecord.RevNo,
                                     StartIndex,
                                     EndIndex,
                                     strSortOrder,
                                    (!string.IsNullOrWhiteSpace(strWhere) ? strWhere : null)
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.RelatedTo),
                                Convert.ToString(uc.CheckPointDesc),
                                //LineOperation(Convert.ToBoolean(uc.Yes_No),uc.LineId,uc.HeaderId),
                                Manager.CheckListLineOperationYesNo(uc.Yes_No,uc.LineId,uc.HeaderId,false,uc.ActualId),
                                Manager.CheckListLineOperationRemarks(uc.Remarks,uc.LineId,uc.HeaderId,false),
                                Convert.ToString(uc.CreatedBy),
                                uc.CreatedOn.HasValue ? Convert.ToString(uc.CreatedOn.Value.ToShortDateString()) : "",
                                Convert.ToString(uc.EditedBy),
                                uc.EditedOn.HasValue ? Convert.ToString(uc.EditedOn.Value.ToShortDateString()) : "",
                                getChecklistDetails(uc.CheckListId.ToString())[0],
                                getChecklistDetails(uc.CheckListId.ToString())[1],
                                getChecklistDetails(uc.CheckListId.ToString())[2],
                                getChecklistDetails(uc.CheckListId.ToString())[3],
                                Convert.ToString(uc.IsLatest),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [NonAction]
        public string[] getChecklistDetails(string lineId)
        {
            string[] checklist = new string[4];
            int LineID = Int32.Parse(lineId);
            var data = (from p2 in db.PLN002
                        join
                            p1 in db.PLN001 on p2.HeaderId equals p1.HeaderId
                        where p2.LineId == LineID
                        select p1).FirstOrDefault();
            if (data != null)
            {
                checklist[0] = Manager.GetUserNameFromPsNo(data.CreatedBy); //prepared by
                checklist[1] = Manager.GetUserNameFromPsNo(data.ApprovedBy); // approved by
                checklist[2] = data.ApprovedOn.ToString(); // approved on
                checklist[3] = data.Document; // document no
            }
            return checklist;
        }

        [HttpPost]
        public ActionResult SaveHeader(PLN003 pln003, string strHeaderId, string strLineId, string strIsLatest)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                //string HeaderId = fc["hdHeaderId"];
                //string LineId = fc["hdLineId"];
                int NewHeaderId = 0;
                if (pln003.HeaderId > 0)
                {
                    PLN003 objPLN003 = db.PLN003.Where(x => x.HeaderId == pln003.HeaderId).FirstOrDefault();
                    if (objPLN003 != null)
                    {
                        objPLN003.Product = pln003.Product;
                        objPLN003.ProcessLicensor = pln003.ProcessLicensor;
                        objPLN003.JobNo = pln003.JobNo;
                        objPLN003.ReviseRemark = pln003.ReviseRemark;
                        objPLN003.ApprovedBy = pln003.ApprovedBy;
                        objPLN003.EditedBy = objClsLoginInfo.UserName;
                        objPLN003.EditedOn = DateTime.Now;
                        if (objPLN003.Status == clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue())
                        {
                            objPLN003.RevNo = Convert.ToInt32(objPLN003.RevNo) + 1;
                            objPLN003.ReturnRemark = null;
                            objPLN003.ApprovedOn = null;
                            objPLN003.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                        }
                        if (!Convert.ToBoolean(strIsLatest))
                        {
                            List<PLN004> lstPLN004 = db.PLN004.Where(x => x.HeaderId == objPLN003.HeaderId).ToList();
                            if (lstPLN004 != null && lstPLN004.Count > 0)
                            {
                                db.PLN004.RemoveRange(lstPLN004);
                            }
                            string strRefSketch = clsImplementationEnum.PlanList.Reference_Sketch.GetStringValue();
                            string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                            var latestRecord = db.PLN001
                                     .Where(x
                                                => x.Plan == strRefSketch &&
                                                   x.Status == strStatus
                                            )
                                     .OrderByDescending(x => x.HeaderId)
                                     .Select(x => new { x.HeaderId, x.RevNo })
                                     .FirstOrDefault();
                            List<PLN002> lstPLN002 = db.PLN002.Where(x => x.HeaderId == latestRecord.HeaderId).ToList();
                            if (lstPLN002 != null && lstPLN002.Count > 0)
                            {
                                List<PLN004> NewlstPLN004 = lstPLN002.Select(x => new PLN004()
                                {
                                    LineId = x.LineId,
                                    HeaderId = objPLN003.HeaderId,
                                    Project = objPLN003.Project,
                                    Document = objPLN003.Document,
                                    RevNo = objPLN003.RevNo,
                                    Plan = objPLN003.ProcessPlan,
                                    PlanRevNo = x.RevNo,
                                    CheckListId = x.LineId,
                                    //Yes_No = false,
                                    //Yes_No = clsImplementationEnum.ChecklistYesNo.No.GetStringValue(), as per observation 15003
                                    Yes_No = string.Empty,
                                    CreatedBy = objClsLoginInfo.UserName,
                                    CreatedOn = DateTime.Now
                                }).ToList();
                                db.PLN004.AddRange(NewlstPLN004);
                            }
                        }
                        else
                        {
                            List<PLN004> lstPLN004 = db.PLN004.Where(x => x.HeaderId == objPLN003.HeaderId).ToList();
                            if (lstPLN004.Count > 0)
                            {
                                List<PLN004> lstTruePLN004 = new List<PLN004>();

                                foreach (PLN004 objPLN004 in lstPLN004)
                                {
                                    //objPLN004.Yes_No = (!string.IsNullOrWhiteSpace(strLineId) && Array.ConvertAll(strLineId.Split(',').ToArray(), s => int.Parse(s)).Length > 0 && Array.ConvertAll(strLineId.Split(',').ToArray(), s => int.Parse(s)).Contains(objPLN004.LineId) ? true : false);
                                    objPLN004.EditedBy = objClsLoginInfo.UserName;
                                    objPLN004.EditedOn = DateTime.Now;
                                }
                            }
                        }
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.PlanMessages.Update.ToString();
                        NewHeaderId = objPLN003.HeaderId;
                        objResponseMsg.Status = objPLN003.Status;
                        objResponseMsg.Revision = objPLN003.RevNo;
                        objResponseMsg.HeaderID = NewHeaderId;

                        //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                        Manager.UpdatePDN002(objPLN003.HeaderId, objPLN003.Status, objPLN003.RevNo, objPLN003.Project, objPLN003.Document);
                        //string unReleasedStatus = clsImplementationEnum.PlanningDinStatus.UNRELEASED.GetStringValue();
                        //PDN002 objPDN002 = db.PDN002.Where(x => x.RefId == objPLN003.HeaderId && x.Status == unReleasedStatus).FirstOrDefault();
                        //if (objPDN002 != null)
                        //{
                        //    objPDN002.RevNo = objPLN003.RevNo;
                        //    objPDN002.Status = objPLN003.Status;
                        //    db.SaveChanges();
                        //}
                        //--

                        //if (hasAttachments)
                        //{
                        var folderPath = "PLN003/" + NewHeaderId + "/" + objPLN003.RevNo;
                        //    var existing = clsUpload.getDocs(folderPath);
                        //    var toDelete = new Dictionary<string, string>();
                        //    foreach (var item in existing)
                        //    {
                        //        if (Attach.Where(q => q.Key.Equals(item.Key)).Count() <= 0)
                        //            toDelete.Add(item.Key, item.Value);
                        //        else
                        //        {
                        //            if (Attach.Where(q => q.Key.Equals(item.Key)).FirstOrDefault().Value.Substring(0, 3) != "URL")
                        //                toDelete.Add(item.Key, item.Value);
                        //        }
                        //    }
                        //    foreach (var item in toDelete)
                        //        clsUpload.DeleteFile(folderPath, item.Key);
                        //    var toUpload = Attach.Where(q => q.Value.Substring(0, 3) != "URL");

                        //    foreach (var attch in toUpload)
                        //    {
                        //        var base64Data = attch.Value;
                        //        var dataBytes = Helper.FromBase64(attch.Value);
                        //        //var dataBytes = Encoding.ASCII.GetBytes(stringData);
                        //        clsUpload.Upload(attch.Key, dataBytes, folderPath);
                        //    }
                        //}
                        //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Process Details not available.";
                    }
                }
                else
                {
                    PLN003 objPLN003 = db.PLN003.Add(new PLN003
                    {
                        Project = pln003.Project,
                        Document = pln003.Document,
                        Customer = pln003.Customer,
                        RevNo = 0,
                        Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                        CDD = Helper.ToNullIfTooEarlyForDb(pln003.CDD),
                        Product = pln003.Product,
                        ProcessLicensor = pln003.ProcessLicensor,
                        ProcessPlan = pln003.ProcessPlan,
                        JobNo = pln003.JobNo,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now,
                        ApprovedBy = pln003.ApprovedBy
                    });
                    db.SaveChanges();
                    NewHeaderId = objPLN003.HeaderId;
                    if (!string.IsNullOrWhiteSpace(strHeaderId))
                    {
                        int planHeaderId = Convert.ToInt32(strHeaderId);
                        List<PLN002> lstPLN002 = db.PLN002.Where(x => x.HeaderId == planHeaderId).ToList();
                        List<PLN004> lstPLN004 = new List<PLN004>();
                        if (lstPLN002.Count > 0)
                        {
                            List<PLN002> lstTruePLN002 = new List<PLN002>();
                            if (!string.IsNullOrWhiteSpace(strLineId))
                            {
                                int[] arrayLineId = Array.ConvertAll(strLineId.Split(',').ToArray(), s => int.Parse(s));
                                lstTruePLN002 = lstPLN002.Where(x => arrayLineId.Contains(x.LineId)).ToList();
                            }
                            foreach (PLN002 objPLN002 in lstPLN002)
                            {
                                PLN004 objPLN004 = new PLN004();
                                objPLN004.HeaderId = objPLN003.HeaderId;
                                objPLN004.Project = objPLN003.Project;
                                objPLN004.Document = objPLN003.Document;
                                objPLN004.RevNo = objPLN003.RevNo;
                                objPLN004.Plan = objPLN003.ProcessPlan;
                                objPLN004.PlanRevNo = objPLN002.RevNo;
                                objPLN004.CheckListId = objPLN002.LineId;
                                objPLN004.Yes_No = clsImplementationEnum.ChecklistYesNo.No.ToString();
                                objPLN004.Remarks = "";
                                //objPLN004.Yes_No = (lstTruePLN002.Where(x => x.LineId == objPLN002.LineId).FirstOrDefault() != null ? true : false);
                                objPLN004.CreatedBy = objClsLoginInfo.UserName;
                                objPLN004.CreatedOn = DateTime.Now;
                                lstPLN004.Add(objPLN004);
                            }
                        }
                        if (lstPLN004 != null && lstPLN004.Count > 0)
                        {
                            db.PLN004.AddRange(lstPLN004);
                            db.SaveChanges();
                        }
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PlanMessages.Insert.ToString();
                    objResponseMsg.Status = objPLN003.Status;
                    objResponseMsg.Revision = objPLN003.RevNo;
                    objResponseMsg.HeaderID = NewHeaderId;

                    //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                    Manager.UpdatePDN002(objPLN003.HeaderId, objPLN003.Status, objPLN003.RevNo, objPLN003.Project, objPLN003.Document);
                    //string unReleasedStatus = clsImplementationEnum.PlanningDinStatus.UNRELEASED.GetStringValue();
                    //PDN002 objPDN002 = db.PDN002.Where(x => x.RefId == objPLN003.HeaderId && x.Status == unReleasedStatus).FirstOrDefault();
                    //if (objPDN002 != null)
                    //{
                    //    objPDN002.RevNo = objPLN003.RevNo;
                    //    objPDN002.Status = objPLN003.Status;
                    //    db.SaveChanges();
                    //}
                    //--

                    //if (hasAttachments)
                    //{
                    var folderPath = "PLN003/" + NewHeaderId + "/" + objPLN003.RevNo;
                    //    var existing = clsUpload.getDocs(folderPath);
                    //    var toDelete = new Dictionary<string, string>();
                    //    foreach (var item in existing)
                    //    {
                    //        if (Attach.Where(q => q.Key.Equals(item.Key)).Count() <= 0)
                    //            toDelete.Add(item.Key, item.Value);
                    //        else
                    //        {
                    //            if (Attach.Where(q => q.Key.Equals(item.Key)).FirstOrDefault().Value.Substring(0, 3) != "URL")
                    //                toDelete.Add(item.Key, item.Value);
                    //        }
                    //    }
                    //    foreach (var item in toDelete)
                    //        clsUpload.DeleteFile(folderPath, item.Key);
                    //    var toUpload = Attach.Where(q => q.Value.Substring(0, 3) != "URL");

                    //    foreach (var attch in toUpload)
                    //    {
                    //        var base64Data = attch.Value;
                    //        var dataBytes = Helper.FromBase64(attch.Value);
                    //        //var dataBytes = Encoding.ASCII.GetBytes(stringData);
                    //        clsUpload.Upload(attch.Key, dataBytes, folderPath);
                    //    }
                    //}
                    //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveHeaderLine(int LineId, int HeaderId, string ActualId, string YesNo, string Remarks)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                PLN003 objPLN003 = db.PLN003.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                var splitid = ActualId.Split(',');
                if (Convert.ToInt32(splitid[0]) == 0)
                {
                    PLN004 objPLN004 = db.PLN004.Where(x => x.CheckListId == LineId && x.HeaderId == HeaderId).FirstOrDefault();
                    objPLN004.Yes_No = YesNo;
                    objPLN004.Remarks = Remarks;
                    objPLN004.EditedBy = objClsLoginInfo.UserName;
                    objPLN004.EditedOn = DateTime.Now;
                    db.SaveChanges();
                }
                else
                {
                    int ActualLineId = Convert.ToInt32(splitid[0]);
                    PLN004 objPLN004 = db.PLN004.Where(x => x.LineId == ActualLineId && x.HeaderId == HeaderId).FirstOrDefault();
                    objPLN004.Yes_No = YesNo;
                    objPLN004.Remarks = Remarks;
                    objPLN004.EditedBy = objClsLoginInfo.UserName;
                    objPLN004.EditedOn = DateTime.Now;
                    db.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SendForApproval(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrLineStatus = { clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(), clsImplementationEnum.PlanStatus.Returned.GetStringValue() };
                PLN003 objPLN003 = db.PLN003.Where(x => x.HeaderId == HeaderId && arrLineStatus.Contains(x.Status)).FirstOrDefault();
                if (objPLN003 != null)
                {
                    objPLN003.Status = clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue();
                    objPLN003.SubmittedBy = objClsLoginInfo.UserName;
                    objPLN003.SubmittedOn = DateTime.Now;

                    //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                    Manager.UpdatePDN002(objPLN003.HeaderId, objPLN003.Status, objPLN003.RevNo, objPLN003.Project, objPLN003.Document);
                    //string unReleasedStatus = clsImplementationEnum.PlanningDinStatus.UNRELEASED.GetStringValue();
                    //PDN002 objPDN002 = db.PDN002.Where(x => x.RefId == objPLN003.HeaderId && x.Status == unReleasedStatus).FirstOrDefault();
                    //if (objPDN002 != null)
                    //{
                    //    objPDN002.RevNo = objPLN003.RevNo;
                    //    objPDN002.Status = objPLN003.Status;
                    //}
                    //--

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Checklist successfully sent for approval.";

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PLNG1.GetStringValue(),
                                                        objPLN003.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objPLN003.Project, clsImplementationEnum.PlanList.Reference_Sketch.GetStringValue(), objPLN003.RevNo.Value.ToString(), objPLN003.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Reference_Sketch.GetStringValue(), objPLN003.HeaderId.ToString(), true),
                                                        objPLN003.ApprovedBy);
                    #endregion
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Checklist not available for approval.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //added by nikita on 31/08/2017
        [HttpPost]
        public ActionResult RetractStatus(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PLN003 objPLN003 = db.PLN003.Where(u => u.HeaderId == headerId).SingleOrDefault();
                if (objPLN003 != null)
                {
                    objPLN003.Status = clsImplementationEnum.PLCStatus.DRAFT.GetStringValue();
                    objPLN003.EditedBy = objClsLoginInfo.UserName;
                    objPLN003.EditedOn = DateTime.Now;
                    objPLN003.SubmittedOn = null;
                    objPLN003.SubmittedBy = null;
                    db.SaveChanges();
                    Manager.UpdatePDN002(objPLN003.HeaderId, objPLN003.Status, objPLN003.RevNo, objPLN003.Project, objPLN003.Document);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Retract.ToString();
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public string LineOperation(bool flag, int LineId, int HeaderId)
        {
            if (flag)
            {
                return "<label class=\"mt-checkbox mt-checkbox-outline\">" +
                       "<input type=\"checkbox\" name=\"chkYesNo\" checked=\"checked\" sHeaderId=\"" + HeaderId + "\" sLineId=\"" + LineId + "\" />" +
                       "<span></ span >" +
                       "</label>";
            }
            else
            {
                return "<label class=\"mt-checkbox mt-checkbox-outline\">" +
                       "<input type=\"checkbox\" name=\"chkYesNo\" sHeaderId=\"" + HeaderId + "\" sLineId=\"" + LineId + "\" />" +
                       "<span></ span >" +
                       "</label>";
            }
        }


        [HttpPost]
        public ActionResult GetCustomerProjectWise(string projectCode)
        {
            ApproverModel objApproverModel = Manager.GetCustomerProjectWise(projectCode);
            string strCdd = Manager.GetContractWiseCdd(projectCode);
            if (!string.IsNullOrWhiteSpace(strCdd))
            {
                DateTime dtCdd = Convert.ToDateTime(strCdd);
                objApproverModel.Cdd = dtCdd.Date.ToShortDateString();
            }
            return Json(objApproverModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetHistoryDetails(string strRole, int headerid)
        {
            ViewBag.Role = strRole;
            Session["headerid"] = Convert.ToInt32(headerid);
            return PartialView("_GetHistoryDetailsPartial");
        }

        [HttpPost]
        public JsonResult LoadReferenceSketchHistory(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "INITIATOR")
                {
                    strWhere += "1=1   and HeaderId='" + Convert.ToInt32(Session["headerid"]) + "'";

                }
                else
                {
                    strWhere += "1=1 and pln003.ApprovedBy = '" + objClsLoginInfo.UserName + "'  and HeaderId='" + Convert.ToInt32(Session["headerid"]) + "'";

                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "pln003.Project+' - '+com1.t_dsca",
                                                "Document",
                                                "pln003.Customer+' - '+com6.t_nama",
                                                "Status",
                                                "Product",
                                                "ProcessLicensor",
                                                "ProcessPlan",
                                                "JobNo"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_FETCH_REFERENCESKETCH_HEADERS_HISTORY
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString(uc.Customer),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.CDD),
                               Convert.ToString(uc.ProcessPlan),
                               Convert.ToString(uc.CreatedBy),
                               uc.CreatedOn.HasValue ? Convert.ToString(uc.CreatedOn.Value.ToShortDateString()) : "",
                               Convert.ToString(uc.EditedBy),
                               uc.EditedOn.HasValue ? Convert.ToString(uc.EditedOn.Value.ToShortDateString()) : "",
                               Convert.ToString(uc.SubmittedBy),
                               Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),
                               Convert.ToString(uc.ApprovedBy),
                               uc.ApprovedOn.HasValue ? Convert.ToString(uc.ApprovedOn.Value.ToShortDateString()) : "",
                               Convert.ToString(uc.ReturnRemark),
                               (param.CTQCompileStatus.ToUpper() == "INITIATOR"?"<center><a title='View' href='"+WebsiteURL+"/PLN/MaintainReferenceSketch/ViewHistory?Id="+Convert.ToInt32(uc.Id)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a></center>":"<center><a title='View' href='"+WebsiteURL+"/PLN/ApproveReferenceSketch/ViewHistory?Id="+Convert.ToInt32(uc.Id)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a><a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintainFXR/ShowHistoryTimeline?HeaderID=" + Convert.ToInt32(uc.Id) + "')><i style = 'margin-left:5px;'  class='fa fa-clock-o'></i></a></center>")
                               //GetButtonsStatusWise(uc.Status,uc.HeaderId,(lstResult.Count >0 && lstResult.Select(x=>x.RevNo).Max() == uc.RevNo && uc.Status == clsImplementationEnum.PlanStatus.Approved.GetStringValue()?true:false))
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]//, AllowAnonymous, UserPermissions
        public ActionResult ViewHistory(int Id = 0)
        {
            if (Id > 0)
            {
                string currentUser = objClsLoginInfo.UserName;
                //PLN003_Log objPLN003_Log = db.PLN003_Log.Where(x => x.Id == Id && x.CreatedBy == currentUser).FirstOrDefault();
                //if (objPLN003_Log == null)
                //{
                //    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                //}
            }
            ViewBag.Id = Id;
            return View();
        }

        [HttpPost]
        public ActionResult LoadRefSketchHistoryFormPartial(int Id = 0)
        {
            PLN003_Log objPLN003_Log = db.PLN003_Log.Where(x => x.Id == Id).FirstOrDefault();
            if (objPLN003_Log != null)
            {
                var projectDetails = db.COM001.Where(x => x.t_cprj == objPLN003_Log.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                objPLN003_Log.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string ApproverName = db.COM003.Where(x => x.t_psno == objPLN003_Log.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                objPLN003_Log.ApprovedBy = objPLN003_Log.ApprovedBy + " - " + ApproverName;
                string customerName = db.COM006.Where(x => x.t_bpid == objPLN003_Log.Customer).Select(x => x.t_nama).FirstOrDefault();
                objPLN003_Log.Customer = objPLN003_Log.Customer + " - " + customerName;
                string strRefSketch = clsImplementationEnum.PlanList.Reference_Sketch.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strRefSketch &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();
                if (latestRecord != null)
                {
                    //if (latestRecord.HeaderId == objPLN003.HeaderId)
                    //{

                    //}
                }
                ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments1("PLN003/" + objPLN003_Log.HeaderId + "/" + objPLN003_Log.RevNo));
            }
            else
            {
                objPLN003_Log = new PLN003_Log();
                objPLN003_Log.ProcessPlan = clsImplementationEnum.PlanList.Reference_Sketch.GetStringValue();
                objPLN003_Log.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                objPLN003_Log.RevNo = 0;
            }
            // ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            //var urlPrefix = "http://" + Request.Url.Authority + "/PLN/MaintainReferenceSketch/ViewHistory?Id=";// AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.LastIndexOf('=') + 1);
            var urlPrefix = WebsiteURL + "/PLN/MaintainReferenceSketch/ViewHistory?Id=";
            if (!objClsLoginInfo.GetUserRoleList().Contains(clsImplementationEnum.UserRoleName.SHOP.GetStringValue()))
            {
                ViewBag.RevPrev = (db.PLN003_Log.Any(q => (q.HeaderId == objPLN003_Log.HeaderId && q.RevNo == (objPLN003_Log.RevNo - 1))) ? urlPrefix + db.PLN003_Log.Where(q => (q.HeaderId == objPLN003_Log.HeaderId && q.RevNo == (objPLN003_Log.RevNo - 1))).FirstOrDefault().Id : null);
                ViewBag.RevNext = (db.PLN003_Log.Any(q => (q.HeaderId == objPLN003_Log.HeaderId && q.RevNo == (objPLN003_Log.RevNo + 1))) ? urlPrefix + db.PLN003_Log.Where(q => (q.HeaderId == objPLN003_Log.HeaderId && q.RevNo == (objPLN003_Log.RevNo + 1))).FirstOrDefault().Id : null);
                ViewBag.Isshopuser = false;
            }
            else
            {
                ViewBag.Isshopuser = true;
            }
            return PartialView("_LoadRefSketchHistoryFormPartial", objPLN003_Log);
        }

        [HttpPost]
        public JsonResult LoadCheckListHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                int Id = Convert.ToInt32(param.CTQHeaderId);
                PLN003_Log objPLN003_Log = db.PLN003_Log.Where(x => x.Id == Id).FirstOrDefault();

                strWhere += "1=1 and pln004.RefId = " + objPLN003_Log.Id + "";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = { "RelatedTo", "[CheckPoint]" };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_FETCH_REFERENCESKETCH_HEADER_LINE_HISTORY
                                (
                                     objPLN003_Log.HeaderId,
                                     StartIndex,
                                     EndIndex,
                                     strSortOrder,
                                    (!string.IsNullOrWhiteSpace(strWhere) ? strWhere : null)
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.RelatedTo),
                                Convert.ToString(uc.CheckPointDesc),
                                //LineOperation(Convert.ToBoolean(uc.Yes_No),uc.LineId,uc.HeaderId),
                                Manager.CheckListLineOperationYesNo(uc.Yes_No,uc.LineId,uc.HeaderId,true,uc.ActualId),
                                Manager.CheckListLineOperationRemarks(uc.Remarks,uc.LineId,uc.HeaderId,true),
                                Convert.ToString(uc.CreatedBy),
                                uc.CreatedOn.HasValue ? Convert.ToString(uc.CreatedOn.Value.ToShortDateString()) : "",
                                Convert.ToString(uc.EditedBy),
                                uc.EditedOn.HasValue ? Convert.ToString(uc.EditedOn.Value.ToShortDateString()) : "",
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public bool checkProjectExist(string projectcode)
        {
            bool Flag = false;
            if (!string.IsNullOrWhiteSpace(projectcode))
            {
                PLN003 objPLN003 = db.PLN003.Where(x => x.Project == projectcode).FirstOrDefault();
                if (objPLN003 != null)
                {
                    Flag = true;
                }
            }
            return Flag;
        }

        [HttpPost]
        public ActionResult ReviseHeader(int strHeaderId, string strRemarks)
        {
            CustomResponceMsg objResponseMsg = new CustomResponceMsg();
            try
            {
                PLN003 objPLN003 = db.PLN003.Where(u => u.HeaderId == strHeaderId).SingleOrDefault();
                if (objPLN003 != null)
                {
                    objPLN003.RevNo = Convert.ToInt32(objPLN003.RevNo) + 1;
                    objPLN003.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                    objPLN003.ReviseRemark = strRemarks;
                    objPLN003.EditedBy = objClsLoginInfo.UserName;
                    objPLN003.EditedOn = DateTime.Now;
                    objPLN003.ReturnRemark = null;
                    objPLN003.ApprovedOn = null;
                    objPLN003.SubmittedBy = null;
                    objPLN003.SubmittedOn = null;
                    db.SaveChanges();
                    Manager.UpdatePDN002(objPLN003.HeaderId, objPLN003.Status, objPLN003.RevNo, objPLN003.Project, objPLN003.Document);
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderID = objPLN003.HeaderId;
                    objResponseMsg.Status = objPLN003.Status;
                    objResponseMsg.rev = objPLN003.RevNo;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revision;
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        #region Inline Grid New Method
        [HttpPost]
        public JsonResult LoadCheckListDataNew(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                PLN003 objPLN001 = db.PLN003.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                string HeaderStatus = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                bool IsDisable = false;
                if (objPLN001 != null)
                {
                    HeaderStatus = objPLN001.Status;
                }
                if (HeaderId == 0 || (HeaderId > 0 && (HeaderStatus.ToLower() == clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue().ToLower() || HeaderStatus.ToLower() == clsImplementationEnum.PlanStatus.Approved.GetStringValue().ToLower())))
                {
                    IsDisable = true;
                }

                string strRefSketch = clsImplementationEnum.PlanList.Reference_Sketch.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strRefSketch &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();

                if (latestRecord == null)
                {
                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = 0,
                        iTotalDisplayRecords = 0,
                        aaData = new string[] { }
                    }, JsonRequestBehavior.AllowGet);
                }
                int? PlanRev = 0;
                if (HeaderId > 0)
                {
                    strWhere += "pln004.HeaderId = " + HeaderId + " and ";
                    PLN004 objPLN004 = db.PLN004.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    if (objPLN004 != null)
                    {
                        PlanRev = objPLN004.PlanRevNo;
                    }
                    else
                    {
                        PlanRev = 0;
                    }
                }
                else
                {
                    PlanRev = latestRecord.RevNo;
                }
                strWhere += "1=1 and pln002.[Plan] in('" + clsImplementationEnum.PlanList.Reference_Sketch.GetStringValue() + "') and pln002.RevNo = " + PlanRev + "";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = { "RelatedTo", "[CheckPoint]" };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
               // var strSortOrder = string.Empty;
                var lstResult = db.SP_FETCH_REFERENCESKETCH_HEADER_LINE
                                (
                                     HeaderId,
                                     latestRecord.RevNo,
                                     StartIndex,
                                     EndIndex,
                                     strSortOrder,
                                    (!string.IsNullOrWhiteSpace(strWhere) ? strWhere : null)
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.RelatedTo),
                                Convert.ToString(uc.CheckPointDesc),
                                //LineOperation(Convert.ToBoolean(uc.Yes_No),uc.LineId,uc.HeaderId),
                                (new PLNCommonFunction()).CheckListLineOperationYesNoNew(uc.Yes_No,uc.LineId,uc.HeaderId,IsDisable,uc.ActualId,"Yes_No","UpdateData(this)"),
                                (new PLNCommonFunction()).CheckListLineOperationRemarksNew(uc.Remarks,uc.LineId,uc.HeaderId,IsDisable,uc.ActualId,"Remarks","UpdateData(this)"),
                                Convert.ToString(uc.CreatedBy),
                                uc.CreatedOn.HasValue ? Convert.ToString(uc.CreatedOn.Value.ToShortDateString()) : "",
                                Convert.ToString(uc.EditedBy),
                                uc.EditedOn.HasValue ? Convert.ToString(uc.EditedOn.Value.ToShortDateString()) : "",
                                //getChecklistDetails(uc.CheckListId.ToString())[0],
                                //getChecklistDetails(uc.CheckListId.ToString())[1],
                                //getChecklistDetails(uc.CheckListId.ToString())[2],
                                //getChecklistDetails(uc.CheckListId.ToString())[3],
                                Convert.ToString(uc.IsLatest),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult UpdateData(string id, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var splitid = id.Split(',');
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    db.SP_PLN_UPDATE_CHECKLIST_TABLE_COLUMN("pln004", "LineId", splitid[0], columnName, columnValue);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}