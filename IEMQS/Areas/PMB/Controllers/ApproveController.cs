﻿using IEMQS.Areas.MSW.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.PMB.Controllers
{
    public class ApproveController : clsBase
    {
        //modified by:Nikita Vibhandik (13-09-2017)
        //changes: added common function for Planning Din (PDN002) (Function name - Updatepdn002)
        [SessionExpireFilter]
        // GET: PMB/Approve
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult GetHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("PMBApproveGridPartial");
        }

        public ActionResult ApproveSelectedPMB(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
                for (int i = 0; i < arrayHeaderIds.Length; i++)
                {
                    int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                    db.SP_PMB_APPROVE(HeaderId, objClsLoginInfo.UserName);
                    PMB001 objPMB001 = db.PMB001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    int newId = db.PMB001_Log.Where(q => q.HeaderId == HeaderId).OrderByDescending(q => q.ApprovedOn).FirstOrDefault().Id;
                    Manager.UpdatePDN002(objPMB001.HeaderId, objPMB001.Status, objPMB001.RevNo, objPMB001.Project, objPMB001.Document, newId);

                    int? currentRevisionNull = db.PMB001.Where(q => q.HeaderId == HeaderId).FirstOrDefault().RevNo;
                    int currentRevision = ((currentRevisionNull == null) ? 0 : currentRevisionNull.Value);
                    //clsUpload.CopyFolderContents("PMB001/" + HeaderId + "/R" + currentRevision, "PMB001/" + HeaderId + "/R" + (currentRevision+1));
                    var oldfolderPath = "PMB001//" + HeaderId + "//R" + currentRevision;
                    var newfolderPath = "PMB001//" + HeaderId + "//R" + (currentRevision + 1);

                    //(new clsFileUpload()).CopyFolderContentsAsync(oldfolderPath, newfolderPath);

                    IEMQS.Areas.Utility.Controllers.FileUploadController _obj = new Utility.Controllers.FileUploadController();
                    _obj.CopyDataOnFCSServerAsync(oldfolderPath, newfolderPath, oldfolderPath, HeaderId, newfolderPath, HeaderId, IEMQS.DESServices.CommonService.GetUseIPConfig, IEMQS.DESServices.CommonService.objClsLoginInfo.UserName, 0);


                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(),
                                                        objPMB001.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objPMB001.Project, clsImplementationEnum.PlanList.Pre_Manufacturing.GetStringValue(), objPMB001.RevNo.Value.ToString(), objPMB001.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Pre_Manufacturing.GetStringValue(), objPMB001.HeaderId.ToString(), false),
                                                        objPMB001.CreatedBy);
                    #endregion
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.QCPMessages.Approve.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult LoadPMBApproveHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue() + "')  and ApprovedBy = " + user+"";
                }
                else
                {
                    strWhere += "1=1";
                }
              //  strWhere += " and ApprovedBy=" + user;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (Project like '%" + param.sSearch
                         + "%' or Document like '%" + param.sSearch
                         + "%' or Customer like '%" + param.sSearch
                         + "%' or RevNo like '%" + param.sSearch
                         + "%' or Product like '%" + param.sSearch
                         + "%' or ProcessLicensor like '%" + param.sSearch
                         + "%' or Status like '%" + param.sSearch + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                var lstResult = db.SP_PMB_GET_HEADERDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {

                         Convert.ToString(uc.HeaderId),
                           Convert.ToString(GetCodeAndNameByProject(uc.Project)),
                           Convert.ToString(uc.Document),
                           Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Project)),
                           //Convert.ToString(uc.CDD),
                           Convert.ToString(uc.Product),
                           Convert.ToString(uc.ProcessLicensor),
                           Convert.ToString("R"+uc.RevNo),
                           Convert.ToString(uc.Status),
                            Convert.ToString(uc.SubmittedBy),
                            Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),
                            Convert.ToString(uc.ApprovedBy),
                            Convert.ToString(Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy")),

                          // "<a title='View' href='/PMB/Approve/PMBApproveDetail?Id="+Convert.ToInt32(uc.HeaderId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a>"+" "+(uc.RevNo>0 ?"<a title=\"History\" onclick=\"ViewHistoryForProjectPLN('"+uc.HeaderId+"','Initiator','/PMB/Approve/GetHistoryView','History of Pre Manufacturing')\"><i style='margin-left:5px;' class='fa fa-history'></i></a>":"")+" "+("<a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PMB/Header/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')><i class='fa fa-clock-o'></i></a>"),
                         Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public string GetCodeAndNameByProject(string project)
        {
            var projdesc = db.COM001.Where(i => i.t_cprj == project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            return projdesc;
        }
        [SessionExpireFilter]
        public ActionResult PMBApproveDetail(int Id = 0)
        {
            PMB001 objPMB001 = new PMB001();
            var user = objClsLoginInfo.UserName;

            var lstProjectDesc = (from a in db.COM001
                                  select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).ToList();
            ViewBag.Project = new SelectList(lstProjectDesc, "projectCode", "projectDescription");

            //List<string> lstProCat = clsImplementationEnum.getProductCategory().ToList();
            //ViewBag.lstProCat = lstProCat.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            //ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments("PMB001/" + objPMB001.HeaderId + "/" + objPMB001.RevNo));

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.lstyesno = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            var project = (from a in db.PMB001
                           where a.HeaderId == Id
                           select a.Project).FirstOrDefault();

            ViewBag.Customer = Manager.GetCustomerCodeAndNameByProject(project);

            var approver = (from t1 in db.ATH001
                            join t2 in db.COM003 on t1.Employee equals t2.t_psno
                            where t2.t_actv == 1
                            select new { t1.Employee, Desc = t2.t_psno + " - " + t2.t_name }).Distinct().ToList();
            ViewBag.approver = new SelectList(approver, "Employee", "Desc");

            if (Id > 0)
            {
                ViewBag.Customer = Manager.GetCustomerCodeAndNameByProject(project);
                objPMB001 = db.PMB001.Where(x => x.HeaderId == Id).FirstOrDefault();               
                var PlanningDinID = db.PDN002.Where(x => x.RefId == objPMB001.HeaderId && x.DocumentNo == objPMB001.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;
                ViewBag.Revoke = Manager.IsRevokeApplicable(PlanningDinID, Id, clsImplementationEnum.PlanList.Pre_Manufacturing.GetStringValue());
                if (objPMB001.ApprovedBy != null)
                {
                    if (objPMB001.ApprovedBy.Trim() != objClsLoginInfo.UserName.Trim())
                    {
                        ViewBag.IsDisplayMode = true;
                    }
                    else
                    {
                        ViewBag.IsDisplayMode = false;
                    }
                }
                else
                {
                    ViewBag.IsDisplayMode = true;
                }
            }
            return View(objPMB001);
        }
        [HttpPost]
        public ActionResult GetHistoryView(int headerid = 0)
        {
            PMB001_Log objPMB001 = new PMB001_Log();
            //ViewBag.Role = strRole;
            ViewBag.HeaderId = headerid;
            return PartialView("getHistoryPartial", objPMB001);
        }
        [HttpPost]
        public JsonResult LoadPMBHeaderHistoryData(JQueryDataTableParamModel param, string HeaderId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;

                string strWhere = string.Empty;
                if (!string.IsNullOrWhiteSpace(HeaderId))
                {
                    strWhere += " HeaderId = '" + HeaderId + "'";
                }
                //strWhere += "(CreatedBy = " + user + " or ApprovedBy = " + user+")";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (Project like '%" + param.sSearch
                         + "%' or Document like '%" + param.sSearch
                         + "%' or Customer like '%" + param.sSearch
                         + "%' or RevNo like '%" + param.sSearch
                         + "%' or Product like '%" + param.sSearch
                         + "%' or ProcessLicensor like '%" + param.sSearch
                         + "%' or Status like '%" + param.sSearch + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                #region Sorting
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstResult = db.SP_PMB_GET_HISTORY_HEADERDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(GetCodeAndNameByProject(uc.Project)),
                               Convert.ToString(uc.Document),
                               Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Project)),
                               // Convert.ToDateTime(uc.CDD).ToString("dd/MM/yyyy"),
                               //Convert.ToString(uc.CDD),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.SubmittedBy),
                               Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),
                               Convert.ToString(uc.ApprovedBy),
                               Convert.ToString(Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy")),

                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.Id)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        //[HttpPost]
        //public ActionResult GetHistoryView()
        //{
        //    PMB001_Log objPMB001 = new PMB001_Log();
        //    return PartialView("getHistoryPartial", objPMB001);
        //}

        [SessionExpireFilter]
        public ActionResult getHistoryDetail(int Id = 0)
        {
            PMB001_Log objPMB001 = new PMB001_Log();
            var user = objClsLoginInfo.UserName;
            var lstProjectDesc = (from a in db.COM001
                                  select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).ToList();
            ViewBag.Project = new SelectList(lstProjectDesc, "projectCode", "projectDescription");
            //ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments("PMB001/" + objPMB001.HeaderId + "/" + objPMB001.RevNo));
            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.lstyesno = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            List<string> lstProCat = clsImplementationEnum.getProductCategory().ToList();
            ViewBag.lstProCat = lstProCat.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            var approver = (from t1 in db.ATH001
                            join t2 in db.COM003 on t1.Employee equals t2.t_psno
                            where t2.t_actv == 1
                            select new { t1.Employee, Desc = t2.t_psno + " - " + t2.t_name }).Distinct().ToList();
            ViewBag.approver = new SelectList(approver, "Employee", "Desc");
            if (Id > 0)
            {
                var project = (from a in db.PMB001_Log
                               where a.Id == Id
                               select a.Project).FirstOrDefault();
                ViewBag.Customer = Manager.GetCustomerCodeAndNameByProject(project);
                objPMB001 = db.PMB001_Log.Where(x => x.Id == Id).FirstOrDefault();
                if (objPMB001.CreatedBy != objClsLoginInfo.UserName && objPMB001.ApprovedBy != objClsLoginInfo.UserName)
                {
                    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                }
            }
            return View(objPMB001);
        }
        [HttpPost]
        public ActionResult getHeaderId(string project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                project = project.Split('-')[0].Trim();
                int Headerid = (from a in db.PMB001
                                where a.Project == project
                                select a.HeaderId).FirstOrDefault();
                string status = (from a in db.PMB001
                                 where a.Project == project
                                 select a.Status).FirstOrDefault();
                objResponseMsg.Key = true;
                objResponseMsg.Value = Headerid.ToString() + "|" + status;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SavePMBHeader(PMB001 pmb001, FormCollection fc)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                string s1, s2;
                s1 = fc["txtdoc"].ToString();
                s2 = s1.Substring(0, 3);
                s2 = s1.Substring(s1.Length - 2);
                // bool d1;
                //if (fc["ddlJob"].ToString() == "Yes")
                //{
                //    d1 = true;
                //}
                //else
                //{
                //    d1 = false;
                //}

                if (!string.IsNullOrWhiteSpace(fc["hfHeaderId"].ToString()) && Convert.ToInt32(fc["hfHeaderId"].ToString()) > 0)
                {
                    int hid = Convert.ToInt32(fc["hfHeaderId"].ToString());
                    PMB001 objPMB001 = db.PMB001.Where(x => x.HeaderId == hid).FirstOrDefault();
                    string rev = Regex.Replace(fc["txtRev"].ToString(), "[^0-9]+", string.Empty);
                    objPMB001.Project = fc["hfProject"].ToString();

                    objPMB001.DocumentNo = Convert.ToInt32(s2);
                    objPMB001.Document = fc["txtdoc"].ToString();
                    objPMB001.Customer = fc["txtCust"].ToString().Split('-')[0];
                    if (objPMB001.Status == clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue())
                    {
                        objPMB001.RevNo = Convert.ToInt32(objPMB001.RevNo) + 1;
                        objPMB001.Status = clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue();
                    }
                    /*if (string.IsNullOrWhiteSpace(fc["txtCdd"].ToString()) || fc["txtCdd"].ToString() == "01-01-0001" || fc["txtCdd"].ToString() == "01/01/0001")
                    {
                        objPMB001.CDD = null;
                    }
                    else
                    {
                        objPMB001.CDD = Convert.ToDateTime(fc["txtCdd"].ToString());
                    }*/
                    objPMB001.Product = fc["ddlCat"].ToString();
                    objPMB001.ProcessLicensor = fc["txtlicence"].ToString();
                    //objPMB001.Attachment = "";
                    //objPMB001.JobDescription = fc["ddlJob"].ToString();
                    objPMB001.EditedBy = objClsLoginInfo.UserName;
                    objPMB001.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CTQMessages.Update.ToString();
                    objResponseMsg.Revision = "R" + objPMB001.RevNo.ToString();
                    objResponseMsg.status = objPMB001.Status;
                    Manager.UpdatePDN002(objPMB001.HeaderId, objPMB001.Status, objPMB001.RevNo, objPMB001.Project, objPMB001.Document);
                }
                else
                {
                    string project = fc["txtProject"].ToString();
                    string rev = Regex.Replace(fc["txtRev"].ToString(), "[^0-9]+", string.Empty);
                    PMB001 objPMB001 = new PMB001();
                    objPMB001.Project = fc["txtProject"].ToString();
                    objPMB001.DocumentNo = Convert.ToInt32(s2);
                    objPMB001.Document = fc["txtdoc"].ToString();
                    objPMB001.Customer = fc["txtCust"].ToString().Split('-')[0];
                    objPMB001.RevNo = Convert.ToInt32(rev);
                    objPMB001.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                    /*if (string.IsNullOrWhiteSpace(fc["txtCdd"].ToString()) || fc["txtCdd"].ToString() == "01-01-0001" || fc["txtCdd"].ToString() == "01/01/0001")
                    {
                        objPMB001.CDD = null;
                    }
                    else
                    {
                        objPMB001.CDD = Convert.ToDateTime(fc["txtCdd"].ToString());
                    }*/
                    objPMB001.Product = fc["ddlCat"].ToString();
                    objPMB001.ProcessLicensor = fc["txtlicence"].ToString();
                    //objPMB001.Attachment = "";
                    //objPMB001.JobDescription = fc["ddlJob"].ToString();
                    objPMB001.CreatedBy = objClsLoginInfo.UserName;
                    objPMB001.CreatedOn = DateTime.Now;
                    db.PMB001.Add(objPMB001);
                    db.SaveChanges();
                    Manager.UpdatePDN002(objPMB001.HeaderId, objPMB001.Status, objPMB001.RevNo, objPMB001.Project, objPMB001.Document);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CTQMessages.Insert.ToString();
                    objResponseMsg.Revision = "R" + objPMB001.RevNo.ToString();
                    objResponseMsg.status = objPMB001.Status;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReturnSelectedPMB(string strHeaderIds, string remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
                for (int i = 0; i < arrayHeaderIds.Length; i++)
                {
                    int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                    PMB001 objPMB001 = db.PMB001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    objPMB001.Status = clsImplementationEnum.CTQStatus.Returned.GetStringValue();
                    objPMB001.ApprovedBy = objClsLoginInfo.UserName;
                    objPMB001.ApprovedOn = DateTime.Now;
                    objPMB001.ReturnRemark = remarks;
                    db.SaveChanges();
                    Manager.UpdatePDN002(objPMB001.HeaderId, objPMB001.Status, objPMB001.RevNo, objPMB001.Project, objPMB001.Document);

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(),
                                                        objPMB001.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objPMB001.Project, clsImplementationEnum.PlanList.Pre_Manufacturing.GetStringValue(), objPMB001.RevNo.Value.ToString(), objPMB001.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Pre_Manufacturing.GetStringValue(), objPMB001.HeaderId.ToString(), false),
                                                        objPMB001.CreatedBy);
                    #endregion
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.QCPMessages.Return.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult getCodeValue(string project, string approver)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {

                objResponseMsg.projdesc = db.COM001.Where(i => i.t_cprj == project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objResponseMsg.appdesc = db.COM003.Where(i => i.t_psno == approver && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        //public ActionResult SaveHeader(PMB001 pmb001, bool hasAttachments, Dictionary<string, string> Attach)
        public ActionResult SaveHeader(PMB001 pmb001)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                string s1, s2;
                s1 = pmb001.Document.ToString();
                s2 = s1.Substring(0, 3);
                s2 = s1.Substring(s1.Length - 2);

                int NewHeaderId = 0;
                if (pmb001.HeaderId > 0)
                {
                    PMB001 objPMB001 = db.PMB001.Where(x => x.HeaderId == pmb001.HeaderId).FirstOrDefault();
                    if (objPMB001 != null)
                    {
                        objPMB001.Product = pmb001.Product;
                        //objPMB001.JobDescription = pmb001.JobDescription;
                        objPMB001.ProcessLicensor = pmb001.ProcessLicensor;
                        objPMB001.ApprovedBy = pmb001.ApprovedBy.Split('-')[0].ToString().Trim();
                        objPMB001.EditedBy = objClsLoginInfo.UserName;
                        objPMB001.EditedOn = DateTime.Now;
                        if (objPMB001.Status == clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue())
                        {
                            objPMB001.RevNo = Convert.ToInt32(objPMB001.RevNo) + 1;
                            objPMB001.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                        }
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.status = objPMB001.Status;
                        objResponseMsg.Value = clsImplementationMessage.PlanMessages.Update.ToString();
                        objResponseMsg.HeaderId = objPMB001.HeaderId;
                        objResponseMsg.Revision = "R" + objPMB001.RevNo.ToString();
                        Manager.UpdatePDN002(objPMB001.HeaderId, objPMB001.Status, objPMB001.RevNo, objPMB001.Project, objPMB001.Document);
                        //if (hasAttachments)
                        //{
                        var folderPath = "PMB001/" + objPMB001.HeaderId + "/R" + objPMB001.RevNo;

                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Process Details not available.";
                    }
                }
                else
                {
                    PMB001 objPMB001 = db.PMB001.Add(new PMB001
                    {
                        Project = pmb001.Project.Split('-')[0].ToString().Trim(),
                        DocumentNo = Convert.ToInt32(s2),
                        Document = pmb001.Document,
                        Customer = pmb001.Customer.Split('-')[0].ToString().Trim(),
                        RevNo = 0,
                        Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                        //CDD = Helper.ToNullIfTooEarlyForDb(pmb001.CDD),
                        Product = pmb001.Product,
                        ProcessLicensor = pmb001.ProcessLicensor,
                        //JobDescription = pmb001.JobDescription,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now,
                        ApprovedBy = pmb001.ApprovedBy.Split('-')[0].ToString().Trim()
                    });
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.status = objPMB001.Status;
                    objResponseMsg.Value = clsImplementationMessage.PlanMessages.Insert.ToString();
                    objResponseMsg.HeaderId = objPMB001.HeaderId;
                    objResponseMsg.Revision = "R" + objPMB001.RevNo.ToString();
                    Manager.UpdatePDN002(objPMB001.HeaderId, objPMB001.Status, objPMB001.RevNo, objPMB001.Project, objPMB001.Document);
                    //if (hasAttachments)
                    //{
                    var folderPath = "PMB001/" + NewHeaderId + "/R" + objPMB001.RevNo;

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RevokeDocument(int HeaderId, int pdinId, string Remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (Manager.IsRevokeApplicable(pdinId, HeaderId, clsImplementationEnum.PlanList.Pre_Manufacturing.GetStringValue()))
                {
                    objResponseMsg.Key = Manager.RevokePDINDocument(HeaderId, clsImplementationEnum.PlanList.Pre_Manufacturing, Remarks);
                    if (objResponseMsg.Key)
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revoke;
                    }
                    else
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.UnSuccess;
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.pdnRevokeMessage;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #region Export Excell
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_PMB_GET_HEADERDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      Document = li.Document,
                                      Customer = li.Customer,
                                      Product = li.Product,
                                      ProcessLicensor = li.ProcessLicensor,
                                      Status = li.Status,
                                      RevNo = "R" + li.RevNo,
                                      SubmittedBy = li.SubmittedBy,
                                      SubmittedOn = li.SubmittedOn,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {
                    var lst = db.SP_PMB_GET_HISTORY_HEADERDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      Document = li.Document,
                                      Customer = li.Customer,
                                      Product = li.Product,
                                      ProcessLicensor = li.ProcessLicensor,
                                      Status = li.Status,
                                      RevNo = "R" + li.RevNo,
                                      SubmittedBy = li.SubmittedBy,
                                      SubmittedOn = li.SubmittedOn,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}