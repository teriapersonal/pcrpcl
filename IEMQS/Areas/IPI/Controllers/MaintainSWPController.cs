﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.IPI.Controllers
{
    public class MaintainSWPController : clsBase
    {
        #region Utility

        //public string GenerateAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onKeyPressMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false)
        //{
        //    string strAutoComplete = string.Empty;

        //    string inputID = columnName + "" + rowId.ToString();
        //    string hdElementId = hdElement + "" + rowId.ToString();
        //    string inputName = columnName;
        //    string inputValue = columnValue;
        //    string className = "autocomplete form-control";
        //    string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
        //    string onKeypressEvent = !string.IsNullOrEmpty(onKeyPressMethod) ? "onkeypress='" + onKeyPressMethod + "'" : "";

        //    strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' hdElement='" + hdElement + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onKeypressEvent + " " + (disabled ? "disabled" : "") + " />";

        //    return strAutoComplete;
        //}

        public static string GenerateTextboxFor(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string maxLength = "", bool disabled = false)
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control";

            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onChange='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            htmlControl = "<input type='text' " + (isReadOnly ? "disabled='disabled'" : "") + " id='" + inputID + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + " maxlength='" + maxLength + " " + (disabled ? "disabled" : "") + "'  />";

            return htmlControl;
        }
        //public string GenerateAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false)
        //{
        //    string strAutoComplete = string.Empty;

        //    string inputID = columnName + "" + rowId.ToString();
        //    string hdElementId = hdElement + "" + rowId.ToString();
        //    string inputName = columnName;
        //    string inputValue = columnValue;
        //    string className = "autocomplete form-control";
        //    string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur=" + onBlurMethod + "" : "";
        //    string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick=" + onClickMethod + "" : "";

        //    strAutoComplete = "<input type=\"text\" " + (isReadOnly ? "readonly=\"readonly\"" : "") + " id=\"" + inputID + "\" data-headerid=\"" + rowId + "\" hdElement=\"" + hdElement + "\" value=\"" + inputValue + "\" colname=\"" + columnName + "\" name=\"" + inputID + "\" class=\"" + className + "\" style=\"" + inputStyle + "\"  " + onBlurEvent + " " + onClickEvent + " " + (disabled ? "disabled" : "") + " />";

        //    return strAutoComplete;
        //}
        public string GenerateAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false)
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur=" + onBlurMethod + "" : "";
            // string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick=" + onClickMethod + "" : "";

            strAutoComplete = "<input type=\"text\" " + (isReadOnly ? "disabled=\"disabled\"" : "") + " id=\"" + inputID + "\" data-headerid=\"" + rowId + "\" hdElement=\"" + hdElement + "\" value=\"" + inputValue + "\" oldvalue=\"" + inputValue + "\" colname=\"" + columnName + "\" name=\"" + inputID + "\" class=\"" + className + "\" style=\"" + inputStyle + "\"  " + onBlurEvent + "  " + (disabled ? "disabled" : "") + " />";

            return strAutoComplete;
        }
        public string GeneralNotes(int rowId, string status, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, bool IsSeamOffered = false)
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control " + (IsSeamOffered ? "clsseamoffered" : "");
            inputStyle = "min-width:150px!important;";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur=" + onBlurMethod + "" : "";
            // string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick=" + onClickMethod + "" : "";

            strAutoComplete = "<div class=\"input-icon right\">"
                                 + "<i id=\"aGeneralNotesView" + inputID + "\" class=\"iconspace fa fa-search " + (isReadOnly ? "disabledvalue" : "") + (IsSeamOffered ? "clsseamoffered" : "") + "\" style=\"cursor:pointer;color: #337ab7;\" title=\"General Notes\" onclick=\"OpenKeyDetails(" + rowId + ",'" + status + "'," + (isReadOnly ? "true" : "false") + ")\"></i>"
                                 //+ "<input type=\"text\" " + "readonly=\"readonly\"" + " id=\"" + inputID + "\" data-headerid=\"" + rowId + "\" hdElement=\"" + hdElement + "\" value=\"" + inputValue + "\" oldvalue=\"" + inputValue + "\" colname=\"" + columnName
                                 + "<input type=\"text\" " + "disabled=\"disabled\"" + " id=\"" + inputID + "\" data-headerid=\"" + rowId + "\" hdElement=\"" + hdElement + "\" value=\"" + inputValue + "\" oldvalue=\"" + inputValue + "\" colname=\"" + columnName
                                 + "\" name=\"" + inputID + "\" class=\"" + className + "\" style=\"" + inputStyle + "\"  " + onBlurEvent + "  " + (disabled ? "disabled" : "") + " />"
                                 + "</div>";

            //strAutoComplete = "<input type=\"text\" " + "disabled=\"disabled\""  + " id=\"" + inputID + "\" data-headerid=\"" + rowId + "\" hdElement=\"" + hdElement + "\" value=\"" + inputValue + "\" oldvalue=\"" + inputValue + "\" colname=\"" + columnName 
            //                        + "\" name=\"" + inputID + "\" class=\"" + className + "\" style=\"" + inputStyle + "\"  " + onBlurEvent + "  " + (disabled ? "disabled" : "") + " />"+
            //                  "<a id=\"aGeneralNotesView" + inputID + "\" " + "onclick='OpenKeyDetails(" + rowId + ")'" + "><i class='iconspace fa fa-search " + (isReadOnly ? "disabledicon" : "") + "' style='float:left;margin-top:8px;font-size:20px;'></i></a>";
            //"<a id=\"aGeneralNotesView" + inputID + "\" " + (!isReadOnly ? "onclick='OpenKeyDetails(" + rowId + ")'" : "") + "><i class='iconspace fa fa-search "+(isReadOnly ? "disabledicon":"")+"' style='float:left;margin-top:8px;font-size:20px;'></i></a>";

            return strAutoComplete;
        }
        public string HTMLAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string maxlength = "", string validate = "")
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string FieldValidate = !string.IsNullOrEmpty(validate) ? "validate='" + validate + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onchange='" + onBlurMethod + "'" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "disabled='disabled'" : "") + " id='" + inputID + "' hdElement='" + hdElementId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + (disabled ? "disabled" : "") + " " + MaxCharcters + " " + FieldValidate + " />";

            return strAutoComplete;
        }
        [NonAction]
        public static string GenerateTextbox(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onKeyPressMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string maxlength = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control";
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onchange=\"" + onBlurMethod + "\"" : "";
            string onKeypressEvent = !string.IsNullOrEmpty(onKeyPressMethod) ? "onkeypress='" + onKeyPressMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            //htmlControl = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' " + MaxCharcters + " value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + "  " + onKeypressEvent + " "+ onClickEvent +" spara='" + rowId + "' />";
            htmlControl = "<input type=\"text\" " + (isReadOnly ? "disabled=\"disabled\"" : "") + " id=\"" + inputID + "\" " + MaxCharcters + " value=\"" + inputValue + "\" oldvalue=\"" + inputValue + "\" colname=\"" + columnName + "\" name=\"" + inputID + "\" class=\"" + className + "\" style=\"" + inputStyle + "\"  " + onBlurEvent + "  " + onKeypressEvent + " " + onClickEvent + " spara=\"" + rowId + "\" />";

            return htmlControl;
        }
        [NonAction]
        public static string GenerateHTMLTextbox(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", bool disabled = false, string maxlength = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control";
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength=\"" + maxlength + "\"" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur=\"" + onBlurMethod + "\"" : "";

            htmlControl = "<input type=\"text\" " + (isReadOnly ? "disabled=\"disabled\"" : "") + " id=\"" + inputID + "\" " + MaxCharcters + " value=\"" + inputValue + "\" oldvalue=\"" + inputValue + "\" colname=\"" + columnName + "\" name=\"" + inputID + "\" class=\"" + className + "\" style=\"" + inputStyle + "\"  " + onBlurEvent + " " + (disabled ? "disabled" : "") + " />";

            return htmlControl;
        }
        public static string MultiSelectDropdown(int rowId, List<SelectItemList> list, string selectedValue, bool Disabled = false, string OnChangeEvent = "", string ColumnName = "")
        {
            string multipleSelect = string.Empty;
            string[] arrayVal = { };

            try
            {
                if (!string.IsNullOrWhiteSpace(selectedValue))
                {
                    arrayVal = selectedValue.Split(',');
                }

                string inputID = ColumnName + "" + rowId.ToString();
                multipleSelect += "<select name=\"ddlmultiple\" id=\"" + inputID + "\" multiple=\"multiple\"  style=\"width: 100 % \" colname=\"" + ColumnName + "\" class=\"form-control\" " + (Disabled ? "disabled" : "") + (OnChangeEvent != string.Empty ? " onchange=" + OnChangeEvent + "" : "") + " >";
                foreach (var item in list)
                {
                    multipleSelect += "<option value=\"" + item.id + "\" " + ((arrayVal.Contains(item.id)) ? " selected" : "") + " >" + item.text + "</option>";
                }
                multipleSelect += "</select>";
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return multipleSelect;
        }
        #endregion

        [SessionExpireFilter]
        // GET: IPI/MaintainSWP
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult getNotes(string QP)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            List<clsTask> list = new List<clsTask>();
            list = (from a in db.SWP002
                    where a.QualityProject == QP
                    select new clsTask
                    {
                        id = a.NoteNumber.ToString(),
                        text = a.NoteDescription
                    }).ToList();

            objResponseMsg.lsttask = list;
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [SessionExpireFilter]
        public ActionResult SWPDetail(int HeaderId = 0)
        {
            SWP010 objSWPS010 = new SWP010();
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            var locaDescription = (from a in db.COM002
                                   where a.t_dimx == objClsLoginInfo.Location
                                   select a.t_desc).FirstOrDefault();

            ViewBag.Location = objClsLoginInfo.Location + "-" + locaDescription;

            var JointType = (from g2 in db.GLB002
                             join g1 in db.GLB001 on g2.Category equals g1.Id
                             where g1.Category == "Joint Type"
                             select g2.Code).Distinct().ToList();
            ViewBag.JointType = JointType;
            ViewBag.newJointType = JointType.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();
            ViewBag.headerid = HeaderId;

            var lstDhtIsr = clsImplementationEnum.GetDHTISR();
            ViewBag.dhtisr = lstDhtIsr.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

            var QP = (from a in db.SWP010
                      where a.HeaderId == HeaderId
                      select a.QualityProject).FirstOrDefault();



            var MainWPS = (from a in db.WPS012_Log
                           where a.QualityProject == QP
                           select a.WPSNumber).ToList();

            ViewBag.MainWPS = MainWPS;
            ViewBag.newMainWPS = MainWPS.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();
            if (HeaderId > 0)
            {
                objSWPS010 = db.SWP010.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                ViewBag.BU = objSWPS010.BU;
                ViewBag.Location = objSWPS010.Location + "-" + (from a in db.COM002
                                                                where a.t_dimx == objSWPS010.Location
                                                                select a.t_desc).FirstOrDefault();
                //List<clsTask> list = new List<clsTask>();
                //list = (from a in db.SWP002
                //        where a.QualityProject == QP
                //        select new clsTask
                //        {
                //            id = a.NoteNumber.ToString(),
                //            text = a.NoteDescription
                //        }).ToList();
                //ViewBag.swpNotes = list;

            }
            //return View(objSWPS010);
            return PartialView("SWPDetail", objSWPS010);
        }
        [HttpPost]
        public ActionResult getParameterSlip(string headerid)
        {
            SWP010 objSWP010 = new SWP010();
            int hd = Convert.ToInt32(headerid);
            ViewBag.headerid = hd;

            if (hd > 0)
            {
                objSWP010 = db.SWP010.Where(x => x.HeaderId == hd).FirstOrDefault();
                var locaDescription = (from a in db.COM002
                                       where a.t_dimx == objSWP010.Location
                                       select a.t_desc).FirstOrDefault();
                ViewBag.Location = objSWP010.Location + "-" + locaDescription;
            }
            return PartialView("_ParameterSlip", objSWP010);
        }

        [HttpPost]
        public ActionResult GetHistoryView(int headerId = 0, string SeamNo = "", int tpheaderId = 0)
        {
            SWP010_Log objSWP010 = db.SWP010_Log.Where(i => i.HeaderId == headerId && i.SeamNo == SeamNo).FirstOrDefault();
            TempData["id"] = objSWP010.Id; //ViewBag.SWPHeaderId = headerId;
            Session["id"] = objSWP010.Id;  //ViewBag.TPHeaderId = headerId; //ViewBag.TPHeaderId = tpheaderId;
            return PartialView("_SWPHistoryPartial", objSWP010);
            //ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            //var locaDescription = (from a in db.COM002
            //                       where a.t_dimx == objClsLoginInfo.Location
            //                       select a.t_desc).FirstOrDefault();
            //ViewBag.Location = objClsLoginInfo.Location + "-" + locaDescription;
            //if (Id > 0)
            //{
            //    objSWPS010 = db.SWP010.Where(x => x.HeaderId == Id).FirstOrDefault();
            //    var JointType = (from g2 in db.GLB002
            //                     join g1 in db.GLB001 on g2.Category equals g1.Id
            //                     where g1.Category == "Joint Type"
            //                     select g2.Code).Distinct().ToList();
            //    ViewBag.JointType = JointType;

            //    var QP = (from a in db.SWP010
            //              where a.HeaderId == Id
            //              select a.QualityProject).FirstOrDefault();

            //    var MainWPS = (from a in db.WPS012_Log
            //                   where a.QualityProject == QP
            //                   select a.WPSNumber).ToList();

            //    ViewBag.MainWPS = MainWPS;
            //}
        }
        [SessionExpireFilter]
        public ActionResult SWPHistoryDetail(int Id = 0)
        {

            SWP010_Log objSWP010_Log = new SWP010_Log();
            // Id = Convert.ToInt32(TempData["id"]);

            //objSWP010_Log = db.SWP010_Log.Where(x => x.HeaderId == HeaderId && x.Id== lineid).FirstOrDefault();
            objSWP010_Log = db.SWP010_Log.Where(x => x.Id == Id).FirstOrDefault();

            var user = objClsLoginInfo.UserName;
            if (objSWP010_Log != null)
            {
                ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
                var locaDescription = (from a in db.COM002
                                       where a.t_dimx == objClsLoginInfo.Location
                                       select a.t_desc).FirstOrDefault();
                ViewBag.Location = objClsLoginInfo.Location + "-" + locaDescription;
                if (Id > 0)
                {
                    objSWP010_Log = db.SWP010_Log.Where(x => x.Id == Id).FirstOrDefault();
                    var JointType = (from g2 in db.GLB002
                                     join g1 in db.GLB001 on g2.Category equals g1.Id
                                     where g1.Category == "Joint Type"
                                     select g2.Code).Distinct().ToList();
                    ViewBag.JointType = JointType;

                    var QP = objSWP010_Log.QualityProject;

                    var MainWPS = (from a in db.WPS012_Log
                                   where a.QualityProject == QP
                                   select a.WPSNumber).ToList();

                    ViewBag.MainWPS = MainWPS;
                    ViewBag.Headerid = objSWP010_Log.HeaderId;
                    ViewBag.QualityProject = objSWP010_Log.QualityProject;
                    ViewBag.SWPNotes = objSWP010_Log.SWPNotes;
                    ViewBag.RevNo = objSWP010_Log.RevNo;
                    ViewBag.Status = objSWP010_Log.Status;
                    ViewBag.Project = objSWP010_Log.Project;
                    ViewBag.ApprovedBy = objSWP010_Log.ApprovedBy;
                    ViewBag.BU = objSWP010_Log.BU;
                    ViewBag.jointype = objSWP010_Log.JointType;
                }
            }
            return View(objSWP010_Log);
            // return View();
        }

        [HttpPost]
        public ActionResult GetHeaderGridDataPartial(int HeaderId, string status)
        {

            ViewBag.Status = status;


            var lstDhtIsr = clsImplementationEnum.GetDHTISR();
            ViewBag.dhtisr = lstDhtIsr.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

            var swp = db.SWP010.Where(m => m.HeaderId == HeaderId).FirstOrDefault();
            var QP = swp.QualityProject;
            var JointType = (from g2 in db.GLB002
                             join g1 in db.GLB001 on g2.Category equals g1.Id
                             where g1.Category == "Joint Type" && g2.BU.Equals(swp.BU, StringComparison.OrdinalIgnoreCase) && g2.IsActive == true && swp.Location.Equals(g2.Location)
                             select g2.Code).Distinct().ToList();

            ViewBag.newJointType = JointType.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

            //var MainWPS = (from a in db.WPS012_Log
            //               where a.QualityProject == QP
            //               select new
            //               {
            //                   a.WPSNumber,
            //                   a.WPSRevNo
            //               }).ToList();
            var Status = clsImplementationEnum.UTCTQStatus.Approved.GetStringValue();
            var MainWPS = (from a in db.WPS012
                           where (a.QualityProject == QP || (from b in db.WPS013 where b.QualityProject == QP select b.IQualityProject).ToList().Contains(a.QualityProject))
                           && a.Status.Equals(Status)
                           select new
                           {
                               a.WPSNumber,
                               a.WPSRevNo
                           }).ToList();

            MainWPS.Select(x => x.WPSNumber).Distinct();
            ViewBag.newMainWPS = MainWPS.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.WPSNumber, CatID = x.WPSRevNo + "-" + x.WPSNumber }).ToList();
            ViewBag.newAlternate1 = MainWPS.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.WPSNumber, CatID = x.WPSRevNo + "-" + x.WPSNumber }).ToList();
            ViewBag.newAlternate2 = MainWPS.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.WPSNumber, CatID = x.WPSRevNo + "-" + x.WPSNumber }).ToList();
            ViewBag.headerid = HeaderId;
            //ViewBag.Project = swp.Project;
            ViewBag.DefaultSearch = Convert.ToString(Session["TP_SWP_Search"]); //ViewBag.DefaultSearch = "";// Convert.ToString(Session["TP_SWP_Search"]);

            if (!string.IsNullOrEmpty(Convert.ToString(Session["TP_SWP_PageSize"])))
            {
                ViewBag.DefaultPageSize = Convert.ToString(Session["TP_SWP_PageSize"]);
            }
            else
            {
                //ViewBag.DefaultPageSize = "50";
                ViewBag.DefaultPageSize = "10";
            }

            List<string> lstTP_SWPSeams = (List<string>)Session["TP_SWP_SeamList"];
            if (lstTP_SWPSeams != null && lstTP_SWPSeams.Count > 0)
            {
                ViewBag.lstTP_SWPSeams = String.Join(",", lstTP_SWPSeams);
            }
            else
            {
                ViewBag.lstTP_SWPSeams = string.Empty;
            }
            ViewBag.hdnBU = swp.BU;
            ViewBag.hdnLocation = swp.Location;
            //string approvedString = clsImplementationEnum.TestPlanStatus.Approved.GetStringValue();
            //List<string> lstQproj = new List<string>();
            //lstQproj.Add(QP);
            //lstQproj.AddRange(db.QMS017.Where(c => c.BU == swp.BU && c.Location == swp.Location && c.IQualityProject == QP).Select(x => x.QualityProject).ToList());

            //var lstQualityIds1 = (from a in db.QMS015_Log
            //                      where lstQproj.Contains(a.QualityProject) && (a.QualityIdApplicableFor == "Seam") && a.Status == approvedString && a.Location == swp.Location
            //                      select new CategoryData { Value = a.QualityId, Code = a.QualityId, CategoryDescription = a.QualityId + " - " + a.QualityIdDesc, Date = a.ApprovedOn }).Distinct().OrderByDescending(o => o.Date).ToList();


            //ViewBag.QualityID = lstQualityIds1;
            //ViewBag.status = swp.Status;
            return PartialView("_SWPGridPartial");
            //return PartialView("_HTMLGridPartial");
        }
        public JsonResult getWPSNumber(int HeaderId)
        {
            var swp = db.SWP010.Where(m => m.HeaderId == HeaderId).FirstOrDefault();
            var Status = clsImplementationEnum.UTCTQStatus.Approved.GetStringValue();
            var MainWPS = (from a in db.WPS012
                           where (a.QualityProject == swp.QualityProject || (from b in db.WPS013 where b.QualityProject == swp.QualityProject && b.Location == swp.Location select b.IQualityProject).ToList().Contains(a.QualityProject) || (from b in db.WPS013 where b.IQualityProject == swp.QualityProject && b.Location == swp.Location select b.QualityProject).ToList().Contains(a.QualityProject))
                           && a.Status.Equals(Status)
                           && a.Jointtype.Equals(swp.JointType)
                           && a.Location.Equals(swp.Location)

                           select new
                           {
                               a.WPSNumber,
                               a.WPSRevNo
                           }).ToList();
            MainWPS.Select(x => x.WPSNumber).Distinct();
            return Json(MainWPS.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.WPSNumber, CatID = x.WPSRevNo + "|" + x.WPSNumber }).ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult QualityProjectWiseGridPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_QualityProjectWiseGridPartial");
        }

        [HttpPost]
        public JsonResult LoadQualityProjectWiseHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";

                //if (param.CTQCompileStatus.ToUpper() == "PENDING")
                //{
                //    whereCondition += "1=1 and status in('" + clsImplementationEnum.CTQStatus.DRAFT.GetStringValue() + "','" + clsImplementationEnum.CTQStatus.Returned.GetStringValue() + "')";
                //}
                //else
                //{
                //    whereCondition += "1=1";
                //}

                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "swp10.BU", "swp10.Location");

                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or swp10.QualityProject like '%" + param.sSearch + "%'or (swp10.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%')";
                }
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                //  var lstHeader = db.SP_IPI_GetQualityIDHeader(startIndex, endIndex, "", whereCondition).ToList();
                var lstResult = db.SP_SWP_Get_QualityProjectwise_HeaderList(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.QualityProject),
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.BU),
                               Convert.ToString(uc.Location),
                                "<center><a class='' href='" + WebsiteURL + "/IPI/MaintainSWP/SWPDetail?HeaderId="+Convert.ToInt32(uc.HeaderId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a></center>"
                           }).ToList();
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);

                //int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                //var res = from a in lstHeader
                //          select new[] {
                //        a.QualityProject,
                //        a.Project,
                //        a.BU,
                //        a.Location,
                //        "<center><a class='btn btn-xs green' href='/IPI/MaintainSWP/SWPDetail?Id="+Convert.ToInt32(a.HeaderId)+"'>View<i style='margin-left:5px;' class='fa fa-eye'></i></a></center>",
                //    };
                //return Json(new
                //{
                //    sEcho = param.sEcho,
                //    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                //    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                //    aaData = res,
                //}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult LoadSWPHeaderHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                string strWhere = "1=1";
                //int id =Convert.ToInt32(TempData["id"]);
                //var seamno = db.SWP010_Log.Where(m => m.HeaderId == HeaderId && m.Id==id).FirstOrDefault();

                //strWhere += " and CreatedBy=" + user;
                strWhere += " and HeaderId=" + HeaderId;// + "and (SeamNo = '" + seamno.SeamNo+"')";
                //+ " or ApprovedBy = " + user;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (Project like '%" + param.sSearch
                       + "%' or QualityProject like '%" + param.sSearch
                       + "%' or BU like '%" + param.sSearch
                       + "%' or RevNo like '%" + param.sSearch
                       + "%' or Location like '%" + param.sSearch
                       + "%' or Status like '%" + param.sSearch + "%')";
                }

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_SWP_GET_HISTORY_HEADERDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.QualityProject),
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.BU),
                               Convert.ToString(uc.Location),
                                Convert.ToString("R"+uc.RevNo),
                                 Convert.ToString(uc.Status),
                               "<center><a class='' target='_blank' href='" + WebsiteURL + "/IPI/MaintainSWP/SWPHistoryDetail?Id="+(uc.Id)+"'><i style='' class='fa fa-eye'></i></a>"+""+"<a class='' href='javascript:void(0)' onclick=ShowTimeline('/IPI/MaintainSWP/ShowLogTimeline?id="+Convert.ToInt32(uc.Id)+"');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a></center>",
                               Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.Id),

                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]

        #region CreatedBy Ajay Chauhan
        public JsonResult LoadSWPHeaderData(JQueryDataTableParamModel param, string qualityProj, string BU, string Location)
        {
            try
            {

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                var IsDisplayOnly = !string.IsNullOrEmpty(Request["IsDisplayOnly"]) ? Convert.ToBoolean(Request["IsDisplayOnly"].ToString()) : false;

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.Location;
                string location = Location.Split('-')[0];
                //List<SelectItemList> list = new List<SelectItemList>();
                List<SelectItemList> list = new List<SelectItemList>();
                //list = (from a in db.SWP002
                //        where a.QualityProject == qualityProj
                //        select new SelectItemList
                //        {
                //            id = a.NoteNumber.ToString(),
                //            text = a.NoteNumber.ToString() + "-" + a.NoteDescription.ToString()
                //        }).ToList();

                list = Manager.GetGeneralNotes(qualityProj, "", location).Select(i => new SelectItemList { id = i.NoteNumber.ToString(), text = i.NoteNumber.ToString() + "-" + i.NoteDescription.ToString() }).Distinct().ToList();

                string strWhere = "1=1";

                if (!string.IsNullOrEmpty(qualityProj) && !string.IsNullOrEmpty(Location))
                {
                    strWhere += " AND UPPER(s10.QualityProject) = '" + qualityProj.ToUpper() + "' AND UPPER(s10.Location) = '" + location.ToUpper() + "'";
                }
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "and 1=1 and s10.status in('" + clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.QualityIdHeaderStatus.Returned.GetStringValue() + "')";
                }
                var strApproved = clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue();
                string[] arrayLikeCon = {
                                                "s10.SeamNo",
                                                "s10.SeamDescription",
                                                //"selectedJointType",
                                                "s10.JointType",
                                                "MainWPSNumber",
                                                "AlternateWPS1",
                                                "AlternateWPS2",
                                                "MainWPSRevNo",
                                                "AlternateWPS1RevNo",
                                                "AlternateWPS2RevNo",
                                                "Preheat",
                                                "DHTISR",
                                                "SWPNotes",
                                                "s10.Status"
                    };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                Session["TP_SWP_Search"] = param.sSearch;
                Session["TP_SWP_PageSize"] = Convert.ToString(param.iDisplayLength);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_SWP_Get_HeaderList
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere, BU, user
                                ).ToList();
                //var listSP_GET_ALL_PRE_HEAT_STAGE_SEAM_OFFERED = db.SP_GET_ALL_PRE_HEAT_STAGE_SEAM_OFFERED(qualityProj, BU, Location).ToList();
                //var listSP_SWP_GET_TOKEN_GENERATED_WPS_NUMBER = db.SP_SWP_GET_TOKEN_GENERATED_WPS_NUMBER(qualityProj, BU, Location).ToList();

                var listSP_SWP_Get_HeaderList_Ent = new List<SP_SWP_Get_HeaderList_Ent>();
                lstResult.ForEach(i =>
                {
                    //bool SeamOffered = false;
                    //IsSeamOfferedCheckEnt objIsSeamOfferedCheckEnt = new IsSeamOfferedCheckEnt();
                    if (!IsDisplayOnly)
                    {
                        //SeamOffered = IsSeamOffered(i.HeaderId);
                        //objIsSeamOfferedCheckEnt = CheckSeamIsOffered(listSP_GET_ALL_PRE_HEAT_STAGE_SEAM_OFFERED, listSP_SWP_GET_TOKEN_GENERATED_WPS_NUMBER, i, BU);
                    }
                    listSP_SWP_Get_HeaderList_Ent.Add(new SP_SWP_Get_HeaderList_Ent
                    {
                        AlternateWPS1 = i.AlternateWPS1,
                        AlternateWPS1RevNo = i.AlternateWPS1RevNo,
                        AlternateWPS2 = i.AlternateWPS2,
                        AlternateWPS2RevNo = i.AlternateWPS2RevNo,
                        DHTISR = i.DHTISR,
                        HeaderId = i.HeaderId,
                        //IsSeamOffered = SeamOffered,
                        JointType = i.JointType,
                        Location = i.Location,
                        MainWPSNumber = i.MainWPSNumber,
                        MainWPSRevNo = i.MainWPSRevNo,
                        Preheat = i.Preheat,
                        QualityProject = i.QualityProject,
                        Returnremarks = i.Returnremarks,
                        RevNo = i.RevNo,
                        ROW_NO = i.ROW_NO,
                        SeamDescription = i.SeamDescription,
                        SeamNo = i.SeamNo,
                        SeamRev = i.SeamRev,
                        selectedJointType = i.selectedJointType,
                        Status = i.Status,
                        SWPNotes = i.SWPNotes,
                        TotalCount = i.TotalCount,
                        WPSNumbers1 = i.WPSNumbers1,
                        WPSNumbers2 = i.WPSNumbers2,
                        WPSNumbers3 = i.WPSNumbers3,
                        //MainWPSDisabled = (objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered || objIsSeamOfferedCheckEnt.WPSNo1TokenGenerate),
                        //AlternativeWPS1Disabled = (objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered || objIsSeamOfferedCheckEnt.WPSNo2TokenGenerate),
                        //AlternativeWPS2Disabled = (objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered || objIsSeamOfferedCheckEnt.WPSNo3TokenGenerate),
                        //PWHTAndNotesDisabled = (objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered || objIsSeamOfferedCheckEnt.WeldVisualStageOffered),
                        //JointTypeDisabled = (objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered || objIsSeamOfferedCheckEnt.WPSNo1TokenGenerate || objIsSeamOfferedCheckEnt.WPSNo2TokenGenerate || objIsSeamOfferedCheckEnt.WPSNo3TokenGenerate),
                        //IsAllPreHeatStageOffered = objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered,
                        //WPSNo1TokenGenerate = objIsSeamOfferedCheckEnt.WPSNo1TokenGenerate,
                        //WPSNo2TokenGenerate = objIsSeamOfferedCheckEnt.WPSNo2TokenGenerate,
                        //WPSNo3TokenGenerate = objIsSeamOfferedCheckEnt.WPSNo3TokenGenerate,
                        //WeldVisualStageOffered = objIsSeamOfferedCheckEnt.WeldVisualStageOffered
                    });
                });
                var data = (from uc in listSP_SWP_Get_HeaderList_Ent
                            select new[]
                           {
                                Convert.ToString(uc.HeaderId),
                                uc.SeamNo,
                                uc.selectedJointType, //(IsDisplayOnly? uc.selectedJointType: (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue().ToLower() || uc.JointTypeDisabled)? GenerateAutoComplete(uc.HeaderId, "JointType",uc.selectedJointType,"\"Updateswp(this,"+uc.HeaderId+",'"+uc.Status+"');\"", true,"","JointType")+""+Helper.GenerateHidden(uc.HeaderId, "hdnJointType") : GenerateAutoComplete(uc.HeaderId, "JointType",uc.selectedJointType,"\"Updateswp(this,"+uc.HeaderId+",'"+uc.Status+"');\"", false,"","JointType", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnJointType")),
                                (IsDisplayOnly?GetWPSHistoryLink(uc.MainWPSNumber,uc.MainWPSRevNo): uc.MainWPSNumber),//uc.MainWPSNumber, //(IsDisplayOnly? uc.MainWPSNumber: (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue().ToLower() || uc.MainWPSDisabled || String.IsNullOrEmpty(uc.selectedJointType))? GenerateAutoComplete(uc.HeaderId, "MainWPSNumber",uc.MainWPSNumber,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", true,"","MainWPSNumber")+""+Helper.GenerateHidden(uc.HeaderId, "hdnMainWPSNumber") : GenerateAutoComplete(uc.HeaderId, "MainWPSNumber",uc.MainWPSNumber,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"",  false,"","MainWPSNumber", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnMainWPSNumber")),
                                uc.MainWPSRevNo.ToString(), //(IsDisplayOnly ? uc.MainWPSRevNo.ToString(): GenerateHTMLTextbox(uc.HeaderId,"MainWPSRevNo",uc.MainWPSRevNo.ToString(),"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');", true,"", false)),
                                (IsDisplayOnly?GetWPSHistoryLink(uc.AlternateWPS1,uc.AlternateWPS1RevNo): uc.AlternateWPS1),//uc.AlternateWPS1, //(IsDisplayOnly? uc.AlternateWPS1: (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue().ToLower()|| uc.AlternativeWPS1Disabled || String.IsNullOrEmpty(uc.MainWPSNumber))? GenerateAutoComplete(uc.HeaderId, "AlternateWPS1",uc.AlternateWPS1,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", true,"","AlternateWPS1", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnAlternateWPS1"):GenerateAutoComplete(uc.HeaderId, "AlternateWPS1",uc.AlternateWPS1,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", false,"","AlternateWPS1", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnAlternateWPS1")),
                                uc.AlternateWPS1RevNo.ToString(), //(IsDisplayOnly? uc.AlternateWPS1RevNo.ToString(): GenerateHTMLTextbox(uc.HeaderId,"AlternateWPS1RevNo",uc.AlternateWPS1RevNo.ToString(),"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');", true,"", false)),
                                (IsDisplayOnly?GetWPSHistoryLink(uc.AlternateWPS2,uc.AlternateWPS2RevNo): uc.AlternateWPS2),//uc.AlternateWPS2, //(IsDisplayOnly? uc.AlternateWPS2: (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue().ToLower()|| uc.AlternativeWPS2Disabled || String.IsNullOrEmpty(uc.AlternateWPS2))?GenerateAutoComplete(uc.HeaderId, "AlternateWPS2",uc.AlternateWPS2,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", true,"","AlternateWPS2", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnAlternateWPS2"):GenerateAutoComplete(uc.HeaderId, "AlternateWPS2",uc.AlternateWPS2,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", false,"","AlternateWPS2", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnAlternateWPS2")),
                                uc.AlternateWPS2RevNo.ToString(), //(IsDisplayOnly? uc.AlternateWPS2RevNo.ToString(): GenerateHTMLTextbox(uc.HeaderId,"AlternateWPS2RevNo",uc.AlternateWPS2RevNo.ToString(),"UpdateData(this, "+ uc.HeaderId +",'"+ uc.Status +"');", true,"",false)),
                                Convert.ToString(uc.Preheat), //(IsDisplayOnly? Convert.ToString(uc.Preheat): (uc.Status == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue()|| uc.PWHTAndNotesDisabled || String.IsNullOrEmpty(uc.MainWPSNumber) ? GenerateTextbox(uc.HeaderId,"Preheat",Convert.ToString(uc.Preheat),"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');","return isNumber(event)", "", true,"") : GenerateTextbox(uc.HeaderId,"Preheat",Convert.ToString(uc.Preheat),"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');","return isNumber(event)","", false,""))),
                                uc.DHTISR, //(IsDisplayOnly? Convert.ToString(uc.DHTISR): (uc.Status == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue()|| uc.PWHTAndNotesDisabled || String.IsNullOrEmpty(uc.MainWPSNumber)? GenerateAutoComplete(uc.HeaderId, "DHTISR",uc.DHTISR,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", true,"","DHTISR", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnDHTISR") : GenerateAutoComplete(uc.HeaderId, "DHTISR",uc.DHTISR,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", false,"","DHTISR", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnDHTISR"))),
                                uc.SWPNotes,//GetGeneralNotesDescriptions(list,uc.SWPNotes), //(IsDisplayOnly? GetGeneralNotesDescriptions(list,uc.SWPNotes): (uc.Status == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue()|| uc.PWHTAndNotesDisabled|| String.IsNullOrEmpty(uc.MainWPSNumber) ?
                                //MultiSelectDropdown(uc.HeaderId,list,uc.SWPNotes,true,"Updateswp(this,'"+Convert.ToString(uc.HeaderId)+"','"+ uc.Status +"');","SWPNotes") 
                                //: MultiSelectDropdown(uc.HeaderId,list,uc.SWPNotes, false, "Updateswp(this,'"+Convert.ToString(uc.HeaderId)+"','"+ uc.Status +"');", "SWPNotes"))),
                                // GeneralNotes(uc.HeaderId ,uc.Status, "SWPNotes",uc.SWPNotes,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", true,"","SWPNotes", false,uc.PWHTAndNotesDisabled): GeneralNotes(uc.HeaderId ,uc.Status, "SWPNotes",uc.SWPNotes,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", false,"","SWPNotes", false,uc.PWHTAndNotesDisabled))),

                                GenerateHTMLTextbox(uc.HeaderId,"RevNo",Convert.ToString(uc.RevNo),"", false,"",true),
                                GenerateHTMLTextbox(uc.HeaderId,"Status",uc.Status,"", false,"",true),
                                uc.Status,
                                uc.Returnremarks,
                                (IsDisplayOnly ? "" :
                                          HTMLActionString(uc.HeaderId, uc.Status, "Delete", "Delete Details", "fa fa-trash-o", "DeleteSeamDetails(" + uc.HeaderId + ",'"+uc.Status+"');",!IsDeleteApplicable(uc.Status, uc.RevNo, uc.SeamNo, uc.QualityProject)) //HTMLActionString(uc.HeaderId, uc.Status, "Delete", "Delete Details", "fa fa-trash-o", "DeleteSeamDetails(" + uc.HeaderId + ","+uc.TPHeaderId+");",!(uc.Status == clsImplementationEnum.TestPlanStatus.Draft.GetStringValue() || uc.Status == clsImplementationEnum.TestPlanStatus.Returned.GetStringValue() || uc.TPStatus == clsImplementationEnum.TestPlanStatus.Draft.GetStringValue()|| uc.TPStatus == clsImplementationEnum.TestPlanStatus.Returned.GetStringValue()))
                                        + HTMLActionString(uc.HeaderId,uc.Status,"CopySWPID","Copy SWP","fa fa-files-o","fnCopy("+uc.HeaderId+");",string.IsNullOrWhiteSpace(uc.MainWPSNumber))//!(uc.RevNo  > 0) && !(uc.RevNo==0 && uc.Status==strApproved) )
                                        + HTMLActionString(uc.HeaderId,uc.Status,"btnHistory","History","fa fa-history","SWPHeaderHistory(\""+ uc.HeaderId +"\",\""+uc.SeamNo+"\");",!(uc.RevNo  > 0)) //+ HTMLActionString(uc.HeaderId,uc.Status,"btnHistory","History","fa fa-history","SWPHeaderHistory(\""+ uc.HeaderId +"\",\""+uc.SeamNo+"\","+uc.TPHeaderId+");",!(uc.RevNo  > 0))
                                        + HTMLActionString(uc.HeaderId,uc.Status,"TransferSWPID","Transfer SWP","fa fa-arrow-circle-right","fnTransfer(\""+ uc.HeaderId +"\");",!(uc.RevNo  > 0) && !(uc.RevNo==0 && uc.Status==strApproved))
                                        +(uc.Status != clsImplementationEnum.UTCTQStatus.Approved.GetStringValue()?"":Helper.GenerateActionIcon(uc.HeaderId,"ReviseSeam","Revise","fa fa-repeat","CheckReviseSeam("+uc.HeaderId+")"))
                                    )+ HTMLActionString(uc.HeaderId, uc.Status, "ParameterSWPID", "Parameter Slip", "fa fa-print", "ParameterSlip(\"" + uc.HeaderId + "\");")
                                        +"<a class='' href='javascript:void(0)' onclick=ShowTimeline('/IPI/MaintainSWP/ShowTimeline?HeaderID="+Convert.ToInt32(uc.HeaderId)+"');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a>", //+"<a class='' href='javascript:void(0)' onclick=ShowTimeline('/IPI/MaintainSWP/ShowTimeline?HeaderID="+Convert.ToInt32(uc.HeaderId)+"&&tpheaderid="+Convert.ToInt32(uc.TPHeaderId)+"');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a>",
                                GetGeneralNotesDescriptions(list,uc.SWPNotes,true),
                                Convert.ToString(uc.WPSNo1TokenGenerate)+"#"+Convert.ToString(uc.WPSNo2TokenGenerate)+"#"+Convert.ToString(uc.WPSNo3TokenGenerate)+"#"+Convert.ToString(uc.IsAllPreHeatStageOffered)+"#"+Convert.ToString(uc.WeldVisualStageOffered)+"#"+Convert.ToString(uc.SeamRev)
                           }).ToList();
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        //observation : 27194
        //public JsonResult LoadTPSWPHeaderData(JQueryDataTableParamModel param, string qualityProj, string BU, string Location)
        //{
        //    try
        //    {

        //        var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
        //        var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
        //        var IsDisplayOnly = !string.IsNullOrEmpty(Request["IsDisplayOnly"]) ? Convert.ToBoolean(Request["IsDisplayOnly"].ToString()) : false;

        //        int StartIndex = param.iDisplayStart + 1;
        //        int EndIndex = param.iDisplayStart + param.iDisplayLength;
        //        var user = objClsLoginInfo.Location;
        //        string location = Location.Split('-')[0];
        //        //List<SelectItemList> list = new List<SelectItemList>();
        //        List<SelectItemList> list = new List<SelectItemList>();
        //        //list = (from a in db.SWP002
        //        //        where a.QualityProject == qualityProj
        //        //        select new SelectItemList
        //        //        {
        //        //            id = a.NoteNumber.ToString(),
        //        //            text = a.NoteNumber.ToString() + "-" + a.NoteDescription.ToString()
        //        //        }).ToList();

        //        list = Manager.GetGeneralNotes(qualityProj, "", location).Select(i => new SelectItemList { id = i.NoteNumber.ToString(), text = i.NoteNumber.ToString() + "-" + i.NoteDescription.ToString() }).Distinct().ToList();

        //        string strWhere = "1=1";

        //        if (!string.IsNullOrEmpty(qualityProj) && !string.IsNullOrEmpty(Location))
        //        {
        //            strWhere += " AND UPPER(s10.QualityProject) = '" + qualityProj.ToUpper() + "' AND UPPER(s10.Location) = '" + location.ToUpper() + "'";
        //        }
        //        if (param.CTQCompileStatus.ToUpper() == "PENDING")
        //        {
        //            strWhere += " and 1=1 and (s10.status in('" + clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.QualityIdHeaderStatus.Returned.GetStringValue() + "') OR Q20.status in('" + clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.QualityIdHeaderStatus.Returned.GetStringValue() + "'))";
        //        }
        //        var strApproved = clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue();
        //        string[] arrayLikeCon = {
        //                                        "s10.SeamNo",
        //                                        "s10.SeamDescription",
        //                                        "s10.selectedJointType", //"s10.JointType",
        //                                        "s10.MainWPSNumber",
        //                                        "s10.AlternateWPS1",
        //                                        "s10.AlternateWPS2",
        //                                        "s10.MainWPSRevNo",
        //                                        "s10.AlternateWPS1RevNo",
        //                                        "s10.AlternateWPS2RevNo",
        //                                        "s10.Preheat",
        //                                        "s10.DHTISR",
        //                                        "s10.SWPNotes",
        //                                        "s10.Status"
        //            };
        //        if (!string.IsNullOrWhiteSpace(param.sSearch))
        //        {
        //            strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
        //        }
        //        else
        //        {
        //            strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
        //        }
        //        Session["TP_SWP_Search"] = param.sSearch;
        //        Session["TP_SWP_PageSize"] = Convert.ToString(param.iDisplayLength);

        //        string strSortOrder = string.Empty;
        //        string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
        //        string sortDirection = Convert.ToString(Request["sSortDir_0"]);

        //        if (!string.IsNullOrWhiteSpace(sortColumnName))
        //        {
        //            strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
        //        }
        //        //if (param.FilterDocNo == "1")
        //        //{
        //        //    if (!string.IsNullOrWhiteSpace(strSortOrder))
        //        //    {
        //        //        strSortOrder += " iif(tm.editedon > tm.tpeditedon , tm.editedon , tm.tpeditedon) desc";
        //        //    }
        //        //    else
        //        //    {
        //        //        strSortOrder = " Order By iif(tm.editedon > tm.tpeditedon , tm.editedon , tm.tpeditedon) desc";
        //        //    }
        //        //}
        //        var lstResult = db.SP_SWP_Get_HeaderList
        //                        (
        //                        StartIndex, EndIndex, strSortOrder, strWhere, BU, user
        //                        ).ToList();
        //        //var listSP_GET_ALL_PRE_HEAT_STAGE_SEAM_OFFERED = db.SP_GET_ALL_PRE_HEAT_STAGE_SEAM_OFFERED(qualityProj, BU, Location).ToList();
        //        //var listSP_SWP_GET_TOKEN_GENERATED_WPS_NUMBER = db.SP_SWP_GET_TOKEN_GENERATED_WPS_NUMBER(qualityProj, BU, Location).ToList();

        //        var listSP_SWP_Get_HeaderList_Ent = new List<SP_SWP_Get_HeaderList_Ent>();
        //        lstResult.ForEach(i =>
        //        {
        //            //bool SeamOffered = false;
        //            //IsSeamOfferedCheckEnt objIsSeamOfferedCheckEnt = new IsSeamOfferedCheckEnt();
        //            if (!IsDisplayOnly)
        //            {
        //                //SeamOffered = IsSeamOffered(i.HeaderId);
        //                //objIsSeamOfferedCheckEnt = CheckSeamIsOffered(listSP_GET_ALL_PRE_HEAT_STAGE_SEAM_OFFERED, listSP_SWP_GET_TOKEN_GENERATED_WPS_NUMBER, i, BU);
        //            }
        //            listSP_SWP_Get_HeaderList_Ent.Add(new SP_SWP_Get_HeaderList_Ent
        //            {
        //                AlternateWPS1 = i.AlternateWPS1,
        //                AlternateWPS1RevNo = i.AlternateWPS1RevNo,
        //                AlternateWPS2 = i.AlternateWPS2,
        //                AlternateWPS2RevNo = i.AlternateWPS2RevNo,
        //                DHTISR = i.DHTISR,
        //                HeaderId = i.HeaderId,
        //                //IsSeamOffered = SeamOffered,
        //                JointType = i.JointType,
        //                Location = i.Location,
        //                MainWPSNumber = i.MainWPSNumber,
        //                MainWPSRevNo = i.MainWPSRevNo,
        //                Preheat = i.Preheat,
        //                QualityProject = i.QualityProject,
        //                Returnremarks = i.Returnremarks,
        //                RevNo = i.RevNo,
        //                ROW_NO = i.ROW_NO,
        //                SeamDescription = i.SeamDescription,
        //                SeamNo = i.SeamNo,
        //                SeamRev = i.SeamRev,
        //                selectedJointType = i.selectedJointType,
        //                Status = i.Status,
        //                SWPNotes = i.SWPNotes,
        //                TotalCount = i.TotalCount,
        //                WPSNumbers1 = i.WPSNumbers1,
        //                WPSNumbers2 = i.WPSNumbers2,
        //                WPSNumbers3 = i.WPSNumbers3,

        //                QualityId = Convert.ToString(i.QualityId),
        //                QualityIdRev = i.QualityIdRev,
        //                PTCApplicable = i.PTCApplicable,
        //                PTCNumber = Convert.ToString(i.PTCNumber),
        //                TPHeaderId = i.TPHeaderId,
        //                TPStatus = Convert.ToString(i.TPStatus),
        //                TPRevNo = i.TPRevNo

        //                //MainWPSDisabled = (objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered || objIsSeamOfferedCheckEnt.WPSNo1TokenGenerate),
        //                //AlternativeWPS1Disabled = (objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered || objIsSeamOfferedCheckEnt.WPSNo2TokenGenerate),
        //                //AlternativeWPS2Disabled = (objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered || objIsSeamOfferedCheckEnt.WPSNo3TokenGenerate),
        //                //PWHTAndNotesDisabled = (objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered || objIsSeamOfferedCheckEnt.WeldVisualStageOffered),
        //                //JointTypeDisabled = (objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered || objIsSeamOfferedCheckEnt.WPSNo1TokenGenerate || objIsSeamOfferedCheckEnt.WPSNo2TokenGenerate || objIsSeamOfferedCheckEnt.WPSNo3TokenGenerate),
        //                //IsAllPreHeatStageOffered = objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered,
        //                //WPSNo1TokenGenerate = objIsSeamOfferedCheckEnt.WPSNo1TokenGenerate,
        //                //WPSNo2TokenGenerate = objIsSeamOfferedCheckEnt.WPSNo2TokenGenerate,
        //                //WPSNo3TokenGenerate = objIsSeamOfferedCheckEnt.WPSNo3TokenGenerate,
        //                //WeldVisualStageOffered = objIsSeamOfferedCheckEnt.WeldVisualStageOffered
        //            });
        //        });
        //        var data = (from uc in listSP_SWP_Get_HeaderList_Ent
        //                    select new[]
        //                   {
        //                        Convert.ToString(uc.HeaderId),
        //                        uc.SeamNo,
        //                        uc.selectedJointType, //(IsDisplayOnly? uc.selectedJointType: (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue().ToLower() || uc.JointTypeDisabled)? GenerateAutoComplete(uc.HeaderId, "JointType",uc.selectedJointType,"\"Updateswp(this,"+uc.HeaderId+",'"+uc.Status+"');\"", true,"","JointType")+""+Helper.GenerateHidden(uc.HeaderId, "hdnJointType") : GenerateAutoComplete(uc.HeaderId, "JointType",uc.selectedJointType,"\"Updateswp(this,"+uc.HeaderId+",'"+uc.Status+"');\"", false,"","JointType", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnJointType")),
        //                        (IsDisplayOnly?GetWPSHistoryPrint(uc.MainWPSNumber,uc.MainWPSRevNo,uc.selectedJointType): uc.MainWPSNumber),//uc.MainWPSNumber, //(IsDisplayOnly? uc.MainWPSNumber: (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue().ToLower() || uc.MainWPSDisabled || String.IsNullOrEmpty(uc.selectedJointType))? GenerateAutoComplete(uc.HeaderId, "MainWPSNumber",uc.MainWPSNumber,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", true,"","MainWPSNumber")+""+Helper.GenerateHidden(uc.HeaderId, "hdnMainWPSNumber") : GenerateAutoComplete(uc.HeaderId, "MainWPSNumber",uc.MainWPSNumber,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"",  false,"","MainWPSNumber", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnMainWPSNumber")),
        //                        uc.MainWPSRevNo.ToString(), //(IsDisplayOnly ? uc.MainWPSRevNo.ToString(): GenerateHTMLTextbox(uc.HeaderId,"MainWPSRevNo",uc.MainWPSRevNo.ToString(),"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');", true,"", false)),
        //                        (IsDisplayOnly?GetWPSHistoryLink(uc.AlternateWPS1,uc.AlternateWPS1RevNo): uc.AlternateWPS1),//uc.AlternateWPS1, //(IsDisplayOnly? uc.AlternateWPS1: (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue().ToLower()|| uc.AlternativeWPS1Disabled || String.IsNullOrEmpty(uc.MainWPSNumber))? GenerateAutoComplete(uc.HeaderId, "AlternateWPS1",uc.AlternateWPS1,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", true,"","AlternateWPS1", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnAlternateWPS1"):GenerateAutoComplete(uc.HeaderId, "AlternateWPS1",uc.AlternateWPS1,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", false,"","AlternateWPS1", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnAlternateWPS1")),
        //                        uc.AlternateWPS1RevNo.ToString(), //(IsDisplayOnly? uc.AlternateWPS1RevNo.ToString(): GenerateHTMLTextbox(uc.HeaderId,"AlternateWPS1RevNo",uc.AlternateWPS1RevNo.ToString(),"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');", true,"", false)),
        //                        (IsDisplayOnly?GetWPSHistoryLink(uc.AlternateWPS2,uc.AlternateWPS2RevNo): uc.AlternateWPS2),//uc.AlternateWPS2, //(IsDisplayOnly? uc.AlternateWPS2: (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue().ToLower()|| uc.AlternativeWPS2Disabled || String.IsNullOrEmpty(uc.AlternateWPS2))?GenerateAutoComplete(uc.HeaderId, "AlternateWPS2",uc.AlternateWPS2,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", true,"","AlternateWPS2", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnAlternateWPS2"):GenerateAutoComplete(uc.HeaderId, "AlternateWPS2",uc.AlternateWPS2,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", false,"","AlternateWPS2", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnAlternateWPS2")),
        //                        uc.AlternateWPS2RevNo.ToString(), //(IsDisplayOnly? uc.AlternateWPS2RevNo.ToString(): GenerateHTMLTextbox(uc.HeaderId,"AlternateWPS2RevNo",uc.AlternateWPS2RevNo.ToString(),"UpdateData(this, "+ uc.HeaderId +",'"+ uc.Status +"');", true,"",false)),
        //                        Convert.ToString(uc.Preheat), //(IsDisplayOnly? Convert.ToString(uc.Preheat): (uc.Status == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue()|| uc.PWHTAndNotesDisabled || String.IsNullOrEmpty(uc.MainWPSNumber) ? GenerateTextbox(uc.HeaderId,"Preheat",Convert.ToString(uc.Preheat),"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');","return isNumber(event)", "", true,"") : GenerateTextbox(uc.HeaderId,"Preheat",Convert.ToString(uc.Preheat),"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');","return isNumber(event)","", false,""))),
        //                        uc.DHTISR, //(IsDisplayOnly? Convert.ToString(uc.DHTISR): (uc.Status == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue()|| uc.PWHTAndNotesDisabled || String.IsNullOrEmpty(uc.MainWPSNumber)? GenerateAutoComplete(uc.HeaderId, "DHTISR",uc.DHTISR,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", true,"","DHTISR", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnDHTISR") : GenerateAutoComplete(uc.HeaderId, "DHTISR",uc.DHTISR,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", false,"","DHTISR", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnDHTISR"))),
        //                        uc.SWPNotes,//GetGeneralNotesDescriptions(list,uc.SWPNotes), //(IsDisplayOnly? GetGeneralNotesDescriptions(list,uc.SWPNotes): (uc.Status == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue()|| uc.PWHTAndNotesDisabled|| String.IsNullOrEmpty(uc.MainWPSNumber) ?
        //                        //MultiSelectDropdown(uc.HeaderId,list,uc.SWPNotes,true,"Updateswp(this,'"+Convert.ToString(uc.HeaderId)+"','"+ uc.Status +"');","SWPNotes") 
        //                        //: MultiSelectDropdown(uc.HeaderId,list,uc.SWPNotes, false, "Updateswp(this,'"+Convert.ToString(uc.HeaderId)+"','"+ uc.Status +"');", "SWPNotes"))),
        //                        // GeneralNotes(uc.HeaderId ,uc.Status, "SWPNotes",uc.SWPNotes,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", true,"","SWPNotes", false,uc.PWHTAndNotesDisabled): GeneralNotes(uc.HeaderId ,uc.Status, "SWPNotes",uc.SWPNotes,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", false,"","SWPNotes", false,uc.PWHTAndNotesDisabled))),

        //                        GenerateHTMLTextbox(uc.HeaderId,"RevNo",Convert.ToString(uc.RevNo),"", false,"",true),
        //                        GenerateHTMLTextbox(uc.HeaderId,"Status",uc.Status,"", false,"",true),
        //                        uc.Status,
        //                        uc.Returnremarks,

        //                         Convert.ToString(uc.QualityId),
        //                         Convert.ToString("R" +uc.QualityIdRev),
        //                         uc.PTCApplicable == true ? "Yes" : "No",
        //                         Convert.ToString(uc.PTCNumber),
        //                         Convert.ToString(uc.TPHeaderId),
        //                         Convert.ToString(uc.TPStatus),
        //                         Convert.ToString("R" +uc.TPRevNo),
        //                        (IsDisplayOnly ? ""  //(IsDisplayOnly ? "test" 
        //                        :
        //                                  HTMLActionString(uc.HeaderId, uc.Status, "Delete", "Delete Details", "fa fa-trash-o", "DeleteSeamDetails(" + uc.HeaderId + ","+uc.TPHeaderId+");",!(uc.Status == clsImplementationEnum.TestPlanStatus.Draft.GetStringValue() || uc.Status == clsImplementationEnum.TestPlanStatus.Returned.GetStringValue() || uc.TPStatus == clsImplementationEnum.TestPlanStatus.Draft.GetStringValue()|| uc.TPStatus == clsImplementationEnum.TestPlanStatus.Returned.GetStringValue()))+
        //                                  //HTMLActionString(uc.HeaderId, uc.Status, "Revise", "Revise Details", "fa fa-retweet", "ReviseSeamDetails(\""+uc.SeamNo+"\"," + uc.HeaderId + ","+uc.TPHeaderId+",\"" + uc.Status + "\",\""+uc.TPStatus+"\");",!(uc.Status == clsImplementationEnum.TestPlanStatus.Approved.GetStringValue() || uc.TPStatus == clsImplementationEnum.TestPlanStatus.Approved.GetStringValue()))+" "+
        //                                  Manager.generateSeamHistoryButton(param.Project,uc.QualityProject, uc.SeamNo,"Seam Details")
        //                                + HTMLActionString(uc.HeaderId,uc.Status,"Test Plan Stages","Test Plan Stages","fa fa-newspaper-o","GetTestPlanLinePartial("+uc.TPHeaderId+",\""+uc.SeamNo+"\",true);") //+ HTMLActionString(uc.HeaderId,uc.Status,"Test Plan Stages","Test Plan Stages","fa fa-newspaper-o","GetTestPlanLinePartial("+uc.TPHeaderId+",\""+uc.SeamNo+"\",true,\"swp\");") //+ HTMLActionString(uc.HeaderId,uc.Status,"Test Plan Stages","Test Plan Stages","fa fa-newspaper-o","GetTestPlanLinePartial("+uc.TPHeaderId+",\""+uc.SeamNo+"\",true,\"swp\",\""+uc.TPStatus+"\");")
        //                                + HTMLActionString(uc.HeaderId,uc.Status,"CopySWPID","Copy SWP","fa fa-files-o","fnCopy("+uc.HeaderId+","+uc.TPHeaderId+");",false)//!(uc.RevNo  > 0) && !(uc.RevNo==0 && uc.Status==strApproved) )
        //                                + HTMLActionString(uc.HeaderId,uc.Status,"btnHistory","History","fa fa-history","SWPHeaderHistory(\""+ uc.HeaderId +"\",\""+ uc.TPHeaderId +"\",\""+uc.SeamNo+"\");",false) //+ HTMLActionString(uc.HeaderId,uc.Status,"btnHistory","History","fa fa-history","SWPHeaderHistory(\""+ uc.HeaderId +"\",\""+uc.SeamNo+"\","+uc.TPHeaderId+");",false)
        //                                //+ HTMLActionString(uc.HeaderId,uc.Status,"TransferSWPID","Transfer SWP","fa fa-arrow-circle-right","fnTransfer(\""+ uc.HeaderId +"\");",!(uc.RevNo  > 0) && !(uc.RevNo==0 && uc.Status==strApproved))
        //                                +(uc.Status != clsImplementationEnum.UTCTQStatus.Approved.GetStringValue()?"":Helper.GenerateActionIcon(uc.HeaderId,"ReviseSeam","Revise","fa fa-repeat","CheckReviseSeam("+uc.HeaderId+")"))
        //                            )
        //                            + HTMLActionString(uc.HeaderId, uc.Status, "ParameterSWPID", "Parameter Slip", "fa fa-print", "ParameterSlip(\"" + uc.HeaderId + "\");")
        //                            +"<a class='' href='javascript:void(0)' onclick=ShowTimeline('/IPI/MaintainSWP/ShowTimeline?HeaderID="+Convert.ToInt32(uc.HeaderId)+"');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a>", //+"<a class='' href='javascript:void(0)' onclick=ShowTimeline('/IPI/MaintainSWP/ShowTimeline?HeaderID="+Convert.ToInt32(uc.HeaderId)+"&&tpheaderid="+Convert.ToInt32(uc.TPHeaderId)+"');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a>",
        //                        GetGeneralNotesDescriptions(list,uc.SWPNotes,true),
        //                           Convert.ToString(uc.WPSNo1TokenGenerate)+"#"+Convert.ToString(uc.WPSNo2TokenGenerate)+"#"+Convert.ToString(uc.WPSNo3TokenGenerate)+"#"+Convert.ToString(uc.IsAllPreHeatStageOffered)+"#"+Convert.ToString(uc.WeldVisualStageOffered)+"#"+Convert.ToString(uc.SeamRev)+"#"+Convert.ToString("R" + uc.RevNo) +"#"+Convert.ToString(uc.TPStatus) +"#"+Convert.ToString("R" + uc.TPRevNo) +"#"+Convert.ToString("R" + uc.QualityIdRev)

        //                   }).ToList();
        //        return Json(new
        //        {
        //            sEcho = Convert.ToInt32(param.sEcho),
        //            iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
        //            iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
        //            aaData = data,
        //            strSortOrder = strSortOrder,
        //            whereCondition = strWhere //whereCondition = strWhere,
        //            //isRevise = param.FilterDocNo
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        return Json(new
        //        {
        //            sEcho = param.sEcho,
        //            iTotalRecords = "0",
        //            iTotalDisplayRecords = "0",
        //            aaData = new string[] { }
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //}

        //[HttpPost]
        //public JsonResult GetInlineEditableRow(int id, string qualityProj, string BU, string Location, bool isReadOnly = false, string Project = "")
        //{
        //    try
        //    {
        //        var data = new List<string[]>();
        //        string location = Location.Split('-')[0];
        //        var list = Manager.GetGeneralNotes(qualityProj, "", location).Select(i => new SelectItemList { id = i.NoteNumber.ToString(), text = i.NoteNumber.ToString() + "-" + i.NoteDescription.ToString() }).Distinct().ToList();

        //        if (id != 0)
        //        {
        //            var seamno = db.SWP010.Where(x => x.HeaderId == id).Select(i => i.SeamNo).FirstOrDefault();
        //            var IsDisplayOnly = !string.IsNullOrEmpty(Request["IsDisplayOnly"]) ? Convert.ToBoolean(Request["IsDisplayOnly"].ToString()) : false;
        //            string strWhere = " UPPER(s10.QualityProject) = '" + qualityProj.ToUpper() + "' AND UPPER(s10.Location) = '" + location.ToUpper() + "'";
        //            if (!string.IsNullOrWhiteSpace(seamno))
        //            {
        //                strWhere += " AND UPPER(s10.SeamNo) = '" + seamno.ToUpper() + "' ";
        //            }
        //            //strWhere = "s10.HeaderId = " + id
        //            var lstResult = db.SP_SWP_Get_HeaderList
        //                          (
        //                           1, 0, "", strWhere, BU, objClsLoginInfo.Location
        //                          ).Take(1).ToList();

        //            var listSP_GET_ALL_PRE_HEAT_STAGE_SEAM_OFFERED = db.SP_GET_ALL_PRE_HEAT_STAGE_SEAM_OFFERED(qualityProj, BU, Location).ToList();
        //            var listSP_SWP_GET_TOKEN_GENERATED_WPS_NUMBER = db.SP_SWP_GET_TOKEN_GENERATED_WPS_NUMBER(qualityProj, BU, Location).ToList();

        //            var strApproved = clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue();
        //            var listSP_SWP_Get_HeaderList_Ent = new List<SP_SWP_Get_HeaderList_Ent>();
        //            lstResult.ForEach(i =>
        //            {
        //                //bool SeamOffered = false;
        //                IsSeamOfferedCheckEnt objIsSeamOfferedCheckEnt = new IsSeamOfferedCheckEnt();
        //                if (!IsDisplayOnly)
        //                {
        //                    //SeamOffered = IsSeamOffered(i.HeaderId);
        //                    objIsSeamOfferedCheckEnt = CheckSeamIsOffered(listSP_GET_ALL_PRE_HEAT_STAGE_SEAM_OFFERED, listSP_SWP_GET_TOKEN_GENERATED_WPS_NUMBER, i, BU);
        //                }
        //                listSP_SWP_Get_HeaderList_Ent.Add(new SP_SWP_Get_HeaderList_Ent
        //                {
        //                    AlternateWPS1 = i.AlternateWPS1,
        //                    AlternateWPS1RevNo = i.AlternateWPS1RevNo,
        //                    AlternateWPS2 = i.AlternateWPS2,
        //                    AlternateWPS2RevNo = i.AlternateWPS2RevNo,
        //                    DHTISR = i.DHTISR,
        //                    HeaderId = i.HeaderId,
        //                    //IsSeamOffered = SeamOffered,
        //                    JointType = i.JointType,
        //                    Location = i.Location,
        //                    MainWPSNumber = i.MainWPSNumber,
        //                    MainWPSRevNo = i.MainWPSRevNo,
        //                    Preheat = i.Preheat,
        //                    QualityProject = i.QualityProject,
        //                    Returnremarks = i.Returnremarks,
        //                    RevNo = i.RevNo,
        //                    ROW_NO = i.ROW_NO,
        //                    SeamDescription = i.SeamDescription,
        //                    SeamNo = i.SeamNo,
        //                    SeamRev = i.SeamRev,
        //                    selectedJointType = i.selectedJointType,
        //                    Status = i.Status,
        //                    SWPNotes = i.SWPNotes,
        //                    TotalCount = i.TotalCount,
        //                    WPSNumbers1 = i.WPSNumbers1,
        //                    WPSNumbers2 = i.WPSNumbers2,
        //                    WPSNumbers3 = i.WPSNumbers3,
        //                    MainWPSDisabled = (objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered || objIsSeamOfferedCheckEnt.WPSNo1TokenGenerate),
        //                    AlternativeWPS1Disabled = (objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered || objIsSeamOfferedCheckEnt.WPSNo2TokenGenerate),
        //                    AlternativeWPS2Disabled = (objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered || objIsSeamOfferedCheckEnt.WPSNo3TokenGenerate),
        //                    PWHTAndNotesDisabled = (objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered || objIsSeamOfferedCheckEnt.WeldVisualStageOffered),
        //                    JointTypeDisabled = (objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered || objIsSeamOfferedCheckEnt.WPSNo1TokenGenerate || objIsSeamOfferedCheckEnt.WPSNo2TokenGenerate || objIsSeamOfferedCheckEnt.WPSNo3TokenGenerate),
        //                    IsAllPreHeatStageOffered = objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered,
        //                    WPSNo1TokenGenerate = objIsSeamOfferedCheckEnt.WPSNo1TokenGenerate,
        //                    WPSNo2TokenGenerate = objIsSeamOfferedCheckEnt.WPSNo2TokenGenerate,
        //                    WPSNo3TokenGenerate = objIsSeamOfferedCheckEnt.WPSNo3TokenGenerate,
        //                    WeldVisualStageOffered = objIsSeamOfferedCheckEnt.WeldVisualStageOffered,
        //                    QualityId = Convert.ToString(i.QualityId),
        //                    QualityIdRev = i.QualityIdRev,
        //                    PTCApplicable = i.PTCApplicable,
        //                    PTCNumber = Convert.ToString(i.PTCNumber),
        //                    TPHeaderId = i.TPHeaderId,
        //                    TPStatus = Convert.ToString(i.TPStatus),
        //                });
        //            });
        //            List<SelectListItem> BoolenList = new List<SelectListItem>() { new SelectListItem { Text = "Yes", Value = "True" }, new SelectListItem { Text = "No", Value = "False" } };
        //            bool isDisabled = false;
        //            data = (from uc in listSP_SWP_Get_HeaderList_Ent
        //                    select new[]
        //                   {
        //                        clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//Convert.ToString(uc.HeaderId),
        //                        clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//uc.SeamNo,
        //                        (IsDisplayOnly || isReadOnly ||  (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue().ToLower())
        //                        ?  uc.selectedJointType
        //                        : (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue().ToLower() || uc.JointTypeDisabled)? GenerateAutoComplete(uc.HeaderId, "JointType",uc.selectedJointType,"\"Updateswp(this,"+uc.HeaderId+",'"+uc.Status+"');\"", isDisabled,"","JointType")+""+Helper.GenerateHidden(uc.HeaderId, "hdnJointType") : GenerateAutoComplete(uc.HeaderId, "JointType",uc.selectedJointType,"\"Updateswp(this,"+uc.HeaderId+",'"+uc.Status+"');\"", false,"","JointType", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnJointType")),//"width: 115px !important;" //: (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue().ToLower() || uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue().ToLower()|| uc.JointTypeDisabled)? GenerateAutoComplete(uc.HeaderId, "JointType",uc.selectedJointType,"\"Updateswp(this,"+uc.HeaderId+",'"+uc.Status+"');\"", isDisabled,"","JointType")+""+Helper.GenerateHidden(uc.HeaderId, "hdnJointType") : GenerateAutoComplete(uc.HeaderId, "JointType",uc.selectedJointType,"\"Updateswp(this,"+uc.HeaderId+",'"+uc.Status+"');\"", false,"","JointType", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnJointType")),//"width: 115px !important;" //: (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue().ToLower()|| uc.JointTypeDisabled)? GenerateAutoComplete(uc.HeaderId, "JointType",uc.selectedJointType,"\"Updateswp(this,"+uc.HeaderId+",'"+uc.Status+"');\"", (uc.Status == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue()),"","JointType")+""+Helper.GenerateHidden(uc.HeaderId, "hdnJointType") : GenerateAutoComplete(uc.HeaderId, "JointType",uc.selectedJointType,"\"Updateswp(this,"+uc.HeaderId+",'"+uc.Status+"');\"", false,"","JointType", (uc.Status == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue()))+""+Helper.GenerateHidden(uc.HeaderId, "hdnJointType")),//"width: 115px !important;"
        //                        (IsDisplayOnly || isReadOnly ||  (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue().ToLower())
        //                        ? (IsDisplayOnly?GetWPSHistoryLink(uc.MainWPSNumber,uc.MainWPSRevNo): uc.MainWPSNumber)
        //                        : (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue().ToLower() || uc.MainWPSDisabled || String.IsNullOrEmpty(uc.selectedJointType))? GenerateAutoComplete(uc.HeaderId, "MainWPSNumber",uc.MainWPSNumber,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"",isDisabled,"","MainWPSNumber")+""+Helper.GenerateHidden(uc.HeaderId, "hdnMainWPSNumber") : GenerateAutoComplete(uc.HeaderId, "MainWPSNumber",uc.MainWPSNumber,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"",  false,"","MainWPSNumber", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnMainWPSNumber")),//"width:160px !important;padding-left:5px;" //: (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue().ToLower() || uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue().ToLower()|| uc.MainWPSDisabled || String.IsNullOrEmpty(uc.selectedJointType))? GenerateAutoComplete(uc.HeaderId, "MainWPSNumber",uc.MainWPSNumber,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"",isDisabled,"","MainWPSNumber")+""+Helper.GenerateHidden(uc.HeaderId, "hdnMainWPSNumber") : GenerateAutoComplete(uc.HeaderId, "MainWPSNumber",uc.MainWPSNumber,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"",  false,"","MainWPSNumber", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnMainWPSNumber")),//"width:160px !important;padding-left:5px;" //: ( uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue().ToLower()|| uc.MainWPSDisabled || String.IsNullOrEmpty(uc.selectedJointType))? GenerateAutoComplete(uc.HeaderId, "MainWPSNumber",uc.MainWPSNumber,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"",(uc.Status == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue()),"","MainWPSNumber")+""+Helper.GenerateHidden(uc.HeaderId, "hdnMainWPSNumber") : GenerateAutoComplete(uc.HeaderId, "MainWPSNumber",uc.MainWPSNumber,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"",  false,"","MainWPSNumber", (uc.Status == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue()))+""+Helper.GenerateHidden(uc.HeaderId, "hdnMainWPSNumber")),//"width:160px !important;padding-left:5px;"
        //                        (IsDisplayOnly || isReadOnly ||  (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue().ToLower())
        //                        ?  uc.MainWPSRevNo.ToString()
        //                        : GenerateHTMLTextbox(uc.HeaderId,"MainWPSRevNo",uc.MainWPSRevNo.ToString(),"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');", true,"", false)),
        //                        (IsDisplayOnly || isReadOnly ||  (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue().ToLower())
        //                        ?  (IsDisplayOnly?GetWPSHistoryLink(uc.AlternateWPS1,uc.AlternateWPS1RevNo): uc.AlternateWPS1)
        //                        : (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue().ToLower()|| uc.AlternativeWPS1Disabled || String.IsNullOrEmpty(uc.MainWPSNumber))? GenerateAutoComplete(uc.HeaderId, "AlternateWPS1",uc.AlternateWPS1,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", isDisabled,"","AlternateWPS1", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnAlternateWPS1"):GenerateAutoComplete(uc.HeaderId, "AlternateWPS1",uc.AlternateWPS1,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", false,"","AlternateWPS1", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnAlternateWPS1")), //: (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue().ToLower() || uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue().ToLower()|| uc.AlternativeWPS1Disabled || String.IsNullOrEmpty(uc.MainWPSNumber) )? GenerateAutoComplete(uc.HeaderId, "AlternateWPS1",uc.AlternateWPS1,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", isDisabled,"","AlternateWPS1", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnAlternateWPS1"):GenerateAutoComplete(uc.HeaderId, "AlternateWPS1",uc.AlternateWPS1,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", false,"","AlternateWPS1", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnAlternateWPS1")), //: (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue().ToLower()|| uc.AlternativeWPS1Disabled || String.IsNullOrEmpty(uc.MainWPSNumber) )? GenerateAutoComplete(uc.HeaderId, "AlternateWPS1",uc.AlternateWPS1,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", (uc.Status == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue()),"","AlternateWPS1", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnAlternateWPS1"):GenerateAutoComplete(uc.HeaderId, "AlternateWPS1",uc.AlternateWPS1,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", false,"","AlternateWPS1", (uc.Status == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue()))+""+Helper.GenerateHidden(uc.HeaderId, "hdnAlternateWPS1")),
        //                        (IsDisplayOnly || isReadOnly ||  (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue().ToLower())
        //                        ?  uc.AlternateWPS1RevNo.ToString()
        //                        : GenerateHTMLTextbox(uc.HeaderId,"AlternateWPS1RevNo",uc.AlternateWPS1RevNo.ToString(),"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');", true,"", false)),
        //                        (IsDisplayOnly || isReadOnly ||  (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue().ToLower())
        //                        ?  (IsDisplayOnly?GetWPSHistoryLink(uc.AlternateWPS2,uc.MainWPSRevNo): uc.AlternateWPS2)
        //                        : (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue().ToLower()|| uc.AlternativeWPS2Disabled || String.IsNullOrEmpty(uc.AlternateWPS1))?GenerateAutoComplete(uc.HeaderId, "AlternateWPS2",uc.AlternateWPS2,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", isDisabled,"","AlternateWPS2", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnAlternateWPS2"):GenerateAutoComplete(uc.HeaderId, "AlternateWPS2",uc.AlternateWPS2,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", false,"","AlternateWPS2", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnAlternateWPS2")), //: (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue().ToLower()|| uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue().ToLower()|| uc.AlternativeWPS2Disabled || String.IsNullOrEmpty(uc.AlternateWPS1))?GenerateAutoComplete(uc.HeaderId, "AlternateWPS2",uc.AlternateWPS2,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", isDisabled,"","AlternateWPS2", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnAlternateWPS2"):GenerateAutoComplete(uc.HeaderId, "AlternateWPS2",uc.AlternateWPS2,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", false,"","AlternateWPS2", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnAlternateWPS2")), //: (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue().ToLower()|| uc.AlternativeWPS2Disabled || String.IsNullOrEmpty(uc.AlternateWPS1))?GenerateAutoComplete(uc.HeaderId, "AlternateWPS2",uc.AlternateWPS2,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", (uc.Status == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue()),"","AlternateWPS2", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnAlternateWPS2"):GenerateAutoComplete(uc.HeaderId, "AlternateWPS2",uc.AlternateWPS2,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", false,"","AlternateWPS2", (uc.Status == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue()))+""+Helper.GenerateHidden(uc.HeaderId, "hdnAlternateWPS2")),
        //                        (IsDisplayOnly || isReadOnly ||  (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue().ToLower())
        //                        ?  uc.AlternateWPS2RevNo.ToString()
        //                        : GenerateHTMLTextbox(uc.HeaderId,"AlternateWPS2RevNo",uc.AlternateWPS2RevNo.ToString(),"UpdateData(this, "+ uc.HeaderId +",'"+ uc.Status +"');", true,"",false)),
        //                        (IsDisplayOnly || isReadOnly ||  (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue().ToLower())
        //                        ?  Convert.ToString(uc.Preheat)
        //                        : (uc.Status == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue()|| uc.PWHTAndNotesDisabled || String.IsNullOrEmpty(uc.MainWPSNumber) ? GenerateTextbox(uc.HeaderId,"Preheat",Convert.ToString(uc.Preheat),"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');","return isNumber(event)", "", isDisabled,"") : GenerateTextbox(uc.HeaderId,"Preheat",Convert.ToString(uc.Preheat),"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');","return isNumber(event)","", false,""))), //: (uc.Status == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue()|| uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue().ToLower()|| uc.PWHTAndNotesDisabled || String.IsNullOrEmpty(uc.MainWPSNumber) ? GenerateTextbox(uc.HeaderId,"Preheat",Convert.ToString(uc.Preheat),"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');","return isNumber(event)", "", isDisabled,"") : GenerateTextbox(uc.HeaderId,"Preheat",Convert.ToString(uc.Preheat),"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');","return isNumber(event)","", false,""))), //: (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue().ToLower()|| uc.PWHTAndNotesDisabled || String.IsNullOrEmpty(uc.MainWPSNumber) ? GenerateTextbox(uc.HeaderId,"Preheat",Convert.ToString(uc.Preheat),"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');","return isNumber(event)", "", (uc.Status == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue()),"") : GenerateTextbox(uc.HeaderId,"Preheat",Convert.ToString(uc.Preheat),"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');","return isNumber(event)","", (uc.Status == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue()),""))),
        //                        (IsDisplayOnly || isReadOnly ||  (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue().ToLower())
        //                        ? Convert.ToString(uc.DHTISR)
        //                        : (uc.Status == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue()|| uc.PWHTAndNotesDisabled || String.IsNullOrEmpty(uc.MainWPSNumber)? GenerateAutoComplete(uc.HeaderId, "DHTISR",uc.DHTISR,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", isDisabled,"","DHTISR", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnDHTISR") : GenerateAutoComplete(uc.HeaderId, "DHTISR",uc.DHTISR,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", false,"","DHTISR", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnDHTISR"))), //: (uc.Status == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue()||  uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue().ToLower() || uc.PWHTAndNotesDisabled || String.IsNullOrEmpty(uc.MainWPSNumber) ? GenerateAutoComplete(uc.HeaderId, "DHTISR",uc.DHTISR,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", isDisabled,"","DHTISR", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnDHTISR") : GenerateAutoComplete(uc.HeaderId, "DHTISR",uc.DHTISR,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", false,"","DHTISR", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnDHTISR"))), //: (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue().ToLower() || uc.PWHTAndNotesDisabled || String.IsNullOrEmpty(uc.MainWPSNumber) ? GenerateAutoComplete(uc.HeaderId, "DHTISR",uc.DHTISR,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", (uc.Status == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue()),"","DHTISR", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnDHTISR") : GenerateAutoComplete(uc.HeaderId, "DHTISR",uc.DHTISR,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", false,"","DHTISR", (uc.Status == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue()))+""+Helper.GenerateHidden(uc.HeaderId, "hdnDHTISR"))),
        //                        (IsDisplayOnly || isReadOnly ||  (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue().ToLower())
        //                        ? uc.SWPNotes//GetGeneralNotesDescriptions(list,uc.SWPNotes)
        //                        : (uc.Status == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue()|| uc.PWHTAndNotesDisabled ? //: (uc.Status == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue()|| uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue().ToLower() || uc.PWHTAndNotesDisabled ? //: (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue().ToLower() || uc.PWHTAndNotesDisabled ?
        //                        //MultiSelectDropdown(uc.HeaderId,list,uc.SWPNotes,true,"Updateswp(this,'"+Convert.ToString(uc.HeaderId)+"','"+ uc.Status +"');","SWPNotes") 
        //                        //: MultiSelectDropdown(uc.HeaderId,list,uc.SWPNotes, false, "Updateswp(this,'"+Convert.ToString(uc.HeaderId)+"','"+ uc.Status +"');", "SWPNotes"))),
        //                         GeneralNotes(uc.HeaderId ,uc.Status, "SWPNotes",uc.SWPNotes,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", true,"","SWPNotes", false,uc.PWHTAndNotesDisabled)
        //                        : GeneralNotes(uc.HeaderId ,uc.Status, "SWPNotes",uc.SWPNotes,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", false,"","SWPNotes", false,uc.PWHTAndNotesDisabled))),

        //                        clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//Convert.ToString(uc.RovNo),
        //                        clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//Convert.ToString(uc.Status),
        //                        Convert.ToString(uc.Status),
        //                        Convert.ToString(uc.Returnremarks),

        //                        (IsDisplayOnly || isReadOnly
        //                            ? uc.QualityId
        //                            : (HTMLAutoCompleteOnBlur(uc.HeaderId, "txtQualityId", Convert.ToString(uc.QualityId), "UpdateTPData(this, "+ uc.TPHeaderId +", \""+ uc.TPStatus +"\");",false,"","QualityId",(string.Equals(uc.TPStatus,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) || uc.LatestOfferedSeqNo > 0 ?true:false),"","required")+""+Helper.GenerateHidden(uc.HeaderId,"QualityId", !string.IsNullOrEmpty(uc.QualityId) ? uc.QualityId.Split('-')[0] : ""))), //: (HTMLAutoCompleteOnBlur(uc.HeaderId, "txtQualityId", Convert.ToString(uc.QualityId), "UpdateTPData(this, "+ uc.TPHeaderId +", \""+ uc.TPStatus +"\");",false,"","QualityId",(string.Equals(uc.TPStatus,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)  || uc.TPStatus.ToLower() == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue().ToLower() || uc.LatestOfferedSeqNo > 0?true:false),"","required")+""+Helper.GenerateHidden(uc.HeaderId,"QualityId", !string.IsNullOrEmpty(uc.QualityId) ? uc.QualityId.Split('-')[0] : ""))),

        //                            uc.QualityIdRev == null ? Convert.ToString(uc.QualityIdRev) : "R"+Convert.ToString(uc.QualityIdRev),
        //                              (IsDisplayOnly || isReadOnly ||((uc.TPStatus == clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue() || uc.TPStatus ==clsImplementationEnum.TestPlanStatus.Returned.GetStringValue() ) && uc.TPRevNo == 0 )
        //                            ? (uc.PTCApplicable == true?"Yes":"No")
        //                            : Helper.GenerateDropdown(uc.HeaderId, "PTCApplicable", new SelectList(BoolenList, "Value", "Text", Convert.ToString(uc.PTCApplicable)),"","UpdateTPData(this, "+ uc.TPHeaderId +", \""+ uc.TPStatus +"\");",string.Equals(uc.TPStatus,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue()) || uc.LatestOfferedSeqNo > 0 ?true:false)) , //: Helper.GenerateDropdown(uc.HeaderId, "PTCApplicable", new SelectList(BoolenList, "Value", "Text", Convert.ToString(uc.PTCApplicable)),"","UpdateTPData(this, "+ uc.TPHeaderId +", \""+ uc.TPStatus +"\");",string.Equals(uc.TPStatus,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue())  || uc.TPStatus.ToLower() == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue().ToLower() || uc.LatestOfferedSeqNo > 0?true:false)) ,

        //                           (IsDisplayOnly || isReadOnly||((uc.TPStatus == clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue() || uc.TPStatus ==clsImplementationEnum.TestPlanStatus.Returned.GetStringValue() ) && uc.TPRevNo == 0 )
        //                            ? Convert.ToString(uc.PTCNumber)
        //                            :GenerateHTMLTextboxOnChanged(uc.HeaderId, "PTCNumber", Convert.ToString(uc.PTCNumber),"UpdateTPData(this, "+ uc.TPHeaderId +", \""+ uc.TPStatus +"\");", ( string.Equals(uc.TPStatus,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue()) || !(uc.PTCApplicable == true) || uc.LatestOfferedSeqNo > 0 )?true:false, "", false, "12")), //:GenerateHTMLTextboxOnChanged(uc.HeaderId, "PTCNumber", Convert.ToString(uc.PTCNumber),"UpdateTPData(this, "+ uc.TPHeaderId +", \""+ uc.TPStatus +"\");", ( string.Equals(uc.TPStatus,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue()) || uc.TPStatus.ToLower() == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue().ToLower()|| !(uc.PTCApplicable == true) || uc.LatestOfferedSeqNo > 0 )?true:false, "", false, "12")),

        //                          Convert.ToString(uc.TPHeaderId),
        //                          Convert.ToString(uc.TPStatus),
        //                          Convert.ToString("R" +uc.TPRevNo),
        //                        (IsDisplayOnly ? "" ://(IsDisplayOnly ? "test" :
        //                                HTMLActionString(uc.HeaderId, uc.Status, "Delete", "Delete Details", "fa fa-trash-o", "DeleteSeamDetails(" + uc.HeaderId + ","+uc.TPHeaderId+");",!(uc.Status == clsImplementationEnum.TestPlanStatus.Draft.GetStringValue() || uc.Status == clsImplementationEnum.TestPlanStatus.Returned.GetStringValue() || uc.TPStatus == clsImplementationEnum.TestPlanStatus.Draft.GetStringValue()|| uc.TPStatus == clsImplementationEnum.TestPlanStatus.Returned.GetStringValue()))
        //                                //+ HTMLActionString(uc.HeaderId, uc.Status, "Revise", "Revise Details", "fa fa-retweet", "ReviseSeamDetails(\""+uc.SeamNo+"\"," + uc.HeaderId + ","+uc.TPHeaderId+",\"" + uc.Status + "\",\""+uc.TPStatus+"\");",!(uc.Status == clsImplementationEnum.TestPlanStatus.Approved.GetStringValue() || uc.TPStatus == clsImplementationEnum.TestPlanStatus.Approved.GetStringValue()))+" "
        //                                + Manager.generateSeamHistoryButton(Project,uc.QualityProject, uc.SeamNo,"Seam Details")
        //                                + HTMLActionString(uc.HeaderId,uc.Status,"Test Plan Stages","Test Plan Stages","fa fa-newspaper-o","GetTestPlanLinePartial("+uc.TPHeaderId+",\""+uc.SeamNo+"\",true);")  //+ HTMLActionString(uc.HeaderId,uc.Status,"Test Plan Stages","Test Plan Stages","fa fa-newspaper-o","GetTestPlanLinePartial("+uc.TPHeaderId+",\""+uc.SeamNo+"\",true,\"swp\");") //+ HTMLActionString(uc.HeaderId,uc.Status,"Test Plan Stages","Test Plan Stages","fa fa-newspaper-o","GetTestPlanLinePartial("+uc.TPHeaderId+",\""+uc.SeamNo+"\",true,\"swp\",\""+uc.TPStatus+"\");")
        //                                + HTMLActionString(uc.HeaderId,uc.Status,"CopySWPID","Copy SWP","fa fa-files-o","fnCopy("+uc.HeaderId+","+uc.TPHeaderId+");",false)//!(uc.RevNo  > 0) && !(uc.RevNo==0 && uc.Status==strApproved) )
        //                                + HTMLActionString(uc.HeaderId,uc.Status,"btnHistory","History","fa fa-history","SWPHeaderHistory(\""+ uc.HeaderId +"\",\""+ uc.TPHeaderId +"\",\""+uc.SeamNo+"\");",false) //+ HTMLActionString(uc.HeaderId,uc.Status,"btnHistory","History","fa fa-history","SWPHeaderHistory(\""+ uc.HeaderId +"\",\""+uc.SeamNo+"\","+uc.TPHeaderId+");",false)
        //                                //+ HTMLActionString(uc.HeaderId,uc.Status,"TransferSWPID","Transfer SWP","fa fa-arrow-circle-right","fnTransfer(\""+ uc.HeaderId +"\");",!(uc.RevNo  > 0) && !(uc.RevNo==0 && uc.Status==strApproved))
        //                                +(uc.Status != clsImplementationEnum.UTCTQStatus.Approved.GetStringValue()?"":Helper.GenerateActionIcon(uc.HeaderId,"ReviseSeam","Revise","fa fa-repeat","CheckReviseSeam("+uc.HeaderId+")"))
        //                            )
        //                            + HTMLActionString(uc.HeaderId, uc.Status, "ParameterSWPID", "Parameter Slip", "fa fa-print", "ParameterSlip(\"" + uc.HeaderId + "\");")
        //                                +"<a class='' href='javascript:void(0)' onclick=ShowTimeline('/IPI/MaintainSWP/ShowTimeline?HeaderID="+Convert.ToInt32(uc.HeaderId)+"');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a>", //+"<a class='' href='javascript:void(0)' onclick=ShowTimeline('/IPI/MaintainSWP/ShowTimeline?HeaderID="+Convert.ToInt32(uc.HeaderId)+"&&tpheaderid="+Convert.ToInt32(uc.TPHeaderId)+"');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a>",
        //                         GetGeneralNotesDescriptions(list,uc.SWPNotes,true),
        //                        Convert.ToString(uc.WPSNo1TokenGenerate)+"#"+Convert.ToString(uc.WPSNo2TokenGenerate)+"#"+Convert.ToString(uc.WPSNo3TokenGenerate)+"#"+Convert.ToString(uc.IsAllPreHeatStageOffered)+"#"+Convert.ToString(uc.WeldVisualStageOffered)+"#"+Convert.ToString(uc.SeamRev)+"#"+Convert.ToString("R" + uc.RevNo) +"#"+Convert.ToString(uc.TPStatus) +"#"+Convert.ToString("R" + uc.TPRevNo) +"#"+Convert.ToString("R" + uc.QualityIdRev)

        //                   }).ToList();
        //        }
        //        return Json(new
        //        {
        //            aaData = data
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        return Json(new
        //        {
        //            aaData = new string[] { }
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //}

        [HttpPost]
        public JsonResult GetInlineEditableRow(int id, string qualityProj, string BU, string Location, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();
                string location = Location.Split('-')[0];
                var list = Manager.GetGeneralNotes(qualityProj, "", location).Select(i => new SelectItemList { id = i.NoteNumber.ToString(), text = i.NoteNumber.ToString() + "-" + i.NoteDescription.ToString() }).Distinct().ToList();

                if (id != 0)
                {
                    var IsDisplayOnly = !string.IsNullOrEmpty(Request["IsDisplayOnly"]) ? Convert.ToBoolean(Request["IsDisplayOnly"].ToString()) : false;

                    var lstResult = db.SP_SWP_Get_HeaderList
                                (
                                1, 0, "", "s10.HeaderId = " + id, BU, objClsLoginInfo.Location
                                ).Take(1).ToList();

                    var listSP_GET_ALL_PRE_HEAT_STAGE_SEAM_OFFERED = db.SP_GET_ALL_PRE_HEAT_STAGE_SEAM_OFFERED(qualityProj, BU, Location).ToList();
                    var listSP_SWP_GET_TOKEN_GENERATED_WPS_NUMBER = db.SP_SWP_GET_TOKEN_GENERATED_WPS_NUMBER(qualityProj, BU, Location).ToList();

                    var strApproved = clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue();
                    var listSP_SWP_Get_HeaderList_Ent = new List<SP_SWP_Get_HeaderList_Ent>();
                    lstResult.ForEach(i =>
                    {
                        //bool SeamOffered = false;
                        IsSeamOfferedCheckEnt objIsSeamOfferedCheckEnt = new IsSeamOfferedCheckEnt();
                        if (!IsDisplayOnly)
                        {
                            //SeamOffered = IsSeamOffered(i.HeaderId);
                            objIsSeamOfferedCheckEnt = CheckSeamIsOffered(listSP_GET_ALL_PRE_HEAT_STAGE_SEAM_OFFERED, listSP_SWP_GET_TOKEN_GENERATED_WPS_NUMBER, i, BU);
                        }
                        listSP_SWP_Get_HeaderList_Ent.Add(new SP_SWP_Get_HeaderList_Ent
                        {
                            AlternateWPS1 = i.AlternateWPS1,
                            AlternateWPS1RevNo = i.AlternateWPS1RevNo,
                            AlternateWPS2 = i.AlternateWPS2,
                            AlternateWPS2RevNo = i.AlternateWPS2RevNo,
                            DHTISR = i.DHTISR,
                            HeaderId = i.HeaderId,
                            //IsSeamOffered = SeamOffered,
                            JointType = i.JointType,
                            Location = i.Location,
                            MainWPSNumber = i.MainWPSNumber,
                            MainWPSRevNo = i.MainWPSRevNo,
                            Preheat = i.Preheat,
                            QualityProject = i.QualityProject,
                            Returnremarks = i.Returnremarks,
                            RevNo = i.RevNo,
                            ROW_NO = i.ROW_NO,
                            SeamDescription = i.SeamDescription,
                            SeamNo = i.SeamNo,
                            SeamRev = i.SeamRev,
                            selectedJointType = i.selectedJointType,
                            Status = i.Status,
                            SWPNotes = i.SWPNotes,
                            TotalCount = i.TotalCount,
                            WPSNumbers1 = i.WPSNumbers1,
                            WPSNumbers2 = i.WPSNumbers2,
                            WPSNumbers3 = i.WPSNumbers3,
                            MainWPSDisabled = (objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered || objIsSeamOfferedCheckEnt.WPSNo1TokenGenerate),
                            AlternativeWPS1Disabled = (objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered || objIsSeamOfferedCheckEnt.WPSNo2TokenGenerate),
                            AlternativeWPS2Disabled = (objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered || objIsSeamOfferedCheckEnt.WPSNo3TokenGenerate),
                            PWHTAndNotesDisabled = (objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered || objIsSeamOfferedCheckEnt.WeldVisualStageOffered),
                            JointTypeDisabled = (objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered || objIsSeamOfferedCheckEnt.WPSNo1TokenGenerate || objIsSeamOfferedCheckEnt.WPSNo2TokenGenerate || objIsSeamOfferedCheckEnt.WPSNo3TokenGenerate),
                            IsAllPreHeatStageOffered = objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered,
                            WPSNo1TokenGenerate = objIsSeamOfferedCheckEnt.WPSNo1TokenGenerate,
                            WPSNo2TokenGenerate = objIsSeamOfferedCheckEnt.WPSNo2TokenGenerate,
                            WPSNo3TokenGenerate = objIsSeamOfferedCheckEnt.WPSNo3TokenGenerate,
                            WeldVisualStageOffered = objIsSeamOfferedCheckEnt.WeldVisualStageOffered
                        });
                    });
                    data = (from uc in listSP_SWP_Get_HeaderList_Ent
                            select new[]
                           {
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//Convert.ToString(uc.HeaderId),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//uc.SeamNo,
                                (IsDisplayOnly || isReadOnly ||  (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue().ToLower())
                                ?  uc.selectedJointType
                                : (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue().ToLower() || uc.JointTypeDisabled)? GenerateAutoComplete(uc.HeaderId, "JointType",uc.selectedJointType,"\"Updateswp(this,"+uc.HeaderId+",'"+uc.Status+"');\"", true,"","JointType")+""+Helper.GenerateHidden(uc.HeaderId, "hdnJointType") : GenerateAutoComplete(uc.HeaderId, "JointType",uc.selectedJointType,"\"Updateswp(this,"+uc.HeaderId+",'"+uc.Status+"');\"", false,"","JointType", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnJointType")),//"width: 115px !important;"
                                (IsDisplayOnly || isReadOnly ||  (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue().ToLower())
                                ? (IsDisplayOnly?GetWPSHistoryLink(uc.MainWPSNumber,uc.MainWPSRevNo): uc.MainWPSNumber)
                                : (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue().ToLower() || uc.MainWPSDisabled || String.IsNullOrEmpty(uc.selectedJointType))? GenerateAutoComplete(uc.HeaderId, "MainWPSNumber",uc.MainWPSNumber,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"",true,"","MainWPSNumber")+""+Helper.GenerateHidden(uc.HeaderId, "hdnMainWPSNumber") : GenerateAutoComplete(uc.HeaderId, "MainWPSNumber",uc.MainWPSNumber,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"",  false,"","MainWPSNumber", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnMainWPSNumber")),//"width:160px !important;padding-left:5px;"
                                (IsDisplayOnly || isReadOnly ||  (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue().ToLower())
                                ?  uc.MainWPSRevNo.ToString()
                                : GenerateHTMLTextbox(uc.HeaderId,"MainWPSRevNo",uc.MainWPSRevNo.ToString(),"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');", true,"", false)),
                                (IsDisplayOnly || isReadOnly ||  (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue().ToLower())
                                ?  (IsDisplayOnly?GetWPSHistoryLink(uc.AlternateWPS1,uc.AlternateWPS1RevNo): uc.AlternateWPS1)
                                : (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue().ToLower()|| uc.AlternativeWPS1Disabled || String.IsNullOrEmpty(uc.MainWPSNumber))? GenerateAutoComplete(uc.HeaderId, "AlternateWPS1",uc.AlternateWPS1,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", true,"","AlternateWPS1", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnAlternateWPS1"):GenerateAutoComplete(uc.HeaderId, "AlternateWPS1",uc.AlternateWPS1,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", false,"","AlternateWPS1", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnAlternateWPS1")),
                                (IsDisplayOnly || isReadOnly ||  (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue().ToLower())
                                ?  uc.AlternateWPS1RevNo.ToString()
                                : GenerateHTMLTextbox(uc.HeaderId,"AlternateWPS1RevNo",uc.AlternateWPS1RevNo.ToString(),"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');", true,"", false,"25")),
                                (IsDisplayOnly || isReadOnly ||  (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue().ToLower())
                                ?  (IsDisplayOnly?GetWPSHistoryLink(uc.AlternateWPS2,uc.MainWPSRevNo): uc.AlternateWPS2)
                                : (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue().ToLower()|| uc.AlternativeWPS2Disabled || String.IsNullOrEmpty(uc.AlternateWPS1))?GenerateAutoComplete(uc.HeaderId, "AlternateWPS2",uc.AlternateWPS2,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", true,"","AlternateWPS2", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnAlternateWPS2"):GenerateAutoComplete(uc.HeaderId, "AlternateWPS2",uc.AlternateWPS2,"\"Updateswp(this, "+ uc.HeaderId +",'"+ uc.Status +"');\"", false,"","AlternateWPS2", false)+""+Helper.GenerateHidden(uc.HeaderId, "hdnAlternateWPS2")),
                                (IsDisplayOnly || isReadOnly ||  (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue().ToLower())
                                ?  uc.AlternateWPS2RevNo.ToString()
                                : GenerateHTMLTextbox(uc.HeaderId,"AlternateWPS2RevNo",uc.AlternateWPS2RevNo.ToString(),"UpdateData(this, "+ uc.HeaderId +",'"+ uc.Status +"');", true,"",false)),
                                (IsDisplayOnly || isReadOnly ||  (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue().ToLower())
                                ?  Convert.ToString(uc.Preheat)
                                :
                                ( uc.JointType != null && uc.JointType.ToUpper() == "BUTTERING" ) ? GenerateTextbox(uc.HeaderId, "Preheat", Convert.ToString(uc.Preheat), "Updateswp(this, " + uc.HeaderId + ",'" + uc.Status + "');", "return isNumber(event)", "", false, "", "10") :
                                ((uc.Status == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue() || uc.PWHTAndNotesDisabled || String.IsNullOrEmpty(uc.MainWPSNumber)
                                ? GenerateTextbox(uc.HeaderId, "Preheat", Convert.ToString(uc.Preheat), "Updateswp(this, " + uc.HeaderId + ",'" + uc.Status + "');", "return isNumber(event)", "", true, "", "10")
                                : GenerateTextbox(uc.HeaderId, "Preheat", Convert.ToString(uc.Preheat), "Updateswp(this, " + uc.HeaderId + ",'" + uc.Status + "');", "return isNumber(event)", "", false, "", "10")))
                                ),
                                
                                (IsDisplayOnly || isReadOnly || (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue().ToLower())
                                ? Convert.ToString(uc.DHTISR)
                                : (uc.Status == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue() || uc.PWHTAndNotesDisabled || String.IsNullOrEmpty(uc.MainWPSNumber) ? GenerateAutoComplete(uc.HeaderId, "DHTISR", uc.DHTISR, "\"Updateswp(this, " + uc.HeaderId + ",'" + uc.Status + "');\"", true, "", "DHTISR", false) + "" + Helper.GenerateHidden(uc.HeaderId, "hdnDHTISR") : GenerateAutoComplete(uc.HeaderId, "DHTISR", uc.DHTISR, "\"Updateswp(this, " + uc.HeaderId + ",'" + uc.Status + "');\"", false, "", "DHTISR", false) + "" + Helper.GenerateHidden(uc.HeaderId, "hdnDHTISR"))),
                                (IsDisplayOnly || isReadOnly || (uc.Status.ToLower() == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue().ToLower())
                                ? uc.SWPNotes//GetGeneralNotesDescriptions(list,uc.SWPNotes)
                                : (uc.Status == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue() || uc.PWHTAndNotesDisabled ?
                                 //MultiSelectDropdown(uc.HeaderId,list,uc.SWPNotes,true,"Updateswp(this,'"+Convert.ToString(uc.HeaderId)+"','"+ uc.Status +"');","SWPNotes") 
                                 //: MultiSelectDropdown(uc.HeaderId,list,uc.SWPNotes, false, "Updateswp(this,'"+Convert.ToString(uc.HeaderId)+"','"+ uc.Status +"');", "SWPNotes"))),
                                 GeneralNotes(uc.HeaderId, uc.Status, "SWPNotes", uc.SWPNotes, "\"Updateswp(this, " + uc.HeaderId + ",'" + uc.Status + "');\"", true, "", "SWPNotes", false, uc.PWHTAndNotesDisabled)
                                : GeneralNotes(uc.HeaderId, uc.Status, "SWPNotes", uc.SWPNotes, "\"Updateswp(this, " + uc.HeaderId + ",'" + uc.Status + "');\"", false, "", "SWPNotes", false, uc.PWHTAndNotesDisabled))),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//Convert.ToString(uc.RovNo),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//Convert.ToString(uc.Status),
                                Convert.ToString(uc.Status),
                                Convert.ToString(uc.Returnremarks),
                                (IsDisplayOnly ? "" :
                                          HTMLActionString(uc.HeaderId, uc.Status, "Delete", "Delete Details", "fa fa-trash-o", "DeleteSeamDetails(" + uc.HeaderId + ");", !IsDeleteApplicable(uc.Status, uc.RevNo, uc.SeamNo, uc.QualityProject))
                                        + HTMLActionString(uc.HeaderId, uc.Status, "CopySWPID", "Copy SWP", "fa fa-files-o", "fnCopy(" + uc.HeaderId + ");", string.IsNullOrWhiteSpace(uc.MainWPSNumber))//!(uc.RevNo  > 0) && !(uc.RevNo==0 && uc.Status==strApproved) )
                                        + HTMLActionString(uc.HeaderId, uc.Status, "btnHistory", "History", "fa fa-history", "SWPHeaderHistory(\"" + uc.HeaderId + "\",\"" + uc.SeamNo + "\");", !(uc.RevNo > 0))
                                        + HTMLActionString(uc.HeaderId, uc.Status, "TransferSWPID", "Transfer SWP", "fa fa-arrow-circle-right", "fnTransfer(\"" + uc.HeaderId + "\");", !(uc.RevNo > 0) && !(uc.RevNo == 0 && uc.Status == strApproved))
                                        + (uc.Status != clsImplementationEnum.UTCTQStatus.Approved.GetStringValue() ? "" : Helper.GenerateActionIcon(uc.HeaderId, "ReviseSeam", "Revise", "fa fa-repeat", "CheckReviseSeam(" + uc.HeaderId + ")"))
                                    ) + HTMLActionString(uc.HeaderId, uc.Status, "ParameterSWPID", "Parameter Slip", "fa fa-print", "ParameterSlip(\"" + uc.HeaderId + "\");")
                                        + "<a class='' href='javascript:void(0)' onclick=ShowTimeline('/IPI/MaintainSWP/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a>",
                                 GetGeneralNotesDescriptions(list, uc.SWPNotes, true),
                                Convert.ToString(uc.WPSNo1TokenGenerate) + "#" + Convert.ToString(uc.WPSNo2TokenGenerate) + "#" + Convert.ToString(uc.WPSNo3TokenGenerate) + "#" + Convert.ToString(uc.IsAllPreHeatStageOffered) + "#" + Convert.ToString(uc.WeldVisualStageOffered) + "#" + Convert.ToString(uc.SeamRev)
                           }).ToList();
            }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
        }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
}, JsonRequestBehavior.AllowGet);
            }
        }

        //[NonAction]
        //public static string GenerateHTMLTextboxOnChanged(int rowId, string columnName, string columnValue = "", string onChangeMethod = "", bool isReadOnly = false, string inputStyle = "", bool disabled = false, string maxlength = "")
        //{
        //    string htmlControl = "";

        //    string inputID = columnName + "" + rowId.ToString();
        //    string inputName = columnName;
        //    string inputValue = columnValue;
        //    string className = "form-control";
        //    string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
        //    string onBlurEvent = !string.IsNullOrEmpty(onChangeMethod) ? "onchange='" + onChangeMethod + "'" : "";

        //    htmlControl = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' " + MaxCharcters + " value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + (disabled ? "disabled" : "") + " />";

        //    return htmlControl;
        //}


        //public string HTMLAutoCompleteOnBlur(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string maxlength = "", string validate = "")
        //{
        //    string strAutoComplete = string.Empty;

        //    string inputID = columnName + "" + rowId.ToString();
        //    string hdElementId = hdElement + "" + rowId.ToString();
        //    string inputName = columnName;
        //    string inputValue = columnValue;
        //    string className = "autocomplete form-control";
        //    string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
        //    string FieldValidate = !string.IsNullOrEmpty(validate) ? "validate='" + validate + "'" : "";
        //    string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";

        //    strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' hdElement='" + hdElementId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + (disabled ? "disabled" : "") + " " + MaxCharcters + " " + FieldValidate + " />";

        //    return strAutoComplete;
        //}
        public bool IsDeleteApplicable(string Status, int? revNo, string seamno, string qproject)
{
    bool IsDeleteApplicable = false;
    if (Status == clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue() || Status == clsImplementationEnum.QualityIdHeaderStatus.Returned.GetStringValue())
    {
        IsDeleteApplicable = true;
        if (db.WPS025.Where(x => x.QualityProject == qproject && x.SeamNo == seamno).Any())
        {
            IsDeleteApplicable = false;
        }
    }
    return IsDeleteApplicable;
}
public string GetWPSHistoryLink(string WPSNo, int? WPSRevNo)
{
    var link = "";
    if (!string.IsNullOrEmpty(WPSNo) && WPSRevNo.HasValue)
    {
        var objHistory = db.WPS010_Log.Where(i => i.WPSNumber == WPSNo && i.WPSRevNo == WPSRevNo.Value).FirstOrDefault();
        if (objHistory != null)
        {
            link = "<a href=\"/WPS/MaintainWPSHeader/ViewHistory?id=" + objHistory.Id + "\" target=\"_blank\">" + WPSNo + "</a>";
        }
        else
        {
            link = WPSNo;
        }
    }
    else
    {
        link = WPSNo;
    }
    return link;
}

public string GetWPSHistoryPrint(string WPSNo, int? WPSRevNo, string Type)
{
    var link = "";
    if (!string.IsNullOrEmpty(WPSNo) && WPSRevNo.HasValue)
    {
        var objHistory = db.WPS010_Log.Where(i => i.WPSNumber == WPSNo && i.WPSRevNo == WPSRevNo.Value).FirstOrDefault();
        if (objHistory != null)
        {
            link = "<a href=\"javascript:void(0)\" title=\"Print\" onclick=\"PrintPDIN(" + objHistory.Id + ",'" + Type + "')\">" + WPSNo + "</a>";
        }
        else
        {
            link = WPSNo;
        }
    }
    else
    {
        link = WPSNo;
    }
    return link;
}

public JsonResult CheckSeamIsApplicableForRevise(int HeaderId)
{
    var IsApplicable = true;
    clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
    try
    {
        var objSWP010 = db.SWP010.Where(i => i.HeaderId == HeaderId).FirstOrDefault();
        var lstResult = db.SP_SWP_Get_HeaderList
                           (
                           1, 0, "", "s10.HeaderId = " + HeaderId, objSWP010.BU, objClsLoginInfo.Location
                           ).Take(1).FirstOrDefault();

        var listSP_GET_ALL_PRE_HEAT_STAGE_SEAM_OFFERED = db.SP_GET_ALL_PRE_HEAT_STAGE_SEAM_OFFERED(objSWP010.QualityProject, objSWP010.BU, objSWP010.Location).Where(i => i.SeamNo == objSWP010.SeamNo).ToList();
        var listSP_SWP_GET_TOKEN_GENERATED_WPS_NUMBER = db.SP_SWP_GET_TOKEN_GENERATED_WPS_NUMBER(objSWP010.QualityProject, objSWP010.BU, objSWP010.Location).Where(i => i.SeamNo == objSWP010.SeamNo).ToList();
        IsSeamOfferedCheckEnt objIsSeamOfferedCheckEnt = new IsSeamOfferedCheckEnt();
        objIsSeamOfferedCheckEnt = CheckSeamIsOffered(listSP_GET_ALL_PRE_HEAT_STAGE_SEAM_OFFERED, listSP_SWP_GET_TOKEN_GENERATED_WPS_NUMBER, lstResult, objSWP010.BU);

        var MainWPSDisabled = (objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered || objIsSeamOfferedCheckEnt.WPSNo1TokenGenerate);
        var AlternativeWPS1Disabled = (objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered || objIsSeamOfferedCheckEnt.WPSNo2TokenGenerate);
        var AlternativeWPS2Disabled = (objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered || objIsSeamOfferedCheckEnt.WPSNo3TokenGenerate);
        var PWHTAndNotesDisabled = (objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered || objIsSeamOfferedCheckEnt.WeldVisualStageOffered);
        var JointTypeDisabled = (objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered || objIsSeamOfferedCheckEnt.WPSNo1TokenGenerate || objIsSeamOfferedCheckEnt.WPSNo2TokenGenerate || objIsSeamOfferedCheckEnt.WPSNo3TokenGenerate);

        if (MainWPSDisabled && AlternativeWPS1Disabled && AlternativeWPS2Disabled && PWHTAndNotesDisabled && JointTypeDisabled)
        {
            IsApplicable = false;
        }
        if (objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered)
        {
            objResponseMsg.Status = "All Pre-Heat Stages Are Offered So Not Allow To Revise Seam.";
        }
        else if (MainWPSDisabled && AlternativeWPS1Disabled && AlternativeWPS2Disabled && PWHTAndNotesDisabled)
        {
            objResponseMsg.Status = "Weld Visual Is Offered And Token Generate For All WPS So Not Allow To Revise Seam.";
        }
        else if (!IsApplicable)
        {
            objResponseMsg.Status = "You Can Not Revise This Due To All Pre Heat Stages Are Offered Or Weld Visual Stage Is Offered.";
        }
        objResponseMsg.Key = IsApplicable;
    }
    catch (Exception ex)
    {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        objResponseMsg.Key = false;
        objResponseMsg.Value = ex.Message;

    }
    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
}
public IsSeamOfferedCheckEnt CheckSeamIsOffered(List<SP_GET_ALL_PRE_HEAT_STAGE_SEAM_OFFERED_Result> listSP_GET_ALL_PRE_HEAT_STAGE_SEAM_OFFERED, List<SP_SWP_GET_TOKEN_GENERATED_WPS_NUMBER_Result> listSP_SWP_GET_TOKEN_GENERATED_WPS_NUMBER, SP_SWP_Get_HeaderList_Result item, string BU)
{
    var objIsSeamOfferedCheckEnt = new IsSeamOfferedCheckEnt();
    if (listSP_GET_ALL_PRE_HEAT_STAGE_SEAM_OFFERED.Any(i => i.SeamNo == item.SeamNo && i.QualityProject == item.QualityProject && i.BU == (string.IsNullOrEmpty(BU) ? i.BU : BU) && i.Location == item.Location && i.IsAllPreHeatStageOffered.HasValue && i.IsAllPreHeatStageOffered.Value))
    {
        objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered = true;
        objIsSeamOfferedCheckEnt.WPSNo1TokenGenerate = true;
        objIsSeamOfferedCheckEnt.WPSNo2TokenGenerate = true;
        objIsSeamOfferedCheckEnt.WPSNo3TokenGenerate = true;
        objIsSeamOfferedCheckEnt.WeldVisualStageOffered = true;
    }
    else if (listSP_GET_ALL_PRE_HEAT_STAGE_SEAM_OFFERED.Any(i => i.SeamNo == item.SeamNo && i.QualityProject == item.QualityProject && i.BU == (string.IsNullOrEmpty(BU) ? i.BU : BU) && i.Location == item.Location && i.IsAllPreHeatStageOffered.HasValue && i.IsAllPreHeatStageOffered.Value == false && i.WeldVisualStageOffered.HasValue && i.WeldVisualStageOffered.Value))
    {
        objIsSeamOfferedCheckEnt.WeldVisualStageOffered = true;
    }

    if (!objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered)
    {
        if (listSP_SWP_GET_TOKEN_GENERATED_WPS_NUMBER.Any(i => i.SeamNo == item.SeamNo && i.QualityProject == item.QualityProject && i.BU == (string.IsNullOrEmpty(BU) ? i.BU : BU) && i.Location == item.Location && i.WPSNumber == item.MainWPSNumber && i.WPSRevNo == item.MainWPSRevNo))
        {
            objIsSeamOfferedCheckEnt.WPSNo1TokenGenerate = true;
        }
        if (listSP_SWP_GET_TOKEN_GENERATED_WPS_NUMBER.Any(i => i.SeamNo == item.SeamNo && i.QualityProject == item.QualityProject && i.BU == (string.IsNullOrEmpty(BU) ? i.BU : BU) && i.Location == item.Location && i.WPSNumber == item.AlternateWPS1 && i.WPSRevNo == item.AlternateWPS1RevNo))
        {
            objIsSeamOfferedCheckEnt.WPSNo2TokenGenerate = true;
        }
        if (listSP_SWP_GET_TOKEN_GENERATED_WPS_NUMBER.Any(i => i.SeamNo == item.SeamNo && i.QualityProject == item.QualityProject && i.BU == (string.IsNullOrEmpty(BU) ? i.BU : BU) && i.Location == item.Location && i.WPSNumber == item.AlternateWPS2 && i.WPSRevNo == item.AlternateWPS2RevNo))
        {
            objIsSeamOfferedCheckEnt.WPSNo3TokenGenerate = true;
        }
    }
    return objIsSeamOfferedCheckEnt;
}

public ActionResult GeneralNotesGridDataPartial(int GNHeaderId, string ExistingNotes, bool isReadonly = false)
{
    ViewBag.GNHeaderId = GNHeaderId;
    ViewBag.ExistingNotes = ExistingNotes;
    //ViewBag.isReadonly = isReadonly;
    var objSWP010 = db.SWP010.Where(i => i.HeaderId == GNHeaderId).FirstOrDefault();
    var seamdtl = db.SP_SWP_Get_HeaderList
                       (
                       0, 0, "", "s10.HeaderId=" + GNHeaderId, objSWP010.BU, objSWP010.Location //0, 0, "", "s10.HeaderId=" + GNHeaderId, objSWP010.BU, objSWP010.Location
                       ).FirstOrDefault();
    IsSeamOfferedCheckEnt objIsSeamOfferedCheckEnt = new IsSeamOfferedCheckEnt();
    var listSP_GET_ALL_PRE_HEAT_STAGE_SEAM_OFFERED = db.SP_GET_ALL_PRE_HEAT_STAGE_SEAM_OFFERED(objSWP010.QualityProject, objSWP010.BU, objSWP010.Location).ToList();
    var listSP_SWP_GET_TOKEN_GENERATED_WPS_NUMBER = db.SP_SWP_GET_TOKEN_GENERATED_WPS_NUMBER(objSWP010.QualityProject, objSWP010.BU, objSWP010.Location).ToList();
    objIsSeamOfferedCheckEnt = CheckSeamIsOffered(listSP_GET_ALL_PRE_HEAT_STAGE_SEAM_OFFERED, listSP_SWP_GET_TOKEN_GENERATED_WPS_NUMBER, seamdtl, objSWP010.BU);
    var PWHTAndNotesDisabled = (objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered || objIsSeamOfferedCheckEnt.WeldVisualStageOffered);
    ViewBag.isReadonly = (seamdtl.Status == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue() || PWHTAndNotesDisabled || String.IsNullOrEmpty(seamdtl.MainWPSNumber) ? true : false);

    return PartialView("_GeneralNotesGridDataPartial");
}
[HttpPost]
public JsonResult LoadGenerealNotesData(JQueryDataTableParamModel param)
{
    try
    {
        var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
        var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
        int StartIndex = param.iDisplayStart + 1;
        int EndIndex = param.iDisplayStart + param.iDisplayLength;
        var user = objClsLoginInfo.UserName;
        //int headerid = Convert.ToInt32(param.Headerid);

        string strWhere = string.Empty;
        string QualityProject = param.Project;
        string bu = param.BU;
        string location = param.Location;

        if (!string.IsNullOrWhiteSpace(param.sSearch))
        {
            strWhere = " (NoteNumber like '%" + param.sSearch
                 + "%' or NoteDescription like '%" + param.sSearch
                 + "%')";
        }
        else
        {
            strWhere = " 1=1";
        }
        string strSortOrder = string.Empty;
        string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
        string sortDirection = Convert.ToString(Request["sSortDir_0"]);

        if (!string.IsNullOrWhiteSpace(sortColumnName))
        {
            strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
        }
        var lstResult = db.SP_SWP_GET_GENERAL_NOTES
                        (
                        StartIndex, EndIndex, strSortOrder, QualityProject, bu, location, strWhere
                        ).ToList();

        var data = (from uc in lstResult
                    select new[]
                   {
                               Convert.ToString(uc.NoteNumber),
                               Convert.ToString(uc.NoteNumber),
                               Convert.ToString(uc.NoteDescription),
                           }).ToList();

        return Json(new
        {
            sEcho = Convert.ToInt32(param.sEcho),
            iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
            iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
            aaData = data
        }, JsonRequestBehavior.AllowGet);
    }
    catch (Exception ex)
    {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

        return Json(new
        {
            sEcho = param.sEcho,
            iTotalRecords = "0",
            iTotalDisplayRecords = "0",
            aaData = ""
        }, JsonRequestBehavior.AllowGet);
    }
}
public string GetGeneralNotesDescriptions(List<SelectItemList> list, string SWPNotes, bool isToolTip = false)
{
    if (!string.IsNullOrEmpty(SWPNotes))
    {
        var NotesArray = SWPNotes.Split(',');
        var lst = list.Where(i => NotesArray.Contains(i.id)).Select(i => i.text).ToArray();
        if (isToolTip)
        {
            return string.Join("\n", lst);
        }
        else
        {
            return string.Join(",", lst);
        }
    }
    else
    {
        return "";
    }
}

[HttpPost]
public ActionResult UpdateData(string headerId, string columnName, string columnValue)
{
    clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
    try
    {
        int HeaderId = Convert.ToInt32(headerId);
        SWP010 objSWP010 = db.SWP010.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
        if (objSWP010 == null)
        {
            objResponseMsg.Key = false;
            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error;
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        //if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
        if (objSWP010 != null ? IsApplicable(objSWP010.Status, false) : true)
        {
            if (string.IsNullOrWhiteSpace(objSWP010.CreatedBy))
            {
                objSWP010.CreatedBy = objClsLoginInfo.UserName;
                objSWP010.CreatedOn = DateTime.Now;
                db.SaveChanges();
            }
            if (!string.IsNullOrEmpty(columnName))
            {
                if (columnName == "MainWPSNumber")
                {
                    if (!string.IsNullOrEmpty(columnValue))
                    {
                        var values = columnValue.Split('|');
                        db.SP_PLN_UPDATE_CHECKLIST_TABLE_COLUMN("SWP010", "HeaderId", headerId, columnName, values[1]);
                        db.SP_PLN_UPDATE_CHECKLIST_TABLE_COLUMN("SWP010", "HeaderId", headerId, "MainWPSRevNo", values[0]);
                    }
                    else
                    {
                        objSWP010.MainWPSNumber = null;
                        objSWP010.MainWPSRevNo = null;
                        objSWP010.AlternateWPS1 = null;
                        objSWP010.AlternateWPS1RevNo = null;
                        objSWP010.AlternateWPS2 = null;
                        objSWP010.AlternateWPS2RevNo = null;
                        objSWP010.Preheat = null;
                        objSWP010.DHTISR = null;
                        objSWP010.SWPNotes = null;
                        db.SaveChanges();
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                }
                else if (columnName == "AlternateWPS1")
                {
                    if (!string.IsNullOrEmpty(columnValue))
                    {
                        var values = columnValue.Split('|');
                        db.SP_PLN_UPDATE_CHECKLIST_TABLE_COLUMN("SWP010", "HeaderId", headerId, columnName, values[1]);
                        db.SP_PLN_UPDATE_CHECKLIST_TABLE_COLUMN("SWP010", "HeaderId", headerId, "AlternateWPS1RevNo", values[0]);
                    }
                    else
                    {
                        objSWP010.AlternateWPS1 = null;
                        objSWP010.AlternateWPS1RevNo = null;
                        objSWP010.AlternateWPS2 = null;
                        objSWP010.AlternateWPS2RevNo = null;
                        db.SaveChanges();
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                }
                else if (columnName == "AlternateWPS2")
                {
                    if (!string.IsNullOrEmpty(columnValue))
                    {
                        var values = columnValue.Split('|');
                        db.SP_PLN_UPDATE_CHECKLIST_TABLE_COLUMN("SWP010", "HeaderId", headerId, columnName, values[1]);
                        db.SP_PLN_UPDATE_CHECKLIST_TABLE_COLUMN("SWP010", "HeaderId", headerId, "AlternateWPS2RevNo", values[0]);
                    }
                    else
                    {
                        objSWP010.AlternateWPS2 = null;
                        objSWP010.AlternateWPS2RevNo = null;
                        db.SaveChanges();
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                }
                else if (columnName == "JointType")
                {
                    if (!string.IsNullOrEmpty(columnValue))
                    {
                        var glb002 = db.GLB002.Where(m => m.Category == 74 && m.Code == columnValue).FirstOrDefault();
                        if (glb002 != null)
                        {
                            db.SP_PLN_UPDATE_CHECKLIST_TABLE_COLUMN("SWP010", "HeaderId", headerId, columnName, columnValue);
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                        }
                        else
                        {

                            objResponseMsg.Key = false;
                            //objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                        }
                    }
                    else
                    {
                        //db.SP_PLN_UPDATE_CHECKLIST_TABLE_COLUMN("SWP010", "HeaderId", headerId, columnName, columnValue);
                        objSWP010.JointType = null;
                        objSWP010.MainWPSNumber = null;
                        objSWP010.MainWPSRevNo = null;
                        objSWP010.AlternateWPS1 = null;
                        objSWP010.AlternateWPS1RevNo = null;
                        objSWP010.AlternateWPS2 = null;
                        objSWP010.AlternateWPS2RevNo = null;
                        objSWP010.Preheat = null;
                        objSWP010.DHTISR = null;
                        objSWP010.SWPNotes = null;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                    }
                }
                //  DHTISR
                else
                {
                    db.SP_PLN_UPDATE_CHECKLIST_TABLE_COLUMN("SWP010", "HeaderId", headerId, columnName, columnValue);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                }
            }
        }
        else
        {
            objResponseMsg.Key = true;
            objResponseMsg.Remarks = clsImplementationMessage.CommonMessages.NotInEditStatus;
        }
    }
    catch (Exception ex)
    {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        objResponseMsg.Key = false;
        objResponseMsg.Value = ex.Message;

    }
    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
}

[HttpPost]
public ActionResult ReviseData(int headerId)
{
    clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
    try
    {
        SWP010 objSWP010 = db.SWP010.Where(x => x.HeaderId == headerId).FirstOrDefault();
        if (objSWP010.Status == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue())
        {
            objSWP010.RevNo = Convert.ToInt32(objSWP010.RevNo) + 1;
            objSWP010.Status = clsImplementationEnum.UTCTQStatus.DRAFT.GetStringValue();
            objSWP010.EditedBy = null;
            objSWP010.EditedOn = null;
            objSWP010.CreatedBy = objClsLoginInfo.UserName;
            objSWP010.CreatedOn = DateTime.Now;
            objSWP010.ApprovedBy = null;
            objSWP010.ApprovedOn = null;
            objSWP010.ReturnedBy = null;
            objSWP010.ReturnedOn = null;
            objSWP010.SubmittedBy = null;
            objSWP010.SubmittedOn = null;
            objSWP010.Returnremarks = null;
            db.SaveChanges();
            objResponseMsg.HeaderId = objSWP010.HeaderId;
            objResponseMsg.Status = objSWP010.Status;
            objResponseMsg.RevNo = Convert.ToString(objSWP010.RevNo);
        }
        objResponseMsg.Key = true;
        objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
    }
    catch (Exception ex)
    {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        objResponseMsg.Key = false;
        objResponseMsg.Value = ex.Message;

    }
    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
}

//[HttpPost]
//public ActionResult ReviseSWPTPData(int swpheaderId, int tpheaderid, string reviseto)
//{
//    clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
//    //clsHelper.ResponseMsgWithStatus objResponseMsgswp = new clsHelper.ResponseMsgWithStatus();

//    try
//    {
//        bool isSWPRevised = false;
//        bool isTPRevised = false;
//        if (reviseto == "SWP" || reviseto == "BOTH")
//        {
//            SWP010 objSWP010 = db.SWP010.Where(x => x.HeaderId == swpheaderId).FirstOrDefault();
//            if (objSWP010 != null)
//            {
//                //objResponseMsgswp = isReviseApplicable(swpheaderId);
//                //if (objResponseMsgswp.Key)
//                //{
//                    if (objSWP010.Status == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue())
//                    {
//                        objSWP010.RevNo = Convert.ToInt32(objSWP010.RevNo) + 1;
//                        objSWP010.Status = clsImplementationEnum.UTCTQStatus.DRAFT.GetStringValue();
//                        objSWP010.EditedBy = objClsLoginInfo.UserName;
//                        objSWP010.EditedOn = DateTime.Now;
//                        objSWP010.CreatedBy = objClsLoginInfo.UserName;
//                        objSWP010.CreatedOn = DateTime.Now;
//                        objSWP010.ApprovedBy = null;
//                        objSWP010.ApprovedOn = null;
//                        objSWP010.ReturnedBy = null;
//                        objSWP010.ReturnedOn = null;
//                        objSWP010.SubmittedBy = null;
//                        objSWP010.SubmittedOn = null;
//                        objSWP010.Returnremarks = null;
//                        isSWPRevised = true;
//                    }
//                    else { objResponseMsg.dataValue = "This shop weld plan is not approved, it cannot be revise now!!"; }
//                //}
//                //else
//                //{
//                //    objResponseMsg.dataValue += objResponseMsgswp.dataValue;
//                //}
//            }
//        }
//        if (reviseto == "TP" || reviseto == "BOTH")
//        {
//            QMS020 objQMS020 = db.QMS020.Where(c => c.HeaderId == tpheaderid).FirstOrDefault();
//            if (objQMS020 != null)
//            {
//                if (string.Equals(objQMS020.Status, clsImplementationEnum.TestPlanStatus.Approved.GetStringValue(), StringComparison.OrdinalIgnoreCase))
//                {
//                    objQMS020.Status = clsImplementationEnum.TestPlanStatus.Draft.GetStringValue();
//                    objQMS020.RevNo = objQMS020.RevNo + 1;
//                    objQMS020.EditedBy = objClsLoginInfo.UserName;
//                    objQMS020.EditedOn = DateTime.Now;

//                    objQMS020.SubmittedBy = null;
//                    objQMS020.SubmittedOn = null;
//                    objQMS020.ApprovedBy = null;
//                    objQMS020.ApprovedOn = null;
//                    objQMS020.ReturnedBy = null;
//                    objQMS020.ReturnedOn = null;
//                    objQMS020.ReturnRemark = null;
//                    if (objQMS020.QMS021.Any())
//                    {
//                        objQMS020.QMS021.ToList().ForEach(i => i.RevNo = objQMS020.RevNo);
//                    }
//                    isTPRevised = true;
//                }
//                else
//                {
//                    objResponseMsg.dataValue += "  This test plan is not approved, it cannot be revise now!!";
//                }
//            }
//        }
//        db.SaveChanges();

//        if (isSWPRevised || isTPRevised)
//        {
//            objResponseMsg.Key = true;
//            objResponseMsg.Value += "Revision has been generated successfully" + (!(isSWPRevised && isTPRevised) ? ((isSWPRevised ? " for shop weld plan" : "") + (isTPRevised ? " for test plan" : "")) : "");
//        }
//    }
//    catch (Exception ex)
//    {
//        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
//        objResponseMsg.Key = false;
//        objResponseMsg.Value = ex.Message;

//    }
//    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
//}

//public clsHelper.ResponseMsgWithStatus isReviseApplicable(int HeaderId)
//{
//    var IsApplicable = true;
//    clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
//    try
//    {
//        var objSWP010 = db.SWP010.Where(i => i.HeaderId == HeaderId).FirstOrDefault();
//        var lstResult = db.SP_SWP_Get_HeaderList
//                           (
//                           1, 0, "", "HeaderId = " + HeaderId, objSWP010.BU, objClsLoginInfo.Location //1, 0, "", "s10.HeaderId = " + HeaderId, objSWP010.BU, objClsLoginInfo.Location
//                           ).Take(1).FirstOrDefault();

//        var listSP_GET_ALL_PRE_HEAT_STAGE_SEAM_OFFERED = db.SP_GET_ALL_PRE_HEAT_STAGE_SEAM_OFFERED(objSWP010.QualityProject, objSWP010.BU, objSWP010.Location).Where(i => i.SeamNo == objSWP010.SeamNo).ToList();
//        var listSP_SWP_GET_TOKEN_GENERATED_WPS_NUMBER = db.SP_SWP_GET_TOKEN_GENERATED_WPS_NUMBER(objSWP010.QualityProject, objSWP010.BU, objSWP010.Location).Where(i => i.SeamNo == objSWP010.SeamNo).ToList();
//        IsSeamOfferedCheckEnt objIsSeamOfferedCheckEnt = new IsSeamOfferedCheckEnt();
//        objIsSeamOfferedCheckEnt = CheckSeamIsOffered(listSP_GET_ALL_PRE_HEAT_STAGE_SEAM_OFFERED, listSP_SWP_GET_TOKEN_GENERATED_WPS_NUMBER, lstResult, objSWP010.BU);

//        var MainWPSDisabled = (objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered || objIsSeamOfferedCheckEnt.WPSNo1TokenGenerate);
//        var AlternativeWPS1Disabled = (objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered || objIsSeamOfferedCheckEnt.WPSNo2TokenGenerate);
//        var AlternativeWPS2Disabled = (objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered || objIsSeamOfferedCheckEnt.WPSNo3TokenGenerate);
//        var PWHTAndNotesDisabled = (objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered || objIsSeamOfferedCheckEnt.WeldVisualStageOffered);
//        var JointTypeDisabled = (objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered || objIsSeamOfferedCheckEnt.WPSNo1TokenGenerate || objIsSeamOfferedCheckEnt.WPSNo2TokenGenerate || objIsSeamOfferedCheckEnt.WPSNo3TokenGenerate);

//        if (MainWPSDisabled && AlternativeWPS1Disabled && AlternativeWPS2Disabled && PWHTAndNotesDisabled && JointTypeDisabled)
//        {
//            IsApplicable = false;
//        }
//        if (objIsSeamOfferedCheckEnt.IsAllPreHeatStageOffered)
//        {
//            objResponseMsg.dataValue += " All Pre-Heat Stages Are Offered So Not Allow To Revise Seam.";
//        }
//        else if (MainWPSDisabled && AlternativeWPS1Disabled && AlternativeWPS2Disabled && PWHTAndNotesDisabled)
//        {
//            objResponseMsg.dataValue += " Weld Visual Is Offered And Token Generate For All WPS So Not Allow To Revise Seam.";
//        }
//        else if (!IsApplicable)
//        {
//            objResponseMsg.dataValue += " You Can Not Revise This Due To All Pre Heat Stages Are Offered Or Weld Visual Stage Is Offered.";
//        }

//        objResponseMsg.Key = IsApplicable;
//    }
//    catch (Exception ex)
//    {
//        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
//        objResponseMsg.Key = false;
//        objResponseMsg.Value = ex.Message;

//    }
//    return objResponseMsg;

//}
#endregion

public JsonResult LoadHeaderData(JQueryDataTableParamModel param, string qualityProj, string BU)
{
    try
    {
        var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
        var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
        int StartIndex = param.iDisplayStart + 1;
        int EndIndex = param.iDisplayStart + param.iDisplayLength;
        var user = objClsLoginInfo.Location;
        List<clsTask> list = new List<clsTask>();
        //list = (from a in db.SWP002
        //        where a.QualityProject == qualityProj
        //        select new clsTask
        //        {
        //            id = a.NoteNumber.ToString(),
        //            text = a.NoteDescription
        //        }).ToList();
        list = Manager.GetGeneralNotes(qualityProj, "", "").Select(i => new clsTask { id = i.NoteNumber.ToString(), text = i.NoteNumber.ToString() + "-" + i.NoteDescription.ToString() }).ToList();

        //< option value = '" + object.id + "' > " + object.text + " </ option >

        //int Headerid =Convert.ToInt32(param.Headerid) ;
        string strWhere = "1=1";
        //  strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "BU", "Location");
        if (!string.IsNullOrEmpty(qualityProj))
        {
            strWhere += " AND UPPER(s10.QualityProject) = '" + qualityProj.ToUpper() + "'";
        }
        if (param.CTQCompileStatus.ToUpper() == "PENDING")
        {
            strWhere += "and 1=1 and s10.status in('" + clsImplementationEnum.FDMSStatus.DRAFT.GetStringValue() + "','" + clsImplementationEnum.QualityIdHeaderStatus.Returned.GetStringValue() + "')";
        }
        //else
        //{
        //    strWhere += "1=1";
        //}
        if (!string.IsNullOrWhiteSpace(param.sSearch))
        {
            //strWhere += "and (Project+' - '+c.t_dsca like '%" + param.sSearch
            //     + "%' or Location +' - '+ e.t_desc like '%" + param.sSearch
            //     + "%' or BU +' - '+ d.t_desc like '%" + param.sSearch
            //     + "%' or QualityProject like '%" + param.sSearch
            //     + "%' or SeamNo like '%" + param.sSearch
            //     + "%' or JointType like '%" + param.sSearch
            //     + "%' or MainWPSNumber like '%" + param.sSearch
            //     + "%' or MainWPSRevNo like '%" + param.sSearch
            //     + "%' or AlternateWPS1RevNo like '%" + param.sSearch
            //     + "%' or AlternateWPS1 like '%" + param.sSearch
            //     + "%' or AlternateWPS2 like '%" + param.sSearch
            //     + "%' or AlternateWPS2RevNo like '%" + param.sSearch
            //     + "%' or CreatedBy +' - '+ b.t_name like '%" + param.sSearch
            //     + "%' or CreatedOn like '%" + param.sSearch
            //     + "%')";
        }
        var lstResult = db.SP_SWP_Get_HeaderList
                        (
                        StartIndex, EndIndex, "", strWhere, BU, user
                        ).ToList();

        var data = (from uc in lstResult
                    select new[]
                   {
                                Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.SeamNo),
                               GetJointType( uc.selectedJointType ,  uc.JointType , uc.Status ,  uc.HeaderId.ToString()),
                               GetMWPS(uc.WPSNumbers1 , uc.MainWPSNumber , uc.Status , uc.HeaderId.ToString()),
                               Convert.ToString(uc.MainWPSRevNo),
                               GetMWPS1(uc.WPSNumbers2 , uc.AlternateWPS1 , uc.Status , uc.HeaderId.ToString()),
                               Convert.ToString(uc.AlternateWPS1RevNo),
                               GetMWPS2(uc.WPSNumbers3 , uc.AlternateWPS2 , uc.Status , uc.HeaderId.ToString()),
                               Convert.ToString(uc.AlternateWPS2RevNo),
                               (uc.Status == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue() || uc.Status == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue()?"<input type=\"text\" class=\"form-control\" disabled id=\"txtPreheat_"+uc.HeaderId+"\" name=\"txtPreheat_\" value=\""+Convert.ToString(uc.Preheat)+"\" spara=\""+uc.HeaderId+"\" />":"<input type=\"text\" class=\"form-control\" id=\"txtPreheat_"+uc.HeaderId+"\" name=\"txtPreheat_\" value=\""+Convert.ToString(uc.Preheat)+"\" spara=\""+uc.HeaderId+"\" />"),//Convert.ToString(uc.Preheat),
                               (uc.Status == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue() || uc.Status == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue()?"<input type=\"text\" disabled class=\"form-control\" id=\"txtDHTISR"+uc.HeaderId+"\" name=\"txtDHTISR_\" value=\""+Convert.ToString(uc.DHTISR)+"\" spara=\""+uc.HeaderId+"\" />":"<input type=\"text\" class=\"form-control\" id=\"txtDHTISR"+uc.HeaderId+"\" name=\"txtDHTISR_\" value=\""+Convert.ToString(uc.DHTISR)+"\" spara=\""+uc.HeaderId+"\" />"),//Convert.ToString(uc.DHTISR),
                               (uc.Status == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue() || uc.Status == clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue()?"<select disabled name=\"ddlSWPNotes\" id=\"ddlSWPNotes_"+uc.HeaderId+"\" multiple=\"multiple\" class=\"form-control\">"+AutoCompleteSelect(list,uc.QualityProject,uc.SWPNotes)+"</select>":"<select name=\"ddlSWPNotes\" id=\"ddlSWPNotes_"+uc.HeaderId+"\" multiple=\"multiple\" class=\"form-control\">"+AutoCompleteSelect(list,uc.QualityProject,uc.SWPNotes)+"</select>"),//Convert.ToString(uc.SWPNotes),
                               Convert.ToString(uc.RevNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.Returnremarks),
                               //Convert.ToString(uc.HeaderId),
                               (!string.IsNullOrEmpty(uc.Status)?(string.Equals(uc.Status,clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue(),StringComparison.OrdinalIgnoreCase)?HTMLActionString(uc.HeaderId, uc.Status, "Delete", "Delete Details", "fa fa-trash-o", "DeleteSeamDetails(" + uc.HeaderId + ","+uc.TPHeaderId+");",!(uc.Status == clsImplementationEnum.TestPlanStatus.Draft.GetStringValue() || uc.Status == clsImplementationEnum.TestPlanStatus.Returned.GetStringValue() || uc.TPStatus == clsImplementationEnum.TestPlanStatus.Draft.GetStringValue()|| uc.TPStatus == clsImplementationEnum.TestPlanStatus.Returned.GetStringValue()))+HTMLActionString(uc.HeaderId,uc.Status,"CopySWPID","Copy SWP","fa fa-files-o","fnCopy("+uc.HeaderId+");")+"   "+HTMLActionString(uc.HeaderId,uc.Status,"TransferSWPID","Transfer SWP","fa fa-arrow-circle-right","fnTransfer(\""+ uc.HeaderId +"\");"):string.Empty):string.Empty)+"&nbsp;"+HTMLActionString(uc.HeaderId,uc.Status,"ParameterSWPID","Parameter Slip","fa fa-print","ParameterSlip(\""+ uc.HeaderId +"\");")
                               //Convert.ToString(uc.HeaderId),
                              // Convert.ToString(uc.HeaderId)
                           }).ToList();

        return Json(new
        {
            sEcho = Convert.ToInt32(param.sEcho),
            iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
            iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
            aaData = data
        }, JsonRequestBehavior.AllowGet);
    }
    catch (Exception ex)
    {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

        return Json(new
        {
            sEcho = param.sEcho,
            iTotalRecords = "0",
            iTotalDisplayRecords = "0",
            aaData = ""
        }, JsonRequestBehavior.AllowGet);
    }
}

[HttpPost]
public ActionResult DeleteSeamDetails(int headerId)
{
    clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
    try
    {
        if (headerId > 0)
        {
            SWP010 objSWP010 = new SWP010();
            objSWP010 = db.SWP010.Where(x => x.HeaderId == headerId).FirstOrDefault();
            if (objSWP010.Status != clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue())
            {
                objSWP010.JointType = null;
                objSWP010.MainWPSNumber = null;
                objSWP010.MainWPSRevNo = null;
                objSWP010.AlternateWPS1 = null;
                objSWP010.AlternateWPS1RevNo = null;
                objSWP010.AlternateWPS2 = null;
                objSWP010.AlternateWPS2RevNo = null;
                objSWP010.Preheat = null;
                objSWP010.DHTISR = null;
                objSWP010.SWPNotes = null;
                objSWP010.SubmittedBy = null;
                objSWP010.SubmittedOn = null;
                objSWP010.ApprovedBy = null;
                objSWP010.ApprovedOn = null;
                objSWP010.ReturnedBy = null;
                objSWP010.ReturnedOn = null;
                objSWP010.Returnremarks = null;
                if (objSWP010.Status == clsImplementationEnum.TestPlanStatus.Approved.GetStringValue())
                {
                    objSWP010.RevNo = objSWP010.RevNo + 1;
                }
                objSWP010.Status = clsImplementationEnum.TestPlanStatus.Draft.GetStringValue();

                db.SaveChanges();
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
            }
            else
            {
                objResponseMsg.Remarks = "This data is already submitted, it cannot be delete now!!";
            }
            objResponseMsg.Key = true;
        }
        else
        {
            objResponseMsg.Key = false;
            objResponseMsg.Value = clsImplementationMessage.CommonMessages.DuplicateQID.ToString();
        }
    }
    catch (Exception ex)
    {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        objResponseMsg.Key = false;
        objResponseMsg.Value = ex.Message;

    }
    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
}

//[HttpPost]
//public ActionResult DeleteSeamDetails(int swpid, int tpid)
//{
//    clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
//    try
//    {
//        bool isdeleted = false;
//        if (swpid > 0)
//        {
//            SWP010 objSWP010 = new SWP010();
//            objSWP010 = db.SWP010.Where(x => x.HeaderId == swpid).FirstOrDefault();
//            if (objSWP010 != null)
//            {
//                if (IsDeleteApplicable(objSWP010.Status, objSWP010.RevNo, objSWP010.SeamNo, objSWP010.QualityProject))
//                {
//                    if (objSWP010.Status != clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue())
//                    {
//                        objSWP010.JointType = null;
//                        objSWP010.MainWPSNumber = null;
//                        objSWP010.MainWPSRevNo = null;
//                        objSWP010.AlternateWPS1 = null;
//                        objSWP010.AlternateWPS1RevNo = null;
//                        objSWP010.AlternateWPS2 = null;
//                        objSWP010.AlternateWPS2RevNo = null;
//                        objSWP010.Preheat = null;
//                        objSWP010.DHTISR = null;
//                        objSWP010.SWPNotes = null;
//                        objSWP010.SubmittedBy = null;
//                        objSWP010.SubmittedOn = null;
//                        objSWP010.ApprovedBy = null;
//                        objSWP010.ApprovedOn = null;
//                        objSWP010.ReturnedBy = null;
//                        objSWP010.ReturnedOn = null;
//                        objSWP010.Returnremarks = null;
//                        if (objSWP010.Status == clsImplementationEnum.TestPlanStatus.Approved.GetStringValue())
//                        {
//                            objSWP010.RevNo = objSWP010.RevNo + 1;
//                        }
//                        objSWP010.Status = clsImplementationEnum.TestPlanStatus.Draft.GetStringValue();

//                        db.SaveChanges();
//                        isdeleted = true;
//                    }
//                    else
//                    {
//                        objResponseMsg.Value += "This shop weld plan is already submitted, it cannot be delete now!!";
//                    }
//                }
//                else
//                {
//                    objResponseMsg.Value += "You cannot delete shop weld plan, because Token has been generated for this seam.";
//                }

//                QMS020 objQMS020 = db.QMS020.Where(x => x.HeaderId == tpid).FirstOrDefault();
//                if ((new clsManager()).GetMaxOfferedSequenceNo(objQMS020.QualityProject, objQMS020.BU, objQMS020.Location, objQMS020.SeamNo) == 0 && (objQMS020.Status == clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue() || objQMS020.Status == clsImplementationEnum.TestPlanStatus.Returned.GetStringValue()) && objQMS020.RevNo == 0 &&
//                     !string.IsNullOrEmpty(Convert.ToString(objQMS020.QualityId)))
//                {
//                    QMS021 objQMS021 = new QMS021();
//                    List<QMS021> lstStages = db.QMS021.Where(x => x.HeaderId == tpid).ToList();

//                    if (lstStages != null && lstStages.Count > 0)
//                    {
//                        List<int> lstTPLines = lstStages.Select(a => a.LineId).ToList();
//                        #region Chem/Hardness/PMi/Ferrite                    
//                        List<QMS023> lstDesQMS023 = db.QMS023.Where(x => lstTPLines.Contains(x.TPLineId.HasValue ? x.TPLineId.Value : 0)).ToList();
//                        if (lstDesQMS023 != null && lstDesQMS023.Count > 0)
//                        {
//                            db.QMS023.RemoveRange(lstDesQMS023);
//                        }

//                        List<QMS022> lstDesQMS022 = db.QMS022.Where(x => lstTPLines.Contains(x.TPLineId.HasValue ? x.TPLineId.Value : 0)).ToList();
//                        if (lstDesQMS022 != null && lstDesQMS022.Count > 0)
//                        {
//                            db.QMS022.RemoveRange(lstDesQMS022);
//                        }
//                        #endregion

//                        db.QMS021.RemoveRange(lstStages);
//                    }
//                    db.SaveChanges();
//                    objQMS020.QualityId = null;
//                    objQMS020.QualityIdRev = null;
//                    objQMS020.PTCApplicable = false;
//                    objQMS020.PTCNumber = "";
//                    db.SaveChanges();
//                    isdeleted = true;
//                }
//                else
//                {
//                    objResponseMsg.Value += "This test plan is already submitted, it cannot be delete now!!";
//                }
//                objResponseMsg.Key = isdeleted;
//                if (isdeleted)
//                {
//                    objResponseMsg.Value += clsImplementationMessage.CommonMessages.Delete.ToString();
//                }
//            }
//            else
//            {
//                objResponseMsg.Key = false;
//                objResponseMsg.Value = clsImplementationMessage.CommonMessages.DuplicateQID.ToString();
//            }
//        }
//    }
//    catch (Exception ex)
//    {
//        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
//        objResponseMsg.Key = false;
//        objResponseMsg.Value = ex.Message;
//    }
//    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
//}

//[HttpPost]
//public ActionResult DeleteSeamDetails(int swpid, int tpid, string deleteto)
//{
//    clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
//    try
//    {
//        bool isSWPdeleted = false;
//        bool isTPdeleted = false;
//        if (deleteto == "SWP" || deleteto == "BOTH")
//        {
//            SWP010 objSWP010 = new SWP010();
//            objSWP010 = db.SWP010.Where(x => x.HeaderId == swpid).FirstOrDefault();
//            if (objSWP010 != null)
//            {
//                if (IsDeleteApplicable(objSWP010.Status, objSWP010.RevNo, objSWP010.SeamNo, objSWP010.QualityProject))
//                {
//                    if (objSWP010.Status != clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue())
//                    {
//                        objSWP010.JointType = null;
//                        objSWP010.MainWPSNumber = null;
//                        objSWP010.MainWPSRevNo = null;
//                        objSWP010.AlternateWPS1 = null;
//                        objSWP010.AlternateWPS1RevNo = null;
//                        objSWP010.AlternateWPS2 = null;
//                        objSWP010.AlternateWPS2RevNo = null;
//                        objSWP010.Preheat = null;
//                        objSWP010.DHTISR = null;
//                        objSWP010.SWPNotes = null;
//                        objSWP010.SubmittedBy = null;
//                        objSWP010.SubmittedOn = null;
//                        objSWP010.ApprovedBy = null;
//                        objSWP010.ApprovedOn = null;
//                        objSWP010.ReturnedBy = null;
//                        objSWP010.ReturnedOn = null;
//                        objSWP010.Returnremarks = null;
//                        if (objSWP010.Status == clsImplementationEnum.TestPlanStatus.Approved.GetStringValue())
//                        {
//                            objSWP010.RevNo = objSWP010.RevNo + 1;
//                        }
//                        objSWP010.Status = clsImplementationEnum.TestPlanStatus.Draft.GetStringValue();

//                        db.SaveChanges();
//                        isSWPdeleted = true;
//                    }
//                    else
//                    {
//                        objResponseMsg.Remarks += "This shop weld plan is already submitted, it cannot be delete now!!";
//                    }
//                }
//                else
//                {
//                    objResponseMsg.Remarks += "You cannot delete shop weld plan, because Token has been generated for this seam.";
//                }
//            }
//            //}
//            if (deleteto == "TP" || deleteto == "BOTH")
//            {
//                QMS020 objQMS020 = db.QMS020.Where(x => x.HeaderId == tpid).FirstOrDefault();
//                if ((new clsManager()).GetMaxOfferedSequenceNo(objQMS020.QualityProject, objQMS020.BU, objQMS020.Location, objQMS020.SeamNo) == 0)
//                {
//                    if ((objQMS020.Status == clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue() || objQMS020.Status == clsImplementationEnum.TestPlanStatus.Returned.GetStringValue()) && objQMS020.RevNo == 0 &&
//                         !string.IsNullOrEmpty(Convert.ToString(objQMS020.QualityId)))
//                    {
//                        QMS021 objQMS021 = new QMS021();
//                        List<QMS021> lstStages = db.QMS021.Where(x => x.HeaderId == tpid).ToList();

//                        if (lstStages != null && lstStages.Count > 0)
//                        {
//                            List<int> lstTPLines = lstStages.Select(a => a.LineId).ToList();
//                            #region Chem/Hardness/PMi/Ferrite                    
//                            List<QMS023> lstDesQMS023 = db.QMS023.Where(x => lstTPLines.Contains(x.TPLineId.HasValue ? x.TPLineId.Value : 0)).ToList();
//                            if (lstDesQMS023 != null && lstDesQMS023.Count > 0)
//                            {
//                                db.QMS023.RemoveRange(lstDesQMS023);
//                            }

//                            List<QMS022> lstDesQMS022 = db.QMS022.Where(x => lstTPLines.Contains(x.TPLineId.HasValue ? x.TPLineId.Value : 0)).ToList();
//                            if (lstDesQMS022 != null && lstDesQMS022.Count > 0)
//                            {
//                                db.QMS022.RemoveRange(lstDesQMS022);
//                            }
//                            #endregion

//                            db.QMS021.RemoveRange(lstStages);
//                        }
//                        db.SaveChanges();
//                        objQMS020.QualityId = null;
//                        objQMS020.QualityIdRev = null;
//                        objQMS020.PTCApplicable = false;
//                        objQMS020.PTCNumber = "";
//                        db.SaveChanges();
//                        isTPdeleted = true;
//                    }
//                    else
//                    {
//                        if (!string.IsNullOrEmpty(Convert.ToString(objQMS020.QualityId))) //if (string.IsNullOrEmpty(Convert.ToString(objQMS020.QualityId)))
//                        {
//                            objResponseMsg.Remarks += "Quality Id not available, test plan cannot be delete now!!";
//                        }
//                        else
//                        {
//                            objResponseMsg.Remarks += "This test plan is already submitted, it cannot be delete now!!";
//                        }
//                    }
//                }
//                else
//                {
//                    objResponseMsg.Remarks += "Stage already offered, test plan cannot be delete now!!";
//                }
//            }
//        }
//        //if (deleteto == "TP" || deleteto == "BOTH")
//        //{
//        //    QMS020 objQMS020 = db.QMS020.Where(x => x.HeaderId == tpid).FirstOrDefault();
//        //    if ((new clsManager()).GetMaxOfferedSequenceNo(objQMS020.QualityProject, objQMS020.BU, objQMS020.Location, objQMS020.SeamNo) == 0)
//        //    {
//        //        if ((objQMS020.Status == clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue() || objQMS020.Status == clsImplementationEnum.TestPlanStatus.Returned.GetStringValue()) && objQMS020.RevNo == 0 &&
//        //             !string.IsNullOrEmpty(Convert.ToString(objQMS020.QualityId)))
//        //        {
//        //            QMS021 objQMS021 = new QMS021();
//        //            List<QMS021> lstStages = db.QMS021.Where(x => x.HeaderId == tpid).ToList();

//        //            if (lstStages != null && lstStages.Count > 0)
//        //            {
//        //                List<int> lstTPLines = lstStages.Select(a => a.LineId).ToList();
//        //                #region Chem/Hardness/PMi/Ferrite                    
//        //                List<QMS023> lstDesQMS023 = db.QMS023.Where(x => lstTPLines.Contains(x.TPLineId.HasValue ? x.TPLineId.Value : 0)).ToList();
//        //                if (lstDesQMS023 != null && lstDesQMS023.Count > 0)
//        //                {
//        //                    db.QMS023.RemoveRange(lstDesQMS023);
//        //                }

//        //                List<QMS022> lstDesQMS022 = db.QMS022.Where(x => lstTPLines.Contains(x.TPLineId.HasValue ? x.TPLineId.Value : 0)).ToList();
//        //                if (lstDesQMS022 != null && lstDesQMS022.Count > 0)
//        //                {
//        //                    db.QMS022.RemoveRange(lstDesQMS022);
//        //                }
//        //                #endregion

//        //                db.QMS021.RemoveRange(lstStages);
//        //            }
//        //            db.SaveChanges();
//        //            objQMS020.QualityId = null;
//        //            objQMS020.QualityIdRev = null;
//        //            objQMS020.PTCApplicable = false;
//        //            objQMS020.PTCNumber = "";
//        //            db.SaveChanges();
//        //            isTPdeleted = true;
//        //        }
//        //        else
//        //        {
//        //            if (string.IsNullOrEmpty(Convert.ToString(objQMS020.QualityId)))
//        //            {
//        //                objResponseMsg.Remarks += "Quality Id not available, test plan cannot be delete now!!";
//        //            }
//        //            else
//        //            {
//        //                objResponseMsg.Remarks += "This test plan is already submitted, it cannot be delete now!!";
//        //            }
//        //        }
//        //    }
//        //    else
//        //    {
//        //        objResponseMsg.Remarks += "Stage already offered, test plan cannot be delete now!!";
//        //    }
//        //}

//        if (isSWPdeleted || isTPdeleted)
//        {
//            objResponseMsg.Key = true;
//            objResponseMsg.Value += clsImplementationMessage.CommonMessages.Delete.ToString() + (!(isSWPdeleted && isTPdeleted) ? ((isSWPdeleted ? " for shop weld plan" : "") + (isTPdeleted ? " for test plan" : "")) : "");
//        }
//    }
//    catch (Exception ex)
//    {
//        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
//        objResponseMsg.Key = false;
//        objResponseMsg.Value = ex.Message;
//    }
//    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
//}

public bool IsSeamOffered(int HeaderId)
{
    var status = clsImplementationEnum.PartlistOfferInspectionStatus.READY_TO_OFFER.GetStringValue();
    var objSWP010 = db.SWP010.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
    var maxOfferedSequenceNo = (new clsManager()).GetMaxOfferedSequenceNo(objSWP010.QualityProject, objSWP010.BU, objSWP010.Location, objSWP010.SeamNo);
    if (maxOfferedSequenceNo > 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}
public string AutoCompleteSelect(List<clsTask> list, string qualityProject, string selectedValue)
{
    //string strOption = string.Empty;
    string strOption = string.Empty;
    string[] arrayVal = { };
    if (!string.IsNullOrWhiteSpace(selectedValue))
    {
        arrayVal = selectedValue.Split(',');
    }
    foreach (var item in list)
    {
        if (arrayVal.Contains(item.id))
        {
            strOption += "<option value = '" + item.id + "' selected>" + item.text + "</option>";
        }
        else
        {
            strOption += "<option value = '" + item.id + "'>" + item.text + "</option>";
        }
    }
    return strOption;
}

public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", bool disabled = false)
{
    string htmlControl = "";

    string inputID = buttonName + "" + rowId.ToString();
    string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

    if (!string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue())) //if (!string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
    {//{
        if (disabled)
        {
            htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;opacity: 0.5;margin-left:5px;' Title='" + buttonTooltip + "' class='" + className + "' ></i>"; //htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;opacity: 0.5;' Title='" + buttonTooltip + "' class='disabledicon " + className + "' ></i>";
        }
        else
            htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;margin-left:5px;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";  //htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='iconspace " + className + "'" + onClickEvent + " ></i>";
    }//}

    //htmlControl = "<a id='" + inputID + "' name='" + inputID + "' class='btn btn-outline btn-circle btn-sm blue' " + onClickEvent + " ><i class='" + className + "'></i> " + buttonName + "</a>";

    //htmlControl = "<i  data-modal='' id='" + inputID + "' name='" + inputID + "' style='cursor:Pointer;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";

    return htmlControl;
}
public string GetMWPS(string WPSNumbers, string MainWPSNumber, string Status, string HeaderId)
{
    string[] MWPS = null;
    string htmlOption = "<option value=''> Select WPS </option>";
    MWPS = WPSNumbers.Split(',');
    if (!string.IsNullOrWhiteSpace(MainWPSNumber))
    {
        for (int i = 0; i < MWPS.Length; i++)
        {
            if (MWPS[i] == MainWPSNumber)
            {
                htmlOption += "<option value=" + MWPS[i] + " selected >" + MWPS[i] + "</option>";
            }
            else
            {
                htmlOption += "<option value=" + MWPS[i] + ">" + MWPS[i] + "</option>";
            }

        }
        if (Status == clsImplementationEnum.TestPlanStatus.Draft.GetStringValue() || Status == clsImplementationEnum.QualityIdHeaderStatus.Returned.GetStringValue())
        {
            return "<select name='txtmainWPS' id='txtmainWPS_" + HeaderId + "'   class='form-control input-sm input-small input-inline' onchange='update(" + HeaderId + ")'>" + htmlOption + "</select>";
        }
        else
        {
            return "<select name='txtmainWPS' id='txtmainWPS_" + HeaderId + "' disabled class='form-control input-sm input-small input-inline' onchange='update(" + HeaderId + ")'>" + htmlOption + "</select>";
        }

    }
    else
    {
        for (int i = 0; i < MWPS.Length; i++)
        {
            htmlOption += "<option value=" + MWPS[i] + ">" + MWPS[i] + "</option>";
        }
        if (Status == clsImplementationEnum.TestPlanStatus.Draft.GetStringValue() || Status == clsImplementationEnum.QualityIdHeaderStatus.Returned.GetStringValue())
        {
            return "<select name='txtmainWPS' id='txtmainWPS_" + HeaderId + "' class='form-control input-sm input-small input-inline' onchange='update(" + HeaderId + ")' >" + htmlOption + "</select>";
        }
        else
        {
            return "<select name='txtmainWPS'  id='txtmainWPS_" + HeaderId + "' disabled class='form-control input-sm input-small input-inline' onchange='update(" + HeaderId + ")'>" + htmlOption + "</select>";
        }
    }
}
public string GetMWPS1(string WPSNumbers, string MainWPSNumber, string Status, string HeaderId)
{
    string[] MWPS = null;
    string htmlOption = "<option value=''> Select  Alternate WPS1 </option>";
    MWPS = WPSNumbers.Split(',');
    if (!string.IsNullOrWhiteSpace(MainWPSNumber))
    {
        for (int i = 0; i < MWPS.Length; i++)
        {
            if (MWPS[i] == MainWPSNumber)
            {
                htmlOption += "<option value=" + MWPS[i] + " selected >" + MWPS[i] + "</option>";
            }
            else
            {
                htmlOption += "<option value=" + MWPS[i] + ">" + MWPS[i] + "</option>";
            }

        }
        if (Status == clsImplementationEnum.TestPlanStatus.Draft.GetStringValue() || Status == clsImplementationEnum.QualityIdHeaderStatus.Returned.GetStringValue())
        {
            return "<select name='txtmainWPS1' id='txtmainWPS1_" + HeaderId + "' class='form-control input-sm input-small input-inline'  onchange='update1(" + HeaderId + ")' >" + htmlOption + "</select>";
        }
        else
        {
            return "<select name='txtmainWPS1' id='txtmainWPS1_" + HeaderId + "' disabled class='form-control input-sm input-small input-inline'  onchange='update1(" + HeaderId + ")'>" + htmlOption + "</select>";
        }

    }
    else
    {
        for (int i = 0; i < MWPS.Length; i++)
        {
            htmlOption += "<option value=" + MWPS[i] + ">" + MWPS[i] + "</option>";
        }
        if (Status == clsImplementationEnum.TestPlanStatus.Draft.GetStringValue() || Status == clsImplementationEnum.QualityIdHeaderStatus.Returned.GetStringValue())
        {
            return "<select name='txtmainWPS1' id='txtmainWPS1_" + HeaderId + "' class='form-control input-sm input-small input-inline'  onchange='update1(" + HeaderId + ")'>" + htmlOption + "</select>";
        }
        else
        {
            return "<select name='txtmainWPS1' id='txtmainWPS1_" + HeaderId + "' disabled class='form-control input-sm input-small input-inline'  onchange='update1(" + HeaderId + ")'>" + htmlOption + "</select>";
        }
    }
}
public string GetMWPS2(string WPSNumbers, string MainWPSNumber, string Status, string HeaderId)
{
    string[] MWPS = null;
    string htmlOption = "<option value=''> Select Alternate WPS2 </option>";
    MWPS = WPSNumbers.Split(',');
    if (!string.IsNullOrWhiteSpace(MainWPSNumber))
    {
        for (int i = 0; i < MWPS.Length; i++)
        {
            if (MWPS[i] == MainWPSNumber)
            {
                htmlOption += "<option value=" + MWPS[i] + " selected >" + MWPS[i] + "</option>";
            }
            else
            {
                htmlOption += "<option value=" + MWPS[i] + ">" + MWPS[i] + "</option>";
            }

        }
        if (Status == clsImplementationEnum.TestPlanStatus.Draft.GetStringValue() || Status == clsImplementationEnum.QualityIdHeaderStatus.Returned.GetStringValue())
        {
            return "<select name='txtmainWPS2' id='txtmainWPS2_" + HeaderId + "'  class='form-control input-sm input-small input-inline'  onchange='update2(" + HeaderId + ")'>" + htmlOption + "</select>";
        }
        else
        {
            return "<select name='txtmainWPS2' id='txtmainWPS2_" + HeaderId + "' disabled class='form-control input-sm input-small input-inline'  onchange='update2(" + HeaderId + ")'>" + htmlOption + "</select>";
        }

    }
    else
    {
        for (int i = 0; i < MWPS.Length; i++)
        {
            htmlOption += "<option value=" + MWPS[i] + ">" + MWPS[i] + "</option>";
        }
        if (Status == clsImplementationEnum.TestPlanStatus.Draft.GetStringValue() || Status == clsImplementationEnum.QualityIdHeaderStatus.Returned.GetStringValue())
        {
            return "<select name='txtmainWPS2' id='txtmainWPS2_" + HeaderId + "' class='form-control input-sm input-small input-inline'  onchange='update2(" + HeaderId + ")'>" + htmlOption + "</select>";
        }
        else
        {
            return "<select name='txtmainWPS2' id='txtmainWPS2_" + HeaderId + "' disabled class='form-control input-sm input-small input-inline'  onchange='update2(" + HeaderId + ")' >" + htmlOption + "</select>";
        }
    }
}
public string GetJointType(string selectedJointType, string JointType, string Status, string HeaderId)
{
    string[] JT = null;
    string htmlOption = "<option value=''> Select Joint Type </option>";
    JT = JointType.Split(',');
    if (!string.IsNullOrWhiteSpace(selectedJointType))
    {
        for (int i = 0; i < JT.Length; i++)
        {
            if (JT[i] == selectedJointType)
            {
                htmlOption += "<option value=" + JT[i] + " selected >" + JT[i] + "</option>";
            }
            else
            {
                htmlOption += "<option value=" + JT[i] + ">" + JT[i] + "</option>";
            }

        }
        if (Status == clsImplementationEnum.TestPlanStatus.Draft.GetStringValue() || Status == clsImplementationEnum.QualityIdHeaderStatus.Returned.GetStringValue())
        {
            return "<select name='txtJT' id='txtJT_" + HeaderId + "'  class='form-control input-sm input-small input-inline' onchange='jointType(" + HeaderId + ")' >" + htmlOption + "</select>";
        }
        else
        {
            return "<select name='txtJT' id='txtJT_" + HeaderId + "' disabled  class='form-control input-sm input-small input-inline' onchange='jointType(" + HeaderId + ")' >" + htmlOption + "</select>";
        }

    }
    else
    {
        for (int i = 0; i < JT.Length; i++)
        {
            htmlOption += "<option value=" + JT[i] + ">" + JT[i] + "</option>";
        }
        if (Status == clsImplementationEnum.TestPlanStatus.Draft.GetStringValue() || Status == clsImplementationEnum.QualityIdHeaderStatus.Returned.GetStringValue())
        {
            return "<select name='txtJT' id='txtJT_" + HeaderId + "'  class='form-control input-sm input-small input-inline' onchange='jointType(" + HeaderId + ")' >" + htmlOption + "</select>";
        }
        else
        {
            return "<select name='txtJT' id='txtJT_" + HeaderId + "' disabled class='form-control input-sm input-small input-inline'  onchange='jointType(" + HeaderId + ")'>" + htmlOption + "</select>";
        }
    }

}
[HttpPost]
public ActionResult getCodeValue(string project, string approver, string BU, string headerid)
{
    ResponceMsgWithStatus1 objResponseMsg = new ResponceMsgWithStatus1();
    try
    {
        int hd = Convert.ToInt32(headerid);
        project = project.Split('-')[0].Trim();
        objResponseMsg.QP = db.SWP010.Where(i => i.HeaderId == hd).Select(i => i.QualityProject).FirstOrDefault();
        var objSWP010 = db.SWP010.Where(i => i.HeaderId == hd).FirstOrDefault();
        var wps = objSWP010.MainWPSNumber;

        objResponseMsg.seam = db.SWP010.Where(i => i.HeaderId == hd).Select(i => i.SeamNo).FirstOrDefault();
        objResponseMsg.projdesc = db.COM001.Where(i => i.t_cprj == project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
        objResponseMsg.appdesc = db.COM003.Where(i => i.t_psno == approver && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
        objResponseMsg.budesc = db.COM002.Where(i => i.t_dimx == BU && i.t_dtyp == 2).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
        //List<WPS> MWPS = (from a in db.SWP010
        //                  where a.QualityProject == objResponseMsg.QP
        //                  select new WPS { id = a.MainWPSNumber, text = a.MainWPSNumber }).Distinct().ToList();

        //List<WPS> AWPS1 = (from a in db.SWP010
        //                   where a.QualityProject == objResponseMsg.QP
        //                   select new WPS { id = a.AlternateWPS1, text = a.AlternateWPS1 }).Distinct().ToList();

        //List<WPS> AWPS2 = (from a in db.SWP010
        //                   where a.QualityProject == objResponseMsg.QP
        //                   select new WPS { id = a.AlternateWPS2, text = a.AlternateWPS2 }).Distinct().ToList();
        List<WPS> l1 = new List<WPS>();
        if (!string.IsNullOrEmpty(objSWP010.MainWPSNumber))
        {
            l1.Add(new WPS { id = objSWP010.MainWPSNumber, text = objSWP010.MainWPSNumber });
        }
        if (!string.IsNullOrEmpty(objSWP010.AlternateWPS1))
        {
            l1.Add(new WPS { id = objSWP010.AlternateWPS1, text = objSWP010.AlternateWPS1 });
        }
        if (!string.IsNullOrEmpty(objSWP010.AlternateWPS2))
        {
            l1.Add(new WPS { id = objSWP010.AlternateWPS2, text = objSWP010.AlternateWPS2 });
        }
        //List <WPS> MWPS = (from a in db.SWP010
        //                  where a.HeaderId == hd
        //                  && !String.IsNullOrEmpty(a.MainWPSNumber)
        //                  select new WPS { id = a.MainWPSNumber, text = a.MainWPSNumber }).Distinct().ToList();

        //List<WPS> AWPS1 = (from a in db.SWP010
        //                   where a.HeaderId == hd
        //                   && !String.IsNullOrEmpty(a.AlternateWPS1)
        //                   select new WPS { id = a.AlternateWPS1, text = a.AlternateWPS1 }).Distinct().ToList();

        //List<WPS> AWPS2 = (from a in db.SWP010
        //                   where a.HeaderId == hd
        //                   && !String.IsNullOrEmpty(a.AlternateWPS2)
        //                   select new WPS { id = a.AlternateWPS2, text = a.AlternateWPS2 }).Distinct().ToList();

        //List<WPS> l1 = MWPS.Union(AWPS1).Union(AWPS2).ToList();
        //l1.Select(x => x.id).Distinct();
        objResponseMsg.lsttask = l1.Distinct().ToList();

        List<Process> Process1 = (from a in db.WPS011
                                  where a.WPSNumber == wps
                                  select new Process { id = a.WeldingProcess, text = a.WeldingProcess }).Distinct().ToList();

        List<Layer> Layer1 = (from a in db.WPS011
                              where a.WPSNumber == wps
                              select new Layer { id = a.WeldLayer, text = a.WeldLayer }).Distinct().ToList();

        objResponseMsg.lstProcess = Process1.Distinct().ToList();
        objResponseMsg.lstLayer = Layer1.Distinct().ToList();
    }
    catch (Exception ex)
    {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        objResponseMsg.Key = false;
        objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
    }
    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
}
[HttpPost]
public ActionResult getWeldingProcess(string wps, int revNo)
{
    ResponceMsgWithStatus1 objResponseMsg = new ResponceMsgWithStatus1();
    List<Process> Process1 = (from a in db.WPS011_Log
                              where a.WPSNumber == wps && a.WPSRevNo == revNo
                              select new Process { id = a.WeldingProcess, text = a.WeldingProcess }).Distinct().ToList();
    objResponseMsg.lstProcess = Process1.Distinct().ToList();
    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
}
[HttpPost]
public ActionResult getWeldingLayer(string wps, string wp, int revNo)
{
    ResponceMsgWithStatus1 objResponseMsg = new ResponceMsgWithStatus1();

    List<Layer> Layer1 = (from a in db.WPS011_Log
                          where a.WPSNumber == wps && a.WeldingProcess == wp && a.WPSRevNo == revNo
                          select new Layer { id = a.WeldLayer, text = a.WeldLayer }).Distinct().ToList();
    objResponseMsg.lstLayer = Layer1.Distinct().ToList();
    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
}
//Observation 15969
[HttpPost]
public ActionResult getAWSClass(string wps, string wp, string wl, int rev)
{
    ResponceMsgWithStatus1 objResponseMsg = new ResponceMsgWithStatus1();

    List<Layer> Layer1 = (from a in db.WPS011_Log
                          where a.WPSNumber == wps && a.WeldingProcess == wp && a.WPSRevNo == rev && a.WeldLayer == wl
                          select new Layer { id = a.AWSClass, text = a.AWSClass }).Distinct().ToList();
    objResponseMsg.lstAWSClass = Layer1.Distinct().ToList();
    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
}

[HttpPost]
public ActionResult getFillerMetalSize(string wps, string wp, string wl, int rev, string aws)
{
    ResponceMsgWithStatus1 objResponseMsg = new ResponceMsgWithStatus1();

    List<Layer> Layer1 = (from a in db.WPS011_Log
                          where a.WPSNumber == wps && a.WeldingProcess == wp && a.WPSRevNo == rev && a.WeldLayer == wl && a.AWSClass == aws
                          select new Layer { id = a.FillerMetalSize, text = a.FillerMetalSize }).Distinct().ToList();
    objResponseMsg.lstFillerMetal = Layer1.Distinct().ToList();
    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
}

[HttpPost]
public ActionResult verifyApprover(string approver)
{
    ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
    try
    {
        approver = approver.Split('-')[0].Trim();
        var Exists = db.COM003.Any(x => x.t_psno == approver && x.t_actv == 1);
        if (Exists == false)
        {
            objResponseMsg.Key = false;
        }
        else
        {
            objResponseMsg.Key = true;
        }
    }
    catch (Exception ex)
    {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        objResponseMsg.Key = false;
        objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
    }
    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
}

[HttpPost]
public ActionResult SaveHeader(SWP010 swp010)
{
    ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
    try
    {
        SWP010 objSWP010 = db.SWP010.Where(x => x.HeaderId == swp010.HeaderId).FirstOrDefault();
        if (objSWP010 != null)
        {
            objSWP010.JointType = swp010.JointType;
            objSWP010.MainWPSNumber = swp010.MainWPSNumber;
            objSWP010.AlternateWPS1 = swp010.AlternateWPS1;
            objSWP010.AlternateWPS2 = swp010.AlternateWPS2;
            objSWP010.MainWPSRevNo = swp010.MainWPSRevNo;
            objSWP010.AlternateWPS1RevNo = swp010.AlternateWPS1RevNo;
            objSWP010.AlternateWPS2RevNo = swp010.AlternateWPS2RevNo;
            objSWP010.Preheat = swp010.Preheat;
            objSWP010.DHTISR = swp010.DHTISR;
            objSWP010.SWPNotes = swp010.SWPNotes;
            objSWP010.ApprovedBy = swp010.ApprovedBy.Split('-')[0].ToString().Trim();
            objSWP010.EditedBy = objClsLoginInfo.UserName;
            objSWP010.EditedOn = DateTime.Now;
            if (objSWP010.Status == clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue())
            {
                objSWP010.RevNo = Convert.ToInt32(objSWP010.RevNo) + 1;
                objSWP010.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
            }
            db.SaveChanges();
            objResponseMsg.Key = true;
            objResponseMsg.Value = clsImplementationMessage.PlanMessages.Update.ToString();
        }
        else
        {
            objSWP010.JointType = swp010.JointType;
            objSWP010.MainWPSNumber = swp010.MainWPSNumber;
            objSWP010.AlternateWPS1 = swp010.AlternateWPS1;
            objSWP010.AlternateWPS2 = swp010.AlternateWPS2;
            objSWP010.MainWPSRevNo = swp010.MainWPSRevNo;
            objSWP010.AlternateWPS1RevNo = swp010.AlternateWPS1RevNo;
            objSWP010.AlternateWPS2RevNo = swp010.AlternateWPS2RevNo;
            objSWP010.Preheat = swp010.Preheat;
            objSWP010.DHTISR = swp010.DHTISR;
            objSWP010.SWPNotes = swp010.SWPNotes;
            objSWP010.ApprovedBy = swp010.ApprovedBy.Split('-')[0].ToString().Trim();
            objSWP010.RevNo = 0;
            objSWP010.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
            objSWP010.CreatedBy = objClsLoginInfo.UserName;
            objSWP010.CreatedOn = DateTime.Now;
            db.SWP010.Add(objSWP010);
            db.SaveChanges();
            objResponseMsg.Key = true;
            objResponseMsg.Value = clsImplementationMessage.PlanMessages.Insert.ToString();
        }
    }
    catch (Exception ex)
    {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        objResponseMsg.Key = false;
        objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
    }
    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
}

[HttpPost]
public JsonResult sentForApproval(string Approver, int headerid)
{
    clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
    try
    {
        if (headerid > 0)
        {
            if (Approver.Split('-')[0].ToString().Trim() == objClsLoginInfo.UserName)
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.SMessage.ToString();
            }
            else
            {
                SWP010 objSWP010 = db.SWP010.Where(x => x.HeaderId == headerid).FirstOrDefault();
                objSWP010.Status = clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue();
                objSWP010.ApprovedBy = Approver.Split('-')[0].ToString().Trim();
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.AMessage.ToString();
            }
        }
    }
    catch (Exception ex)
    {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        objResponseMsg.Key = false;
        objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
    }
    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
}

[HttpPost]
public JsonResult MultiSWPSendForApproval(string strHeader)
{
    clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
    try
    {
        int lstNotSubmitted = 0;
        int lstSubmitted = 0;
        string lstMissedMendatoryData = "";
        string[] arrayheaderids = strHeader.Split(',');
        foreach (var item in arrayheaderids)
        {
            int headerid = Convert.ToInt32(item);
            if (headerid > 0)
            {
                SWP010 objSWP010 = db.SWP010.Where(x => x.HeaderId == headerid).FirstOrDefault();
                if (objSWP010 != null)
                {
                    if (IsApplicable(objSWP010.Status, false))
                    {
                        if (objSWP010.JointType.ToUpper() == "BUTTERING")
                        {
                            if (String.IsNullOrEmpty(objSWP010.JointType) || String.IsNullOrEmpty(objSWP010.MainWPSNumber) || (!objSWP010.MainWPSRevNo.HasValue) || String.IsNullOrEmpty(objSWP010.DHTISR))
                            {
                                lstMissedMendatoryData += objSWP010.SeamNo + ",";
                                objResponseMsg.Value = "Please Maintain JointType,WPS Number,WPS Rev No & DHT/ISR";
                            }
                            else
                            {
                                if (objSWP010.Status.Equals(clsImplementationEnum.CTQStatus.DRAFT.GetStringValue()) || objSWP010.Status.Equals(clsImplementationEnum.CTQStatus.Returned.GetStringValue()))
                                {
                                    objSWP010.Status = clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue();
                                    objSWP010.SubmittedBy = objClsLoginInfo.UserName;
                                    objSWP010.SubmittedOn = DateTime.Now;
                                    objSWP010.Returnremarks = null;
                                    objSWP010.ReturnedOn = null;
                                    objSWP010.ReturnedBy = null;
                                    db.SaveChanges();

                                    #region Send Notification
                                    QMS020 objQMS020 = db.QMS020.Where(x => x.QualityProject == objSWP010.QualityProject && x.Project == objSWP010.Project && x.Location == objSWP010.Location && x.BU == objSWP010.BU).FirstOrDefault();
                                    string url = null;
                                    if (objQMS020 != null)
                                        url = "/IPI/ApproveTestPlan/TestPlanDetails?headerID=" + objQMS020.HeaderId;
                                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.WE2.GetStringValue(), objSWP010.Project, objSWP010.BU, objSWP010.Location, "Shop Weld Plan for Seam " + objSWP010.SeamNo + " has been submitted for your Approval", clsImplementationEnum.NotificationType.Information.GetStringValue(), url);
                                    #endregion
                                    lstSubmitted++;
                                }
                            }
                        }
                        else if (String.IsNullOrEmpty(objSWP010.JointType) || String.IsNullOrEmpty(objSWP010.MainWPSNumber) || (!objSWP010.MainWPSRevNo.HasValue) || (!objSWP010.Preheat.HasValue) || String.IsNullOrEmpty(objSWP010.DHTISR))
                        {
                            lstMissedMendatoryData += objSWP010.SeamNo + ",";
                            objResponseMsg.Value = "Please Maintain JointType,WPS Number,WPS Rev No,Preheat & DHT/ISR";
                        }
                        else
                        {
                            if (objSWP010.Status.Equals(clsImplementationEnum.CTQStatus.DRAFT.GetStringValue()) || objSWP010.Status.Equals(clsImplementationEnum.CTQStatus.Returned.GetStringValue()))
                            {
                                objSWP010.Status = clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue();
                                objSWP010.SubmittedBy = objClsLoginInfo.UserName;
                                objSWP010.SubmittedOn = DateTime.Now;
                                objSWP010.Returnremarks = null;
                                objSWP010.ReturnedOn = null;
                                objSWP010.ReturnedBy = null;
                                db.SaveChanges();

                                #region Send Notification
                                QMS020 objQMS020 = db.QMS020.Where(x => x.QualityProject == objSWP010.QualityProject && x.Project == objSWP010.Project && x.Location == objSWP010.Location && x.BU == objSWP010.BU).FirstOrDefault();
                                string url = null;
                                if (objQMS020 != null)
                                    url = "/IPI/ApproveTestPlan/TestPlanDetails?headerID=" + objQMS020.HeaderId;
                                (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.WE2.GetStringValue(), objSWP010.Project, objSWP010.BU, objSWP010.Location, "Shop Weld Plan for Seam " + objSWP010.SeamNo + " has been submitted for your Approval", clsImplementationEnum.NotificationType.Information.GetStringValue(), url);
                                #endregion
                                lstSubmitted++;
                            }
                        }
                    }
                    else
                    {
                        lstNotSubmitted++;
                    }
                }
            }
        }
        if (lstNotSubmitted > 0)
        {
            objResponseMsg.NotDraft = string.Format("Seam {0} have been skipped as Shop Weld Plan {0} are either Sent For Approval Or Approved.", string.Join(",", lstSubmitted));
        }
        if (lstSubmitted > 0)
        {
            objResponseMsg.Key = true;
            objResponseMsg.Value = string.Format("{0} Shop Weld Plan is sent for approval successfully.", lstSubmitted);
        }
        if (lstMissedMendatoryData.Length > 0)
        {
            lstMissedMendatoryData = lstMissedMendatoryData.Substring(0, lstMissedMendatoryData.Length - 1);
            //objResponseMsg.Value += string.Format("Please Maintain JointType,WPS Number,WPS Rev No,Preheat & DHT/ISR for Seam No {0}.", lstMissedMendatoryData);
        }
        //objResponseMsg.Value = "Weld Plan is sent for approval";//clsImplementationMessage.CommonMessages.sentforApprove.ToString();
    }
    catch (Exception ex)
    {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        objResponseMsg.Key = false;
        objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
    }
    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
}

//[HttpPost]
//public JsonResult MultiSWPSendForApproval(string strHeader)
//{
//    clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
//    try
//    {
//        if (!string.IsNullOrWhiteSpace(strHeader))
//        {
//            var result = db.SP_SWP_TP_SEND_FOR_APRPOVAL(strHeader, objClsLoginInfo.UserName).FirstOrDefault();
//            #region Linq send for approval for SWP & TP
//            /*
//            foreach (var item in arrayheaderids)
//            {
//                int headerid = Convert.ToInt32(item);
//                if (headerid > 0)
//                {
//                    SWP010 objSWP010 = db.SWP010.Where(x => x.HeaderId == headerid).FirstOrDefault();
//                    if (objSWP010 != null)
//                    {
//                        #region submit SWP
//                        if (IsApplicable(objSWP010.Status, false))
//                        {
//                            if (String.IsNullOrEmpty(objSWP010.JointType) || String.IsNullOrEmpty(objSWP010.MainWPSNumber) || (!objSWP010.MainWPSRevNo.HasValue) || (!objSWP010.Preheat.HasValue) || String.IsNullOrEmpty(objSWP010.DHTISR))
//                            {
//                                lstMissedMendatoryData += objSWP010.SeamNo + ",";
//                                //objResponseMsg.Value = "Please Maintain JointType,WPS Number,WPS Rev No,Preheat & DHT/ISR";
//                            }
//                            else
//                            {
//                                if (objSWP010.Status.Equals(clsImplementationEnum.CTQStatus.DRAFT.GetStringValue()) || objSWP010.Status.Equals(clsImplementationEnum.CTQStatus.Returned.GetStringValue()))
//                                {
//                                    objSWP010.Status = clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue();
//                                    objSWP010.SubmittedBy = objClsLoginInfo.UserName;
//                                    objSWP010.SubmittedOn = DateTime.Now;
//                                    objSWP010.Returnremarks = null;
//                                    objSWP010.ReturnedOn = null;
//                                    objSWP010.ReturnedBy = null;
//                                    db.SaveChanges();

//                                    #region Send Notification
//                                    QMS020 objQMS020 = db.QMS020.Where(x => x.QualityProject == objSWP010.QualityProject && x.Project == objSWP010.Project && x.Location == objSWP010.Location && x.BU == objSWP010.BU).FirstOrDefault();
//                                    string url = null;
//                                    if (objQMS020 != null)
//                                        url = "/IPI/ApproveTestPlan/TestPlanDetails?headerID=" + objQMS020.HeaderId;
//                                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.WE2.GetStringValue(), objSWP010.Project, objSWP010.BU, objSWP010.Location, "Shop Weld Plan for Seam " + objSWP010.SeamNo + " has been submitted for your Approval", clsImplementationEnum.NotificationType.Information.GetStringValue(), url);
//                                    #endregion
//                                    lstSubmitted++;
//                                }
//                            }
//                        }
//                        else
//                        {
//                            lstNotSubmitted++;
//                        }
//                        #endregion
//                        #region submit TP
//                        var qms20 = db.QMS020.Where(i => i.QualityProject == objSWP010.QualityProject && i.SeamNo == objSWP010.SeamNo && i.Location == objSWP010.Location).FirstOrDefault();
//                        if (qms20 != null)
//                        {
//                            if (string.Equals(qms20.Status, clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue()) || string.Equals(qms20.Status, clsImplementationEnum.TestPlanStatus.Returned.GetStringValue()))
//                            {
//                                (new TestPlanController()).SaveStageTypewiseData(qms20.HeaderId);

//                                if ((new TestPlanController()).IsSamplingPlanisMaintained(qms20.HeaderId, null, qms20.BU, qms20.Location, false))
//                                {
//                                    if ((new TestPlanController()).IsSamplingPlanisMaintained(qms20.HeaderId, null, qms20.BU, qms20.Location, false, false))
//                                    {
//                                        if ((new TestPlanController()).IsDataMaintained(qms20.HeaderId, null, qms20.BU, qms20.Location, false, true))
//                                        {
//                                            if ((new TestPlanController()).IsDataMaintained(qms20.HeaderId, null, qms20.BU, qms20.Location, false))
//                                            {
//                                                if (qms20 != null)
//                                                {
//                                                    #region Send for Approval

//                                                    if (db.QMS021.Where(i => i.HeaderId == qms20.HeaderId).Any())
//                                                    {
//                                                        if (Manager.IsApplicableForParallelStage(qms20.Project, qms20.QualityProject, qms20.BU, qms20.Location, qms20.SeamNo)) // pending che
//                                                        {
//                                                            qms20.Status = clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue();
//                                                            qms20.SubmittedBy = objClsLoginInfo.UserName;
//                                                            qms20.SubmittedOn = DateTime.Now;
//                                                            qms20.ReturnedBy = null;
//                                                            qms20.ReturnedOn = null;
//                                                            qms20.ReturnRemark = null;
//                                                            db.QMS021.Where(i => i.HeaderId == qms20.HeaderId).ToList().ForEach(i => { i.SubmittedBy = objClsLoginInfo.UserName; i.SubmittedOn = DateTime.Now; });
//                                                            db.SaveChanges();

//                                                            #region Send Notification
//                                                            (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.WE2.GetStringValue(), qms20.Project, qms20.BU, qms20.Location, "Test Plan for Seam: " + qms20.SeamNo + " of Project: " + qms20.QualityProject + "  has been submitted for your Approval", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/ApproveTestPlan/TestPlanDetails?HeaderID=" + qms20.HeaderId);
//                                                            #endregion

//                                                            lstQIDSubmitted.Add(qms20.SeamNo);
//                                                        }
//                                                        else
//                                                        {
//                                                            lstNotApplicableForParallel.Add(qms20.SeamNo);
//                                                        }
//                                                    }
//                                                    else
//                                                    {
//                                                        lstQIDWithoutLines.Add(qms20.SeamNo);
//                                                    }

//                                                    #endregion
//                                                }
//                                            }
//                                            else
//                                            {
//                                                lstMaintainFerrite.Add(qms20.SeamNo);
//                                            }
//                                        }
//                                        else
//                                        {
//                                            lstMaintainHardness.Add(qms20.SeamNo);
//                                        }
//                                    }
//                                    else
//                                    {
//                                        lstMaintainPMI.Add(qms20.SeamNo);
//                                    }
//                                }
//                                else
//                                {
//                                    if (qms20 != null)
//                                    {
//                                        lstMaintainSamplingplan.Add(qms20.SeamNo);
//                                    }
//                                }
//                            }
//                            else
//                            {
//                                lstQIDNotDraft.Add(qms20.SeamNo);
//                            }
//                        }
//                        #endregion
//                    }
//                }
//            }*/
//            #endregion
//            var arr = strHeader.Split(',').ToArray();
//            bool isError = false;
//            List<string> seams = new List<string>();
//            if (!string.IsNullOrWhiteSpace(result.swpNotDraft))
//            {
//                isError = true;
//                seams.AddRange(result.swpNotDraft.Split(',').ToArray());
//                //objResponseMsg.ActionKey = true;
//                objResponseMsg.NotDraft = string.Format("Seam {0} have been skipped as Shop Weld Plan {0} are either Sent For Approval Or Approved.", string.Join(",", result.swpNotDraft)); //objResponseMsg.NotDraftswp = string.Format("Seam {0} have been skipped as Shop Weld Plan {0} are either Sent For Approval Or Approved.", string.Join(",", result.swpNotDraft));  //objResponseMsg.NotDraftswp = string.Format("Shop Weld Plan for Seam {0} is either Sent For Approval Or Approved.", string.Join(",", result.swpNotDraft));
//            }
//            if (result.sCount > 0)
//            {
//                objResponseMsg.Value = string.Format("{0} Shop Weld Plan is sent for approval successfully.", result.sCount);
//            }
//            if (!string.IsNullOrWhiteSpace(result.swpMissedMendatoryData))
//            {
//                isError = true; //objResponseMsg.ActionKey = true;
//                seams.AddRange(result.swpMissedMendatoryData.Split(',').ToArray());
//                objResponseMsg.ActionValue = string.Format("Please Maintain JointType,WPS Number,WPS Rev No,Preheat & DHT/ISR for Seam No {0}.", result.swpMissedMendatoryData);
//            }

//            if (result.tpSCount > 0)
//            {
//                objResponseMsg.Value += string.Format(clsImplementationMessage.TestPlanMessages.SentForApproval, result.tpSCount);
//            }
//            if (!string.IsNullOrWhiteSpace(result.WithoutLines))
//            {
//                isError = true;
//                //objResponseMsg.dataKey = true;
//                seams.AddRange(result.WithoutLines.Split(',').ToArray());
//                objResponseMsg.WithoutLines = string.Format("No Stage(s) Exist(s) For Seam :{0}.Please Add Stages.", result.WithoutLines);
//            }
//            if (!string.IsNullOrWhiteSpace(result.tpNotDraft))
//            {
//                isError = true; //objResponseMsg.dataKey = true;
//                seams.AddRange(result.tpNotDraft.Split(',').ToArray());
//                objResponseMsg.NotDraft = string.Format("Seam {0} have been skipped as Test plan {0} are either Sent For Approval Or Approved", result.tpNotDraft);  //objResponseMsg.NotDraft = string.Format("Test plan  for Seam {0} is either Sent For Approval Or Approved", result.tpNotDraft);
//            }
//            if (!string.IsNullOrWhiteSpace(result.NotApplicableForParallel))
//            {
//                isError = true; //objResponseMsg.dataKey = true;
//                seams.AddRange(result.NotApplicableForParallel.Split(',').ToArray());
//                objResponseMsg.NotApplicableForParallel = string.Format("Seam {0} have been skipped as PT/MT cannot be the parallel stages with RT/UT", result.NotApplicableForParallel);
//            }
//            if (!string.IsNullOrWhiteSpace(result.MaintainSamplingplan))
//            {
//                isError = true; //objResponseMsg.dataKey = true;
//                seams.AddRange(result.MaintainSamplingplan.Split(',').ToArray());
//                objResponseMsg.Remarks = string.Format("Seam {0} have been skipped.Please 'Maintain Sampling Plan and Element for Chemical Stages'.", result.MaintainSamplingplan);
//            }
//            if (!string.IsNullOrWhiteSpace(result.MaintainHardness))
//            {
//                isError = true; //objResponseMsg.dataKey = true;
//                seams.AddRange(result.MaintainHardness.Split(',').ToArray());
//                objResponseMsg.dataValue = string.Format("Seam {0} have been skipped.Please 'Maintain hardness required Value'.", result.MaintainHardness);
//            }
//            if (!string.IsNullOrWhiteSpace(result.MaintainFerrite))
//            {
//                isError = true; //objResponseMsg.dataKey = true;
//                seams.AddRange(result.MaintainFerrite.Split(',').ToArray());
//                objResponseMsg.TPISkipped = string.Format("Seam {0} have been skipped.Please 'Maintain ferrite test required range'.", result.MaintainFerrite);
//            }
//            if (!string.IsNullOrWhiteSpace(result.MaintainPMI))
//            {
//                isError = true; //objResponseMsg.dataKey = true;
//                seams.AddRange(result.MaintainPMI.Split(',').ToArray());
//                objResponseMsg.tpi = string.Format("Seam {0} have been skipped.Please 'Maintain Sampling Plan and Element for PMI Stages'.", result.MaintainPMI);
//            }
//            objResponseMsg.Key = true;
//            if (arr.Count() > 1 && isError)
//            {
//                objResponseMsg.IsInformation = true;
//                objResponseMsg.dataValue = string.Format("{0} Seams skipped due to required data missing. Please try to submit individual for check actual reason for particular seam.", seams.Distinct().Count());
//            }
//        }
//        else
//        {
//            objResponseMsg.Key = false;
//            objResponseMsg.Value = "Please select record";
//        }
//        //objResponseMsg.Value = "Weld Plan is sent for approval";//clsImplementationMessage.CommonMessages.sentforApprove.ToString();
//    }
//    catch (Exception ex)
//    {
//        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
//        objResponseMsg.Key = false;
//        objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
//    }
//    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
//}

[HttpPost]
public JsonResult createRevison(int headerid)
{
    clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
    try
    {
        int? revison = (from a in db.SWP010
                        where a.HeaderId == headerid
                        select a.RevNo).FirstOrDefault();
        revison = revison + 1;
        SWP010 objSWP010 = db.SWP010.Where(x => x.HeaderId == headerid).FirstOrDefault();
        objSWP010.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
        objSWP010.RevNo = revison;
        objSWP010.EditedBy = objClsLoginInfo.UserName;
        objSWP010.EditedOn = DateTime.Now;
        db.SaveChanges();
        objResponseMsg.Key = true;
        objResponseMsg.Value = revison.ToString();
    }
    catch (Exception ex)
    {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        objResponseMsg.Key = false;
        objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
    }
    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
}

[HttpPost]
public ActionResult getCopy(int headerid)
{
    ViewBag.headerid = headerid;
    var QualityProject = db.SWP010.Where(i => i.HeaderId == headerid).Select(i => i.QualityProject).FirstOrDefault();
    ViewBag.QualityProject = QualityProject;
    var SeamNo = db.SWP010.Where(i => i.HeaderId == headerid).Select(i => i.SeamNo).FirstOrDefault();
    ViewBag.SeamNo = SeamNo;
    //var objQMS020 = db.QMS020.Where(i => i.HeaderId == headerid).FirstOrDefault();
    //ViewBag.QualityProject = objQMS020.QualityProject;
    //ViewBag.SeamNo = objQMS020.SeamNo;
    //ViewBag.Seams = db.QMS020.Where(i => i.QualityProject.Equals(objQMS020.QualityProject, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(objQMS020.Location, StringComparison.OrdinalIgnoreCase) && i.BU.Equals(objQMS020.BU, StringComparison.OrdinalIgnoreCase)).OrderBy(i => i.SeamNo).Select(i => i.SeamNo).ToList();

    return PartialView("_copyPartial");
}

[HttpPost]
public ActionResult GetSeamResult(int HId, string QP, string term)
{

    List<SeamModel> lstSeam = GetSeamList(HId, QP, term).ToList();
    return Json(lstSeam, JsonRequestBehavior.AllowGet);
}

[HttpPost]
public ActionResult copyDetails(int strheaderID, string fromSeam, string toSeam)
{
    clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
    try
    {
        var objSWP010 = db.SWP010.Where(i => i.HeaderId == strheaderID).FirstOrDefault();

        List<SWP010> allSeams = db.SWP010.Where(i => i.QualityProject.Equals(objSWP010.QualityProject, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(objSWP010.Location, StringComparison.OrdinalIgnoreCase) && i.BU.Equals(objSWP010.BU, StringComparison.OrdinalIgnoreCase)).OrderBy(i => i.SeamNo).ToList();

        List<string> newSeamList = new List<string>();
        bool start = false;

        foreach (SWP010 seam in allSeams)
        {
            if (seam.SeamNo == fromSeam)
            {
                start = true;
            }

            if (start)
                newSeamList.Add(seam.SeamNo);

            if (seam.SeamNo == toSeam)
            {
                break;
            }
        }

        //int h1 = (from a in db.SWP010
        //          where a.SeamNo == fromSeam
        //          select a.HeaderId).FirstOrDefault();

        //int h2 = (from a in db.SWP010
        //          where a.SeamNo == toSeam
        //          select a.HeaderId).FirstOrDefault();

        //var strlstseam = string.Join(",", (from a in db.SWP010
        //                                   where a.HeaderId >= h1 && a.HeaderId <= h2
        //                                   select a.SeamNo).ToList());
        var strlstseam = string.Join(",", newSeamList);
        string currentUser = objClsLoginInfo.UserName;
        var currentLoc = objClsLoginInfo.Location;
        var Result = db.SP_IPI_SWP_COPY(currentUser, strheaderID, strlstseam).FirstOrDefault();
        if (Result.ToString() == "IS")
        {
            objResponseMsg.Key = true;
            objResponseMsg.Value = "Details has been Copied successfully.";
        }
        else
        {
            objResponseMsg.Key = false;
            objResponseMsg.Value = "Details not available.";
        }
    }
    catch (Exception ex)
    {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        objResponseMsg.Key = false;
        objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
    }
    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
}

//[HttpPost]
//public ActionResult copyDetails(int swpHeaderId, int tpheaderId, string fromSeam, string toSeam) //public ActionResult copyDetails(int swpHeaderId, int tpheaderId, string fromSeam, string toSeam, string strOption)
//{
//    clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
//    try
//    {
//        var objSWP010 = db.SWP010.Where(i => i.HeaderId == swpHeaderId).FirstOrDefault();

//        List<string> testSeamList = (from i in db.SWP010
//                                     where i.QualityProject.Equals(objSWP010.QualityProject, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(objSWP010.Location, StringComparison.OrdinalIgnoreCase) && i.BU.Equals(objSWP010.BU, StringComparison.OrdinalIgnoreCase) &&
//                                           i.SeamNo.CompareTo(fromSeam) >= 0 && i.SeamNo.CompareTo(toSeam) <= 0
//                                     select i.SeamNo).ToList();

//        var destinationSeams = string.Join(",", testSeamList);

//        string currentUser = objClsLoginInfo.UserName;
//        bool isCopied = false;
//        var Result = db.SP_IPI_SWP_TP_COPY(currentUser, swpHeaderId, tpheaderId, destinationSeams).FirstOrDefault();
//        List<string> seams = new List<string>();
//        if (!string.IsNullOrWhiteSpace(Result.SuccessTPSeam))
//        {
//            seams.AddRange(Result.SuccessTPSeam.Split(','));
//        }
//        if (!string.IsNullOrWhiteSpace(Result.SuccessSWPPSeam))
//        {
//            seams.AddRange(Result.SuccessSWPPSeam.Split(','));
//        }
//        if (Convert.ToInt32(seams.Distinct().Count()) > 0)
//        {
//            isCopied = true;
//            objResponseMsg.Value = seams.Distinct().Count() + " Seam data is copied successfully";
//        }
//        else
//        {
//            objResponseMsg.Key = false;
//            objResponseMsg.Value = "Details not available.";
//        }
//        //if (strOption == "BOTH")
//        //{
//        //    var Result = db.SP_IPI_SWP_TP_COPY(currentUser, swpHeaderId, tpheaderId, destinationSeams).FirstOrDefault();
//        //    List<string> seams = new List<string>();
//        //    if (!string.IsNullOrWhiteSpace(Result.SuccessTPSeam))
//        //    {
//        //        seams.AddRange(Result.SuccessTPSeam.Split(','));
//        //    }
//        //    if (!string.IsNullOrWhiteSpace(Result.SuccessSWPPSeam))
//        //    {
//        //        seams.AddRange(Result.SuccessSWPPSeam.Split(','));
//        //    }
//        //    if (Convert.ToInt32(seams.Distinct().Count()) > 0)
//        //    {
//        //        isCopied = true;
//        //        objResponseMsg.Value = seams.Distinct().Count() + " Seam data is copied successfully";
//        //    }
//        //    else
//        //    {
//        //        objResponseMsg.Key = false;
//        //        objResponseMsg.Value = "No Seams found for copy!!";
//        //    }
//        //}
//        //if (strOption == "TP")
//        //{
//        //    var Result = db.SP_IPI_TEST_PLAN_COPY(currentUser, tpheaderId, destinationSeams).FirstOrDefault();
//        //    if (Convert.ToInt32(Result) > 0)
//        //    {
//        //        isCopied = true;
//        //        objResponseMsg.Value = Convert.ToString(Result) + " Seam data is copied successfully";
//        //    }
//        //    else
//        //    {
//        //        objResponseMsg.Key = false;
//        //        objResponseMsg.Value = "No Seams found for copy!!";
//        //    }
//        //}
//        //if (strOption == "SWP")
//        //{
//        //    var currentLoc = objClsLoginInfo.Location;
//        //    var Result = db.SP_IPI_SWP_COPY(currentUser, swpHeaderId, destinationSeams).FirstOrDefault();
//        //    if (Result.ToString() == "IS")
//        //    {
//        //        isCopied = true;
//        //        objResponseMsg.Value = "Details has been Copied successfully.";
//        //    }
//        //    else
//        //    {
//        //        objResponseMsg.Key = false;
//        //        objResponseMsg.Value = "No Seams found for copy!!";
//        //    }
//        //}
//        objResponseMsg.Key = isCopied;
//    }
//    catch (Exception ex)
//    {
//        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
//        objResponseMsg.Key = false;
//        objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
//    }
//    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
//}
public List<SeamModel> GetSeamList(int HId, string QualityProject, string terms)
{
    List<SeamModel> lstSeamModel = new List<SeamModel>();
    var objSWP010 = db.SWP010.Where(i => i.HeaderId == HId).FirstOrDefault();
    var lstSeam = db.SWP010.Where(i => i.QualityProject.Equals(objSWP010.QualityProject, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(objSWP010.Location, StringComparison.OrdinalIgnoreCase) && i.BU.Equals(objSWP010.BU, StringComparison.OrdinalIgnoreCase) && i.SeamNo.Contains(terms)).OrderBy(i => i.SeamNo).Select(z => new SeamModel
    {
        Code = z.SeamNo,
        Name = z.SeamNo
    }).ToList(); ;
    //var lstSeam = db.SWP010.Where(i => i.QualityProject == QualityProject && i.SeamNo.Contains(terms)).Select(z => new SeamModel
    //{
    //    Code = z.SeamNo,
    //    Name = z.SeamNo
    //}).ToList();
    if (lstSeam.Count() > 0)
    {
        lstSeamModel = lstSeam.Select(x => new SeamModel { Code = x.Code, Name = x.Name }).ToList();
    }

    return lstSeamModel;
}
[HttpPost]
public ActionResult getTransfer(int headerid)
{
    ViewBag.headerid = headerid;
    var QualityProject = db.SWP010.Where(i => i.HeaderId == headerid).Select(i => i.QualityProject).FirstOrDefault();
    ViewBag.QualityProject = QualityProject;
    return PartialView("_transferPartial");
}
[HttpPost]
public ActionResult GetProjectResult(string term)
{

    List<SeamModel> lstSeam = GetProjectList(term).ToList();
    return Json(lstSeam, JsonRequestBehavior.AllowGet);
}
public List<SeamModel> GetProjectList(string terms)
{
    List<SeamModel> lstQPModel = new List<SeamModel>();

    var lstQP = db.SWP010.Select(z => new SeamModel
    {
        Code = z.QualityProject,
        Name = z.QualityProject
    }).ToList();
    if (lstQP.Count() > 0)
    {
        lstQPModel = lstQP.Select(x => new SeamModel { Code = x.Code, Name = x.Name }).ToList();
    }

    return lstQPModel;
}
[HttpPost]
public ActionResult transferDetails(int strheaderID, string fromSeam, string toSeam, string fromProject, string toProject)
{
    clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
    var approveString = clsImplementationEnum.UTCTQStatus.Approved.GetStringValue();
    var sourceSeams = string.Empty;
    var destQMSProjects = string.Empty;

    var objSWP010 = db.SWP010.Where(i => i.HeaderId == strheaderID).FirstOrDefault();

    #region Get Source Seams List for transfer

    List<SWP010> allSeams = db.SWP010.Where(i => i.QualityProject.Equals(objSWP010.QualityProject, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(objSWP010.Location, StringComparison.OrdinalIgnoreCase) && i.BU.Equals(objSWP010.BU, StringComparison.OrdinalIgnoreCase)).OrderBy(i => i.SeamNo).ToList();
    List<string> newSeamList = new List<string>();
    bool start = false;
    foreach (SWP010 seam in allSeams)
    {
        if (seam.SeamNo == fromSeam)
        {
            start = true;
        }
        if (start)
            newSeamList.Add(seam.SeamNo);
        if (seam.SeamNo == toSeam)
        {
            break;
        }
    }
    if (newSeamList.Count > 0)
        sourceSeams = string.Join(",", newSeamList);

    #endregion

    #region Get Destinatino QMS project for transfer

    List<SWP010> allQMSProjects = (from a in db.SWP010
                                   where a.Location.Contains(objClsLoginInfo.Location)
                                   orderby a.QualityProject
                                   select a).Distinct().ToList();
    List<string> newQMSProjectList = new List<string>();

    start = false;
    foreach (SWP010 proj in allQMSProjects)
    {
        if (proj.QualityProject == fromProject)
        {
            start = true;
        }
        if (start)
            newQMSProjectList.Add(proj.QualityProject);
        if (proj.QualityProject == toProject)
        {
            break;
        }
    }
    if (newQMSProjectList.Count > 0)
        destQMSProjects = string.Join(",", newQMSProjectList);
    #endregion
    try
    {

        //int h1 = (from a in db.SWP010
        //          where a.SeamNo == fromSeam
        //          select a.HeaderId).FirstOrDefault();

        //int h2 = (from a in db.SWP010
        //          where a.SeamNo == toSeam
        //          select a.HeaderId).FirstOrDefault();

        //int hp1 = (from a in db.SWP010
        //           where a.QualityProject == fromProject
        //           select a.HeaderId).FirstOrDefault();

        //int hp2 = (from a in db.SWP010
        //           where a.QualityProject == toProject
        //           select a.HeaderId).FirstOrDefault();

        //var strlstseam = string.Join(",", (from a in db.SWP010
        //                                   where a.HeaderId >= h1 && a.HeaderId <= h2
        //                                   select a.SeamNo).ToList());

        //var strlstproj = string.Join(",", (from a in db.SWP010
        //                                   where a.HeaderId >= hp1 && a.HeaderId <= hp2
        //                                   select a.QualityProject).Distinct().ToList());

        string currentUser = objClsLoginInfo.UserName;
        var currentLoc = objClsLoginInfo.Location;
        var Result = db.SP_IPI_SWP_TRANSFER(currentUser, strheaderID, sourceSeams, destQMSProjects).FirstOrDefault();
        if (Result.ToString() == "IS")
        {
            objResponseMsg.Key = true;
            objResponseMsg.Value = "Details has been Transfered successfully.";
        }
        else
        {
            objResponseMsg.Key = false;
            objResponseMsg.Value = "Details not available.";
        }
    }
    catch (Exception ex)
    {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        objResponseMsg.Key = false;
        objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
    }
    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
}
[HttpPost]
public ActionResult GetQualityProjectDetails(string QP, string term)
{
    string location = objClsLoginInfo.Location;
    List<Projects> lstQualityProject;
    var project = db.QMS010.Where(x => x.QualityProject == QP && x.Location == location).Select(x => x.Project).FirstOrDefault();
    if (!string.IsNullOrEmpty(term))
    {
        lstQualityProject = (from a in db.QMS010
                             where a.QualityProject.ToLower().Contains(term.ToLower()) && a.Location.Equals(location) && a.Project == project
                             orderby a.QualityProject
                             select new Projects { Value = a.QualityProject, projectCode = a.QualityProject }).Distinct().ToList();
        //db.SWP010.Where(i => i.QualityProject.ToLower().Contains(term.ToLower()) && i.Location.Equals(location)).OrderBy(i => i.QualityProject).Select(i => new Projects { Value = i.QualityProject, projectCode = i.QualityProject }).ToList();//&& i.CreatedBy.Equals(username,StringComparison.OrdinalIgnoreCase)
    }
    else
        lstQualityProject = (from a in db.QMS010
                             where a.Location.Equals(location) && a.Project == project
                             orderby a.QualityProject
                             select new Projects { Value = a.QualityProject, projectCode = a.QualityProject }).Distinct().ToList();
    //lstQualityProject = db.SWP010.Where(i => i.Location.Equals(location)).OrderBy(i => i.QualityProject).Select(i => new Projects { Value = i.QualityProject, projectCode = i.QualityProject }).ToList();//&& i.CreatedBy.Equals(username,StringComparison.OrdinalIgnoreCase)
    return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
}

[HttpPost]
public ActionResult UpdateJT(string header, string JT)
{
    ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
    try
    {
        int HeaderId = Convert.ToInt32(header);
        SWP010 objSWP010 = db.SWP010.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
        if (objSWP010 != null)
        {
            objSWP010.JointType = JT;
            db.SaveChanges();
            objResponseMsg.Key = true;
        }
        else
        {
            objResponseMsg.Key = false;
            objResponseMsg.Value = "Something went wrong..!";
        }
    }
    catch (Exception ex)
    {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        objResponseMsg.Key = false;
        objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
    }
    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
}
[HttpPost]
public ActionResult UpdateWPS(string header, string WPS, string Index)
{
    ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
    try
    {
        int HeaderId = Convert.ToInt32(header);
        string QualityProject = (from a in db.SWP010
                                 where a.HeaderId == HeaderId
                                 select a.QualityProject).FirstOrDefault();

        int? Revno = (from a in db.WPS012_Log
                      where a.QualityProject == QualityProject
                      select a.WPSRevNo).FirstOrDefault();


        string ALT1 = (from a in db.SWP010
                       where a.HeaderId == HeaderId
                       select a.AlternateWPS1).FirstOrDefault();

        string ALT2 = (from a in db.SWP010
                       where a.HeaderId == HeaderId
                       select a.AlternateWPS2).FirstOrDefault();

        if (ALT1 == "" && ALT2 == "")
        {
            SWP010 objSWP010 = db.SWP010.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            if (objSWP010 != null)
            {
                objSWP010.MainWPSNumber = WPS;
                objSWP010.MainWPSRevNo = Revno;
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Something went wrong..!";
            }
        }
        else
        {
            if (ALT1 != WPS && ALT2 != WPS)
            {
                SWP010 objSWP010 = db.SWP010.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                if (objSWP010 != null)
                {
                    objSWP010.MainWPSNumber = WPS;
                    objSWP010.MainWPSRevNo = Revno;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Something went wrong..!";
                }
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Main WPS No. Already exists..!";
            }
        }
    }
    catch (Exception ex)
    {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        objResponseMsg.Key = false;
        objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
    }
    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
}
[HttpPost]
public ActionResult UpdateWPS1(string header, string WPS, string Index)
{
    ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
    try
    {
        int HeaderId = Convert.ToInt32(header);

        string QualityProject = (from a in db.SWP010
                                 where a.HeaderId == HeaderId
                                 select a.QualityProject).FirstOrDefault();

        int? Revno = (from a in db.WPS012_Log
                      where a.QualityProject == QualityProject
                      select a.WPSRevNo).FirstOrDefault();

        string MWPS = (from a in db.SWP010
                       where a.HeaderId == HeaderId
                       select a.MainWPSNumber).FirstOrDefault();

        string ALT2 = (from a in db.SWP010
                       where a.HeaderId == HeaderId
                       select a.AlternateWPS2).FirstOrDefault();

        if (MWPS == "" && ALT2 == "")
        {
            SWP010 objSWP010 = db.SWP010.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            if (objSWP010 != null)
            {
                objSWP010.AlternateWPS1 = WPS;
                objSWP010.MainWPSRevNo = Revno;
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Something went wrong..!";
            }
        }
        else
        {
            if (MWPS != WPS && ALT2 != WPS)
            {
                SWP010 objSWP010 = db.SWP010.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                if (objSWP010 != null)
                {
                    objSWP010.AlternateWPS1 = WPS;
                    objSWP010.MainWPSRevNo = Revno;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Something went wrong..!";
                }
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Alternate WPS No1. Already exists..!";
            }
        }
    }
    catch (Exception ex)
    {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        objResponseMsg.Key = false;
        objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
    }
    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
}
[HttpPost]
public ActionResult UpdateWPS2(string header, string WPS, string Index)
{
    ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
    try
    {
        int HeaderId = Convert.ToInt32(header);
        string QualityProject = (from a in db.SWP010
                                 where a.HeaderId == HeaderId
                                 select a.QualityProject).FirstOrDefault();

        int? Revno = (from a in db.WPS012_Log
                      where a.QualityProject == QualityProject
                      select a.WPSRevNo).FirstOrDefault();

        string MWPS = (from a in db.SWP010
                       where a.HeaderId == HeaderId
                       select a.MainWPSNumber).FirstOrDefault();

        string ALT1 = (from a in db.SWP010
                       where a.HeaderId == HeaderId
                       select a.AlternateWPS1).FirstOrDefault();

        if (MWPS == "" && ALT1 == "")
        {
            SWP010 objSWP010 = db.SWP010.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            if (objSWP010 != null)
            {
                objSWP010.AlternateWPS2 = WPS;
                objSWP010.MainWPSRevNo = Revno;
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Something went wrong..!";
            }
        }
        else
        {
            if (MWPS != WPS && ALT1 != WPS)
            {
                SWP010 objSWP010 = db.SWP010.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                if (objSWP010 != null)
                {
                    objSWP010.AlternateWPS2 = WPS;
                    objSWP010.MainWPSRevNo = Revno;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Something went wrong..!";
                }
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Alternate WPS No2. Already exists..!";
            }
        }
    }
    catch (Exception ex)
    {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        objResponseMsg.Key = false;
        objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
    }
    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
}
[HttpPost]
public ActionResult getRevNo(string WPS, string QP, string Location, int HeaderId)
{
    ResponceMsgWithStatus1 objResponseMsg = new ResponceMsgWithStatus1();
    string strApproved = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
    try
    {
        int? Rev;
        //if (db.WPS013.Any(i => i.IQualityProject == QP))
        //{
        //    var MainQualityProject = db.WPS013.Where(i => i.IQualityProject == QP).FirstOrDefault().QualityProject;
        //    Rev = db.WPS012.Where(i => i.WPSNumber == WPS && i.QualityProject == MainQualityProject && i.Status == strApproved && i.Location == Location).Select(i => i.WPSRevNo).FirstOrDefault();
        //}
        //else
        //{
        //    Rev = db.WPS012.Where(i => i.WPSNumber == WPS && i.QualityProject == QP && i.Status == strApproved && i.Location == Location).Select(i => i.WPSRevNo).FirstOrDefault();
        //}
        var objSWP010 = db.SWP010.Where(i => i.HeaderId == HeaderId).FirstOrDefault();
        if (objSWP010.MainWPSNumber == WPS)
        {
            Rev = objSWP010.MainWPSRevNo;
        }
        else if (objSWP010.AlternateWPS1 == WPS)
        {
            Rev = objSWP010.AlternateWPS1RevNo;
        }
        else if (objSWP010.AlternateWPS2 == WPS)
        {
            Rev = objSWP010.AlternateWPS2RevNo;
        }
        else
        {
            Rev = null;
        }
        objResponseMsg.Revi = Rev.ToString();
    }
    catch (Exception ex)
    {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        objResponseMsg.Key = false;
        objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
    }
    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
}
[HttpPost]
public ActionResult verifyWPS(string WPS, string QP)
{
    ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
    try
    {
        int? HeaderId = (from a in db.SWP010
                         where a.AlternateWPS1 == WPS || a.AlternateWPS2 == WPS
                         select a.HeaderId).FirstOrDefault();
        if (HeaderId > 0)
        {
            objResponseMsg.Key = false;
        }
        else
        {
            objResponseMsg.Key = true;
        }
    }
    catch (Exception ex)
    {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        objResponseMsg.Key = false;
        objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
    }
    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
}
[HttpPost]
public ActionResult verifyWPS1(string WPS, string QP)
{
    ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
    try
    {
        int? HeaderId = (from a in db.SWP010
                         where a.MainWPSNumber == WPS || a.AlternateWPS2 == WPS
                         select a.HeaderId).FirstOrDefault();
        if (HeaderId > 0)
        {
            objResponseMsg.Key = false;
        }
        else
        {
            objResponseMsg.Key = true;
        }
    }
    catch (Exception ex)
    {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        objResponseMsg.Key = false;
        objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
    }
    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
}
[HttpPost]
public ActionResult verifyWPS2(string WPS, string QP)
{
    ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
    try
    {
        int? HeaderId = (from a in db.SWP010
                         where a.AlternateWPS1 == WPS || a.MainWPSNumber == WPS
                         select a.HeaderId).FirstOrDefault();
        if (HeaderId > 0)
        {
            objResponseMsg.Key = false;
        }
        else
        {
            objResponseMsg.Key = true;
        }
    }
    catch (Exception ex)
    {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        objResponseMsg.Key = false;
        objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
    }
    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
}
public ActionResult ShowTimeline(int HeaderId) //public ActionResult ShowTimeline(int HeaderId, int tpheaderid)
{
    TimelineViewModel model = new TimelineViewModel();

    SWP010 objSWP010 = db.SWP010.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
    if (objSWP010 != null)
    {
        model.ApprovedBy = objSWP010.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objSWP010.ApprovedBy) : null;
        model.ApprovedOn = objSWP010.ApprovedOn;
        model.SubmittedBy = objSWP010.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objSWP010.SubmittedBy) : null;
        model.SubmittedOn = objSWP010.SubmittedOn;
        model.CreatedBy = objSWP010.CreatedBy != null ? Manager.GetUserNameFromPsNo(objSWP010.CreatedBy) : null;
        model.CreatedOn = objSWP010.CreatedOn;
        model.EditedBy = objSWP010.EditedBy != null ? Manager.GetUserNameFromPsNo(objSWP010.EditedBy) : null;
        model.EditedOn = objSWP010.EditedOn;
        model.ReturnedBy = objSWP010.ReturnedBy != null ? Manager.GetUserNameFromPsNo(objSWP010.ReturnedBy) : null;
        model.ReturnedOn = objSWP010.ReturnedOn;
    }

    //QMS020 objQMS020 = db.QMS020.Where(x => x.HeaderId == tpheaderid).FirstOrDefault();
    //if (objQMS020 != null)
    //{
    //    model.TPApprovedBy = objQMS020.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objQMS020.ApprovedBy) : null;
    //    model.TPApprovedOn = objQMS020.ApprovedOn;
    //    model.TPSubmittedBy = objQMS020.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objQMS020.SubmittedBy) : null;
    //    model.TPSubmittedOn = objQMS020.SubmittedOn;
    //    model.TPCreatedBy = objQMS020.CreatedBy != null ? Manager.GetUserNameFromPsNo(objQMS020.CreatedBy) : null;
    //    model.TPCreatedOn = objQMS020.CreatedOn;
    //    model.TPEditedBy = objQMS020.EditedBy != null ? Manager.GetUserNameFromPsNo(objQMS020.EditedBy) : null;
    //    model.TPEditedOn = objQMS020.EditedOn;
    //}
    return PartialView("~/Views/Shared/_TimelineProgress2.cshtml", model);  //return PartialView("_SWPTPTimelineProgress", model);
}
public ActionResult ShowLogTimeline(int id)
{
    TimelineViewModel model = new TimelineViewModel();

    SWP010_Log objSWP010 = db.SWP010_Log.Where(x => x.Id == id).FirstOrDefault();
    model.ApprovedBy = objSWP010.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objSWP010.ApprovedBy) : null;
    model.ApprovedOn = objSWP010.ApprovedOn;
    model.SubmittedBy = objSWP010.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objSWP010.SubmittedBy) : null;
    model.SubmittedOn = objSWP010.SubmittedOn;
    model.CreatedBy = objSWP010.CreatedBy != null ? Manager.GetUserNameFromPsNo(objSWP010.CreatedBy) : null;
    model.CreatedOn = objSWP010.CreatedOn;
    model.EditedBy = objSWP010.EditedBy != null ? Manager.GetUserNameFromPsNo(objSWP010.EditedBy) : null;
    model.EditedOn = objSWP010.EditedOn;


    return PartialView("~/Views/Shared/_TimelineProgress2.cshtml", model);
}

[HttpPost]
public ActionResult LoadSWPTransferDetailPartial(int HeaderID, string location)
{
    ViewBag.Location = location;//db.COM002.Where(a => a.t_dimx == location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
    var objSWP010 = db.SWP010.Where(i => i.HeaderId == HeaderID).FirstOrDefault();
    ViewBag.QualityProject = objSWP010.QualityProject;
    ViewBag.SeamNo = objSWP010.SeamNo;
    ViewBag.Seams = GetSeamListForTransfer(objSWP010.QualityProject, objSWP010.Location, objSWP010.BU);
    ViewBag.IQualityProject = GetIdenticalProjectList(objSWP010.QualityProject);
    return PartialView("_LoadSWPTransferDetailPartial");
}

private List<string> GetIdenticalProjectList(string QualityProject)
{
    List<string> IqProjectList = new List<string>();
    var lstiproject = db.WPS013.Where(i => i.QualityProject == QualityProject).Select(i => i.IQualityProject).Distinct().OrderBy(i => i).ToList();
    if (lstiproject.Count == 0)
    {
        var qproject = db.WPS013.Where(i => i.IQualityProject == QualityProject).Select(i => i.QualityProject).FirstOrDefault();
        if (qproject != null)
        {
            IqProjectList.Add(qproject);
        }
        lstiproject = db.WPS013.Where(i => i.QualityProject == qproject && i.IQualityProject != QualityProject).Select(i => i.IQualityProject).Distinct().OrderBy(i => i).ToList();
        IqProjectList.AddRange(lstiproject);
    }
    else
    {
        IqProjectList.AddRange(lstiproject);
    }

    return IqProjectList;
}
private List<string> GetSeamListForTransfer(string QualityProject, string Location, string BU)
{
    return db.SWP010.Where(i => i.QualityProject.Equals(QualityProject, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(Location, StringComparison.OrdinalIgnoreCase) && i.BU.Equals(BU, StringComparison.OrdinalIgnoreCase) && !string.IsNullOrEmpty(i.MainWPSNumber)).OrderBy(i => i.SeamNo).Select(i => i.SeamNo).ToList();
}

public bool IsApplicable(string status, bool isEqual)
{
    bool flag = false;
    string sendforapproval = clsImplementationEnum.CommonStatus.SendForApprovel.GetStringValue();
    if (isEqual)
    {
        if (status == sendforapproval)
        {
            flag = true;
        }
    }
    else
    {
        if (status != sendforapproval)
        {
            flag = true;
        }
    }

    return flag;
}

#region Excel Import/Export
public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "", string BU = "", string Location = "")
{
    clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
    clsHelper.ResponceMsgWithFileName objexport = new clsHelper.ResponceMsgWithFileName();
    try
    {
        string strFileName = string.Empty;
        if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
        {
            List<SP_SWP_Get_HeaderList_Result> lst = db.SP_SWP_Get_HeaderList(1, int.MaxValue, strSortOrder, whereCondition, BU, Location).ToList();
            if (!lst.Any())
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "No Data Found";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }

            objexport = GenerateHeaderExcel(lst);
            strFileName = objexport.FileName;
        }
        objResponseMsg.Key = true;
        objResponseMsg.Value = strFileName;
        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
    }
    catch (Exception ex)
    {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        objResponseMsg.Key = false;
        objResponseMsg.Value = "Error in excel generate, Please try again";
        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
    }
}

[HttpPost]
public ActionResult tranferSWPDetails(int strheaderID, string fromSeam, string toSeam, string fromQProj, string toQProj)
{
    string currentUser = objClsLoginInfo.UserName;
    clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
    var approveString = clsImplementationEnum.TestPlanStatus.Approved.GetStringValue();
    var sourceSeams = string.Empty;
    var destQMSProjects = string.Empty;

    var objSWP010 = db.SWP010.Where(i => i.HeaderId == strheaderID).FirstOrDefault();

    #region Get Source Seams List for transfer

    List<string> allSeams = GetSeamListForTransfer(objSWP010.QualityProject, objSWP010.Location, objSWP010.BU); ;
    List<string> newSeamList = new List<string>();
    bool start = false;
    foreach (string seam in allSeams)
    {
        if (seam == fromSeam)
        {
            start = true;
        }
        if (start)
            newSeamList.Add(seam);
        if (seam == toSeam)
        {
            break;
        }
    }
    if (newSeamList.Count > 0)
        sourceSeams = string.Join(",", newSeamList);

    #endregion

    #region Get Destinatino QMS project for transfer

    var IQualityProjectList = GetIdenticalProjectList(objSWP010.QualityProject);
    //List<SWP010> allQMSProjects = (from a in db.SWP010
    //                               where a.Location.Contains(objClsLoginInfo.Location) && IQualityProjectList.Contains(a.QualityProject)
    //                               orderby a.QualityProject
    //                               select a).Distinct().OrderBy(x => x.QualityProject).ToList();
    List<string> newQMSProjectList = new List<string>();

    start = false;
    foreach (string proj in IQualityProjectList)
    {
        if (proj == fromQProj)
        {
            start = true;
        }
        if (start)
        {
            newQMSProjectList.Add(proj);
        }
        if (proj == toQProj)
        {
            break;
        }
    }
    if (newQMSProjectList.Count > 0)
        destQMSProjects = string.Join(",", newQMSProjectList.Distinct());

    #endregion

    try
    {
        if (newQMSProjectList.Count > 0)
        {
            var Result = 0;
            foreach (var qualityproject in newQMSProjectList.Distinct().ToList())
            {
                foreach (var seamno in newSeamList.Distinct().ToList())
                {
                    var objSWP010Source = db.SWP010.Where(i => i.QualityProject == objSWP010.QualityProject && i.SeamNo == seamno && i.Location == objSWP010.Location).FirstOrDefault();

                    var objSWP010Destination = db.SWP010.Where(i => i.QualityProject == qualityproject && i.SeamNo == seamno && i.Location == objSWP010.Location && string.IsNullOrEmpty(i.MainWPSNumber)).FirstOrDefault();
                    if (objSWP010Destination != null)
                    {
                        objSWP010Destination.MainWPSNumber = objSWP010Source.MainWPSNumber;
                        objSWP010Destination.MainWPSRevNo = objSWP010Source.MainWPSRevNo;
                        objSWP010Destination.AlternateWPS1 = objSWP010Source.AlternateWPS1;
                        objSWP010Destination.AlternateWPS1RevNo = objSWP010Source.AlternateWPS1RevNo;
                        objSWP010Destination.AlternateWPS2 = objSWP010Source.AlternateWPS2;
                        objSWP010Destination.AlternateWPS2RevNo = objSWP010Source.AlternateWPS2RevNo;
                        objSWP010Destination.Preheat = objSWP010Source.Preheat;
                        objSWP010Destination.DHTISR = objSWP010Source.DHTISR;
                        objSWP010Destination.SWPNotes = objSWP010Source.SWPNotes;
                        objSWP010Destination.JointType = objSWP010Source.JointType;
                        objSWP010Destination.CreatedBy = objClsLoginInfo.UserName;
                        objSWP010Destination.CreatedOn = DateTime.Now;
                        objSWP010Destination.EditedBy = objClsLoginInfo.UserName;
                        objSWP010Destination.EditedOn = DateTime.Now;
                        Result = Result + 1;
                    }
                }
            }
            db.SaveChanges();
            if (Result > 0)
            {
                objResponseMsg.Key = true;
                objResponseMsg.Value = Convert.ToString(Result) + " Seam data is transfered successfully";
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "No Seams found for transfer";
            }
        }
        else
        {
            objResponseMsg.Key = false;
            objResponseMsg.Value = "No QualityProject found for transfer";
        }

    }
    catch (Exception ex)
    {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        objResponseMsg.Key = false;
        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
    }
    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
}

[HttpPost]
[SessionExpireFilter]
public ActionResult ImportExcel(HttpPostedFileBase upload, string aQualityProject, string aProject, string aBU, string aLocation)
{
    clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
    SWP010 objImport = new SWP010();
    objImport.QualityProject = aQualityProject.Trim();
    objImport.Project = aProject.Trim();
    objImport.BU = aBU.Trim();
    objImport.Location = aLocation.Trim();
    if (upload != null && !string.IsNullOrWhiteSpace(aProject))
    {
        if (Path.GetExtension(upload.FileName).ToLower() == ".xlsx" || Path.GetExtension(upload.FileName).ToLower() == ".xls")
        {
            ExcelPackage package = new ExcelPackage(upload.InputStream);
            ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();

            DataTable dtHeaderExcel = new DataTable();
            List<CategoryData> errors = new List<CategoryData>();

            string Approved = clsImplementationEnum.TestPlanStatus.Approved.GetStringValue();
            string Draft = clsImplementationEnum.TestPlanStatus.Draft.GetStringValue();
            string Returned = clsImplementationEnum.TestPlanStatus.Returned.GetStringValue();

            bool isError;
            DataSet ds = ToChechValidation(package, objImport, out isError);
            if (!isError)
            {
                try
                {
                    foreach (DataRow item in ds.Tables[0].Rows)
                    {
                        SWP010 objSWP010 = new SWP010();
                        string SeamNo = item.Field<string>("SeamNo");
                        string JointType = item.Field<string>("Joint Type");
                        string WPSNumber = item.Field<string>("WPS Number");
                        string WPSRevNo = item.Field<string>("WPS RevNo");
                        string AlternateWPS1 = item.Field<string>("Alternate WPS1");
                        string AlternateWPS1RevNo = item.Field<string>("Alternate WPS1 RevNo");
                        string AlternateWPS2 = item.Field<string>("Alternate WPS2");
                        string AlternateWPS2RevNo = item.Field<string>("Alternate WPS2 RevNo");
                        string PreHeat = item.Field<string>("Pre-Heat (Deg °C)");
                        string DHTISR = item.Field<string>("DHT / ISR");
                        string SWPNotes = item.Field<string>("SWP Notes");
                        string SWPRevNo = item.Field<string>("SWP RevNo");
                        string SWPStatus = item.Field<string>("SWP Status");
                        string ReturnRemarks = item.Field<string>("Return Remarks");
                        string DraftStatus = clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue();
                        string ReturnedStatus = clsImplementationEnum.QualityIdHeaderStatus.Returned.GetStringValue();
                        string ApprovedStatus = clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue();

                        bool isSave = false;
                        objSWP010 = db.SWP010.Where(x => x.SeamNo.Trim() == SeamNo.Trim() && x.QualityProject.Trim() == objImport.QualityProject.Trim()
                                                        && x.Project.Trim() == objImport.Project.Trim() && x.BU.Trim() == objImport.BU.Trim()
                                                        && x.Location == objImport.Location).FirstOrDefault();
                        #region listing
                        var lstJointType = Manager.GetSubCatagories("Joint Type", objImport.BU, objImport.Location).Select(i => i.Code).ToList();
                        var lstDhtIsr = clsImplementationEnum.GetDHTISR();
                        var listDhtIsr = lstDhtIsr.AsEnumerable().Select(x => x.ToString()).ToList();
                        var lstSWPNotes = Manager.GetGeneralNotes(objImport.QualityProject, "", objImport.Location).Select(i => i.NoteNumber.ToString()).ToList();


                        var lstWPS = (from a in db.WPS012
                                      where (a.QualityProject == objSWP010.QualityProject || (from b in db.WPS013 where b.QualityProject == objSWP010.QualityProject select b.IQualityProject).ToList().Contains(a.QualityProject))
                                      && a.Status.Equals(ApprovedStatus)
                                      && a.Jointtype.Equals(objSWP010.JointType)
                                      select new
                                      {
                                          id = a.WPSNumber,
                                          rev = a.WPSRevNo
                                      }).Distinct().ToList();
                        #endregion

                        if (objSWP010 != null ? ((objSWP010.Status != clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()) && !IsSeamOffered(objSWP010.HeaderId)) : false)
                        {
                            if ((objSWP010.Status == ApprovedStatus) &&
                                (objSWP010.JointType != JointType || objSWP010.MainWPSNumber != WPSNumber || objSWP010.AlternateWPS1 != AlternateWPS1 || objSWP010.AlternateWPS2 != AlternateWPS2
                                || objSWP010.Preheat != Convert.ToInt32(PreHeat) || objSWP010.DHTISR != DHTISR || objSWP010.SWPNotes != SWPNotes))
                            {
                                isSave = true;
                                objSWP010.RevNo = Convert.ToInt32(objSWP010.RevNo) + 1;
                                objSWP010.Status = clsImplementationEnum.UTCTQStatus.DRAFT.GetStringValue();
                                objSWP010.EditedBy = objClsLoginInfo.UserName;
                                objSWP010.EditedOn = DateTime.Now;
                            }
                            if (objSWP010.Status == DraftStatus || objSWP010.Status == ReturnedStatus)
                            {
                                isSave = true;
                            }
                            if (isSave)
                            {
                                #region update columns
                                objSWP010.JointType = lstJointType.Where(x => x.ToString() == JointType).Select(x => x.ToString()).FirstOrDefault();
                                objSWP010.MainWPSNumber = lstWPS.Where(x => x.id.ToString() == WPSNumber).Select(x => x.id.ToString()).FirstOrDefault();
                                objSWP010.RevNo = lstWPS.Where(x => x.id.ToString() == WPSNumber).Select(x => x.rev).FirstOrDefault();
                                if (!string.IsNullOrWhiteSpace(AlternateWPS1))
                                {
                                    objSWP010.AlternateWPS1 = lstWPS.Where(x => x.id.ToString() == AlternateWPS1).Select(x => x.id.ToString()).FirstOrDefault();
                                    objSWP010.AlternateWPS1RevNo = lstWPS.Where(x => x.id.ToString() == AlternateWPS1).Select(x => x.rev).FirstOrDefault();
                                }
                                else
                                {
                                    objSWP010.AlternateWPS1 = null;
                                    objSWP010.AlternateWPS1RevNo = null;
                                    objSWP010.AlternateWPS2 = null;
                                    objSWP010.AlternateWPS2RevNo = null;
                                }
                                if (!string.IsNullOrWhiteSpace(AlternateWPS2))
                                {
                                    objSWP010.AlternateWPS2 = lstWPS.Where(x => x.id.ToString() == AlternateWPS2).Select(x => x.id.ToString()).FirstOrDefault();
                                    objSWP010.AlternateWPS2RevNo = lstWPS.Where(x => x.id.ToString() == AlternateWPS2).Select(x => x.rev).FirstOrDefault();
                                }
                                else
                                {
                                    objSWP010.AlternateWPS2 = null;
                                    objSWP010.AlternateWPS2RevNo = null;
                                }
                                if (!string.IsNullOrWhiteSpace(PreHeat))
                                {
                                    objSWP010.Preheat = Convert.ToInt32(PreHeat);
                                }
                                else
                                {
                                    objSWP010.Preheat = null;
                                }
                                objSWP010.DHTISR = DHTISR;
                                if (!string.IsNullOrWhiteSpace(SWPNotes))
                                {
                                    string[] arrSWPNotes = SWPNotes.Split(',');
                                    List<string> lstnotes = new List<string>();
                                    foreach (var arr in arrSWPNotes)
                                    {
                                        if (lstSWPNotes.Contains(arr))
                                        {
                                            lstnotes.Add(arr);
                                        }
                                    }
                                    if (lstnotes.Count > 0)
                                    {
                                        objSWP010.SWPNotes = string.Join(",", lstnotes.ToList());
                                    }
                                }
                                else { objSWP010.SWPNotes = null; }
                                db.SaveChanges();
                                #endregion
                            }
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Shop Weld Plan imported successfully";
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                }
            }
            else
            {
                objResponseMsg = ErrorExportToExcel(ds);
            }
        }
        else
        {
            objResponseMsg.Key = false;
            objResponseMsg.Value = "Excel file is not valid";
        }
    }
    else
    {
        objResponseMsg.Key = false;
        objResponseMsg.Value = "Please enter valid Project no.";
    }
    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
}


public clsHelper.ResponceMsgWithFileName GenerateHeaderExcel(List<SP_SWP_Get_HeaderList_Result> lstdata)
{
    clsHelper.ResponceMsgWithFileName objResponseMsg = new clsHelper.ResponceMsgWithFileName();
    try
    {
        MemoryStream ms = new MemoryStream();
        using (FileStream fs = System.IO.File.OpenRead(Server.MapPath("~/Resources/IPI/Shop Weld Plan Excel Template.xlsx")))
        {
            using (ExcelPackage excelPackage = new ExcelPackage(fs))
            {
                ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();
                int i = 2;

                foreach (var item in lstdata)
                {
                    excelWorksheet.Cells[i, 1].Value = item.SeamNo;
                    excelWorksheet.Cells[i, 2].Value = item.selectedJointType;
                    excelWorksheet.Cells[i, 3].Value = item.MainWPSNumber;
                    excelWorksheet.Cells[i, 4].Value = item.MainWPSRevNo.ToString();
                    excelWorksheet.Cells[i, 5].Value = item.AlternateWPS1;
                    excelWorksheet.Cells[i, 6].Value = item.AlternateWPS1RevNo.ToString();
                    excelWorksheet.Cells[i, 7].Value = item.AlternateWPS2;
                    excelWorksheet.Cells[i, 8].Value = item.AlternateWPS2RevNo.ToString();
                    excelWorksheet.Cells[i, 9].Value = Convert.ToString(item.Preheat);
                    excelWorksheet.Cells[i, 10].Value = Convert.ToString(item.DHTISR);
                    excelWorksheet.Cells[i, 11].Value = item.SWPNotes;
                    excelWorksheet.Cells[i, 12].Value = Convert.ToString(item.RevNo);
                    excelWorksheet.Cells[i, 13].Value = Convert.ToString(item.Status);
                    excelWorksheet.Cells[i, 14].Value = Convert.ToString(item.Returnremarks);
                    i++;
                }
                excelPackage.SaveAs(ms);
            }
        }

        ms.Position = 0;

        string fileName;
        string excelFilePath = Helper.ExcelFilePath(objClsLoginInfo.UserName, out fileName);

        Byte[] bin = ms.ToArray();
        System.IO.File.WriteAllBytes(excelFilePath, bin);

        objResponseMsg.Key = false;
        objResponseMsg.Value = "Error";
        objResponseMsg.FileName = fileName;
        return objResponseMsg;
    }
    catch (Exception ex)
    {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
    }
}

public clsHelper.ResponceMsgWithFileName ErrorExportToExcel(DataSet ds)
{
    clsHelper.ResponceMsgWithFileName objResponseMsg = new clsHelper.ResponceMsgWithFileName();
    try
    {
        MemoryStream ms = new MemoryStream();
        using (FileStream fs = System.IO.File.OpenRead(Server.MapPath("~/Resources/IPI/Shop Weld Plan Excel Template.xlsx")))
        {
            using (ExcelPackage excelPackage = new ExcelPackage(fs))
            {
                ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();
                int i = 2;
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    //Seam no
                    if (!string.IsNullOrWhiteSpace(item.Field<string>(14)))
                    {
                        excelWorksheet.Cells[i, 1].Value = item.Field<string>(0) + " (Note: " + item.Field<string>(14).ToString() + " )";
                        excelWorksheet.Cells[i, 1].Style.Font.Color.SetColor(Color.Red);
                    }
                    else { excelWorksheet.Cells[i, 1].Value = item.Field<string>(0); }

                    //joint type
                    if (!string.IsNullOrWhiteSpace(item.Field<string>(15)))
                    {
                        excelWorksheet.Cells[i, 2].Value = item.Field<string>(1) + " (Note: " + item.Field<string>(15).ToString() + " )";
                        excelWorksheet.Cells[i, 2].Style.Font.Color.SetColor(Color.Red);
                    }
                    else { excelWorksheet.Cells[i, 2].Value = item.Field<string>(1); }

                    //wps number
                    if (!string.IsNullOrWhiteSpace(item.Field<string>(16)))
                    {
                        excelWorksheet.Cells[i, 3].Value = item.Field<string>(2) + " (Note: " + item.Field<string>(16).ToString() + " )";
                        excelWorksheet.Cells[i, 3].Style.Font.Color.SetColor(Color.Red);
                    }
                    else { excelWorksheet.Cells[i, 3].Value = item.Field<string>(2); }
                    //wps rev
                    excelWorksheet.Cells[i, 4].Value = item.Field<string>(3);

                    //Alternative WPS 1
                    if (!string.IsNullOrWhiteSpace(item.Field<string>(17)))
                    {
                        excelWorksheet.Cells[i, 5].Value = item.Field<string>(4) + " (Note: " + item.Field<string>(17).ToString() + " )";
                        excelWorksheet.Cells[i, 5].Style.Font.Color.SetColor(Color.Red);
                    }
                    else { excelWorksheet.Cells[i, 5].Value = item.Field<string>(4); }
                    //Alternative WPS 1 rev
                    excelWorksheet.Cells[i, 6].Value = item.Field<string>(5);

                    //Alternative WPS 2
                    if (!string.IsNullOrWhiteSpace(item.Field<string>(18)))
                    {
                        excelWorksheet.Cells[i, 7].Value = item.Field<string>(6) + " (Note: " + item.Field<string>(18).ToString() + " )";
                        excelWorksheet.Cells[i, 7].Style.Font.Color.SetColor(Color.Red);
                    }
                    else { excelWorksheet.Cells[i, 7].Value = item.Field<string>(6); }
                    //Alternative WPS 2 rev
                    excelWorksheet.Cells[i, 8].Value = item.Field<string>(7);

                    //Pre-Heat (Deg °C)
                    if (!string.IsNullOrWhiteSpace(item.Field<string>(19)))
                    {
                        excelWorksheet.Cells[i, 9].Value = item.Field<string>(8) + " (Note: " + item.Field<string>(19).ToString() + " )";
                        excelWorksheet.Cells[i, 9].Style.Font.Color.SetColor(Color.Red);
                    }
                    else { excelWorksheet.Cells[i, 9].Value = item.Field<string>(8); }

                    //DHT / ISR
                    if (!string.IsNullOrWhiteSpace(item.Field<string>(20)))
                    {
                        excelWorksheet.Cells[i, 10].Value = item.Field<string>(9) + " (Note: " + item.Field<string>(20).ToString() + " )";
                        excelWorksheet.Cells[i, 10].Style.Font.Color.SetColor(Color.Red);
                    }
                    else { excelWorksheet.Cells[i, 10].Value = item.Field<string>(9); }

                    //SWP Notes
                    if (!string.IsNullOrWhiteSpace(item.Field<string>(21)))
                    {
                        excelWorksheet.Cells[i, 11].Value = item.Field<string>(10) + " (Note: " + item.Field<string>(21).ToString() + " )";
                        excelWorksheet.Cells[i, 11].Style.Font.Color.SetColor(Color.Red);
                    }
                    else
                    {
                        excelWorksheet.Cells[i, 11].Value = item.Field<string>(10);
                    }
                    excelWorksheet.Cells[i, 12].Value = item.Field<string>(11);
                    excelWorksheet.Cells[i, 13].Value = item.Field<string>(12);
                    excelWorksheet.Cells[i, 14].Value = item.Field<string>(13);
                    i++;
                }
                excelPackage.SaveAs(ms);
            }
        }

        ms.Position = 0;

        string fileName;
        string excelFilePath = Helper.ExcelFilePath(objClsLoginInfo.UserName, out fileName);

        Byte[] bin = ms.ToArray();
        System.IO.File.WriteAllBytes(excelFilePath, bin);

        objResponseMsg.Key = false;
        objResponseMsg.Value = "Error";
        objResponseMsg.FileName = fileName;
        return objResponseMsg;
    }
    catch (Exception ex)
    {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
    }
}

public DataSet ToChechValidation(ExcelPackage package, SWP010 objImport, out bool isError)
{
    ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();
    DataTable dtHeaderExcel = new DataTable();
    isError = false;
    try
    {
        #region Read Lines data from excel 
        foreach (var firstRowCell in excelWorksheet.Cells[1, 1, 1, excelWorksheet.Dimension.End.Column])
        {
            dtHeaderExcel.Columns.Add(firstRowCell.Text);
        }

        for (var rowNumber = 2; rowNumber <= excelWorksheet.Dimension.End.Row; rowNumber++)
        {
            var row = excelWorksheet.Cells[rowNumber, 1, rowNumber, excelWorksheet.Dimension.End.Column];
            var newRow = dtHeaderExcel.NewRow();
            if (!string.IsNullOrWhiteSpace(excelWorksheet.Cells[rowNumber, 1].Value.ToString()) && !string.IsNullOrWhiteSpace(excelWorksheet.Cells[rowNumber, 12].Value.ToString())
                && !string.IsNullOrWhiteSpace(excelWorksheet.Cells[rowNumber, 13].Value.ToString()))
            {
                foreach (var cell in row)
                {
                    newRow[cell.Start.Column - 1] = cell.Text;
                }
            }

            dtHeaderExcel.Rows.Add(newRow);
        }
        #endregion

        #region Validation for Lines
        dtHeaderExcel.Columns.Add("SeamNoErrorMsg"); //15
        dtHeaderExcel.Columns.Add("JointTypeErrorMsg");//16
        dtHeaderExcel.Columns.Add("WPSNumberErrorMsg");//17
        dtHeaderExcel.Columns.Add("AlternateWPS1ErrorMsg");//18
        dtHeaderExcel.Columns.Add("AlternateWPS2ErrorMsg");//19
        dtHeaderExcel.Columns.Add("PreHeatErrorMsg");//20
        dtHeaderExcel.Columns.Add("DHTISRErrorMsg");//21
        dtHeaderExcel.Columns.Add("SWPNotesErrorMsg");//22

        var lstJointType = Manager.GetSubCatagories("Joint Type", objImport.BU, objImport.Location).Select(i => i.Code).ToList();
        var lstDhtIsr = clsImplementationEnum.GetDHTISR();
        var listDhtIsr = lstDhtIsr.AsEnumerable().Select(x => x.ToString()).ToList();

        foreach (DataRow item in dtHeaderExcel.Rows)
        {
            string SeamNo = item.Field<string>("SeamNo");
            string JointType = item.Field<string>("Joint Type");
            string WPSNumber = item.Field<string>("WPS Number");
            string WPSRevNo = item.Field<string>("WPS RevNo");
            string AlternateWPS1 = item.Field<string>("Alternate WPS1");
            string AlternateWPS1RevNo = item.Field<string>("Alternate WPS1 RevNo");
            string AlternateWPS2 = item.Field<string>("Alternate WPS2");
            string AlternateWPS2RevNo = item.Field<string>("Alternate WPS2 RevNo");
            string PreHeat = item.Field<string>("Pre-Heat (Deg °C)");
            string DHTISR = item.Field<string>("DHT / ISR");
            string SWPNotes = item.Field<string>("SWP Notes");
            string SWPRevNo = item.Field<string>("SWP RevNo");
            string SWPStatus = item.Field<string>("SWP Status");
            string ReturnRemarks = item.Field<string>("Return Remarks");
            string errorMessage = string.Empty;
            string approvedString = clsImplementationEnum.TestPlanStatus.Approved.GetStringValue();
            string Draft = clsImplementationEnum.TestPlanStatus.Draft.GetStringValue();
            string Returned = clsImplementationEnum.TestPlanStatus.Returned.GetStringValue();

            SWP010 objSWP010 = new SWP010();
            objSWP010 = db.SWP010.Where(x => x.SeamNo.Trim() == SeamNo.Trim()
                                            && x.QualityProject.Trim() == objImport.QualityProject.Trim() && x.Project.Trim() == objImport.Project.Trim() && x.BU.Trim() == objImport.BU.Trim() && x.Location == objImport.Location).FirstOrDefault();
            if (objSWP010 != null ? ((objSWP010.Status != clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()) && !IsSeamOffered(objSWP010.HeaderId)) : false)
            {
                var Status = clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue();
                var lstWPS = (from a in db.WPS012
                              where (a.QualityProject == objSWP010.QualityProject || (from b in db.WPS013 where b.QualityProject == objSWP010.QualityProject select b.IQualityProject).ToList().Contains(a.QualityProject))
                              && a.Status.Equals(Status)
                              && a.Jointtype.Equals(objSWP010.JointType)
                              select new
                              {
                                  a.WPSNumber,
                                  a.WPSRevNo
                              }).Distinct().ToList();
                //JointType
                if (string.IsNullOrEmpty(JointType))
                {
                    item["JointTypeErrorMsg"] = "Joint Type is required";
                    isError = true;
                }
                else
                {
                    if (!lstJointType.Contains(JointType))
                    {
                        item["JointTypeErrorMsg"] = "Joint Type is not valid";
                        isError = true;
                    }
                }
                //WPSNumber
                if (string.IsNullOrEmpty(WPSNumber))
                {
                    item["WPSNumberErrorMsg"] = "WPS Number is required";
                    isError = true;
                }
                else
                {
                    if (!lstWPS.Any(a => a.WPSNumber == WPSNumber))
                    {
                        item["WPSNumberErrorMsg"] = "WPS Number is not valid";
                        isError = true;
                    }
                }
                //AlternateWPS1
                if (!string.IsNullOrEmpty(AlternateWPS1))
                {
                    if (!lstWPS.Any(a => a.WPSNumber == AlternateWPS1))
                    {
                        item["AlternateWPS1ErrorMsg"] = "Alternate WPS Number 1 is not valid";
                        isError = true;
                    }
                    else
                    {
                        if (WPSNumber == AlternateWPS1)
                        {
                            item["AlternateWPS1ErrorMsg"] = "Alternate WPS Number 1 must be unique value.";
                            isError = true;
                        }
                    }
                }
                //AlternateWPS2
                if (!string.IsNullOrEmpty(AlternateWPS2))
                {
                    if (!lstWPS.Any(a => a.WPSNumber == AlternateWPS2))
                    {
                        item["AlternateWPS2ErrorMsg"] = "Alternate WPS Number 2 is not valid";
                        isError = true;
                    }
                    else
                    {
                        if (WPSNumber == AlternateWPS2 || AlternateWPS1 == AlternateWPS2)
                        {
                            item["AlternateWPS2ErrorMsg"] = "Alternate WPS Number 2 must be unique value.";
                            isError = true;
                        }
                    }
                }

                //preheat no
                int Preheatno;
                if (!string.IsNullOrWhiteSpace(PreHeat))
                {
                    if (!int.TryParse(PreHeat, out Preheatno))
                    {
                        item["PreheatErrorMsg"] = "'" + PreHeat + "' is Not Valid, Please Enter Only Numeric Value"; isError = true;
                    }
                    else if (PreHeat.Length > 10)
                    {
                        item["PreheatErrorMsg"] = "Preheat (Deg. °C) have maximum limit of 10 digit"; isError = true;
                    }
                }
                // else
                // { item["PreheatErrorMsg"] = "Preheat (Deg. °C) is required"; isError = true; }
                //DHT/ISR
                if (!string.IsNullOrWhiteSpace(DHTISR))
                {
                    if (!listDhtIsr.Contains(DHTISR))
                    {
                        item["DHTISRErrorMsg"] = "DHT/ISR is not valid"; isError = true;
                    }
                }
                // else
                //  { item["DHTISRErrorMsg"] = "DHT/ISR is required"; isError = true; }

                //SWP Notes
                var lstSWPNotes = Manager.GetGeneralNotes(objImport.QualityProject, "", objImport.Location).Select(i => i.NoteNumber.ToString()).ToList();
                if (!string.IsNullOrWhiteSpace(SWPNotes))
                {
                    string[] arrSWPNotes = SWPNotes.Split(',');
                    List<string> lstnotes = new List<string>();
                    foreach (var arr in arrSWPNotes)
                    {
                        if (!string.IsNullOrWhiteSpace(arr))
                        {
                            if (!lstSWPNotes.Contains(arr))
                            {
                                lstnotes.Add(arr);
                            }
                        }
                    }
                    if (lstnotes.Count > 0)
                    {
                        item["SWPNotesErrorMsg"] = string.Join(",", lstnotes.ToList()) + " is not valid"; isError = true;
                    }
                }
                else
                {
                    //   item["SWPNotesErrorMsg"] = "SWP Notes is required"; isError = true;
                }
            }
            else
            {
                if (objSWP010 == null)
                {
                    item["SeamNoErrorMsg"] = "Seam No is not valid";
                    isError = true;
                }
            }
        }
        #endregion
    }
    catch (Exception ex)
    {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
    }
    DataSet ds = new DataSet();
    ds.Tables.Add(dtHeaderExcel);
    return ds;
}
#endregion


public class SeamModel
{
    public string Code { get; set; }
    public string Name { get; set; }
}
public class ResponceMsgWithStatus1 : clsHelper.ResponseMsg
{
    public string budesc;
    public string projdesc;
    public string seam;
    public string QP;
    public string Revi;
    public string appdesc;
    public List<WPS> lsttask { get; set; }
    public List<Process> lstProcess { get; set; }
    public List<Layer> lstLayer { get; set; }
    public List<Layer> lstAWSClass { get; set; }
    public List<Layer> lstFillerMetal { get; set; }
}
public class WPS
{
    public string id { get; set; }
    public string text { get; set; }
}
public class Process
{
    public string id { get; set; }
    public string text { get; set; }
}
public class Layer
{
    public string id { get; set; }
    public string text { get; set; }
}
public class SP_SWP_Get_HeaderList_Ent : SP_SWP_Get_HeaderList_Result
{
    public SP_SWP_Get_HeaderList_Ent()
    {
        //objIsSeamOfferedCheckEnt = new IsSeamOfferedCheckEnt();
    }
    public bool IsSeamOffered { get; set; }
    public bool MainWPSDisabled { get; set; }
    public bool AlternativeWPS1Disabled { get; set; }
    public bool AlternativeWPS2Disabled { get; set; }
    public bool PWHTAndNotesDisabled { get; set; }
    public bool JointTypeDisabled { get; set; }
    public bool IsAllPreHeatStageOffered { get; set; }
    public bool WPSNo1TokenGenerate { get; set; }
    public bool WPSNo2TokenGenerate { get; set; }
    public bool WPSNo3TokenGenerate { get; set; }
    public bool WeldVisualStageOffered { get; set; }
}

public class IsSeamOfferedCheckEnt
{
    public bool IsAllPreHeatStageOffered { get; set; }
    public bool WeldVisualStageOffered { get; set; }
    public bool WPSNo1TokenGenerate { get; set; }
    public bool WPSNo2TokenGenerate { get; set; }
    public bool WPSNo3TokenGenerate { get; set; }
}
    }
}