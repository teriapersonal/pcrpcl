﻿using IEMQS.Areas.IPI.Models;
using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
namespace IEMQS.Areas.IPI.Controllers
{
    public class DisplayProtocolController : clsBase
    {
        // GET: IPI/DisplayProtocol
        [SessionExpireFilter]
        public ActionResult Index()
        {
            ViewBag.Location = objClsLoginInfo.Location;
            return View();
        }
        public ActionResult GetSeamListOfferDataPartial(string qualityProject, string seamNumber)
        {
            ViewBag.QualityProject = qualityProject;
            ViewBag.SeamNumber = seamNumber;
            return PartialView("_LoadProtocolData");
        }
        [HttpPost]
        public ActionResult GetFolderPath(int lineid = 0)
        {
            string folderpath = "QMS031/" + lineid;
            QMS031 qms31 = db.QMS031.Where(x => x.LineId == lineid).FirstOrDefault();
            if (qms31 != null)
            {
                QMS050 objQMS050 = db.QMS050.Where(x => x.QualityProject == qms31.QualityProject && x.Location == qms31.Location && x.BU == qms31.BU && x.SeamNo == qms31.SeamNo && x.StageCode == qms31.StageCode && x.StageSequence == qms31.StageSequance).OrderByDescending(x => x.RequestId).FirstOrDefault();
                if (objQMS050 != null)
                {
                    folderpath = "QMS050/" + objQMS050.HeaderId + "/" + objQMS050.IterationNo + "/" + objQMS050.RequestNoSequence;
                }
            }
            return Json(folderpath, JsonRequestBehavior.AllowGet);
        }

        public ActionResult loadStagewiseProtocolDataTable(JQueryDataTableParamModel param)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                string qualityProject = param.QualityProject;
                string seamNumber = param.SeamNo;
                string whereCondition = "1=1 and qms31.ProtocolType is not null ";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms31.BU", "qms31.Location");
                string[] columnName = { "qms31.SeamNo", "StageSequance", "dbo.GET_IPI_STAGECODE_DESCRIPTION(qms31.StageCode, qms31.BU, qms31.Location) ", "ProtocolType" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                if (!string.IsNullOrEmpty(qualityProject) && !string.IsNullOrEmpty(seamNumber))
                {
                    whereCondition += (seamNumber.Equals("ALL") ? " AND qms31.QualityProject = '" + qualityProject + "' " : " AND qms31.QualityProject = '" + qualityProject + "' AND qms31.SeamNo ='" + seamNumber + "'");
                }

                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = string.Empty; ;
                sortDirection = Convert.ToString(Request["sSortDir_0"]); // asc or desc
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstHeader = db.SP_IPI_DISPLAY_PROTOCOL_LIST_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                var res = from h in lstHeader
                          select new[] {
                               Convert.ToString(h.SeamNo),
                               Convert.ToString(h.StageCode),
                               Convert.ToString(h.StageSequance),
                               Convert.ToString(h.ProtocolDescription),
                               Convert.ToString(h.ProtocolType),
                               Convert.ToString(h.ProtocolId),
                               Convert.ToString(h.ProtocolLinkedBy),
                               Convert.ToString(h.ProtocolLinkedOn),
                            (!string.IsNullOrWhiteSpace(h.ProtocolType) ? (h.ProtocolType == clsImplementationEnum.ProtocolType.Other_Files.GetStringValue() ? ("&nbsp;&nbsp;<a target=\"_blank\" onclick=\"ShowOtherFiles("+ h.LineId +")\" Title=\"View Protocol Details\" ><i style=\"margin - left:5px;\" class=\"fa fa-eye\"></i></a>") : ("&nbsp;&nbsp;<a target=\"_blank\"  href=\"" + WebsiteURL + "/PROTOCOL/"+h.ProtocolType+"/Linkage/"+h.ProtocolId + "" +"\" Title=\"View Protocol Details\" ><i style=\"margin - left:5px;\" class=\"fa fa-eye\"></i></a>")) : "<a Title=\"No Protocol Attached\" ><i style=\"opacity:0.3;\" class=\"fa fa-file-text\"></i></a>")

                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #region Next prev 
        [HttpPost]
        public ActionResult nextprevbyProjectorseam(string seamNo, string qualityProject, string option)
        {
            ResponsenextorPre objresponse = new ResponsenextorPre();
            List<Projects> lstprojects = listqualityprojects();
            List<seamnolist> lstSeam = null;
            string forall = "ALL";
            int projindexno = 0;
            int seamindexno = 0;
            int minseam = 0;
            int maxseam = 0;
            int minproj = 0;
            int maxproj = 0;

            try
            {
                if (!string.IsNullOrEmpty(qualityProject) && !string.IsNullOrEmpty(seamNo))
                {
                    projindexno = lstprojects.Select((item, index) => new { index, item }).Where(x => x.item.projectCode.ToLower() == qualityProject.ToLower()).Select(x => x.index).FirstOrDefault();
                    minproj = lstprojects.Select((item, index) => new { index, item }).Min(x => x.index);
                    maxproj = lstprojects.Select((item, index) => new { index, item }).Max(x => x.index);

                    if (!string.IsNullOrEmpty(option))
                    {
                        switch (option)
                        {
                            case "PrevProject":
                                ViewBag.Title = "PrevProject";
                                projindexno = (projindexno - 1);
                                objresponse.PreviousProjectButton = (projindexno == minproj) ? true : ((lstprojects.Select((item, index) => new { index, item }).Any(x => x.index == projindexno)) ? false : true);
                                objresponse.NextProjectButton = (lstprojects.Select((item, index) => new { index, item }).Any(x => x.index == (projindexno + 1))) ? false : true;
                                qualityProject = mainqualityProject(projindexno);
                                break;
                            case "NextProject":
                                ViewBag.Title = "NextProject";
                                projindexno = (projindexno + 1);
                                objresponse.PreviousProjectButton = (lstprojects.Select((item, index) => new { index, item }).Any(x => x.index == (projindexno - 1))) ? false : true;
                                objresponse.NextProjectButton = (projindexno == maxproj) ? true : ((lstprojects.Select((item, index) => new { index, item }).Any(x => x.index == projindexno)) ? false : true);
                                qualityProject = mainqualityProject(projindexno);
                                break;
                            default:
                                ViewBag.Title = "NextSeam";
                                objresponse.PreviousProjectButton = (lstprojects.Select((item, index) => new { index, item }).Any(x => x.index == (projindexno - 1))) ? false : true;
                                objresponse.NextProjectButton = (lstprojects.Select((item, index) => new { index, item }).Any(x => x.index == (projindexno + 1))) ? false : true;

                                break;
                        }
                    }
                    else
                    {
                        objresponse.PreviousProjectButton = (lstprojects.Select((item, index) => new { index, item }).Any(x => x.index == (projindexno - 1))) ? false : true;
                        objresponse.NextProjectButton = (lstprojects.Select((item, index) => new { index, item }).Any(x => x.index == (projindexno + 1))) ? false : true;
                    }

                    lstSeam = GetProjectwisePart(qualityProject);

                    if (lstSeam.Select((item, index) => new { index, item }).Any(x => x.item.SeamNo.ToLower() == seamNo.ToLower()))
                    {
                        seamindexno = lstSeam.Select((item, index) => new { index, item }).Where(x => x.item.SeamNo.ToLower() == seamNo.ToLower()).Select(x => x.index).FirstOrDefault();
                        minseam = lstSeam.Select((item, index) => new { index, item }).Min(x => x.index);
                        maxseam = lstSeam.Select((item, index) => new { index, item }).Max(x => x.index);
                    }
                    else
                        seamNo = forall;

                    if (seamNo.ToLower() == forall.ToLower())
                    {
                        objresponse.PreviousSeamNoButton = true;
                        objresponse.NextSeamNoButton = true;
                    }
                    else
                    {
                        objresponse.PreviousSeamNoButton = (lstSeam.Select((item, Index) => new { Index, item }).Any(x => x.Index == (seamindexno - 1))) ? false : true;
                        objresponse.NextSeamNoButton = (lstSeam.Select((item, Index) => new { Index, item }).Any(x => x.Index == (seamindexno + 1))) ? false : true;
                    }


                    switch (option)
                    {
                        case "PrevSeam":
                            ViewBag.Title = "PrevSeam";
                            seamindexno = (seamindexno - 1);
                            objresponse.PreviousSeamNoButton = (seamindexno == minseam) ? true : ((lstSeam.Select((item, index) => new { index, item }).Any(x => x.index == seamindexno)) ? false : true);
                            objresponse.NextSeamNoButton = (lstSeam.Select((item, index) => new { index, item }).Any(x => x.index == (seamindexno + 1))) ? false : true;
                            seamNo = lstSeam.Select((item, index) => new { index, item }).Where(x => x.index == seamindexno).FirstOrDefault().item.SeamNo;
                            break;
                        case "NextSeam":
                            ViewBag.Title = "NextSeam";
                            seamindexno = (seamindexno + 1);
                            objresponse.PreviousSeamNoButton = (lstSeam.Select((item, index) => new { index, item }).Any(x => x.index == (seamindexno - 1))) ? false : true;
                            objresponse.NextSeamNoButton = (seamindexno == maxseam) ? true : ((lstSeam.Select((item, index) => new { index, item }).Any(x => x.index == seamindexno)) ? false : true);
                            seamNo = lstSeam.Select((item, index) => new { index, item }).Where(x => x.index == seamindexno).FirstOrDefault().item.SeamNo;
                            break;
                        default:
                            ViewBag.Title = "NextPart";
                            break;
                    }

                    objresponse.Project = qualityProject;
                    objresponse.Value = seamNo;
                    objresponse.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objresponse.Key = false;
                objresponse.Value = "Error Occures Please try again";
            }
            return Json(objresponse, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetSeamNoByQualityProject(string term, string qualityProject)
        {
            try
            {
                var data = db.QMS030.Where(x => x.QualityProject.Equals(qualityProject) && x.SeamNo.Contains(term)).Select(x => x.SeamNo).Distinct().ToList();
                return Json(new { Data = data });
            }
            catch (Exception ex)
            {
                return Json(new { Error = ex.Message });
            }
        }
        public List<Projects> listqualityprojects()
        {
            string username = objClsLoginInfo.UserName;
            var lstATHLocation = db.ATH001.Where(x => x.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(x => x.Location).Distinct().ToList();
            var lstCOMLocation = db.COM003.Where(i => i.t_psno.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_loca).Distinct().ToList();

            var lstCOMBU = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.BU).Distinct().ToList();

            lstCOMLocation = lstATHLocation.Union(lstCOMLocation).ToList();

            List<Projects> lstQualityProject = new List<Projects>();

            lstQualityProject = db.QMS010.Where(i => lstCOMLocation.Contains(i.Location)
                                                    && lstCOMBU.Contains(i.BU)
                                                    ).Select(i => new Projects { Value = i.QualityProject, projectCode = i.QualityProject }).Distinct().ToList();
            return lstQualityProject;
        }
        public List<seamnolist> GetProjectwisePart(string qualityProject)
        {
            List<seamnolist> lstseam = new List<seamnolist>();
            lstseam = db.QMS030.Where(x => x.QualityProject.Equals(qualityProject)).Select(x => new seamnolist { SeamNo = x.SeamNo }).Distinct().ToList();
            return lstseam;
        }

        public string mainqualityProject(int indexno = 0)
        {
            string project = string.Empty;
            List<Projects> lstprojects = listqualityprojects();
            project = lstprojects.Select((item, index) => new { index, item }).Where(x => x.index == indexno).FirstOrDefault().item.projectCode;
            return project;
        }

        public class seamnolist
        {
            public string SeamNo { get; set; }
        }
        public class ResponsenextorPre : clsHelper.ResponseMsg
        {
            public string Project { get; set; }
            public string Part { get; set; }
            public bool NextProjectButton { get; set; }
            public bool PreviousProjectButton { get; set; }
            public bool NextSeamNoButton { get; set; }
            public bool PreviousSeamNoButton { get; set; }
        }

        #endregion
    }
}