﻿using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.IPI.Controllers
{
    public class ApproveICLPartController : clsBase
    {

        // GET: IPI/ApproveICLPart
        [SessionExpireFilter]
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult LoadHeaderPartial(string status, string qualityProject)
        {
            ViewBag.Status = status;
            ViewBag.qualityProject = qualityProject;
            return PartialView("_LoadHeaderGridPartial");
        }

        [HttpPost]
        public JsonResult LoadICLPartHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = String.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    whereCondition += " 1=1 and QualityProject IS NOT NULL and status in('" + clsImplementationEnum.ICLPartStatus.SendForApproval.GetStringValue() + "')";
                }
                else
                {
                    whereCondition += " 1=1 and QualityProject IS NOT NULL ";
                }
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms35.BU", "qms35.Location");
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += " and (qms35.Location like '%" + param.sSearch + "%' or bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or qms35.QualityProject like '%" + param.sSearch + "%'or (qms35.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%')";
                }

                //  var lstHeader = db.SP_IPI_GetQualityIDHeader(startIndex, endIndex, "", whereCondition).ToList();
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstHeader = db.SP_IPI_ICL_PART_HEADER_INDEX_DATA(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstHeader
                          select new[] {
                        Convert.ToString(a.ROW_NO),
                        a.QualityProject,
                        a.Project,
                        a.BU,
                        a.Location,
                        Convert.ToString(a.HeaderId)
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult LoadICLPartLineGridData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "";
                strWhere += Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();
                string[] columnName = { "StageCode", "StageSequence", "AcceptanceStandard", "ApplicableSpecification", "InspectionExtent", "StageStatus", "Remarks", "LineId", };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_IPI_ICL_PART_LINE_DETAILS
                              (
                             StartIndex,
                             EndIndex,
                             strSortOrder,
                             strWhere
                              ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                //Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.StageCode),
                                Convert.ToString(uc.StageSequence),
                                Convert.ToString(uc.AcceptanceStandard),
                                Convert.ToString(uc.ApplicableSpecification),
                                !string.IsNullOrWhiteSpace(uc.InspectionExtent) ? uc.InspectionExtent.Split('-')[1].Trim() : "",
                                Convert.ToString(uc.Parallel),
                                 Convert.ToString(uc.StageStatus),
                                Convert.ToString(uc.Remarks),
                                Convert.ToString(uc.ProtocolTypeDescription),
                                "<a class='btn btn-xs' href='javascript:void(0)' onclick=ShowTimeline('/IPI/ApproveICLPart/ShowTimeline?HeaderID="+Convert.ToInt32(uc.HeaderId)+"&LineId="+Convert.ToInt32(uc.LineId)+"');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a>"
                                + Helper.HTMLActionString(uc.LineId,"ViewProtocol","View Protocol","fa fa-file-text", uc.ProtocolType == clsImplementationEnum.ProtocolType.Other_Files.GetStringValue() ? "OpenAttachmentPopUp("+uc.LineId+", false,'QMS036/" + uc.LineId+"')" : "ShowProtocol('" + WebsiteURL + "/PROTOCOL/" + uc.ProtocolType + "/Linkage/" + Convert.ToInt32(uc.ProtocolId) + "')","", uc.ProtocolType == clsImplementationEnum.ProtocolType.Other_Files.GetStringValue() ? false : uc.ProtocolId == null),
                                Convert.ToString(uc.LineId),
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        //[HttpPost]
        //public JsonResult LoadICLPartHeaderData(JQueryDataTableParamModel param)
        //{
        //    try
        //    {
        //        var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
        //        var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
        //        int StartIndex = param.iDisplayStart + 1;
        //        int EndIndex = param.iDisplayStart + param.iDisplayLength;

        //        string strWhere = string.Empty;
        //        if (param.CTQCompileStatus.ToUpper() == "PENDING")
        //        {
        //            strWhere += " 1=1 and status in('" + clsImplementationEnum.ICLPartStatus.SendForApproval.GetStringValue() + "')";
        //        }
        //        else
        //        {
        //            strWhere += " 1=1";
        //        }
        //        strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.Location, "BU", "Location");
        //        var lstResult = db.SP_IPI_ICL_PART_HEADER_DETAILS
        //                       (
        //                       StartIndex, EndIndex, "", strWhere
        //                       ).ToList();

        //        var data = (from uc in lstResult
        //                    select new[]
        //                   {
        //                          Convert.ToString(uc.HeaderId),
        //                    Convert.ToString(uc.QualityProject),
        //                     Convert.ToString(uc.BU),
        //                    Convert.ToString(uc.PartNo),
        //                    Convert.ToString(uc.CreatedBy),
        //                     Convert.ToString(uc.Location),
        //                      Convert.ToString(uc.Status),
        //                    "<center><a class='btn btn-xs green' href='/IPI/ApproveICLPart/ViewDetails?HeaderID="+uc.HeaderId+"'>View<i style='margin-left:5px;' class='fa fa-eye'></i></a></center>",
        //                    }).ToList();

        //        return Json(new
        //        {
        //            sEcho = Convert.ToInt32(param.sEcho),
        //            iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
        //            iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
        //            aaData = data
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        return Json(new
        //        {
        //            sEcho = param.sEcho,
        //            iTotalRecords = "0",
        //            iTotalDisplayRecords = "0",
        //            aaData = ""
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //}

        [SessionExpireFilter]
        [AllowAnonymous]
        //[UserPermissions]
        public ActionResult ViewDetails(string headerID)
        {
            int HeaderId = 0;
            if (!string.IsNullOrWhiteSpace(headerID))
            {
                HeaderId = Convert.ToInt32(headerID);
            }
            QMS035 objQMS035 = db.QMS035.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            if (objQMS035 != null)
            {
                objQMS035.BU = db.COM002.Where(a => a.t_dimx == objQMS035.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objQMS035.Project = db.COM001.Where(i => i.t_cprj == objQMS035.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objQMS035.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objQMS035.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            }
            return View(objQMS035);
        }

        //[HttpPost]
        //public JsonResult LoadICLPartLineGridData(JQueryDataTableParamModel param)
        //{
        //    try
        //    {
        //        var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
        //        var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
        //        int StartIndex = param.iDisplayStart + 1;
        //        int EndIndex = param.iDisplayStart + param.iDisplayLength;
        //        string strWhere = "";
        //        strWhere += Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();
        //        string[] columnName = { "StageCode", "StageSequance", "AcceptanceStandard", "ApplicableSpecification", "InspectionExtent", "RevNo", "StageStatus", "Remarks", "LineId", };
        //        strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
        //        var lstResult = db.SP_IPI_ICL_PART_LINE_DETAILS
        //                      (
        //                     StartIndex,
        //                     EndIndex,
        //                     "",
        //                     strWhere
        //                      ).ToList();

        //        var data = (from uc in lstResult
        //                    select new[]
        //                   {
        //                        Convert.ToString(uc.ROW_NO),
        //                        //Convert.ToString(uc.HeaderId),
        //                        Convert.ToString(uc.StageCode),
        //                        Convert.ToString(uc.StageSequence),
        //                        Convert.ToString(uc.AcceptanceStandard),
        //                        Convert.ToString(uc.ApplicableSpecification),
        //                        Convert.ToString(uc.InspectionExtent),
        //                        Convert.ToString(uc.Parallel),
        //                         Convert.ToString(uc.StageStatus),
        //                        Convert.ToString(uc.Remarks),
        //                        Convert.ToString(uc.LineId),
        //                   }).ToList();

        //        return Json(new
        //        {
        //            sEcho = Convert.ToInt32(param.sEcho),
        //            iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
        //            iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
        //            aaData = data
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        return Json(new
        //        {
        //            sEcho = param.sEcho,
        //            iTotalRecords = "0",
        //            iTotalDisplayRecords = "0",
        //            aaData = ""
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //}
        [HttpPost]
        public ActionResult LoadTPIData(int headerId)
        {
            QMS035 objQMS035 = new QMS035();
            QMS025 objQMS025 = new QMS025();
            if (headerId > 0)
            {
                objQMS035 = db.QMS035.Where(x => x.HeaderId == headerId).FirstOrDefault();

                if (objQMS035 != null)
                {
                    ViewBag.Stages = db.QMS002.Where(i => i.BU.Equals(objQMS035.BU, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(objQMS035.Location, StringComparison.OrdinalIgnoreCase)).OrderBy(x => x.StageCode).Select(i => new CategoryData { Value = i.StageCode, Code = i.StageCode, CategoryDescription = i.StageCode + "-" + i.StageDesc }).ToList();
                    var lstTPIAgency = Manager.GetSubCatagories("TPI Agency", objQMS035.BU, objQMS035.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
                    var lstTPIIntervention = Manager.GetSubCatagories("TPI Intervention", objQMS035.BU, objQMS035.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
                    ViewBag.TPIAgency = lstTPIAgency;
                    ViewBag.TPIIntervention = lstTPIIntervention;
                    objQMS025.BU = db.COM002.Where(a => a.t_dimx == objQMS035.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                    objQMS025.Project = db.COM001.Where(i => i.t_cprj == objQMS035.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                    objQMS025.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objQMS035.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                    objQMS025.QualityProject = objQMS035.QualityProject;
                    ViewBag.Action = "singleapprove";
                }
            }

            return PartialView("_LoadTPIDataHtml", objQMS025);
        }

        [HttpPost]
        public JsonResult LoadTPHeaderDataold(JQueryDataTableParamModel param, string qProj)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += " 1=1 and status in('" + clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue() + "')";
                }
                else
                {
                    strWhere += " 1=1";
                }
                if (!string.IsNullOrEmpty(qProj))
                {
                    strWhere += " AND UPPER(QualityProject) = '" + qProj.ToUpper() + "'";
                }
                string[] columnName = { "QualityProject", "BU", "PartNo", "Location", "CreatedBy", "Status" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.Location, "", "Location");
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_IPI_PART_ICL_HEADER_DETAILS
                               (
                               StartIndex, EndIndex, "", strWhere
                               ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.Project),
                                Convert.ToString(uc.QualityProject),
                                Convert.ToString(uc.QualityProject),
                                Convert.ToString(uc.PartNo),
                                Convert.ToString(uc.ChildPartNo),
                                Convert.ToString(uc.Quantity),
                                Convert.ToString(uc.ParentPartNo),
                                Convert.ToString(uc.QualityId),
                                Convert.ToString(uc.QualityId),
                                Convert.ToString(uc.QualityIdRevNo),
                                Convert.ToString(uc.RevNo),
                                Convert.ToString(uc.Status)
                            //Convert.ToString(uc.HeaderId),
                            //Helper.GenerateHidden(uc.HeaderId, "HeaderId", Convert.ToString(uc.HeaderId)),
                            //Convert.ToString(uc.QualityProject),
                            // Convert.ToString(uc.BU),
                            //Convert.ToString(uc.PartNo),
                            //Convert.ToString(uc.CreatedBy),
                            // Convert.ToString(uc.Location),
                            //  Convert.ToString(uc.Status),
                            //"<a class='btn btn-xs' href='javascript:void(0)' onclick=ShowTimeline('/IPI/ApproveICLPart/ShowTimeline?HeaderID="+Convert.ToInt32(uc.HeaderId)+"');><i style = 'margin-left:5px;' class='fa fa-clock-o'></i></a>",
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ApproveByApproverSelected(string strHeaderIds)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = ApprovePartICLCommon(strHeaderIds);
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApproveAllPartICL(string QualityProject, string WhereCondition)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string strSendForApproval = clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue();
                //string whereCondition = "1=1 AND Status IN ('" + clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue() + "') AND UPPER(QualityProject) = '" + QualityProject.ToUpper() + "' ";
                //whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.Location, "", "Location");

                string strHeaderIds = string.Empty;
                if (!string.IsNullOrWhiteSpace(WhereCondition))
                {
                    var objList = db.SP_IPI_PART_ICL_HEADER_DETAILS(0, 0, "", WhereCondition).ToList();

                    //double checking here to fetch only pending records
                    var objPendingList = objList.Where(x => x.QualityProject.Equals(QualityProject, StringComparison.OrdinalIgnoreCase) && x.Status.Equals(strSendForApproval, StringComparison.OrdinalIgnoreCase)).ToList();
                    if (objPendingList != null && objPendingList.Count > 0)
                    {
                        strHeaderIds = string.Join(",", objPendingList.Select(x => x.HeaderId).ToList());
                        objResponseMsg = ApprovePartICLCommon(strHeaderIds);
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No record found for approval";
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Something went Wrong..!";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ICLSeamMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponseMsgWithStatus ApprovePartICLCommon(string strHeaderIds)
        {
            int isattempted = 0;
            int isnotattempted = 0;
            clsHelper.ResponseMsgWithStatus objResponseMsg1 = new clsHelper.ResponseMsgWithStatus();
            QMS036 objQMS036 = new QMS036();
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
            int[] headerID = strHeaderIds.Split(',').Select(int.Parse).ToArray();
            try
            {
                string strStageName = string.Empty;
                bool key2 = true;
                bool key3 = true;
                List<string> lstTPIStagesSkipped = new List<string>();
                List<string> lstTPIStagesforSeamSkipped = new List<string>();
                string deleted = clsImplementationEnum.TestPlanStatus.Deleted.GetStringValue();
                for (int i = 0; i < arrayHeaderIds.Length; i++)
                {
                    int strHeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                    objQMS036 = db.QMS036.Where(x => x.HeaderId == strHeaderId).FirstOrDefault();
                    if (objQMS036.QMS035.Status == clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue())
                    {
                        //var lstQMS036 = (from dbo in db.QMS036
                        //                 where dbo.HeaderId == strHeaderId && dbo.StageStatus != deleted
                        //                 select dbo.StageCode.Trim()).Distinct().ToArray();
                        bool isStageExists = true;
                        //UpdateTPIStagesInICL(strHeaderId);

                        //var lstQMS025 = (from dbo in db.QMS025
                        //                 where dbo.Location == objQMS036.Location && dbo.Project == objQMS036.Project && dbo.QualityProject == objQMS036.QualityProject && dbo.BU == objQMS036.BU && dbo.Project == objQMS036.Project
                        //                 select dbo.Stage.Trim()).ToList();
                        //string strStageName = string.Empty;
                        //QMS010 objQMS010 = db.QMS010.Where(x => x.QualityProject == objQMS036.QualityProject && x.Location == objQMS036.Location && x.BU == objQMS036.BU).FirstOrDefault();
                        //if (objQMS010.TPIOnlineApproval)
                        //{
                        //    foreach (var lst in lstQMS036)
                        //    {
                        //        if (!lstQMS025.Contains(lst))
                        //        {
                        //            strStageName += lst + ",";
                        //            isStageExists = false;
                        //        }
                        //    }
                        //}
                        //else
                        //{
                        //List<string> stageType = new List<string>() { "PT", "MT", "RT", "UT" };

                        //foreach (var lst in lstQMS036)
                        //{
                        //    var sType = db.QMS002.Where(x => x.Location == objQMS036.Location && x.BU == objQMS036.BU && x.StageCode == lst).Select(x => x.StageType).FirstOrDefault();
                        //    if (stageType.Contains(sType))
                        //    {
                        //        if (!lstQMS025.Contains(lst))
                        //        {
                        //            lstTPIStagesSkipped.Add(lst);
                        //            lstTPIStagesforSeamSkipped.Add(objQMS036.PartNo);
                        //            strStageName += lst + ",";
                        //            isStageExists = false;
                        //        }
                        //    }
                        //}
                        //}
                        if (isStageExists)
                        {
                            var objQMS035 = db.QMS035.Where(x => x.HeaderId == strHeaderId).FirstOrDefault();
                            var stageStatus = clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue();
                            int maxOfferSeq = (new clsManager()).GetMaxOfferedSequenceNo_PART(objQMS035.QualityProject, objQMS035.BU, objQMS035.Location, objQMS035.PartNo, objQMS035.ParentPartNo);
                            var lstTPStages = db.QMS036.Where(x => x.HeaderId == objQMS035.HeaderId && x.StageSequence > maxOfferSeq).ToList();
                            if (lstTPStages.Any())
                            {
                                if (Manager.IsLTFPSProject(objQMS035.Project, objQMS035.BU, objQMS035.Location))
                                {
                                    objResponseMsg1.Key = true;
                                }
                                else
                                {
                                    objResponseMsg1 = InsUpdForReadyToOffer(objQMS035.HeaderId, lstTPStages);
                                }
                            }
                            if (objResponseMsg1.Key == true)
                            {
                                db.SP_IPI_ICL_PART_APPROVE(strHeaderId, "", objClsLoginInfo.UserName);
                                //objResponseMsg.Key = true;
                                //objResponseMsg.Value = "Document approved successfully";
                                headerID = headerID.Where(val => val != strHeaderId).ToArray();
                                isattempted++;
                                #region Send Notification
                                (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), objQMS035.Project, objQMS035.BU, objQMS035.Location, "ICL for Part: " + objQMS035.PartNo + " of Project: " + objQMS035.QualityProject + "  has been Approved", clsImplementationEnum.NotificationType.Information.GetStringValue());
                                #endregion

                            }
                            else
                            {
                                //objResponseMsg.Key = false;
                                //objResponseMsg.Value = clsImplementationMessage.ICLSeamMessages.Error.ToString();
                                key2 = false;
                            }
                        }
                        else
                        {
                            //objResponseMsg.Key = false;
                            //objResponseMsg.Value = "Add Stages " + strStageName;
                            //objResponseMsg.tpi = "AddTPI";
                            key3 = false;
                        }
                    }
                    else
                    {
                        isnotattempted++;
                    }
                }
                if (!key2)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.ICLSeamMessages.Error.ToString();
                }
                else if (!key3)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Add Stages " + strStageName;
                    objResponseMsg.tpi = "AddTPI";
                    if (lstTPIStagesSkipped.Any())
                    {
                        objResponseMsg.TPISkipped = string.Format("Part {0} have been skipped as TPI Agency &  Intervention not maintained for stage(s) : {1}", string.Join(",", lstTPIStagesforSeamSkipped.Distinct()), string.Join(", ", lstTPIStagesSkipped.Distinct()));
                    }
                    objResponseMsg.headerid = string.Join(",", headerID.Select(x => x.ToString()).ToArray());
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = string.Format("{0} Part ICL(s) Approved successfully", isattempted);

                    if (isnotattempted > 0)
                    {
                        objResponseMsg.Remarks = string.Format("Part ICL(s) {0} have been skipped as Part ICL(s) {0} has been already attended", isnotattempted);
                    }
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ICLSeamMessages.Error.ToString();
            }
            return objResponseMsg;
        }

        public class ResponceMsgWithHeaderID : clsHelper.ResponseMsg
        {
            public int HeaderId;
        }
        public clsHelper.ResponseMsg UpdateTPIStagesInICL(int strHeaderId)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                QMS036 objQMS036 = new QMS036();
                var lstQMS036 = (from dbo in db.QMS036
                                 where dbo.HeaderId == strHeaderId
                                 select dbo.StageCode.Trim()).Distinct().ToArray();
                //string strStage = lstQMS036[i];
                for (int j = 0; j < lstQMS036.Length; j++)
                {
                    string strStage = lstQMS036[j];
                    //QMS036 newobjQMS036 = db.QMS036.Where(x => x.StageCode == strStage && x.HeaderId == strHeaderId).FirstOrDefault();
                    var obj036 = db.QMS036.Where(x => x.StageCode == strStage && x.HeaderId == strHeaderId).ToList();
                    ////QMS025 objQMS025 = db.QMS025.Where(a => a.Location == objQMS036.Location && a.BU == objQMS036.BU && a.Project == objQMS036.Project && a.QualityProject == objQMS036.QualityProject && strStage.Contains(a.Stage)).FirstOrDefault();
                    foreach (var newobjQMS036 in obj036)
                    {
                        QMS025 objQMS025 = db.QMS025.Where(a => a.Location == newobjQMS036.Location && a.BU == newobjQMS036.BU && a.Project == newobjQMS036.Project && a.QualityProject == newobjQMS036.QualityProject && strStage.Contains(a.Stage)).FirstOrDefault();

                        if (objQMS025 != null)
                        {
                            newobjQMS036.FirstTPIAgency = objQMS025.FirstTPIAgency;
                            newobjQMS036.FirstTPIIntervention = objQMS025.FirstTPIIntervention;
                            newobjQMS036.SecondTPIAgency = objQMS025.SecondTPIAgency;
                            newobjQMS036.SecondTPIIntervention = objQMS025.SecondTPIIntervention;
                            newobjQMS036.ThirdTPIAgency = objQMS025.ThirdTPIAgency;
                            newobjQMS036.ThirdTPIIntervention = objQMS025.ThirdTPIIntervention;
                            newobjQMS036.FourthTPIAgency = objQMS025.ForthTPIAgency;
                            newobjQMS036.FourthTPIIntervention = objQMS025.ForthTPIIntervention;
                            newobjQMS036.FifthTPIAgency = objQMS025.FifthTPIAgency;
                            newobjQMS036.FifthTPIIntervention = objQMS025.FifthTPIIntervention;
                            newobjQMS036.SixthTPIAgency = objQMS025.SixthTPIAgency;
                            newobjQMS036.SixthTPIIntervention = objQMS025.SixthTPIIntervention;
                            newobjQMS036.EditedBy = objClsLoginInfo.UserName;
                            newobjQMS036.EditedOn = DateTime.Now;
                            db.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return objResponseMsg;
        }


        public clsHelper.ResponseMsgWithStatus InsUpdForReadyToOffer(int headerid, List<QMS036> lstQMS36)
        {
            string currentUser = objClsLoginInfo.UserName;
            var currentLoc = objClsLoginInfo.Location;
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            QMS045 objQMS045 = new QMS045();
            var objQMS035 = db.QMS035.Where(x => x.HeaderId == headerid).FirstOrDefault();
            if (objQMS035 != null)
            {
                string RTOStatus = clsImplementationEnum.SeamListInspectionStatus.ReadyToOffer.GetStringValue();
                int maxOfferSeq = (new clsManager()).GetMaxOfferedSequenceNo_PART(objQMS035.QualityProject, objQMS035.BU, objQMS035.Location, objQMS035.PartNo, objQMS035.ParentPartNo);
                List<QMS045> lstOldQMS045 = db.QMS045.Where(i => i.QualityProject == objQMS035.QualityProject && i.BU == objQMS035.BU && i.PartNo == objQMS035.PartNo && i.ChildPartNo == objQMS035.ChildPartNo && i.ParentPartNo == objQMS035.ParentPartNo && i.Project == objQMS035.Project && i.Location == objQMS035.Location && (i.InspectionStatus == null || i.InspectionStatus == RTOStatus) && i.StageSequence > maxOfferSeq).ToList();
                if (lstOldQMS045.Count > 0)
                {
                    db.QMS045.RemoveRange(lstOldQMS045);
                    db.SaveChanges();
                }

                foreach (var item in lstQMS36)
                {
                    objQMS045 = db.QMS045.Where(i => i.QualityProject.Equals(item.QualityProject, StringComparison.OrdinalIgnoreCase) && i.StageCode.Equals(item.StageCode, StringComparison.OrdinalIgnoreCase) && i.StageSequence == item.StageSequence && i.BU == item.BU && i.PartNo == item.PartNo && i.ParentPartNo == item.ParentPartNo && i.Project == item.Project && i.Location == item.Location).FirstOrDefault();
                    if (objQMS045 != null)
                    {
                        #region Update Part Offer
                        if (item.StageStatus.Equals(clsImplementationEnum.ICLPartStatus.Deleted.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                        {
                            db.QMS045.Remove(objQMS045);
                        }
                        else
                        {
                            objQMS045.ProductionDrawingNo = item.QMS035.ProductionDrawingNo;
                            objQMS045.ParentPartNo = item.QMS035.ParentPartNo;
                            objQMS045.ChildPartNo = item.QMS035.ChildPartNo;
                            objQMS045.Quantity = item.QMS035.Quantity;
                            objQMS045.UOM = item.QMS035.UOM;
                            objQMS045.Remarks = item.Remarks;
                            objQMS045.ProtocolId = item.ProtocolId;
                            objQMS045.ProtocolType = item.ProtocolType;
                            objQMS045.EditedBy = objClsLoginInfo.UserName; ;
                            objQMS045.EditedOn = DateTime.Now;
                        }
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        #endregion

                    }
                    else
                    {
                        if (!item.StageStatus.Equals(clsImplementationEnum.ICLSeamStatus.Deleted.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                        {
                            objQMS045 = new QMS045();
                            objQMS045.QualityProject = item.QualityProject;
                            objQMS045.Project = item.Project;
                            objQMS045.BU = item.BU;
                            objQMS045.Location = currentLoc;
                            objQMS045.PartNo = item.PartNo;
                            objQMS045.ParentPartNo = item.ParentPartNo;
                            objQMS045.ChildPartNo = item.ChildPartNo;
                            objQMS045.Quantity = item.QMS035.Quantity;
                            objQMS045.UOM = item.QMS035.UOM;
                            objQMS045.StageCode = item.StageCode;
                            objQMS045.IterationNo = 1;
                            objQMS045.StageSequence = item.StageSequence;
                            objQMS045.Remarks = item.Remarks;
                            objQMS045.ProtocolId = item.ProtocolId;
                            objQMS045.ProtocolType = item.ProtocolType;
                            objQMS045.CreatedBy = objClsLoginInfo.UserName;
                            objQMS045.CreatedOn = DateTime.Now;
                            db.QMS045.Add(objQMS045);
                            db.SaveChanges();
                        }
                        objResponseMsg.Key = true;
                    }
                }

                // function for make part ready to offer 
                Manager.ReadyToOffer_PART(objQMS035, true);

            }

            //var readyToOfferStatus = clsImplementationEnum.PartlistOfferInspectionStatus.READY_TO_OFFER.GetStringValue();
            //var clearedStatus = clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue();

            //var ReadytoOfferStages = db.QMS045.Where(i => i.QualityProject.Equals(objQMS035.QualityProject, StringComparison.OrdinalIgnoreCase)
            //                                     && i.Project == objQMS035.Project && i.BU == objQMS035.BU && i.PartNo == objQMS035.PartNo && i.Location == objQMS035.Location && i.InspectionStatus.Equals(readyToOfferStatus)).ToList();
            //foreach (var readyToOfferStage in ReadytoOfferStages)
            //{
            //    readyToOfferStage.InspectionStatus = null;
            //    readyToOfferStage.Quantity = null;
            //    readyToOfferStage.BalanceQuantity = null;
            //    db.SaveChanges();
            //}
            //int maxOfferedSequenceNo = (new clsManager()).GetMaxOfferedSequenceNo_PART(objQMS035.QualityProject, objQMS035.BU, objQMS035.Location, objQMS035.PartNo);

            //bool isApplicableForRTO = true;
            //if (maxOfferedSequenceNo == 0)
            //{
            //    isApplicableForRTO = (new Utility.Controllers.GeneralController()).IsPartApplicableForRTO(objQMS035.Project, objQMS035.QualityProject, objQMS035.ChildPartNo);
            //}

            //if (isApplicableForRTO && !db.QMS045.Any(c => c.QualityProject.Equals(objQMS035.QualityProject, StringComparison.OrdinalIgnoreCase)
            //                                         && c.Project == objQMS035.Project && c.BU == objQMS035.BU && c.PartNo == objQMS035.PartNo && c.Location == objQMS035.Location
            //                                         && c.InspectionStatus != clearedStatus && c.StageSequence <= maxOfferedSequenceNo))
            //{
            //    int LowerSequence = db.QMS045.Where(i => i.QualityProject.Equals(objQMS035.QualityProject, StringComparison.OrdinalIgnoreCase)
            //                                     && i.Project == objQMS035.Project && i.BU == objQMS035.BU && i.PartNo == objQMS035.PartNo && i.Location == objQMS035.Location && (i.StageSequence > maxOfferedSequenceNo || (i.StageSequence == maxOfferedSequenceNo && (i.InspectionStatus == null || i.InspectionStatus.Equals(readyToOfferStatus))))).OrderBy(o => o.StageSequence).FirstOrDefault().StageSequence.Value;

            //    var LowerSeqStages = db.QMS045.Where(i => i.QualityProject.Equals(objQMS035.QualityProject, StringComparison.OrdinalIgnoreCase)
            //                                         && i.Project == objQMS035.Project && i.BU == objQMS035.BU && i.PartNo == objQMS035.PartNo && i.Location == objQMS035.Location && i.StageSequence == LowerSequence && (i.InspectionStatus == null || i.InspectionStatus.Equals(readyToOfferStatus))).ToList();
            //    foreach (var lowerStage in LowerSeqStages)
            //    {
            //        lowerStage.InspectionStatus = clsImplementationEnum.PartlistOfferInspectionStatus.READY_TO_OFFER.GetStringValue();
            //        lowerStage.Quantity = objQMS035.Quantity;
            //        lowerStage.BalanceQuantity = objQMS035.Quantity;
            //        db.SaveChanges();
            //    }
            //}

            return objResponseMsg;
        }

        [SessionExpireFilter]
        [AllowAnonymous]
        [HttpPost]
        public ActionResult ReturnICLPart(string strHeaderIds, string returnremark)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                List<string> lstICLNotSendForApprove = new List<string>();
                List<string> lstICLAttended = new List<string>();
                if (!string.IsNullOrEmpty(strHeaderIds))
                {
                    int[] headerIds = Array.ConvertAll(strHeaderIds.Split(','), i => Convert.ToInt32(i));
                    foreach (var headerId in headerIds)
                    {
                        var objQMS035 = db.QMS035.Where(i => i.HeaderId == headerId).FirstOrDefault();
                        if (objQMS035 != null)
                        {
                            if (objQMS035.Status == clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue())
                            {
                                objQMS035.Status = clsImplementationEnum.QualityIdHeaderStatus.Returned.GetStringValue();
                                objQMS035.ReturnedBy = objClsLoginInfo.UserName;
                                objQMS035.ReturnedOn = DateTime.Now;
                                objQMS035.Remarks = returnremark;
                                db.SaveChanges();
                                #region Send Notification
                                (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.WE3.GetStringValue(), objQMS035.Project, objQMS035.BU, objQMS035.Location, "ICL for Seam: " + objQMS035.PartNo + " of Project: " + objQMS035.QualityProject + "  has been Returned", clsImplementationEnum.NotificationType.Information.GetStringValue());
                                #endregion
                                lstICLAttended.Add(headerId.ToString());
                            }
                            else
                            {
                                lstICLNotSendForApprove.Add(headerId.ToString());
                            }
                        }
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = string.Format("{0} Part ICL(s) Returned successfully", lstICLAttended.Count());
                    if (lstICLNotSendForApprove.Any())
                    {
                        objResponseMsg.Remarks = string.Format("Part ICL(s) {0} have been skipped as Part(s) {0} has been already attended", lstICLNotSendForApprove.Count());
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        [AllowAnonymous]
        [HttpPost]
        public ActionResult ApproveICLPart(int strHeaderId)
        {
            QMS036 objQMS036 = new QMS036();
            objQMS036 = db.QMS036.Where(x => x.HeaderId == strHeaderId).FirstOrDefault();
            ResponceMsgWithTPI objResponseMsg = new ResponceMsgWithTPI();
            clsHelper.ResponseMsg objResponseMsg1 = new clsHelper.ResponseMsg();
            try
            {
                var lstQMS036 = (from dbo in db.QMS036
                                 where dbo.HeaderId == strHeaderId
                                 select dbo.StageCode.Trim()).Distinct().ToArray();
                bool isStageExists = true;
                for (int i = 0; i < lstQMS036.Length; i++)
                {

                    string strStage = lstQMS036[i];
                    QMS036 newobjQMS036 = db.QMS036.Where(x => x.StageCode == strStage && x.HeaderId == strHeaderId).FirstOrDefault();
                    QMS025 objQMS025 = db.QMS025.Where(a => a.Location == objQMS036.Location && a.BU == objQMS036.BU && a.Project == objQMS036.Project && strStage.Contains(a.Stage)).FirstOrDefault();
                    if (objQMS025 != null)
                    {
                        newobjQMS036.FirstTPIAgency = objQMS025.FirstTPIAgency;
                        newobjQMS036.FirstTPIIntervention = objQMS025.FirstTPIIntervention;
                        newobjQMS036.SecondTPIAgency = objQMS025.SecondTPIAgency;
                        newobjQMS036.SecondTPIIntervention = objQMS025.SecondTPIIntervention;
                        newobjQMS036.ThirdTPIAgency = objQMS025.ThirdTPIAgency;
                        newobjQMS036.ThirdTPIIntervention = objQMS025.ThirdTPIIntervention;
                        newobjQMS036.FourthTPIAgency = objQMS025.ForthTPIAgency;
                        newobjQMS036.FourthTPIIntervention = objQMS025.ForthTPIIntervention;
                        newobjQMS036.FifthTPIAgency = objQMS025.FifthTPIAgency;
                        newobjQMS036.FifthTPIIntervention = objQMS025.FifthTPIIntervention;
                        newobjQMS036.SixthTPIAgency = objQMS025.SixthTPIAgency;
                        newobjQMS036.SixthTPIIntervention = objQMS025.SixthTPIIntervention;
                        newobjQMS036.EditedBy = objClsLoginInfo.UserName;
                        newobjQMS036.EditedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                }
                var lstQMS025 = (from dbo in db.QMS025
                                 where dbo.Location == objQMS036.Location && dbo.BU == objQMS036.BU && dbo.Project == objQMS036.Project
                                 select dbo.Stage.Trim()).ToList();
                string strStageName = string.Empty;
                foreach (var lst in lstQMS036)
                {
                    if (!lstQMS025.Contains(lst))
                    {
                        strStageName += lst + ",";
                        isStageExists = false;
                    }
                }
                if (isStageExists)
                {
                    var objQMS035 = db.QMS035.Where(x => x.HeaderId == strHeaderId).FirstOrDefault();
                    var stageStatus = clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue();
                    var lstTPStages = db.QMS036.Where(x => x.HeaderId == objQMS035.HeaderId && !x.StageStatus.Equals(stageStatus, StringComparison.OrdinalIgnoreCase)).ToList();
                    if (lstTPStages.Any())
                    {
                        objResponseMsg1 = InsUpdOnPartOffer(objQMS035.HeaderId, lstTPStages);
                    }
                    if (objResponseMsg1.Key == true)
                    {
                        db.SP_IPI_ICL_PART_APPROVE(strHeaderId, "", objClsLoginInfo.UserName);

                        #region Send Notification
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), objQMS035.Project, objQMS035.BU, objQMS035.Location, "ICL for Part: " + objQMS035.PartNo + " of Project: " + objQMS035.QualityProject + "  has been Approved", clsImplementationEnum.NotificationType.Information.GetStringValue());
                        #endregion

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Document approved successfully";
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.ICLSeamMessages.Error.ToString();
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Add Stages " + strStageName;
                    objResponseMsg.tpi = "AddTPI";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ICLSeamMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponseMsg InsUpdOnPartOffer(int headerid, List<QMS036> lstQMS36)
        {
            //string currentUser = objClsLoginInfo.UserName;

            //var currentLoc = (from a in db.COM003
            //                  join b in db.COM002 on a.t_loca equals b.t_dimx
            //                  where b.t_dtyp == 1
            //                  && a.t_psno.Equals(currentUser, StringComparison.OrdinalIgnoreCase)
            //                  select b.t_dimx).FirstOrDefault().ToString();


            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();

            QMS045 objQMS045 = new QMS045();
            var objQMS035 = db.QMS035.Where(x => x.HeaderId == headerid).FirstOrDefault();
            if (objQMS035 != null)
            {
                foreach (var item in lstQMS36)
                {
                    objQMS045 = db.QMS045.Where(i => i.QualityProject.Equals(item.QualityProject, StringComparison.OrdinalIgnoreCase) && i.StageCode.Equals(item.StageCode, StringComparison.OrdinalIgnoreCase) && i.StageSequence == item.StageSequence && i.Location == item.Location && i.BU == item.BU && i.PartNo == item.PartNo).FirstOrDefault();
                    if (objQMS045 != null)
                    {

                        //objQMS040.StageStatus = item.StageStatus;
                        //if (string.Equals(objQMS020.Status, clsImplementationEnum.TestPlanStatus.Approved.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                        //{
                        //    objQMS020.Status = clsImplementationEnum.TestPlanStatus.Draft.GetStringValue();
                        //    objQMS020.RevNo += 1;
                        //}
                        //db.SaveChanges();
                        objResponseMsg.Key = true;
                    }
                    else
                    {
                        objQMS045 = new QMS045();
                        objQMS045.QualityProject = item.QualityProject;
                        objQMS045.Project = item.Project;
                        objQMS045.BU = item.BU;
                        objQMS045.Location = item.Location; //currentLoc;
                        objQMS045.PartNo = item.PartNo;
                        objQMS045.ParentPartNo = item.ParentPartNo;
                        objQMS045.ChildPartNo = item.ChildPartNo;
                        objQMS045.Quantity = objQMS035.Quantity;
                        objQMS045.BalanceQuantity = objQMS035.Quantity;
                        objQMS045.IterationNo = 1;
                        objQMS045.UOM = objQMS035.UOM;
                        objQMS045.StageCode = item.StageCode;
                        objQMS045.StageSequence = item.StageSequence;
                        objQMS045.Remarks = item.Remarks;
                        objQMS045.CreatedBy = objClsLoginInfo.UserName;
                        objQMS045.CreatedOn = DateTime.Now;
                        db.QMS045.Add(objQMS045);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        //objResponseMsg.Value = /*true*/;
                    }
                }
            }

            int LowerSequence = db.QMS045.Where(i => i.QualityProject.Equals(objQMS035.QualityProject, StringComparison.OrdinalIgnoreCase)
                                                 && i.Project == objQMS035.Project && i.BU == objQMS035.BU && i.PartNo == objQMS035.PartNo && i.Location == objQMS035.Location).OrderBy(o => o.StageSequence).FirstOrDefault().StageSequence.Value;

            var LowerSeqStages = db.QMS045.Where(i => i.QualityProject.Equals(objQMS035.QualityProject, StringComparison.OrdinalIgnoreCase)
                                                 && i.Project == objQMS035.Project && i.BU == objQMS035.BU && i.PartNo == objQMS035.PartNo && i.Location == objQMS035.Location && i.StageSequence == LowerSequence).ToList();

            foreach (var lowerStage in LowerSeqStages)
            {
                lowerStage.InspectionStatus = clsImplementationEnum.PartlistOfferInspectionStatus.READY_TO_OFFER.GetStringValue();
                db.SaveChanges();
            }

            return objResponseMsg;
        }

        public class ResponceMsgWithTPI : clsHelper.ResponseMsg
        {
            public string tpi;
            public string headerid;
        }

        //[HttpPost]
        //public ActionResult ApproveByApproverSelected(string strHeaderIds)
        //{
        //    clsHelper.ResponseMsg objResponseMsg1 = new clsHelper.ResponseMsg();
        //    QMS036 objQMS036 = new QMS036();
        //    ResponceMsgWithTPI objResponseMsg = new ResponceMsgWithTPI();
        //    string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
        //    try
        //    {
        //        for (int i = 0; i < arrayHeaderIds.Length; i++)
        //        {
        //            int strHeaderId = Convert.ToInt32(arrayHeaderIds[i]);
        //            objQMS036 = db.QMS036.Where(x => x.HeaderId == strHeaderId).FirstOrDefault();
        //            var lstQMS036 = (from dbo in db.QMS036
        //                             where dbo.HeaderId == strHeaderId
        //                             select dbo.StageCode.Trim()).Distinct().ToArray();

        //            bool isStageExists = true;
        //            for (int j = 0; j < lstQMS036.Length; j++)
        //            {
        //                string strStage = lstQMS036[j];
        //                QMS036 newobjQMS036 = db.QMS036.Where(x => x.StageCode == strStage && x.HeaderId == strHeaderId).FirstOrDefault();
        //                QMS025 objQMS025 = db.QMS025.Where(a => a.Location == objQMS036.Location && a.BU == objQMS036.BU && a.Project == objQMS036.Project && strStage.Contains(a.Stage)).FirstOrDefault();
        //                if (objQMS025 != null)
        //                {
        //                    newobjQMS036.FirstTPIAgency = objQMS025.FirstTPIAgency;
        //                    newobjQMS036.FirstTPIIntervention = objQMS025.FirstTPIIntervention;
        //                    newobjQMS036.SecondTPIAgency = objQMS025.SecondTPIAgency;
        //                    newobjQMS036.SecondTPIIntervention = objQMS025.SecondTPIIntervention;
        //                    newobjQMS036.ThirdTPIAgency = objQMS025.ThirdTPIAgency;
        //                    newobjQMS036.ThirdTPIIntervention = objQMS025.ThirdTPIIntervention;
        //                    newobjQMS036.FourthTPIAgency = objQMS025.ForthTPIAgency;
        //                    newobjQMS036.FourthTPIIntervention = objQMS025.ForthTPIIntervention;
        //                    newobjQMS036.FifthTPIAgency = objQMS025.FifthTPIAgency;
        //                    newobjQMS036.FifthTPIIntervention = objQMS025.FifthTPIIntervention;
        //                    newobjQMS036.SixthTPIAgency = objQMS025.SixthTPIAgency;
        //                    newobjQMS036.SixthTPIIntervention = objQMS025.SixthTPIIntervention;
        //                    newobjQMS036.EditedBy = objClsLoginInfo.UserName;
        //                    newobjQMS036.EditedOn = DateTime.Now;
        //                    db.SaveChanges();
        //                }
        //            }
        //            var lstQMS025 = (from dbo in db.QMS025
        //                             where dbo.Location == objQMS036.Location && dbo.BU == objQMS036.BU && dbo.Project == objQMS036.Project
        //                             select dbo.Stage.Trim()).ToList();
        //            string strStageName = string.Empty;
        //            foreach (var lst in lstQMS036)
        //            {
        //                if (!lstQMS025.Contains(lst))
        //                {
        //                    strStageName += lst + ",";
        //                    isStageExists = false;
        //                }
        //            }
        //            if (isStageExists)
        //            {
        //                var objQMS035 = db.QMS035.Where(x => x.HeaderId == strHeaderId).FirstOrDefault();
        //                var stageStatus = clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue();
        //                var lstTPStages = db.QMS036.Where(x => x.HeaderId == objQMS035.HeaderId && !x.StageStatus.Equals(stageStatus, StringComparison.OrdinalIgnoreCase)).ToList();
        //                if (lstTPStages.Any())
        //                {
        //                    objResponseMsg1 = InsUpdOnPartOffer(objQMS035.HeaderId, lstTPStages);
        //                }
        //                if (objResponseMsg1.Key == true)
        //                {
        //                    db.SP_IPI_ICL_PART_APPROVE(strHeaderId, "", objClsLoginInfo.UserName);
        //                    objResponseMsg.Key = true;
        //                    objResponseMsg.Value = "Document approved successfully";
        //                }
        //                else
        //                {
        //                    objResponseMsg.Key = false;
        //                    objResponseMsg.Value = clsImplementationMessage.ICLSeamMessages.Error.ToString();
        //                }
        //            }
        //            else
        //            {
        //                objResponseMsg.Key = false;
        //                objResponseMsg.Value = "Add Stages " + strStageName;
        //                objResponseMsg.tpi = "AddTPI";
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        objResponseMsg.Key = false;
        //        objResponseMsg.Value = clsImplementationMessage.ICLSeamMessages.Error.ToString();
        //    }
        //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        public ActionResult LoadTPIIndexData(int headerId, int lineId)
        {
            QMS035 objQMS035 = new QMS035();
            QMS025 objQMS025 = new QMS025();
            objQMS035 = db.QMS035.Where(x => x.HeaderId == headerId).FirstOrDefault();

            if (objQMS035 != null)
            {
                objQMS025.BU = db.COM002.Where(a => a.t_dimx == objQMS035.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objQMS025.Project = db.COM001.Where(i => i.t_cprj == objQMS035.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objQMS025.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objQMS035.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objQMS025.QualityProject = objQMS035.QualityProject;
                ViewBag.Action = "multipleapprove";
            }
            else
            {
                objQMS025.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objClsLoginInfo.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                ViewBag.Action = "addmultipleapprove";
            }
            return PartialView("_LoadTPIDataHtml", objQMS025);
        }

        //[HttpPost]
        //public ActionResult LoadTPIData(int headerId, int lineId)
        //{
        //    QMS035 objQMS035 = new QMS035();
        //    QMS025 objQMS025 = new QMS025();
        //    objQMS035 = db.QMS035.Where(x => x.HeaderId == headerId).FirstOrDefault();

        //    if (objQMS035 != null)
        //    {
        //        ViewBag.Stages = db.QMS002.Where(i => i.BU.Equals(objQMS035.BU, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(objQMS035.Location, StringComparison.OrdinalIgnoreCase)).Select(i => new CategoryData { Value = i.StageCode, Code = i.StageCode, CategoryDescription = i.StageCode + "-" + i.StageDesc }).ToList();
        //        var lstTPIAgency = Manager.GetSubCatagories("TPI Agency", objQMS035.BU, objQMS035.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
        //        var lstTPIIntervention = Manager.GetSubCatagories("TPI Intervention", objQMS035.BU, objQMS035.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
        //        ViewBag.TPIAgency = lstTPIAgency;
        //        ViewBag.TPIIntervention = lstTPIIntervention;
        //        objQMS025.BU = db.COM002.Where(a => a.t_dimx == objQMS035.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
        //        objQMS025.Project = db.COM001.Where(i => i.t_cprj == objQMS035.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
        //        objQMS025.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objQMS035.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
        //        objQMS025.QualityProject = objQMS035.QualityProject;
        //        ViewBag.Action = "singleapprove";
        //    }
        //    return PartialView("_LoadTPIDataHtml", objQMS025);
        //}
        [HttpPost]
        public ActionResult SaveTPI(FormCollection fc, QMS025 QMS025)
        {


            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                string location = fc["Location"].Split('-')[0];
                string Status = clsImplementationEnum.ICLSeamStatus.Draft.GetStringValue();
                string Approved = clsImplementationEnum.ICLSeamStatus.Approved.GetStringValue();
                string loc = fc["Location"].Split('-')[0];
                string Fromstage = fc["hdFromStage"]; //fc["Stage"].Split('-')[0];
                string Tostage = fc["hdToStage"];//fc["ToStage"].Split('-')[0];
                string project = QMS025.Project.Split('-')[0];
                string BU = QMS025.BU.Split('-')[0];

                var lstResultForstage = db.SP_GET_PART_ICL_STAGECODE_FORTPI(Fromstage, Tostage, QMS025.QualityProject).ToList();


                if (lstResultForstage.Count() > 0)
                {
                    foreach (var Stagecode in lstResultForstage.ToList())
                    {
                        if (!db.QMS025.Any(x => x.QualityProject == QMS025.QualityProject && x.Project == project && x.Location == location && x.BU == BU && x.Stage == Stagecode.StageCode.ToString()))
                        {
                            QMS025 objQMS025 = new QMS025();
                            objQMS025.QualityProject = QMS025.QualityProject;
                            objQMS025.Project = QMS025.Project.Split('-')[0];
                            objQMS025.BU = QMS025.BU.Split('-')[0];
                            objQMS025.Location = fc["Location"].Split('-')[0];
                            objQMS025.Stage = Stagecode.StageCode.ToString();

                            if (!string.IsNullOrWhiteSpace(fc["FirstTPIAgency"]))
                                objQMS025.FirstTPIAgency = fc["FirstTPIAgency"].Split('-')[0];
                            else
                                objQMS025.FirstTPIAgency = null;

                            if (!string.IsNullOrWhiteSpace(fc["FirstTPIIntervention"]))
                                objQMS025.FirstTPIIntervention = fc["FirstTPIIntervention"].Split('-')[0];
                            else
                                objQMS025.FirstTPIIntervention = null;


                            if (!string.IsNullOrWhiteSpace(fc["SecondTPIAgency"]))
                                objQMS025.SecondTPIAgency = fc["SecondTPIAgency"].Split('-')[0];
                            else
                                objQMS025.SecondTPIAgency = null;

                            if (!string.IsNullOrWhiteSpace(fc["SecondTPIIntervention"]))
                                objQMS025.SecondTPIIntervention = fc["SecondTPIIntervention"].Split('-')[0];
                            else
                                objQMS025.SecondTPIIntervention = null;


                            if (!string.IsNullOrWhiteSpace(fc["ThirdTPIAgency"]))
                                objQMS025.ThirdTPIAgency = fc["ThirdTPIAgency"].Split('-')[0];
                            else
                                objQMS025.ThirdTPIAgency = null;

                            if (!string.IsNullOrWhiteSpace(fc["ThirdTPIIntervention"]))
                                objQMS025.ThirdTPIIntervention = fc["ThirdTPIIntervention"].Split('-')[0];
                            else
                                objQMS025.ThirdTPIIntervention = null;


                            if (!string.IsNullOrWhiteSpace(fc["ForthTPIAgency"]))
                                objQMS025.ForthTPIAgency = fc["ForthTPIAgency"].Split('-')[0];
                            else
                                objQMS025.ForthTPIAgency = null;

                            if (!string.IsNullOrWhiteSpace(fc["ForthTPIIntervention"]))
                                objQMS025.ForthTPIIntervention = fc["ForthTPIIntervention"].Split('-')[0];
                            else
                                objQMS025.ForthTPIIntervention = null;


                            if (!string.IsNullOrWhiteSpace(fc["FifthTPIAgency"]))
                                objQMS025.FifthTPIAgency = fc["FifthTPIAgency"].Split('-')[0];
                            else
                                objQMS025.FifthTPIAgency = null;

                            if (!string.IsNullOrWhiteSpace(fc["FifthTPIIntervention"]))
                                objQMS025.FifthTPIIntervention = fc["FifthTPIIntervention"].Split('-')[0];
                            else
                                objQMS025.FifthTPIIntervention = null;


                            if (!string.IsNullOrWhiteSpace(fc["SixthTPIAgency"]))
                                objQMS025.SixthTPIAgency = fc["SixthTPIAgency"].Split('-')[0];
                            else
                                objQMS025.SixthTPIAgency = null;

                            if (!string.IsNullOrWhiteSpace(fc["SixthTPIIntervention"]))
                                objQMS025.SixthTPIIntervention = fc["SixthTPIIntervention"].Split('-')[0];
                            else
                                objQMS025.SixthTPIIntervention = null;
                            objQMS025.CreatedBy = objClsLoginInfo.UserName;
                            objQMS025.CreatedOn = DateTime.Now;
                            db.QMS025.Add(objQMS025);
                            db.SaveChanges();
                        }
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Record Added Successfully";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Record Not Found";
                    //objResponseMsg.Status = "Record Not Found";
                    ViewBag.Action = "Add";
                    return Json(objResponseMsg);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg);
        }
        //[HttpPost]
        //public ActionResult SaveTPI(FormCollection fc, QMS025 QMS025)
        //{

        //    QMS025 objQMS025 = new QMS025();
        //    ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
        //    try
        //    {
        //        string location = fc["Location"].Split('-')[0];
        //        string Status = clsImplementationEnum.ICLPartStatus.Draft.GetStringValue();
        //        string Approved = clsImplementationEnum.ICLPartStatus.Approved.GetStringValue();
        //        if (QMS025.Id > 0)
        //        {
        //            objQMS025 = db.QMS025.Where(x => x.Id == QMS025.Id).FirstOrDefault();
        //            objQMS025.QualityProject = QMS025.QualityProject;
        //            objQMS025.Stage = QMS025.Stage;
        //            //objQMS025.FirstTPIAgency = QMS025.FirstTPIAgency.Split('-')[0];
        //            //objQMS025.FirstTPIIntervention = QMS025.FirstTPIIntervention.Split('-')[0];

        //            if (!string.IsNullOrWhiteSpace(QMS025.FirstTPIAgency))
        //                objQMS025.FirstTPIAgency = QMS025.FirstTPIAgency.Split('-')[0];
        //            else
        //                objQMS025.FirstTPIAgency = null;

        //            if (!string.IsNullOrWhiteSpace(QMS025.FirstTPIIntervention))
        //                objQMS025.FirstTPIIntervention = QMS025.FirstTPIIntervention.Split('-')[0];
        //            else
        //                objQMS025.FirstTPIIntervention = null;


        //            if (!string.IsNullOrWhiteSpace(QMS025.SecondTPIAgency))
        //                objQMS025.SecondTPIAgency = QMS025.SecondTPIAgency.Split('-')[0];
        //            else
        //                objQMS025.SecondTPIAgency = null;

        //            if (!string.IsNullOrWhiteSpace(QMS025.SecondTPIIntervention))
        //                objQMS025.SecondTPIIntervention = QMS025.SecondTPIIntervention.Split('-')[0];
        //            else
        //                objQMS025.SecondTPIIntervention = null;


        //            if (!string.IsNullOrWhiteSpace(QMS025.ThirdTPIAgency))
        //                objQMS025.ThirdTPIAgency = QMS025.ThirdTPIAgency.Split('-')[0];
        //            else
        //                objQMS025.ThirdTPIAgency = null;

        //            if (!string.IsNullOrWhiteSpace(QMS025.ThirdTPIIntervention))
        //                objQMS025.ThirdTPIIntervention = QMS025.ThirdTPIIntervention.Split('-')[0];
        //            else
        //                objQMS025.ThirdTPIIntervention = null;


        //            if (!string.IsNullOrWhiteSpace(QMS025.ForthTPIAgency))
        //                objQMS025.ForthTPIAgency = QMS025.ForthTPIAgency.Split('-')[0];
        //            else
        //                objQMS025.ForthTPIAgency = null;

        //            if (!string.IsNullOrWhiteSpace(QMS025.ForthTPIIntervention))
        //                objQMS025.ForthTPIIntervention = QMS025.ForthTPIIntervention.Split('-')[0];
        //            else
        //                objQMS025.ForthTPIIntervention = null;


        //            if (!string.IsNullOrWhiteSpace(QMS025.FifthTPIAgency))
        //                objQMS025.FifthTPIAgency = QMS025.FifthTPIAgency.Split('-')[0];
        //            else
        //                objQMS025.FifthTPIAgency = null;

        //            if (!string.IsNullOrWhiteSpace(QMS025.FifthTPIIntervention))
        //                objQMS025.FifthTPIIntervention = QMS025.FifthTPIIntervention.Split('-')[0];
        //            else
        //                objQMS025.FifthTPIIntervention = null;


        //            if (!string.IsNullOrWhiteSpace(QMS025.SixthTPIAgency))
        //                objQMS025.SixthTPIAgency = QMS025.SixthTPIAgency.Split('-')[0];
        //            else
        //                objQMS025.SixthTPIAgency = null;

        //            if (!string.IsNullOrWhiteSpace(QMS025.SixthTPIIntervention))
        //                objQMS025.SixthTPIIntervention = QMS025.SixthTPIIntervention.Split('-')[0];
        //            else
        //                objQMS025.SixthTPIIntervention = null;


        //            objQMS025.Remarks = QMS025.Remarks;

        //            objQMS025.CreatedBy = QMS025.CreatedBy;
        //            objQMS025.CreatedOn = QMS025.CreatedOn;
        //            objQMS025.EditedBy = objClsLoginInfo.UserName;
        //            objQMS025.EditedOn = DateTime.Now;
        //            int Id = objQMS025.Id;
        //            objResponseMsg.Key = true;
        //            objResponseMsg.Value = Id.ToString();
        //            db.SaveChanges();
        //        }
        //        else
        //        {

        //            string loc = fc["Location"].Split('-')[0];
        //            string Fromstage = fc["Stage"].Split('-')[0];
        //            string Tostage = fc["ToStage"].Split('-')[0];



        //            var lstResultForstage = db.SP_GET_PART_ICL_STAGECODE_FORTPI
        //                     (
        //                     Fromstage, Tostage, QMS025.QualityProject).ToList();

        //            if (lstResultForstage.Count > 0)
        //            {
        //                foreach (var Stagecode in lstResultForstage.ToList())
        //                {
        //                    objQMS025.QualityProject = QMS025.QualityProject;
        //                    objQMS025.Project = QMS025.Project.Split('-')[0];
        //                    objQMS025.BU = QMS025.BU.Split('-')[0];
        //                    objQMS025.Location = loc;
        //                    objQMS025.Stage = Stagecode.StageCode.ToString();

        //                    if (!string.IsNullOrWhiteSpace(fc["FirstTPIAgency"]))
        //                        objQMS025.FirstTPIAgency = fc["FirstTPIAgency"].Split('-')[0];
        //                    else
        //                        objQMS025.FirstTPIAgency = null;

        //                    if (!string.IsNullOrWhiteSpace(fc["FirstTPIIntervention"]))
        //                        objQMS025.FirstTPIIntervention = fc["FirstTPIIntervention"].Split('-')[0];
        //                    else
        //                        objQMS025.FirstTPIIntervention = null;


        //                    if (!string.IsNullOrWhiteSpace(fc["SecondTPIAgency"]))
        //                        objQMS025.SecondTPIAgency = fc["SecondTPIAgency"].Split('-')[0];
        //                    else
        //                        objQMS025.SecondTPIAgency = null;

        //                    if (!string.IsNullOrWhiteSpace(fc["SecondTPIIntervention"]))
        //                        objQMS025.SecondTPIIntervention = fc["SecondTPIIntervention"].Split('-')[0];
        //                    else
        //                        objQMS025.SecondTPIIntervention = null;


        //                    if (!string.IsNullOrWhiteSpace(fc["ThirdTPIAgency"]))
        //                        objQMS025.ThirdTPIAgency = fc["ThirdTPIAgency"].Split('-')[0];
        //                    else
        //                        objQMS025.ThirdTPIAgency = null;

        //                    if (!string.IsNullOrWhiteSpace(fc["ThirdTPIIntervention"]))
        //                        objQMS025.ThirdTPIIntervention = fc["ThirdTPIIntervention"].Split('-')[0];
        //                    else
        //                        objQMS025.ThirdTPIIntervention = null;


        //                    if (!string.IsNullOrWhiteSpace(fc["ForthTPIAgency"]))
        //                        objQMS025.ForthTPIAgency = fc["ForthTPIAgency"].Split('-')[0];
        //                    else
        //                        objQMS025.ForthTPIAgency = null;

        //                    if (!string.IsNullOrWhiteSpace(fc["ForthTPIIntervention"]))
        //                        objQMS025.ForthTPIIntervention = fc["ForthTPIIntervention"].Split('-')[0];
        //                    else
        //                        objQMS025.ForthTPIIntervention = null;


        //                    if (!string.IsNullOrWhiteSpace(fc["FifthTPIAgency"]))
        //                        objQMS025.FifthTPIAgency = fc["FifthTPIAgency"].Split('-')[0];
        //                    else
        //                        objQMS025.FifthTPIAgency = null;

        //                    if (!string.IsNullOrWhiteSpace(fc["FifthTPIIntervention"]))
        //                        objQMS025.FifthTPIIntervention = fc["FifthTPIIntervention"].Split('-')[0];
        //                    else
        //                        objQMS025.FifthTPIIntervention = null;


        //                    if (!string.IsNullOrWhiteSpace(fc["SixthTPIAgency"]))
        //                        objQMS025.SixthTPIAgency = fc["SixthTPIAgency"].Split('-')[0];
        //                    else
        //                        objQMS025.SixthTPIAgency = null;

        //                    if (!string.IsNullOrWhiteSpace(fc["SixthTPIIntervention"]))
        //                        objQMS025.SixthTPIIntervention = fc["SixthTPIIntervention"].Split('-')[0];
        //                    else
        //                        objQMS025.SixthTPIIntervention = null;

        //                    objQMS025.CreatedBy = objClsLoginInfo.UserName;
        //                    objQMS025.CreatedOn = DateTime.Now;
        //                    db.QMS025.Add(objQMS025);
        //                    db.SaveChanges();
        //                    int Id = objQMS025.Id;
        //                    objResponseMsg.Key = true;
        //                    objResponseMsg.Value = Id.ToString();

        //                }
        //            }
        //            else
        //            {
        //                objResponseMsg.Key = false;
        //                objResponseMsg.Value = "Record Not Found";
        //                //objResponseMsg.Status = "Record Not Found";
        //                ViewBag.Action = "Add";
        //                return Json(objResponseMsg);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        objResponseMsg.Key = false;
        //        objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
        //    }
        //    return Json(objResponseMsg);
        //}

        public class BUWiseDropdown
        {
            public string Project { get; set; }
            public string BUDescription { get; set; }
            public bool Key;
            public string Value;

            public List<CategoryData> TPIAgency { get; set; }
            public List<CategoryData> TPIIntervention { get; set; }
            public List<CategoryData> Stage { get; set; }
        }
        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            if (LineId > 0)
            {
                QMS036 objQMS036 = db.QMS036.Where(x => x.LineId == LineId).FirstOrDefault();
                model.ApprovedBy = objQMS036.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objQMS036.ApprovedBy) : null;
                model.ApprovedOn = objQMS036.ApprovedOn;
                model.SubmittedBy = objQMS036.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objQMS036.SubmittedBy) : null;
                model.SubmittedOn = objQMS036.SubmittedOn;
                model.CreatedBy = objQMS036.CreatedBy != null ? Manager.GetUserNameFromPsNo(objQMS036.CreatedBy) : null;
                model.CreatedOn = objQMS036.CreatedOn;
                model.EditedBy = objQMS036.EditedBy != null ? Manager.GetUserNameFromPsNo(objQMS036.EditedBy) : null;
                model.EditedOn = objQMS036.EditedOn;
            }
            else
            {
                QMS035 objQMS035 = db.QMS035.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.ApprovedBy = objQMS035.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objQMS035.ApprovedBy) : null;
                model.ApprovedOn = objQMS035.ApprovedOn;
                model.SubmittedBy = objQMS035.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objQMS035.SubmittedBy) : null;
                model.SubmittedOn = objQMS035.SubmittedOn;
                model.CreatedBy = objQMS035.CreatedBy != null ? Manager.GetUserNameFromPsNo(objQMS035.CreatedBy) : null;
                model.CreatedOn = objQMS035.CreatedOn;
                model.EditedBy = objQMS035.EditedBy != null ? Manager.GetUserNameFromPsNo(objQMS035.EditedBy) : null;
                model.EditedOn = objQMS035.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress2.cshtml", model);
        }
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.APPROVEINDEX.GetStringValue())
                {
                    var lst = db.SP_IPI_ICL_PART_HEADER_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_IPI_PART_ICL_HEADER_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      PartNo = li.PartNo,
                                      ParentPartNo = li.ParentPartNo,
                                      ChildPartNo = li.ChildPartNo,
                                      QualityId = li.QualityId,
                                      QualityIdRevNo = li.QualityIdRevNo,
                                      RevNo = li.RevNo,
                                      Status = li.Status,
                                      Quantity = li.Quantity,
                                      UOM = li.UOM,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      SubmittedBy = li.SubmittedBy,
                                      SubmittedOn = li.SubmittedOn,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn,
                                      ReturnedBy = li.ReturnedBy,
                                      ReturnedOn = li.ReturnedOn,
                                      Remarks = li.Remarks
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_IPI_ICL_PART_LINE_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      PartNo = li.PartNo,
                                      ParentPartNo = li.ParentPartNo,
                                      ChildPartNo = li.ChildPartNo,
                                      StageCode = li.StageCode,
                                      StageSequence = li.StageSequence,
                                      StageStatus = li.StageStatus,
                                      Parallel = li.Parallel,
                                      AcceptanceStandard = li.AcceptanceStandard,
                                      ApplicableSpecification = li.ApplicableSpecification,
                                      InspectionExtent = li.InspectionExtent,
                                      Remarks = li.Remarks,
                                      FirstTPIAgency = li.FirstTPIAgency,
                                      FirstTPIIntervention = li.FirstTPIIntervention,
                                      SecondTPIAgency = li.SecondTPIAgency,
                                      SecondTPIIntervention = li.SecondTPIIntervention,
                                      ThirdTPIAgency = li.ThirdTPIAgency,
                                      ThirdTPIIntervention = li.ThirdTPIIntervention,
                                      FourthTPIAgency = li.FourthTPIAgency,
                                      FourthTPIIntervention = li.FourthTPIIntervention,
                                      FifthTPIAgency = li.FifthTPIAgency,
                                      FifthTPIIntervention = li.FifthTPIIntervention,
                                      SixthTPIAgency = li.SixthTPIAgency,
                                      SixthTPIIntervention = li.SixthTPIIntervention,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {
                    var lst = db.SP_IPI_SEAM_ICL_HISTORY_HEADER_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      SeamNo = li.SeamNo,
                                      RevNo = li.RevNo,
                                      TPRevNo = li.TPRevNo,
                                      PTCApplicable = li.PTCApplicable,
                                      PTCNumber = li.PTCNumber,
                                      Status = li.Status,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else
                {
                    var lst = db.SP_IPI_SEAM_ICL_HISTORY_LINE_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      SeamNo = li.SeamNo,
                                      StageCode = li.StageCode,
                                      StageSequance = li.StageSequance,
                                      StageStatus = li.StageStatus,
                                      Parallel = li.Parallel,
                                      FirstTPIAgency = li.FirstTPIAgency,
                                      FirstTPIIntervention = li.FirstTPIIntervention,
                                      SecondTPIAgency = li.SecondTPIAgency,
                                      SecondTPIIntervention = li.SecondTPIIntervention,
                                      ThirdTPIAgency = li.ThirdTPIAgency,
                                      ThirdTPIIntervention = li.ThirdTPIIntervention,
                                      ForthTPIAgency = li.ForthTPIAgency,
                                      ForthTPIIntervention = li.ForthTPIIntervention,
                                      FifthTPIAgency = li.FifthTPIAgency,
                                      FifthTPIIntervention = li.FifthTPIIntervention,
                                      SixthTPIAgency = li.SixthTPIAgency,
                                      SixthTPIIntervention = li.SixthTPIIntervention,
                                      AcceptanceStandard = li.AcceptanceStandard,
                                      ApplicableSpecification = li.ApplicableSpecification,
                                      InspectionExtent = li.InspectionExtent,
                                      Remarks = li.Remarks,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetProject(string qms)
        {   //get project and BU by Qms project
            if (!string.IsNullOrWhiteSpace(qms))
            {

                BUWiseDropdown objProjectDataModel = new BUWiseDropdown();

                string project = db.QMS010.Where(a => a.QualityProject == qms).FirstOrDefault().Project;
                string location = objClsLoginInfo.Location;
                var lstProject = (from a in db.COM001
                                  where a.t_cprj == project
                                  select new { ProjectDesc = a.t_cprj + " - " + a.t_dsca }).FirstOrDefault();
                string strBU = db.COM001.Where(i => i.t_cprj == project).FirstOrDefault().t_entu;

                var BUDescription = (from a in db.COM002
                                     where a.t_dtyp == 2 && a.t_dimx == strBU
                                     select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();

                var lstStage = db.QMS002.Where(i => i.BU.Equals(strBU, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(location, StringComparison.OrdinalIgnoreCase)).Select(i => new CategoryData { Value = i.StageCode, Code = i.StageCode, CategoryDescription = i.StageCode + "-" + i.StageDesc }).ToList();
                var lstTPIAgency = Manager.GetSubCatagories("TPI Agency", strBU, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
                var lstTPIIntervention = Manager.GetSubCatagories("TPI Intervention", strBU, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();

                objProjectDataModel.Project = lstProject.ProjectDesc;
                objProjectDataModel.BUDescription = BUDescription.BUDesc;

                objProjectDataModel.TPIAgency = lstTPIAgency.Any() ? lstTPIAgency : null;
                objProjectDataModel.TPIIntervention = lstTPIIntervention.Any() ? lstTPIIntervention : null;
                objProjectDataModel.Stage = lstStage.Any() ? lstStage : null;

                if (lstTPIAgency.Any() && lstTPIIntervention.Any() && lstStage.Any())
                {
                    objProjectDataModel.Key = true;
                }
                else
                {
                    List<string> lstCategory = new List<string>();
                    if (!lstTPIAgency.Any())
                        lstCategory.Add("TPI Agency");
                    if (!lstTPIIntervention.Any())
                        lstCategory.Add("TPI Intervention");

                    objProjectDataModel.Value = string.Join(",", lstCategory.ToList()) + " Category not exist for selected project/Quality Project, Please contact Admin";
                }

                return Json(objProjectDataModel, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }





    }
}