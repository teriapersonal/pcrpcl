﻿using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.IPI.Controllers
{
    public class ApproveTestPlanController : clsBase
    {
        //GET: IPI/ApproveTestPlan

        #region Main page
        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadHeaderPartial(string status, string qualityProject, string location)
        {
            ViewBag.Status = status;
            ViewBag.qualityProject = qualityProject;
            ViewBag.Location = location;

            ViewBag.DefaultSearch = Convert.ToString(Session["TP_SWP_Search"]);

            if (!string.IsNullOrEmpty(Convert.ToString(Session["TP_SWP_PageSize"])))
            {
                ViewBag.DefaultPageSize = Convert.ToString(Session["TP_SWP_PageSize"]);
            }
            else
            {
                //ViewBag.DefaultPageSize = "10";
                ViewBag.DefaultPageSize = "200"; // Code Change by rahul (Task ID = 18239)

            }

            List<string> lstTP_SWPSeams = (List<string>)Session["TP_SWP_SeamList"];
            if (lstTP_SWPSeams != null && lstTP_SWPSeams.Count > 0)
            {
                ViewBag.lstTP_SWPSeams = String.Join(",", lstTP_SWPSeams);
            }
            else
            {
                ViewBag.lstTP_SWPSeams = string.Empty;
            }

            return PartialView("_LoadHeaderGridPartial");
        }

        [HttpPost]
        public JsonResult LoadTPHeaderData_pending(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += " 1=1 and (qms020.status in('" + clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue() + "') or swp010.status in('" + clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue() + "'))";
                }
                else
                {
                    strWhere += " 1=1";
                }
                string[] columnName = { "qms020.QualityProject", "qms020.Location", "qms020.PTCApplicable", "qms020.PTCNumber", "qms020.Status", "qms020.BU", "qms020.Project" };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.Location, "", "Location");
                //strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.Location, "ST2.BU", "ST2.Location");

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_IPI_GET_TESTPLAN_INDEXRESULT
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                 Convert.ToString(uc.HeaderId),
                            Convert.ToString(uc.QualityProject),
                            Convert.ToString(uc.Project),
                            Convert.ToString(uc.BU),
                            Convert.ToString(uc.Location),
                            "<center><a class='' title='View' href='" + WebsiteURL + "/IPI/ApproveTestPlan/TestPlanDetails?HeaderID="+uc.HeaderId+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a></center>",
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadTPHeaderData(JQueryDataTableParamModel param, string qProj, string location)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;


                Session["TP_SWP_PageSize"] = Convert.ToString(param.iDisplayLength);

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += " 1=1 and status in('" + clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue() + "')";
                }
                else
                {
                    strWhere += " 1=1";
                }
                if (!string.IsNullOrEmpty(qProj))
                {
                    strWhere += " AND UPPER(QualityProject) = '" + qProj.ToUpper() + "'";
                }
                if (!string.IsNullOrEmpty(location))
                {
                    strWhere += " AND qms30.Location in('" + location + "')";
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                string[] columnName = { "SeamNo", "qms30.SeamDescription", "RevNo", "qms30.QualityId", "QualityIdRev", "PTCApplicable", "PTCNumber", "Status" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);

                }
                Session["TP_SWP_Search"] = param.sSearch;

                //strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.Location, "ST2.BU", "ST2.Location");
                var lstResult = db.SP_IPI_TEST_PLAN_HEADER_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                  Convert.ToString(uc.HeaderId),
                            Convert.ToString(uc.HeaderId),
                      //      Convert.ToString(uc.Location),
                            Convert.ToString(uc.SeamNo),
                            Convert.ToString(uc.QualityId),
                            string.IsNullOrEmpty(Convert.ToString(uc.QualityIdRev))?Convert.ToString(uc.QualityIdRev):"R"+Convert.ToString(uc.QualityIdRev),
                            Convert.ToString(uc.PTCApplicable == true ? "Yes":"No"),
                            Convert.ToString(uc.PTCNumber),
                            string.IsNullOrEmpty(Convert.ToString(uc.RevNo))?Convert.ToString(uc.RevNo):"R"+Convert.ToString(uc.RevNo),
                            Convert.ToString(uc.Status),
                            "<center style='display:inline;'>" + Manager.generateSeamHistoryButton(uc.Project.Split('-')[0].Trim(),uc.QualityProject, uc.SeamNo,"Seam Details") +
                                    ("<a class='' href='javascript:void(0)' title='Timeline' onclick=ShowTimeline('/IPI/ApproveTestPlan/ShowTimeline?HeaderID="+Convert.ToInt32(uc.HeaderId)+"');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a>")
                                    +"</center>",
                            //"<center><a class='btn btn-xs green' href='/IPI/ApproveTestPlan/TestPlanDetails?HeaderID="+uc.HeaderId+"'>View<i style='margin-left:5px;' class='fa fa-eye'></i></a></center>",
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData =new string[] {}
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        [HttpPost]
        public ActionResult LoadTestPlanLineData(int headerId)
        {
            QMS020 objQMS020 = db.QMS020.Where(x => x.HeaderId == headerId).FirstOrDefault();
            if (objQMS020 != null)
            {

            }
            return PartialView("_LoadTPLinesDetailsForm", objQMS020);
        }

        #region View Details
        [SessionExpireFilter]
        [AllowAnonymous]
        //[UserPermissions]
        public ActionResult TestPlanDetails(string headerID)
        {
            int HeaderId = 0;
            if (!string.IsNullOrWhiteSpace(headerID))
            {
                HeaderId = Convert.ToInt32(headerID);
            }
            QMS020 objQMS020 = db.QMS020.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            ViewBag.BU = db.COM002.Where(a => a.t_dimx == objQMS020.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            ViewBag.Project = db.COM001.Where(i => i.t_cprj == objQMS020.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objQMS020.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            SWP010 objSWP010 = db.SWP010.Where(x => x.QualityProject == objQMS020.QualityProject && x.Project == objQMS020.Project && x.Location == objQMS020.Location && x.BU == objQMS020.BU).FirstOrDefault();
            if (objSWP010 != null)
                ViewBag.SWPHeaderId = objSWP010.HeaderId;
            else
                ViewBag.SWPHeaderId = 0;
            ViewBag.HeaderID = headerID;
            //if (objQMS020 != null)
            //{
            //    objQMS020.BU = db.COM002.Where(a => a.t_dimx == objQMS020.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            //    objQMS020.Project = db.COM001.Where(i => i.t_cprj == objQMS020.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            //    ViewBag.Location = objQMS020.Location;
            //    objQMS020.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objQMS020.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            //}

            Session["TP_SWP_SeamList"] = null;
            Session["TP_SWP_PageSize"] = null;
            Session["TP_SWP_Search"] = null;

            return View(objQMS020);
        }
        [HttpPost]
        public ActionResult LoadTPHeaderForm(int HeaderId)
        {
            QMS020 objQMS020 = new QMS020();
            if (HeaderId > 0)
            {
                objQMS020 = db.QMS020.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                ViewBag.Location = objQMS020.Location;
                objQMS020.BU = db.COM002.Where(a => a.t_dimx == objQMS020.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objQMS020.Project = db.COM001.Where(i => i.t_cprj == objQMS020.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objQMS020.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objQMS020.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();

                //var lstQualityIds1 = (from a in db.QMS015_Log
                //                      where a.QualityProject == objQMS020.QualityProject && (a.QualityIdApplicableFor == "Seam" || a.QualityIdApplicableFor == "Seam")
                //                      select new CategoryData { Value = a.QualityId, Code = a.QualityId, CategoryDescription = a.QualityId + " - " + a.QualityIdDesc }).Distinct().ToList();

                //ViewBag.QualityID = lstQualityIds1;
                //var lstQID15 = db.QMS015_Log.Where(i => i.QualityProject == objQMS020.QualityProject && i.QualityIdApplicableFor == "Seam").Select(i => i.QualityId).Distinct().ToList();
                //var lstQID17 = db.QMS017_Log.Where(i => i.IQualityProject == objQMS020.QualityProject).Select(i => i.QualityId).Distinct().ToList();
                //ViewBag.lstQualityId = lstQID15;//.Union(lstQID17);
                //ViewBag.lstPTCApplicable = clsImplementationEnum.getyesno().ToArray();
            }



            return PartialView("_LoadTPHeaderForm", objQMS020);
        }
        [HttpPost]
        public JsonResult LoadTestPlanLineGridData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "";
                strWhere += Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();
                string[] columnName = { "StageCode", "StageSequance", "AcceptanceStandard", "ApplicableSpecification", "InspectionExtent", "RevNo", "StageStatus", "Remarks", "LineId", };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_IPI_TEST_PLAN_LINEDETAILS
                                (
                               StartIndex,
                               EndIndex,
                               strSortOrder,
                               strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                //Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.StageCode),
                                Convert.ToString(uc.StageSequance),
                                Convert.ToString(uc.AcceptanceStandard),
                                Convert.ToString(uc.ApplicableSpecification),
                                Convert.ToString(uc.InspectionExtent.Split('-')[1].Trim()),
                                string.IsNullOrEmpty(Convert.ToString(uc.RevNo))?Convert.ToString(uc.RevNo):"R"+Convert.ToString(uc.RevNo),
                                 Convert.ToString(uc.StageStatus),
                                Convert.ToString(uc.Remarks),
                                "<nobr><center style=\"display:inline;\">" + 
                                (new TestPlanController()).MaintainIPIStagetypewiseGridButton(uc.HeaderId,uc.LineId,uc.QualityProject,uc.Project.Split('-')[0].Trim(),uc.BU.Split('-')[0].Trim(),uc.Location.Split('-')[0].Trim(),uc.SeamNo,uc.RevNo,uc.StageCode.Split('-')[0].Trim(),uc.StageCode,uc.StageSequance,false,(new TestPlanController()).StageTypeAction(uc.HeaderId, uc.StageCode.Split('-')[0].Trim(), uc.BU.Split('-')[0].Trim(), uc.Location.Split('-')[0].Trim(),true),true,uc.StageType)
                                 +"<a class='iconspace btn btn-xs' href='javascript:void(0)' title='Timeline' onclick=ShowTimeline('/IPI/ApproveTestPlan/ShowTimeline?HeaderID="+Convert.ToInt32(uc.HeaderId)+"&LineId="+Convert.ToInt32(uc.LineId)+"');><i style='' class='iconspace fa fa-clock-o'></i></a></center></nobr>"
                           }).ToList();


                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ApproveTestPlan(int strHeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objQMS020 = db.QMS020.Where(x => x.HeaderId == strHeaderId).FirstOrDefault();
                var stageStatus = clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue();
                var lstTPStages = db.QMS021.Where(x => x.HeaderId == strHeaderId && !x.StageStatus.Equals(stageStatus, StringComparison.OrdinalIgnoreCase)).ToList();
                if (lstTPStages.Count > 0)
                {
                    //objResponseMsg = TransferDeatils(objQMS020.HeaderId, lstTPStages);
                    QMS030 objQMS030 = db.QMS030.Where(x => x.QualityProject == objQMS020.QualityProject && x.SeamNo == objQMS020.SeamNo && x.Project == objQMS020.Project && x.BU == objQMS020.BU && x.Location == objQMS020.Location).FirstOrDefault();
                    if (objQMS030 != null)
                    {
                        objResponseMsg = InsertUpdatInICL(objQMS020, objQMS030, true, lstTPStages);
                    }
                    else
                    {
                        QMS030 objDesQMS030 = new QMS030();
                        objResponseMsg = InsertUpdatInICL(objQMS020, objDesQMS030, false, lstTPStages);
                    }
                }
                else
                {
                    objResponseMsg.Key = true;
                }
                if (objResponseMsg.Key == true)
                {
                    db.SP_IPI_TEST_PLAN_APPROVE(strHeaderId, "", objClsLoginInfo.UserName);

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.WE3.GetStringValue(), objQMS020.Project, objQMS020.BU, objQMS020.Location, "Test Plan for Seam: " + objQMS020.SeamNo + " of Project: " + objQMS020.QualityProject + "  has been Approved", clsImplementationEnum.NotificationType.Information.GetStringValue());
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Approve.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApproveByApproverSelected(string strHeaderIds)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
            List<string> lstTPNotSendForApprove = new List<string>();
            List<string> lstTPAttended = new List<string>();
            QMS020 objQMS020 = new QMS020();
            try
            {
                for (int i = 0; i < arrayHeaderIds.Length; i++)
                {
                    int headerId = Convert.ToInt32(arrayHeaderIds[i]);
                    objQMS020 = db.QMS020.Where(x => x.HeaderId == headerId).FirstOrDefault();
                    if (objQMS020.Status == clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue())
                    {
                        var stageStatus = clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue();
                        var lstTPStages = db.QMS021.Where(x => x.HeaderId == headerId && !x.StageStatus.Equals(stageStatus, StringComparison.OrdinalIgnoreCase)).ToList();
                        if (lstTPStages.Count > 0)
                        {
                            QMS030 objQMS030 = db.QMS030.Where(x => x.QualityProject == objQMS020.QualityProject && x.SeamNo == objQMS020.SeamNo && x.Project == objQMS020.Project && x.BU == objQMS020.BU && x.Location == objQMS020.Location).FirstOrDefault();
                            if (objQMS030 != null)
                            {
                                objResponseMsg = InsertUpdatInICL(objQMS020, objQMS030, true, lstTPStages);
                            }
                            else
                            {
                                QMS030 objDesQMS030 = new QMS030();
                                objResponseMsg = InsertUpdatInICL(objQMS020, objDesQMS030, false, lstTPStages);
                            }
                        }
                        else
                        {
                            objResponseMsg.Key = true;
                        }
                        if (objResponseMsg.Key == true)
                        {
                            db.SP_IPI_TEST_PLAN_APPROVE(headerId, "", objClsLoginInfo.UserName);

                            //#region Send Notification
                            //(new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.WE3.GetStringValue(), objQMS020.Project, objQMS020.BU, objQMS020.Location, "Test Plan for Seam: " + objQMS020.SeamNo + " of Project: " + objQMS020.QualityProject + "  has been Approved", clsImplementationEnum.NotificationType.Information.GetStringValue());
                            //#endregion

                            objResponseMsg.Key = true;
                            lstTPAttended.Add(headerId.ToString());
                            //objResponseMsg.Value = clsImplementationMessage.CommonMessages.Approve.ToString();
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = true;
                        lstTPNotSendForApprove.Add(headerId.ToString());
                    }
                }
                objResponseMsg.Value = string.Format("{0} Test Plan(s) Approved successfully", lstTPAttended.Count());

                if (objResponseMsg.Key)
                {
                    #region Send Notification
                    QMS030 objQMS030 = db.QMS030.FirstOrDefault(c => c.QualityProject == objQMS020.QualityProject && c.Project == objQMS020.Project && c.BU == objQMS020.BU && c.Location == objQMS020.Location && c.SeamNo == objQMS020.SeamNo);
                    if (objQMS030 != null)
                    {
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), objQMS030.Project, objQMS030.BU, objQMS030.Location, "Test Plan has been approved for project " + objQMS030.QualityProject + ", Kindly maintain Seam ICL", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/MaintainSeamICL/AddHeader?HeaderID=" + objQMS030.HeaderId);
                    }
                    #endregion
                }

                if (lstTPNotSendForApprove.Any())
                {
                    objResponseMsg.Remarks = string.Format("Test Plan(s) {0} have been skipped as Test Plan(s) {0} has been already attended", lstTPNotSendForApprove.Count());
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReturnTestPlan(string strHeaderIds, string returnremark)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                List<string> lstTPNotSendForApprove = new List<string>();
                List<string> lstTPAttended = new List<string>();
                if (!string.IsNullOrEmpty(strHeaderIds))
                {
                    int[] headerIds = Array.ConvertAll(strHeaderIds.Split(','), i => Convert.ToInt32(i));
                    foreach (var headerId in headerIds)
                    {
                        var objQMS020 = db.QMS020.Where(i => i.HeaderId == headerId).FirstOrDefault();
                        if (objQMS020 != null)
                        {
                            if (objQMS020.Status == clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue())
                            {
                                objQMS020.Status = clsImplementationEnum.QualityIdHeaderStatus.Returned.GetStringValue();
                                objQMS020.ReturnedBy = objClsLoginInfo.UserName;
                                objQMS020.ReturnedOn = DateTime.Now;
                                objQMS020.ReturnRemark = returnremark;
                                db.SaveChanges();
                                #region Send Notification
                                (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.WE3.GetStringValue(), objQMS020.Project, objQMS020.BU, objQMS020.Location, "Test Plan for Seam: " + objQMS020.SeamNo + " of Project: " + objQMS020.QualityProject + "  has been Returned", clsImplementationEnum.NotificationType.Information.GetStringValue());
                                #endregion
                                lstTPAttended.Add(headerId.ToString());
                            }
                            else
                            {
                                lstTPNotSendForApprove.Add(headerId.ToString());
                            }
                        }
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = string.Format("{0} Test Plan(s) Returned successfully", lstTPAttended.Count());
                    if (lstTPNotSendForApprove.Any())
                    {
                        objResponseMsg.Remarks = string.Format("Test Plan(s) {0} have been skipped as Test Plan(s) {0} has been already attended", lstTPNotSendForApprove.Count());
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        public class ResponceMsgWithHeaderID : clsHelper.ResponseMsg
        {
            public int HeaderId;
        }
        public clsHelper.ResponseMsgWithStatus InsertUpdatInICL(QMS020 objSrcQMS020, QMS030 objDesQMS030, bool IsEdited, List<QMS021> lstQMS21)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                QMS025 objQMS025 = new QMS025();
                QMS021 objSRCQMS021 = db.QMS021.Where(x => x.HeaderId == objSrcQMS020.HeaderId).FirstOrDefault();
                //objDesQMS030.BU = objSrcQMS020.BU;
                //objDesQMS030.QualityProject = objSrcQMS020.QualityProject;
                //objDesQMS030.Project = objSrcQMS020.Project;
                //objDesQMS030.SeamNo = objSrcQMS020.SeamNo;
                //objDesQMS030.TPRevNo = objSrcQMS020.RevNo;
                //objDesQMS030.Location = objSrcQMS020.Location;
                //objDesQMS030.RevNo = 0;
                //objDesQMS030.PTCApplicable = objSrcQMS020.PTCApplicable;
                //objDesQMS030.PTCNumber = objSrcQMS020.PTCNumber;
                //objDesQMS030.CreatedBy = objSrcQMS020.CreatedBy;
                //objDesQMS030.CreatedOn = objSrcQMS020.CreatedOn;
                //objDesQMS030.Status = clsImplementationEnum.TestPlanStatus.Draft.GetStringValue();

                //if (IsEdited)
                //{
                //    if (objDesQMS030.Status == clsImplementationEnum.TestPlanStatus.Approved.GetStringValue())
                //    {
                //        objDesQMS030.RevNo = Convert.ToInt32(objDesQMS030.RevNo) + 1;
                //    }
                //    db.SaveChanges();
                //    objResponseMsg.Key = true;
                //    objResponseMsg.Value = clsImplementationMessage.CTQMessages.Update.ToString();
                //}
                if (!IsEdited)
                {
                    objDesQMS030.BU = objSrcQMS020.BU;
                    objDesQMS030.QualityProject = objSrcQMS020.QualityProject;
                    objDesQMS030.Project = objSrcQMS020.Project;
                    objDesQMS030.SeamNo = objSrcQMS020.SeamNo;
                    objDesQMS030.TPRevNo = objSrcQMS020.RevNo;
                    objDesQMS030.Location = objSrcQMS020.Location;
                    objDesQMS030.RevNo = 0;
                    objDesQMS030.PTCApplicable = objSrcQMS020.PTCApplicable;
                    objDesQMS030.PTCNumber = objSrcQMS020.PTCNumber;
                    objDesQMS030.Status = clsImplementationEnum.TestPlanStatus.Draft.GetStringValue();
                    objDesQMS030.CreatedBy = objClsLoginInfo.UserName;
                    objDesQMS030.CreatedOn = DateTime.Now;
                    db.QMS030.Add(objDesQMS030);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CTQMessages.Insert.ToString();
                }

                QMS030 objQMS030 = new QMS030();
                QMS031 objDesQMS031 = new QMS031();
                List<QMS025> lstQMS025 = new List<QMS025>();
                var objQMS020 = db.QMS020.Where(x => x.HeaderId == objSrcQMS020.HeaderId).FirstOrDefault();

                if (!string.IsNullOrEmpty(objQMS020.QualityProject) && !string.IsNullOrEmpty(objQMS020.SeamNo) && !string.IsNullOrEmpty(objQMS020.Location))
                {
                    objQMS030 = db.QMS030.Where(i => i.QualityProject.Equals(objQMS020.QualityProject, StringComparison.OrdinalIgnoreCase) && i.SeamNo.Equals(objQMS020.SeamNo, StringComparison.OrdinalIgnoreCase) && i.BU == objQMS020.BU && i.Project == objQMS020.Project && i.Location.Equals(objQMS020.Location, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                    bool isUpdated = false;

                    foreach (var item in lstQMS21)
                    {
                        bool isStageLinkedInLTFPS = (new TestPlanController()).isStageLinkedInLTFPS(item.LineId);
                        if (!isStageLinkedInLTFPS)
                        {
                            objDesQMS031 = db.QMS031.Where(i => i.HeaderId == objQMS030.HeaderId && i.StageCode.Equals(item.StageCode, StringComparison.OrdinalIgnoreCase) && i.StageSequance == item.StageSequance).FirstOrDefault();
                            if (objDesQMS031 != null)
                            {
                                objDesQMS031.StageStatus = item.StageStatus;
                                objDesQMS031.AcceptanceStandard = item.AcceptanceStandard;
                                objDesQMS031.ApplicableSpecification = item.ApplicableSpecification;
                                objDesQMS031.InspectionExtent = item.InspectionExtent;
                                objDesQMS031.Remarks = item.Remarks;

                                //if (string.Equals(objQMS030.Status, clsImplementationEnum.TestPlanStatus.Approved.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                                //{
                                //    objQMS030.TPRevNo = objQMS020.RevNo;
                                //    objQMS030.RevNo = (objQMS030.RevNo) + 1;
                                //}
                                //objQMS030.Status = clsImplementationEnum.TestPlanStatus.Draft.GetStringValue();
                                isUpdated = true;
                                db.SaveChanges();
                            }
                            else
                            {
                                if (item.StageStatus != clsImplementationEnum.TestPlanStatus.Deleted.GetStringValue())
                                {
                                    objDesQMS031 = new QMS031();
                                    objDesQMS031.HeaderId = objQMS030.HeaderId;
                                    objDesQMS031.QualityProject = objQMS030.QualityProject;
                                    objDesQMS031.Project = objQMS030.Project;
                                    objDesQMS031.BU = objQMS030.BU;
                                    objDesQMS031.Location = objQMS030.Location;
                                    objDesQMS031.SeamNo = objQMS030.SeamNo;
                                    objDesQMS031.StageCode = item.StageCode;
                                    objDesQMS031.StageSequance = item.StageSequance;
                                    objDesQMS031.StageStatus = clsImplementationEnum.TestPlanStatus.Added.GetStringValue();
                                    objDesQMS031.AcceptanceStandard = item.AcceptanceStandard;
                                    objDesQMS031.ApplicableSpecification = item.ApplicableSpecification;
                                    objDesQMS031.InspectionExtent = item.InspectionExtent;
                                    objDesQMS031.Remarks = item.Remarks;
                                    objDesQMS031.CreatedBy = objClsLoginInfo.UserName;
                                    objDesQMS031.CreatedOn = DateTime.Now;
                                    //objQMS030.TPRevNo = objQMS020.RevNo;
                                    db.QMS031.Add(objDesQMS031);
                                    //if (string.Equals(objQMS030.Status, clsImplementationEnum.TestPlanStatus.Approved.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                                    //{
                                    //    objQMS030.Status = clsImplementationEnum.TestPlanStatus.Draft.GetStringValue();
                                    //    objQMS030.RevNo = (objQMS030.RevNo) + 1;
                                    //}
                                    isUpdated = true;
                                    db.SaveChanges();

                                }

                            }
                        }
                        var lstStageTypes = Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.InsertTPStagesintoTPI.GetStringValue()).Split(',').ToArray();
                        if (item.StageStatus != clsImplementationEnum.TestPlanStatus.Deleted.GetStringValue())
                        {
                            var isStageApplicableforTPI = db.QMS002.Any(i => i.BU.Equals(objQMS020.BU, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(objQMS020.Location, StringComparison.OrdinalIgnoreCase) && i.StageCode == item.StageCode && lstStageTypes.Contains(i.StageType));
                            if (isStageApplicableforTPI)
                            {
                                objQMS025 = db.QMS025.Where(a => a.Location == item.Location && a.BU == item.BU && a.Project == item.Project && a.QualityProject == item.QualityProject && a.Stage == item.StageCode).FirstOrDefault();
                                if (objQMS025 == null && !lstQMS025.Where(a => a.Location == item.Location && a.BU == item.BU && a.Project == item.Project && a.QualityProject == item.QualityProject && a.Stage == item.StageCode).Any())
                                {
                                    objQMS025 = new QMS025();
                                    objQMS025.Project = item.Project;
                                    objQMS025.QualityProject = item.QualityProject;
                                    objQMS025.Location = item.Location;
                                    objQMS025.BU = item.BU;
                                    objQMS025.Stage = item.StageCode;
                                    objQMS025.CreatedBy = objClsLoginInfo.UserName;
                                    objQMS025.CreatedOn = DateTime.Now;
                                    lstQMS025.Add(objQMS025);
                                }
                            }
                        }
                    }
                    if (lstQMS025.Count() > 0)
                    {
                        db.QMS025.AddRange(lstQMS025);
                    }

                    if (isUpdated)
                    {
                        if (string.Equals(objQMS030.Status, clsImplementationEnum.TestPlanStatus.Approved.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                        {
                            objQMS030.RevNo = (objQMS030.RevNo) + 1;
                        }
                        objQMS030.Status = clsImplementationEnum.TestPlanStatus.Draft.GetStringValue();
                        objQMS030.TPRevNo = objQMS020.RevNo;
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objDesQMS030.HeaderId;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return objResponseMsg;
        }
        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            if (LineId > 0)
            {
                QMS021 objQMS021 = db.QMS021.Where(x => x.LineId == LineId).FirstOrDefault();
                model.ApprovedBy = objQMS021.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objQMS021.ApprovedBy) : null;
                model.ApprovedOn = objQMS021.ApprovedOn;
                model.SubmittedBy = objQMS021.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objQMS021.SubmittedBy) : null;
                model.SubmittedOn = objQMS021.SubmittedOn;
                model.CreatedBy = objQMS021.CreatedBy != null ? Manager.GetUserNameFromPsNo(objQMS021.CreatedBy) : null;
                model.CreatedOn = objQMS021.CreatedOn;
                model.EditedBy = objQMS021.EditedBy != null ? Manager.GetUserNameFromPsNo(objQMS021.EditedBy) : null;
                model.EditedOn = objQMS021.EditedOn;
            }
            else
            {
                QMS020 objQMS020 = db.QMS020.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.ApprovedBy = objQMS020.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objQMS020.ApprovedBy) : null;
                model.ApprovedOn = objQMS020.ApprovedOn;
                model.SubmittedBy = objQMS020.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objQMS020.SubmittedBy) : null;
                model.SubmittedOn = objQMS020.SubmittedOn;
                model.CreatedBy = objQMS020.CreatedBy != null ? Manager.GetUserNameFromPsNo(objQMS020.CreatedBy) : null;
                model.CreatedOn = objQMS020.CreatedOn;
                model.EditedBy = objQMS020.EditedBy != null ? Manager.GetUserNameFromPsNo(objQMS020.EditedBy) : null;
                model.EditedOn = objQMS020.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress2.cshtml", model);
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.APPROVEINDEX.GetStringValue())
                {
                    List<SP_IPI_GET_TESTPLAN_INDEXRESULT_Result> lst = db.SP_IPI_GET_TESTPLAN_INDEXRESULT(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    List<SP_IPI_TEST_PLAN_HEADER_DETAILS_Result> lst = db.SP_IPI_TEST_PLAN_HEADER_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      SeamNo = li.SeamNo,
                                      QualityId = li.QualityId,
                                      QualityIdRev = li.QualityIdRev,
                                      PTCApplicable = li.PTCApplicable,
                                      PTCNumber = li.PTCApplicable,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn,
                                      RevNo = li.RevNo,
                                      Status = li.Status
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else
                {
                    var lst = db.SP_IPI_TEST_PLAN_LINEDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      SeamNo = li.SeamNo,
                                      RevNo = li.RevNo,
                                      StageCode = li.StageCode,
                                      StageSequance = li.StageSequance,
                                      StageStatus = li.StageStatus,
                                      AcceptanceStandard = li.AcceptanceStandard,
                                      ApplicableSpecification = li.ApplicableSpecification,
                                      InspectionExtent = li.InspectionExtent,
                                      Remarks = li.Remarks,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }


    }
}