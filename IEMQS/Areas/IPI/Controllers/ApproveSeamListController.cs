﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.IPI.Controllers
{
    public class ApproveSeamListController : clsBase
    {
        #region Header
        [SessionExpireFilter]
        public ActionResult Index(string Project)
        {
            ViewBag.chkProject = Project;
            return View();
        }
        public ActionResult GetHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;            
            return PartialView("_GetHeaderGridDataPartial");
        }
        public ActionResult LoadHeaderData(JQueryDataTableParamModel param, string status, string Project)
        {
            try
            {
                if (Project != null && Project != "")
                {
                    int? StartIndex = param.iDisplayStart + 1;
                    int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                    var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                    string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                    string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                    string whereCondition = "1=1";
                    string strSortOrder = string.Empty;
                    whereCondition += " and Project='" + Project.ToString() + "'";
                    // whereCondition += " and CreatedBy = " + objClsLoginInfo.UserName;
                    if (status.ToUpper() == "PENDING")
                    {
                        whereCondition += " and status in('" + clsImplementationEnum.SeamListStatus.SentForApproval.GetStringValue() + "')";
                    }
                    string[] columnName = { "qms.QualityProject", "qms.Project+'-'+com1.t_dsca", "qms.ContractNo+'-'+com4.t_desc", "com6.t_bpid+'-'+com6.t_nama", "Status", "SeamListNo" };
                    //whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                    if (!string.IsNullOrEmpty(param.sSearch))
                        whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                    else
                        whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);

                    if (!string.IsNullOrWhiteSpace(sortColumnName))
                    {
                        strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                    }

                    //var lstHeader = db.SP_IPI_GETSEAMLISTHEADER(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                    var lstHeader1 = db.SP_IPI_GETSEAMLISTHEADER(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                    lstHeader1 = lstHeader1.Where(x => x.Project.Contains(Project)).ToList();

                    int? totalRecords = lstHeader1.Select(i => i.TotalCount).FirstOrDefault();
                    var res = from h in lstHeader1
                              select new[] {
                           Convert.ToString(h.HeaderId),
                           h.Project,
                           Convert.ToString(h.HeaderId),
                           Convert.ToString( h.ContractNo),
                           Convert.ToString( h.Customer),
                           Convert.ToString( h.QualityProject),
                           Convert.ToString( h.Location),
                           Convert.ToString( h.SeamListNo),
                           Convert.ToString( "R"+h.SeamListRev),
                           Convert.ToString(h.Status),
                           "<center style=\"white-space: nowrap;\"><a href=\""+WebsiteURL+"/IPI/ApproveSeamList/Viewlines?id="+h.HeaderId+" &Project="+Project+" \" Title=\"View lines\"><i class=\"fa fa-eye\"></i></a><a class='btn btn-xs' href='javascript:void(0)' title='Show Timeline' onclick=ShowTimeline('/IPI/MaintainSeamList/ShowTimeline?HeaderID=" + h.HeaderId + "');><i style='margin-left:-2px;' class='fa fa-clock-o'></i></a><a><i onclick=ViewHistoryData('"+ h.HeaderId +"'); class='fa fa-history' title='History Record' style='cursor: pointer; margin-left: -5px;'></i></a></center>"
                    };
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                        strSortOrder = strSortOrder,
                        whereCondition = whereCondition
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    int? StartIndex = param.iDisplayStart + 1;
                    int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                    var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                    string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                    string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                    string whereCondition = "1=1";
                    string strSortOrder = string.Empty;
                    // whereCondition += " and CreatedBy = " + objClsLoginInfo.UserName;
                    if (status.ToUpper() == "PENDING")
                    {
                        whereCondition += " and status in('" + clsImplementationEnum.SeamListStatus.SentForApproval.GetStringValue() + "')";
                    }
                    string[] columnName = { "qms.QualityProject", "qms.Project+'-'+com1.t_dsca", "qms.ContractNo+'-'+com4.t_desc", "com6.t_bpid+'-'+com6.t_nama", "Status", "SeamListNo" };
                    //whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                    if (!string.IsNullOrEmpty(param.sSearch))
                        whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                    else
                        whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);

                    if (!string.IsNullOrWhiteSpace(sortColumnName))
                    {
                        strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                    }

                    var lstHeader = db.SP_IPI_GETSEAMLISTHEADER(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                    int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                    var res = from h in lstHeader
                              select new[] {
                           Convert.ToString(h.HeaderId),
                           h.Project,
                           Convert.ToString(h.HeaderId),
                           Convert.ToString( h.ContractNo),
                           Convert.ToString( h.Customer),
                           Convert.ToString( h.QualityProject),
                           Convert.ToString( h.Location),
                           Convert.ToString( h.SeamListNo),
                           Convert.ToString( "R"+h.SeamListRev),
                           Convert.ToString(h.Status),
                           "<center style=\"white-space: nowrap;\"><a href=\""+WebsiteURL+"/IPI/ApproveSeamList/Viewlines?id="+h.HeaderId+"\" Title=\"View lines\"><i class=\"fa fa-eye\"></i></a><a class='btn btn-xs' href='javascript:void(0)' title='Show Timeline' onclick=ShowTimeline('/IPI/MaintainSeamList/ShowTimeline?HeaderID=" + h.HeaderId + "');><i style='margin-left:-2px;' class='fa fa-clock-o'></i></a><a><i onclick=ViewHistoryData('"+ h.HeaderId +"'); class='fa fa-history' title='History Record' style='cursor: pointer; margin-left: -5px;'></i></a></center>"
                    };
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                        strSortOrder = strSortOrder,
                        whereCondition = whereCondition
                    }, JsonRequestBehavior.AllowGet);
                }
                   
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }


        #endregion

        #region Lines

        public ActionResult Viewlines(int id,string Project)
        {
            QMS011 objQMS011 = db.QMS011.FirstOrDefault(x => x.HeaderId == id);
            objQMS011.Project = Manager.GetProjectAndDescription(objQMS011.Project);
            objQMS011.Customer = db.COM006.Where(x => x.t_bpid == objQMS011.Customer).Select(x => x.t_bpid + "-" + x.t_nama).FirstOrDefault();
            objQMS011.ContractNo = Manager.GetContractorAndDescription(objQMS011.ContractNo);
            ViewBag.QualityProject = string.IsNullOrWhiteSpace(objQMS011.Location) ? objQMS011.QualityProject : objQMS011.QualityProject + " (" + objQMS011.Location + ")";
            ViewBag.chkProject = Project;
            return View(objQMS011);
        }

        [HttpPost]
        public ActionResult Approve(int id,string Project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS011 objQMS011 = db.QMS011.FirstOrDefault(c => c.HeaderId == id);
                if (objQMS011 != null && objQMS011.Status == clsImplementationEnum.SeamListStatus.SentForApproval.GetStringValue())
                {
                    db.SP_IPI_SEAMLISTAPPROVE(id, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.QCPMessages.Approve.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "This seam list has been already attended. Please refresh the page.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            ViewBag.chkProject = Project;
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ApproveSelectedHeader(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
                for (int i = 0; i < arrayHeaderIds.Length; i++)
                {
                    int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                    QMS011 objQMS011 = db.QMS011.FirstOrDefault(c => c.HeaderId == HeaderId);
                    if (objQMS011 != null && objQMS011.Status == clsImplementationEnum.SeamListStatus.SentForApproval.GetStringValue())
                    {
                        db.SP_IPI_SEAMLISTAPPROVE(HeaderId, objClsLoginInfo.UserName);
                    }                    
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.QCPMessages.Approve.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> Return(int id, string buHeadComments,string Project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS011 objQMS011 = await db.QMS011.FirstOrDefaultAsync(x => x.HeaderId == id);
                if (objQMS011 != null && objQMS011.Status != clsImplementationEnum.SeamListStatus.SentForApproval.GetStringValue())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "This seam list has been already attended. Please refresh the page.";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                objQMS011.Comments =Convert.ToString(buHeadComments);
                objQMS011.Status = clsImplementationEnum.IMBStatus.Returned.GetStringValue();
                objQMS011.ReturnedBy = objClsLoginInfo.UserName;
                objQMS011.ReturnedOn = DateTime.Now;
                objQMS011.SubmittedBy = null;
                objQMS011.SubmittedOn = null;
                objQMS011.ApprovedBy = null;
                objQMS011.ApprovedOn = null;
                await db.SaveChangesAsync();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.QCPMessages.Return.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            ViewBag.chkProject = Project;
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadLinesData(JQueryDataTableParamModel param, int id)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;

                string whereCondition = "1=1 and qms.HeaderId = " + id + " ";
                string[] columnName = { "qms.SeamNo", "qms.SeamDescription", "qms.Position", "qms.Thickness", "qms.Material", "qms.ViewDrawing", "qms.WeldType", "qms.SeamLengthArea", "qms.WEPDetail", "qms.Remarks", "qms.AssemblyNo" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                else
                {
                    strSortOrder = " Order By SubmittedOn desc ";
                }

                var lstLines = db.SP_IPI_GETSEAMLISTLINES(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();

                var res = from c in lstLines
                          select new[] {
                                        Convert.ToString(c.ROW_NO),
                                        Convert.ToString(c.HeaderId),
                                        Convert.ToString(c.LineId),
                                        c.WeldType,
                                        c.SeamDescription,
                                        c.ManualSeamNo,
                                        c.SeamNo,
                                        "R"+c.SeamRev,
                                        c.Position,
                                        c.AssemblyNo,
                                        c.Thickness,
                                        c.ThicknessDefault,
                                        c.Material,
                                        c.Unit,
                                        c.WEPDetail,
                                        c.ViewDrawing,
                                        Convert.ToString(c.SeamLengthArea),
                                        c.Remarks,
                                        Manager.generateSeamHistoryButton(c.Project,c.QualityProject, c.SeamNo,"Seam Details")
                                       + " " + (c.SeamDescription == clsImplementationEnum.AutoCJCSeamType.RA.GetStringValue()? HTMLActionString(c.LineId, "","Seam Quantity", "Seam Quantity", "fa fa-reorder","LoadSeamQuantityPartial("+c.LineId+")"):HTMLActionString(c.LineId, "","Seam Quantity", "Seam Quantity", "fa fa-reorder disabledicon",""))
                          };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""

                }, JsonRequestBehavior.AllowGet);
            }
        }
        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", bool isVisible = true, bool atagrequired = false)
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick=" + onClickMethod : "";

            if (!string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
            {
                if (string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue()))
                {
                    htmlControl = "";// "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }
                else
                {
                    if (isVisible)
                    {
                        htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer; margin-right: 3px; margin-left: 0px;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                    }
                    else
                    {
                        htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer; margin-right: 3px; margin-left: 0px; display:none;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                    }
                    if (atagrequired)
                    {
                        htmlControl = "<a>" + htmlControl + "</a>";
                    }
                }
            }

            return htmlControl;
        }

        #endregion

        [HttpPost]
        public ActionResult GetSeamDetails(string Project,string QualityProject, string seamNo)
        {
            SeamDetailsModel objSeamDetailsModel = new SeamDetailsModel();
            QMS031 objQMS031 = db.QMS031.Where(x => x.SeamNo.Trim() == seamNo.Trim() && x.QualityProject.Trim() == QualityProject.Trim() && x.Project.Trim() == Project.Trim()).FirstOrDefault();
            if(objQMS031 != null)
            {
                objSeamDetailsModel.QualityProject = objQMS031.QualityProject;
                objSeamDetailsModel.Project = objQMS031.Project;
                objSeamDetailsModel.BU = objQMS031.BU;
                objSeamDetailsModel.Location = objQMS031.Location;
                objSeamDetailsModel.SeamNo = objQMS031.SeamNo;
               // objSeamDetailsModel.StageCode = objQMS031.StageCode;
               // objSeamDetailsModel.StageSequence = Convert.ToInt32(objQMS031.StageSequance);
                //objSeamDetailsModel.tableId = (Convert.ToBoolean(db.Database.SqlQuery<bool>(@"select  dbo.CHECK_AVAIALBLE_NDE_STAGE('"+ objQMS031.StageCode + "','"+ objQMS031 .BU+ "','"+ objQMS031 .Location+ "')"))?60:50);
            }
            return Json(objSeamDetailsModel,JsonRequestBehavior.AllowGet);
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_IPI_GETSEAMLISTHEADER(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      ContractNo = li.ContractNo,
                                      Customer = li.Customer,
                                      QualityProject = li.QualityProject,
                                      SeamListNo = li.SeamListNo,
                                      SeamListRev = li.SeamListRev,
                                      Status = li.Status,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_IPI_GETSEAMLISTLINES(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from li in lst
                                  select new
                                  {
                                      WeldType = li.WeldType,
                                      SeamCategory = li.SeamDescription,
                                      Number = li.ManualSeamNo,
                                      Revision = "R" + li.SeamRev,
                                      Position = li.Position,
                                      AssemblyNumber = li.AssemblyNo,
                                      Thickness = li.Thickness,
                                      WEPDetail = li.WEPDetail,
                                      ViewDrawing = li.ViewDrawing,
                                      SeamLengthArea = li.SeamLengthArea.ToString(),
                                      Remarks = li.Remarks,
                                      Action = "Edit" 
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

    }
    public class SeamDetailsModel
    {
        public string QualityProject { get; set; }
        public string Project { get; set; }
        public string BU { get; set; }
        public string Location { get; set; }
        public string SeamNo { get; set; }
        //public string StageCode { get; set; }
        //public int StageSequence { get; set; }
        //public int tableId { get; set; }
    }
}