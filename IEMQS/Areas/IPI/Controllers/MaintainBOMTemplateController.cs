﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.IPI.Controllers
{
    public class MaintainBOMTemplateController : clsBase
    {

        #region Utility
        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", bool isVisible = true)
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            //if (!string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
            //{
            //    htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            //}
            if (isVisible)
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            }
            else
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer; display:none' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            }

            return htmlControl;
        }

        public static string GenerateTextboxFor(int rowId, string status, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control";

            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";
            if (string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
            {
                htmlControl = "<input disabled type='text' id='" + inputID + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + "  />";
            }
            else
            {
                htmlControl = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + "  />";
            }

            return htmlControl;
        }
        #endregion

        #region Header
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult loadHeaderDataTable(JQueryDataTableParamModel param, string status)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;

                string whereCondition = "1=1";
                whereCondition += " and CreatedBy = " + objClsLoginInfo.UserName;
                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and status in('" + clsImplementationEnum.IMBStatus.Draft.GetStringValue() + "')";
                }

                string[] columnName = { "bom.Project+'-'+com1.t_dsca", "com6.t_bpid+'-'+com6.t_nama", "bom.ContractNo+'-'+com4.t_desc", "TemplateNo", "bom.RevNo" };
                // whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }


                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstHeader = db.SP_IPI_BOMTEMPLATEHEADER(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from h in lstHeader
                          select new[] {
                           h.TemplateNo,
                           Convert.ToString(h.HeaderId),
                           Convert.ToString( h.ContractNo),
                           Convert.ToString( h.Project),
                           Convert.ToString( h.Customer),
                           Convert.ToString( "R"+h.RevNo),
                           Convert.ToString(h.Status),
                           ""
                          };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetHeaderGridDataPartial");
        }
        [SessionExpireFilter]
        public ActionResult AddHeader(int? headerId)
        {
            BOM001 objBOM001 = new BOM001();
            try
            {
                if (headerId != null && headerId > 0)
                {
                    objBOM001 = db.BOM001.FirstOrDefault(x => x.HeaderId == headerId && x.CreatedBy == objClsLoginInfo.UserName);
                    ViewBag.Customer = Manager.GetCustomerCodeAndNameByProject(objBOM001.Project);
                    ViewBag.Contract = Manager.GetContractProjectWise(objBOM001.Project);
                    ViewBag.Project = db.Database.SqlQuery<string>("SELECT dbo.GET_PROJECTDESC_BY_CODE('" + objBOM001.Project + "')").FirstOrDefault();
                    TempData["headerid"] = headerId;
                    if (objBOM001 == null)
                    {
                        return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                    }
                    ViewBag.Action = "edit";
                }
                else
                {
                    objBOM001.HeaderId = 0;
                    objBOM001.Status = clsImplementationEnum.BOMTemplateStatus.Draft.GetStringValue();
                    objBOM001.RevNo = 0;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View(objBOM001);
        }
        [HttpPost]
        public ActionResult GetParentiem(string parent, string child, int headerId, int Level)

        {   //get project and BU by Qms project

            if (!string.IsNullOrWhiteSpace(child))
            {
                BUWiseDropdown objProjectDataModel = new BUWiseDropdown();
                bool headerexist = db.BOM002.Any(m => m.HeaderId == headerId);
                if (headerexist)
                {

                    bool Getparent = db.BOM002.Any(m => m.HeaderId == headerId && m.ParentItemDescription == parent && m.ChildItemDescription == child && m.Level == Level);
                    if (!Getparent)
                    {
                        objProjectDataModel.parentStatus = true;
                        return Json(objProjectDataModel, JsonRequestBehavior.AllowGet);

                    }
                    else
                    {
                        objProjectDataModel.parentStatus = false;
                        return Json(objProjectDataModel, JsonRequestBehavior.AllowGet);
                    }




                }
                else
                {
                    objProjectDataModel.parentStatus = true;
                    //objProjectDataModel.childStatus = true;
                    return Json(objProjectDataModel, JsonRequestBehavior.AllowGet);
                }


            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult Getchildlist(int headerid, int level)
        {
            BUWiseDropdown objProjectDataModel = new BUWiseDropdown();
            level = level - 1;
            var lstparent = db.BOM002.Where(i => i.HeaderId == headerid && i.Level == level).Select(i => new CategoryData { Value = i.ChildItemDescription, Code = i.ChildItemDescription, CategoryDescription = i.ChildItemDescription }).ToList();
            if (lstparent.Count > 0)
            {
                objProjectDataModel.Parentlist = lstparent.Any() ? lstparent : null;
                objProjectDataModel.Value = "true";
                return Json(objProjectDataModel, JsonRequestBehavior.AllowGet);
            }
            else
            {
                objProjectDataModel.Value = "false";
                objProjectDataModel.Parentlist = lstparent.Any() ? lstparent : null;
                return Json(objProjectDataModel, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]
        [HttpPost]
        public JsonResult CreateRevision(int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (HeaderId > 0)
                {
                    BOM001 objBOM001 = db.BOM001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                    objBOM001.Status = clsImplementationEnum.PAMStatus.Draft.GetStringValue();
                    objBOM001.RevNo += 1;
                    objBOM001.EditedBy = objClsLoginInfo.UserName;
                    objBOM001.EditedOn = DateTime.Now;

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revision.ToString();
                    objResponseMsg.HeaderId = HeaderId;
                    objResponseMsg.Status = objBOM001.Status;
                    objResponseMsg.RevNo = "R" + objBOM001.RevNo;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
                throw;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        public class BUWiseDropdown
        {
            public bool parentStatus { get; set; }
            public bool childStatus { get; set; }
            public bool Key;
            public string Value;
            public List<CategoryData> Parentlist { get; set; }


        }
        public async Task<ActionResult> EditHeader(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                BOM001 objBOM001 = await db.BOM001.FirstOrDefaultAsync(x => x.HeaderId == headerId);
                if (objBOM001 != null)
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = objBOM001.Status;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Document Not Found";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
                //throw;
            }
            return Json(objResponseMsg);
        }
        [HttpPost]
        public ActionResult SaveHeader(FormCollection fc)
        {
            string status = clsImplementationEnum.BOMTemplateStatus.Draft.GetStringValue();
            int revNo = 0;
            BOM001 objBOM001 = new BOM001();
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (fc != null)
                {
                    int headerId = Convert.ToInt32(fc["HeaderId"]);
                    string LoginUser = objClsLoginInfo.UserName;
                    if (headerId > 0)
                    {
                        objBOM001 = db.BOM001.Where(i => i.HeaderId == headerId).FirstOrDefault();
                        objBOM001.TemplateNo = fc["TemplateNo"];
                        objBOM001.Customer = fc["Customer"];
                        objBOM001.Project = fc["Project"];
                        objBOM001.ContractNo = fc["ContractNo"];
                        if (objBOM001.Status == clsImplementationEnum.BOMTemplateStatus.Submitted.GetStringValue())
                        {
                            objBOM001.RevNo = objBOM001.RevNo + 1;
                        }
                        objBOM001.Status = clsImplementationEnum.BOMTemplateStatus.Draft.GetStringValue();
                        objBOM001.EditedBy = LoginUser;
                        objBOM001.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = objBOM001.HeaderId.ToString();
                        status = objBOM001.Status;
                        revNo = Convert.ToInt32(objBOM001.RevNo);
                    }
                    else
                    {
                        objBOM001.TemplateNo = fc["TemplateNo"];
                        objBOM001.Customer = fc["Customer"].Split('-')[0];
                        objBOM001.Project = fc["Project"];
                        objBOM001.ContractNo = fc["ContractNo"].Split('-')[0]; ;
                        objBOM001.Status = clsImplementationEnum.BOMTemplateStatus.Draft.GetStringValue();
                        objBOM001.RevNo = 0;
                        objBOM001.CreatedBy = LoginUser;
                        objBOM001.CreatedOn = DateTime.Now;
                        db.BOM001.Add(objBOM001);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = objBOM001.HeaderId.ToString();
                        status = objBOM001.Status;
                        revNo = Convert.ToInt32(objBOM001.RevNo);
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            var response = new { Key = objResponseMsg.Key, Value = objResponseMsg.Value, Status = status, RevNo = revNo };
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SendToDCC(int? headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (headerId > 0)
                {
                    db.SP_IPI_BOMTEMPLATESENTTODCC(headerId, objClsLoginInfo.UserName);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Document Submitted Successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg);
        }
        [HttpPost]
        public bool checkProjectExist(string projectcode)
        {
            bool Flag = false;
            if (!string.IsNullOrWhiteSpace(projectcode))
            {
                BOM001 objBOM001 = db.BOM001.Where(x => x.Project == projectcode).FirstOrDefault();
                if (objBOM001 != null)
                {
                    Flag = true;
                }
            }
            return Flag;
        }
        [HttpPost]
        public ActionResult GetCustomerProjectWise(string projectCode)
        {
            ApproverModel objApproverModel = Manager.GetCustomerProjectWise(projectCode);
            return Json(objApproverModel, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetContractProjectWise(string projectCode)
        {
            ApproverModel objApproverModel = Manager.GetContractProjectWise(projectCode);
            return Json(objApproverModel, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Lines
        [HttpPost]
        public ActionResult AddLinesPartial(int headerId, int? lineId)
        {
            BOM002 objBOM002 = new BOM002();

            if (lineId != null && lineId > 0)
            {
                objBOM002 = db.BOM002.FirstOrDefault(x => x.HeaderId == headerId && x.LineId == lineId);
                ViewBag.parent = objBOM002.ParentItemDescription;
                ViewBag.Action = "edit";
            }
            else
            {
                objBOM002.HeaderId = headerId;
            }
            return PartialView("_AddLinesPartial", objBOM002);
        }
        //public ActionResult loadLinesDataTable(JQueryDataTableParamModel param, int headerId)
        //{
        //    try
        //    {
        //        int? StartIndex = param.iDisplayStart + 1;
        //        int? EndIndex = param.iDisplayStart + param.iDisplayLength;

        //        string condition = string.Empty;
        //        condition = "bom.HeaderId = " + headerId;

        //        if (!string.IsNullOrWhiteSpace(param.sSearch))
        //        {
        //            condition += " and ( bom.Level like '%" + param.sSearch + "%' or bom.ParentItemDescription like '%" + param.sSearch + "%' or bom.ChilditemDescription like '%" + param.sSearch + "%' or bom.CreatedBy like '%" + param.sSearch + "%')";
        //        }
        //        var lstLines = db.SP_IPI_BOMTEMPLATELINES(StartIndex, EndIndex, "", condition).ToList();
        //        int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();

        //        var res = from c in lstLines
        //                  select new[] {
        //                                Convert.ToString(c.ROW_NO),
        //                                Convert.ToString(c.HeaderId),
        //                                Convert.ToString(c.LineId),
        //                                Convert.ToString(c.Level),
        //                                Convert.ToString(c.ParentItemDescription),
        //                                Convert.ToString(c.ChilditemDescription),
        //                                ""
        //                  };

        //        return Json(new
        //        {
        //            sEcho = param.sEcho,
        //            iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
        //            iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
        //            aaData = res
        //        }, JsonRequestBehavior.AllowGet);

        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        return Json(new
        //        {
        //            sEcho = param.sEcho,
        //            iTotalDisplayRecords = "0",
        //            iTotalRecords = "0",
        //            aaData = ""
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //}



        public ActionResult loadLinesDataTable(JQueryDataTableParamModel param, int headerId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string condition = string.Empty;
                condition = "bom.HeaderId = " + headerId;
                //if (param.CTQCompileStatus.ToLower() == clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue().ToLower())
                //{
                //    isEditable = false;
                //}

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    condition += " and ( bom.Level like '%" + param.sSearch + "%' or bom.ParentItemDescription like '%" + param.sSearch + "%' or bom.ChilditemDescription like '%" + param.sSearch + "%' or bom.CreatedBy like '%" + param.sSearch + "%')";
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstLines = db.SP_IPI_BOMTEMPLATELINES(StartIndex, EndIndex, strSortOrder, condition).ToList();
                int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();
                int newRecordId = 0;
                var newRecord = new[] {
                                        "",
                                         Helper.GenerateHidden(newRecordId, "HeaderId", param.Headerid),
                                         "",
                                        Helper.GenerateTextbox(newRecordId, "Level","","checklevelFirst();"),
                                        Helper.GenerateTextbox(newRecordId, "ParentItemDescription"),
                                        Helper.GenerateTextbox(newRecordId, "ChilditemDescription"),
                                        HTMLActionString(newRecordId,"","Save","Save Record","fa fa-save","SaveNewRecord(0);"),
                                    };


                var res = (from c in lstLines
                           select new[] {
                                        Convert.ToString(c.ROW_NO),

                                         Helper.GenerateHidden(c.LineId, "HeaderId", c.HeaderId.ToString()),
                                         Helper.GenerateHidden(c.LineId, "LineId", c.LineId.ToString()),

                                         //GenerateTextboxFor(c.LineId ,"", "Level", Convert.ToString(c.Level), "UpdateData(this, "+c.LineId+");"),
                                         //GenerateTextboxFor(c.LineId ,"", "ParentItemDescription", Convert.ToString(c.ParentItemDescription), "UpdateData(this, "+c.LineId+");"),
                                         //GenerateTextboxFor(c.LineId ,"", "ChilditemDescription", Convert.ToString(c.ChilditemDescription), "UpdateData(this, "+c.LineId+");"),
                                         //HTMLActionString(c.HeaderId,"","Delete","Delete Record","fa fa-trash-o","DeleteRecord("+ c.LineId +");") ,

                                         //GenerateTextboxFor(c.LineId ,"", "Level", Convert.ToString(c.Level), "checklevelother(this, "+c.LineId+");"),
                                          Helper.GenerateTextbox(c.LineId , "Level", Convert.ToString(c.Level), "checklevelother(this, "+c.LineId+");", true, "", "5"),

                                         Helper.GenerateTextbox(c.LineId ,"ParentItemDescription", Convert.ToString(c.ParentItemDescription), "", true, "", "100"),
                                         Helper.GenerateTextbox(c.LineId ,"ChilditemDescription", Convert.ToString(c.ChilditemDescription), "", true, "", "100"),
                                        // HTMLActionString(c.HeaderId,"","Delete","Delete Record","fa fa-trash-o","DeleteRecord("+ c.LineId +");") ,
                                        HTMLActionString(c.LineId,"","Edit","Edit Record","fa fa-pencil-square-o","EnabledEditLine("+ c.LineId +");") + HTMLActionString(c.LineId,"","Update","Update Record","fa fa-floppy-o","SaveNewRecord(" + c.LineId + "); ", false) + " " + HTMLActionString(c.LineId,"","Cancel","Cancel Edit","fa fa-ban","CancelEditLine(" + c.LineId + "); ", false) + " " + HTMLActionString(c.LineId,"","Delete","Delete Record","fa fa-trash-o","DeleteRecord("+ c.LineId +");") ,





                          }).ToList();
                res.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = condition
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        //[HttpPost]
        //public ActionResult SaveLines(FormCollection fc)
        //{
        //    string status = clsImplementationEnum.BOMTemplateStatus.Draft.GetStringValue();
        //    int revNo = 0;
        //    BOM002 objBOM002 = new BOM002();
        //    clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
        //    try
        //    {
        //        if (fc != null)
        //        {
        //            int headerId = Convert.ToInt32(fc["HeaderId"]);
        //            BOM001 objBOM001 = new BOM001();
        //            if (headerId > 0)
        //            {
        //                objBOM001 = db.BOM001.Where(i => i.HeaderId == headerId).FirstOrDefault();
        //                string LoginUser = objClsLoginInfo.UserName;
        //                int lineId = Convert.ToInt32(fc["LineId"]);
        //                if (lineId > 0)
        //                {
        //                    objBOM002 = db.BOM002.Where(i => i.HeaderId == headerId && i.LineId == lineId).FirstOrDefault();
        //                    objBOM002.TemplateNo = objBOM001.TemplateNo;
        //                    objBOM002.RevNo = objBOM001.RevNo;
        //                    objBOM002.Level = Convert.ToInt32(fc["Level"]);
        //                    objBOM002.ParentItemDescription = fc["Parent"];
        //                    objBOM002.ChildItemDescription = fc["ChildItemDescription"];
        //                    objBOM002.EditedBy = LoginUser;
        //                    objBOM002.EditedOn = DateTime.Now;
        //                    db.SaveChanges();
        //                    objResponseMsg.Key = true;
        //                    objResponseMsg.Value = objBOM001.HeaderId.ToString();
        //                }
        //                else
        //                {
        //                    objBOM002.HeaderId = objBOM001.HeaderId;
        //                    objBOM002.TemplateNo = objBOM001.TemplateNo;
        //                    objBOM002.RevNo = objBOM001.RevNo;
        //                    objBOM002.Level = Convert.ToInt32(fc["Level"]);
        //                    objBOM002.ParentItemDescription = fc["Parent"];
        //                    objBOM002.ChildItemDescription = fc["ChildItemDescription"];
        //                    objBOM002.CreatedBy = LoginUser;
        //                    objBOM002.CreatedOn = DateTime.Now;
        //                    db.BOM002.Add(objBOM002);
        //                    db.SaveChanges();
        //                    objResponseMsg.Key = true;
        //                    objResponseMsg.Value = objBOM001.HeaderId.ToString();
        //                }
        //                if (objResponseMsg.Key == true && objBOM001.Status == clsImplementationEnum.BOMTemplateStatus.SenttoDCC.GetStringValue())
        //                {
        //                    objBOM001.RevNo = objBOM001.RevNo + 1;
        //                }
        //                objBOM001.Status = clsImplementationEnum.BOMTemplateStatus.Draft.GetStringValue();
        //                db.SaveChanges();
        //                status = objBOM001.Status;
        //                revNo = Convert.ToInt32(objBOM001.RevNo);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        objResponseMsg.Key = false;
        //        objResponseMsg.Value = ex.ToString();
        //    }
        //    var response = new { Key = objResponseMsg.Key, Value = objResponseMsg.Value, Status = status, RevNo = revNo };
        //    return Json(response, JsonRequestBehavior.AllowGet);
        //}
        [HttpPost]
        public ActionResult SaveLines(FormCollection fc, int LineId)
        {
            int newRowIndex = 0;
            string status = clsImplementationEnum.BOMTemplateStatus.Draft.GetStringValue();
            int revNo = 0;

            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (fc != null)
                {
                    if (LineId > 0)
                    {
                        int HeaderId = Convert.ToInt32(fc["HeaderId" + LineId]);
                        int level = Convert.ToInt32(fc["Level" + LineId]);
                        string parent = fc["ParentItemDescription" + LineId];
                        string child = fc["ChildItemDescription" + LineId];

                        BOM001 objBOM001 = db.BOM001.Where(i => i.HeaderId == HeaderId).FirstOrDefault();
                        var lstBOM002 = db.BOM002.Where(m => m.Level == level && m.ParentItemDescription == parent && m.ChildItemDescription == child && m.HeaderId == HeaderId && m.LineId != LineId).ToList();
                        if (parent == child)
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Parent Item and Child Item must be different";
                        }
                        else
                        {
                            var lstparentexist = db.BOM002.Where(m => m.Level == level && m.ParentItemDescription == parent && m.ChildItemDescription==child && m.HeaderId == HeaderId && m.LineId != LineId).ToList();
                            if (lstparentexist.Count == 0)
                            {
                                if (lstBOM002.Count > 0)
                                {
                                    //BOM002 objBOM002 = new BOM002();
                                    //objBOM002.HeaderId = objBOM001.HeaderId;
                                    //objBOM002.TemplateNo = objBOM001.TemplateNo;
                                    //objBOM002.RevNo = objBOM001.RevNo;
                                    //objBOM002.Level = Convert.ToInt32(fc["Level" + LineId]);
                                    //objBOM002.ParentItemDescription = fc["ParentItemDescription" + LineId];
                                    //objBOM002.ChildItemDescription = fc["ChildItemDescription" + LineId];
                                    //objBOM002.CreatedBy = objClsLoginInfo.UserName;
                                    //objBOM002.CreatedOn = DateTime.Now;
                                    //db.BOM002.Add(objBOM002);
                                    //db.SaveChanges();
                                    objResponseMsg.Key = false;
                                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate;
                                }
                                else
                                {

                                    BOM002 objBOM002 = db.BOM002.Where(m => m.LineId == LineId).FirstOrDefault();
                                    objBOM002.Level = Convert.ToInt32(fc["Level" + LineId]);
                                    objBOM002.ParentItemDescription = fc["ParentItemDescription" + LineId];
                                    objBOM002.ChildItemDescription = fc["ChildItemDescription" + LineId];
                                    objBOM002.EditedBy = objClsLoginInfo.UserName;
                                    objBOM002.EditedOn = DateTime.Now;
                                    db.SaveChanges();

                                    objResponseMsg.Key = true;
                                    objResponseMsg.Value = objBOM001.HeaderId.ToString();

                                    if (objResponseMsg.Key == true && objBOM001.Status == clsImplementationEnum.BOMTemplateStatus.Submitted.GetStringValue())
                                    {
                                        objBOM001.RevNo = objBOM001.RevNo + 1;
                                    }
                                    objBOM001.Status = clsImplementationEnum.BOMTemplateStatus.Draft.GetStringValue();
                                    db.SaveChanges();
                                }
                            }
                            else
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = "Parent Item descrption is already exist";
                            }
                        }
                        status = objBOM001.Status;
                        revNo = Convert.ToInt32(objBOM001.RevNo);
                    }
                    else
                    {
                        int HeaderId = Convert.ToInt32(fc["HeaderId" + newRowIndex]);
                        int level = Convert.ToInt32(fc["Level" + newRowIndex]);
                        string parent = fc["ParentItemDescription" + newRowIndex];
                        string child = fc["ChildItemDescription" + newRowIndex];

                        BOM001 objBOM001 = db.BOM001.Where(i => i.HeaderId == HeaderId).FirstOrDefault();
                        BOM002 objBOM002 = new BOM002();
                       
                        var lstBOM002 = db.BOM002.Where(m => m.Level == level && m.ParentItemDescription == parent && m.ChildItemDescription == child && m.HeaderId == HeaderId && m.LineId != LineId).ToList();
                        if (parent == child)
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Parent Item and Child Item must be different";
                        }
                        else
                        {
                            var lstparentexist = db.BOM002.Where(m => m.Level == level && m.ParentItemDescription == parent && m.ChildItemDescription==child  && m.HeaderId == HeaderId && m.LineId != LineId).ToList();
                            if (lstparentexist.Count == 0)
                            {
                                if (lstBOM002.Count > 0)
                                {
                                    objResponseMsg.Key = false;
                                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate;
                                }
                                else
                                {
                                    objBOM002.HeaderId = objBOM001.HeaderId;
                                    objBOM002.TemplateNo = objBOM001.TemplateNo;
                                    objBOM002.RevNo = objBOM001.RevNo;
                                    objBOM002.Level = Convert.ToInt32(fc["Level" + newRowIndex]);
                                    objBOM002.ParentItemDescription = fc["ParentItemDescription" + newRowIndex];
                                    objBOM002.ChildItemDescription = fc["ChildItemDescription" + newRowIndex];
                                    objBOM002.CreatedBy = objClsLoginInfo.UserName;
                                    objBOM002.CreatedOn = DateTime.Now;
                                    db.BOM002.Add(objBOM002);
                                    db.SaveChanges();
                                    objResponseMsg.Key = true;
                                    objResponseMsg.Value = objBOM001.HeaderId.ToString();

                                    if (objResponseMsg.Key == true && objBOM001.Status == clsImplementationEnum.BOMTemplateStatus.Submitted.GetStringValue())
                                    {
                                        objBOM001.RevNo = objBOM001.RevNo + 1;
                                    }
                                    objBOM001.Status = clsImplementationEnum.BOMTemplateStatus.Draft.GetStringValue();
                                    db.SaveChanges();
                                }
                            }
                            else
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = "Parent Item descrption is already exist";
                            }
                        }
                        status = objBOM001.Status;
                        revNo = Convert.ToInt32(objBOM001.RevNo);

                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            var response = new { Key = objResponseMsg.Key, Value = objResponseMsg.Value, Status = status, RevNo = revNo };
            return Json(response, JsonRequestBehavior.AllowGet);
        }





        /// <summary>
        /// 
        /// </summary>
        /// <param name="LineId"></param>
        /// <param name="columnName"></param>
        /// <param name="columnValue"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UpdateData(int LineId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string table_name = "bom002";
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    db.SP_COMMON_LINES_UPDATE(LineId, columnName, columnValue, table_name, objClsLoginInfo.UserName);

                    //ReviseData(headerId);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.LineUpdate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateNewRecord(FormCollection fc, int LineId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int HeaderId = Convert.ToInt32(fc["HeaderId" + LineId]);
                int level = Convert.ToInt32(fc["Level" + LineId]);
                string Parent = fc["ParentItemDescription" + LineId];
                string Child = fc["ChildItemDescription" + LineId];

                BOM002 objbom002 = new BOM002();
                var checkdata = db.BOM002.Where(m => m.Level == level && m.ParentItemDescription == Parent && m.ChildItemDescription == Child && m.HeaderId == HeaderId).FirstOrDefault();
                if (checkdata == null)
                {

                    var qry = db.BOM002.Where(m => m.LineId == LineId && m.HeaderId == HeaderId).FirstOrDefault();
                    objbom002.Level = Convert.ToInt32(qry.Level);
                    objbom002.ParentItemDescription = qry.ParentItemDescription;
                    objbom002.ChildItemDescription = qry.ChildItemDescription;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Success";


                }
                else
                {
                    if (checkdata.LineId == LineId)
                    {
                        var qry = db.BOM002.Where(m => m.LineId == LineId && m.HeaderId == HeaderId).FirstOrDefault();
                        objbom002.Level = Convert.ToInt32(qry.Level);
                        objbom002.ParentItemDescription = qry.ParentItemDescription;
                        objbom002.ChildItemDescription = qry.ChildItemDescription;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Success";
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Record Already exist";
                    }
                }


                //ReviseData(headerId);
                // objResponseMsg.Key = true;
                // objResponseMsg.Value = clsImplementationMessage.CommonMessages.LineUpdate.ToString();

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult DeleteBOMLine(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                BOM002 objBOM002 = db.BOM002.Where(x => x.LineId == Id).FirstOrDefault();
                db.BOM002.Remove(objBOM002);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> DeleteLines(int headerId, int lineId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            BOM001 objBOM001 = await db.BOM001.FirstOrDefaultAsync(x => x.HeaderId == headerId);
            try
            {
                BOM002 objBOM002 = await db.BOM002.Where(x => x.HeaderId == headerId && x.LineId == lineId).FirstOrDefaultAsync();
                if (objBOM002 != null)
                {
                    db.BOM002.Remove(objBOM002);
                    await db.SaveChangesAsync();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Line deleted successfully";
                }
                if (objResponseMsg.Key == true && objBOM001.Status == clsImplementationEnum.BOMTemplateStatus.Submitted.GetStringValue())
                {
                    objBOM001.RevNo = objBOM001.RevNo + 1;
                }
                objBOM001.Status = clsImplementationEnum.BOMTemplateStatus.Draft.GetStringValue();
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            var response = new
            {
                Key = objResponseMsg.Key,
                Value = objResponseMsg.Value,
                RevNo = objBOM001.RevNo,
                Status = objBOM001.Status
            };
            return Json(response);
        }
        #endregion

        #region History
        [HttpPost]
        public ActionResult GetHistoryDetails(int headerId, string forAction)
        {
            ViewBag.HeaderId = headerId;
            ViewBag.type = forAction;
            return PartialView("_GetHistoryDetailsPartial");
        }
        [HttpPost]
        public JsonResult LoadHistoryDetailData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;

                string whereCondition = "1=1 and bom.HeaderId = " + HeaderId + " ";
                //whereCondition += " and (ApprovedBy = '" + objClsLoginInfo.UserName + "' or CreatedBy = '" + objClsLoginInfo.UserName + "' ) ";
                string[] columnName = { "bom.Project +'-'+ com1.t_dsca", "bom.ContractNo+'-'+com4.t_desc", "com6.t_bpid+'-'+com6.t_nama", "bom.Status", "bom.TemplateNo", "bom.RevNo" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_IPI_BOMTEMPLATEHEADER_History(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstResult.Select(i => i.TotalCount).FirstOrDefault();

                var data = (from h in lstResult
                            select new[]
                           {
                               Convert.ToString(h.Id),
                               h.TemplateNo,
                               Convert.ToString( h.ContractNo),
                               Convert.ToString( h.Project),
                               Convert.ToString( h.Customer),
                               Convert.ToString( "R"+h.RevNo),
                               Convert.ToString(h.Status),
                               ""
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition

                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }


        [SessionExpireFilter]
        public ActionResult HistoryView(int id, string type)
        {
            BOM001_log objBOM001_Log = new BOM001_log();
            objBOM001_Log = db.BOM001_log.Where(x => x.Id == id).FirstOrDefault();
            objBOM001_Log.Customer = Manager.GetCustomerCodeAndNameByProject(objBOM001_Log.Project);
            objBOM001_Log.ContractNo = Manager.GetContractorAndDescription(objBOM001_Log.ContractNo);
            objBOM001_Log.Project = db.Database.SqlQuery<string>("SELECT dbo.GET_PROJECTDESC_BY_CODE('" + objBOM001_Log.Project + "')").FirstOrDefault();

            ViewBag.type = type;
            return View(objBOM001_Log);
        }

        public ActionResult loadLinesHistoryDataTable(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                int Id = Convert.ToInt32(param.CTQHeaderId);

                string strSortOrder = string.Empty;

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string condition = string.Empty;
                condition = "bom.RefId = " + Id;

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    condition += " and ( bom.Level like '%" + param.sSearch + "%' or bom.ParentItemDescription like '%" + param.sSearch + "%' or bom.ChilditemDescription like '%" + param.sSearch + "%' or bom.CreatedBy like '%" + param.sSearch + "%')";
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstLines = db.SP_IPI_BOMTEMPLATELINES_History(StartIndex, EndIndex, strSortOrder, condition).ToList();
                int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();

                var data = (from h in lstLines
                            select new[]
                           {
                               Convert.ToString(h.ROW_NO),
                               Convert.ToString(h.LineId),
                               Convert.ToString(h.HeaderId),
                               Convert.ToString( h.Level),
                               Convert.ToString( h.ParentItemDescription),
                               Convert.ToString( h.ChilditemDescription),
                           }).ToList();

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = condition
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_IPI_BOMTEMPLATEHEADER(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      TemplateNo = li.TemplateNo,
                                      ContractNo = li.ContractNo,
                                      Customer = li.Customer,
                                      Project = li.Project,
                                      RevNo = li.RevNo,
                                      Status = li.Status,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_IPI_BOMTEMPLATELINES(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from li in lst
                                  select new
                                  {
                                      Level = li.Level,
                                      ParentItemDescription = li.ParentItemDescription,
                                      ChilditemDescription = li.ChilditemDescription
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
    }
}