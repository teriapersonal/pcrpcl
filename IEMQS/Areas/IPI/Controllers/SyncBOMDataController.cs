﻿using IEMQS.Areas.IPI.Models;
using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.IPI.Controllers
{
    public class SyncBOMDataController : clsBase
    {
        // GET: IPI/SyncBOMData

        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult Index()
        {
            ViewBag.Title = "Sync BOM Data";
            var lstProj = Manager.getProjectsByUser(objClsLoginInfo.UserName).ToList();
            ViewBag.Project = lstProj.Select(i => new { Value = i.projectCode, Text = i.projectDescription });

            var lstLocation = Manager.GetUserwiseLocation(objClsLoginInfo.UserName).ToList();
            ViewBag.Location = (from ath1 in lstLocation
                                join com2 in db.COM002 on ath1 equals com2.t_dimx
                                select new { Value = ath1, Text = com2.t_desc }).Distinct().ToList();
            var lstSync = clsImplementationEnum.GetSyncBOMData().ToList();
            ViewBag.Sync = lstSync.Select(i => new { Value = clsImplementationEnum.convertTOTable(i), Text = i.ToString() });
            return View();
        }
        [HttpPost]
        public ActionResult GetItemResult(string term)
        {
            string whrCondition = string.Empty;
            if (!string.IsNullOrWhiteSpace(term))
            {
                whrCondition = " where ltrim(t_item)like  '%" + term.TrimStart() + "%' ";
            }
            List<string> lstSrcCOM012 = db.Database.SqlQuery<string>(@"SELECT  ltrim(t_item) from " + LNLinkedServer + ".dbo.ttcibd001175" + whrCondition).Take(10).ToList();

            var CItemList = (from lst in lstSrcCOM012
                             select new { id = lst, text = lst }
                             ).Distinct().Take(5).ToList();

            return Json(CItemList, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult LoadDataGridPartial(string wmr, string item)
        {
            if (wmr != "")
            {
                ViewBag.GridTitle = wmr;
                ViewBag.WMR = wmr;
                ViewBag.COM012 = null;
            }

            if (item != "")
            {
                ViewBag.GridTitle = item;
                ViewBag.WMR = null;
                List<COM012> lstSrcCOM012 = db.Database.SqlQuery<COM012>(@"SELECT  * from " + LNLinkedServer + ".dbo.ttcibd001175  where ltrim(t_item) =  '" + item + "'").ToList();
                ViewBag.COM012 = lstSrcCOM012;
            }
            return PartialView("_LoadSyncGridPartial");
        }

        [HttpPost]
        public JsonResult LoadDataGridData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                strWhereCondition += " 1=1 and  ltrim(t_mitm) ='" + param.Title.ToUpper() + "'";
                #endregion

                //search Condition 
                string[] columnName = { "t_mitm", "t_pono", "t_seqn", "t_sitm", "t_efco", "t_indt" };
                strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                var lstResult = db.SP_IPI_GET_SYNC_DATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.t_mitm),
                            Convert.ToString(uc.t_pono),
                            Convert.ToString(uc.t_seqn),
                            Convert.ToString(uc.t_sitm),
                            Convert.ToString(uc.t_efco),
                            Convert.ToString(uc.t_indt),
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SyncData(string Project, string Location, string Sync, string item)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string wmr = string.Empty;
                if (Sync.ToLower() == clsImplementationEnum.SyncBOMData.COM011.ToString().ToLower())
                {
                    string loc = Location.Substring(0, 1);
                     wmr = Project + "-" + loc.ToUpper() + "-" + "WMR";
                    #region Sync Com011
                    List<COM011> lstSrcCOM011 = db.Database.SqlQuery<COM011>(@"SELECT  * from " + LNLinkedServer + ".dbo.ttibom010175 where ltrim(rtrim(t_mitm)) = '" + wmr + "'").ToList();
                    List<COM011> lstDestCOM011 = db.COM011.Where(x => x.t_mitm.TrimStart() == wmr.TrimStart()).ToList();
                    objResponseMsg = SyncDataWithCOM011(lstSrcCOM011, lstDestCOM011, wmr);

                    #endregion
                }
                if (Sync.ToLower() == clsImplementationEnum.SyncBOMData.COM012.ToString().ToLower())
                {

                    #region Sync Com012
                    List<COM012> lstSrcCOM012 = db.Database.SqlQuery<COM012>(@"SELECT  * from " + LNLinkedServer + ".dbo.ttcibd001175 where ltrim(t_item) = '" + item.Trim() + "'").ToList();
                    List<COM012> lstDestCOM012 = db.COM012.Where(x => x.t_item.TrimStart() == item.TrimStart()).ToList();
                    objResponseMsg = SyncDataWithCOM012(lstSrcCOM012, lstDestCOM012, item);
                    #endregion
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data sync successfully";
                objResponseMsg.item = item;
                objResponseMsg.ActionValue = wmr;

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public clsHelper.ResponseMsgWithStatus SyncDataWithCOM011(List<COM011> lstSrcCOM011, List<COM011> lstDestCOM011, string wmr)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            List<COM011> lstRemove = new List<COM011>();
            foreach (var lst in lstDestCOM011)
            {
                if (!lstSrcCOM011.Any(x => x.t_pono == lst.t_pono && x.t_mitm.TrimStart() == wmr.TrimStart() && x.t_seqn == lst.t_seqn))
                {
                    COM011 remove = db.COM011.Where(x => x.t_pono == lst.t_pono && x.t_mitm.TrimStart() == wmr.TrimStart() && x.t_seqn == lst.t_seqn).FirstOrDefault();
                    lstRemove.Add(remove);
                }
            }
            db.COM011.RemoveRange(lstRemove);
            db.SaveChanges();
            if (lstSrcCOM011.Count > 0)
            {
                foreach (COM011 objSrcCOM011 in lstSrcCOM011)
                {
                    bool isAdded = false;
                    COM011 objCOM011 = db.COM011.Where(m => m.t_pono == objSrcCOM011.t_pono && m.t_mitm.TrimStart() == wmr.TrimStart() && m.t_seqn == objSrcCOM011.t_seqn).FirstOrDefault();
                    if (objCOM011 == null)
                    {
                        objCOM011 = new COM011();
                        isAdded = true;
                    }
                    objCOM011.t_mitm = objSrcCOM011.t_mitm;
                    objCOM011.t_pono = objSrcCOM011.t_pono;
                    objCOM011.t_seqn = objSrcCOM011.t_seqn;
                    objCOM011.t_sitm = objSrcCOM011.t_sitm;
                    objCOM011.t_efco = objSrcCOM011.t_efco;
                    objCOM011.t_indt = objSrcCOM011.t_indt;
                    objCOM011.t_exco = objSrcCOM011.t_exco;
                    objCOM011.t_exdt = objSrcCOM011.t_exdt;
                    objCOM011.t_leng = objSrcCOM011.t_leng;
                    objCOM011.t_widt = objSrcCOM011.t_widt;
                    objCOM011.t_noun = objSrcCOM011.t_noun;
                    objCOM011.t_qana = objSrcCOM011.t_qana;
                    objCOM011.t_scpf = objSrcCOM011.t_scpf;
                    objCOM011.t_scpq = objSrcCOM011.t_scpq;
                    objCOM011.t_cwar = objSrcCOM011.t_cwar;
                    objCOM011.t_opno = objSrcCOM011.t_opno;
                    objCOM011.t_cpha = objSrcCOM011.t_cpha;
                    objCOM011.t_phst = objSrcCOM011.t_phst;
                    objCOM011.t_exin = objSrcCOM011.t_exin;
                    objCOM011.t_ledm = objSrcCOM011.t_ledm;
                    objCOM011.t_ltom = objSrcCOM011.t_ltom;
                    objCOM011.t_ltov = objSrcCOM011.t_ltov;
                    objCOM011.t_ltou = objSrcCOM011.t_ltou;
                    objCOM011.t_stcf = objSrcCOM011.t_stcf;
                    objCOM011.t_unef = objSrcCOM011.t_unef;
                    objCOM011.t_pdoc = objSrcCOM011.t_pdoc;
                    objCOM011.t_yldo = objSrcCOM011.t_yldo;
                    objCOM011.t_scro = objSrcCOM011.t_scro;
                    objCOM011.t_lsel = objSrcCOM011.t_lsel;
                    objCOM011.t_expl = objSrcCOM011.t_expl;
                    objCOM011.t_mrou = objSrcCOM011.t_mrou;
                    objCOM011.t_preq = objSrcCOM011.t_preq;
                    objCOM011.t_almi = objSrcCOM011.t_almi;
                    objCOM011.t_altp = objSrcCOM011.t_altp;
                    objCOM011.t_usup = objSrcCOM011.t_usup;
                    objCOM011.t_rdsp = objSrcCOM011.t_rdsp;
                    objCOM011.t_iwip = objSrcCOM011.t_iwip;
                    objCOM011.t_sbsr = objSrcCOM011.t_sbsr;
                    objCOM011.t_owns = objSrcCOM011.t_owns;
                    objCOM011.t_ifmi = objSrcCOM011.t_ifmi;
                    objCOM011.t_hppr = objSrcCOM011.t_hppr;
                    objCOM011.t_icfm = objSrcCOM011.t_icfm;
                    objCOM011.t_txta = objSrcCOM011.t_txta;
                    objCOM011.t_Refcntd = objSrcCOM011.t_Refcntd;
                    objCOM011.t_Refcntu = objSrcCOM011.t_Refcntu;

                    if (isAdded)
                    {
                        db.COM011.Add(objCOM011);
                    }
                    db.SaveChanges();
                }
            }
            objResponseMsg.Key = true;
            return objResponseMsg;
        }

        public clsHelper.ResponseMsgWithStatus SyncDataWithCOM012(List<COM012> lstSrcCOM012, List<COM012> lstDestCOM012, string item)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            List<COM012> lstRemove = new List<COM012>();
            foreach (var lst in lstDestCOM012)
            {
                if (!lstSrcCOM012.Any(x => x.t_item == lst.t_item))
                {
                    COM012 remove = db.COM012.Where(x => x.t_item == lst.t_item).FirstOrDefault();
                    lstRemove.Add(remove);
                }
            }
            db.COM012.RemoveRange(lstRemove);
            db.SaveChanges();
            if (lstSrcCOM012.Count > 0)
            {
                foreach (COM012 SrcCOM012 in lstSrcCOM012)
                {
                    bool isAdded = false;
                    COM012 objCOM012 = db.COM012.Where(m => m.t_item.TrimStart() == SrcCOM012.t_item.TrimStart()).FirstOrDefault();
                    if (objCOM012 == null)
                    {
                        objCOM012 = new COM012();
                        isAdded = true;
                    }
                    objCOM012.t_item = SrcCOM012.t_item;
                    objCOM012.t_kitm = SrcCOM012.t_kitm;
                    objCOM012.t_citg = SrcCOM012.t_citg;
                    objCOM012.t_itmt = SrcCOM012.t_itmt;
                    objCOM012.t_dsca = SrcCOM012.t_dsca;
                    objCOM012.t_dscb = SrcCOM012.t_dscb;
                    objCOM012.t_dscc = SrcCOM012.t_dscc;
                    objCOM012.t_dscd = SrcCOM012.t_dscd;
                    objCOM012.t_seak = SrcCOM012.t_seak;
                    objCOM012.t_seab = SrcCOM012.t_seab;
                    objCOM012.t_uset = SrcCOM012.t_uset;
                    objCOM012.t_cuni = SrcCOM012.t_cuni;
                    objCOM012.t_cwun = SrcCOM012.t_cwun;
                    objCOM012.t_wght = SrcCOM012.t_wght;
                    objCOM012.t_ctyp = SrcCOM012.t_ctyp;
                    objCOM012.t_ltct = SrcCOM012.t_ltct;
                    objCOM012.t_csel = SrcCOM012.t_csel;
                    objCOM012.t_csig = SrcCOM012.t_csig;
                    objCOM012.t_ctyo = SrcCOM012.t_ctyo;
                    objCOM012.t_cpcl = SrcCOM012.t_cpcl;
                    objCOM012.t_cood = SrcCOM012.t_cood;
                    objCOM012.t_eitm = SrcCOM012.t_eitm;
                    objCOM012.t_umer = SrcCOM012.t_umer;
                    objCOM012.t_cpln = SrcCOM012.t_cpln;
                    objCOM012.t_ccde = SrcCOM012.t_ccde;
                    objCOM012.t_cmnf = SrcCOM012.t_cmnf;
                    objCOM012.t_cean = SrcCOM012.t_cean;
                    objCOM012.t_cont = SrcCOM012.t_cont;
                    objCOM012.t_cntr = SrcCOM012.t_cntr;
                    objCOM012.t_cprj = SrcCOM012.t_cprj;
                    objCOM012.t_repl = SrcCOM012.t_repl;
                    objCOM012.t_cpva = SrcCOM012.t_cpva;
                    objCOM012.t_dfit = SrcCOM012.t_dfit;
                    objCOM012.t_stoi = SrcCOM012.t_stoi;
                    objCOM012.t_cpcp = SrcCOM012.t_cpcp;
                    objCOM012.t_unef = SrcCOM012.t_unef;
                    objCOM012.t_ichg = SrcCOM012.t_ichg;
                    objCOM012.t_uefs = SrcCOM012.t_uefs;
                    objCOM012.t_seri = SrcCOM012.t_seri;
                    objCOM012.t_styp = SrcCOM012.t_styp;
                    objCOM012.t_psiu = SrcCOM012.t_psiu;
                    objCOM012.t_efco = SrcCOM012.t_efco;
                    objCOM012.t_indt = SrcCOM012.t_indt;
                    objCOM012.t_chma = SrcCOM012.t_chma;
                    objCOM012.t_edco = SrcCOM012.t_edco;
                    objCOM012.t_mcoa = SrcCOM012.t_mcoa;
                    objCOM012.t_opts = SrcCOM012.t_opts;
                    objCOM012.t_envc = SrcCOM012.t_envc;
                    objCOM012.t_sayn = SrcCOM012.t_sayn;
                    objCOM012.t_subc = SrcCOM012.t_subc;
                    objCOM012.t_srce = SrcCOM012.t_srce;
                    objCOM012.t_cnfg = SrcCOM012.t_cnfg;
                    objCOM012.t_lmdt = SrcCOM012.t_lmdt;
                    objCOM012.t_exin = SrcCOM012.t_exin;
                    objCOM012.t_imag = SrcCOM012.t_imag;
                    objCOM012.t_efpr = SrcCOM012.t_efpr;
                    objCOM012.t_ippg = SrcCOM012.t_ippg;
                    objCOM012.t_ppeg = SrcCOM012.t_ppeg;
                    objCOM012.t_dpeg = SrcCOM012.t_dpeg;
                    objCOM012.t_dpcr = SrcCOM012.t_dpcr;
                    objCOM012.t_dptp = SrcCOM012.t_dptp;
                    objCOM012.t_dpuu = SrcCOM012.t_dpuu;
                    objCOM012.t_elrq = SrcCOM012.t_elrq;
                    objCOM012.t_elcm = SrcCOM012.t_elcm;
                    objCOM012.t_icsi = SrcCOM012.t_icsi;
                    objCOM012.t_txta = SrcCOM012.t_txta;
                    objCOM012.t_cdf_dens = SrcCOM012.t_cdf_dens;
                    objCOM012.t_cdf_dsca = SrcCOM012.t_cdf_dsca;
                    objCOM012.t_cdf_fvmi = SrcCOM012.t_cdf_fvmi;
                    objCOM012.t_cdf_thic = SrcCOM012.t_cdf_thic;
                    objCOM012.t_Refcntd = SrcCOM012.t_Refcntd;
                    objCOM012.t_Refcntu = SrcCOM012.t_Refcntu;

                    if (isAdded)
                    {
                        db.COM012.Add(objCOM012);
                    }
                    db.SaveChanges();
                }
            }
            objResponseMsg.Key = true;
            return objResponseMsg;
        }
    }
}