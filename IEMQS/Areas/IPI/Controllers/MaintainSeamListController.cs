﻿using IEMQS.Areas.IPI.Models;
using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using static IEMQS.Areas.HTC.Controllers.MaintainHTCController;

namespace IEMQS.Areas.IPI.Controllers
{
    public class MaintainSeamListController : clsBase
    {
        #region Utility
        public static bool isFieldEditable(string position = "", string AssemblyName = "", string project = "")
        {
            IEMQSEntitiesContext db = new IEMQSEntitiesContext();
            bool isEditable = false;
            var lstPosition = position.Split(',').ToList();
            //Obs ID#19247. SP input param "assemblyName" passed as empty.
            //As per discussed with Trishul Tandel on 07-01-2019, Material and Thickness should be populated based on only Project & FindNo. Assembly/ParentPart should not be required.
            var results = db.SP_IPI_GETSEAMLISTDETAILS(project, "", "").ToList();

            if (results.Count > 0)
            {
                isEditable = results.Where(x => lstPosition.Contains(x.FindNo.ToString()) && x.ProductType.ToLower() == clsImplementationEnum.ProductType.FRG.GetStringValue().ToLower()).Any();
            }
            return isEditable;
        }
        public string ReplaceNewLine(string oldString)
        {
            string ReplaceString = Regex.Replace(oldString, @"\t|\n|\r", string.Empty);
            return ReplaceString;
        }
        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", bool isVisible = true, bool atagrequired = false)
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick=" + onClickMethod : "";

            if (!string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
            {
                if (string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue()))
                {
                    htmlControl = "";// "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }
                else
                {
                    if (isVisible)
                    {
                        htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer; margin-right: 3px; margin-left: 0px;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                    }
                    else
                    {
                        htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer; margin-right: 3px; margin-left: 0px; display:none;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                    }
                    if (atagrequired)
                    {
                        htmlControl = "<a>" + htmlControl + "</a>";
                    }
                }
            }

            return htmlControl;
        }

        public static string MultiSelectDropdown(List<SelectItemList> list, int rowID, string selectedValue, bool Disabled = false, string OnChangeEvent = "", string OnBlurEvent = "", string ColumnName = "")
        {
            string multipleSelect = string.Empty;
            string[] arrayVal = { };

            try
            {
                if (!string.IsNullOrWhiteSpace(selectedValue))
                {
                    arrayVal = selectedValue.Split(',');
                }
                // var repeatedVal = arrayVal.GroupBy(x => x).ToList();

                multipleSelect += "<select data-name='ddlmultiple' data-oldvalue='" + selectedValue + "' name='ddlmultiple" + rowID + "' id='ddlmultiple" + rowID + "' data-lineid='" + rowID + "' multiple='multiple'  style='width: 100 % ' colname='" + ColumnName + "' class='form-control multiselect-drodown' " + (Disabled ? "disabled " : "") + (OnChangeEvent != string.Empty ? "onchange=" + OnChangeEvent + "" : "") + (OnBlurEvent != string.Empty ? " onblur='" + OnBlurEvent + "'" : "") + " >";
                foreach (var val in arrayVal)
                {
                    if (val != null)
                    {
                        var item = list.FirstOrDefault(w => w.id == val);
                        if (item != null)
                            multipleSelect += "<option value='" + item.id + "' selected" + " >" + item.text + "</option>";
                    }
                }
                foreach (var item in list)
                {
                    if (arrayVal.Contains(item.id + ""))
                        continue;

                    multipleSelect += "<option value='" + item.id + "'  " + ((arrayVal.Contains(item.id)) ? " selected" : "") + " >" + item.text + "</option>";
                    //foreach (var val in repeatedVal)
                    //{
                    //    if (item.id == val.Key)
                    //    {
                    //        for (int i = 0; i < val.Count() - 1; i++)
                    //        {
                    //            multipleSelect += "<option value='" + item.id + "' data-new='true' selected" + " >" + item.text + "</option>";
                    //        }
                    //    }
                    //}
                }
                multipleSelect += "</select>";
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return multipleSelect;
        }

        public static string GenerateTextboxFor(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string maxLength = "", bool disabled = false, string AssemblyValue = "", string ProjectValue = "", string Position = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";
            maxLength = !string.IsNullOrEmpty(maxLength) ? "maxlength='" + maxLength + "'" : "";
            string productType = string.Empty;

            if (columnName.ToLower() == "Thickness".ToLower() && !string.IsNullOrEmpty(AssemblyValue) && !string.IsNullOrEmpty(ProjectValue) && !string.IsNullOrEmpty(Position))
            {
                //var result = isFieldEditable(Position, AssemblyValue, ProjectValue);
                //if (result)
                //{
                //    //isReadOnly = false;
                //    productType = "frg";
                //}
            }

            if (columnName == "SeamLengthArea")
            {
                htmlControl = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id=" + inputID + " data-producttype='" + productType + "' data-lineid='" + rowId + "' value='" + inputValue + "' data-oldvalue='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + " numeric' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + " " + maxLength + " " + (disabled ? "disabled" : "") + "  />";
            }
            else if (columnName.ToLower() == "thickness")
            {
                htmlControl = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id=" + inputID + " data-producttype='" + productType + "' data-lineid='" + rowId + "' value='" + inputValue + "' data-oldvalue='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + " thickness' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + " " + maxLength + " " + (disabled ? "disabled" : "") + "  />";
            }
            else
            {
                htmlControl = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id=" + inputID + " data-producttype='" + productType + "' data-lineid='" + rowId + "' value='" + inputValue + "' data-oldvalue='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + " " + maxLength + " " + (disabled ? "disabled" : "") + "  />";
            }

            return htmlControl;
        }
        public string GenerateAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false)
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = columnName == "SeamDescription" ? "autocomplete form-control clsseamdesc" : "autocomplete form-control";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick=" + onClickMethod + "" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' data-lineid='" + rowId + "' hdElement='" + hdElement + "' value='" + inputValue + "' data-oldvalue='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + " " + (disabled ? "disabled" : "") + " />";

            return strAutoComplete;
        }

        [NonAction]
        public static string GenerateGridButton(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", string style = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";


            htmlControl = "<a id='" + inputID + "' name='" + inputID + "' style='display:" + style + "' ' " + onClickEvent + " ><i class='" + className + "'></i> " + buttonName + "</a>";

            //htmlControl = "<i  data-modal='' id='" + inputID + "' name='" + inputID + "' style='cursor:Pointer;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";

            return htmlControl;
        }

        public string GetParentPart(string parentPart, string objParentPart, List<FN_GET_HBOM_DATA_Result> objBOMlist, List<string> listparentpart)
        {
            listparentpart.Add(parentPart);
            if (objBOMlist.Any(i => i.Part == parentPart))
            {
                var parents = objBOMlist.Where(i => i.Part == parentPart).ToList();
                foreach (var item in parents)
                {
                    string parentNode = string.Empty;
                    parentNode = GetParentPart(item.ParentPart, parentNode, objBOMlist, listparentpart);
                }
            }
            return parentPart;
        }
        public List<string> GetPartParentList(string positions, List<FN_GET_HBOM_DATA_Result> objBOMlist)
        {
            List<string> listparentpart = new List<string>();
            var positionArr = positions.Split(',');
            var lstParts = objBOMlist.Where(x => positionArr.Contains(x.FindNo)).Select(x => new { Part = x.Part, ParentPart = x.ParentPart }).ToList();

            foreach (var item in lstParts)
            {
                string parentpart = string.Empty;
                parentpart = GetParentPart(item.ParentPart, parentpart, objBOMlist, listparentpart);
            }
            listparentpart = listparentpart.Distinct().ToList();
            return listparentpart;
        }

        [HttpPost]
        public bool IsSeamApplicableForDelete(string Project, string QualityProject, string SeamNo)
        {
            bool returnValue = true;

            // Check For Test Plan Lines
            if (db.QMS021.Any(c => c.Project == Project && c.QualityProject == QualityProject && c.SeamNo == SeamNo))
            {
                returnValue = false;
            }
            // Check For Show Weld Plan
            else if (db.SWP010.Any(c => c.Project == Project && c.QualityProject == QualityProject && c.SeamNo == SeamNo && c.MainWPSNumber != null))
            {
                returnValue = false;
            }

            return returnValue;

        }

        #endregion

        #region Header

        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Index(string Project)
        {
            ViewBag.IsDisplayOnly = false;
            ViewBag.chkProject = Project;
            return View();
        }
        [HttpPost]
        public bool CheckQualityProjectExist(string qualityProjectCode, string projectCode)
        {
            bool Flag = false;
            if (!string.IsNullOrWhiteSpace(qualityProjectCode) && !string.IsNullOrWhiteSpace(projectCode))
            {
                QMS011 objQMS011 = db.QMS011.Where(x => x.Project == projectCode && x.QualityProject == qualityProjectCode).FirstOrDefault();
                if (objQMS011 != null)
                {
                    Flag = true;
                }
            }
            return Flag;
        }
        public string GetMaxSeamListNo(string qualityProjectCode)
        {
            return db.Database.SqlQuery<string>("SELECT dbo.FN_GENERATE_MAX_IPI_SEAMLIST_NO('" + qualityProjectCode + "')").FirstOrDefault();
        }

        [HttpPost]
        public async Task<ActionResult> GetQualityProjectDetail(string project)
        {
            QualityProjectDetail objQualityProjectDetail = new QualityProjectDetail();
            if (!string.IsNullOrWhiteSpace(project))
            {
                //var qualityproject = db.QMS010.Where(m => m.Project == project).FirstOrDefault();
                //var qualityprojectdetail = qualityproject.QualityProject;


                try
                {
                    QMS011 objHeader = await db.QMS011.Where(i => i.QualityProject == project && (i.Status.ToLower() == "Draft".ToLower() || i.Status.ToLower() == "Returned".ToLower())).FirstOrDefaultAsync();
                    if (objHeader == null)
                    {
                        objHeader = await db.QMS011.Where(i => i.QualityProject == project && i.Status.ToLower() == "SentForApproval".ToLower()).FirstOrDefaultAsync();
                        if (objHeader == null)
                        {
                            objHeader = await db.QMS011.Where(i => i.QualityProject == project).FirstOrDefaultAsync();
                        }
                    }

                    if (objHeader != null && objHeader.Status == clsImplementationEnum.SeamListStatus.Approved.GetStringValue())
                    {
                        objQualityProjectDetail.Condition = "approved";
                    }
                    if (objHeader != null && objHeader.Status == clsImplementationEnum.SeamListStatus.SentForApproval.GetStringValue())
                    {
                        objQualityProjectDetail.Condition = "sentforapproval";
                    }
                    else if (objHeader != null && (objHeader.Status == clsImplementationEnum.SeamListStatus.Draft.GetStringValue() || objHeader.Status == clsImplementationEnum.SeamListStatus.Returned.GetStringValue()))
                    {
                        objQualityProjectDetail.Condition = "edit";
                    }
                    else
                    {
                        string str = db.Database.SqlQuery<string>("SELECT dbo.FN_GENERATE_MAX_IPI_SEAMLIST_NO('" + project + "')").FirstOrDefault();
                        objQualityProjectDetail.Condition = "ok";
                        objQualityProjectDetail.SeamListNo = str.Split('@')[0].ToString();
                        objQualityProjectDetail.DocNo = Convert.ToInt32(str.Split('@')[1]);
                        objQualityProjectDetail.Status = clsImplementationEnum.SeamListStatus.Draft.GetStringValue();
                    }

                    if (objQualityProjectDetail.Condition == "approved" || objQualityProjectDetail.Condition == "edit")
                    {
                        objQualityProjectDetail.SeamListNo = objHeader.SeamListNo;
                        objQualityProjectDetail.HeaderId = objHeader.HeaderId;
                        objQualityProjectDetail.Status = objHeader.Status;
                        objQualityProjectDetail.DocNo = objHeader.DocNo;
                        objQualityProjectDetail.SeamListDescription = objHeader.SeamListDescription;
                        objQualityProjectDetail.ReviseRemark = objHeader.ReviseRemark;
                    }
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    throw;
                }
            }
            return Json(objQualityProjectDetail);
        }


        [HttpPost]
        public ActionResult GetProject(string qms)
        {   //get project and BU by Qms project

            if (!string.IsNullOrWhiteSpace(qms))
            {
                BUWiseDropdown objProjectDataModel = new BUWiseDropdown();
                var lstlocation = Manager.GetUserwiseLocation(objClsLoginInfo.UserName);
                var lstProject = (from a in db.QMS010
                                  where a.Project == qms && lstlocation.Contains(a.Location)// && a.Location == objClsLoginInfo.Location
                                  select new { ProjectDesc = a.QualityProject + " (" + a.Location.Trim() + ")" }).FirstOrDefault();
                if (lstProject != null)
                {
                    objProjectDataModel.Project = lstProject.ToString();
                    var lstquality = db.QMS010.Where(i => i.Project == qms && lstlocation.Contains(i.Location)).Select(a => new CategoryData { Value = a.QualityProject + " (" + a.Location.Trim() + ")", Code = a.QualityProject + "#" + a.Location.Trim(), CategoryDescription = a.QualityProject + " (" + a.Location.Trim() + ")" }).Distinct().ToList();
                    objProjectDataModel.Qualityproject = lstquality.Any() ? lstquality : null;
                    return Json(objProjectDataModel, JsonRequestBehavior.AllowGet);
                }
                else
                {

                    return Json("", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public class BUWiseDropdown : ProjectDataModel
        {
            public string Project { get; set; }
            public string Location { get; set; }


            public bool Key;
            public string Value;

            public List<CategoryData> Qualityproject { get; set; }


        }

        public class ProjectDataModel
        {
            public string BUDescription { get; set; }

        }
        public ActionResult loadHeaderDataTable(JQueryDataTableParamModel param, string status, string Project)
        {
            try
            {
                if (Project != null && Project != "")
                {
                    int? StartIndex = param.iDisplayStart + 1;
                    int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                    var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                    var IsDisplayOnly = !string.IsNullOrEmpty(Request["IsDisplayOnly"]) ? Convert.ToBoolean(Request["IsDisplayOnly"].ToString()) : false;
                    string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                    string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                    string whereCondition = "1=1";
                    string strSortOrder = string.Empty;
                    // whereCondition += " and CreatedBy = " + objClsLoginInfo.UserName;
                    whereCondition += " and Project='" + Project.ToString() + "'";
                    if (status.ToUpper() == "PENDING")
                    {
                        whereCondition += " and status in('" + clsImplementationEnum.SeamListStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.SeamListStatus.Returned.GetStringValue() + "')";
                    }

                    string[] columnName = { "qms.QualityProject", "qms.Project+'-'+com1.t_dsca", "qms.ContractNo+'-'+com4.t_desc", "com6.t_bpid+'-'+com6.t_nama", "Status", "SeamListNo" };
                    // whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                    if (!string.IsNullOrEmpty(param.sSearch))
                    {
                        whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                    }
                    else
                    {
                        whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                    }

                    if (!string.IsNullOrWhiteSpace(sortColumnName))
                    {
                        strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                    }


                    var lstHeader1 = db.SP_IPI_GETSEAMLISTHEADER(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                    lstHeader1 = lstHeader1.Where(x => x.Project.Contains(Project)).ToList();

                    int? totalRecords = lstHeader1.Select(i => i.TotalCount).FirstOrDefault();
                    var res = from h in lstHeader1
                              select new[] {
                           Convert.ToString(h.HeaderId),
                           h.Project,
                           Convert.ToString(h.HeaderId),
                           Convert.ToString(h.ContractNo),
                           Convert.ToString(h.Customer),
                           Convert.ToString(h.QualityProject),
                           Convert.ToString(h.Location),
                           Convert.ToString(h.SeamListNo),
                           Convert.ToString("R"+h.SeamListRev),
                           Convert.ToString(h.Status),
                             IsDisplayOnly? HTMLActionString(h.HeaderId,"","View","View lines","fa fa-eye","DisplayHeader(" + h.HeaderId+ "); ",true,true) + " " + HTMLActionString(h.HeaderId,"","Timeline","Show Timeline","fa fa-clock-o","ShowTimeline('/IPI/MaintainSeamList/ShowTimeline?HeaderID="+ h.HeaderId +"');",true,true) + " " + HTMLActionString(h.HeaderId,"","History","History Record","fa fa-history","ViewHistoryData('"+ Convert.ToInt32(h.HeaderId) +"');", true, true)
                           :HTMLActionString(h.HeaderId,"","View","View lines","fa fa-eye","EditHeader(" + h.HeaderId+ "); ",true,true) + " " + HTMLActionString(h.HeaderId,"","Timeline","Show Timeline","fa fa-clock-o","ShowTimeline('/IPI/MaintainSeamList/ShowTimeline?HeaderID="+ h.HeaderId +"');",true,true) + " " + HTMLActionString(h.HeaderId,"","History","History Record","fa fa-history","ViewHistoryData('"+ Convert.ToInt32(h.HeaderId) +"');", true, true)
                    };

                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                        strSortOrder = strSortOrder,
                        whereCondition = whereCondition
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    int? StartIndex = param.iDisplayStart + 1;
                    int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                    var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                    var IsDisplayOnly = !string.IsNullOrEmpty(Request["IsDisplayOnly"]) ? Convert.ToBoolean(Request["IsDisplayOnly"].ToString()) : false;
                    string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                    string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                    string whereCondition = "1=1";
                    string strSortOrder = string.Empty;
                    // whereCondition += " and CreatedBy = " + objClsLoginInfo.UserName;
                    if (status.ToUpper() == "PENDING")
                    {
                        whereCondition += " and status in('" + clsImplementationEnum.SeamListStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.SeamListStatus.Returned.GetStringValue() + "')";
                    }

                    string[] columnName = { "qms.QualityProject", "qms.Project+'-'+com1.t_dsca", "qms.ContractNo+'-'+com4.t_desc", "com6.t_bpid+'-'+com6.t_nama", "Status", "SeamListNo","SeamListRev" };
                    // whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                    if (!string.IsNullOrEmpty(param.sSearch))
                    {
                        whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                    }
                    else
                    {
                        whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                    }

                    if (!string.IsNullOrWhiteSpace(sortColumnName))
                    {
                        strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                    }


                    var lstHeader = db.SP_IPI_GETSEAMLISTHEADER(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                    int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                    var res = from h in lstHeader
                              select new[] {
                           Convert.ToString(h.HeaderId),
                           h.Project,
                           Convert.ToString(h.HeaderId),
                           Convert.ToString(h.ContractNo),
                           Convert.ToString(h.Customer),
                           Convert.ToString(h.QualityProject),
                           Convert.ToString(h.Location),
                           Convert.ToString(h.SeamListNo),
                           Convert.ToString("R"+h.SeamListRev),
                           Convert.ToString(h.Status),
                             IsDisplayOnly? HTMLActionString(h.HeaderId,"","View","View lines","fa fa-eye","DisplayHeader(" + h.HeaderId+ "); ",true,true) + " " + HTMLActionString(h.HeaderId,"","Timeline","Show Timeline","fa fa-clock-o","ShowTimeline('/IPI/MaintainSeamList/ShowTimeline?HeaderID="+ h.HeaderId +"');",true,true) + " " + HTMLActionString(h.HeaderId,"","History","History Record","fa fa-history","ViewHistoryData('"+ Convert.ToInt32(h.HeaderId) +"');", true, true)
                           :HTMLActionString(h.HeaderId,"","View","View lines","fa fa-eye","EditHeader(" + h.HeaderId+ "); ",true,true) + " " + HTMLActionString(h.HeaderId,"","Timeline","Show Timeline","fa fa-clock-o","ShowTimeline('/IPI/MaintainSeamList/ShowTimeline?HeaderID="+ h.HeaderId +"');",true,true) + " " + HTMLActionString(h.HeaderId,"","History","History Record","fa fa-history","ViewHistoryData('"+ Convert.ToInt32(h.HeaderId) +"');", true, true)
                    };

                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                        strSortOrder = strSortOrder,
                        whereCondition = whereCondition
                    }, JsonRequestBehavior.AllowGet);
                }


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.Title = "IPI";

            QMS011 objQMS011 = db.QMS011.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            model.ApprovedBy = Manager.GetUserNameFromPsNo(objQMS011.ApprovedBy);
            model.ApprovedOn = objQMS011.ApprovedOn;

            model.CreatedBy = Manager.GetUserNameFromPsNo(objQMS011.CreatedBy);
            model.CreatedOn = objQMS011.CreatedOn;

            model.ReturnedBy = Manager.GetUserNameFromPsNo(objQMS011.ReturnedBy);
            model.ReturnedOn = objQMS011.ReturnedOn;

            model.SubmittedBy = Manager.GetUserNameFromPsNo(objQMS011.SubmittedBy);
            model.SubmittedOn = objQMS011.SubmittedOn;

            return PartialView("~/Views/Shared/_TimelineProgress2.cshtml", model);
        }
        public ActionResult GetHeaderGridDataPartial(string status, string IsDisplayOnly, string Project)
        {
            ViewBag.Status = status;
            ViewBag.IsDisplayOnly = IsDisplayOnly;
            ViewBag.chkProject = Project;
            return PartialView("_GetHeaderGridDataPartial");
        }

        //Changed by 89385821 on 07-09-2017 to add Excel import Functionality
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult AddHeader(int? headerId, HttpPostedFileBase upload, int? HeaderId, string Project)
        {
            QMS011 objQMS011 = new QMS011();
            //if (upload != null && HeaderId.HasValue)
            //    ReadExcel(upload, HeaderId.Value);

            try
            {
                if (!string.IsNullOrEmpty(Project))
                {
                    ViewBag.Project = Manager.GetProjectAndDescription(Project);

                }
                if (headerId != null && headerId > 0)
                {
                    objQMS011 = db.QMS011.Where(x => x.HeaderId == headerId).FirstOrDefault();
                    ViewBag.Project = Manager.GetProjectAndDescription(objQMS011.Project);
                    ViewBag.Customer = db.COM006.Where(x => x.t_bpid == objQMS011.Customer).Select(x => x.t_bpid + "-" + x.t_nama).FirstOrDefault();
                    ViewBag.ContractNo = Manager.GetContractorAndDescription(objQMS011.ContractNo);
                    ViewBag.QualityProject = string.IsNullOrWhiteSpace(objQMS011.Location) ? objQMS011.QualityProject : objQMS011.QualityProject + " (" + objQMS011.Location + ")";
                    ViewBag.Action = "edit";
                }
                else
                {


                    objQMS011.HeaderId = 0;
                    objQMS011.Status = clsImplementationEnum.SeamListStatus.Draft.GetStringValue();
                    objQMS011.SeamListRev = 0;
                    objQMS011.DocNo = 0;
                    objQMS011.ReviseRemark = "First Revision";


                }
                ViewBag.IsDisplayOnly = false;
                ViewBag.chkProject = Project;
                TempData["VW_IPI_GtETHBOMLIST"] = null;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View(objQMS011);
        }
        [HttpPost]
        public async Task<ActionResult> EditHeader(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS011 objQMS011 = await db.QMS011.FirstOrDefaultAsync(x => x.HeaderId == headerId);
                if (objQMS011 != null)
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = objQMS011.Status;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Document Not Found";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
                //throw;
            }
            return Json(objResponseMsg);
        }

        [HttpPost]
        public ActionResult SaveHeader(FormCollection fc)
        {
            string status = clsImplementationEnum.SeamListStatus.Draft.GetStringValue();
            int SeamListRev = 0;
            QMS011 objQMS011 = new QMS011();
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (fc != null)
                {
                    int headerId = Convert.ToInt32(fc["HeaderId"]);
                    string LoginUser = objClsLoginInfo.UserName;
                    if (headerId > 0)
                    {
                        objQMS011 = db.QMS011.Where(i => i.HeaderId == headerId).FirstOrDefault();

                        if (objQMS011.Status == clsImplementationEnum.SeamListStatus.SentForApproval.GetStringValue())
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "This document is already submitted for approval. Please refresh the page.";
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }

                        if (objQMS011.Status != clsImplementationEnum.SeamListStatus.SentForApproval.GetStringValue())
                        {
                            objQMS011.Location = fc["Location"];
                            objQMS011.Customer = fc["Customer"].Split('-')[0].Trim();
                            objQMS011.ContractNo = fc["ContractNo"].Split('-')[0].Trim();
                            objQMS011.QualityProject = fc["QualityProject"];
                            objQMS011.Project = fc["Project"];
                            objQMS011.ReviseRemark = fc["ReviseRemark"];
                            objQMS011.SeamListNo = fc["SeamListNo"];
                            objQMS011.SeamListDescription = fc["SeamListDescription"];
                            objQMS011.DocNo = Convert.ToInt32(fc["DocNo"]);
                            if (objQMS011.Status == clsImplementationEnum.SeamListStatus.Approved.GetStringValue())
                            {
                                objQMS011.SeamListRev = objQMS011.SeamListRev + 1;
                            }
                            objQMS011.Status = clsImplementationEnum.SeamListStatus.Draft.GetStringValue();
                            objQMS011.EditedBy = LoginUser;
                            objQMS011.EditedOn = DateTime.Now;
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = objQMS011.HeaderId.ToString();
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "This Document is sent for approval, You can't edit now";
                        }
                        status = objQMS011.Status;
                        SeamListRev = Convert.ToInt32(objQMS011.SeamListRev);
                    }
                    else
                    {
                        objQMS011.Location = fc["Location"];
                        objQMS011.Customer = fc["Customer"].Split('-')[0].Trim();
                        objQMS011.ContractNo = fc["ContractNo"].Split('-')[0].Trim();
                        objQMS011.QualityProject = fc["QualityProject"];
                        objQMS011.Project = fc["Project"];
                        objQMS011.SeamListNo = fc["SeamListNo"];
                        objQMS011.SeamListDescription = fc["SeamListDescription"];
                        objQMS011.DocNo = Convert.ToInt32(fc["DocNo"]);
                        objQMS011.ReviseRemark = fc["ReviseRemark"];
                        objQMS011.Status = clsImplementationEnum.SeamListStatus.Draft.GetStringValue();
                        objQMS011.SeamListRev = 0;
                        objQMS011.CreatedBy = LoginUser;
                        objQMS011.CreatedOn = DateTime.Now;
                        db.QMS011.Add(objQMS011);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = objQMS011.HeaderId.ToString();
                        status = objQMS011.Status;
                        SeamListRev = Convert.ToInt32(objQMS011.SeamListRev);
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            var response = new { Key = objResponseMsg.Key, Value = objResponseMsg.Value, Status = status, SeamListRev = SeamListRev };
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SendForApprove(int headerId, string Project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            QMS011 objQMS011 = new QMS011();
            try
            {
                if (headerId > 0)
                {
                    string RA = clsImplementationEnum.AutoCJCSeamType.RA.GetStringValue();
                    objQMS011 = db.QMS011.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    List<QMS012> lstQMS012 = db.QMS012.Where(i => i.HeaderId == headerId).ToList();
                    if (lstQMS012 != null && lstQMS012.Count == 0)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Please add Seam in this document";
                    }
                    else if (objQMS011.Status == clsImplementationEnum.SeamListStatus.Returned.GetStringValue())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This document is returned, Please edit this document and then send for approval.";
                    }
                    else if (objQMS011.Status == clsImplementationEnum.SeamListStatus.Approved.GetStringValue())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This document is already approved.";
                    }
                    else if (objQMS011.Status == clsImplementationEnum.SeamListStatus.SentForApproval.GetStringValue())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This document is already submitted for approval. Please refresh the page.";
                    }
                    else if (db.QMS012.Any(x => x.SeamCategory == RA && (!x.SeamQuantity.HasValue) && x.HeaderId == headerId))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Please maintain Seam Quantity for seam category 'RA'";
                    }
                    else
                    {
                        objQMS011.Status = clsImplementationEnum.SeamListStatus.SentForApproval.GetStringValue();
                        objQMS011.SubmittedBy = objClsLoginInfo.UserName;
                        objQMS011.SubmittedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = objQMS011.SeamListNo + " Successfully Send For Approval";

                        //#region Send Mail
                        //Hashtable _ht = new Hashtable();
                        //EmailSend _objEmail = new EmailSend();
                        //_ht["[ApprovarName]"] = Manager.GetUserNameFromPsNo(objIMB001.ApprovedBy);
                        //_ht["[Document]"] = objIMB001.Document;
                        //_ht["[Name]"] = Manager.GetUserNameFromPsNo(objIMB001.CreatedBy);
                        //MAIL001 objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.IMB.SentforApproval).SingleOrDefault();
                        //_objEmail.MailToAdd = Manager.GetMailIdFromPsNo(objIMB001.ApprovedBy);
                        //_ht["[Subject]"] = objIMB001.Document + " Is Sent For Approval";
                        //_objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                        //#endregion
                    }
                    ViewBag.chkProject = Project;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg);
        }

        [HttpPost]
        public JsonResult CreateRevision(int strHeaderId, string strRemarks)
        {
            CustomResponceMsg objResponseMsg = new CustomResponceMsg();
            try
            {
                if (strHeaderId > 0)
                {
                    QMS011 objQMS011 = db.QMS011.Where(x => x.HeaderId == strHeaderId).FirstOrDefault();

                    objQMS011.Status = clsImplementationEnum.PAMStatus.Draft.GetStringValue();
                    objQMS011.SeamListRev += 1;
                    objQMS011.SubmittedBy = null;
                    objQMS011.SubmittedOn = null;
                    objQMS011.ApprovedBy = null;
                    objQMS011.ApprovedOn = null;
                    objQMS011.ReturnedBy = null;
                    objQMS011.ReturnedOn = null;
                    objQMS011.ReviseRemark = strRemarks;
                    objQMS011.EditedBy = objClsLoginInfo.UserName;
                    objQMS011.EditedOn = DateTime.Now;

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revision.ToString();
                    objResponseMsg.HeaderID = strHeaderId;
                    objResponseMsg.Status = objQMS011.Status;
                    objResponseMsg.rev = objQMS011.SeamListRev;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
                throw;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteSelectedHeader(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<string> NotDeletedSeamList = new List<string>();
                string[] arrayHeaderIds = strHeaderIds.Split(',');
                for (int i = 0; i < arrayHeaderIds.Length; i++)
                {
                    if (!string.IsNullOrWhiteSpace(arrayHeaderIds[i]))
                    {
                        int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                        QMS011 objQMS011 = db.QMS011.Where(c => c.HeaderId == HeaderId).FirstOrDefault();
                        if (objQMS011 != null)
                        {
                            if (objQMS011.SeamListRev == 0 && (objQMS011.Status == clsImplementationEnum.SeamListStatus.Draft.GetStringValue() || objQMS011.Status == clsImplementationEnum.SeamListStatus.Returned.GetStringValue()))
                            {
                                //delete logic                               
                                var objQMS012_LogList = db.QMS012_Log.Where(x => x.HeaderId == HeaderId).ToList();
                                if (objQMS012_LogList.Count > 0)
                                {
                                    db.QMS012_Log.RemoveRange(objQMS012_LogList);
                                }

                                var objQMS011_LogList = db.QMS011_Log.Where(x => x.HeaderId == HeaderId).ToList();
                                if (objQMS011_LogList.Count > 0)
                                {
                                    db.QMS011_Log.RemoveRange(objQMS011_LogList);
                                }

                                var objQMS012List = db.QMS012.Where(x => x.HeaderId == HeaderId).ToList();
                                if (objQMS012List.Count > 0)
                                {
                                    db.QMS012.RemoveRange(objQMS012List);
                                }

                                db.QMS011.Remove(objQMS011);
                                db.SaveChanges();
                            }
                            else
                            {
                                NotDeletedSeamList.Add(objQMS011.SeamListNo);
                            }
                        }
                    }
                }
                if (NotDeletedSeamList.Count > 0)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Seam List No(s): " + string.Join(",", NotDeletedSeamList) + " are not deleted. since they have been already attended. Please refresh the page.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Lines

        public JsonResult GetPosition(string param)
        {
            try
            {
                //var result = db.VW_IPI_GETHBOMLIST.Where(x => x.Project == param).Select(x => x.FindNo).ToList();
                var result = Manager.GetHBOMData(param).Select(x => x.FindNo).ToList();

                var items = (from li in result
                             select new
                             {
                                 id = li.ToString(),
                                 text = li.ToString()
                             });

                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult FatchSeamDescription(string weldType)
        {
            try
            {
                var data = (from c in db.BOM003
                            where c.JointTypeCode == weldType
                            select new CategoryData()
                            {
                                Code = c.SeamCategory,
                                CategoryDescription = c.SeamCategoryDescription,
                            }).ToList();
                data.ForEach(x => { x.Code = ReplaceNewLine(x.Code); x.CategoryDescription = ReplaceNewLine(x.CategoryDescription); });
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetUnitByWeldTypeAndCatDesc(string weldType, string catDesc)
        {
            try
            {
                var data = db.BOM003.Where(x => x.JointTypeCode == weldType && x.SeamCategory == catDesc).Select(x => x.Unit).FirstOrDefault();

                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AddLinesPartial(int headerId, int? lineId, string project, string qualityProject)
        {
            string buCode = db.COM001.Where(i => i.t_cprj == project).FirstOrDefault().t_entu;

            List<GLB002> lstWeldType = Manager.GetSubCatagories("Joint Type", buCode, objClsLoginInfo.Location).ToList();
            List<GLB002> lstWEP = Manager.GetSubCatagories("WEP Detail Code", buCode, objClsLoginInfo.Location).ToList();
            List<GLB002> lstSeamMaster = Manager.GetSubCatagories("Seam Master", buCode, objClsLoginInfo.Location).ToList();
            ViewBag.WeldType = lstWeldType.AsEnumerable().Select(x => new CategoryData { Code = x.Code, CategoryDescription = x.Code + "-" + x.Description }).ToList();
            ViewBag.WEP = lstWEP.AsEnumerable().Select(x => new CategoryData { Code = x.Code, CategoryDescription = x.Code + "-" + x.Description }).ToList();
            ViewBag.SeamMaster = lstSeamMaster.AsEnumerable().Select(x => new CategoryData { Code = x.Code, CategoryDescription = x.Code + "-" + x.Description }).ToList();

            ViewBag.Project = project;
            QMS012 objQMS012 = new QMS012();
            if (lineId != null && lineId > 0)
            {
                objQMS012 = db.QMS012.FirstOrDefault(x => x.HeaderId == headerId && x.LineId == lineId);

                ViewBag.SeamCategory = (from li in lstSeamMaster
                                        where li.Code == objQMS012.SeamCategory
                                        select li.Code + "-" + li.Description).FirstOrDefault();
                ViewBag.WEPDetail = (from li in lstWEP
                                     where li.Code == objQMS012.WEPDetail
                                     select li.Code + "-" + li.Description).FirstOrDefault();
                ViewBag.WeldType = (from li in lstWeldType
                                    where li.Code == objQMS012.WeldType
                                    select li.Code + "-" + li.Description).FirstOrDefault();
                ViewBag.Action = "edit";
            }
            else
            {
                objQMS012.HeaderId = headerId;
                objQMS012.Project = project;
                objQMS012.QualityProject = qualityProject;
                objQMS012.SeamRev = 0;

            }
            return PartialView("_AddLinesPartial", objQMS012);
        }

        public ActionResult FetchLinesData(int HeaderId, string IsDisplayOnly)
        {
            QMS011 objQMS011 = db.QMS011.FirstOrDefault(x => x.HeaderId == HeaderId);
            ViewBag.lstJointType = (from c in db.BOM003
                                    group c by new
                                    {
                                        c.JointTypeCode,
                                        c.JointTypeDescription,
                                    } into gcs
                                    select new CategoryData()
                                    {
                                        Code = gcs.Key.JointTypeCode,
                                        CategoryDescription = gcs.Key.JointTypeDescription,
                                    }).ToList();

            string buCode = db.COM001.Where(i => i.t_cprj == objQMS011.Project).FirstOrDefault().t_entu;
            List<GLB002> lstWEP = Manager.GetSubCatagories("WEP Detail Code", buCode, objClsLoginInfo.Location).ToList();
            ViewBag.WEP = lstWEP.AsEnumerable().Select(x => new CategoryData { Code = x.Code, CategoryDescription = x.Code + "-" + x.Description }).ToList();
            ViewBag.IsDisplayOnly = IsDisplayOnly;

            return PartialView("_LinesGridPartial", objQMS011);
        }

        public ActionResult loadLinesDataTable(JQueryDataTableParamModel param, string Project)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                var objQMS011 = db.QMS011.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                var IsDisplayOnly = param.IsDisplayOnly;// !string.IsNullOrEmpty(Request["IsDisplayOnly"]) ? Convert.ToBoolean(Request["IsDisplayOnly"].ToString()) : false;
                //var result = db.VW_IPI_GETHBOMLIST.Where(x => x.Project == Project).Select(x => x.FindNo).Distinct().ToList();
                var isEditable = true;

                if (IsDisplayOnly || objQMS011.Status.ToLower() == clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue().ToLower() || objQMS011.Status.ToLower() == clsImplementationEnum.PAMStatus.Approved.GetStringValue().ToLower())
                    isEditable = false;

                /*var items = (from li in result
                             select new SelectItemList
                             {
                                 id = li.ToString(),
                                 text = li.ToString()
                             }).ToList();*/
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;

                string whereCondition = "qms.HeaderId = " + Convert.ToInt32(param.CTQHeaderId);
                string[] columnName = { "qms.SeamNo", "qms.SeamDescription", "qms.Position", "qms.Thickness", "qms.Material", "qms.ViewDrawing", "qms.WeldType", "qms.SeamLengthArea", "qms.WEPDetail", "qms.Remarks", "qms.AssemblyNo" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                else
                {
                    strSortOrder = " Order By EditedOn desc ";
                }

                var lstLines = db.SP_IPI_GETSEAMLISTLINES(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();//.OrderBy(x => x.ManualSeamNo).ToList();
                int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();

                #region inline grid code developed by Ajay Chauhan
                /*int newRecordId = 0;
                var newRecord = new[] {
                                    "",
                                    Helper.GenerateHidden(newRecordId, "HeaderId", Convert.ToString(param.CTQHeaderId)),
                                    "",
                                    GenerateAutoComplete(newRecordId, "WeldType","","", "", false,"","WeldType",false)+""+Helper.GenerateHidden(newRecordId, "hdnWeldType", Convert.ToString(newRecordId)),
                                    GenerateAutoComplete(newRecordId, "SeamDescription","","UpdateData(this, "+ newRecordId +");","", false,"","SeamDescription",false)+""+Helper.GenerateHidden(newRecordId, "hdnSeamDescription",Convert.ToString(newRecordId)),
                                    GenerateTextboxFor(newRecordId,"ManualSeamNo","","CalculateSeamNo(this, "+ newRecordId +");","",false,"","14",false,"","",""),
                                    GenerateTextboxFor(newRecordId,"SeamNo","","","",true,"","30",false,"","",""),
                                    GenerateTextboxFor(newRecordId,"SeamRev","R0","","",true,"","5",false,"","",""),
                                    MultiSelectDropdown(items,newRecordId,"Position",false,"","","Position"),
                                    GenerateAutoComplete(newRecordId, "AssemblyNo","","UpdateData(this, "+ newRecordId +");", "", false,"","AssemblyNo",false)+""+Helper.GenerateHidden(newRecordId, "hdnAssemblyNo",Convert.ToString(newRecordId)),
                                    GenerateTextboxFor(newRecordId,"Thickness","","","",false,"","500",false,"","",""),
                                    GenerateTextboxFor(newRecordId,"ThicknessDefault","","","",true,"","500",false,"","",""),
                                    GenerateTextboxFor(newRecordId,"Material","","","",true,"","1000",false,"","",""),
                                    GenerateTextboxFor(newRecordId,"Unit","","","",true,"","5",false,"","",""),
                                    GenerateAutoComplete(newRecordId, "WEPDetail","","", "",false,"","WEPDetail",false)+""+Helper.GenerateHidden(newRecordId, "hdnWEPDetail",string.Empty),
                                    GenerateTextboxFor(newRecordId,"ViewDrawing","","","",false,"","25",false,"","",""),
                                    GenerateTextboxFor(newRecordId,"SeamLengthArea","","","",false,"","100",false,"","",""),
                                    GenerateTextboxFor(newRecordId,"Remarks","","","",false,"","500",false,"","",""),
                                    HTMLActionString(newRecordId,"","Add","Add New Record","fa fa-plus","SaveNewRecord();"),
                                };*/

                var res = (from c in lstLines
                           select new[] {
                                        Convert.ToString(c.ROW_NO),
                                        Convert.ToString(c.HeaderId),//Helper.GenerateHidden(c.HeaderId, "HeaderId", Convert.ToString(c.HeaderId)),
                                        Convert.ToString(c.LineId), //Helper.GenerateHidden(c.LineId, "LineId", Convert.ToString(c.LineId)),
                                        c.WeldType, //GenerateAutoComplete(c.LineId, "WeldType",c.WeldType,"UpdateData(this, "+ c.LineId +");", "", false,"","WeldType", true)+""+Helper.GenerateHidden(c.LineId, "hdnWeldType",c.WeldType),
                                        c.SeamDescription, //GenerateAutoComplete(c.LineId, "SeamDescription",c.SeamDescription,"UpdateData(this, "+ c.LineId +");","",false,"","SeamDescription", true)+""+Helper.GenerateHidden(c.LineId, "hdnSeamDescription",c.SeamDescription),
                                        c.ManualSeamNo, //GenerateTextboxFor(c.LineId,"ManualSeamNo",c.ManualSeamNo,"UpdateData(this, "+ c.LineId +");","", false,"","14",true,"","",""),
                                        c.SeamNo, //GenerateTextboxFor(c.LineId,"SeamNo",c.SeamNo,"UpdateData(this, "+ c.LineId +");","", true,"","30",false,"","",""),
                                        "R"+c.SeamRev, //  GenerateTextboxFor(c.LineId,"SeamRev",Convert.ToString("R"+c.SeamRev),"UpdateData(this, "+ c.LineId +");","", true,"","5",false,"","",""),
                                        c.Position, //MultiSelectDropdown(items,c.LineId,c.Position,true,"","UpdateData(this, "+ c.LineId +");","Position"),
                                        c.AssemblyNo, //GenerateAutoComplete(c.LineId, "AssemblyNo",c.AssemblyNo,"UpdateData(this, "+ c.LineId +");", "",false,"","AssemblyNo", true)+""+Helper.GenerateHidden(c.LineId, "hdnAssemblyNo",c.AssemblyNo),
                                        c.Thickness, //GenerateTextboxFor(c.LineId,"Thickness",c.Thickness,"UpdateData(this, "+ c.LineId +");","", true,"","1000",false,c.AssemblyNo, c.Project, c.Position),
                                        c.ThicknessDefault, //GenerateTextboxFor(c.LineId,"ThicknessDefault",c.ThicknessDefault,"UpdateData(this, "+ c.LineId +");","", true,"","1000",false,c.AssemblyNo, c.Project, c.Position),
                                        c.Material, //GenerateTextboxFor(c.LineId,"Material",c.Material,"UpdateData(this, "+ c.LineId +");","", true,"","1000",false,"","",""),
                                        c.Unit, //GenerateTextboxFor(c.LineId,"Unit",c.Unit,"","", true,"UpdateData(this, "+ c.LineId +");","5",false,"","",""),
                                        c.WEPDetail, //GenerateAutoComplete(c.LineId, "WEPDetail",c.WEPDetail,"UpdateData(this, "+ c.LineId +");", "",false,"","WEPDetail", true)+""+Helper.GenerateHidden(c.LineId, "hdnWEPDetail",c.WEPDetail),
                                        c.ViewDrawing, //GenerateTextboxFor(c.LineId,"ViewDrawing",c.ViewDrawing,"UpdateData(this, "+ c.LineId +");","", false,"","25",true,"","",""),
                                        Convert.ToString(c.SeamLengthArea), //GenerateTextboxFor(c.LineId,"SeamLengthArea",Convert.ToString(c.SeamLengthArea),"UpdateData(this, "+ c.LineId +");","", false,"","100",true,"","",""),
                                        c.Remarks, //GenerateTextboxFor(c.LineId,"Remarks",c.Remarks,"UpdateData(this, "+ c.LineId +");","", false,"","500",true,"","",""),
                                        Manager.generateSeamHistoryButton(c.Project,c.QualityProject, c.SeamNo,"Seam Details") +" "+
                                        (IsDisplayOnly ?"" :  ( objQMS011.Status.ToLower() != clsImplementationEnum.PAMStatus.Draft.GetStringValue().ToLower() && objQMS011.Status.ToLower() != clsImplementationEnum.PAMStatus.Returned.GetStringValue().ToLower() ? HTMLActionString(c.LineId,"","Edit","Edit Record","fa fa-pencil-square-o disabledicon","") + " " + HTMLActionString(c.LineId,"","Delete","Delete Record","fa fa-trash-o disabledicon","") + " " + HTMLActionString(c.LineId, "","Revise", "Revise", "fa fa-repeat disabledicon", "") : (new clsManager()).GetMaxOfferedSequenceNo(c.QualityProject, c.BU, c.Location, c.SeamNo) > 0 ? HTMLActionString(c.LineId,"","Edit","Edit Record","fa fa-pencil-square-o disabledicon","") + " " + HTMLActionString(c.LineId,"","Delete","Delete Record","fa fa-trash-o disabledicon","") + " " + HTMLActionString(c.LineId, "","Revise", "Revise", "fa fa-repeat disabledicon", "")  :
                                        (isEditable ? (c.Status.ToLower() == clsImplementationEnum.PAMStatus.Approved.GetStringValue().ToLower() ? HTMLActionString(c.LineId,"","Edit","Edit Record","fa fa-pencil-square-o disabledicon","") : HTMLActionString(c.LineId,"","Edit","Edit Record","fa fa-pencil-square-o","EnabledEditLine(" + c.LineId + "); ") + HTMLActionString(c.LineId,"","Update","Update Record","fa fa-floppy-o","EditLine(" + c.LineId + "); ", false) + " " + HTMLActionString(c.LineId,"","Cancel","Cancel Edit","fa fa-ban","CancelEditLine(" + c.LineId + "); ", false))+ " " + HTMLActionString(c.LineId,"","Delete","Delete Record","fa fa-trash-o","DeleteLine("+ c.HeaderId+ "," + c.LineId +");") + " " + (c.Status.ToLower() == clsImplementationEnum.PAMStatus.Approved.GetStringValue().ToLower() ? HTMLActionString(c.LineId, "","Revise", "Revise", "fa fa-repeat", "LineRevision("+c.LineId+");") : HTMLActionString(c.LineId, "","Revise", "Revise", "fa fa-repeat disabledicon", "")) : HTMLActionString(c.LineId,"","Edit","Edit Record","fa fa-pencil-square-o disabledicon","") + " " + HTMLActionString(c.LineId,"","Delete","Delete Record","fa fa-trash-o disabledicon","") + " " + HTMLActionString(c.LineId, "","Revise", "Revise", "fa fa-repeat disabledicon", "")))
                                        )
                                        + " " + ( c.SeamDescription == clsImplementationEnum.AutoCJCSeamType.RA.GetStringValue()? HTMLActionString(c.LineId, "","Seam Quantity", "Seam Quantity", "fa fa-reorder","LoadSeamQuantityPartial("+c.LineId+",'"+(isEditable && c.Status.ToLower() != clsImplementationEnum.PAMStatus.Approved.GetStringValue().ToLower())+"')"):HTMLActionString(c.LineId, "","Seam Quantity", "Seam Quantity", "fa fa-reorder disabledicon",""))
                          }).ToList();
                // res.Insert(0, newRecord);
                #endregion
                #region test
                //if (objQMS011.Status.ToLower() != clsImplementationEnum.PAMStatus.Draft.GetStringValue().ToLower() && objQMS011.Status.ToLower() != clsImplementationEnum.PAMStatus.Returned.GetStringValue().ToLower())
                //{
                //    HTMLActionString(c.LineId, "", "Edit", "Edit Record", "fa fa-pencil-square-o disabledicon", "") + " " + HTMLActionString(c.LineId, "", "Delete", "Delete Record", "fa fa-trash-o disabledicon", "") + " " + HTMLActionString(c.LineId, "", "Revise", "Revise", "fa fa-repeat disabledicon", "")
                //}
                //else
                //{
                //    if ((new clsManager()).GetMaxOfferedSequenceNo(c.QualityProject, c.BU, c.Location, c.SeamNo) > 0)
                //    {
                //        HTMLActionString(c.LineId, "", "Edit", "Edit Record", "fa fa-pencil-square-o disabledicon", "") + " " + HTMLActionString(c.LineId, "", "Delete", "Delete Record", "fa fa-trash-o disabledicon", "") + " " + HTMLActionString(c.LineId, "", "Revise", "Revise", "fa fa-repeat disabledicon", "")
                //    }
                //    else
                //    {
                //        if (isEditable)
                //        {
                //            if (c.Status.ToLower() == clsImplementationEnum.PAMStatus.Approved.GetStringValue().ToLower())
                //            {
                //                HTMLActionString(c.LineId, "", "Edit", "Edit Record", "fa fa-pencil-square-o disabledicon", "")
                //            }
                //            else
                //            {
                //                HTMLActionString(c.LineId, "", "Edit", "Edit Record", "fa fa-pencil-square-o", "EnabledEditLine(" + c.LineId + "); ") + HTMLActionString(c.LineId, "", "Update", "Update Record", "fa fa-floppy-o", "EditLine(" + c.LineId + "); ", false) + " " + HTMLActionString(c.LineId, "", "Cancel", "Cancel Edit", "fa fa-ban", "CancelEditLine(" + c.LineId + "); ", false)) +" " + HTMLActionString(c.LineId, "", "Delete", "Delete Record", "fa fa-trash-o", "DeleteLine(" + c.HeaderId + "," + c.LineId + ");")
                //                if (c.Status.ToLower() == clsImplementationEnum.PAMStatus.Approved.GetStringValue().ToLower())
                //                {
                //                    HTMLActionString(c.LineId, "", "Revise", "Revise", "fa fa-repeat", "LineRevision(" + c.LineId + ");")
                //                }
                //                else
                //                {
                //                    HTMLActionString(c.LineId, "", "Revise", "Revise", "fa fa-repeat disabledicon", "")
                //                }
                //            }
                //        }
                //        else
                //        {
                //            HTMLActionString(c.LineId, "", "Edit", "Edit Record", "fa fa-pencil-square-o disabledicon", "") + " " + HTMLActionString(c.LineId, "", "Delete", "Delete Record", "fa fa-trash-o disabledicon", "") + " " + HTMLActionString(c.LineId, "", "Revise", "Revise", "fa fa-repeat disabledicon", "")
                //        }
                //    }
                //}
                #endregion
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""

                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetInlineEditableRow(int id, int HeaderId, string Project, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();
                var result = new List<string>();
                if (TempData["VW_IPI_GtETHBOMLIST"] == null)
                    //result = db.VW_IPI_GETHBOMLIST.Where(x => x.Project == Project).Select(x => x.FindNo).Distinct().ToList();
                    result = Manager.GetHBOMData(Project).Select(x => x.FindNo).ToList();
                else
                    result = (List<string>)TempData["VW_IPI_GtETHBOMLIST"];
                TempData["VW_IPI_GtETHBOMLIST"] = result;

                var items = (from li in result
                             select new SelectItemList
                             {
                                 id = li.ToString(),
                                 text = li.ToString()
                             }).ToList();

                if (id == 0)
                {
                    int newRecordId = 0;
                    var newRecord = new[] {
                                    clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                    Helper.GenerateHidden(newRecordId, "HeaderId", Convert.ToString(HeaderId)),
                                    clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                    GenerateAutoComplete(newRecordId, "WeldType","","", "", false,"","WeldType",false)+""+Helper.GenerateHidden(newRecordId, "hdnWeldType", Convert.ToString(newRecordId)),
                                    GenerateAutoComplete(newRecordId, "SeamDescription","","UpdateData(this, "+ newRecordId +");","", false,"","SeamDescription",false)+""+Helper.GenerateHidden(newRecordId, "hdnSeamDescription",Convert.ToString(newRecordId)),
                                    GenerateTextboxFor(newRecordId,"ManualSeamNo","","CalculateSeamNo(this, "+ newRecordId +");","",false,"","14",false,"","",""),
                                    GenerateTextboxFor(newRecordId,"SeamNo","","","",true,"","30",false,"","",""),
                                    GenerateTextboxFor(newRecordId,"SeamRev","R0","","",true,"","5",false,"","",""),
                                    MultiSelectDropdown(items,newRecordId,"Position",false,"","","Position"),
                                    GenerateAutoComplete(newRecordId, "AssemblyNo","","UpdateData(this, "+ newRecordId +");", "", false,"","AssemblyNo",false)+""+Helper.GenerateHidden(newRecordId, "hdnAssemblyNo",Convert.ToString(newRecordId)),
                                    GenerateTextboxFor(newRecordId,"Thickness","","","",false,"","500",false,"","",""),
                                    GenerateTextboxFor(newRecordId,"ThicknessDefault","","","",true,"","500",false,"","",""),
                                    GenerateTextboxFor(newRecordId,"Material","","","",true,"","1000",false,"","",""),
                                    GenerateTextboxFor(newRecordId,"Unit","","","",true,"","5",false,"","",""),
                                    GenerateAutoComplete(newRecordId, "WEPDetail","","", "",false,"","WEPDetail",false)+""+Helper.GenerateHidden(newRecordId, "hdnWEPDetail",string.Empty),
                                    GenerateTextboxFor(newRecordId,"ViewDrawing","","","",false,"","25",false,"","",""),
                                    GenerateTextboxFor(newRecordId,"SeamLengthArea","","","",false,"","",false,"","",""),
                                    GenerateTextboxFor(newRecordId,"Remarks","","","",false,"","500",false,"","",""),
                                    HTMLActionString(newRecordId,"","Add","Add New Record","fa fa-plus","SaveNewRecord();"),
                                };
                    data.Add(newRecord);
                }
                else
                {
                    var objQMS011 = db.QMS011.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    var IsDisplayOnly = !string.IsNullOrEmpty(Request["IsDisplayOnly"]) ? Convert.ToBoolean(Request["IsDisplayOnly"].ToString()) : false;
                    var isEditable = true;

                    if (objQMS011.Status.ToLower() == clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue().ToLower() || objQMS011.Status.ToLower() == clsImplementationEnum.PAMStatus.Approved.GetStringValue().ToLower())
                    {
                        isEditable = false;
                    }

                    var lstLines = db.SP_IPI_GETSEAMLISTLINES(1, 0, "", "qms.LineId = " + id).Take(1).ToList();
                    string str;
                    data = (from c in lstLines
                            select new[] {
                                    clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//Convert.ToString(c.ROW_NO),
                                    isReadOnly? Convert.ToString(c.HeaderId): Helper.GenerateHidden(c.LineId, "HeaderId", Convert.ToString(c.HeaderId)),
                                    isReadOnly? Convert.ToString(c.LineId):Helper.GenerateHidden(c.LineId, "LineId", Convert.ToString(c.LineId)),
                                    isReadOnly? c.WeldType: IsApplicableForUpdate(c.QualityProject,c.SeamNo,out str)==true ? GenerateAutoComplete(c.LineId, "WeldType",c.WeldType,"UpdateData(this, "+ c.LineId +");", "", false,"","WeldType", false)+""+Helper.GenerateHidden(c.LineId, "hdnWeldType",c.WeldType) : c.WeldType+""+Helper.GenerateHidden(c.LineId, "hdnWeldType",c.WeldType),
                                    isReadOnly? c.SeamDescription:IsApplicableForUpdate(c.QualityProject,c.SeamNo,out str)==true ? GenerateAutoComplete(c.LineId, "SeamDescription",c.SeamDescription,"UpdateData(this, "+ c.LineId +");CalculateSeamNo(this, "+ c.LineId +");","",false,"","SeamDescription", false)+""+Helper.GenerateHidden(c.LineId, "hdnSeamDescription",c.SeamDescription): c.SeamDescription+""+Helper.GenerateHidden(c.LineId, "hdnSeamDescription",c.SeamDescription),
                                    //c.WeldType+""+Helper.GenerateHidden(c.LineId, "hdnWeldType",c.WeldType),
                                    //GenerateAutoComplete(c.LineId, "WeldType",c.WeldType,"UpdateData(this, "+ c.LineId +");", "", false,"","WeldType", false)+""+Helper.GenerateHidden(c.LineId, "hdnWeldType",c.WeldType),
                                    //c.SeamDescription+""+Helper.GenerateHidden(c.LineId, "hdnSeamDescription",c.SeamDescription),
                                    //GenerateAutoComplete(c.LineId, "SeamDescription",c.SeamDescription,"UpdateData(this, "+ c.LineId +");","",false,"","SeamDescription", false)+""+Helper.GenerateHidden(c.LineId, "hdnSeamDescription",c.SeamDescription),
                                    //GenerateAutoComplete(c.LineId, "SeamDescription",c.SeamDescription,"UpdateData(this, "+ c.LineId +");","",false,"","SeamDescription", true),
                                    c.ManualSeamNo,// : GenerateTextboxFor(c.LineId,"ManualSeamNo",c.ManualSeamNo,"UpdateData(this, "+ c.LineId +");","", false,"","14",true,"","",""),
                                    Helper.GenerateHidden(c.LineId, "hdnSeamNo",c.SeamNo)+""+c.SeamNo,// : GenerateTextboxFor(c.LineId,"SeamNo",c.SeamNo,"UpdateData(this, "+ c.LineId +");","", true,"","30",false,"","",""),
                                    "R"+c.SeamRev,// : GenerateTextboxFor(c.LineId,"SeamRev",Convert.ToString("R"+c.SeamRev),"UpdateData(this, "+ c.LineId +");","", true,"","5",false,"","",""),
                                    isReadOnly? c.Position : MultiSelectDropdown(items,c.LineId,c.Position,true,"","UpdateData(this, "+ c.LineId +");","Position"),
                                    isReadOnly? c.AssemblyNo : GenerateAutoComplete(c.LineId, "AssemblyNo",c.AssemblyNo,"UpdateData(this, "+ c.LineId +");", "",false,"","AssemblyNo", true)+""+Helper.GenerateHidden(c.LineId, "hdnAssemblyNo",c.AssemblyNo),
                                    isReadOnly? c.Thickness : GenerateTextboxFor(c.LineId,"Thickness",c.Thickness,"UpdateData(this, "+ c.LineId +");","", true,"","1000",false,c.AssemblyNo, c.Project, c.Position),
                                    isReadOnly? c.ThicknessDefault : GenerateTextboxFor(c.LineId,"ThicknessDefault",c.ThicknessDefault,"UpdateData(this, "+ c.LineId +");","", true,"","1000",false,c.AssemblyNo, c.Project, c.Position),
                                    isReadOnly? c.Material : GenerateTextboxFor(c.LineId,"Material",c.Material,"UpdateData(this, "+ c.LineId +");","", true,"","1000",false,"","",""),
                                    c.Unit,// : GenerateTextboxFor(c.LineId,"Unit",c.Unit,"","", true,"UpdateData(this, "+ c.LineId +");","5",false,"","",""),
                                    isReadOnly? c.WEPDetail : GenerateAutoComplete(c.LineId, "WEPDetail",c.WEPDetail,"UpdateData(this, "+ c.LineId +");", "",false,"","WEPDetail", true)+""+Helper.GenerateHidden(c.LineId, "hdnWEPDetail",c.WEPDetail),
                                    isReadOnly? c.ViewDrawing : GenerateTextboxFor(c.LineId,"ViewDrawing",c.ViewDrawing,"UpdateData(this, "+ c.LineId +");","", false,"","25",true,"","",""),
                                    isReadOnly? Convert.ToString(c.SeamLengthArea): GenerateTextboxFor(c.LineId,"SeamLengthArea",Convert.ToString(c.SeamLengthArea),"UpdateData(this, "+ c.LineId +");","", false,"","",true,"","",""),
                                    isReadOnly? c.Remarks : GenerateTextboxFor(c.LineId,"Remarks",c.Remarks,"UpdateData(this, "+ c.LineId +");","", false,"","500",true,"","",""),
                                    Manager.generateSeamHistoryButton(c.Project,c.QualityProject, c.SeamNo,"Seam Details") +" "+ ( objQMS011.Status.ToLower() != clsImplementationEnum.PAMStatus.Draft.GetStringValue().ToLower() && objQMS011.Status.ToLower() != clsImplementationEnum.PAMStatus.Returned.GetStringValue().ToLower() ? HTMLActionString(c.LineId,"","Edit","Edit Record","fa fa-pencil-square-o disabledicon","") + " " + HTMLActionString(c.LineId,"","Delete","Delete Record","fa fa-trash-o disabledicon","") + " " + HTMLActionString(c.LineId, "","Revise", "Revise", "fa fa-repeat disabledicon", "") : (new clsManager()).GetMaxOfferedSequenceNo(c.QualityProject, c.BU, c.Location, c.SeamNo) > 0 ? HTMLActionString(c.LineId,"","Edit","Edit Record","fa fa-pencil-square-o disabledicon","") + " " + HTMLActionString(c.LineId,"","Delete","Delete Record","fa fa-trash-o disabledicon","") + " " + HTMLActionString(c.LineId, "","Revise", "Revise", "fa fa-repeat disabledicon", "") : (isEditable ? (c.Status.ToLower() == clsImplementationEnum.PAMStatus.Approved.GetStringValue().ToLower() ? HTMLActionString(c.LineId,"","Edit","Edit Record","fa fa-pencil-square-o disabledicon","") : HTMLActionString(c.LineId,"","Edit","Edit Record","fa fa-pencil-square-o","EnabledEditLine(" + c.LineId + "); ") + HTMLActionString(c.LineId,"","Update","Update Record","fa fa-floppy-o","EditLine(" + c.LineId + "); ", false) + " " + HTMLActionString(c.LineId,"","Cancel","Cancel Edit","fa fa-ban","CancelEditLine(" + c.LineId + "); ", false))+ " " + HTMLActionString(c.LineId,"","Delete","Delete Record","fa fa-trash-o","DeleteLine("+ c.HeaderId+ "," + c.LineId +");") + " " + (c.Status.ToLower() == clsImplementationEnum.PAMStatus.Approved.GetStringValue().ToLower() ? HTMLActionString(c.LineId, "","Revise", "Revise", "fa fa-repeat", "LineRevision("+c.LineId+");") : HTMLActionString(c.LineId, "","Revise", "Revise", "fa fa-repeat disabledicon", "")) : HTMLActionString(c.LineId,"","Edit","Edit Record","fa fa-pencil-square-o disabledicon","") + " " + HTMLActionString(c.LineId,"","Delete","Delete Record","fa fa-trash-o disabledicon","") + " " + HTMLActionString(c.LineId, "","Revise", "Revise", "fa fa-repeat disabledicon", "")))
                                    + " " + (c.SeamDescription == clsImplementationEnum.AutoCJCSeamType.RA.GetStringValue()? HTMLActionString(c.LineId, "","Seam Quantity", "Seam Quantity", "fa fa-reorder","LoadSeamQuantityPartial("+c.LineId+",'"+(isEditable && !isReadOnly && c.Status.ToLower() != clsImplementationEnum.PAMStatus.Approved.GetStringValue().ToLower())+"')"):HTMLActionString(c.LineId, "","Seam Quantity", "Seam Quantity", "fa fa-reorder disabledicon",""))
                        }).ToList();

                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveLines(FormCollection fc, int LineId)
        {
            int newRowIndex = 0;
            string status = clsImplementationEnum.SeamListStatus.Draft.GetStringValue();
            int SeamListRev = 0;
            QMS012 objQMS012 = objQMS012 = new QMS012();
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (fc != null)
                {
                    int headerId = Convert.ToInt32(fc["HeaderId" + LineId]); //Convert.ToInt32(fc["HeaderId" + newRowIndex]);
                    QMS011 objQMS011 = new QMS011();
                    if (headerId > 0)
                    {
                        objQMS011 = db.QMS011.Where(i => i.HeaderId == headerId).FirstOrDefault();
                        if (objQMS011.Status != clsImplementationEnum.SeamListStatus.SentForApproval.GetStringValue() || objQMS011.ApprovedBy == objClsLoginInfo.UserName)
                        {
                            string LoginUser = objClsLoginInfo.UserName;
                            //int lineId = Convert.ToInt32(fc["LineId" + newRowIndex]);
                            if (LineId > 0)
                            {
                                objQMS012 = db.QMS012.Where(x => x.LineId == LineId).FirstOrDefault();
                                objQMS012.Location = objQMS011.Location;

                                if (objQMS012.WeldType != fc["hdnWeldType" + LineId])
                                {
                                    objQMS012.OldWeldType = objQMS012.WeldType;
                                    objQMS012.WeldType = fc["hdnWeldType" + LineId];
                                }
                                if (objQMS012.SeamCategory != fc["hdnSeamDescription" + LineId])
                                {
                                    objQMS012.OldSeamCategory = objQMS012.SeamCategory;
                                    objQMS012.SeamCategory = fc["hdnSeamDescription" + LineId];
                                    objQMS012.SeamDescription = fc["hdnSeamDescription" + LineId];
                                    objQMS012.SeamNo = fc["hdnSeamNo" + LineId];
                                }


                                objQMS012.Position = fc["ddlmultiple" + LineId];
                                objQMS012.AssemblyNo = fc["hdnAssemblyNo" + LineId];
                                objQMS012.Thickness = fc["Thickness" + LineId];
                                objQMS012.ThicknessDefault = fc["ThicknessDefault" + LineId];
                                objQMS012.Material = fc["Material" + LineId];
                                objQMS012.WEPDetail = fc["hdnWEPDetail" + LineId];
                                objQMS012.ViewDrawing = fc["ViewDrawing" + LineId];
                                if (!string.IsNullOrEmpty(fc["SeamLengthArea" + LineId]))
                                {
                                    objQMS012.SeamLengthArea = Convert.ToInt32(fc["SeamLengthArea" + LineId]);
                                }
                                else
                                {
                                    objQMS012.SeamLengthArea = null;
                                }
                                objQMS012.Remarks = fc["Remarks" + LineId];
                                db.SaveChanges();
                                objResponseMsg.Key = true;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();

                            }
                            else
                            {
                                objQMS012 = new QMS012();
                                objQMS012.HeaderId = headerId;
                                objQMS012.Location = objQMS011.Location;
                                objQMS012.SeamListNo = objQMS011.SeamListNo;
                                objQMS012.Project = objQMS011.Project;
                                objQMS012.QualityProject = objQMS011.QualityProject;
                                objQMS012.SeamCategory = fc["hdnSeamDescription" + newRowIndex];
                                objQMS012.WeldType = fc["hdnWeldType" + newRowIndex];
                                objQMS012.SeamDescription = fc["hdnSeamDescription" + newRowIndex];
                                objQMS012.ManualSeamNo = Manager.ReplaceIllegalCharForSeamNo(fc["ManualSeamNo" + newRowIndex]);
                                objQMS012.SeamNo = Manager.ReplaceIllegalCharForSeamNo(fc["SeamNo" + newRowIndex]);
                                objQMS012.SeamRev = 0;
                                objQMS012.Position = fc["ddlmultiple" + newRowIndex];
                                objQMS012.Thickness = fc["Thickness" + newRowIndex];
                                objQMS012.ThicknessDefault = fc["ThicknessDefault" + newRowIndex];
                                objQMS012.Material = fc["Material" + newRowIndex];
                                objQMS012.ViewDrawing = fc["ViewDrawing" + newRowIndex];
                                objQMS012.SeamLengthArea = Convert.ToInt32(fc["SeamLengthArea" + newRowIndex]);
                                objQMS012.Unit = fc["Unit" + newRowIndex];
                                objQMS012.WEPDetail = fc["hdnWEPDetail" + newRowIndex];
                                objQMS012.Remarks = fc["Remarks" + newRowIndex];
                                objQMS012.AssemblyNo = fc["hdnAssemblyNo" + newRowIndex];
                                objQMS012.Status = clsImplementationEnum.SeamListStatus.Draft.GetStringValue();
                                objQMS012.CreatedBy = LoginUser;
                                objQMS012.CreatedOn = DateTime.Now;
                                db.QMS012.Add(objQMS012);
                                db.SaveChanges();
                                objResponseMsg.Key = true;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                            }
                            if (objResponseMsg.Key == true && objQMS011.Status == clsImplementationEnum.SeamListStatus.Approved.GetStringValue())
                            {
                                objQMS011.SeamListRev = objQMS011.SeamListRev + 1;
                            }
                            objQMS011.Status = clsImplementationEnum.SeamListStatus.Draft.GetStringValue();

                            db.SaveChanges();
                            status = objQMS011.Status;
                            SeamListRev = Convert.ToInt32(objQMS011.SeamListRev);
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Document is sent for approval, you can not add lines now";
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            var response = new { Key = objResponseMsg.Key, Value = objResponseMsg.Value, Status = status, SeamListRev = SeamListRev, LineId = objQMS012.LineId };
            return Json(response, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult DeleteLine(int LineID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                //string[] arrayStatus = { clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue(), clsImplementationEnum.PTMTCTQStatus.Returned.GetStringValue() };
                QMS012 objQMS012 = db.QMS012.Where(x => x.LineId == LineID).FirstOrDefault();

                if (objQMS012 != null)
                {
                    if (objQMS012.QMS011.Status == clsImplementationEnum.SeamListStatus.SentForApproval.GetStringValue())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This document is already submitted for approval. Please refresh the page.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }

                    db.QMS012.Remove(objQMS012);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details successfully deleted.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Details not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        [HttpPost]
        public JsonResult ReviseSeam(int LineID)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                QMS012 objQMS012 = db.QMS012.Where(x => x.LineId == LineID).FirstOrDefault();
                objQMS012.SeamRev += 1;
                objQMS012.Status = clsImplementationEnum.PAMStatus.Draft.GetStringValue();
                objQMS012.EditedBy = objClsLoginInfo.UserName;
                objQMS012.EditedOn = DateTime.Now;
                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revision.ToString();
                objResponseMsg.Status = objQMS012.Status;
                objResponseMsg.Revision = objQMS012.SeamRev != null ? Convert.ToInt32(objQMS012.SeamRev) : 0;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> DeleteLines(int headerId, int lineId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            QMS011 objQMS011 = await db.QMS011.FirstOrDefaultAsync(x => x.HeaderId == headerId);
            try
            {
                if (objQMS011.Status != clsImplementationEnum.SeamListStatus.SentForApproval.GetStringValue())
                {
                    QMS012 objQMS012 = await db.QMS012.Where(x => x.HeaderId == headerId && x.LineId == lineId).FirstOrDefaultAsync();

                    string outReason;
                    if (IsApplicableForDelete(objQMS012.QualityProject, objQMS012.SeamNo, out outReason))
                    {
                        if (objQMS012 != null)
                        {
                            RemoveSeamRelatedData(objQMS012.QualityProject, objQMS012.SeamNo);
                            //add QMS013 
                            QMS013 objQMS013 = new QMS013();

                            objQMS013.Project = objQMS012.Project;
                            objQMS013.QualityProject = objQMS012.QualityProject;
                            objQMS013.SeamNo = objQMS012.SeamNo;
                            objQMS013.DeletedBy = objClsLoginInfo.UserName.ToString();
                            objQMS013.DeletedOn = System.DateTime.Now;
                            db.QMS013.Add(objQMS013);
                            db.QMS012.Remove(objQMS012);

                            await db.SaveChangesAsync();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "Line deleted successfully";
                        }
                        if (objResponseMsg.Key == true && objQMS011.Status == clsImplementationEnum.SeamListStatus.Approved.GetStringValue())
                        {
                            objQMS011.SeamListRev = objQMS011.SeamListRev + 1;
                        }
                        objQMS011.Status = clsImplementationEnum.SeamListStatus.Draft.GetStringValue();
                        db.SaveChanges();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = outReason;
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Document is sent for approval, You can't delete now";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            var response = new
            {
                Key = objResponseMsg.Key,
                Value = objResponseMsg.Value,
                SeamListRev = objQMS011.SeamListRev,
                Status = objQMS011.Status
            };
            return Json(response);
        }

        public void RemoveSeamRelatedData(string QProject, string SeamNo)
        {
            #region Remove Offering Data

            // Remove Chemical Data if exists
            List<QMS051> lstChemicalData = db.QMS051.Where(c => c.QualityProject == QProject && c.SeamNo == SeamNo).ToList();
            if (lstChemicalData.Count > 0)
                db.QMS051.RemoveRange(lstChemicalData);

            // Remove Offering Data if exists
            List<QMS040> lstOfferingData = db.QMS040.Where(c => c.QualityProject == QProject && c.SeamNo == SeamNo).ToList();
            if (lstOfferingData.Count > 0)
                db.QMS040.RemoveRange(lstOfferingData);

            #endregion

            #region Remove ICL Data

            // Remove Protocol Data which is attached with stages
            // Remove ICL Data if exists
            List<QMS031> lstICLData = db.QMS031.Where(c => c.QualityProject == QProject && c.SeamNo == SeamNo).ToList();
            foreach (var itemICL in lstICLData)
            {
                if (!string.IsNullOrWhiteSpace(itemICL.ProtocolType))
                    (new MaintainSeamICLController()).AttachedProtocol(itemICL.LineId, string.Empty, string.Empty);
            }
            if (lstICLData.Count > 0)
                db.QMS031.RemoveRange(lstICLData);

            List<QMS030> lstICLHeaderData = db.QMS030.Where(c => c.QualityProject == QProject && c.SeamNo == SeamNo).ToList();
            if (lstICLHeaderData.Count > 0)
                db.QMS030.RemoveRange(lstICLHeaderData);

            List<QMS031_Log> lstICLLogLinesData = db.QMS031_Log.Where(c => c.QualityProject == QProject && c.SeamNo == SeamNo).ToList();
            if (lstICLLogLinesData.Count > 0)
                db.QMS031_Log.RemoveRange(lstICLLogLinesData);

            List<QMS030_Log> lstICLLogHeaderData = db.QMS030_Log.Where(c => c.QualityProject == QProject && c.SeamNo == SeamNo).ToList();
            if (lstICLLogHeaderData.Count > 0)
                db.QMS030_Log.RemoveRange(lstICLLogHeaderData);

            #endregion

            #region Remove Test Plan & Shop Weld Plan Data

            // Remove Test Plan Data if exists
            List<QMS021> lstTestPlanData = db.QMS021.Where(c => c.QualityProject == QProject && c.SeamNo == SeamNo).ToList();
            if (lstTestPlanData.Count > 0)
            {
                // Remove Test Plan Chemical/PMI Data if exists
                List<QMS023> lstTPChemicalElementData = db.QMS023.Where(c => c.QualityProject == QProject && c.SeamNo == SeamNo).ToList();
                if (lstTPChemicalElementData.Count > 0)
                    db.QMS023.RemoveRange(lstTPChemicalElementData);

                List<QMS022> lstTPChemicalData = db.QMS022.Where(c => c.QualityProject == QProject && c.SeamNo == SeamNo).ToList();
                if (lstTPChemicalData.Count > 0)
                    db.QMS022.RemoveRange(lstTPChemicalData);

                db.QMS021.RemoveRange(lstTestPlanData);
            }

            List<QMS020> lstTestPlanHeaderData = db.QMS020.Where(c => c.QualityProject == QProject && c.SeamNo == SeamNo).ToList();
            if (lstTestPlanHeaderData.Count > 0)
                db.QMS020.RemoveRange(lstTestPlanHeaderData);

            List<QMS021_log> lstTestPlanLogData = db.QMS021_log.Where(c => c.QualityProject == QProject && c.SeamNo == SeamNo).ToList();
            if (lstTestPlanLogData.Count > 0)
                db.QMS021_log.RemoveRange(lstTestPlanLogData);

            List<QMS020_log> lstTestPlanHeaderLogData = db.QMS020_log.Where(c => c.QualityProject == QProject && c.SeamNo == SeamNo).ToList();
            if (lstTestPlanHeaderLogData.Count > 0)
                db.QMS020_log.RemoveRange(lstTestPlanHeaderLogData);


            // Remove Shop Weld Plan Data if exists
            List<SWP010> lstSWPHeaderData = db.SWP010.Where(c => c.QualityProject == QProject && c.SeamNo == SeamNo).ToList();
            if (lstSWPHeaderData.Count > 0)
                db.SWP010.RemoveRange(lstSWPHeaderData);

            List<SWP010_Log> lstSWPHeaderLogData = db.SWP010_Log.Where(c => c.QualityProject == QProject && c.SeamNo == SeamNo).ToList();
            if (lstSWPHeaderLogData.Count > 0)
                db.SWP010_Log.RemoveRange(lstSWPHeaderLogData);

            #endregion
        }
        [HttpPost]
        public JsonResult GetSeamno(string seamno,string qproject)
        {

            try
            {
                var objSeam = (from t1 in db.QMS013
                               join com3 in db.COM003 on t1.DeletedBy equals com3.t_psno
                               where t1.SeamNo.ToLower() == seamno.ToLower() && t1.QualityProject == qproject
                               select new
                               {
                                   text = com3.t_name
                               }).Select(s => s.text);


                if (objSeam != null)
                {
                    return Json(objSeam, JsonRequestBehavior.AllowGet);
                }
                return Json(false, JsonRequestBehavior.AllowGet);

            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }


        }
        public bool IsApplicableForDelete(string QProject, string SeamNo, out string Reason)
        {
            bool isApplicableDelete = true;
            Reason = string.Empty;
            string RTOStatus = clsImplementationEnum.SeamListInspectionStatus.ReadyToOffer.GetStringValue();
            if (db.WPS025.Any(c => c.QualityProject == QProject && c.SeamNo == SeamNo))
            {
                isApplicableDelete = false;
                Reason = "You can't delete, because token already generated for this seam";
            }
            else if (db.QMS040.Any(c => c.QualityProject == QProject && c.SeamNo == SeamNo && c.InspectionStatus != null && c.InspectionStatus != RTOStatus))
            {
                isApplicableDelete = false;
                Reason = "You can't delete, because seam already offered for inspection";
            }
            else if (db.LTF002.Any(c => c.QualityProject == QProject && c.SeamNo == SeamNo))
            {
                isApplicableDelete = false;
                Reason = "You can't delete, because seam already linked with LTFPS document";
            }
            return isApplicableDelete;
        }

        public bool IsApplicableForUpdate(string QProject, string SeamNo, out string Reason)
        {
            bool isApplicableDelete = true;
            Reason = string.Empty;
            string RTOStatus = clsImplementationEnum.SeamListInspectionStatus.ReadyToOffer.GetStringValue();
            if (db.WPS025.Any(c => c.QualityProject == QProject && c.SeamNo == SeamNo))
            {
                isApplicableDelete = false;
                Reason = "You can't Update, because token already generated for this seam";
            }
            else if (db.QMS040.Any(c => c.QualityProject == QProject && c.SeamNo == SeamNo && c.InspectionStatus != null && c.InspectionStatus != RTOStatus))
            {
                isApplicableDelete = false;
                Reason = "You can't Update, because seam already offered for inspection";
            }
            else if (db.LTF002.Any(c => c.QualityProject == QProject && c.SeamNo == SeamNo))
            {
                isApplicableDelete = false;
                Reason = "You can't Update, because seam already linked with LTFPS document";
            }
            return isApplicableDelete;
        }

        public JsonResult GetThicknessandMaterialDetails(string project, string position, string assembly)
        {
            var lstPosition = position.Split(',').ToList();
            //Obs ID#19247. SP input param "assemblyName" passed as empty.
            //As per discussed with Trishul Tandel on 07-01-2019, Material and Thickness should be populated based on only Project & FindNo. Assembly/ParentPart should not be required.
            var results = db.SP_IPI_GETSEAMLISTDETAILS(project, assembly, "").ToList();
            string Thickness = string.Empty;
            string Material = string.Empty;
            bool isEditable = false;
            if (results.Count > 0)
            {
                //Thickness = string.Join(",", results.Where(x => lstPosition.Contains(x.FindNo.ToString())).Select(x => x.Thickness).ToList());
                //Material = string.Join(",", results.Where(x => lstPosition.Contains(x.FindNo.ToString())).Select(x => x.MaterialDescription).ToList());

                var ThicknessDefaultTemp = string.Empty;
                var MaterialTemp = string.Empty;
                for (int i = 0; i < lstPosition.Count(); i++)
                {
                    var tempresult = results.Where(x => x.FindNo == lstPosition[i]).FirstOrDefault();
                    if (tempresult != null)
                    {
                        if (ThicknessDefaultTemp.Length == 0)
                        {
                            if (tempresult.Thickness.HasValue)
                            {
                                ThicknessDefaultTemp = tempresult.Thickness.Value.ToString();
                            }
                        }
                        else
                        {
                            if (tempresult.Thickness.HasValue)
                            {
                                ThicknessDefaultTemp += "," + tempresult.Thickness.Value.ToString();
                            }
                        }


                        if (MaterialTemp.Length == 0)
                        {
                            if (!string.IsNullOrEmpty(tempresult.MaterialDescription))
                            {
                                MaterialTemp = tempresult.MaterialDescription.ToString();
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(tempresult.MaterialDescription))
                            {
                                MaterialTemp += "," + tempresult.MaterialDescription.ToString();
                            }
                        }
                    }
                }
                Thickness = ThicknessDefaultTemp;
                Material = MaterialTemp;
                isEditable = results.Where(x => lstPosition.Contains(x.FindNo.ToString()) && x.ProductType.ToLower() == clsImplementationEnum.ProductType.FRG.GetStringValue().ToLower()).Any();
            }

            var data = new
            {
                Thickness = Thickness,
                Material = Material,
                isEditable = isEditable
            };

            return Json(data, JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetAssembyByPosition(string project, string position)
        {

            var results = db.SP_IPI_GETPARTPARENTLIST(project, position).ToList();
            //var items = (from li in results
            //             select new
            //             {
            //                 id = li.ToString(),
            //                 text = li.ToString()
            //             });
            var items = (from li in results
                         select new CategoryData()
                         {
                             Code = li.ToString(),
                             CategoryDescription = li.ToString()
                         });
            return Json(items, JsonRequestBehavior.AllowGet);
        }
        public string GetMaxSeamNo(string seamCategory, string qualityProject)
        {
            return db.Database.SqlQuery<string>("SELECT dbo.FN_IPI_GENERATE_MAX_SEAMNO('" + qualityProject + "','" + seamCategory + "')").FirstOrDefault();
        }

        public bool IsSeamNoExists(string seamno, string qualityPtoject)
        {
            //Name : Ajay Chauhan
            //Date : 15 -11-2017
            //Desc : Seam list no should be unique in QMS project
            //var manualSeamNo = db.QMS012.Where(x => x.HeaderId == headerId && x.SeamNo == seamno).Any(); before
            seamno = Manager.ReplaceIllegalCharForSeamNo(seamno);
            var manualSeamNo = db.QMS012.Where(x => x.QualityProject == qualityPtoject && x.SeamNo == seamno).Any();
            if (manualSeamNo)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_IPI_GETSEAMLISTHEADER(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      ContractNo = li.ContractNo,
                                      Customer = li.Customer,
                                      QualityProject = li.QualityProject,
                                      SeamListNo = li.SeamListNo,
                                      SeamListRev = "R" + li.SeamListRev,
                                      Status = li.Status,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_IPI_GETSEAMLISTLINES(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from li in lst
                                  select new
                                  {
                                      WeldType = Convert.ToString(li.WeldType),
                                      SeamCategory = Convert.ToString(li.SeamDescription),
                                      Number = Convert.ToString(li.ManualSeamNo),
                                      Revision = "R" + Convert.ToString(li.SeamRev),
                                      Position = Convert.ToString(li.Position),
                                      AssemblyNumber = Convert.ToString(li.AssemblyNo),
                                      FinishedThickness = Convert.ToString(li.Thickness),
                                      ThicknessOfParts = Convert.ToString(li.ThicknessDefault),
                                      WEPDetail = Convert.ToString(li.WEPDetail),
                                      ViewDrawing = Convert.ToString(li.ViewDrawing),
                                      SeamLengthArea = Convert.ToString(li.SeamLengthArea),
                                      Remarks = Convert.ToString(li.Remarks),
                                      SeamQuantity = Convert.ToString(li.SeamQuantity),
                                      Action = "Edit"
                                  }).ToList();

                    strFileName = GenerateExcelSeamList(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region 25365 : pop up for Seam quantity
        [HttpPost]
        public ActionResult GetSeamQuantityPartial(int lineid = 0, bool isEditable = false)
        {
            QMS012 objQMS012 = new QMS012();
            if (lineid > 0)
            {
                objQMS012 = db.QMS012.Where(i => i.LineId == lineid).FirstOrDefault();
            }
            ViewBag.isEditable = isEditable;
            return PartialView("_LoadSeamQuantityData", objQMS012);
        }

        [HttpPost]
        public ActionResult SaveQuantity(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                QMS011 objQMS011 = new QMS011();
                QMS012 objQMS012 = new QMS012();
                int lineid = Convert.ToInt32(fc["LineId"]);
                objQMS012 = db.QMS012.Where(i => i.LineId == lineid).FirstOrDefault();
                if (objQMS012 != null)
                {
                    if (objQMS012.QMS011.Status != clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue())
                    {
                        if (objQMS012.SeamCategory == clsImplementationEnum.AutoCJCSeamType.RA.GetStringValue())
                        {
                            objQMS012.SeamQuantity = Convert.ToDouble(fc["SeamQuantity"]);
                        }
                        if (objQMS012.QMS011.Status == clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue())
                        {
                            ReviseSeam(lineid);
                        }
                        objResponseMsg.Value = "Record saved successfully";
                    }
                    else
                    {
                        objResponseMsg.Remarks = clsImplementationMessage.CommonMessages.NotInEditStatus;
                    }
                }
                db.SaveChanges();
                objResponseMsg.Key = true;

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region History
        [HttpPost]
        public ActionResult GetHistoryDetails(int headerId, string forAction)
        {
            ViewBag.HeaderId = headerId;
            ViewBag.type = forAction;
            return PartialView("_GetHistoryDetailsPartial");
        }

        [HttpPost]
        public JsonResult LoadHistoryDetailData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;

                string whereCondition = "1=1 and HeaderId = " + HeaderId + " ";
                //whereCondition += " and (ApprovedBy = '" + objClsLoginInfo.UserName + "' or CreatedBy = '" + objClsLoginInfo.UserName + "' ) ";
                string[] columnName = { "qms.QualityProject", "qms.Project +'-'+ com1.t_dsca", "qms.ContractNo+'-'+com4.t_desc", "com6.t_bpid+'-'+com6.t_nama", "Status", "SeamListNo" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_IPI_GETSEAMLISTHEADER_History(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstResult.Select(i => i.TotalCount).FirstOrDefault();

                var data = (from uc in lstResult
                            select new[]
                           {
                           Convert.ToString(uc.QualityProject),
                           uc.Project,
                           uc.Customer,
                           uc.ContractNo,
                           uc.Location,
                           Convert.ToString(uc.SeamListNo),
                           Convert.ToString("R" + uc.SeamListRev),
                           Convert.ToString(uc.Status),
                           uc.CreatedBy,
                           Convert.ToDateTime( uc.CreatedOn).ToString("dd/MM/yyyy"),
                           uc.ApprovedBy,
                           Convert.ToDateTime( uc.ApprovedOn).ToString("dd/MM/yyyy"),
                           Convert.ToString(uc.Id)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition

                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]
        public ActionResult HistoryView(int id, string type)
        {
            QMS011_Log objQMS011_Log = new QMS011_Log();
            objQMS011_Log = db.QMS011_Log.Where(x => x.Id == id).FirstOrDefault();
            objQMS011_Log.Project = Manager.GetProjectAndDescription(objQMS011_Log.Project);
            objQMS011_Log.Customer = db.COM006.Where(x => x.t_bpid == objQMS011_Log.Customer).Select(x => x.t_bpid + "-" + x.t_nama).FirstOrDefault();
            objQMS011_Log.ContractNo = Manager.GetContractorAndDescription(objQMS011_Log.ContractNo);
            objQMS011_Log.CreatedBy = objQMS011_Log.CreatedBy + "-" + Manager.GetUserNameFromPsNo(objQMS011_Log.CreatedBy);
            objQMS011_Log.EditedBy = objQMS011_Log.EditedBy + "-" + Manager.GetUserNameFromPsNo(objQMS011_Log.EditedBy);
            objQMS011_Log.ApprovedBy = objQMS011_Log.ApprovedBy + "-" + Manager.GetUserNameFromPsNo(objQMS011_Log.ApprovedBy);
            objQMS011_Log.QualityProject = string.IsNullOrWhiteSpace(objQMS011_Log.Location) ? objQMS011_Log.QualityProject : objQMS011_Log.QualityProject + " (" + objQMS011_Log.Location + ")";
            ViewBag.type = type;
            return View(objQMS011_Log);
        }

        [HttpPost]
        public JsonResult LoadLinesHistoryData(JQueryDataTableParamModel param, int refId)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = string.Empty;
                whereCondition = "RefId = " + refId;

                string[] columnName = { "qms.SeamNo", "qms.SeamDescription", "qms.SeamListNo", "CONVERT( VARCHAR(10),qms.SeamRev)", "qms.Position", "qms.Thickness", "qms.Material", "qms.ViewDrawing", "qms.WeldType", "qms.SeamLengthArea", "qms.Unit", "qms.WEPDetail", "qms.Remarks", "qms.AssemblyNo" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);

                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstLines = db.SP_IPI_GETSEAMLISTLINES_Header(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();

                var res = from c in lstLines
                          select new[] {
                                        Convert.ToString(c.ROW_NO),
                                        c.SeamNo,
                                        c.SeamDescription,
                                        c.SeamListNo,
                                        "R" + c.SeamRev,
                                        c.Position,
                                        c.Thickness,
                                        //Convert.ToString(c.ThicknessDefault),
                                        c.Material,
                                        c.ViewDrawing,
                                        c.WeldType,
                                        Convert.ToString(c.SeamLengthArea),
                                        c.Unit,
                                        c.WEPDetail,
                                        c.Remarks,
                                        c.AssemblyNo
                          };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,

                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string GetSeamListHistoryOld(int HeaderId)
        {
            StringBuilder str = new StringBuilder();
            int PreviousRevNo = 0;
            int CurrentRevNo = 1;
            var IsNewEntry = false;
            var IsDeleted = false;
            List<RevisionHistoryDtl> objQMS012_LogCur = null;
            try
            {
                var objQMS011 = db.QMS011.Where(i => i.HeaderId == HeaderId).FirstOrDefault();
                if (objQMS011 != null && objQMS011.SeamListRev > 0)
                {
                    var SeamListRev = objQMS011.SeamListRev;
                    var objQMS012_Log = db.QMS012_Log.Where(i => i.HeaderId == HeaderId).ToList();


                    str.Append("<table id='tblSeamListRevisionHistory' class='table table-bordered'>");

                    #region Header
                    //Create Header Row
                    str.Append("<thead>");
                    str.Append("<tr><th>Revision</th><th>Action</th><th>SeamNo</th><th>Modification Details</th></tr>");
                    str.Append("</thead>");
                    #endregion

                    #region Body
                    str.Append("<tbody>");
                    str.Append("<tr><td>R0</td><td>New Entry</td><td></td><td></td></tr>");
                    #endregion
                    while (CurrentRevNo <= SeamListRev)
                    {
                        var PrevLog = db.QMS011_Log.Where(i => i.HeaderId == HeaderId && i.SeamListRev == PreviousRevNo).FirstOrDefault();
                        var CurLog = db.QMS011_Log.Where(i => i.HeaderId == HeaderId && i.SeamListRev == CurrentRevNo).FirstOrDefault();

                        var objQMS012_LogPrev = objQMS012_Log.Where(i => i.RefId == PrevLog.Id).ToList();
                        if (CurrentRevNo == SeamListRev)
                        {
                            objQMS012_LogCur = db.QMS012.Where(i => i.HeaderId == HeaderId).Select(i =>
                            new RevisionHistoryDtl
                            {
                                SeamNo = i.SeamNo,
                                SeamDescription = i.SeamDescription,
                                SeamListNo = i.SeamListNo,
                                SeamRev = i.SeamRev,
                                Status = i.Status,
                                Position = i.Position,
                                Thickness = i.Thickness,
                                Material = i.Material,
                                ViewDrawing = i.ViewDrawing,
                                WeldType = i.WeldType,
                                SeamLengthArea = i.SeamLengthArea,
                                Unit = i.Unit,
                                WEPDetail = i.WEPDetail,
                                Remarks = i.Remarks,
                                AssemblyNo = i.AssemblyNo,
                                DocNoSeam = i.DocNoSeam,
                                QualityProject = i.QualityProject,
                                Project = i.Project,
                                SeamCategory = i.SeamCategory,
                                ManualSeamNo = i.ManualSeamNo
                            }).ToList();
                        }
                        else
                        {
                            objQMS012_LogCur = objQMS012_Log.Where(i => i.RefId == CurLog.Id).Select(i =>
                            new RevisionHistoryDtl
                            {
                                SeamNo = i.SeamNo,
                                SeamDescription = i.SeamDescription,
                                SeamListNo = i.SeamListNo,
                                SeamRev = i.SeamRev,
                                Status = i.Status,
                                Position = i.Position,
                                Thickness = i.Thickness,
                                Material = i.Material,
                                ViewDrawing = i.ViewDrawing,
                                WeldType = i.WeldType,
                                SeamLengthArea = i.SeamLengthArea,
                                Unit = i.Unit,
                                WEPDetail = i.WEPDetail,
                                Remarks = i.Remarks,
                                AssemblyNo = i.AssemblyNo,
                                DocNoSeam = i.DocNoSeam,
                                QualityProject = i.QualityProject,
                                Project = i.Project,
                                SeamCategory = i.SeamCategory,
                                ManualSeamNo = i.ManualSeamNo
                            }).ToList();
                        }
                        foreach (var cur in objQMS012_LogCur)
                        {
                            IsNewEntry = true;
                            foreach (var prev in objQMS012_LogPrev)
                            {
                                if (prev.SeamNo == cur.SeamNo)
                                {
                                    IsNewEntry = false;
                                    var oType = prev.GetType();
                                    foreach (var oProperty in oType.GetProperties())
                                    {
                                        if (oProperty.Name.ToLower() != ("id").ToLower() && oProperty.Name.ToLower() != ("RefId").ToLower() &&
                                            oProperty.Name.ToLower() != ("LineId").ToLower() && oProperty.Name.ToLower() != ("HeaderId").ToLower() &&
                                            oProperty.Name.ToLower() != ("CreatedBy").ToLower() && oProperty.Name.ToLower() != ("CreatedOn").ToLower() &&
                                            oProperty.Name.ToLower() != ("EditedBy").ToLower() && oProperty.Name.ToLower() != ("EditedOn").ToLower() &&
                                            oProperty.Name.ToLower() != ("ApprovedBy").ToLower() && oProperty.Name.ToLower() != ("ApprovedOn").ToLower() &&
                                            oProperty.Name.ToLower() != ("SubmittedBy").ToLower() && oProperty.Name.ToLower() != ("SubmittedOn").ToLower() &&
                                            oProperty.Name.ToLower() != ("Status").ToLower() && oProperty.Name.ToLower() != ("QualityProject").ToLower() &&
                                            oProperty.Name.ToLower() != ("Project").ToLower() && oProperty.Name.ToLower() != ("SeamCategory").ToLower() &&
                                            oProperty.Name.ToLower() != ("ManualSeamNo").ToLower() && oProperty.Name.ToLower() != ("DocNoSeam").ToLower())
                                        {
                                            var oOldValue = oProperty.GetValue(prev, null);
                                            //var oNewValue = oProperty.GetValue(cur, null);
                                            var oNewValue = cur.GetType().GetProperty(oProperty.Name).GetValue(cur, null);

                                            // this will handle the scenario where either value is null
                                            if (!object.Equals(oOldValue, oNewValue))
                                            {
                                                // Handle the display values when the underlying value is null
                                                var sOldValue = oOldValue == null ? "null" : oOldValue.ToString();
                                                var sNewValue = oNewValue == null ? "null" : oNewValue.ToString();

                                                str.Append("<tr><td>R" + CurrentRevNo.ToString() + "</td><td>Modified</td><td>" + cur.SeamNo.ToString() + "</td><td>" + oProperty.Name + " was: " + sOldValue + "; is: " + sNewValue + "</td></tr>");
                                            }
                                        }

                                    }

                                    break;
                                }
                            }
                            if (IsNewEntry)
                            {
                                str.Append("<tr><td>R" + CurrentRevNo.ToString() + "</td><td>Added</td><td>" + cur.SeamNo.ToString() + "</td><td></td></tr>");
                            }
                        }

                        #region Delete From Previous Revision
                        foreach (var prev in objQMS012_LogPrev)
                        {
                            IsDeleted = true;
                            foreach (var cur in objQMS012_LogCur)
                            {
                                if (prev.SeamNo == cur.SeamNo)
                                {
                                    IsDeleted = false;
                                    break;
                                }
                            }
                            if (IsDeleted)
                            {
                                str.Append("<tr><td>R" + CurLog.SeamListRev.ToString() + "</td><td>Deleted</td><td>" + prev.SeamNo.ToString() + "</td><td></td></tr>");
                            }
                        }
                        #endregion

                        PreviousRevNo++;
                        CurrentRevNo++;
                    }

                    str.Append("</table>");
                }
            }
            catch (Exception)
            {
                throw;
            }
            return str.ToString();
        }

        public string GetSeamListHistory(int HeaderId)
        {
            StringBuilder str = new StringBuilder();
            int PreviousRevNo = 0;
            int CurrentRevNo = 1;
            var IsNewEntry = false;
            var IsDeleted = false;
            List<RevisionHistoryDtl> objQMS012_LogCur = null;
            List<string> listtd = null;
            try
            {
                var objQMS011 = db.QMS011.Where(i => i.HeaderId == HeaderId).FirstOrDefault();
                if (objQMS011 != null && objQMS011.SeamListRev > 0)
                {
                    var SeamListRev = objQMS011.SeamListRev;
                    var objQMS012_Log = db.QMS012_Log.Where(i => i.HeaderId == HeaderId).ToList();


                    str.Append("<table id=\"tblSeamListRevisionHistory\" class=\"table table-bordered\">");

                    #region Header
                    //Create Header Row
                    str.Append("<thead>");
                    str.Append("<tr><th style=\"max-width:100px;width:100px;\">Revision</th><th>Action</th><th>SeamNo</th><th>Modification Details</th></tr>");
                    str.Append("</thead>");
                    #endregion

                    #region Body
                    str.Append("<tbody>");
                    str.Append("<tr><td style=\"max-width:100px;width:100px;\"><b>R0</b></td><td>New Entry</td><td></td><td></td></tr>");
                    #endregion
                    while (CurrentRevNo <= SeamListRev)
                    {
                        listtd = new List<string>();
                        var PrevLog = db.QMS011_Log.Where(i => i.HeaderId == HeaderId && i.SeamListRev == PreviousRevNo).FirstOrDefault();
                        var CurLog = db.QMS011_Log.Where(i => i.HeaderId == HeaderId && i.SeamListRev == CurrentRevNo).FirstOrDefault();

                        var objQMS012_LogPrev = objQMS012_Log.Where(i => i.RefId == PrevLog.Id).ToList();
                        if (CurrentRevNo == SeamListRev)
                        {
                            objQMS012_LogCur = db.QMS012.Where(i => i.HeaderId == HeaderId).Select(i =>
                            new RevisionHistoryDtl
                            {
                                SeamNo = i.SeamNo,
                                SeamDescription = i.SeamDescription,
                                SeamListNo = i.SeamListNo,
                                SeamRev = i.SeamRev,
                                Status = i.Status,
                                Position = i.Position,
                                Thickness = i.Thickness,
                                Material = i.Material,
                                ViewDrawing = i.ViewDrawing,
                                WeldType = i.WeldType,
                                SeamLengthArea = i.SeamLengthArea,
                                Unit = i.Unit,
                                WEPDetail = i.WEPDetail,
                                Remarks = i.Remarks,
                                AssemblyNo = i.AssemblyNo,
                                DocNoSeam = i.DocNoSeam,
                                QualityProject = i.QualityProject,
                                Project = i.Project,
                                SeamCategory = i.SeamCategory,
                                ManualSeamNo = i.ManualSeamNo,
                                ThicknessDefault = i.ThicknessDefault
                            }).ToList();
                        }
                        else
                        {
                            objQMS012_LogCur = objQMS012_Log.Where(i => i.RefId == CurLog.Id).Select(i =>
                            new RevisionHistoryDtl
                            {
                                SeamNo = i.SeamNo,
                                SeamDescription = i.SeamDescription,
                                SeamListNo = i.SeamListNo,
                                SeamRev = i.SeamRev,
                                Status = i.Status,
                                Position = i.Position,
                                Thickness = i.Thickness,
                                Material = i.Material,
                                ViewDrawing = i.ViewDrawing,
                                WeldType = i.WeldType,
                                SeamLengthArea = i.SeamLengthArea,
                                Unit = i.Unit,
                                WEPDetail = i.WEPDetail,
                                Remarks = i.Remarks,
                                AssemblyNo = i.AssemblyNo,
                                DocNoSeam = i.DocNoSeam,
                                QualityProject = i.QualityProject,
                                Project = i.Project,
                                SeamCategory = i.SeamCategory,
                                ManualSeamNo = i.ManualSeamNo,
                                ThicknessDefault = i.ThicknessDefault
                            }).ToList();
                        }
                        foreach (var cur in objQMS012_LogCur)
                        {
                            IsNewEntry = true;

                            foreach (var prev in objQMS012_LogPrev)
                            {
                                if (prev.SeamNo == cur.SeamNo)
                                {
                                    IsNewEntry = false;
                                    var oType = prev.GetType();
                                    foreach (var oProperty in oType.GetProperties())
                                    {
                                        if (oProperty.Name.ToLower() != ("id").ToLower() && oProperty.Name.ToLower() != ("RefId").ToLower() &&
                                            oProperty.Name.ToLower() != ("LineId").ToLower() && oProperty.Name.ToLower() != ("HeaderId").ToLower() &&
                                            oProperty.Name.ToLower() != ("CreatedBy").ToLower() && oProperty.Name.ToLower() != ("CreatedOn").ToLower() &&
                                            oProperty.Name.ToLower() != ("EditedBy").ToLower() && oProperty.Name.ToLower() != ("EditedOn").ToLower() &&
                                            oProperty.Name.ToLower() != ("ApprovedBy").ToLower() && oProperty.Name.ToLower() != ("ApprovedOn").ToLower() &&
                                            oProperty.Name.ToLower() != ("SubmittedBy").ToLower() && oProperty.Name.ToLower() != ("SubmittedOn").ToLower() &&
                                            oProperty.Name.ToLower() != ("Status").ToLower() && oProperty.Name.ToLower() != ("QualityProject").ToLower() &&
                                            oProperty.Name.ToLower() != ("Project").ToLower() && oProperty.Name.ToLower() != ("SeamCategory").ToLower() &&
                                            oProperty.Name.ToLower() != ("ManualSeamNo").ToLower() && oProperty.Name.ToLower() != ("DocNoSeam").ToLower() &&
                                            oProperty.Name.ToLower() != ("Location").ToLower())
                                        {
                                            var oOldValue = oProperty.GetValue(prev, null);
                                            //var oNewValue = oProperty.GetValue(cur, null);
                                            var oNewValue = cur.GetType().GetProperty(oProperty.Name).GetValue(cur, null);

                                            // this will handle the scenario where either value is null
                                            if (!object.Equals(oOldValue, oNewValue))
                                            {
                                                // Handle the display values when the underlying value is null
                                                var sOldValue = oOldValue == null ? "null" : oOldValue.ToString();
                                                var sNewValue = oNewValue == null ? "null" : oNewValue.ToString();
                                                listtd.Add("<td>Modified</td><td>" + cur.SeamNo.ToString() + "</td><td>" + oProperty.Name + " was: " + (oProperty.Name.ToLower() == ("SeamRev").ToLower() ? "R" + sOldValue : sOldValue) + "; is: " + (oProperty.Name.ToLower() == ("SeamRev").ToLower() ? "R" + sNewValue : sNewValue) + "</td>");
                                                //str.Append("<tr><td>R" + CurrentRevNo.ToString() + "</td><td>Modified</td><td>" + cur.SeamNo.ToString() + "</td><td>" + oProperty.Name + " was: " + sOldValue + "; is: " + sNewValue + "</td></tr>");
                                            }
                                        }

                                    }

                                    break;
                                }
                            }
                            if (IsNewEntry)
                            {
                                listtd.Add("<td>Added</td><td>" + cur.SeamNo.ToString() + "</td><td></td>");
                                //str.Append("<tr><td>R" + CurrentRevNo.ToString() + "</td><td>Added</td><td>" + cur.SeamNo.ToString() + "</td><td></td></tr>");
                            }
                        }

                        #region Delete From Previous Revision
                        foreach (var prev in objQMS012_LogPrev)
                        {
                            IsDeleted = true;
                            foreach (var cur in objQMS012_LogCur)
                            {
                                if (prev.SeamNo == cur.SeamNo)
                                {
                                    IsDeleted = false;
                                    break;
                                }
                            }
                            if (IsDeleted)
                            {
                                listtd.Add("<td>Deleted</td><td>" + prev.SeamNo.ToString() + "</td><td></td>");
                                //str.Append("<tr><td>R" + CurLog.SeamListRev.ToString() + "</td><td>Deleted</td><td>" + prev.SeamNo.ToString() + "</td><td></td></tr>");
                            }
                        }
                        #endregion

                        if (listtd.Count > 0)
                        {
                            for (int k = 0; k < listtd.Count; k++)
                            {
                                if (k == 0)
                                {
                                    str.Append("<tr><td rowspan='" + listtd.Count.ToString() + "'><b>R" + CurrentRevNo.ToString() + "</b></td>" + listtd[k] + "</tr>");
                                }
                                else
                                {
                                    str.Append("<tr rowspan='" + listtd.Count.ToString() + "'>" + listtd[k] + "</tr>");
                                }
                            }
                        }
                        PreviousRevNo++;
                        CurrentRevNo++;
                    }

                    str.Append("</table>");
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
            return str.ToString();
        }

        #endregion



        #region Display Seam Liat
        [SessionExpireFilter]
        [UserPermissions]
        public ActionResult DisplaySeamList(string Project)
        {
            ViewBag.IsDisplayOnly = true;
            ViewBag.chkProject = Project;
            return View("Index");
        }
        public ActionResult DisplayHeader(int? headerId, HttpPostedFileBase upload, int? HeaderId, string Project)
        {
            QMS011 objQMS011 = new QMS011();
            //if (upload != null && HeaderId.HasValue)
            //    ReadExcel(upload, HeaderId.Value);

            try
            {
                if (headerId != null && headerId > 0)
                {
                    objQMS011 = db.QMS011.Where(x => x.HeaderId == headerId).FirstOrDefault();
                    ViewBag.Project = Manager.GetProjectAndDescription(objQMS011.Project);
                    ViewBag.Customer = db.COM006.Where(x => x.t_bpid == objQMS011.Customer).Select(x => x.t_bpid + "-" + x.t_nama).FirstOrDefault();
                    ViewBag.ContractNo = Manager.GetContractorAndDescription(objQMS011.ContractNo);
                    ViewBag.QualityProject = string.IsNullOrWhiteSpace(objQMS011.Location) ? objQMS011.QualityProject : objQMS011.QualityProject + " (" + objQMS011.Location + ")";
                    ViewBag.Action = "edit";
                }
                else
                {
                    objQMS011.HeaderId = 0;
                    objQMS011.Status = clsImplementationEnum.SeamListStatus.Draft.GetStringValue();
                    objQMS011.SeamListRev = 0;
                    objQMS011.DocNo = 0;
                    objQMS011.ReviseRemark = "First Revision";
                }
                ViewBag.chkProject = Project;
                ViewBag.IsDisplayOnly = true;

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            // return View(objQMS011);
            return View("AddHeader", objQMS011);
        }
        #endregion

        //Added by 89385821 on 07-09-2017 to add Excel import Functionality
        #region ExcelHelpers

        public ActionResult ReadExcel(HttpPostedFileBase upload, int HeaderId)
        {

            if (Path.GetExtension(upload.FileName) == ".xlsx" || Path.GetExtension(upload.FileName) == ".xls")
            {
                ExcelPackage package = new ExcelPackage(upload.InputStream);
                DataTable dt = ToDataTable(package);

                var header = db.QMS011.Where(q => q.HeaderId == HeaderId).FirstOrDefault();

                return validateAndSave(dt, HeaderId);
            }
            else
            {
                return RedirectToAction("AddHeader", new { HeaderId = HeaderId });
            }
        }

        public ActionResult ReadExcel1()
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            int HeaderId = Convert.ToInt32(Request.Form["HeaderId"]);
            if (Request.Files.Count == 0)
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please select a file first!";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            HttpPostedFileBase upload = Request.Files[0];
            if (Path.GetExtension(upload.FileName) == ".xlsx" || Path.GetExtension(upload.FileName) == ".xls")
            {
                ExcelPackage package = new ExcelPackage(upload.InputStream);
                DataTable dt = Manager.ExcelToDataTable(package);
                return validateAndSave(dt, HeaderId);
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please upload valid excel file!";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }

        }
        public DataTable ToDataTable(ExcelPackage package)
        {
            ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();
            DataTable dt = new DataTable();
            foreach (var firstRowCell in excelWorksheet.Cells[1, 1, 1, excelWorksheet.Dimension.End.Column])
            {
                dt.Columns.Add(firstRowCell.Text);
            }
            for (var rowNumber = 2; rowNumber <= excelWorksheet.Dimension.End.Row; rowNumber++)
            {
                var row = excelWorksheet.Cells[rowNumber, 1, rowNumber, excelWorksheet.Dimension.End.Column];
                var newRow = dt.NewRow();
                foreach (var cell in row)
                {
                    newRow[cell.Start.Column - 1] = cell.Text;
                }
                dt.Rows.Add(newRow);
            }
            return dt;
        }

        [SessionExpireFilter]
        public ActionResult validateAndSave(DataTable dt, int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();

            QMS011 objQMS011 = db.QMS011.FirstOrDefault(c => c.HeaderId == headerId);
            if (objQMS011 != null && objQMS011.Status == clsImplementationEnum.SeamListStatus.SentForApproval.GetStringValue())
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "This document is already submitted for approval. Please refresh the page.";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }

            bool hasError = false;
            if (!dt.Columns.Contains("Error"))
            {
                dt.Columns.Add("Error");
            }
            dt.Columns.Add("Material", typeof(string));
            //dt.Columns.Add("Thickness", typeof(string));
            dt.Columns.Add("SeamNo", typeof(string));
            dt.Columns.Add("HeaderId", typeof(Int32));
            try
            {
                var header = db.QMS011.Where(q => q.HeaderId == headerId).FirstOrDefault();
                string buCode = db.COM001.Where(i => i.t_cprj == header.Project).FirstOrDefault().t_entu;
                /// var objBOMlist = db.SP_IPI_GETHBOMLIST_VW(header.Project).Distinct().ToList();
                List<FN_GET_HBOM_DATA_Result> objBOMlist = Manager.GetHBOMData(header.Project).ToList();

                var positions = objBOMlist.Select(x => x.FindNo).ToList();
                var positionParentParts = objBOMlist.Select(x => x.ParentPart).ToList();
                //var weldTypes = Manager.GetSubCatagories("Joint Type", buCode, objClsLoginInfo.Location).ToList().Select(q => q.Code).Distinct().ToList();
                var lstUnit = db.BOM003.ToList();

                var weldTypes = lstUnit.Select(x => x.JointTypeCode).Distinct().ToList();
                var weps = Manager.GetSubCatagories("WEP Detail Code", buCode, objClsLoginInfo.Location).Select(q => q.Code).ToList();
                var lstSeamno = db.QMS012.Where(x => x.HeaderId == headerId).Select(x => new { x.SeamNo, x.LineId }).ToList();
                var lstSeamMaster = lstUnit.Select(x => new { JoinTypeCode = x.JointTypeCode, SeamCategory = x.SeamCategory }).Distinct().ToList();

                //List<GLB002> lstSeamMaster = Manager.GetSubCatagories("Seam Master", buCode, objClsLoginInfo.Location).ToList();
                #region Validation
                foreach (DataRow item in dt.Rows)
                {
                    List<string> errors = new List<string>();
                    var seam = item.Field<string>(1);
                    var weldType = item.Field<string>(0);

                    if (string.IsNullOrEmpty(item.Field<string>(13)))
                    {
                        errors.Add("Action is missing. use keyword new/add, edit, delete, revise any of one in action column");
                        hasError = true;
                        item.SetField<string>(17, string.Join(",", errors.ToArray()));
                        continue;
                    }

                    //WeldType should be from Master GBL002, Mandatory
                    if (string.IsNullOrEmpty(item.Field<string>(0)))
                    { errors.Add("Weld Type is Mandatory"); hasError = true; }

                    if (!weldTypes.Contains(item.Field<string>(0)))
                    { errors.Add("Invalid Weld Type"); hasError = true; }

                    //WeldType should be from Master GBL002, Mandatory
                    if (string.IsNullOrEmpty(item.Field<string>(1)))
                    { errors.Add("Seam Category is Mandatory"); hasError = true; }

                    var seamMasterList = lstSeamMaster.Where(x => x.JoinTypeCode == weldType).Select(x => x.SeamCategory).ToList();

                    //Seam Master from Master
                    if (!seamMasterList.Contains(seam))
                    { errors.Add("Wrong combination of Weld type and Seam Category"); hasError = true; }
                    else
                    {
                        if (!string.IsNullOrWhiteSpace(item.Field<string>(1)) ? item.Field<string>(1) == clsImplementationEnum.AutoCJCSeamType.RA.GetStringValue() && string.IsNullOrWhiteSpace(item.Field<string>(12)) : false)
                        { errors.Add("Seam Quantity is Mandatory"); hasError = true; }

                        if (!string.IsNullOrWhiteSpace(item.Field<string>(1)) ? item.Field<string>(1) == clsImplementationEnum.AutoCJCSeamType.RA.GetStringValue() && !string.IsNullOrWhiteSpace(item.Field<string>(12)) : false)
                        {
                            int Quantity;
                            if (!int.TryParse(item.Field<string>(12), out Quantity))
                            {
                                errors.Add(Quantity + "' is Not Valid, Please Enter Only Numeric Value"); hasError = true;
                            }
                        }
                    }

                    //Drawing Number Not Mandatory. If Exist: should be less than 25 characters
                    if (!string.IsNullOrWhiteSpace(item.Field<string>(9)) && item.Field<string>(9).Length > 25)
                    { errors.Add("Drawing No should be < 25 characters"); hasError = true; }


                    // Seam Length (Numeric, < 10, Mandatory)
                    if (string.IsNullOrEmpty(item.Field<string>(10)))
                    { errors.Add("Seam Length is Mandatory"); hasError = true; }

                    if (!IsAllDigits(item.Field<string>(10)))
                    { errors.Add("Seam Length should be Numeric Only"); hasError = true; }

                    if (item.Field<string>(10).Length > 10)
                    { errors.Add("Seam Length should be Maximum 10 digits"); hasError = true; }

                    //WEP Mandatory for 17547 observation.
                    string wepname = item.Field<string>(8);
                    string seamname = item.Field<string>(1);
                    if ((string.IsNullOrWhiteSpace(wepname) || string.IsNullOrEmpty(wepname)) && (seamname.ToLower() == clsImplementationEnum.SeamCategory.LongSeam.GetStringValue().ToLower() || seamname.ToLower() == clsImplementationEnum.SeamCategory.CircSeam.GetStringValue().ToLower() || seamname.ToLower() == clsImplementationEnum.SeamCategory.NozzleWeld.GetStringValue().ToLower()))
                    { errors.Add("WEP is Mandatory for this " + seamname + " Seam"); hasError = true; }

                    //WEP should be from Master GBL002, Mandatory
                    //if (!weps.Contains(item.Field<string>(8)))
                    //{ errors.Add("Invalid WEP"); hasError = true; } make WEP Details non-mandatory as per requirements by satish

                    if (string.IsNullOrEmpty(item.Field<string>(2)))
                    {
                        errors.Add("Number is mandatory");
                        hasError = true;
                    }
                    else
                    {
                        var seamno = Manager.ReplaceIllegalCharForSeamNo(item.Field<string>(1) + item.Field<string>(2));
                        //if (!string.IsNullOrEmpty(item.Field<string>(12)))
                        //{
                        string action = item.Field<string>(13).Trim().ToLower();
                        bool IsExist = false;
                        if (action == "edit" || action == "delete" || action == "revise")
                        {
                            var objQMS12line = lstSeamno.Where(i => i.SeamNo == seamno).FirstOrDefault();
                            IsExist = lstSeamno.Where(i => i.SeamNo == seamno && i.LineId != objQMS12line.LineId).Any();

                            if (action == "edit")
                            {
                                var objQMS012 = db.QMS012.Where(i => i.HeaderId == headerId && i.SeamNo == seamno).FirstOrDefault();
                                if (objQMS012 != null)
                                {
                                    var status = objQMS012.Status;
                                    if (status.ToLower() == clsImplementationEnum.ICLSeamStatus.Approved.GetStringValue().ToLower())
                                    {
                                        errors.Add("Approved seams can not be edit.");
                                        hasError = true;
                                    }
                                }
                                else
                                {
                                    errors.Add("Seam not present");
                                    hasError = true;
                                }
                            }
                            //Delete code need to be add
                            if (action == "delete")
                            {
                                var objQMS012 = db.QMS012.Where(i => i.HeaderId == headerId && i.SeamNo == seamno).FirstOrDefault();
                                string outReason;
                                if (objQMS012 == null)
                                {
                                    errors.Add("Seam not present");
                                    hasError = true;
                                }
                                else if (!IsApplicableForDelete(objQMS012.QualityProject, objQMS012.SeamNo, out outReason))
                                {
                                    errors.Add(outReason);
                                    hasError = true;
                                }
                            }

                            if (action == "revise")
                            {
                                var objQMS012 = db.QMS012.Where(i => i.HeaderId == headerId && i.SeamNo == seamno).FirstOrDefault();
                                if(objQMS012 == null)
                                {
                                    errors.Add("Seam not present");
                                    hasError = true;
                                }
                                else if (objQMS012.Status.ToLower() != clsImplementationEnum.ICLSeamStatus.Approved.GetStringValue().ToLower())
                                {
                                    errors.Add("Only approved seams can be revised.");
                                    hasError = true;
                                }
                                else
                                {
                                    string whereCondition = "1=1 and qms.HeaderId = " + headerId + " and qms.SeamNo='" + seamno + "'";
                                    var seamDetailList = db.SP_IPI_GETSEAMLISTLINES(0, 0, "", whereCondition).FirstOrDefault();
                                    if ((new clsManager()).GetMaxOfferedSequenceNo(seamDetailList.QualityProject, seamDetailList.BU, seamDetailList.Location, seamDetailList.SeamNo) > 0)
                                    {
                                        errors.Add("Offered seam can not be revised.");
                                        hasError = true;
                                    }
                                }
                            }
                        }
                        else
                        {
                            IsExist = IsSeamNoExists(seamno, header.QualityProject);
                            //IsExist = lstSeamno.Where(i => i.SeamNo == seamno).Any();
                        }

                        //if (!lstSeamno.Contains(seamno))
                        if (!IsExist)
                        {
                            item.SetField(16, seamno);
                        }
                        else
                        {
                            errors.Add("Seam No " + seamno + " must be unique");
                            hasError = true;
                            item.SetField<string>(14, string.Join(",", errors.ToArray()));
                        }
                        //}
                    }


                    //Position Number from BOM List. If multiple then input should be comma seperated.
                    string positionCell = item.Field<string>(4);
                    if (string.IsNullOrEmpty(positionCell))
                    {
                        errors.Add("Position Number is Mandatory");
                        hasError = true;
                        item.SetField<string>(14, string.Join(",", errors.ToArray()));
                    }
                    else
                    {
                        string[] splittedPositions = positionCell.Split(',');
                        List<string> ParentParts = new List<string>();

                        //var result = positions.Where(i => splittedPositions.Contains(i.ToString())).Select(i => new { Position = i.ToString() }).Distinct();
                        var result = true;
                        foreach (var pos in splittedPositions)
                        {
                            if (!positions.Any(i => i.ToString() == pos.Trim()))
                            {
                                result = false;
                            }
                        }

                        if (result)
                        {
                            var assemblies = GetPartParentList(positionCell, objBOMlist);

                            if (!string.IsNullOrEmpty(item.Field<string>(5)))
                            {
                                if (!assemblies.Contains(item.Field<string>(5)))
                                {
                                    errors.Add("Invalid Assembly");
                                    hasError = true;
                                }
                                else
                                {
                                    //var results = db.SP_IPI_GETSEAMLISTDETAILS(header.Project, item.Field<string>(5), "").ToList();
                                    //var results = objBOMlist.Where(x => x.Project == header.Project && x.ParentPart == item.Field<string>(5)).ToList();

                                    var ThicknessDefaultTemp = string.Empty;
                                    var MaterialTemp = string.Empty;
                                    for (int i = 0; i < splittedPositions.Count(); i++)
                                    {
                                        var tempresult = objBOMlist.Where(x => x.FindNo == splittedPositions[i]).FirstOrDefault();
                                        if (tempresult != null)
                                        {
                                            if (ThicknessDefaultTemp.Length == 0)
                                            {
                                                if (tempresult.Thickness.HasValue)
                                                {
                                                    ThicknessDefaultTemp = tempresult.Thickness.Value.ToString();
                                                }
                                            }
                                            else
                                            {
                                                if (tempresult.Thickness.HasValue)
                                                {
                                                    ThicknessDefaultTemp += "," + tempresult.Thickness.Value.ToString();
                                                }
                                            }


                                            if (MaterialTemp.Length == 0)
                                            {
                                                if (!string.IsNullOrEmpty(tempresult.MaterialDescription))
                                                {
                                                    MaterialTemp = tempresult.MaterialDescription.ToString();
                                                }
                                            }
                                            else
                                            {
                                                if (!string.IsNullOrEmpty(tempresult.MaterialDescription))
                                                {
                                                    MaterialTemp += "," + tempresult.MaterialDescription.ToString();
                                                }
                                            }
                                        }
                                    }
                                    string ThicknessDefault = ThicknessDefaultTemp;
                                    string Material = MaterialTemp;
                                    //string ThicknessDefault = string.Join(",", results.Where(x => splittedPositions.Contains(x.FindNo.ToString())).Select(x => x.Thickness).ToList());
                                    //string Material = string.Join(",", results.Where(x => splittedPositions.Contains(x.FindNo.ToString())).Select(x => x.MaterialDescription).ToList());

                                    if (!string.IsNullOrEmpty(ThicknessDefault))
                                    {
                                        if (!string.IsNullOrEmpty(Material))
                                        {
                                            //if (string.IsNullOrEmpty(item.Field<string>(12)))
                                            //{
                                            //    string action = item.Field<string>(12).Trim().ToLower();
                                            //    if ((action.ToLower() == "new" || action.ToLower() == "add" || action.ToLower() == "new record" || action.ToLower() == "new line") && string.IsNullOrEmpty(item.Field<string>(6)))
                                            //    {
                                            //        item.SetField(6, ThicknessDefault.Trim());
                                            //    }
                                            //}
                                            //above code is commented and below code is added on 16-04-2019. As discussed with trishul if thinckness is blank then set default thickness.
                                            //start code
                                            if (string.IsNullOrWhiteSpace(item.Field<string>(6)))
                                            {
                                                item.SetField(6, ThicknessDefault.Trim());
                                            }
                                            //end code

                                            item.SetField(4, positionCell.Trim());
                                            item.SetField(7, ThicknessDefault.Trim());
                                            item.SetField(17, headerId);
                                            item.SetField(15, Material.Trim());
                                        }
                                        else { errors.Add("Material is not available"); hasError = true; }
                                    }
                                    else { errors.Add("Thickness is not available"); hasError = true; }
                                }
                            }
                            else
                            {
                                item.SetField(4, positionCell.Trim());
                                item.SetField(17, headerId);
                                errors.Add("Assembly is required"); hasError = true;
                            }
                        }
                        else
                        {
                            errors.Add("Position No. doesn't match with BOM List");
                            hasError = true;
                        }

                        if (errors.Count > 0)
                        {
                            item.SetField<string>(14, string.Join(",", errors.ToArray()));
                        }
                    }
                    #endregion
                }

                if (!hasError)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        string action = item.Field<string>(13).Trim().ToLower();
                        switch (action)
                        {
                            #region Add line
                            case "new":
                            case "add":
                            case "new record":
                            case "new line":
                                {
                                    string weldType = item.Field<string>(0).Trim();
                                    string categoryCode = item.Field<string>(1).Split('-')[0].Trim();

                                    string seamNoFunc = GetMaxSeamNo(categoryCode, header.QualityProject);
                                    string unit = lstUnit.Where(x => x.JointTypeCode == weldType && x.SeamCategory == categoryCode).Select(x => x.Unit).FirstOrDefault();
                                    //if (categoryCode.Equals("OW") || categoryCode.Equals("CR"))
                                    //    unit = "Square Meter";
                                    //else unit = "mm";
                                    QMS012 objQMS012 = new QMS012();
                                    objQMS012.Location = objQMS011.Location;
                                    objQMS012.HeaderId = headerId;
                                    objQMS012.AssemblyNo = item.Field<string>(5);
                                    objQMS012.Material = item.Field<string>(15);
                                    objQMS012.Thickness = item.Field<string>(6);
                                    objQMS012.ThicknessDefault = item.Field<string>(7);
                                    objQMS012.Position = item.Field<string>(4);
                                    objQMS012.Project = header.Project;
                                    objQMS012.QualityProject = header.QualityProject;
                                    objQMS012.SeamDescription = !string.IsNullOrEmpty(item.Field<string>(1)) ? item.Field<string>(1).Trim() : null;
                                    objQMS012.SeamCategory = categoryCode;
                                    objQMS012.SeamListNo = header.SeamListNo;
                                    objQMS012.SeamNo = !string.IsNullOrEmpty(item.Field<string>(16)) ? Manager.ReplaceIllegalCharForSeamNo(item.Field<string>(16).Trim()) : null;
                                    objQMS012.ManualSeamNo = !string.IsNullOrEmpty(item.Field<string>(2)) ? Manager.ReplaceIllegalCharForSeamNo(item.Field<string>(2).Trim()) : null;
                                    objQMS012.DocNoSeam = int.Parse(seamNoFunc.Split('@')[1]);
                                    objQMS012.SeamQuantity = !string.IsNullOrEmpty(item.Field<string>(12)) ? Convert.ToDouble(item.Field<string>(12)) : 0;

                                    objQMS012.SeamRev = 0;
                                    if (!string.IsNullOrEmpty(item.Field<string>(10)))
                                    {
                                        objQMS012.SeamLengthArea = int.Parse(item.Field<string>(10).Trim());
                                    }
                                    else
                                    {
                                        objQMS012.SeamLengthArea = null;
                                    }
                                    objQMS012.Remarks = !string.IsNullOrEmpty(item.Field<string>(11)) ? item.Field<string>(11).Trim() : null;
                                    objQMS012.Unit = unit;
                                    objQMS012.ViewDrawing = !string.IsNullOrEmpty(item.Field<string>(9)) ? item.Field<string>(9).Trim() : null;
                                    objQMS012.WeldType = !string.IsNullOrEmpty(item.Field<string>(0)) ? item.Field<string>(0).Trim() : null;
                                    objQMS012.WEPDetail = !string.IsNullOrEmpty(item.Field<string>(8)) ? item.Field<string>(8).Trim() : null;
                                    objQMS012.Status = clsImplementationEnum.ICLSeamStatus.Draft.GetStringValue();
                                    objQMS012.CreatedBy = objClsLoginInfo.UserName;
                                    objQMS012.CreatedOn = DateTime.Now;
                                    if (!string.IsNullOrEmpty(item.Field<string>(12)))
                                    {
                                        objQMS012.SeamQuantity = Convert.ToDouble(item.Field<string>(12));
                                    }
                                    else
                                    {
                                        objQMS012.SeamQuantity = null;
                                    }
                                    db.QMS012.Add(objQMS012);
                                    db.SaveChanges();
                                }
                                break;
                            #endregion
                            #region Edit line
                            case "edit":
                            case "edit record":
                            case "edit line":
                                {
                                    string seamNo = Manager.ReplaceIllegalCharForSeamNo(item.Field<string>(16).Trim());
                                    QMS012 objQMS012 = new QMS012();
                                    objQMS012 = db.QMS012.Where(i => i.HeaderId == headerId && i.SeamNo == seamNo).FirstOrDefault();

                                    if (objQMS012.Status == clsImplementationEnum.ICLSeamStatus.Draft.GetStringValue() && objQMS012.SeamRev == int.Parse(item.Field<string>(3).TrimStart('R')))
                                    {
                                        objQMS012.Location = objQMS011.Location;
                                        objQMS012.Position = !string.IsNullOrEmpty(item.Field<string>(4)) ? item.Field<string>(4).Trim() : null;
                                        objQMS012.AssemblyNo = !string.IsNullOrEmpty(item.Field<string>(5)) ? item.Field<string>(5).Trim() : null;
                                        objQMS012.Material = item.Field<string>(15);
                                        objQMS012.Thickness = item.Field<string>(6);
                                        if (!string.IsNullOrEmpty(item.Field<string>(10)))
                                        {
                                            objQMS012.SeamLengthArea = int.Parse(item.Field<string>(10).Trim());
                                        }
                                        else
                                        {
                                            objQMS012.SeamLengthArea = null;
                                        }
                                        objQMS012.Remarks = !string.IsNullOrEmpty(item.Field<string>(11)) ? item.Field<string>(11).Trim() : null;
                                        objQMS012.ViewDrawing = !string.IsNullOrEmpty(item.Field<string>(9)) ? item.Field<string>(9).Trim() : null;
                                        objQMS012.WEPDetail = !string.IsNullOrEmpty(item.Field<string>(8)) ? item.Field<string>(8).Trim() : null;
                                        if (!string.IsNullOrEmpty(item.Field<string>(12)))
                                        {
                                            objQMS012.SeamQuantity = Convert.ToDouble(item.Field<string>(12));
                                        }
                                        else
                                        {
                                            objQMS012.SeamQuantity = null;
                                        }
                                        objQMS012.EditedBy = objClsLoginInfo.UserName;
                                        objQMS012.EditedOn = DateTime.Now;
                                        db.SaveChanges();
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }

                                break;
                            #endregion
                            #region delete line
                            case "delete":
                            case "delete record":
                            case "delete line":
                                {
                                    string seamNoToDelete = Manager.ReplaceIllegalCharForSeamNo(item.Field<string>(16).Trim());
                                    QMS012 objQMS012ToDelete = new QMS012();
                                    objQMS012ToDelete = db.QMS012.Where(i => i.HeaderId == headerId && i.SeamNo == seamNoToDelete).FirstOrDefault();
                                    if (objQMS012ToDelete != null)
                                    {
                                        RemoveSeamRelatedData(objQMS012ToDelete.QualityProject, objQMS012ToDelete.SeamNo);
                                        db.QMS012.Remove(objQMS012ToDelete);
                                        db.SaveChanges();
                                    }
                                }
                                break;
                            #endregion
                            #region revise line
                            case "revise":
                            case "revise record":
                            case "revise line":
                                {
                                    string seamNo = Manager.ReplaceIllegalCharForSeamNo(item.Field<string>(16).Trim());
                                    QMS012 objQMS012 = new QMS012();
                                    objQMS012 = db.QMS012.Where(i => i.HeaderId == headerId && i.SeamNo == seamNo).FirstOrDefault();
                                    if (objQMS012.Status == clsImplementationEnum.ICLSeamStatus.Approved.GetStringValue())
                                    {
                                        objQMS012.SeamRev += 1;
                                        objQMS012.Location = objQMS011.Location;
                                        objQMS012.Status = clsImplementationEnum.PAMStatus.Draft.GetStringValue();
                                        objQMS012.Position = !string.IsNullOrEmpty(item.Field<string>(4)) ? item.Field<string>(4).Trim() : null;
                                        objQMS012.AssemblyNo = !string.IsNullOrEmpty(item.Field<string>(5)) ? item.Field<string>(5).Trim() : null;
                                        objQMS012.Material = item.Field<string>(15);
                                        objQMS012.Thickness = item.Field<string>(6);
                                        if (!string.IsNullOrEmpty(item.Field<string>(10)))
                                        {
                                            objQMS012.SeamLengthArea = int.Parse(item.Field<string>(10).Trim());
                                        }
                                        else
                                        {
                                            objQMS012.SeamLengthArea = null;
                                        }
                                        if (!string.IsNullOrEmpty(item.Field<string>(12)))
                                        {
                                            objQMS012.SeamQuantity = Convert.ToDouble(item.Field<string>(12));
                                        }
                                        else
                                        {
                                            objQMS012.SeamQuantity = null;
                                        }
                                        objQMS012.Remarks = !string.IsNullOrEmpty(item.Field<string>(11)) ? item.Field<string>(11).Trim() : null;
                                        objQMS012.ViewDrawing = !string.IsNullOrEmpty(item.Field<string>(9)) ? item.Field<string>(9).Trim() : null;
                                        objQMS012.WEPDetail = !string.IsNullOrEmpty(item.Field<string>(8)) ? item.Field<string>(8).Trim() : null;
                                        objQMS012.EditedBy = objClsLoginInfo.UserName;
                                        objQMS012.EditedOn = DateTime.Now;
                                        db.SaveChanges();
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                                break;
                                #endregion
                        }
                    }

                    int HeaderID = headerId;
                    //return RedirectToAction("AddHeader?headerId="+Convert.ToString(headerId));
                    //return RedirectToAction("AddHeader", "MaintainSeamList", new { headerId = Convert.ToString(headerId) });
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Data Upload Successfully!";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return ErrorExportToExcel(dt);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return null;
            }
        }


        public ActionResult ErrorExportToExcel(DataTable dt)
        {
            clsHelper.ResponceMsgWithFileName objResponseMsg = new clsHelper.ResponceMsgWithFileName();
            if (dt.Columns.Contains("HeaderId"))
            {
                dt.Columns.Remove("HeaderID");
            }
            if (dt.Columns.Contains("Material"))
            {
                dt.Columns.Remove("Material");
            }
            //if (dt.Columns.Contains("Thickness"))
            //{
            //    dt.Columns.Remove("Thickness");
            //}
            if (dt.Columns.Contains("SeamNo"))
            {
                dt.Columns.Remove("SeamNo");
            }
            try
            {
                MemoryStream ms = new MemoryStream();
                using (FileStream fs = System.IO.File.OpenRead(Server.MapPath("~/Areas/IPI/Views/MaintainSeamList/SeamList_Error.xlsx")))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();
                        int i = 2;
                        foreach (DataRow item in dt.Rows)
                        {
                            excelWorksheet.Cells[i, 1].Value = item.Field<string>(0);
                            excelWorksheet.Cells[i, 2].Value = item.Field<string>(1);
                            excelWorksheet.Cells[i, 3].Value = item.Field<string>(2);
                            excelWorksheet.Cells[i, 4].Value = item.Field<string>(3);
                            excelWorksheet.Cells[i, 5].Value = item.Field<string>(4);
                            excelWorksheet.Cells[i, 6].Value = item.Field<string>(5);
                            excelWorksheet.Cells[i, 7].Value = item.Field<string>(6);
                            excelWorksheet.Cells[i, 8].Value = item.Field<string>(7);
                            excelWorksheet.Cells[i, 9].Value = item.Field<string>(8);
                            excelWorksheet.Cells[i, 10].Value = item.Field<string>(9);
                            excelWorksheet.Cells[i, 11].Value = item.Field<string>(10);
                            excelWorksheet.Cells[i, 12].Value = item.Field<string>(11);
                            excelWorksheet.Cells[i, 13].Value = item.Field<string>(12);
                            excelWorksheet.Cells[i, 14].Value = item.Field<string>(13);
                            excelWorksheet.Cells[i, 15].Value = item.Field<string>(14);
                            i++;
                        }
                        excelPackage.SaveAs(ms);
                    }
                }

                ms.Position = 0;
                string fileName;
                string excelFilePath = Helper.ExcelFilePath("ErrorList", out fileName);
                Byte[] bin = ms.ToArray();
                System.IO.File.WriteAllBytes(excelFilePath, bin);

                objResponseMsg.Key = false;
                objResponseMsg.FileName = fileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

                //var cd = new System.Net.Mime.ContentDisposition
                //{
                //    FileName = "ErrorList.xlsx",
                //    Inline = false,
                //};
                //// Added by Dharmesh Vasani(90348357) not downloading file after Generate issue
                //Response.Clear();
                //Response.AppendHeader("Content-Disposition", cd.ToString());
                //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //Response.Cache.SetCacheability(HttpCacheability.NoCache);
                //Response.BinaryWrite(ms.ToArray());
                //Response.Flush();
                //Response.End();
                //return new FileStreamResult(ms, "application/xlsx")
                //{
                //    FileDownloadName = "ErrorList.xlsx"
                //};
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        bool IsAllDigits(string s)
        {
            return s.All(char.IsDigit);
        }

        #region Export To Excel

        public string GenerateExcelSeamList<T>(List<T> query, string userName)
        {
            //try
            {
                using (ExcelPackage excelPackage = new ExcelPackage())
                {
                    ExcelWorksheet ws = excelPackage.Workbook.Worksheets.Add("Sheet1");

                    //Added by Dharmesh Vasani for Change Excel Column Title
                    //get our column headings
                    var t = typeof(T);
                    var Headings = t.GetProperties();
                    string xmlData = HttpContext.Server.MapPath("~/ExcelSetting.xml");
                    DataSet ds = new DataSet();//Using dataset to read xml file  
                    ds.ReadXml(xmlData);
                    var DataColumns = new List<ExcelDateCoumn>();
                    DataColumns = (from rows in ds.Tables[0].AsEnumerable()
                                   select new ExcelDateCoumn
                                   {
                                       Name = (rows[0].ToString()).Trim()
                                   }).ToList();

                    List<string> lstDateCoumnName = DataColumns.Select(x => x.Name.ToUpper()).ToList();
                    var lstTextCoumnName = new string[] { "POSITION", "FINISHEDTHICKNESS", "THICKNESSOFPARTS" };

                    for (int i = 0; i < Headings.Count(); i++)
                    {
                        //Added by Dharmesh Vasani for Change Excel Column Title
                        ws.Cells[1, i + 1].Value = Headings[i].Name;
                        //For Date Format                        
                        if (lstDateCoumnName.Contains(Headings[i].Name.ToUpper()))
                        {
                            var firstRowCell = ws.Cells[ws.Dimension.Start.Row, ws.Dimension.Start.Column, 1, ws.Dimension.End.Column].Where(x => x.Text.ToUpper() == Headings[i].Name.ToUpper()).FirstOrDefault();
                            if (firstRowCell != null)
                            {
                                ws.Column(firstRowCell.Start.Column).Style.Numberformat.Format = "dd-MMM-yyyy hh:mm";
                                ws.Column(firstRowCell.Start.Column).AutoFit();
                            }
                            //ws.Column(ws.Cells[1, i + 1].Columns).Style.Numberformat.Format = "yyyy-mm-dd";
                        }
                        if (lstTextCoumnName.Contains(Headings[i].Name.ToUpper()))
                        {
                            var firstRowCell = ws.Cells[ws.Dimension.Start.Row, ws.Dimension.Start.Column, 1, ws.Dimension.End.Column].Where(x => x.Text.ToUpper() == Headings[i].Name.ToUpper()).FirstOrDefault();
                            if (firstRowCell != null)
                            {
                                ws.Column(firstRowCell.Start.Column).Style.Numberformat.Format = "@";
                                ws.Column(firstRowCell.Start.Column).AutoFit();
                            }
                        }
                    }

                    //populate our Data
                    if (query.Count() > 0)
                    {
                        ws.Cells["A2"].LoadFromCollection(query);
                    }

                    //Format the header                    
                    using (ExcelRange rng = ws.Cells[1, 1, 1, Headings.Count()])
                    {
                        rng.Style.Font.Bold = true;
                        rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid; //Set Pattern for the background to Solid
                        rng.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(79, 129, 189)); //Set color to dark blue
                        rng.Style.Font.Color.SetColor(System.Drawing.Color.White);
                    }

                    //End
                    string fileName;
                    string excelFilePath = Helper.ExcelFilePath(userName, out fileName);

                    ws.Cells.AutoFitColumns();

                    Byte[] bin = excelPackage.GetAsByteArray();
                    System.IO.File.WriteAllBytes(excelFilePath, bin);

                    return fileName;
                }
            }
        }

        public FileStreamResult ExportToExcel(int id)
        {
            List<ExportSeamList> listExport = new List<ExportSeamList>();
            try
            {
                var lstResult = db.SP_IPI_GETSEAMLISTLINESFOREXCEL(id).ToList();

                foreach (var item in lstResult)
                {
                    listExport.Add(new ExportSeamList { Thickness = item.Thickness, Number = item.Number, Seam_Category = item.Seam_Category, Position = item.Position, Revision = item.Revision, View___Drawing = item.View___Drawing, Weld_Type = item.Weld_Type, Seam_Length_Area = item.Seam_Length_Area.ToString(), WEP_Detail = item.WEP_Detail, Remarks = item.Remarks, Assembly_Number = item.Assembly_Number, Action = item.Action, });
                }

                MemoryStream ms = new MemoryStream();
                using (FileStream fs = System.IO.File.OpenRead(Server.MapPath("~/Areas/IPI/Views/MaintainSeamList/SeamList.xlsx")))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();
                        int i = 2;
                        foreach (var item in lstResult)
                        {
                            excelWorksheet.Cells[i, 1].Value = item.Weld_Type.ToString();
                            excelWorksheet.Cells[i, 2].Value = item.Seam_Category.ToString();
                            excelWorksheet.Cells[i, 3].Value = item.Number.ToString();
                            excelWorksheet.Cells[i, 4].Value = item.Revision.ToString();
                            excelWorksheet.Cells[i, 5].Value = item.Position.ToString();
                            excelWorksheet.Cells[i, 6].Value = item.Assembly_Number.ToString();
                            excelWorksheet.Cells[i, 7].Value = item.Thickness.ToString();
                            excelWorksheet.Cells[i, 8].Value = item.ThicknessDefault.ToString();
                            excelWorksheet.Cells[i, 9].Value = item.WEP_Detail.ToString();
                            excelWorksheet.Cells[i, 10].Value = item.View___Drawing.ToString();
                            excelWorksheet.Cells[i, 11].Value = item.Seam_Length_Area.ToString();
                            excelWorksheet.Cells[i, 12].Value = item.Remarks.ToString();
                            excelWorksheet.Cells[i, 13].Value = Convert.ToString(item.SeamQuantity);
                            excelWorksheet.Cells[i, 14].Value = item.Action.ToString();
                            i++;
                        }
                        excelPackage.SaveAs(ms);
                    }
                }

                ms.Position = 0;

                return new FileStreamResult(ms, "application/xlsx")
                {
                    FileDownloadName = "SeamList.xlsx"
                };
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return null;
            }
        }

        #endregion

        [Serializable]
        public class ExportSeamList
        {
            public string Weld_Type { get; set; }
            public string Seam_Category { get; set; }
            public string Number { get; set; }
            public string Revision { get; set; }
            public string Position { get; set; }
            public string Assembly_Number { get; set; }
            public string Thickness { get; set; }
            public string Thickness_Default { get; set; }
            public string WEP_Detail { get; set; }
            public string View___Drawing { get; set; }
            public string Seam_Length_Area { get; set; }
            public string Remarks { get; set; }
            public string Action { get; set; }
        }

        public class ExcelExportHelper
        {
            public static string ExcelContentType
            {
                get
                { return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"; }
            }

            public static DataTable ListToDataTable<T>(List<T> data)
            {
                PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
                DataTable dataTable = new DataTable();

                for (int i = 0; i < properties.Count; i++)
                {
                    PropertyDescriptor property = properties[i];
                    dataTable.Columns.Add(property.Name, Nullable.GetUnderlyingType(property.PropertyType) ?? property.PropertyType);
                }

                object[] values = new object[properties.Count];
                foreach (T item in data)
                {
                    for (int i = 0; i < values.Length; i++)
                    {
                        values[i] = properties[i].GetValue(item);
                    }

                    dataTable.Rows.Add(values);
                }
                return dataTable;
            }

            public static byte[] ExportExcel(DataTable dataTable, string heading = "", bool showSrNo = false, params string[] columnsToTake)
            {

                byte[] result = null;
                using (ExcelPackage package = new ExcelPackage())
                {
                    ExcelWorksheet workSheet = package.Workbook.Worksheets.Add(String.Format("{0} Data", heading));
                    int startRowFrom = String.IsNullOrEmpty(heading) ? 1 : 3;

                    if (showSrNo)
                    {
                        DataColumn dataColumn = dataTable.Columns.Add("SrNo.", typeof(int));
                        dataColumn.SetOrdinal(0);
                        int index = 1;
                        foreach (DataRow item in dataTable.Rows)
                        {
                            item[0] = index;
                            index++;
                        }
                    }


                    // add the content into the Excel file 
                    workSheet.Cells["A" + startRowFrom].LoadFromDataTable(dataTable, true);

                    // autofit width of cells with small content 
                    int columnIndex = 1;
                    foreach (DataColumn column in dataTable.Columns)
                    {
                        ExcelRange columnCells = workSheet.Cells[workSheet.Dimension.Start.Row, columnIndex, workSheet.Dimension.End.Row, columnIndex];
                        int maxLength = columnCells.Max(cell => cell.Value.ToString().Count());
                        if (maxLength < 150)
                        {
                            workSheet.Column(columnIndex).AutoFit();
                        }


                        columnIndex++;
                    }

                    #region Set Header Row Style
                    // format header - bold, yellow on black 
                    using (ExcelRange r = workSheet.Cells[startRowFrom, 1, startRowFrom, dataTable.Columns.Count])
                    {
                        r.Style.Font.Color.SetColor(System.Drawing.Color.Black);
                        r.Style.Font.Bold = true;
                        r.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.None;
                        //r.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        //r.Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFF"));
                    }
                    #endregion

                    #region For Set Border
                    //// format cells - add borders 
                    ////using (ExcelRange r = workSheet.Cells[startRowFrom + 1, 1, startRowFrom + dataTable.Rows.Count, dataTable.Columns.Count])//Skip Header Row
                    //using (ExcelRange r = workSheet.Cells[startRowFrom, 1, startRowFrom + dataTable.Rows.Count, dataTable.Columns.Count])
                    //{
                    //    r.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    //    r.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    //    r.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    //    r.Style.Border.Right.Style = ExcelBorderStyle.Thin;

                    //    r.Style.Border.Top.Color.SetColor(System.Drawing.Color.Black);
                    //    r.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
                    //    r.Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);
                    //    r.Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
                    //}
                    #endregion

                    #region Remove Extra Column For Export
                    // removed ignored columns 
                    for (int i = dataTable.Columns.Count - 1; i >= 0; i--)
                    {
                        if (i == 0 && showSrNo)
                        {
                            continue;
                        }
                        if (!columnsToTake.Contains(dataTable.Columns[i].ColumnName))
                        {
                            workSheet.DeleteColumn(i + 1);
                        }
                    }
                    #endregion

                    #region Set Heading If Pass
                    if (!String.IsNullOrEmpty(heading))
                    {
                        workSheet.Cells["A1"].Value = heading;
                        workSheet.Cells["A1"].Style.Font.Size = 20;

                        workSheet.InsertColumn(1, 1);
                        workSheet.InsertRow(1, 1);
                        workSheet.Column(1).Width = 5;
                    }
                    #endregion

                    result = package.GetAsByteArray();
                }

                return result;
            }

            public static byte[] ExportExcel<T>(List<T> data, string Heading = "", bool showSlno = false, params string[] ColumnsToTake)
            {
                return ExportExcel(ListToDataTable<T>(data), Heading, showSlno, ColumnsToTake);
            }

        }


        #endregion

        #region Section Wise Seam List

        [HttpPost]
        public ActionResult GetQualityProjectList(string term)
        {
            List<ddlValue> objList = null;
            var lstlocation = Manager.GetUserwiseLocation(objClsLoginInfo.UserName);
            if (!string.IsNullOrWhiteSpace(term))
            {
                objList = (from a in db.QMS010
                           where lstlocation.Contains(a.Location) && a.QualityProject.Trim().ToLower().Contains(term.Trim().ToLower())
                           select new ddlValue { Text = a.QualityProject + " (" + a.Location.Trim() + ")", Value = a.QualityProject, id = a.QualityProject + "#" + a.Location.Trim() }).Distinct().ToList();
            }
            else
            {
                objList = (from a in db.QMS010
                           where lstlocation.Contains(a.Location)
                           select new ddlValue { Text = a.QualityProject + " (" + a.Location.Trim() + ")", Value = a.QualityProject, id = a.QualityProject + "#" + a.Location.Trim() }).Distinct().Take(10).ToList();
            }
            return Json(objList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetAssemblyNoList(string qualityProject, string term)
        {
            List<ddlValue> objList = null;
            var lstlocation = Manager.GetUserwiseLocation(objClsLoginInfo.UserName);
            if (!string.IsNullOrWhiteSpace(qualityProject))
            {
                if (!string.IsNullOrWhiteSpace(term))
                {
                    objList = (from a in db.QMS012
                               where a.QualityProject.Trim().ToLower() == qualityProject.Trim().ToLower()
                               && a.AssemblyNo.ToLower().Contains(term.ToLower())
                               select new ddlValue { Text = a.AssemblyNo, Value = a.AssemblyNo, id = a.AssemblyNo }).Distinct().ToList();
                }
                else
                {
                    objList = (from a in db.QMS012
                               where a.QualityProject.Trim().ToLower() == qualityProject.Trim().ToLower()
                               select new ddlValue { Text = a.AssemblyNo, Value = a.AssemblyNo, id = a.AssemblyNo }).Distinct().ToList();
                }
            }

            return Json(objList, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult SectionWiseSeamList()
        {
            return View();
        }

        public ActionResult LoadSectionWiseSeamListGridDataPartial(string QualityProject, string AssemblyNo)
        {
            ViewBag.QualityProject = QualityProject;
            ViewBag.AssemblyNo = AssemblyNo;
            return PartialView("_LoadSectionWiseSeamListGridDataPartial");
        }

        public ActionResult LoadSectionWiseSeamListGridData(JQueryDataTableParamModel param, string assemblyNo)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                if (!string.IsNullOrEmpty(assemblyNo))
                {
                    StartIndex = null;
                    EndIndex = null;
                }

                string QualityProject = param.Project;

                string Project = "";
                string Location = param.Location;
                var objQMS010 = db.QMS010.Where(x => x.QualityProject == QualityProject).FirstOrDefault();
                if (objQMS010 != null)
                {
                    Project = objQMS010.Project;
                    // Location = objQMS010.Location;
                }

                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;

                string whereCondition = " QualityProject = '" + QualityProject + "'";
                string[] columnName = { "WeldType", "SeamCategory", "SeamNo", "SeamRev", "Position", "WEPDetail", "SeamLengthArea", "ViewDrawing", "Thickness", "ThicknessDefault" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                else
                {
                    strSortOrder = " Order By SeamNo asc ";
                }

                var lstLines = db.SP_IPI_GET_SECTION_WISE_SEAM_LIST_DATA(Project, Location, StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();


                List<SP_IPI_GET_SECTION_WISE_SEAM_LIST_DATA_Result> temp = new List<SP_IPI_GET_SECTION_WISE_SEAM_LIST_DATA_Result>();
                if (!string.IsNullOrEmpty(assemblyNo))
                {

                    var TopCount = lstLines.Where(x => x.TopAssembly == assemblyNo).Count();
                    if (TopCount > 0)
                    {
                        foreach (var item in lstLines.Where(x => x.TopAssembly == assemblyNo))
                        {
                            temp.Add(item);
                        }
                    }
                    var MainCount = lstLines.Where(x => x.MainAssembly == assemblyNo).Count();
                    if (MainCount > 0)
                    {
                        foreach (var item in lstLines.Where(x => x.MainAssembly == assemblyNo))
                        {
                            temp.Add(item);
                        }
                    }
                    var SubCount = lstLines.Where(x => x.SubAssembly == assemblyNo).Count();
                    if (SubCount > 0)
                    {
                        foreach (var item in lstLines.Where(x => x.SubAssembly == assemblyNo))
                        {
                            temp.Add(item);
                        }
                    }
                    var ChildCount = lstLines.Where(x => x.ChildAssembly == assemblyNo).Count();
                    if (ChildCount > 0)
                    {
                        foreach (var item in lstLines.Where(x => x.ChildAssembly == assemblyNo))
                        {
                            temp.Add(item);
                        }
                    }

                    if (sortColumnName == "")
                        temp = temp.OrderBy(x => x.ROW_NO).ToList();
                    lstLines = temp;

                    if (totalRecords != null && totalRecords != 0)
                    {
                        var tempCount = totalRecords - lstLines.Count();
                        totalRecords = totalRecords - tempCount;
                    }

                }


                var res = (from c in lstLines
                           select new[] {
                                        Convert.ToString(c.ROW_NO),
                                        Convert.ToString(c.LineId),
                                        c.WeldType,
                                        c.SeamCategory,
                                        c.ManualSeamNo,
                                        c.SeamNo,
                                        "R"+c.SeamRev,
                                        c.Position,
                                        c.ChildAssembly,
                                        c.SubAssembly,
                                        c.MainAssembly,
                                        c.TopAssembly,
                                        c.Thickness,
                                        c.ThicknessDefault,
                                        c.WEPDetail,
                                        c.ViewDrawing,
                                        Convert.ToString(c.SeamLengthArea),
                                        c.Result
                          }).ToList();

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""

                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GenerateExcelSectionWiseSeamList(string whereCondition, string strSortOrder, string QualityProject, string assemblyNo, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                string Project = string.Empty;
                var objQMS010 = db.QMS010.Where(x => x.QualityProject == QualityProject).FirstOrDefault();
                if (objQMS010 != null)
                    Project = objQMS010.Project;

                var lst = db.SP_IPI_GET_SECTION_WISE_SEAM_LIST_DATA(Project, objClsLoginInfo.Location, 1, int.MaxValue, strSortOrder, whereCondition).ToList();

                List<SP_IPI_GET_SECTION_WISE_SEAM_LIST_DATA_Result> temp = new List<SP_IPI_GET_SECTION_WISE_SEAM_LIST_DATA_Result>();
                if (!string.IsNullOrEmpty(assemblyNo))
                {

                    var TopCount = lst.Where(x => x.TopAssembly == assemblyNo).Count();
                    if (TopCount > 0)
                    {
                        foreach (var item in lst.Where(x => x.TopAssembly == assemblyNo))
                        {
                            temp.Add(item);
                        }
                    }
                    var MainCount = lst.Where(x => x.MainAssembly == assemblyNo).Count();
                    if (MainCount > 0)
                    {
                        foreach (var item in lst.Where(x => x.MainAssembly == assemblyNo))
                        {
                            temp.Add(item);
                        }
                    }
                    var SubCount = lst.Where(x => x.SubAssembly == assemblyNo).Count();
                    if (SubCount > 0)
                    {
                        foreach (var item in lst.Where(x => x.SubAssembly == assemblyNo))
                        {
                            temp.Add(item);
                        }
                    }
                    var ChildCount = lst.Where(x => x.ChildAssembly == assemblyNo).Count();
                    if (ChildCount > 0)
                    {
                        foreach (var item in lst.Where(x => x.ChildAssembly == assemblyNo))
                        {
                            temp.Add(item);
                        }
                    }

                    lst = temp;
                }




                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No data available";
                }

                var newlst = (from li in lst
                              select new
                              {
                                  WeldType = li.WeldType,
                                  SeamCategory = li.SeamDescription,
                                  Number = li.ManualSeamNo,
                                  Revision = "R" + Convert.ToString(li.SeamRev),
                                  Position = li.Position,
                                  ChildAssemblyNumber = li.ChildAssembly,
                                  SubAssemblyNumber = li.SubAssembly,
                                  MainAssemblyNumber = li.MainAssembly,
                                  TopAssembly = li.TopAssembly,
                                  FinishedThickness = li.Thickness,
                                  ThicknessOfParts = li.ThicknessDefault,
                                  WEPDetail = li.WEPDetail,
                                  ViewDrawing = li.ViewDrawing,
                                  SeamLengthArea = li.SeamLengthArea,
                                  seamquantity = li.SeamListNo,
                                  Results = li.Result,
                              }).ToList();

                strFileName = GenerateExcelSeamList(newlst, objClsLoginInfo.UserName);

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion
    }

    public class Parents
    {
        public Parents()
        {
            lstPartParent = new List<Parents>();
        }

        public string ParentpartName { get; set; }
        public List<Parents> lstPartParent { get; set; }
    }
}