﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using Org.BouncyCastle.Math;

namespace IEMQS.Areas.IPI.Controllers
{
    public class ReportsController : clsBase
    {
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Index()
        {
            ViewBag.isQC = Manager.IsUserInRole(objClsLoginInfo.UserName, new string[] { clsImplementationEnum.UserRoleName.QC2.GetStringValue(), clsImplementationEnum.UserRoleName.QC3.GetStringValue(), clsImplementationEnum.UserRoleName.QI1.GetStringValue(), clsImplementationEnum.UserRoleName.QI2.GetStringValue(), clsImplementationEnum.UserRoleName.QI3.GetStringValue() });
            return View();
        }

        [HttpPost]
        public ActionResult LoadReportInputFilterPartial(string reporttype)
        {
            //0 - false, 1 - true

            ViewBag.ShowLocation = 0;
            ViewBag.ShowProject = 0;
            ViewBag.ShowSeamNo = 0;
            ViewBag.ShowQID = 0;
            ViewBag.ReportType = reporttype;
            ViewBag.PrintedBy = objClsLoginInfo.UserName;
            var lstStageType = new List<SelectListItem>()
            { new SelectListItem { Text = "PT", Value = "PT" },
              new SelectListItem { Text = "MT", Value = "MT" },
              new SelectListItem { Text = "UT", Value = "UT" },
              new SelectListItem { Text = "RT", Value = "RT" },
            };
            if (reporttype == clsImplementationEnum.IPIReports.QID.GetStringValue())
            {
                ViewBag.ShowLocation = 1;
                ViewBag.ShowProject = 1;
                ViewBag.ShowQID = 1;
                ViewBag.ShowQIDApplicableFor = 1;

                var lstQualityIdApplicableList = new List<SelectListItem>();
                lstQualityIdApplicableList.Add(new SelectListItem { Text = "All", Value = "All" });
                List<string> lstQualityIdApplicable = clsImplementationEnum.GetQualityIdApplicableCategory().ToList();
                foreach (string item in lstQualityIdApplicable)
                {
                    lstQualityIdApplicableList.Add(new SelectListItem { Text = item, Value = item });
                }
                ViewBag.VBlstQualityIdApplicable = lstQualityIdApplicableList;
            }
            else if (reporttype == clsImplementationEnum.IPIReports.TP.GetStringValue())
            {
                ViewBag.ShowLocation = 1;
                ViewBag.ShowProject = 1;
                ViewBag.ShowSeamNo = 1;
            }
            else if (reporttype == clsImplementationEnum.IPIReports.SWP.GetStringValue())
            {
                ViewBag.ShowLocation = 1;
                ViewBag.ShowProject = 1;
                ViewBag.ShowSeamNo = 1;
            }
            else if (reporttype == clsImplementationEnum.IPIReports.GeneralNotes.GetStringValue())
            {
                ViewBag.ShowLocation = 1;
                ViewBag.ShowProject = 1;
            }
            else if (reporttype == clsImplementationEnum.IPIReports.SeamWiseWeldersWelding.GetStringValue())
            {
                ViewBag.ShowSeamNoFrom = 1;
                ViewBag.ShowSeamNoTo = 1;
                ViewBag.ShowProjectAutoComplete = 1;
            }
            else if (reporttype == clsImplementationEnum.IPIReports.SeamICL.GetStringValue())
            {
                ViewBag.ShowFromToDate = 1;
                ViewBag.ShowFromToQualityProjectBuwise = 1;
                ViewBag.ShowFromToShop = 1;

                var objATH001 = db.ATH001.Where(i => i.Employee == objClsLoginInfo.UserName).Select(i => new { BU = i.BU, Location = i.Location, Role = i.Role }).ToList();
                var objLocation = objATH001.Select(i => i.Location).Distinct().ToList();

                var listLocation = (from c1 in db.COM002
                                    where c1.t_dtyp == 1 && objLocation.Contains(c1.t_dimx)
                                    select new { id = c1.t_dimx, text = c1.t_desc, CatID = c1.t_dimx, CatDesc = c1.t_desc }
                                  ).Distinct().ToList();

                ViewBag.Location = listLocation;
            }
            else if (reporttype == clsImplementationEnum.IPIReports.PrintTestPlan.GetStringValue())
            {
                ViewBag.ShowLocationAutocomplete = 1;
                ViewBag.ShowProjectAutoComplete = 1;
                ViewBag.ShowTypewiseStages = 1;
                var objATH001 = db.ATH001.Where(i => i.Employee == objClsLoginInfo.UserName).Select(i => new { BU = i.BU, Location = i.Location, Role = i.Role }).ToList();
                var objLocation = objATH001.Select(i => i.Location).Distinct().ToList();

                var listLocation = (from c1 in db.COM002
                                    where c1.t_dtyp == 1 && objLocation.Contains(c1.t_dimx)
                                    select new { id = c1.t_dimx, text = c1.t_desc, CatID = c1.t_dimx, CatDesc = c1.t_desc }
                                  ).Distinct().ToList();
                lstStageType.Add(new SelectListItem { Text = "Chemical", Value = "Chemical" });
                lstStageType.Add(new SelectListItem { Text = "INSP", Value = "INSP" });

                ViewBag.Location = listLocation;
                ViewBag.ShowStageType = 1;
            }
            else if (reporttype == clsImplementationEnum.IPIReports.SeamStatus.GetStringValue())
            {
                var lstSeamApprovalStatus = new List<SelectListItem>()
                                            { new SelectListItem { Text = "Select", Value = "" },
                                              new SelectListItem { Text = "Approved", Value = "Approved" },
                                              new SelectListItem { Text = "WIP (Work In Progress)", Value = "WIP" },
                                              new SelectListItem { Text = "Draft", Value = "Draft" }
                                            };

                ViewBag.ShowProjectAutoComplete = 1;
                ViewBag.ShowSeamApprovalStatus = 1;
                ViewBag.SeamApprovalStatus = lstSeamApprovalStatus;

            }
            else if (reporttype == clsImplementationEnum.IPIReports.SeamwiseGroundSpotGPAReportForOverlay.GetStringValue())
            {
                var objATH001 = db.ATH001.Where(i => i.Employee == objClsLoginInfo.UserName).Select(i => new { BU = i.BU, Location = i.Location, Role = i.Role }).ToList();
                var objLocation = objATH001.Select(i => i.Location).Distinct().ToList();
                var listLocation = (from c1 in db.COM002
                                    where c1.t_dtyp == 1 && objLocation.Contains(c1.t_dimx)
                                    select new { id = c1.t_dimx, text = c1.t_desc, CatID = c1.t_dimx, CatDesc = c1.t_desc }
                                  ).Distinct().ToList();
                ViewBag.Location = listLocation;
                ViewBag.ShowRPTFromToDate = 1;
                ViewBag.ShowRPTFromToQualityProject = 1;
                ViewBag.ShowRPTFromToShop = 1;
                ViewBag.ShowFromToSeamNobyProject = 1;
                ViewBag.ShowRPTFromToStages = 1;
                ViewBag.ShowRPTLocation = 1;
            }
            else if (reporttype == clsImplementationEnum.IPIReports.InspectionEntryAttendance.GetStringValue())
            {
                var objATH001 = db.ATH001.Where(i => i.Employee == objClsLoginInfo.UserName).Select(i => new { BU = i.BU, Location = i.Location, Role = i.Role }).ToList();
                var objLocation = objATH001.Select(i => i.Location).Distinct().ToList();
                var listLocation = (from c1 in db.COM002
                                    where c1.t_dtyp == 1 && objLocation.Contains(c1.t_dimx)
                                    select new { id = c1.t_dimx, text = c1.t_desc, CatID = c1.t_dimx, CatDesc = c1.t_desc }
                              ).Distinct().ToList();

                ViewBag.Location = listLocation;
                ViewBag.ShowLocationAutocomplete = 1;
                ViewBag.ShowFromToDate = 1;
                ViewBag.ShowFromToQualityProject = 1;
                ViewBag.ShowFromToShop = 1;
                ViewBag.ShowFromToSeamStage = 1;
                ViewBag.ShowSeamApprovalStatus = 1;
                ViewBag.ExcludeNuclearProject = 1;

                var lstSeamApprovalStatus = new List<SelectListItem>()
                                            { new SelectListItem { Text = "Attended", Value = "" },
                                              new SelectListItem { Text = "Cleared", Value = "Cleared" },
                                              new SelectListItem { Text = "Rejected", Value = "Rejected" }
                                            };

                ViewBag.SeamApprovalStatus = lstSeamApprovalStatus;

            }
            else if (reporttype == clsImplementationEnum.IPIReports.ChemicalTest.GetStringValue())
            {
                var objATH001 = db.ATH001.Where(i => i.Employee == objClsLoginInfo.UserName).Select(i => new { BU = i.BU, Location = i.Location, Role = i.Role }).ToList();
                var objLocation = objATH001.Select(i => i.Location).Distinct().ToList();
                var listLocation = (from c1 in db.COM002
                                    where c1.t_dtyp == 1 && objLocation.Contains(c1.t_dimx)
                                    select new { id = c1.t_dimx, text = c1.t_desc, CatID = c1.t_dimx, CatDesc = c1.t_desc }
                              ).Distinct().ToList();

                ViewBag.Location = listLocation;
                ViewBag.ShowRPTFromToDate = 1;
                ViewBag.ShowFromToQualityProject = 1;
                ViewBag.ShowFromToSeamNobyQualityProject = 1;
                ViewBag.ShowFromToAllStages = 1;
                ViewBag.ShowLocationAutocomplete = 1;
                ViewBag.ShowRPTSeqn = 1; ViewBag.ShowRPTFromToStageseqinput = 1;
                ViewBag.ShowSeamApprovalStatus = 1;
                ViewBag.WeldingProcess = 1;
                ViewBag.Instrument = 1; ViewBag.ShowTPIinReport = 1;
                ViewBag.ReportStageType = clsImplementationEnum.SeamStageType.Chemical.GetStringValue();
                var lstSeamApprovalStatus = new List<SelectListItem>()
                                            {
                                              new SelectListItem { Text = "Select", Value = "" },
                                              new SelectListItem { Text = "Cleared", Value = "Cleared" },
                                              new SelectListItem { Text = "Returned", Value = "Returned" },
                                              new SelectListItem { Text = "Rejected", Value = "Rejected" }
                                            };

                ViewBag.SeamApprovalStatus = lstSeamApprovalStatus;

                List<CategoryData> lstWeldingProcess = Manager.GetSubCatagorywithoutBULocation("Welding Process").ToList();
                ViewBag.WeldingProcessList = lstWeldingProcess.AsEnumerable().Select(x => new { CatID = x.Value, CatDesc = x.CategoryDescription, id = x.Value, text = x.CategoryDescription }).OrderBy(x => x.CatID).ToList();

                List<CategoryData> lstInstrument = Manager.GetSubCatagorywithoutBULocation("Instrument Group").ToList();
                ViewBag.InstrumentList = lstInstrument.AsEnumerable().Select(x => new { CatID = x.Value, CatDesc = x.CategoryDescription, id = x.Value, text = x.CategoryDescription }).OrderBy(x => x.CatID).ToList();

                ViewBag.ShowInspectionRemark = 1;
            }
            else if (reporttype == clsImplementationEnum.IPIReports.FerriteTest.GetStringValue())
            {
                var objATH001 = db.ATH001.Where(i => i.Employee == objClsLoginInfo.UserName).Select(i => new { BU = i.BU, Location = i.Location, Role = i.Role }).ToList();
                var objLocation = objATH001.Select(i => i.Location).Distinct().ToList();
                var listLocation = (from c1 in db.COM002
                                    where c1.t_dtyp == 1 && objLocation.Contains(c1.t_dimx)
                                    select new { id = c1.t_dimx, text = c1.t_desc, CatID = c1.t_dimx, CatDesc = c1.t_desc }
                              ).Distinct().ToList();

                ViewBag.Location = listLocation;
                ViewBag.ShowRPTFromToDate = 1;
                ViewBag.ShowFromToQualityProject = 1;
                ViewBag.ShowFromToSeamNobyQualityProject = 1;
                ViewBag.ShowFromToAllStages = 1;
                ViewBag.ShowLocationAutocomplete = 1;
                ViewBag.ShowRPTSeqn = 1; ViewBag.ShowRPTFromToStageseqinput = 1;
                ViewBag.ShowSeamApprovalStatus = 1;
                ViewBag.WeldingProcess = 1;
                ViewBag.Instrument = 1; ViewBag.ShowTPIinReport = 1;
                ViewBag.ReportStageType = clsImplementationEnum.SeamStageType.Ferrite.GetStringValue();
                var lstSeamApprovalStatus = new List<SelectListItem>()
                                            {   new SelectListItem { Text = "Select", Value = "" }, new SelectListItem { Text = "Cleared", Value = "Cleared" },
                                              new SelectListItem { Text = "Returned", Value = "Returned" },
                                              new SelectListItem { Text = "Rejected", Value = "Rejected" }
                                            };

                ViewBag.SeamApprovalStatus = lstSeamApprovalStatus;

                List<CategoryData> lstWeldingProcess = Manager.GetSubCatagorywithoutBULocation("Welding Process").ToList();
                ViewBag.WeldingProcessList = lstWeldingProcess.AsEnumerable().Select(x => new { CatID = x.Value, CatDesc = x.CategoryDescription, id = x.Value, text = x.CategoryDescription }).OrderBy(x => x.CatID).ToList();

                List<CategoryData> lstInstrument = Manager.GetSubCatagorywithoutBULocation("Instrument Group").ToList();
                ViewBag.InstrumentList = lstInstrument.AsEnumerable().Select(x => new { CatID = x.Value, CatDesc = x.CategoryDescription, id = x.Value, text = x.CategoryDescription }).OrderBy(x => x.CatID).ToList();

                ViewBag.ShowInspectionRemark = 1;
            }
            else if (reporttype == clsImplementationEnum.IPIReports.HardnessTest.GetStringValue())
            {
                var objATH001 = db.ATH001.Where(i => i.Employee == objClsLoginInfo.UserName).Select(i => new { BU = i.BU, Location = i.Location, Role = i.Role }).ToList();
                var objLocation = objATH001.Select(i => i.Location).Distinct().ToList();
                var listLocation = (from c1 in db.COM002
                                    where c1.t_dtyp == 1 && objLocation.Contains(c1.t_dimx)
                                    select new { id = c1.t_dimx, text = c1.t_desc, CatID = c1.t_dimx, CatDesc = c1.t_desc }
                              ).Distinct().ToList();

                ViewBag.Location = listLocation;
                ViewBag.ShowRPTFromToDate = 1;
                ViewBag.ShowFromToQualityProject = 1;
                ViewBag.ShowFromToSeamNobyQualityProject = 1;
                ViewBag.ShowFromToAllStages = 1;
                ViewBag.ShowLocationAutocomplete = 1;
                ViewBag.ShowRPTSeqn = 1; ViewBag.ShowRPTFromToStageseqinput = 1;
                ViewBag.ShowSeamApprovalStatus = 1;
                ViewBag.Instrument = 1;
                ViewBag.ShowTPIinReport = 1;
                ViewBag.ReportStageType = clsImplementationEnum.SeamStageType.Hardness.GetStringValue();
                var lstSeamApprovalStatus = new List<SelectListItem>()
                                            {  new SelectListItem { Text = "Select", Value = "" },  new SelectListItem { Text = "Cleared", Value = "Cleared" },
                                              new SelectListItem { Text = "Returned", Value = "Returned" },
                                              new SelectListItem { Text = "Rejected", Value = "Rejected" }
                                            };

                ViewBag.SeamApprovalStatus = lstSeamApprovalStatus;

                List<CategoryData> lstWeldingProcess = Manager.GetSubCatagorywithoutBULocation("Welding Process").ToList();
                ViewBag.WeldingProcessList = lstWeldingProcess.AsEnumerable().Select(x => new { CatID = x.Value, CatDesc = x.CategoryDescription, id = x.Value, text = x.CategoryDescription }).OrderBy(x => x.CatID).ToList();

                List<CategoryData> lstInstrument = Manager.GetSubCatagorywithoutBULocation("Instrument Group").ToList();
                ViewBag.InstrumentList = lstInstrument.AsEnumerable().Select(x => new { CatID = x.Value, CatDesc = x.CategoryDescription, id = x.Value, text = x.CategoryDescription }).OrderBy(x => x.CatID).ToList();

                ViewBag.ShowInspectionRemark = 1;
            }
            else if (reporttype == clsImplementationEnum.IPIReports.PMITest.GetStringValue())
            {
                var objATH001 = db.ATH001.Where(i => i.Employee == objClsLoginInfo.UserName).Select(i => new { BU = i.BU, Location = i.Location, Role = i.Role }).ToList();
                var objLocation = objATH001.Select(i => i.Location).Distinct().ToList();
                var listLocation = (from c1 in db.COM002
                                    where c1.t_dtyp == 1 && objLocation.Contains(c1.t_dimx)
                                    select new { id = c1.t_dimx, text = c1.t_desc, CatID = c1.t_dimx, CatDesc = c1.t_desc }
                              ).Distinct().ToList();

                ViewBag.Location = listLocation;
                ViewBag.ShowRPTFromToDate = 1;
                ViewBag.ShowFromToQualityProject = 1;
                ViewBag.ShowFromToSeamNobyQualityProject = 1;
                ViewBag.ShowFromToAllStages = 1;
                ViewBag.ShowLocationAutocomplete = 1;
                ViewBag.ShowRPTSeqn = 1; ViewBag.ShowRPTFromToStageseqinput = 1;
                ViewBag.ShowSeamApprovalStatus = 1;
                ViewBag.Instrument = 1; ViewBag.ShowTPIinReport = 1;
                ViewBag.ReportStageType = clsImplementationEnum.SeamStageType.PMI.GetStringValue();
                var lstSeamApprovalStatus = new List<SelectListItem>()
                                            {  new SelectListItem { Text = "Select", Value = "" }, new SelectListItem { Text = "Cleared", Value = "Cleared" },
                                              new SelectListItem { Text = "Returned", Value = "Returned" },
                                              new SelectListItem { Text = "Rejected", Value = "Rejected" }
                                            };

                ViewBag.SeamApprovalStatus = lstSeamApprovalStatus;

                List<CategoryData> lstWeldingProcess = Manager.GetSubCatagorywithoutBULocation("Welding Process").ToList();
                ViewBag.WeldingProcessList = lstWeldingProcess.AsEnumerable().Select(x => new { CatID = x.Value, CatDesc = x.CategoryDescription, id = x.Value, text = x.CategoryDescription }).OrderBy(x => x.CatID).ToList();

                List<CategoryData> lstInstrument = Manager.GetSubCatagorywithoutBULocation("Instrument Group").ToList();
                ViewBag.InstrumentList = lstInstrument.AsEnumerable().Select(x => new { CatID = x.Value, CatDesc = x.CategoryDescription, id = x.Value, text = x.CategoryDescription }).OrderBy(x => x.CatID).ToList();

                ViewBag.ShowInspectionRemark = 1;
            }
            else if (reporttype == clsImplementationEnum.IPIReports.RepairSummary.GetStringValue())
            {
                var objATH001 = db.ATH001.Where(i => i.Employee == objClsLoginInfo.UserName).Select(i => new { BU = i.BU, Location = i.Location, Role = i.Role }).ToList();
                var objLocation = objATH001.Select(i => i.Location).Distinct().ToList();
                var listLocation = (from c1 in db.COM002
                                    where c1.t_dtyp == 1 && objLocation.Contains(c1.t_dimx)
                                    select new { id = c1.t_dimx, text = c1.t_desc, CatID = c1.t_dimx, CatDesc = c1.t_desc }
                              ).Distinct().ToList();
                ViewBag.ShowFromToShop = 1;
                ViewBag.Location = listLocation;
                ViewBag.ShowRPTFromToDate = 1;
                ViewBag.ShowFromToQualityProject = 1;
                ViewBag.ShowFromToSeamNobyQualityProject = 1;
                ViewBag.ShowFromToAllStages = 1;
                ViewBag.FetchfromQMS012 = 1;
                ViewBag.FetchNDEStages = 1;
                ViewBag.FetchNDEStagesexceptPTMT = 1;
                ViewBag.ShowLocationAutocomplete = 1;
                ViewBag.ReportStageType = "RT,UT";
            }
            else if (reporttype == clsImplementationEnum.IPIReports.AI_horizontal.GetStringValue())
            {
                var objATH001 = db.ATH001.Where(i => i.Employee == objClsLoginInfo.UserName).Select(i => new { BU = i.BU, Location = i.Location, Role = i.Role }).ToList();
                var objLocation = objATH001.Select(i => i.Location).Distinct().ToList();
                var listLocation = (from c1 in db.COM002
                                    where c1.t_dtyp == 1 && objLocation.Contains(c1.t_dimx)
                                    select new { id = c1.t_dimx, text = c1.t_desc, CatID = c1.t_dimx, CatDesc = c1.t_desc }
                              ).Distinct().ToList();
                ViewBag.Location = listLocation;

                ViewBag.UserInputPreparedBy = 1;
                ViewBag.UserInputApprovedBy = 1;
                ViewBag.UserInputLatestDrawingDINNo = 1;
                ViewBag.ShowUserInputDate = 1;

                ViewBag.FetchfromQMS012 = 1;
                ViewBag.ShowLocation = 1;
                ViewBag.ShowProject = 1;
                ViewBag.ShowSeamTypeMultiple = 1;
                ViewBag.ShowSeamNo = 1;
                // ViewBag.ShowFromToStagesByMultiselectSeamno = 1;
                ViewBag.ShowStageMultiple = 1;

            }
            else if (reporttype == clsImplementationEnum.IPIReports.ICL_DPP.GetStringValue())
            {
                ViewBag.ShowLocation = 1;
                ViewBag.ShowBU = 1;
                ViewBag.ShowFromToQualityProjectByBULocation = 1;
                ViewBag.TestplanRev = 1;
                ViewBag.ShowRPTFromToDate = 1;
                ViewBag.ShowSeamApprovalStatus = 1;
                var lstSeamApprovalStatus = new List<SelectListItem>()
                                            {
                                              new SelectListItem { Text = "Approved", Value = "Approved" },
                                              new SelectListItem { Text = "Pending", Value = "Pending" }
                                            };
                ViewBag.SeamApprovalStatus = lstSeamApprovalStatus;
            }
            else if (reporttype == clsImplementationEnum.IPIReports.Seam_StageClearanceAnalysis_Report.GetStringValue())
            {
                var objATH001 = db.ATH001.Where(i => i.Employee == objClsLoginInfo.UserName).Select(i => new { BU = i.BU, Location = i.Location, Role = i.Role }).ToList();
                var objLocation = objATH001.Select(i => i.Location).Distinct().ToList();
                var listLocation = (from c1 in db.COM002
                                    where c1.t_dtyp == 1 && objLocation.Contains(c1.t_dimx)
                                    select new { id = c1.t_dimx, text = c1.t_desc, CatID = c1.t_dimx, CatDesc = c1.t_desc }
                              ).Distinct().ToList();
                ViewBag.Location = listLocation;

                ViewBag.ShowRPTFromToDate = 1;
                ViewBag.FetchfromQMS012 = 1;
                ViewBag.ShowLocation = 1;
                ViewBag.ShowProject = 1;
                ViewBag.ShowSeamNo = 1;
                ViewBag.ShowShopMultiple = 1;
                ViewBag.ShowStageMultiple = 1;

                ViewBag.StageMultipleList = db.QMS050.Where(i => i.StageCode != null && i.StageCode != "").Select(i => new { StageCode = i.StageCode, id = i.StageCode, text = i.StageCode }).Distinct().ToList();

            }
            else if (reporttype == clsImplementationEnum.IPIReports.InspectionRejectionAnalysis.GetStringValue())
            {
                var objATH001 = db.ATH001.Where(i => i.Employee == objClsLoginInfo.UserName).Select(i => new { BU = i.BU, Location = i.Location, Role = i.Role }).ToList();
                var objLocation = objATH001.Select(i => i.Location).Distinct().ToList();
                var listLocation = (from c1 in db.COM002
                                    where c1.t_dtyp == 1 && objLocation.Contains(c1.t_dimx)
                                    select new { id = c1.t_dimx, text = c1.t_desc, CatID = c1.t_dimx, CatDesc = c1.t_desc }
                                  ).Distinct().ToList();
                ViewBag.Location = listLocation;
                //Offerred Date
                ViewBag.ShowFromToDate = 1;
                //Inspection Date 
                ViewBag.ShowRPTFromToDate = 1;
                ViewBag.ShowFromToQualityProjectBuwise = 1;
                ViewBag.ShowFromToShop = 1;
                ViewBag.ShowFromToAllStages = 1;
                ViewBag.ShowLocation = 1;
            }
            else if (reporttype == clsImplementationEnum.IPIReports.ProjectSeamStatus.GetStringValue())
            {
                var lstSeamStageType = new List<SelectListItem>()
                                            { new SelectListItem { Text = "Select", Value = "" },
                                              new SelectListItem { Text = "All", Value = "All" },
                                              new SelectListItem { Text = "After PWHT", Value = "After PWHT" },
                                              new SelectListItem { Text = "Before PWHT", Value = "Before PWHT" },
                                              new SelectListItem { Text = "Is Hydro", Value = "Is Hydro" }
                                           };

                ViewBag.ShowProjectAutoComplete = 1;
                ViewBag.SeamStageType = 1;
                ViewBag.SeamStageTypes = lstSeamStageType;
            }
            else if (reporttype == clsImplementationEnum.IPIReports.WebBasedProtocolStatus.GetStringValue())
            {
                ViewBag.Web = 1;
                ViewBag.ShowSeamTypeMultiple1 = 1;
                ViewBag.ShowSeamNo1 = 1;
                ViewBag.ShowStageMultiple1 = 1;

            }
            ViewBag.StageType = lstStageType;
            return PartialView("_ReportInputFilterHtmlPartial");
        }

        public JsonResult GetUserLocation()
        {
            try
            {
                var objATH001 = db.ATH001.Where(i => i.Employee == objClsLoginInfo.UserName).Select(i => new { BU = i.BU, Location = i.Location, Role = i.Role }).ToList();
                var objLocation = objATH001.Select(i => i.Location).Distinct().ToList();

                var listLocation = (from c1 in db.COM002
                                    where c1.t_dtyp == 1 && objLocation.Contains(c1.t_dimx)
                                    select new { id = c1.t_dimx, text = c1.t_desc, CatID = c1.t_dimx, CatDesc = c1.t_desc }
                                  ).Distinct().ToList();
                return Json(listLocation, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetUserBU()
        {
            try
            {
                var objATH001 = db.ATH001.Where(i => i.Employee == objClsLoginInfo.UserName).Select(i => new { BU = i.BU, Location = i.Location, Role = i.Role }).ToList();
                var objBU = objATH001.Select(i => i.BU).Distinct().ToList();

                var listBU = (from c1 in db.COM002
                              where c1.t_dtyp == 2 && objBU.Contains(c1.t_dimx)
                              select new { id = c1.t_dimx, text = c1.t_desc, CatID = c1.t_dimx, CatDesc = c1.t_desc }
                                  ).Distinct().ToList();
                return Json(listBU, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetQualityProjects(string search)
        {
            string username = objClsLoginInfo.UserName;
            var lstATHLocation = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.Location).Distinct().ToList();
            var lstCOMLocation = db.COM003.Where(i => i.t_psno.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_loca).Distinct().ToList();

            var lstCOMBU = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.BU).Distinct().ToList();

            lstCOMLocation = lstATHLocation.Union(lstCOMLocation).ToList();

            List<ddlValue> lstQualityProject = new List<ddlValue>();
            if (!string.IsNullOrEmpty(search))
            {
                lstQualityProject = db.QMS010.Where(i => i.QualityProject.ToLower().Contains(search.ToLower())
                                                                 && lstCOMLocation.Contains(i.Location)
                                                                 && lstCOMBU.Contains(i.BU)
                                                               ).Select(i => new ddlValue { id = i.QualityProject, text = i.QualityProject }).Distinct().ToList();//&& i.CreatedBy.Equals(username,StringComparison.OrdinalIgnoreCase)
            }
            else
            {
                lstQualityProject = db.QMS010.Where(i => lstCOMLocation.Contains(i.Location)
                                                        && lstCOMBU.Contains(i.BU)
                                                        ).Select(i => new ddlValue { id = i.QualityProject, text = i.QualityProject }).Take(10).Distinct().ToList();
            }

            return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetQualityProjectsV2(string term, string location = "", bool isNuclearProjectIncluded = true)
        {
            string username = objClsLoginInfo.UserName;
            var lstATHLocation = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.Location).Distinct().ToList();
            var lstCOMLocation = db.COM003.Where(i => i.t_psno.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_loca).Distinct().ToList();

            var lstCOMBU = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.BU).Distinct().ToList();

            lstCOMLocation = lstATHLocation.Union(lstCOMLocation).ToList();

            List<ddlValue> lstQualityProject = new List<ddlValue>();
            if (!string.IsNullOrEmpty(term))
            {
                lstQualityProject = db.QMS010.Where(i => i.QualityProject.Contains(term)
                                                                 && (location == "" ? lstCOMLocation.Contains(i.Location) : i.Location == location)
                                                                 && (lstCOMBU.Contains(i.BU))
                                                                 && (isNuclearProjectIncluded == false ? (i.BU != "02") : (i.BU == i.BU))
                                                               ).Select(i => new ddlValue { Value = i.QualityProject, Text = i.QualityProject }).Distinct().ToList();//&& i.CreatedBy.Equals(username,StringComparison.OrdinalIgnoreCase)
            }
            else
            {
                lstQualityProject = db.QMS010.Where(i => (location == "" ? lstCOMLocation.Contains(i.Location) : i.Location == location)
                                                        && (lstCOMBU.Contains(i.BU))
                                                        && (isNuclearProjectIncluded == false ? (i.BU != "02") : (i.BU == i.BU))
                                                        ).Select(i => new ddlValue { Value = i.QualityProject, Text = i.QualityProject }).Take(10).Distinct().ToList();
            }

            return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetShopV2(string term)
        {
            string username = objClsLoginInfo.UserName;
            var lstATHLocation = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.Location).Distinct().ToList();
            var lstCOMLocation = db.COM003.Where(i => i.t_psno.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_loca).Distinct().ToList();

            var lstCOMBU = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.BU).Distinct().ToList();

            lstCOMLocation = lstATHLocation.Union(lstCOMLocation).ToList();

            List<ddlValue> lstShop = new List<ddlValue>();
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstShop = (from glb002 in db.GLB002
                           join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                           where glb001.Category.Equals("Shop", StringComparison.OrdinalIgnoreCase) && lstCOMLocation.Contains(glb002.Location) && lstCOMBU.Contains(glb002.BU)
                                 && (glb002.Description.Contains(term) || glb002.Code.Contains(term))
                           select new ddlValue { Text = glb002.Description, Value = glb002.Code }).Distinct().OrderBy(x => x.Value).ToList();
            }
            else
            {
                lstShop = (from glb002 in db.GLB002
                           join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                           where glb001.Category.Equals("Shop", StringComparison.OrdinalIgnoreCase) && lstCOMLocation.Contains(glb002.Location) && lstCOMBU.Contains(glb002.BU)
                           select new ddlValue { Text = glb002.Description, Value = glb002.Code }).Distinct().OrderBy(x => x.Value).Take(10).ToList();
            }
            return Json(lstShop, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetShopMultiple()
        {
            string username = objClsLoginInfo.UserName;
            var lstATHLocation = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.Location).Distinct().ToList();
            var lstCOMLocation = db.COM003.Where(i => i.t_psno.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_loca).Distinct().ToList();

            var lstCOMBU = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.BU).Distinct().ToList();

            lstCOMLocation = lstATHLocation.Union(lstCOMLocation).ToList();

            var lstShop = (from glb002 in db.GLB002
                           join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                           where glb001.Category.Equals("Shop", StringComparison.OrdinalIgnoreCase) && lstCOMLocation.Contains(glb002.Location) && lstCOMBU.Contains(glb002.BU)
                           select new { id = glb002.Code, text = glb002.Description, CatID = glb002.Code, CatDesc = glb002.Description }).Distinct().OrderBy(x => x.id).ToList();

            return Json(lstShop, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSeamStageV2(string term)
        {
            string username = objClsLoginInfo.UserName;
            var lstATHLocation = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.Location).Distinct().ToList();
            var lstCOMLocation = db.COM003.Where(i => i.t_psno.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_loca).Distinct().ToList();

            var lstCOMBU = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.BU).Distinct().ToList();

            lstCOMLocation = lstATHLocation.Union(lstCOMLocation).ToList();

            List<ddlValue> lstStagecode = new List<ddlValue>();
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstStagecode = (from a in db.QMS002
                                where lstCOMLocation.Contains(a.Location) && lstCOMBU.Contains(a.BU) && (a.StageDesc.Contains(term) || a.StageCode.Contains(term))
                                select new ddlValue { Text = a.StageCode, Value = a.StageCode }).Distinct().ToList();
            }
            else
            {
                lstStagecode = (from a in db.QMS002
                                where lstCOMLocation.Contains(a.Location) && lstCOMBU.Contains(a.BU)
                                select new ddlValue { Text = a.StageCode, Value = a.StageCode }).Distinct().ToList();
            }
            return Json(lstStagecode, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetProjects(string term, string location)
        {
            if (string.IsNullOrWhiteSpace(location))
            {
                location = objClsLoginInfo.Location;
            }
            List<ddlValue> lstProject = new List<ddlValue>();
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstProject = (from q50 in db.QMS050
                              join com001 in db.COM001 on q50.Project equals com001.t_cprj
                              where q50.Location.Trim() == location.Trim() && (com001.t_dsca.Contains(term) || q50.Project.Contains(term))
                              select new ddlValue { Text = q50.Project + "-" + com001.t_dsca, Value = q50.Project }).Distinct().Take(10).ToList();
            }
            else
            {
                lstProject = (from q50 in db.QMS050
                              join com001 in db.COM001 on q50.Project equals com001.t_cprj
                              where q50.Location.Trim() == location.Trim()
                              select new ddlValue { Text = q50.Project + "-" + com001.t_dsca, Value = q50.Project }).Distinct().Take(10).ToList();
            }
            return Json(lstProject, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetShop(string term, string location)
        {
            if (string.IsNullOrWhiteSpace(location))
            {
                location = objClsLoginInfo.Location;
            }
            List<ddlValue> lstShop = new List<ddlValue>();
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstShop = (from glb002 in db.GLB002
                           join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                           where glb001.Category.Equals("Shop", StringComparison.OrdinalIgnoreCase) && glb002.Location == location
                                 && (glb002.Description.Contains(term) || glb002.Code.Contains(term))
                           select new ddlValue { Text = glb002.Description, Value = glb002.Code }).Distinct().Take(10).ToList();
            }
            else
            {
                lstShop = (from glb002 in db.GLB002
                           join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                           where glb001.Category.Equals("Shop", StringComparison.OrdinalIgnoreCase) && glb002.Location == location
                           select new ddlValue { Text = glb002.Description, Value = glb002.Code }).Distinct().Take(10).ToList();
            }
            return Json(lstShop, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetStagecode(string term, string location, string stagetype)
        {
            List<ddlValue> lstStagecode = new List<ddlValue>();
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstStagecode = (from a in db.QMS002
                                where a.Location == location && a.StageType == stagetype && (a.StageDesc.Contains(term) || a.StageCode.Contains(term))
                                select new ddlValue { Text = a.StageCode + "-" + a.StageDesc, Value = a.StageCode }).Distinct().ToList();
            }
            else
            {
                lstStagecode = (from a in db.QMS002
                                where a.Location == location && a.StageType == stagetype
                                select new ddlValue { Text = a.StageCode + "-" + a.StageDesc, Value = a.StageCode }).Distinct().ToList();
            }
            return Json(lstStagecode, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSeams(string term, string location, string fromProject, string toProject, bool IsQualityProject = false)
        {
            if (string.IsNullOrWhiteSpace(location))
            {
                location = objClsLoginInfo.Location;
            }
            List<ddlValue> lstSeamNo = new List<ddlValue>();

            //List<QMS050> lstAllQualityProject = (from a in db.QMS050
            //                                     select a).OrderBy(x => x.Project.Trim()).Distinct().ToList();

            List<string> newProjectList = new List<string>();
            bool start = false;
            if (IsQualityProject)
            {
                if (!string.IsNullOrWhiteSpace(term))
                {

                    lstSeamNo = (from a in db.QMS050
                                 where a.SeamNo.Contains(term) && a.QualityProject.CompareTo(fromProject) >= 0 && a.QualityProject.CompareTo(toProject) <= 0
                                 select new ddlValue { Text = a.SeamNo, Value = a.SeamNo }).Distinct().Take(10).OrderBy(x => x.Value).ToList();
                }
                else
                {
                    lstSeamNo = (from a in db.QMS050
                                 where a.QualityProject.CompareTo(fromProject) >= 0 && a.QualityProject.CompareTo(toProject) <= 0
                                 select new ddlValue { Text = a.SeamNo, Value = a.SeamNo }).Distinct().Take(10).OrderBy(x => x.Value).ToList();
                }
            }
            else
            {
                //foreach (QMS050 proj in lstAllQualityProject)
                //{
                //    if (proj.Project == fromProject)
                //    {
                //        start = true;
                //    }

                //    if (start)
                //        newProjectList.Add(proj.Project);

                //    if (proj.Project == toProject)
                //    {
                //        break;
                //    }
                //}

                //newProjectList = newProjectList.Distinct().ToList();

                if (!string.IsNullOrWhiteSpace(term))
                {
                    lstSeamNo = (from a in db.QMS050
                                 where a.SeamNo.Contains(term) && a.QualityProject.CompareTo(fromProject) >= 0 && a.QualityProject.CompareTo(toProject) <= 0 //newProjectList.Contains(a.QualityProject)
                                 select new ddlValue { Text = a.SeamNo, Value = a.SeamNo }).Distinct().Take(10).OrderBy(x => x.Value).ToList();

                    //lstSeamNo = (from a in db.QMS050
                    //             where a.SeamNo.Contains(term) && newProjectList.Contains(a.Project)
                    //             select new ddlValue { Text = a.SeamNo, Value = a.SeamNo }).Distinct().Take(10).ToList();
                }
                else
                {
                    lstSeamNo = (from a in db.QMS050
                                 where a.QualityProject.CompareTo(fromProject) >= 0 && a.QualityProject.CompareTo(toProject) <= 0 //newProjectList.Contains(a.QualityProject)
                                 select new ddlValue { Text = a.SeamNo, Value = a.SeamNo }).Distinct().Take(10).OrderBy(x => x.Value).ToList();

                    //lstSeamNo = (from a in db.QMS050
                    //             where newProjectList.Contains(a.Project)
                    //             select new ddlValue { Text = a.SeamNo, Value = a.SeamNo }).Distinct().Take(10).ToList();
                }
            }
            return Json(lstSeamNo, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSeamsbyQualityproject(string term, string location, string fromProject, string toProject, bool FetchfromQMS012 = false)
        {
            if (string.IsNullOrWhiteSpace(location))
            {
                location = objClsLoginInfo.Location;
            }
            List<ddlValue> lstSeamNo = new List<ddlValue>();
            if (FetchfromQMS012)
            {
                //List<QMS012> lstAllQualityProject = (from a in db.QMS012
                //                                     select a).OrderBy(x => x.QualityProject.Trim()).Distinct().OrderBy(x => x.QualityProject).ToList();

                //List<string> newProjectList = new List<string>();
                //bool start = false;

                //foreach (QMS012 proj in lstAllQualityProject)
                //{
                //    if (proj.QualityProject == fromProject)
                //    {
                //        start = true;
                //    }

                //    if (start)
                //        newProjectList.Add(proj.QualityProject);

                //    if (proj.QualityProject == toProject)
                //    {
                //        break;
                //    }
                //}
                //newProjectList = newProjectList.Distinct().ToList();

                if (!string.IsNullOrWhiteSpace(term))
                {
                    lstSeamNo = (from a in db.QMS012
                                 where a.SeamNo.Contains(term) && a.QualityProject.CompareTo(fromProject) >= 0 && a.QualityProject.CompareTo(toProject) <= 0
                                 select new ddlValue { Text = a.SeamNo, Value = a.SeamNo }).Distinct().Take(10).OrderBy(x => x.Value).ToList();
                }
                else
                {
                    lstSeamNo = (from a in db.QMS012
                                 where a.QualityProject.CompareTo(fromProject) >= 0 && a.QualityProject.CompareTo(toProject) <= 0
                                 select new ddlValue { Text = a.SeamNo, Value = a.SeamNo }).Distinct().Take(10).OrderBy(x => x.Value).ToList();
                }
            }
            else
            {
                //List<string> lstAllQualityProject = (from a in db.QMS050
                //                                     select a).OrderBy(x => x.QualityProject.Trim()).Distinct().OrderBy(x => x.QualityProject).Select(s=>s.QualityProject).ToList();

                //List<string> newProjectList = new List<string>();
                //bool start = false;

                //foreach (string proj in lstAllQualityProject)
                //{
                //    if (proj == fromProject)
                //    {
                //        start = true;
                //    }

                //    if (start)
                //        newProjectList.Add(proj);

                //    if (proj == toProject)
                //    {
                //        break;
                //    }
                //}
                //newProjectList = newProjectList.Distinct().ToList();

                if (!string.IsNullOrWhiteSpace(term))
                {
                    lstSeamNo = (from a in db.QMS050
                                 where a.SeamNo.Contains(term) && a.QualityProject.CompareTo(fromProject) >= 0 && a.QualityProject.CompareTo(toProject) <= 0    //newProjectList.Contains(a.QualityProject)
                                 select new ddlValue { Text = a.SeamNo, Value = a.SeamNo }).Distinct().Take(10).OrderBy(x => x.Value).ToList();
                }
                else
                {
                    lstSeamNo = (from a in db.QMS050
                                 where a.QualityProject.CompareTo(fromProject) >= 0 && a.QualityProject.CompareTo(toProject) <= 0 //newProjectList.Contains(a.QualityProject)
                                 select new ddlValue { Text = a.SeamNo, Value = a.SeamNo }).Distinct().Take(10).OrderBy(x => x.Value).ToList();
                }
            }
            return Json(lstSeamNo, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetAllSeamsbyQualityproject(string term, string location, string selectedProject, bool FetchfromQMS012 = false)
        {
            List<string> lstLocation = new List<string>();
            if (string.IsNullOrWhiteSpace(location))
            {
                lstLocation.Add(objClsLoginInfo.Location.ToString());
            }
            else
            {
                lstLocation = location.Split(',').Distinct().ToList();
            }
            List<ddlValue> lstSeamNo = new List<ddlValue>();
            List<string> newProjectList = new List<string>();
            if (!string.IsNullOrWhiteSpace(selectedProject))
            {
                newProjectList = selectedProject.Split(',').Distinct().ToList();
            }
            if (FetchfromQMS012)
            {
                if (!string.IsNullOrWhiteSpace(term))
                {
                    lstSeamNo = (from a in db.QMS012
                                 where a.SeamNo.Contains(term) && newProjectList.Contains(a.QualityProject) && lstLocation.Contains(a.Location)
                                 select new ddlValue { Text = a.SeamNo, Value = a.SeamNo }).Distinct().Take(10).OrderBy(x => x.Value).ToList();
                }
                else
                {
                    lstSeamNo = (from a in db.QMS012
                                 where newProjectList.Contains(a.QualityProject) && lstLocation.Contains(a.Location)
                                 select new ddlValue { Text = a.SeamNo, Value = a.SeamNo }).Distinct().Take(10).OrderBy(x => x.Value).ToList();
                }
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(term))
                {
                    lstSeamNo = (from a in db.QMS050
                                 where a.SeamNo.Contains(term) && newProjectList.Contains(a.QualityProject) && lstLocation.Contains(a.Location)
                                 select new ddlValue { Text = a.SeamNo, Value = a.SeamNo }).Distinct().Take(10).OrderBy(x => x.Value).ToList();
                }
                else
                {
                    lstSeamNo = (from a in db.QMS050
                                 where newProjectList.Contains(a.QualityProject) && lstLocation.Contains(a.Location)
                                 select new ddlValue { Text = a.SeamNo, Value = a.SeamNo }).Distinct().Take(10).OrderBy(x => x.Value).ToList();
                }
            }
            return Json(lstSeamNo, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetAllStagesbyStageType(string term, string location, string stagetype = "", bool FetchNDEStages = false, bool FetchNDEStagesexceptPTMT = false)
        {
            List<string> lstLocation = new List<string>();
            List<string> lstStage = new List<string>();

            if (string.IsNullOrWhiteSpace(location))
            {
                lstLocation.Add(objClsLoginInfo.Location.ToString());
            }
            else
            {
                lstLocation = location.Split(',').Distinct().ToList();
            }

            if (string.IsNullOrWhiteSpace(stagetype))
            {
                lstStage.Add(stagetype);
            }
            else
            {
                lstStage = stagetype.Split(',').Distinct().ToList();
            }

            List<ddlValue> lstSeamNo = new List<ddlValue>();
            List<ddlValue> lstStageCode = new List<ddlValue>();

            if (!string.IsNullOrWhiteSpace(term))
            {
                lstStageCode = (from a in db.QMS002
                                where a.StageCode.Contains(term) && a.StageCode != null && a.StageCode != "" && lstLocation.Contains(a.Location) && (stagetype != "" ? lstStage.Contains(a.StageType) : true) && (FetchNDEStages == true ? a.StageDepartment == "NDE" : true)
                                select new ddlValue { Text = a.StageCode, Value = a.StageCode }).OrderBy(x => x.Value).Distinct().Take(10).ToList();
            }
            else
            {
                lstStageCode = (from a in db.QMS002
                                where a.StageCode != null && a.StageCode != "" && lstLocation.Contains(a.Location) && (stagetype != "" ? lstStage.Contains(a.StageType) : true) && (FetchNDEStages == true ? a.StageDepartment == "NDE" : true)
                                select new ddlValue { Text = a.StageCode, Value = a.StageCode }).OrderBy(x => x.Value).Distinct().Take(10).ToList();
            }
            return Json(lstStageCode, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetStages(string term, string location, string fromProject, string toProject, string fromSeam, string toSeam, bool IsQualityProject = false)
        {
            if (string.IsNullOrWhiteSpace(location))
            {
                location = objClsLoginInfo.Location;
            }
            List<ddlValue> lstSeamNo = new List<ddlValue>();
            List<ddlValue> lstStageCode = new List<ddlValue>();
            if (IsQualityProject)
            {
                if (!string.IsNullOrWhiteSpace(term))
                {
                    lstStageCode = (from a in db.QMS050
                                    where a.StageCode.Contains(term) && a.StageCode != null && a.StageCode != "" && a.QualityProject.CompareTo(fromProject) >= 0 && a.QualityProject.CompareTo(toProject) <= 0
                                     && a.SeamNo.CompareTo(fromSeam) >= 0 && a.SeamNo.CompareTo(toSeam) <= 0
                                    select new ddlValue { Text = a.StageCode, Value = a.StageCode }).Distinct().Take(10).ToList();
                }
                else
                {
                    lstStageCode = (from a in db.QMS050
                                    where a.StageCode != null && a.StageCode != "" && a.QualityProject.CompareTo(fromProject) >= 0 && a.QualityProject.CompareTo(toProject) <= 0
                                    && a.SeamNo.CompareTo(fromSeam) >= 0 && a.SeamNo.CompareTo(toSeam) <= 0
                                    select new ddlValue { Text = a.StageCode, Value = a.StageCode }).Distinct().Take(10).ToList();
                }
            }
            else
            {
                List<QMS050> lstAllQualityProject = (from a in db.QMS050
                                                     select a).OrderBy(x => x.Project.Trim()).Distinct().ToList();

                List<string> newProjectList = new List<string>();
                bool start = false;

                foreach (QMS050 proj in lstAllQualityProject)
                {
                    if (proj.Project == fromProject)
                    {
                        start = true;
                    }

                    if (start)
                        newProjectList.Add(proj.Project);

                    if (proj.Project == toProject)
                    {
                        break;
                    }
                }
                newProjectList = newProjectList.Distinct().ToList();
                lstSeamNo = (from a in db.QMS050
                             where newProjectList.Contains(a.Project)
                             select new ddlValue { Text = a.SeamNo, Value = a.SeamNo }).Distinct().ToList();
                List<string> newSeamNoList = new List<string>();
                foreach (var proj in lstSeamNo)
                {
                    if (proj.Text == fromSeam)
                    {
                        start = true;
                    }

                    if (start)
                        newSeamNoList.Add(proj.Text);

                    if (proj.Text == toSeam)
                    {
                        break;
                    }
                }
                newSeamNoList = newSeamNoList.Distinct().ToList();

                if (!string.IsNullOrWhiteSpace(term))
                {
                    lstStageCode = (from a in db.QMS050
                                    where a.StageCode.Contains(term) && a.StageCode != null && a.StageCode != "" && newProjectList.Contains(a.Project) && newSeamNoList.Contains(a.SeamNo)
                                    select new ddlValue { Text = a.StageCode, Value = a.StageCode }).Distinct().Take(10).ToList();
                }
                else
                {
                    lstStageCode = (from a in db.QMS050
                                    where newProjectList.Contains(a.Project) && a.StageCode != null && a.StageCode != "" && newSeamNoList.Contains(a.SeamNo)
                                    select new ddlValue { Text = a.StageCode, Value = a.StageCode }).Distinct().Take(10).ToList();
                }
            }
            return Json(lstStageCode, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetStagesbySeamNo(string term, string project, string seamno)
        {
            List<string> lstProject = new List<string>();
            if (!string.IsNullOrWhiteSpace(project))
            {
                lstProject = project.Split(',').ToList();
            }

            List<string> lstSeamNo = new List<string>();
            if (!string.IsNullOrWhiteSpace(seamno))
            {
                lstSeamNo = seamno.Split(',').ToList();
            }
            List<ddlValue> lstStageCode;

            if (!string.IsNullOrWhiteSpace(term))
            {
                lstStageCode = (from a in db.QMS050
                                where a.StageCode.Contains(term) && a.StageCode != null && a.StageCode != "" && lstProject.Contains(a.QualityProject) && lstSeamNo.Contains(a.SeamNo)
                                select new ddlValue { id = a.StageCode, Text = a.StageCode, Value = a.StageCode, text = a.StageCode }).Distinct().Take(10).ToList();
            }
            else
            {
                lstStageCode = (from a in db.QMS050
                                where lstProject.Contains(a.QualityProject) && a.StageCode != null && a.StageCode != "" && lstSeamNo.Contains(a.SeamNo)
                                select new ddlValue { id = a.StageCode, Text = a.StageCode, Value = a.StageCode, text = a.StageCode }).Distinct().ToList();

                lstStageCode.AddRange((from a in db.QMS060
                                       where lstProject.Contains(a.QualityProject) && a.StageCode != null && a.StageCode != "" && lstSeamNo.Contains(a.SeamNo)
                                       select new ddlValue { id = a.StageCode, Text = a.StageCode, Value = a.StageCode, text = a.StageCode }).Distinct().ToList());
            }
            return Json(lstStageCode, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult GetSeamListByQualityProject(string search, string quality_projects, string seam_type, bool withseamtype)
        {
            List<ddlValue> lstSeam = new List<ddlValue>();
            List<string> listProject = quality_projects.Split(',').ToList();
            List<string> listSeamType = new List<string>();
            if (!string.IsNullOrEmpty(seam_type))
            {
                listSeamType = seam_type.Split(',').ToList();
            }
            if (string.IsNullOrEmpty(seam_type) && withseamtype != true)
            {
                if (!string.IsNullOrEmpty(quality_projects))
                {
                    if (!string.IsNullOrWhiteSpace(search))
                    {
                        lstSeam = (from c1 in db.QMS011
                                   join c2 in db.QMS012 on c1.HeaderId equals c2.HeaderId
                                   where listProject.Contains(c1.QualityProject) && (c2.SeamNo.Contains(search) || c2.SeamDescription.Contains(search))
                                   select new ddlValue
                                   {
                                       id = c2.SeamNo,
                                       text = c2.SeamNo,
                                   }).Distinct().ToList();
                    }
                    else
                    {
                        lstSeam = (from c1 in db.QMS011
                                   join c2 in db.QMS012 on c1.HeaderId equals c2.HeaderId
                                   where listProject.Contains(c1.QualityProject)
                                   select new ddlValue
                                   {
                                       id = c2.SeamNo,
                                       text = c2.SeamNo,
                                   }).Distinct().Take(10).ToList();
                    }

                    lstSeam = lstSeam.Distinct().ToList();
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(quality_projects) && !string.IsNullOrEmpty(seam_type))
                {
                    if (!string.IsNullOrWhiteSpace(search))
                    {
                        lstSeam = (from c1 in db.QMS011
                                   join c2 in db.QMS012 on c1.HeaderId equals c2.HeaderId
                                   where listProject.Contains(c1.QualityProject) && listSeamType.Contains(c2.SeamCategory) && (c2.SeamNo.Contains(search) || c2.SeamDescription.Contains(search))
                                   select new ddlValue
                                   {
                                       id = c2.SeamNo,
                                       text = c2.SeamNo,
                                   }).Distinct().ToList();
                    }
                    else
                    {
                        lstSeam = (from c1 in db.QMS011
                                   join c2 in db.QMS012 on c1.HeaderId equals c2.HeaderId
                                   where listProject.Contains(c1.QualityProject) && listSeamType.Contains(c2.SeamCategory)
                                   select new ddlValue
                                   {
                                       id = c2.SeamNo,
                                       text = c2.SeamNo,
                                   }).Distinct().ToList();
                    }

                    lstSeam = lstSeam.Distinct().ToList();
                }
            }
            return Json(lstSeam, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetQIDListByQualityProject_Location(string search, string location1, string quality_projects, string QIDApplicableFor = "")
        {
            List<ddlValue> lstSeam = new List<ddlValue>();
            List<string> listlocation = location1.Split(',').ToList();
            List<string> listproject = quality_projects.Split(',').ToList();

            if (!string.IsNullOrEmpty(location1) && !string.IsNullOrEmpty(quality_projects))
            {
                if (!string.IsNullOrWhiteSpace(search))
                {
                    lstSeam = (from c1 in db.QMS020
                               where listlocation.Contains(c1.Location) && listproject.Contains(c1.QualityProject) && (c1.QualityId.Contains(search))
                               select new ddlValue
                               {
                                   id = c1.QualityId,
                                   text = c1.QualityId,
                               }).Distinct().ToList();
                }
                else
                {

                    lstSeam = (from c1 in db.QMS020
                               where listlocation.Contains(c1.Location) && listproject.Contains(c1.QualityProject)
                               select new ddlValue
                               {
                                   id = c1.QualityId,
                                   text = c1.QualityId,
                               }).Distinct().Take(10).ToList();
                }

                lstSeam = lstSeam.Distinct().ToList();
            }
            return Json(lstSeam, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetToAuthorisedQualityProject(string term, string fromproject)
        {
            try
            {
                var bu = db.QMS010.Where(i => i.QualityProject.Equals(fromproject, StringComparison.OrdinalIgnoreCase)).Select(i => i.BU).FirstOrDefault();

                string username = objClsLoginInfo.UserName;
                var lstATHLocation = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.Location).Distinct().ToList();
                var lstCOMLocation = db.COM003.Where(i => i.t_psno.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_loca).Distinct().ToList();

                var lstCOMBU = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.BU).Distinct().ToList();

                lstCOMLocation = lstATHLocation.Union(lstCOMLocation).ToList();

                List<ddlValue> lstQualityProject = new List<ddlValue>();
                if (!string.IsNullOrEmpty(term))
                {
                    lstQualityProject = db.QMS010.Where(i => i.QualityProject.ToLower().Contains(term.ToLower())
                                                                     && lstCOMLocation.Contains(i.Location)
                                                                   && i.BU.Trim() == bu.Trim()
                                                                   ).Select(i => new ddlValue { Value = i.QualityProject, Text = i.QualityProject }).Distinct().ToList();//&& i.CreatedBy.Equals(username,StringComparison.OrdinalIgnoreCase)
                }
                else
                {
                    lstQualityProject = db.QMS010.Where(i => lstCOMLocation.Contains(i.Location)
                                                            && i.BU.Trim() == bu.Trim()
                                                            ).Select(i => new ddlValue { Value = i.QualityProject, Text = i.QualityProject }).Take(10).Distinct().ToList();
                }

                return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public string GetInspectionAgency(string qproject)
        {
            string Agency = string.Empty;
            string inspectionagency = Manager.GetInspectionAgency(qproject);
            if (!string.IsNullOrWhiteSpace(inspectionagency))
            {
                string[] arr = inspectionagency.Split(',').ToArray();
                var sqlQuery = " SELECT SkippedTPI  from QMS026";
                var queryResult = db.Database.SqlQuery<QMS026>(sqlQuery);
                var query = queryResult.Select(info => info.SkippedTPI);
                Agency = string.Join("#", arr.Where(x => !query.Contains(x.ToString())).Take(3).ToArray());
            }
            return Agency;
        }

        [HttpPost]
        public ActionResult GetSeamTypeListByQualityProject(string search, string quality_projects)
        {
            List<ddlValue> lstSeamType = new List<ddlValue>();
            List<string> listProject = quality_projects.Split(',').ToList();

            if (!string.IsNullOrEmpty(quality_projects))
            {
                lstSeamType = (from c1 in db.QMS012
                               where listProject.Contains(c1.QualityProject)
                               select new ddlValue
                               {
                                   id = c1.SeamCategory,
                                   text = c1.SeamCategory,
                               }).Distinct().Take(10).ToList();
                lstSeamType = lstSeamType.Distinct().ToList();
            }
            return Json(lstSeamType, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult DownloadPDFReportFORChemical(string Location, string fQualityProject, string tQualityProject, string fSeam, string tSeam, DateTime? fDate, DateTime? tDate, string fStage, string tStage, string fStageSeq, string tStageSeq, string weldingProcess, string seamAprroveStatus, string TestInstrument, string PrintBy, string type)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            List<string> lstRequestId = new List<string>();
            List<byte[]> lstFileBytes = new List<byte[]>();
            List<RpeortnoClass> lstReportNo = new List<RpeortnoClass>();
            List<ReportList> GetReportList = new List<ReportList>();
            List<RpeortnoClass> GetReportList2 = new List<RpeortnoClass>();

            List<RpeortnoClass> lstofPMI = new List<RpeortnoClass>();

            var SignUrl = ConfigurationManager.AppSettings["SignUrl"];

            try
            {

                GetReportList = db.Database.SqlQuery<ReportList>("SP_RPT_SEAM_CHEMICAL_ANALYSIS_REPORT @Location={0},@QualityProject_f={1},@QualityProject_t={2},@SeamNo_f={3},@SeamNo_t={4},@InspectionDate_f={5},@InspectionDate_t={6},@StageCode_f={7},@StageCode_t={8},@Seqn_f={9},@Seqn_t={10},@WeldingProcess={11},@SeamResult={12},@TestInstrument={13},@PrintBy={14},@RequestId={15}", Location, fQualityProject, tQualityProject, fSeam, tSeam, fDate, tDate, fStage, tStage, fStageSeq, tStageSeq, weldingProcess, seamAprroveStatus, TestInstrument, PrintBy, "").ToList();
                var _GetReportList = GetReportList.OrderBy(a => a.RefRequestId).Select(x => new { x.RefRequestId, x.QualityProject, x.StageCode, x.PageCnt, x.ReportNumber }).Distinct();

                foreach (var item in _GetReportList)
                {
                    RpeortnoClass objModel = new RpeortnoClass();
                    objModel.RequestId = item.RefRequestId;
                    objModel.QualityProject = item.QualityProject;
                    objModel.PageCnt = item.PageCnt;
                    objModel.StageCode = item.StageCode;
                    objModel.StageType = type;
                    objModel.Reportnumber = item.ReportNumber;
                    lstReportNo.Add(objModel);
                }

                // GetReportList2.AddRange(_GetReportList);
                for (int j = 0; j < lstReportNo.Count; j++)
                {

                    string Reporturl = "";

                    string strReturnType = type;

                    if (strReturnType.ToLower().Contains("chemical"))
                    {
                        Reporturl = "/IPI/QC/CHEMICAL_ANALYSIS_REPORT";
                    }
                    else if (strReturnType.ToLower().Contains("ferrite"))
                    {
                        Reporturl = "/IPI/QC/FERRITE_REPORT";
                    }
                    else if (strReturnType.ToLower().Contains("hardness"))
                    {
                        Reporturl = "/IPI/QC/HARDNESS_REPORT";
                    }
                    else if (strReturnType.ToLower().Contains("pmi"))
                    {
                        Reporturl = "/IPI/QC/PMI_TEST_REPORT";
                    }
                    else if (lstReportNo[j].ProtocolId != null)
                    {
                        Reporturl = "/IPI/PROTOCOL/" + lstReportNo[j].ProtocolId;
                    }
                    else
                    {
                        Reporturl = string.Empty;
                    }

                    if (!string.IsNullOrWhiteSpace(Reporturl))
                    {
                        List<SSRSParam> reportParams = new List<SSRSParam>();
                        if (strReturnType.ToLower().Contains("chemical") || strReturnType.ToLower().Contains("ferrite") || strReturnType.ToLower().Contains("hardness") || strReturnType.ToLower().Contains("pmi"))
                        {
                            reportParams.Add(new SSRSParam { Param = "Location", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "QualityProject_f", Value = Convert.ToString(lstReportNo[j].QualityProject) });
                            reportParams.Add(new SSRSParam { Param = "QualityProject_t", Value = Convert.ToString(lstReportNo[j].QualityProject) });
                            reportParams.Add(new SSRSParam { Param = "SeamNo_f", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "SeamNo_t", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "InspectionDate_f", Value = Convert.ToString(DBNull.Value) });
                            reportParams.Add(new SSRSParam { Param = "InspectionDate_t" });
                            reportParams.Add(new SSRSParam { Param = "StageCode_f", Value = Convert.ToString(lstReportNo[j].StageCode) });
                            reportParams.Add(new SSRSParam { Param = "StageCode_t", Value = Convert.ToString(lstReportNo[j].StageCode) });
                            reportParams.Add(new SSRSParam { Param = "Seqn_f", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "Seqn_t", Value = "" });
                            if (!strReturnType.ToLower().Contains("hardness") && !strReturnType.ToLower().Contains("pmi"))
                            {
                                reportParams.Add(new SSRSParam { Param = "WeldingProcess", Value = "" });
                            }
                            reportParams.Add(new SSRSParam { Param = "SeamResult", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "TestInstrument", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintBy", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "InspectionRemark", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintTPI1", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintTPI2", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintTPI3", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "RequestId", Value = Convert.ToString(lstReportNo[j].RequestId) });
                            reportParams.Add(new SSRSParam { Param = "TPIDateApproval", Value = "No" });
                            reportParams.Add(new SSRSParam { Param = "SignURL", Value = SignUrl });
                            reportParams.Add(new SSRSParam { Param = "PageCount", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "TotalPage", Value = "" });

                        }
                        else
                        {
                            reportParams.Add(new SSRSParam { Param = "HeaderId", Value = Convert.ToString(lstReportNo[j].ProtocolId) });
                        }

                        byte[] fileBytes = (new Utility.Controllers.GeneralController()).GetPDFBytesfromSSRSreport(Reporturl, reportParams);
                        var reader = new iTextSharp.text.pdf.PdfReader(fileBytes);
                        var total = reader.NumberOfPages;
                        lstReportNo[j].PageCnt = total;

                    }



                }




                for (int j = 0; j < lstReportNo.Count; j++)
                {
                    QMS050 objQMS050 = new QMS050();
                    int PageCount = 0;
                    int _RequestId = Convert.ToInt32(lstReportNo[j].RequestId);
                    //int TOtalPage = 0;
                    int MainTotal = 0;
                    var lst = lstReportNo;
                    var data = lstReportNo.FirstOrDefault(a => a.RequestId == lstReportNo[j].RequestId && a.Reportnumber == lstReportNo[j].Reportnumber);

                    var data2 = lstReportNo.Where(a => a.Reportnumber == lstReportNo[j].Reportnumber && a.RequestId < lstReportNo[j].RequestId).Sum(a => a.PageCnt);

                    PageCount = PageCount + Convert.ToInt32(data2);

                    var data3 = lstReportNo.Where(a => a.Reportnumber == lstReportNo[j].Reportnumber).Sum(a => a.PageCnt);
                    MainTotal = Convert.ToInt32(data3);

                    string Reporturl = "";
                    string strReturnType = type;

                    if (strReturnType.ToLower().Contains("chemical"))
                    {
                        Reporturl = "/IPI/QC/CHEMICAL_ANALYSIS_REPORT";
                    }
                    else if (strReturnType.ToLower().Contains("ferrite"))
                    {
                        Reporturl = "/IPI/QC/FERRITE_REPORT";
                    }
                    else if (strReturnType.ToLower().Contains("hardness"))
                    {
                        Reporturl = "/IPI/QC/HARDNESS_REPORT";
                    }
                    else if (strReturnType.ToLower().Contains("pmi"))
                    {
                        Reporturl = "/IPI/QC/PMI_TEST_REPORT";
                    }
                    else if (lstReportNo[j].ProtocolId != null)
                    {
                        Reporturl = "/IPI/PROTOCOL/" + lstReportNo[j].ProtocolId;
                    }
                    else
                    {
                        Reporturl = string.Empty;
                    }

                    if (!string.IsNullOrWhiteSpace(Reporturl))
                    {
                        List<SSRSParam> reportParams = new List<SSRSParam>();
                        if (strReturnType.ToLower().Contains("chemical") || strReturnType.ToLower().Contains("ferrite") || strReturnType.ToLower().Contains("hardness") || strReturnType.ToLower().Contains("pmi"))
                        {
                            reportParams.Add(new SSRSParam { Param = "Location", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "QualityProject_f", Value = Convert.ToString(GetReportList[j].QualityProject) });
                            reportParams.Add(new SSRSParam { Param = "QualityProject_t", Value = Convert.ToString(GetReportList[j].QualityProject) });
                            reportParams.Add(new SSRSParam { Param = "SeamNo_f", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "SeamNo_t", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "InspectionDate_f", Value = Convert.ToString(DBNull.Value) });
                            reportParams.Add(new SSRSParam { Param = "InspectionDate_t" });
                            reportParams.Add(new SSRSParam { Param = "StageCode_f", Value = Convert.ToString(GetReportList[j].StageCode) });
                            reportParams.Add(new SSRSParam { Param = "StageCode_t", Value = Convert.ToString(GetReportList[j].StageCode) });
                            reportParams.Add(new SSRSParam { Param = "Seqn_f", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "Seqn_t", Value = "" });
                            if (!strReturnType.ToLower().Contains("hardness") && !strReturnType.ToLower().Contains("pmi"))
                            {
                                reportParams.Add(new SSRSParam { Param = "WeldingProcess", Value = "" });
                            }
                            reportParams.Add(new SSRSParam { Param = "SeamResult", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "TestInstrument", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintBy", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "InspectionRemark", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintTPI1", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintTPI2", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintTPI3", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "RequestId", Value = Convert.ToString(lstReportNo[j].RequestId) });
                            reportParams.Add(new SSRSParam { Param = "TPIDateApproval", Value = "No" });
                            reportParams.Add(new SSRSParam { Param = "SignURL", Value = SignUrl });
                            reportParams.Add(new SSRSParam { Param = "PageCount", Value = Convert.ToString(PageCount) });
                            reportParams.Add(new SSRSParam { Param = "TotalPage", Value = Convert.ToString(MainTotal) });

                        }
                        else
                        {
                            reportParams.Add(new SSRSParam { Param = "HeaderId", Value = Convert.ToString(lstReportNo[j].ProtocolId) });
                        }

                        byte[] fileBytes = (new Utility.Controllers.GeneralController()).GetPDFBytesfromSSRSreport(Reporturl, reportParams);
                        var reader = new iTextSharp.text.pdf.PdfReader(fileBytes);
                        var total = reader.NumberOfPages;
                        lstReportNo[j].PageCnt = total;
                        if (fileBytes != null)
                        {
                            lstFileBytes.Add(fileBytes);
                        }
                        PageCount++;
                    }
                    //objQMS050 = db.QMS050.FirstOrDefault(a => a.RequestId == _RequestId);
                    //if (objQMS050 != null)
                    //{

                    //    objQMS050.IsCompiledByQA = true;
                    //    objQMS050.CompiledBy = objClsLoginInfo.UserName;
                    //    objQMS050.CompiledOn = DateTime.Now;
                    //    db.SaveChanges();

                    //}

                }



                if (lstFileBytes.Count > 0)
                {
                    //string fileName = fQualityProject;
                    string fileName = fQualityProject + "_" + fStage;
                    fileName = string.Join("�", fileName.Split(Path.GetInvalidFileNameChars()));
                    string pdfName = String.Format("{0}_{1}.{2}", fileName, DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"), "pdf");
                    //string pdfName = String.Format("{0}", fileName, DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"), "pdf");
                    string OutFile = Server.MapPath("~/Resources/Download/") + pdfName;
                    //string outFilePath = (new IEMQS.Areas.Utility.Controllers.FileUploadController()).MergedPDFFileWithMultipleFileBytes(lstFileBytes, OutFile);
                    string outFilePath = MergedPDFFileWithMultipleFileBytes(lstFileBytes, OutFile);

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = outFilePath;
                    objResponseMsg.CheckStageType = pdfName;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No file found.";
                }

                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error Occures Please try again";
                return Json(objResponseMsg);
            }
        }

        public ActionResult DownloadPDFReportFORPMI(string Location, string fQualityProject, string tQualityProject, string fSeam, string tSeam, DateTime? fDate, DateTime? tDate, string fStage, string tStage, string fStageSeq, string tStageSeq, string weldingProcess, string seamAprroveStatus, string TestInstrument, string PrintBy, string type)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            List<string> lstRequestId = new List<string>();
            List<byte[]> lstFileBytes = new List<byte[]>();
            List<RpeortnoClass> lstReportNo = new List<RpeortnoClass>();
            List<PMIReportList> GetReportList = new List<PMIReportList>();
            List<RpeortnoClass> GetReportList2 = new List<RpeortnoClass>();

            List<RpeortnoClass> lstofPMI = new List<RpeortnoClass>();

            var SignUrl = ConfigurationManager.AppSettings["SignUrl"];

            try
            {

                GetReportList = db.Database.SqlQuery<PMIReportList>("SP_RPT_SEAM_PMI_TEST_REPORT @Location={0},@QualityProject_f={1},@QualityProject_t={2},@SeamNo_f={3},@SeamNo_t={4},@InspectionDate_f={5},@InspectionDate_t={6},@StageCode_f={7},@StageCode_t={8},@Seqn_f={9},@Seqn_t={10},@SeamResult={11},@TestInstrument={12},@PrintBy={13},@RequestId={14}", Location, fQualityProject, tQualityProject, fSeam, tSeam, fDate, tDate, fStage, tStage, fStageSeq, tStageSeq,  seamAprroveStatus, TestInstrument, PrintBy, "").ToList();
                var _GetReportList = GetReportList.OrderBy(a=>a.ReportNumber).ThenBy(a=>a.RefRequestId).Select(x => new { x.RefRequestId, x.QualityProject, x.StageCode, x.PageCnt, x.ReportNumber }).Distinct();

                foreach (var item in _GetReportList)
                {
                    RpeortnoClass objModel = new RpeortnoClass();
                    objModel.RequestId = item.RefRequestId;
                    objModel.QualityProject = item.QualityProject;
                    objModel.PageCnt = item.PageCnt;
                    objModel.StageCode = item.StageCode;
                    objModel.StageType = type;
                    objModel.Reportnumber = item.ReportNumber;
                    lstReportNo.Add(objModel);
                }

                // GetReportList2.AddRange(_GetReportList);
                for (int j = 0; j < lstReportNo.Count; j++)
                {

                    string Reporturl = "";

                    string strReturnType = type;

                    if (strReturnType.ToLower().Contains("chemical"))
                    {
                        Reporturl = "/IPI/QC/CHEMICAL_ANALYSIS_REPORT";
                    }
                    else if (strReturnType.ToLower().Contains("ferrite"))
                    {
                        Reporturl = "/IPI/QC/FERRITE_REPORT";
                    }
                    else if (strReturnType.ToLower().Contains("hardness"))
                    {
                        Reporturl = "/IPI/QC/HARDNESS_REPORT";
                    }
                    else if (strReturnType.ToLower().Contains("pmi"))
                    {
                        Reporturl = "/IPI/QC/PMI_TEST_REPORT";
                    }
                    else if (lstReportNo[j].ProtocolId != null)
                    {
                        Reporturl = "/IPI/PROTOCOL/" + lstReportNo[j].ProtocolId;
                    }
                    else
                    {
                        Reporturl = string.Empty;
                    }

                    if (!string.IsNullOrWhiteSpace(Reporturl))
                    {
                        List<SSRSParam> reportParams = new List<SSRSParam>();
                        if (strReturnType.ToLower().Contains("chemical") || strReturnType.ToLower().Contains("ferrite") || strReturnType.ToLower().Contains("hardness") || strReturnType.ToLower().Contains("pmi"))
                        {
                            reportParams.Add(new SSRSParam { Param = "Location", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "QualityProject_f", Value = Convert.ToString(lstReportNo[j].QualityProject) });
                            reportParams.Add(new SSRSParam { Param = "QualityProject_t", Value = Convert.ToString(lstReportNo[j].QualityProject) });
                            reportParams.Add(new SSRSParam { Param = "SeamNo_f", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "SeamNo_t", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "InspectionDate_f", Value = Convert.ToString(DBNull.Value) });
                            reportParams.Add(new SSRSParam { Param = "InspectionDate_t" });
                            reportParams.Add(new SSRSParam { Param = "StageCode_f", Value = Convert.ToString(lstReportNo[j].StageCode) });
                            reportParams.Add(new SSRSParam { Param = "StageCode_t", Value = Convert.ToString(lstReportNo[j].StageCode) });
                            reportParams.Add(new SSRSParam { Param = "Seqn_f", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "Seqn_t", Value = "" });
                            if (!strReturnType.ToLower().Contains("hardness") && !strReturnType.ToLower().Contains("pmi"))
                            {
                                reportParams.Add(new SSRSParam { Param = "WeldingProcess", Value = "" });
                            }
                            reportParams.Add(new SSRSParam { Param = "SeamResult", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "TestInstrument", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintBy", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "InspectionRemark", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintTPI1", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintTPI2", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintTPI3", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "RequestId", Value = Convert.ToString(lstReportNo[j].RequestId) });
                            reportParams.Add(new SSRSParam { Param = "TPIDateApproval", Value = "No" });
                            reportParams.Add(new SSRSParam { Param = "SignURL", Value = SignUrl });
                            reportParams.Add(new SSRSParam { Param = "PageCount", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "TotalPage", Value = "" });

                        }
                        else
                        {
                            reportParams.Add(new SSRSParam { Param = "HeaderId", Value = Convert.ToString(lstReportNo[j].ProtocolId) });
                        }

                        byte[] fileBytes = (new Utility.Controllers.GeneralController()).GetPDFBytesfromSSRSreport(Reporturl, reportParams);
                        var reader = new iTextSharp.text.pdf.PdfReader(fileBytes);
                        var total = reader.NumberOfPages;
                        lstReportNo[j].PageCnt = total;

                    }



                }




                for (int j = 0; j < lstReportNo.Count; j++)
                {
                    QMS050 objQMS050 = new QMS050();
                    int PageCount = 0;
                    int _RequestId = Convert.ToInt32(lstReportNo[j].RequestId);
                    //int TOtalPage = 0;
                    int MainTotal = 0;
                    var lst = lstReportNo;
                    var data = lstReportNo.FirstOrDefault(a => a.RequestId == lstReportNo[j].RequestId && a.Reportnumber == lstReportNo[j].Reportnumber);

                    var data2 = lstReportNo.Where(a => a.Reportnumber == lstReportNo[j].Reportnumber && a.RequestId < lstReportNo[j].RequestId).Sum(a => a.PageCnt);

                    PageCount = PageCount + Convert.ToInt32(data2);

                    var data3 = lstReportNo.Where(a => a.Reportnumber == lstReportNo[j].Reportnumber).Sum(a => a.PageCnt);
                    MainTotal = Convert.ToInt32(data3);

                    string Reporturl = "";
                    string strReturnType = type;

                    if (strReturnType.ToLower().Contains("chemical"))
                    {
                        Reporturl = "/IPI/QC/CHEMICAL_ANALYSIS_REPORT";
                    }
                    else if (strReturnType.ToLower().Contains("ferrite"))
                    {
                        Reporturl = "/IPI/QC/FERRITE_REPORT";
                    }
                    else if (strReturnType.ToLower().Contains("hardness"))
                    {
                        Reporturl = "/IPI/QC/HARDNESS_REPORT";
                    }
                    else if (strReturnType.ToLower().Contains("pmi"))
                    {
                        Reporturl = "/IPI/QC/PMI_TEST_REPORT";
                    }
                    else if (lstReportNo[j].ProtocolId != null)
                    {
                        Reporturl = "/IPI/PROTOCOL/" + lstReportNo[j].ProtocolId;
                    }
                    else
                    {
                        Reporturl = string.Empty;
                    }

                    if (!string.IsNullOrWhiteSpace(Reporturl))
                    {
                        List<SSRSParam> reportParams = new List<SSRSParam>();
                        if (strReturnType.ToLower().Contains("chemical") || strReturnType.ToLower().Contains("ferrite") || strReturnType.ToLower().Contains("hardness") || strReturnType.ToLower().Contains("pmi"))
                        {
                            reportParams.Add(new SSRSParam { Param = "Location", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "QualityProject_f", Value = Convert.ToString(GetReportList[j].QualityProject) });
                            reportParams.Add(new SSRSParam { Param = "QualityProject_t", Value = Convert.ToString(GetReportList[j].QualityProject) });
                            reportParams.Add(new SSRSParam { Param = "SeamNo_f", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "SeamNo_t", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "InspectionDate_f", Value = Convert.ToString(DBNull.Value) });
                            reportParams.Add(new SSRSParam { Param = "InspectionDate_t" });
                            reportParams.Add(new SSRSParam { Param = "StageCode_f", Value = Convert.ToString(GetReportList[j].StageCode) });
                            reportParams.Add(new SSRSParam { Param = "StageCode_t", Value = Convert.ToString(GetReportList[j].StageCode) });
                            reportParams.Add(new SSRSParam { Param = "Seqn_f", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "Seqn_t", Value = "" });
                            if (!strReturnType.ToLower().Contains("hardness") && !strReturnType.ToLower().Contains("pmi"))
                            {
                                reportParams.Add(new SSRSParam { Param = "WeldingProcess", Value = "" });
                            }
                            reportParams.Add(new SSRSParam { Param = "SeamResult", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "TestInstrument", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintBy", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "InspectionRemark", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintTPI1", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintTPI2", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintTPI3", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "RequestId", Value = Convert.ToString(lstReportNo[j].RequestId) });
                            reportParams.Add(new SSRSParam { Param = "TPIDateApproval", Value = "No" });
                            reportParams.Add(new SSRSParam { Param = "SignURL", Value = SignUrl });
                            reportParams.Add(new SSRSParam { Param = "PageCount", Value = Convert.ToString(PageCount) });
                            reportParams.Add(new SSRSParam { Param = "TotalPage", Value = Convert.ToString(MainTotal) });

                        }
                        else
                        {
                            reportParams.Add(new SSRSParam { Param = "HeaderId", Value = Convert.ToString(lstReportNo[j].ProtocolId) });
                        }

                        byte[] fileBytes = (new Utility.Controllers.GeneralController()).GetPDFBytesfromSSRSreport(Reporturl, reportParams);
                        var reader = new iTextSharp.text.pdf.PdfReader(fileBytes);
                        var total = reader.NumberOfPages;
                        lstReportNo[j].PageCnt = total;
                        if (fileBytes != null)
                        {
                            lstFileBytes.Add(fileBytes);
                        }
                        PageCount++;
                    }
                 
                }



                if (lstFileBytes.Count > 0)
                {
                    //string fileName = fQualityProject;
                    string fileName = fQualityProject + "_" + fStage;
                    fileName = string.Join("�", fileName.Split(Path.GetInvalidFileNameChars()));
                    string pdfName = String.Format("{0}_{1}.{2}", fileName, DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"), "pdf");
                    //string pdfName = String.Format("{0}", fileName, DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"), "pdf");
                    string OutFile = Server.MapPath("~/Resources/Download/") + pdfName;
                    //string outFilePath = (new IEMQS.Areas.Utility.Controllers.FileUploadController()).MergedPDFFileWithMultipleFileBytes(lstFileBytes, OutFile);
                    string outFilePath = MergedPDFFileWithMultipleFileBytes(lstFileBytes, OutFile);

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = outFilePath;
                    objResponseMsg.CheckStageType = pdfName;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No file found.";
                }

                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error Occures Please try again";
                return Json(objResponseMsg);
            }
        }

        public ActionResult DownloadPDFReportFORHardNess(string Location, string fQualityProject, string tQualityProject, string fSeam, string tSeam, DateTime? fDate, DateTime? tDate, string fStage, string tStage, string fStageSeq, string tStageSeq, string weldingProcess, string seamAprroveStatus, string TestInstrument, string PrintBy, string type)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            List<string> lstRequestId = new List<string>();
            List<byte[]> lstFileBytes = new List<byte[]>();
            List<RpeortnoClass> lstReportNo = new List<RpeortnoClass>();
            List<HardnessReportList> GetReportList = new List<HardnessReportList>();
            List<RpeortnoClass> GetReportList2 = new List<RpeortnoClass>();

            List<RpeortnoClass> lstofPMI = new List<RpeortnoClass>();

            var SignUrl = ConfigurationManager.AppSettings["SignUrl"];

            try
            {

                GetReportList = db.Database.SqlQuery<HardnessReportList>("SP_RPT_SEAM_HARDNESS_TEST_REPORT @Location={0},@QualityProject_f={1},@QualityProject_t={2},@SeamNo_f={3},@SeamNo_t={4},@InspectionDate_f={5},@InspectionDate_t={6},@StageCode_f={7},@StageCode_t={8},@Seqn_f={9},@Seqn_t={10},@SeamResult={11},@TestInstrument={12},@PrintBy={13},@RequestId={14}", Location, fQualityProject, tQualityProject, fSeam, tSeam, fDate, tDate, fStage, tStage, fStageSeq, tStageSeq, seamAprroveStatus, TestInstrument, PrintBy, "").ToList();
                var _GetReportList = GetReportList.OrderBy(a => a.ReportNumber).ThenBy(a=>a.RefRequestId).Select(x => new { x.RefRequestId, x.QualityProject, x.StageCode, x.PageCnt, x.ReportNumber }).Distinct();

                foreach (var item in _GetReportList)
                {
                    RpeortnoClass objModel = new RpeortnoClass();
                    objModel.RequestId = item.RefRequestId;
                    objModel.QualityProject = item.QualityProject;
                    objModel.PageCnt = item.PageCnt;
                    objModel.StageCode = item.StageCode;
                    objModel.StageType = type;
                    objModel.Reportnumber = item.ReportNumber;
                    lstReportNo.Add(objModel);
                }

                // GetReportList2.AddRange(_GetReportList);
                for (int j = 0; j < lstReportNo.Count; j++)
                {

                    string Reporturl = "";

                    string strReturnType = type;

                    if (strReturnType.ToLower().Contains("chemical"))
                    {
                        Reporturl = "/IPI/QC/CHEMICAL_ANALYSIS_REPORT";
                    }
                    else if (strReturnType.ToLower().Contains("ferrite"))
                    {
                        Reporturl = "/IPI/QC/FERRITE_REPORT";
                    }
                    else if (strReturnType.ToLower().Contains("hardness"))
                    {
                        Reporturl = "/IPI/QC/HARDNESS_REPORT";
                    }
                    else if (strReturnType.ToLower().Contains("pmi"))
                    {
                        Reporturl = "/IPI/QC/PMI_TEST_REPORT";
                    }
                    else if (lstReportNo[j].ProtocolId != null)
                    {
                        Reporturl = "/IPI/PROTOCOL/" + lstReportNo[j].ProtocolId;
                    }
                    else
                    {
                        Reporturl = string.Empty;
                    }

                    if (!string.IsNullOrWhiteSpace(Reporturl))
                    {
                        List<SSRSParam> reportParams = new List<SSRSParam>();
                        if (strReturnType.ToLower().Contains("chemical") || strReturnType.ToLower().Contains("ferrite") || strReturnType.ToLower().Contains("hardness") || strReturnType.ToLower().Contains("pmi"))
                        {
                            reportParams.Add(new SSRSParam { Param = "Location", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "QualityProject_f", Value = Convert.ToString(lstReportNo[j].QualityProject) });
                            reportParams.Add(new SSRSParam { Param = "QualityProject_t", Value = Convert.ToString(lstReportNo[j].QualityProject) });
                            reportParams.Add(new SSRSParam { Param = "SeamNo_f", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "SeamNo_t", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "InspectionDate_f", Value = Convert.ToString(DBNull.Value) });
                            reportParams.Add(new SSRSParam { Param = "InspectionDate_t" });
                            reportParams.Add(new SSRSParam { Param = "StageCode_f", Value = Convert.ToString(lstReportNo[j].StageCode) });
                            reportParams.Add(new SSRSParam { Param = "StageCode_t", Value = Convert.ToString(lstReportNo[j].StageCode) });
                            reportParams.Add(new SSRSParam { Param = "Seqn_f", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "Seqn_t", Value = "" });
                            if (!strReturnType.ToLower().Contains("hardness") && !strReturnType.ToLower().Contains("pmi"))
                            {
                                reportParams.Add(new SSRSParam { Param = "WeldingProcess", Value = "" });
                            }
                            reportParams.Add(new SSRSParam { Param = "SeamResult", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "TestInstrument", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintBy", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "InspectionRemark", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintTPI1", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintTPI2", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintTPI3", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "RequestId", Value = Convert.ToString(lstReportNo[j].RequestId) });
                            reportParams.Add(new SSRSParam { Param = "TPIDateApproval", Value = "No" });
                            reportParams.Add(new SSRSParam { Param = "SignURL", Value = SignUrl });
                            reportParams.Add(new SSRSParam { Param = "PageCount", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "TotalPage", Value = "" });

                        }
                        else
                        {
                            reportParams.Add(new SSRSParam { Param = "HeaderId", Value = Convert.ToString(lstReportNo[j].ProtocolId) });
                        }

                        byte[] fileBytes = (new Utility.Controllers.GeneralController()).GetPDFBytesfromSSRSreport(Reporturl, reportParams);
                        var reader = new iTextSharp.text.pdf.PdfReader(fileBytes);
                        var total = reader.NumberOfPages;
                        lstReportNo[j].PageCnt = total;

                    }



                }




                for (int j = 0; j < lstReportNo.Count; j++)
                {
                    QMS050 objQMS050 = new QMS050();
                    int PageCount = 0;
                    int _RequestId = Convert.ToInt32(lstReportNo[j].RequestId);
                    //int TOtalPage = 0;
                    int MainTotal = 0;
                    var lst = lstReportNo;
                    var data = lstReportNo.FirstOrDefault(a => a.RequestId == lstReportNo[j].RequestId && a.Reportnumber == lstReportNo[j].Reportnumber);

                    var data2 = lstReportNo.Where(a => a.Reportnumber == lstReportNo[j].Reportnumber && a.RequestId < lstReportNo[j].RequestId).Sum(a => a.PageCnt);

                    PageCount = PageCount + Convert.ToInt32(data2);

                    var data3 = lstReportNo.Where(a => a.Reportnumber == lstReportNo[j].Reportnumber).Sum(a => a.PageCnt);
                    MainTotal = Convert.ToInt32(data3);

                    string Reporturl = "";
                    string strReturnType = type;

                    if (strReturnType.ToLower().Contains("chemical"))
                    {
                        Reporturl = "/IPI/QC/CHEMICAL_ANALYSIS_REPORT";
                    }
                    else if (strReturnType.ToLower().Contains("ferrite"))
                    {
                        Reporturl = "/IPI/QC/FERRITE_REPORT";
                    }
                    else if (strReturnType.ToLower().Contains("hardness"))
                    {
                        Reporturl = "/IPI/QC/HARDNESS_REPORT";
                    }
                    else if (strReturnType.ToLower().Contains("pmi"))
                    {
                        Reporturl = "/IPI/QC/PMI_TEST_REPORT";
                    }
                    else if (lstReportNo[j].ProtocolId != null)
                    {
                        Reporturl = "/IPI/PROTOCOL/" + lstReportNo[j].ProtocolId;
                    }
                    else
                    {
                        Reporturl = string.Empty;
                    }

                    if (!string.IsNullOrWhiteSpace(Reporturl))
                    {
                        List<SSRSParam> reportParams = new List<SSRSParam>();
                        if (strReturnType.ToLower().Contains("chemical") || strReturnType.ToLower().Contains("ferrite") || strReturnType.ToLower().Contains("hardness") || strReturnType.ToLower().Contains("pmi"))
                        {
                            reportParams.Add(new SSRSParam { Param = "Location", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "QualityProject_f", Value = Convert.ToString(GetReportList[j].QualityProject) });
                            reportParams.Add(new SSRSParam { Param = "QualityProject_t", Value = Convert.ToString(GetReportList[j].QualityProject) });
                            reportParams.Add(new SSRSParam { Param = "SeamNo_f", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "SeamNo_t", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "InspectionDate_f", Value = Convert.ToString(DBNull.Value) });
                            reportParams.Add(new SSRSParam { Param = "InspectionDate_t" });
                            reportParams.Add(new SSRSParam { Param = "StageCode_f", Value = Convert.ToString(GetReportList[j].StageCode) });
                            reportParams.Add(new SSRSParam { Param = "StageCode_t", Value = Convert.ToString(GetReportList[j].StageCode) });
                            reportParams.Add(new SSRSParam { Param = "Seqn_f", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "Seqn_t", Value = "" });
                            if (!strReturnType.ToLower().Contains("hardness") && !strReturnType.ToLower().Contains("pmi"))
                            {
                                reportParams.Add(new SSRSParam { Param = "WeldingProcess", Value = "" });
                            }
                            reportParams.Add(new SSRSParam { Param = "SeamResult", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "TestInstrument", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintBy", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "InspectionRemark", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintTPI1", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintTPI2", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintTPI3", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "RequestId", Value = Convert.ToString(lstReportNo[j].RequestId) });
                            reportParams.Add(new SSRSParam { Param = "TPIDateApproval", Value = "No" });
                            reportParams.Add(new SSRSParam { Param = "SignURL", Value = SignUrl });
                            reportParams.Add(new SSRSParam { Param = "PageCount", Value = Convert.ToString(PageCount) });
                            reportParams.Add(new SSRSParam { Param = "TotalPage", Value = Convert.ToString(MainTotal) });

                        }
                        else
                        {
                            reportParams.Add(new SSRSParam { Param = "HeaderId", Value = Convert.ToString(lstReportNo[j].ProtocolId) });
                        }

                        byte[] fileBytes = (new Utility.Controllers.GeneralController()).GetPDFBytesfromSSRSreport(Reporturl, reportParams);
                        var reader = new iTextSharp.text.pdf.PdfReader(fileBytes);
                        var total = reader.NumberOfPages;
                        lstReportNo[j].PageCnt = total;
                        if (fileBytes != null)
                        {
                            lstFileBytes.Add(fileBytes);
                        }
                        PageCount++;
                    }
                    //objQMS050 = db.QMS050.FirstOrDefault(a => a.RequestId == _RequestId);
                    //if (objQMS050 != null)
                    //{

                    //    objQMS050.IsCompiledByQA = true;
                    //    objQMS050.CompiledBy = objClsLoginInfo.UserName;
                    //    objQMS050.CompiledOn = DateTime.Now;
                    //    db.SaveChanges();

                    //}

                }



                if (lstFileBytes.Count > 0)
                {
                    //string fileName = fQualityProject;
                    string fileName = fQualityProject + "_" + fStage;
                    fileName = string.Join("�", fileName.Split(Path.GetInvalidFileNameChars()));
                    string pdfName = String.Format("{0}_{1}.{2}", fileName, DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"), "pdf");
                    //string pdfName = String.Format("{0}", fileName, DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"), "pdf");
                    string OutFile = Server.MapPath("~/Resources/Download/") + pdfName;
                    //string outFilePath = (new IEMQS.Areas.Utility.Controllers.FileUploadController()).MergedPDFFileWithMultipleFileBytes(lstFileBytes, OutFile);
                    string outFilePath = MergedPDFFileWithMultipleFileBytes(lstFileBytes, OutFile);

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = outFilePath;
                    objResponseMsg.CheckStageType = pdfName;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No file found.";
                }

                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error Occures Please try again";
                return Json(objResponseMsg);
            }
        }

        public ActionResult DownloadPDFReportFORFerrite(string Location, string fQualityProject, string tQualityProject, string fSeam, string tSeam, DateTime? fDate, DateTime? tDate, string fStage, string tStage, string fStageSeq, string tStageSeq, string weldingProcess, string seamAprroveStatus, string TestInstrument, string PrintBy, string type)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            List<string> lstRequestId = new List<string>();
            List<byte[]> lstFileBytes = new List<byte[]>();
            List<RpeortnoClass> lstReportNo = new List<RpeortnoClass>();
            List<FerriteReportList> GetReportList = new List<FerriteReportList>();
            List<RpeortnoClass> GetReportList2 = new List<RpeortnoClass>();

            List<RpeortnoClass> lstofPMI = new List<RpeortnoClass>();

            var SignUrl = ConfigurationManager.AppSettings["SignUrl"];

            try
            {

                GetReportList = db.Database.SqlQuery<FerriteReportList>("SP_RPT_SEAM_FERRITE_TEST_REPORT @Location={0},@QualityProject_f={1},@QualityProject_t={2},@SeamNo_f={3},@SeamNo_t={4},@InspectionDate_f={5},@InspectionDate_t={6},@StageCode_f={7},@StageCode_t={8},@Seqn_f={9},@Seqn_t={10},@WeldingProcess={11},@SeamResult={12},@TestInstrument={13},@PrintBy={14},@RequestId={15}", Location, fQualityProject, tQualityProject, fSeam, tSeam, fDate, tDate, fStage, tStage, fStageSeq, tStageSeq, weldingProcess, seamAprroveStatus, TestInstrument, PrintBy, "").ToList();
                var _GetReportList = GetReportList.OrderBy(a => a.ReportNumber).ThenBy(a => a.RefRequestId).Select(x => new { x.RefRequestId, x.QualityProject, x.StageCode, x.PageCnt, x.ReportNumber }).Distinct();

                foreach (var item in _GetReportList)
                {
                    RpeortnoClass objModel = new RpeortnoClass();
                    objModel.RequestId = item.RefRequestId;
                    objModel.QualityProject = item.QualityProject;
                    objModel.PageCnt = item.PageCnt;
                    objModel.StageCode = item.StageCode;
                    objModel.StageType = type;
                    objModel.Reportnumber = item.ReportNumber;
                    lstReportNo.Add(objModel);
                }

                // GetReportList2.AddRange(_GetReportList);
                for (int j = 0; j < lstReportNo.Count; j++)
                {

                    string Reporturl = "";

                    string strReturnType = type;

                    if (strReturnType.ToLower().Contains("chemical"))
                    {
                        Reporturl = "/IPI/QC/CHEMICAL_ANALYSIS_REPORT";
                    }
                    else if (strReturnType.ToLower().Contains("ferrite"))
                    {
                        Reporturl = "/IPI/QC/FERRITE_REPORT";
                    }
                    else if (strReturnType.ToLower().Contains("hardness"))
                    {
                        Reporturl = "/IPI/QC/HARDNESS_REPORT";
                    }
                    else if (strReturnType.ToLower().Contains("pmi"))
                    {
                        Reporturl = "/IPI/QC/PMI_TEST_REPORT";
                    }
                    else if (lstReportNo[j].ProtocolId != null)
                    {
                        Reporturl = "/IPI/PROTOCOL/" + lstReportNo[j].ProtocolId;
                    }
                    else
                    {
                        Reporturl = string.Empty;
                    }

                    if (!string.IsNullOrWhiteSpace(Reporturl))
                    {
                        List<SSRSParam> reportParams = new List<SSRSParam>();
                        if (strReturnType.ToLower().Contains("chemical") || strReturnType.ToLower().Contains("ferrite") || strReturnType.ToLower().Contains("hardness") || strReturnType.ToLower().Contains("pmi"))
                        {
                            reportParams.Add(new SSRSParam { Param = "Location", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "QualityProject_f", Value = Convert.ToString(lstReportNo[j].QualityProject) });
                            reportParams.Add(new SSRSParam { Param = "QualityProject_t", Value = Convert.ToString(lstReportNo[j].QualityProject) });
                            reportParams.Add(new SSRSParam { Param = "SeamNo_f", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "SeamNo_t", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "InspectionDate_f", Value = Convert.ToString(DBNull.Value) });
                            reportParams.Add(new SSRSParam { Param = "InspectionDate_t" });
                            reportParams.Add(new SSRSParam { Param = "StageCode_f", Value = Convert.ToString(lstReportNo[j].StageCode) });
                            reportParams.Add(new SSRSParam { Param = "StageCode_t", Value = Convert.ToString(lstReportNo[j].StageCode) });
                            reportParams.Add(new SSRSParam { Param = "Seqn_f", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "Seqn_t", Value = "" });
                            if (!strReturnType.ToLower().Contains("hardness") && !strReturnType.ToLower().Contains("pmi"))
                            {
                                reportParams.Add(new SSRSParam { Param = "WeldingProcess", Value = "" });
                            }
                            reportParams.Add(new SSRSParam { Param = "SeamResult", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "TestInstrument", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintBy", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "InspectionRemark", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintTPI1", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintTPI2", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintTPI3", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "RequestId", Value = Convert.ToString(lstReportNo[j].RequestId) });
                            reportParams.Add(new SSRSParam { Param = "TPIDateApproval", Value = "No" });
                            reportParams.Add(new SSRSParam { Param = "SignURL", Value = SignUrl });
                            reportParams.Add(new SSRSParam { Param = "PageCount", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "TotalPage", Value = "" });

                        }
                        else
                        {
                            reportParams.Add(new SSRSParam { Param = "HeaderId", Value = Convert.ToString(lstReportNo[j].ProtocolId) });
                        }

                        byte[] fileBytes = (new Utility.Controllers.GeneralController()).GetPDFBytesfromSSRSreport(Reporturl, reportParams);
                        var reader = new iTextSharp.text.pdf.PdfReader(fileBytes);
                        var total = reader.NumberOfPages;
                        lstReportNo[j].PageCnt = total;

                    }



                }




                for (int j = 0; j < lstReportNo.Count; j++)
                {
                    QMS050 objQMS050 = new QMS050();
                    int PageCount = 0;
                    int _RequestId = Convert.ToInt32(lstReportNo[j].RequestId);
                    //int TOtalPage = 0;
                    int MainTotal = 0;
                    var lst = lstReportNo;
                    var data = lstReportNo.FirstOrDefault(a => a.RequestId == lstReportNo[j].RequestId && a.Reportnumber == lstReportNo[j].Reportnumber);

                    var data2 = lstReportNo.Where(a => a.Reportnumber == lstReportNo[j].Reportnumber && a.RequestId < lstReportNo[j].RequestId).Sum(a => a.PageCnt);

                    PageCount = PageCount + Convert.ToInt32(data2);

                    var data3 = lstReportNo.Where(a => a.Reportnumber == lstReportNo[j].Reportnumber).Sum(a => a.PageCnt);
                    MainTotal = Convert.ToInt32(data3);

                    string Reporturl = "";
                    string strReturnType = type;

                    if (strReturnType.ToLower().Contains("chemical"))
                    {
                        Reporturl = "/IPI/QC/CHEMICAL_ANALYSIS_REPORT";
                    }
                    else if (strReturnType.ToLower().Contains("ferrite"))
                    {
                        Reporturl = "/IPI/QC/FERRITE_REPORT";
                    }
                    else if (strReturnType.ToLower().Contains("hardness"))
                    {
                        Reporturl = "/IPI/QC/HARDNESS_REPORT";
                    }
                    else if (strReturnType.ToLower().Contains("pmi"))
                    {
                        Reporturl = "/IPI/QC/PMI_TEST_REPORT";
                    }
                    else if (lstReportNo[j].ProtocolId != null)
                    {
                        Reporturl = "/IPI/PROTOCOL/" + lstReportNo[j].ProtocolId;
                    }
                    else
                    {
                        Reporturl = string.Empty;
                    }

                    if (!string.IsNullOrWhiteSpace(Reporturl))
                    {
                        List<SSRSParam> reportParams = new List<SSRSParam>();
                        if (strReturnType.ToLower().Contains("chemical") || strReturnType.ToLower().Contains("ferrite") || strReturnType.ToLower().Contains("hardness") || strReturnType.ToLower().Contains("pmi"))
                        {
                            reportParams.Add(new SSRSParam { Param = "Location", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "QualityProject_f", Value = Convert.ToString(GetReportList[j].QualityProject) });
                            reportParams.Add(new SSRSParam { Param = "QualityProject_t", Value = Convert.ToString(GetReportList[j].QualityProject) });
                            reportParams.Add(new SSRSParam { Param = "SeamNo_f", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "SeamNo_t", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "InspectionDate_f", Value = Convert.ToString(DBNull.Value) });
                            reportParams.Add(new SSRSParam { Param = "InspectionDate_t" });
                            reportParams.Add(new SSRSParam { Param = "StageCode_f", Value = Convert.ToString(GetReportList[j].StageCode) });
                            reportParams.Add(new SSRSParam { Param = "StageCode_t", Value = Convert.ToString(GetReportList[j].StageCode) });
                            reportParams.Add(new SSRSParam { Param = "Seqn_f", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "Seqn_t", Value = "" });
                            if (!strReturnType.ToLower().Contains("hardness") && !strReturnType.ToLower().Contains("pmi"))
                            {
                                reportParams.Add(new SSRSParam { Param = "WeldingProcess", Value = "" });
                            }
                            reportParams.Add(new SSRSParam { Param = "SeamResult", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "TestInstrument", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintBy", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "InspectionRemark", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintTPI1", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintTPI2", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintTPI3", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "RequestId", Value = Convert.ToString(lstReportNo[j].RequestId) });
                            reportParams.Add(new SSRSParam { Param = "TPIDateApproval", Value = "No" });
                            reportParams.Add(new SSRSParam { Param = "SignURL", Value = SignUrl });
                            reportParams.Add(new SSRSParam { Param = "PageCount", Value = Convert.ToString(PageCount) });
                            reportParams.Add(new SSRSParam { Param = "TotalPage", Value = Convert.ToString(MainTotal) });

                        }
                        else
                        {
                            reportParams.Add(new SSRSParam { Param = "HeaderId", Value = Convert.ToString(lstReportNo[j].ProtocolId) });
                        }

                        byte[] fileBytes = (new Utility.Controllers.GeneralController()).GetPDFBytesfromSSRSreport(Reporturl, reportParams);
                        var reader = new iTextSharp.text.pdf.PdfReader(fileBytes);
                        var total = reader.NumberOfPages;
                        lstReportNo[j].PageCnt = total;
                        if (fileBytes != null)
                        {
                            lstFileBytes.Add(fileBytes);
                        }
                        PageCount++;
                    }
                    //objQMS050 = db.QMS050.FirstOrDefault(a => a.RequestId == _RequestId);
                    //if (objQMS050 != null)
                    //{

                    //    objQMS050.IsCompiledByQA = true;
                    //    objQMS050.CompiledBy = objClsLoginInfo.UserName;
                    //    objQMS050.CompiledOn = DateTime.Now;
                    //    db.SaveChanges();

                    //}

                }



                if (lstFileBytes.Count > 0)
                {
                    //string fileName = fQualityProject;
                    string fileName = fQualityProject + "_" + fStage;
                    fileName = string.Join("�", fileName.Split(Path.GetInvalidFileNameChars()));
                    string pdfName = String.Format("{0}_{1}.{2}", fileName, DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"), "pdf");
                    //string pdfName = String.Format("{0}", fileName, DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"), "pdf");
                    string OutFile = Server.MapPath("~/Resources/Download/") + pdfName;
                    //string outFilePath = (new IEMQS.Areas.Utility.Controllers.FileUploadController()).MergedPDFFileWithMultipleFileBytes(lstFileBytes, OutFile);
                    string outFilePath = MergedPDFFileWithMultipleFileBytes(lstFileBytes, OutFile);

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = outFilePath;
                    objResponseMsg.CheckStageType = pdfName;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No file found.";
                }

                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error Occures Please try again";
                return Json(objResponseMsg);
            }
        }

        private string MergedPDFFileWithMultipleFileBytes(List<byte[]> lstFileBytes, string fileName, string filetype = "pdf")
        {

            try
            {
                using (var doc = new iTextSharp.text.Document())
                using (FileStream stream = new FileStream(fileName, FileMode.Create))
                using (var pdf = new iTextSharp.text.pdf.PdfCopy(doc, stream))
                {
                    doc.Open();
                    foreach (var fileBytes in lstFileBytes)
                    {
                        try
                        {
                            var reader = new iTextSharp.text.pdf.PdfReader(fileBytes);
                            iTextSharp.text.pdf.PdfImportedPage page = null;

                            for (int i = 0; i < reader.NumberOfPages; i++)
                            {
                                page = pdf.GetImportedPage(reader, i + 1);
                                pdf.AddPage(page);
                            }
                            pdf.FreeReader(reader);
                            reader.Close();
                        }
                        catch (Exception ex)
                        {
                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        }
                    }

                    pdf.Close();
                    stream.Close();
                }
            }
            catch (Exception ex)
            {
                System.IO.File.Delete(fileName);
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return fileName;
        }


        public class QMS026
        {
            public string SkippedTPI { get; set; }
        }


        #region Web based Protocol
        [HttpPost]
        public ActionResult GetSeamListByQualityProject1(string quality_projects)
        {
            List<ddlValue> lstSeam = new List<ddlValue>();
            List<string> listProject = quality_projects.Split(',').ToList();
            List<string> listSeamType = new List<string>();
            lstSeam = (from c1 in db.QMS011
                       join c2 in db.QMS012 on c1.HeaderId equals c2.HeaderId
                       where listProject.Contains(c1.QualityProject)
                       select new ddlValue
                       {
                           id = c2.SeamNo,
                           text = c2.SeamNo,
                           //  }).Distinct().Take(10).ToList();
                       }).Distinct().ToList();

            lstSeam = lstSeam.Distinct().ToList();
            return Json(lstSeam, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetStagesbySeamNo1(string project, string seamno)
        {
            List<string> lstProject = new List<string>();
            if (!string.IsNullOrWhiteSpace(project))
            {
                lstProject = project.Split(',').ToList();
            }

            List<string> lstSeamNo = new List<string>();
            if (!string.IsNullOrWhiteSpace(seamno))
            {
                lstSeamNo = seamno.Split(',').ToList();
            }
            List<ddlValue> lstStageCode;


            lstStageCode = (from a in db.QMS050
                            where lstProject.Contains(a.QualityProject) && a.StageCode != null && a.StageCode != "" && lstSeamNo.Contains(a.SeamNo)
                            select new ddlValue { id = a.StageCode, Text = a.StageCode, Value = a.StageCode, text = a.StageCode }).Distinct().ToList();

            lstStageCode.AddRange((from a in db.QMS060
                                   where lstProject.Contains(a.QualityProject) && a.StageCode != null && a.StageCode != "" && lstSeamNo.Contains(a.SeamNo)
                                   select new ddlValue { id = a.StageCode, Text = a.StageCode, Value = a.StageCode, text = a.StageCode }).Distinct().ToList());

            return Json(lstStageCode, JsonRequestBehavior.AllowGet);
        }
        #endregion


        public class ReportList
        {
            // public int SeamRowNo { get; set; }
            public string ProjectDesc { get; set; }
            public string QualityProject { get; set; }
            public string Project { get; set; }
            public string Location { get; set; }
            public string SeamNo { get; set; }
            public string StageCode { get; set; }
            public string StageDesc { get; set; }
            public int StageSequence { get; set; }

            public Int64 RequestNo { get; set; }
            public string WeldingProcess { get; set; }
            public int Sample { get; set; }
            public string Position { get; set; }
            public double? Depth { get; set; }

            public string Element1 { get; set; }
            public double? ReqMinValue1 { get; set; }
            public double? ReqMaxValue1 { get; set; }

            public double? ExaminationResult1 { get; set; }
            public string Element2 { get; set; }
            public double? ExaminationResult2 { get; set; }

            public double? ReqMinValue2 { get; set; }
            public double? ReqMaxValue2 { get; set; }

            public string Element3 { get; set; }
            public double? ExaminationResult3 { get; set; }

            public double? ReqMinValue3 { get; set; }
            public double? ReqMaxValue3 { get; set; }

            public string Element4 { get; set; }
            public double? ExaminationResult4 { get; set; }

            public double? ReqMinValue4 { get; set; }
            public double? ReqMaxValue4 { get; set; }

            public string Element5 { get; set; }
            public double? ExaminationResult5 { get; set; }

            public double? ReqMinValue5 { get; set; }
            public double? ReqMaxValue5 { get; set; }

            public string Element6 { get; set; }
            public double? ExaminationResult6 { get; set; }

            public double? ReqMinValue6 { get; set; }
            public double? ReqMaxValue6 { get; set; }

            public string Element7 { get; set; }
            public double? ExaminationResult7 { get; set; }

            public double? ReqMinValue7 { get; set; }
            public double? ReqMaxValue7 { get; set; }

            public string Element8 { get; set; }
            public double? ExaminationResult8 { get; set; }
            public double? ReqMinValue8 { get; set; }
            public double? ReqMaxValue8 { get; set; }
            public string ApplicableSpecification { get; set; }
            public string Remarks { get; set; }
            public string Instrument { get; set; }
            public string ChemicalProcedureNo { get; set; }
            public string TestResult { get; set; }
            public string LocationName { get; set; }
            public string QualityId { get; set; }
            public string ElementGroup { get; set; }
            public DateTime InspectedOn { get; set; }
            public string ReportNumber { get; set; }
            public int RefRequestId { get; set; }

            public int? PageCnt { get; set; }

        }

        public class PMIReportList
        {
            // public int SeamRowNo { get; set; }
            public string ProjectDesc { get; set; }
            public string QualityProject { get; set; }
            public string Project { get; set; }
            public string Location { get; set; }
            public string SeamNo { get; set; }
            public string StageCode { get; set; }
            public string StageDesc { get; set; }
            public int StageSequence { get; set; }

            public Int64 RequestNo { get; set; }
            public string WeldingProcess { get; set; }
            public int Sample { get; set; }
            public string Position { get; set; }
            public double? Depth { get; set; }

            public string Element1 { get; set; }
            public double? ReqMinValue1 { get; set; }
            public double? ReqMaxValue1 { get; set; }

            public double? ExaminationResult1 { get; set; }
            public string Element2 { get; set; }
            public double? ExaminationResult2 { get; set; }

            public double? ReqMinValue2 { get; set; }
            public double? ReqMaxValue2 { get; set; }

            public string Element3 { get; set; }
            public double? ExaminationResult3 { get; set; }

            public double? ReqMinValue3 { get; set; }
            public double? ReqMaxValue3 { get; set; }

            public string Element4 { get; set; }
            public double? ExaminationResult4 { get; set; }

            public double? ReqMinValue4 { get; set; }
            public double? ReqMaxValue4 { get; set; }

            public string Element5 { get; set; }
            public double? ExaminationResult5 { get; set; }

            public double? ReqMinValue5 { get; set; }
            public double? ReqMaxValue5 { get; set; }

            public string Element6 { get; set; }
            public double? ExaminationResult6 { get; set; }

            public double? ReqMinValue6 { get; set; }
            public double? ReqMaxValue6 { get; set; }

            public double? InsideReading1 { get; set; }
            public double? OutsideReading1 { get; set; }
            public double? InsideReading2 { get; set; }
            public double? OutsideReading2 { get; set; }
            public double ? InsideReading3 { get; set; }
            public double? OutsideReading3 { get; set; }
            public double? InsideReading4 { get; set; }
            public double ? OutsideReading4 { get; set; }
            public double? InsideReading5 { get; set; }
            public double? OutsideReading5 { get; set; }
            public double? InsideReading6 { get; set; }
            public double? OutsideReading6 { get; set; }
            public string TPRemarks { get; set; }
            public string ApplicableSpecification { get; set; }
            public string Remarks { get; set; }
            public string Instrument { get; set; }
            public string PMIProcedureNo { get; set; }
            public string TestResult { get; set; }
            public string LocationName { get; set; }
            public string QualityId { get; set; }
            public string ElementGroup { get; set; }
            public DateTime InspectedOn { get; set; }
            public string ReportNumber { get; set; }
            public int RefRequestId { get; set; }

            public int? PageCnt { get; set; }

        }

        public class HardnessReportList
        {
            // public int SeamRowNo { get; set; }
            public int qms52LineId { get; set; }
            public string ProjectDesc { get; set; }
            public string QualityProject { get; set; }
            public string Project { get; set; }
            public string Location { get; set; }
            public string SeamNo { get; set; }
            public string StageCode { get; set; }
            public string StageDesc { get; set; }
            public int StageSequence { get; set; }

            public Int64 RequestNo { get; set; }
            public int QMS050HeaderId { get; set; }
            public int HardnessId { get; set; }

            public int SpotNo { get; set; }
            public string SeamLocation { get; set; }

            public double? InsideReading1 { get; set; }
             public double?InsideReading2{ get; set; }
             public double?InsideReading3{ get; set; }
             public double?InsideReading4 { get; set; }
            public double? InsideReading5 { get; set; }
            public double? InsideAverage { get; set; }

            public double? OutsideReading1 { get; set; }
            public double?OutsideReading2 {get;set;}
            public double?OutsideReading3 {get;set;}
            public double?OutsideReading4 {get;set;}
            public double? OutsideReading5{ get; set; }
            public double? OutsideAverage { get; set; }
            public string ApplicableSpecification { get; set; }
            public string Remarks { get; set; }
            public string TPRemarks { get; set; }
            public string Instrument { get; set; }
            
            public string HardnessProcedureNo { get; set; }
            public string TestResult { get; set; }
            public string LocationName { get; set; }
            public string QualityId { get; set; }
            public string ElementGroup { get; set; }
            public DateTime InspectedOn { get; set; }
            public string ReportNumber { get; set; }
            public int RefRequestId { get; set; }


            public int? PageCnt { get; set; }

            public int? HardnessRequiredValue { get; set; }
            public string HardnessUnit { get; set; }
            public string AcceptanceStd { get; set; }

            public string harnessgroup { get; set; }
        }

        public class FerriteReportList
        {
            // public int SeamRowNo { get; set; }

            
            public string ProjectDesc { get; set; }
            public string QualityProject { get; set; }
            public string Project { get; set; }
            public string Location { get; set; }
            public string SeamNo { get; set; }
            public string StageCode { get; set; }
            public string StageDesc { get; set; }
            public int StageSequence { get; set; }

            public Int64 RequestNo { get; set; }
            public int QMS050HeaderId { get; set; }
            //public int HardnessId { get; set; }
            public string WeldingProcess { get; set; }
            public int SpotNo { get; set; }
            public string SeamLocation { get; set; }

            public double? Reading1 { get; set; }
            public double? Reading2 { get; set; }
            public double? Reading3 { get; set; }
            public double? Reading4 { get; set; }
            public double? Reading5 { get; set; }
            public double? Reading6 { get; set; }
            public double? Average { get; set; }
        
            public string ApplicableSpecification { get; set; }
            public string Remarks { get; set; }
            public string TPRemarks { get; set; }
            public string Instrument { get; set; }

            public string FerriteProcedureNo { get; set; }
            public string TestResult { get; set; }
            public string LocationName { get; set; }
            public string QualityId { get; set; }
            public string ElementGroup { get; set; }
            public DateTime? InspectedOn { get; set; }
            public string ReportNumber { get; set; }
            public int RefRequestId { get; set; }

            public int? PageCnt { get; set; }

            public int? HardnessRequiredValue { get; set; }
            public string FerriteUnit { get; set; }
            public string Ferritegroup { get; set; }

            public string harnessgroup { get; set; }
        
            public int? FerriteMinValue{get;set;}
        }   public int? FerriteMaxValue { get; set; }
    }

}