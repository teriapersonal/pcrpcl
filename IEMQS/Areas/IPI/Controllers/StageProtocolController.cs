﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.IPI.Controllers
{
    public class StageProtocolController : clsBase
    {
        // GET: IPI/StageProtocol
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Index()
        {
            ViewBag.Title = "Stagewise Protocol Master";
            return View();
        }

        public ActionResult LoadProtocolDetailPartial()
        {
            var ProtocolType = GetSubCatagory("Protocol Type", objClsLoginInfo.Location, string.Empty);
            string otherfiles = clsImplementationEnum.ProtocolType.Other_Files.GetStringValue();
            ViewBag.lstProtocolType = ProtocolType.Where(x => x.Value.ToLower() != otherfiles.ToLower());

            int BU = Convert.ToInt32(clsImplementationEnum.BLDNumber.BU.GetStringValue());
            var lstBUs = Manager.GetUserwiseBU(objClsLoginInfo.UserName);
            int LOC = Convert.ToInt32(clsImplementationEnum.BLDNumber.LOC.GetStringValue());
            var lstLocations = Manager.GetUserwiseLocation(objClsLoginInfo.UserName);

            ViewBag.lstBu = (from ath1 in db.ATH001
                             join com2 in db.COM002 on ath1.BU equals com2.t_dimx
                             where ath1.Employee.Trim() == objClsLoginInfo.UserName.Trim() && ath1.BU != ""
                             select new { Value = ath1.BU, Code = ath1.BU, CategoryDescription = com2.t_desc }).Distinct().ToList();
            ViewBag.lstlocation = (from ath1 in db.ATH001
                                   join com2 in db.COM002 on ath1.Location equals com2.t_dimx
                                   where ath1.Employee.Trim() == objClsLoginInfo.UserName.Trim()
                                   select new CategoryData { Value = ath1.Location, Code = ath1.Location, CategoryDescription = com2.t_desc }).Distinct().ToList();


            return PartialView("_LoadStagewiseProtocolPartial");
        }

        public ActionResult GetAllLocations(string selectedBU)
        {
            List<CategoryData> lstCategoryData = new List<CategoryData>();
            lstCategoryData = (from ath1 in db.ATH001
                               join com2 in db.COM002 on ath1.Location equals com2.t_dimx
                               where ath1.Employee.Trim() == objClsLoginInfo.UserName.Trim() && (selectedBU != "" ? selectedBU.Contains(ath1.BU) : true)
                               select new CategoryData { Value = ath1.Location, Code = ath1.Location, CategoryDescription = com2.t_desc }).Distinct().ToList();
            return Json(lstCategoryData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadProtocolsMaintainProtocolDataTable(JQueryDataTableParamModel param, string status)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1 and deletedby is null ";

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayColumn = { "pro000.StageCode", "dbo.fun_GETCATEGORYONLYDESCRIPTION(ProtocolType, pro000.Location, pro000.BU, 'Protocol Type')", "Location", "BU" };
                    whereCondition += arrayColumn.MakeDatatableSearchCondition(param.sSearch);
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "BU", "Location");
                var lstHeader = db.SP_IPI_GET_STAGEWISE_PROTOCOL_DATA(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from uc in lstHeader
                          select new[] {
                           Convert.ToString(uc.BUDesc),
                           Convert.ToString(uc.LocationDesc),
                           Convert.ToString(uc.StageCode),
                           Convert.ToString(uc.ProtocolTypeDesc),
                          Helper.HTMLActionString(uc.Id, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteUtilityGridData("+ uc.Id +",'/IPI/StageProtocol/DeleteProtocol',null,'tblProtocolData')")
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult SaveProtocolDetail(PRL000 prl000)
        {

            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL000 objPRL000 = new PRL000();
                List<PRL000> lstexistingPRL000 = db.PRL000.Where(x => x.DeletedBy == null).ToList();
                List<PRL000> lstPRL000 = new List<PRL000>();
                var listBU = prl000.BU.Split(',');
                var listLocation = prl000.Location.Split(',');
                var listStageCode = prl000.StageCode.Split(',');
                var listProtocolType = prl000.ProtocolType.Split(',');

                foreach (var bu in listBU)
                {
                    foreach (var loc in listLocation)
                    {
                        foreach (var stage in listStageCode)
                        {
                            foreach (var protocoltype in listProtocolType)
                            {
                                if (!lstPRL000.Where(x => x.BU == bu && x.Location == loc && x.StageCode == stage && x.ProtocolType == protocoltype).Any() && !lstexistingPRL000.Where(x => x.BU == bu && x.Location == loc && x.StageCode == stage && x.ProtocolType == protocoltype).Any() && db.QMS002.Where(x => x.BU == bu && x.Location == loc && x.StageCode == stage).Any())
                                {
                                    lstPRL000.Add(new PRL000
                                    {
                                        BU = bu,
                                        Location = loc,
                                        CreatedBy = objClsLoginInfo.UserName,
                                        CreatedOn = DateTime.Now,
                                        StageCode = stage,
                                        ProtocolType = protocoltype
                                    });
                                }
                            }
                        }
                    }
                }

                if (lstPRL000.Count() > 0)
                {
                    db.PRL000.AddRange(lstPRL000);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult DeleteProtocol(int id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PRL000 objPRL000 = db.PRL000.Where(x => x.Id == id).FirstOrDefault();
                if (objPRL000 != null)
                {
                    objPRL000.DeletedBy = objClsLoginInfo.UserName;
                    objPRL000.DeletedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Record successfully deleted.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Project not available for delete.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public IQueryable<CategoryData> GetSubCatagory(string Key, string strLoc, string BU = "")
        {
            IQueryable<CategoryData> lstGLB002 = null;
            if (BU != string.Empty) //Get BU and Location Base Sub Category
            {
                lstGLB002 = (from glb002 in db.GLB002
                             join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                             where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.BU.Equals(BU, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true && strLoc.Equals(glb002.Location)
                             select glb002).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Description }).Distinct();
            }
            else //Get Location Base Sub Category
            {
                lstGLB002 = (from glb002 in db.GLB002
                             join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                             where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true && strLoc.Equals(glb002.Location)
                             select glb002).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Description }).Distinct();
            }
            return lstGLB002;
        }

        public JsonResult GetAllBU()
        {
            int BU = Convert.ToInt32(clsImplementationEnum.BLDNumber.BU.GetStringValue());

            var lstBUs = Manager.getBUsByID(BU);

            try
            {
                var items = (from li in lstBUs
                             select new
                             {
                                 id = li.t_dimx,
                                 text = li.t_desc,
                             }).ToList();

                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetAllLocation()
        {
            int LOC = Convert.ToInt32(clsImplementationEnum.BLDNumber.LOC.GetStringValue());

            var lstLocations = Manager.getBUsByID(LOC);

            try
            {
                var items = (from li in lstLocations
                             select new
                             {
                                 id = li.t_dimx,
                                 text = li.t_desc,
                             }).ToList();

                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetAllStages(string selectedBU, string selectedLoc)
        {
            List<CategoryData> lstCategoryData = new List<CategoryData>();
            lstCategoryData = db.QMS002.Where(x => x.StageCode != "" && selectedLoc.Contains(x.Location) && x.IsProtocolApplicable == true && selectedBU.Contains(x.BU)).
                                Select(i =>
                                new CategoryData
                                { id = i.StageCode, text = i.StageCode + "-" + i.StageDesc }
                                ).Distinct().ToList();
            return Json(lstCategoryData, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllProtocol()
        {
            var ProtocolType = GetSubCatagory("Protocol Type", objClsLoginInfo.Location, string.Empty);
            string otherfiles = clsImplementationEnum.ProtocolType.Other_Files.GetStringValue();
            var lstProtocolType = ProtocolType.Where(x => x.Value.ToLower() != otherfiles.ToLower());

            try
            {
                var items = (from li in lstProtocolType
                             select new
                             {
                                 id = li.Value,
                                 text = li.CategoryDescription,
                             }).ToList();

                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
    }
}