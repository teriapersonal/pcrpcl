﻿using IEMQS.Models;
using IEMQSImplementation;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IEMQS.Areas.IPI.Controllers
{
    public class ApproveBOMTemplateController : clsBase
    {

        #region Header
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetHeaderGridDataPartial");
        }
        public ActionResult LoadHeaderData(JQueryDataTableParamModel param, string status)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                //string SortingFields = param.iSortingCols;

                Func<IMB001, string> orderingFunction = (tsk =>
                                        sortColumnIndex == 2 && isLocationSortable ? Convert.ToString(tsk.Location) : "");

                string whereCondition = "1=1";

                //whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qcp1.BU", "qcp1.Location");
                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and status in('" + clsImplementationEnum.BOMTemplateStatus.Submitted.GetStringValue() + "')";
                }
                string[] columnName = { "bom.Project+'-'+com1.t_dsca", "com6.t_bpid+'-'+com6.t_nama", "bom.ContractNo+'-'+com4.t_desc", "TemplateNo", "bom.RevNo" };
                // whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstHeader = db.SP_IPI_BOMTEMPLATEHEADER(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from h in lstHeader
                          select new[] {
                           h.TemplateNo,
                           Convert.ToString(h.HeaderId),
                           Convert.ToString( h.ContractNo),
                           Convert.ToString( h.Project),
                           Convert.ToString( h.Customer),
                           Convert.ToString(h.Status),
                           Convert.ToString( "R"+h.RevNo),
                           ""
                          };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Lines
        [SessionExpireFilter]
        public ActionResult Viewlines(int id)
        {
            ViewBag.id = id;
            BOM001 objBOM001 = db.BOM001.FirstOrDefault(x => x.HeaderId == id);
            objBOM001.Project = db.Database.SqlQuery<string>("SELECT dbo.GET_PROJECTDESC_BY_CODE('" + objBOM001.Project + "')").FirstOrDefault();
            objBOM001.ContractNo = db.Database.SqlQuery<string>("SELECT dbo.GET_ContractName_BY_Code('" + objBOM001.ContractNo + "')").FirstOrDefault();
            objBOM001.Customer = db.Database.SqlQuery<string>("SELECT dbo.GET_CUSTOMERNAMEAME_BY_CODE('" + objBOM001.Customer + "')").FirstOrDefault();

            return View(objBOM001);
        }

        public ActionResult LoadLinesData(JQueryDataTableParamModel param, int id)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string condition = string.Empty;
                condition = "bom.HeaderId = " + id;

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    condition += " and ( bom.Level like '%" + param.sSearch + "%' or bom.ParentItemDescription like '%" + param.sSearch + "%' or bom.ChilditemDescription like '%" + param.sSearch + "%' or bom.CreatedBy like '%" + param.sSearch + "%')";
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstLines = db.SP_IPI_BOMTEMPLATELINES(StartIndex, EndIndex, strSortOrder, condition).ToList();
                int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();

                var res = from c in lstLines
                          select new[] {
                                        Convert.ToString(c.ROW_NO),
                                        Convert.ToString(c.HeaderId),
                                        Convert.ToString(c.LineId),
                                        Convert.ToString(c.Level),
                                        Convert.ToString(c.ParentItemDescription),
                                        Convert.ToString(c.ChilditemDescription),
                                        ""
                          };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords,
                    iTotalRecords = totalRecords,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = condition
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""

                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Export To Excel

        public FileStreamResult ExportToExcel(int id)
        {
            List<ExportBOMCreationWithARMTemplateResult> listExport = new List<ExportBOMCreationWithARMTemplateResult>();
            try
            {
                var lstResult = db.SP_IPI_GetBOMCreationWithARMTemplateForExportToExcel
                               (id).ToList();

                foreach (var item in lstResult)
                {
                    listExport.Add(new ExportBOMCreationWithARMTemplateResult { ItemName = item.ItemName, String2 = item.STRING2, String3 = item.STRING3, String4 = item.STRING4 });
                }

                //string[] columns = { "ItemName", "String2", "String3", "String4" };
                //byte[] filecontent = ExcelExportHelper.ExportExcel(listExport, "", false, columns);
                //return File(filecontent, ExcelExportHelper.ExcelContentType, "BOMCreationWithARMTemplate.xlsx"); //return type FileContentResult

                MemoryStream ms = new MemoryStream();
                using (FileStream fs = System.IO.File.OpenRead(Server.MapPath("~/Areas/IPI/Views/ApproveBOMTemplate/BOMCreationWithARMTemplate.xlsx")))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();                        
                        int i = 2;
                        foreach (var item in lstResult)
                        {
                            //listExport.Add(new ExportBOMCreationWithARMTemplateResult { ItemName = item.ItemName, String2 = item.STRING2, String3 = item.STRING3, String4 = item.STRING4 });
                            excelWorksheet.Cells[i, 5].Value = item.ItemName.ToString();
                            excelWorksheet.Cells[i, 6].Value = item.STRING2.ToString();
                            excelWorksheet.Cells[i, 7].Value = item.STRING3.ToString();
                            excelWorksheet.Cells[i, 8].Value = item.STRING4.ToString();
                            i++;
                        }
                        excelPackage.SaveAs(ms); // This is the important part.
                    }
                }

                ms.Position = 0;

                return new FileStreamResult(ms, "application/xlsx")
                {
                    FileDownloadName = "ExportPlanningBOM.xlsx"
                };
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_IPI_BOMTEMPLATEHEADER(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      TemplateNo = li.TemplateNo,
                                      ContractNo = li.ContractNo,
                                      Customer = li.Customer,
                                      Project = li.Project,
                                      Status = li.Status,
                                      RevNo = li.RevNo,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_IPI_BOMTEMPLATELINES(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from li in lst
                                  select new
                                  {
                                      Level = li.Level,
                                      ParentItemDescription = li.ParentItemDescription,
                                      ChilditemDescription = li.ChilditemDescription
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        [Serializable]
        public class ExportBOMCreationWithARMTemplateResult
        {
            public string ItemName { get; set; }
            public string String2 { get; set; }
            public string String3 { get; set; }
            public string String4 { get; set; }
        }

        public class ExcelExportHelper
        {
            public static string ExcelContentType
            {
                get
                { return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"; }
            }

            public static DataTable ListToDataTable<T>(List<T> data)
            {
                PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
                DataTable dataTable = new DataTable();

                for (int i = 0; i < properties.Count; i++)
                {
                    PropertyDescriptor property = properties[i];
                    dataTable.Columns.Add(property.Name, Nullable.GetUnderlyingType(property.PropertyType) ?? property.PropertyType);
                }

                object[] values = new object[properties.Count];
                foreach (T item in data)
                {
                    for (int i = 0; i < values.Length; i++)
                    {
                        values[i] = properties[i].GetValue(item);
                    }

                    dataTable.Rows.Add(values);
                }
                return dataTable;
            }

            public static byte[] ExportExcel(DataTable dataTable, string heading = "", bool showSrNo = false, params string[] columnsToTake)
            {

                byte[] result = null;
                using (ExcelPackage package = new ExcelPackage())
                {
                    ExcelWorksheet workSheet = package.Workbook.Worksheets.Add(String.Format("{0} Data", heading));
                    int startRowFrom = String.IsNullOrEmpty(heading) ? 1 : 3;

                    if (showSrNo)
                    {
                        DataColumn dataColumn = dataTable.Columns.Add("SrNo.", typeof(int));
                        dataColumn.SetOrdinal(0);
                        int index = 1;
                        foreach (DataRow item in dataTable.Rows)
                        {
                            item[0] = index;
                            index++;
                        }
                    }


                    // add the content into the Excel file 
                    workSheet.Cells["A" + startRowFrom].LoadFromDataTable(dataTable, true);

                    // autofit width of cells with small content 
                    int columnIndex = 1;
                    foreach (DataColumn column in dataTable.Columns)
                    {
                        ExcelRange columnCells = workSheet.Cells[workSheet.Dimension.Start.Row, columnIndex, workSheet.Dimension.End.Row, columnIndex];
                        int maxLength = columnCells.Max(cell => cell.Value.ToString().Count());
                        if (maxLength < 150)
                        {
                            workSheet.Column(columnIndex).AutoFit();
                        }


                        columnIndex++;
                    }

                    #region Set Header Row Style
                    // format header - bold, yellow on black 
                    using (ExcelRange r = workSheet.Cells[startRowFrom, 1, startRowFrom, dataTable.Columns.Count])
                    {
                        r.Style.Font.Color.SetColor(System.Drawing.Color.Black);
                        r.Style.Font.Bold = true;
                        r.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.None;
                        //r.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        //r.Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFF"));
                    }
                    #endregion

                    #region For Set Border
                    //// format cells - add borders 
                    ////using (ExcelRange r = workSheet.Cells[startRowFrom + 1, 1, startRowFrom + dataTable.Rows.Count, dataTable.Columns.Count])//Skip Header Row
                    //using (ExcelRange r = workSheet.Cells[startRowFrom, 1, startRowFrom + dataTable.Rows.Count, dataTable.Columns.Count])
                    //{
                    //    r.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    //    r.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    //    r.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    //    r.Style.Border.Right.Style = ExcelBorderStyle.Thin;

                    //    r.Style.Border.Top.Color.SetColor(System.Drawing.Color.Black);
                    //    r.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
                    //    r.Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);
                    //    r.Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
                    //}
                    #endregion

                    #region Remove Extra Column For Export
                    // removed ignored columns 
                    for (int i = dataTable.Columns.Count - 1; i >= 0; i--)
                    {
                        if (i == 0 && showSrNo)
                        {
                            continue;
                        }
                        if (!columnsToTake.Contains(dataTable.Columns[i].ColumnName))
                        {
                            workSheet.DeleteColumn(i + 1);
                        }
                    }
                    #endregion

                    #region Set Heading If Pass
                    if (!String.IsNullOrEmpty(heading))
                    {
                        workSheet.Cells["A1"].Value = heading;
                        workSheet.Cells["A1"].Style.Font.Size = 20;

                        workSheet.InsertColumn(1, 1);
                        workSheet.InsertRow(1, 1);
                        workSheet.Column(1).Width = 5;
                    }
                    #endregion

                    result = package.GetAsByteArray();
                }

                return result;
            }

            public static byte[] ExportExcel<T>(List<T> data, string Heading = "", bool showSlno = false, params string[] ColumnsToTake)
            {
                return ExportExcel(ListToDataTable<T>(data), Heading, showSlno, ColumnsToTake);
            }

        }
    }
}