﻿using IEMQSImplementation;
using IEMQSImplementation.CBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQS.Models;
using IEMQS.Areas.NDE.Models;
using System.Globalization;
using static IEMQSImplementation.clsImplementationEnum;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Areas.WQ.Models;
using iTextSharp.text.pdf;
using System.Dynamic;
using System.ComponentModel;

namespace IEMQS.Areas.IPI.Controllers
{
    public class WelderPSlipController : clsBase
    {
        private readonly CBLEntitiesContext CBLDB;
        public WelderPSlipController()
        {
            CBLDB = new CBLEntitiesContext();
        }

        [SessionExpireFilter]
        // GET: IPI/WelderPSlip
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult WelderPSDetail(int Id = 0)
        {
            WPS025 objWPS025 = new WPS025();
            if (Id > 0)
            {
                objWPS025 = db.WPS025.Where(x => x.Id == Id).FirstOrDefault();
            }
            return View(objWPS025);
        }

        [SessionExpireFilter]
        public ActionResult Slips()
        {
            ViewBag.StartDate = DateTime.Now.AddDays(-6).ToString("dd/MM/yyyy");
            ViewBag.EndDate = DateTime.Now.ToString("dd/MM/yyyy");
            ViewBag.Tab = Request.QueryString["tab"] != null ? Convert.ToString(Request.QueryString["tab"]) : "";
            return View();
        }

        public ActionResult LTPCHtmlPSlip(int? id, string weldingProcess, string weldLayer, string type, string aWSClass, string fMSize, string JointType, string WPSNumber, int WPSRevNo, string APIRequestFrom = "")
        {

            ViewBag.Type = type;
            SP_RPT_WELDERPARAMETERSLIPFORLTPC_Result welderParameterSlipForLTPC = db.SP_RPT_WELDERPARAMETERSLIPFORLTPC(weldingProcess, weldLayer, type, id, aWSClass, fMSize, WPSNumber, WPSRevNo).FirstOrDefault();

            if (welderParameterSlipForLTPC == null)
            {
                welderParameterSlipForLTPC = new SP_RPT_WELDERPARAMETERSLIPFORLTPC_Result();
                welderParameterSlipForLTPC.WeldingProcess = weldingProcess;
            }          

            if (type == clsImplementationEnum.TypeOfTokenProcess.WPS.GetStringValue())
            {
                var objWPS025 = db.WPS025.FirstOrDefault(f => f.Id == id);
                if (objWPS025 != null)
                    ViewBag.QRCodeImgScr = GetQRText(objWPS025.RefTokenId);
            }
            else if (type == clsImplementationEnum.TypeOfTokenProcess.WPP.GetStringValue())
                ViewBag.QRCodeImgScr = GetQRText(id);

            return View(welderParameterSlipForLTPC);
        }
        //[SessionExpireFilter] // Temporary commented for testing on Mobile Application
        public ActionResult HtmlPSlip(int? id, string weldingProcess, string weldLayer, string type, string aWSClass, string fMSize, string JointType, string WPSNumber, int WPSRevNo, string APIRequestFrom = "")
        {
            if ((string.Equals(JointType, "Butt Joint", StringComparison.OrdinalIgnoreCase) || string.Equals(JointType, "Butt Joints", StringComparison.OrdinalIgnoreCase)) &&
                    (weldingProcess == "141(TIG)" || weldingProcess == "111(MMAW)" || weldingProcess == "121(SAW)" || weldingProcess == "131(MIG)" || weldingProcess == "135(MAG)"))
            {
                return RedirectToAction("LTPCHtmlPSlip", new { id = id, weldingProcess = weldingProcess, weldLayer = weldLayer, type = type, aWSClass = aWSClass, fMSize = fMSize, JointType = JointType, WPSNumber = WPSNumber, WPSRevNo = WPSRevNo, APIRequestFrom = APIRequestFrom });
            }
            else if ((string.Equals(JointType, "Overlay", StringComparison.OrdinalIgnoreCase)) &&
                    (weldingProcess == "141(TIG)" || weldingProcess == "111(MMAW)" || weldingProcess == "121(SAW)" || weldingProcess == "131(MIG)" || weldingProcess == "135(MAG)"))
            {
                return RedirectToAction("LTPCHtmlPSlip_Overlay", new { id = id, weldingProcess = weldingProcess, weldLayer = weldLayer, type = type, aWSClass = aWSClass, fMSize = fMSize, JointType = JointType, WPSNumber = WPSNumber, WPSRevNo = WPSRevNo, APIRequestFrom = APIRequestFrom });
            }

            SP_RPT_WELDERPARAMETERSLIP_Result data = db.SP_RPT_WELDERPARAMETERSLIP(weldingProcess, weldLayer, type, id, aWSClass, fMSize, WPSNumber, WPSRevNo).FirstOrDefault();

            if (data == null)
            {
                data = new SP_RPT_WELDERPARAMETERSLIP_Result();
                data.WeldingProcess = weldingProcess;
            }

            ViewBag.Type = type;
            if (!string.IsNullOrEmpty(weldingProcess))
            {
                List<CBL002HtmlSleep> CBL002HtmlSleepLst = db.Database.SqlQuery<CBL002HtmlSleep>("SP_CBL_GetDataForHtmlSlip @QualityProject={0},@WeldingProcess={1}", data.QualityProject, data.WeldingProcess).ToList();

                if (data.WeldingProcess == "SAW" || data.WeldingProcess == "ESW")
                {
                    var CBL002HtmlSleep = CBL002HtmlSleepLst.Select(s => new
                    {
                        SizeMM = s.SizeMM,
                        BatchNoWire = s.BatchNoWire,
                        BatchFlux = s.BatchFlux
                    });

                    List<ExpandoObject> joinData = new List<ExpandoObject>();

                    foreach (var item in CBL002HtmlSleep)
                    {
                        IDictionary<string, object> itemExpando = new ExpandoObject();
                        foreach (PropertyDescriptor property
                                 in
                                 TypeDescriptor.GetProperties(item.GetType()))
                        {
                            itemExpando.Add(property.Name, property.GetValue(item));
                        }
                        joinData.Add(itemExpando as ExpandoObject);
                    }

                    ViewBag.TypeSAWandESW = joinData;
                }
                else
                {
                    var CBL002HtmlSleep = CBL002HtmlSleepLst.Select(s => new
                    {
                        SizeMM = s.SizeMM,
                        BatchNo = s.BatchNo ?? ""
                    }).ToList();

                    List<ExpandoObject> joinData = new List<ExpandoObject>();
                    foreach (var item in CBL002HtmlSleep)
                    {
                        IDictionary<string, object> itemExpando = new ExpandoObject();
                        foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(item.GetType()))
                        {
                            itemExpando.Add(property.Name, property.GetValue(item));
                        }
                        joinData.Add(itemExpando as ExpandoObject);
                    }
                    ViewBag.NotContainsSAWandESW = joinData;
                }
            }

            if (string.Equals(JointType, "Overlay", StringComparison.OrdinalIgnoreCase))
            {
                ViewBag.JointType = JointType;
            }
            else if (string.Equals(JointType, "Buttering", StringComparison.OrdinalIgnoreCase))
            {
                ViewBag.JointType = JointType;
            }

            if (type == clsImplementationEnum.TypeOfTokenProcess.WPS.GetStringValue())
            {
                var objWPS025 = db.WPS025.FirstOrDefault(f => f.Id == id);
                if (objWPS025 != null)
                    ViewBag.QRCodeImgScr = GetQRText(objWPS025.RefTokenId);
            }
            else if (type == clsImplementationEnum.TypeOfTokenProcess.WPP.GetStringValue())
                ViewBag.QRCodeImgScr = GetQRText(id);

            if (APIRequestFrom == "Mobile")
            {
                return PartialView("_MobileHTMLPSlip", data);
            }
            else
            {
                return View(data);
            }
        }

        public ActionResult LTPCHtmlPSlip_Overlay(int? id, string weldingProcess, string weldLayer, string type, string aWSClass, string fMSize, string JointType, string WPSNumber, int WPSRevNo, string APIRequestFrom = "")
        {
            SP_RPT_WELDERPARAMETERSLIP_Result data = db.SP_RPT_WELDERPARAMETERSLIP(weldingProcess, weldLayer, type, id, aWSClass, fMSize, WPSNumber, WPSRevNo).FirstOrDefault();

            if (data == null)
            {
                data = new SP_RPT_WELDERPARAMETERSLIP_Result();
                data.WeldingProcess = weldingProcess;
            }

            ViewBag.Type = type;
            if (!string.IsNullOrEmpty(weldingProcess))
            {
                List<CBL002HtmlSleep> CBL002HtmlSleepLst = db.Database.SqlQuery<CBL002HtmlSleep>("SP_CBL_GetDataForHtmlSlip @QualityProject={0},@WeldingProcess={1}", data.QualityProject, data.WeldingProcess).ToList();

                if (data.WeldingProcess == "SAW" || data.WeldingProcess == "ESW")
                {
                    var CBL002HtmlSleep = CBL002HtmlSleepLst.Select(s => new
                    {
                        SizeMM = s.SizeMM,
                        BatchNoWire = s.BatchNoWire,
                        BatchFlux = s.BatchFlux
                    });

                    List<ExpandoObject> joinData = new List<ExpandoObject>();

                    foreach (var item in CBL002HtmlSleep)
                    {
                        IDictionary<string, object> itemExpando = new ExpandoObject();
                        foreach (PropertyDescriptor property
                                 in
                                 TypeDescriptor.GetProperties(item.GetType()))
                        {
                            itemExpando.Add(property.Name, property.GetValue(item));
                        }
                        joinData.Add(itemExpando as ExpandoObject);
                    }

                    ViewBag.TypeSAWandESW = joinData;
                }
                else
                {
                    var CBL002HtmlSleep = CBL002HtmlSleepLst.Select(s => new
                    {
                        SizeMM = s.SizeMM,
                        BatchNo = s.BatchNo ?? ""
                    }).ToList();

                    List<ExpandoObject> joinData = new List<ExpandoObject>();
                    foreach (var item in CBL002HtmlSleep)
                    {
                        IDictionary<string, object> itemExpando = new ExpandoObject();
                        foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(item.GetType()))
                        {
                            itemExpando.Add(property.Name, property.GetValue(item));
                        }
                        joinData.Add(itemExpando as ExpandoObject);
                    }
                    ViewBag.NotContainsSAWandESW = joinData;
                }
            }

            ViewBag.JointType = JointType;
           
            if (type == clsImplementationEnum.TypeOfTokenProcess.WPS.GetStringValue())
            {
                var objWPS025 = db.WPS025.FirstOrDefault(f => f.Id == id);
                if (objWPS025 != null)
                    ViewBag.QRCodeImgScr = GetQRText(objWPS025.RefTokenId);
            }
            else if (type == clsImplementationEnum.TypeOfTokenProcess.WPP.GetStringValue())
                ViewBag.QRCodeImgScr = GetQRText(id);

            if (APIRequestFrom == "Mobile")
            {
                return PartialView("_MobileHTMLPSlip", data);
            }
            else
            {
                return View(data);
            }
        }

        public string GetQRText(int? id)
        {
            var objWPS026 = db.WPS026.FirstOrDefault(w => w.Id == id);
            if (objWPS026 != null)
            {
                var QRCodeURL = db.CONFIG.Where(w => w.Key == "QRCodeURL").Select(s => s.Value).FirstOrDefault();
                return QRCodeURL + objWPS026.EU + "~" + objWPS026.WelderPSNO + "~" + (objWPS026.CreatedOn.HasValue ? objWPS026.CreatedOn.Value.ToString() : "") + "~" + objWPS026.TokenNumber;
            }
            return "";
        }

        [SessionExpireFilter]
        public ActionResult ParameterSlipDetail(int Id = 0)
        {
            WPS025 objWPS025 = new WPS025();
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();

            if (Id > 0)
            {
                objWPS025 = db.WPS025.Where(x => x.Id == Id).FirstOrDefault();
                var locaDescription = (from a in db.COM002
                                       where a.t_dimx == objWPS025.Location
                                       select a.t_desc).FirstOrDefault();
                ViewBag.Location = objWPS025.Location + "-" + locaDescription;
            }
            else
            {
                objWPS025.ValidUpto = DateTime.Now;
            }
            return View(objWPS025);
        }

        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult ConsumableSlipDetail(int Id = 0)
        {
            WPS026 objWPS026 = new WPS026();
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            ViewBag.userLocation = objClsLoginInfo.Location;
            ViewBag.TokenProcess = clsImplementationEnum.GetTypeOfTokenProcess().ToArray();
            if (Id > 0)
            {
                objWPS026 = db.WPS026.Where(x => x.Id == Id).FirstOrDefault();
                var locaDescription = (from a in db.WQR008
                                       where a.Location == objWPS026.Location
                                       select a.Description).FirstOrDefault();
                ViewBag.Location = locaDescription;
                var objWPS025 = db.WPS025.Where(i => i.RefTokenId == Id).FirstOrDefault();
                if (objWPS025 != null)
                {
                    ViewBag.ParameterSpliId = objWPS025.Id;
                    ViewBag.JointType = objWPS025.JointType;
                }
                else
                {
                    if (!string.IsNullOrEmpty(objWPS026.WPPNumber))
                    {
                        ViewBag.JointType = db.WPP010_Log.Where(i => i.WPPNumber == objWPS026.WPPNumber).Select(o => o.JointType).FirstOrDefault();
                    }
                    else
                    {
                        ViewBag.JointType = db.WPS010.Where(i => i.WPSNumber == objWPS026.WPSNumber).Select(o => o.Jointtype).FirstOrDefault();
                    }
                }
                ViewBag.SeamCategory = db.QMS012.Where(x => x.SeamNo == objWPS026.SeamNo && x.QualityProject == objWPS026.QualityProject).Select(x => x.SeamCategory).FirstOrDefault();
                ViewBag.Location = db.WQR008.Where(i => i.Location == objWPS026.Location).FirstOrDefault().Description;

                var objCOM012Consume = (from com12 in db.COM012
                                        join com11 in db.COM011 on com12.t_item.TrimEnd() equals com11.t_sitm.TrimEnd()
                                        where com11.t_mitm.ToUpper().TrimStart() == (objWPS026.Project + "-" + objWPS026.Location.Substring(0, 1) + "-WMR") && com12.t_dscc == objWPS026.FillerMetalSize
                                        && ((objWPS026.ConsumableItem != "" ? (com12.t_item.TrimEnd().Contains(objWPS026.ConsumableItem)) : true) || (objWPS026.ConsumableItem != "" ? (com12.t_dsca.Contains(objWPS026.ConsumableItem)) : true))
                                        select new { text = com12.t_item.TrimEnd() + "#" + com12.t_dsca }
                                ).FirstOrDefault();

                if (!string.IsNullOrWhiteSpace(objWPS026.FluxItem))
                {
                    var objCOM012Flux = (from com12 in db.COM012
                                         join com11 in db.COM011 on com12.t_item.TrimEnd() equals com11.t_sitm.TrimEnd()
                                         where com11.t_mitm.ToUpper().TrimStart() == (objWPS026.Project + "-" + objWPS026.Location.Substring(0, 1) + "-WMR") && com12.t_dscb == objWPS026.FluxAWSClass
                                         && ((objWPS026.FluxItem != "" ? (com12.t_item.TrimEnd().Contains(objWPS026.FluxItem)) : true) || (objWPS026.FluxItem != "" ? (com12.t_dsca.Contains(objWPS026.FluxItem)) : true))
                                         select new { text = com12.t_item.TrimEnd() + "#" + com12.t_dsca }
                                ).FirstOrDefault();

                    ViewBag.FluxItem = objCOM012Flux != null ? objCOM012Flux.text : objWPS026.FluxItem;
                }
                else
                { ViewBag.FluxItem = objWPS026.FluxItem; }

                var WelderName = Manager.GetWelderNameByWelderPSNo(objWPS026.WelderPSNO);
                ViewBag.ConsumableItem = objCOM012Consume != null ? objCOM012Consume.text : objWPS026.ConsumableItem;
                ViewBag.WelderPSNO = objWPS026.WelderPSNO + "-" + WelderName + (!string.IsNullOrEmpty(objWPS026.WelderStamp) ? "-" + objWPS026.WelderStamp : "");
                ViewBag.WelderStamp = (!string.IsNullOrEmpty(objWPS026.WelderStamp) ? objWPS026.WelderStamp : "");
                //ViewBag.WelderPSNO = objWPS026.WelderPSNO + "-" + db.COM003.Where(i => i.t_psno == objWPS026.WelderPSNO).FirstOrDefault().t_name + (StampNo != string.Empty ? "-" + StampNo : "");
                ViewBag.Fluxtradename = db.WPS011_Log.Where(i => i.WPSNumber == objWPS026.WPSNumber && i.WPSRevNo == objWPS026.WPSRevNo && i.WeldLayer == objWPS026.WeldLayer && i.Location.Trim() == objWPS026.Location.Trim() && i.AWSClass.Trim() == objWPS026.AWSClass.Trim()).Select(i => i.FluxTradeName).FirstOrDefault();
                var objToken = db.SP_IPI_GET_TOKEN_LIST(0, 0, "", "datediff(hh, CreatedOn, getdate()) < 12 and ranking = 1 and Id=" + objWPS026.Id, "").FirstOrDefault();

                ViewBag.IsActive = "false";
                if (objToken != null)
                {
                    ViewBag.IsActive = "true";
                }
            }
            else
            {
                objWPS026.ValidUpto = DateTime.Now;
                ViewBag.WelderStamp = "";
            }

            ViewBag.Tab = Request.QueryString["from"] != null ? Convert.ToString(Request.QueryString["from"]) : "";

            return PartialView("ConsumableSlipDetail", objWPS026);
        }

        [HttpPost]
        public ActionResult GetEULocation(string term)
        {
            List<CategoryData> lstLocation = null;
            if (!string.IsNullOrEmpty(term))
            {

                lstLocation = (from w8 in db.WQR008
                               where (w8.Location.Contains(term) || w8.Description.Contains(term))
                               select new CategoryData { Code = w8.EU, Value = w8.Location, Description = w8.Location + "-" + w8.Description }
                                ).ToList();
            }
            else
            {
                lstLocation = (from w8 in db.WQR008
                               select new CategoryData { Code = w8.EU, Value = w8.Location, Description = w8.Location + "-" + w8.Description }
                              ).ToList();
            }

            return Json(lstLocation, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetQProjectResult(string term, string location)
        {
            var SeamList = db.WPS025.Where(i => i.Location == location && (term != "" ? (i.QualityProject.Contains(term)) : true)).Select(i => new { projectCode = i.QualityProject, projectDescription = i.QualityProject }).Distinct().ToList();

            return Json(SeamList, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetSeamResult(string qualityProject, string location)
        {
            List<clsTask> SeamList = new List<clsTask>();
            var list = db.WPS025.Where(i => i.QualityProject == qualityProject && i.Location == location).Distinct().ToList();

            SeamList = list.Where(a => Manager.IsSeamApplicableForPrintSlip(a.Project, a.QualityProject, a.BU, a.Location, a.SeamNo)).Select(s => new clsTask { id = s.SeamNo, text = s.SeamNo }).ToList();

            return Json(SeamList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetWPSNoResult(string term, string qualityProject, string seam, string location)
        {
            string[] arraySeam = seam.Split(',').ToArray();
            //+ "#" + String.Join(", ", i.WeldingProcess)
            var WProcessList = db.WPS025.Where(i => i.QualityProject == qualityProject && arraySeam.Contains(i.SeamNo)).Select(i => new { i.WeldingProcess }).Distinct().ToList();
            string wprocess = String.Join("+", WProcessList.Select(i => i.WeldingProcess));
            var WPSNoList = db.WPS025.Where(i => i.QualityProject == qualityProject
            && i.Location == location
            && arraySeam.Contains(i.SeamNo)
            && (term != "" ? (i.WPSNumber.Contains(term)) : true)).Select(i => new { id = i.WPSNumber + "#" + i.WPSRevNo, text = (i.WPSNumber + "#" + wprocess) }).Distinct().ToList();


            var seamCount =
                from i in db.WPS025
                where i.QualityProject == qualityProject
                        && i.Location == location
                        && arraySeam.Contains(i.SeamNo)
                        && (term != "" ? (i.WPSNumber.Contains(term)) : true)
                group i by i.WPSNumber into seamCountGroup

                select new
                {
                    WPSNumber = seamCountGroup.Key,
                    Count = seamCountGroup.Select(l => l.SeamNo).Distinct().Count(),
                    MainSeamCount = arraySeam.Count(),
                };

            if (seamCount.Where(x => x.MainSeamCount != x.Count).Any())
            {
                return Json(new
                {
                    WPSNoList = WPSNoList,
                    isValid = false.ToString(),
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new
                {
                    WPSNoList = WPSNoList,
                    isValid = true.ToString(),
                }, JsonRequestBehavior.AllowGet);
            }
            //return Json(WPSNoList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetWProcessResult(string term, string qualityProject, string seam, string wpsno, int wpsrevno, string location)
        {
            string[] arraySeam = seam.Split(',').ToArray();
            //int revno = int.Parse(wpsrevno);
            var WProcessList = db.WPS025.Where(i => i.QualityProject == qualityProject && i.Location == location && arraySeam.Contains(i.SeamNo) && i.WPSNumber == wpsno && i.WPSRevNo == wpsrevno && (term != "" ? (i.WeldingProcess.Contains(term)) : true)).Select(i => new { id = i.WeldingProcess, text = i.WeldingProcess }).Distinct().ToList();

            return Json(WProcessList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetWelderResult(string term, string qualityProject, string seam, string wpsno, int wpsrevno, string wprocess, string location)
        {
            string[] arraySeam = seam.Split(',').ToArray();
            var WelderList = (from i in db.WPS025
                              join com in db.COM003 on i.WelderPSNO equals com.t_psno
                              where i.QualityProject == qualityProject && i.Location == location && arraySeam.Contains(i.SeamNo) && i.WPSNumber == wpsno && i.WPSRevNo == wpsrevno && i.WeldingProcess == wprocess
                              && ((term != "" ? (i.WelderPSNO.Contains(term)) : true) || (term != "" ? (com.t_name.Contains(term)) : true))
                              select new { id = i.WelderPSNO, text = i.WelderPSNO + " - " + com.t_name }
                                 ).Distinct().ToList();

            //var WelderList = db.WPS025.Where(i => i.QualityProject == qualityProject && i.Location == objClsLoginInfo.Location && arraySeam.Contains(i.SeamNo) && i.WPSNumber == wpsno && i.WPSRevNo == wpsrevno && i.WeldingProcess == wprocess).Select(i => new { id = i.WelderPSNO, text = i.WelderPSNO }).Distinct().ToList();

            return Json(WelderList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetWLayerResult(string term, string wpsno, int wpsrevno, string location)
        {

            var WLayerList = db.WPS025.Where(i => i.WPSNumber == wpsno && i.WPSRevNo == wpsrevno && i.Location == location && (term != "" ? (i.WeldLayer.Contains(term)) : true)).Select(i => new { id = i.WeldLayer, text = i.WeldLayer }).Distinct().ToList();

            return Json(WLayerList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetAWSResult(string term, string wpsno, int wpsrevno, string layer, string location, string tokenfor, string process)
        {
            List<clsTask> AWSList = new List<clsTask>();
            if (tokenfor == clsImplementationEnum.TypeOfTokenProcess.WPP.GetStringValue())
            {
                AWSList = db.WPP011_Log.Where(i => i.WPPNumber == wpsno && i.WPPRevNo == wpsrevno
                                           && i.WSNo == layer
                                           //&& i.Location.Trim() == objClsLoginInfo.Location.Trim()
                                           && (term != "" ? (i.AWSClass.Contains(term)) : true))
                                           .Select(i => new clsTask { id = i.AWSClass + "#" + i.FluxName, text = i.AWSClass }).Distinct().ToList();
            }
            else
            {
                AWSList = db.WPS011_Log.Where(i => i.WPSNumber == wpsno && i.WPSRevNo == wpsrevno && i.WeldingProcess == process && i.WeldLayer == layer
                //&& i.Location.Trim() == objClsLoginInfo.Location.Trim() 
                && (term != "" ? (i.AWSClass.Contains(term)) : true)).Select(i => new clsTask { id = i.AWSClass + "#" + i.FluxTradeName, text = i.AWSClass }).Distinct().ToList();
            }
            return Json(AWSList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSizeResult(string wpsno, int wpsrevno, string layer, string aws, string location, string tokenfor, string APIRequestFrom = "", string UserLocation = "")
        {
            UserLocation = APIRequestFrom == "Mobile" ? UserLocation : objClsLoginInfo.Location.Trim();
            List<clsTask> SizeList = new List<clsTask>();
            if (tokenfor == clsImplementationEnum.TypeOfTokenProcess.WPP.GetStringValue())
            {
                SizeList = db.WPP011_Log.Where(i => i.WPPNumber == wpsno && i.WPPRevNo == wpsrevno
                                                    && i.WSNo == layer
                                                 && i.AWSClass == aws
                                                 //&& i.Location.Trim() == UserLocation
                                                 )
                                                 //&& (term != "" ? (i.SizeofFillerMetal.Contains(term)) : true))
                                                 .Select(i => new clsTask { id = i.SizeofFillerMetal, text = i.SizeofFillerMetal }).Distinct().ToList();
            }
            else
            {
                SizeList = db.WPS011_Log.Where(i => i.WPSNumber == wpsno && i.WPSRevNo == wpsrevno && i.WeldLayer == layer && i.AWSClass == aws
                //&& i.Location.Trim() == UserLocation
                //&& (term != "" ? (i.FillerMetalSize.Contains(term)) : true)
                ).Select(i => new clsTask { id = i.FillerMetalSize, text = i.FillerMetalSize }).Distinct().ToList();
            }
            string strSizeList = string.Empty;
            if (SizeList.Count > 0)
            {
                strSizeList = string.Join(",", SizeList.Select(x => x.id));
            }

            if (APIRequestFrom == "Mobile")
            {
                return Json(new
                {
                    Key = true,
                    Value = "Success",
                    data = strSizeList
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(strSizeList, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetCItemResult(string term, string plWMR, string size, string aws)
        {

            var CItemList = (from com12 in db.COM012
                             join com11 in db.COM011 on com12.t_item.TrimEnd() equals com11.t_sitm.TrimEnd()
                             where com11.t_mitm.ToUpper().TrimStart() == plWMR.ToUpper() && com12.t_dscc == size && com12.t_dscb == aws
                             && ((term != "" ? (com12.t_item.TrimEnd().Contains(term)) : true) || (term != "" ? (com12.t_dsca.Contains(term)) : true))
                             select new { id = com12.t_item.TrimEnd(), text = com12.t_item.TrimEnd() + "#" + com12.t_dsca }
                                 ).Distinct().ToList();

            return Json(CItemList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetFAWSResult(string term, string ftrade, string location)
        {

            var FAWSList = db.WPS001.Where(i => i.BrandName == ftrade && i.Location.Trim() == objClsLoginInfo.Location.Trim() && (term != "" ? (i.AWSClass.Contains(term)) : true)).Select(i => new { id = i.AWSClass, text = i.AWSClass }).Distinct().ToList();

            return Json(FAWSList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetFItemResult(string term, string plWMR, string faws, string size)
        {

            var FItemList = (from com12 in db.COM012
                             join com11 in db.COM011 on com12.t_item.TrimEnd() equals com11.t_sitm.TrimEnd()
                             where com11.t_mitm.ToUpper().TrimStart() == plWMR.ToUpper() && com12.t_dscb == faws //&& com12.t_dscc == size
                             && ((term != "" ? (com12.t_item.TrimEnd().Contains(term)) : true) || (term != "" ? (com12.t_dsca.Contains(term)) : true))
                             select new { id = com12.t_item.TrimEnd(), text = com12.t_item.TrimEnd() + "#" + com12.t_dsca }
                                 ).Distinct().ToList();

            return Json(FItemList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveConsumable(FormCollection fc, string APIRequestFrom = "", string UserName = "", string UserLocation = "")
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int unicodeofA = 65;
                bool IsSaveOnly = fc["IsSaveOnly"] + "" == "True";
                int tokenLimit24Hour = 26;
                string weldingprocess = string.Empty;
                string welderpsno = string.Empty;
                string wpsnumber = string.Empty;
                int? wpsrevno = 0;
                string welder = (fc["txtWelder"].ToString().Split('-')[0]).ToString().Trim();
                string jointtype = fc["txtWPSJointType"].ToString().Trim();
                string welderstamp = fc["hdnWelderStamp"].ToString().Trim();
                string weldingtype = fc["hdWeldingType"].ToString().Trim();

                /*var lstTypeQualified = Manager.GetSubCatagorywithoutBULocation("Welding Type");
                string TypeQualifiedCode = lstTypeQualified.Where(w => w.CategoryDescription.Trim().ToLower() == weldingtype.Trim().ToLower() || w.Value.Trim().ToLower() == weldingtype.Trim().ToLower()).Select(s => s.Value).FirstOrDefault();
                if (!string.IsNullOrEmpty(TypeQualifiedCode))
                    weldingtype = TypeQualifiedCode;*/

                DateTime fromDateLimitforDay = DateTime.Now.AddHours(-24);
                DateTime fromDateLimitforSameData = DateTime.Now.AddHours(-12);
                if (fromDateLimitforDay.Date != DateTime.Now.Date)
                {
                    fromDateLimitforDay = DateTime.Now.Date;
                }
                int totalToken = db.WPS026.Where(x => x.WelderPSNO == welder && x.CreatedOn >= fromDateLimitforDay).Count();

                if (totalToken >= tokenLimit24Hour)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.TokemMessages.OverLimit.ToString();
                }
                else
                {
                    string[] seamArray = fc["txtSeam"].ToString().Split(',').ToArray();
                    string customMessage = string.Empty;
                    string newTokens = string.Empty;
                    string existTokens = string.Empty;
                    for (int i = 0; i < seamArray.Length; i++)
                    {

                        WPS026 objWPS026 = new WPS026();
                        var Id = Convert.ToInt32("0" + fc["Id"]);
                        if (Id > 0)
                        {
                            objWPS026 = db.WPS026.FirstOrDefault(f => f.Id == Id);
                            objWPS026.TokenNumber = (IsSaveOnly ? "" : ((char)(unicodeofA + totalToken + i)).ToString());
                            objWPS026.CreatedOn = (IsSaveOnly ? objWPS026.CreatedOn : DateTime.Now);
                            objWPS026.EditedBy = UserName;
                            objWPS026.EditedOn = DateTime.Now;
                        }

                        objWPS026.QualityProject = fc["hdQP"].ToString();
                        objWPS026.Project = fc["hdQP"].ToString().Split('/')[0];
                        objWPS026.BU = fc["txtBU"].ToString().Split('-')[0].Trim();
                        objWPS026.Location = fc["Location"].ToString().Trim();
                        objWPS026.EU = fc["txtEU"].ToString();
                        objWPS026.TokenNumber = (IsSaveOnly ? "" : ((char)(unicodeofA + totalToken + i)).ToString());
                        objWPS026.SeamNo = seamArray[i];
                        if (fc["TokenProcess"].ToString() == clsImplementationEnum.TypeOfTokenProcess.WPP.GetStringValue())
                        {
                            objWPS026.WPPNumber = fc["txtWPSNo"].ToString();
                        }
                        else
                        {
                            objWPS026.WPSNumber = fc["txtWPSNo"].ToString();
                        }
                        objWPS026.TokenFor = fc["TokenProcess"].ToString();
                        objWPS026.WeldingProcess = fc["txtWProcess"].ToString();
                        objWPS026.WPSRevNo = Convert.ToInt32(fc["txtWPSRevNo"]);
                        objWPS026.WelderPSNO = welder;
                        objWPS026.WeldingType = weldingtype;
                        objWPS026.WelderStamp = welderstamp;
                        objWPS026.WeldLayer = fc["hdWLayer"].ToString();
                        objWPS026.AWSClass = fc["hfAWS"].ToString();
                        objWPS026.FillerMetalSize = fc["txtSize"].ToString();
                        objWPS026.FluxAWSClass = fc["txtFAWS"] != null ? fc["txtFAWS"].ToString() : string.Empty;
                        objWPS026.ConsumableItem = fc["hfCItem"].ToString();
                        objWPS026.FluxItem = fc["hfFItem"] != null ? fc["hfFItem"].ToString() : string.Empty;
                        objWPS026.ConsumableItemCategory = db.COM013.Where(x => x.t_item.TrimEnd() == objWPS026.ConsumableItem).Select(x => x.t_csgp.TrimEnd()).FirstOrDefault();
                        objWPS026.FluxItemCategory = db.COM013.Where(x => x.t_item.TrimEnd() == objWPS026.FluxItem).Select(x => x.t_csgp.TrimEnd()).FirstOrDefault();
                        objWPS026.CreatedBy = UserName;
                        objWPS026.CreatedOn = DateTime.Now;
                        objWPS026.UserLocation = APIRequestFrom == "Mobile" ? UserLocation : objClsLoginInfo.Location.Trim();
                        objWPS026.IdenticalSeamNo = Convert.ToString(fc["IdenticalSeamNo"]);
                        if (IsSaveOnly)
                        {
                            //& x.CreatedOn >= fromDateLimitforSameData);//token will not re-gererate for same record till 12 hour 
                            if (!IsTokenAlreadyExist(objWPS026))
                            {
                                if (Id == 0)
                                {
                                    objWPS026.EditedBy = null;
                                    objWPS026.EditedOn = null;
                                    db.WPS026.Add(objWPS026);
                                }
                                db.SaveChanges();
                                objResponseMsg.Id = objWPS026.Id;
                                objResponseMsg.appdesc = objWPS026.TokenNumber;
                            }
                            else
                            {
                                Manager.ConcatString(ref existTokens, objWPS026.SeamNo);
                            }
                        }
                        else
                        {
                            #region
                            #endregion
                            //& x.CreatedOn >= fromDateLimitforSameData);//token will not re-gererate for same record till 12 hour 
                            if (!IsTokenAlreadyExist(objWPS026))
                            {
                                if (Id == 0)
                                {
                                    db.WPS026.Add(objWPS026);
                                }
                                db.SaveChanges();
                                weldingprocess = objWPS026.WeldingProcess;
                                welderpsno = objWPS026.WelderPSNO;
                                wpsnumber = objWPS026.WPSNumber;
                                wpsrevno = objWPS026.WPSRevNo;
                                if (string.IsNullOrWhiteSpace(newTokens))
                                {
                                    newTokens = "'" + objWPS026.TokenNumber + "'";
                                }
                                else
                                {
                                    newTokens = newTokens + "," + "'" + objWPS026.TokenNumber + "'";
                                }

                                ////Observation 15417
                                //var objWPS025 = new WPS025();
                                //objWPS025.BU = objWPS026.BU;
                                //objWPS025.JointType = fc["txtWPSJointType"].ToString().Trim();
                                //objWPS025.Location = objWPS026.Location;
                                ////objWPS025.PrintedBy = objClsLoginInfo.UserName;
                                ////objWPS025.PrintedOn = DateTime.Now;
                                //objWPS025.Project = objWPS026.Project;
                                //objWPS025.QualityProject = objWPS026.QualityProject;
                                //objWPS025.RefTokenId = objWPS026.Id;
                                //objWPS025.SeamNo = objWPS026.SeamNo;
                                ////objWPS025.ValidUpto = objWPS026.BU;
                                //objWPS025.WelderPSNO = objWPS026.WelderPSNO;
                                //objWPS025.WelderStamp = Manager.GetWelderStamp(objWPS026.WelderPSNO);
                                //objWPS025.WeldingProcess = objWPS026.WeldingProcess;
                                //objWPS025.WeldLayer = objWPS026.WeldLayer;
                                //objWPS025.WPSNumber = objWPS026.WPSNumber;
                                //objWPS025.WPSRevNo = objWPS026.WPSRevNo;
                                //db.WPS025.Add(objWPS025);
                                //db.SaveChanges();

                                objResponseMsg.Id = objWPS026.Id;
                                objResponseMsg.appdesc = objWPS026.TokenNumber;
                            }
                            else
                            {
                                Manager.ConcatString(ref existTokens, objWPS026.SeamNo);
                            }
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(existTokens))
                    {
                        objResponseMsg.Key = false;
                        customMessage = customMessage + "Token for Seam No " + existTokens + " is not generated as it is already present for this combination within last 12 hours";
                        objResponseMsg.Value = customMessage;
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        if (IsSaveOnly)
                            customMessage = "Token has been Saved\n";
                        else if (!string.IsNullOrWhiteSpace(newTokens))
                        {
                            UpdateCertificates(weldingprocess, welderpsno, jointtype, welderstamp, wpsnumber, wpsrevno, weldingtype);
                            customMessage = "Token No(s) " + newTokens + " has been generated\n";
                        }
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = customMessage;
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public void UpdateCertificates(string weldprocess, string welderpsno, string jointtype, string welderstamp, string wpsnumber, int? wpsrevno, string weldingtype)
        {
            /*var weldingtype = string.Empty;
            var objWPS010_Log = db.WPS010_Log.Where(i => i.WPSNumber == wpsnumber && i.WPSRevNo == wpsrevno.Value).FirstOrDefault();
            if (objWPS010_Log != null)
            {
                if (weldprocess == objWPS010_Log.WeldingProcess1)
                {
                    weldingtype = objWPS010_Log.WeldingType1;
                }
                else if (weldprocess == objWPS010_Log.WeldingProcess2)
                {
                    weldingtype = objWPS010_Log.WeldingType2;
                }
                else if (weldprocess == objWPS010_Log.WeldingProcess3)
                {
                    weldingtype = objWPS010_Log.WeldingType3;
                }
                else if (weldprocess == objWPS010_Log.WeldingProcess4)
                {
                    weldingtype = objWPS010_Log.WeldingType4;
                }*/

            if (!string.IsNullOrEmpty(weldingtype))
            {
                if (jointtype.ToLower() == "groove/fillet" || jointtype.ToLower() == "overlay")
                {
                    weldprocess = GetSameWeldingProcessByWeldingProcess(weldprocess);
                    var splitWeldingProcess = weldprocess.Split(',');
                    var wpq = db.WML001.Where(x => splitWeldingProcess.Contains(x.WeldingProcessQualified) && x.Welder == welderpsno && x.WelderStampNo == welderstamp && x.TypeQualified == weldingtype).ToList();
                    if (wpq.Count > 0)
                    {
                        wpq.ForEach(x =>
                        {
                            x.LastWeldDate = DateTime.Now;
                            x.ValueUpto = DateTime.Now.AddDays(180);
                        });
                    }

                    var wopq = db.WML002.Where(x => (splitWeldingProcess.Contains(x.WeldingProcessMachineQualified) || splitWeldingProcess.Contains(x.WeldingProcessEsQualified)) && x.WelderName == welderpsno && x.WelderStampNo == welderstamp && x.TypeOfWeldingAutomaticQualified == weldingtype).ToList();
                    if (wopq.Count > 0)
                    {
                        wopq.ForEach(x =>
                        {
                            x.LastWeldDate = DateTime.Now;
                            x.ValueUpto = DateTime.Now.AddDays(180);
                        });
                    }
                    db.SaveChanges();
                }
                else if (jointtype.ToLower() == "t#ts")
                {
                    weldprocess = GetSameWeldingProcessByWeldingProcess(weldprocess);
                    var splitWeldingProcess = weldprocess.Split(',');
                    var tts = db.WML003.Where(x => splitWeldingProcess.Contains(x.WeldingProcessEsQualified) && x.WelderName == welderpsno && x.WelderStampNo == welderstamp && x.WeldingTypeQualified == weldingtype).ToList();
                    if (tts.Count > 0)
                    {
                        tts.ForEach(x =>
                        {
                            x.LastWeldDate = DateTime.Now;
                            x.ValueUpto = DateTime.Now.AddDays(180);
                        });
                    }
                    db.SaveChanges();
                }
            }
            // }
        }
        public bool IsTokenAlreadyExist(WPS026 objWPS026)
        {
            var objToken = db.SP_IPI_GET_TOKEN_LIST(0, 0, "", "datediff(hh, CreatedOn, getdate()) < 12 and ranking = 1 and Welder ='" + objWPS026.WelderPSNO + "' and WeldingProcessCode ='" + objWPS026.WeldingProcess + "' and BUCode ='" + objWPS026.BU + "' and LocationCode='" + objWPS026.Location + "' and ProjectCode='" + objWPS026.Project + "'", "").ToList();
            var isExist = objToken.Where(x => x.QualityProject == objWPS026.QualityProject &&
                                             //x.Project == objWPS026.Project &&
                                             //x.BU == objWPS026.BU &&
                                             //x.Location == objWPS026.Location &&
                                             //x.EU == objWPS026.EU &&
                                             (!string.IsNullOrWhiteSpace(x.TokenNumber)) &&
                                             x.SeamNo == objWPS026.SeamNo &&
                                             x.WPSNumber == objWPS026.WPSNumber &&
                                             //x.WeldingProcess == objWPS026.WeldingProcess &&
                                             x.WPSRevNo == objWPS026.WPSRevNo &&
                                             // x.WelderPSNO == objWPS026.WelderPSNO &&
                                             x.WeldLayer == objWPS026.WeldLayer &&
                                             x.AWSClass == objWPS026.AWSClass &&
                                             x.FillerMetalSize == objWPS026.FillerMetalSize &&
                                             x.FluxAWSClass == objWPS026.FluxAWSClass &&
                                             x.ConsumableItem == objWPS026.ConsumableItem &&
                                             x.FluxItem == objWPS026.FluxItem).FirstOrDefault();

            return isExist != null;
        }
        [HttpPost]
        public ActionResult CopyConsumableSlip(int id)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            WPS026 objSrcWPS026 = new WPS026();
            WPS026 objDestWPS026 = new WPS026();
            var Msg = string.Empty;
            try
            {
                string currentUser = objClsLoginInfo.UserName;
                var currentLoc = objClsLoginInfo.Location;
                if (id > 0)
                {
                    objSrcWPS026 = db.WPS026.Where(i => i.Id == id).FirstOrDefault();
                }
                var strApproved = CommonStatus.Approved.GetStringValue();

                var seams = (from a in db.SWP010
                             where a.QualityProject == objSrcWPS026.QualityProject && a.Location.Trim() == objSrcWPS026.Location
                             && a.Project == objSrcWPS026.Project && a.BU == objSrcWPS026.BU && a.Status == strApproved
                             && a.SeamNo == objSrcWPS026.SeamNo
                             select a).Distinct().ToList();

                if (seams.Any(a => Manager.IsSeamApplicableForPrintSlip(objSrcWPS026.Project, objSrcWPS026.QualityProject, objSrcWPS026.BU, objSrcWPS026.Location, objSrcWPS026.SeamNo)))
                //if (Manager.IsSeamApplicableForPrintSlip(objSrcWPS026.Project, objSrcWPS026.QualityProject, objSrcWPS026.BU, objSrcWPS026.Location, objSrcWPS026.SeamNo))
                {
                    var JointType = "";
                    if (string.IsNullOrEmpty(objSrcWPS026.WPSNumber))
                    {
                        Msg = "WPSNumber Not Maintain For Copy Token!";
                    }
                    if (objSrcWPS026.TokenFor == clsImplementationEnum.TypeOfTokenProcess.WPP.GetStringValue())
                    {
                        JointType = db.WPP010_Log.Where(i => i.WPPNumber == objSrcWPS026.WPSNumber).Select(o => o.JointType).FirstOrDefault();
                    }
                    else
                    {
                        JointType = db.WPS010.Where(i => i.WPSNumber == objSrcWPS026.WPSNumber).Select(o => o.Jointtype).FirstOrDefault();
                    }

                    var objWelderDtl = db.SP_GEN_GET_WELDER_DETAILS_BY_JOINTTYPE(JointType, objSrcWPS026.WelderPSNO, objSrcWPS026.WeldingProcess, objSrcWPS026.Location).ToList();
                    if (objWelderDtl.Count == 0)
                    {
                        Msg = "Welder Is Not Qualified For Copy Token!";
                    }
                }
                else
                {
                    Msg = "Seam Is Not Applicable For Copy Token!";
                }

                #region Copy Consumable Slip
                if (!string.IsNullOrEmpty(Msg))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = Msg;
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    objDestWPS026 = objSrcWPS026;
                    objDestWPS026.TokenNumber = " ";
                    objDestWPS026.CreatedBy = currentUser;
                    objDestWPS026.CreatedOn = DateTime.Now;
                    objDestWPS026.PrintedBy = null;
                    objDestWPS026.PrintedOn = null;
                    objDestWPS026.ValidUpto = null;
                    objDestWPS026.EditedBy = null;
                    objDestWPS026.EditedOn = null;
                    if (!IsTokenAlreadyExist(objDestWPS026))
                    {
                        db.WPS026.Add(objDestWPS026);
                        db.SaveChanges();
                    }
                    else
                    {
                        var existTokens = string.Empty;
                        Manager.ConcatString(ref existTokens, objDestWPS026.SeamNo);
                        Msg = "Token for Seam No " + existTokens + " is not generated as it is already present for this combination within last 12 hours";
                    }
                    #endregion
                    if (!string.IsNullOrEmpty(Msg))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = Msg;
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objResponseMsg.HeaderId = objDestWPS026.Id;
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.CopyRecord;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult getDropdownValue()
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();

            List<clsTask> list = new List<clsTask>();
            list = db.SWP010.Select(s => s.QualityProject).Distinct().Select(x => new clsTask { id = x.ToString(), text = x.ToString() }).ToList();

            //list = (from a in db.SWP010
            //        select new clsTask
            //        {
            //            id = a.QualityProject,
            //            text = a.QualityProject
            //        }).Distinct().ToList();

            objResponseMsg.lstWeldingProcess = list;

            List<clsTask> list1 = new List<clsTask>();
            list1 = (from a in db.SWP010
                     select new clsTask
                     {
                         id = a.SeamNo,
                         text = a.SeamNo
                     }).Distinct().ToList();

            objResponseMsg.lstSeam = list1;
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult getQProjectChange(string term, string QualityProject, string Location, string tokenfor)
        {
            Location = objClsLoginInfo.Location.Trim();
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            var strApproved = CommonStatus.Approved.GetStringValue();

            var objSWP010 = (from a in db.SWP010
                             where a.QualityProject == QualityProject
                             select new { a.Project, a.BU }).FirstOrDefault();
            if (objSWP010 != null)
            {
                var PrjectDesc = (from a in db.COM001
                                  where a.t_cprj == objSWP010.Project
                                  select a.t_dsca).FirstOrDefault();

                //var BU = (from a in db.SWP010
                //          where a.QualityProject == QualityProject
                //          select a.BU).FirstOrDefault();

                var BUDesc = (from a in db.COM002
                              where a.t_dimx == objSWP010.BU
                              select a.t_desc).FirstOrDefault();

                objResponseMsg.Project = objSWP010.Project + "-" + PrjectDesc;
                objResponseMsg.BU = objSWP010.BU + "-" + BUDesc;
            }
            string approved = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
            List<clsTask> list1 = new List<clsTask>();

            //var Location = objClsLoginInfo.Location;
            if (tokenfor == clsImplementationEnum.TypeOfTokenProcess.WPS.GetStringValue())
            {
                var seams = (from a in db.SWP010
                             where a.QualityProject == QualityProject && a.Location.Trim() == Location && a.Project == objSWP010.Project && a.BU == objSWP010.BU && a.Status == strApproved
                             && (term != "" ? (a.SeamNo.Contains(term)) : true)
                             select a).Distinct().ToList();

                list1 = seams.Where(a => Manager.IsSeamApplicableForPrintSlip(a.Project, a.QualityProject, a.BU, a.Location, a.SeamNo)).Take(25).Select(s => new clsTask { id = s.SeamNo, text = s.SeamNo }).ToList();
            }
            if (tokenfor == clsImplementationEnum.TypeOfTokenProcess.WPP.GetStringValue())
            {
                var seamWPP = (from a in db.WPP013_Log
                               where a.QualityProject == QualityProject && a.Location.Trim() == Location
                               && (term != "" ? (a.Seam.Contains(term)) : true)
                               select a).Distinct().ToList();
                list1 = seamWPP.Where(a => Manager.IsSeamApplicableForPrintSlip(a.Project, a.QualityProject, a.BU, a.Location, a.Seam))
                        .Select(s => new { id = s.Seam, text = s.Seam }).Distinct().ToList()
                        .Select(i => new clsTask { id = i.id, text = i.text }).Distinct().ToList();
            }
            objResponseMsg.lstSeam = list1;

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult OnSeamSelected(string SeamNo, string QualityProject, string Location, string tokenfor)
        {
            Location = objClsLoginInfo.Location;
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            var strApproved = CommonStatus.Approved.GetStringValue();

            var Project = (from a in db.SWP010
                           where a.QualityProject == QualityProject
                           select a.Project).FirstOrDefault();

            var BU = (from a in db.SWP010
                      where a.QualityProject == QualityProject
                      select a.BU).FirstOrDefault();

            List<clsTask> list = new List<clsTask>();
            var strrelease = WPPStatus.Released.GetStringValue();
            //var Location = objClsLoginInfo.Location;
            if (tokenfor == clsImplementationEnum.TypeOfTokenProcess.WPP.GetStringValue())
            {
                var lstWPPNumber = (from w10 in db.WPP010_Log
                                    join w13 in db.WPP013_Log on w10.HeaderId equals w13.HeaderId
                                    where (w10.QualityProject == QualityProject || w10.QualityProject.Contains(QualityProject))
                                           && w10.Location == Location && w10.Status == strrelease && w13.Seam == SeamNo
                                    select new { id = w10.WPPNumber, text = w10.WPPNumber + " #" + (w10.WeldingProcess1 == null || w10.WeldingProcess1 == "" ? "" : w10.WeldingProcess1) + (w10.WeldingProcess2 == null || w10.WeldingProcess2 == "" ? "" : "+" + w10.WeldingProcess2) + (w10.WeldingProcess3 == null || w10.WeldingProcess3 == "" ? "" : "+" + w10.WeldingProcess3) }).Distinct().ToList();
                list = (from wpp in lstWPPNumber
                        select new { id = wpp.id, text = wpp.text }).Distinct().ToList()
                        .Select(i => new clsTask { id = i.id, text = i.text }).Distinct().ToList();
            }
            else
            {
                SWP010 objSWP010 = db.SWP010.Where(c => c.SeamNo == SeamNo && c.Project == Project && c.QualityProject == QualityProject && c.BU == BU && c.Location.Trim() == Location.Trim() && c.Status == strApproved).FirstOrDefault();
                if (objSWP010 != null)
                {
                    if (!string.IsNullOrWhiteSpace(objSWP010.MainWPSNumber))
                    {
                        var lstWeldProcess = (from a in db.WPS010_Log
                                              where a.WPSNumber == objSWP010.MainWPSNumber && a.WPSRevNo == objSWP010.MainWPSRevNo
                                              select new { weldingProcess = (a.WeldingProcess1 == null || a.WeldingProcess1 == "" ? "" : a.WeldingProcess1) + (a.WeldingProcess2 == null || a.WeldingProcess2 == "" ? "" : "+" + a.WeldingProcess2) + (a.WeldingProcess3 == null || a.WeldingProcess3 == "" ? "" : "+" + a.WeldingProcess3) + (a.WeldingProcess4 == null || a.WeldingProcess4 == "" ? "" : "+" + a.WeldingProcess4) }).FirstOrDefault();

                        list.Add(new clsTask { id = objSWP010.MainWPSNumber, text = objSWP010.MainWPSNumber + " #" + (lstWeldProcess.weldingProcess != null ? lstWeldProcess.weldingProcess : "") });
                    }
                    if (!string.IsNullOrWhiteSpace(objSWP010.AlternateWPS1))
                    {
                        var lstWeldProcess = (from a in db.WPS010_Log
                                              where a.WPSNumber == objSWP010.AlternateWPS1 && a.WPSRevNo == objSWP010.AlternateWPS1RevNo
                                              select new { weldingProcess = (a.WeldingProcess1 == null || a.WeldingProcess1 == "" ? "" : a.WeldingProcess1) + (a.WeldingProcess2 == null || a.WeldingProcess2 == "" ? "" : "+" + a.WeldingProcess2) + (a.WeldingProcess3 == null || a.WeldingProcess3 == "" ? "" : "+" + a.WeldingProcess3) + (a.WeldingProcess4 == null || a.WeldingProcess4 == "" ? "" : "+" + a.WeldingProcess4) }).FirstOrDefault();

                        list.Add(new clsTask { id = objSWP010.AlternateWPS1, text = objSWP010.AlternateWPS1 + " #" + (lstWeldProcess.weldingProcess != null ? lstWeldProcess.weldingProcess : "") });
                    }
                    if (!string.IsNullOrWhiteSpace(objSWP010.AlternateWPS2))
                    {
                        var lstWeldProcess = (from a in db.WPS010_Log
                                              where a.WPSNumber == objSWP010.AlternateWPS2 && a.WPSRevNo == objSWP010.AlternateWPS2RevNo
                                              select new { weldingProcess = (a.WeldingProcess1 == null || a.WeldingProcess1 == "" ? "" : a.WeldingProcess1) + (a.WeldingProcess2 == null || a.WeldingProcess2 == "" ? "" : "+" + a.WeldingProcess2) + (a.WeldingProcess3 == null || a.WeldingProcess3 == "" ? "" : "+" + a.WeldingProcess3) + (a.WeldingProcess4 == null || a.WeldingProcess4 == "" ? "" : "+" + a.WeldingProcess4) }).FirstOrDefault();

                        list.Add(new clsTask { id = objSWP010.AlternateWPS2, text = objSWP010.AlternateWPS2 + " #" + (lstWeldProcess.weldingProcess != null ? lstWeldProcess.weldingProcess : "") });
                    }
                }
            }
            objResponseMsg.lstWPS = list;
            objResponseMsg.Value = db.QMS012.Where(x => x.QualityProject == QualityProject && x.SeamNo == SeamNo).Select(x => x.SeamCategory).FirstOrDefault();
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult verifyEmployee(string employee)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                employee = employee.Split('-')[0].Trim();
                var Exists = db.COM003.Any(x => x.t_psno == employee && x.t_actv == 1);
                if (Exists == false)
                {
                    objResponseMsg.Key = false;
                }
                else
                {
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult getDataEdit(string QualityProject, int Id)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();

            var objWPS025 = (from a in db.WPS025
                             where a.Id == Id
                             select a
                          ).FirstOrDefault();
            var WPSNum = objWPS025.WPSNumber;
            objResponseMsg.WPSNum = WPSNum;

            List<clsTask> list1 = new List<clsTask>();
            list1 = (from a in db.SWP010
                     where a.QualityProject == QualityProject
                     select new clsTask
                     {
                         id = a.MainWPSNumber,
                         text = a.MainWPSNumber
                     }).Distinct().ToList();

            objResponseMsg.lstWPS = list1;

            //List<clsTask> Layer1 = (from a in db.WPS011
            //                        where a.WPSNumber == WPSNum
            //                        select new clsTask { id = a.WeldLayer, text = a.WeldLayer }).Distinct().ToList();

            //objResponseMsg.lstLayer = Layer1.Distinct().ToList();


            List<clsTask> Layer1 = (from a in db.WPS011
                                    where a.WPSNumber == WPSNum && a.WeldingProcess == objWPS025.WeldingProcess
                                    select new clsTask { id = a.WeldLayer, text = a.WeldLayer }).Distinct().ToList();

            objResponseMsg.lstLayer = Layer1.Distinct().ToList();

            var lstWeldProcess = (from a in db.WPS010
                                  where a.WPSNumber == WPSNum
                                  select new clsWPSWeldPorcess { weldingProcess1 = a.WeldingProcess1, weldingProcess2 = a.WeldingProcess2, weldingProcess3 = a.WeldingProcess3, weldingProcess4 = a.WeldingProcess4 }).FirstOrDefault();
            List<string> weldprocess = new List<string>();

            if (lstWeldProcess != null)
            {
                if (!string.IsNullOrWhiteSpace(lstWeldProcess.weldingProcess1))
                {
                    weldprocess.Add(lstWeldProcess.weldingProcess1);
                }
                if (!string.IsNullOrWhiteSpace(lstWeldProcess.weldingProcess2))
                {
                    weldprocess.Add(lstWeldProcess.weldingProcess2);
                }
                if (!string.IsNullOrWhiteSpace(lstWeldProcess.weldingProcess3))
                {
                    weldprocess.Add(lstWeldProcess.weldingProcess3);
                }
                if (!string.IsNullOrWhiteSpace(lstWeldProcess.weldingProcess4))
                {
                    weldprocess.Add(lstWeldProcess.weldingProcess4);
                }
            }

            objResponseMsg.lstWeldingProcess = weldprocess.Select(i => new clsTask { id = i, text = i }).ToList();

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult getRevNumber(string WPSNum, string QualityProject, string Location, string BU, string SeamNo, string tokenfor)
        {
            Location = objClsLoginInfo.Location;
            var strApproved = CommonStatus.Approved.GetStringValue();
            var strReleased = clsImplementationEnum.WPPStatus.Released.GetStringValue();
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            if (tokenfor == clsImplementationEnum.TypeOfTokenProcess.WPP.GetStringValue())
            {
                objResponseMsg.WPSRev = db.WPP010_Log.Where(a => a.WPPNumber == WPSNum && (a.Status == strApproved || a.Status == strReleased)).FirstOrDefault().WPPRevNo.ToString();

                var lstWeldProcess = (from a in db.WPP010_Log
                                      where a.WPPNumber == WPSNum
                                      select new clsWPSWeldPorcess { weldingProcess1 = a.WeldingProcess1, weldingProcess2 = a.WeldingProcess2, weldingProcess3 = a.WeldingProcess3 }).FirstOrDefault();
                List<string> weldprocess = new List<string>();

                if (lstWeldProcess != null)
                {
                    if (!string.IsNullOrWhiteSpace(lstWeldProcess.weldingProcess1))
                    {
                        weldprocess.Add(lstWeldProcess.weldingProcess1);
                    }
                    if (!string.IsNullOrWhiteSpace(lstWeldProcess.weldingProcess2))
                    {
                        weldprocess.Add(lstWeldProcess.weldingProcess2);
                    }
                    if (!string.IsNullOrWhiteSpace(lstWeldProcess.weldingProcess3))
                    {
                        weldprocess.Add(lstWeldProcess.weldingProcess3);
                    }
                }

                objResponseMsg.lstWeldingProcess = weldprocess.Select(i => new clsTask { id = i, text = i }).ToList();
                objResponseMsg.JointType = db.WPP010_Log.Where(i => i.WPPNumber == WPSNum).Select(o => o.JointType).FirstOrDefault();

            }
            else
            {
                var Rev = (from a in db.SWP010
                           where a.MainWPSNumber == WPSNum && a.QualityProject == QualityProject
                           && a.Location == Location && a.BU == BU && a.SeamNo == SeamNo
                           && a.Status == strApproved
                           orderby a.MainWPSRevNo descending
                           select a.MainWPSRevNo).FirstOrDefault();

                if (Rev != null)
                {
                    objResponseMsg.WPSRev = Rev.ToString();
                }
                else
                {
                    Rev = (from a in db.SWP010
                           where a.AlternateWPS1 == WPSNum && a.QualityProject == QualityProject
                           && a.Location.Trim() == Location.Trim() && a.BU == BU && a.SeamNo == SeamNo
                           && a.Status == strApproved
                           orderby a.AlternateWPS1 descending
                           select a.AlternateWPS1RevNo).FirstOrDefault();
                    if (Rev != null)
                    {
                        objResponseMsg.WPSRev = Rev.ToString();
                    }
                    else
                    {
                        Rev = (from a in db.SWP010
                               where a.AlternateWPS2 == WPSNum && a.QualityProject == QualityProject
                               && a.Location.Trim() == Location.Trim() && a.BU == BU && a.SeamNo == SeamNo
                               && a.Status == strApproved
                               orderby a.AlternateWPS2 descending
                               select a.AlternateWPS2RevNo).FirstOrDefault();
                        if (Rev != null)
                        {
                            objResponseMsg.WPSRev = Rev.ToString();
                        }
                        else
                        {
                            objResponseMsg.WPSRev = string.Empty;
                        }
                    }
                }

                var WPSRevNo = Convert.ToInt32(objResponseMsg.WPSRev);
                var lstWeldProcess = (from a in db.WPS010_Log
                                      where a.WPSNumber == WPSNum && a.WPSRevNo == WPSRevNo
                                      select new clsWPSWeldPorcess { weldingProcess1 = a.WeldingProcess1, weldingProcess2 = a.WeldingProcess2, weldingProcess3 = a.WeldingProcess3, weldingProcess4 = a.WeldingProcess4 }).FirstOrDefault();
                List<string> weldprocess = new List<string>();

                if (lstWeldProcess != null)
                {
                    if (!string.IsNullOrWhiteSpace(lstWeldProcess.weldingProcess1))
                    {
                        weldprocess.Add(lstWeldProcess.weldingProcess1);
                    }
                    if (!string.IsNullOrWhiteSpace(lstWeldProcess.weldingProcess2))
                    {
                        weldprocess.Add(lstWeldProcess.weldingProcess2);
                    }
                    if (!string.IsNullOrWhiteSpace(lstWeldProcess.weldingProcess3))
                    {
                        weldprocess.Add(lstWeldProcess.weldingProcess3);
                    }
                    if (!string.IsNullOrWhiteSpace(lstWeldProcess.weldingProcess4))
                    {
                        weldprocess.Add(lstWeldProcess.weldingProcess4);
                    }
                }

                objResponseMsg.lstWeldingProcess = weldprocess.Select(i => new clsTask { id = i, text = i }).ToList();

                //List<clsTask> Layer1 = (from a in db.WPS011
                //                        where a.WPSNumber == WPSNum
                //                        select new clsTask { id = a.WeldLayer, text = a.WeldLayer }).Distinct().ToList();

                //objResponseMsg.lstLayer = Layer1.Distinct().ToList();
                objResponseMsg.JointType = db.WPS010.Where(i => i.WPSNumber == WPSNum).Select(o => o.Jointtype).FirstOrDefault();
            }

            if (objResponseMsg.JointType.ToLower() == "buttering")
                //objResponseMsg.JointType = "Groove/Fillet";
                objResponseMsg.JointType = "buttering";
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetWeldingLayers(string term, string WPSNum, string WeldingProcess, int WPSRevNo, string tokenfor)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            List<clsTask> Layer1 = new List<clsTask>();
            if (tokenfor == clsImplementationEnum.TypeOfTokenProcess.WPP.GetStringValue())
            {
                Layer1 = (from a in db.WPP011_Log
                          where a.WPPNumber == WPSNum && a.WeldingProcess == WeldingProcess && a.WPPRevNo == WPSRevNo
                          && (term != "" ? (a.WSNo.Contains(term)) : true)
                          select new clsTask { id = a.WSNo, text = a.WSNo }).Distinct().ToList();
            }
            else
            {
                Layer1 = (from a in db.WPS011_Log
                          where a.WPSNumber == WPSNum && a.WeldingProcess == WeldingProcess && a.WPSRevNo == WPSRevNo
                          && (term != "" ? (a.WeldLayer.Contains(term)) : true)
                          select new clsTask { id = a.WeldLayer, text = a.WeldLayer }).Distinct().ToList();
            }
            objResponseMsg.lstLayer = Layer1.Distinct().ToList();
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult LoadData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string strWhere = string.Empty;
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);


                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = "(Project + ' - ' + b.t_dsca like '%" + param.sSearch
                         + "%' or QualityProject like '%" + param.sSearch
                         + "%' or BU + ' - ' + c.t_desc  like '%" + param.sSearch
                         + "%' or Location + ' - ' + c.t_desc  like '%" + param.sSearch
                         + "%' or SeamNo like '%" + param.sSearch
                         + "%' or WeldingProcess like '%" + param.sSearch
                         + "%' or WelderPSNO +' - '+ e.t_name  like '%" + param.sSearch
                         + "%' or PrintedBy +' - '+ f.t_name  like '%" + param.sSearch
                         + "%' or PrintedOn like '%" + param.sSearch
                         + "%' or WelderStamp like '%" + param.sSearch
                         + "%')";
                }
                else
                {
                    strWhere = " 1 = 1";
                }
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_IPI_Welder_ParameterSlip
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.QualityProject),
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.BU),
                               Convert.ToString(uc.Location),
                               Convert.ToString(uc.SeamNo),
                               Convert.ToString(uc.WeldingProcess),
                               Convert.ToString(uc.WelderPSNO),
                               Convert.ToString(uc.WelderStamp),
                               Convert.ToString(uc.PrintedBy),
                               Convert.ToString(uc.PrintedOn),
                               Convert.ToString(uc.Id),
                               Convert.ToString(uc.Id)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetSlipListDataPartial(string status, string project, string FrDate = "", string ToDate = "")
        {
            ViewBag.Status = status;
            ViewBag.searchproject = project;
            ViewBag.frdate = FrDate;
            ViewBag.todate = ToDate;
            return PartialView("_SlipListPartial");
        }

        [HttpPost]
        public JsonResult LoadTokenlistGridData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string strWhere = "1=1";
                string strActiveCondition = string.Empty;
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string status = param.Status;
                string Qp = param.QualityProject;
                string frdate = param.Frdate;
                string todate = param.Todate;

                bool IsExpired = false;
                if (status.ToLower() == "active")
                {
                    strActiveCondition += "  WHERE    1=1 and datediff(hh, wps26.CreatedOn, getdate()) < 12  and (TokenNumber != ' ' )";
                    strWhere += " and datediff(hh, CreatedOn, getdate()) < 12 and ranking = 1 and (TokenNumber is not null or TokenNumber != ' ' ) ";
                }
                else if (status.ToLower() == "draft")
                {
                    strWhere += " and (TokenNumber is null OR TokenNumber = ' ' ) ";
                }
                else
                {
                    IsExpired = true;
                    strWhere += " and (datediff(hh, CreatedOn, getdate()) > 12 or (datediff(hh, CreatedOn, getdate()) < 12 and ranking <> 1) ) and (TokenNumber is not null or TokenNumber != ' ' ) ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and ( Project like '%" + param.sSearch
                         + "%' or QualityProject like '%" + param.sSearch
                         + "%' or BU like '%" + param.sSearch
                         + "%' or Location  like '%" + param.sSearch
                         + "%' or SeamNo like '%" + param.sSearch
                         + "%' or WPSNumber like '%" + param.sSearch
                         + "%' or WPPNumber like '%" + param.sSearch
                         + "%' or TokenFor like '%" + param.sSearch
                         + "%' or WeldingProcess like '%" + param.sSearch
                         + "%' or WelderPSNO  like '%" + param.sSearch
                         + "%' or WelderStamp like '%" + param.sSearch
                         + "%' or PrintedBy  like '%" + param.sSearch
                         + "%' or PrintedOn like '%" + param.sSearch
                         + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                if (!string.IsNullOrEmpty(Qp))
                    strWhere += " and QualityProject = '" + Qp + "'";

                if (!string.IsNullOrEmpty(frdate) && !string.IsNullOrEmpty(todate))
                {
                    string str = "+ ' 12:00:00 AM' as datetime";
                    strWhere += " and CreatedOn between cast('" + frdate + "' " + str + ") and Cast('" + todate + "' " + str + ")";
                    //  and CreatedOn between CAST('2020-10-08' + ' 12:00:00 AM' as datetime) and CAST('2020-10-14' + ' 12:00:00 AM' as datetime)
                }

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                else
                {
                    strSortOrder = " Order By CreatedOn desc ";
                }

                var lstResult = db.SP_IPI_GET_TOKEN_LIST
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere, strActiveCondition
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                              "<center>"+
                              "<a title='View Details' class='btn btn-xs' href='"+WebsiteURL+"/IPI/WelderPSlip/ConsumableSlipDetail?Id="+uc.Id+"&from="+status+"'><i class='iconspace fa fa-eye'></i></a>"+
                              (IsExpired  ? "<i id=\"btnCopyData\" name=\"Copy\" title=\"Copy Record\" class=\"iconspace fa fa-files-o\" onclick=\"CopyRecord("+ uc.Id + ",'"+uc.TokenNumber+"');\"></i>" :  "")+
                              "<a title='Timeline' href='javascript:void(0)' onclick=ShowTimeline('/IPI/WelderPSlip/ShowTimeline?Id=" + uc.Id + "');><i class='iconspace fa fa-clock-o'></i></a>"+
                              "</center>",
                               Convert.ToString(uc.Id),//For Checkbox
                               Convert.ToString(uc.QualityProject),
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.SeamNo),
                               Convert.ToString(uc.WeldingProcess),
                               Convert.ToString(uc.WelderPSNO),
                               Convert.ToString(uc.BU),
                               Convert.ToString(uc.WelderStamp),
                               Convert.ToString(uc.WeldingType),
                               Convert.ToString(uc.Location),
                               Convert.ToString(uc.TokenNumber),
                               Convert.ToString(uc.TokenFor == clsImplementationEnum.TypeOfTokenProcess.WPP.GetStringValue() ? uc.TokenFor :  clsImplementationEnum.TypeOfTokenProcess.WPS.GetStringValue() ),
                               Convert.ToString(uc.WPSNumber),
                               Convert.ToString(uc.PrintedBy),
                               Convert.ToString(uc.PrintedOn)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ShowTimeline(int Id)
        {
            var model = new Utility.Models.TimelineViewModel();
            model.Title = "WelderPSlip";
            WPS026 objWPS026 = db.WPS026.FirstOrDefault(f => f.Id == Id);
            model.PrintedBy = objWPS026.PrintedBy != null ? Manager.GetUserNameFromPsNo(objWPS026.PrintedBy) : null;
            model.PrintedOn = objWPS026.PrintedOn;
            model.CreatedBy = objWPS026.CreatedBy != null ? Manager.GetUserNameFromPsNo(objWPS026.CreatedBy) : null;
            model.CreatedOn = objWPS026.CreatedOn;
            model.EditedBy = objWPS026.EditedBy != null ? Manager.GetUserNameFromPsNo(objWPS026.EditedBy) : null;
            model.EditedOn = objWPS026.EditedOn;

            return PartialView("~/Views/Shared/_TimelineProgress2.cshtml", model);
        }

        [HttpPost]
        public ActionResult GetQualityProjects(string term, string tokenfor)
        {
            string username = objClsLoginInfo.UserName;
            var lstATHLocation = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.Location).Distinct().ToList();
            var lstCOMLocation = db.COM003.Where(i => i.t_psno.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_loca).Distinct().ToList();

            var lstCOMBU = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.BU).Distinct().ToList();

            lstCOMLocation = lstATHLocation.Union(lstCOMLocation).ToList();

            var lstQualityProject = db.QMS010.Where(i => (term != "" && term != null ? (i.QualityProject.ToLower().Contains(term.ToLower())) : true)
                                                        && lstCOMLocation.Contains(i.Location)
                                                        && lstCOMBU.Contains(i.BU)
                                                    ).Select(i => new Projects { Value = i.QualityProject.Trim(), projectCode = i.QualityProject.Trim(), BU = i.BU.Trim() }).Distinct().ToList();//&& i.CreatedBy.Equals(username,StringComparison.OrdinalIgnoreCase)

            return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveHeader(SWP010 swp010, FormCollection fc)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                if (!string.IsNullOrWhiteSpace(fc["hfID"].ToString()) && Convert.ToInt32(fc["hfID"].ToString()) > 0)
                {
                    objResponseMsg.Id = Convert.ToInt32(fc["hfID"]);
                }
                else
                {
                    DateTimeFormatInfo usDtfi = new CultureInfo("en-US", false).DateTimeFormat;

                    WPS025 objWPS025 = new WPS025();
                    objWPS025.QualityProject = fc["txtQP"].ToString();
                    objWPS025.Project = fc["txtProject"].ToString().Split('-')[0];
                    objWPS025.BU = fc["txtBU"].ToString().Split('-')[0].Trim();
                    objWPS025.Location = fc["Location"].ToString().Trim();
                    objWPS025.SeamNo = fc["txtSeam"].ToString();
                    objWPS025.WelderPSNO = fc["txtEmp"].ToString().Split('-')[0];
                    objWPS025.WeldingProcess = fc["txtWP"].ToString();
                    objWPS025.WPSNumber = fc["txtWPS"].ToString();
                    objWPS025.WPSRevNo = Convert.ToInt32(fc["txtWPSRev"].ToString());
                    objWPS025.WeldLayer = fc["txtWL"].ToString();
                    objWPS025.JointType = fc["txtJT"].ToString();
                    objWPS025.PrintedBy = objClsLoginInfo.UserName;
                    objWPS025.PrintedOn = DateTime.Now;

                    objWPS025.ValidUpto = DateTime.Now; //Convert.ToDateTime(fc["ValidUpto"], usDtfi);
                    objWPS025.WelderStamp = Manager.GetWelderStamp(objWPS025.WelderPSNO);
                    db.WPS025.Add(objWPS025);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PlanMessages.Insert.ToString();
                    objResponseMsg.Id = objWPS025.Id;
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    List<SP_IPI_Welder_ParameterSlip_Result> lst = db.SP_IPI_Welder_ParameterSlip(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                    }

                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      SeamNo = li.SeamNo,
                                      WeldingProcess = li.WeldingProcess,
                                      WelderPSNO = li.WelderPSNO,
                                      PrintedBy = li.PrintedBy,
                                      PrintedOn = li.PrintedOn
                                  }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                if (gridType == clsImplementationEnum.GridType.MAINTAININDEX.GetStringValue())
                {
                    List<SP_IPI_GET_TOKEN_LIST_Result> lst = db.SP_IPI_GET_TOKEN_LIST(1, int.MaxValue, strSortOrder, whereCondition, "").ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                    }

                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = Convert.ToString(li.QualityProject),
                                      Project = Convert.ToString(li.Project),
                                      BU = Convert.ToString(li.BU),
                                      WelderStamp = Convert.ToString(li.WelderStamp),
                                      Location = Convert.ToString(li.Location),
                                      TokenNumber = Convert.ToString(li.TokenNumber),
                                      SeamNo = Convert.ToString(li.SeamNo),
                                      WPSNumber = Convert.ToString(li.WPSNumber),
                                      WeldingProcess = Convert.ToString(li.WeldingProcess),
                                      WelderPSNO = Convert.ToString(li.WelderPSNO),
                                      PrintedBy = li.PrintedBy,
                                      PrintedOn = li.PrintedOn
                                  }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.WPQCertificate.GetStringValue())
                {
                    List<SP_WML_WPQCERTIFICATE_DETAILS_Result> lst = db.SP_WML_WPQCERTIFICATE_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                    }

                    NDEModels objNDEModels = new NDEModels();
                    ModelRoleWiseIndex objModelRoleWiseIndex = new ModelRoleWiseIndex();
                    var listWeldingProcess = Manager.GetSubCatagorywithoutBULocation("Welding Process");
                    var WeldingType = Manager.GetSubCatagorywithoutBULocation("Welding Type");
                    var Backing = Manager.GetSubCatagorywithoutBULocation("Backing");
                    var InertGasBacking = Manager.GetSubCatagorywithoutBULocation("Inert gas backing");

                    var newlst = lst.Select(uc => new
                    {
                        Id = uc.Id,
                        WelderPsNo = uc.WelderPsNo,
                        Welder = uc.Welder,
                        WelderStampNo = uc.WelderStampNo,
                        Shop = db.COM002.Where(m => m.t_dtyp == 3 && m.t_dimx != "" && (m.t_dimx == uc.Shop)).Select(m => m.t_dimx + "-" + m.t_desc).FirstOrDefault(),
                        WQTNo = uc.WQTNo,
                        WeldingProcessQualified = GetCodeDesc(uc.WeldingProcessQualified, listWeldingProcess),
                        TypeQualified = GetCodeDesc(uc.TypeQualified, WeldingType),
                        PlatePipePositionActual = uc.PlatePipePositionActual,
                        PipeWeldingPositionQualified = uc.PipeWeldingPositionQualified,
                        PlatePipePositionQualified = uc.PlatePipePositionQualified,
                        PNo = uc.PNo,
                        BaseMetalThicknessQualified = uc.BaseMetalThicknessQualified,
                        DepositedThicknessQualified = uc.DepositedThicknessQualified,
                        PlatePipeQualified = uc.PlatePipeQualified,
                        FNo = uc.FNo,
                        BackingQualified = GetCodeDesc(uc.BackingQualified, Backing),
                        InertGasBackingQualified = GetCodeDesc(uc.InertGasBackingQualified, InertGasBacking),
                        CircWeldOvly = uc.CircWeldOvly,
                        LastWeldDate = (uc.LastWeldDate.HasValue ? uc.LastWeldDate.Value.ToString("dd/MM/yyyy") : ""),
                        ValueUpto = (uc.ValueUpto.HasValue ? uc.ValueUpto.Value.ToString("dd/MM/yyyy") : ""),
                        Remarks = uc.Remarks,
                    }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.WOPQCertificate.GetStringValue())
                {
                    List<SP_WML_WOPQCERTIFICATE_DETAILS_Result> lst = db.SP_WML_WOPQCERTIFICATE_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                    }

                    NDEModels objNDEModels = new NDEModels();
                    ModelRoleWiseIndex objModelRoleWiseIndex = new ModelRoleWiseIndex();
                    var listWeldingProcess = Manager.GetSubCatagorywithoutBULocation("Welding Process");
                    var WeldingType = Manager.GetSubCatagorywithoutBULocation("Welding Type");
                    var Backing = Manager.GetSubCatagorywithoutBULocation("Backing");
                    var InertGasBacking = Manager.GetSubCatagorywithoutBULocation("Inert gas backing");

                    var newlst = lst.Select(uc => new
                    {
                        Id = uc.Id,
                        WelderPsNo = uc.WelderPsNo,
                        Welder = uc.Welder,
                        WelderStampNo = uc.WelderStampNo,
                        Shop = db.COM002.Where(m => m.t_dtyp == 3 && m.t_dimx != "" && (m.t_dimx == uc.Shop)).Select(m => m.t_dimx + "-" + m.t_desc).FirstOrDefault(),
                        WQTNo = uc.WQTNo,
                        WeldingProcessEsQualified = GetCodeDesc(uc.WeldingProcessEsQualified, listWeldingProcess),
                        TypeOfWeldingAutomaticQualified = GetCodeDesc(uc.TypeOfWeldingAutomaticQualified, WeldingType),
                        IPlatePipeOverlayWeldingPositionQualified = uc.IPlatePipeOverlayWeldingPositionQualified,
                        IiPipeFilletWeldingPositionQualified = uc.IiPipeFilletWeldingPositionQualified,
                        IPlatePipeFilletWeldingPositionQualified = uc.IPlatePipeFilletWeldingPositionQualified,
                        PNo = uc.PNo,
                        BaseMetalThicknessQualified = uc.BaseMetalThicknessQualified,
                        DepositedThicknessQualified = uc.DepositedThicknessQualified,
                        PlatePipeEnterDiaIfPipeOrTubeQualified = uc.PlatePipeEnterDiaIfPipeOrTubeQualified,
                        FNo = uc.FNo,
                        BackingQualified = GetCodeDesc(uc.BackingQualified, Backing),
                        InertGasBackingQualified = GetCodeDesc(uc.InertGasBackingQualified, InertGasBacking),
                        CircWeldOvly = uc.CircWeldOvly,
                        LastWeldDate = (uc.LastWeldDate.HasValue ? uc.LastWeldDate.Value.ToString("dd/MM/yyyy") : ""),
                        ValueUpto = (uc.ValueUpto.HasValue ? uc.ValueUpto.Value.ToString("dd/MM/yyyy") : ""),
                        Remarks = uc.Remarks,
                    }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.TTSCertificate.GetStringValue())
                {
                    List<SP_WML_TSSCERTIFICATE_DETAILS_Result> lst = db.SP_WML_TSSCERTIFICATE_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                    }

                    NDEModels objNDEModels = new NDEModels();
                    ModelRoleWiseIndex objModelRoleWiseIndex = new ModelRoleWiseIndex();
                    var listWeldingProcess = Manager.GetSubCatagorywithoutBULocation("Welding Process");
                    var WeldingType = Manager.GetSubCatagorywithoutBULocation("Welding Type");
                    var Backing = Manager.GetSubCatagorywithoutBULocation("Backing");
                    var InertGasBacking = Manager.GetSubCatagorywithoutBULocation("Inert gas backing");
                    var JointTypeWQ = Manager.GetSubCatagorywithoutBULocation("Joint Type WQ");

                    var newlst = lst.Select(uc =>
                            new
                            {
                                Id = uc.Id,
                                WelderPsNo = uc.WelderPsNo,
                                WelderName = uc.WelderName,
                                WelderStampNo = uc.WelderStampNo,
                                Shop = db.COM002.Where(m => m.t_dtyp == 3 && m.t_dimx != "" && (m.t_dimx == uc.Shop)).Select(m => m.t_dimx + "-" + m.t_desc).FirstOrDefault(),
                                WQTNo = uc.WQTNo,
                                WeldingProcessEsQualified = GetCodeDesc(uc.WeldingProcessEsQualified, listWeldingProcess),
                                WeldingTypeQualified = GetCodeDesc(uc.WeldingTypeQualified, WeldingType),
                                JoinType = GetCodeDesc(uc.JoinType, JointTypeWQ),
                                LastWeldDate = uc.LastWeldDate.HasValue ? uc.LastWeldDate.Value.ToString("dd/MM/yyyy") : "",
                                ValueUpto = uc.ValueUpto.HasValue ? uc.ValueUpto.Value.ToString("dd/MM/yyyy") : "",
                                Remarks = uc.Remarks
                            }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    List<SP_WML_GET_NOTES_DATA_Result> lst = db.SP_WML_GET_NOTES_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }

                    var newlst = (from li in lst
                                  select new
                                  {
                                      Notes = Convert.ToString(li.Notes)
                                  }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult getCodeValue(string project, string approver, string BU)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                objResponseMsg.projdesc = db.COM001.Where(i => i.t_cprj == project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objResponseMsg.appdesc = db.COM003.Where(i => i.t_psno == approver && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
                objResponseMsg.BUdesc = db.COM002.Where(i => i.t_dimx == BU).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetLocation(string term)
        {
            List<CategoryData> lstLocation = null;
            string username = objClsLoginInfo.UserName;
            var lstATHLocation = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.Location).Distinct().ToList();
            lstLocation = (from w8 in db.WQR008
                           where lstATHLocation.Contains(w8.ParentLocation) && (string.IsNullOrEmpty(term) ? true : w8.Location.Contains(term) || w8.Description.Contains(term))
                           select new CategoryData { Code = w8.EU, Value = w8.Location, Description = w8.Description }
                            ).Distinct().ToList();

            return Json(lstLocation, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetTokenBase(string BU, string Location = "")
        {
            if (String.IsNullOrWhiteSpace(Location))
                Location = objClsLoginInfo.Location;
            Location = db.WQR008.Where(x => x.Location == Location).Select(x => x.ParentLocation).FirstOrDefault();
            var subCategory = Manager.GetSubCatagories("TokenBase", BU, Location);
            List<CategoryData> lstLocation = null;
            lstLocation = (from w8 in subCategory
                           select new CategoryData { Code = w8.Code, Value = w8.Description, Description = w8.Code + "-" + w8.Description }
                            ).ToList();
            return Json(lstLocation, JsonRequestBehavior.AllowGet);
        }

        // ALSO USED IN MOBILE APPLICATION
        [HttpPost]
        public ActionResult UpdatePrintData(int Id, string JointType, string APIRequestFrom = "", string UserName = "")
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                var objWPS026 = db.WPS026.Where(i => i.Id == Id).FirstOrDefault();
                if (objWPS026 != null)
                {
                    var objWPS025 = db.WPS025.Where(i => i.RefTokenId == Id).FirstOrDefault();
                    if (objWPS025 == null)
                    {
                        ////Observation 15417
                        List<WPS025> lstWPS025 = new List<WPS025>();
                        List<string> lstSeam = !string.IsNullOrWhiteSpace(objWPS026.IdenticalSeamNo) ? objWPS026.IdenticalSeamNo.Split(',').ToList() : null;
                        if (lstSeam != null)
                        {
                            lstSeam.Insert(0, objWPS026.SeamNo);
                            foreach (var item in lstSeam)
                            {
                                objWPS025 = new WPS025();
                                objWPS025.QualityProject = objWPS026.QualityProject;
                                objWPS025.Project = objWPS026.Project;
                                objWPS025.BU = objWPS026.BU;
                                objWPS025.Location = objWPS026.Location;
                                objWPS025.SeamNo = item;
                                objWPS025.WeldingProcess = objWPS026.WeldingProcess;
                                objWPS025.WelderPSNO = objWPS026.WelderPSNO;
                                objWPS025.WPSNumber = objWPS026.WPSNumber;
                                objWPS025.WPSRevNo = objWPS026.WPSRevNo;
                                objWPS025.WeldLayer = objWPS026.WeldLayer;
                                objWPS025.JointType = JointType.Trim();
                                objWPS025.PrintedBy = UserName;
                                objWPS025.PrintedOn = DateTime.Now;
                                //objWPS025.ValidUpto = objWPS026.BU;
                                objWPS025.WelderStamp = objWPS026.WelderStamp; //Manager.GetWelderStamp(objWPS026.WelderPSNO);
                                objWPS025.RefTokenId = objWPS026.Id;
                                lstWPS025.Add(objWPS025);
                            }
                        }
                        else
                        {
                            objWPS025 = new WPS025();
                            objWPS025.QualityProject = objWPS026.QualityProject;
                            objWPS025.Project = objWPS026.Project;
                            objWPS025.BU = objWPS026.BU;
                            objWPS025.Location = objWPS026.Location;
                            objWPS025.SeamNo = objWPS026.SeamNo;
                            objWPS025.WeldingProcess = objWPS026.WeldingProcess;
                            objWPS025.WelderPSNO = objWPS026.WelderPSNO;
                            objWPS025.WPSNumber = objWPS026.WPSNumber;
                            objWPS025.WPSRevNo = objWPS026.WPSRevNo;
                            objWPS025.WeldLayer = objWPS026.WeldLayer;
                            objWPS025.JointType = JointType.Trim();
                            objWPS025.PrintedBy = UserName;
                            objWPS025.PrintedOn = DateTime.Now;
                            //objWPS025.ValidUpto = objWPS026.BU;
                            objWPS025.WelderStamp = objWPS026.WelderStamp; //Manager.GetWelderStamp(objWPS026.WelderPSNO);
                            objWPS025.RefTokenId = objWPS026.Id;
                            lstWPS025.Add(objWPS025);
                        }
                        db.WPS025.AddRange(lstWPS025);
                        db.SaveChanges();
                    }
                    else
                    {
                        objWPS025.PrintedBy = UserName;
                        objWPS025.PrintedOn = DateTime.Now;
                    }
                    objWPS026.PrintedBy = UserName;
                    objWPS026.PrintedOn = DateTime.Now;

                    db.SaveChanges();
                    var id = db.WPS025.Where(x => x.SeamNo == objWPS026.SeamNo && x.QualityProject == objWPS026.QualityProject && x.RefTokenId == objWPS026.Id).FirstOrDefault().Id;

                    objResponseMsg.Key = true;
                    objResponseMsg.Id = id;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteTokenGenerate(string strIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrayIds = strIds.Split(',').ToArray();

                var lstDeleteToken = db.WPS026.Where(i => arrayIds.Contains(i.Id.ToString())).ToList();
                if (lstDeleteToken != null && lstDeleteToken.Count > 0)
                {
                    db.WPS026.RemoveRange(lstDeleteToken);
                    db.SaveChanges();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Token Founds For Delete!";
                    return Json(objResponseMsg);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Token Deleted Successfully!";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg);
        }

        #region WML Lists
        [SessionExpireFilter]
        [HttpPost]
        public PartialViewResult LoadWMLListPartial(string Location, string JointType, string WeldingProcess, string ProjectNo)
        {
            ViewBag.Location = Location;
            ViewBag.WeldingProcess = WeldingProcess;
            //TempData["JointTypeOrg"] = JointType;
            ViewBag.JointTypeOrg = JointType;
            if (JointType == "buttering")
            {
                JointType = "Groove/Fillet";
            }
            ViewBag.JointType = JointType;
            ViewBag.ProjectNo = ProjectNo;
            return PartialView("_WMLListPartial");
        }

        [HttpPost]// Groove/Fillet Certificate
        public JsonResult LoadWMLWPQCertificateData(JQueryDataTableParamModel param, string Location, string WeldingProcess, string JointType)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                string[] columnName = { "Welder", "WelderName", "WelderStampNo", "Shop", "WQTNo", "WeldingProcessCodeName", "WeldingTypeCodeName", "PlatePipePositionActual", "PipeWeldingPositionQualified", "PlatePipePositionQualified", "PNo", "BaseMetalThicknessQualified", "DepositedThicknessQualified", "PlatePipeQualified", "FNo", "BackingCodeName", "InertGasBackingCodeName", "CircWeldOvly", "LastWeldDate", "ValueUpto", "Remarks" };
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string QualityProject = Convert.ToString(param.QualityProject);
                string Project = QualityProject.Split('/')[0];
                if (sortColumnName.ToLower() == "WelderName".ToLower())
                {
                    sortColumnName = "Welder";
                }
                else if (sortColumnName.ToLower() == "WeldingProcessCodeName".ToLower())
                {
                    sortColumnName = "WeldingProcessQualified";
                }
                else if (sortColumnName.ToLower() == "WeldingTypeCodeName".ToLower())
                {
                    sortColumnName = "TypeQualified";
                }
                else if (sortColumnName.ToLower() == "BackingCodeName".ToLower())
                {
                    sortColumnName = "BackingQualified";
                }
                else if (sortColumnName.ToLower() == "InertGasBackingCodeName".ToLower())
                {
                    sortColumnName = "InertGasBackingQualified";
                }

                if (!string.Equals(JointType, "Butt joint", StringComparison.OrdinalIgnoreCase) && !string.Equals(JointType, "Butt Joints", StringComparison.OrdinalIgnoreCase) && !string.Equals(JointType, "Buttering", StringComparison.OrdinalIgnoreCase) &&  db.WML005.Any(i => i.Project == Project || i.IProject == Project))
                {
                    var listProjects = db.WML005.Where(i => i.Project == Project || i.IProject == Project).ToList().Select(i => i.Project).Distinct().ToArray();
                    if (db.WML001.Any(i => listProjects.Contains(i.Project)))
                    {
                        Project = string.Join(",", listProjects);
                    }
                    else
                    {
                        Project = "";
                    }
                }
                else
                {
                    Project = "";
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                WeldingProcess = GetSameWeldingProcessByWeldingProcess(WeldingProcess);
                strWhere += "1=1 and ValueUpto >= cast(getdate() as date) and Location='" + Location + "' and WeldingProcessQualified in (SELECT Value FROM dbo.fn_Split('" + WeldingProcess + "',','))";
                if (!string.IsNullOrEmpty(Project))
                {
                    strWhere += " and Project in(Select * from dbo.fn_Split('" + Project + "',',')) ";
                }
                else
                {
                    strWhere += " and ISNULL(Project,'')='" + Project + "' ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                var lstResult = db.SP_WML_WPQCERTIFICATE_DETAILS(StartIndex, EndIndex, strSortOrder, strWhere).ToList();

                //NDEModels objNDEModels = new NDEModels();
                //ModelRoleWiseIndex objModelRoleWiseIndex = new ModelRoleWiseIndex();
                //var listWeldingProcess = Manager.GetSubCatagorywithoutBULocation("Welding Process");
                //var WeldingType = Manager.GetSubCatagorywithoutBULocation("Welding Type");
                //var Backing = Manager.GetSubCatagorywithoutBULocation("Backing");
                //var InertGasBacking = Manager.GetSubCatagorywithoutBULocation("Inert gas backing");


                var data = lstResult.Select(uc => new
                {
                    Id = uc.Id,
                    WelderPsNo = uc.WelderPsNo,
                    Welder = uc.Welder,
                    WelderStampNo = uc.WelderStampNo,
                    Shop = uc.Shop,// db.COM002.Where(m => m.t_dtyp == 3 && m.t_dimx != "" && (m.t_dimx == uc.Shop)).Select(m => m.t_dimx + "-" + m.t_desc).FirstOrDefault(),
                    WQTNo = uc.WQTNo,
                    uc.WeldingProcessQualified,//WeldingProcessQualified = GetCodeDesc(uc.WeldingProcessQualified, listWeldingProcess),
                    uc.TypeQualified,//TypeQualified = GetCodeDesc(uc.TypeQualified, WeldingType),
                    PlatePipePositionActual = uc.PlatePipePositionActual,
                    PipeWeldingPositionQualified = uc.PipeWeldingPositionQualified,
                    PlatePipePositionQualified = uc.PlatePipePositionQualified,
                    PNo = uc.PNo,
                    BaseMetalThicknessQualified = uc.BaseMetalThicknessQualified,
                    DepositedThicknessQualified = uc.DepositedThicknessQualified,
                    PlatePipeQualified = uc.PlatePipeQualified,
                    FNo = uc.FNo,
                    BackingQualified = uc.BackingQualified,//BackingQualified = GetCodeDesc(uc.BackingQualified, Backing),
                    InertGasBackingQualified = uc.InertGasBackingQualified,//InertGasBackingQualified = GetCodeDesc(uc.InertGasBackingQualified, InertGasBacking),
                    CircWeldOvly = uc.CircWeldOvly,
                    LastWeldDate = (uc.LastWeldDate.HasValue ? uc.LastWeldDate.Value.ToString("dd/MM/yyyy") : ""),
                    ValueUpto = (uc.ValueUpto.HasValue ? uc.ValueUpto.Value.ToString("dd/MM/yyyy") : ""),
                    Remarks = uc.Remarks,
                    uc.TypeQualifiedCode,
                });

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]//Overlay Certificate
        public JsonResult LoadWMLWOPQCertificateData(JQueryDataTableParamModel param, string Location, string WeldingProcess)
        {
            var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
            int StartIndex = param.iDisplayStart + 1;
            int EndIndex = param.iDisplayStart + param.iDisplayLength;

            string strWhere = string.Empty;
            string[] columnName = { "WelderPsNo", "Welder", "WelderStampNo", "Shop", "WQTNo", "WeldingProcessCodeName", "WeldingTypeCodeName", "IPlatePipeOverlayWeldingPositionQualified", "IiPipeFilletWeldingPositionQualified", "IPlatePipeFilletWeldingPositionQualified", "PNo", "PlatePipeEnterDiaIfPipeOrTubeQualified", "FNo", "BackingCodeName", "CircWeldOvly", "LastWeldDate", "ValueUpto", "Remarks" };
            string strSortOrder = string.Empty;
            string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
            string sortDirection = Convert.ToString(Request["sSortDir_0"]);
            string QualityProject = Convert.ToString(param.QualityProject);
            string Project = QualityProject.Split('/')[0];

            if (sortColumnName.ToLower() == "WeldingProcessCodeName".ToLower())
            {
                sortColumnName = "WeldingProcessEsQualified";
            }
            else if (sortColumnName.ToLower() == "WeldingTypeCodeName".ToLower())
            {
                sortColumnName = "TypeOfWeldingAutomaticQualified";
            }
            else if (sortColumnName.ToLower() == "BackingCodeName".ToLower())
            {
                sortColumnName = "BackingQualified";
            }

            if (db.WML005.Any(i => i.Project == Project || i.IProject == Project))
            {
                var listProjects = db.WML005.Where(i => i.Project == Project || i.IProject == Project).ToList().Select(i => i.Project).Distinct().ToArray();
                if (db.WML002.Any(i => listProjects.Contains(i.Project)))
                {
                    Project = string.Join(",", listProjects);
                }
                else
                {
                    Project = "";
                }
            }
            else
            {
                Project = "";
            }

            if (!string.IsNullOrWhiteSpace(sortColumnName))
            {
                strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
            }
            WeldingProcess = GetSameWeldingProcessByWeldingProcess(WeldingProcess);
            strWhere += "1=1 and ValueUpto >= cast(getdate() as date) and Location='" + Location + "' and WeldingProcessEsQualified in (SELECT Value FROM dbo.fn_Split('" + WeldingProcess + "',','))";
            if (!string.IsNullOrEmpty(Project))
            {
                strWhere += " and Project in(Select * from dbo.fn_Split('" + Project + "',',')) ";
            }
            else
            {
                strWhere += " and ISNULL(Project,'')='" + Project + "' ";
            }

            if (!string.IsNullOrWhiteSpace(param.sSearch))
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
            else
                strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
            var lstResult = db.SP_WML_WOPQCERTIFICATE_DETAILS(StartIndex, EndIndex, strSortOrder, strWhere).ToList();

            //NDEModels objNDEModels = new NDEModels();
            //ModelRoleWiseIndex objModelRoleWiseIndex = new ModelRoleWiseIndex();
            //var listWeldingProcess = Manager.GetSubCatagorywithoutBULocation("Welding Process");
            //var WeldingType = Manager.GetSubCatagorywithoutBULocation("Welding Type");
            //var Backing = Manager.GetSubCatagorywithoutBULocation("Backing");
            //var InertGasBacking = Manager.GetSubCatagorywithoutBULocation("Inert gas backing");

            var data = lstResult.Select(uc => new
            {
                Id = uc.Id,
                WelderPsNo = uc.WelderPsNo,
                Welder = uc.Welder,
                WelderStampNo = uc.WelderStampNo,
                Shop = uc.Shop, //db.COM002.Where(m => m.t_dtyp == 3 && m.t_dimx != "" && (m.t_dimx == uc.Shop)).Select(m => m.t_dimx + "-" + m.t_desc).FirstOrDefault(),
                WQTNo = uc.WQTNo,
                uc.WeldingProcessEsQualified,//WeldingProcessEsQualified = GetCodeDesc(uc.WeldingProcessEsQualified, listWeldingProcess),
                uc.TypeOfWeldingAutomaticQualified,//TypeOfWeldingAutomaticQualified = GetCodeDesc(uc.TypeOfWeldingAutomaticQualified, WeldingType),
                IPlatePipeOverlayWeldingPositionQualified = uc.IPlatePipeOverlayWeldingPositionQualified,
                IiPipeFilletWeldingPositionQualified = uc.IiPipeFilletWeldingPositionQualified,
                IPlatePipeFilletWeldingPositionQualified = uc.IPlatePipeFilletWeldingPositionQualified,
                PNo = uc.PNo,
                BaseMetalThicknessQualified = uc.BaseMetalThicknessQualified,
                DepositedThicknessQualified = uc.DepositedThicknessQualified,
                PlatePipeEnterDiaIfPipeOrTubeQualified = uc.PlatePipeEnterDiaIfPipeOrTubeQualified,
                FNo = uc.FNo,
                BackingQualified = uc.BackingQualified,//BackingQualified = GetCodeDesc(uc.BackingQualified, Backing),
                InertGasBackingQualified = uc.InertGasBackingQualified,//InertGasBackingQualified = GetCodeDesc(uc.InertGasBackingQualified, InertGasBacking),
                CircWeldOvly = uc.CircWeldOvly,
                LastWeldDate = (uc.LastWeldDate.HasValue ? uc.LastWeldDate.Value.ToString("dd/MM/yyyy") : ""),
                ValueUpto = (uc.ValueUpto.HasValue ? uc.ValueUpto.Value.ToString("dd/MM/yyyy") : ""),
                Remarks = uc.Remarks,
                uc.TypeOfWeldingAutomaticQualifiedCode,
            });

            return Json(new
            {
                sEcho = Convert.ToInt32(param.sEcho),
                iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                aaData = data,
                strSortOrder = strSortOrder,
                whereCondition = strWhere
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]//T#TS Certificate
        public JsonResult LoadWMLTSSCertificateData(JQueryDataTableParamModel param, string Location, string WeldingProcess)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                string[] columnName = { "WelderPsNo", "Welder", "WelderStampNo", "Shop", "WQTNo", "WeldingProcessCodeName", "WeldingTypeCodeName", "JointTypeCodeName", "LastWeldDate", "ValueUpto", "Remarks" };
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string QualityProject = Convert.ToString(param.QualityProject);
                string Project = QualityProject.Split('/')[0];

                if (sortColumnName.ToLower() == "Welder".ToLower())
                {
                    sortColumnName = "WelderPsNo";
                }
                else if (sortColumnName.ToLower() == "WeldingProcessCodeName".ToLower())
                {
                    sortColumnName = "WeldingProcessEsQualified";
                }
                else if (sortColumnName.ToLower() == "WeldingTypeCodeName".ToLower())
                {
                    sortColumnName = "WeldingTypeQualified";
                }
                else if (sortColumnName.ToLower() == "JointTypeCodeName".ToLower())
                {
                    sortColumnName = "JoinType";
                }

                if (db.WML005.Any(i => i.Project == Project || i.IProject == Project))
                {
                    var listProjects = db.WML005.Where(i => i.Project == Project || i.IProject == Project).ToList().Select(i => i.Project).Distinct().ToArray();
                    if (db.WML003.Any(i => listProjects.Contains(i.Project)))
                    {
                        Project = string.Join(",", listProjects);
                    }
                    else
                    {
                        Project = "";
                    }
                }
                else
                {
                    Project = "";
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                WeldingProcess = GetSameWeldingProcessByWeldingProcess(WeldingProcess);
                strWhere += "1=1 and ValueUpto >= cast(getdate() as date) and Location='" + Location + "' and WeldingProcessEsQualified in (SELECT Value FROM dbo.fn_Split('" + WeldingProcess + "',','))";
                if (!string.IsNullOrEmpty(Project))
                {
                    strWhere += " and Project in(Select * from dbo.fn_Split('" + Project + "',',')) ";
                }
                else
                {
                    strWhere += " and ISNULL(Project,'')='" + Project + "' ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                var lstResult = db.SP_WML_TSSCERTIFICATE_DETAILS(StartIndex, EndIndex, strSortOrder, strWhere).ToList();

                //NDEModels objNDEModels = new NDEModels();
                //ModelRoleWiseIndex objModelRoleWiseIndex = new ModelRoleWiseIndex();
                //var listWeldingProcess = Manager.GetSubCatagorywithoutBULocation("Welding Process");
                //var WeldingType = Manager.GetSubCatagorywithoutBULocation("Welding Type");
                //var Backing = Manager.GetSubCatagorywithoutBULocation("Backing");
                //var InertGasBacking = Manager.GetSubCatagorywithoutBULocation("Inert gas backing");
                //var JointTypeWQ = Manager.GetSubCatagorywithoutBULocation("Joint Type WQ");

                var data = lstResult.Select(uc =>
                            new
                            {
                                Id = uc.Id,
                                WelderPsNo = uc.WelderPsNo,
                                WelderName = uc.WelderName,
                                WelderStampNo = uc.WelderStampNo,
                                Shop = uc.Shop,//db.COM002.Where(m => m.t_dtyp == 3 && m.t_dimx != "" && (m.t_dimx == uc.Shop)).Select(m => m.t_dimx + "-" + m.t_desc).FirstOrDefault(),
                                WQTNo = uc.WQTNo,
                                WeldingProcessEsQualified = uc.WeldingProcessEsQualified,//WeldingProcessEsQualified = GetCodeDesc(uc.WeldingProcessEsQualified, listWeldingProcess),
                                WeldingTypeQualified = uc.WeldingTypeQualified,//WeldingTypeQualified = GetCodeDesc(uc.WeldingTypeQualified, WeldingType),
                                JoinType = uc.JoinType,//JoinType = GetCodeDesc(uc.JoinType, JointTypeWQ),
                                LastWeldDate = uc.LastWeldDate.HasValue ? uc.LastWeldDate.Value.ToString("dd/MM/yyyy") : "",
                                ValueUpto = uc.ValueUpto.HasValue ? uc.ValueUpto.Value.ToString("dd/MM/yyyy") : "",
                                Remarks = uc.Remarks,
                                uc.WeldingTypeQualifiedCode
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]//WML Notes
        public JsonResult LoadWMLNotes(JQueryDataTableParamModel param, string Location, string JointType)
        {
            try
            {
                #region Datatable Sorting 
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string strWhereCondition = string.Empty;
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                strWhereCondition += "1=1 ";

                #endregion

                //search Condition 

                if (!string.IsNullOrEmpty(JointType))
                {
                    switch (JointType)
                    {
                        case "groove/fillet":
                            strWhereCondition += "and IsGroove = 1 ";
                            break;
                        case "overlay":
                            strWhereCondition += "and IsOverlay = 1 ";
                            break;
                        case "t#ts":
                            strWhereCondition += "and IsTTS = 1 ";
                            break;
                    }
                }

                string QualityProject = Convert.ToString(param.QualityProject);
                string Project = QualityProject.Split('/')[0];
                string WMLType = string.Empty;
                List<WML004> listWML004 = new List<WML004>();

                if (db.WML005.Any(i => i.Project == Project || i.IProject == Project))
                {
                    var objWML005 = db.WML005.Where(i => i.Project == Project || i.IProject == Project).FirstOrDefault();
                    if (objWML005 != null)
                    {
                        WMLType = objWML005.WMLType;
                    }
                }

                if (!string.IsNullOrEmpty(WMLType))
                    strWhereCondition += "and WMLType = '" + WMLType + "' ";

                if (!string.IsNullOrEmpty(Location))
                    strWhereCondition += "and Location = '" + Location + "' ";

                string[] columnName = { "Notes" };
                if (!string.IsNullOrEmpty(param.sSearch))
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);

                var lstResult = db.SP_WML_GET_NOTES_DATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = lstResult.Select(uc =>
                            new
                            {
                                Location = uc.Location,
                                Notes = uc.Notes,
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        private string GetCodeDesc(string Code, List<CategoryData> lstGBL)
        {
            try
            {
                string description = lstGBL.FirstOrDefault(f => f.Value == Code).CategoryDescription;
                if (string.IsNullOrWhiteSpace(description))
                {
                    description = Code;
                }
                return description;
            }
            catch (Exception)
            {
                return Code;
            }
        }
        private string GetSameWeldingProcessByWeldingProcess(string WeldingProcess)
        {
            try
            {
                if (WeldingProcess.ToLower() == ("SAW").ToLower() || WeldingProcess.ToLower() == ("SAW-T").ToLower())
                {
                    WeldingProcess = "SAW,SAW-T";
                }
                else if (WeldingProcess.ToLower() == ("GTAW").ToLower() || WeldingProcess.ToLower() == ("GTAW-P").ToLower()
                        || WeldingProcess.ToLower() == ("HW GTAW").ToLower())
                {
                    WeldingProcess = "GTAW,GTAW-P,HW GTAW";
                }
                else if (WeldingProcess.ToLower() == ("GMAW").ToLower() || WeldingProcess.ToLower() == ("GMAW-P").ToLower()
                        || WeldingProcess.ToLower() == ("FCAW").ToLower())
                {
                    WeldingProcess = "GMAW,GMAW-P,FCAW";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
            return WeldingProcess;
        }
        #endregion

        public JsonResult GetIdenticalSeamNo(string search, string QualityProject, string Location, string SeamNo, string WPSNo, string WPSRevNo)
        {
            List<SelectItemList> listIdenticalProject = new List<SelectItemList>();
            if (!string.IsNullOrEmpty(SeamNo) && !string.IsNullOrEmpty(WPSNo) && !string.IsNullOrEmpty(WPSRevNo))
            {
                int WPSRev = Convert.ToInt32(WPSRevNo);
                Location = objClsLoginInfo.Location.Trim();
                var objSWP010 = (from a in db.SWP010
                                 where a.QualityProject == QualityProject
                                 select new { a.Project, a.BU }).FirstOrDefault();
                var strApproved = CommonStatus.Approved.GetStringValue();

                var SeamCategory = db.QMS012.Where(i => i.QualityProject.ToUpper() == QualityProject.ToUpper() && i.Location.ToUpper() == i.Location.ToUpper() && i.SeamNo.ToUpper() == SeamNo.ToUpper()).FirstOrDefault().SeamCategory;

                var seams = (from a in db.SWP010
                             where a.QualityProject == QualityProject
                             && a.Location.Trim() == Location
                             && a.Project == objSWP010.Project
                             && a.BU == objSWP010.BU
                             && a.Status == strApproved
                             && a.SeamNo.StartsWith(SeamCategory)
                             && ((a.MainWPSNumber == WPSNo && a.MainWPSRevNo == WPSRev) ||
                                (a.AlternateWPS1 == WPSNo && a.AlternateWPS1RevNo == WPSRev) ||
                                (a.AlternateWPS2 == WPSNo && a.AlternateWPS2RevNo == WPSRev))
                             select a).Distinct().ToList();
                listIdenticalProject = seams.Where(x => x.SeamNo != SeamNo).Select(x => new SelectItemList() { id = x.SeamNo, text = x.SeamNo }).ToList();
            }
            return Json(listIdenticalProject, JsonRequestBehavior.AllowGet);
        }

        public string GetWhiteSpaces(int count)
        {
            var str = "";
            for (int i = 0; i < count; i++)
            {
                str = str + " ";
            }
            return str;
        }
        public string GetStringWithWhiteSpaces(string title, string value)
        {
            value = System.Text.RegularExpressions.Regex.Replace(value, "\t", "/t", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            title = System.Text.RegularExpressions.Regex.Replace(title, "\t", "/t", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            var str = title + " : " + value;

            int len = str.Length;

            if (len > 48)
            {
                var arrString = Split(str, 48);
                string lastString = arrString.Last();
                if (lastString.Length < 48)
                {
                    int tmp_whitespaces = 48 - lastString.Length;
                    str = str + GetWhiteSpaces(tmp_whitespaces);
                }
            }
            else
            {
                int whitespaces = 48 - len;
                str = title + " : " + GetWhiteSpaces(whitespaces) + value;
            }
            if (title != "Valid Upto")
                str = str + "------------------------------------------------";
            return str;
        }

        // ONLY USED IN MOBILE APPLICATION
        [HttpPost]
        public ActionResult UpdateAndGetPrintDataMobile(clsUpdateAndGetPrintDataMobile printData)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                var objWPS026 = db.WPS026.Where(i => i.Id == printData.Id).FirstOrDefault();
                if (objWPS026 != null)
                {
                    var objWPS025 = db.WPS025.Where(i => i.RefTokenId == printData.Id).FirstOrDefault();
                    if (objWPS025 == null)
                    {
                        ////Observation 15417
                        List<WPS025> lstWPS025 = new List<WPS025>();
                        List<string> lstSeam = !string.IsNullOrWhiteSpace(objWPS026.IdenticalSeamNo) ? objWPS026.IdenticalSeamNo.Split(',').ToList() : null;
                        if (lstSeam != null)
                        {
                            lstSeam.Insert(0, objWPS026.SeamNo);
                            foreach (var item in lstSeam)
                            {
                                objWPS025 = new WPS025();
                                objWPS025.QualityProject = objWPS026.QualityProject;
                                objWPS025.Project = objWPS026.Project;
                                objWPS025.BU = objWPS026.BU;
                                objWPS025.Location = objWPS026.Location;
                                objWPS025.SeamNo = item;
                                objWPS025.WeldingProcess = objWPS026.WeldingProcess;
                                objWPS025.WelderPSNO = objWPS026.WelderPSNO;
                                objWPS025.WPSNumber = objWPS026.WPSNumber;
                                objWPS025.WPSRevNo = objWPS026.WPSRevNo;
                                objWPS025.WeldLayer = objWPS026.WeldLayer;
                                objWPS025.JointType = printData.JointType.Trim();
                                objWPS025.PrintedBy = printData.UserName;
                                objWPS025.PrintedOn = DateTime.Now;
                                //objWPS025.ValidUpto = objWPS026.BU;
                                objWPS025.WelderStamp = objWPS026.WelderStamp; //Manager.GetWelderStamp(objWPS026.WelderPSNO);
                                objWPS025.RefTokenId = objWPS026.Id;
                                lstWPS025.Add(objWPS025);
                            }
                        }
                        else
                        {
                            objWPS025 = new WPS025();
                            objWPS025.QualityProject = objWPS026.QualityProject;
                            objWPS025.Project = objWPS026.Project;
                            objWPS025.BU = objWPS026.BU;
                            objWPS025.Location = objWPS026.Location;
                            objWPS025.SeamNo = objWPS026.SeamNo;
                            objWPS025.WeldingProcess = objWPS026.WeldingProcess;
                            objWPS025.WelderPSNO = objWPS026.WelderPSNO;
                            objWPS025.WPSNumber = objWPS026.WPSNumber;
                            objWPS025.WPSRevNo = objWPS026.WPSRevNo;
                            objWPS025.WeldLayer = objWPS026.WeldLayer;
                            objWPS025.JointType = printData.JointType.Trim();
                            objWPS025.PrintedBy = printData.UserName;
                            objWPS025.PrintedOn = DateTime.Now;
                            //objWPS025.ValidUpto = objWPS026.BU;
                            objWPS025.WelderStamp = objWPS026.WelderStamp; //Manager.GetWelderStamp(objWPS026.WelderPSNO);
                            objWPS025.RefTokenId = objWPS026.Id;
                            lstWPS025.Add(objWPS025);
                        }
                        db.WPS025.AddRange(lstWPS025);
                        db.SaveChanges();
                    }
                    else
                    {
                        objWPS025.PrintedBy = printData.UserName;
                        objWPS025.PrintedOn = DateTime.Now;
                    }
                    objWPS026.PrintedBy = printData.UserName;
                    objWPS026.PrintedOn = DateTime.Now;

                    db.SaveChanges();
                    var id = db.WPS025.Where(x => x.SeamNo == objWPS026.SeamNo && x.QualityProject == objWPS026.QualityProject && x.RefTokenId == objWPS026.Id).FirstOrDefault().Id;

                    SP_RPT_WELDERPARAMETERSLIP_Result res_print_data = GetHtmlPSlipDataMobile(id, printData);

                    var QRCodeImgScr = "";
                    if (printData.type == clsImplementationEnum.TypeOfTokenProcess.WPS.GetStringValue())
                    {
                        if (objWPS025 != null)
                            QRCodeImgScr = GetQRText(objWPS025.RefTokenId);
                    }
                    else if (printData.type == clsImplementationEnum.TypeOfTokenProcess.WPP.GetStringValue())
                        QRCodeImgScr = GetQRText(id);

                    #region
                    bool IsNotWPP = printData.type != "WPP" ? true : false;
                    string header_print_string = "";
                    string print_string = "";

                    var WP = res_print_data.WeldingProcess == "GMAW" ? "FCAW" : res_print_data.WeldingProcess;
                    WP = (WP == "HW GTAW" ? "HW-GTAW" : WP);


                    if (printData.weldingProcess.ToUpper() == "A-GTAW")
                    {
                        header_print_string = "There is no print format available for " + printData.weldingProcess.ToUpper() + " process, Please contact to WE.";
                        if (header_print_string.Length > 48)
                        {
                            header_print_string = header_print_string + "\n";
                        }
                    }
                    else
                    {
                        var slip_title = res_print_data.WeldingProcess + " Parameter Slip " + res_print_data.JointType;
                        var len = slip_title.Length;
                        if (len > 48)
                        {
                            header_print_string = slip_title + "\n";
                        }
                        else
                        {
                            double whitespaces = 48 - len;
                            double both_side = whitespaces / 2;
                            int left_spaces = 0;
                            int right_spaces = 0;
                            if ((both_side % 1) != 0) // if decimal
                            {
                                left_spaces = Convert.ToInt32(both_side - 0.5);
                                right_spaces = Convert.ToInt32(both_side + 1);
                            }
                            else
                            {
                                left_spaces = Convert.ToInt32(both_side);
                                right_spaces = Convert.ToInt32(both_side);
                            }
                            header_print_string = GetWhiteSpaces(left_spaces) + slip_title + GetWhiteSpaces(right_spaces);
                            header_print_string = header_print_string + "------------------------------------------------";
                        }

                        header_print_string = header_print_string + GetStringWithWhiteSpaces("Date / Time", DateTime.Now.ToString("dd-MM-yyyy/hh:mm tt"));
                        header_print_string = header_print_string + GetStringWithWhiteSpaces("Welder Stamp/PS No", res_print_data.WelderPSNO);
                        header_print_string = header_print_string + GetStringWithWhiteSpaces("Welder Name", res_print_data.WelderName);
                        // QR Code skip

                        print_string = print_string + "------------------------------------------------";
                        print_string = print_string + GetStringWithWhiteSpaces("Project", res_print_data.QualityProject);
                        print_string = print_string + GetStringWithWhiteSpaces("Seam No", res_print_data.SeamNo);

                        if (!string.IsNullOrEmpty(res_print_data.IdenticalSeamNo) && !string.IsNullOrWhiteSpace(res_print_data.IdenticalSeamNo))
                        {
                            print_string = print_string + GetStringWithWhiteSpaces("Identical Seam", res_print_data.IdenticalSeamNo);
                        }

                        print_string = print_string + GetStringWithWhiteSpaces("Welding Type", res_print_data.WeldingType);
                        print_string = print_string + GetStringWithWhiteSpaces((printData.type == "WPP" ? "WPP" : "WPS") + " No / Rev No", res_print_data.WPSNumber + " / R" + res_print_data.WPSRevNo);
                        print_string = print_string + GetStringWithWhiteSpaces((printData.type == "WPP" ? "WPP" : "WPS") + " Processes", res_print_data.WeldingProcesses);

                        print_string = print_string + GetStringWithWhiteSpaces("P No", res_print_data.ParentMaterial);
                        print_string = print_string + GetStringWithWhiteSpaces("Layer", res_print_data.WeldLayer);

                        // Min. O/L thk (mm)
                        if (IsNotWPP)
                        {
                            if (res_print_data.JointType == "Overlay" || "ESW" == WP || "ESSC" == WP)
                            {
                                print_string = print_string + GetStringWithWhiteSpaces("Min. O/L thk (mm)", res_print_data.MinOverlaythk);
                            }
                        }
                        print_string = print_string + GetStringWithWhiteSpaces("Consumable/F No.", res_print_data.AWSClass + " / " + res_print_data.FNumber);

                        if (IsNotWPP)
                        {
                            print_string = print_string + GetStringWithWhiteSpaces("Brand Name", res_print_data.FillerMetalTradeName);
                        }
                        // Flux  // FluxTradeName
                        if (res_print_data.JointType == "Overlay")
                        {
                            string[] temp1 = { "SAW_Overlay", "SASC_Overlay", "ESW_Overlay", "ESSC_Overlay" };
                            if (Array.Exists(temp1, element => element == WP + "_" + res_print_data.JointType))
                            {
                                print_string = print_string + GetStringWithWhiteSpaces("Flux", res_print_data.FluxTradeName);
                            }
                        }
                        else
                        {
                            string[] temp2 = { "SAW", "SASC", "ESW", "ESSC", "FCAW", "SAW-T" };
                            if (Array.Exists(temp2, element => element == WP))
                            {
                                print_string = print_string + GetStringWithWhiteSpaces("Flux", res_print_data.FluxTradeName);
                            }
                        }

                        // Position // OverlayPosition
                        if (res_print_data.JointType == "Overlay" || "ESW" == WP || "ESSC" == WP)
                        {
                            print_string = print_string + GetStringWithWhiteSpaces("Position", res_print_data.OverlayPosition);
                        }

                        // Groove / Fillet Position
                        if (IsNotWPP)
                        {
                            if (res_print_data.JointType != "Overlay")
                            {
                                string[] temp3 = { "SMAW", "SAW", "SASC", "GTAW", "HW-GTAW", "FCAW", "SAW-T" };
                                if (Array.Exists(temp3, element => element == WP))
                                {
                                    print_string = print_string + GetStringWithWhiteSpaces("Groove / Fillet Position", res_print_data.PositionofGrooveTTSJointWeldingProcess);
                                }
                            }
                            print_string = print_string + GetStringWithWhiteSpaces("Welding Progression", res_print_data.WeldingProgressionWeldingProcess);
                        }

                        print_string = print_string + GetStringWithWhiteSpaces("Preheat (°C)Min", res_print_data.Preheat.HasValue ? Convert.ToString(res_print_data.Preheat) : "0");
                        print_string = print_string + GetStringWithWhiteSpaces("Interpass (°C)Max", Convert.ToString(res_print_data.InterpassTemp));

                        // Preheat Maint.
                        if (IsNotWPP)
                        {
                            print_string = print_string + GetStringWithWhiteSpaces("Preheat Maint.", res_print_data.PrehearMaint);
                        }

                        // ShieldingGasGasType  // Shielding Gas
                        if ("GTAW" == WP || "HW-GTAW" == WP || "FCAW" == WP)
                        {
                            print_string = print_string + GetStringWithWhiteSpaces("Shielding Gas", res_print_data.ShieldingGasGasType);
                            print_string = print_string + GetStringWithWhiteSpaces("Comp.(%)/Flow Rate(LPM)", (printData.type == "WPP" ? res_print_data.ShieldingGasFlowRateLPM : res_print_data.ShieldingGasComposition + "/" + res_print_data.ShieldingGasFlowRateLPM));
                        }

                        // Backing Gas // BackingGasGasType // BackingGasComposition // BackingGasFlowRateLPM
                        if (IsNotWPP)
                        {
                            if (res_print_data.JointType != "Overlay")
                            {
                                string[] temp4 = { "GTAW", "HW-GTAW" };
                                if (Array.Exists(temp4, element => element == WP))
                                {
                                    print_string = print_string + GetStringWithWhiteSpaces("Backing Gas", res_print_data.BackingGasGasType);
                                    print_string = print_string + GetStringWithWhiteSpaces("Comp.(%)/Flow Rate(LPM)", res_print_data.BackingGasComposition + "/" + res_print_data.BackingGasFlowRateLPM);
                                }
                            }
                        }

                        // Trailing Gas // TrailingGasGasType // TrailingGasComposition // TrailingGasFlowRateLPM
                        if (res_print_data.JointType != "Overlay")
                        {
                            string[] tem = { "GTAW", "HW-GTAW" };
                            if (Array.Exists(tem, element => element == WP))
                            {
                                print_string = print_string + GetStringWithWhiteSpaces("Trailing Gas", res_print_data.TrailingGasGasType);
                                print_string = print_string + GetStringWithWhiteSpaces("Comp.(%)/Flow Rate(LPM)", res_print_data.TrailingGasComposition + "/" + res_print_data.TrailingGasFlowRateLPM);
                            }
                        }

                        print_string = print_string + GetStringWithWhiteSpaces("Size (mm)", res_print_data.FillerMetalSize);
                        print_string = print_string + GetStringWithWhiteSpaces("Current/ polarity", res_print_data.CurrentPolarity);
                        print_string = print_string + GetStringWithWhiteSpaces("Current (Amp)", res_print_data.CurrentAMP);
                        print_string = print_string + GetStringWithWhiteSpaces("Voltage (V)", res_print_data.Voltage);

                        // Bead Length (mm) // BeadLength
                        if ("SMAW" == WP)
                        {
                            print_string = print_string + GetStringWithWhiteSpaces("Bead Length (mm)", res_print_data.BeadLength);
                        }

                        // Travel Speed(mm/min) // TravelSpeed
                        string[] temp5 = { "SAW", "SASC", "GTAW", "HW-GTAW", "ESW", "ESSC", "FCAW", "SAW-T" };
                        if (Array.Exists(temp5, element => element == WP))
                        {
                            print_string = print_string + GetStringWithWhiteSpaces("Travel Speed(mm/min)", res_print_data.TravelSpeed);
                        }

                        if (IsNotWPP)
                        {
                            // HeatInput
                            if (res_print_data.JointType != "Overlay")
                            {
                                string[] temp6 = { "SMAW", "SAW", "SASC", "GTAW", "HW-GTAW", "FCAW", "SAW-T" };
                                if (Array.Exists(temp6, element => element == WP))
                                {
                                    print_string = print_string + GetStringWithWhiteSpaces("Max heat Input(kJ/mm)", res_print_data.HeatInput);
                                }
                            }

                            // WireFeedSpeed
                            string[] temp7 = { "GTAW_Overlay", "HW-GTAW_Overlay" };
                            if (Array.Exists(temp7, element => element == WP + "_" + res_print_data.JointType))
                            {
                                print_string = print_string + GetStringWithWhiteSpaces("Wire Feed (m/min)", res_print_data.WireFeedSpeed);
                            }

                            // Tungsten Type / Size (mm)  // TungstenSize
                            string[] temp8 = { "GTAW", "HW-GTAW" };
                            if (Array.Exists(temp8, element => element == WP))
                            {
                                print_string = print_string + GetStringWithWhiteSpaces("Tungsten Type / Size (mm)", res_print_data.TungstenSize);
                            }

                            print_string = print_string + GetStringWithWhiteSpaces("String/Weave", res_print_data.StringWeaveBead1);

                            // Multi/Single Elect. // MultiSingleElectrode
                            if (res_print_data.JointType == "Overlay")
                            {
                                string[] temp9 = { "SMAW_Overlay" };
                                if (Array.Exists(temp9, element => element == WP + "_" + res_print_data.JointType))
                                {
                                    print_string = print_string + GetStringWithWhiteSpaces("Multi/Single Elect.", res_print_data.MultiSingleElectrode);
                                }
                            }
                            else
                            {
                                string[] temp10 = { "SAW", "SASC", "GTAW", "HW-GTAW", "ESW", "ESSC", "FCAW", "SAW-T" };
                                if (Array.Exists(temp10, element => element == WP))
                                {
                                    print_string = print_string + GetStringWithWhiteSpaces("Multi/Single Elect.", res_print_data.MultiSingleElectrode);
                                }
                            }

                            // Electrode Spacing // ElectrodeSpacing
                            if ("SAW-T" == WP)
                            {
                                print_string = print_string + GetStringWithWhiteSpaces("Electrode Spacing", res_print_data.ElectrodeSpacing);
                            }

                            // Multi/Single Pass // MultipleSinglepassperside
                            if (res_print_data.JointType != "Overlay")
                            {
                                string[] temp11 = { "SMAW", "SAW", "SASC", "GTAW", "HW-GTAW", "SAW-T" };
                                if (Array.Exists(temp11, element => element == WP))
                                {
                                    print_string = print_string + GetStringWithWhiteSpaces("Multi/Single Pass", res_print_data.MultipleSinglepassperside);
                                }
                            }

                            // Multi/Single Layer // SingleMultipleLayer
                            if (res_print_data.JointType == "Overlay")
                            {
                                string[] temp12 = { "SMAW_Overlay", "SAW_Overlay", "SASC_Overlay", "GTAW_Overlay", "HW-GTAW_Overlay", "SAW-T_Overlay" };
                                if (Array.Exists(temp12, element => element == WP + "_" + res_print_data.JointType))
                                {
                                    print_string = print_string + GetStringWithWhiteSpaces("Multi/Single Layer", res_print_data.SingleMultipleLayer);
                                }
                            }
                            else
                            {
                                string[] temp13 = { "FCAW" };
                                if (Array.Exists(temp13, element => element == WP))
                                {
                                    print_string = print_string + GetStringWithWhiteSpaces("Multi/Single Layer", res_print_data.SingleMultipleLayer);
                                }
                            }

                            // Single/Multiple Layer
                            string[] temp14 = { "ESW", "ESSC" };
                            if (Array.Exists(temp14, element => element == WP))
                            {
                                print_string = print_string + GetStringWithWhiteSpaces("Single/Multiple Layer", res_print_data.SingleMultipleLayer);
                            }

                            // Metal Transfer mode // Modeofmetaltransfer
                            string[] temp15 = { "FCAW" };
                            if (Array.Exists(temp15, element => element == WP))
                            {
                                print_string = print_string + GetStringWithWhiteSpaces("Metal Transfer mode", res_print_data.Modeofmetaltransfer);
                            }

                            // Stick out (mm) // ContactTubeWorkDistance
                            string[] temp16 = { "SAW", "SASC", "ESW", "ESSC", "FCAW", "SAW-T" };
                            if (Array.Exists(temp16, element => element == WP))
                            {
                                print_string = print_string + GetStringWithWhiteSpaces("Stick out (mm)", res_print_data.ContactTubeWorkDistance);
                            }

                            // Supplemental device // SupplementalDevice
                            if (res_print_data.JointType == "Overlay")
                            {
                                string[] temp17 = { "SAW_Overlay", "SASC_Overlay" };
                                if (Array.Exists(temp17, element => element == WP + "_" + res_print_data.JointType))
                                {
                                    print_string = print_string + GetStringWithWhiteSpaces("Supplemental device", res_print_data.SupplementalDevice);
                                }
                            }
                            else
                            {
                                string[] temp18 = { "ESW", "ESSC" };
                                if (Array.Exists(temp18, element => element == WP))
                                {
                                    print_string = print_string + GetStringWithWhiteSpaces("Supplemental device", res_print_data.SupplementalDevice);
                                }
                            }
                        }

                        string notes_text = "Refer " + (printData.type == "WPP" ? "WPP" : "SWP") + " notes nos/text";
                        notes_text = notes_text + GetWhiteSpaces(24);
                        print_string = print_string + notes_text;

                        print_string = print_string + "------------------------------------------------";
                        res_print_data.NoteDescription = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";
                        if (!string.IsNullOrEmpty(res_print_data.NoteDescription) && !string.IsNullOrWhiteSpace(res_print_data.NoteDescription))
                        {
                            print_string = print_string + "                     Notes                      ";
                            print_string = print_string + res_print_data.NoteDescription + "\n";
                            print_string = print_string + "------------------------------------------------";
                        }


                        // Supervisor Name
                        print_string = print_string + GetStringWithWhiteSpaces("Supervisor Name", res_print_data.SupervisorName);
                        print_string = print_string + GetStringWithWhiteSpaces("Supervisor P.S Number", res_print_data.SupervisorPSNo);

                        if (IsNotWPP)
                        {
                            print_string = print_string + GetStringWithWhiteSpaces("Valid Upto", Convert.ToString(res_print_data.ValidUpto));
                        }
                    }
                    #endregion

                    return Json(new
                    {
                        Key = true,
                        Value = "Success",
                        data = res_print_data,
                        header_print_string = header_print_string,
                        print_string = print_string,
                        QRCodeImgScr = QRCodeImgScr
                    }, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    return Json(new
                    {
                        Key = false,
                        Value = "No data found.",
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();

                return Json(new
                {
                    Key = false,
                    Value = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        static List<string> Split(string str, int chunkSize)
        {
            List<string> arrString = new List<string>();
            for (int i = 0; i < str.Length; i += chunkSize)
            {
                if (i + chunkSize > str.Length) chunkSize = str.Length - i;
                arrString.Add(str.Substring(i, chunkSize));

            }
            return arrString;
        }

        // ONLY USED IN MOBILE APPLICATION
        public SP_RPT_WELDERPARAMETERSLIP_Result GetHtmlPSlipDataMobile(int? id, clsUpdateAndGetPrintDataMobile printData)
        {
            SP_RPT_WELDERPARAMETERSLIP_Result data;
            try
            {
                data = db.SP_RPT_WELDERPARAMETERSLIP(printData.weldingProcess, printData.weldLayer, printData.type, id, printData.aWSClass, printData.fMSize, printData.WPSNumber, printData.WPSRevNo).FirstOrDefault();
                if (data == null)
                {
                    data = new SP_RPT_WELDERPARAMETERSLIP_Result();
                    data.WeldingProcess = printData.weldingProcess;
                }
                return data;
            }
            catch (Exception ex)
            {
                data = new SP_RPT_WELDERPARAMETERSLIP_Result();
                return data;
            }
        }


        public class clsTask
        {
            public string id { get; set; }
            public string text { get; set; }
        }

        public class clsWPSWeldPorcess
        {
            public string weldingProcess1 { get; set; }
            public string weldingProcess2 { get; set; }
            public string weldingProcess3 { get; set; }
            public string weldingProcess4 { get; set; }
        }
        public class ResponceMsgWithStatus : clsHelper.ResponseMsg
        {
            public List<clsTask> lstWeldingProcess { get; set; }
            public List<clsTask> lstSeam { get; set; }
            public List<clsTask> lstLayer { get; set; }
            public List<clsTask> lstWPS { get; set; }
            public string list { get; set; }
            public string BU { get; set; }
            public string Project { get; set; }
            public string WPSRev { get; set; }
            public string JointType { get; set; }
            public string WPSNum { get; set; }
            public string projdesc { get; set; }
            public string appdesc { get; set; }
            public string BUdesc { get; set; }
            public int Id { get; set; }

        }

        public class clsUpdateAndGetPrintDataMobile
        {
            public int Id { get; set; }
            public string JointType { get; set; }
            public string weldingProcess { get; set; }
            public string weldLayer { get; set; }
            public string type { get; set; }
            public string aWSClass { get; set; }
            public string fMSize { get; set; }
            public string WPSNumber { get; set; }
            public int WPSRevNo { get; set; }
            public string UserName { get; set; }
        }

        public class CBL002HtmlSleep
        {
            public string SizeMM { get; set; }
            public string BatchNoWire { get; set; }
            public string BatchNoFlux { get; set; }
            public string BatchNo { get; set; }
            public string BatchFlux { get; set; }
            public string WeldingProcess { get; set; }
            public Int64 HeaderID { get; set; }



        }
    }

}
