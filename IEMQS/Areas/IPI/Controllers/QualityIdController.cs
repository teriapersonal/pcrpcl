﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQS.Models;
using IEMQS.Areas.NDE.Models;
using System.Data.Entity.Core.Objects;
using IEMQS.Areas.IPI.Models;
using IEMQS.Areas.Utility.Models;

namespace IEMQS.Areas.IPI.Controllers
{
    public class QualityIdController : clsBase
    {
        // GET: IPI/QualityId
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetQualityIDGridDataPartial(string status)
        {
            ViewBag.status = status;
            return PartialView("_GetQualityIDGridDataPartial");
        }

        public ActionResult LoadHeaderDataTable(JQueryDataTableParamModel param, string status)
        {
            try
            {
                var role = (from a in db.ATH001
                            join b in db.ATH004 on a.Role equals b.Id
                            where a.Employee.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                            select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;



                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms15.BU", "qms15.Location");
                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and qms15.Status in('" + clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.QualityIdHeaderStatus.Returned.GetStringValue() + "')";
                }
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or qms15.QualityProject like '%" + param.sSearch + "%'or (qms15.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%')";
                }
                #region Condition For QID Type
                if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.WE3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    whereCondition += " and qms15.QualityIdApplicableFor in('" + clsImplementationEnum.QualityIDApplicable.SEAM.GetStringValue() + "')";
                }
                else if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    whereCondition += " and qms15.QualityIdApplicableFor in('" + clsImplementationEnum.QualityIDApplicable.PART.GetStringValue() + "', '" + clsImplementationEnum.QualityIDApplicable.ASSEMBLY.GetStringValue() + "')";
                }
                #endregion

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                //  var lstHeader = db.SP_IPI_GetQualityIDHeader(startIndex, endIndex, "", whereCondition).ToList();
                var lstHeader = db.SP_IPI_GET_QUALITYPROJECT_RESULT(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstHeader
                          select new[] {

                        a.QualityProject,
                        a.Project,
                        a.BU,
                        a.Location,
                        Convert.ToString(a.HasLines),
                        Convert.ToString(a.HeaderId),
                        ////a.BU,
                        ////a.Project,
                        //a.QualityId,
                        //a.QualityIdDesc,
                        //Convert.ToString(a.RevNo),
                        //a.Status,
                        //a.CreatedBy,
                        //a.EditedBy,
                        
                        //a.Status,
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        #region QualityId Header
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult AddQualityIdHeader(int? id)
        {
            QMS015 objQMS015 = new QMS015();
            //var location = (from a in db.COM003
            //                join b in db.COM002 on a.t_loca equals b.t_dimx
            //                where b.t_dtyp == 1 && a.t_psno.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
            //                select b.t_dimx + " - " + b.t_desc).FirstOrDefault();

            //ViewBag.Location = location;


            if (id > 0)
            {
                objQMS015 = db.QMS015.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.Project = db.COM001.Where(i => i.t_cprj.Equals(objQMS015.Project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + "-" + i.t_dsca).FirstOrDefault();
                ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objQMS015.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                //ViewBag.IdenticalProject = string.Join(",", db.QMS017.Where(i => i.HeaderId == id).Select(i => i.IQualityProject).ToList());
            }
            else
            {
                objQMS015.HeaderId = 0;
                objQMS015.Status = clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue();
                objQMS015.RevNo = 0;
                ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objClsLoginInfo.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            }

            #region Check For Indentical Project Editable or not
            if (!string.IsNullOrEmpty(objQMS015.QualityProject))
            {
                ViewBag.IdenticalProjectEditable = CheckIdenticalProjectEditable(objQMS015.QualityProject);
            }
            #endregion

            return View(objQMS015);
        }

        public bool CheckIdenticalProjectEditable(string QualityProject)
        {
            string whereCondition = "1=1";
            whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms15.BU", "qms15.Location");

            whereCondition += " AND UPPER(QualityProject) = '" + QualityProject.ToUpper() + "'";

            var lstResult = db.SP_IPI_GetQualityIDHeader(1, 100000, "", whereCondition).ToList();
            if (lstResult.Any(c => c.Status != clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue() || c.RevNo > 0))
            {
                return false;
            }
            return true;
        }

        [HttpPost]
        public ActionResult IsIdenticalProjectEditable(string QualityProject)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            objResponseMsg.Key = CheckIdenticalProjectEditable(QualityProject);
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveHeader(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            QMS015 objQMS015 = new QMS015();
            int newRowIndex = 0;
            try
            {
                string strProject = !string.IsNullOrEmpty(fc["Project"]) ? fc["Project"].Split('-')[0] : string.Empty;
                string strBU = !string.IsNullOrEmpty(fc["BU"]) ? fc["BU"].Split('-')[0] : string.Empty;
                string strLocation = !string.IsNullOrEmpty(fc["Location"]) ? fc["Location"].Split('-')[0] : string.Empty;
                string strQualityProject = !string.IsNullOrEmpty(fc["QualityProject"]) ? fc["QualityProject"] : string.Empty;
                string strQualityId = !string.IsNullOrEmpty(fc["QualityId" + newRowIndex]) ? fc["QualityId" + newRowIndex] : string.Empty;
                string hId = fc["HeaderId" + newRowIndex];
                int headerId = string.IsNullOrEmpty(fc["HeaderId0"]) ? 0 : Convert.ToInt32(fc["HeaderId0"]);// fc["QualityIdDesc" + newRowIndex]
                string strIdenticalProj = !string.IsNullOrEmpty(fc["IdenticalProject"]) ? fc["IdenticalProject"] : string.Empty;

                if (fc != null)
                {
                    if (isQualityIdExist(strQualityProject, strProject, strBU, strLocation, strQualityId, headerId))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Quality Id " + strQualityId + " for " + strQualityProject + "  already exist";
                    }
                    else
                    {
                        string value = string.Empty;
                        if (headerId > 0)
                        {
                            #region Update Existing QualityIdHeader
                            objQMS015 = db.QMS015.Where(i => i.HeaderId == headerId).FirstOrDefault();
                            if (!string.Equals(objQMS015.Status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
                            {

                                objQMS015.QualityIdDesc = fc["QualityIdDesc"];
                                objQMS015.QualityIdApplicableFor = fc["QualityIdApplicableFor"];
                                objQMS015.EditedBy = objClsLoginInfo.UserName;
                                objQMS015.EditedOn = DateTime.Now;

                                if (string.Equals(objQMS015.Status, clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue()))
                                {
                                    //var lstLines = db.QMS016.Where(i => i.HeaderId == headerId).ToList();
                                    //if (lstLines.Any())
                                    //    lstLines.ForEach(i => i.RevNo = objQMS015.RevNo + 1);

                                    ////var lstIdenticalProj = db.QMS017.Where(i => i.HeaderId == headerId).ToList();
                                    ////if (lstIdenticalProj.Any())
                                    ////    lstIdenticalProj.ForEach(i => i.RevNo = objQMS015.RevNo + 1);

                                    //objQMS015.RevNo += 1;
                                    //objQMS015.Status = clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue();
                                    objResponseMsg = ReviseHeaderWithLines(objQMS015.HeaderId, false, 0);
                                    objQMS015.Status = objResponseMsg.HeaderStatus;
                                    objQMS015.RevNo = Convert.ToInt32(objResponseMsg.RevNo);
                                }

                                db.SaveChanges();

                                objResponseMsg.Key = true;
                                value = clsImplementationMessage.QualityIDMessage.Update;
                            }
                            else
                            {
                                objResponseMsg.Remarks = clsImplementationMessage.CommonMessages.NotInEditStatus;
                            }

                            objResponseMsg.HeaderId = objQMS015.HeaderId;
                            objResponseMsg.HeaderStatus = objQMS015.Status;
                            objResponseMsg.RevNo = Convert.ToString(objQMS015.RevNo);
                            #endregion
                        }
                        else
                        {
                            #region Add New QualityID Header
                            objQMS015.QualityProject = fc["QualityProject"];
                            objQMS015.Project = fc["Project"].Split('-')[0];
                            objQMS015.BU = fc["BU"].Split('-')[0];
                            objQMS015.Location = fc["Location"].Split('-')[0];
                            objQMS015.QualityId = Convert.ToString(fc["QualityId" + newRowIndex]).ToUpper();
                            objQMS015.QualityIdDesc = fc["QualityIdDesc" + newRowIndex];
                            objQMS015.QualityIdApplicableFor = fc["QualityIdApplicableFor" + newRowIndex];
                            objQMS015.RevNo = 0;
                            objQMS015.Status = clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue();
                            objQMS015.CreatedBy = objClsLoginInfo.UserName;
                            objQMS015.CreatedOn = DateTime.Now;

                            db.QMS015.Add(objQMS015);
                            db.SaveChanges();

                            objResponseMsg.Key = true;
                            value = clsImplementationMessage.QualityIDMessage.Save;
                            objResponseMsg.HeaderId = objQMS015.HeaderId;
                            objResponseMsg.HeaderStatus = objQMS015.Status;
                            objResponseMsg.RevNo = Convert.ToString(objQMS015.RevNo);
                            #endregion
                        }

                        //if (!string.IsNullOrEmpty(strIdenticalProj))
                        //{
                        //    objResponseMsg = SaveIdenticalProject(objQMS015, strIdenticalProj);
                        //    objResponseMsg.Value = value;
                        //    objResponseMsg.HeaderId = objQMS015.HeaderId;
                        //    objResponseMsg.HeaderStatus = objQMS015.Status;
                        //    objResponseMsg.RevNo = Convert.ToString(objQMS015.RevNo);
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetQualityIDDataPartial(string Status, string QualityProject, string Location, string BU)
        {
            List<string> lstQualityIdApplicable = clsImplementationEnum.GetQualityIdApplicableCategory().ToList();

            var role = (from a in db.ATH001
                        join b in db.ATH004 on a.Role equals b.Id
                        where a.Employee.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase) && a.Location == Location && a.BU == BU
                        select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();

            if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.WE3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
            {
                //var DefaultEncoderType = lstQualityIdApplicable.Where(i => i.Equals(clsImplementationEnum.QualityIDApplicable.SEAM.GetStringValue())).FirstOrDefault();
                ViewBag.QualityIdApplicable = lstQualityIdApplicable.Where(i => i.Equals(clsImplementationEnum.QualityIDApplicable.SEAM.GetStringValue())).Select(x => new CategoryData { Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            }
            else if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
            {
                string[] arrQualityIdCategory = { clsImplementationEnum.QualityIDApplicable.PART.GetStringValue(), clsImplementationEnum.QualityIDApplicable.ASSEMBLY.GetStringValue() };
                ViewBag.QualityIdApplicable = lstQualityIdApplicable.AsEnumerable().Where(i => arrQualityIdCategory.Contains(i.ToString(), StringComparer.OrdinalIgnoreCase)).Select(x => new CategoryData { Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();
            }

            ViewBag.status = Status;
            ViewBag.qualityProject = QualityProject;
            ViewBag.location = Location;
            ViewBag.bu = BU;

            return PartialView("_GetQualityIDDataPartial");
        }

        [HttpPost]
        public ActionResult GetQualityIDData(JQueryDataTableParamModel param, string qualityProj, string status, string location, string bu)
        {
            try
            {
                var role = (from a in db.ATH001
                            join b in db.ATH004 on a.Role equals b.Id
                            where a.Employee.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase) && a.Location == location && a.BU == bu
                            select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";
                //whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms15.BU", "qms15.Location");
                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(bu))
                {
                    whereCondition += " AND qms15.BU IN('" + bu + "')";
                }
                if (!string.IsNullOrEmpty(location))
                {
                    whereCondition += " AND qms15.Location IN('" + location + "')";
                }
                if (!string.IsNullOrEmpty(qualityProj))
                {
                    whereCondition += " AND UPPER(QualityProject) = '" + qualityProj.ToUpper() + "'";
                }
                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and qms15.Status in('" + clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.QualityIdHeaderStatus.Returned.GetStringValue() + "')";
                }

                #region Condition For QID Type
                if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.WE3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    whereCondition += " and qms15.QualityIdApplicableFor in('" + clsImplementationEnum.QualityIDApplicable.SEAM.GetStringValue() + "')";
                }
                else if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    whereCondition += " and qms15.QualityIdApplicableFor in('" + clsImplementationEnum.QualityIDApplicable.PART.GetStringValue() + "', '" + clsImplementationEnum.QualityIDApplicable.ASSEMBLY.GetStringValue() + "')";
                }
                #endregion


                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (qms15.QualityId like '%" + param.sSearch
                        + "%' or qms15.QualityIdDesc like '%" + param.sSearch
                        + "%' or qms15.QualityIdApplicableFor like '%" + param.sSearch
                        + "%' or qms15.RevNo like '%" + param.sSearch
                        + "%' or qms15.Status like '%" + param.sSearch
                        + "%' or ecom3.t_psno like '%" + param.sSearch
                        + "%' or ecom3.t_name like '%" + param.sSearch
                        + "%')";
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var lstResult = db.SP_IPI_GetQualityIDHeader(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                List<SelectListItem> BoolenList = new List<SelectListItem>() { new SelectListItem { Text = "Yes", Value = "Yes" }, new SelectListItem { Text = "No", Value = "No" } };

                //var Location = (from a in db.COM002
                //                where a.t_dtyp == 1 && a.t_dimx != ""
                //                select new { a.t_dimx, Desc = a.t_desc }).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                    "",
                                    Helper.GenerateHidden(newRecordId, "HeaderId", ""),
                                    Helper.GenerateHidden(newRecordId,"HasLines","false"),
                                    Helper.GenerateTextbox(newRecordId, "QualityId", "", "", false, "", "20"),
                                    Helper.GenerateTextbox(newRecordId, "QualityIdDesc", "", "", false, "", "40"),
                                    Helper.GenerateTextbox(newRecordId, "QualityIdApplicableFor", role.Any(i => i.RoleDesc.Equals("we3", StringComparison.OrdinalIgnoreCase)) ? "Seam" : "", "", false, "", "15"),
                                    "R0",
                                    "",
                                    clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue(),
                                    GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveHeader();" ),
                           

                                    //HTMLActionString(newRecordId,clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue(),"Add","Add Rocord","fa fa-plus","SaveHeader();"),
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                                Convert.ToString(uc.HeaderId),
                                  Helper.GenerateHidden(uc.HeaderId, "HeaderId", Convert.ToString(uc.HeaderId)),
                               Helper.GenerateHidden(uc.HeaderId,"HasLines",Convert.ToString(uc.HasLines)),                         
                               //Helper.GenerateHTMLTextbox(uc.HeaderId, "QualityId", Convert.ToString(uc.QualityId), "UpdateHeader(this, "+ uc.HeaderId +");",false,"",string.Equals(uc.Status,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue())?true:false),
                               Convert.ToString(uc.QualityId),
                               ((uc.Status == clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue() || uc.Status == clsImplementationEnum.QualityIdHeaderStatus.Returned.GetStringValue() )&& uc.RevNo == 0 ? Helper.GenerateHTMLTextboxOnChanged(uc.HeaderId, "QualityIdDesc", Convert.ToString(uc.QualityIdDesc), "UpdateHeader(this, "+ uc.HeaderId +");",false,"",string.Equals(uc.Status,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue())?true:false, "40") : Convert.ToString(uc.QualityIdDesc)),
                               //Helper.GenerateHTMLTextbox(uc.HeaderId, "QualityIdApplicableFor", Convert.ToString(uc.QualityIdApplicableFor),"UpdateHeader(this, "+ uc.HeaderId +");",false,"",string.Equals(uc.Status,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue())?true:false),
                               Convert.ToString(uc.QualityIdApplicableFor),
                               "R"+Convert.ToString(uc.RevNo),
                               Convert.ToString(uc.ReturnRemark),
                               Convert.ToString(uc.Status),
                               //Helper.GenerateGridButton(uc.HeaderId, "Delete", "Delete Rocord", "fa fa-trash-o", "DeleteQualityID("+ uc.HeaderId +","+uc.HasLines+");")+"|"+Helper.GenerateGridButton(uc.HeaderId, "AddLine", "Add Line", "fa fa-plus", "GetQualityIDLinesPartial("+ uc.HeaderId +",0);"),onClick="kickUser(\"'.$rowUsers['id_user'].'\")">Kick</a>
                               "<center>"+((uc.Status == clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue() || uc.Status == clsImplementationEnum.QualityIdHeaderStatus.Returned.GetStringValue()) && uc.RevNo == 0 ? HTMLActionString(uc.HeaderId,uc.Status,"Delete","Delete QualityID","fa fa-trash-o","DeleteQualityID("+ uc.HeaderId +","+uc.HasLines.ToString().ToLower()+");") : HTMLActionString(uc.HeaderId,uc.Status,"Delete","Delete QualityID","fa fa-trash-o","",true))
                               +""+"<a  href='javascript:void(0)' onclick=ShowTimeline('/IPI/QualityId/ShowTimeline?HeaderID="+Convert.ToInt32(uc.HeaderId)+"');><i style = 'margin-left:5px;' class='fa fa-clock-o'></i></a>"
                               +""+(!string.IsNullOrEmpty(uc.Status)?(uc.RevNo>0 ? HTMLActionString(uc.HeaderId,uc.Status,"HistoryQualityID","History","fa fa-history","HistoryDetailsPartial(\""+ uc.HeaderId +"\",\""+Helper.replaceQuote(uc.QualityId)+"\");"):HTMLActionString(uc.HeaderId,uc.Status,"HistoryQualityID","History","fa fa-history","",true)):HTMLActionString(uc.HeaderId,uc.Status,"HistoryQualityID","History","fa fa-history","",true))
                               +""+(uc.HasLines.Value ? HTMLActionString(uc.HeaderId,uc.Status,"CopyQualityID","Copy QualityID","fa fa-files-o","CopyQualityIDPartial("+uc.HeaderId+",\""+ uc.QualityProject +"\");"):  HTMLActionString(uc.HeaderId,uc.Status,"CopyQualityID","Copy QualityID","fa fa-files-o","",true))
                               +"</center>",                              
                               //+"|"+HTMLActionString(uc.HeaderId,uc.Status,"AddLine","Add Line","fa fa-plus","GetQualityIDLinesPartial("+ uc.HeaderId +",0);")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdateHeader(int headerId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            QMS015 objQMS015 = new QMS015();
            try
            {
                if (headerId > 0)
                    objQMS015 = db.QMS015.Where(i => i.HeaderId == headerId).FirstOrDefault();
                if (!string.Equals(objQMS015.Status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
                {
                    if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                    {
                        db.SP_IPI_UPDATE_COLUMN(headerId, columnName, columnValue);
                    }
                    if (string.Equals(objQMS015.Status, clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                    {
                        //objQMS015.Status = clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue();
                        //if (db.QMS016.Where(i => i.HeaderId == headerId).Any())
                        //{
                        //    db.QMS016.Where(i => i.HeaderId == headerId).ToList().ForEach(i => i.RevNo = objQMS015.RevNo + 1);
                        //}
                        //objQMS015.RevNo += 1;
                        objResponseMsg = ReviseHeaderWithLines(objQMS015.HeaderId, false, 0);
                        objQMS015.Status = objResponseMsg.HeaderStatus;
                        objQMS015.RevNo = Convert.ToInt32(objResponseMsg.RevNo);
                    }
                    objQMS015.EditedBy = objClsLoginInfo.UserName;
                    objQMS015.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Value = "Saved Successfully";
                }
                else
                {
                    objResponseMsg.Remarks = clsImplementationMessage.CommonMessages.NotInEditStatus;
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteQID(string strHeader, bool hasLines)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            QMS015 objQMS015 = new QMS015();
            List<QMS015> lstQMS015 = new List<QMS015>();
            List<QMS016> lstQMS016 = new List<QMS016>();

            int inQIDDeleted = 0, inQIDWithLines = 0;

            try
            {
                if (!string.IsNullOrEmpty(strHeader))
                {
                    int[] headerIds = Array.ConvertAll(strHeader.Split(','), i => Convert.ToInt32(i));
                    foreach (var headerId in headerIds)
                    {
                        if (headerId > 0)
                        {
                            objQMS015 = db.QMS015.Where(i => i.HeaderId == headerId).FirstOrDefault();
                            if (string.Equals(objQMS015.Status, clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue()) || string.Equals(objQMS015.Status, clsImplementationEnum.QualityIdHeaderStatus.Returned.GetStringValue()))
                            {
                                #region Revision exists
                                if (objQMS015.RevNo > 0)
                                {
                                    var lstLines = db.QMS016.Where(i => i.HeaderId == headerId).ToList();
                                    if (lstLines.Any())
                                        db.QMS016.RemoveRange(lstLines);

                                    //var lstIdenticalProject = db.QMS017.Where(i => i.HeaderId == headerId).ToList();
                                    //if (lstIdenticalProject.Any())
                                    //    db.QMS017.RemoveRange(lstIdenticalProject);
                                    //db.QMS015.Remove(objQMS015);

                                    db.SaveChanges();

                                    #region Restoring Previous Version

                                    var histHeader = db.QMS015_Log.OrderByDescending(x => x.RevNo).FirstOrDefault();
                                    var histLines = db.QMS016_Log.Where(i => i.RefId == histHeader.Id && i.HeaderId == headerId).ToList();

                                    objQMS015.HeaderId = histHeader.HeaderId;
                                    objQMS015.QualityProject = histHeader.QualityProject;
                                    objQMS015.Project = histHeader.Project;
                                    objQMS015.BU = histHeader.BU;
                                    objQMS015.Location = histHeader.Location;
                                    objQMS015.QualityId = histHeader.QualityId;
                                    objQMS015.QualityIdApplicableFor = histHeader.QualityIdApplicableFor;
                                    objQMS015.RevNo = histHeader.RevNo + 1;
                                    objQMS015.Status = clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue();
                                    objQMS015.CreatedBy = objClsLoginInfo.UserName;
                                    objQMS015.CreatedOn = DateTime.Now;

                                    db.SaveChanges();

                                    db.QMS016.AddRange(db.QMS016_Log.Where(i => i.RefId == histHeader.Id && headerId == histHeader.HeaderId).Select(i => new QMS016
                                    {
                                        LineId = i.LineId,
                                        HeaderId = histHeader.HeaderId,
                                        QualityProject = histHeader.QualityProject,
                                        Project = histHeader.Project,
                                        BU = histHeader.BU,
                                        Location = histHeader.Location,
                                        QualityId = histHeader.QualityId,
                                        RevNo = objQMS015.RevNo,
                                        StageCode = i.StageCode,
                                        StageSequance = i.StageSequance,
                                        StageStatus = clsImplementationEnum.QualityIdStageStatus.ADDED.GetStringValue(),
                                        AcceptanceStandard = i.AcceptanceStandard,
                                        ApplicableSpecification = i.ApplicableSpecification,
                                        CreatedBy = objClsLoginInfo.UserName,
                                        CreatedOn = DateTime.Now
                                    }).ToList());

                                    //var histIdenticalProj = db.QMS017_Log.Where(i => i.RefId == histHeader.Id && headerId == histHeader.HeaderId).ToList();
                                    //if (histIdenticalProj.Any())
                                    //{
                                    //    db.QMS017.AddRange(histIdenticalProj.Select(i => new QMS017
                                    //    {
                                    //        LineId = i.LineId,
                                    //        HeaderId = objQMS015.HeaderId,
                                    //        QualityProject = i.QualityProject,
                                    //        Project = i.Project,
                                    //        BU = i.BU,
                                    //        Location = i.Location,
                                    //        //QualityId = i.QualityId,
                                    //        RevNo = objQMS015.RevNo,
                                    //        IQualityProject = i.IQualityProject,
                                    //        CreatedBy = objClsLoginInfo.UserName,
                                    //        CreatedOn = DateTime.Now
                                    //    }));
                                    //}
                                    #endregion

                                    db.SaveChanges();
                                    inQIDDeleted++;

                                }
                                #endregion
                                else
                                {
                                    var lstLines = db.QMS016.Where(i => i.HeaderId == headerId).ToList();
                                    if (lstLines.Any())
                                    {
                                        foreach (var item in lstLines)
                                        {
                                            List<QMS019> lstElement = db.QMS019.Where(x => x.QIDLineId == item.LineId).ToList();
                                            db.QMS019.RemoveRange(lstElement);
                                            List<QMS018> lstSamplingPlan = db.QMS018.Where(x => x.QIDLineId == item.LineId).ToList();
                                            db.QMS018.RemoveRange(lstSamplingPlan);
                                        }
                                        if (hasLines)
                                        {
                                            db.QMS016.RemoveRange(lstLines);
                                            //var lstIProject = db.QMS017.Where(i => i.HeaderId == headerId).ToList();
                                            //if (lstIProject.Any())
                                            //{
                                            //    db.QMS017.RemoveRange(lstIProject);
                                            //}
                                            db.QMS015.Remove(objQMS015);
                                            db.SaveChanges();
                                            inQIDDeleted++;
                                        }
                                        else
                                        {
                                            inQIDWithLines++;
                                        }
                                    }
                                    else
                                    {
                                        //var lstIProject = db.QMS017.Where(i => i.HeaderId == headerId).ToList();
                                        //if (lstIProject.Any())
                                        //{
                                        //    db.QMS017.RemoveRange(lstIProject);
                                        //}
                                        db.QMS015.Remove(objQMS015);
                                        db.SaveChanges();
                                        inQIDDeleted++;
                                    }

                                    //if (hasLines)
                                    //{
                                    //    var lstLines = db.QMS016.Where(i => i.HeaderId == headerId).ToList();
                                    //    if (lstLines.Any())
                                    //        db.QMS016.RemoveRange(lstLines);
                                    //    //lstQMS016.AddRange(lstLines);
                                    //    db.QMS015.Remove(objQMS015);
                                    //    db.SaveChanges();
                                    //    inQIDDeleted++;
                                    //}
                                    //else
                                    //{
                                    //    var lstLines = db.QMS016.Where(i => i.HeaderId == headerId).ToList();
                                    //    if (lstLines.Any())
                                    //    { }
                                    //    else
                                    //    { }
                                    //    db.QMS015.Remove(objQMS015);
                                    //    db.SaveChanges();
                                    //    //inQIDDeleted++;
                                    //}
                                    //db.QMS015.Remove(objQMS015);
                                    //db.SaveChanges();
                                }

                            }
                            else
                            {
                                objResponseMsg.Remarks = "This data is already submitted, it cannot be delete now!!";
                            }
                        }
                    }

                    if (inQIDWithLines > 0)
                    {
                        objResponseMsg.Status = string.Format("{0} QID(s) is/are having lines.", inQIDWithLines);
                    }
                    if (inQIDDeleted > 0)
                    {
                        objResponseMsg.Value = string.Format(clsImplementationMessage.QualityIDMessage.Delete, inQIDDeleted);
                    }
                    objResponseMsg.Key = true;

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }


            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveIdenticalProject(string qualityProject, string strIdenticalProj)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            List<QMS017> lstExistingProject = new List<QMS017>();
            List<QMS017> lstQMS017 = new List<QMS017>();
            try
            {
                var location = (from a in db.COM003
                                join b in db.COM002 on a.t_loca equals b.t_dimx
                                where b.t_dtyp == 1 && a.t_psno.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                                select b.t_dimx).FirstOrDefault();

                if (!string.IsNullOrEmpty(strIdenticalProj))
                {
                    string[] arrIdenticalProj = strIdenticalProj.Split(',');
                    lstExistingProject = db.QMS017.Where(i => i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase)).ToList();
                    //&& i.Location.Equals(location, StringComparison.OrdinalIgnoreCase)

                    if (lstExistingProject.Any())
                    {
                        db.QMS017.RemoveRange(lstExistingProject);
                        db.SaveChanges();
                    }
                    foreach (var item in arrIdenticalProj)
                    {
                        lstQMS017.Add(new QMS017
                        {
                            QualityProject = qualityProject,
                            Project = GetProjectBU(qualityProject).Project,
                            BU = GetProjectBU(qualityProject).BU,
                            Location = location,
                            IQualityProject = item,
                            CreatedBy = objClsLoginInfo.UserName,
                            CreatedOn = DateTime.Now
                        });
                    }
                    db.QMS017.AddRange(lstQMS017);
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Identical Project Saved Successfully";

                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Identical Project is not available";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        public ModelProjectBU GetProjectBU(string qualityProject)
        {
            ModelProjectBU objModelProjectBU = new ModelProjectBU();
            objModelProjectBU.Project = db.QMS010.Where(i => i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase)).Select(i => i.Project).FirstOrDefault();
            objModelProjectBU.BU = db.COM001.Where(i => i.t_cprj == objModelProjectBU.Project).FirstOrDefault().t_entu;

            return objModelProjectBU;
        }

        public clsHelper.ResponseMsgWithStatus SaveIdenticalProject(QMS015 objQMS015, string strIdenticalProj)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            List<QMS017> lstQMS017 = new List<QMS017>();
            List<string> lstExistingProj = new List<string>();
            try
            {
                string[] arrIdenticalProj = strIdenticalProj.Split(',');

                var lstExistingQualityProj = db.QMS017.Where(i => i.HeaderId == objQMS015.HeaderId).ToList();
                if (lstExistingQualityProj.Any())
                {
                    db.QMS017.RemoveRange(lstExistingQualityProj);
                    db.SaveChanges();
                }
                foreach (var item in arrIdenticalProj)
                {
                    if (isIdenticalProjectExist(objQMS015.QualityProject, objQMS015.BU, objQMS015.Location, objQMS015.QualityId, item))
                    {
                        lstExistingProj.Add(item);
                    }
                    else
                    {
                        QMS017 objQMS017 = new QMS017();
                        objQMS017.HeaderId = objQMS015.HeaderId;
                        objQMS017.QualityProject = objQMS015.QualityProject;
                        objQMS017.Project = objQMS015.Project;
                        objQMS017.BU = objQMS015.BU;
                        objQMS017.Location = objQMS015.Location;
                        //objQMS017.QualityId = objQMS015.QualityId;
                        objQMS017.RevNo = objQMS015.RevNo;
                        objQMS017.IQualityProject = item;
                        objQMS017.CreatedBy = objClsLoginInfo.UserName;
                        objQMS017.CreatedOn = DateTime.Now;

                        lstQMS017.Add(objQMS017);
                    }
                    //db.QMS017.Add()
                }

                if (lstQMS017.Any())
                {
                    db.QMS017.AddRange(lstQMS017);
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    //objResponseMsg.Value = "Identical Project Added Successfully";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return objResponseMsg;
        }
        public clsHelper.ResponseMsg SaveIdenticalProject_Old(QMS015 objQMS015, string strIdenticalProj, bool isUpdate)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            List<QMS017> lstQMS017 = new List<QMS017>();
            List<string> lstExistingProj = new List<string>();
            try
            {
                string[] arrIdenticalProj = strIdenticalProj.Split(',');

                foreach (var item in arrIdenticalProj)
                {
                    if (isIdenticalProjectExist(objQMS015.QualityProject, objQMS015.BU, objQMS015.Location, objQMS015.QualityId, item))
                    {
                        lstExistingProj.Add(item);
                    }
                    else
                    {
                        QMS017 objQMS017 = new QMS017();
                        objQMS017.HeaderId = objQMS015.HeaderId;
                        objQMS017.QualityProject = objQMS015.QualityProject;
                        objQMS017.Project = objQMS015.Project;
                        objQMS017.BU = objQMS015.BU;
                        objQMS017.Location = objQMS015.Location;
                        //objQMS017.QualityId = objQMS015.QualityId;
                        objQMS017.RevNo = objQMS015.RevNo;
                        objQMS017.IQualityProject = item;
                        objQMS017.CreatedBy = objClsLoginInfo.UserName;
                        objQMS017.CreatedOn = DateTime.Now;

                        lstQMS017.Add(objQMS017);
                    }
                    //db.QMS017.Add()
                }

                if (lstQMS017.Any())
                {
                    db.QMS017.AddRange(lstQMS017);
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Identical Project Added Successfully";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return objResponseMsg;
        }
        #endregion

        #region QualityId Stages
        [HttpPost]
        public ActionResult GetQualityIDLinesPartial(int headerId)
        {
            QMS016 objQMS016 = new QMS016();
            QMS015 objQMS015 = new QMS015();
            if (headerId > 0)
                objQMS015 = db.QMS015.Where(i => i.HeaderId == headerId).FirstOrDefault();

            var lstAcceptanceStandard = Manager.GetSubCatagories("Acceptance Standard", objQMS015.BU, objQMS015.Location).Select(i => new CategoryData { Value = i.Description, Code = i.Description, CategoryDescription = i.Description }).ToList();
            var lstApplicableSpecification = Manager.GetSubCatagories("Applicable Specification", objQMS015.BU, objQMS015.Location).Select(i => new CategoryData { Value = i.Description, Code = i.Description, CategoryDescription = i.Description }).ToList();
            var lstInspectionExtent = Manager.GetSubCatagories("Inspection Extent", objQMS015.BU, objQMS015.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            NDEModels objNDEModels = new NDEModels();
            if (objQMS015.QualityIdApplicableFor == clsImplementationEnum.QualityIDApplicable.SEAM.GetStringValue())
            {
                ViewBag.Stages = db.QMS002.Where(i => i.BU.Equals(objQMS015.BU.Trim(), StringComparison.OrdinalIgnoreCase) && i.Location.Equals(objQMS015.Location.Trim(), StringComparison.OrdinalIgnoreCase)).OrderBy(o => o.SequenceNo).Select(i => new CategoryData { Value = i.StageCode, Code = i.StageCode, CategoryDescription = i.StageCode + "-" + i.StageDesc }).ToList();
            }
            else
            {
                string StageTypeExludedinOfferDropdown = clsImplementationEnum.ConfigParameter.StageTypeExludedinOfferDropdown.GetStringValue();
                var lstStages = Manager.GetConfigValue(StageTypeExludedinOfferDropdown).ToUpper().Split(',').ToArray();
                ViewBag.Stages = db.QMS002.Where(i => i.BU.Equals(objQMS015.BU, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(objQMS015.Location, StringComparison.OrdinalIgnoreCase) && !lstStages.Contains(i.StageType)).OrderBy(o => o.SequenceNo).Select(i => new CategoryData { Value = i.StageCode, Code = i.StageCode, CategoryDescription = i.StageCode + "-" + i.StageDesc }).ToList();
            }
            ViewBag.AcceptanceStandard = lstAcceptanceStandard;
            ViewBag.ApplicableSpecification = lstApplicableSpecification;
            ViewBag.InspectionExtent = lstInspectionExtent;
            ViewBag.HeaderStatus = objQMS015.Status;
            ViewBag.QualityIdName = objQMS015.QualityId;

            {
                objQMS016.HeaderId = headerId;
                objQMS016.RevNo = objQMS015.RevNo;
            }
            return PartialView("_GetQualityIDLinesPartial", objQMS016);
        }

        public ActionResult LoadQualityIdLinesDataTable_Old(JQueryDataTableParamModel param, int refHeaderId)
        {
            try
            {
                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms16.BU", "qms16.Location");
                //if (status.ToUpper() == "PENDING")
                //{
                //    whereCondition += " and qms15.Status in('" + clsImplementationEnum.QualityIdStageStatus.Draft.GetStringValue() + "')";
                //}
                whereCondition += " AND HeaderId = " + refHeaderId;
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += " and (qms16.StageCode like '%" + param.sSearch + "%' or qms16.StageStatus like '%" + param.sSearch + "%' or qms16.AcceptanceStandard like '%" + param.sSearch + "%' or qms16.ApplicableSpecification like '%" + param.sSearch + "%' or qms16.InspectionExtent like '%" + param.sSearch + "%' or qms16.Remarks like '%" + param.sSearch + "%' or qms16.QualityId like '%" + param.sSearch + "%')";
                }

                var lstHeader = db.SP_IPI_GetQualityIdLines(startIndex, endIndex, "", whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstHeader
                          select new[] {
                        Convert.ToString(a.HeaderId),
                        Convert.ToString(a.LineId),
                        Convert.ToString(a.QualityId),
                        a.StageCode ,
                        Convert.ToString(a.StageSequance),
                        a.StageStatus,
                        a.AcceptanceStandard,
                        a.ApplicableSpecification,
                        a.InspectionExtent,
                        a.Remarks
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res

                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult LoadQualityIDStagesDataTable(JQueryDataTableParamModel param, int refHeaderId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;
                string strSortOrder = string.Empty;
                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms16.BU", "qms16.Location");
                whereCondition += " AND  qms16.HeaderId = " + refHeaderId;
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += " and (qms16.StageCode like '%" + param.sSearch + "%' or qms16.StageStatus like '%" + param.sSearch + "%' or qms16.AcceptanceStandard like '%" + param.sSearch + "%' or qms16.ApplicableSpecification like '%" + param.sSearch + "%' or qms16.InspectionExtent like '%" + param.sSearch + "%' or qms16.Remarks like '%" + param.sSearch + "%' or qms16.QualityId like '%" + param.sSearch + "%')";
                }

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstStages = db.SP_IPI_GetQualityIdLines(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstStages.Select(i => i.TotalCount).FirstOrDefault();

                //List<SelectListItem> BoolenList = new List<SelectListItem>() { new SelectListItem { Text = "Yes", Value = "Yes" }, new SelectListItem { Text = "No", Value = "No" } };

                //var Location = (from a in db.COM002
                //                where a.t_dtyp == 1 && a.t_dimx != ""
                //                select new { a.t_dimx, Desc = a.t_desc }).ToList();

                int newRecordId = 0;
                NDEModels objNDEModels = new NDEModels();
                var newRecord = new[] {
                                    Helper.GenerateHidden(newRecordId, "HeaderId", Convert.ToString(refHeaderId)),
                                    Helper.GenerateHidden(newRecordId, "LineId", ""),
                                    Helper.GenerateHidden(newRecordId, "QualityId", db.QMS015.Where(i=>i.HeaderId==refHeaderId).Select(i=>i.QualityId).FirstOrDefault()),
                                    HTMLAutoComplete(newRecordId,"txtStageCode","","CheckDuplicateStages(this,"+ refHeaderId +",0);",false,"","StageCode")+""+Helper.GenerateHidden(newRecordId,"StageCode"),//Helper.GenerateTextbox(newRecordId, "StageCode"),
                   
                     Helper.GenerateLabelFor(newRecordId, "StageSequance", ""),

                    HTMLAutoComplete(newRecordId, "AcceptanceStandard","","",false,"","AcceptanceStandard",false, "100")+""+Helper.GenerateHidden(newRecordId,"AcceptanceStandard"),
                                    HTMLAutoComplete(newRecordId, "ApplicableSpecification","","",false,"","ApplicableSpecification",false, "100")+""+Helper.GenerateHidden(newRecordId,"ApplicableSpecification"),
                                    HTMLAutoComplete(newRecordId, "txtInspectionExtent","","",false,"","InspectionExtent")+""+Helper.GenerateHidden(newRecordId,"InspectionExtent"),
                                    Helper.GenerateHTMLTextbox(newRecordId, "Remarks","","",false,"",false,"100"),
                                    clsImplementationEnum.QualityIdStageStatus.ADDED.GetStringValue(),
                                    GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveLines(0);" ),
                                };

                var data = (from uc in lstStages
                            select new[]
                            {
                               Helper.GenerateHidden(uc.LineId, "HeaderId", Convert.ToString(uc.HeaderId)),
                               Helper.GenerateHidden(uc.LineId,"LineId",Convert.ToString(uc.LineId)),
                               Helper.GenerateHidden(uc.LineId, "QualityId", Convert.ToString(uc.QualityId)),
                               objNDEModels.GetCategory(uc.StageCode, uc.BU.Split('-')[0].Trim(), uc.Location.Split('-')[0].Trim()).CategoryDescription,//uc.StageCode,
                               Convert.ToString(uc.StageSequance),
                               //HTMLAutoComplete(uc.LineId, "StageSequance", objNDEModels.GetCategory(uc.StageCode).CategoryDescription, "UpdateQIDStage(this, "+ uc.LineId +");")+""+Helper.GenerateHidden(uc.LineId,"StageCode",uc.StageCode),
                               HTMLAutoComplete(uc.LineId, "AcceptanceStandard", uc.AcceptanceStandard, "UpdateQIDStage(this, "+ uc.LineId +");",false,"","",((string.Equals(uc.Status,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)) || string.Equals(uc.StageStatus, clsImplementationEnum.TestPlanStatus.Deleted.GetStringValue())?true:false), "100", "required"),
                               HTMLAutoComplete(uc.LineId, "ApplicableSpecification", uc.ApplicableSpecification, "UpdateQIDStage(this, "+ uc.LineId +");",false,"","",((string.Equals(uc.Status,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false)|| string.Equals(uc.StageStatus, clsImplementationEnum.TestPlanStatus.Deleted.GetStringValue())?true:false), "100", "required"),
                               HTMLAutoComplete(uc.LineId, "txtInspectionExtent", objNDEModels.GetInspectionExtent(uc.InspectionExtent, uc.BU.Split('-')[0].Trim(), uc.Location.Split('-')[0].Trim()).CategoryDescription,"UpdateQIDStage(this, "+ uc.LineId +");",false,"","InspectionExtent",((string.Equals(uc.Status,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false)|| string.Equals(uc.StageStatus, clsImplementationEnum.TestPlanStatus.Deleted.GetStringValue())?true:false),"","required")+""+Helper.GenerateHidden(uc.LineId,"InspectionExtent",uc.InspectionExtent),
                               Helper.GenerateHTMLTextboxOnChanged(uc.LineId, "Remarks", Convert.ToString(uc.Remarks),"UpdateQIDStage(this, "+ uc.LineId +");",false,"",((string.Equals(uc.Status,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false)|| string.Equals(uc.StageStatus, clsImplementationEnum.TestPlanStatus.Deleted.GetStringValue())?true:false),"100"),
                               uc.StageStatus,
                               (uc.Status == clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()  || uc.StageStatus == clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue() ? HTMLActionString(uc.LineId,uc.Status,"Delete","Delete QIDStage","fa fa-trash-o","",true) : HTMLActionString(uc.LineId,uc.Status,"Delete","Delete QIDStage","fa fa-trash-o","DeleteQualityIdStage("+ uc.HeaderId +","+uc.LineId+");")) +
                               Helper.MaintainIPIStagetypewiseGridButton(uc.HeaderId,uc.LineId,uc.QualityProject,uc.Project.Split('-')[0].Trim(),uc.BU.Split('-')[0].Trim(),uc.Location.Split('-')[0].Trim(),uc.QualityId,uc.RevNo,uc.StageCode.Split('-')[0].Trim(),uc.StageCode,uc.StageSequance,(uc.Status != clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue() && !string.Equals(uc.StageStatus,clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue(),StringComparison.OrdinalIgnoreCase)),StageTypeAction(uc.HeaderId, uc.StageCode.Split('-')[0].Trim(), uc.BU.Split('-')[0].Trim(), uc.Location.Split('-')[0].Trim(),true),false,uc.StageType)
                               //Helper.GenerateActionIcon(uc.HeaderId, "Sampling Plan", "Sampling Plan", "fa fa-file-text-o", "LoadIPISamplingPlan("+uc.HeaderId+","+uc.LineId+","+(string.Equals(uc.Status,clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue(),StringComparison.OrdinalIgnoreCase) && !string.Equals(uc.StageStatus,clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue(),StringComparison.OrdinalIgnoreCase)) +")","",false) :Helper.GenerateActionIcon(uc.HeaderId, "Sampling Plan", "Sampling Plan", "fa fa-file-text-o", "","",true))
                            }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstStages.Count > 0 ? lstStages.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstStages.Count > 0 ? lstStages.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public bool isStageLinkedInLTFPS(int LineID)
        {
            var objQMS016 = db.QMS016.Where(x => x.LineId == LineID).FirstOrDefault();
            if (objQMS016 != null)
            {
                string deleted = clsImplementationEnum.LTFPSLineStatus.Deleted.GetStringValue();
                List<string> lstQproj = new List<string>();
                lstQproj.Add(objQMS016.QualityProject);
                lstQproj.AddRange(db.QMS017.Where(c => c.BU == objQMS016.BU && c.Location == objQMS016.Location && c.IQualityProject == objQMS016.QualityProject).Select(x => x.QualityProject).ToList());

                var qidStages = (from qid in db.QMS016
                                 join qheader in db.QMS015 on qid.HeaderId equals qheader.HeaderId
                                 join tph in db.QMS020 on qid.QualityId equals tph.QualityId
                                 join tpl in db.QMS021 on tph.HeaderId equals tpl.HeaderId
                                 where lstQproj.Contains(qid.QualityProject) && tpl.StageCode == objQMS016.StageCode && tpl.StageSequance == objQMS016.StageSequance
                                 select tpl.SeamNo
                                 ).ToList();
                if (db.LTF002.Any(x => lstQproj.Contains(x.QualityProject) && x.Location.Equals(objQMS016.Location) && x.BU.Equals(objQMS016.BU) && qidStages.Contains(x.SeamNo) && x.Stage.Equals(objQMS016.StageCode) && x.StageSequence == objQMS016.StageSequance && x.LNTStatus != deleted))
                {
                    return true;
                }
            }
            return false;
        }

        [HttpPost]
        public ActionResult CheckDuplicateStages(int? headerId, int? lineId, string stageCode)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            QMS015 objQMS015 = new QMS015();
            try
            {
                if (headerId > 0)
                    objQMS015 = db.QMS015.Where(i => i.HeaderId == headerId).FirstOrDefault();

                int rowId = lineId.HasValue ? lineId.Value : 0;

                if (isQualityIdStageExist(objQMS015.QualityProject, objQMS015.BU, objQMS015.Location, objQMS015.QualityId, Convert.ToInt32(objQMS015.RevNo), stageCode, rowId))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.QualityIDLineMessage.Duplicate;
                }
                else
                {
                    objResponseMsg.Key = true;
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveLines(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            QMS015 objQMS015 = new QMS015();
            QMS016 objQMS016 = new QMS016();
            try
            {
                if (fc != null)
                {
                    int headerId = Convert.ToInt32(fc["HeaderId0"]);
                    int lineId = !string.IsNullOrEmpty(fc["LineId0"]) ? Convert.ToInt32(fc["LineId0"]) : 0;
                    string stageCode = !string.IsNullOrEmpty(fc["StageCode0"]) ? fc["StageCode0"] : "";
                    if (headerId > 0)
                        objQMS015 = db.QMS015.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    if (objQMS015.Status != clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue())
                    {
                        if (isQualityIdStageExist(objQMS015.QualityProject, objQMS015.BU, objQMS015.Location, objQMS015.QualityId, Convert.ToInt32(objQMS015.RevNo), fc["StageCode0"], lineId))
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.QualityIDLineMessage.Duplicate;

                        }
                        else
                        {
                            if (lineId > 0)
                            {
                                #region Update Existing QualityID Stage
                                objQMS016 = db.QMS016.Where(i => i.HeaderId == headerId && i.LineId == lineId).FirstOrDefault();

                                objQMS016.StageCode = Convert.ToString(fc["StageCode"]).Trim();
                                objQMS016.StageSequance = Convert.ToInt32(fc["StageSequance"]);
                                objQMS016.StageStatus = clsImplementationEnum.QualityIdStageStatus.MODIFIED.GetStringValue();
                                objQMS016.AcceptanceStandard = Convert.ToString(fc["AcceptanceStandard"]).Trim();
                                objQMS016.ApplicableSpecification = Convert.ToString(fc["ApplicableSpecification"]).Trim();
                                objQMS016.InspectionExtent = Convert.ToString(fc["InspectionExtent"]).Trim();
                                objQMS016.Remarks = fc["Remarks"];
                                if (objQMS015.RevNo > 0 || objQMS015.Status != clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue())
                                {
                                    objQMS016.EditedBy = objClsLoginInfo.UserName;
                                    objQMS016.EditedOn = DateTime.Now;
                                }

                                if (string.Equals(objQMS015.Status, clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue()))
                                {
                                    objResponseMsg = ReviseHeaderWithLines(headerId, false, 0);
                                    ////objQMS016.RevNo = objQMS015.RevNo + 1;
                                    //var lstLines = db.QMS016.Where(i => i.HeaderId == headerId).ToList();
                                    //if (lstLines.Any())
                                    //    lstLines.ForEach(i => i.RevNo = objQMS015.RevNo + 1);


                                    ////var lstIdenticalProj = db.QMS017.Where(i => i.HeaderId == headerId).ToList();
                                    ////if (lstIdenticalProj.Any())
                                    ////    lstIdenticalProj.ForEach(i => i.RevNo = objQMS015.RevNo + 1);

                                    //objQMS015.Status = clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue();
                                    //objQMS015.RevNo += 1;
                                    objQMS015.Status = objResponseMsg.HeaderStatus;
                                    objQMS015.RevNo = Convert.ToInt32(objResponseMsg.RevNo);
                                }

                                db.SaveChanges();
                                objResponseMsg.Key = true;
                                objResponseMsg.Value = clsImplementationMessage.QualityIDLineMessage.Update;
                                objResponseMsg.HeaderId = objQMS015.HeaderId;
                                objResponseMsg.HeaderStatus = objQMS015.Status;
                                objResponseMsg.RevNo = Convert.ToString(objQMS015.RevNo);
                                #endregion
                            }
                            else
                            {
                                #region Add New QualityId Stage
                                objQMS016.HeaderId = headerId;
                                objQMS016.QualityProject = objQMS015.QualityProject;
                                objQMS016.Project = objQMS015.Project;
                                objQMS016.BU = objQMS015.BU;
                                objQMS016.Location = objQMS015.Location;
                                objQMS016.QualityId = objQMS015.QualityId;
                                objQMS016.RevNo = objQMS015.RevNo;
                                objQMS016.StageCode = Convert.ToString(fc["StageCode0"]).Trim();
                                objQMS016.StageSequance = db.QMS002.Where(i => i.StageCode == stageCode && i.BU == objQMS015.BU && i.Location == objQMS015.Location).FirstOrDefault().SequenceNo;//Convert.ToInt32(fc["StageSequance0"]);
                                objQMS016.StageStatus = clsImplementationEnum.QualityIdStageStatus.ADDED.GetStringValue();
                                objQMS016.AcceptanceStandard = Convert.ToString(fc["AcceptanceStandard0"]).Trim().Replace(",", ""); ;
                                objQMS016.ApplicableSpecification = Convert.ToString(fc["ApplicableSpecification0"]).Trim().Replace(",", ""); ;
                                objQMS016.InspectionExtent = Convert.ToString(fc["InspectionExtent0"]).Trim();
                                objQMS016.Remarks = fc["Remarks0"];
                                objQMS016.CreatedBy = objClsLoginInfo.UserName;
                                objQMS016.CreatedOn = DateTime.Now;
                                if (string.Equals(objQMS015.Status, clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue()))
                                {
                                    objResponseMsg = ReviseHeaderWithLines(headerId, false, 0);
                                    objQMS016.RevNo = Convert.ToInt32(objResponseMsg.RevNo);
                                    objQMS015.Status = objResponseMsg.HeaderStatus;
                                    objQMS015.RevNo = Convert.ToInt32(objResponseMsg.RevNo);
                                    //var lstLines = db.QMS016.Where(i => i.HeaderId == headerId).ToList();
                                    //if (lstLines.Any())
                                    //    lstLines.ForEach(i => i.RevNo = objQMS015.RevNo + 1);

                                    ////var lstIdenticalProj = db.QMS017.Where(i => i.HeaderId == headerId).ToList();
                                    ////if (lstIdenticalProj.Any())
                                    ////    lstIdenticalProj.ForEach(i => i.RevNo = objQMS015.RevNo + 1);

                                    //objQMS015.Status = clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue();
                                    //objQMS015.RevNo += 1;
                                }
                                db.QMS016.Add(objQMS016);
                                db.SaveChanges();

                                objResponseMsg.Key = true;
                                objResponseMsg.Value = clsImplementationMessage.QualityIDLineMessage.Save;
                                objResponseMsg.HeaderId = objQMS015.HeaderId;
                                objResponseMsg.HeaderStatus = objQMS015.Status;
                                objResponseMsg.RevNo = Convert.ToString(objQMS015.RevNo);
                                #endregion
                            }
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Remarks = clsImplementationMessage.CommonMessages.NotInEditStatus;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult UpdateQIDStage(int lineId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            QMS016 objQMS016 = new QMS016();
            try
            {
                if (lineId > 0)
                {
                    objQMS016 = db.QMS016.Where(i => i.LineId == lineId).FirstOrDefault();
                    var qms15 = db.QMS015.Where(i => i.HeaderId == objQMS016.HeaderId).FirstOrDefault();
                    if (!string.Equals(qms15.Status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
                    {
                        if (!string.IsNullOrEmpty(columnName))
                        {
                            db.SP_IPI_UPDATEQIDSTAGE(lineId, columnName, columnValue);

                            if (string.Equals(qms15.Status, clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                            {
                                ReviseHeader(qms15.HeaderId, false, 0);
                                //qms15.Status = clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue();
                                //if (db.QMS016.Where(i => i.HeaderId == qms15.HeaderId).Any())
                                //{
                                //    db.QMS016.Where(i => i.HeaderId == qms15.HeaderId).ToList().ForEach(i => i.RevNo = qms15.RevNo + 1);
                                //}
                                //qms15.RevNo += 1;
                            }

                            if (qms15.RevNo == 0 && qms15.Status == clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue())
                            {
                                objQMS016.StageStatus = clsImplementationEnum.QualityIdStageStatus.ADDED.GetStringValue();
                                objQMS016.CreatedBy = objClsLoginInfo.UserName;
                                objQMS016.CreatedOn = DateTime.Now;
                            }
                            else
                            {
                                objQMS016.StageStatus = clsImplementationEnum.QualityIdStageStatus.MODIFIED.GetStringValue();
                                objQMS016.EditedBy = objClsLoginInfo.UserName;
                                objQMS016.EditedOn = DateTime.Now;
                            }
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "Saved Successfully";
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Column name or value is null";
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Remarks = clsImplementationMessage.CommonMessages.NotInEditStatus;
                    }
                    objResponseMsg.HeaderId = qms15.HeaderId;
                    objResponseMsg.HeaderStatus = qms15.Status;
                    objResponseMsg.RevNo = "R" + qms15.RevNo;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReviseHeader(int headerid, bool updateLineflag, int lineid)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {

                //if (headerid > 0)
                //{
                //    QMS015 objQMS015 = db.QMS015.Where(i => i.HeaderId == headerid).FirstOrDefault();

                //    if (string.Equals(objQMS015.Status, clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue()))
                //    {
                //        int? newRev = objQMS015.RevNo + 1;
                //        #region QID stages
                //        var lstLines = db.QMS016.Where(i => i.HeaderId == headerid).ToList();
                //        if (lstLines.Any())
                //            lstLines.ForEach(i => i.RevNo = newRev);
                //        #endregion

                //        #region Sampling Plan

                //        var lstisSamplingPlan = (from q16 in lstLines
                //                                 join q02 in db.QMS002 on q16.StageCode.Trim() equals q02.StageCode.Trim()
                //                                 where q16.HeaderId == headerid && q02.StageType == "chemical"
                //                                 select q16
                //                     ).ToList();
                //        if (updateLineflag)
                //        {
                //            if (lineid > 0)
                //            {
                //                QMS016 objLineupdate = lstLines.Where(x => x.LineId == lineid).FirstOrDefault();
                //                objLineupdate.StageStatus = clsImplementationEnum.QualityIdStageStatus.MODIFIED.GetStringValue();
                //                objLineupdate.EditedBy = objClsLoginInfo.UserName;
                //                objLineupdate.EditedOn = DateTime.Now;
                //            }
                //        }
                //        foreach (var item in lstisSamplingPlan)
                //        {
                //            QMS018 objSP = db.QMS018.Where(x => x.QIDLineId == item.LineId).FirstOrDefault();
                //            if (objSP != null)
                //            {
                //                QMS018 insertSP = new QMS018();
                //                insertSP = objSP;
                //                insertSP.QualityIdRev = newRev;
                //                insertSP.CreatedBy = objClsLoginInfo.UserName;
                //                insertSP.CreatedOn = DateTime.Now;
                //                insertSP.CreatedBy = objClsLoginInfo.UserName;
                //                insertSP.CreatedOn = DateTime.Now;
                //                db.QMS018.Add(insertSP);
                //                db.SaveChanges();

                //                List<QMS019> existingElement = db.QMS019.Where(x => x.HeaderId == objSP.HeaderId).ToList();
                //                db.QMS019.AddRange(existingElement.Select(i => new QMS019
                //                {
                //                    HeaderId = insertSP.HeaderId,
                //                    QIDLineId = insertSP.QIDLineId,
                //                    Project = insertSP.Project,
                //                    QualityProject = insertSP.QualityProject,
                //                    BU = insertSP.BU,
                //                    Location = insertSP.Location,
                //                    QualityId = insertSP.QualityId,
                //                    QualityIdRev = insertSP.QualityIdRev,
                //                    StageCode = insertSP.StageCode,
                //                    StageSequence = insertSP.StageSequence,
                //                    Element = i.Element,
                //                    ReqMinValue = i.ReqMinValue,
                //                    ReqMaxValue = i.ReqMaxValue,
                //                    CreatedBy = objClsLoginInfo.UserName,
                //                    CreatedOn = DateTime.Now
                //                }));
                //                db.SaveChanges();
                //                //insertSP.HeaderId
                //            }

                //        }
                //        #endregion

                //        objQMS015.Status = clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue();
                //        objQMS015.RevNo = newRev;
                //        objResponseMsg.HeaderStatus = objQMS015.Status;
                //    }
                //db.SaveChanges();
                objResponseMsg = ReviseHeaderWithLines(headerid, updateLineflag, lineid);
                objResponseMsg.Key = true;

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public clsHelper.ResponseMsgWithStatus ReviseHeaderWithLines(int headerid, bool updateLineflag, int lineid)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (headerid > 0)
                {
                    QMS015 objQMS015 = db.QMS015.Where(i => i.HeaderId == headerid).FirstOrDefault();

                    if (string.Equals(objQMS015.Status, clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue()))
                    {
                        int? newRev = objQMS015.RevNo + 1;
                        #region QID stages
                        var lstLines = db.QMS016.Where(i => i.HeaderId == headerid).ToList();
                        if (lstLines.Any())
                            lstLines.ForEach(i => i.RevNo = newRev);
                        #endregion

                        #region Sampling Plan
                        string chemical = clsImplementationEnum.SeamStageType.Chemical.GetStringValue().ToLower();
                        string PMI = clsImplementationEnum.SeamStageType.PMI.GetStringValue().ToLower();
                        var lstisSamplingPlan = (from q16 in lstLines
                                                 join q02 in db.QMS002 on q16.StageCode.Trim() equals q02.StageCode.Trim()
                                                 where q16.HeaderId == headerid && (q02.StageType.ToLower() == chemical || q02.StageType.ToLower() == PMI)
                                                 select q16
                                     ).ToList();
                        if (updateLineflag)
                        {
                            if (lineid > 0)
                            {
                                QMS016 objLineupdate = lstLines.Where(x => x.LineId == lineid).FirstOrDefault();
                                objLineupdate.StageStatus = clsImplementationEnum.QualityIdStageStatus.MODIFIED.GetStringValue();
                                objLineupdate.EditedBy = objClsLoginInfo.UserName;
                                objLineupdate.EditedOn = DateTime.Now;
                            }
                        }
                        foreach (var item in lstisSamplingPlan)
                        {
                            QMS018 objSP = db.QMS018.Where(x => x.QIDLineId == item.LineId).OrderByDescending(x => x.QualityIdRev).FirstOrDefault();
                            int samplingPlanId = objSP.HeaderId;

                            if (objSP != null)
                            {
                                if (objSP.QualityIdRev != newRev)
                                {
                                    QMS018 insertSP = new QMS018();
                                    insertSP = objSP;
                                    insertSP.QualityIdRev = newRev;
                                    insertSP.CreatedBy = objClsLoginInfo.UserName;
                                    insertSP.CreatedOn = DateTime.Now;
                                    insertSP.CreatedBy = objClsLoginInfo.UserName;
                                    insertSP.CreatedOn = DateTime.Now;
                                    db.QMS018.Add(insertSP);
                                    db.SaveChanges();

                                    List<QMS019> existingElement = db.QMS019.Where(x => x.HeaderId == samplingPlanId).ToList();
                                    db.QMS019.AddRange(existingElement.Select(i => new QMS019
                                    {
                                        HeaderId = insertSP.HeaderId,
                                        QIDLineId = objSP.QIDLineId,
                                        Project = objSP.Project,
                                        QualityProject = objSP.QualityProject,
                                        BU = objSP.BU,
                                        Location = objSP.Location,
                                        QualityId = insertSP.QualityId,
                                        QualityIdRev = insertSP.QualityIdRev,
                                        StageCode = insertSP.StageCode,
                                        StageSequence = insertSP.StageSequence,
                                        Element = i.Element,
                                        ReqMinValue = i.ReqMinValue,
                                        ReqMaxValue = i.ReqMaxValue,
                                        CreatedBy = objClsLoginInfo.UserName,
                                        CreatedOn = DateTime.Now
                                    }));
                                    db.SaveChanges();
                                }
                                //insertSP.HeaderId
                            }

                        }
                        #endregion

                        objQMS015.Status = clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue();
                        objQMS015.RevNo = newRev;
                        objQMS015.EditedBy = objClsLoginInfo.UserName;
                        objQMS015.EditedOn = DateTime.Now;
                        objQMS015.SubmittedBy = null;
                        objQMS015.SubmittedOn = null;
                        objQMS015.ApprovedBy = null;
                        objQMS015.ApprovedOn = null;
                        objQMS015.ReturnedBy = null;
                        objQMS015.ReturnedOn = null; objQMS015.ReturnRemark = null;
                    }
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderStatus = objQMS015.Status;
                    objResponseMsg.RevNo = objQMS015.RevNo.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return objResponseMsg;
        }

        [HttpPost]
        public ActionResult DeleteQualityIdStage(int headerId, int lineId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            QMS015 objQMS015 = new QMS015();
            QMS016 objQMS016 = new QMS016();
            try
            {
                if (headerId > 0 && lineId > 0)
                {
                    objQMS015 = db.QMS015.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    objQMS016 = db.QMS016.Where(i => i.HeaderId == headerId && i.LineId == lineId).FirstOrDefault();

                    if (objQMS016 != null)
                    {
                        if (objQMS015.Status != clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue())
                        {
                            if (objQMS015.Status == clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue() && objQMS015.RevNo == 0)
                            {
                                string chemical = clsImplementationEnum.SeamStageType.Chemical.GetStringValue();
                                string PMI = clsImplementationEnum.SeamStageType.PMI.GetStringValue();

                                if (Manager.FetchStageType(objQMS016.StageCode.ToLower(), objQMS016.BU, objQMS016.Location) == chemical || Manager.FetchStageType(objQMS016.StageCode.ToLower(), objQMS016.BU, objQMS016.Location) == PMI)
                                {
                                    var lstQMS018 = db.QMS018.Where(i => i.QIDLineId == lineId).ToList();
                                    db.QMS018.RemoveRange(lstQMS018);
                                    var lstQMS019 = db.QMS019.Where(i => i.QIDLineId == lineId).ToList();
                                    db.QMS019.RemoveRange(lstQMS019);
                                }
                                db.QMS016.Remove(objQMS016);
                            }
                            else
                            {
                                objQMS016.StageStatus = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
                                objQMS016.DeletedBy = objClsLoginInfo.UserName;
                                objQMS016.DeletedOn = DateTime.Now;

                                if (string.Equals(objQMS015.Status, clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue()))
                                {
                                    //var lstLines = db.QMS016.Where(i => i.HeaderId == headerId).ToList();
                                    //if (lstLines.Any())
                                    //    lstLines.ForEach(i => i.RevNo = objQMS015.RevNo + 1);

                                    //objQMS015.Status = clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue();
                                    //objQMS015.RevNo += 1;
                                    objResponseMsg = ReviseHeaderWithLines(objQMS015.HeaderId, false, 0);
                                    objQMS015.Status = objResponseMsg.HeaderStatus;
                                    objQMS015.RevNo = Convert.ToInt32(objResponseMsg.RevNo);
                                }

                            }

                            db.SaveChanges();
                            objResponseMsg.Value = clsImplementationMessage.QualityIDLineMessage.Delete;
                        }
                        else
                        {
                            objResponseMsg.Remarks = "This data is already submitted, it cannot be delete now!!";
                        }
                        objResponseMsg.Key = true;

                        objResponseMsg.HeaderId = objQMS015.HeaderId;
                        objResponseMsg.HeaderStatus = objQMS015.Status;
                        objResponseMsg.RevNo = Convert.ToString(objQMS015.RevNo);
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }


            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region 557 - Hardness Ferrite PMI FDD
        [HttpPost]
        public ActionResult GetHardnessFerritePartial(int lineid = 0, bool isEditable = false, string qProj = "", string Project = "", string bu = "", string loc = "", string qId = "", int qRev = 0, string stage = "")
        {
            QMS016 objQMS016 = new QMS016();

            List<string> lstQproj = new List<string>();
            lstQproj.Add(qProj);
            lstQproj.AddRange(db.QMS017.Where(c => c.BU == bu && c.Location == loc && c.IQualityProject == qProj).Select(x => x.QualityProject).ToList());

            int Qidlineid = lineid > 0 ? lineid : db.QMS016.Where(i => lstQproj.Contains(i.QualityProject) //&& i.Project == Project
                                                     && i.BU == bu && i.Location == loc && i.QualityId == qId
                                                     && i.StageCode == stage
                                            ).Select(x => x.LineId).FirstOrDefault();
            if (Qidlineid > 0)
            {
                objQMS016 = db.QMS016.Where(i => i.LineId == Qidlineid).FirstOrDefault();
            }
            ViewBag.StageType = Manager.FetchStageType(objQMS016.StageCode, objQMS016.BU, objQMS016.Location);
            ViewBag.isEditable = isEditable;

            return PartialView("~/Areas/IPI/Views/Shared/_LoadHardnessFerriteData.cshtml", objQMS016);
        }

        [HttpPost]
        public ActionResult SaveHardness(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                QMS015 objQMS015 = new QMS015();
                QMS016 objQMS016 = new QMS016();
                int lineid = Convert.ToInt32(fc["LineId"]);
                objQMS016 = db.QMS016.Where(i => i.LineId == lineid).FirstOrDefault();
                if (objQMS016 != null)
                {
                    if (objQMS016.QMS015.Status != clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue() && objQMS016.StageStatus != clsImplementationEnum.LTFPSLineStatus.Deleted.GetStringValue())
                    {
                        if (Convert.ToString(fc["StageType"]) == clsImplementationEnum.SeamStageType.Hardness.GetStringValue())
                        {
                            objQMS016.HardnessRequiredValue = Convert.ToInt32(fc["HardnessRequiredValue"]);
                            objQMS016.HardnessUnit = Convert.ToString(fc["HardnessUnit"]);
                        }
                        if (Convert.ToString(fc["StageType"]) == clsImplementationEnum.SeamStageType.Ferrite.GetStringValue())
                        {
                            objQMS016.FerriteMinValue = Convert.ToDouble(fc["FerriteMinValue"]);
                            objQMS016.FerriteMaxValue = Convert.ToDouble(fc["FerriteMaxValue"]);
                            objQMS016.FerriteUnit = Convert.ToString(fc["FerriteUnit"]);
                        }
                        if (string.Equals(objQMS015.Status, clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue()))
                        {
                            objResponseMsg = ReviseHeaderWithLines(objQMS016.HeaderId, false, 0);
                            objQMS016.RevNo = Convert.ToInt32(objResponseMsg.RevNo);
                            objQMS016.QMS015.Status = objResponseMsg.HeaderStatus;
                            objQMS016.QMS015.RevNo = Convert.ToInt32(objResponseMsg.RevNo);
                        }
                        if (string.Equals(objQMS016.StageStatus, clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue()))
                        {
                            objQMS016.StageStatus = clsImplementationEnum.QualityIdStageStatus.MODIFIED.GetStringValue();
                        }
                        objResponseMsg.Value = "Record saved successfully";
                    }
                    else
                    {
                        objResponseMsg.Remarks = clsImplementationMessage.CommonMessages.NotInEditStatus;
                    }
                }
                db.SaveChanges();
                objResponseMsg.Key = true;

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Copy QID With Stages
        public ActionResult CopyQualityIDPartial(string qualityProj, string qID)
        {
            ViewBag.QualityProject = qualityProj;
            ViewBag.QualityID = qID;

            ViewBag.QIDs = db.QMS015.Where(i => i.QualityProject.Equals(qualityProj, StringComparison.OrdinalIgnoreCase)).OrderBy(i => i.QualityId).Select(i => i.QualityId).ToList();

            return PartialView("_CopyQualityIDPartial");
        }

        [HttpPost]
        public ActionResult CopyToDestination(int headerId, string destQId, string qIDfrom, string qIDTo)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            int inSkippedQIDs = 0, inCopiedQIDs = 0;
            string[] arrAlphabet = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
            QMS015 objQMS015Src = new QMS015();
            QMS015 objQMS015Dest = new QMS015();

            try
            {
                string currentUser = objClsLoginInfo.UserName;

                var currentLoc = (from a in db.COM003
                                  join b in db.COM002 on a.t_loca equals b.t_dimx
                                  where b.t_dtyp == 1
                                  && a.t_psno.Equals(currentUser, StringComparison.OrdinalIgnoreCase)
                                  select b.t_dimx).FirstOrDefault().ToString();
                if (headerId > 0)
                {
                    objQMS015Src = db.QMS015.Where(i => i.HeaderId == headerId).FirstOrDefault();

                    List<QMS015> allQIDs = db.QMS015.Where(i => i.QualityProject.Equals(objQMS015Src.QualityProject, StringComparison.OrdinalIgnoreCase)).OrderBy(i => i.QualityId).ToList();

                    List<QMS015> newQIDList = new List<QMS015>();
                    bool start = false;

                    foreach (QMS015 QID in allQIDs)
                    {
                        if (QID.QualityId == qIDfrom)
                        {
                            start = true;
                        }

                        if (start)
                            newQIDList.Add(QID);

                        if (QID.QualityId == qIDTo)
                        {
                            break;
                        }
                    }

                    List<QMS016> existingQIDs = db.QMS016.Where(i => i.HeaderId == headerId).ToList();

                    foreach (QMS015 newQID in newQIDList)
                    {
                        if (!db.QMS016.Where(c => c.HeaderId == newQID.HeaderId).Any())
                        {
                            db.QMS016.AddRange(existingQIDs.Select(i => new QMS016
                            {
                                HeaderId = newQID.HeaderId,
                                QualityProject = newQID.QualityProject,
                                Project = newQID.Project,
                                BU = newQID.BU,
                                Location = newQID.Location,
                                QualityId = newQID.QualityId,
                                RevNo = newQID.RevNo,
                                StageCode = i.StageCode,
                                StageSequance = i.StageSequance,
                                StageStatus = clsImplementationEnum.QualityIdStageStatus.ADDED.GetStringValue(),
                                AcceptanceStandard = i.AcceptanceStandard,
                                ApplicableSpecification = i.ApplicableSpecification,
                                InspectionExtent = i.InspectionExtent,
                                HardnessRequiredValue = i.HardnessRequiredValue,
                                HardnessUnit = i.HardnessUnit,
                                FerriteMaxValue = i.FerriteMaxValue,
                                FerriteMinValue = i.FerriteMinValue,
                                FerriteUnit = i.FerriteUnit,
                                Remarks = i.Remarks,
                                CreatedBy = objClsLoginInfo.UserName,
                                CreatedOn = DateTime.Now
                            }));
                            db.SaveChanges();

                            List<QMS016> lstQms16 = newQID.QMS016.ToList();
                            foreach (var item in existingQIDs)
                            {
                                QMS016 objQIDLines = db.QMS016.Where(x => x.StageCode == item.StageCode && x.StageSequance == item.StageSequance && x.HeaderId == newQID.HeaderId).FirstOrDefault();
                                QMS018 objSP = db.QMS018.Where(x => x.QIDLineId == item.LineId).FirstOrDefault();
                                if (objSP != null)
                                {
                                    int samplingPlanId = objSP.HeaderId;
                                    QMS018 insertSP = new QMS018();
                                    insertSP = objSP;
                                    insertSP.QIDLineId = objQIDLines.LineId;
                                    insertSP.QualityId = objQIDLines.QualityId;
                                    insertSP.QualityIdRev = objQIDLines.RevNo;
                                    insertSP.CreatedBy = objClsLoginInfo.UserName;
                                    insertSP.CreatedOn = DateTime.Now;
                                    insertSP.CreatedBy = objClsLoginInfo.UserName;
                                    insertSP.CreatedOn = DateTime.Now;
                                    db.QMS018.Add(insertSP);
                                    db.SaveChanges();

                                    List<QMS019> existingElement = db.QMS019.Where(x => x.HeaderId == samplingPlanId).ToList();
                                    db.QMS019.AddRange(existingElement.Select(i => new QMS019
                                    {
                                        HeaderId = insertSP.HeaderId,
                                        QIDLineId = insertSP.QIDLineId,
                                        Project = insertSP.Project,
                                        QualityProject = insertSP.QualityProject,
                                        BU = insertSP.BU,
                                        Location = insertSP.Location,
                                        QualityId = insertSP.QualityId,
                                        QualityIdRev = insertSP.QualityIdRev,
                                        StageCode = insertSP.StageCode,
                                        StageSequence = insertSP.StageSequence,
                                        Element = i.Element,
                                        ReqMinValue = i.ReqMinValue,
                                        ReqMaxValue = i.ReqMaxValue,
                                        CreatedBy = objClsLoginInfo.UserName,
                                        CreatedOn = DateTime.Now
                                    }));
                                    db.SaveChanges();
                                    //insertSP.HeaderId
                                }

                            }
                        }


                    }

                }

                //if (arrAlphabet.Contains(qIDfrom, StringComparer.OrdinalIgnoreCase) && arrAlphabet.Contains(qIDTo, StringComparer.OrdinalIgnoreCase))
                //{
                //    inQIDFrom = Array.FindIndex(arrAlphabet, i => i.Equals(qIDfrom, StringComparison.OrdinalIgnoreCase));//Array.IndexOf(arrAlphabet, tagFrom);
                //    inQIDTo = Array.FindIndex(arrAlphabet, i => i.Equals(qIDTo, StringComparison.OrdinalIgnoreCase));// Array.IndexOf(arrAlphabet, tagTo);

                //    #region Copy For Alphabetic Range
                //    while (inQIDFrom <= inQIDTo)
                //    {
                //        var item = arrAlphabet.GetValue(inQIDFrom);
                //        var qualityID = item.ToString();//Convert.ToChar(inTagFrom).ToString();
                //        var ProjectWiseBULocation = Manager.getProjectWiseBULocation(objQMS015Src.Project);
                //        if (isQualityIdExist(objQMS015Src.QualityProject, objQMS015Src.Project, objQMS015Src.BU, currentLoc, qualityID))
                //        {
                //            inSkippedQIDs++;
                //            //var lstStages = db.QMS016.Where(i => i.QualityProject == objQMS015Src.QualityProject && i.Location == currentLoc && i.QualityId == qualityId).ToList();
                //            //if (lstStages.Any()){}
                //        }
                //        else
                //        {
                //            #region QID Header
                //            objQMS015Dest = new QMS015();
                //            objQMS015Dest.QualityProject = objQMS015Src.QualityProject;
                //            objQMS015Dest.Project = objQMS015Src.Project;
                //            objQMS015Dest.BU = objQMS015Src.BU;
                //            objQMS015Dest.Location = currentLoc;
                //            objQMS015Dest.QualityId = qualityID;
                //            //objQMS015Dest.QualityIdDesc = fc["QualityIdDesc"];
                //            objQMS015Dest.QualityIdApplicableFor = objQMS015Src.QualityIdApplicableFor;
                //            objQMS015Dest.RevNo = 0;
                //            objQMS015Dest.Status = clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue();
                //            objQMS015Dest.CreatedBy = objClsLoginInfo.UserName;
                //            objQMS015Dest.CreatedOn = DateTime.Now;

                //            db.QMS015.Add(objQMS015Dest);
                //            db.SaveChanges();
                //            #endregion

                //            #region QID Stages
                //            var strStageStatus = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
                //            var lstStages = db.QMS016.Where(i => i.HeaderId == headerId && i.QualityId == destQId && !i.StageCode.Equals(strStageStatus, StringComparison.OrdinalIgnoreCase)).ToList();
                //            if (lstStages.Any())
                //            {
                //                db.QMS016.AddRange(lstStages.Select(i => new QMS016
                //                {
                //                    HeaderId = objQMS015Dest.HeaderId,
                //                    QualityProject = i.QualityProject,
                //                    Project = i.Project,
                //                    BU = i.BU,
                //                    Location = currentLoc,
                //                    QualityId = qualityID,
                //                    RevNo = objQMS015Dest.RevNo,
                //                    StageCode = i.StageCode,
                //                    StageSequance = i.StageSequance,
                //                    StageStatus = clsImplementationEnum.QualityIdStageStatus.ADDED.GetStringValue(),
                //                    AcceptanceStandard = i.AcceptanceStandard,
                //                    ApplicableSpecification = i.ApplicableSpecification,
                //                    InspectionExtent = i.InspectionExtent,
                //                    Remarks = i.Remarks,
                //                    CreatedBy = objClsLoginInfo.UserName,
                //                    CreatedOn = DateTime.Now
                //                }));
                //            }
                //            #endregion

                //            #region Identical Project
                //            //var lstIdentical = db.QMS017.Where(i => i.HeaderId == headerId).ToList();
                //            //if (lstIdentical.Any())
                //            //{
                //            //    db.QMS017.AddRange(lstIdentical.Select(i => new QMS017
                //            //    {
                //            //        HeaderId = objQMS015Dest.HeaderId,
                //            //        QualityProject = objQMS015Dest.QualityProject,
                //            //        Project = objQMS015Dest.Project,
                //            //        BU = objQMS015Dest.BU,
                //            //        Location = currentLoc,
                //            //        //QualityId = qualityID,
                //            //        RevNo = objQMS015Dest.RevNo,
                //            //        IQualityProject = i.IQualityProject,
                //            //        CreatedBy = objClsLoginInfo.UserName,
                //            //        CreatedOn = DateTime.Now
                //            //    }));
                //            //}
                //            #endregion

                //            inCopiedQIDs++;
                //            db.SaveChanges();

                //        }
                //        inQIDFrom++;
                //    }
                //    #endregion
                //}
                //else
                //{
                //    inQIDFrom = Convert.ToInt32(qIDfrom);
                //    inQIDTo = Convert.ToInt32(qIDTo);

                //    #region Copy for Numeric Range
                //    while (inQIDFrom <= inQIDTo)
                //    {
                //        var qualityID = Convert.ToString(inQIDFrom);// Convert.ToChar(inTagFrom).ToString();
                //        var ProjectWiseBULocation = Manager.getProjectWiseBULocation(objQMS015Src.Project);
                //        if (isQualityIdExist(objQMS015Src.QualityProject, objQMS015Src.Project, objQMS015Src.BU, currentLoc, qualityID))
                //        { inSkippedQIDs++; }
                //        else
                //        {
                //            #region QID Header
                //            objQMS015Dest = new QMS015();
                //            objQMS015Dest.QualityProject = objQMS015Src.QualityProject;
                //            objQMS015Dest.Project = objQMS015Src.Project;
                //            objQMS015Dest.BU = objQMS015Src.BU;
                //            objQMS015Dest.Location = currentLoc;
                //            objQMS015Dest.QualityId = qualityID;
                //            //objQMS015Dest.QualityIdDesc = fc["QualityIdDesc"];
                //            objQMS015Dest.QualityIdApplicableFor = objQMS015Src.QualityIdApplicableFor;
                //            objQMS015Dest.RevNo = 0;
                //            objQMS015Dest.Status = clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue();
                //            objQMS015Dest.CreatedBy = objClsLoginInfo.UserName;
                //            objQMS015Dest.CreatedOn = DateTime.Now;

                //            db.QMS015.Add(objQMS015Dest);
                //            db.SaveChanges();
                //            #endregion

                //            #region QID Stages

                //            var strStageStatus = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
                //            var lstStages = db.QMS016.Where(i => i.HeaderId == headerId && i.QualityId == destQId && !i.StageCode.Equals(strStageStatus, StringComparison.OrdinalIgnoreCase)).ToList();
                //            if (lstStages.Any())
                //            {
                //                db.QMS016.AddRange(lstStages.Select(i => new QMS016
                //                {
                //                    HeaderId = objQMS015Dest.HeaderId,
                //                    QualityProject = i.QualityProject,
                //                    Project = i.Project,
                //                    BU = i.BU,
                //                    Location = currentLoc,
                //                    QualityId = qualityID,
                //                    RevNo = objQMS015Dest.RevNo,
                //                    StageCode = i.StageCode,
                //                    StageSequance = i.StageSequance,
                //                    StageStatus = clsImplementationEnum.QualityIdStageStatus.ADDED.GetStringValue(),
                //                    AcceptanceStandard = i.AcceptanceStandard,
                //                    ApplicableSpecification = i.ApplicableSpecification,
                //                    InspectionExtent = i.InspectionExtent,
                //                    Remarks = i.Remarks,
                //                    CreatedBy = objClsLoginInfo.UserName,
                //                    CreatedOn = DateTime.Now
                //                }));
                //            }
                //            #endregion

                //            #region Identical Project
                //            //var lstIdentical = db.QMS017.Where(i => i.HeaderId == headerId).ToList();
                //            //if (lstIdentical.Any())
                //            //{
                //            //    db.QMS017.AddRange(lstIdentical.Select(i => new QMS017
                //            //    {
                //            //        HeaderId = objQMS015Dest.HeaderId,
                //            //        QualityProject = objQMS015Dest.QualityProject,
                //            //        Project = objQMS015Dest.Project,
                //            //        BU = objQMS015Dest.BU,
                //            //        Location = currentLoc,
                //            //        //QualityId = qualityID,
                //            //        RevNo = objQMS015Dest.RevNo,
                //            //        IQualityProject = i.IQualityProject,
                //            //        CreatedBy = objClsLoginInfo.UserName,
                //            //        CreatedOn = DateTime.Now
                //            //    }));
                //            //}
                //            #endregion

                //            inCopiedQIDs++;
                //            db.SaveChanges();
                //        }
                //        inQIDFrom++;
                //    }
                //    #endregion
                //}

                string SkippedQIDMsg = string.Empty, copyMsg = string.Empty;
                if (inSkippedQIDs > 0)
                    SkippedQIDMsg += string.Format("System has skipped {0} QID", inSkippedQIDs);
                if (inCopiedQIDs > 0)
                    copyMsg += string.Format("{0} QID data is copied successfully", inCopiedQIDs);

                copyMsg = "QID data is copied successfully";

                objResponseMsg.Key = true;
                objResponseMsg.Value = copyMsg;
                objResponseMsg.Status = SkippedQIDMsg;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Transfer QID
        public ActionResult TransferQualityIDPartial(string qualityProj)
        {
            ViewBag.QualityProject = qualityProj;
            ViewBag.QIDs = db.QMS015.Where(i => i.QualityProject.Equals(qualityProj, StringComparison.OrdinalIgnoreCase)).OrderBy(i => i.QualityId).Select(i => i.QualityId).ToList();
            return PartialView("_TransferQualityIDPartial");
        }

        [HttpPost]
        public ActionResult TransferQIDTODestination(string srcQualityProj, string srcQIDs, string destQPFrom, string destQPTo, bool withLines)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            int inTransferredQID = 0, inSkippedQID = 0;
            try
            {
                string currentUser = objClsLoginInfo.UserName;

                var currentLoc = (from a in db.COM003
                                  join b in db.COM002 on a.t_loca equals b.t_dimx
                                  where b.t_dtyp == 1
                                  && a.t_psno.Equals(currentUser, StringComparison.OrdinalIgnoreCase)
                                  select b.t_dimx).FirstOrDefault().ToString();
                QMS015 objQMS015Src = new QMS015();
                QMS015 objQMS015Dest = new QMS015();

                string destProjectFrom = destQPFrom.Split('/')[0];
                string destProjectTo = destQPTo.Split('/')[0];

                List<QMS010> lstQMS010 = new List<QMS010>();
                #region Destination QP Range
                if (string.Equals(destProjectFrom, destProjectTo, StringComparison.OrdinalIgnoreCase))
                {
                    if (string.Equals(destQPFrom, destQPTo, StringComparison.OrdinalIgnoreCase))
                    {
                        lstQMS010 = db.QMS010.Where(i => i.QualityProject.Equals(destQPFrom, StringComparison.OrdinalIgnoreCase)).ToList();
                    }
                    else
                    {
                        var minQTFrom = db.QMS010.Where(i => i.Project.Equals(destProjectFrom, StringComparison.OrdinalIgnoreCase)).Select(i => i.Id).FirstOrDefault();
                        var maxQTFrom = db.QMS010.Where(i => i.Project.Equals(destProjectFrom, StringComparison.OrdinalIgnoreCase)).Select(i => i.Id).FirstOrDefault();
                        lstQMS010 = db.QMS010.Where(i => i.Id >= minQTFrom && i.Id <= maxQTFrom).ToList();
                        lstQMS010 = lstQMS010.OrderBy(i => i.Id).ToList();
                    }
                }
                else
                {
                    var minQTFrom = db.QMS010.Where(i => i.Project.Equals(destProjectFrom, StringComparison.OrdinalIgnoreCase)).Select(i => i.Id).FirstOrDefault();
                    var maxQTFrom = db.QMS010.Where(i => i.Project.Equals(destProjectFrom, StringComparison.OrdinalIgnoreCase)).Max(i => i.Id);
                    var lstFrom = db.QMS010.Where(i => i.Id >= minQTFrom && i.Id <= maxQTFrom).ToList();

                    var minQTTo = db.QMS010.Where(i => i.Project.Equals(destProjectTo, StringComparison.OrdinalIgnoreCase)).Min(i => i.Id);
                    var maxQTTo = db.QMS010.Where(i => i.Project.Equals(destProjectTo, StringComparison.OrdinalIgnoreCase)).Select(i => i.Id).FirstOrDefault();
                    var lstTO = db.QMS010.Where(i => i.Id >= minQTTo && i.Id <= maxQTTo).ToList();

                    var lst = lstFrom.Union(lstTO);
                    lstQMS010.AddRange(lst);
                    lstQMS010 = lstQMS010.OrderBy(i => i.Id).ToList();
                }
                #endregion

                if (!string.IsNullOrEmpty(srcQIDs))
                {
                    var arrSrcQID = srcQIDs.Split(',');
                    var lstQIDs = db.QMS015.Where(i => i.QualityProject.Equals(srcQualityProj, StringComparison.OrdinalIgnoreCase) && arrSrcQID.Contains(i.QualityId)).ToList();

                    if (lstQIDs.Any())
                    {
                        foreach (var qid in lstQIDs)//List Of QualityProject with same QualityProject containing listed QIDs
                        {
                            foreach (var item in lstQMS010)
                            {
                                if (isQualityIdExist(item.QualityProject, item.Project, item.BU, currentLoc, qid.QualityId))
                                {
                                    inSkippedQID++;
                                }
                                else
                                {
                                    #region QID Header Transfer
                                    objQMS015Dest = new QMS015();
                                    objQMS015Dest.QualityProject = item.QualityProject;
                                    objQMS015Dest.Project = item.Project;
                                    objQMS015Dest.BU = item.BU;
                                    objQMS015Dest.Location = currentLoc;
                                    objQMS015Dest.QualityId = qid.QualityId;
                                    objQMS015Dest.QualityIdDesc = qid.QualityIdDesc;
                                    objQMS015Dest.QualityIdApplicableFor = qid.QualityIdApplicableFor;
                                    objQMS015Dest.RevNo = 0;
                                    objQMS015Dest.Status = clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue();
                                    objQMS015Dest.CreatedBy = objClsLoginInfo.UserName;
                                    objQMS015Dest.CreatedOn = DateTime.Now;
                                    db.QMS015.Add(objQMS015Dest);
                                    db.SaveChanges();
                                    #endregion

                                    if (withLines)
                                    {
                                        #region QID Stages

                                        string strStageStatus = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
                                        var lstStages = db.QMS016.Where(i => i.HeaderId == qid.HeaderId && i.QualityId == qid.QualityId && !i.StageCode.Equals(strStageStatus, StringComparison.OrdinalIgnoreCase)).ToList();
                                        if (lstStages.Any())
                                        {
                                            db.QMS016.AddRange(lstStages.Select(i => new QMS016
                                            {
                                                HeaderId = objQMS015Dest.HeaderId,
                                                QualityProject = item.QualityProject,
                                                Project = item.Project,
                                                BU = item.BU,
                                                Location = currentLoc,
                                                QualityId = i.QualityId,
                                                RevNo = objQMS015Dest.RevNo,
                                                StageCode = i.StageCode,
                                                StageSequance = i.StageSequance,
                                                StageStatus = clsImplementationEnum.QualityIdStageStatus.ADDED.GetStringValue(),
                                                AcceptanceStandard = i.AcceptanceStandard,
                                                ApplicableSpecification = i.ApplicableSpecification,
                                                InspectionExtent = i.InspectionExtent,
                                                HardnessRequiredValue = i.HardnessRequiredValue,
                                                HardnessUnit = i.HardnessUnit,
                                                FerriteMaxValue = i.FerriteMaxValue,
                                                FerriteMinValue = i.FerriteMinValue,
                                                FerriteUnit = i.FerriteUnit,
                                                Remarks = i.Remarks,
                                                CreatedBy = objClsLoginInfo.UserName,
                                                CreatedOn = DateTime.Now
                                            }));
                                        }
                                        #endregion
                                    }

                                    #region QID Identical Projects
                                    //var identicalProj = db.QMS017.Where(i => i.HeaderId == qid.HeaderId).ToList();
                                    //if (identicalProj.Any())
                                    //{
                                    //    db.QMS017.AddRange(identicalProj.Select(i => new QMS017
                                    //    {
                                    //        HeaderId = objQMS015Dest.HeaderId,
                                    //        QualityProject = objQMS015Dest.QualityProject,
                                    //        Project = objQMS015Dest.Project,
                                    //        BU = objQMS015Dest.BU,
                                    //        Location = currentLoc,
                                    //        //QualityId = qid.QualityId,
                                    //        RevNo = objQMS015Dest.RevNo,
                                    //        IQualityProject = i.IQualityProject,
                                    //        CreatedBy = objClsLoginInfo.UserName,
                                    //        CreatedOn = DateTime.Now
                                    //    }));
                                    //}
                                    #endregion
                                    db.SaveChanges();
                                    inTransferredQID++;
                                }
                            }
                        }
                    }
                }

                if (inSkippedQID > 0)
                {
                    objResponseMsg.Status = string.Format("system has skipped {0} QID", inSkippedQID);
                }
                if (inTransferredQID > 0)
                {
                    objResponseMsg.Value = string.Format("{0} QID data is transferred to destination project successfully", inTransferredQID);
                }

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult TransferQIDTODestinationNEW(string srcQualityProj, string srcQIDs, string destQPFrom, string destQPTo, bool withLines)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            int inTransferredQID = 0, inSkippedQID = 0;
            try
            {
                string currentUser = objClsLoginInfo.UserName;
                string notFoundStages = string.Empty;
                List<string> arrNotFoundStages = new List<string>();

                var currentLoc = (from a in db.COM003
                                  join b in db.COM002 on a.t_loca equals b.t_dimx
                                  where b.t_dtyp == 1
                                  && a.t_psno.Equals(currentUser, StringComparison.OrdinalIgnoreCase)
                                  select b.t_dimx).FirstOrDefault().ToString();

                List<QMS010> lstQMS010 = new List<QMS010>();
                List<QMS010> destQMSProjects = new List<QMS010>();

                #region Get all desctionation quality projects

                var lstATHLocation = db.ATH001.Where(i => i.Employee.Equals(currentUser, StringComparison.OrdinalIgnoreCase)).Select(i => i.Location).Distinct().ToList();
                var lstCOMLocation = db.COM003.Where(i => i.t_psno.Equals(currentUser, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_loca).Distinct().ToList();

                var lstCOMBU = db.ATH001.Where(i => i.Employee.Equals(currentUser, StringComparison.OrdinalIgnoreCase)).Select(i => i.BU).Distinct().ToList();

                lstCOMLocation = lstATHLocation.Union(lstCOMLocation).ToList();

                List<QMS010> allQMSProjects = db.QMS010.Where(i => lstCOMLocation.Contains(i.Location) //&& i.CreatedBy.Equals(username,StringComparison.OrdinalIgnoreCase)
                                      && lstCOMBU.Contains(i.BU)
                                      ).ToList();

                bool start = false;
                foreach (QMS010 QMSProj in allQMSProjects)
                {
                    if (QMSProj.QualityProject == destQPFrom)
                    {
                        start = true;
                    }
                    if (start)
                        destQMSProjects.Add(QMSProj);

                    if (QMSProj.QualityProject == destQPTo)
                    {
                        break;
                    }
                }
                #endregion

                if (!string.IsNullOrEmpty(srcQIDs))
                {
                    var arrSrcQID = srcQIDs.Split(',');
                    var lstQIDs = db.QMS015.Where(i => i.QualityProject.Equals(srcQualityProj, StringComparison.OrdinalIgnoreCase) && arrSrcQID.Contains(i.QualityId)).ToList();

                    var destLocations = destQMSProjects.Select(s => s.Location).Distinct();
                    var destBU = destQMSProjects.Select(s => s.BU).Distinct();
                    //var destStages = db.QMS002.Where(c => destLocations.Contains(c.Location) && destBU.Contains(c.BU)).Select(s => s.StageCode).Distinct();

                    // check for stages exists or not in destination location
                    foreach (var proj in destQMSProjects)
                    {
                        foreach (var qid in lstQIDs)
                        {
                            List<QMS016> qidStages = db.QMS016.Where(i => i.HeaderId == qid.HeaderId).ToList();
                            foreach (QMS016 stage in qidStages)
                            {
                                if (!db.QMS002.Where(c => c.StageCode == stage.StageCode && c.Location == proj.Location && c.BU == proj.BU).Any())
                                {
                                    if (!arrNotFoundStages.Contains(stage.StageCode))
                                        arrNotFoundStages.Add(stage.StageCode);
                                }
                            }
                        }
                    }

                    if (arrNotFoundStages.Count == 0)
                    {
                        foreach (var qid in lstQIDs)
                        {
                            foreach (var item in destQMSProjects)
                            {
                                if (isQualityIdExist(item.QualityProject, item.Project, item.BU, currentLoc, qid.QualityId))
                                {
                                    inSkippedQID++;
                                }
                                else
                                {
                                    #region QID Header Transfer
                                    QMS015 objQMS015Dest = new QMS015();
                                    objQMS015Dest.QualityProject = item.QualityProject;
                                    objQMS015Dest.Project = item.Project;
                                    objQMS015Dest.BU = item.BU;
                                    objQMS015Dest.Location = item.Location;
                                    objQMS015Dest.QualityId = qid.QualityId;
                                    objQMS015Dest.QualityIdDesc = qid.QualityIdDesc;
                                    objQMS015Dest.QualityIdApplicableFor = qid.QualityIdApplicableFor;
                                    objQMS015Dest.RevNo = 0;
                                    objQMS015Dest.Status = clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue();
                                    objQMS015Dest.CreatedBy = objClsLoginInfo.UserName;
                                    objQMS015Dest.CreatedOn = DateTime.Now;
                                    db.QMS015.Add(objQMS015Dest);
                                    db.SaveChanges();
                                    #endregion

                                    if (withLines)
                                    {
                                        #region QID Stages

                                        string strStageStatus = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
                                        var lstStages = db.QMS016.Where(i => i.HeaderId == qid.HeaderId && i.QualityId == qid.QualityId && !i.StageCode.Equals(strStageStatus, StringComparison.OrdinalIgnoreCase)).ToList();
                                        if (lstStages.Count > 0)
                                        {
                                            db.QMS016.AddRange(lstStages.Select(i => new QMS016
                                            {
                                                HeaderId = objQMS015Dest.HeaderId,
                                                QualityProject = item.QualityProject,
                                                Project = item.Project,
                                                BU = item.BU,
                                                Location = item.Location,
                                                QualityId = i.QualityId,
                                                RevNo = objQMS015Dest.RevNo,
                                                StageCode = i.StageCode,
                                                StageSequance = i.StageSequance,
                                                StageStatus = clsImplementationEnum.QualityIdStageStatus.ADDED.GetStringValue(),
                                                AcceptanceStandard = i.AcceptanceStandard,
                                                ApplicableSpecification = i.ApplicableSpecification,
                                                InspectionExtent = i.InspectionExtent,
                                                HardnessRequiredValue = i.HardnessRequiredValue,
                                                HardnessUnit = i.HardnessUnit,
                                                FerriteMaxValue = i.FerriteMaxValue,
                                                FerriteMinValue = i.FerriteMinValue,
                                                FerriteUnit = i.FerriteUnit,
                                                Remarks = i.Remarks,
                                                CreatedBy = objClsLoginInfo.UserName,
                                                CreatedOn = DateTime.Now
                                            }));
                                            db.SaveChanges();

                                            #region Chemical Test
                                            List<QMS016> lstQms16 = db.QMS016.Where(x => x.HeaderId == objQMS015Dest.HeaderId).ToList();
                                            foreach (var item1 in lstStages)
                                            {
                                                QMS016 objQIDLines = db.QMS016.Where(x => x.StageCode == item1.StageCode && x.StageSequance == item1.StageSequance && x.HeaderId == objQMS015Dest.HeaderId).FirstOrDefault();
                                                QMS018 objSP = db.QMS018.Where(x => x.QIDLineId == item1.LineId).FirstOrDefault();
                                                if (objSP != null)
                                                {
                                                    int samplingPlanId = objSP.HeaderId;
                                                    QMS018 insertSP = new QMS018();
                                                    insertSP = objSP;
                                                    insertSP.Location = objQIDLines.Location;
                                                    insertSP.BU = objQIDLines.BU;
                                                    insertSP.Project = objQIDLines.Project;
                                                    insertSP.QualityProject = objQIDLines.QualityProject;
                                                    insertSP.QIDLineId = objQIDLines.LineId;
                                                    insertSP.QualityId = objQIDLines.QualityId;
                                                    insertSP.QualityIdRev = objQIDLines.RevNo;
                                                    insertSP.CreatedBy = objClsLoginInfo.UserName;
                                                    insertSP.CreatedOn = DateTime.Now;
                                                    insertSP.CreatedBy = objClsLoginInfo.UserName;
                                                    insertSP.CreatedOn = DateTime.Now;
                                                    db.QMS018.Add(insertSP);
                                                    db.SaveChanges();

                                                    List<QMS019> existingElement = db.QMS019.Where(x => x.HeaderId == samplingPlanId).ToList();
                                                    db.QMS019.AddRange(existingElement.Select(i => new QMS019
                                                    {
                                                        HeaderId = insertSP.HeaderId,
                                                        QIDLineId = insertSP.QIDLineId,
                                                        Project = insertSP.Project,
                                                        QualityProject = insertSP.QualityProject,
                                                        BU = insertSP.BU,
                                                        Location = insertSP.Location,
                                                        QualityId = insertSP.QualityId,
                                                        QualityIdRev = insertSP.QualityIdRev,
                                                        StageCode = insertSP.StageCode,
                                                        StageSequence = insertSP.StageSequence,
                                                        Element = i.Element,
                                                        ReqMinValue = i.ReqMinValue,
                                                        ReqMaxValue = i.ReqMaxValue,
                                                        CreatedBy = objClsLoginInfo.UserName,
                                                        CreatedOn = DateTime.Now
                                                    }));
                                                    db.SaveChanges();
                                                    //insertSP.HeaderId
                                                }
                                                #endregion
                                            }
                                        }
                                        #endregion
                                    }
                                    inTransferredQID++;
                                }
                            }
                        }
                    }

                }

                if (arrNotFoundStages.Count > 0)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = string.Format("{0} - missing Stages for destination Location or BU", string.Join(", ", arrNotFoundStages));
                }
                if (inSkippedQID > 0)
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Status = string.Format("system has skipped {0} QID", inSkippedQID);
                }
                if (inTransferredQID > 0)
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = string.Format("{0} QID data is transferred to destination project successfully", inTransferredQID);
                }


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region History
        public ActionResult GetHistoryDetails(int? headerId)
        {
            QMS015 objQMS015 = db.QMS015.Where(i => i.HeaderId == headerId).FirstOrDefault();
            return PartialView("_GetHistoryDetailsPartial", objQMS015);
        }

        [HttpPost]
        public JsonResult LoadHistoryDetailData(JQueryDataTableParamModel param, int headerId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";

                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms15.BU", "qms15.Location");
                whereCondition += "and qms15.HeaderId = " + headerId;


                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " and (qms15.Status like '%" + param.sSearch + "%' or qms15.RevNo like '%" + param.sSearch + "%' or (qms15.QualityId+'-'+qms15.QualityIdDesc) like '%" + param.sSearch + "%')";
                }
                var lstResult = db.SP_IPI_GetQualityIDHeaderHistory(StartIndex, EndIndex, "", whereCondition).ToList();

                int? totalRecords = lstResult.Select(i => i.TotalCount).FirstOrDefault();
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var data = (from uc in lstResult
                            select new[]
                           {
                           Convert.ToString(uc.ROW_NO),
                           Convert.ToString(uc.Id),
                           uc.QualityId +" - "+uc.QualityIdDesc,
                           Convert.ToString("R" + uc.RevNo),
                           Convert.ToString(uc.Status),
                           uc.CreatedBy,
                           Convert.ToDateTime(uc.CreatedOn).ToString("dd/MM/yyyy"),
                           uc.EditedBy,
                           Convert.ToDateTime(uc.EditedOn).ToString("dd/MM/yyyy")
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult HistoryView(int id)
        {
            ViewBag.id = id;
            QMS015_Log objQMS015 = db.QMS015_Log.FirstOrDefault(x => x.Id == id);

            var location = (from a in db.COM003
                            join b in db.COM002 on a.t_loca equals b.t_dimx
                            where b.t_dtyp == 1 && a.t_psno.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                            select b.t_dimx + " - " + b.t_desc).FirstOrDefault();

            ViewBag.Location = location;
            ViewBag.Project = db.COM001.Where(i => i.t_cprj.Equals(objQMS015.Project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + "-" + i.t_dsca).FirstOrDefault();
            //ViewBag.IdenticalProject = string.Join(",", db.QMS017.Where(i => i.HeaderId == id).Select(i => i.IQualityProject).ToList());

            string BU = db.COM001.Where(i => i.t_cprj == objQMS015.Project).FirstOrDefault().t_entu;

            var BUDescription = (from a in db.COM002
                                 where a.t_dtyp == 2 && a.t_dimx == BU
                                 select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();

            ViewBag.BU = BUDescription.BUDesc;
            return View(objQMS015);
        }

        [HttpPost]
        public JsonResult LoadLinesHistoryData(JQueryDataTableParamModel param, int refId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                var totalRecords = new ObjectParameter("totalRecords", typeof(int));
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                //string SortingFields = param.iSortingCols;
                //readonly= "readonly"

                Func<QCP003_Log, string> orderingFunction = (tsk =>
                                        sortColumnIndex == 2 && isLocationSortable ? Convert.ToString(tsk.Location) : "");
                string whereCondition = "1=1";
                whereCondition += " and qms16.RefId = " + refId;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " and ( qms16.QualityProject like '%" + param.sSearch + "%' or qms16.Project like '%" + param.sSearch + "%' or QualityId like '%" + param.sSearch + "%' or StageCode like '%" + param.sSearch + "%' or StageStatus like '%" + param.sSearch + "%'  or AcceptanceStandard like '%" + param.sSearch + "%'   or ApplicableSpecification like '%" + param.sSearch + "%' or InspectionExtent like '%" + param.sSearch + "%' or RevisionDesc like '%" + param.sSearch + "%' or Remarks like '%" + param.sSearch + "%' or CreatedBy like '%" + param.sSearch + "%')";
                }
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = string.Empty; ;
                sortDirection = Convert.ToString(Request["sSortDir_0"]); // asc or desc
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstSublines = db.SP_IPI_GetQualityIDLinesHistory(StartIndex, EndIndex, "", whereCondition).ToList();
                int totalRecord = Convert.ToInt32(lstSublines.Select(x => x.TotalCount).FirstOrDefault());
                string StageTypeExludedinOfferDropdown = clsImplementationEnum.ConfigParameter.StageTypeExludedinOfferDropdown.GetStringValue();
                var lstStages = Manager.GetConfigValue(StageTypeExludedinOfferDropdown).ToUpper().Split(',').ToArray();

                var data = (from uc in lstSublines
                            select new[]
                            {
                           Convert.ToString(uc.ROW_NO),
                           Convert.ToString(uc.QualityProject),
                           Convert.ToString(uc.Project),
                           Convert.ToString(uc.QualityId),
                           Convert.ToString(uc.StageCode),
                           Convert.ToString(uc.StageSequance),
                           Convert.ToString(uc.StageStatus),
                           Convert.ToString(uc.AcceptanceStandard),
                           Convert.ToString(uc.ApplicableSpecification),
                           Convert.ToString(uc.InspectionExtent),
                           Convert.ToString(uc.Remarks),
                           Convert.ToString(uc.CreatedBy),
                           Convert.ToDateTime(uc.CreatedOn).ToString("dd/MM/yyyy HH:MM"),
                           Helper.MaintainIPIStagetypewiseGridButton(uc.HeaderId,uc.LineId,uc.QualityProject,uc.Project.Split('-')[0].Trim(),uc.BU.Split('-')[0].Trim(),uc.Location.Split('-')[0].Trim(),uc.QualityId,uc.RevNo,uc.StageCode.Split('-')[0].Trim(),uc.StageCode.Split('-')[0].Trim(),uc.StageSequance,false,lstStages.Contains(uc.StageType),true,uc.StageType)
                           //Helper.MaintainIPISamplingPlanGridButton(uc.HeaderId,uc.LineId,uc.RevNo,uc.StageCode,false,(new QualityIdController()).IsSamplingPlanisMaintained(uc.HeaderId, uc.StageCode.Split('-')[0].Trim(), uc.BU.Split('-')[0].Trim(), uc.Location.Split('-')[0].Trim(),true),true)
                           }).ToList();
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = totalRecord,
                    iTotalDisplayRecords = totalRecord,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Send For Approval
        [HttpPost]
        public ActionResult MultiQualityIdSendForApproval(string strHeader)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();

            List<QMS015> lstQualityIds = new List<QMS015>();
            List<int> QualityHeaderIds = new List<int>();
            List<int> skippedQualityIds = new List<int>();
            try
            {

                if (!string.IsNullOrEmpty(strHeader))
                {
                    #region old Code
                    //int[] headerIds = Array.ConvertAll(strHeader.Split(','), s => int.Parse(s));
                    //lstQualityIds = db.QMS015.Where(i => headerIds.Contains(i.HeaderId)).ToList();
                    //var lstQMS016 = db.QMS016.Where(i => headerIds.Contains(i.HeaderId)).ToList();
                    //lstQualityIds.ForEach(i =>
                    //{
                    //    i.Status = clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(); i.SubmittedBy = objClsLoginInfo.UserName; i.SubmittedOn = DateTime.Now;
                    //});

                    //lstQMS016.ForEach(i =>
                    //{
                    //     i.SubmittedBy = objClsLoginInfo.UserName; i.SubmittedOn = DateTime.Now;
                    //});
                    #endregion

                    string[] arrayHeaderIds = strHeader.Split(',').ToArray();
                    foreach (var item in arrayHeaderIds)
                    {
                        int HeaderId = Convert.ToInt32(item);

                        skippedQualityIds.Add(HeaderId);

                    }
                    lstQualityIds = db.QMS015.Where(i => QualityHeaderIds.Contains(i.HeaderId)).ToList();
                    lstQualityIds.ForEach(i =>
                    {
                        i.Status = clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(); i.SubmittedBy = objClsLoginInfo.UserName; i.SubmittedOn = DateTime.Now;
                    });
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = string.Format(clsImplementationMessage.QualityIDMessage.SentForApproval, lstQualityIds.Count);
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please Select QualityId for sending for approval";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SendForApproval(string strHeader)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();

            List<QMS015> lstQualityIds = new List<QMS015>();
            List<string> lstQIDWithoutLines = new List<string>();
            List<string> lstQIDNotDraft = new List<string>();
            List<string> lstQIDSubmitted = new List<string>();
            List<string> lstMaintainSamplingplan = new List<string>();
            List<string> lstMaintainHardness = new List<string>();
            List<string> lstMaintainFerrite = new List<string>();
            List<string> lstMaintainPMI = new List<string>();
            try
            {
                if (!string.IsNullOrEmpty(strHeader))
                {
                    int[] headerIds = Array.ConvertAll(strHeader.Split(','), s => int.Parse(s));
                    lstQualityIds = db.QMS015.Where(i => headerIds.Contains(i.HeaderId)).ToList();

                    foreach (var item in lstQualityIds)
                    {
                        var qms15 = db.QMS015.Where(i => i.HeaderId == item.HeaderId).FirstOrDefault();
                        if (IsSamplingPlanisMaintained(item.HeaderId, null, item.BU, item.Location, false))
                        {
                            if (IsSamplingPlanisMaintained(item.HeaderId, null, item.BU, item.Location, false, false))
                            {
                                if (IsDataMaintained(item.HeaderId, null, item.BU, item.Location, false, true))
                                {
                                    if (IsDataMaintained(item.HeaderId, null, item.BU, item.Location, false))
                                    {
                                        if (qms15 != null)
                                        {
                                            #region send for approval
                                            if (string.Equals(qms15.Status, clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue()) || string.Equals(qms15.Status, clsImplementationEnum.QualityIdHeaderStatus.Returned.GetStringValue()))
                                            {
                                                if (db.QMS016.Where(i => i.HeaderId == qms15.HeaderId).Any())
                                                {
                                                    qms15.Status = clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue();
                                                    qms15.SubmittedBy = objClsLoginInfo.UserName;
                                                    qms15.SubmittedOn = DateTime.Now;
                                                    qms15.ReturnedBy = null;
                                                    qms15.ReturnedOn = null;
                                                    qms15.ReturnRemark = null;
                                                    db.QMS016.Where(i => i.HeaderId == qms15.HeaderId).ToList().ForEach(i => { i.SubmittedBy = objClsLoginInfo.UserName; i.SubmittedOn = DateTime.Now; });
                                                    db.SaveChanges();

                                                    #region Send Notification
                                                    (new clsManager()).SendNotification(qms15.QualityIdApplicableFor == clsImplementationEnum.QualityIDApplicable.SEAM.GetStringValue() ? "WE2" : "QC2", qms15.Project, qms15.BU, qms15.Location, "QID: " + qms15.QualityId + " has been submitted for your approval", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/ApproveQualityId/ViewQualityId?headerId=" + qms15.HeaderId);
                                                    #endregion

                                                    lstQIDSubmitted.Add(qms15.QualityId);
                                                }
                                                else
                                                {
                                                    lstQIDWithoutLines.Add(qms15.QualityId);
                                                }
                                            }
                                            else
                                            {
                                                lstQIDNotDraft.Add(qms15.QualityId);
                                            }
                                            #endregion
                                        }
                                    }
                                    else
                                    {
                                        lstMaintainFerrite.Add(qms15.QualityId);
                                    }
                                }
                                else
                                {
                                    lstMaintainHardness.Add(qms15.QualityId);
                                }
                            }
                            else
                            {
                                lstMaintainPMI.Add(qms15.QualityId);
                            }
                        }
                        else
                        {
                            if (qms15 != null)
                            {
                                lstMaintainSamplingplan.Add(qms15.QualityId);
                            }
                        }
                    }
                    objResponseMsg.Key = true;
                    if (lstQIDSubmitted.Any())
                    {
                        objResponseMsg.Value = string.Format(clsImplementationMessage.QualityIDMessage.SentForApproval, lstQIDSubmitted.Count);
                    }
                    if (lstQIDWithoutLines.Any())
                    {
                        objResponseMsg.WithoutLines = string.Format("No Stage(s) Exist(s) For QualityID :{0}.Please Add Stages.", string.Join(",", lstQIDWithoutLines));
                    }
                    if (lstQIDNotDraft.Any())
                    {
                        objResponseMsg.NotDraft = string.Format("QualityID(s) {0} have been skipped as QualityID(s) {0} are either Sent For Approval Or Approved", string.Join(",", lstQIDNotDraft));
                    }
                    if (lstMaintainSamplingplan.Any())
                    {
                        objResponseMsg.Remarks = string.Format("QualityID(s) {0} have been skipped.Please 'Maintain Sampling Plan and Element for Chemical Stages'.", string.Join(",", lstMaintainSamplingplan));
                    }
                    if (lstMaintainHardness.Any())
                    {
                        objResponseMsg.dataValue = string.Format("QualityID(s) {0} have been skipped.Please 'Maintain hardness required Value'.", string.Join(",", lstMaintainHardness));
                    }
                    if (lstMaintainFerrite.Any())
                    {
                        objResponseMsg.TPISkipped = string.Format("QualityID(s) {0} have been skipped.Please 'Maintain ferrite test required range'.", string.Join(",", lstMaintainFerrite));
                    }
                    if (lstMaintainPMI.Any())
                    {
                        objResponseMsg.tpi = string.Format("QualityID(s) {0} have been skipped.Please 'Maintain Sampling Plan and Element for PMI Stages'.", string.Join(",", lstMaintainPMI));
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please Select QualityID for sending for approval";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }




        [HttpPost]
        public ActionResult SendForApproval_Old(int headerId)
        {
            QMS015 objQMS015 = new QMS015();
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (headerId > 0)
                {
                    objQMS015 = db.QMS015.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    if (!string.Equals(objQMS015.Status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
                    {
                        var lstQMS016 = db.QMS016.Where(i => i.HeaderId == headerId).ToList();
                        if (lstQMS016.Any())
                        {
                            objQMS015.Status = clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue();
                            objQMS015.SubmittedBy = objClsLoginInfo.UserName;
                            objQMS015.SubmittedOn = DateTime.Now;

                            //lstQMS016.ForEach(i =>
                            //{
                            //    i.SubmittedBy = objClsLoginInfo.UserName; i.SubmittedOn = DateTime.Now;
                            //});

                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = string.Format(clsImplementationMessage.QualityIDMessage.SentForApproval, objQMS015.QualityId);
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "No Lines Exists for " + objQMS015.QualityId + " . Please add lines  ";
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.QualityIDMessage.SentForApproval;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region QID Common Methods

        //public 
        public bool StageTypeAction(int headerid, string stagecode, string bu, string location, bool isGridlevel = false)
        {
            bool flag = false;
            string stagetype = Manager.FetchStageType(stagecode, bu, location);
            string chemical = clsImplementationEnum.SeamStageType.Chemical.GetStringValue();
            string pmi = clsImplementationEnum.SeamStageType.PMI.GetStringValue();
            string hardness = clsImplementationEnum.SeamStageType.Hardness.GetStringValue();
            string ferrite = clsImplementationEnum.SeamStageType.Ferrite.GetStringValue();
            if (stagetype == chemical || stagetype == pmi)
            {
                flag = IsSamplingPlanisMaintained(headerid, stagecode, bu, location, isGridlevel);
            }
            if (stagetype == hardness)
            {
                flag = IsDataMaintained(headerid, stagecode, bu, location, isGridlevel, true);
            }
            if (stagetype == ferrite)
            {
                flag = IsDataMaintained(headerid, stagecode, bu, location, isGridlevel);
            }
            return flag;
        }
        public bool IsDataMaintained(int headerid, string stagecode, string bu, string location, bool isGridlevel = false, bool ishardness = false)
        {
            string[] status = { clsImplementationEnum.QualityIdStageStatus.ADDED.GetStringValue(), clsImplementationEnum.QualityIdStageStatus.MODIFIED.GetStringValue() };
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            bool flag = true;
            string hardness = clsImplementationEnum.SeamStageType.Hardness.GetStringValue();
            string ferrite = clsImplementationEnum.SeamStageType.Ferrite.GetStringValue();
            List<QMS016> lstQidHardnessLines = new List<QMS016>();
            if (isGridlevel)
            {
                if (ishardness)
                {
                    flag = Manager.FetchStageType(stagecode, bu, location) == hardness;
                }
                else
                {
                    flag = Manager.FetchStageType(stagecode, bu, location) == ferrite;
                }
            }
            else
            {
                if (ishardness)
                {
                    lstQidHardnessLines = (from q16 in db.QMS016
                                           join q02 in db.QMS002 on q16.StageCode.Trim() equals q02.StageCode.Trim()
                                           where q16.HeaderId == headerid && q02.StageType == hardness && status.Contains(q16.StageStatus)
                                           select q16
                                           ).ToList();
                    foreach (var item in lstQidHardnessLines)
                    {
                        if ((item.HardnessRequiredValue != null || item.HardnessRequiredValue > 0) && !string.IsNullOrWhiteSpace(item.HardnessUnit))
                        {
                            flag = true;
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                }
                else
                {
                    List<QMS016> lstQidFerriteLines = new List<QMS016>();
                    lstQidFerriteLines = (from q16 in db.QMS016
                                          join q02 in db.QMS002 on q16.StageCode.Trim() equals q02.StageCode.Trim()
                                          where q16.HeaderId == headerid && q02.StageType == ferrite && status.Contains(q16.StageStatus)
                                          select q16
                                           ).ToList();
                    foreach (var item in lstQidFerriteLines)
                    {
                        if ((item.FerriteMinValue != null || item.FerriteMinValue > 0) && (item.FerriteMaxValue != null || item.FerriteMaxValue > 0) && !string.IsNullOrWhiteSpace(item.FerriteUnit))
                        {
                            flag = true;
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                }
            }
            return flag;
        }

        public bool IsSamplingPlanisMaintained(int headerid, string stagecode, string bu, string location, bool isGridlevel = false, bool isChemCheck = true)
        {
            bool flag = true;
            string chemical = clsImplementationEnum.SeamStageType.Chemical.GetStringValue();
            string pmi = clsImplementationEnum.SeamStageType.PMI.GetStringValue();
            List<QMS016> lstQidChemicalLines = new List<QMS016>();
            if (isGridlevel)
            {
                string type = Manager.FetchStageType(stagecode, bu, location);
                flag = (type == chemical || type == pmi);
            }
            else
            {
                string[] status = { clsImplementationEnum.QualityIdStageStatus.ADDED.GetStringValue(), clsImplementationEnum.QualityIdStageStatus.MODIFIED.GetStringValue() };
                if (isChemCheck)
                {
                    lstQidChemicalLines = (from q16 in db.QMS016
                                           join q02 in db.QMS002 on q16.StageCode.Trim() equals q02.StageCode.Trim()
                                           where q16.HeaderId == headerid && q02.StageType == chemical && status.Contains(q16.StageStatus)
                                           select q16
                                           ).ToList();
                }
                else
                {
                    lstQidChemicalLines = (from q16 in db.QMS016
                                           join q02 in db.QMS002 on q16.StageCode.Trim() equals q02.StageCode.Trim()
                                           where q16.HeaderId == headerid && q02.StageType == pmi && status.Contains(q16.StageStatus)
                                           select q16
                                         ).ToList();
                }

                foreach (var item in lstQidChemicalLines)
                {
                    QMS018 objSamplingplan = db.QMS018.Where(x => x.QIDLineId == item.LineId && x.QualityIdRev == item.RevNo).FirstOrDefault();
                    if (objSamplingplan != null)
                    {
                        if (objSamplingplan.QMS019.Count() > 0)
                        {
                            flag = true;
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                    else
                    {
                        flag = false;
                        break;
                    }
                }
            }
            return flag;
        }
        [HttpPost]
        public ActionResult GetQualityProjectDetails(string term)
        {
            string username = objClsLoginInfo.UserName;
            var lstATHLocation = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.Location).Distinct().ToList();
            var lstCOMLocation = db.COM003.Where(i => i.t_psno.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_loca).Distinct().ToList();

            var lstCOMBU = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.BU).Distinct().ToList();

            lstCOMLocation = lstATHLocation.Union(lstCOMLocation).ToList();

            List<Projects> lstQualityProject = new List<Projects>();
            if (!string.IsNullOrEmpty(term))
            {
                lstQualityProject = db.QMS010.Where(i => i.QualityProject.ToLower().Contains(term.ToLower())
                                                                 && lstCOMLocation.Contains(i.Location)
                                                                 && lstCOMBU.Contains(i.BU)
                                                               ).Select(i => new Projects { Value = i.QualityProject, projectCode = i.QualityProject }).Distinct().ToList();//&& i.CreatedBy.Equals(username,StringComparison.OrdinalIgnoreCase)
            }
            else
            {
                lstQualityProject = db.QMS010.Where(i => lstCOMLocation.Contains(i.Location)
                                                        && lstCOMBU.Contains(i.BU)
                                                        ).Select(i => new Projects { Value = i.QualityProject, projectCode = i.QualityProject }).Take(10).Distinct().ToList();
            }

            return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDestQualityProjectDetails(string term)
        {
            string username = objClsLoginInfo.UserName;
            var lstATHLocation = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.Location).Distinct().ToList();
            var lstCOMLocation = db.COM003.Where(i => i.t_psno.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_loca).Distinct().ToList();

            var lstCOMBU = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.BU).Distinct().ToList();

            lstCOMLocation = lstATHLocation.Union(lstCOMLocation).ToList();

            //var location = (from a in db.COM003
            //                join b in db.COM002 on a.t_loca equals b.t_dimx
            //                where b.t_dtyp == 1 && a.t_psno.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
            //                select b.t_dimx).FirstOrDefault();

            List<Projects> lstQualityProject = new List<Projects>();

            if (!string.IsNullOrEmpty(term))
            {
                lstQualityProject = db.QMS010.Where(i => i.QualityProject.ToLower().Contains(term.ToLower())
                                                && lstCOMLocation.Contains(i.Location) //&& i.CreatedBy.Equals(username,StringComparison.OrdinalIgnoreCase)
                                                && lstCOMBU.Contains(i.BU)
                                                ).Select(i => new Projects { Value = i.QualityProject, projectCode = i.QualityProject }).ToList();
            }
            else
            {
                lstQualityProject = db.QMS010.Where(i => lstCOMLocation.Contains(i.Location)
                                                        && lstCOMBU.Contains(i.BU)
                                                        ).Select(i => new Projects { Value = i.QualityProject, projectCode = i.QualityProject }).Take(10).ToList();
            }
            return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetQualityProjectWiseProjectBUDetail(string qualityProject, string location, string qid)
        {
            ModelQualityId objModelQualityId = new ModelQualityId();
            var project = db.QMS010.Where(i => i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase)).Select(i => i.Project).FirstOrDefault();
            objModelQualityId.Project = db.COM001.Where(i => i.t_cprj.Equals(project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + "-" + i.t_dsca).FirstOrDefault();// project;
            var ProjectWiseBULocation = Manager.getProjectWiseBULocation(project);
            objModelQualityId.QCPBU = ProjectWiseBULocation.QCPBU;
            if (isQualityIdExist(qualityProject, project, ProjectWiseBULocation.QCPBU.Split('-')[0], location, qid))
                objModelQualityId.headerAlreadyExist = true;

            var identicalProject = db.QMS017.Where(i => i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase)).Select(i => i.IQualityProject).Distinct().ToList();
            objModelQualityId.IdenticalProj = string.Join(",", identicalProject);
            return Json(objModelQualityId, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetIdenticalProjects(string search, string param, string bu = "", string loc = "")
        {
            List<ddlValue> identicalProject = new List<ddlValue>();
            if (!string.IsNullOrEmpty(search))
            {
                identicalProject = db.QMS010.Where(i => !i.QualityProject.Equals(param, StringComparison.OrdinalIgnoreCase) && i.BU == bu && i.Location == loc && i.QualityProject.ToLower().Contains(search.ToLower())).Select(i => new ddlValue { id = i.QualityProject, text = i.QualityProject }).Distinct().ToList();
            }
            else
            {
                identicalProject = db.QMS010.Where(i => !i.QualityProject.Equals(param, StringComparison.OrdinalIgnoreCase) && i.BU == bu && i.Location == loc).Select(i => new ddlValue { id = i.QualityProject, text = i.QualityProject }).Distinct().Take(10).ToList();
            }
            return Json(identicalProject, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetIdenticalProjectEdit(string param)
        {
            List<ddlValue> identicalProject = db.QMS010.Where(i => !i.QualityProject.Equals(param, StringComparison.OrdinalIgnoreCase)).Select(i => new ddlValue { id = i.QualityProject, text = i.QualityProject }).Distinct().ToList();
            return Json(identicalProject, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSquence(string stagecode)
        {
            var Sequence = db.QMS002.Where(i => i.StageCode == stagecode).FirstOrDefault().SequenceNo;
            return Json(Sequence, JsonRequestBehavior.AllowGet);
        }

        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", bool isDisable = false)
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (isDisable)
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer; opacity:0.3;margin-left:5px;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";
            }
            else
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;margin-left:5px;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            }
            return htmlControl;
        }

        public string HTMLAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string maxlength = "", string validate = "")
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string FieldValidate = !string.IsNullOrEmpty(validate) ? "validate='" + validate + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onchange='" + onBlurMethod + "'" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' hdElement='" + hdElementId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + (disabled ? "disabled" : "") + " " + MaxCharcters + " " + FieldValidate + " />";

            return strAutoComplete;
        }

        public bool isQualityIdExist(string qualityProject, string project, string bu, string location, string qid, int headerId = 0)
        {
            bool isQualityIdExist = false;
            List<QMS015> lstQMS015 = db.QMS015.Where(i => i.Location.Equals(location, StringComparison.OrdinalIgnoreCase)).ToList();

            if (!string.IsNullOrEmpty(qualityProject) && !string.IsNullOrEmpty(project) && !string.IsNullOrEmpty(bu) && !string.IsNullOrEmpty(qid))
            {
                lstQMS015 = lstQMS015.Where(i => i.QualityProject.Trim().Equals(qualityProject.Trim(), StringComparison.OrdinalIgnoreCase) && i.Project.Trim().Equals(project.Trim(), StringComparison.OrdinalIgnoreCase) && i.BU.Trim().Equals(bu.Trim(), StringComparison.OrdinalIgnoreCase) && i.QualityId.Trim().Equals(qid.Trim(), StringComparison.OrdinalIgnoreCase) && i.HeaderId != headerId).ToList();

                if (lstQMS015.Any())
                    isQualityIdExist = true;
            }

            return isQualityIdExist;
        }

        public bool isIdenticalProjectExist(string qualityProj, string bu, string location, string qualityId, string iQualityProj)
        {
            bool isIdenticalProjectExist = false;
            List<QMS017> lstQMS017 = new List<QMS017>();

            if (!string.IsNullOrEmpty(qualityProj) && !string.IsNullOrEmpty(qualityProj) && !string.IsNullOrEmpty(qualityProj) && !string.IsNullOrEmpty(qualityProj) && !string.IsNullOrEmpty(qualityProj))
            {
                lstQMS017 = db.QMS017.Where(i => i.QualityProject.Equals(qualityProj, StringComparison.OrdinalIgnoreCase) && i.BU.Equals(bu, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && i.IQualityProject.Equals(iQualityProj, StringComparison.OrdinalIgnoreCase)).ToList();
                if (lstQMS017.Any())
                    isIdenticalProjectExist = true;
            }

            return isIdenticalProjectExist;
        }

        public bool isQualityIdStageExist(string qualityProj, string bu, string location, string qualityId, int revNo, string stageCode, int lineId)
        {
            string p_qualityProj, p_bu, p_location, p_qualityId, p_stageCode;
            p_qualityProj = qualityProj.Trim();
            p_bu = bu.Trim();
            p_location = location.Trim();
            p_qualityId = qualityId.Trim();
            p_stageCode = stageCode.Trim();

            bool isQualityIdStageExist = false;
            if (!string.IsNullOrEmpty(qualityProj) && !string.IsNullOrEmpty(bu) && !string.IsNullOrEmpty(location) && !string.IsNullOrEmpty(qualityId) && !string.IsNullOrEmpty(stageCode))
            {
                string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
                var lstQMS016 = db.QMS016.Where(i => i.QualityProject.Equals(p_qualityProj, StringComparison.OrdinalIgnoreCase) &&
                                                   i.BU.Equals(p_bu, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(p_location, StringComparison.OrdinalIgnoreCase) &&
                                                   i.QualityId.Equals(p_qualityId, StringComparison.OrdinalIgnoreCase) && i.RevNo == revNo &&
                                                   i.StageCode.Equals(p_stageCode, StringComparison.OrdinalIgnoreCase) && i.LineId != lineId && i.StageStatus != deleted).ToList();
                if (lstQMS016.Any())
                    isQualityIdStageExist = true;
            }

            return isQualityIdStageExist;
        }
        [NonAction]
        public static string GenerateGridButton(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";


            htmlControl = "<a id='" + inputID + "' name='" + inputID + "' class='btn btn-outline btn-circle btn-sm blue' " + onClickEvent + " ><i class='" + className + "'></i> " + buttonName + "</a>";

            //htmlControl = "<i  data-modal='' id='" + inputID + "' name='" + inputID + "' style='cursor:Pointer;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";

            return htmlControl;
        }
        #endregion
        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            if (LineId > 0)
            {
                QMS016 objQMS016 = db.QMS016.Where(x => x.LineId == LineId).FirstOrDefault();
                model.ApprovedBy = objQMS016.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objQMS016.ApprovedBy) : null;
                model.ApprovedOn = objQMS016.ApprovedOn;
                model.SubmittedBy = objQMS016.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objQMS016.SubmittedBy) : null;
                model.SubmittedOn = objQMS016.SubmittedOn;
                model.CreatedBy = objQMS016.CreatedBy != null ? Manager.GetUserNameFromPsNo(objQMS016.CreatedBy) : null;
                model.CreatedOn = objQMS016.CreatedOn;
                model.EditedBy = objQMS016.EditedBy != null ? Manager.GetUserNameFromPsNo(objQMS016.EditedBy) : null;
                model.EditedOn = objQMS016.EditedOn;
            }
            else
            {
                QMS015 objQMS015 = db.QMS015.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.ApprovedBy = objQMS015.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objQMS015.ApprovedBy) : null;
                model.ApprovedOn = objQMS015.ApprovedOn;
                model.SubmittedBy = objQMS015.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objQMS015.SubmittedBy) : null;
                model.SubmittedOn = objQMS015.SubmittedOn;
                model.CreatedBy = objQMS015.CreatedBy != null ? Manager.GetUserNameFromPsNo(objQMS015.CreatedBy) : null;
                model.CreatedOn = objQMS015.CreatedOn;
                model.EditedBy = objQMS015.EditedBy != null ? Manager.GetUserNameFromPsNo(objQMS015.EditedBy) : null;
                model.EditedOn = objQMS015.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress2.cshtml", model);
        }
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    List<SP_IPI_GET_QUALITYPROJECT_RESULT_Result> lst = db.SP_IPI_GET_QUALITYPROJECT_RESULT(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    List<SP_IPI_GetQualityIDHeader_Result> lst = db.SP_IPI_GetQualityIDHeader(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      QualityIdApplicableFor = li.QualityIdApplicableFor,
                                      QualityId = li.QualityId,
                                      QualityIdDesc = li.QualityIdDesc,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      RevNo = li.RevNo,
                                      Status = li.Status
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);

                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYVIEW.GetStringValue())
                {
                    var lst = db.SP_IPI_GetQualityIDHeaderHistory(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      QualityId = li.QualityId,
                                      QualityIdDesc = li.QualityIdDesc,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      RevNo = li.RevNo,
                                      Status = li.Status
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {
                    var lst = db.SP_IPI_GetQualityIDLinesHistory(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      QualityId = li.QualityId,
                                      RevNo = li.RevNo,
                                      Status = li.StageStatus,
                                      StageCode = li.StageCode,
                                      StageSequance = li.StageSequance,
                                      StageStatus = li.StageStatus,
                                      AcceptanceStandard = li.AcceptanceStandard,
                                      ApplicableSpecification = li.ApplicableSpecification,
                                      InspectionExtent = li.InspectionExtent,
                                      Remarks = li.Remarks,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);

                }
                else
                {
                    var lst = db.SP_IPI_GetQualityIdLines(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      QualityId = li.QualityId,
                                      RevNo = li.RevNo,
                                      Status = li.Status,
                                      StageCode = li.StageCode,
                                      StageSequance = li.StageSequance,
                                      StageStatus = li.StageStatus,
                                      AcceptanceStandard = li.AcceptanceStandard,
                                      ApplicableSpecification = li.ApplicableSpecification,
                                      InspectionExtent = li.InspectionExtent,
                                      Remarks = li.Remarks,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ShowLogTimeline(int id, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            if (LineId > 0)
            {

            }
            else
            {
                QMS015_Log objQMS015log = db.QMS015_Log.Where(x => x.Id == id).FirstOrDefault();
                model.ApprovedBy = objQMS015log.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objQMS015log.ApprovedBy) : null;
                model.ApprovedOn = objQMS015log.ApprovedOn;
                model.SubmittedBy = objQMS015log.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objQMS015log.SubmittedBy) : null;
                model.SubmittedOn = objQMS015log.SubmittedOn;
                model.CreatedBy = objQMS015log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objQMS015log.CreatedBy) : null;
                model.CreatedOn = objQMS015log.CreatedOn;
                model.EditedBy = objQMS015log.EditedBy != null ? Manager.GetUserNameFromPsNo(objQMS015log.EditedBy) : null;
                model.EditedOn = objQMS015log.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress2.cshtml", model);
        }




        [HttpPost]
        public ActionResult GetNextPrevQualityProject(string qualityProject, string option)
        {

            string username = objClsLoginInfo.UserName;
            var lstATHLocation = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.Location).Distinct().ToList();
            var lstCOMLocation = db.COM003.Where(i => i.t_psno.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_loca).Distinct().ToList();

            var lstCOMBU = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.BU).Distinct().ToList();

            lstCOMLocation = lstATHLocation.Union(lstCOMLocation).ToList();

            List<Projects> lstQualityProject = new List<Projects>();
            lstQualityProject = db.QMS010.Where(i => lstCOMLocation.Contains(i.Location)
                                                    && lstCOMBU.Contains(i.BU)
                                                    ).Select(i => new Projects { Value = i.QualityProject, projectCode = i.QualityProject }).Distinct().ToList();

            //List<Projects> lstQualityProject = new List<Projects>();
            //lstQualityProject = db.QMS020.Select(i => new Projects { Value = i.QualityProject, projectCode = i.QualityProject, Text = i.Location }).Distinct().OrderBy(x => x.Value).ThenBy(x => x.Text).ToList();

            ResponceMsgWithHeaderID objresponse = new ResponceMsgWithHeaderID();
            List<Projects> lstprojects = lstQualityProject;
            int projindexno = 0;
            int minproj = 0;
            int maxproj = 0;

            try
            {
                if (!string.IsNullOrEmpty(qualityProject))
                {
                    projindexno = lstprojects.Select((item, index) => new { index, item }).Where(x => x.item.projectCode.ToLower() == qualityProject.ToLower()).Select(x => x.index).FirstOrDefault();
                    minproj = lstprojects.Select((item, index) => new { index, item }).Min(x => x.index);
                    maxproj = lstprojects.Select((item, index) => new { index, item }).Max(x => x.index);

                    switch (option)
                    {
                        case "PrevProject":
                            ViewBag.Title = "PrevProject";
                            projindexno = (projindexno - 1);
                            qualityProject = lstprojects.Select((item, index) => new { index, item }).Where(x => x.index == projindexno).FirstOrDefault().item.projectCode;

                            break;
                        case "NextProject":
                            ViewBag.Title = "NextProject";
                            projindexno = (projindexno + 1);
                            qualityProject = lstprojects.Select((item, index) => new { index, item }).Where(x => x.index == projindexno).FirstOrDefault().item.projectCode;

                            break;
                        default:
                            ViewBag.Title = "";
                            break;
                    }

                    objresponse.Project = qualityProject;
                    objresponse.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objresponse.Key = false;
                objresponse.Value = "Error Occures Please try again";
            }

            return Json(objresponse, JsonRequestBehavior.AllowGet);

        }







        public class ResponceMsgWithHeaderID : clsHelper.ResponseMsg
        {
            public string Status;
            public int HeaderId;
            public int Sequence;
            public string Stage;
            public string Project;
            public string BU;
            public string Location;
            public string ProjectCode;
            public string BUCode;
            public string LocationCode;
            public int TPHeaderId;
            public int SWPHeaderId;
        }


    }
}