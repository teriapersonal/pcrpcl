﻿using IEMQS.Areas.IPI.Models;
using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace IEMQS.Areas.IPI.Controllers
{
    public class PartlistOfferInspectionController : clsBase
    {
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult Index()
        {
            ViewBag.Tab = null;
            return View();
        }

        public ActionResult ShowTimelineOfferInspectionPartial(int headerId)
        {
            TimelineViewModel model = new TimelineViewModel();
            QMS045 objQMS045 = db.QMS045.FirstOrDefault(x => x.HeaderId == headerId);
            model.CreatedBy = Manager.GetPsidandDescription(objQMS045.CreatedBy);
            model.CreatedOn = objQMS045.CreatedOn;
            model.EditedBy = Manager.GetPsidandDescription(objQMS045.EditedBy);
            model.EditedOn = objQMS045.EditedOn;
            return PartialView("_ShowTimelineOfferInspectionPartial", model);
        }
        public ActionResult ShowTimelineNonNDEInspectionPartial(int requestId)
        {
            TimelineViewModel model = new TimelineViewModel();
            QMS055 objQMS055 = db.QMS055.FirstOrDefault(x => x.RequestId == requestId);
            model.OfferedBy = Manager.GetPsidandDescription(objQMS055.OfferedBy);
            model.OfferedOn = objQMS055.OfferedOn;
            model.InspectedBy = Manager.GetPsidandDescription(objQMS055.InspectedBy);
            model.InspectedOn = objQMS055.InspectedOn;
            model.OfferedtoCustomerBy = Manager.GetPsidandDescription(objQMS055.OfferedtoCustomerBy);
            model.OfferedtoCustomerOn = objQMS055.OfferedtoCustomerOn;
            model.TPIResultBy = Manager.GetPsidandDescription(objQMS055.EditedBy);
            model.TPIResultOn = objQMS055.TPIAgency1ResultOn;
            return PartialView("_ShowTimelineNonNDEInspectionPartial", model);
        }
        [HttpPost]
        public ActionResult LoadPartlistOfferDataPartial(string status, string qualityProject, string partNumber)
        {
            ViewBag.QualityProject = qualityProject;
            ViewBag.PartNumber = partNumber;
            if (status.ToLower() == "reoffer")
            {
                ViewBag.Status = "Pending";
                return PartialView("_LoadPartlistReOfferDataPartial");
            }
            else if (status.ToLower() == "returnnderequest")
            {
                ViewBag.Status = "ReturnNDERequest";
                return PartialView("_GetShopRequestReturnDataPartial");
            }
            else
            {
                ViewBag.Status = status;
                return PartialView("_LoadPartlistOfferDataPartial");
            }

        }

        [HttpPost]
        public JsonResult LoadPartlistOfferData(JQueryDataTableParamModel param, string qualityProject, string partNumber)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string _urlFrom = "all";
                string strWhere = string.Empty;

                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and InspectionStatus in('" + clsImplementationEnum.PartlistOfferInspectionStatus.READY_TO_OFFER.GetStringValue() + "','" + clsImplementationEnum.PartlistOfferInspectionStatus.PARTIALLY_OFFERED.GetStringValue() + "','" + clsImplementationEnum.PartlistOfferInspectionStatus.REWORK.GetStringValue() + "','" + clsImplementationEnum.PartlistOfferInspectionStatus.RETURNED.GetStringValue() + "')";
                    _urlFrom = "offer";
                }
                else
                {
                    strWhere += "1=1";
                }

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                string SpecialStageCode = Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.SpecialStageCodeForRTO.GetStringValue());
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms045.BU", "qms045.Location");
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                //"qms045.Project+' - '+com1.t_dsca",
                                                "QualityProject",
                                                "ProductionDrawingNo",
                                                "InspectionStatus",
                                                "[L&TInspectionResult]",
                                                //"Remarks",
                                                //"ltrim(rtrim(qms045.BU))+' - '+bcom2.t_desc",
                                                //"ltrim(rtrim(qms045.Location))+' - '+lcom2.t_desc",
                                                "PartNo",
                                                "ParentPartNo",
                                                "ChildPartNo",
                                                "dbo.GET_IPI_STAGECODE_DESCRIPTION(qms045.StageCode,ltrim(rtrim(qms045.BU)),ltrim(rtrim(qms045.Location)))",
                                                "StageSequence",
                                                //"IterationNo",
                                                "Quantity",
                                                "BalanceQuantity",
                                                "qms045.CreatedBy +' - '+com003c.t_name"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                if (!string.IsNullOrEmpty(qualityProject) && !string.IsNullOrEmpty(partNumber))
                {
                    strWhere += (partNumber.Equals("ALL") ? " AND QualityProject='" + qualityProject + "' " : " AND QualityProject='" + qualityProject + "' AND PartNo='" + partNumber + "' ");
                }
                var lstResult = db.SP_FETCH_PARTLISTOFFERINSPECTION_HEADERS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.QualityProject),
                               Convert.ToString(uc.PartNo) + (param.CTQCompileStatus.ToUpper() == "ALL" && IsPendingForInspection(uc.QualityProject, uc.BU.Split('-')[0], uc.Location.Split('-')[0], uc.PartNo,uc.ParentPartNo) ? "<a  href=\"javascript: void(0);\" Title=\"View Pending Inspection Data\" onclick=\"GetIPIPendingInspectionDataForPart('"+ uc.Project.Split('-')[0] +"','"+ uc.QualityProject +"','"+ uc.BU.Split('-')[0] +"','"+ uc.Location.Split('-')[0] +"','"+ uc.PartNo + "')\"><i class=\"fa fa-exclamation-circle\" style=\"margin-left:5px; color: red; font-size: 20px;\"></i></a>" : ""),
                               Convert.ToString(uc.ChildPartNo),
                               Convert.ToString(uc.Quantity),
                               Convert.ToString(uc.BalanceQuantity),
                               (Convert.ToString(uc.StageCode) ==SpecialStageCode) ?"<a title=\"View \" class=''  onclick=\"GetIPIPendingInspectionDataForSpecialStage('"+uc.Project.Split('-')[0]+"','"+uc.QualityProject+"','"+uc.BU.Split('-')[0]+"','"+uc.Location.Split('-')[0]+"','"+uc.ChildPartNo+"')\" >"+ Convert.ToString(uc.StageCodeDesc)+"</a>":  Convert.ToString(uc.StageCodeDesc),
                               Convert.ToString(uc.StageSequence),
                               Convert.ToString(uc.IterationNo),
                               Convert.ToString(uc.InspectionStatus),
                               Convert.ToString(uc.LNTInspectorResult),
                               "<center style=\"white-space:nowrap;\"><a title=\"View Details\" class='' href='" +WebsiteURL + "/IPI/PartlistOfferInspection/ViewDetails?HeaderID="+Convert.ToInt32(uc.HeaderId)+"&from="+ _urlFrom +"'><i style='margin-left:5px;' class='fa fa-eye'></i></a>"+(uc.InspectionStatus != clsImplementationEnum.PartlistOfferInspectionStatus.UNDER_INSPECTION.GetStringValue() && uc.BalanceQuantity > 0?"<a title=\"Request Offer\" class='' onclick=\"RequestOffer("+uc.HeaderId+")\" ><i style='margin-left:5px;cursor:pointer' class='fa fa-paper-plane'  title=\"Request Offer\"></i></a>":"<a title=\"Request Offer\" ><i style='margin-left:5px;cursor:pointer;opacity:0.3;' class='fa fa-paper-plane'  title=\"Request Offer\"></i></a>")+Manager.generateIPIGridButtons(uc.QualityProject,uc.Project,uc.BU,uc.Location,uc.PartNo,uc.ParentPartNo,uc.ChildPartNo,uc.StageCode,Convert.ToString(uc.StageSequence),"Part Details",(Convert.ToBoolean(uc.isNDEStage)?65:55),"","Part Details","Part Request Details")+"&nbsp;&nbsp;<a  href=\"javascript: void(0);\" Title=\"View Timeline\" onclick=\"ShowTimeline("+ uc.HeaderId +")\"><i style=\"margin - left:5px;\" class=\"fa fa-clock-o\"></i></a>"
                               + (Convert.ToString(uc.ProtocolType) == clsImplementationEnum.ProtocolType.Other_Files.GetStringValue() ? ( "&nbsp;&nbsp;<a onclick=\"OpenProtocolPopUp("+ uc.HeaderId +");\" Title=\"View Protocol Files\" ><i style=\"margin - left:5px;\" class=\"fa fa-file-text\"></i></a>"  ) : ((uc.ProtocolId.HasValue ? "&nbsp;&nbsp;<a onclick=\"OpenProtocol('" + WebsiteURL + "/PROTOCOL/"+uc.ProtocolType+"/Linkage/"+uc.ProtocolId + ((uc.InspectionStatus == null || string.Equals(uc.InspectionStatus,clsImplementationEnum.SeamListInspectionStatus.ReadyToOffer.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(uc.InspectionStatus,clsImplementationEnum.SeamListInspectionStatus.Rework.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(uc.InspectionStatus,clsImplementationEnum.SeamListInspectionStatus.Returned.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(uc.InspectionStatus,clsImplementationEnum.SeamListInspectionStatus.Rejected.GetStringValue(),StringComparison.OrdinalIgnoreCase)) ? "?m=1', '"+ uc.ProtocolType +"',"+ uc.ProtocolId +");" : "', '"+ uc.ProtocolType +"',"+ uc.ProtocolId +");" ) +"\" Title=\"View Protocol Details\" ><i style=\"margin - left:5px;\" class=\"fa fa-file-text\"></i></a>" : "&nbsp;&nbsp;<a Title=\"No Protocol Attached\" ><i style=\"opacity:0.3;\" class=\"fa fa-file-text\"></i></a>")) )
                               +"</center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public bool IsPendingForInspection(string qualityProject, string BU, string Location, string partNo, string parentpartno)
        {
            if (db.QMS045.Any(c => c.QualityProject == qualityProject && c.BU == BU && c.Location == Location && c.PartNo == partNo && c.ParentPartNo == parentpartno && c.InspectionStatus != null))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        #region Pending inspection for special stage
        [HttpPost]
        public ActionResult GetPendingInspectionDataForSpecialStage(string project, string qualityProject, string bu, string location, string partcode)
        {
            var lstResult = db.SP_IPI_GET_PENDING_INSPECTION_DATA(project, qualityProject, bu, location, partcode).ToList();
            List<ReturnedData> lstSeam = lstResult.Where(x => x.Type.ToLower() == "seam").Select(x => new ReturnedData { Description = x.Name + " - " + x.Description }).ToList();
            List<CategoryData> lstPart = lstResult.Where(x => x.Type.ToLower() == "part").Select(x => new CategoryData { Description = x.Name + " - " + x.Description }).ToList();
            ViewBag.Seam = lstSeam;
            ViewBag.Part = lstPart;

            return PartialView("GetPendingInspectionDataPartial");
        }
        #endregion

        #region Get Pending Inspection Data
        [HttpPost]
        public ActionResult GetPendingInspectionData(string project, string qualityProject, string bu, string location, string partno)
        {
            var lstResult = db.SP_IPI_GET_PENDING_INSPECTION_DATA_FOR_PART(project, qualityProject, bu, location, partno).ToList();
            List<ReturnedData> lstSeam = lstResult.Where(x => x.Type.ToLower() == "seam").Select(x => new ReturnedData { Description = x.Name + " - " + x.Description }).ToList();
            List<CategoryData> lstPart = lstResult.Where(x => x.Type.ToLower() == "part").Select(x => new CategoryData { Description = x.Name + " - " + x.Description }).ToList();
            ViewBag.Seam = lstSeam;
            ViewBag.Part = lstPart;
            ViewBag.InspectionType = "PART";

            return PartialView("~/Areas/IPI/Views/PartlistOfferInspection/GetPendingInspectionDataPartial.cshtml");
        }
        #endregion

        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult ViewDetails(int HeaderID = 0, string from = "")
        {
            ViewBag.fromURL = from;
            if (HeaderID > 0)
            {
                QMS045 objQMS045 = db.QMS045.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
                if (objQMS045 != null)
                {
                    return View(objQMS045);
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
            }

        }

        [HttpPost]
        public ActionResult LoadPartlistOfferInspectionFormPartial(QMS045 qms045)
        {

            string stageCodeDesc = db.Database.SqlQuery<string>("select  dbo.GET_IPI_STAGECODE_DESCRIPTION('" + qms045.StageCode + "','" + qms045.BU + "','" + qms045.Location + "')").FirstOrDefault();
            ViewBag.Stage = qms045.StageCode;
            var projectDetails = db.COM001.Where(x => x.t_cprj == qms045.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
            ViewBag.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;

            ViewBag.ProtocolTypeDescription = string.Empty;
            if (!string.IsNullOrWhiteSpace(Convert.ToString(qms045.ProtocolType)))
            {
                if (qms045.ProtocolId.HasValue)
                {
                    ViewBag.ProtocolTypeDescription = Manager.GetCategoryOnlyDescription(qms045.ProtocolType, qms045.BU, qms045.Location, "Protocol Type");
                    ViewBag.ProtocolURL = WebsiteURL + "/PROTOCOL/" + qms045.ProtocolType + "/Linkage/" + qms045.ProtocolId + ((string.Equals(qms045.InspectionStatus, clsImplementationEnum.SeamListInspectionStatus.ReadyToOffer.GetStringValue(), StringComparison.OrdinalIgnoreCase) || string.Equals(qms045.InspectionStatus, clsImplementationEnum.SeamListInspectionStatus.Rework.GetStringValue(), StringComparison.OrdinalIgnoreCase) || string.Equals(qms045.InspectionStatus, clsImplementationEnum.SeamListInspectionStatus.Returned.GetStringValue(), StringComparison.OrdinalIgnoreCase) || string.Equals(qms045.InspectionStatus, clsImplementationEnum.SeamListInspectionStatus.Rejected.GetStringValue(), StringComparison.OrdinalIgnoreCase)) ? "?m=1" : "");
                }
                else
                {
                    QMS036 objQMS036 = db.QMS036.FirstOrDefault(c => c.Project == qms045.Project && c.QualityProject == qms045.QualityProject && c.BU == qms045.BU && c.Location == qms045.Location && c.PartNo == qms045.PartNo && c.ParentPartNo == qms045.ParentPartNo && c.StageCode == qms045.StageCode && c.StageSequence == qms045.StageSequence);
                    if (objQMS036 != null)
                    {
                        ViewBag.ProtocolURL = "OpenAttachmentPopUp(" + objQMS036.LineId + ",false,'QMS036/" + objQMS036.LineId + "');";
                    }
                }
            }
            var BUDescription = (from a in db.COM002
                                 where a.t_dtyp == 2 && a.t_dimx == qms045.BU
                                 select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
            qms045.BU = Convert.ToString(BUDescription.BUDesc);
            qms045.StageCode = stageCodeDesc;
            var locDescription = (from a in db.COM002
                                  where a.t_dtyp == 1 && a.t_dimx == qms045.Location
                                  select new { Location = a.t_dimx + "-" + a.t_desc }).FirstOrDefault();
            qms045.Location = locDescription.Location;
            qms045.CreatedBy = Manager.GetPsidandDescription(qms045.CreatedBy);
            ViewBag.fromURL = Convert.ToString(qms045.UOM);  // used UOM for from URL

            ViewBag.PartDescription = Manager.GET_PART_DESCRIPTION_FROM_HBOM(qms045.Project, qms045.ChildPartNo);
            ViewBag.ParentPartDescription = Manager.GET_PART_DESCRIPTION_FROM_HBOM(qms045.Project, qms045.ParentPartNo);

            return PartialView("_LoadPartlistOfferInspectionFormPartial", qms045);
        }

        [HttpPost]
        public ActionResult GetPartlistOfferPartial(int headerID)
        {
            QMS045 objQMS045 = db.QMS045.Where(x => x.HeaderId == headerID).FirstOrDefault();
            QMS055 QMS055 = new QMS055();
            QMS055.RequestId = 0;
            QMS055.refHeaderId = objQMS045.HeaderId;
            List<GLB002> lstGLB002 = Manager.GetSubCatagories("Shop", objQMS045.BU, objQMS045.Location).ToList();
            List<BULocWiseCategoryModel> lstBULocWiseCategoryModel = lstGLB002.Select(x => new BULocWiseCategoryModel { CatDesc = (x.Code + "-" + x.Description), CatID = x.Code }).ToList();
            ViewBag.lstSubCatagories = new JavaScriptSerializer().Serialize(lstBULocWiseCategoryModel);
            //QMS055 objQMS055 = db.QMS055.Where(x=>x.HeaderId)
            var IsProtocolApplicable = db.QMS002.Where(x => x.Location == objQMS045.Location && x.BU == objQMS045.BU && x.StageCode == objQMS045.StageCode).Select(x => x.IsProtocolApplicable).FirstOrDefault();
            if (IsProtocolApplicable)
                ViewBag.ProtocolApplicable = "Yes";
            else
                ViewBag.ProtocolApplicable = "No";
            bool IsSTAGEDEPARTMENTNDE = db.QMS002.Where(x => x.Location == objQMS045.Location && x.BU == objQMS045.BU && x.StageCode == objQMS045.StageCode && x.StageDepartment == "NDE").Any();
            ViewBag.IsSTAGEDEPARTMENTNDE = IsSTAGEDEPARTMENTNDE;

            if (IsSTAGEDEPARTMENTNDE)
            {
                //NDE
                ViewBag.folderPath = "QMS065//" + objQMS045.HeaderId + "//" + objQMS045.IterationNo + "//" + 1;
                ViewBag.FCS_TableId = objQMS045.HeaderId;
            }
            else
            {
                //Non NDE
                ViewBag.folderPath = "QMS055//" + objQMS045.HeaderId + "//" + objQMS045.IterationNo + "//" + 1;
                ViewBag.FCS_TableId = objQMS045.HeaderId;
            }

            QMS002 objQMS002Doc = db.QMS002.Where(x => x.StageCode == objQMS045.StageCode && x.BU == objQMS045.BU && x.Location == objQMS045.Location).FirstOrDefault();
            if (objQMS002Doc != null)
            {
                if (objQMS002Doc.IsProtocolApplicable)
                {
                    ViewBag.ISDocRequired = "false";
                }
            }

            #region Fetch data from request (16514 :While offering stage, system should copy offer data from previous stage to new stage.)
            var objPreviousStageSeq = (from qms45 in db.QMS045
                                       where qms45.QualityProject == objQMS045.QualityProject && qms45.Project == objQMS045.Project && qms45.PartNo == objQMS045.PartNo && qms45.ParentPartNo == objQMS045.ParentPartNo
                                             && qms45.Location == objQMS045.Location && qms45.BU == objQMS045.BU
                                             && qms45.StageSequence <= objQMS045.StageSequence && qms45.HeaderId != objQMS045.HeaderId
                                       orderby qms45.StageSequence descending
                                       select qms45).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            string SurfaceCondition = string.Empty, ShopDesc = string.Empty;
            if (objPreviousStageSeq != null)
            {
                bool IsSTAGENDE = db.Database.SqlQuery<bool>("SELECT dbo.FUN_GET_IPI_ISSTAGEDEPARTMENTNDE('" + objPreviousStageSeq.HeaderId + "')").FirstOrDefault();
                if (IsSTAGENDE)
                {
                    QMS065 objQMS065 = db.QMS065
                                        .Where(x => x.HeaderId == objPreviousStageSeq.HeaderId)
                                        .OrderByDescending(x => x.OfferedOn)
                                        .FirstOrDefault();
                    if (objQMS065 != null)
                    {
                        ShopDesc = objNDEModels.GetCategory("Shop", objQMS065.Shop, objQMS065.BU, objQMS065.Location).CategoryDescription;
                        QMS055.Shop = objQMS065.Shop;
                        QMS055.ShopLocation = objQMS065.ShopLocation;
                        QMS055.ShopRemark = objQMS065.ShopRemark;
                    }
                }
                else
                {
                    QMS055 objQMS055 = db.QMS055
                                        .Where(x => x.refHeaderId == objPreviousStageSeq.HeaderId)
                                        .OrderByDescending(x => x.OfferedOn)
                                        .FirstOrDefault();
                    if (objQMS055 != null)
                    {
                        ShopDesc = objNDEModels.GetCategory("Shop", objQMS055.Shop, objQMS055.BU, objQMS055.Location).CategoryDescription;
                        QMS055.Shop = objQMS055.Shop;
                        QMS055.ShopLocation = objQMS055.ShopLocation;
                        QMS055.ShopRemark = objQMS055.ShopRemark;
                    }
                }
            }
            ViewBag.ShopDesc = ShopDesc;
            #endregion


            return PartialView("_GetPartlistOfferPartial", QMS055);
        }

        [HttpPost]
        public JsonResult checkQuantity(int HeaderId, int qty)
        {
            bool flag = true;
            QMS045 objQMS045 = db.QMS045.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            if (objQMS045 != null)
            {
                if (qty > objQMS045.BalanceQuantity)
                {
                    flag = false;
                }
                else if (qty <= 0)
                {
                    flag = false;
                }
                else
                {
                    flag = true;
                }
            }
            return Json(flag);
        }
        // ALSO USED in Mobile Application
        [HttpPost]
        public ActionResult RequestOfferedQuantity(int OfferedQuantity, string ShopLocation, string ShopRemark, string Shop, int headerID, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                // Check QUANTITY available or not when Request from MOBILE APPLICATION
                if (APIRequestFrom == "Mobile")
                {
                    bool flag = true;
                    QMS045 tempObjQMS045 = db.QMS045.Where(x => x.HeaderId == headerID).FirstOrDefault();
                    if (tempObjQMS045 != null)
                    {
                        if (OfferedQuantity > tempObjQMS045.BalanceQuantity)
                        {
                            flag = false;
                        }
                        else if (OfferedQuantity <= 0)
                        {
                            flag = false;
                        }
                        else
                        {
                            flag = true;
                        }
                    }

                    if (flag == false)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Entered quantity not available to offer.";
                        return Json(objResponseMsg);
                    }
                }


                QMS045 objQMS045 = db.QMS045.Where(x => x.HeaderId == headerID).FirstOrDefault();

                objResponseMsg = Manager.IsApplicableForOffer(objQMS045.HeaderId, clsImplementationEnum.InspectionFor.PART.GetStringValue(), false);
                if (!objResponseMsg.Key)
                {
                    return Json(objResponseMsg);
                }

                QMS036 objQMS036 = db.QMS036.Where(x =>
                                                        x.QualityProject == objQMS045.QualityProject &&
                                                        x.Project == objQMS045.Project &&
                                                        x.BU == objQMS045.BU &&
                                                        x.Location == objQMS045.Location &&
                                                        x.PartNo == objQMS045.PartNo &&
                                                        x.ParentPartNo == objQMS045.ParentPartNo &&
                                                        x.ChildPartNo == objQMS045.ChildPartNo &&
                                                        x.StageCode == objQMS045.StageCode).FirstOrDefault();

                if (objQMS045 != null)
                {

                    QMS002 objQMS002 = db.QMS002.Where(x => x.StageDepartment.ToUpper() == "NDE" && x.StageCode == objQMS045.StageCode && x.BU == objQMS045.BU && x.Location == objQMS045.Location).FirstOrDefault();
                    QMS002 objQMS002Doc = db.QMS002.Where(x => x.StageCode == objQMS045.StageCode && x.BU == objQMS045.BU && x.Location == objQMS045.Location).FirstOrDefault();

                    if (objQMS002 != null)
                    {
                        #region NDE Entry

                        long RequestNo = 0;
                        int ReqSeqNo = 0;
                        int Iteration = Convert.ToInt32(objQMS045.IterationNo);
                        //RequestNo = db.Database.SqlQuery<long>("SELECT dbo.FN_GENERATE_MAX_IPI_SEAMNDEREQUESTNO('QMS065')").FirstOrDefault();

                        ObjectParameter outParm = new ObjectParameter("result", typeof(long));
                        var SPResult = db.SP_IPI_GET_NEXT_REQUEST_NO(objQMS045.BU, objQMS045.Location, outParm);
                        RequestNo = (long)outParm.Value;

                        ReqSeqNo = 1;
                        QMS065 objQMS065 = db.QMS065.Add(new QMS065
                        {
                            HeaderId = objQMS045.HeaderId,
                            QualityProject = objQMS045.QualityProject,
                            Project = objQMS045.Project,
                            BU = objQMS045.BU,
                            Location = objQMS045.Location,
                            PartAssemblyFindNumber = objQMS045.PartNo,
                            PartAssemblyChildItemCode = objQMS045.ChildPartNo,
                            ChildItemCodeDesc = db.Database.SqlQuery<string>("select Description from dbo.FN_GET_HBOM_DATA('" + objQMS045.Project + "') where Part = '" + objQMS045.ChildPartNo + "' and FindNo = '" + objQMS045.PartNo + "'").FirstOrDefault(),
                            //ChildItemCodeDesc = db.Database.SqlQuery<string>("select Description from VW_IPI_GETHBOMLIST where Part = '" + objQMS045.ChildPartNo + "' and FindNo = '" + objQMS045.PartNo + "'").FirstOrDefault(),
                            StageCode = objQMS045.StageCode,
                            StageSequence = objQMS045.StageSequence,
                            IterationNo = objQMS045.IterationNo,
                            RequestNo = RequestNo,
                            RequestNoSequence = ReqSeqNo,
                            RequestStatus = clsImplementationEnum.NDESeamRequestStatus.NotAttended.GetStringValue(),
                            ManufacturingCode = db.QMS010.Where(x => x.QualityProject == objQMS045.QualityProject).Select(x => x.ManufacturingCode).FirstOrDefault(),
                            TPIAgency1 = objQMS036 != null ? objQMS036.FirstTPIAgency : null,
                            TPIAgency1Intervention = objQMS036 != null ? objQMS036.FirstTPIIntervention : null,
                            TPIAgency2 = objQMS036 != null ? objQMS036.SecondTPIAgency : null,
                            TPIAgency2Intervention = objQMS036 != null ? objQMS036.SecondTPIIntervention : null,
                            TPIAgency3 = objQMS036 != null ? objQMS036.ThirdTPIAgency : null,
                            TPIAgency3Intervention = objQMS036 != null ? objQMS036.ThirdTPIIntervention : null,
                            TPIAgency4 = objQMS036 != null ? objQMS036.FourthTPIAgency : null,
                            TPIAgency4Intervention = objQMS036 != null ? objQMS036.FourthTPIIntervention : null,
                            TPIAgency5 = objQMS036 != null ? objQMS036.FifthTPIAgency : null,
                            TPIAgency5Intervention = objQMS036 != null ? objQMS036.FifthTPIIntervention : null,
                            TPIAgency6 = objQMS036 != null ? objQMS036.SixthTPIAgency : null,
                            TPIAgency6Intervention = objQMS036 != null ? objQMS036.SixthTPIIntervention : null,
                            Shop = Shop,
                            ShopLocation = ShopLocation,
                            ShopRemark = ShopRemark,
                            OfferedQuantity = OfferedQuantity,
                            ProductionDrawingNo = objQMS045.ProductionDrawingNo,
                            CreatedBy = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName,
                            CreatedOn = DateTime.Now,
                            OfferedBy = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName,
                            OfferedOn = DateTime.Now
                        });

                        objQMS045.BalanceQuantity = objQMS045.BalanceQuantity - OfferedQuantity;
                        objQMS045.EditedBy = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                        objQMS045.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        if (objQMS045.BalanceQuantity == 0)
                        {
                            objQMS045.InspectionStatus = clsImplementationEnum.PartlistOfferInspectionStatus.UNDER_INSPECTION.GetStringValue();
                        }
                        else
                        {
                            objQMS045.InspectionStatus = clsImplementationEnum.PartlistOfferInspectionStatus.PARTIALLY_OFFERED.GetStringValue();
                        }
                        db.SaveChanges();

                        #region Send Notification

                        if (Manager.IsNDERequestAutoGenerate(objQMS065.Project, objQMS065.BU, objQMS065.Location))
                        {
                            (new PartlistNDEInspectionController()).GenerateRequest(objQMS065, Manager.GetSystemUserPSNo());
                        }
                        else
                        {
                            (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), objQMS045.Project, objQMS045.BU, objQMS045.Location, "Stage: " + objQMS045.StageCode + " of Part: " + objQMS045.PartNo + " & Project No: " + objQMS045.QualityProject + " has been offered for Inspection", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/PartlistNDEInspection/ViewOfferDetails?RequestId=" + objQMS065.RequestId, null, APIRequestFrom, UserName);
                        }


                        #endregion

                        #endregion
                    }
                    else
                    {
                        long RequestNo = 0;
                        int ReqSeqNo = 0;
                        int Iteration = Convert.ToInt32(objQMS045.IterationNo);

                        #region Non NDE

                        //RequestNo = db.Database.SqlQuery<long>("SELECT dbo.FN_GENERATE_MAX_IPI_SEAMNDEREQUESTNO('QMS055')").FirstOrDefault();

                        ObjectParameter outParm = new ObjectParameter("result", typeof(long));
                        var SPResult = db.SP_IPI_GET_NEXT_REQUEST_NO(objQMS045.BU, objQMS045.Location, outParm);
                        RequestNo = (long)outParm.Value;

                        ReqSeqNo = 1;
                        QMS055 objQMS055 = db.QMS055.Add(new QMS055
                        {
                            QualityProject = objQMS045.QualityProject,
                            Project = objQMS045.Project,
                            BU = objQMS045.BU,
                            Location = objQMS045.Location,
                            PartNo = objQMS045.PartNo,
                            ParentPartNo = objQMS045.ParentPartNo,
                            ChildPartNo = objQMS045.ChildPartNo,
                            StageCode = objQMS045.StageCode,
                            StageSequence = objQMS045.StageSequence,
                            IterationNo = Iteration,
                            RequestNo = RequestNo,
                            RequestNoSequence = ReqSeqNo,
                            OfferedQuantity = OfferedQuantity,
                            ShopLocation = ShopLocation,
                            ShopRemark = ShopRemark,
                            Shop = Shop,
                            OfferedBy = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName,
                            OfferedOn = DateTime.Now,
                            CreatedBy = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName,
                            CreatedOn = DateTime.Now,
                            refHeaderId = objQMS045.HeaderId,
                            TPIAgency1 = objQMS036 != null ? objQMS036.FirstTPIAgency : null,
                            TPIAgency1Intervention = objQMS036 != null ? objQMS036.FirstTPIIntervention : null,
                            TPIAgency2 = objQMS036 != null ? objQMS036.SecondTPIAgency : null,
                            TPIAgency2Intervention = objQMS036 != null ? objQMS036.SecondTPIIntervention : null,
                            TPIAgency3 = objQMS036 != null ? objQMS036.ThirdTPIAgency : null,
                            TPIAgency3Intervention = objQMS036 != null ? objQMS036.ThirdTPIIntervention : null,
                            TPIAgency4 = objQMS036 != null ? objQMS036.FourthTPIAgency : null,
                            TPIAgency4Intervention = objQMS036 != null ? objQMS036.FourthTPIIntervention : null,
                            TPIAgency5 = objQMS036 != null ? objQMS036.FifthTPIAgency : null,
                            TPIAgency5Intervention = objQMS036 != null ? objQMS036.FifthTPIIntervention : null,
                            TPIAgency6 = objQMS036 != null ? objQMS036.SixthTPIAgency : null,
                            TPIAgency6Intervention = objQMS036 != null ? objQMS036.SixthTPIIntervention : null,
                            ProtocolType = objQMS045.ProtocolType,
                            ProtocolId = objQMS045.ProtocolId,
                        });

                        objQMS045.BalanceQuantity = objQMS045.BalanceQuantity - OfferedQuantity;
                        objQMS045.EditedBy = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                        objQMS045.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        if (objQMS045.BalanceQuantity == 0)
                        {
                            objQMS045.InspectionStatus = clsImplementationEnum.PartlistOfferInspectionStatus.UNDER_INSPECTION.GetStringValue();
                        }
                        else
                        {
                            objQMS045.InspectionStatus = clsImplementationEnum.PartlistOfferInspectionStatus.PARTIALLY_OFFERED.GetStringValue();
                        }
                        db.SaveChanges();

                        if (objQMS055.ProtocolId.HasValue && !string.IsNullOrWhiteSpace(objQMS055.ProtocolType))
                        {
                            string PRLTableName = Manager.GetLinkedProtocolTableName(objQMS055.ProtocolType);
                            if (!string.IsNullOrWhiteSpace(PRLTableName))
                            {

                                db.Database.ExecuteSqlCommand("UPDATE " + PRLTableName + " SET RequestNo=" + RequestNo + ",OfferDate=GETDATE() WHERE HeaderId = " + objQMS055.ProtocolId);
                            }
                        }


                        #region Send Notification
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), objQMS045.Project, objQMS045.BU, objQMS045.Location, "Stage: " + objQMS045.StageCode + " of Part: " + objQMS045.PartNo + " & Project No: " + objQMS045.QualityProject + " has been offered for Inspection", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/PartlistOfferInspection/ViewOfferedDetails?RequestId=" + objQMS055.RequestId, null, APIRequestFrom, UserName);
                        #endregion

                        #endregion
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Partlist offered Successfully";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Partlist not available.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }

            return Json(objResponseMsg);
        }

        //public QMS065 addPartlistDetails(QMS065 objQMS065,QMS045 objQMS045)
        //{
        //    QMS012 objQMS012 = db.QMS012.Where(x => x.AssemblyNo == objQMS045.PartNo).FirstOrDefault();
        //    if(objQMS012 != null)
        //    {
        //        string[] arrayPartPosition = objQMS012.Position.Split(',').ToArray();
        //        string[] arrayPartThickness = objQMS012.Thickness.Split(',').ToArray();
        //        string[] arrayPartMaterial = objQMS012.Material.Split(',').ToArray();

        //        objQMS065.Part1Position = arrayPartPosition.ElementAtOrDefault(0) != null?Convert.ToString(arrayPartPosition[0]):null;
        //        objQMS065.Part1thickness = arrayPartThickness.ElementAtOrDefault(0) != null ? Convert.ToString(arrayPartThickness[0]) : null;
        //        objQMS065.Part1Material = arrayPartMaterial.ElementAtOrDefault(0) != null ? Convert.ToString(arrayPartMaterial[0]) : null;

        //        objQMS065.Part2Position = arrayPartPosition.ElementAtOrDefault(1) != null ? Convert.ToString(arrayPartPosition[1]) : null;
        //        objQMS065.Part2thickness = arrayPartThickness.ElementAtOrDefault(1) != null ? Convert.ToString(arrayPartThickness[1]) : null;
        //        objQMS065.Part2Material = arrayPartMaterial.ElementAtOrDefault(1) != null ? Convert.ToString(arrayPartMaterial[1]) : null;

        //        objQMS065.Part3Position = arrayPartPosition.ElementAtOrDefault(2) != null ? Convert.ToString(arrayPartPosition[2]) : null;
        //        objQMS065.Part3thickness = arrayPartThickness.ElementAtOrDefault(2) != null ? Convert.ToString(arrayPartThickness[2]) : null;
        //        objQMS065.Part3Material = arrayPartMaterial.ElementAtOrDefault(2) != null ? Convert.ToString(arrayPartMaterial[2]) : null;
        //    }
        //    return objQMS065;
        //}

        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult RequestedOffers(string tab)
        {
            ViewBag.Tab = tab;
            return View();
        }

        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult ReOffersView()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadPartlistRequestedOfferDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_LoadPartlistRequestedOfferDataPartial");
        }

        [HttpPost]
        public ActionResult LoadPartlistReOfferDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_LoadPartlistReOfferDataPartial");
        }

        [HttpPost]
        public JsonResult LoadPartlistOfferRequestedData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string _urlFrom = "all";

                string strWhere = string.Empty;

                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and (TestResult is null or TestResult='Returned by TPI') and OfferedBy is not null and OfferedOn is not null and OfferedtoCustomerBy is null ";
                    _urlFrom = "attend";
                }
                else if (param.CTQCompileStatus.ToUpper() == "PENDING TPI")
                {
                    strWhere += "1=1 and (TestResult in('" + clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue() + "','" + clsImplementationEnum.PartlistOfferInspectionStatus.PARTIALLY_CLEARED.GetStringValue() + "')) and OfferedBy is not null and OfferedOn is not null and OfferedtoCustomerBy is null and 1 in (select TPIOnlineApproval from Qms010 qms10 where qms10.QualityProject = qms055.QualityProject) ";
                    _urlFrom = "offertpi";
                }
                else
                {
                    strWhere += "1=1 ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                //"qms055.Project+' - '+com1.t_dsca",
                                                "QualityProject",
                                                "TestResult",
                                                "Shop",
                                                "ShopLocation",
                                                "ShopRemark",
                                                //"ltrim(rtrim(qms055.BU))+' - '+bcom2.t_desc",
                                                //"ltrim(rtrim(qms055.Location))+' - '+lcom2.t_desc",
                                                "PartNo",
                                                //"ParentPartNo",
                                                //"ChildPartNo",
                                                "(dbo.GET_IPI_STAGECODE_DESCRIPTION(qms055.StageCode,ltrim(rtrim(qms055.BU)),ltrim(rtrim(qms055.Location))))",
                                                "StageSequence",
                                                "IterationNo",
                                                "RequestNo",
                                                "RequestNoSequence",
                                                "OfferedQuantity",
                                                //"ClearedQuantity",
                                                //"RejectedQuantity",
                                                "qms055.InspectedBy +' - '+com003i.t_name",
                                                "qms055.OfferedtoCustomerBy +' - '+com003oc.t_name",
                                                "Shop",
                                                "ShopLocation",
                                                "qms055.OfferedBy +' - '+com003o.t_name",
                                                "qms055.CreatedBy +' - '+com003c.t_name"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                string SpecialStageCode = Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.SpecialStageCodeForRTO.GetStringValue());
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms055.BU", "qms055.Location");
                var lstResult = db.SP_FETCH_PARTLISTTESTDETAILS_HEADERS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.QualityProject),
                               //Convert.ToString(uc.Project),
                               //Convert.ToString(uc.BU),
                               //Convert.ToString(uc.Location),
                               Convert.ToString(uc.PartNo),
                               Convert.ToString(uc.ChildPartNo),
                               Convert.ToString(uc.OfferedQuantity),
                               (Convert.ToString(uc.StageCode) ==SpecialStageCode) ?"<a title=\"View \" class=''  onclick=\"GetIPIPendingInspectionDataForSpecialStage('"+uc.Project.Split('-')[0]+"','"+uc.QualityProject+"','"+uc.BU.Split('-')[0]+"','"+uc.Location.Split('-')[0]+"','"+uc.ChildPartNo+"')\" >"+ Convert.ToString(uc.StageCodeDesc)+"</a>":  Convert.ToString(uc.StageCodeDesc),
                               Convert.ToString(uc.StageSequence),
                               Convert.ToString(uc.IterationNo),
                               Convert.ToString(uc.RequestNo),
                               Convert.ToString(uc.RequestNoSequence),
                               //Convert.ToString(uc.ParentPartNo),
                               //Convert.ToString(uc.ChildPartNo),
                               Convert.ToString(uc.TestResult),
                               Convert.ToString(uc.Shop),
                               Convert.ToString(uc.ShopLocation),
                               Convert.ToString(uc.ShopRemark),
                               //Convert.ToString(uc.CreatedBy),
                               //Convert.ToString(uc.CreatedOn),
                               Convert.ToString(uc.OfferedBy),
                               Convert.ToString(uc.OfferedOn),
                               Convert.ToString(uc.InspectedBy),
                               Convert.ToString(uc.InspectedOn),
                               Convert.ToString(uc.OfferedtoCustomerBy),
                               Convert.ToString(uc.OfferedtoCustomerOn),
                               "<center style=\"white-space: nowrap;\"><a title=\"View Details\" target=\"_blank\" class='' href='" + WebsiteURL + "/IPI/PartlistOfferInspection/ViewOfferedDetails?RequestId="+Convert.ToInt32(uc.RequestId)+"&from="+ _urlFrom +"'><i style='margin-left:5px;' class='fa fa-eye'></i></a>"+Manager.generateIPIGridButtons(uc.QualityProject,uc.Project,uc.BU,uc.Location,uc.PartNo,uc.ParentPartNo,uc.ChildPartNo,uc.StageCode,Convert.ToString(uc.StageSequence),"Part Details",55,"","Part Details","Part Request Details")+"&nbsp;&nbsp;<a  href=\"javascript: void(0);\" Title=\"View Timeline\" onclick=\"ShowTimeline("+ uc.RequestId +")\"><i style=\"margin - left:5px;\" class=\"fa fa-clock-o\"></i></a>"
                               + (Convert.ToString(uc.ProtocolType) == clsImplementationEnum.ProtocolType.Other_Files.GetStringValue() ? ( "&nbsp;&nbsp;<a onclick=\"OpenProtocolPopUp("+ uc.refHeaderId +");\" Title=\"View Protocol Files\" ><i style=\"margin - left:5px;\" class=\"fa fa-file-text\"></i></a>"  ) : (uc.ProtocolId.HasValue ? "&nbsp;&nbsp;<a target=\"_blank\"  href=\"" + WebsiteURL + "/PROTOCOL/"+uc.ProtocolType+"/Linkage/"+uc.ProtocolId + (((uc.TestResult==null || uc.TestResult=="Returned by TPI") && uc.OfferedBy != null && _urlFrom != "offertpi") ? "?m=1" : "") + "\" Title=\"View Protocol Details\" ><i style=\"margin - left:5px;\" class=\"fa fa-file-text\"></i></a>" : "&nbsp;&nbsp;<a Title=\"No Protocol Attached\" ><i style=\"opacity:0.3;\" class=\"fa fa-file-text\"></i></a>") )
                               +"</center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        // ONLY USED IN MOBILE APPLICATION
        [HttpPost]
        public JsonResult AutoCompletePositionsNoListByQualityProject(string QualityProject, string PositionNo)
        {
            try
            {
                IEnumerable<string> lstPartNo = (from qms in db.QMS055
                                                 join qms001 in db.QMS001
                                                 on new { qms.Project, qms.BU, qms.Location }
                                                 equals new { qms001.Project, qms001.BU, qms001.Location }
                                                 where qms.QualityProject == QualityProject && qms.PartNo.Contains(PositionNo)
                                                 select qms.PartNo).Distinct().ToList();

                return Json(new
                {
                    Key = true,
                    Value = "Success",
                    data = lstPartNo
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Key = false,
                    Value = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        // ONLY USED IN MOBILE APPLICATION
        [HttpPost]
        public JsonResult AttendOfferedPartlistMobile(JQueryDataTableParamModel param, string UserName)
        {
            try
            {
                int? StartIndex = (param.iDisplayStart * param.iDisplayLength) + 1;
                int? EndIndex = (param.iDisplayStart * param.iDisplayLength) + param.iDisplayLength;

                string strWhere = string.Empty;

                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and (TestResult is null or TestResult='Returned by TPI') and OfferedBy is not null and OfferedOn is not null and OfferedtoCustomerBy is null ";
                }
                else if (param.CTQCompileStatus.ToUpper() == "PENDING TPI")
                {
                    strWhere += "1=1 and (TestResult in('" + clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue() + "','" + clsImplementationEnum.PartlistOfferInspectionStatus.PARTIALLY_CLEARED.GetStringValue() + "')) and OfferedBy is not null and OfferedOn is not null and OfferedtoCustomerBy is null and 1 in (select TPIOnlineApproval from Qms010 qms10 where qms10.QualityProject = qms055.QualityProject) ";
                }
                else
                {
                    strWhere += "1=1 ";
                }

                string sortColumnName = "QualityProject";
                string sortDirection = "asc"; ;
                string strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";

                string SpecialStageCode = Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.SpecialStageCodeForRTO.GetStringValue());
                strWhere += Manager.MakeDefaultWhere(UserName, "qms055.BU", "qms055.Location");
                var lstResult = db.SP_FETCH_PARTLISTTESTDETAILS_HEADERS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                int? totalRecords = lstResult.Select(i => i.TotalCount).FirstOrDefault();

                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    TotalCount = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    data_list = lstResult
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                    TotalCount = 0,
                    data_list = new List<string>()
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ReturnRequestDetails(int id, string from)
        {
            QMS065 qms065 = db.QMS065.FirstOrDefault(x => x.RequestId == id);

            string stageCodeDesc = db.Database.SqlQuery<string>("select  dbo.GET_IPI_STAGECODE_DESCRIPTION('" + qms065.StageCode + "','" + qms065.BU + "','" + qms065.Location + "')").FirstOrDefault();
            qms065.StageCode = stageCodeDesc;
            ViewBag.IsLTFPS = Manager.IsLTFPSProject(qms065.Project, qms065.BU, qms065.Location);

            List<GLB002> lstGLB002 = Manager.GetSubCatagories("Shop", qms065.BU, qms065.Location).ToList();
            List<BULocWiseCategoryModel> lstBULocWiseCategoryModel = lstGLB002.Select(x => new BULocWiseCategoryModel { CatDesc = (x.Code + " - " + x.Description), CatID = x.Code }).ToList();
            ViewBag.lstSubCatagories = new JavaScriptSerializer().Serialize(lstBULocWiseCategoryModel);
            ViewBag.Shop = GetCategoryDescriptionFromList(lstGLB002, qms065.Shop);

            var projectDetails = db.COM001.Where(x => x.t_cprj == qms065.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
            ViewBag.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
            ViewBag.BU = qms065.BU;
            ViewBag.Location = qms065.Location;
            qms065.OfferedBy = Manager.GetPsidandDescription(qms065.OfferedBy);
            qms065.QCReturnedBy = Manager.GetPsidandDescription(qms065.QCReturnedBy);
            List<GLB002> lstCategory = Manager.GetSubCatagories("TPI Agency", qms065.BU, qms065.Location, null);
            List<GLB002> lstCategoryTPI = Manager.GetSubCatagories("TPI Intervention", qms065.BU, qms065.Location, null);
            qms065.TPIAgency1 = GetCategoryDescriptionFromList(lstCategory, qms065.TPIAgency1);
            qms065.TPIAgency2 = GetCategoryDescriptionFromList(lstCategory, qms065.TPIAgency2);
            qms065.TPIAgency3 = GetCategoryDescriptionFromList(lstCategory, qms065.TPIAgency3);
            qms065.TPIAgency4 = GetCategoryDescriptionFromList(lstCategory, qms065.TPIAgency4);
            qms065.TPIAgency5 = GetCategoryDescriptionFromList(lstCategory, qms065.TPIAgency5);
            qms065.TPIAgency6 = GetCategoryDescriptionFromList(lstCategory, qms065.TPIAgency6);
            qms065.TPIAgency1Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms065.TPIAgency1Intervention);
            qms065.TPIAgency2Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms065.TPIAgency2Intervention);
            qms065.TPIAgency3Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms065.TPIAgency3Intervention);
            qms065.TPIAgency4Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms065.TPIAgency4Intervention);
            qms065.TPIAgency5Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms065.TPIAgency5Intervention);
            qms065.TPIAgency6Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms065.TPIAgency6Intervention);
            var BUDescription = (from a in db.COM002
                                 where a.t_dtyp == 2 && a.t_dimx == qms065.BU
                                 select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
            qms065.BU = Convert.ToString(BUDescription.BUDesc);

            var locDescription = (from a in db.COM002
                                  where a.t_dtyp == 1 && a.t_dimx == qms065.Location
                                  select new { Location = a.t_dimx + "-" + a.t_desc }).FirstOrDefault();
            qms065.Location = locDescription.Location;

            ViewBag.IsQCReturned = (qms065.QCIsReturned == true ? "Yes" : "No");
            ViewBag.fromURL = from;
            return View(qms065);
        }

        [HttpPost]
        public ActionResult ReOfferNDERequest(int RequestId, string Shop, string ShopLocation, string ShopRemark)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (!(new MaintainSeamListOfferInspectionController()).IsApplicableForNDEReOfferReturnedRequest(RequestId, clsImplementationEnum.InspectionFor.PART.GetStringValue()))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "This request has been already offered";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                QMS065 objQMS065 = db.QMS065.Where(x => x.RequestId == RequestId).FirstOrDefault();

                objQMS065.QCIsReturned = null;
                objQMS065.Shop = Shop;
                objQMS065.ShopLocation = ShopLocation;
                objQMS065.ShopRemark = ShopRemark;
                objQMS065.RequestGeneratedBy = objClsLoginInfo.UserName;
                objQMS065.RequestGeneratedOn = DateTime.Now;
                objQMS065.ConfirmedBy = objClsLoginInfo.UserName;
                objQMS065.ConfirmedOn = DateTime.Now;
                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = "The stage " + objQMS065.StageCode + "  has been offered";

                #region Send Notification
                (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), objQMS065.Project, objQMS065.BU, objQMS065.Location, "Stage: " + objQMS065.StageCode + " of Part: " + objQMS065.PartAssemblyFindNumber + " & Project No: " + objQMS065.QualityProject + " has been offered for Inspection", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/PartlistNDEInspection/ViewOfferDetails?RequestId=" + objQMS065.RequestId);
                #endregion

                return Json(objResponseMsg);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error Occures Please try again";
                return Json(objResponseMsg);
            }

        }

        [HttpPost]
        public JsonResult LoadPartlistReOfferData(JQueryDataTableParamModel param, string qualityProject, string partNumber)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string _urlFrom = "all";
                string strWhere = string.Empty;

                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and (TestResult in('') or TestResult is null) and OfferedBy is null and OfferedOn is null ";
                    _urlFrom = "reoffer";
                }
                else
                {
                    strWhere += "1=1 ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                //"qms055.Project+' - '+com1.t_dsca",
                                                "QualityProject",
                                                "TestResult",
                                                "Shop",
                                                "ShopLocation",
                                                "ShopRemark",
                                                //"ltrim(rtrim(qms055.BU))+' - '+bcom2.t_desc",
                                                //"ltrim(rtrim(qms055.Location))+' - '+lcom2.t_desc",
                                                "PartNo",
                                                //"ParentPartNo",
                                                "ChildPartNo",
                                                "(dbo.GET_IPI_STAGECODE_DESCRIPTION(qms055.StageCode,ltrim(rtrim(qms055.BU)),ltrim(rtrim(qms055.Location))))",
                                                "StageSequence",
                                                "IterationNo",
                                                "RequestNo",
                                                "RequestNoSequence",
                                                "OfferedQuantity",
                                                //"ClearedQuantity",
                                                //"RejectedQuantity",
                                                "qms055.InspectedBy +' - '+com003i.t_name",
                                                "qms055.OfferedtoCustomerBy +' - '+com003oc.t_name",
                                                "Shop",
                                                "ShopLocation",
                                                "qms055.OfferedBy +' - '+com003o.t_name",
                                                "qms055.CreatedBy +' - '+com003c.t_name"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms055.BU", "qms055.Location");
                if (!string.IsNullOrEmpty(qualityProject) && !string.IsNullOrEmpty(partNumber))
                {
                    strWhere += (partNumber.Equals("ALL") ? " AND QualityProject='" + qualityProject + "' " : " AND QualityProject='" + qualityProject + "' AND PartNo='" + partNumber + "' ");
                }
                var lstResult = db.SP_FETCH_PARTLISTTESTDETAILS_HEADERS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                              Convert.ToString(uc.QualityProject),
                               Convert.ToString(uc.PartNo),
                               Convert.ToString(uc.ChildPartNo),
                               Convert.ToString(uc.OfferedQuantity),
                               Convert.ToString(uc.StageCodeDesc),
                               Convert.ToString(uc.StageSequence),
                               Convert.ToString(uc.IterationNo),
                               Convert.ToString(uc.RequestNo),
                               Convert.ToString(uc.RequestNoSequence),
                               Convert.ToString(uc.TestResult),
                               Convert.ToString(uc.Shop),
                               Convert.ToString(uc.ShopLocation),
                               Convert.ToString(uc.ShopRemark),
                               //Convert.ToString(uc.CreatedBy),
                               //Convert.ToString(uc.CreatedOn),
                               Convert.ToString(uc.OfferedBy),
                               Convert.ToString(uc.OfferedOn),
                               Convert.ToString(uc.InspectedBy),
                               Convert.ToString(uc.InspectedOn),
                               Convert.ToString(uc.OfferedtoCustomerBy),
                               Convert.ToString(uc.OfferedtoCustomerOn),
                               "<center style=\"white-space: nowrap;\"><a title=\"View Details\" class='' href='" + WebsiteURL + "/IPI/PartlistOfferInspection/ViewReOfferedDetails?RequestId="+Convert.ToInt32(uc.RequestId)+"&from="+ _urlFrom +"'><i style='margin-left:5px;' class='fa fa-eye'></i></a>"+Manager.generateIPIGridButtons(uc.QualityProject,uc.Project,uc.BU,uc.Location,uc.PartNo,uc.ParentPartNo,uc.ChildPartNo,uc.StageCode,Convert.ToString(uc.StageSequence),"Part Details",55,"","Part Details","Part Request Details")+"&nbsp;&nbsp;<a  href=\"javascript: void(0);\" Title=\"View Timeline\" onclick=\"ShowTimeline("+ uc.RequestId +")\"><i style=\"margin - left:5px;\" class=\"fa fa-clock-o\"></i></a>"
                               + (Convert.ToString(uc.ProtocolType) == clsImplementationEnum.ProtocolType.Other_Files.GetStringValue() ? ( "&nbsp;&nbsp;<a onclick=\"OpenProtocolPopUp("+ uc.refHeaderId +");\" Title=\"View Protocol Files\" ><i style=\"margin - left:5px;\" class=\"fa fa-file-text\"></i></a>"  ) : (uc.ProtocolId.HasValue ? "&nbsp;&nbsp;<a target=\"_blank\"  href=\"" + WebsiteURL + "/PROTOCOL/"+uc.ProtocolType+"/Linkage/"+uc.ProtocolId + "?m=1" + "\" Title=\"View Protocol Details\" ><i style=\"margin - left:5px;\" class=\"fa fa-file-text\"></i></a>" : "&nbsp;&nbsp;<a Title=\"No Protocol Attached\" ><i style=\"opacity:0.3;\" class=\"fa fa-file-text\"></i></a>") )
                               +"</center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }


        // ONLY USED IN Mobile Application
        [HttpPost]
        public JsonResult LoadPartlistReOfferDataMobile(JQueryDataTableParamModel param, string qualityProject, string partNumber, string UserName = "")
        {
            try
            {

                int? StartIndex = (param.iDisplayStart * param.iDisplayLength) + 1;
                int? EndIndex = (param.iDisplayStart * param.iDisplayLength) + param.iDisplayLength;

                string strWhere = string.Empty;

                if (param.CTQCompileStatus == "Pending")
                {
                    strWhere += "1=1 and (TestResult in('') or TestResult is null) and OfferedBy is null and OfferedOn is null ";
                }
                else
                {
                    strWhere += "1=1 ";
                }

                string strSortOrder = " Order By QualityProject asc ";

                strWhere += Manager.MakeDefaultWhere(UserName, "qms055.BU", "qms055.Location");
                if (!string.IsNullOrEmpty(qualityProject) && !string.IsNullOrEmpty(partNumber))
                {
                    strWhere += (partNumber.Equals("ALL") ? " AND QualityProject='" + qualityProject + "' " : " AND QualityProject='" + qualityProject + "' AND PartNo='" + partNumber + "' ");
                }
                var lstResult = db.SP_FETCH_PARTLISTTESTDETAILS_HEADERS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();


                int? totalRecords = lstResult.Select(i => i.TotalCount).FirstOrDefault();

                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    TotalCount = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    data_list = lstResult
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                    TotalCount = 0,
                    data_list = new List<string>()
                }, JsonRequestBehavior.AllowGet);
            }
        }


        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult ViewOfferedDetails(int RequestId = 0, string from = "")
        {
            ViewBag.fromURL = from;
            if (RequestId > 0)
            {
                QMS055 objQMS055 = db.QMS055.Where(x => x.RequestId == RequestId).FirstOrDefault();
                if (objQMS055 != null)
                {
                    var listQMS055 = JsonConvert.SerializeObject(objQMS055, Formatting.None,
                                                           new JsonSerializerSettings()
                                                           {
                                                               ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                                                           });
                    ViewBag.QMS055 = listQMS055;
                    return View(objQMS055);
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
            }
        }

        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult ViewReOfferedDetails(int RequestId = 0, string from = "")
        {
            ViewBag.fromURL = from;
            if (RequestId > 0)
            {
                QMS055 objQMS055 = db.QMS055.Where(x => x.RequestId == RequestId).FirstOrDefault();
                if (objQMS055 != null)
                {
                    var listQMS055 = JsonConvert.SerializeObject(objQMS055, Formatting.None,
                                                           new JsonSerializerSettings()
                                                           {
                                                               ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                                                           });
                    ViewBag.QMS055 = listQMS055;

                    return View(objQMS055);
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
            }
        }

        public string GetCategoryDescriptionFromList(List<GLB002> lst, string code)
        {
            if (!string.IsNullOrWhiteSpace(code))
                return lst.Where(x => x.Code == code).Select(s => s.Code + " - " + s.Description).FirstOrDefault();
            else
                return null;
        }

        //test result (qc attend)
        [HttpPost]
        public ActionResult LoadPartlistRequestedOfferFormPartial(string strModel, string fromURL)
        {
            QMS055 qms055 = JsonConvert.DeserializeObject<QMS055>(strModel);
            ViewBag.IsQ2Role = objClsLoginInfo.ListRoles.Any(x => x.ToString() == clsImplementationEnum.UserRoleName.QI2.GetStringValue() || x.ToString() == clsImplementationEnum.UserRoleName.QC2.GetStringValue());
            if (!string.IsNullOrWhiteSpace(Convert.ToString(qms055.ProtocolType)))
            {
                if (qms055.ProtocolId.HasValue)
                {
                    ViewBag.ProtocolURL = WebsiteURL + "/PROTOCOL/" + qms055.ProtocolType + "/Linkage/" + qms055.ProtocolId + (((qms055.TestResult == null || qms055.TestResult == "Returned by TPI") && qms055.OfferedBy != null) ? "?m=1" : "");
                    ViewBag.ProtocolTypeDescription = Manager.GetCategoryOnlyDescription(qms055.ProtocolType, qms055.BU, qms055.Location, "Protocol Type");
                }
                else
                {
                    QMS036 objQMS036 = db.QMS036.FirstOrDefault(c => c.Project == qms055.Project && c.QualityProject == qms055.QualityProject && c.BU == qms055.BU && c.Location == qms055.Location && c.PartNo == qms055.PartNo && c.ParentPartNo == qms055.ParentPartNo && c.StageCode == qms055.StageCode && c.StageSequence == qms055.StageSequence);
                    if (objQMS036 != null)
                    {
                        ViewBag.ProtocolURL = "OpenAttachmentPopUp(" + objQMS036.LineId + ",false,'QMS036/" + objQMS036.LineId + "');";
                        ViewBag.ProtocolTypeDescription = "";
                    }
                }
            }
            ViewBag.IsGroundSpot = db.QMS002.Where(x => x.StageCode == qms055.StageCode && x.BU == qms055.BU && x.Location == qms055.Location).Select(x => x.IsGroundSpot).FirstOrDefault();
            string strReturned = clsImplementationEnum.SeamListInspectionStatus.Returned.GetStringValue();
            string strRejected = clsImplementationEnum.SeamListInspectionStatus.Rejected.GetStringValue();
            ViewBag.GroundSpot = db.QMS055.Where(x => (x.RequestNo == qms055.RequestNo && x.RequestId < qms055.RequestId) && (x.TestResult == strReturned || x.TestResult == strRejected)).OrderByDescending(x => x.RequestId).Select(x => x.GroundSpot).FirstOrDefault();

            string stageCodeDesc = db.Database.SqlQuery<string>("select  dbo.GET_IPI_STAGECODE_DESCRIPTION('" + qms055.StageCode + "','" + qms055.BU + "','" + qms055.Location + "')").FirstOrDefault();
            qms055.StageCode = stageCodeDesc;
            ViewBag.IsLTFPS = Manager.IsLTFPSProject(qms055.Project, qms055.BU, qms055.Location);

            var projectDetails = db.COM001.Where(x => x.t_cprj == qms055.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
            ViewBag.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
            ViewBag.isTPIOnline = db.QMS010.Where(x => x.QualityProject == qms055.QualityProject).Select(x => x.TPIOnlineApproval).FirstOrDefault();
            // qms055.ClearedQuantity = qms055.OfferedQuantity;
            List<GLB002> lstCategory = Manager.GetSubCatagories("TPI Agency", qms055.BU, qms055.Location, null);
            List<GLB002> lstCategoryTPI = Manager.GetSubCatagories("TPI Intervention", qms055.BU, qms055.Location, null);
            qms055.TPIAgency1 = GetCategoryDescriptionFromList(lstCategory, qms055.TPIAgency1);
            qms055.TPIAgency2 = GetCategoryDescriptionFromList(lstCategory, qms055.TPIAgency2);
            qms055.TPIAgency3 = GetCategoryDescriptionFromList(lstCategory, qms055.TPIAgency3);
            qms055.TPIAgency4 = GetCategoryDescriptionFromList(lstCategory, qms055.TPIAgency4);
            qms055.TPIAgency5 = GetCategoryDescriptionFromList(lstCategory, qms055.TPIAgency5);
            qms055.TPIAgency6 = GetCategoryDescriptionFromList(lstCategory, qms055.TPIAgency6);
            qms055.TPIAgency1Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms055.TPIAgency1Intervention);
            qms055.TPIAgency2Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms055.TPIAgency2Intervention);
            qms055.TPIAgency3Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms055.TPIAgency3Intervention);
            qms055.TPIAgency4Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms055.TPIAgency4Intervention);
            qms055.TPIAgency5Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms055.TPIAgency5Intervention);
            qms055.TPIAgency6Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms055.TPIAgency6Intervention);
            var BUDescription = (from a in db.COM002
                                 where a.t_dtyp == 2 && a.t_dimx == qms055.BU
                                 select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
            qms055.BU = Convert.ToString(BUDescription.BUDesc);
            var locDescription = (from a in db.COM002
                                  where a.t_dtyp == 1 && a.t_dimx == qms055.Location
                                  select new { Location = a.t_dimx + "-" + a.t_desc }).FirstOrDefault();
            qms055.Location = locDescription.Location;
            ViewBag.remark = GetPreviousRequestRemark(qms055.RequestNo, qms055.RequestId);
            ViewBag.fromURL = fromURL;
            ViewBag.DocumentRequired = false;

            ViewBag.PartDescription = Manager.GET_PART_DESCRIPTION_FROM_HBOM(qms055.Project, qms055.ChildPartNo);
            ViewBag.ParentPartDescription = Manager.GET_PART_DESCRIPTION_FROM_HBOM(qms055.Project, qms055.ParentPartNo);

            if (ViewBag.IsLTFPS)
            {
                ViewBag.DocRevNumber = qms055.LTF002.DocRevNo;
                ViewBag.DocumentRequired = qms055.LTF002.DocumentRequired;
                int LTFPSHeaderId = Convert.ToInt32(qms055.LTFPSHeaderId);
                LTF002 objLTF002 = db.LTF002.Where(x => x.LineId == LTFPSHeaderId).FirstOrDefault();
                if (objLTF002 != null)
                {
                    if (!string.IsNullOrWhiteSpace(objLTF002.RefDocument) && (objLTF002.DocumentRequired) && !ViewBag.isTPIOnline)
                    {
                        ViewBag.IsRevAdd = true;
                    }
                    else
                    {
                        ViewBag.IsRevAdd = false;
                    }
                }
                else
                {
                    ViewBag.IsRevAdd = false;
                }
            }
            return PartialView("_LoadPartlistRequestedOfferFormPartial", qms055);
        }

        // ONLY USED IN MOBILE APPLICATION
        [HttpPost]
        public ActionResult AttendPartOfferedViewMoreMobile(int RequestId, string fromURL)
        {
            try
            {
                db.Configuration.ProxyCreationEnabled = false;
                QMS055 qms055 = db.QMS055.Where(x => x.RequestId == RequestId).FirstOrDefault();
                db.Entry(qms055).State = System.Data.Entity.EntityState.Detached;
                //QMS055 qms055 = JsonConvert.DeserializeObject<QMS055>(strModel);

                //if (!string.IsNullOrWhiteSpace(Convert.ToString(qms055.ProtocolType)))
                //{
                //    if (qms055.ProtocolId.HasValue)
                //    {
                //        ViewBag.ProtocolURL = WebsiteURL + "/PROTOCOL/" + qms055.ProtocolType + "/Linkage/" + qms055.ProtocolId + (((qms055.TestResult == null || qms055.TestResult == "Returned by TPI") && qms055.OfferedBy != null) ? "?m=1" : "");
                //        ViewBag.ProtocolTypeDescription = Manager.GetCategoryOnlyDescription(qms055.ProtocolType, qms055.BU, qms055.Location, "Protocol Type");
                //    }
                //    else
                //    {
                //        QMS036 objQMS036 = db.QMS036.FirstOrDefault(c => c.Project == qms055.Project && c.QualityProject == qms055.QualityProject && c.BU == qms055.BU && c.Location == qms055.Location && c.PartNo == qms055.PartNo && c.ParentPartNo == qms055.ParentPartNo && c.StageCode == qms055.StageCode && c.StageSequence == qms055.StageSequence);
                //        if (objQMS036 != null)
                //        {
                //            ViewBag.ProtocolURL = "OpenAttachmentPopUp(" + objQMS036.LineId + ",false,'QMS036/" + objQMS036.LineId + "');";
                //            ViewBag.ProtocolTypeDescription = "";
                //        }
                //    }
                //}
                var IsGroundSpot = db.QMS002.Where(x => x.StageCode == qms055.StageCode && x.BU == qms055.BU && x.Location == qms055.Location).Select(x => x.IsGroundSpot).FirstOrDefault();
                string strReturned = clsImplementationEnum.SeamListInspectionStatus.Returned.GetStringValue();
                string strRejected = clsImplementationEnum.SeamListInspectionStatus.Rejected.GetStringValue();
                var GroundSpot = db.QMS055.Where(x => (x.RequestNo == qms055.RequestNo && x.RequestId < qms055.RequestId) && (x.TestResult == strReturned || x.TestResult == strRejected)).OrderByDescending(x => x.RequestId).Select(x => x.GroundSpot).FirstOrDefault();

                string stageCodeDesc = db.Database.SqlQuery<string>("select  dbo.GET_IPI_STAGECODE_DESCRIPTION('" + qms055.StageCode + "','" + qms055.BU + "','" + qms055.Location + "')").FirstOrDefault();
                qms055.StageCode = stageCodeDesc;
                var IsLTFPS = Manager.IsLTFPSProject(qms055.Project, qms055.BU, qms055.Location);

                var projectDetails = db.COM001.Where(x => x.t_cprj == qms055.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                var Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                var isTPIOnline = db.QMS010.Where(x => x.QualityProject == qms055.QualityProject).Select(x => x.TPIOnlineApproval).FirstOrDefault();
                // qms055.ClearedQuantity = qms055.OfferedQuantity;
                List<GLB002> lstCategory = Manager.GetSubCatagories("TPI Agency", qms055.BU, qms055.Location, null);
                List<GLB002> lstCategoryTPI = Manager.GetSubCatagories("TPI Intervention", qms055.BU, qms055.Location, null);
                qms055.TPIAgency1 = GetCategoryDescriptionFromList(lstCategory, qms055.TPIAgency1);
                qms055.TPIAgency2 = GetCategoryDescriptionFromList(lstCategory, qms055.TPIAgency2);
                qms055.TPIAgency3 = GetCategoryDescriptionFromList(lstCategory, qms055.TPIAgency3);
                qms055.TPIAgency4 = GetCategoryDescriptionFromList(lstCategory, qms055.TPIAgency4);
                qms055.TPIAgency5 = GetCategoryDescriptionFromList(lstCategory, qms055.TPIAgency5);
                qms055.TPIAgency6 = GetCategoryDescriptionFromList(lstCategory, qms055.TPIAgency6);
                qms055.TPIAgency1Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms055.TPIAgency1Intervention);
                qms055.TPIAgency2Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms055.TPIAgency2Intervention);
                qms055.TPIAgency3Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms055.TPIAgency3Intervention);
                qms055.TPIAgency4Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms055.TPIAgency4Intervention);
                qms055.TPIAgency5Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms055.TPIAgency5Intervention);
                qms055.TPIAgency6Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms055.TPIAgency6Intervention);
                var BUDescription = (from a in db.COM002
                                     where a.t_dtyp == 2 && a.t_dimx == qms055.BU
                                     select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
                qms055.BU = Convert.ToString(BUDescription.BUDesc);
                var locDescription = (from a in db.COM002
                                      where a.t_dtyp == 1 && a.t_dimx == qms055.Location
                                      select new { Location = a.t_dimx + "-" + a.t_desc }).FirstOrDefault();
                qms055.Location = locDescription.Location;
                var remark = GetPreviousRequestRemark(qms055.RequestNo, qms055.RequestId);

                var DocumentRequired = false;
                var DocRevNumber = "";
                var IsRevAdd = false;
                var PartDescription = Manager.GET_PART_DESCRIPTION_FROM_HBOM(qms055.Project, qms055.ChildPartNo);
                var ParentPartDescription = Manager.GET_PART_DESCRIPTION_FROM_HBOM(qms055.Project, qms055.ParentPartNo);

                if (IsLTFPS)
                {
                    int LTFPSHeaderId = Convert.ToInt32(qms055.LTFPSHeaderId);
                    LTF002 objLTF002 = db.LTF002.Where(x => x.LineId == LTFPSHeaderId).FirstOrDefault();
                    if (objLTF002 != null)
                    {
                        DocRevNumber = objLTF002.DocRevNo;
                        DocumentRequired = objLTF002.DocumentRequired;
                        if (!string.IsNullOrWhiteSpace(objLTF002.RefDocument) && (objLTF002.DocumentRequired) && !isTPIOnline)
                        {
                            IsRevAdd = true;
                        }
                        else
                        {
                            IsRevAdd = false;
                        }
                    }
                    else
                    {
                        IsRevAdd = false;
                    }
                }

                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    QMS055 = qms055,
                    IsLTFPS,
                    IsGroundSpot,
                    GroundSpot,
                    isTPIOnline,
                    DocRevNumber,
                    DocumentRequired,
                    remark,
                    IsRevAdd,
                    Project,
                    PartDescription,
                    ParentPartDescription
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult LoadPartlistReOfferFormPartial(string strModel, string fromURL)
        {
            QMS055 qms055 = JsonConvert.DeserializeObject<QMS055>(strModel);
            ViewBag.ProtocolTypeDescription = string.Empty;
            if (!string.IsNullOrWhiteSpace(Convert.ToString(qms055.ProtocolType)))
            {
                if (qms055.ProtocolId.HasValue)
                {
                    ViewBag.ProtocolURL = WebsiteURL + "/PROTOCOL/" + qms055.ProtocolType + "/Linkage/" + qms055.ProtocolId + "?m=1";
                    ViewBag.ProtocolTypeDescription = Manager.GetCategoryOnlyDescription(qms055.ProtocolType, qms055.BU, qms055.Location, "Protocol Type");
                }
                else
                {
                    QMS036 objQMS036 = db.QMS036.FirstOrDefault(c => c.Project == qms055.Project && c.QualityProject == qms055.QualityProject && c.BU == qms055.BU && c.Location == qms055.Location && c.PartNo == qms055.PartNo && c.ParentPartNo == qms055.ParentPartNo && c.StageCode == qms055.StageCode && c.StageSequence == qms055.StageSequence);
                    if (objQMS036 != null)
                    {
                        ViewBag.ProtocolURL = "OpenAttachmentPopUp(" + objQMS036.LineId + ",false,'QMS036/" + objQMS036.LineId + "');";
                        ViewBag.ProtocolTypeDescription = "";
                    }
                }
            }
            string stageCodeDesc = db.Database.SqlQuery<string>("select  dbo.GET_IPI_STAGECODE_DESCRIPTION('" + qms055.StageCode + "','" + qms055.BU + "','" + qms055.Location + "')").FirstOrDefault();
            qms055.StageCode = stageCodeDesc;
            ViewBag.IsLTFPS = Manager.IsLTFPSProject(qms055.Project, qms055.BU, qms055.Location);
            ViewBag.IsGroundSpot = db.QMS002.Where(x => x.StageCode == qms055.StageCode && x.BU == qms055.BU && x.Location == qms055.Location).Select(x => x.IsGroundSpot).FirstOrDefault();
            string strReturned = clsImplementationEnum.SeamListInspectionStatus.Returned.GetStringValue();
            string strRejected = clsImplementationEnum.SeamListInspectionStatus.Rejected.GetStringValue();
            ViewBag.GroundSpot = db.QMS055.Where(x => (x.RequestNo == qms055.RequestNo && x.RequestId < qms055.RequestId) && (x.TestResult == strReturned || x.TestResult == strRejected)).OrderByDescending(x => x.RequestId).Select(x => x.GroundSpot).FirstOrDefault();

            var projectDetails = db.COM001.Where(x => x.t_cprj == qms055.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
            ViewBag.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
            List<GLB002> lstCategory = Manager.GetSubCatagories("TPI Agency", qms055.BU, qms055.Location, null);
            List<GLB002> lstCategoryTPI = Manager.GetSubCatagories("TPI Intervention", qms055.BU, qms055.Location, null);
            qms055.TPIAgency1 = GetCategoryDescriptionFromList(lstCategory, qms055.TPIAgency1);
            qms055.TPIAgency2 = GetCategoryDescriptionFromList(lstCategory, qms055.TPIAgency2);
            qms055.TPIAgency3 = GetCategoryDescriptionFromList(lstCategory, qms055.TPIAgency3);
            qms055.TPIAgency4 = GetCategoryDescriptionFromList(lstCategory, qms055.TPIAgency4);
            qms055.TPIAgency5 = GetCategoryDescriptionFromList(lstCategory, qms055.TPIAgency5);
            qms055.TPIAgency6 = GetCategoryDescriptionFromList(lstCategory, qms055.TPIAgency6);
            qms055.TPIAgency1Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms055.TPIAgency1Intervention);
            qms055.TPIAgency2Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms055.TPIAgency2Intervention);
            qms055.TPIAgency3Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms055.TPIAgency3Intervention);
            qms055.TPIAgency4Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms055.TPIAgency4Intervention);
            qms055.TPIAgency5Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms055.TPIAgency5Intervention);
            qms055.TPIAgency6Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms055.TPIAgency6Intervention);

            var BUDescription = (from a in db.COM002
                                 where a.t_dtyp == 2 && a.t_dimx == qms055.BU
                                 select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
            qms055.BU = Convert.ToString(BUDescription.BUDesc);
            var locDescription = (from a in db.COM002
                                  where a.t_dtyp == 1 && a.t_dimx == qms055.Location
                                  select new
                                  { Location = a.t_dimx + "-" + a.t_desc }).FirstOrDefault();
            qms055.Location = locDescription.Location;

            ViewBag.fromURL = fromURL;


            //  qms055.ClearedQuantity = qms055.OfferedQuantity;
            ViewBag.remark = GetPreviousRequestRemark(qms055.RequestNo, qms055.RequestId);

            var IsProtocolApplicable = db.QMS002.Where(x => x.Location == qms055.Location && x.BU == qms055.BU && x.StageCode == qms055.StageCode).Select(x => x.IsProtocolApplicable).FirstOrDefault();
            if (IsProtocolApplicable)
                ViewBag.ProtocolApplicable = "Yes";
            else
                ViewBag.ProtocolApplicable = "No";

            if (qms055.LTFPSHeaderId > 0)
            {
                ViewBag.folderPath = "QMS055_LTFPS//" + qms055.LTFPSHeaderId + "//" + qms055.IterationNo + "//" + qms055.RequestNoSequence;
                ViewBag.FCS_TableId = qms055.LTFPSHeaderId;
            }
            else
            {
                ViewBag.folderPath = "QMS055//" + qms055.refHeaderId + "//" + qms055.IterationNo + "//" + qms055.RequestNoSequence;
                ViewBag.FCS_TableId = qms055.refHeaderId;
            }


            return PartialView("_LoadPartlistReOfferFormPartial", qms055);
        }

        // ONLY USED IN MOBILE APPLICATION
        [HttpPost]
        public ActionResult LoadPartlistReOfferFormPartialMobile(int RequestId, string APIRequestFrom = "", string UserName = "")
        {
            try
            {
                db.Configuration.ProxyCreationEnabled = false;
                QMS055 qms055 = db.QMS055.Where(x => x.RequestId == RequestId).Select(y => y).FirstOrDefault();
                db.Entry(qms055).State = System.Data.Entity.EntityState.Detached;
                QMS045 qms045 = db.QMS045.Where(x => x.HeaderId == qms055.refHeaderId).Select(y => y).FirstOrDefault();

                bool ProtocolApplicable = false;
                string ProtocolTypeDescription = "";
                bool? IsLTFPS = false;
                bool? IsGroundSpot = false;
                double? GroundSpot = 0;
                string Project = "";
                string remark = "";

                ProtocolTypeDescription = string.Empty;
                if (!string.IsNullOrWhiteSpace(Convert.ToString(qms055.ProtocolType)))
                {
                    if (qms055.ProtocolId.HasValue)
                    {
                        ProtocolTypeDescription = Manager.GetCategoryOnlyDescription(qms055.ProtocolType, qms055.BU, qms055.Location, "Protocol Type");
                    }
                    else
                    {
                        QMS036 objQMS036 = db.QMS036.FirstOrDefault(c => c.Project == qms055.Project && c.QualityProject == qms055.QualityProject && c.BU == qms055.BU && c.Location == qms055.Location && c.PartNo == qms055.PartNo && c.ParentPartNo == qms055.ParentPartNo && c.StageCode == qms055.StageCode && c.StageSequence == qms055.StageSequence);
                        if (objQMS036 != null)
                        {

                            ProtocolTypeDescription = "";
                        }
                    }
                }
                string stageCodeDesc = db.Database.SqlQuery<string>("select  dbo.GET_IPI_STAGECODE_DESCRIPTION('" + qms055.StageCode + "','" + qms055.BU + "','" + qms055.Location + "')").FirstOrDefault();
                qms055.StageCode = stageCodeDesc;
                IsLTFPS = Manager.IsLTFPSProject(qms055.Project, qms055.BU, qms055.Location);
                IsGroundSpot = db.QMS002.Where(x => x.StageCode == qms055.StageCode && x.BU == qms055.BU && x.Location == qms055.Location).Select(x => x.IsGroundSpot).FirstOrDefault();
                string strReturned = clsImplementationEnum.SeamListInspectionStatus.Returned.GetStringValue();
                string strRejected = clsImplementationEnum.SeamListInspectionStatus.Rejected.GetStringValue();
                GroundSpot = db.QMS055.Where(x => (x.RequestNo == qms055.RequestNo && x.RequestId < qms055.RequestId) && (x.TestResult == strReturned || x.TestResult == strRejected)).OrderByDescending(x => x.RequestId).Select(x => x.GroundSpot).FirstOrDefault();

                var projectDetails = db.COM001.Where(x => x.t_cprj == qms055.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                List<GLB002> lstCategory = Manager.GetSubCatagories("TPI Agency", qms055.BU, qms055.Location, null);
                List<GLB002> lstCategoryTPI = Manager.GetSubCatagories("TPI Intervention", qms055.BU, qms055.Location, null);
                qms055.TPIAgency1 = GetCategoryDescriptionFromList(lstCategory, qms055.TPIAgency1);
                qms055.TPIAgency2 = GetCategoryDescriptionFromList(lstCategory, qms055.TPIAgency2);
                qms055.TPIAgency3 = GetCategoryDescriptionFromList(lstCategory, qms055.TPIAgency3);
                qms055.TPIAgency4 = GetCategoryDescriptionFromList(lstCategory, qms055.TPIAgency4);
                qms055.TPIAgency5 = GetCategoryDescriptionFromList(lstCategory, qms055.TPIAgency5);
                qms055.TPIAgency6 = GetCategoryDescriptionFromList(lstCategory, qms055.TPIAgency6);
                qms055.TPIAgency1Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms055.TPIAgency1Intervention);
                qms055.TPIAgency2Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms055.TPIAgency2Intervention);
                qms055.TPIAgency3Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms055.TPIAgency3Intervention);
                qms055.TPIAgency4Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms055.TPIAgency4Intervention);
                qms055.TPIAgency5Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms055.TPIAgency5Intervention);
                qms055.TPIAgency6Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms055.TPIAgency6Intervention);

                var BUDescription = (from a in db.COM002
                                     where a.t_dtyp == 2 && a.t_dimx == qms055.BU
                                     select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
                qms055.BU = Convert.ToString(BUDescription.BUDesc);
                var locDescription = (from a in db.COM002
                                      where a.t_dtyp == 1 && a.t_dimx == qms055.Location
                                      select new
                                      { Location = a.t_dimx + "-" + a.t_desc }).FirstOrDefault();
                qms055.Location = locDescription.Location;


                //  qms055.ClearedQuantity = qms055.OfferedQuantity;
                remark = GetPreviousRequestRemark(qms055.RequestNo, qms055.RequestId);

                var IsProtocolApplicable = db.QMS002.Where(x => x.Location == qms055.Location && x.BU == qms055.BU && x.StageCode == qms055.StageCode).Select(x => x.IsProtocolApplicable).FirstOrDefault();
                if (IsProtocolApplicable)
                    ProtocolApplicable = true;
                else
                    ProtocolApplicable = false;

                //if (qms055.LTFPSHeaderId > 0)
                //{
                //    ViewBag.folderPath = "QMS055_LTFPS/" + qms055.LTFPSHeaderId + "/" + qms055.IterationNo + "/" + qms055.RequestNoSequence;
                //}
                //else
                //{
                //    ViewBag.folderPath = "QMS055/" + qms055.refHeaderId + "/" + qms055.IterationNo + "/" + qms055.RequestNoSequence;
                //}
                return Json(new
                {
                    res = 1,
                    msg = "Success",
                    QMS055 = qms055,
                    Total_Quantity = qms045.BalanceQuantity,
                    Available_To_Offer_Quantity = qms045.Quantity,
                    IsLTFPS,
                    ProtocolApplicable,
                    ProtocolTypeDescription,
                    IsGroundSpot,
                    GroundSpot,
                    remark,
                    Project,

                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = 0,
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult GetPartlistRequestedOfferedPartial(QMS055 qms055)
        {
            return PartialView("_GetPartlistRequestedOfferedPartial", qms055);
        }

        [HttpPost]
        public JsonResult checkRequestedQuantity(int RequestId, int qty)
        {
            bool flag = true;
            QMS055 objQMS055 = db.QMS055.Where(x => x.RequestId == RequestId).FirstOrDefault();
            if (objQMS055 != null)
            {
                if (qty > objQMS055.OfferedQuantity)
                {
                    flag = false;
                }
                else
                {
                    flag = true;
                }
            }
            return Json(flag);
        }

        // ALSO USED in MOBILE APPLICATION
        [HttpPost]
        public ActionResult SaveClearedQuantity(QMS055 qms055, string ltfpsRevNo = "", bool IsRequireReworkLTFPS = false, double groundspot = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string ApprovedStatus = clsImplementationEnum.ICLPartStatus.Approved.GetStringValue();
                string clearedStatus = clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue();
                bool callReadyToOffer = false;
                bool parentReadyToOffer = false;
                QMS055 objQMS055Noti = null;
                QMS055 objQMS055 = db.QMS055.Where(x => x.RequestId == qms055.RequestId).FirstOrDefault();
                QMS036 objQMS036 = db.QMS036.Where(x =>
                                                        x.QualityProject == objQMS055.QualityProject &&
                                                        x.Project == objQMS055.Project &&
                                                        x.BU == objQMS055.BU &&
                                                        x.Location == objQMS055.Location &&
                                                        x.PartNo == objQMS055.PartNo &&
                                                        x.ParentPartNo == objQMS055.ParentPartNo &&
                                                        x.ChildPartNo == objQMS055.ChildPartNo &&
                                                        x.StageCode == objQMS055.StageCode).FirstOrDefault();
                if (objQMS055 != null)
                {

                    objResponseMsg = Manager.IsApplicableForAttend(objQMS055.RequestId, clsImplementationEnum.InspectionFor.PART.GetStringValue(), "QC");
                    if (!objResponseMsg.Key)
                    {
                        return Json(objResponseMsg);
                    }

                    bool isLTFPS = Manager.IsLTFPSProject(objQMS055.Project, objQMS055.BU, objQMS055.Location);
                    QMS045 objQMS045 = new QMS045();
                    LTF002 objLTF002 = new LTF002();
                    bool TPIOnlineApproval = false;

                    if (isLTFPS)
                    {
                        objLTF002 = db.LTF002.Where(x => x.LineId == objQMS055.LTFPSHeaderId).FirstOrDefault();
                        TPIOnlineApproval = db.QMS010.Where(x => x.QualityProject == objLTF002.QualityProject).Select(x => x.TPIOnlineApproval).FirstOrDefault();
                    }
                    else
                    {
                        objQMS045 = db.QMS045.Where(x => x.HeaderId == objQMS055.refHeaderId).FirstOrDefault();
                        TPIOnlineApproval = db.QMS010.Where(x => x.QualityProject == objQMS045.QualityProject).Select(x => x.TPIOnlineApproval).FirstOrDefault();
                    }

                    if (qms055.ClearedQuantity > 0 && (qms055.RejectedQuantity == null || qms055.RejectedQuantity == 0))
                    {

                        //All offered Cleared
                        objQMS055.ClearedQuantity = qms055.ClearedQuantity;
                        objQMS055.GroundSpot = groundspot;
                        objQMS055.TestResult = clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue();

                        //TPI Cleared in Case of TPI not applicable for customer

                        if (!TPIOnlineApproval)
                        {
                            updateTPIStatus(objQMS055);
                        }
                        db.SaveChanges();


                        if (isLTFPS)
                        {
                            objLTF002.LNTStatus = clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue();
                            objLTF002.LNTInspectorResultOn = DateTime.Now;

                            string strInspectionStatus = GetLTFPSInspectionStatus(objLTF002.LineId, 1);
                            bool IsTPIOnlineApproval = (new clsManager()).isTPIRequired(objLTF002.QualityProject, "QMS055", objQMS055.RequestId);

                            if (!string.IsNullOrEmpty(strInspectionStatus))
                            {
                                if (!IsTPIOnlineApproval)
                                {
                                    objLTF002.InspectionStatus = strInspectionStatus;
                                }
                            }
                            if (!IsTPIOnlineApproval)
                            {
                                if (!string.IsNullOrWhiteSpace(ltfpsRevNo))
                                {
                                    objLTF002.DocRevNo = ltfpsRevNo;
                                }
                                callReadyToOffer = true;
                                //db.SP_LTFPS_RELEASE_READY_TO_OFFER(objLTF002.HeaderId).FirstOrDefault();
                                //List<List<LTF002>> groups = db.LTF002.Where(x => x.PartNo == objLTF002.PartNo && x.QualityProject == objLTF002.QualityProject && x.Location == objLTF002.Location && x.OperationNo > objLTF002.OperationNo).OrderBy(x => x.OperationNo).GroupBy(c => c.OperationNo)
                                //   .Select(group => group.ToList())
                                //   .ToList();

                                //if (groups != null && groups.Count > 0)
                                //{
                                //    List<LTF002> lstLTF002 = groups.FirstOrDefault().ToList();
                                //    if (lstLTF002 != null && lstLTF002.Count > 0)
                                //    {
                                //        lstLTF002.ForEach(x =>
                                //        {
                                //            x.InspectionStatus = x.InspectionStatus == null ? clsImplementationEnum.PartlistOfferInspectionStatus.READY_TO_OFFER.GetStringValue() : x.InspectionStatus;
                                //            // x.BalanceQuantity = x.InspectionStatus == null ? objQMS055.ClearedQuantity : (x.BalanceQuantity == null ? 0 : x.BalanceQuantity) + objQMS055.ClearedQuantity;
                                //        }
                                //        );
                                //    }
                                //}
                            }
                        }
                        else
                        {
                            if (objQMS045.BalanceQuantity > 0)
                            {
                                objQMS045.L_TInspectionResult = clsImplementationEnum.PartlistOfferInspectionStatus.PARTIALLY_CLEARED.GetStringValue();
                                objQMS045.LNTInspectorResultOn = DateTime.Now;
                                //No change in Inspection Status
                            }
                            else
                            {
                                objQMS045.L_TInspectionResult = clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue();
                                objQMS045.LNTInspectorResultOn = DateTime.Now;
                            }

                            #region update Protocol 
                            if (objQMS045.ProtocolId.HasValue && !string.IsNullOrWhiteSpace(objQMS045.ProtocolType))
                            {
                                string PRLTableName = Manager.GetLinkedProtocolTableName(qms055.ProtocolType);
                                if (!string.IsNullOrWhiteSpace(PRLTableName))
                                {
                                    if (Convert.ToInt32((objQMS045.ProtocolType).Replace("PROTOCOL", "")) > 36)
                                    {

                                        db.Database.ExecuteSqlCommand("UPDATE " + PRLTableName + " SET  InspectedBy='" + (APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName) + "',InspectedOn=GETDATE(),InspectionDate=GETDATE(),Result='ACCEPTED' WHERE HeaderId = " + qms055.ProtocolId);
                                    }
                                    else
                                    {
                                        db.Database.ExecuteSqlCommand("UPDATE " + PRLTableName + " SET InspectionDate=GETDATE() WHERE HeaderId = " + qms055.ProtocolId);
                                    }
                                }
                            }
                            #endregion

                            string strInspectionStatus = GetInspectionStatus(objQMS045.HeaderId, (objQMS045.Quantity == null ? 0 : Convert.ToInt32(objQMS045.Quantity)));
                            bool IsTPIOnlineApproval = (new clsManager()).isTPIRequired(objQMS045.QualityProject, "QMS055", objQMS055.RequestId);

                            if (!string.IsNullOrEmpty(strInspectionStatus))
                            {
                                if (!IsTPIOnlineApproval)
                                {
                                    objQMS045.InspectionStatus = strInspectionStatus;
                                }
                            }
                            if (!IsTPIOnlineApproval)
                            {
                                if (!db.QMS045.Any(x => x.PartNo == objQMS045.PartNo && x.ParentPartNo == objQMS045.ParentPartNo && x.QualityProject == objQMS045.QualityProject && x.Location == objQMS045.Location && x.StageSequence == objQMS045.StageSequence && x.HeaderId != objQMS045.HeaderId && x.InspectionStatus != clearedStatus))
                                {
                                    List<List<QMS045>> groups = db.QMS045.Where(x => x.PartNo == objQMS045.PartNo && x.ParentPartNo == objQMS045.ParentPartNo && x.QualityProject == objQMS045.QualityProject && x.Location == objQMS045.Location && x.StageSequence > objQMS045.StageSequence).OrderBy(x => x.StageSequence).GroupBy(c => c.StageSequence)
                                       .Select(group => group.ToList())
                                       .ToList();

                                    if (groups != null && groups.Count > 0)
                                    {
                                        List<QMS045> lstQMS045 = groups.FirstOrDefault().ToList();
                                        if (lstQMS045 != null && lstQMS045.Count > 0)
                                        {
                                            lstQMS045.ForEach(x =>
                                            {
                                                x.InspectionStatus = x.InspectionStatus == null ? clsImplementationEnum.PartlistOfferInspectionStatus.READY_TO_OFFER.GetStringValue() : x.InspectionStatus;
                                                x.BalanceQuantity = x.InspectionStatus == null ? objQMS055.ClearedQuantity : (x.BalanceQuantity == null ? 0 : x.BalanceQuantity) + objQMS055.ClearedQuantity;
                                            });

                                            lstQMS045.ForEach(x =>
                                            {
                                                x.InspectionStatus = (x.BalanceQuantity > 0 && x.InspectionStatus != clsImplementationEnum.PartlistOfferInspectionStatus.READY_TO_OFFER.GetStringValue()) ? clsImplementationEnum.PartlistOfferInspectionStatus.PARTIALLY_OFFERED.GetStringValue() : x.InspectionStatus;
                                            });

                                        }
                                    }
                                }
                            }

                            parentReadyToOffer = true;

                        }
                        //bool IsTPIOnlineApproval = db.QMS010.Where(x => x.QualityProject == objQMS045.QualityProject).Select(x => x.TPIOnlineApproval).FirstOrDefault();
                    }
                    else if (qms055.RejectedQuantity > 0 && (qms055.ClearedQuantity == null || qms055.ClearedQuantity == 0))
                    {
                        //All Rejected
                        objQMS055.RejectedQuantity = qms055.RejectedQuantity;
                        objQMS055.TestResult = clsImplementationEnum.PartlistOfferInspectionStatus.REJECTED.GetStringValue();
                        objQMS055.QCRemarks = qms055.QCRemarks;
                        objQMS055.GroundSpot = groundspot;
                        db.SaveChanges();
                        //Commented by Dharmesh For Issue related this Status
                        //  objQMS045.InspectionStatus = clsImplementationEnum.PartlistOfferInspectionStatus.PARTIALLY_REWORK.GetStringValue();

                        if (isLTFPS)
                        {
                            if (objLTF002.LNTStatus == null)
                            {
                                objLTF002.LNTStatus = clsImplementationEnum.PartlistOfferInspectionStatus.REJECTED.GetStringValue();
                                objLTF002.LNTInspectorResultOn = DateTime.Now;
                            }
                            if (IsRequireReworkLTFPS)
                            {
                                objLTF002.InspectionStatus = clsImplementationEnum.SeamListInspectionStatus.ReworkNotAttached.GetStringValue();
                            }
                        }
                        else
                        {
                            if (objQMS045.L_TInspectionResult == null)
                            {
                                objQMS045.L_TInspectionResult = clsImplementationEnum.PartlistOfferInspectionStatus.PARTIALLY_REJECTED.GetStringValue();
                                objQMS045.LNTInspectorResultOn = DateTime.Now;
                            }
                        }

                        if (!IsRequireReworkLTFPS)
                        {
                            bool flag = AddPendingQuantitiesDetails(objQMS055, objQMS036, clsImplementationEnum.PartlistOfferInspectionStatus.REJECTED.GetStringValue(), false, APIRequestFrom, UserName);
                        }

                        objQMS055Noti = db.QMS055.Where(x => x.RequestNo == objQMS055.RequestNo && x.refHeaderId == objQMS055.refHeaderId).OrderByDescending(o => o.CreatedOn).FirstOrDefault();

                    }
                    else
                    {
                        // Some Cleared and some Rejected
                        objQMS055.RejectedQuantity = qms055.RejectedQuantity;
                        objQMS055.ClearedQuantity = qms055.ClearedQuantity;
                        objQMS055.QCRemarks = qms055.QCRemarks;
                        objQMS055.TestResult = clsImplementationEnum.PartlistOfferInspectionStatus.PARTIALLY_CLEARED.GetStringValue();

                        if (isLTFPS)
                        {
                            objLTF002.LNTStatus = clsImplementationEnum.PartlistOfferInspectionStatus.PARTIALLY_CLEARED.GetStringValue();
                            objLTF002.LNTInspectorResultOn = DateTime.Now;
                        }
                        else
                        {
                            objQMS045.L_TInspectionResult = clsImplementationEnum.PartlistOfferInspectionStatus.PARTIALLY_CLEARED.GetStringValue();
                            objQMS045.LNTInspectorResultOn = DateTime.Now;
                        }
                        db.SaveChanges();
                        if (qms055.RejectedQuantity > 0)
                        {
                            bool flag = AddPendingQuantitiesDetails(objQMS055, objQMS036, clsImplementationEnum.PartlistOfferInspectionStatus.REJECTED.GetStringValue(), false, APIRequestFrom, UserName);

                            objQMS055Noti = db.QMS055.Where(x => x.RequestNo == objQMS055.RequestNo && x.refHeaderId == objQMS055.refHeaderId).OrderByDescending(o => o.CreatedOn).FirstOrDefault();

                        }
                        //bool TPIOnlineApproval = false;

                        //if (isLTFPS)
                        //{
                        //    TPIOnlineApproval = db.QMS010.Where(x => x.QualityProject == objLTF002.QualityProject).Select(x => x.TPIOnlineApproval).FirstOrDefault();
                        //}
                        //else
                        //{
                        //    TPIOnlineApproval = db.QMS010.Where(x => x.QualityProject == objQMS045.QualityProject).Select(x => x.TPIOnlineApproval).FirstOrDefault();
                        //}
                        if (!TPIOnlineApproval)
                        {
                            updateTPIStatus(objQMS055);
                        }
                    }

                    objQMS055.EditedBy = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                    objQMS055.EditedOn = DateTime.Now;
                    objQMS055.InspectedBy = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                    objQMS055.InspectedOn = DateTime.Now;

                    if (isLTFPS)
                    {
                        objLTF002.EditedOn = DateTime.Now;
                        objLTF002.EditedBy = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                    }
                    else
                    {
                        objQMS045.EditedOn = DateTime.Now;
                        objQMS045.EditedBy = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                    }

                    db.SaveChanges();

                    if (callReadyToOffer)
                    {
                        db.SP_LTFPS_RELEASE_READY_TO_OFFER(objLTF002.HeaderId).FirstOrDefault();
                    }

                        //if (parentReadyToOffer)
                        //{

                        //string specialStage = Convert.ToString(Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.SpecialStageCodeForRTO.GetStringValue()));
                        //if (objQMS045.InspectionStatus == clearedStatus && objQMS045.StageCode.ToUpper() == specialStage.ToUpper())
                        //{
                        //    List<QMS012> SeamList = db.QMS012.Where(c => c.AssemblyNo == objQMS045.ChildPartNo && c.QualityProject == objQMS045.QualityProject && c.Project == objQMS045.Project).ToList();
                        //    foreach (var seam in SeamList)
                        //    {
                        //        QMS030 objQMS030 = db.QMS030.FirstOrDefault(c => c.Project == objQMS045.Project && c.QualityProject == objQMS045.QualityProject && c.SeamNo == seam.SeamNo && c.Status == ApprovedStatus);
                        //        if (objQMS030 != null)
                        //        {
                        //            Manager.ReadyToOffer_SEAM(objQMS030, true);
                        //        }
                        //    }
                        //}

                    #region Code for make parent part ready to offer 

                        //if (Manager.IsPARTFullyCleared(objQMS045.Project, objQMS045.QualityProject, objQMS045.BU, objQMS045.Location, objQMS045.PartNo))
                        //{
                        //    // get seams which are associated with this parts and ready to offer
                        //    List<string> SeamList = db.QMS012_Log.Where(c => c.QualityProject == objQMS045.QualityProject && c.Project == objQMS045.Project && c.Status == ApprovedStatus).ToList().Where(w => w.Position.Split(',').Contains(objQMS045.PartNo)).Select(s => s.SeamNo).Distinct().ToList();
                        //    foreach (var seam in SeamList)
                        //    {
                        //        //QMS012_Log objQMS012 = db.QMS012_Log.FirstOrDefault(c => c.Project == objQMS045.Project && c.QualityProject == objQMS045.QualityProject && c.SeamNo == seam && c.Status == ApprovedStatus);
                        //        //if(Manager.IsChildPartCleared(objQMS045.Project, objQMS045.QualityProject, objQMS045.BU, objQMS045.Location, objQMS012.AssemblyNo))
                        //        //{
                        //        QMS030 objQMS030 = db.QMS030.FirstOrDefault(c => c.Project == objQMS045.Project && c.QualityProject == objQMS045.QualityProject && c.SeamNo == seam && c.Status == ApprovedStatus);
                        //        if (objQMS030 != null)
                        //        {
                        //            Manager.ReadyToOffer_SEAM(objQMS030);
                        //        }
                        //        //}
                        //    }

                        //    QMS035 objQMS035 = db.QMS035.FirstOrDefault(c => c.Project == objQMS045.Project && c.QualityProject == objQMS045.QualityProject && c.BU == objQMS045.BU && c.Location == objQMS045.Location && c.ChildPartNo == objQMS045.ParentPartNo && c.Status == ApprovedStatus);
                        //    if (objQMS035 != null)
                        //    {
                        //        Manager.ReadyToOffer_PART(objQMS035);
                        //    }
                        //    else
                        //    {
                        //        List<QMS012> lstQMS012 = db.QMS012.Where(c => c.Project == objQMS045.Project && c.QualityProject == objQMS045.QualityProject && c.AssemblyNo == objQMS045.ParentPartNo).ToList();
                        //        foreach (var objQMS012 in lstQMS012)
                        //        {
                        //            //if (Manager.IsChildPartCleared(objQMS045.Project, objQMS045.QualityProject, objQMS045.BU, objQMS045.Location, objQMS012.AssemblyNo))
                        //            //{
                        //            QMS030 objQMS030 = db.QMS030.FirstOrDefault(c => c.Project == objQMS045.Project && c.QualityProject == objQMS045.QualityProject && c.SeamNo == objQMS012.SeamNo && c.Status == ApprovedStatus);
                        //            if (objQMS030 != null)
                        //            {
                        //                Manager.ReadyToOffer_SEAM(objQMS030);
                        //            }
                        //            //}
                        //        }
                        //    }

                        //}

                    #endregion
                        //}

                    #region Send Notification                    
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PROD3.GetStringValue(), objQMS055.Project, objQMS055.BU, objQMS055.Location, "Stage: " + objQMS055.StageCode + " of Part: " + objQMS055.PartNo + " & Project No: " + objQMS055.QualityProject + " has been Inspected. Cleared Quantity: " + objQMS055.ClearedQuantity + ", Rejected Qty: " + objQMS055.RejectedQuantity, clsImplementationEnum.NotificationType.Information.GetStringValue(), objQMS055Noti != null ? "/IPI/PartlistOfferInspection/ViewReOfferedDetails?RequestId=" + objQMS055Noti.RequestId : null, null, APIRequestFrom, UserName);
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Submitted successfully.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Submitted offer not available.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public void updateTPIStatus(QMS055 objQMS055, bool isTPIReturn = false)
        {
            if (isTPIReturn)
            {
                if (objQMS055.TPIAgency1 != null && !string.IsNullOrWhiteSpace(objQMS055.TPIAgency1))
                {
                    objQMS055.TPIAgency1Result = clsImplementationEnum.PartlistOfferInspectionStatus.RETURNED.GetStringValue();
                    objQMS055.TPIAgency1ResultOn = DateTime.Now;
                }
                if (objQMS055.TPIAgency2 != null && !string.IsNullOrWhiteSpace(objQMS055.TPIAgency2))
                {
                    objQMS055.TPIAgency2Result = clsImplementationEnum.PartlistOfferInspectionStatus.RETURNED.GetStringValue();
                    objQMS055.TPIAgency2ResultOn = DateTime.Now;
                }
                if (objQMS055.TPIAgency3 != null && !string.IsNullOrWhiteSpace(objQMS055.TPIAgency3))
                {
                    objQMS055.TPIAgency3Result = clsImplementationEnum.PartlistOfferInspectionStatus.RETURNED.GetStringValue();
                    objQMS055.TPIAgency3ResultOn = DateTime.Now;
                }
                if (objQMS055.TPIAgency4 != null && !string.IsNullOrWhiteSpace(objQMS055.TPIAgency4))
                {
                    objQMS055.TPIAgency4Result = clsImplementationEnum.PartlistOfferInspectionStatus.RETURNED.GetStringValue();
                    objQMS055.TPIAgency4ResultOn = DateTime.Now;
                }
                if (objQMS055.TPIAgency5 != null && !string.IsNullOrWhiteSpace(objQMS055.TPIAgency5))
                {
                    objQMS055.TPIAgency5Result = clsImplementationEnum.PartlistOfferInspectionStatus.RETURNED.GetStringValue();
                    objQMS055.TPIAgency5ResultOn = DateTime.Now;
                }
                if (objQMS055.TPIAgency6 != null && !string.IsNullOrWhiteSpace(objQMS055.TPIAgency6))
                {
                    objQMS055.TPIAgency6Result = clsImplementationEnum.PartlistOfferInspectionStatus.RETURNED.GetStringValue();
                    objQMS055.TPIAgency6ResultOn = DateTime.Now;
                }
            }
            else
            {
                if (objQMS055.TPIAgency1 != null && !string.IsNullOrWhiteSpace(objQMS055.TPIAgency1))
                {
                    objQMS055.TPIAgency1Result = clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue();
                    objQMS055.TPIAgency1ResultOn = DateTime.Now;
                }
                if (objQMS055.TPIAgency2 != null && !string.IsNullOrWhiteSpace(objQMS055.TPIAgency2))
                {
                    objQMS055.TPIAgency2Result = clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue();
                    objQMS055.TPIAgency2ResultOn = DateTime.Now;
                }
                if (objQMS055.TPIAgency3 != null && !string.IsNullOrWhiteSpace(objQMS055.TPIAgency3))
                {
                    objQMS055.TPIAgency3Result = clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue();
                    objQMS055.TPIAgency3ResultOn = DateTime.Now;
                }
                if (objQMS055.TPIAgency4 != null && !string.IsNullOrWhiteSpace(objQMS055.TPIAgency4))
                {
                    objQMS055.TPIAgency4Result = clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue();
                    objQMS055.TPIAgency4ResultOn = DateTime.Now;
                }
                if (objQMS055.TPIAgency5 != null && !string.IsNullOrWhiteSpace(objQMS055.TPIAgency5))
                {
                    objQMS055.TPIAgency5Result = clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue();
                    objQMS055.TPIAgency5ResultOn = DateTime.Now;
                }
                if (objQMS055.TPIAgency6 != null && !string.IsNullOrWhiteSpace(objQMS055.TPIAgency6))
                {
                    objQMS055.TPIAgency6Result = clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue();
                    objQMS055.TPIAgency6ResultOn = DateTime.Now;
                }
            }
        }
        // ALSO USED in MOBILE APPLICATION
        [HttpPost]
        public ActionResult ReturnedQuantity(QMS055 qms055, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS055 objQMS055 = db.QMS055.Where(x => x.RequestId == qms055.RequestId).FirstOrDefault();
                QMS036 objQMS036 = db.QMS036.Where(x =>
                                                        x.QualityProject == objQMS055.QualityProject &&
                                                        x.Project == objQMS055.Project &&
                                                        x.BU == objQMS055.BU &&
                                                        x.Location == objQMS055.Location &&
                                                        x.PartNo == objQMS055.PartNo &&
                                                        x.ParentPartNo == objQMS055.ParentPartNo &&
                                                        x.ChildPartNo == objQMS055.ChildPartNo &&
                                                        x.StageCode == objQMS055.StageCode).FirstOrDefault();
                if (objQMS055 != null)
                {
                    objResponseMsg = Manager.IsApplicableForAttend(objQMS055.RequestId, clsImplementationEnum.InspectionFor.PART.GetStringValue(), "QC");
                    if (!objResponseMsg.Key)
                    {
                        return Json(objResponseMsg);
                    }

                    QMS045 objQMS045 = new QMS045();
                    LTF002 objLTF002 = new LTF002();
                    bool isLTFPS = Manager.IsLTFPSProject(objQMS055.Project, objQMS055.BU, objQMS055.Location);
                    if (isLTFPS)
                    {
                        objLTF002 = db.LTF002.Where(x => x.LineId == objQMS055.LTFPSHeaderId).FirstOrDefault();
                    }
                    else
                    {
                        objQMS045 = db.QMS045.Where(x => x.HeaderId == objQMS055.refHeaderId).FirstOrDefault();
                    }
                    objQMS055.QCRemarks = qms055.QCRemarks;
                    objQMS055.TestResult = clsImplementationEnum.PartlistOfferInspectionStatus.RETURNED.GetStringValue();
                    db.SaveChanges();
                    if (qms055.OfferedQuantity > 0)
                    {
                        bool flag = AddPendingQuantitiesDetails(objQMS055, objQMS036, clsImplementationEnum.PartlistOfferInspectionStatus.RETURNED.GetStringValue(), false, APIRequestFrom, UserName);
                    }
                    objQMS055.EditedBy = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                    objQMS055.EditedOn = DateTime.Now;
                    objQMS055.InspectedBy = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                    objQMS055.InspectedOn = DateTime.Now;

                    if (isLTFPS)
                    {
                        objLTF002.EditedOn = DateTime.Now;
                        objLTF002.EditedBy = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                    }
                    else
                    {
                        objQMS045.EditedOn = DateTime.Now;
                        objQMS045.EditedBy = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                    }

                    db.SaveChanges();

                    #region Send Notification
                    QMS055 objQMS055Noti = db.QMS055.Where(x => x.RequestNo == objQMS055.RequestNo && x.refHeaderId == objQMS055.refHeaderId).OrderByDescending(o => o.CreatedOn).FirstOrDefault();
                    if (objQMS055Noti != null)
                    {
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PROD3.GetStringValue(), objQMS055.Project, objQMS055.BU, objQMS055.Location, "Stage: " + objQMS055.StageCode + " of Part: " + objQMS055.PartNo + " & Project No: " + objQMS055.QualityProject + " has been Returned for Quantity: " + objQMS055.OfferedQuantity, clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/PartlistOfferInspection/ViewReOfferedDetails?RequestId=" + objQMS055Noti.RequestId, null, APIRequestFrom, UserName);
                    }
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Offer returned successfully.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Requested offer not available.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        // ALSO USED in MOBILE APPLICATION
        [HttpPost]
        public ActionResult SendOfferToCustomer(QMS055 qms055, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS055 objQMS055 = db.QMS055.Where(x => x.RequestId == qms055.RequestId).FirstOrDefault();
                if (objQMS055 != null)
                {
                    objQMS055.OfferedtoCustomerBy = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                    objQMS055.OfferedtoCustomerOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Offered successfully.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "offer not available.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        // ALSO USED in MOBILE APPLICATION
        [HttpPost]
        public ActionResult ReOfferedQuantity(QMS055 qms055, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS055 objQMS055 = db.QMS055.Where(x => x.RequestId == qms055.RequestId).FirstOrDefault();
                if (objQMS055 != null)
                {
                    objQMS055.OfferedBy = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                    objQMS055.OfferedOn = DateTime.Now;

                    if (objQMS055.ProtocolId.HasValue)
                    {
                        string PRLTableName = Manager.GetLinkedProtocolTableName(objQMS055.ProtocolType);
                        db.Database.ExecuteSqlCommand("UPDATE " + PRLTableName + " SET OfferDate=GETDATE() WHERE HeaderId = " + objQMS055.ProtocolId);
                    }
                    db.SaveChanges();

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), objQMS055.Project, objQMS055.BU, objQMS055.Location, "Stage: " + objQMS055.StageCode + " of Part: " + objQMS055.PartNo + " & Project No: " + objQMS055.QualityProject + " has been re offered for Inspection for Quantity: " + objQMS055.OfferedQuantity, clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/PartlistOfferInspection/ViewOfferedDetails?RequestId=" + objQMS055.RequestId, null, APIRequestFrom, UserName);
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Part offered successfully.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Re offer Data not available.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        // ALSO USED in MOBILE APPLICATION
        [HttpPost]
        public ActionResult CheckPartListOffered(int? headerId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS045 objQMS045 = db.QMS045.Where(x => x.HeaderId == headerId).FirstOrDefault();
                if (objQMS045 != null)
                {
                    var stageStatus = clsImplementationEnum.TestPlanStatus.Approved.GetStringValue();
                    var lstTPStages = db.QMS036.Where(x => x.QualityProject == objQMS045.QualityProject && x.Project == objQMS045.Project
                                    && x.Location == objQMS045.Location && x.BU == objQMS045.BU && x.PartNo == objQMS045.PartNo && x.ParentPartNo == objQMS045.ParentPartNo
                                    && (x.StageSequence < objQMS045.StageSequence || (x.StageCode == objQMS045.StageCode && x.StageSequence == objQMS045.StageSequence)) && !x.StageStatus.Equals(stageStatus, StringComparison.OrdinalIgnoreCase)).ToList();
                    if (lstTPStages.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Please Approve sequence " + lstTPStages[0].StageSequence + " of Stage " + lstTPStages[0].StageCode + " in Test Plan";
                    }
                    else
                    {
                        objResponseMsg.Key = true;
                    }

                    #region Check For PRotocol Filled or not by Production Team
                    if (objResponseMsg.Key && objQMS045.ProtocolId.HasValue && !Manager.IsProtocolFilledByProduction(objQMS045.ProtocolId.Value, objQMS045.ProtocolType, objQMS045.BU, objQMS045.Location))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "You can't offer stage without fill protocol data";
                    }
                    #endregion
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please Try Again";
                }

                return Json(objResponseMsg);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error Occures Please try again";
                return Json(objResponseMsg);
            }
        }
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult TPIView()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadPartlistTPIDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_LoadPartlistTPIDataPartial");
        }

        [HttpPost]
        public JsonResult LoadPartlistTPIData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;

                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and (TestResult in('" + clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue() + "','" + clsImplementationEnum.PartlistOfferInspectionStatus.PARTIALLY_CLEARED.GetStringValue() + "')) and OfferedtoCustomerBy is not null and OfferedtoCustomerOn is not null and TPIAgency1Result is null ";
                }
                else
                {
                    strWhere += "1=1 ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                //"qms055.Project+' - '+com1.t_dsca",
                                                "QualityProject",
                                                "TestResult",
                                                "Shop",
                                                "ShopLocation",
                                                "ShopRemark",
                                                //"ltrim(rtrim(qms055.BU))+' - '+bcom2.t_desc",
                                                //"ltrim(rtrim(qms055.Location))+' - '+lcom2.t_desc",
                                                "PartNo",
                                                //"ParentPartNo",
                                                "ChildPartNo",
                                                "(dbo.GET_IPI_STAGECODE_DESCRIPTION(qms055.StageCode,ltrim(rtrim(qms055.BU)),ltrim(rtrim(qms055.Location))))",
                                                "StageSequence",
                                                "IterationNo",
                                                "RequestNo",
                                                "RequestNoSequence",
                                                "OfferedQuantity",
                                                //"ClearedQuantity",
                                                //"RejectedQuantity",
                                                "qms055.InspectedBy +' - '+com003i.t_name",
                                                "qms055.OfferedtoCustomerBy +' - '+com003oc.t_name",
                                                "Shop",
                                                "ShopLocation",
                                                "qms055.OfferedBy +' - '+com003o.t_name",
                                                "qms055.CreatedBy +' - '+com003c.t_name"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms055.BU", "qms055.Location");
                var lstResult = db.SP_FETCH_PARTLISTTESTDETAILS_HEADERS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.QualityProject),
                               Convert.ToString(uc.PartNo),
                               Convert.ToString(uc.ChildPartNo),
                               Convert.ToString(uc.OfferedQuantity),
                               Convert.ToString(uc.StageCodeDesc),
                               Convert.ToString(uc.StageSequence),
                               Convert.ToString(uc.IterationNo),
                               Convert.ToString(uc.RequestNo),
                               Convert.ToString(uc.RequestNoSequence),
                               Convert.ToString(uc.TestResult),
                               Convert.ToString(uc.Shop),
                               Convert.ToString(uc.ShopLocation),
                               Convert.ToString(uc.ShopRemark),
                               //Convert.ToString(uc.CreatedBy),
                               //Convert.ToString(uc.CreatedOn),
                               Convert.ToString(uc.OfferedBy),
                               Convert.ToString(uc.OfferedOn),
                               Convert.ToString(uc.InspectedBy),
                               Convert.ToString(uc.InspectedOn),
                               Convert.ToString(uc.OfferedtoCustomerBy),
                               Convert.ToString(uc.OfferedtoCustomerOn),
                               "<center style=\"white-space: nowrap;\"><a title=\"View Details\" class='' href='" + WebsiteURL + "/IPI/PartlistOfferInspection/TPIViewDetails?RequestId="+Convert.ToInt32(uc.RequestId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a>"+Manager.generateIPIGridButtons(uc.QualityProject,uc.Project,uc.BU,uc.Location,uc.PartNo,uc.ParentPartNo,uc.ChildPartNo,uc.StageCode,Convert.ToString(uc.StageSequence),"Part Details",55,"",":Part Details","Part Request Details")+"&nbsp;&nbsp;<a  href=\"javascript: void(0);\" Title=\"View Timeline\" onclick=\"ShowTimeline("+ uc.RequestId +")\"><i style=\"margin - left:5px;\" class=\"fa fa-clock-o\"></i></a>"
                               + (Convert.ToString(uc.ProtocolType) == clsImplementationEnum.ProtocolType.Other_Files.GetStringValue() ? ( "&nbsp;&nbsp;<a onclick=\"OpenProtocolPopUp("+ uc.refHeaderId +");\" Title=\"View Protocol Files\" ><i style=\"margin - left:5px;\" class=\"fa fa-file-text\"></i></a>"  ) : (uc.ProtocolId.HasValue ? "&nbsp;&nbsp;<a target=\"_blank\"  href=\"" + WebsiteURL + "/PROTOCOL/"+uc.ProtocolType+"/Linkage/"+uc.ProtocolId + "\" Title=\"View Protocol Details\" ><i style=\"margin - left:5px;\" class=\"fa fa-file-text\"></i></a>" : "&nbsp;&nbsp;<a Title=\"No Protocol Attached\" ><i style=\"opacity:0.3;\" class=\"fa fa-file-text\"></i></a>") )
                               +"</center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult TPIViewDetails(int RequestId = 0)
        {
            if (RequestId > 0)
            {
                QMS055 objQMS055 = db.QMS055.Where(x => x.RequestId == RequestId).FirstOrDefault();
                if (objQMS055 != null)
                {
                    var listQMS055 = JsonConvert.SerializeObject(objQMS055, Formatting.None,
                                                          new JsonSerializerSettings()
                                                          {
                                                              ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                                                          });
                    ViewBag.QMS055 = listQMS055;

                    return View(objQMS055);
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
            }
        }

        [HttpPost]
        public ActionResult LoadPartlistTPIFormPartial(string strModel)
        {
            QMS055 qms055 = JsonConvert.DeserializeObject<QMS055>(strModel);
            if (!string.IsNullOrWhiteSpace(Convert.ToString(qms055.ProtocolType)))
            {
                if (qms055.ProtocolId.HasValue)
                {
                    ViewBag.ProtocolURL = WebsiteURL + "/PROTOCOL/" + qms055.ProtocolType + "/Linkage/" + qms055.ProtocolId;
                    ViewBag.ProtocolTypeDescription = Manager.GetCategoryOnlyDescription(qms055.ProtocolType, qms055.BU, qms055.Location, "Protocol Type");
                }
                else
                {
                    QMS036 objQMS036 = db.QMS036.FirstOrDefault(c => c.Project == qms055.Project && c.QualityProject == qms055.QualityProject && c.BU == qms055.BU && c.Location == qms055.Location && c.PartNo == qms055.PartNo && c.ParentPartNo == qms055.ParentPartNo && c.StageCode == qms055.StageCode && c.StageSequence == qms055.StageSequence);
                    if (objQMS036 != null)
                    {
                        ViewBag.ProtocolURL = "OpenAttachmentPopUp(" + objQMS036.LineId + ",false,'QMS036/" + objQMS036.LineId + "');";
                        ViewBag.ProtocolTypeDescription = "";
                    }
                }
            }
            string stageCodeDesc = db.Database.SqlQuery<string>("select  dbo.GET_IPI_STAGECODE_DESCRIPTION('" + qms055.StageCode + "','" + qms055.BU + "','" + qms055.Location + "')").FirstOrDefault();
            qms055.StageCode = stageCodeDesc;

            ViewBag.IsLTFPS = Manager.IsLTFPSProject(qms055.Project, qms055.BU, qms055.Location);

            var projectDetails = db.COM001.Where(x => x.t_cprj == qms055.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
            ViewBag.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
            List<GLB002> lstCategory = Manager.GetSubCatagories("TPI Agency", qms055.BU, qms055.Location, null);
            List<GLB002> lstCategoryTPI = Manager.GetSubCatagories("TPI Intervention", qms055.BU, qms055.Location, null);
            qms055.TPIAgency1 = GetCategoryDescriptionFromList(lstCategory, qms055.TPIAgency1);
            qms055.TPIAgency2 = GetCategoryDescriptionFromList(lstCategory, qms055.TPIAgency2);
            qms055.TPIAgency3 = GetCategoryDescriptionFromList(lstCategory, qms055.TPIAgency3);
            qms055.TPIAgency4 = GetCategoryDescriptionFromList(lstCategory, qms055.TPIAgency4);
            qms055.TPIAgency5 = GetCategoryDescriptionFromList(lstCategory, qms055.TPIAgency5);
            qms055.TPIAgency6 = GetCategoryDescriptionFromList(lstCategory, qms055.TPIAgency6);
            qms055.TPIAgency1Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms055.TPIAgency1Intervention);
            qms055.TPIAgency2Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms055.TPIAgency2Intervention);
            qms055.TPIAgency3Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms055.TPIAgency3Intervention);
            qms055.TPIAgency4Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms055.TPIAgency4Intervention);
            qms055.TPIAgency5Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms055.TPIAgency5Intervention);
            qms055.TPIAgency6Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms055.TPIAgency6Intervention);
            var BUDescription = (from a in db.COM002
                                 where a.t_dtyp == 2 && a.t_dimx == qms055.BU
                                 select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
            qms055.BU = Convert.ToString(BUDescription.BUDesc);
            var locDescription = (from a in db.COM002
                                  where a.t_dtyp == 1 && a.t_dimx == qms055.Location
                                  select new { Location = a.t_dimx + "-" + a.t_desc }).FirstOrDefault();
            qms055.Location = locDescription.Location;


            if (Convert.ToBoolean(ViewBag.IsLTFPS))
            {
                int LTFPSHeaderId = Convert.ToInt32(qms055.LTFPSHeaderId);
                LTF002 objLTF002 = db.LTF002.Where(x => x.LineId == LTFPSHeaderId).FirstOrDefault();
                if (objLTF002 != null)
                {
                    if (!string.IsNullOrWhiteSpace(objLTF002.RefDocument) && (objLTF002.DocumentRequired))
                    {
                        ViewBag.IsRevAdd = true;
                    }
                    else
                    {
                        ViewBag.IsRevAdd = false;
                    }
                }
                else
                {
                    ViewBag.IsRevAdd = false;
                }
            }

            // qms055.ClearedQuantity = qms055.OfferedQuantity;
            return PartialView("_LoadPartlistTPIFormPartial", qms055);
        }

        [HttpPost]
        public ActionResult ReturnByTPI(QMS055 qms055)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS055 objQMS055 = db.QMS055.Where(x => x.RequestId == qms055.RequestId).FirstOrDefault();
                QMS036 objQMS036 = db.QMS036.Where(x =>
                                                        x.QualityProject == objQMS055.QualityProject &&
                                                        x.Project == objQMS055.Project &&
                                                        x.BU == objQMS055.BU &&
                                                        x.Location == objQMS055.Location &&
                                                        x.PartNo == objQMS055.PartNo &&
                                                        x.ParentPartNo == objQMS055.ParentPartNo &&
                                                        x.ChildPartNo == objQMS055.ChildPartNo &&
                                                        x.StageCode == objQMS055.StageCode).FirstOrDefault();
                if (objQMS055 != null)
                {
                    QMS045 objQMS045 = new QMS045();
                    LTF002 objLTF002 = new LTF002();
                    bool isLTFPS = Manager.IsLTFPSProject(objQMS055.Project, objQMS055.BU, objQMS055.Location);
                    if (isLTFPS)
                    {
                        objLTF002 = db.LTF002.Where(x => x.LineId == objQMS055.LTFPSHeaderId).FirstOrDefault();
                    }
                    else
                    {
                        objQMS045 = db.QMS045.Where(x => x.HeaderId == objQMS055.refHeaderId).FirstOrDefault();
                    }

                    updateTPIStatus(objQMS055, true);
                    db.SaveChanges();
                    bool flag = AddPendingQuantitiesDetails(objQMS055, objQMS036, clsImplementationEnum.PartlistOfferInspectionStatus.RETURNED.GetStringValue(), true);
                    objQMS055.EditedBy = objClsLoginInfo.UserName;
                    objQMS055.EditedOn = DateTime.Now;

                    if (isLTFPS)
                    {
                        objLTF002.EditedOn = DateTime.Now;
                        objLTF002.EditedBy = objClsLoginInfo.UserName;
                    }
                    else
                    {
                        objQMS045.EditedOn = DateTime.Now;
                        objQMS045.EditedBy = objClsLoginInfo.UserName;
                    }

                    db.SaveChanges();

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), objQMS055.Project, objQMS055.BU, objQMS055.Location, "Stage: " + objQMS055.StageCode + " of Part: " + objQMS055.PartNo + " & Project No: " + objQMS055.QualityProject + " has been Returned by TPI", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/PartlistOfferInspection/ViewOfferedDetails?RequestId=" + objQMS055.RequestId);
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Returned successfully.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Offer not available.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ClearByTPI(QMS055 qms055, string ltfpsRevNo = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string ApprovedStatus = clsImplementationEnum.ICLPartStatus.Approved.GetStringValue();
                string clearedStatus = clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue();
                bool callReadyToOffer = false;
                bool parentReadyToOffer = false;
                QMS055 objQMS055 = db.QMS055.Where(x => x.RequestId == qms055.RequestId).FirstOrDefault();
                if (objQMS055 != null)
                {
                    QMS045 objQMS045 = new QMS045();
                    LTF002 objLTF002 = new LTF002();

                    bool isLTFPS = Manager.IsLTFPSProject(objQMS055.Project, objQMS055.BU, objQMS055.Location);
                    if (isLTFPS)
                    {
                        objLTF002 = db.LTF002.Where(x => x.LineId == objQMS055.LTFPSHeaderId).FirstOrDefault();
                    }
                    else
                    {
                        objQMS045 = db.QMS045.Where(x => x.HeaderId == objQMS055.refHeaderId).FirstOrDefault();
                    }

                    updateTPIStatus(objQMS055);
                    db.SaveChanges();
                    if (!isLTFPS)
                    {
                        string InspectionStatus = GetInspectionStatus(objQMS045.HeaderId, (objQMS045.Quantity == null ? 0 : Convert.ToInt32(objQMS045.Quantity)), true);
                        if (!string.IsNullOrEmpty(InspectionStatus))
                        {
                            objQMS045.InspectionStatus = InspectionStatus;
                        }

                        if (!db.QMS045.Any(x => x.PartNo == objQMS045.PartNo && x.ParentPartNo == objQMS045.ParentPartNo && x.QualityProject == objQMS045.QualityProject && x.Location == objQMS045.Location && x.StageSequence == objQMS045.StageSequence && x.HeaderId != objQMS045.HeaderId && x.InspectionStatus != clearedStatus))
                        {
                            List<List<QMS045>> groups = db.QMS045.Where(x => x.PartNo == objQMS045.PartNo && x.ParentPartNo == objQMS045.ParentPartNo && x.QualityProject == objQMS045.QualityProject && x.Location == objQMS045.Location && x.StageSequence > objQMS045.StageSequence).OrderBy(x => x.StageSequence).GroupBy(c => c.StageSequence)
                               .Select(group => group.ToList())
                               .ToList();
                            if (groups != null && groups.Count > 0)
                            {
                                List<QMS045> lstQMS045 = groups.FirstOrDefault().ToList();
                                if (lstQMS045 != null && lstQMS045.Count > 0)
                                {
                                    lstQMS045.ForEach(x =>
                                        {
                                            x.InspectionStatus = x.InspectionStatus == null ? clsImplementationEnum.PartlistOfferInspectionStatus.READY_TO_OFFER.GetStringValue() : x.InspectionStatus;
                                            x.BalanceQuantity = x.InspectionStatus == null ? objQMS055.ClearedQuantity : (x.BalanceQuantity == null ? 0 : x.BalanceQuantity) + objQMS055.ClearedQuantity;
                                        }
                                    );

                                    lstQMS045.ForEach(x =>
                                    {
                                        x.InspectionStatus = (x.BalanceQuantity > 0 && x.InspectionStatus != clsImplementationEnum.PartlistOfferInspectionStatus.READY_TO_OFFER.GetStringValue()) ? clsImplementationEnum.PartlistOfferInspectionStatus.PARTIALLY_OFFERED.GetStringValue() : x.InspectionStatus;
                                    });

                                }
                            }
                        }

                        parentReadyToOffer = true;

                    }
                    else
                    {
                        string InspectionStatus = GetLTFPSInspectionStatus(objLTF002.LineId, 1, true);
                        if (!string.IsNullOrEmpty(InspectionStatus))
                        {
                            objLTF002.InspectionStatus = InspectionStatus;
                        }
                        if (!string.IsNullOrWhiteSpace(ltfpsRevNo))
                        {
                            objLTF002.DocRevNo = ltfpsRevNo;
                        }
                        callReadyToOffer = true;
                        //db.SP_LTFPS_RELEASE_READY_TO_OFFER(objLTF002.HeaderId).FirstOrDefault();
                        //List<List<LTF002>> groups = db.LTF002.Where(x => x.PartNo == objLTF002.PartNo && x.QualityProject == objLTF002.QualityProject && x.Location == objLTF002.Location && x.OperationNo > objLTF002.OperationNo).OrderBy(x => x.OperationNo).GroupBy(c => c.OperationNo)
                        //       .Select(group => group.ToList())
                        //       .ToList();
                        //if (groups != null && groups.Count > 0)
                        //{
                        //    List<LTF002> lstLTF002 = groups.FirstOrDefault().ToList();
                        //    if (lstLTF002 != null && lstLTF002.Count > 0)
                        //    {
                        //        lstLTF002.ForEach(x =>
                        //        {
                        //            x.InspectionStatus = x.InspectionStatus == null ? clsImplementationEnum.PartlistOfferInspectionStatus.READY_TO_OFFER.GetStringValue() : x.InspectionStatus;
                        //            // x.BalanceQuantity = x.InspectionStatus == null ? objQMS055.ClearedQuantity : (x.BalanceQuantity == null ? 0 : x.BalanceQuantity) + objQMS055.ClearedQuantity;
                        //        }
                        //        );
                        //    }
                        //}
                    }

                    // db.SaveChanges();
                    objQMS055.EditedBy = objClsLoginInfo.UserName;
                    objQMS055.EditedOn = DateTime.Now;
                    if (!isLTFPS)
                    {
                        objQMS045.EditedOn = DateTime.Now;
                        objQMS045.EditedBy = objClsLoginInfo.UserName;
                    }
                    else
                    {
                        objLTF002.EditedOn = DateTime.Now;
                        objLTF002.EditedBy = objClsLoginInfo.UserName;
                    }
                    db.SaveChanges();

                    if (callReadyToOffer)
                    {
                        db.SP_LTFPS_RELEASE_READY_TO_OFFER(objLTF002.HeaderId).FirstOrDefault();
                    }

                    //if (parentReadyToOffer)
                    //{

                    //    string specialStage = Convert.ToString(Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.SpecialStageCodeForRTO.GetStringValue()));
                    //    if (objQMS045.InspectionStatus == clearedStatus && objQMS045.StageCode.ToUpper() == specialStage.ToUpper())
                    //    {
                    //        List<QMS012> SeamList = db.QMS012.Where(c => c.AssemblyNo == objQMS045.ChildPartNo && c.QualityProject == objQMS045.QualityProject && c.Project == objQMS045.Project).ToList();
                    //        foreach (var seam in SeamList)
                    //        {
                    //            QMS030 objQMS030 = db.QMS030.FirstOrDefault(c => c.Project == objQMS045.Project && c.QualityProject == objQMS045.QualityProject && c.SeamNo == seam.SeamNo && c.Status == ApprovedStatus);
                    //            if (objQMS030 != null)
                    //            {
                    //                Manager.ReadyToOffer_SEAM(objQMS030, true);
                    //            }
                    //        }
                    //    }

                    #region Code for make parent part ready to offer 

                    //if (Manager.IsPARTFullyCleared(objQMS045.Project, objQMS045.QualityProject, objQMS045.BU, objQMS045.Location, objQMS045.PartNo))
                    //{
                    //    // get seams which are associated with this parts and ready to offer
                    //    List<string> SeamList = db.QMS012_Log.Where(c => c.QualityProject == objQMS045.QualityProject && c.Project == objQMS045.Project && c.Status == ApprovedStatus).ToList().Where(w => w.Position.Split(',').Contains(objQMS045.PartNo)).Select(s => s.SeamNo).Distinct().ToList();
                    //    foreach (var seam in SeamList)
                    //    {
                    //        //QMS012_Log objQMS012 = db.QMS012_Log.FirstOrDefault(c => c.Project == objQMS045.Project && c.QualityProject == objQMS045.QualityProject && c.SeamNo == seam && c.Status == ApprovedStatus);
                    //        //if (Manager.IsChildPartCleared(objQMS045.Project, objQMS045.QualityProject, objQMS045.BU, objQMS045.Location, objQMS012.AssemblyNo))
                    //        //{
                    //        QMS030 objQMS030 = db.QMS030.FirstOrDefault(c => c.Project == objQMS045.Project && c.QualityProject == objQMS045.QualityProject && c.SeamNo == seam && c.Status == ApprovedStatus);
                    //        if (objQMS030 != null)
                    //        {
                    //            Manager.ReadyToOffer_SEAM(objQMS030);
                    //        }
                    //        //}
                    //    }

                    //    QMS035 objQMS035 = db.QMS035.FirstOrDefault(c => c.Project == objQMS045.Project && c.QualityProject == objQMS045.QualityProject && c.BU == objQMS045.BU && c.Location == objQMS045.Location && c.ChildPartNo == objQMS045.ParentPartNo && c.Status == ApprovedStatus);
                    //    if (objQMS035 != null)
                    //    {
                    //        Manager.ReadyToOffer_PART(objQMS035);
                    //    }
                    //    else
                    //    {
                    //        List<QMS012> lstQMS012 = db.QMS012.Where(c => c.Project == objQMS045.Project && c.QualityProject == objQMS045.QualityProject && c.AssemblyNo == objQMS045.ParentPartNo).ToList();
                    //        foreach (var objQMS012 in lstQMS012)
                    //        {
                    //            //if (Manager.IsChildPartCleared(objQMS045.Project, objQMS045.QualityProject, objQMS045.BU, objQMS045.Location, objQMS012.AssemblyNo))
                    //            //{
                    //            QMS030 objQMS030 = db.QMS030.FirstOrDefault(c => c.Project == objQMS045.Project && c.QualityProject == objQMS045.QualityProject && c.SeamNo == objQMS012.SeamNo && c.Status == ApprovedStatus);
                    //            if (objQMS030 != null)
                    //            {
                    //                Manager.ReadyToOffer_SEAM(objQMS030);
                    //            }
                    //            //}
                    //        }
                    //    }

                    //}

                    #endregion
                    //}

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.QC3.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PROD3.GetStringValue(), objQMS055.Project, objQMS055.BU, objQMS055.Location, "Stage: " + objQMS055.StageCode + " of Part: " + objQMS055.PartNo + " & Project No: " + objQMS055.QualityProject + " has been Cleared by TPI", clsImplementationEnum.NotificationType.Information.GetStringValue());
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Cleared successfully.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Offer not available.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public bool AddPendingQuantitiesDetails(QMS055 qms055, QMS036 objQMS036, string Status, bool isTPI = false, string APIRequestFrom = "", string UserName = "")
        {
            bool flag = true;
            int IterationNo = 0;
            int ReqSeqNo = 0;
            int OfferedQuantity = 0;
            string protocolResult = string.Empty;
            if (Status == clsImplementationEnum.PartlistOfferInspectionStatus.REJECTED.GetStringValue())
            {
                protocolResult = Status.ToUpper();
                IterationNo = Convert.ToInt32(qms055.IterationNo + 1);
                ReqSeqNo = Convert.ToInt32(qms055.RequestNoSequence);
                OfferedQuantity = Convert.ToInt32(qms055.RejectedQuantity);
                if (qms055.refHeaderId > 0)
                {
                    string OldfolderPath = "QMS055//" + qms055.refHeaderId.Value + "//" + qms055.IterationNo + "//" + ReqSeqNo;
                    string NewfolderPath = "QMS055//" + qms055.refHeaderId.Value + "//" + IterationNo + "//" + ReqSeqNo;
                    Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                    _objFUC.CopyDataOnFCSServerAsync(OldfolderPath, NewfolderPath, OldfolderPath, qms055.refHeaderId.Value, NewfolderPath, qms055.refHeaderId.Value, DESServices.CommonService.GetUseIPConfig, DESServices.CommonService.objClsLoginInfo.UserName, 0, false);
                    //(new clsFileUpload()).CopyFolderContentsAsync(OldfolderPath, NewfolderPath);
                }
                else
                {
                    string OldfolderPath = "QMS055_LTFPS//" + qms055.LTFPSHeaderId.Value + "//" + qms055.IterationNo + "//" + ReqSeqNo;
                    string NewfolderPath = "QMS055_LTFPS//" + qms055.LTFPSHeaderId.Value + "//" + IterationNo + "//" + ReqSeqNo;

                    Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                    _objFUC.CopyDataOnFCSServerAsync(OldfolderPath, NewfolderPath, OldfolderPath, qms055.LTFPSHeaderId.Value, NewfolderPath, qms055.LTFPSHeaderId.Value, DESServices.CommonService.GetUseIPConfig, DESServices.CommonService.objClsLoginInfo.UserName, 0, false);

                    //(new clsFileUpload()).CopyFolderContentsAsync(OldfolderPath, NewfolderPath);
                }
            }
            else if (Status == clsImplementationEnum.PartlistOfferInspectionStatus.RETURNED.GetStringValue())
            {
                IterationNo = Convert.ToInt32(qms055.IterationNo);
                ReqSeqNo = Convert.ToInt32(qms055.RequestNoSequence + 1);
                OfferedQuantity = Convert.ToInt32(qms055.OfferedQuantity);
                if (qms055.refHeaderId > 0)
                {
                    string OldfolderPath = "QMS055//" + qms055.refHeaderId.Value + "//" + IterationNo + "//" + qms055.RequestNoSequence;
                    string NewfolderPath = "QMS055//" + qms055.refHeaderId.Value + "//" + IterationNo + "//" + ReqSeqNo;

                    Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                    _objFUC.CopyDataOnFCSServerAsync(OldfolderPath, NewfolderPath, OldfolderPath, qms055.refHeaderId.Value, NewfolderPath, qms055.refHeaderId.Value, DESServices.CommonService.GetUseIPConfig, DESServices.CommonService.objClsLoginInfo.UserName, 0, false);

                    //(new clsFileUpload()).CopyFolderContentsAsync(OldfolderPath, NewfolderPath);
                }
                else
                {
                    string OldfolderPath = "QMS055_LTFPS//" + qms055.LTFPSHeaderId.Value + "//" + IterationNo + "//" + qms055.RequestNoSequence;
                    string NewfolderPath = "QMS055_LTFPS//" + qms055.LTFPSHeaderId.Value + "//" + IterationNo + "//" + ReqSeqNo;

                    Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                    _objFUC.CopyDataOnFCSServerAsync(OldfolderPath, NewfolderPath, OldfolderPath, qms055.LTFPSHeaderId.Value, NewfolderPath, qms055.LTFPSHeaderId.Value, DESServices.CommonService.GetUseIPConfig, DESServices.CommonService.objClsLoginInfo.UserName, 0, false);

                    //(new clsFileUpload()).CopyFolderContentsAsync(OldfolderPath, NewfolderPath);
                }
            }

            QMS055 objQMS055 = db.QMS055.Add(new QMS055
            {

                QualityProject = qms055.QualityProject,
                Project = qms055.Project,
                BU = qms055.BU,
                Location = qms055.Location,
                PartNo = qms055.PartNo,
                ParentPartNo = qms055.ParentPartNo,
                ChildPartNo = qms055.ChildPartNo,
                StageCode = qms055.StageCode,
                StageSequence = qms055.StageSequence,
                IterationNo = IterationNo,
                RequestNo = qms055.RequestNo,
                RequestNoSequence = ReqSeqNo,
                OfferedQuantity = OfferedQuantity,//Make calculation
                ShopLocation = qms055.ShopLocation,
                ShopRemark = qms055.ShopRemark,
                Shop = qms055.Shop,
                OfferedBy = isTPI ? qms055.OfferedBy : null,
                OfferedOn = isTPI ? qms055.OfferedOn : null,
                OfferedtoCustomerBy = null,
                OfferedtoCustomerOn = null,
                CreatedBy = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName,
                CreatedOn = DateTime.Now,
                refHeaderId = qms055.refHeaderId,
                TestResult = isTPI ? "Returned by TPI" : null,
                TPIAgency1 = objQMS036 != null ? objQMS036.FirstTPIAgency : null,
                TPIAgency1Intervention = objQMS036 != null ? objQMS036.FirstTPIIntervention : null,
                TPIAgency2 = objQMS036 != null ? objQMS036.SecondTPIAgency : null,
                TPIAgency2Intervention = objQMS036 != null ? objQMS036.SecondTPIIntervention : null,
                TPIAgency3 = objQMS036 != null ? objQMS036.ThirdTPIAgency : null,
                TPIAgency3Intervention = objQMS036 != null ? objQMS036.ThirdTPIIntervention : null,
                TPIAgency4 = objQMS036 != null ? objQMS036.FourthTPIAgency : null,
                TPIAgency4Intervention = objQMS036 != null ? objQMS036.FourthTPIIntervention : null,
                TPIAgency5 = objQMS036 != null ? objQMS036.FifthTPIAgency : null,
                TPIAgency5Intervention = objQMS036 != null ? objQMS036.FifthTPIIntervention : null,
                TPIAgency6 = objQMS036 != null ? objQMS036.SixthTPIAgency : null,
                TPIAgency6Intervention = objQMS036 != null ? objQMS036.SixthTPIIntervention : null,
                LTFPSHeaderId = qms055.LTFPSHeaderId,
            });
            db.SaveChanges();

            #region Protocol
            int oldReqId = qms055.RequestId;
            int newReqId = objQMS055.RequestId;
            if (qms055.ProtocolId.HasValue && !string.IsNullOrWhiteSpace(qms055.ProtocolType))
            {
                string PRLTableName = Manager.GetLinkedProtocolTableName(qms055.ProtocolType);
                if (!string.IsNullOrWhiteSpace(PRLTableName))
                {
                    if (Convert.ToInt32((qms055.ProtocolType).Replace("PROTOCOL", "")) > 36)
                    {
                        db.Database.ExecuteSqlCommand("UPDATE " + PRLTableName + " SET InspectedBy='" + (APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName) + "',InspectedOn=GETDATE(),InspectionDate=GETDATE(),Result='" + protocolResult + "' WHERE HeaderId = " + qms055.ProtocolId);
                    }
                    else
                    {
                        db.Database.ExecuteSqlCommand("UPDATE " + PRLTableName + " SET InspectionDate=GETDATE() WHERE HeaderId = " + qms055.ProtocolId);
                    }
                    ObjectParameter newProtocolId = new ObjectParameter("pNewProtocolId", typeof(int?));
                    db.SP_COMMON_GENERATE_DUPLICATE_ROW_IN_SAME_TABLE("dbo", PRLTableName, "HeaderId=" + qms055.ProtocolId, "HeaderId,InspectionDate,OfferDate,ProtocolType,ProtocolId", newProtocolId, qms055.ProtocolType, qms055.OfferedBy);
                    objQMS055.ProtocolId = Convert.ToInt32(newProtocolId.Value);
                    objQMS055.ProtocolType = qms055.ProtocolType;
                    db.SaveChanges();

                    db.Database.ExecuteSqlCommand("UPDATE " + PRLTableName + " SET IterationNo=" + objQMS055.IterationNo + ", RequestNo=" + objQMS055.RequestNo + ", RequestNoSequence=" + objQMS055.RequestNoSequence + " WHERE HeaderId = " + newProtocolId.Value);


                }
            }
            //SELECT @pProtocolId = ProtocolId, @pProtocolType = ProtocolType, @pCreated = OfferedBy FROM QMS050 WHERE RequestId = @RequestId

            //SELECT @pProtocolIterationNo = IterationNo, @pProtocolRequestNo = RequestNo, @pProtocolRequestNoSequence = RequestNoSequence FROM QMS050 WHERE RequestId = @ID
            #endregion
            return flag;
        }

        public string GetInspectionStatus(int refHeaderID, int totalQty, bool IsTPI = false)
        {
            string strInspectionStatus = string.Empty;
            int sumClearedQuantities = 0;
            if (IsTPI)
            {
                List<QMS055> lstQMS055 = db.QMS055.Where(x => x.refHeaderId == refHeaderID).ToList();
                if (lstQMS055 != null && lstQMS055.Count > 0)
                {
                    bool Flag = false;
                    foreach (var qms055 in lstQMS055)
                    {

                        if (
                                qms055.TPIAgency1Result == clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue() ||
                                qms055.TPIAgency2Result == clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue() ||
                                qms055.TPIAgency3Result == clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue() ||
                                qms055.TPIAgency4Result == clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue() ||
                                qms055.TPIAgency5Result == clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue() ||
                                qms055.TPIAgency6Result == clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue()
                            )
                        {
                            Flag = true;
                        }
                        else
                        {
                            Flag = false;
                            break;
                        }

                    }
                    if (Flag)
                    {
                        sumClearedQuantities = Convert.ToInt32(lstQMS055.Sum(x => x.ClearedQuantity));
                        if (totalQty == sumClearedQuantities)
                        {
                            strInspectionStatus = clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue();
                        }
                        //else
                        //{
                        //    strInspectionStatus = clsImplementationEnum.PartlistOfferInspectionStatus.PARTIALLY_OFFERED.GetStringValue();
                        //}
                    }
                }
            }
            else
            {
                List<QMS055> lstQMS055 = db.QMS055.Where(x => x.refHeaderId == refHeaderID).ToList();
                if (lstQMS055 != null && lstQMS055.Count > 0)
                {
                    sumClearedQuantities = Convert.ToInt32(lstQMS055.Sum(x => x.ClearedQuantity));
                    if (totalQty == sumClearedQuantities)
                    {
                        strInspectionStatus = clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue();
                    }
                }
            }
            return strInspectionStatus;
        }

        public string GetLTFPSInspectionStatus(int refLTFPSID, int totalQty, bool IsTPI = false)
        {
            string strInspectionStatus = string.Empty;
            int sumClearedQuantities = 0;
            if (IsTPI)
            {
                List<QMS055> lstQMS055 = db.QMS055.Where(x => x.LTFPSHeaderId == refLTFPSID).ToList();
                if (lstQMS055 != null && lstQMS055.Count > 0)
                {
                    bool Flag = false;
                    foreach (var qms055 in lstQMS055)
                    {

                        if (
                                qms055.TPIAgency1Result == clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue() ||
                                qms055.TPIAgency2Result == clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue() ||
                                qms055.TPIAgency3Result == clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue() ||
                                qms055.TPIAgency4Result == clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue() ||
                                qms055.TPIAgency5Result == clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue() ||
                                qms055.TPIAgency6Result == clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue()
                            )
                        {
                            Flag = true;
                        }
                        else
                        {
                            Flag = false;
                            break;
                        }

                    }
                    if (Flag)
                    {
                        sumClearedQuantities = Convert.ToInt32(lstQMS055.Sum(x => x.ClearedQuantity));
                        if (totalQty == sumClearedQuantities)
                        {
                            strInspectionStatus = clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue();
                        }
                        //else
                        //{
                        //    strInspectionStatus = clsImplementationEnum.PartlistOfferInspectionStatus.PARTIALLY_OFFERED.GetStringValue();
                        //}
                    }
                }
            }
            else
            {
                List<QMS055> lstQMS055 = db.QMS055.Where(x => x.LTFPSHeaderId == refLTFPSID).ToList();
                if (lstQMS055 != null && lstQMS055.Count > 0)
                {
                    sumClearedQuantities = Convert.ToInt32(lstQMS055.Sum(x => x.ClearedQuantity));
                    if (totalQty == sumClearedQuantities)
                    {
                        strInspectionStatus = clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue();
                    }
                }
            }
            return strInspectionStatus;
        }

        [HttpPost]
        public ActionResult LoadPartlistFilteredDataPartial(FilterParaModel filterparamodel, string isFromSeam = "")
        {
            if (!string.IsNullOrWhiteSpace(isFromSeam) && isFromSeam.ToLower() == "true")
            {
                return PartialView("_LoadSeamListFilteredDataPartial", filterparamodel);
            }
            else
            {
                QMS045 objQMS045 = new QMS045();
                if (!string.IsNullOrWhiteSpace(filterparamodel.ParentPartNo))
                {
                    objQMS045 = db.QMS045.Where(x => x.PartNo == filterparamodel.Number && x.ParentPartNo == filterparamodel.ParentPartNo && x.QualityProject == filterparamodel.QualityProject && x.Project == filterparamodel.Project && filterparamodel.BU == filterparamodel.BU && x.Location == filterparamodel.Location).FirstOrDefault();
                }
                else
                {
                    objQMS045 = db.QMS045.Where(x => x.PartNo == filterparamodel.Number && x.QualityProject == filterparamodel.QualityProject && x.Project == filterparamodel.Project && filterparamodel.BU == filterparamodel.BU && x.Location == filterparamodel.Location).FirstOrDefault();
                }
                if (objQMS045 != null)
                {
                    filterparamodel.ParentPartNo = objQMS045.ParentPartNo;
                    filterparamodel.PartDescription = Manager.GET_PART_DESCRIPTION_FROM_HBOM(objQMS045.Project, objQMS045.ChildPartNo);
                    filterparamodel.ParentPartDescription = Manager.GET_PART_DESCRIPTION_FROM_HBOM(objQMS045.Project, objQMS045.ParentPartNo);
                }
                else
                {
                    return null;
                }
                return PartialView("_LoadPartlistFilteredDataPartial", filterparamodel);
            }

        }

        [HttpPost]
        public JsonResult LoadFilteredPartlistData(JQueryDataTableParamModel param, string qualityProject, string project, string bu, string location, string partNo)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;

                strWhere = "1=1";
                strWhere += string.Format(" and QualityProject in('{0}') and qms045.Project in('{1}') and qms045.BU in('{2}') and qms045.Location in('{3}') and PartNo in('{4}')", qualityProject, project, bu, location, partNo);

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "InspectionStatus",
                                                "[L&TInspectionResult]",
                                                 "(dbo.GET_IPI_STAGECODE_DESCRIPTION(qms055.StageCode,ltrim(rtrim(qms055.BU)),ltrim(rtrim(qms055.Location))))",
                                                "StageSequence",
                                                //"IterationNo",
                                                "OfferedQuantity",
                                                "BalanceQuantity"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                // strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms045.BU", "qms045.Location");
                var lstResult = db.SP_FETCH_PARTLISTOFFERINSPECTION_HEADERS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.StageCodeDesc),
                               Convert.ToString(uc.StageSequence),
                               Convert.ToString(uc.IterationNo),
                               Convert.ToString(uc.InspectionStatus),
                               Convert.ToString(uc.Quantity),
                               Convert.ToString(uc.BalanceQuantity),
                               Convert.ToString(uc.LNTInspectorResult),
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.isNDEStage),
                               Convert.ToString(uc.StageCode),
                               Convert.ToString(uc.StageSequence)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = 0,
                    iTotalDisplayRecords = 0,
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult LoadFilteredDataPartial(RequestFilterParaModel requestfilterparamodel, int tableID = 0)
        {
            requestfilterparamodel.StageCode = requestfilterparamodel.StageCode.Split('-')[0].Trim();
            string StageDesc = db.QMS002.Where(x => x.StageCode == requestfilterparamodel.StageCode).Select(x => x.StageDesc).FirstOrDefault();
            string strStageDesc = requestfilterparamodel.StageCode;
            if (!string.IsNullOrWhiteSpace(StageDesc))
                strStageDesc += " - " + StageDesc;
            requestfilterparamodel.StageDesc = strStageDesc;

            if (tableID == 55)
            {
                return PartialView("_Load55FilteredDataPartial", requestfilterparamodel);
            }
            else if (tableID == 65)
            {
                if (string.IsNullOrWhiteSpace(requestfilterparamodel.ParentPartNo))
                {
                    QMS045 objQMS045 = db.QMS045.Where(x => x.PartNo == requestfilterparamodel.Number && x.ParentPartNo == requestfilterparamodel.ParentPartNo && x.QualityProject == requestfilterparamodel.QualityProject && x.Project == requestfilterparamodel.Project && requestfilterparamodel.BU == requestfilterparamodel.BU && x.Location == requestfilterparamodel.Location).FirstOrDefault();
                    requestfilterparamodel.ParentPartNo = objQMS045.ParentPartNo;
                }
                return PartialView("_Load65FilteredDataPartial", requestfilterparamodel);
            }
            else if (tableID == 50)
            {
                return PartialView("_Load50FilteredDataPartial", requestfilterparamodel);
            }
            else
            {
                return PartialView("_Load60FilteredDataPartial", requestfilterparamodel);
            }
        }

        [HttpPost]
        public JsonResult LoadFilteredReqData(JQueryDataTableParamModel param, string qualityProject, string project, string bu, string location, string partNo, string stageCode, int stageSeq)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[0]) + "," + Convert.ToString(Request["sColumns"].Split(',')[1]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                strWhere = "1=1";
                strWhere += string.Format(" and QualityProject in('{0}') and qms055.Project in('{1}') and qms055.BU in('{2}') and qms055.Location in('{3}') and qms055.PartNo in('{4}') and qms055.StageCode in('{5}') and qms055.StageSequence in({6})", qualityProject, project, bu, location, partNo, stageCode, stageSeq);



                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "qms055.Project+' - '+com1.t_dsca",
                                                "QualityProject",
                                                "TestResult",
                                                "Shop",
                                                "ShopLocation",
                                                "ShopRemark"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_FETCH_PARTLISTTESTDETAILS_HEADERS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.RequestNo),
                               Convert.ToString(uc.RequestNoSequence),
                               Convert.ToString(uc.IterationNo),
                               Convert.ToString(uc.TestResult),
                               Convert.ToString(uc.OfferedQuantity),
                               Convert.ToString(uc.ClearedQuantity),
                               Convert.ToString(uc.RejectedQuantity),
                               Convert.ToString(uc.Shop),
                               Convert.ToString(uc.ShopLocation),
                               Convert.ToString(uc.ShopRemark),
                               Convert.ToString(uc.OfferedBy),
                               Convert.ToString(uc.OfferedOn),
                               Convert.ToString(uc.InspectedBy),
                               Convert.ToString(uc.InspectedOn),
                               Convert.ToString(uc.QCRemarks),
                               Convert.ToString(uc.OfferedtoCustomerBy),
                               Convert.ToString(uc.OfferedtoCustomerOn),
                               Convert.ToString(uc.TPIAgency1),
                               Convert.ToString(uc.TPIAgency1Intervention),
                               Convert.ToString(uc.TPIAgency1Result),
                               Convert.ToString(uc.TPIAgency1ResultOn),
                               Convert.ToString(uc.TPIAgency2),
                               Convert.ToString(uc.TPIAgency2Intervention),
                               Convert.ToString(uc.TPIAgency2Result),
                               Convert.ToString(uc.TPIAgency2ResultOn),
                               Convert.ToString(uc.TPIAgency3),
                               Convert.ToString(uc.TPIAgency3Intervention),
                               Convert.ToString(uc.TPIAgency3Result),
                               Convert.ToString(uc.TPIAgency3ResultOn),
                               Convert.ToString(uc.TPIAgency4),
                               Convert.ToString(uc.TPIAgency4Intervention),
                               Convert.ToString(uc.TPIAgency4Result),
                               Convert.ToString(uc.TPIAgency4ResultOn),
                               Convert.ToString(uc.TPIAgency5),
                               Convert.ToString(uc.TPIAgency5Intervention),
                               Convert.ToString(uc.TPIAgency5Result),
                               Convert.ToString(uc.TPIAgency5ResultOn),
                               Convert.ToString(uc.TPIAgency6),
                               Convert.ToString(uc.TPIAgency6Intervention),
                               Convert.ToString(uc.TPIAgency6Result),
                               Convert.ToString(uc.TPIAgency6ResultOn),
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = 0,
                    iTotalDisplayRecords = 0,
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadFilteredNDEReqData(JQueryDataTableParamModel param, string qualityProject, string project, string bu, string location, string partNo, string stageCode, int stageSeq)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[0]) + "," + Convert.ToString(Request["sColumns"].Split(',')[1]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                strWhere = "1=1";
                strWhere += string.Format(" and QualityProject in('{0}') and qms065.Project in('{1}') and qms065.BU in('{2}') and qms065.Location in('{3}') and qms065.PartAssemblyFindNumber in('{4}') and qms065.StageCode in('{5}') and qms065.StageSequence in({6})", qualityProject, project, bu, location, partNo, stageCode, stageSeq);


                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "qms065.Project+' - '+com1.t_dsca",
                                                "QualityProject",
                                                "Shop",
                                                "ShopLocation",
                                                "ShopRemark"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_FETCH_IPI_NDE_PARTLIST_OFFERED_RESULT
                                (
                                StartIndex, EndIndex, "", strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.RequestNo),
                               Convert.ToString(uc.RequestNoSequence),
                               Convert.ToString(uc.IterationNo),
                               Convert.ToString(uc.RequestStatus),
                               Convert.ToString(uc.TestResult),
                               Convert.ToString(uc.ManufacturingCode),
                               Convert.ToString(uc.Shop),
                               Convert.ToString(uc.ShopLocation),
                               Convert.ToString(uc.ShopRemark),
                               Convert.ToString(uc.NDETechniqueNo),
                               Convert.ToString(uc.NDETechniqueRevisionNo),
                               Convert.ToString(uc.OfferedQuantity),
                               Convert.ToString(uc.ProductionDrawingNo),
                               Convert.ToString(uc.ReturnRemark),
                               Convert.ToString(uc.TPIAgency1),
                               Convert.ToString(uc.TPIAgency1Intervention),
                               Convert.ToString(uc.TPIAgency2),
                               Convert.ToString(uc.TPIAgency2Intervention),
                               Convert.ToString(uc.TPIAgency3),
                               Convert.ToString(uc.TPIAgency3Intervention),
                               Convert.ToString(uc.TPIAgency4),
                               Convert.ToString(uc.TPIAgency4Intervention),
                               Convert.ToString(uc.TPIAgency5),
                               Convert.ToString(uc.TPIAgency5Intervention),
                               Convert.ToString(uc.TPIAgency6),
                               Convert.ToString(uc.TPIAgency6Intervention),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn),
                               Convert.ToString(uc.RequestGeneratedBy),
                               Convert.ToString(uc.RequestGeneratedOn),
                               Convert.ToString(uc.ConfirmedBy),
                               Convert.ToString(uc.ConfirmedOn),
                               Convert.ToString(uc.OfferedBy),
                               Convert.ToString(uc.OfferedOn),
                               Convert.ToString(uc.ReturnedBy),
                               Convert.ToString(uc.ReturnedOn),
                               Convert.ToString(uc.RejectedBy),
                               Convert.ToString(uc.RejectedOn)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = 0,
                    iTotalDisplayRecords = 0,
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_FETCH_PARTLISTOFFERINSPECTION_HEADERS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      PartNo = li.PartNo,
                                      ParentPartNo = li.ParentPartNo,
                                      ChildPartNo = li.ChildPartNo,
                                      StageCode = li.StageCode,
                                      StageSequence = li.StageSequence,
                                      IterationNo = li.IterationNo,
                                      Quantity = li.Quantity,
                                      BalanceQuantity = li.BalanceQuantity,
                                      InspectionStatus = li.InspectionStatus,
                                      LNTInspectorResult = li.LNTInspectorResult,
                                      Remarks = li.Remarks,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      ProductionDrawingNo = li.ProductionDrawingNo,
                                      StageCodeDesc = li.StageCodeDesc,
                                      isNDEStage = li.isNDEStage
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else
                {
                    var lst = db.SP_FETCH_PARTLISTTESTDETAILS_HEADERS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      PartNo = li.PartNo,
                                      ParentPartNo = li.ParentPartNo,
                                      ChildPartNo = li.ChildPartNo,
                                      StageCode = li.StageCode,
                                      StageSequence = li.StageSequence,
                                      IterationNo = li.IterationNo,
                                      RequestNo = li.RequestNo,
                                      RequestNoSequence = li.RequestNoSequence,
                                      OfferedQuantity = li.OfferedQuantity,
                                      ClearedQuantity = li.ClearedQuantity,
                                      RejectedQuantity = li.RejectedQuantity,
                                      TestResult = li.TestResult,
                                      InspectedBy = li.InspectedBy,
                                      InspectedOn = li.InspectedOn,
                                      QCRemarks = li.QCRemarks,
                                      OfferedtoCustomerBy = li.OfferedtoCustomerBy,
                                      OfferedtoCustomerOn = li.OfferedtoCustomerOn,
                                      TPIAgency1 = li.TPIAgency1,
                                      TPIAgency1Intervention = li.TPIAgency1Intervention,
                                      TPIAgency1Result = li.TPIAgency1Result,
                                      TPIAgency1ResultOn = li.TPIAgency1ResultOn,
                                      TPIAgency2 = li.TPIAgency2,
                                      TPIAgency2Intervention = li.TPIAgency2Intervention,
                                      TPIAgency2Result = li.TPIAgency2Result,
                                      TPIAgency2ResultOn = li.TPIAgency2ResultOn,
                                      TPIAgency3 = li.TPIAgency3,
                                      TPIAgency3Intervention = li.TPIAgency3Intervention,
                                      TPIAgency3Result = li.TPIAgency3Result,
                                      TPIAgency3ResultOn = li.TPIAgency3ResultOn,
                                      TPIAgency4 = li.TPIAgency4,
                                      TPIAgency4Intervention = li.TPIAgency4Intervention,
                                      TPIAgency4Result = li.TPIAgency4Result,
                                      TPIAgency4ResultOn = li.TPIAgency4ResultOn,
                                      TPIAgency5 = li.TPIAgency5,
                                      TPIAgency5Intervention = li.TPIAgency5Intervention,
                                      TPIAgency5Result = li.TPIAgency5Result,
                                      TPIAgency5ResultOn = li.TPIAgency5ResultOn,
                                      TPIAgency6 = li.TPIAgency6,
                                      TPIAgency6Intervention = li.TPIAgency6Intervention,
                                      TPIAgency6Result = li.TPIAgency6Result,
                                      TPIAgency6ResultOn = li.TPIAgency6ResultOn,
                                      Shop = li.Shop,
                                      ShopLocation = li.ShopLocation,
                                      ShopRemark = li.ShopRemark,
                                      OfferedBy = li.OfferedBy,
                                      OfferedOn = li.OfferedOn,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      RequestId = li.RequestId,
                                      refHeaderId = li.refHeaderId,
                                      StageCodeDesc = li.StageCodeDesc
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);

                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        public string GetPreviousRequestRemark(long? requestNo, int requestId)
        {
            string strCleared = clsImplementationEnum.SeamListInspectionStatus.Cleared.GetStringValue();
            return db.QMS055.Where(x => (x.RequestNo == requestNo && x.RequestId < requestId) && x.TestResult != strCleared).OrderByDescending(x => x.RequestId).Select(x => x.QCRemarks).FirstOrDefault();
        }

        #region Pending inspection Report UI
        public ActionResult GetPendingInspectionReportPartial(string InspectionType, string TestType)
        {
            ViewBag.UserName = objClsLoginInfo.UserName.Trim();
            ViewBag.LocationCode = objClsLoginInfo.Location;
            ViewBag.Location = db.COM002.Where(x => x.t_dimx == objClsLoginInfo.Location).Select(x => x.t_desc).FirstOrDefault();

            var lstSeamApprovalStatus = new List<SelectListItem>()
                                            { new SelectListItem { Text = "Select", Value = "" },
                                              new SelectListItem { Text = "Attended", Value = "Attended" },
                                              new SelectListItem { Text = "Non-Attended", Value = "Not-Attended" },
                                            };

            ViewBag.SeamApprovalStatus = lstSeamApprovalStatus;

            List<CategoryData> lstInspectionStatus = new List<CategoryData>()
                                            { new CategoryData { Value = "Cleared", CategoryDescription = "Cleared" },
                                              new CategoryData { Value = "Returned", CategoryDescription = "Returned" },
                                              new CategoryData { Value = "Rejected", CategoryDescription = "Rejected" }
                                            };

            ViewBag.TestType = TestType;
            ViewBag.InspectionType = InspectionType;

            List<CategoryData> lstStageType = Manager.GetSubCatagorywithoutBULocation("Stage Type").ToList();
            ViewBag.InspectionStatus = lstInspectionStatus.AsEnumerable().Select(x => new { CatID = x.Value, CatDesc = x.CategoryDescription, id = x.Value, text = x.CategoryDescription }).OrderBy(x => x.CatID).ToList();

            ViewBag.StageType = lstStageType.AsEnumerable().Select(x => new { CatID = x.Value, CatDesc = x.CategoryDescription, id = x.Value, text = x.CategoryDescription }).OrderBy(x => x.CatID).ToList();

            return PartialView("_GetPendingInspectionReportPartial");
        }

        [HttpPost]
        public ActionResult GetQualityProjectsV2(string term, string location = "")
        {
            string username = objClsLoginInfo.UserName;
            var lstATHLocation = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.Location).Distinct().ToList();
            var lstCOMLocation = db.COM003.Where(i => i.t_psno.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_loca).Distinct().ToList();

            var lstCOMBU = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.BU).Distinct().ToList();

            lstCOMLocation = lstATHLocation.Union(lstCOMLocation).ToList();

            List<ddlValue> lstQualityProject = new List<ddlValue>();
            if (!string.IsNullOrEmpty(term))
            {
                lstQualityProject = db.QMS010.Where(i => i.QualityProject.ToLower().Contains(term.ToLower())
                                                                 && location == "" ? lstCOMLocation.Contains(i.Location) : i.Location == location
                                                                 && lstCOMBU.Contains(i.BU)
                                                               ).Select(i => new ddlValue { Value = i.QualityProject, Text = i.QualityProject }).Distinct().ToList();//&& i.CreatedBy.Equals(username,StringComparison.OrdinalIgnoreCase)
            }
            else
            {
                lstQualityProject = db.QMS010.Where(i => location == "" ? lstCOMLocation.Contains(i.Location) : i.Location == location
                                                        && lstCOMBU.Contains(i.BU)
                                                        ).Select(i => new ddlValue { Value = i.QualityProject, Text = i.QualityProject }).Take(10).Distinct().ToList();
            }

            return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetShopV2(string term)
        {
            string username = objClsLoginInfo.UserName;
            var lstATHLocation = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.Location).Distinct().ToList();
            var lstCOMLocation = db.COM003.Where(i => i.t_psno.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_loca).Distinct().ToList();

            var lstCOMBU = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.BU).Distinct().ToList();

            lstCOMLocation = lstATHLocation.Union(lstCOMLocation).ToList();

            List<ddlValue> lstShop = new List<ddlValue>();
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstShop = (from glb002 in db.GLB002
                           join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                           where glb001.Category.Equals("Shop", StringComparison.OrdinalIgnoreCase) && lstCOMLocation.Contains(glb002.Location) && lstCOMBU.Contains(glb002.BU)
                                 && (glb002.Description.Contains(term) || glb002.Code.Contains(term))
                           select new ddlValue { Text = glb002.Description, Value = glb002.Code }).Distinct().OrderBy(x => x.Value).ToList();
            }
            else
            {
                lstShop = (from glb002 in db.GLB002
                           join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                           where glb001.Category.Equals("Shop", StringComparison.OrdinalIgnoreCase) && lstCOMLocation.Contains(glb002.Location) && lstCOMBU.Contains(glb002.BU)
                           select new ddlValue { Text = glb002.Description, Value = glb002.Code }).Distinct().OrderBy(x => x.Value).Take(10).ToList();
            }
            return Json(lstShop, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetPartsbyQualityproject(string term, string location, string fromProject, string toProject)
        {
            if (string.IsNullOrWhiteSpace(location))
            {
                location = objClsLoginInfo.Location;
            }
            List<ddlValue> lstSeamNo = new List<ddlValue>();
            List<QMS035> lstAllQualityProject = (from a in db.QMS035
                                                 select a).OrderBy(x => x.QualityProject.Trim()).Distinct().OrderBy(x => x.QualityProject).ToList();

            List<string> newProjectList = new List<string>();
            bool start = false;

            foreach (QMS035 proj in lstAllQualityProject)
            {
                if (proj.QualityProject == fromProject)
                {
                    start = true;
                }

                if (start)
                    newProjectList.Add(proj.QualityProject);

                if (proj.QualityProject == toProject)
                {
                    break;
                }
            }
            newProjectList = newProjectList.Distinct().ToList();

            if (!string.IsNullOrWhiteSpace(term))
            {
                lstSeamNo = (from a in db.QMS035
                             where a.PartNo.Contains(term) && newProjectList.Contains(a.QualityProject)
                             select new ddlValue { Text = a.PartNo, Value = a.PartNo }).Distinct().Take(10).OrderBy(x => x.Value).ToList();
            }
            else
            {
                lstSeamNo = (from a in db.QMS035
                             where newProjectList.Contains(a.QualityProject)
                             select new ddlValue { Text = a.PartNo, Value = a.PartNo }).Distinct().Take(10).OrderBy(x => x.Value).ToList();
            }
            return Json(lstSeamNo, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult GetSeamsbyQualityproject(string term, string location, string fromProject, string toProject)
        {
            if (string.IsNullOrWhiteSpace(location))
            {
                location = objClsLoginInfo.Location;
            }
            List<ddlValue> lstSeamNo = new List<ddlValue>();
            List<QMS012> lstAllQualityProject = (from a in db.QMS012
                                                 select a).OrderBy(x => x.QualityProject.Trim()).Distinct().OrderBy(x => x.QualityProject).ToList();

            List<string> newProjectList = new List<string>();
            bool start = false;

            foreach (QMS012 proj in lstAllQualityProject)
            {
                if (proj.QualityProject == fromProject)
                {
                    start = true;
                }

                if (start)
                    newProjectList.Add(proj.QualityProject);

                if (proj.QualityProject == toProject)
                {
                    break;
                }
            }
            newProjectList = newProjectList.Distinct().ToList();

            if (!string.IsNullOrWhiteSpace(term))
            {
                lstSeamNo = (from a in db.QMS012
                             where a.SeamNo.Contains(term) && newProjectList.Contains(a.QualityProject)
                             select new ddlValue { Text = a.SeamNo, Value = a.SeamNo }).Distinct().Take(10).OrderBy(x => x.Value).ToList();
            }
            else
            {
                lstSeamNo = (from a in db.QMS012
                             where newProjectList.Contains(a.QualityProject)
                             select new ddlValue { Text = a.SeamNo, Value = a.SeamNo }).Distinct().Take(10).OrderBy(x => x.Value).ToList();
            }
            return Json(lstSeamNo, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetAllStagesbyStageType(string term, string location, string InspectionType)
        {
            List<string> lstLocation = new List<string>();
            if (string.IsNullOrWhiteSpace(location))
            {
                lstLocation.Add(objClsLoginInfo.Location.ToString());
            }
            else
            {
                lstLocation = location.Split(',').Distinct().ToList();
            }
            List<ddlValue> lstSeamNo = new List<ddlValue>();
            List<ddlValue> lstStageCode = new List<ddlValue>();

            if (!string.IsNullOrWhiteSpace(term))
            {
                lstStageCode = (from a in db.QMS002
                                where a.StageCode.Contains(term) && a.StageCode != null && a.StageCode != "" && lstLocation.Contains(a.Location) && (InspectionType == "" ? a.StageDepartment == InspectionType : true)
                                select new ddlValue { Text = a.StageCode, Value = a.StageCode }).OrderBy(x => x.Value).Distinct().Take(10).ToList();
            }
            else
            {
                lstStageCode = (from a in db.QMS002
                                where a.StageCode != null && a.StageCode != "" && lstLocation.Contains(a.Location) && (InspectionType == "" ? a.StageDepartment == InspectionType : true)
                                select new ddlValue { Text = a.StageCode, Value = a.StageCode }).OrderBy(x => x.Value).Distinct().Take(10).ToList();
            }
            return Json(lstStageCode, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllBU()
        {
            try
            {
                var lstBu = (from ath1 in db.ATH001
                             join com2 in db.COM002 on ath1.BU equals com2.t_dimx
                             where ath1.Employee.Trim() == objClsLoginInfo.UserName.Trim() && ath1.BU != ""
                             select new { id = ath1.BU, text = com2.t_desc }).Distinct().ToList();
                return Json(lstBu, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult GetAllLocation(string term)
        {
            try
            {
                var lstBusLoc = (from ath1 in db.ATH001
                                 join com2 in db.COM002 on ath1.Location equals com2.t_dimx
                                 where ath1.Employee.Trim() == objClsLoginInfo.UserName.Trim() && (term != "" ? (ath1.Location.Contains(term) || com2.t_desc.Contains(term)) : true)//&& (selectedBU != "" ? selectedBU.Contains(ath1.BU) : true)
                                 select new { id = ath1.Location, text = com2.t_desc }).Distinct().ToList();
                return Json(lstBusLoc, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult GetProjectResult()
        {
            var objAccessProjects = db.fn_GetProjectAccessForEmployee(objClsLoginInfo.UserName, "").ToList();
            try
            {
                var lstProjects = (from c1 in db.COM001
                                   where objAccessProjects.Contains(c1.t_cprj)
                                   select new
                                   {
                                       id = c1.t_cprj,
                                       text = c1.t_cprj + " - " + c1.t_dsca
                                   }
                          ).Distinct().ToList();
                return Json(lstProjects, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        /// <summary>
        /// This is method is to get part number for particular quality project.
        /// Created By Darshan Dave 10/12/2018
        /// </summary>
        /// <param name="qualityProject"></param>
        /// <returns></returns>
        public ActionResult GetPartNumberByQualityProject(string term, string qualityProject)
        {
            try
            {
                var data = db.QMS045.Where(x => x.QualityProject.Equals(qualityProject) && x.PartNo.Contains(term)).Select(x => x.PartNo).Distinct().ToList();
                return Json(new { Data = data });
            }
            catch (Exception ex)
            {
                return Json(new { Error = ex.Message });
            }
        }

        #region Next prev 
        [HttpPost]
        public ActionResult nextprevbyProjectorseam(string partNo, string qualityProject, string option)
        {
            ResponsenextorPre objresponse = new ResponsenextorPre();
            List<Projects> lstprojects = listqualityprojects();
            List<partlist> lstParts = null;
            string forall = "ALL";
            int projindexno = 0;
            int seamindexno = 0;
            int minseam = 0;
            int maxseam = 0;
            int minproj = 0;
            int maxproj = 0;

            try
            {
                if (!string.IsNullOrEmpty(qualityProject) && !string.IsNullOrEmpty(partNo))
                {
                    projindexno = lstprojects.Select((item, index) => new { index, item }).Where(x => x.item.projectCode.ToLower() == qualityProject.ToLower()).Select(x => x.index).FirstOrDefault();
                    minproj = lstprojects.Select((item, index) => new { index, item }).Min(x => x.index);
                    maxproj = lstprojects.Select((item, index) => new { index, item }).Max(x => x.index);

                    if (!string.IsNullOrEmpty(option))
                    {
                        switch (option)
                        {
                            case "PrevProject":
                                ViewBag.Title = "PrevProject";
                                projindexno = (projindexno - 1);
                                objresponse.PreviousProjectButton = (projindexno == minproj) ? true : ((lstprojects.Select((item, index) => new { index, item }).Any(x => x.index == projindexno)) ? false : true);
                                objresponse.NextProjectButton = (lstprojects.Select((item, index) => new { index, item }).Any(x => x.index == (projindexno + 1))) ? false : true;
                                qualityProject = mainqualityProject(projindexno);
                                break;
                            case "NextProject":
                                ViewBag.Title = "NextProject";
                                projindexno = (projindexno + 1);
                                objresponse.PreviousProjectButton = (lstprojects.Select((item, index) => new { index, item }).Any(x => x.index == (projindexno - 1))) ? false : true;
                                objresponse.NextProjectButton = (projindexno == maxproj) ? true : ((lstprojects.Select((item, index) => new { index, item }).Any(x => x.index == projindexno)) ? false : true);
                                qualityProject = mainqualityProject(projindexno);
                                break;
                            default:
                                ViewBag.Title = "NextPart";
                                objresponse.PreviousProjectButton = (lstprojects.Select((item, index) => new { index, item }).Any(x => x.index == (projindexno - 1))) ? false : true;
                                objresponse.NextProjectButton = (lstprojects.Select((item, index) => new { index, item }).Any(x => x.index == (projindexno + 1))) ? false : true;

                                break;
                        }
                    }
                    else
                    {
                        objresponse.PreviousProjectButton = (lstprojects.Select((item, index) => new { index, item }).Any(x => x.index == (projindexno - 1))) ? false : true;
                        objresponse.NextProjectButton = (lstprojects.Select((item, index) => new { index, item }).Any(x => x.index == (projindexno + 1))) ? false : true;
                    }

                    lstParts = GetProjectwisePart(qualityProject);

                    if (lstParts.Select((item, index) => new { index, item }).Any(x => x.item.PartNo.ToLower() == partNo.ToLower()))
                    {
                        seamindexno = lstParts.Select((item, index) => new { index, item }).Where(x => x.item.PartNo.ToLower() == partNo.ToLower()).Select(x => x.index).FirstOrDefault();
                        minseam = lstParts.Select((item, index) => new { index, item }).Min(x => x.index);
                        maxseam = lstParts.Select((item, index) => new { index, item }).Max(x => x.index);
                    }
                    else
                        partNo = forall;

                    if (partNo.ToLower() == forall.ToLower())
                    {
                        objresponse.PreviousPartNoButton = true;
                        objresponse.NextPartNoButton = true;
                    }
                    else
                    {
                        objresponse.PreviousPartNoButton = (lstParts.Select((item, Index) => new { Index, item }).Any(x => x.Index == (seamindexno - 1))) ? false : true;
                        objresponse.NextPartNoButton = (lstParts.Select((item, Index) => new { Index, item }).Any(x => x.Index == (seamindexno + 1))) ? false : true;
                    }


                    switch (option)
                    {
                        case "PrevPart":
                            ViewBag.Title = "PrevPart";
                            seamindexno = (seamindexno - 1);
                            objresponse.PreviousPartNoButton = (seamindexno == minseam) ? true : ((lstParts.Select((item, index) => new { index, item }).Any(x => x.index == seamindexno)) ? false : true);
                            objresponse.NextPartNoButton = (lstParts.Select((item, index) => new { index, item }).Any(x => x.index == (seamindexno + 1))) ? false : true;
                            partNo = lstParts.Select((item, index) => new { index, item }).Where(x => x.index == seamindexno).FirstOrDefault().item.PartNo;
                            break;
                        case "NextPart":
                            ViewBag.Title = "NextPart";
                            seamindexno = (seamindexno + 1);
                            objresponse.PreviousPartNoButton = (lstParts.Select((item, index) => new { index, item }).Any(x => x.index == (seamindexno - 1))) ? false : true;
                            objresponse.NextPartNoButton = (seamindexno == maxseam) ? true : ((lstParts.Select((item, index) => new { index, item }).Any(x => x.index == seamindexno)) ? false : true);
                            partNo = lstParts.Select((item, index) => new { index, item }).Where(x => x.index == seamindexno).FirstOrDefault().item.PartNo;
                            break;
                        default:
                            ViewBag.Title = "NextPart";
                            break;
                    }

                    objresponse.Project = qualityProject;
                    objresponse.Value = partNo;
                    objresponse.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objresponse.Key = false;
                objresponse.Value = "Error Occures Please try again";
            }
            return Json(objresponse, JsonRequestBehavior.AllowGet);
        }

        public List<Projects> listqualityprojects()
        {
            string username = objClsLoginInfo.UserName;
            var lstATHLocation = db.ATH001.Where(x => x.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(x => x.Location).Distinct().ToList();
            var lstCOMLocation = db.COM003.Where(i => i.t_psno.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_loca).Distinct().ToList();

            var lstCOMBU = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.BU).Distinct().ToList();

            lstCOMLocation = lstATHLocation.Union(lstCOMLocation).ToList();

            List<Projects> lstQualityProject = new List<Projects>();

            lstQualityProject = db.QMS010.Where(i => lstCOMLocation.Contains(i.Location)
                                                    && lstCOMBU.Contains(i.BU)
                                                    ).Select(i => new Projects { Value = i.QualityProject, projectCode = i.QualityProject }).Distinct().ToList();
            return lstQualityProject;
        }
        public List<partlist> GetProjectwisePart(string qualityProject)
        {
            List<partlist> lstseam = new List<partlist>();
            lstseam = db.QMS045.Where(x => x.QualityProject.Equals(qualityProject)).Select(x => new partlist { PartNo = x.PartNo }).Distinct().ToList();
            return lstseam;
        }

        public string mainqualityProject(int indexno = 0)
        {
            string project = string.Empty;
            List<Projects> lstprojects = listqualityprojects();
            project = lstprojects.Select((item, index) => new { index, item }).Where(x => x.index == indexno).FirstOrDefault().item.projectCode;
            return project;
        }

        public class partlist
        {
            public string PartNo { get; set; }
        }
        public class ResponsenextorPre : clsHelper.ResponseMsg
        {
            public string Project { get; set; }
            public string Part { get; set; }
            public bool NextProjectButton { get; set; }
            public bool PreviousProjectButton { get; set; }
            public bool NextPartNoButton { get; set; }
            public bool PreviousPartNoButton { get; set; }
        }

        #endregion

        #region protocol
        public ActionResult IsProtocolFileRequired(int OfferHeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS045 objQMS045 = db.QMS045.FirstOrDefault(c => c.HeaderId == OfferHeaderId);
                if (Convert.ToString(objQMS045.ProtocolType) == clsImplementationEnum.ProtocolType.Other_Files.GetStringValue())
                {
                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Key = false;
                }

                return Json(objResponseMsg);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error Occures Please try again";
                return Json(objResponseMsg);
            }
        }

        public ActionResult GetPartICLLineId(int OfferHeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS045 objQMS045 = db.QMS045.FirstOrDefault(c => c.HeaderId == OfferHeaderId);
                QMS036 objQMS036 = db.QMS036.FirstOrDefault(c => c.Project == objQMS045.Project && c.QualityProject == objQMS045.QualityProject && c.BU == objQMS045.BU && c.Location == objQMS045.Location && c.PartNo == objQMS045.PartNo && c.StageCode == objQMS045.StageCode && c.StageSequence == objQMS045.StageSequence);

                if (objQMS036 != null)
                {
                    objResponseMsg.Value = objQMS036.LineId.ToString();
                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Value = "ICL Line not found.";
                    objResponseMsg.Key = false;
                }

                return Json(objResponseMsg);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error Occures Please try again";
                return Json(objResponseMsg);
            }
        }

        [HttpPost]
        public ActionResult CopyPartDetails(string QualityProject, string StageCode, string ProtocolType)
        {
            string Readytooffer = clsImplementationEnum.TravelerHeaderStatus.Readytooffer.GetStringValue();
            ViewBag.QualityProjectPartList = db.QMS045.Where(m =>
                                                    m.QualityProject.ToLower() == QualityProject.ToLower() &&
                                                    m.InspectionStatus.ToLower() == Readytooffer.ToLower() &&
                                                    m.IterationNo == 1 &&
                                                    m.StageCode.ToLower() == StageCode.ToLower() &&
                                                    m.ProtocolType.ToLower() == ProtocolType.ToLower()
                                                    ).OrderBy(i => i.PartNo).Select(m => new { Text = m.PartNo, Value = m.PartNo }).Distinct().ToList();

            return PartialView("_CopyPartPopupPartial");
        }

        [HttpPost]
        public ActionResult CopyProtocol(int headerId, string fromPart, string toPart)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS045 objQMS045 = db.QMS045.Where(c => c.HeaderId == headerId).FirstOrDefault();
                string Readytooffer = clsImplementationEnum.TravelerHeaderStatus.Readytooffer.GetStringValue();
                #region Get Destination Seams List For Copy

                List<QMS045> allProtocols = db.QMS045.Where(i =>
                                                            i.IterationNo == 1 &&
                                                            i.StageCode == objQMS045.StageCode &&
                                                            i.InspectionStatus.ToLower() == Readytooffer.ToLower() &&
                                                            i.ProtocolType == objQMS045.ProtocolType
                                                            ).OrderBy(i => i.PartNo).ToList();
                List<int?> newprototocolList = new List<int?>();
                bool start = false;
                foreach (QMS045 Protocols in allProtocols)
                {
                    if (Protocols.PartNo == fromPart)
                    {
                        start = true;
                    }
                    if (start)
                        newprototocolList.Add(Protocols.ProtocolId);
                    if (Protocols.PartNo == toPart)
                    {
                        break;
                    }
                }
                #endregion
                foreach (var ToProtocol in newprototocolList)
                {
                    string role = clsImplementationEnum.ProtocolRoleType.A_Actualvalue_for_PROD.GetStringValue();
                    #region Copy Other files
                    db.SP_IPI_PROTOCOL_COPY_PROTOCOL_PRODUCTION_DATA(objQMS045.ProtocolType, objQMS045.ProtocolId, ToProtocol, objClsLoginInfo.UserName, role);
                    #endregion
                }

                if (newprototocolList.Count > 0)
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = newprototocolList.Count.ToString() + " Protocols has been updated.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No any Protocols available for copy protocol.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        #endregion
    }
}