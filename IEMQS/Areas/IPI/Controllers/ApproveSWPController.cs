﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.IPI.Controllers
{
    public class ApproveSWPController : clsBase
    {
        #region Utility
        public string GenerateAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onKeyPressMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false)
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onKeypressEvent = !string.IsNullOrEmpty(onKeyPressMethod) ? "onkeypress='" + onKeyPressMethod + "'" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' hdElement='" + hdElement + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onKeypressEvent + " " + (disabled ? "disabled" : "") + " />";

            return strAutoComplete;
        }
        public string GeneralNotes(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false)
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control";
            inputStyle = "min-width:150px!important;";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur=" + onBlurMethod + "" : "";
            // string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick=" + onClickMethod + "" : "";

            strAutoComplete = "<div class=\"input-icon right\">"
                                 + "<i id=\"aGeneralNotesView" + inputID + "\" class=\"iconspace fa fa-search " + (isReadOnly ? "disabledvalue" : "") + "\" style=\"cursor:pointer;color: #337ab7;\" title=\"General Notes\" onclick=\"OpenKeyDetails(" + rowId + ",true)\"></i>"
                                 + "<input type=\"text\" " + "disabled=\"disabled\"" + " id=\"" + inputID + "\" data-headerid=\"" + rowId + "\" hdElement=\"" + hdElement + "\" value=\"" + inputValue + "\" oldvalue=\"" + inputValue + "\" colname=\"" + columnName
                                 + "\" name=\"" + inputID + "\" class=\"" + className + "\" style=\"" + inputStyle + "\"  " + onBlurEvent + "  " + (disabled ? "disabled" : "") + " />"
                                 + "</div>";

            //strAutoComplete = "<input type=\"text\" " + "disabled=\"disabled\""  + " id=\"" + inputID + "\" data-headerid=\"" + rowId + "\" hdElement=\"" + hdElement + "\" value=\"" + inputValue + "\" oldvalue=\"" + inputValue + "\" colname=\"" + columnName 
            //                        + "\" name=\"" + inputID + "\" class=\"" + className + "\" style=\"" + inputStyle + "\"  " + onBlurEvent + "  " + (disabled ? "disabled" : "") + " />"+
            //                  "<a id=\"aGeneralNotesView" + inputID + "\" " + "onclick='OpenKeyDetails(" + rowId + ")'" + "><i class='iconspace fa fa-search " + (isReadOnly ? "disabledicon" : "") + "' style='float:left;margin-top:8px;font-size:20px;'></i></a>";
            //"<a id=\"aGeneralNotesView" + inputID + "\" " + (!isReadOnly ? "onclick='OpenKeyDetails(" + rowId + ")'" : "") + "><i class='iconspace fa fa-search "+(isReadOnly ? "disabledicon":"")+"' style='float:left;margin-top:8px;font-size:20px;'></i></a>";

            return strAutoComplete;
        }

        [NonAction]
        public static string GenerateTextbox(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onKeyPressMethod = "", bool isReadOnly = false, string inputStyle = "", string maxlength = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control";
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onKeypressEvent = !string.IsNullOrEmpty(onKeyPressMethod) ? "onkeypress='" + onKeyPressMethod + "'" : "";

            htmlControl = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' " + MaxCharcters + " value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + "  " + onKeypressEvent + " spara='" + rowId + "' />";

            return htmlControl;
        }

        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (!string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            }

            //htmlControl = "<a id='" + inputID + "' name='" + inputID + "' class='btn btn-outline btn-circle btn-sm blue' " + onClickEvent + " ><i class='" + className + "'></i> " + buttonName + "</a>";

            //htmlControl = "<i  data-modal='' id='" + inputID + "' name='" + inputID + "' style='cursor:Pointer;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";

            return htmlControl;
        }
        #endregion

        [SessionExpireFilter]
        // GET: IPI/ApproveSWP
        public ActionResult Index()
        {
            return View();
        }
        [SessionExpireFilter]
        public ActionResult SWPApproveDetail(int Id = 0)
        {
            SWP010 objSWPSWP010 = new SWP010();

            var locaDescription = (from a in db.COM002
                                   where a.t_dimx == objClsLoginInfo.Location
                                   select a.t_desc).FirstOrDefault();
            ViewBag.Location = objClsLoginInfo.Location + "-" + locaDescription;
            if (Id > 0)
            {
                objSWPSWP010 = db.SWP010.Where(x => x.HeaderId == Id).FirstOrDefault();
            }
            return View(objSWPSWP010);
        }

        #region Ajay Code
        [HttpPost]
        public ActionResult GetHeaderGridData(string status)
        {
            ViewBag.Status = status;
            return PartialView("_SWPApproveHeaderGrid");
            //return PartialView("_ApprovegridPartial");
        }

        [HttpPost]
        public JsonResult LoadQualityProjectWiseHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";

                //if (param.CTQCompileStatus.ToUpper() == "PENDING")
                //{
                //    whereCondition += "1=1 and status in('" + clsImplementationEnum.CTQStatus.DRAFT.GetStringValue() + "','" + clsImplementationEnum.CTQStatus.Returned.GetStringValue() + "')";
                //}
                //else
                //{
                //    whereCondition += "1=1";
                //}

                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "swp10.BU", "swp10.Location");

                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or swp10.QualityProject like '%" + param.sSearch + "%'or (swp10.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%')";
                }
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                //  var lstHeader = db.SP_IPI_GetQualityIDHeader(startIndex, endIndex, "", whereCondition).ToList();
                var lstResult = db.SP_SWP_Get_QualityProjectwise_HeaderList(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.QualityProject),
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.BU),
                               Convert.ToString(uc.Location),
                                "<center><a class='' href='" + WebsiteURL + "/IPI/ApproveSWP/SWPDetail?HeaderId="+Convert.ToInt32(uc.HeaderId)+"'><i style='' class='fa fa-eye'></i></a></center>"
                           }).ToList();
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]
        public ActionResult SWPDetail(int HeaderId = 0)
        {
            SWP010 objSWPS010 = new SWP010();
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            var locaDescription = (from a in db.COM002
                                   where a.t_dimx == objClsLoginInfo.Location
                                   select a.t_desc).FirstOrDefault();

            ViewBag.Location = objClsLoginInfo.Location + "-" + locaDescription;

            var JointType = (from g2 in db.GLB002
                             join g1 in db.GLB001 on g2.Category equals g1.Id
                             where g1.Category == "Joint Type"
                             select g2.Code).Distinct().ToList();
            ViewBag.JointType = JointType;
            ViewBag.newJointType = JointType.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();


            var QP = (from a in db.SWP010
                      where a.HeaderId == HeaderId
                      select a.QualityProject).FirstOrDefault();



            var MainWPS = (from a in db.WPS012_Log
                           where a.QualityProject == QP
                           select a.WPSNumber).ToList();

            ViewBag.MainWPS = MainWPS;
            ViewBag.newMainWPS = MainWPS.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();
            if (HeaderId > 0)
            {
                objSWPS010 = db.SWP010.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                ViewBag.BU = objSWPS010.BU;
                ViewBag.Location = objSWPS010.Location + "-" + (from a in db.COM002
                                                                where a.t_dimx == objSWPS010.Location
                                                                select a.t_desc).FirstOrDefault();

                objSWPS010.Project = db.COM001.Where(i => i.t_cprj == objSWPS010.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objSWPS010.BU = db.COM002.Where(i => i.t_dimx == objSWPS010.BU && i.t_dtyp == 2).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
                //List<clsTask> list = new List<clsTask>();
                //list = (from a in db.SWP002
                //        where a.QualityProject == QP
                //        select new clsTask
                //        {
                //            id = a.NoteNumber.ToString(),
                //            text = a.NoteDescription
                //        }).ToList();
                //ViewBag.swpNotes = list;

            }

            return PartialView("SWPDetail", objSWPS010);
        }

        [HttpPost]
        public ActionResult GetHeaderGridDataPartial(int HeaderId, string status)
        {

            ViewBag.Status = status;
            var JointType = (from g2 in db.GLB002
                             join g1 in db.GLB001 on g2.Category equals g1.Id
                             where g1.Category == "Joint Type"
                             select g2.Code).Distinct().ToList();

            ViewBag.newJointType = JointType.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

            var QP = (from a in db.SWP010
                      where a.HeaderId == HeaderId
                      select a.QualityProject).FirstOrDefault();

            var MainWPS = (from a in db.WPS012_Log
                           where a.QualityProject == QP
                           select new
                           {
                               a.WPSNumber,
                               a.WPSRevNo
                           }).ToList();

            ViewBag.newMainWPS = MainWPS.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.WPSNumber, CatID = x.WPSRevNo + "-" + x.WPSNumber }).ToList();
            ViewBag.newAlternate1 = MainWPS.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.WPSNumber, CatID = x.WPSRevNo + "-" + x.WPSNumber }).ToList();
            ViewBag.newAlternate2 = MainWPS.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.WPSNumber, CatID = x.WPSRevNo + "-" + x.WPSNumber }).ToList();

            ViewBag.DefaultSearch = Convert.ToString(Session["TP_SWP_Search"]);

            if (!string.IsNullOrEmpty(Convert.ToString(Session["TP_SWP_PageSize"])))
            {
                ViewBag.DefaultPageSize = Convert.ToString(Session["TP_SWP_PageSize"]);
            }
            else
            {
                ViewBag.DefaultPageSize = "10";
            }

            List<string> lstTP_SWPSeams = (List<string>)Session["TP_SWP_SeamList"];
            if (lstTP_SWPSeams != null && lstTP_SWPSeams.Count > 0)
            {
                ViewBag.lstTP_SWPSeams = String.Join(",", lstTP_SWPSeams);
            }
            else
            {
                ViewBag.lstTP_SWPSeams = string.Empty;
            }

            return PartialView("_SWPGridPartial");
            //return PartialView("_HTMLGridPartial");
        }

        public JsonResult LoadSWPHeaderData(JQueryDataTableParamModel param, string qualityProj, string BU, string Location)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.Location;
                string location = Location.Split('-')[0];
                //List<SelectItemList> list = new List<SelectItemList>();
                List<SelectItemList> list = new List<SelectItemList>();
                //list = (from a in db.SWP002
                //        where a.QualityProject == qualityProj
                //        select new SelectItemList
                //        {
                //            id = a.NoteNumber.ToString(),
                //            text = a.NoteNumber.ToString() + "-" + a.NoteDescription.ToString()
                //        }).ToList();
                list = Manager.GetGeneralNotes(qualityProj, "", location).Select(i => new SelectItemList { id = i.NoteNumber.ToString(), text = i.NoteNumber.ToString() + "-" + i.NoteDescription.ToString() }).ToList();

                string strWhere = "1=1";

                if (!string.IsNullOrEmpty(qualityProj) && !string.IsNullOrEmpty(Location))
                {
                    strWhere += " AND UPPER(s10.QualityProject) = '" + qualityProj.ToUpper() + "' AND UPPER(s10.Location) = '" + location.ToUpper() + "'"; //strWhere += " AND UPPER(s10.QualityProject) = '" + qualityProj.ToUpper() + "' AND UPPER(s10.Location) = '" + location.ToUpper() + "'";
                }
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "and 1=1 and s10.status in('" + clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue() + "')";  //strWhere += "and 1=1 and s10.status in('" + clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue() + "')";
                }
                string[] arrayLikeCon = {
                                                "SeamNo",
                                                "SeamDescription",
                                                "selectedJointType",
                                                "MainWPSNumber",
                                                "AlternateWPS1",
                                                "AlternateWPS2",
                                                "MainWPSRevNo",
                                                "AlternateWPS1RevNo",
                                                "AlternateWPS2RevNo",
                                                "Preheat",
                                                "DHTISR",
                                                "SWPNotes",
                                                "Status"
                    };
              
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {                  
                        strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                Session["TP_SWP_Search"] = param.sSearch;
                Session["TP_SWP_PageSize"] = Convert.ToString(param.iDisplayLength);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_SWP_Get_HeaderList
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere, BU, user
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.HeaderId),
                               //Helper.GenerateHTMLTextbox(uc.HeaderId,"SeamNo",uc.SeamNo,"UpdateData(this, "+ uc.HeaderId +");", false,"",true),
                               Convert.ToString(uc.SeamNo),
                               Convert.ToString(uc.selectedJointType),//GenerateAutoComplete(uc.HeaderId, "JointType",uc.selectedJointType,"UpdateData(this, "+ uc.HeaderId +");","return onlySelect(event)",false,"","JointType",true)+""+Helper.GenerateHidden(uc.HeaderId, "hdnJointType"),
                               Convert.ToString(uc.MainWPSNumber),//GenerateAutoComplete(uc.HeaderId, "MainWPSNumber",uc.MainWPSNumber,"UpdateData(this, "+ uc.HeaderId +");","return onlySelect(event)",false,"","MainWPSNumber",true)+""+Helper.GenerateHidden(uc.HeaderId, "hdnMainWPSNumber"),
                               Convert.ToString(uc.MainWPSRevNo),//Helper.GenerateHTMLTextbox(uc.HeaderId,"MainWPSRevNo",uc.MainWPSRevNo.ToString(),"UpdateData(this, "+ uc.HeaderId +");", false,"", true),
                               Convert.ToString(uc.AlternateWPS1),//GenerateAutoComplete(uc.HeaderId, "AlternateWPS1",uc.AlternateWPS1,"UpdateData(this, "+ uc.HeaderId +");","return onlySelect(event)",true,"","AlternateWPS1",true)+""+Helper.GenerateHidden(uc.HeaderId, "hdnAlternateWPS1"),
                               Convert.ToString(uc.AlternateWPS1RevNo),//Helper.GenerateHTMLTextbox(uc.HeaderId,"AlternateWPS1RevNo",uc.AlternateWPS1RevNo.ToString(),"UpdateData(this, "+ uc.HeaderId +");", false,"", true),
                               Convert.ToString(uc.AlternateWPS2), //GenerateAutoComplete(uc.HeaderId, "AlternateWPS2",uc.AlternateWPS2,"UpdateData(this, "+ uc.HeaderId +");","return onlySelect(event)",true,"","AlternateWPS2",true)+""+Helper.GenerateHidden(uc.HeaderId, "hdnAlternateWPS2"),
                               Convert.ToString(uc.AlternateWPS2RevNo),//Helper.GenerateHTMLTextbox(uc.HeaderId,"AlternateWPS2RevNo",uc.AlternateWPS2RevNo.ToString(),"UpdateData(this, "+ uc.HeaderId +");", false,"", true),
                               Convert.ToString(uc.Preheat),//GenerateTextbox(uc.HeaderId,"Preheat",Convert.ToString(uc.Preheat),"UpdateData(this, "+ uc.HeaderId +");","return isNumber(event)", true,""),
                               Convert.ToString(uc.DHTISR),//Helper.GenerateTextbox(uc.HeaderId,"DHTISR",Convert.ToString(uc.DHTISR),"UpdateData(this, "+ uc.HeaderId +");", true,""),
                               //Helper.MultiSelectDropdown(list,uc.SWPNotes,true,"","SWPNotes"),
                               GeneralNotes(uc.HeaderId , "SWPNotes",uc.SWPNotes,"", true,"","SWPNotes", false), //GeneralNotes(uc.HeaderId , "SWPNotes",Convert.ToString(uc.SWPNotes),"", true,"","SWPNotes", false),
                               Convert.ToString(uc.RevNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.Returnremarks),
                               "<center><a class='' href='javascript:void(0)' onclick=ShowTimeline('/IPI/ApproveSWP/ShowTimeline?HeaderId="+Convert.ToInt32(uc.HeaderId)+"');><i style='' class='fa fa-clock-o'></i></a></center>",
                               (new MaintainSWPController()).GetGeneralNotesDescriptions(list,uc.SWPNotes,true)
                               //(!string.IsNullOrEmpty(uc.Status)?(string.Equals(uc.Status,clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue(),StringComparison.OrdinalIgnoreCase)?HTMLActionString(uc.HeaderId,uc.Status,"CopySWPID","Copy SWP","fa fa-files-o","fnCopy("+uc.HeaderId+");")+"   "+HTMLActionString(uc.HeaderId,uc.Status,"TransferSWPID","Transfer SWP","fa fa-arrow-circle-right","fnTransfer(\""+ uc.HeaderId +"\");"):string.Empty):string.Empty)+"&nbsp;"+HTMLActionString(uc.HeaderId,uc.Status,"ParameterSWPID","Parameter Slip","fa fa-print","ParameterSlip(\""+ uc.HeaderId +"\");")
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion
        [HttpPost]
        public JsonResult LoadHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.Location;

                //int Headerid =Convert.ToInt32(param.Headerid) ;
                string strWhere = "1=1";
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and s10.status in('" + clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue() + "','" + clsImplementationEnum.FDMSStatus.RetDraft.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "1=1";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    //strWhere += "and (Project+' - '+c.t_dsca like '%" + param.sSearch
                    //     + "%' or Location +' - '+ e.t_desc like '%" + param.sSearch
                    //     + "%' or BU +' - '+ d.t_desc like '%" + param.sSearch
                    //     + "%' or QualityProject like '%" + param.sSearch
                    //     + "%' or SeamNo like '%" + param.sSearch
                    //     + "%' or JointType like '%" + param.sSearch
                    //     + "%' or MainWPSNumber like '%" + param.sSearch
                    //     + "%' or MainWPSRevNo like '%" + param.sSearch
                    //     + "%' or AlternateWPS1RevNo like '%" + param.sSearch
                    //     + "%' or AlternateWPS1 like '%" + param.sSearch
                    //     + "%' or AlternateWPS2 like '%" + param.sSearch
                    //     + "%' or AlternateWPS2RevNo like '%" + param.sSearch
                    //     + "%' or CreatedBy +' - '+ b.t_name like '%" + param.sSearch
                    //     + "%' or CreatedOn like '%" + param.sSearch
                    //     + "%')";
                }

                var lstResult = db.SP_SWP_Get_HeaderList
                                (
                                StartIndex, EndIndex, "", strWhere, "01", user
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           { Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.QualityProject),
                               Convert.ToString(uc.Location),
                               GetMWPS(uc.WPSNumbers1 , uc.MainWPSNumber , uc.Status , uc.HeaderId.ToString()),
                               //Convert.ToString(uc.MainWPSRevNo),
                               GetMWPS1(uc.WPSNumbers2 , uc.AlternateWPS1 , uc.Status , uc.HeaderId.ToString()),
                              // Convert.ToString(uc.AlternateWPS1RevNo),
                                GetMWPS2(uc.WPSNumbers3 , uc.AlternateWPS2 , uc.Status , uc.HeaderId.ToString()),
                              // Convert.ToString(uc.AlternateWPS2RevNo),
                               GetJointType( uc.selectedJointType ,  uc.JointType , uc.Status ,  uc.HeaderId.ToString()),
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public string GetMWPS(string WPSNumbers, string MainWPSNumber, string Status, string HeaderId)
        {
            string[] MWPS = null;
            string htmlOption = "<option value=''> Select WPS </option>";
            MWPS = WPSNumbers.Split(',');
            if (!string.IsNullOrWhiteSpace(MainWPSNumber))
            {
                for (int i = 0; i < MWPS.Length; i++)
                {
                    if (MWPS[i] == MainWPSNumber)
                    {
                        htmlOption += "<option value=" + MWPS[i] + " selected >" + MWPS[i] + "</option>";
                    }
                    else
                    {
                        htmlOption += "<option value=" + MWPS[i] + ">" + MWPS[i] + "</option>";
                    }

                }
                if (Status != clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue())
                {
                    return "<select name='txtmainWPS' id='txtmainWPS_" + HeaderId + "'   class='form-control input-sm input-small input-inline' onchange='update(" + HeaderId + ")'>" + htmlOption + "</select>";
                }
                else
                {
                    return "<select name='txtmainWPS' id='txtmainWPS_" + HeaderId + "' disabled class='form-control input-sm input-small input-inline' onchange='update(" + HeaderId + ")'>" + htmlOption + "</select>";
                }

            }
            else
            {
                for (int i = 0; i < MWPS.Length; i++)
                {
                    htmlOption += "<option value=" + MWPS[i] + ">" + MWPS[i] + "</option>";
                }
                if (Status != clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue())
                {
                    return "<select name='txtmainWPS' id='txtmainWPS_" + HeaderId + "' class='form-control input-sm input-small input-inline' onchange='update(" + HeaderId + ")' >" + htmlOption + "</select>";
                }
                else
                {
                    return "<select name='txtmainWPS'  id='txtmainWPS_" + HeaderId + "' disabled class='form-control input-sm input-small input-inline' onchange='update(" + HeaderId + ")'>" + htmlOption + "</select>";
                }
            }
        }
        public string GetMWPS1(string WPSNumbers, string MainWPSNumber, string Status, string HeaderId)
        {
            string[] MWPS = null;
            string htmlOption = "<option value=''> Select  Alternate WPS1 </option>";
            MWPS = WPSNumbers.Split(',');
            if (!string.IsNullOrWhiteSpace(MainWPSNumber))
            {
                for (int i = 0; i < MWPS.Length; i++)
                {
                    if (MWPS[i] == MainWPSNumber)
                    {
                        htmlOption += "<option value=" + MWPS[i] + " selected >" + MWPS[i] + "</option>";
                    }
                    else
                    {
                        htmlOption += "<option value=" + MWPS[i] + ">" + MWPS[i] + "</option>";
                    }

                }
                if (Status != clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue())
                {
                    return "<select name='txtmainWPS1' id='txtmainWPS1_" + HeaderId + "' class='form-control input-sm input-small input-inline'  onchange='update1(" + HeaderId + ")' >" + htmlOption + "</select>";
                }
                else
                {
                    return "<select name='txtmainWPS1' id='txtmainWPS1_" + HeaderId + "' disabled class='form-control input-sm input-small input-inline'  onchange='update1(" + HeaderId + ")'>" + htmlOption + "</select>";
                }

            }
            else
            {
                for (int i = 0; i < MWPS.Length; i++)
                {
                    htmlOption += "<option value=" + MWPS[i] + ">" + MWPS[i] + "</option>";
                }
                if (Status != clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue())
                {
                    return "<select name='txtmainWPS1' id='txtmainWPS1_" + HeaderId + "' class='form-control input-sm input-small input-inline'  onchange='update1(" + HeaderId + ")'>" + htmlOption + "</select>";
                }
                else
                {
                    return "<select name='txtmainWPS1' id='txtmainWPS1_" + HeaderId + "' disabled class='form-control input-sm input-small input-inline'  onchange='update1(" + HeaderId + ")'>" + htmlOption + "</select>";
                }
            }
        }
        public string GetMWPS2(string WPSNumbers, string MainWPSNumber, string Status, string HeaderId)
        {
            string[] MWPS = null;
            string htmlOption = "<option value=''> Select Alternate WPS2 </option>";
            MWPS = WPSNumbers.Split(',');
            if (!string.IsNullOrWhiteSpace(MainWPSNumber))
            {
                for (int i = 0; i < MWPS.Length; i++)
                {
                    if (MWPS[i] == MainWPSNumber)
                    {
                        htmlOption += "<option value=" + MWPS[i] + " selected >" + MWPS[i] + "</option>";
                    }
                    else
                    {
                        htmlOption += "<option value=" + MWPS[i] + ">" + MWPS[i] + "</option>";
                    }

                }
                if (Status != clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue())
                {
                    return "<select name='txtmainWPS2' id='txtmainWPS2_" + HeaderId + "'  class='form-control input-sm input-small input-inline'  onchange='update2(" + HeaderId + ")'>" + htmlOption + "</select>";
                }
                else
                {
                    return "<select name='txtmainWPS2' id='txtmainWPS2_" + HeaderId + "' disabled class='form-control input-sm input-small input-inline'  onchange='update2(" + HeaderId + ")'>" + htmlOption + "</select>";
                }

            }
            else
            {
                for (int i = 0; i < MWPS.Length; i++)
                {
                    htmlOption += "<option value=" + MWPS[i] + ">" + MWPS[i] + "</option>";
                }
                if (Status != clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue())
                {
                    return "<select name='txtmainWPS2' id='txtmainWPS2_" + HeaderId + "' class='form-control input-sm input-small input-inline'  onchange='update2(" + HeaderId + ")'>" + htmlOption + "</select>";
                }
                else
                {
                    return "<select name='txtmainWPS2' id='txtmainWPS2_" + HeaderId + "' disabled class='form-control input-sm input-small input-inline'  onchange='update2(" + HeaderId + ")' >" + htmlOption + "</select>";
                }
            }
        }
        public string GetJointType(string selectedJointType, string JointType, string Status, string HeaderId)
        {
            string[] JT = null;
            string htmlOption = "<option value=''> Select Joint Type </option>";
            JT = JointType.Split(',');
            if (!string.IsNullOrWhiteSpace(selectedJointType))
            {
                for (int i = 0; i < JT.Length; i++)
                {
                    if (JT[i] == selectedJointType)
                    {
                        htmlOption += "<option value=" + JT[i] + " selected >" + JT[i] + "</option>";
                    }
                    else
                    {
                        htmlOption += "<option value=" + JT[i] + ">" + JT[i] + "</option>";
                    }

                }
                if (Status != clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue())
                {
                    return "<select name='txtJT' id='txtJT_" + HeaderId + "'  class='form-control input-sm input-small input-inline' onchange='jointType(" + HeaderId + ")' >" + htmlOption + "</select>";
                }
                else
                {
                    return "<select name='txtJT' id='txtJT_" + HeaderId + "'  class='form-control input-sm input-small input-inline' onchange='jointType(" + HeaderId + ")' >" + htmlOption + "</select>";
                }

            }
            else
            {
                for (int i = 0; i < JT.Length; i++)
                {
                    htmlOption += "<option value=" + JT[i] + ">" + JT[i] + "</option>";
                }
                if (Status != clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue())
                {
                    return "<select name='txtJT' id='txtJT_" + HeaderId + "'  class='form-control input-sm input-small input-inline' onchange='jointType(" + HeaderId + ")' >" + htmlOption + "</select>";
                }
                else
                {
                    return "<select name='txtJT' id='txtJT_" + HeaderId + "' disabled class='form-control input-sm input-small input-inline'  onchange='jointType(" + HeaderId + ")'>" + htmlOption + "</select>";
                }
            }

        }

        [HttpPost]
        public ActionResult getCodeValue(string project, string approver, string BU)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                project = project.Split('-')[0].Trim();
                objResponseMsg.projdesc = db.COM001.Where(i => i.t_cprj == project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objResponseMsg.appdesc = db.COM003.Where(i => i.t_psno == approver && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
                objResponseMsg.budesc = db.COM002.Where(i => i.t_dimx == BU && i.t_dtyp == 2).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult verifyApprover(string approver)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                approver = approver.Split('-')[0].Trim();
                var Exists = db.COM003.Any(x => x.t_psno == approver && x.t_actv == 1);
                if (Exists == false)
                {
                    objResponseMsg.Key = false;
                }
                else
                {
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SaveHeader(SWP010 swp010)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                SWP010 objSWP010 = db.SWP010.Where(x => x.HeaderId == swp010.HeaderId).FirstOrDefault();
                if (objSWP010 != null)
                {
                    objSWP010.JointType = swp010.JointType;
                    objSWP010.MainWPSNumber = swp010.MainWPSNumber;
                    objSWP010.AlternateWPS1 = swp010.AlternateWPS1;
                    objSWP010.AlternateWPS2 = swp010.AlternateWPS2;
                    objSWP010.MainWPSRevNo = swp010.MainWPSRevNo;
                    objSWP010.AlternateWPS1RevNo = swp010.AlternateWPS1RevNo;
                    objSWP010.AlternateWPS2RevNo = swp010.AlternateWPS2RevNo;
                    objSWP010.Preheat = swp010.Preheat;
                    objSWP010.DHTISR = swp010.DHTISR;
                    objSWP010.SWPNotes = swp010.SWPNotes;
                    objSWP010.ApprovedBy = swp010.ApprovedBy.Split('-')[0].ToString().Trim();
                    objSWP010.EditedBy = objClsLoginInfo.UserName;
                    objSWP010.EditedOn = DateTime.Now;
                    if (objSWP010.Status == clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue())
                    {
                        objSWP010.RevNo = Convert.ToInt32(objSWP010.RevNo) + 1;
                        objSWP010.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                    }
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PlanMessages.Update.ToString();
                }
                else
                {
                    objSWP010.JointType = swp010.JointType;
                    objSWP010.MainWPSNumber = swp010.MainWPSNumber;
                    objSWP010.AlternateWPS1 = swp010.AlternateWPS1;
                    objSWP010.AlternateWPS2 = swp010.AlternateWPS2;
                    objSWP010.MainWPSRevNo = swp010.MainWPSRevNo;
                    objSWP010.AlternateWPS1RevNo = swp010.AlternateWPS1RevNo;
                    objSWP010.AlternateWPS2RevNo = swp010.AlternateWPS2RevNo;
                    objSWP010.Preheat = swp010.Preheat;
                    objSWP010.DHTISR = swp010.DHTISR;
                    objSWP010.SWPNotes = swp010.SWPNotes;
                    objSWP010.ApprovedBy = swp010.ApprovedBy.Split('-')[0].ToString().Trim();
                    objSWP010.RevNo = 0;
                    objSWP010.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                    objSWP010.CreatedBy = objClsLoginInfo.UserName;
                    objSWP010.CreatedOn = DateTime.Now;
                    db.SWP010.Add(objSWP010);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PlanMessages.Insert.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ApproveSelectedSWP(string strHeaderIds)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int attended = 0;
                int notattended = 0;
                string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
                foreach (var item in arrayHeaderIds)
                {
                    int HeaderId = Convert.ToInt32(item);
                    SWP010 objSWP010 = db.SWP010.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    if (objSWP010.Status.Equals(clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue()))
                    {
                        db.SP_SWP_APPROVE(HeaderId, objClsLoginInfo.UserName);

                        #region Send Notification
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.WE3.GetStringValue(), objSWP010.Project, objSWP010.BU, objSWP010.Location, "Shop Weld Plan for Seam " + objSWP010.SeamNo + " has been Approved", clsImplementationEnum.NotificationType.Information.GetStringValue());
                        #endregion
                        attended++;
                    }
                    else
                    {
                        notattended++;
                    }
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = string.Format("{0} Shop Weld Plan(s) Approved successfully", attended);

                if (notattended > 0)
                {
                    objResponseMsg.Remarks = string.Format("Shop Weld Plan(s) {0} have been skipped as Shop Weld Plan(s) has been already attended", notattended);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReturnSelectedSWP(string strHeaderIds, string returnremark)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int attended = 0;
                int notattended = 0;
                if (!string.IsNullOrEmpty(strHeaderIds))
                {
                    int[] headerIds = Array.ConvertAll(strHeaderIds.Split(','), i => Convert.ToInt32(i));
                    foreach (var headerId in headerIds)
                    {
                        var objSWP010 = db.SWP010.Where(i => i.HeaderId == headerId).FirstOrDefault();
                        if (objSWP010.Status.Equals(clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue()))
                        {
                            if (objSWP010 != null)
                            {
                                objSWP010.Status = clsImplementationEnum.QualityIdHeaderStatus.Returned.GetStringValue();
                                objSWP010.ReturnedBy = objClsLoginInfo.UserName;
                                objSWP010.ReturnedOn = DateTime.Now;
                                objSWP010.Returnremarks = returnremark;
                                db.SaveChanges();
                            }
                            
                            #region Send Notification
                            (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.WE3.GetStringValue(), objSWP010.Project, objSWP010.BU, objSWP010.Location, "Shop Weld Plan for Seam " + objSWP010.SeamNo + " has been Returned", clsImplementationEnum.NotificationType.Information.GetStringValue());
                            #endregion

                            attended++;
                        }
                        else
                        {
                            notattended++;
                        }
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = string.Format("{0} Shop Weld Plan(s) Returned successfully", attended);

                    if (notattended > 0)
                    {
                        objResponseMsg.Remarks = string.Format("Shop Weld Plan(s) {0} have been skipped as Shop Weld Plan(s) has been already attended", notattended);
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ShowTimeline(int HeaderId)
        {
            TimelineViewModel model = new TimelineViewModel();

            SWP010 objSWP010 = db.SWP010.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            model.ApprovedBy = objSWP010.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objSWP010.ApprovedBy) : null;
            model.ApprovedOn = objSWP010.ApprovedOn;
            model.SubmittedBy = objSWP010.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objSWP010.SubmittedBy) : null;
            model.SubmittedOn = objSWP010.SubmittedOn;
            model.CreatedBy = objSWP010.CreatedBy != null ? Manager.GetUserNameFromPsNo(objSWP010.CreatedBy) : null;
            model.CreatedOn = objSWP010.CreatedOn;
            model.EditedBy = objSWP010.EditedBy != null ? Manager.GetUserNameFromPsNo(objSWP010.EditedBy) : null;
            model.EditedOn = objSWP010.EditedOn;
            model.ReturnedBy = objSWP010.ReturnedBy != null ? Manager.GetUserNameFromPsNo(objSWP010.ReturnedBy) : null;
            model.ReturnedOn = objSWP010.ReturnedOn;

            return PartialView("~/Views/Shared/_TimelineProgress2.cshtml", model);
        }
    }
}