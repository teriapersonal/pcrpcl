﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using OfficeOpenXml;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace IEMQS.Areas.IPI.Controllers
{
    public class TestPlanController : clsBase
    {
        // GET: IPI/TestPlan

        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        #region Index Page
        public ActionResult Index()
        {
            ViewBag.IsDisplayOnly = false;
            return View();
        }

        //index data
        [HttpPost]
        public JsonResult LoadTPHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                var IsDisplayOnly = !string.IsNullOrEmpty(Request["IsDisplayOnly"]) ? Convert.ToBoolean(Request["IsDisplayOnly"].ToString()) : false;
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += " 1=1 and (qms020.status in('" + clsImplementationEnum.TestPlanStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.TestPlanStatus.Returned.GetStringValue() + "') or swp010.status in('" + clsImplementationEnum.TestPlanStatus.Draft.GetStringValue() + "'))";
                }
                else
                {
                    strWhere += " 1=1";
                }

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                string[] columnName = { "qms020.QualityProject", "qms020.Location", "qms020.BU", "qms020.Project" };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                //strWhere += "1=1 and Location='" + objClsLoginInfo.Location + "'"; ;
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.Location, "", "Location");
                var lstResult = db.SP_IPI_GET_TESTPLAN_INDEXRESULT
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.QualityProject),
                            Convert.ToString(uc.Project),
                            Convert.ToString(uc.BU),
                            Convert.ToString(uc.Location),
                            IsDisplayOnly//If any action include then not add for display
                            ? "<center style='display:inline;'><a class='' title='View' href='" + WebsiteURL + "/IPI/TestPlan/DisplayHeader?HeaderID=" + uc.HeaderId+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a></center>"
                            : "<center style='display:inline;'><a class='' title='View' href='" + WebsiteURL + "/IPI/TestPlan/AddHeader?HeaderID=" + uc.HeaderId + "'><i style='margin-left:5px;' class='fa fa-eye'></i></a></center>",
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Test Plan/SWP
        [SessionExpireFilter]
        public ActionResult TPSWPHeader(int headerID)
        {
            QMS020 objQMS020 = db.QMS020.Where(x => x.HeaderId == headerID).FirstOrDefault();
            ViewBag.BU = db.COM002.Where(a => a.t_dimx == objQMS020.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            ViewBag.Project = db.COM001.Where(i => i.t_cprj == objQMS020.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objQMS020.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            SWP010 objSWP010 = db.SWP010.Where(x => x.QualityProject == objQMS020.QualityProject && x.Project == objQMS020.Project && x.Location == objQMS020.Location && x.BU == objQMS020.BU).FirstOrDefault();
            ViewBag.SWPHeaderId = objSWP010.HeaderId;
            ViewBag.HeaderID = headerID;
            return View(objQMS020);
        }
        #endregion

        #region Header Details
        [SessionExpireFilter]
        public ActionResult AddHeader(int headerID)
        {
            ViewBag.IsDisplayOnly = false;
            QMS020 objQMS020 = db.QMS020.Where(x => x.HeaderId == headerID).FirstOrDefault();
            ViewBag.BU = db.COM002.Where(a => a.t_dimx == objQMS020.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            ViewBag.Project = db.COM001.Where(i => i.t_cprj == objQMS020.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objQMS020.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            SWP010 objSWP010 = db.SWP010.Where(x => x.QualityProject == objQMS020.QualityProject && x.Project == objQMS020.Project && x.Location == objQMS020.Location && x.BU == objQMS020.BU).FirstOrDefault();
            if (objSWP010 != null)
                ViewBag.SWPHeaderId = objSWP010.HeaderId;
            else
                ViewBag.SWPHeaderId = 0;
            ViewBag.HeaderID = headerID;
            Session["TP_SWP_SeamList"] = null;
            Session["TP_SWP_PageSize"] = null;
            Session["TP_SWP_Search"] = null;

            return View(objQMS020);
        }

        //maintain form (header) 
        [HttpPost]
        public ActionResult LoadTPHeaderForm(int HeaderId)
        {
            QMS020 objQMS020 = new QMS020();
            if (HeaderId > 0)
            {
                objQMS020 = db.QMS020.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                ViewBag.Location = objQMS020.Location;
                ViewBag.BU = objQMS020.BU;
                objQMS020.BU = db.COM002.Where(a => a.t_dimx == objQMS020.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objQMS020.Project = db.COM001.Where(i => i.t_cprj == objQMS020.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objQMS020.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objQMS020.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            }

            return PartialView("_LoadTPHeaderForm", objQMS020);
        }

        [HttpPost]
        public ActionResult SetSeamNoList(string seamNo, string action)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            List<string> lstTP_SWPSeams = new List<string>();

            try
            {
                if (Session["TP_SWP_SeamList"] != null)
                {
                    lstTP_SWPSeams = (List<string>)Session["TP_SWP_SeamList"];
                }

                if (action == "add")
                {
                    if (!lstTP_SWPSeams.Contains(seamNo))
                    {
                        lstTP_SWPSeams.Add(seamNo);
                    }
                }
                if (action == "remove")
                {
                    if (lstTP_SWPSeams.Contains(seamNo))
                    {
                        lstTP_SWPSeams.Remove(seamNo);
                    }
                }

                Session["TP_SWP_SeamList"] = lstTP_SWPSeams;
                objResponseMsg.Key = true;
            }
            catch
            {
                objResponseMsg.Key = false;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        //maintain data
        [HttpPost]
        public ActionResult GetTestPlanData(JQueryDataTableParamModel param, string qProj, string location)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                var IsDisplayOnly = !string.IsNullOrEmpty(Request["IsDisplayOnly"]) ? Convert.ToBoolean(Request["IsDisplayOnly"].ToString()) : false;

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = string.Empty;
                //string whereCondition = "1=1";
                string strSortOrder = string.Empty;

                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    whereCondition += " 1=1  and status in('" + clsImplementationEnum.TestPlanStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.TestPlanStatus.Returned.GetStringValue() + "')";
                }
                else
                {
                    whereCondition += " 1=1";
                }
                if (!string.IsNullOrEmpty(location))
                {
                    whereCondition += " AND qms30.Location in('" + location + "')";
                }

                if (!string.IsNullOrEmpty(qProj))
                {
                    whereCondition += " AND UPPER(QualityProject) = '" + qProj.ToUpper() + "'";
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {

                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                string[] columnName = { "QualityProject", "SeamNo", "Location", "QualityId", "PTCApplicable", "PTCNumber", "Status", "QualityIdRev", "RevNo" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                Session["TP_SWP_Search"] = param.sSearch;
                Session["TP_SWP_PageSize"] = Convert.ToString(param.iDisplayLength);


                var lstResult = db.SP_IPI_TEST_PLAN_HEADER_DETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                //string ApprovedStatus = clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue();
                //var lstQualityIds1 = (from a in db.QMS015_Log
                //                      where a.QualityProject == qProj && a.QualityIdApplicableFor == "Seam" && a.Status == ApprovedStatus
                //                      select new { Code = a.QualityId, Desc = a.QualityIdDesc }).Distinct().ToList();


                List<SelectListItem> BoolenList = new List<SelectListItem>() { new SelectListItem { Text = "Yes", Value = "True" }, new SelectListItem { Text = "No", Value = "False" } };
                NDEModels objNDEModels = new NDEModels();
                var data = (from uc in lstResult
                            select new[]
                            {
                                    Convert.ToString(uc.HeaderId),
                                    Helper.GenerateHidden(uc.HeaderId, "HeaderId", Convert.ToString(uc.HeaderId)),
                                    Helper.GenerateHidden(uc.HeaderId,"HasLines",Convert.ToString(uc.ISLines)),
                                    Convert.ToString(uc.SeamNo),
                                    Convert.ToString(uc.QualityId),
                                    uc.QualityIdRev == null ? "" : "R"+Convert.ToString(uc.QualityIdRev),
                                    uc.PTCApplicable ? "Yes" : "No",
                                    Convert.ToString(uc.PTCNumber),
                                    Convert.ToString(uc.ReturnRemark),
                                    Convert.ToString(uc.Status),
                                    string.IsNullOrEmpty(Convert.ToString(uc.RevNo))? Convert.ToString(uc.RevNo) : "R"+Convert.ToString(uc.RevNo),
                                // (IsDisplayOnly
                                // ? uc.QualityId
                                // : (uc.Status == clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue() || uc.Status ==clsImplementationEnum.TestPlanStatus.Returned.GetStringValue() ) && uc.RevNo == 0 ? HTMLAutoCompleteOnBlur(uc.HeaderId, "txtQualityId", Convert.ToString(uc.QualityId), "UpdateData(this, "+ uc.HeaderId +", \""+ uc.Status +"\");",false,"","QualityId",(string.Equals(uc.Status,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) || uc.LatestOfferedSeqNo > 0 ?true:false),"","required")+""+Helper.GenerateHidden(uc.HeaderId,"QualityId", !string.IsNullOrEmpty(uc.QualityId) ? uc.QualityId.Split('-')[0] : "") : Convert.ToString(uc.QualityId)),
                                // uc.QualityIdRev == null ? Convert.ToString(uc.QualityIdRev) : "R"+Convert.ToString(uc.QualityIdRev),

                                //  (IsDisplayOnly
                                //  ? (uc.PTCApplicable?"Yes":"No")
                                //  : (uc.Status == clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue() || uc.Status ==clsImplementationEnum.TestPlanStatus.Returned.GetStringValue() ) && uc.RevNo == 0 ? Helper.GenerateDropdown(uc.HeaderId, "PTCApplicable", new SelectList(BoolenList, "Value", "Text", Convert.ToString(uc.PTCApplicable)),"","UpdateData(this, "+ uc.HeaderId +", \""+ uc.Status +"\");",string.Equals(uc.Status,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue()) || uc.LatestOfferedSeqNo > 0 ?true:false) : (uc.PTCApplicable ? "Yes" : "No")),
                                   
                                    //(IsDisplayOnly
                                    //? uc.PTCNumber
                                    //: (uc.Status == clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringVlue() || uc.Status ==clsImplementationEnum.TestPlanStatus.Returned.GetStringValue() ) && uc.RevNo == 0 ? Helper.GenerateHTMLTextboxOnChanged(uc.HeaderId, "PTCNumber", Convert.ToString(uc.PTCNumber),"UpdateData(this, "+ uc.HeaderId +", \""+ uc.Status +"\");", ( string.Equals(uc.Status,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue()) || !uc.PTCApplicable || uc.LatestOfferedSeqNo > 0 )?true:false, "", false, "12") : Convert.ToString(uc.PTCNumber)),
                                    //Convert.ToString(uc.ReturnRemark),
                                    //Convert.ToString(uc.Status),
                                    //string.IsNullOrEmpty(Convert.ToString(uc.RevNo))? Convert.ToString(uc.RevNo) : "R"+Convert.ToString(uc.RevNo),
                                    (IsDisplayOnly
                                    ?""
                                    :"<center style='display:inline;'>" + Manager.generateSeamHistoryButton(uc.Project.Split('-')[0].Trim(),uc.QualityProject, uc.SeamNo,"Seam Details") +
                                    //((uc.LatestOfferedSeqNo == 0 &&   (uc.Status == clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue() || uc.Status ==clsImplementationEnum.TestPlanStatus.Returned.GetStringValue() ) && uc.RevNo == 0 && !string.IsNullOrEmpty(Convert.ToString(uc.QualityId))) ? HTMLActionString(uc.HeaderId, uc.Status, "Delete", "Delete QualityID", "fa fa-trash-o", "DeleteQualtiyId(" + uc.HeaderId + ");") : HTMLActionString(uc.HeaderId, uc.Status, "Delete", "Delete QualityID", "fa fa-trash-o", "DeleteQualtiyId(" + uc.HeaderId + ");",true)) +
                                    
                                    //HTMLActionString(uc.HeaderId,uc.Status,"Test Plan Stages","Test Plan Stages","fa fa-newspaper-o","GetTestPlanLinePartial("+uc.HeaderId+",\""+uc.SeamNo+"\",true,\"tp\");")+
                                    (uc.ISLines.Value ? HTMLActionString(uc.HeaderId,uc.Status,"CopyQualityID","Copy Test Plan","fa fa-files-o","CopyTestPlanData("+uc.HeaderId+");")  : HTMLActionString(uc.HeaderId,uc.Status,"CopyQualityID","Copy Test Plan","fa fa-files-o","CopyTestPlanData("+uc.HeaderId+");",true)) +
                                    (uc.RevNo > 0 ? HTMLActionString(uc.HeaderId,uc.Status,"HistoryTestPlan","History","fa fa-history","HistoryTestPlan(\""+ uc.HeaderId +"\");") : HTMLActionString(uc.HeaderId,uc.Status,"HistoryTestPlan","History","fa fa-history","HistoryTestPlan(\""+ uc.HeaderId +"\");",true)) +
                                    ("<a class='' href='javascript:void(0)' title='Timeline' onclick=ShowTimeline('/IPI/TestPlan/ShowTimeline?HeaderID="+Convert.ToInt32(uc.HeaderId)+"');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a>")
                                    +"</center>")


                                    //uc.LatestOfferedSeqNo > 0 ? "" : (( (uc.Status == clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue() && uc.RevNo == 0) && !string.IsNullOrEmpty(Convert.ToString(uc.QualityId)) ? HTMLActionString(uc.HeaderId, uc.Status, "Delete", "Delete QualityID", "fa fa-trash-o", "DeleteQualtiyId(" + uc.HeaderId + ");") : "")
                                    //   +"   "+ (!string.IsNullOrEmpty(uc.Status)?(!string.Equals(uc.Status,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?HTMLActionString(uc.HeaderId,uc.Status,"CopyQualityID","Copy Test Plan","fa fa-files-o","CopyTestPlanData("+uc.HeaderId+");"):string.Empty):string.Empty)
                                    //   +"   "+(!string.IsNullOrEmpty(uc.Status)?(uc.RevNo>0 ? HTMLActionString(uc.HeaderId,uc.Status,"HistoryTestPlan","History","fa fa-history","HistoryTestPlan(\""+ uc.HeaderId +"\");"):string.Empty):string.Empty)
                                    //   +"   "+"<a class='btn btn-xs' href='javascript:void(0)' onclick=ShowTimeline('/IPI/TestPlan/ShowTimeline?HeaderID="+Convert.ToInt32(uc.HeaderId)+"');><i style = 'margin-left:5px;' class='fa fa-clock-o'></i></a>")

                            }).ToList();

                //data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetInlineEditableRow(int id, string qProj, string location, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();
                location = location.Split('-')[0];
                //if (TempData["TempGeneralNotes"] == null)
                //{
                //    TempData["TempGeneralNotes"] = Manager.GetGeneralNotes(qProj, "", location).Select(i => new SelectItemList { id = i.NoteNumber.ToString(), text = i.NoteNumber.ToString() + "-" + i.NoteDescription.ToString() }).Distinct().ToList();
                //}
                //var list = (List<SelectItemList>)TempData["TempGeneralNotes"];
                //TempData.Keep("TempGeneralNotes");
                string whereCondition = string.Empty;
                //whereCondition += " 1=1";
                if (!string.IsNullOrEmpty(location))
                {
                    whereCondition += " AND qms30.Location in('" + location + "')";
                }

                if (!string.IsNullOrEmpty(qProj))
                {
                    whereCondition += " AND UPPER(QualityProject) = '" + qProj.ToUpper() + "'";
                }
                if (id != 0)
                {
                    var IsDisplayOnly = !string.IsNullOrEmpty(Request["IsDisplayOnly"]) ? Convert.ToBoolean(Request["IsDisplayOnly"].ToString()) : false;

                    var lstResult = db.SP_IPI_TEST_PLAN_HEADER_DETAILS
                                (
                                1, 0, "", "HeaderId = " + id + whereCondition
                                ).Take(1).ToList();

                    List<SelectListItem> BoolenList = new List<SelectListItem>() { new SelectListItem { Text = "Yes", Value = "True" }, new SelectListItem { Text = "No", Value = "False" } };
                    NDEModels objNDEModels = new NDEModels();
                    data = (from uc in lstResult
                            select new[]
                            {

                                    clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                   //Helper.GenerateHidden(uc.HeaderId, "HeaderId", Convert.ToString(uc.HeaderId)),
                                    Helper.GenerateHidden(uc.HeaderId, "HeaderId", Convert.ToString(uc.HeaderId)),
                                    Helper.GenerateHidden(uc.HeaderId,"HasLines",Convert.ToString(uc.ISLines)),
                                    Convert.ToString(uc.SeamNo),
                                //GenerateAutoComplete(uc.HeaderId, "txtQualityId", Convert.ToString(uc.QualityId),"UpdateData(this, "+ uc.HeaderId +");",false,"","QualityId",(string.Equals(uc.Status,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false))+""+Helper.GenerateHidden(uc.HeaderId,"QualityId",uc.QualityId),

                                    (IsDisplayOnly || isReadOnly
                                    ? uc.QualityId
                                    //: (uc.Status == clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue() || uc.Status ==clsImplementationEnum.TestPlanStatus.Returned.GetStringValue() ) && uc.RevNo == 0 ? HTMLAutoCompleteOnBlur(uc.HeaderId, "txtQualityId", Convert.ToString(uc.QualityId), "UpdateData(this, "+ uc.HeaderId +", \""+ uc.Status +"\");",false,"","QualityId",(string.Equals(uc.Status,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) || uc.LatestOfferedSeqNo > 0 ?true:false),"","required")+""+Helper.GenerateHidden(uc.HeaderId,"QualityId", !string.IsNullOrEmpty(uc.QualityId) ? uc.QualityId.Split('-')[0] : "") : Convert.ToString(uc.QualityId)),
                                    : (HTMLAutoCompleteOnBlur(uc.HeaderId, "txtQualityId", Convert.ToString(uc.QualityId), "UpdateData(this, "+ uc.HeaderId +", \""+ uc.Status +"\");",false,"","QualityId",(string.Equals(uc.Status,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) || uc.LatestOfferedSeqNo > 0 ?true:false),"","required")+""+Helper.GenerateHidden(uc.HeaderId,"QualityId", !string.IsNullOrEmpty(uc.QualityId) ? uc.QualityId.Split('-')[0] : ""))),

                                    uc.QualityIdRev == null ? Convert.ToString(uc.QualityIdRev) : "R"+Convert.ToString(uc.QualityIdRev),
                                    (IsDisplayOnly || isReadOnly
                                    ? (uc.PTCApplicable?"Yes":"No")
                                    : (uc.Status == clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue() || uc.Status ==clsImplementationEnum.TestPlanStatus.Returned.GetStringValue() ) && uc.RevNo == 0 ? Helper.GenerateDropdown(uc.HeaderId, "PTCApplicable", new SelectList(BoolenList, "Value", "Text", Convert.ToString(uc.PTCApplicable)),"","UpdateData(this, "+ uc.HeaderId +", \""+ uc.Status +"\");",string.Equals(uc.Status,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue()) || uc.LatestOfferedSeqNo > 0 ?true:false) : (uc.PTCApplicable ? "Yes" : "No")),
                                    //GetPTCNumber(uc.PTCNumber,uc.Status, uc.HeaderId,uc.PTCApplicable),
                                    (IsDisplayOnly || isReadOnly
                                    ? uc.PTCNumber
                                    : (uc.Status == clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue() || uc.Status ==clsImplementationEnum.TestPlanStatus.Returned.GetStringValue() ) && uc.RevNo == 0 ? Helper.GenerateHTMLTextboxOnChanged(uc.HeaderId, "PTCNumber", Convert.ToString(uc.PTCNumber),"UpdateData(this, "+ uc.HeaderId +", \""+ uc.Status +"\");", ( string.Equals(uc.Status,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue()) || !uc.PTCApplicable || uc.LatestOfferedSeqNo > 0 )?true:false, "", false, "12") : Convert.ToString(uc.PTCNumber)),
                                    Convert.ToString(uc.ReturnRemark),
                                    Convert.ToString(uc.Status),
                                    string.IsNullOrEmpty(Convert.ToString(uc.RevNo))? Convert.ToString(uc.RevNo) : "R"+Convert.ToString(uc.RevNo),
                                    (IsDisplayOnly
                                    ?""
                                    :"<center style='display:inline;'>" + Manager.generateSeamHistoryButton(uc.Project.Split('-')[0].Trim(),uc.QualityProject, uc.SeamNo,"Seam Details") +
                                    ((uc.LatestOfferedSeqNo == 0 &&   (uc.Status == clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue() || uc.Status ==clsImplementationEnum.TestPlanStatus.Returned.GetStringValue() ) && uc.RevNo == 0 && !string.IsNullOrEmpty(Convert.ToString(uc.QualityId))) ? HTMLActionString(uc.HeaderId, uc.Status, "Delete", "Delete QualityID", "fa fa-trash-o", "DeleteQualtiyId(" + uc.HeaderId + ");") : HTMLActionString(uc.HeaderId, uc.Status, "Delete", "Delete QualityID", "fa fa-trash-o", "DeleteQualtiyId(" + uc.HeaderId + ");",true)) +
                                    (uc.ISLines.Value ? HTMLActionString(uc.HeaderId,uc.Status,"CopyQualityID","Copy Test Plan","fa fa-files-o","CopyTestPlanData("+uc.HeaderId+");")  : HTMLActionString(uc.HeaderId,uc.Status,"CopyQualityID","Copy Test Plan","fa fa-files-o","CopyTestPlanData("+uc.HeaderId+");",true)) +
                                    (uc.RevNo > 0 ? HTMLActionString(uc.HeaderId,uc.Status,"HistoryTestPlan","History","fa fa-history","HistoryTestPlan(\""+ uc.HeaderId +"\");") : HTMLActionString(uc.HeaderId,uc.Status,"HistoryTestPlan","History","fa fa-history","HistoryTestPlan(\""+ uc.HeaderId +"\");",true)) +
                                    ("<a class='' href='javascript:void(0)' title='Timeline' onclick=ShowTimeline('/IPI/TestPlan/ShowTimeline?HeaderID="+Convert.ToInt32(uc.HeaderId)+"');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a>")
                                    +"</center>")


                                    //uc.LatestOfferedSeqNo > 0 ? "" : (( (uc.Status == clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue() && uc.RevNo == 0) && !string.IsNullOrEmpty(Convert.ToString(uc.QualityId)) ? HTMLActionString(uc.HeaderId, uc.Status, "Delete", "Delete QualityID", "fa fa-trash-o", "DeleteQualtiyId(" + uc.HeaderId + ");") : "")
                                    //   +"   "+ (!string.IsNullOrEmpty(uc.Status)?(!string.Equals(uc.Status,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?HTMLActionString(uc.HeaderId,uc.Status,"CopyQualityID","Copy Test Plan","fa fa-files-o","CopyTestPlanData("+uc.HeaderId+");"):string.Empty):string.Empty)
                                    //   +"   "+(!string.IsNullOrEmpty(uc.Status)?(uc.RevNo>0 ? HTMLActionString(uc.HeaderId,uc.Status,"HistoryTestPlan","History","fa fa-history","HistoryTestPlan(\""+ uc.HeaderId +"\");"):string.Empty):string.Empty)
                                    //   +"   "+"<a class='btn btn-xs' href='javascript:void(0)' onclick=ShowTimeline('/IPI/TestPlan/ShowTimeline?HeaderID="+Convert.ToInt32(uc.HeaderId)+"');><i style = 'margin-left:5px;' class='fa fa-clock-o'></i></a>")

                            }).ToList();
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string HTMLAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string maxlength = "", string validate = "")
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string FieldValidate = !string.IsNullOrEmpty(validate) ? "validate='" + validate + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onchange='" + onBlurMethod + "'" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' hdElement='" + hdElementId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + (disabled ? "disabled" : "") + " " + MaxCharcters + " " + FieldValidate + " />";

            return strAutoComplete;
        }

        public string HTMLAutoCompleteOnBlur(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string maxlength = "", string validate = "")
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string FieldValidate = !string.IsNullOrEmpty(validate) ? "validate='" + validate + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' hdElement='" + hdElementId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + (disabled ? "disabled" : "") + " " + MaxCharcters + " " + FieldValidate + " />";

            return strAutoComplete;
        }

        //partial for header tab(maintain page)
        [HttpPost]
        public ActionResult LoadTPHeaderDataPartial(string status, string qualityProject, string location, string bu = "")
        {
            ViewBag.Status = status;
            ViewBag.qualityProject = qualityProject;
            ViewBag.Location = location;
            string approvedString = clsImplementationEnum.TestPlanStatus.Approved.GetStringValue();
            List<string> lstQproj = new List<string>();
            lstQproj.Add(qualityProject);
            lstQproj.AddRange(db.QMS017.Where(c => c.BU == bu && c.Location == location && c.IQualityProject == qualityProject).Select(x => x.QualityProject).ToList());

            var lstQualityIds1 = (from a in db.QMS015_Log
                                  where lstQproj.Contains(a.QualityProject) && (a.QualityIdApplicableFor == "Seam") && a.Status == approvedString && a.Location == location
                                  select new CategoryData { Value = a.QualityId, Code = a.QualityId, CategoryDescription = a.QualityId + " - " + a.QualityIdDesc, Date = a.ApprovedOn }).Distinct().OrderByDescending(o => o.Date).ToList();


            ViewBag.QualityID = lstQualityIds1;
            ViewBag.DefaultSearch = Convert.ToString(Session["TP_SWP_Search"]); //ViewBag.DefaultSearch = "";// Convert.ToString(Session["TP_SWP_Search"]);

            if (!string.IsNullOrEmpty(Convert.ToString(Session["TP_SWP_PageSize"])))
            {
                ViewBag.DefaultPageSize = Convert.ToString(Session["TP_SWP_PageSize"]);
            }
            else
            {
                //ViewBag.DefaultPageSize = "10";
                ViewBag.DefaultPageSize = "200"; // Code Change by rahul  (Task ID = 18239) 
            }

            List<string> lstTP_SWPSeams = (List<string>)Session["TP_SWP_SeamList"];
            if (lstTP_SWPSeams != null && lstTP_SWPSeams.Count > 0)
            {
                ViewBag.lstTP_SWPSeams = String.Join(",", lstTP_SWPSeams);
            }
            else
            {
                ViewBag.lstTP_SWPSeams = string.Empty;
            }



            return PartialView("_GetHeaderGridDataPartial");
        }
        #endregion

        #region Line Details
        [SessionExpireFilter]
        [AllowAnonymous]
        //[UserPermissions]
        //load partial for test plan stages
        [HttpPost]
        public ActionResult LoadTestPlanLineData(int headerId) //public ActionResult LoadTestPlanLineData(int headerId, bool isBootbox = false, string openfrom = "")
        {
            NDEModels objNDEModels = new NDEModels();
            QMS020 objQMS020 = db.QMS020.Where(x => x.HeaderId == headerId).FirstOrDefault();
            if (objQMS020 != null)
            {
                var lstAcceptanceStandard = Manager.GetSubCatagories("Acceptance Standard", objQMS020.BU, objQMS020.Location).Select(i => new CategoryData { Value = i.Description, Code = i.Description, CategoryDescription = i.Description }).ToList();
                var lstApplicableSpecification = Manager.GetSubCatagories("Applicable Specification", objQMS020.BU, objQMS020.Location).Select(i => new CategoryData { Value = i.Description, Code = i.Description, CategoryDescription = i.Description }).ToList();
                var lstInspectionExtent = Manager.GetSubCatagories("Inspection Extent", objQMS020.BU, objQMS020.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
                string StageTypeExludedinOfferDropdown = clsImplementationEnum.ConfigParameter.StageTypeExludedinOfferDropdown.GetStringValue();
                var lstStages = Manager.GetConfigValue(StageTypeExludedinOfferDropdown).ToUpper().Split(',').ToArray();
                ViewBag.StagesCode = db.QMS002.Where(i => i.BU.Equals(objQMS020.BU, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(objQMS020.Location, StringComparison.OrdinalIgnoreCase)).OrderBy(o => o.SequenceNo).Select(i => new CategoryData { Value = i.StageCode, Code = i.StageCode, CategoryDescription = i.StageCode + "-" + i.StageDesc }).ToList();
                ViewBag.AcceptanceStandard = lstAcceptanceStandard;
                ViewBag.ApplicableSpecification = lstApplicableSpecification;
                ViewBag.InspectionExtent = lstInspectionExtent;
                ViewBag.DisplaySeamNo = objQMS020.SeamNo;
                //ViewBag.IsDisabled = "";
                //if (isBootbox && openfrom == "swp")
                //{
                //    if (objQMS020.Status == clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue())
                //    {
                //        ViewBag.IsDisabled = "true";
                //    }
                //}
                //ViewBag.StagesCode = db.QMS002.Where(i => i.BU.Equals(objQMS020.BU, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(objQMS020.Location, StringComparison.OrdinalIgnoreCase)).Select(i => new CategoryData { Value = i.StageCode, Code = i.StageCode, CategoryDescription = i.StageCode + "-" + i.StageDesc }).ToList();
                //var lstAcceptanceStd = Manager.GetSubCatagories("Acceptance Standard", objQMS020.BU, objQMS020.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
                //var lstApplicableSpeci= Manager.GetSubCatagories("Applicable Specification", objQMS020.BU, objQMS020.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
                //var lstInspectionExt = Manager.GetSubCatagories("Inspection Extent", objQMS020.BU, objQMS020.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
                //ViewBag.AcceptanceStandard = lstAcceptanceStd;
                //ViewBag.ApplicableSpecification = lstApplicableSpeci;
                //ViewBag.InspectionExtent = lstInspectionExt;
            }

            return PartialView("_LoadTPLinesDetailsForm", objQMS020);
        }

        [HttpPost]
        public ActionResult DeleteLine(int LineID)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string revNo = "";
                //only status will updated as Deleted(No Hard Delete)
                QMS021 objQMS021 = db.QMS021.Where(x => x.LineId == LineID).FirstOrDefault();
                if (objQMS021 != null)
                {
                    if (objQMS021.QMS020.Status != clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue())
                    {
                        if (objQMS021.QMS020.Status == clsImplementationEnum.TestPlanStatus.Approved.GetStringValue())
                        {
                            objQMS021.QMS020.Status = clsImplementationEnum.TestPlanStatus.Draft.GetStringValue();
                            objQMS021.QMS020.RevNo = objQMS021.QMS020.RevNo + 1;
                            objQMS021.QMS020.EditedBy = objClsLoginInfo.UserName;
                            objQMS021.QMS020.EditedOn = DateTime.Now;
                            objQMS021.QMS020.SubmittedBy = null;
                            objQMS021.QMS020.SubmittedOn = null;
                            objQMS021.QMS020.ApprovedBy = null;
                            objQMS021.QMS020.ApprovedOn = null;
                            objQMS021.QMS020.ReturnedBy = null;
                            objQMS021.QMS020.ReturnedOn = null; objQMS021.QMS020.ReturnRemark = null;
                        }
                        revNo = "R" + objQMS021.QMS020.RevNo;
                        if (objQMS021.QMS020.RevNo == 0 && (objQMS021.QMS020.Status == clsImplementationEnum.TestPlanStatus.Draft.GetStringValue() || objQMS021.QMS020.Status == clsImplementationEnum.TestPlanStatus.Returned.GetStringValue()))
                        {
                            string chemical = clsImplementationEnum.SeamStageType.Chemical.GetStringValue();
                            string PMI = clsImplementationEnum.SeamStageType.PMI.GetStringValue();

                            if (Manager.FetchStageType(objQMS021.StageCode.ToLower(), objQMS021.BU, objQMS021.Location) == chemical || Manager.FetchStageType(objQMS021.StageCode.ToLower(), objQMS021.BU, objQMS021.Location) == PMI)
                            {
                                var lstQMS022 = db.QMS022.Where(i => i.TPLineId == LineID).ToList();
                                db.QMS022.RemoveRange(lstQMS022);
                                var lstQMS023 = db.QMS023.Where(i => i.TPLineId == LineID).ToList();
                                db.QMS023.RemoveRange(lstQMS023);
                            }

                            db.QMS021.Remove(objQMS021);
                        }
                        else
                        {
                            objQMS021.StageStatus = clsImplementationEnum.TestPlanStatus.Deleted.GetStringValue();
                            objQMS021.EditedBy = objClsLoginInfo.UserName;
                            objQMS021.EditedOn = DateTime.Now;
                        }
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                        objResponseMsg.RevNo = revNo;
                        objResponseMsg.HeaderStatus = clsImplementationEnum.TestPlanStatus.Draft.GetStringValue();
                    }
                    else
                    {
                        objResponseMsg.HeaderStatus = clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue();
                        objResponseMsg.RevNo = "R" + objQMS021.QMS020.RevNo;
                        objResponseMsg.Remarks = clsImplementationMessage.CommonMessages.NotInEditStatus;
                    }
                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //editable grid for stages
        [HttpPost]
        public JsonResult LoadTestPlanLineGridData(JQueryDataTableParamModel param)
        {
            try
            {
                NDEModels objNDEModels = new NDEModels();
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                var IsDisplayOnly = !string.IsNullOrEmpty(Request["IsDisplayOnly"]) ? Convert.ToBoolean(Request["IsDisplayOnly"].ToString()) : false;

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "";
                strWhere += Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();
                //string[] arrayCTQStatus = { clsImplementationEnum.CTQStatus.DRAFT.GetStringValue(), clsImplementationEnum.CTQStatus.Returned.GetStringValue() };

                string[] columnName = { "StageCode", "StageSequance", "AcceptanceStandard", "ApplicableSpecification", "InspectionExtent", "RevNo", "StageStatus", "Remarks", "LineId", };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);

                string strStatus = param.MTStatus;
                //string strStatus = db.QMS020.Where(c => c.HeaderId == Convert.ToInt32(param.CTQHeaderId)).FirstOrDefault().Status;

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {

                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                // get latest offered stage sequence no.
                int hID = Convert.ToInt32(param.CTQHeaderId);
                QMS020 objQMS020 = db.QMS020.FirstOrDefault(c => c.HeaderId == hID);
                int maxOfferedSequenceNo = (new clsManager()).GetMaxOfferedSequenceNo(objQMS020.QualityProject, objQMS020.BU, objQMS020.Location, objQMS020.SeamNo);
                bool isLTFPSProject = Manager.IsLTFPSProject(objQMS020.Project, objQMS020.BU, objQMS020.Location);

                var lstResult = db.SP_IPI_TEST_PLAN_LINEDETAILS
                                (
                               StartIndex,
                               EndIndex,
                               strSortOrder,
                               strWhere
                                ).ToList();
                int newRecordId = 0;

                var data = (from uc in lstResult
                            select new[]
                            {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.StageCode),
                                Convert.ToString(uc.StageSequance),
                                (IsDisplayOnly
                                ? uc.AcceptanceStandard
                                : ((isLTFPSProject && isStageLinkedInLTFPS(uc.LineId)) || Manager.IsStageOffered(objQMS020.QualityProject, objQMS020.BU, objQMS020.Location, objQMS020.SeamNo, uc.StageCode.Split('-')[0].Trim(), uc.StageSequance.Value)) ? uc.AcceptanceStandard : GenerateAutoComplete(uc.LineId, "AcceptanceStandard", uc.AcceptanceStandard, "UpdateLineDetails(this, "+ uc.LineId +", \""+ strStatus +"\");",false,"","",(string.Equals(strStatus,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false)|| string.Equals(uc.StageStatus, clsImplementationEnum.TestPlanStatus.Deleted.GetStringValue())  ? true : false)),
                                (IsDisplayOnly
                                ? uc.ApplicableSpecification
                                : ((isLTFPSProject && isStageLinkedInLTFPS(uc.LineId)) || Manager.IsStageOffered(objQMS020.QualityProject, objQMS020.BU, objQMS020.Location, objQMS020.SeamNo, uc.StageCode.Split('-')[0].Trim(), uc.StageSequance.Value))? uc.ApplicableSpecification : GenerateAutoComplete(uc.LineId, "ApplicableSpecification", uc.ApplicableSpecification, "UpdateLineDetails(this, "+ uc.LineId +", \""+ strStatus +"\");",false,"","",(string.Equals(strStatus,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false)|| string.Equals(uc.StageStatus, clsImplementationEnum.TestPlanStatus.Deleted.GetStringValue())  ? true : false)),
                                (IsDisplayOnly
                                ? objNDEModels.GetInspectionExtent(uc.InspectionExtent.Split('-')[0].Trim(), uc.BU.Split('-')[0].Trim(), uc.Location.Split('-')[0].Trim()).CategoryDescription
                                : ((isLTFPSProject && isStageLinkedInLTFPS(uc.LineId)) ||  Manager.IsStageOffered(objQMS020.QualityProject, objQMS020.BU, objQMS020.Location, objQMS020.SeamNo, uc.StageCode.Split('-')[0].Trim(), uc.StageSequance.Value)) ? objNDEModels.GetInspectionExtent(uc.InspectionExtent.Split('-')[0].Trim(), uc.BU.Split('-')[0].Trim(), uc.Location.Split('-')[0].Trim()).CategoryDescription : GenerateAutoComplete(uc.LineId, "txtInspectionExtent", objNDEModels.GetInspectionExtent(uc.InspectionExtent.Split('-')[0].Trim(), uc.BU.Split('-')[0].Trim(), uc.Location.Split('-')[0].Trim()).CategoryDescription ,"UpdateLineDetails(this, "+ uc.LineId +", \""+ strStatus +"\");",false,"","InspectionExtent",(string.Equals(strStatus,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false) || string.Equals(uc.StageStatus, clsImplementationEnum.TestPlanStatus.Deleted.GetStringValue())  ? true : false)+""+Helper.GenerateHidden(uc.LineId,"InspectionExtent",uc.InspectionExtent.Split('-')[0].Trim())),
                                Convert.ToString(uc.RevNo),
                                (IsDisplayOnly
                                ? uc.Remarks
                                : ((isLTFPSProject && isStageLinkedInLTFPS(uc.LineId)) || Manager.IsStageOffered(objQMS020.QualityProject, objQMS020.BU, objQMS020.Location, objQMS020.SeamNo, uc.StageCode.Split('-')[0].Trim(), uc.StageSequance.Value)) ? uc.Remarks : Helper.GenerateHTMLTextboxOnChanged(uc.LineId, "Remarks", Convert.ToString( uc.Remarks), "UpdateLineDetails(this, " + uc.LineId + ", \""+ strStatus +"\");", (string.Equals(strStatus, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue())  ? true : false) || string.Equals(uc.StageStatus, clsImplementationEnum.TestPlanStatus.Deleted.GetStringValue())  ? true : false)),
                                //Convert.ToString(uc.Remarks),
                                Convert.ToString(uc.StageStatus),
                                (IsDisplayOnly
                                ? "" //? (  MaintainIPIStagetypewiseGridButton(uc.HeaderId,uc.LineId,uc.QualityProject,uc.Project.Split('-')[0].Trim(),uc.BU.Split('-')[0].Trim(),uc.Location.Split('-')[0].Trim(),uc.SeamNo,uc.RevNo,uc.StageCode.Split('-')[0].Trim(),uc.StageCode,uc.StageSequance,IsDisplayOnly,IsDisplayOnly,IsDisplayOnly,uc.StageType))
                                : "<nobr><center style='display:inline;'>" +(( (isLTFPSProject && isStageLinkedInLTFPS(uc.LineId)) || Manager.IsStageOffered(objQMS020.QualityProject, objQMS020.BU, objQMS020.Location, objQMS020.SeamNo, uc.StageCode.Split('-')[0].Trim(), uc.StageSequance.Value) ) ? HTMLActionString(uc.HeaderId,uc.StageStatus,"Delete","Delete Record","fa fa-trash-o","AddDeleteStageMultiple(\"Delete\","+ uc.LineId +",);",true) : ((string.Equals(strStatus,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) || uc.StageStatus == clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue())? HTMLActionString(uc.HeaderId,uc.StageStatus,"Delete","Delete Record","fa fa-trash-o","AddDeleteStageMultiple(\"Delete\","+ uc.LineId +",);",true) : HTMLActionString(uc.HeaderId,uc.StageStatus,"Delete","Delete Record","fa fa-trash-o ","AddDeleteStageMultiple(\"Delete\","+ uc.LineId +",);"))) 
                                //: "<nobr><center style='display:inline;'>" + (( (isLTFPSProject && isStageLinkedInLTFPS(uc.LineId)) || Manager.IsStageOffered(objQMS020.QualityProject, objQMS020.BU, objQMS020.Location, objQMS020.SeamNo, uc.StageCode.Split('-')[0].Trim(), uc.StageSequance.Value) ) ? HTMLActionString(uc.HeaderId,uc.StageStatus,"Delete","Delete Record","fa fa-trash-o","AddDeleteStageMultiple(\"Delete\","+ uc.LineId +",);",true) : ((string.Equals(strStatus,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) || uc.StageStatus == clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue())? HTMLActionString(uc.HeaderId,uc.StageStatus,"Delete","Delete Record","fa fa-trash-o","AddDeleteStageMultiple(\"Delete\","+ uc.LineId +",);",true) : HTMLActionString(uc.HeaderId,uc.StageStatus,"Delete","Delete Record","fa fa-trash-o","AddDeleteStageMultiple(\"Delete\","+ uc.LineId +",);")))
                                    +  MaintainIPIStagetypewiseGridButton(uc.HeaderId,uc.LineId,uc.QualityProject,uc.Project.Split('-')[0].Trim(),uc.BU.Split('-')[0].Trim(),uc.Location.Split('-')[0].Trim(),uc.SeamNo,uc.RevNo,uc.StageCode.Split('-')[0].Trim(),uc.StageCode,uc.StageSequance,(strStatus != clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue() && !string.Equals(uc.StageStatus,clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue(),StringComparison.OrdinalIgnoreCase)),StageTypeAction(uc.HeaderId, uc.StageCode.Split('-')[0].Trim(), uc.BU.Split('-')[0].Trim(), uc.Location.Split('-')[0].Trim(),true),((isLTFPSProject && isStageLinkedInLTFPS(uc.LineId)) || Manager.IsStageOffered(objQMS020.QualityProject, objQMS020.BU, objQMS020.Location, objQMS020.SeamNo, uc.StageCode.Split('-')[0].Trim(), uc.StageSequance.Value) ),uc.StageType)
                                    +("<a class='' href='javascript:void(0)' title='Timeline' onclick=ShowTimeline('/IPI/TestPlan/ShowTimeline?HeaderID="+Convert.ToInt32(uc.HeaderId)+"&LineId="+Convert.ToInt32(uc.LineId)+"');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a>")+"</center></nobr>"),
                           }).ToList();
                if (!IsDisplayOnly)
                {
                    var newRecord = new[] {
                                        Helper.GenerateHidden(newRecordId, "HeaderId", param.CTQHeaderId),
                                        GenerateAutoCompleteonBlur(newRecordId,"txtStageCode","","getSequence(this);",false,"","StageCode")+""+Helper.GenerateHidden(newRecordId,"StageCode"),
                                        Helper.GenerateTextbox(newRecordId,"StageSequance","","CheckDuplicate(this,\""+ maxOfferedSequenceNo +"\");"),
                                        GenerateAutoComplete(newRecordId, "AcceptanceStandard","","",false,"","AcceptanceStandard")+""+Helper.GenerateHidden(newRecordId,"AcceptanceStandard"),
                                        GenerateAutoComplete(newRecordId, "ApplicableSpecification","","",false,"","ApplicableSpecification")+""+Helper.GenerateHidden(newRecordId,"ApplicableSpecification"),
                                        GenerateAutoComplete(newRecordId, "txtInspectionExtent","","",false,"","InspectionExtent")+""+Helper.GenerateHidden(newRecordId,"InspectionExtent"),
                                        "0",
                                        Helper.GenerateTextbox(newRecordId,"Remarks"),
                                        clsImplementationEnum.TestPlanStatus.Added.GetStringValue(),
                                        Helper.GenerateGridButton(newRecordId, "Save", "Add Record", "fa fa-save", "SaveNewStage(\""+ maxOfferedSequenceNo +"\");" ),
                                        };
                    data.Insert(0, newRecord);
                }
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [NonAction]
        public string MaintainIPIStagetypewiseGridButton(int headerid, int lineid, string qProj = "", string Project = "", string bu = "", string loc = "", string seamno = "", int? tpRevNo = 0, string stage = "", string stageDesc = "", int? stageseq = 0, bool isEditableFlag = false, bool isShowButton = false, bool ViewMode = false, string stagetype = "")
        {
            string strButtons = string.Empty;
            if (ViewMode)
            {
                strButtons = Helper.GenerateActionIcon(lineid, "", "", "fa fa-flask", "", "", true);
                if (stagetype == clsImplementationEnum.SeamStageType.Chemical.GetStringValue() || stagetype == clsImplementationEnum.SeamStageType.PMI.GetStringValue())
                {
                    var fn = "LoadIPIMaintainTestPlanSamplingPlan(" + headerid + "," + lineid + ",'" + qProj + "','" + Project + "','" + bu + "','" + loc + "','" + seamno + "'," + tpRevNo + ",'" + stage + "'," + stageseq + ",'true','false','Maintain Sampling Plan')";
                    //"LoadIPIMaintainSamplingPlan(" + headerid + "," + lineid + "," + revno + ",'" + isEditableFlag.ToString().ToLower() + "','Maintain Sampling Plan -" + stage + "')"
                    strButtons = Helper.GenerateActionIcon(lineid, "Sampling Plan", "Sampling Plan", "fa fa-flask", fn, "", !isShowButton);
                }

                if (stagetype == clsImplementationEnum.SeamStageType.Hardness.GetStringValue())
                {
                    var fn = "LoadTestPlanHardnessFerritePartial(" + lineid + ",'false','Maintain hardness required value')";
                    strButtons = Helper.GenerateActionIcon(lineid, "Maintain Hardness required value", "Maintain Hardness required value", "fa fa-flask", fn, "", !isShowButton);
                }

                if (stagetype == clsImplementationEnum.SeamStageType.Ferrite.GetStringValue())
                {
                    var fn = "LoadTestPlanHardnessFerritePartial(" + lineid + ",'false','Maintain ferrite test required range')";
                    strButtons = Helper.GenerateActionIcon(lineid, "Maintain ferrite test required range", "Maintain ferrite test required range", "fa fa-flask", fn, "", !isShowButton);
                }
            }
            else
            {
                strButtons = Helper.GenerateActionIcon(lineid, "", "", "fa fa-flask", "", "", true);

                var fn = "LoadIPITestPlanSamplingPlan(" + headerid + "," + lineid + ",'" + qProj + "','" + Project + "','" + bu + "','" + loc + "','" + seamno + "'," + tpRevNo + ",'" + stage + "'," + stageseq + ",'true','" + isEditableFlag.ToString().ToLower() + "','Maintain Sampling Plan','" + stagetype + "')";
                if (stagetype == clsImplementationEnum.SeamStageType.Chemical.GetStringValue() || stagetype == clsImplementationEnum.SeamStageType.PMI.GetStringValue())
                {
                    strButtons = Helper.GenerateActionIcon(lineid, "Sampling Plan", "Sampling Plan", "fa fa-flask", fn, "", !isShowButton);
                }
                if (stagetype == clsImplementationEnum.SeamStageType.Hardness.GetStringValue())
                {
                    strButtons = Helper.GenerateActionIcon(lineid, "Maintain Hardness required value", "Maintain Hardness required value", "fa fa-flask", fn, "", !isShowButton);
                }

                if (stagetype == clsImplementationEnum.SeamStageType.Ferrite.GetStringValue())
                {
                    strButtons = Helper.GenerateActionIcon(lineid, "Maintain ferrite test required range", "Maintain ferrite test required range", "fa fa-flask", fn, "", !isShowButton);
                }
            }
            return strButtons;
        }
        public bool CheckDuplicateStage(int headerID, string stageCode, int stageSequence)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS021.Any(c => c.HeaderId == headerID && c.StageCode == stageCode && c.StageSequance == stageSequence && c.StageStatus != deleted);
        }

        //save stages (for editable Grid)
        [HttpPost]
        public ActionResult SaveNewStages(FormCollection fc)
        {
            int newRowIndex = 0;
            QMS021 objQMS021 = new QMS021();
            QMS020 objQMS020 = new QMS020();
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int headerId = Convert.ToInt32(fc["HeaderId" + newRowIndex]);
                objQMS020 = db.QMS020.Where(x => x.HeaderId == headerId).FirstOrDefault();
                if (string.IsNullOrWhiteSpace(objQMS020.CreatedBy))
                {
                    objQMS020.CreatedBy = objClsLoginInfo.UserName;
                    objQMS020.CreatedOn = DateTime.Now;
                }
                if (objQMS020.Status != clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue())
                {
                    if (CheckDuplicateStage(headerId, fc["StageCode" + newRowIndex], Convert.ToInt32(fc["StageSequance" + newRowIndex])))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.TestPlanMessages.DuplicateStage;
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }


                    string addedStatus = clsImplementationEnum.TestPlanStatus.Added.GetStringValue();
                    string modifiedStatus = clsImplementationEnum.TestPlanStatus.Modified.GetStringValue();
                    string deletedStatus = clsImplementationEnum.TestPlanStatus.Deleted.GetStringValue();

                    if (objQMS020.QualityId == null && !db.QMS021.Any(x => x.HeaderId == headerId))
                    {
                        objQMS020.CreatedBy = objClsLoginInfo.UserName;
                        objQMS020.CreatedOn = DateTime.Now;
                    }
                    objQMS021.QualityProject = objQMS020.QualityProject;
                    objQMS021.Project = objQMS020.Project;
                    objQMS021.HeaderId = objQMS020.HeaderId;
                    objQMS021.BU = objQMS020.BU;
                    objQMS021.Location = objQMS020.Location;
                    objQMS021.SeamNo = objQMS020.SeamNo;
                    objQMS021.StageCode = fc["StageCode" + newRowIndex];
                    objQMS021.StageSequance = Convert.ToInt32(fc["StageSequance" + newRowIndex]);
                    objQMS021.AcceptanceStandard = fc["AcceptanceStandard" + newRowIndex].Replace(",", ""); ;
                    objQMS021.ApplicableSpecification = fc["ApplicableSpecification" + newRowIndex].Replace(",", ""); ;
                    objQMS021.InspectionExtent = fc["InspectionExtent" + newRowIndex];
                    objQMS021.Remarks = fc["Remarks" + newRowIndex];
                    objQMS021.HeaderId = objQMS020.HeaderId;
                    if (objQMS020.Status == clsImplementationEnum.ICLSeamStatus.Approved.GetStringValue())
                    {
                        objQMS020.RevNo = Convert.ToInt32(objQMS020.RevNo) + 1;
                        objQMS020.Status = clsImplementationEnum.ICLSeamStatus.Draft.GetStringValue();
                        objQMS020.EditedBy = objClsLoginInfo.UserName;
                        objQMS020.EditedOn = DateTime.Now;
                        objQMS020.SubmittedBy = null;
                        objQMS020.SubmittedOn = null;
                        objQMS020.ApprovedBy = null;
                        objQMS020.ApprovedOn = null;
                        objQMS020.ReturnedBy = null;
                        objQMS020.ReturnedOn = null; objQMS020.ReturnRemark = null;
                    }

                    objQMS021.RevNo = Convert.ToInt32(fc["RevNo" + newRowIndex]);
                    objQMS021.StageStatus = addedStatus;
                    objQMS021.CreatedBy = objClsLoginInfo.UserName;
                    objQMS021.CreatedOn = DateTime.Now;
                    db.QMS021.Add(objQMS021);
                    db.SaveChanges();
                    objResponseMsg.Value = clsImplementationMessage.TestPlanMessages.LineInsert;
                }
                else
                {
                    objResponseMsg.Remarks = clsImplementationMessage.CommonMessages.NotInEditStatus;
                }
                objResponseMsg.Key = true;
                objResponseMsg.HeaderStatus = objQMS020.Status;
                objResponseMsg.RevNo = "R" + objQMS020.RevNo;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.TestPlanMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region 25926 - Hardness Ferrite 
        [HttpPost]
        public ActionResult GetTestPlanHardnessFerritePartial(int lineid = 0, bool isEditable = false)
        {
            QMS021 objQMS021 = new QMS021();
            string StageType = string.Empty;
            if (lineid > 0)
            {
                objQMS021 = db.QMS021.Where(i => i.LineId == lineid).FirstOrDefault();
                StageType = Manager.FetchStageType(objQMS021.StageCode, objQMS021.BU, objQMS021.Location);
                string ferrite = clsImplementationEnum.SeamStageType.Ferrite.GetStringValue();
                string hardness = clsImplementationEnum.SeamStageType.Hardness.GetStringValue();
                string approved = clsImplementationEnum.QualityIdStageStatus.APPROVED.GetStringValue();
                if (objQMS021 != null && ((StageType == hardness && (objQMS021.HardnessRequiredValue == null && objQMS021.HardnessUnit == null)) || (StageType == ferrite && (objQMS021.FerriteMaxValue == null && objQMS021.FerriteMinValue == null && objQMS021.FerriteUnit == null))))
                {
                    List<string> lstQproj = new List<string>();
                    lstQproj.Add(objQMS021.QualityProject);
                    lstQproj.AddRange(db.QMS017.Where(c => c.BU == objQMS021.BU && c.Location == objQMS021.Location && c.IQualityProject == objQMS021.QualityProject).Select(x => x.QualityProject).ToList());

                    var QualityId = objQMS021.QMS020.QualityId;
                    var objQIDLine = (from a in db.QMS015_Log
                                      join x in db.QMS016_Log on a.Id equals x.RefId
                                      where x.QualityId == QualityId && lstQproj.Contains(x.QualityProject) && x.Location == objQMS021.Location && x.StageCode == objQMS021.StageCode && x.StageSequance == objQMS021.StageSequance && a.Status == approved
                                      select x).FirstOrDefault();
                    //var objQIDLine = db.QMS016_Log.OrderByDescending(x => x.RevNo).FirstOrDefault(x => x.QualityId == QualityId && lstQproj.Contains(x.QualityProject) && x.Location == objQMS021.Location && x.StageCode == objQMS021.StageCode && x.StageSequance == objQMS021.StageSequance);

                    if (objQIDLine != null && QualityId != null)
                    {
                        objQMS021.HardnessRequiredValue = objQIDLine.HardnessRequiredValue;
                        objQMS021.HardnessUnit = objQIDLine.HardnessUnit;

                        objQMS021.FerriteMaxValue = objQIDLine.FerriteMaxValue;
                        objQMS021.FerriteMinValue = objQIDLine.FerriteMinValue;
                        objQMS021.FerriteUnit = objQIDLine.FerriteUnit;

                        db.SaveChanges();
                    }
                }
            }
            ViewBag.isEditable = isEditable;
            ViewBag.StageType = StageType;
            return PartialView("~/Areas/IPI/Views/Shared/_LoadTestPlanHardnessFerritePartial.cshtml", objQMS021);
        }

        [HttpPost]
        public ActionResult SaveHardness(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                QMS020 objQMS020 = new QMS020();
                QMS021 objQMS021 = new QMS021();
                int lineid = Convert.ToInt32(fc["LineId"]);
                objQMS021 = db.QMS021.Where(i => i.LineId == lineid).FirstOrDefault();
                if (objQMS021 != null)
                {
                    if (objQMS021.QMS020.Status != clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue() && objQMS021.StageStatus != clsImplementationEnum.LTFPSLineStatus.Deleted.GetStringValue())
                    {
                        if (Convert.ToString(fc["StageType"]) == clsImplementationEnum.SeamStageType.Hardness.GetStringValue())
                        {
                            objQMS021.HardnessRequiredValue = Convert.ToInt32(fc["HardnessRequiredValue"]);
                            objQMS021.HardnessUnit = Convert.ToString(fc["HardnessUnit"]);
                        }
                        if (Convert.ToString(fc["StageType"]) == clsImplementationEnum.SeamStageType.Ferrite.GetStringValue())
                        {
                            objQMS021.FerriteMinValue = Convert.ToDouble(fc["FerriteMinValue"]);
                            objQMS021.FerriteMaxValue = Convert.ToDouble(fc["FerriteMaxValue"]);
                            objQMS021.FerriteUnit = Convert.ToString(fc["FerriteUnit"]);
                        }
                        if (string.Equals(objQMS021.StageStatus, clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue()))
                        {
                            objQMS021.StageStatus = clsImplementationEnum.QualityIdStageStatus.MODIFIED.GetStringValue();
                        }
                        objResponseMsg.Value = "Record saved successfully";
                    }
                    else
                    {
                        objResponseMsg.Remarks = clsImplementationMessage.CommonMessages.NotInEditStatus;
                    }
                }
                db.SaveChanges();
                objResponseMsg.Key = true;

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public clsHelper.ResponseMsgWithStatus ReviseHeaderWithLines(int headerid, bool updateLineflag, int lineid)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (headerid > 0)
                {
                    QMS015 objQMS015 = db.QMS015.Where(i => i.HeaderId == headerid).FirstOrDefault();

                    if (string.Equals(objQMS015.Status, clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue()))
                    {
                        int? newRev = objQMS015.RevNo + 1;
                        #region QID stages
                        var lstLines = db.QMS016.Where(i => i.HeaderId == headerid).ToList();
                        if (lstLines.Any())
                            lstLines.ForEach(i => i.RevNo = newRev);
                        #endregion

                        #region Sampling Plan

                        var lstisSamplingPlan = (from q16 in lstLines
                                                 join q02 in db.QMS002 on q16.StageCode.Trim() equals q02.StageCode.Trim()
                                                 where q16.HeaderId == headerid && q02.StageType.ToLower() == "chemical"
                                                 select q16
                                     ).ToList();
                        if (updateLineflag)
                        {
                            if (lineid > 0)
                            {
                                QMS016 objLineupdate = lstLines.Where(x => x.LineId == lineid).FirstOrDefault();
                                objLineupdate.StageStatus = clsImplementationEnum.QualityIdStageStatus.MODIFIED.GetStringValue();
                                objLineupdate.EditedBy = objClsLoginInfo.UserName;
                                objLineupdate.EditedOn = DateTime.Now;
                            }
                        }
                        foreach (var item in lstisSamplingPlan)
                        {
                            QMS018 objSP = db.QMS018.Where(x => x.QIDLineId == item.LineId).OrderByDescending(x => x.QualityIdRev).FirstOrDefault();
                            int samplingPlanId = objSP.HeaderId;

                            if (objSP != null)
                            {
                                if (objSP.QualityIdRev != newRev)
                                {
                                    QMS018 insertSP = new QMS018();
                                    insertSP = objSP;
                                    insertSP.QualityIdRev = newRev;
                                    insertSP.CreatedBy = objClsLoginInfo.UserName;
                                    insertSP.CreatedOn = DateTime.Now;
                                    insertSP.CreatedBy = objClsLoginInfo.UserName;
                                    insertSP.CreatedOn = DateTime.Now;
                                    db.QMS018.Add(insertSP);
                                    db.SaveChanges();

                                    List<QMS019> existingElement = db.QMS019.Where(x => x.HeaderId == samplingPlanId).ToList();
                                    db.QMS019.AddRange(existingElement.Select(i => new QMS019
                                    {
                                        HeaderId = insertSP.HeaderId,
                                        QIDLineId = objSP.QIDLineId,
                                        Project = objSP.Project,
                                        QualityProject = objSP.QualityProject,
                                        BU = objSP.BU,
                                        Location = objSP.Location,
                                        QualityId = insertSP.QualityId,
                                        QualityIdRev = insertSP.QualityIdRev,
                                        StageCode = insertSP.StageCode,
                                        StageSequence = insertSP.StageSequence,
                                        Element = i.Element,
                                        ReqMinValue = i.ReqMinValue,
                                        ReqMaxValue = i.ReqMaxValue,
                                        CreatedBy = objClsLoginInfo.UserName,
                                        CreatedOn = DateTime.Now
                                    }));
                                    db.SaveChanges();
                                }
                                //insertSP.HeaderId
                            }

                        }
                        #endregion

                        objQMS015.Status = clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue();
                        objQMS015.RevNo = newRev;
                        objQMS015.EditedBy = objClsLoginInfo.UserName;
                        objQMS015.EditedOn = DateTime.Now;
                        objQMS015.SubmittedBy = null;
                        objQMS015.SubmittedOn = null;
                        objQMS015.ApprovedBy = null;
                        objQMS015.ApprovedOn = null;
                        objQMS015.ReturnedBy = null;
                        objQMS015.ReturnedOn = null; objQMS015.ReturnRemark = null;
                    }
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderStatus = objQMS015.Status;
                    objResponseMsg.RevNo = objQMS015.RevNo.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return objResponseMsg;
        }
        #endregion

        #region 25926 - Check Chemical /Ferrite/Hardness/PMI data
        public bool StageTypeAction(int headerid, string stagecode, string bu, string location, bool isGridlevel = false)
        {
            bool flag = false;
            string stagetype = Manager.FetchStageType(stagecode, bu, location);
            string chemical = clsImplementationEnum.SeamStageType.Chemical.GetStringValue();
            string pmi = clsImplementationEnum.SeamStageType.PMI.GetStringValue();
            string hardness = clsImplementationEnum.SeamStageType.Hardness.GetStringValue();
            string ferrite = clsImplementationEnum.SeamStageType.Ferrite.GetStringValue();
            if (stagetype == chemical || stagetype == pmi)
            {
                flag = IsSamplingPlanisMaintained(headerid, stagecode, bu, location, isGridlevel);
            }
            if (stagetype == hardness)
            {
                flag = IsDataMaintained(headerid, stagecode, bu, location, isGridlevel, true);
            }
            if (stagetype == ferrite)
            {
                flag = IsDataMaintained(headerid, stagecode, bu, location, isGridlevel);
            }
            return flag;
        }
        public bool IsDataMaintained(int headerid, string stagecode, string bu, string location, bool isGridlevel = false, bool ishardness = false)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            bool flag = true;
            string hardness = clsImplementationEnum.SeamStageType.Hardness.GetStringValue();
            string ferrite = clsImplementationEnum.SeamStageType.Ferrite.GetStringValue();
            List<QMS021> lstQidHardnessLines = new List<QMS021>();
            if (isGridlevel)
            {
                if (ishardness)
                {
                    flag = Manager.FetchStageType(stagecode, bu, location) == hardness;
                }
                else
                {
                    flag = Manager.FetchStageType(stagecode, bu, location) == ferrite;
                }
            }
            else
            {
                string[] status = { clsImplementationEnum.QualityIdStageStatus.ADDED.GetStringValue(), clsImplementationEnum.QualityIdStageStatus.MODIFIED.GetStringValue() };

                if (ishardness)
                {
                    lstQidHardnessLines = (from q21 in db.QMS021
                                           join q02 in db.QMS002 on q21.StageCode.Trim() equals q02.StageCode.Trim()
                                           where q21.HeaderId == headerid && q02.StageType == hardness && status.Contains(q21.StageStatus)
                                           select q21
                                           ).ToList();
                    foreach (var item in lstQidHardnessLines)
                    {
                        if ((item.HardnessRequiredValue != null || item.HardnessRequiredValue > 0) && !string.IsNullOrWhiteSpace(item.HardnessUnit))
                        {
                            flag = true;
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                }
                else
                {
                    List<QMS021> lstQidFerriteLines = new List<QMS021>();
                    lstQidFerriteLines = (from q21 in db.QMS021
                                          join q02 in db.QMS002 on q21.StageCode.Trim() equals q02.StageCode.Trim()
                                          where q21.HeaderId == headerid && q02.StageType == ferrite && status.Contains(q21.StageStatus)
                                          select q21
                                           ).ToList();
                    foreach (var item in lstQidFerriteLines)
                    {
                        if ((item.FerriteMinValue != null || item.FerriteMinValue > 0) && (item.FerriteMaxValue != null || item.FerriteMaxValue > 0) && !string.IsNullOrWhiteSpace(item.FerriteUnit))
                        {
                            flag = true;
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                }
            }
            return flag;
        }
        public bool IsSamplingPlanisMaintained(int headerid, string stagecode, string bu, string location, bool isGridlevel = false, bool isChemCheck = true)
        {
            bool flag = true;
            string chemical = clsImplementationEnum.SeamStageType.Chemical.GetStringValue();
            string pmi = clsImplementationEnum.SeamStageType.PMI.GetStringValue();
            List<QMS021> lstTPChemicalLines = new List<QMS021>();
            if (isGridlevel)
            {
                string type = Manager.FetchStageType(stagecode, bu, location);
                flag = (type == chemical || type == pmi);
            }
            else
            {
                string[] status = { clsImplementationEnum.QualityIdStageStatus.ADDED.GetStringValue(), clsImplementationEnum.QualityIdStageStatus.MODIFIED.GetStringValue() };
                if (isChemCheck)
                {
                    lstTPChemicalLines = (from q21 in db.QMS021
                                          join q02 in db.QMS002 on q21.StageCode.Trim() equals q02.StageCode.Trim()
                                          where q21.HeaderId == headerid && q02.StageType == chemical && status.Contains(q21.StageStatus)
                                          select q21
                                           ).ToList();
                }
                else
                {
                    lstTPChemicalLines = (from q21 in db.QMS021
                                          join q02 in db.QMS002 on q21.StageCode.Trim() equals q02.StageCode.Trim()
                                          where q21.HeaderId == headerid && q02.StageType == pmi && status.Contains(q21.StageStatus)
                                          select q21
                                         ).ToList();
                }

                foreach (var item in lstTPChemicalLines)
                {
                    QMS022 objSamplingplan = db.QMS022.Where(x => x.TPLineId == item.LineId).FirstOrDefault();
                    if (objSamplingplan != null)
                    {
                        if (objSamplingplan.QMS023.Count() > 0)
                        {
                            flag = true;
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                    else
                    {
                        flag = false;
                        break;
                    }
                }
            }
            return flag;
        }

        public void SaveStageTypewiseData(QMS020 qms020, out string MissingStageType) //partial for CTQ History Lines Details
        {
            MissingStageType = string.Empty;
            //db.Configuration.AutoDetectChangesEnabled = false;
            string chemical = clsImplementationEnum.SeamStageType.Chemical.GetStringValue();
            string pmi = clsImplementationEnum.SeamStageType.PMI.GetStringValue();
            string hardness = clsImplementationEnum.SeamStageType.Hardness.GetStringValue();
            string ferrite = clsImplementationEnum.SeamStageType.Ferrite.GetStringValue();
            string[] StageStatus = { clsImplementationEnum.QualityIdStageStatus.ADDED.GetStringValue(), clsImplementationEnum.QualityIdStageStatus.MODIFIED.GetStringValue() };
            //var qms020 = db.QMS020.Find(headerid);

            List<QMS021> lstTPChemicalLines = new List<QMS021>();
            List<QMS021> lstTPhardnessLines = new List<QMS021>();
            List<QMS021> lstTPferriteLines = new List<QMS021>();

            var lstLines = qms020.QMS021.ToList();
            var lstStages = db.QMS002.Where(x => x.BU == qms020.BU && x.Location == qms020.Location).ToList();

            lstTPChemicalLines = (from q21 in lstLines
                                  join q02 in lstStages on q21.StageCode.Trim() equals q02.StageCode.Trim()
                                  where (q02.StageType == chemical || q02.StageType == pmi) && StageStatus.Contains(q21.StageStatus)
                                  select q21).ToList();
            lstTPhardnessLines = (from q21 in lstLines
                                  join q02 in lstStages on q21.StageCode.Trim() equals q02.StageCode.Trim()
                                  where q02.StageType == hardness && StageStatus.Contains(q21.StageStatus)
                                  select q21).ToList();
            lstTPferriteLines = (from q21 in lstLines
                                 join q02 in lstStages on q21.StageCode.Trim() equals q02.StageCode.Trim()
                                 where q02.StageType == ferrite && StageStatus.Contains(q21.StageStatus)
                                 select q21).ToList();

            List<string> lstQproj = new List<string>();
            lstQproj.Add(qms020.QualityProject);
            lstQproj.AddRange(db.QMS017.Where(c => c.BU == qms020.BU && c.Location == qms020.Location && c.IQualityProject == qms020.QualityProject).Select(x => x.QualityProject).ToList());
            string approved = clsImplementationEnum.QualityIdStageStatus.APPROVED.GetStringValue();
            var QualityId = qms020.QualityId;

            for (int cnt = 0; cnt < lstTPChemicalLines.Count; cnt++)
            {
                var item = lstTPChemicalLines[cnt];

                QMS022 objSamplingplan = db.QMS022.Where(x => x.TPLineId == item.LineId).FirstOrDefault();
                // var objQIDLine = db.QMS016_Log.OrderByDescending(x => x.RevNo).FirstOrDefault(x => x.QualityId == QualityId && lstQproj.Contains(x.QualityProject) && x.Location == qms020.Location && x.StageCode == item.StageCode && x.StageSequance == item.StageSequance && x.StageStatus == approved);

                #region fecth chemical and pmi record for respective stage 
                if (objSamplingplan == null)
                {
                    var objQIDLine = (from a in db.QMS015_Log
                                      join x in db.QMS016_Log on a.Id equals x.RefId
                                      where x.QualityId == QualityId && lstQproj.Contains(x.QualityProject) && x.Location == item.Location && x.StageCode == item.StageCode && x.StageSequance == item.StageSequance && a.Status == approved
                                      select x).FirstOrDefault();

                    if (objQIDLine != null)
                    {
                        QMS022 objQMS022 = new QMS022();

                        QMS018 objQMS018 = db.QMS018.Where(i => lstQproj.Contains(i.QualityProject)
                                                && i.BU == item.BU && i.Location == item.Location && i.QualityId == QualityId && i.QualityIdRev == objQIDLine.RevNo
                                                && i.StageCode == item.StageCode && i.StageSequence == item.StageSequance
                                            ).FirstOrDefault();

                        if (objQMS018 != null)
                        {
                            objQMS022 = new QMS022();
                            objQMS022.TPLineId = item.LineId;
                            objQMS022.Project = item.Project;
                            objQMS022.QualityProject = item.QualityProject;
                            objQMS022.BU = item.BU;
                            objQMS022.Location = item.Location;
                            objQMS022.SeamNo = item.SeamNo;
                            objQMS022.TPRevNo = item.QMS020.RevNo;
                            objQMS022.StageCode = objQMS018.StageCode;
                            objQMS022.StageSequence = objQMS018.StageSequence;
                            objQMS022.Depth = objQMS018.Depth;
                            objQMS022.Component = objQMS018.Component;
                            objQMS022.NoOfComponents = objQMS018.NoOfComponents;
                            objQMS022.Procedure = objQMS018.Procedure;
                            objQMS022.NoOfProcedures = objQMS018.NoOfProcedures;
                            objQMS022.Welder = objQMS018.Welder;
                            objQMS022.NoOfWelders = objQMS018.NoOfWelders;
                            objQMS022.Position = objQMS018.Position;
                            objQMS022.NoOfPositions = objQMS018.NoOfPositions;
                            objQMS022.CreatedBy = objClsLoginInfo.UserName;
                            objQMS022.CreatedOn = DateTime.Now;
                            db.QMS022.Add(objQMS022);
                            db.SaveChanges();
                            List<QMS019> lstQMS019 = objQMS018.QMS019.ToList();
                            if (lstQMS019 != null)
                            {
                                db.QMS023.AddRange(
                                                lstQMS019.Select(x =>
                                                new QMS023
                                                {
                                                    TPLineId = item.LineId,
                                                    Project = item.Project,
                                                    QualityProject = item.QualityProject,
                                                    BU = item.BU,
                                                    Location = item.Location,
                                                    SeamNo = item.SeamNo,
                                                    TPRevNo = item.QMS020.RevNo,
                                                    StageCode = x.StageCode,
                                                    StageSequence = x.StageSequence,
                                                    HeaderId = objQMS022.HeaderId,
                                                    Element = x.Element,
                                                    ReqMinValue = x.ReqMinValue,
                                                    ReqMaxValue = x.ReqMaxValue,
                                                    CreatedBy = objClsLoginInfo.UserName,
                                                    CreatedOn = DateTime.Now,
                                                })
                                                );
                                db.SaveChanges();
                            }

                        }
                    }
                    else
                    {
                        string type = Manager.FetchStageType(item.StageCode, item.BU, item.Location);
                        MissingStageType = type;
                        break;
                    }
                }
                else
                {
                    if (objSamplingplan.QMS023.Count() <= 0)
                    {
                        string type = Manager.FetchStageType(item.StageCode, item.BU, item.Location);
                        MissingStageType = type;
                        break;
                    }

                }
                #endregion
            }

            for (int i = 0; i < lstTPhardnessLines.Count; i++)
            {
                var item = lstTPhardnessLines[i];

                if (!item.HardnessRequiredValue.HasValue || string.IsNullOrWhiteSpace(item.HardnessUnit))
                {
                    var objQIDLine = (from a in db.QMS015_Log
                                      join x in db.QMS016_Log on a.Id equals x.RefId
                                      where x.QualityId == QualityId && lstQproj.Contains(x.QualityProject) && x.Location == item.Location && x.StageCode == item.StageCode && x.StageSequance == item.StageSequance && a.Status == approved
                                      select x).FirstOrDefault();
                    //var objQIDLine = db.QMS016_Log.OrderByDescending(x => x.RevNo).FirstOrDefault(x => x.QualityId == QualityId && lstQproj.Contains(x.QualityProject) && x.Location == item.Location && x.StageCode == item.StageCode && x.StageSequance == item.StageSequance && x.StageStatus == approved);
                    #region fecth Hardness/Ferrite record for respective stage 
                    if (objQIDLine != null)
                    {
                        item.HardnessRequiredValue = objQIDLine.HardnessRequiredValue;
                        item.HardnessUnit = objQIDLine.HardnessUnit;
                        db.SaveChanges();
                    }
                    else
                    {
                        MissingStageType = hardness;
                        break;
                    }
                    #endregion
                }
            }

            for (int i = 0; i < lstTPferriteLines.Count; i++)
            {
                var item = lstTPferriteLines[i];

                if (!item.FerriteMinValue.HasValue || !item.FerriteMaxValue.HasValue || string.IsNullOrWhiteSpace(item.FerriteUnit))
                {
                    var objQIDLine = (from a in db.QMS015_Log
                                      join x in db.QMS016_Log on a.Id equals x.RefId
                                      where x.QualityId == QualityId && lstQproj.Contains(x.QualityProject) && x.Location == item.Location && x.StageCode == item.StageCode && x.StageSequance == item.StageSequance && a.Status == approved
                                      select x).FirstOrDefault();
                    // var objQIDLine = db.QMS016_Log.OrderByDescending(x => x.RevNo).FirstOrDefault(x => x.QualityId == QualityId && lstQproj.Contains(x.QualityProject) && x.Location == item.Location && x.StageCode == item.StageCode && x.StageSequance == item.StageSequance && x.StageStatus == approved);

                    #region fecth Hardness/Ferrite record for respective stage 
                    if (objQIDLine != null && QualityId != null)
                    {
                        item.FerriteMaxValue = objQIDLine.FerriteMaxValue;
                        item.FerriteMinValue = objQIDLine.FerriteMinValue;
                        item.FerriteUnit = objQIDLine.FerriteUnit;
                        db.SaveChanges();
                    }
                    else
                    {
                        MissingStageType = ferrite;
                        break;
                    }
                    #endregion
                }

            }
        }
        #endregion

        #region History
        [HttpPost]
        public ActionResult GetTestPlanHistoryDetails(int Id) //partial for CTQ History Lines Details
        {
            QMS020_log objQMS020Log = new QMS020_log();
            objQMS020Log = db.QMS020_log.Where(x => x.HeaderId == Id).FirstOrDefault();
            return PartialView("_TPHistoryPartial", objQMS020Log);
        }

        public ActionResult ViewLogDetails(int? Id)
        {
            QMS020_log objQMS020 = new QMS020_log();
            if (Id > 0)
            {
                objQMS020 = db.QMS020_log.Where(i => i.Id == Id).FirstOrDefault();
                objQMS020.BU = db.COM002.Where(a => a.t_dimx == objQMS020.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objQMS020.Project = db.COM001.Where(i => i.t_cprj == objQMS020.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objQMS020.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objQMS020.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                ViewBag.Action = "edit";
            }
            return View(objQMS020);
        }


        [HttpPost]
        public JsonResult LoadTestPlanHeaderHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;


                strWhere += "1=1 and HeaderId='" + param.Headerid + "'";
                string[] columnName = { "QualityProject", "Location", "QualityId", "PTCApplicable", "PTCNumber", "Status" };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {

                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_IPI_TEST_PLAN_HISTORY_HEADER_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.QualityProject),
                            Convert.ToString(uc.Location),
                            Convert.ToString(uc.SeamNo),
                            Convert.ToString(uc.QualityId),
                            string.IsNullOrEmpty(Convert.ToString(uc.QualityIdRev))?Convert.ToString(uc.QualityIdRev):"R"+Convert.ToString(uc.QualityIdRev),
                            Convert.ToString(uc.PTCApplicable), //Convert.ToString(uc.PTCApplicable == true ? "Yes":"No"),
                            Convert.ToString(uc.PTCNumber),
                            string.IsNullOrEmpty(Convert.ToString(uc.RevNo))?Convert.ToString(uc.RevNo):"R"+Convert.ToString(uc.RevNo),
                            Convert.ToString(uc.Status),
                            "<center style='display:inline;'><a class='' target='_blank' title='View' href='" + WebsiteURL + "/IPI/TestPlan/ViewLogDetails?ID="+uc.Id+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a><a class='' href='javascript:void(0)' title='Timeline' onclick=ShowTimeline('/IPI/TestPlan/ShowLogTimeline?id="+Convert.ToInt32(uc.Id)+"');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a></center>",
                            }).ToList();



                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadTestPlanLineHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "";
                strWhere += "1=1 and RefId=" + param.Headerid;

                string[] columnName = { "StageCode", "StageSequance", "AcceptanceStandard", "ApplicableSpecification", "InspectionExtent", "RevNo", "StageStatus", "Remarks", "LineId", };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {

                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_IPI_TEST_PLAN_HISTORY_LINE_DETAILS
                                (
                               StartIndex,
                               EndIndex,
                               strSortOrder,
                               strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.StageCode),
                                Convert.ToString(uc.StageSequance),
                                Convert.ToString(uc.AcceptanceStandard),
                                Convert.ToString(uc.ApplicableSpecification),
                                Convert.ToString(uc.InspectionExtent),
                                string.IsNullOrEmpty(Convert.ToString(uc.RevNo))?Convert.ToString(uc.RevNo):"R"+Convert.ToString(uc.RevNo),
                                Convert.ToString(uc.StageStatus),
                                Convert.ToString(uc.Remarks),
                                "<nobr>"+
                                MaintainIPIStagetypewiseGridButton(uc.HeaderId,uc.LineId,uc.QualityProject,uc.Project.Split('-')[0].Trim(),uc.BU.Split('-')[0].Trim(),uc.Location.Split('-')[0].Trim(),uc.SeamNo,uc.RevNo,uc.StageCode.Split('-')[0].Trim(),uc.StageCode,uc.StageSequance,false,StageTypeAction(uc.HeaderId, uc.StageCode.Split('-')[0].Trim(), uc.BU.Split('-')[0].Trim(), uc.Location.Split('-')[0].Trim(),true),isStageLinkedInLTFPS(uc.LineId),uc.StageType)
                                +"</nobr>"
                           }).ToList();


                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Copy
        [HttpPost]
        public ActionResult GetCopyDetails(int HeaderID)
        {
            var objQMS020 = db.QMS020.Where(i => i.HeaderId == HeaderID).FirstOrDefault();

            ViewBag.QualityProject = objQMS020.QualityProject;
            ViewBag.SeamNo = objQMS020.SeamNo;

            ViewBag.Seams = db.QMS020.Where(i => i.QualityProject.Equals(objQMS020.QualityProject, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(objQMS020.Location, StringComparison.OrdinalIgnoreCase) && i.BU.Equals(objQMS020.BU, StringComparison.OrdinalIgnoreCase)).OrderBy(i => i.SeamNo).Select(i => i.SeamNo).ToList();

            return PartialView("_LoadTPCopyDetailPartial");
        }
        [HttpPost]
        public ActionResult copyDetails(int strheaderID, string fromSeam, string toSeam)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objQMS020 = db.QMS020.Where(i => i.HeaderId == strheaderID).FirstOrDefault();

                List<QMS020> allSeams = db.QMS020.Where(i => i.QualityProject.Equals(objQMS020.QualityProject, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(objQMS020.Location, StringComparison.OrdinalIgnoreCase) && i.BU.Equals(objQMS020.BU, StringComparison.OrdinalIgnoreCase)).OrderBy(i => i.SeamNo).ToList();

                List<string> newSeamList = new List<string>();
                bool start = false;

                foreach (QMS020 seam in allSeams)
                {
                    if (seam.SeamNo == fromSeam)
                    {
                        start = true;
                    }

                    if (start)
                        newSeamList.Add(seam.SeamNo);

                    if (seam.SeamNo == toSeam)
                    {
                        break;
                    }
                }


                //int h1 = (from a in db.QMS020
                //          where a.SeamNo == fromSeam
                //          select a.HeaderId).FirstOrDefault();

                //int h2 = (from a in db.QMS020
                //          where a.SeamNo == toSeam
                //          select a.HeaderId).FirstOrDefault();

                //var approveString = clsImplementationEnum.TestPlanStatus.Approved.GetStringValue();
                //var lstcopiedseam = (from a in db.QMS020
                //                     where a.HeaderId >= h1 && a.HeaderId <= h2 && a.QualityId != null && a.Status != approveString && a.HeaderId != strheaderID
                //                     select a.SeamNo).ToList();

                //var lstuncopiedseam = (from a in db.QMS020
                //                       where a.HeaderId >= h1 && a.HeaderId <= h2 && a.QualityId == null && a.Status == approveString && a.HeaderId != strheaderID
                //                       select a.SeamNo).ToList();

                //if (h1 > h2)
                //{
                //    objResponseMsg.Key = false;
                //    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                //}
                //else
                //{
                //var strlstseam = string.Join(",", (from a in db.QMS020
                //                                   where a.HeaderId >= h1 && a.HeaderId <= h2
                //                                   select a.SeamNo).ToList());

                var strlstseam = string.Join(",", newSeamList);

                string currentUser = objClsLoginInfo.UserName;
                var Result = db.SP_IPI_TEST_PLAN_COPY(currentUser, strheaderID, strlstseam).FirstOrDefault();
                if (Convert.ToInt32(Result) > 0)
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = Convert.ToString(Result) + " Seam data is copied successfully";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Seams found for copy";
                }

                //}
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Transfer
        [HttpPost]
        public ActionResult LoadTPTransferPartial(int HeaderID, string location)
        {
            ViewBag.Location = location;//db.COM002.Where(a => a.t_dimx == location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();

            var objQMS020 = db.QMS020.Where(i => i.HeaderId == HeaderID).FirstOrDefault();

            ViewBag.QualityProject = objQMS020.QualityProject;
            ViewBag.SeamNo = objQMS020.SeamNo;


            ViewBag.Seams = db.QMS020.Where(i => i.QualityProject.Equals(objQMS020.QualityProject, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(objQMS020.Location, StringComparison.OrdinalIgnoreCase) && i.BU.Equals(objQMS020.BU, StringComparison.OrdinalIgnoreCase)).OrderBy(i => i.SeamNo).Select(i => i.SeamNo).ToList();




            return PartialView("_LoadTPTransferDetailPartial");
        }

        [HttpPost]
        public ActionResult tranferDetails(int strheaderID, string fromSeam, string toSeam, string fromQProj, string toQProj)
        {
            string currentUser = objClsLoginInfo.UserName;
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            var approveString = clsImplementationEnum.TestPlanStatus.Approved.GetStringValue();
            var sourceSeams = string.Empty;
            var destQMSProjects = string.Empty;

            var objQMS020 = db.QMS020.Where(i => i.HeaderId == strheaderID).FirstOrDefault();

            #region Get Source Seams List for transfer

            List<QMS020> allSeams = db.QMS020.Where(i => i.QualityProject.Equals(objQMS020.QualityProject, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(objQMS020.Location, StringComparison.OrdinalIgnoreCase) && i.BU.Equals(objQMS020.BU, StringComparison.OrdinalIgnoreCase)).OrderBy(i => i.SeamNo).ToList();
            List<string> newSeamList = new List<string>();
            bool start = false;
            foreach (QMS020 seam in allSeams)
            {
                if (seam.SeamNo == fromSeam)
                {
                    start = true;
                }
                if (start)
                    newSeamList.Add(seam.SeamNo);
                if (seam.SeamNo == toSeam)
                {
                    break;
                }
            }
            if (newSeamList.Count > 0)
                sourceSeams = string.Join(",", newSeamList);

            #endregion

            #region Get Destinatino QMS project for transfer

            List<QMS020> allQMSProjects = (from a in db.QMS020
                                           where a.Location.Contains(objClsLoginInfo.Location)
                                           orderby a.QualityProject
                                           select a).Distinct().OrderBy(x => x.QualityProject).ToList();
            List<string> newQMSProjectList = new List<string>();

            start = false;
            foreach (QMS020 proj in allQMSProjects)
            {
                if (proj.QualityProject == fromQProj)
                {
                    start = true;
                }
                if (start)
                    newQMSProjectList.Add(proj.QualityProject);
                if (proj.QualityProject == toQProj)
                {
                    break;
                }
            }
            if (newQMSProjectList.Count > 0)
                destQMSProjects = string.Join(",", newQMSProjectList.Distinct());

            #endregion


            //int h1 = (from a in db.QMS020
            //          where a.SeamNo == fromSeam
            //          select a.HeaderId).FirstOrDefault();

            //int h2 = (from a in db.QMS020
            //          where a.SeamNo == toSeam
            //          select a.HeaderId).FirstOrDefault();

            //int q1 = (from a in db.QMS020
            //          where a.QualityProject == fromQProj
            //          select a.HeaderId).FirstOrDefault();

            //int q2 = (from a in db.QMS020
            //          where a.QualityProject == toQProj
            //          select a.HeaderId).FirstOrDefault();
            //var lstCopiedseam = string.Join(",", (from a in db.QMS020
            //                                      where a.HeaderId >= h1 && a.HeaderId <= h2 && a.QualityId != null && a.Status != approveString && a.HeaderId != strheaderID
            //                                      select a.SeamNo).ToList());
            //var strlstseam = string.Join(",", lstCopiedseam);

            //var lstunCopiedseam = string.Join(",", (from a in db.QMS020
            //                                        where a.HeaderId >= h1 && a.HeaderId <= h2 && a.QualityId == null && a.Status == approveString && a.HeaderId != strheaderID
            //                                        select a.SeamNo).ToList());

            //var lstQualityProject = (from a in db.QMS020
            //                         where a.HeaderId >= q1 && a.HeaderId <= q2 && a.Location.Contains(objClsLoginInfo.Location)
            //                         select a.QualityProject).ToList();
            //var strQualityProject = string.Join(",", lstQualityProject);



            try
            {
                //if ((h2 >= h1) && (q2 >= q1))
                //{
                var Result = db.SP_IPI_TEST_PLAN_TRANSFER(currentUser, strheaderID, sourceSeams, destQMSProjects).FirstOrDefault();// FirstOrDefault(); //(currentUser, strheaderID, fromSeam, toSeam, Convert.ToString(fromQProj), Convert.ToString(toQProj)).FirstOrDefault();
                if (Convert.ToInt32(Result) > 0)
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = Convert.ToString(Result) + " Seam data is transfered successfully";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Seams found for transfer";
                }

                //if (result.Result1.ToString() == "IS")
                //{
                //    objResponseMsg.Key = true;
                //    objResponseMsg.Value = lstCopiedseam.Count() + " seams data transfered successfully and system has skipped " + lstunCopiedseam.Count() + " seams";
                //    // objResponseMsg.Value = clsImplementationMessage.CommonMessages.Transfer.ToString();
                //}
                //else
                //{
                //    objResponseMsg.Key = false;
                //    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                //    //objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                //}
                //}
                //else
                //{
                //    objResponseMsg.Key = false;
                //    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                //}
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region insert,update,delete,send for approval (for header and stages)
        //update header data
        [HttpPost]
        public ActionResult UpdateData(int headerId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            string strApproved = clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue();
            try
            {
                QMS020 objQMS020 = new QMS020();
                objQMS020 = db.QMS020.Where(x => x.HeaderId == headerId).FirstOrDefault();
                if (objQMS020 != null)
                {
                    if (string.IsNullOrWhiteSpace(objQMS020.CreatedBy))
                    {
                        objQMS020.CreatedBy = objClsLoginInfo.UserName;
                        objQMS020.CreatedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                    if (objQMS020.Status != clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue())
                    {
                        if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                        {
                            if (columnName == "QualityId")
                            {
                                QMS015_Log objQMS015 = new QMS015_Log();

                                if (objQMS020.QualityId != columnValue)
                                {
                                    List<string> lstQproj = new List<string>();
                                    lstQproj.Add(objQMS020.QualityProject);
                                    lstQproj.AddRange(db.QMS017.Where(c => c.BU == objQMS020.BU && c.Location == objQMS020.Location && c.IQualityProject == objQMS020.QualityProject).Select(x => x.QualityProject).ToList());

                                    objQMS015 = db.QMS015_Log.Where(x => lstQproj.Contains(x.QualityProject) && x.QualityId == columnValue && x.Status == strApproved && x.Location == objQMS020.Location && x.BU == objQMS020.BU).FirstOrDefault();
                                    if (objQMS015 != null)
                                    {
                                        objQMS020.CreatedBy = objClsLoginInfo.UserName;
                                        objQMS020.CreatedOn = DateTime.Now;
                                        objQMS020.QualityId = objQMS015.QualityId;
                                        objQMS020.QualityIdRev = objQMS015.RevNo;
                                        db.SaveChanges();
                                        objResponseMsg = SaveTPHeader(objQMS020, objQMS015, true);
                                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();

                                    }

                                }
                                else
                                {
                                    objResponseMsg.Key = false;
                                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.DuplicateQID.ToString();
                                }
                            }
                            else
                            {
                                if (columnName == "PTCApplicable")
                                {
                                    if (columnValue == "True")
                                    {
                                        columnValue = "1";
                                    }
                                    else
                                    {
                                        columnValue = "0";
                                    }
                                    db.SP_IPI_TESTPLAN_HEADER_UPDATE(headerId, columnName, columnValue);
                                    if (columnValue == "0")
                                    {
                                        db.SP_IPI_TESTPLAN_HEADER_UPDATE(headerId, "PTCNumber", "");
                                    }
                                }
                                else
                                {
                                    db.SP_IPI_TESTPLAN_HEADER_UPDATE(headerId, columnName, columnValue);
                                }
                                objResponseMsg.Key = true;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                            }
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Remarks = clsImplementationMessage.CommonMessages.NotInEditStatus;
                    }

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //insert stages for quality id from QMS016_log
        public clsHelper.ResponseMsgWithStatus SaveTPHeader(QMS020 objSrcQMS020, QMS015_Log objQMS015, bool IsEdited)
        {
            var ApproveString = clsImplementationEnum.TestPlanStatus.Approved.GetStringValue();
            if (objSrcQMS020.Status == clsImplementationEnum.ICLSeamStatus.Approved.GetStringValue())
            {
                objSrcQMS020.RevNo = Convert.ToInt32(objSrcQMS020.RevNo) + 1;
                objSrcQMS020.Status = clsImplementationEnum.ICLSeamStatus.Draft.GetStringValue();
                objSrcQMS020.EditedBy = objClsLoginInfo.UserName;
                objSrcQMS020.EditedOn = DateTime.Now;
            }

            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            objResponseMsg.HeaderStatus = objSrcQMS020.Status;
            objResponseMsg.RevNo = "R" + objSrcQMS020.RevNo;
            try
            {
                QMS016_Log objSRCQMS016_Log = new QMS016_Log();
                var lstQMS21 = db.QMS021.Where(x => x.HeaderId == objSrcQMS020.HeaderId).ToList();
                string chemical = clsImplementationEnum.SeamStageType.Chemical.GetStringValue().ToLower();
                string PMI = clsImplementationEnum.SeamStageType.PMI.GetStringValue().ToLower();

                //var objQMS16 = db.QMS016_Log.Where(x => x.QualityId == objSrcQMS020.QualityId && x.Project == objSrcQMS020.Project && x.QualityProject == objSrcQMS020.QualityProject).Select(x => x.HeaderId).Distinct().FirstOrDefault();
                if (IsEdited)
                {
                    List<QMS021> lstDesQMS021 = db.QMS021.Where(x => x.HeaderId == objSrcQMS020.HeaderId).ToList();

                    if (lstDesQMS021 != null && lstDesQMS021.Count > 0)
                    {
                        List<int> lstTPLines = lstDesQMS021.Select(a => a.LineId).ToList();

                        #region Seam Offering             
                        List<QMS040> lstQMS040 = db.QMS040.Where(x => x.QualityProject == objSrcQMS020.QualityProject && x.Project == objSrcQMS020.Project && x.BU == objSrcQMS020.BU && x.Location == objSrcQMS020.Location && x.SeamNo == objSrcQMS020.SeamNo).ToList();
                        if (lstQMS040 != null && lstQMS040.Count > 0)
                        {
                            #region Remove Chemical/PMI/Ferrite/Hardness Maintained Data       
                            foreach (var itemQMS040 in lstQMS040)
                            {
                                QMS054 itemQMS054 = db.QMS054.Where(x => x.HeaderId == itemQMS040.HeaderId).FirstOrDefault();
                                if (itemQMS054 != null)
                                {
                                    db.QMS054_1.RemoveRange(itemQMS054.QMS054_1.ToList());
                                    db.QMS054.Remove(itemQMS054);
                                }

                                db.QMS053_1.RemoveRange(itemQMS040.QMS053_1.ToList());
                                db.QMS053.RemoveRange(itemQMS040.QMS053.ToList());

                                QMS052 itemQMS052 = db.QMS052.Where(x => x.HeaderId == itemQMS040.HeaderId).FirstOrDefault();
                                if (itemQMS052 != null)
                                {
                                    db.QMS052_1.RemoveRange(itemQMS052.QMS052_1.ToList());
                                    db.QMS052.Remove(itemQMS052);
                                }

                                db.QMS051.RemoveRange(itemQMS040.QMS051.ToList());
                            }
                            #endregion
                            db.QMS040.RemoveRange(lstQMS040);
                        }
                        #endregion

                        #region Seam ICL 

                        List<QMS031_Log> lstQMS031_Log = db.QMS031_Log.Where(x => x.QualityProject == objSrcQMS020.QualityProject && x.Project == objSrcQMS020.Project && x.BU == objSrcQMS020.BU && x.Location == objSrcQMS020.Location && x.SeamNo == objSrcQMS020.SeamNo).ToList();
                        if (lstQMS031_Log != null && lstQMS031_Log.Count > 0)
                        {
                            db.QMS031_Log.RemoveRange(lstQMS031_Log);
                        }
                        List<QMS030_Log> lstQMS030_Log = db.QMS030_Log.Where(x => x.QualityProject == objSrcQMS020.QualityProject && x.Project == objSrcQMS020.Project && x.BU == objSrcQMS020.BU && x.Location == objSrcQMS020.Location && x.SeamNo == objSrcQMS020.SeamNo).ToList();
                        if (lstQMS030_Log != null && lstQMS030_Log.Count > 0)
                        {
                            db.QMS030_Log.RemoveRange(lstQMS030_Log);
                        }
                        List<QMS031> lstQMS031 = db.QMS031.Where(x => x.QualityProject == objSrcQMS020.QualityProject && x.Project == objSrcQMS020.Project && x.BU == objSrcQMS020.BU && x.Location == objSrcQMS020.Location && x.SeamNo == objSrcQMS020.SeamNo).ToList();
                        if (lstQMS031 != null && lstQMS031.Count > 0)
                        {
                            db.QMS031.RemoveRange(lstQMS031);
                        }
                        List<QMS030> lstQMS030 = db.QMS030.Where(x => x.QualityProject == objSrcQMS020.QualityProject && x.Project == objSrcQMS020.Project && x.BU == objSrcQMS020.BU && x.Location == objSrcQMS020.Location && x.SeamNo == objSrcQMS020.SeamNo).ToList();
                        if (lstQMS030 != null && lstQMS030.Count > 0)
                        {
                            db.QMS030.RemoveRange(lstQMS030);
                        }

                        #endregion

                        #region Chem/Hardness/PMi/Ferrite                    
                        List<QMS023> lstDesQMS023 = db.QMS023.Where(x => lstTPLines.Contains(x.TPLineId.HasValue ? x.TPLineId.Value : 0)).ToList();
                        if (lstDesQMS023 != null && lstDesQMS023.Count > 0)
                        {
                            db.QMS023.RemoveRange(lstDesQMS023);
                        }

                        List<QMS022> lstDesQMS022 = db.QMS022.Where(x => lstTPLines.Contains(x.TPLineId.HasValue ? x.TPLineId.Value : 0)).ToList();
                        if (lstDesQMS022 != null && lstDesQMS022.Count > 0)
                        {
                            db.QMS022.RemoveRange(lstDesQMS022);
                        }
                        #endregion

                        db.QMS021.RemoveRange(lstDesQMS021);
                    }
                    db.SaveChanges();
                    QMS002 objQMS002 = new QMS002();
                    //List<QMS016_Log> lstSrcQMS016_Log = db.QMS016_Log.Where(x => x.HeaderId == objQMS16 && x.StageStatus == ApproveString).ToList();
                    List<QMS016_Log> lstSrcQMS016_Log = db.QMS016_Log.Where(x => x.RefId == objQMS015.Id && x.StageStatus == ApproveString).ToList();
                    if (lstSrcQMS016_Log != null)
                    {
                        db.QMS021.AddRange(
                                       lstSrcQMS016_Log.Select(x =>
                                       new QMS021
                                       {
                                           HeaderId = objSrcQMS020.HeaderId,
                                           QualityProject = objSrcQMS020.QualityProject,
                                           Project = objSrcQMS020.Project,
                                           BU = objSrcQMS020.BU,
                                           Location = objSrcQMS020.Location,
                                           SeamNo = objSrcQMS020.SeamNo,
                                           RevNo = objSrcQMS020.RevNo,
                                           StageCode = x.StageCode,
                                           StageSequance = x.StageSequance,//db.QMS016.Where(i => i.StageCode == x.StageCode).FirstOrDefault().SequenceNo,
                                           StageStatus = clsImplementationEnum.TestPlanStatus.Added.GetStringValue(),
                                           AcceptanceStandard = x.AcceptanceStandard,
                                           ApplicableSpecification = x.ApplicableSpecification,
                                           HardnessRequiredValue = x.HardnessRequiredValue,
                                           HardnessUnit = x.HardnessUnit,
                                           FerriteMaxValue = x.FerriteMaxValue,
                                           FerriteMinValue = x.FerriteMinValue,
                                           FerriteUnit = x.FerriteUnit,
                                           InspectionExtent = x.InspectionExtent,
                                           Remarks = x.Remarks,
                                           CreatedBy = objClsLoginInfo.UserName,
                                           CreatedOn = DateTime.Now,
                                       })
                                     );
                        db.SaveChanges();

                        var lstQidLines = lstSrcQMS016_Log.Select(x => x.LineId).Distinct().ToList();
                        #region Chemical/PMI
                        List<QMS018> lstSamplingplan = db.QMS018.Where(x => lstQidLines.Contains(x.QIDLineId.HasValue ? x.QIDLineId.Value : 0) && x.QualityIdRev == objQMS015.RevNo).ToList();
                        List<QMS022> lstQMS022 = new List<QMS022>();
                        foreach (var x in lstSamplingplan)
                        {
                            QMS022 objQMS022 = new QMS022();
                            objQMS022.TPLineId = objSrcQMS020.QMS021.Where(a => a.StageCode == x.StageCode && a.StageSequance == x.StageSequence).Select(xa => xa.LineId).FirstOrDefault();
                            objQMS022.Project = objSrcQMS020.Project;
                            objQMS022.QualityProject = objSrcQMS020.QualityProject;
                            objQMS022.BU = objSrcQMS020.BU;
                            objQMS022.Location = objSrcQMS020.Location;
                            objQMS022.SeamNo = objSrcQMS020.SeamNo;
                            objQMS022.TPRevNo = objSrcQMS020.RevNo;
                            objQMS022.StageCode = x.StageCode;
                            objQMS022.StageSequence = x.StageSequence;
                            objQMS022.Depth = x.Depth;
                            objQMS022.Component = x.Component;
                            objQMS022.NoOfComponents = x.NoOfComponents;
                            objQMS022.Procedure = x.Procedure;
                            objQMS022.NoOfProcedures = x.NoOfProcedures;
                            objQMS022.Welder = x.Welder;
                            objQMS022.NoOfWelders = x.NoOfWelders;
                            objQMS022.Position = x.Position;
                            objQMS022.NoOfPositions = x.NoOfPositions;
                            objQMS022.CreatedBy = objClsLoginInfo.UserName;
                            objQMS022.CreatedOn = DateTime.Now;

                            lstQMS022.Add(objQMS022);
                        }
                        db.QMS022.AddRange(lstQMS022);
                        db.SaveChanges();

                        List<QMS023> lstQMS023 = new List<QMS023>();
                        List<QMS019> lstElement = db.QMS019.Where(x => lstQidLines.Contains(x.QIDLineId.HasValue ? x.QIDLineId.Value : 0) && x.QualityIdRev == objQMS015.RevNo).ToList();
                        foreach (var x in lstElement)
                        {
                            QMS023 objQMS023 = new QMS023();
                            objQMS023.TPLineId = objSrcQMS020.QMS021.Where(a => a.StageCode == x.StageCode && a.StageSequance == x.StageSequence).Select(xa => xa.LineId).FirstOrDefault();
                            objQMS023.Project = objSrcQMS020.Project;
                            objQMS023.QualityProject = objSrcQMS020.QualityProject;
                            objQMS023.BU = objSrcQMS020.BU;
                            objQMS023.Location = objSrcQMS020.Location;
                            objQMS023.SeamNo = objSrcQMS020.SeamNo;
                            objQMS023.TPRevNo = objSrcQMS020.RevNo;
                            objQMS023.StageCode = x.StageCode;
                            objQMS023.StageSequence = x.StageSequence;
                            objQMS023.HeaderId = db.QMS022.Where(a => a.TPLineId == objQMS023.TPLineId && a.StageCode == x.StageCode && a.StageSequence == x.StageSequence).Select(a => a.HeaderId).FirstOrDefault();
                            objQMS023.Element = x.Element;
                            objQMS023.ReqMinValue = x.ReqMinValue;
                            objQMS023.ReqMaxValue = x.ReqMaxValue;
                            objQMS023.CreatedBy = objClsLoginInfo.UserName;
                            objQMS023.CreatedOn = DateTime.Now;
                            lstQMS023.Add(objQMS023);
                        }
                        db.QMS023.AddRange(lstQMS023);
                        db.SaveChanges();
                        #endregion

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.TestPlanMessages.LineUpdate.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                    }
                }
                else
                {
                    //List<QMS016_Log> lstSrcQMS016_Log = db.QMS016_Log.Where(x => x.HeaderId == objQMS16 && x.StageStatus == ApproveString).ToList();
                    List<QMS016_Log> lstSrcQMS016_Log = db.QMS016_Log.Where(x => x.RefId == objQMS015.Id && x.StageStatus == ApproveString).ToList();
                    if (lstSrcQMS016_Log != null)
                    {
                        db.QMS021.AddRange(
                                        lstSrcQMS016_Log.Select(x =>
                                        new QMS021
                                        {
                                            HeaderId = objSrcQMS020.HeaderId,
                                            QualityProject = objSrcQMS020.QualityProject,
                                            Project = objSrcQMS020.Project,
                                            BU = objSrcQMS020.BU,
                                            Location = objSrcQMS020.Location,
                                            SeamNo = objSrcQMS020.SeamNo,
                                            RevNo = objSrcQMS020.RevNo,
                                            StageCode = x.StageCode,
                                            StageSequance = x.StageSequance,// db.QMS002.Where(i => i.StageCode == x.StageCode).FirstOrDefault().SequenceNo,
                                            StageStatus = clsImplementationEnum.TestPlanStatus.Added.GetStringValue(),
                                            AcceptanceStandard = x.AcceptanceStandard,
                                            ApplicableSpecification = x.ApplicableSpecification,
                                            InspectionExtent = x.InspectionExtent,
                                            Remarks = x.Remarks,
                                            HardnessRequiredValue = x.HardnessRequiredValue,
                                            HardnessUnit = x.HardnessUnit,
                                            FerriteMaxValue = x.FerriteMaxValue,
                                            FerriteMinValue = x.FerriteMinValue,
                                            FerriteUnit = x.FerriteUnit,
                                            CreatedBy = objClsLoginInfo.UserName,
                                            CreatedOn = DateTime.Now,
                                        })
                                      );
                        db.SaveChanges();

                        var lstQidLines = lstSrcQMS016_Log.Select(x => x.LineId).Distinct().ToList();
                        #region Chemical/PMI
                        List<QMS018> lstSamplingplan = db.QMS018.Where(x => lstQidLines.Contains(x.QIDLineId.HasValue ? x.QIDLineId.Value : 0) && x.QualityIdRev == objQMS015.RevNo).ToList();
                        List<QMS022> lstQMS022 = new List<QMS022>();
                        foreach (var x in lstSamplingplan)
                        {
                            QMS022 objQMS022 = new QMS022();
                            objQMS022.TPLineId = objSrcQMS020.QMS021.Where(a => a.StageCode == x.StageCode && a.StageSequance == x.StageSequence).Select(xa => xa.LineId).FirstOrDefault();
                            objQMS022.Project = x.Project;
                            objQMS022.QualityProject = x.QualityProject;
                            objQMS022.BU = x.BU;
                            objQMS022.Location = x.Location;
                            objQMS022.SeamNo = objSrcQMS020.SeamNo;
                            objQMS022.TPRevNo = objSrcQMS020.RevNo;
                            objQMS022.StageCode = x.StageCode;
                            objQMS022.StageSequence = x.StageSequence;
                            objQMS022.Depth = x.Depth;
                            objQMS022.Component = x.Component;
                            objQMS022.NoOfComponents = x.NoOfComponents;
                            objQMS022.Procedure = x.Procedure;
                            objQMS022.NoOfProcedures = x.NoOfProcedures;
                            objQMS022.Welder = x.Welder;
                            objQMS022.NoOfWelders = x.NoOfWelders;
                            objQMS022.Position = x.Position;
                            objQMS022.NoOfPositions = x.NoOfPositions;
                            objQMS022.CreatedBy = objClsLoginInfo.UserName;
                            objQMS022.CreatedOn = DateTime.Now;
                            lstQMS022.Add(objQMS022);
                        }
                        db.QMS022.AddRange(lstQMS022);
                        db.SaveChanges();

                        List<QMS023> lstQMS023 = new List<QMS023>();
                        List<QMS019> lstElement = db.QMS019.Where(x => lstQidLines.Contains(x.QIDLineId.HasValue ? x.QIDLineId.Value : 0) && x.QualityIdRev == objQMS015.RevNo).ToList();
                        foreach (var x in lstElement)
                        {
                            QMS023 objQMS023 = new QMS023();
                            objQMS023.TPLineId = objSrcQMS020.QMS021.Where(a => a.StageCode == x.StageCode && a.StageSequance == x.StageSequence).Select(xa => xa.LineId).FirstOrDefault();
                            objQMS023.Project = objSrcQMS020.Project;
                            objQMS023.QualityProject = objSrcQMS020.QualityProject;
                            objQMS023.BU = objSrcQMS020.BU;
                            objQMS023.Location = objSrcQMS020.Location;
                            objQMS023.SeamNo = objSrcQMS020.SeamNo;
                            objQMS023.TPRevNo = objSrcQMS020.RevNo;
                            objQMS023.StageCode = x.StageCode;
                            objQMS023.StageSequence = x.StageSequence;
                            objQMS023.HeaderId = db.QMS022.Where(a => a.TPLineId == objQMS023.TPLineId && a.StageCode == x.StageCode && a.StageSequence == x.StageSequence).Select(a => a.HeaderId).FirstOrDefault();
                            objQMS023.Element = x.Element;
                            objQMS023.ReqMinValue = x.ReqMinValue;
                            objQMS023.ReqMaxValue = x.ReqMaxValue;
                            objQMS023.CreatedBy = objClsLoginInfo.UserName;
                            objQMS023.CreatedOn = DateTime.Now;
                            lstQMS023.Add(objQMS023);
                        }
                        db.QMS023.AddRange(lstQMS023);
                        db.SaveChanges();
                        #endregion

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.TestPlanMessages.HeaderInsert.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                    }
                }

                objResponseMsg.HeaderId = objSrcQMS020.HeaderId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return objResponseMsg;
        }

        //update line/stage data
        [HttpPost]
        public ActionResult UpdateLineData(int headerId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string tableName = string.Empty;
                //tableName = "QMS021";
                QMS020 objQMS020 = db.QMS021.Where(c => c.LineId == headerId).FirstOrDefault().QMS020;
                if (string.IsNullOrWhiteSpace(objQMS020.CreatedBy))
                {
                    objQMS020.CreatedBy = objClsLoginInfo.UserName;
                    objQMS020.CreatedOn = DateTime.Now;
                    db.SaveChanges();
                }
                if (!string.Equals(objQMS020.Status, clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                {
                    if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                    {
                        var dbResult = db.SP_IPI_TESTPLAN_LINE_UPDATE(headerId, columnName, columnValue).FirstOrDefault();
                        if (dbResult.Result.ToString() == "IS")
                        {
                            if (string.Equals(objQMS020.Status, clsImplementationEnum.TestPlanStatus.Approved.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                            {
                                objQMS020.Status = clsImplementationEnum.TestPlanStatus.Draft.GetStringValue();
                                objQMS020.RevNo = objQMS020.RevNo + 1;
                                objQMS020.EditedBy = objClsLoginInfo.UserName;
                                objQMS020.EditedOn = DateTime.Now;

                                objQMS020.SubmittedBy = null;
                                objQMS020.SubmittedOn = null;
                                objQMS020.ApprovedBy = null;
                                objQMS020.ApprovedOn = null;
                                objQMS020.ReturnedBy = null;
                                objQMS020.ReturnedOn = null;
                                objQMS020.ReturnRemark = null;
                                if (objQMS020.QMS021.Any())
                                {
                                    objQMS020.QMS021.ToList().ForEach(i => i.RevNo = objQMS020.RevNo);
                                }
                                db.SaveChanges();
                            }

                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                        }
                    }
                    objResponseMsg.HeaderStatus = objQMS020.Status;
                    objResponseMsg.RevNo = "R" + objQMS020.RevNo;
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Remarks = clsImplementationMessage.CommonMessages.NotInEditStatus;
                    objResponseMsg.HeaderStatus = objQMS020.Status;
                    objResponseMsg.RevNo = "R" + objQMS020.RevNo;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //delete from specific header (quality id,ptc number,ptc applicable)
        [HttpPost]
        public ActionResult DeleteQualtiyId(int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (headerId > 0)
                {
                    QMS020 objQMS020 = new QMS020();
                    QMS021 objQMS021 = new QMS021();
                    objQMS020 = db.QMS020.Where(x => x.HeaderId == headerId).FirstOrDefault();
                    if (objQMS020.Status == clsImplementationEnum.TestPlanStatus.Draft.GetStringValue() || objQMS020.Status == clsImplementationEnum.TestPlanStatus.Returned.GetStringValue())
                    {
                        List<QMS021> lstStages = db.QMS021.Where(x => x.HeaderId == headerId).ToList();

                        if (lstStages != null && lstStages.Count > 0)
                        {
                            List<int> lstTPLines = lstStages.Select(a => a.LineId).ToList();
                            #region Chem/Hardness/PMi/Ferrite                    
                            List<QMS023> lstDesQMS023 = db.QMS023.Where(x => lstTPLines.Contains(x.TPLineId.HasValue ? x.TPLineId.Value : 0)).ToList();
                            if (lstDesQMS023 != null && lstDesQMS023.Count > 0)
                            {
                                db.QMS023.RemoveRange(lstDesQMS023);
                            }

                            List<QMS022> lstDesQMS022 = db.QMS022.Where(x => lstTPLines.Contains(x.TPLineId.HasValue ? x.TPLineId.Value : 0)).ToList();
                            if (lstDesQMS022 != null && lstDesQMS022.Count > 0)
                            {
                                db.QMS022.RemoveRange(lstDesQMS022);
                            }
                            #endregion


                            db.QMS021.RemoveRange(lstStages);
                        }
                        db.SaveChanges();
                        objQMS020.QualityId = null;
                        objQMS020.QualityIdRev = null;
                        objQMS020.PTCApplicable = false;
                        objQMS020.PTCNumber = "";
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                    }
                    else
                    {
                        objResponseMsg.Remarks = "This data is already submitted, it cannot be delete now!!";
                    }
                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.DuplicateQID.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SendForApproval(string strHeader)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();

            List<QMS020> lstQualityIds = new List<QMS020>();
            List<string> lstQIDWithoutLines = new List<string>();
            List<string> lstQIDNotDraft = new List<string>();
            List<string> lstQIDSubmitted = new List<string>();
            List<string> lstNotApplicableForParallel = new List<string>();
            List<string> lstMaintainSamplingplan = new List<string>();
            List<string> lstMaintainHardness = new List<string>();
            List<string> lstMaintainFerrite = new List<string>();
            List<string> lstMaintainPMI = new List<string>();
            try
            {
                if (!string.IsNullOrEmpty(strHeader))
                {
                    int[] headerIds = Array.ConvertAll(strHeader.Split(','), s => int.Parse(s));
                    lstQualityIds = db.QMS020.Where(i => headerIds.Contains(i.HeaderId)).ToList();
                    string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
                    string MissingStage = string.Empty;
                    clsManager clsMgr = new clsManager();
                    //db.Configuration.AutoDetectChangesEnabled = false;
                    for (int i = 0; i < lstQualityIds.Count; i++)
                    {
                        var qms20 = lstQualityIds[i];

                        #region Send for Approval
                        if (string.Equals(qms20.Status, clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue()) || string.Equals(qms20.Status, clsImplementationEnum.TestPlanStatus.Returned.GetStringValue()))
                        {
                            if (qms20.QMS021.Where(x => x.StageStatus != deleted).Any())
                            {
                                if (Manager.IsApplicableForParallelStage(qms20.Project, qms20.QualityProject, qms20.BU, qms20.Location, qms20.SeamNo)) // pending che
                                {
                                    MissingStage = string.Empty;
                                    SaveStageTypewiseData(qms20, out MissingStage);

                                    if (string.IsNullOrWhiteSpace(MissingStage))
                                    {
                                        qms20.Status = clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue();
                                        qms20.SubmittedBy = objClsLoginInfo.UserName;
                                        qms20.SubmittedOn = DateTime.Now;
                                        qms20.ReturnedBy = null;
                                        qms20.ReturnedOn = null;
                                        qms20.ReturnRemark = null;
                                        qms20.QMS021.ToList().ForEach(x => { x.SubmittedBy = objClsLoginInfo.UserName; x.SubmittedOn = DateTime.Now; });
                                        db.SaveChanges();

                                        #region Send Notification
                                        clsMgr.SendNotification(clsImplementationEnum.UserRoleName.WE2.GetStringValue(), qms20.Project, qms20.BU, qms20.Location, "Test Plan for Seam: " + qms20.SeamNo + " of Project: " + qms20.QualityProject + "  has been submitted for your Approval", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/ApproveTestPlan/TestPlanDetails?HeaderID=" + qms20.HeaderId);
                                        #endregion

                                        lstQIDSubmitted.Add(qms20.SeamNo);
                                    }
                                    else
                                    {
                                        if (MissingStage == clsImplementationEnum.SeamStageType.Chemical.GetStringValue())
                                        {
                                            lstMaintainSamplingplan.Add(qms20.SeamNo);
                                        }
                                        else if (MissingStage == clsImplementationEnum.SeamStageType.PMI.GetStringValue())
                                        {
                                            lstMaintainPMI.Add(qms20.SeamNo);
                                        }
                                        else if (MissingStage == clsImplementationEnum.SeamStageType.Hardness.GetStringValue())
                                        {
                                            lstMaintainHardness.Add(qms20.SeamNo);
                                        }
                                        else if (MissingStage == clsImplementationEnum.SeamStageType.Ferrite.GetStringValue())
                                        {
                                            lstMaintainFerrite.Add(qms20.SeamNo);
                                        }
                                    }
                                }
                                else
                                {
                                    lstNotApplicableForParallel.Add(qms20.SeamNo);
                                }
                            }
                            else
                            {
                                lstQIDWithoutLines.Add(qms20.SeamNo);
                            }
                        }
                        else
                        {
                            lstQIDNotDraft.Add(qms20.SeamNo);
                        }
                        #endregion

                    }
                    //db.ChangeTracker.DetectChanges();
                    //db.Configuration.AutoDetectChangesEnabled = true;
                    objResponseMsg.Key = true;
                    if (lstQIDSubmitted.Any())
                    {
                        objResponseMsg.Value = string.Format(clsImplementationMessage.TestPlanMessages.SentForApproval, lstQIDSubmitted.Count);
                    }
                    if (lstQIDWithoutLines.Any())
                    {
                        objResponseMsg.WithoutLines = string.Format("No Stage(s) Exist(s) For Seam :{0}.Please Add Stages.", string.Join(",", lstQIDWithoutLines));
                    }
                    if (lstQIDNotDraft.Any())
                    {
                        objResponseMsg.NotDraft = string.Format("Seam {0} have been skipped as Test plan {0} are either Sent For Approval Or Approved", string.Join(",", lstQIDNotDraft));
                    }
                    if (lstNotApplicableForParallel.Any())
                    {
                        objResponseMsg.NotApplicableForParallel = string.Format("Seam {0} have been skipped as PT/MT cannot be the parallel stages with RT/UT", string.Join(",", lstNotApplicableForParallel));
                    }
                    if (lstMaintainSamplingplan.Any())
                    {
                        objResponseMsg.Remarks = string.Format("Seam {0} have been skipped.Please 'Maintain Sampling Plan and Element for Chemical Stages'.", string.Join(",", lstMaintainSamplingplan));
                    }
                    if (lstMaintainHardness.Any())
                    {
                        objResponseMsg.dataValue = string.Format("Seam {0} have been skipped.Please 'Maintain hardness required Value'.", string.Join(",", lstMaintainHardness));
                    }
                    if (lstMaintainFerrite.Any())
                    {
                        objResponseMsg.TPISkipped = string.Format("Seam {0} have been skipped.Please 'Maintain ferrite test required range'.", string.Join(",", lstMaintainFerrite));
                    }
                    if (lstMaintainPMI.Any())
                    {
                        objResponseMsg.tpi = string.Format("Seam {0} have been skipped.Please 'Maintain Sampling Plan and Element for PMI Stages'.", string.Join(",", lstMaintainPMI));
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please Select Test Plan for sending for approval";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region old code
        [HttpPost]
        public JsonResult LoadTestPlanLineGridData_Old(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "";
                strWhere += Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();
                //string[] arrayCTQStatus = { clsImplementationEnum.CTQStatus.DRAFT.GetStringValue(), clsImplementationEnum.CTQStatus.Returned.GetStringValue() };

                string[] columnName = { "StageCode", "StageSequance", "AcceptanceStandard", "ApplicableSpecification", "InspectionExtent", "RevNo", "StageStatus", "Remarks", "LineId", };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);

                var lstResult = db.SP_IPI_TEST_PLAN_LINEDETAILS
                                (
                               StartIndex,
                               EndIndex,
                               "",
                               strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                //Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.StageCode),
                                Convert.ToString(uc.StageSequance),
                                Convert.ToString(uc.AcceptanceStandard),
                                Convert.ToString(uc.ApplicableSpecification),
                                Convert.ToString(uc.InspectionExtent),
                                Convert.ToString(uc.RevNo),
                                Convert.ToString(uc.StageStatus),
                                Convert.ToString(uc.Remarks),
                                Convert.ToString(uc.LineId),
                           }).ToList();


                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        //save test plan Stages (before editable grid)
        [HttpPost]
        public ActionResult SaveTestPlanLines(FormCollection fc, QMS021 qms021)
        {
            QMS021 objQMS021 = new QMS021();
            QMS020 objQMS020 = new QMS020();

            string addedStatus = clsImplementationEnum.TestPlanStatus.Added.GetStringValue();
            string modifiedStatus = clsImplementationEnum.TestPlanStatus.Modified.GetStringValue();
            string deletedStatus = clsImplementationEnum.TestPlanStatus.Deleted.GetStringValue();

            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                int headerID = qms021.HeaderId;
                bool IsEdited = false;
                objQMS020 = db.QMS020.Where(x => x.HeaderId == headerID).FirstOrDefault();
                if (qms021.LineId > 0)
                {
                    objQMS021 = db.QMS021.Where(x => x.LineId == qms021.LineId).FirstOrDefault();
                    IsEdited = true;
                }
                objQMS021.QualityProject = objQMS020.QualityProject;
                objQMS021.Project = objQMS020.Project;
                objQMS021.BU = objQMS020.BU;
                objQMS021.Location = objQMS020.Location;
                objQMS021.SeamNo = objQMS020.SeamNo;
                objQMS021.StageCode = qms021.StageCode;
                objQMS021.StageSequance = qms021.StageSequance;
                objQMS021.AcceptanceStandard = qms021.AcceptanceStandard;
                objQMS021.ApplicableSpecification = qms021.ApplicableSpecification;
                objQMS021.InspectionExtent = qms021.InspectionExtent;
                objQMS021.Remarks = qms021.Remarks;
                objQMS021.HeaderId = objQMS020.HeaderId;
                if (objQMS020.Status == clsImplementationEnum.ICLSeamStatus.Approved.GetStringValue())
                {
                    objQMS020.RevNo = Convert.ToInt32(objQMS020.RevNo) + 1;
                    objQMS020.Status = clsImplementationEnum.ICLSeamStatus.Draft.GetStringValue();
                    objQMS020.EditedBy = objClsLoginInfo.UserName;
                    objQMS020.EditedOn = DateTime.Now;
                }
                if (IsEdited)
                {
                    objQMS021.RevNo = objQMS020.RevNo;
                    objQMS021.StageStatus = modifiedStatus;
                    objQMS021.EditedBy = objClsLoginInfo.UserName;
                    objQMS021.EditedOn = DateTime.Now;
                    objQMS020.EditedBy = objClsLoginInfo.UserName;
                    objQMS020.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.TestPlanMessages.LineUpdate;
                }
                else
                {
                    objQMS021.RevNo = objQMS020.RevNo;
                    objQMS021.StageStatus = addedStatus;
                    objQMS021.CreatedBy = objClsLoginInfo.UserName;
                    objQMS021.CreatedOn = DateTime.Now;
                    db.QMS021.Add(objQMS021);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.TestPlanMessages.LineInsert;
                }
                objResponseMsg.Status = objQMS020.Status;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.TestPlanMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //public ActionResult LoadTestPlanLineData_old(int headerId, int lineId)
        //{
        //    QMS020 objQMS020 = new QMS020();
        //    QMS021 objQMS021 = new QMS021();
        //    NDEModels objNDEModels = new NDEModels();
        //    objQMS020 = db.QMS020.Where(x => x.HeaderId == headerId).FirstOrDefault();
        //    ViewBag.Stages = db.QMS002.Where(i => i.BU.Equals(objQMS020.BU, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(objQMS020.Location, StringComparison.OrdinalIgnoreCase)).Select(i => new CategoryData { Value = i.StageCode, Code = i.StageCode, CategoryDescription = i.StageCode + "-" + i.StageDesc }).ToList();
        //    var lstAcceptanceStandard = Manager.GetSubCatagories("Acceptance Standard", objQMS020.BU, objQMS020.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
        //    var lstApplicableSpecification = Manager.GetSubCatagories("Applicable Specification", objQMS020.BU, objQMS020.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
        //    var lstInspectionExtent = Manager.GetSubCatagories("Inspection Extent", objQMS020.BU, objQMS020.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
        //    ViewBag.AcceptanceStandard = lstAcceptanceStandard;
        //    ViewBag.ApplicableSpecification = lstApplicableSpecification;
        //    ViewBag.InspectionExtent = lstInspectionExtent;

        //    if (lineId > 0)
        //    {
        //        objQMS021 = db.QMS021.Where(x => x.LineId == lineId).FirstOrDefault();
        //        ViewBag.StageSelected = db.QMS002.Where(i => i.StageCode.Equals(objQMS021.StageCode, StringComparison.OrdinalIgnoreCase)).Select(i => i.StageCode + "-" + i.StageDesc).FirstOrDefault();
        //        ViewBag.AcceptanceStandardSelected = objNDEModels.GetCategory(objQMS021.AcceptanceStandard).CategoryDescription;
        //        ViewBag.ApplicableSpecificationSelected = objNDEModels.GetCategory(objQMS021.ApplicableSpecification).CategoryDescription;
        //        ViewBag.InspectionExtentSelected = objNDEModels.GetCategory(objQMS021.InspectionExtent).CategoryDescription;
        //        ViewBag.Action = "line edit";
        //    }


        //    ViewBag.QProject = objQMS020.QualityProject;
        //    ViewBag.BU = db.COM002.Where(a => a.t_dimx == objQMS020.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
        //    ViewBag.Project = db.COM001.Where(i => i.t_cprj == objQMS020.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
        //    ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objQMS020.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
        //    ViewBag.SeamNo = objQMS020.SeamNo;
        //    ViewBag.RevNo = objQMS020.RevNo;
        //    return PartialView("_LoadTPLinesDetailsForm", objQMS021);
        //}

        [HttpPost]
        public ActionResult UpdateTPHeader(int headerId, string qualityProject, string strQID, string changeText, string strFrom)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS020 objQMS020 = new QMS020();
                objQMS020 = db.QMS020.Where(x => x.HeaderId == headerId).FirstOrDefault();
                if (objQMS020 != null)
                {
                    if (strFrom.ToUpper() == "A")
                    {
                        if (objQMS020.QualityId != changeText)
                        {
                            strQID = changeText;
                            objQMS020.QualityId = changeText;
                            db.SaveChanges();
                            //objResponseMsg = SaveTPHeader(objQMS020, true);
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                        }
                    }
                    if (strFrom.ToUpper() == "B")
                    {
                        objQMS020.PTCApplicable = Convert.ToBoolean(changeText);
                        db.SaveChanges();
                    }
                    if (strFrom.ToUpper() == "C")
                    {
                        objQMS020.PTCNumber = changeText;
                        db.SaveChanges();
                    }
                    if (strFrom.ToUpper() == "B" || strFrom.ToUpper() == "C")
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.TestPlanMessages.HeaderUpdate;
                    }
                    if (strFrom.ToUpper() == "A")
                    {
                        if (objResponseMsg.Key == false)
                        {
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.DuplicateQID.ToString();
                        }
                        else
                        {
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public string GetPTCApplicable(bool? ptcApplicable, string status)
        {
            string[] ptcYesNo = clsImplementationEnum.getyesno().ToArray();
            string htmlOption = "<option value=''>Select PTC Applicable</option>";
            string strSelectedValue;
            if (ptcApplicable == true)
            {
                strSelectedValue = "Yes";
            }
            else
            {
                strSelectedValue = "No";
            }
            for (int i = 0; i < ptcYesNo.Length; i++)
            {
                if (ptcApplicable != null && strSelectedValue == "Yes")
                {
                    if (ptcYesNo[i] == "Yes")
                    {
                        htmlOption += "<option selected value='" + ptcYesNo[i] + "'>" + ptcYesNo[i] + "</option>";
                    }
                    else
                    {
                        htmlOption += "<option value='" + ptcYesNo[i] + "'>" + ptcYesNo[i] + "</option>";
                    }
                }
                else
                {
                    if (ptcYesNo[i] == "No")
                    {
                        htmlOption += "<option selected value='" + ptcYesNo[i] + "'>" + ptcYesNo[i] + "</option>";
                    }
                    else
                    {
                        htmlOption += "<option value='" + ptcYesNo[i] + "'>" + ptcYesNo[i] + "</option>";
                    }
                }
            }
            if (status != clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue())
            {
                return "<select name='ddlptcApplicable' class='form-control input-sm input-small input-inline'>" + htmlOption + "</select>";
            }
            else
            {
                return "<select name='ddlptcApplicable' disabled class='form-control input-sm input-small input-inline'>" + htmlOption + "</select>";
            }
        }

        public string GetPTCNumber_old(string ptcNumber, string status)
        {
            if (ptcNumber != null)
            {
                return "<input type='text' readonly name='txtptcNumber' class='form-control input-sm input-small input-inline' value='" + ptcNumber + "' />";
            }
            else
            {
                return "<input type='text' readonly name='txtptcNumber' class='form-control input-sm input-small input-inline' value='" + ptcNumber + "' />";
            }
        }
        [HttpPost]
        public ActionResult DeleteQID(string strHeader)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            QMS020 objQMS020 = new QMS020();
            List<QMS020> lstQMS020 = new List<QMS020>();
            List<QMS021> lstQMS021 = new List<QMS021>();

            int inQIDDeleted = 0, inQIDWithLines = 0;

            try
            {
                if (!string.IsNullOrEmpty(strHeader))
                {
                    int[] headerIds = Array.ConvertAll(strHeader.Split(','), i => Convert.ToInt32(i));
                    foreach (var headerId in headerIds)
                    {
                        if (headerId > 0)
                        {
                            objQMS020 = db.QMS020.Where(i => i.HeaderId == headerId).FirstOrDefault();
                            #region Revision exists
                            if (objQMS020.RevNo > 0)
                            {
                                var lstLines = db.QMS021.Where(i => i.HeaderId == headerId).ToList();
                                if (lstLines.Any())
                                    db.QMS021.RemoveRange(lstLines);

                                //var lstIdenticalProject = db.QMS017.Where(i => i.HeaderId == headerId).ToList();
                                //if (lstIdenticalProject.Any())
                                //    db.QMS017.RemoveRange(lstIdenticalProject);
                                //db.QMS020.Remove(objQMS020);

                                db.SaveChanges();

                                #region Restoring Previous Version

                                var histHeader = db.QMS020_log.OrderByDescending(x => x.RevNo).FirstOrDefault();
                                var histLines = db.QMS021_log.Where(i => i.RefId == histHeader.Id && i.HeaderId == headerId).ToList();

                                objQMS020.HeaderId = histHeader.HeaderId;
                                objQMS020.QualityProject = histHeader.QualityProject;
                                objQMS020.Project = histHeader.Project;
                                objQMS020.BU = histHeader.BU;
                                objQMS020.Location = histHeader.Location;
                                objQMS020.QualityId = histHeader.QualityId;
                                objQMS020.QualityIdRev = histHeader.QualityIdRev;
                                objQMS020.PTCNumber = histHeader.PTCNumber;
                                objQMS020.PTCApplicable = histHeader.PTCApplicable;
                                objQMS020.RevNo = histHeader.RevNo + 1;
                                objQMS020.Status = clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue();
                                objQMS020.CreatedBy = objClsLoginInfo.UserName;
                                objQMS020.CreatedOn = DateTime.Now;

                                db.SaveChanges();

                                db.QMS021.AddRange(db.QMS021_log.Where(i => i.RefId == histHeader.Id && headerId == histHeader.HeaderId).Select(i => new QMS021
                                {
                                    LineId = i.LineId,
                                    HeaderId = histHeader.HeaderId,
                                    QualityProject = histHeader.QualityProject,
                                    Project = histHeader.Project,
                                    BU = histHeader.BU,
                                    Location = histHeader.Location,
                                    RevNo = objQMS020.RevNo,
                                    StageCode = i.StageCode,
                                    StageSequance = i.StageSequance,
                                    StageStatus = clsImplementationEnum.QualityIdStageStatus.ADDED.GetStringValue(),
                                    AcceptanceStandard = i.AcceptanceStandard,
                                    ApplicableSpecification = i.ApplicableSpecification,
                                    InspectionExtent = i.InspectionExtent,
                                    CreatedBy = objClsLoginInfo.UserName,
                                    CreatedOn = DateTime.Now
                                }).ToList());

                                //var histIdenticalProj = db.QMS017_Log.Where(i => i.RefId == histHeader.Id && headerId == histHeader.HeaderId).ToList();
                                //if (histIdenticalProj.Any())
                                //{
                                //    db.QMS017.AddRange(histIdenticalProj.Select(i => new QMS017
                                //    {
                                //        LineId = i.LineId,
                                //        HeaderId = objQMS020.HeaderId,
                                //        QualityProject = i.QualityProject,
                                //        Project = i.Project,
                                //        BU = i.BU,
                                //        Location = i.Location,
                                //        //QualityId = i.QualityId,
                                //        RevNo = objQMS020.RevNo,
                                //        IQualityProject = i.IQualityProject,
                                //        CreatedBy = objClsLoginInfo.UserName,
                                //        CreatedOn = DateTime.Now
                                //    }));
                                //}
                                #endregion

                                db.SaveChanges();
                                inQIDDeleted++;

                            }
                            #endregion
                            else
                            {
                                var lstLines = db.QMS021.Where(i => i.HeaderId == headerId).ToList();
                                if (lstLines.Any())
                                {
                                    List<QMS021> lstStages = db.QMS021.Where(x => x.HeaderId == headerId).ToList();
                                    if (lstStages != null)
                                    {
                                        db.QMS021.RemoveRange(lstLines);

                                        db.QMS020.Remove(objQMS020);
                                        db.SaveChanges();
                                        inQIDDeleted++;
                                    }
                                    else
                                    {
                                        inQIDWithLines++;
                                    }
                                }
                                else
                                {

                                    db.QMS020.Remove(objQMS020);
                                    db.SaveChanges();
                                    inQIDDeleted++;
                                }

                                //if (hasLines)
                                //{
                                //    var lstLines = db.QMS021.Where(i => i.HeaderId == headerId).ToList();
                                //    if (lstLines.Any())
                                //        db.QMS021.RemoveRange(lstLines);
                                //    //lstQMS021.AddRange(lstLines);
                                //    db.QMS020.Remove(objQMS020);
                                //    db.SaveChanges();
                                //    inQIDDeleted++;
                                //}
                                //else
                                //{
                                //    var lstLines = db.QMS021.Where(i => i.HeaderId == headerId).ToList();
                                //    if (lstLines.Any())
                                //    { }
                                //    else
                                //    { }
                                //    db.QMS020.Remove(objQMS020);
                                //    db.SaveChanges();
                                //    //inQIDDeleted++;
                                //}
                                //db.QMS020.Remove(objQMS020);
                                //db.SaveChanges();
                            }
                        }
                    }

                    if (inQIDWithLines > 0)
                    {
                        objResponseMsg.Status = string.Format("{0} QID(s) is/are having lines.", inQIDWithLines);
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = string.Format(clsImplementationMessage.QualityIDMessage.Delete, inQIDDeleted);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }


            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult LoadTPHeaderData_old(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += " 1=1 and status in('" + clsImplementationEnum.TestPlanStatus.Draft.GetStringValue() + "')";
                }
                else
                {
                    strWhere += " 1=1";
                }
                string[] columnName = { "ST2.QualityProject", "ST2.Location", "ST2.QualityId", "ST2.PTCApplicable", "ST2.PTCNumber", "ST2.Status" };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.Location, "ST2.BU", "ST2.Location");
                var lstResult = db.SP_TPlan_Get_HeaderList
                                (
                                StartIndex, EndIndex, "", strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.QualityProject),
                            Convert.ToString(uc.Location),
                            Convert.ToString(uc.SeamNo),
                            GetQID(uc.QualityIds,uc.Status,uc.QualityId),
                            GetPTCApplicable(uc.PTCApplicable,uc.Status),
                           // GetPTCNumber(uc.PTCNumber,uc.Status),
                            Convert.ToString(uc.Status),
                            Convert.ToString(uc.HeaderId),
                            Convert.ToString(uc.QualityId),
                            Convert.ToString(uc.PTCApplicable),

                            "<center><a class='btn btn-xs green' href='" + WebsiteURL + "/IPI/TestPlan/AddHeader?HeaderID="+uc.HeaderId+"'>View<i style='margin-left:5px;' class='fa fa-eye'></i></a></center>",
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public string GetQID(string strQID, string status, string selectedQID)
        {
            string[] qualityID = null;
            string htmlOption = "<option value=''> Select Quality Id </option>";
            string htmlValue = null;
            if (!string.IsNullOrWhiteSpace(strQID))
            {
                qualityID = strQID.Split(',');
                for (int i = 0; i < qualityID.Length; i++)
                {
                    htmlValue = qualityID[i].Split('|')[0];
                    if (selectedQID != null)
                    {
                        if (htmlValue.Trim() == selectedQID.Trim())
                        {
                            htmlOption += "<option selected value=" + htmlValue + ">" + qualityID[i] + "</option>";
                        }
                        else
                        {
                            htmlOption += "<option value='" + htmlValue + "'>" + qualityID[i] + "</option>";
                        }
                    }
                    else
                    {
                        htmlOption += "<option value='" + htmlValue + "'>" + qualityID[i] + "</option>";
                    }
                }
                if (status != clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue())
                {
                    return "<select name='txtQID'  class='form-control input-sm input-small input-inline'>" + htmlOption + "</select>";
                }
                else
                {
                    return "<select name='txtQID' disabled class='form-control input-sm input-small input-inline'>" + htmlOption + "</select>";
                }
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(selectedQID))
                {
                    htmlValue = selectedQID.Split('|')[0];
                    htmlOption += "<option selected value='" + htmlValue + "'>" + selectedQID + "</option>";
                }
                if (status != clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue())
                {
                    return "<select name='txtQID' class='form-control input-sm input-small input-inline'>" + htmlOption + "</select>";
                }
                else { return "<select name='txtQID' disabled class='form-control input-sm input-small input-inline'>" + htmlOption + "</select>"; }
            }
        }
        [HttpPost]
        public ActionResult SendForApproval_old(int strHeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS020 objQMS020 = db.QMS020.Where(u => u.HeaderId == strHeaderId).SingleOrDefault();
                if (objQMS020 != null)
                {
                    objQMS020.Status = clsImplementationEnum.CommonStatus.SendForApprovel.GetStringValue();
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.sentforApprove.ToString();
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region autocomplete,datatable html functions,common functions

        public bool isStageLinkedInLTFPS(int LineID)
        {
            bool flag = false;
            var objQMS021 = db.QMS021.Where(x => x.LineId == LineID).FirstOrDefault();
            if (objQMS021 != null)
            {
                string deleted = clsImplementationEnum.LTFPSLineStatus.Deleted.GetStringValue();
                if (db.LTF002.Any(x => x.QualityProject.Equals(objQMS021.QualityProject) && x.Location.Equals(objQMS021.Location) && x.BU.Equals(objQMS021.BU) && x.SeamNo.Equals(objQMS021.SeamNo) && x.Stage.Equals(objQMS021.StageCode) && x.StageSequence == objQMS021.StageSequance && x.LineStatus != deleted))
                {
                    flag = true;
                }
            }
            return flag;
        }

        public string GenerateAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string maxlength = "")
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onchange='" + onBlurMethod + "'" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' hdElement='" + hdElementId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + "  " + MaxCharcters + "  " + (disabled ? "disabled" : "") + " />";

            return strAutoComplete;
        }

        public string GenerateAutoCompleteonBlur(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string maxlength = "")
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' hdElement='" + hdElementId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + "  " + MaxCharcters + "  " + (disabled ? "disabled" : "") + " />";

            return strAutoComplete;
        }

        [HttpPost]
        public bool checkStageExist(string stagecode, int header)
        {
            bool Flag = false;
            if (!string.IsNullOrWhiteSpace(stagecode))
            {
                QMS021 objCTQ001 = db.QMS021.Where(x => x.StageCode == stagecode && x.HeaderId == header).FirstOrDefault();
                if (objCTQ001 != null)
                {
                    Flag = true;
                }
            }
            return Flag;
        }

        [HttpPost]
        public bool GetSquence(string stagecode, int seq, int headerId)
        {
            bool Flag = false;
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            QMS002 objQMS002 = new QMS002();
            string strStage = stagecode;
            if (!string.IsNullOrWhiteSpace(stagecode))
            {
                QMS021 QMS021 = db.QMS021.Where(i => i.StageCode == stagecode && i.StageSequance == seq && i.HeaderId == headerId).FirstOrDefault();
                if (QMS021 != null)
                {
                    Flag = true;
                }
            }
            return Flag;
        }

        [HttpPost]
        public ActionResult GetStageSquence(string stagecode, int headerId)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            objResponseMsg.Key = false;
            string strStage = stagecode.Trim();
            if (!string.IsNullOrWhiteSpace(stagecode))
            {
                QMS020 objQMS20 = db.QMS020.Where(i => i.HeaderId == headerId).FirstOrDefault();
                QMS002 objQMS002 = db.QMS002.Where(i => i.StageCode == stagecode && i.Location == objQMS20.Location && i.BU == objQMS20.BU).FirstOrDefault();

                if (objQMS002 != null)
                {
                    objResponseMsg.Sequence = Convert.ToInt32(objQMS002.SequenceNo);
                    objResponseMsg.Key = true;
                }
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CheckDuplicateStages(string stagecode, int headerId, int stageseq)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();

            if (CheckDuplicateStage(headerId, stagecode, stageseq))
            {
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.TestPlanMessages.DuplicateStage;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //    objResponseMsg.Sequence = Convert.ToInt32(Sequence);
        //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        public ActionResult ReviseHeader(int headerid, int lineid)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                QMS020 objQMS020 = db.QMS020.Where(x => x.HeaderId == headerid).FirstOrDefault();
                if (objQMS020 != null)
                {
                    if (objQMS020.Status == clsImplementationEnum.ICLSeamStatus.Approved.GetStringValue())
                    {
                        objQMS020.RevNo = Convert.ToInt32(objQMS020.RevNo) + 1;
                        objQMS020.Status = clsImplementationEnum.ICLSeamStatus.Draft.GetStringValue();
                        objQMS020.EditedBy = objClsLoginInfo.UserName;
                        objQMS020.EditedOn = DateTime.Now;
                        objQMS020.SubmittedBy = null;
                        objQMS020.SubmittedOn = null;
                        objQMS020.ApprovedBy = null;
                        objQMS020.ApprovedOn = null;
                        objQMS020.ReturnedBy = null;
                        objQMS020.ReturnedOn = null; objQMS020.ReturnRemark = null;
                        objResponseMsg.HeaderStatus = objQMS020.Status;
                        objResponseMsg.RevNo = "R" + objQMS020.RevNo;
                    }
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult GetSeamResultforCopy(string term)
        {
            var lstSeam2 = (from a in db.QMS020
                            where a.SeamNo.Contains(term)
                            orderby a.HeaderId
                            select new { seamNo = a.SeamNo, seamDesc = a.SeamNo }).Distinct().ToList();
            return Json(lstSeam2, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSeamResultforTransfer(string term, string qualityProject)
        {
            var lstSeam2 = (from a in db.QMS020
                            where a.SeamNo.Contains(term) && a.QualityProject.Contains(qualityProject)
                            orderby a.HeaderId
                            select new { seamNo = a.SeamNo, seamDesc = a.SeamNo }).Distinct().ToList();
            return Json(lstSeam2, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetQualityProjectResult(string term)
        {
            object lstQualityProject;

            if (!string.IsNullOrEmpty(term))
            {
                lstQualityProject = (from a in db.QMS020
                                     where a.QualityProject.Contains(term) && a.Location.Contains(objClsLoginInfo.Location)
                                     orderby a.QualityProject
                                     select new { Value = a.QualityProject, projectCode = a.QualityProject }).Distinct().ToList();
            }
            else
            {
                lstQualityProject = (from a in db.QMS020
                                     where a.Location.Contains(objClsLoginInfo.Location)
                                     orderby a.QualityProject
                                     select new { Value = a.QualityProject, projectCode = a.QualityProject }).Distinct().ToList();

            }

            return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetSeamsForDelete(JQueryDataTableParamModel param)
        {
            try
            {
                string strWhere = "";
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayLength;

                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;
                string[] columnName = { "SeamNo" };

                string[] splitSeamCategory = param.SeamCategory.Split('-');
                string seamCategory = splitSeamCategory[0].Trim();
                string weldType = splitSeamCategory[1].Trim();

                strWhere = "WHERE Q20.qualityproject = '" + param.QualityProject + "'  AND Q20.Status in ('Draft','Returned','Approved') AND Q21.StageStatus<>'Deleted' AND Q20.Location in('" + param.Location + "')";
                strWhere += "AND Q20.SeamNo in (SELECT Q12.SeamNo FROM QMS012 AS Q12 WHERE seamCategory = '" + seamCategory + "' AND Q12.WeldType='" + weldType + "')";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " AND (Q20.SeamNo LIKE '%' +'" + param.sSearch + "'+'%'"
                    + " OR Q20.QualityId LIKE '%' +'" + param.sSearch + "'+'%'"
                    + " OR Q21.StageSequance LIKE '%' +'" + param.sSearch + "'+'%')";
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                else
                {
                    strSortOrder = " Order By Q20.SeamNo desc ";
                }

                string query = "SELECT Count(Q21.LineId) OVER() AS TotalCount"
                                + ",Q21.HeaderID AS HeaderID"
                                + ",Q21.StageCode"
                                + ",Q20.SeamNo AS SeamNo,"
                                + "QualityId"
                                + ",(SELECT TOP 1 Q15.QualityIdDesc"
                                + " FROM QMS015_Log Q15"
                                + " WHERE Q15.QualityIdApplicableFor = 'Seam'"
                                + " AND Q15.Project = Q20.QualityProject"
                                + " AND Q15.Location = Q20.Location"
                                + " AND Q15.Status = Q20.Status and Q15.QualityId = Q20.QualityId) AS QulityDesc"
                                + ",Q21.StageSequance AS StageSequance "
                                + "FROM QMS020 AS Q20 "
                                + "JOIN QMS021 AS Q21 on Q21.HeaderId = Q20.HeaderId AND Q21.StageCode='" + param.StageCode + "' "
                                + strWhere
                                + strSortOrder
                                + " OFFSET " + (StartIndex - 1) + " ROWS"
                                + " FETCH NEXT " + EndIndex + " ROWS ONLY";

                var lstResult = db.Database.SqlQuery<SeamMultipleAddandDeleteEntity>(query).ToList();

                int? totalCount = lstResult.Select(x => x.TotalCount).FirstOrDefault();
                int? totalRecords = lstResult.Count();
                var res = from a in lstResult
                          select new[] {
                          Convert.ToString(a.HeaderID),
                          a.StageCode,
                          a.SeamNo,
                          a.QualityId,
                          a.QulityDesc,
                          Convert.ToString(a.StageSequance)};

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalCount != null && totalCount > 0 ? totalCount : 0,
                    iTotalRecords = totalCount != null && totalCount > 0 ? totalCount : 0,
                    aaData = res
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult GetSeamsForAdd(JQueryDataTableParamModel param)
        {
            try
            {
                string strWhere = "";
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayLength;

                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;
                string[] columnName = { "SeamNo" };

                string[] splitSeamCategory = param.SeamCategory.Split('-');
                string seamCategory = splitSeamCategory[0].Trim();
                string weldType = splitSeamCategory[1].Trim();

                strWhere = "WHERE Q20.qualityproject = '" + param.QualityProject + "'  AND Q20.Status in ('Draft','Returned','Approved')  AND Q20.Location in('" + param.Location + "')";

                if (!string.IsNullOrEmpty(param.StageCode) && !string.IsNullOrEmpty(param.Sequenceno))
                {
                    strWhere += " AND not (Q21.StageCode = '" + param.StageCode + "' AND Q21.StageSequance != '" + param.Sequenceno + "') ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " AND (Q20.SeamNo LIKE '%' +'" + param.sSearch + "'+'%'"
                              + " OR Q20.QualityIdRev LIKE '%' +'" + param.sSearch + "'+'%'"
                              + " OR Q21.StageSequance LIKE '%' +'" + param.sSearch + "'+'%' " +
                              "OR Q20.RevNo LIKE '%' +'" + param.sSearch + "'+'%') ";
                }
                strWhere += "AND Q20.SeamNo in (SELECT Q12.SeamNo FROM QMS012 AS Q12 WHERE seamCategory = '" + seamCategory + "' AND Q12.WeldType='" + weldType + "')) AS a ";
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                else
                {
                    strSortOrder = " Order By Q20.SeamNo desc ";
                }

                string query = "SELECT COUNT(a.SeamNo) OVER() AS TotalCount,a.* FROM ("
                               + "SELECT DISTINCT Q20.SeamNo AS SeamNo,"
                               + "Q21.HeaderID AS HeaderID,"
                               + "(SELECT TOP 1 Q15.QualityIdDesc "
                               + "FROM QMS015_Log Q15"
                               + " WHERE Q15.QualityIdApplicableFor = 'Seam'"
                               + " AND Q15.Project = Q20.QualityProject"
                               + " AND Q15.Location = Q20.Location"
                               + " AND Q15.Status = Q20.Status"
                               + " AND Q15.QualityId = Q20.QualityId) AS QulityDesc,"
                               + " Q20.RevNo AS RevNo "
                               + " FROM QMS020 AS Q20 "
                               + " JOIN QMS021 AS Q21 on Q21.HeaderId = Q20.HeaderId "
                               + strWhere
                               + strSortOrder
                               + " OFFSET " + (StartIndex - 1) + " ROWS"
                               + " FETCH NEXT " + EndIndex + " ROWS ONLY";

                var lstResult = db.Database.SqlQuery<SeamMultipleAddandDeleteEntity>(query).ToList();

                int? totalCount = lstResult.Select(x => x.TotalCount).FirstOrDefault();
                int? totalRecords = lstResult.Count();
                var res = from a in lstResult
                          select new[] {
                          Convert.ToString(a.HeaderID),
                          a.SeamNo,
                          a.QulityDesc,
                          Convert.ToString(a.RevNo)};

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalCount != null && totalCount > 0 ? totalCount : 0,
                    iTotalRecords = totalCount != null && totalCount > 0 ? totalCount : 0,
                    aaData = res
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetSeamCategory(string qualityProject = "")
        {
            object lstSeamCategory = null;

            if (!string.IsNullOrEmpty(qualityProject))
            {
                var status = new List<string> { "Draft", "Returned", "Approved" };

                lstSeamCategory = db.QMS020.Where(w => w.QualityProject == qualityProject && status.Contains(w.Status)).Join(db.BOM003, T1 => new
                {
                    p1 = T1.WeldType,
                    p2 = T1.SeamType
                },
                 T2 => new
                 {
                     p1 = T2.JointTypeCode,
                     p2 = T2.SeamCategory
                 },
                 (T1, T2) => new
                 {
                     SeamCategory = T2.SeamCategory,
                     JointTypeCode = T2.JointTypeCode,
                     JointTypeDescription = T2.JointTypeDescription,
                     SeamCategoryDescription = T2.SeamCategoryDescription,
                     QualityProject = T1.QualityProject,
                     Value = T2.SeamCategory + "-" + T2.JointTypeCode,
                     projectCode = T2.SeamCategoryDescription + " - " + T2.JointTypeDescription
                 }).OrderBy(ob => ob.SeamCategory).ToList().
                 GroupBy(gb => new
                 {
                     gb.SeamCategory,
                     gb.JointTypeCode,
                     gb.JointTypeDescription,
                     gb.SeamCategoryDescription,
                     gb.QualityProject
                 }).Select(s => s.FirstOrDefault());


                var lstSeamCategoryTest = db.QMS020.Where(w => w.QualityProject == qualityProject).Join(db.BOM003, T1 => new
                {
                    p1 = T1.WeldType,
                    p2 = T1.SeamType
                },
                 T2 => new
                 {
                     p1 = T2.JointTypeCode,
                     p2 = T2.SeamCategory
                 },
                 (T1, T2) => new
                 {
                     SeamCategory = T2.SeamCategory,
                     JointTypeCode = T2.JointTypeCode,
                     JointTypeDescription = T2.JointTypeDescription,
                     SeamCategoryDescription = T2.SeamCategoryDescription,
                     QualityProject = T1.QualityProject,
                     Value = T2.SeamCategory + "-" + T2.JointTypeCode,
                     projectCode = T2.SeamCategoryDescription + " - " + T2.JointTypeDescription
                 }).OrderBy(ob => ob.SeamCategory).ToList().
                 GroupBy(gb => new
                 {
                     gb.SeamCategory,
                     gb.JointTypeCode,
                     gb.JointTypeDescription,
                     gb.SeamCategoryDescription,
                     gb.QualityProject
                 }).Select(s => s.FirstOrDefault());
            }

            return Json(lstSeamCategory, JsonRequestBehavior.AllowGet);
        }

        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", bool disabled = false)
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            //if (!string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
            //{
            if (disabled)
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='' Title='" + buttonTooltip + "' class='disabledicon " + className + "' ></i>";
            }
            else
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='' Title='" + buttonTooltip + "' class='iconspace " + className + "'" + onClickEvent + " ></i>";
            //}

            return htmlControl;
        }

        public string GetPTCNumber(string ptcNumber, string status, int headerid, bool ptcapplicable)
        {
            if (ptcNumber != null)
            {
                if (ptcapplicable)
                {
                    return Helper.GenerateTextbox(headerid, "PTCNumber", ptcNumber, "UpdateData(this, " + headerid + ");", string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()) ? true : false);
                }
                else
                {
                    return Helper.GenerateTextbox(headerid, "PTCNumber", ptcNumber, "UpdateData(this, " + headerid + ");", true);
                }
            }
            else
            {
                return "<input type='text' readonly name='txtptcNumber' class='form-control input-sm input-small input-inline' value='" + ptcNumber + "' />";
            }
        }
        #endregion

        [HttpPost]
        public ActionResult GetQualityProjectDetails(string term)
        {
            List<Projects> lstQualityProject = new List<Projects>();
            lstQualityProject = db.QMS020.Where(i => term == "" ? true : i.QualityProject.Contains(term)
                                                    ).Select(i => new Projects { Value = i.QualityProject, projectCode = i.QualityProject, Text = i.Location, BU = i.BU }).Distinct().Take(10).ToList();

            return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult FetchQualityProjectwiseDetails(string qualityProject, string location)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                var objQMS020 = db.QMS020.Where(i => i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase) && i.Location == location).FirstOrDefault();
                if (objQMS020 != null)
                {
                    objResponseMsg.ProjectCode = objQMS020.Project;
                    objResponseMsg.BUCode = objQMS020.BU;
                    objResponseMsg.LocationCode = objQMS020.Location;

                    objResponseMsg.Project = db.COM001.Where(i => i.t_cprj.Equals(objQMS020.Project.Trim(), StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + "-" + i.t_dsca).FirstOrDefault();// project;
                    objResponseMsg.BU = db.COM002.Where(x => x.t_dimx.Trim() == objQMS020.BU.Trim()).Select(x => x.t_desc).FirstOrDefault();
                    objResponseMsg.Location = db.COM002.Where(x => x.t_dimx.Trim() == objQMS020.Location.Trim()).Select(x => x.t_desc).FirstOrDefault();
                    objResponseMsg.TPHeaderId = objQMS020.HeaderId;
                    SWP010 objSWP010 = db.SWP010.Where(x => x.QualityProject == objQMS020.QualityProject && x.Project == objQMS020.Project && x.Location == objQMS020.Location && x.BU == objQMS020.BU).FirstOrDefault();
                    objResponseMsg.SWPHeaderId = objSWP010 != null ? objSWP010.HeaderId : 0;
                    Session["TP_SWP_SeamList"] = null;
                    Session["TP_SWP_PageSize"] = null;
                    Session["TP_SWP_Search"] = null;
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            if (LineId > 0)
            {
                QMS021 objQMS021 = db.QMS021.Where(x => x.LineId == LineId).FirstOrDefault();
                model.ApprovedBy = objQMS021.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objQMS021.ApprovedBy) : null;
                model.ApprovedOn = objQMS021.ApprovedOn;
                model.SubmittedBy = objQMS021.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objQMS021.SubmittedBy) : null;
                model.SubmittedOn = objQMS021.SubmittedOn;
                model.CreatedBy = objQMS021.CreatedBy != null ? Manager.GetUserNameFromPsNo(objQMS021.CreatedBy) : null;
                model.CreatedOn = objQMS021.CreatedOn;
                model.EditedBy = objQMS021.EditedBy != null ? Manager.GetUserNameFromPsNo(objQMS021.EditedBy) : null;
                model.EditedOn = objQMS021.EditedOn;
            }
            else
            {
                QMS020 objQMS020 = db.QMS020.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.ApprovedBy = objQMS020.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objQMS020.ApprovedBy) : null;
                model.ApprovedOn = objQMS020.ApprovedOn;
                model.SubmittedBy = objQMS020.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objQMS020.SubmittedBy) : null;
                model.SubmittedOn = objQMS020.SubmittedOn;
                model.CreatedBy = objQMS020.CreatedBy != null ? Manager.GetUserNameFromPsNo(objQMS020.CreatedBy) : null;
                model.CreatedOn = objQMS020.CreatedOn;
                model.EditedBy = objQMS020.EditedBy != null ? Manager.GetUserNameFromPsNo(objQMS020.EditedBy) : null;
                model.EditedOn = objQMS020.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress2.cshtml", model);
        }
        public ActionResult ShowLogTimeline(int id, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            if (LineId > 0)
            {

            }
            else
            {
                QMS020_log objQMS020log = db.QMS020_log.Where(x => x.Id == id).FirstOrDefault();
                model.ApprovedBy = objQMS020log.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objQMS020log.ApprovedBy) : null;
                model.ApprovedOn = objQMS020log.ApprovedOn;
                model.SubmittedBy = objQMS020log.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objQMS020log.SubmittedBy) : null;
                model.SubmittedOn = objQMS020log.SubmittedOn;
                model.CreatedBy = objQMS020log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objQMS020log.CreatedBy) : null;
                model.CreatedOn = objQMS020log.CreatedOn;
                model.EditedBy = objQMS020log.EditedBy != null ? Manager.GetUserNameFromPsNo(objQMS020log.EditedBy) : null;
                model.EditedOn = objQMS020log.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress2.cshtml", model);
        }
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            clsHelper.ResponceMsgWithFileName objexport = new clsHelper.ResponceMsgWithFileName();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.MAINTAININDEX.GetStringValue())
                {
                    List<SP_IPI_GET_TESTPLAN_INDEXRESULT_Result> lst = db.SP_IPI_GET_TESTPLAN_INDEXRESULT(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    List<SP_IPI_TEST_PLAN_HEADER_DETAILS_Result> lst = db.SP_IPI_TEST_PLAN_HEADER_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }

                    objexport = GenerateHeaderExcel(lst);
                    strFileName = objexport.FileName;//Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_IPI_TEST_PLAN_LINEDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      SeamNo = li.SeamNo,
                                      RevNo = li.RevNo,
                                      StageCode = li.StageCode,
                                      StageSequance = li.StageSequance,
                                      StageStatus = li.StageStatus,
                                      AcceptanceStandard = li.AcceptanceStandard,
                                      ApplicableSpecification = li.ApplicableSpecification,
                                      InspectionExtent = li.InspectionExtent,
                                      Remarks = li.Remarks,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {
                    var lst = db.SP_IPI_TEST_PLAN_HISTORY_HEADER_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      SeamNo = li.SeamNo,
                                      RevNo = li.RevNo,
                                      QualityId = li.QualityId,
                                      QualityIdRev = li.QualityIdRev,
                                      PTCApplicable = li.PTCApplicable,
                                      PTCNumber = li.PTCNumber,
                                      Status = li.Status,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else
                {
                    var lst = db.SP_IPI_TEST_PLAN_HISTORY_LINE_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      SeamNo = li.SeamNo,
                                      RevNo = li.RevNo,
                                      StageCode = li.StageCode,
                                      StageSequance = li.StageSequance,
                                      StageStatus = li.StageStatus,
                                      AcceptanceStandard = li.AcceptanceStandard,
                                      ApplicableSpecification = li.ApplicableSpecification,
                                      InspectionExtent = li.InspectionExtent,
                                      Remarks = li.Remarks,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [SessionExpireFilter]
        public ActionResult ImportExcel(HttpPostedFileBase upload, string aQualityProject, string aProject, string aBU, string aLocation)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            QMS020 objImport = new QMS020();
            objImport.QualityProject = aQualityProject.Trim();
            objImport.Project = aProject.Trim();
            objImport.BU = aBU.Trim();
            objImport.Location = aLocation.Trim();
            if (upload != null && !string.IsNullOrWhiteSpace(aProject))
            {
                if (Path.GetExtension(upload.FileName).ToLower() == ".xlsx" || Path.GetExtension(upload.FileName).ToLower() == ".xls")
                {
                    ExcelPackage package = new ExcelPackage(upload.InputStream);
                    ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();

                    DataTable dtHeaderExcel = new DataTable();
                    List<CategoryData> errors = new List<CategoryData>();

                    string Approved = clsImplementationEnum.TestPlanStatus.Approved.GetStringValue();
                    string Draft = clsImplementationEnum.TestPlanStatus.Draft.GetStringValue();
                    string Returned = clsImplementationEnum.TestPlanStatus.Returned.GetStringValue();

                    bool isError;
                    DataSet ds = ToChechValidation(package, objImport, out isError);
                    if (!isError)
                    {
                        try
                        {
                            foreach (DataRow item in ds.Tables[0].Rows)
                            {
                                QMS020 objQMS020 = new QMS020();
                                string SeamNo = item.Field<string>("SeamNo");
                                string QID = item.Field<string>("QID");
                                string PTCApplicable = item.Field<string>("PTC Applicable");
                                string PTCNumber = item.Field<string>("PTC Number");
                                objQMS020 = db.QMS020.Where(x => x.SeamNo.Trim() == SeamNo.Trim() && x.QualityProject.Trim() == objImport.QualityProject.Trim() && x.Project.Trim() == objImport.Project.Trim() && x.BU.Trim() == objImport.BU.Trim() && x.Location == objImport.Location).FirstOrDefault();
                                if (objQMS020 != null ? (objQMS020.Status == clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue() || objQMS020.Status == clsImplementationEnum.TestPlanStatus.Returned.GetStringValue()) && objQMS020.RevNo == 0 : false)
                                {
                                    QMS015_Log objQMS015 = new QMS015_Log();
                                    if (objQMS020.QualityId != QID)
                                    {
                                        List<string> lstQproj = new List<string>();
                                        lstQproj.Add(objQMS020.QualityProject);
                                        lstQproj.AddRange(db.QMS017.Where(c => c.BU == objQMS020.BU && c.Location == objQMS020.Location && c.IQualityProject == objQMS020.QualityProject).Select(x => x.QualityProject).ToList());
                                        if (string.IsNullOrWhiteSpace(QID))
                                        {
                                            List<QMS021> lstDesQMS021 = db.QMS021.Where(x => x.HeaderId == objQMS020.HeaderId).ToList();
                                            if (lstDesQMS021 != null && lstDesQMS021.Count > 0)
                                            {
                                                db.QMS021.RemoveRange(lstDesQMS021);
                                            }
                                            objQMS020.EditedBy = objClsLoginInfo.UserName;
                                            objQMS020.EditedOn = DateTime.Now;
                                            objQMS020.QualityId = string.Empty;
                                            objQMS020.QualityIdRev = null;
                                            db.SaveChanges();
                                        }
                                        else
                                        {
                                            objQMS015 = db.QMS015_Log.Where(x => lstQproj.Contains(x.QualityProject) && x.QualityId == QID && x.Status == Approved && x.Location == objQMS020.Location && x.BU == objQMS020.BU).FirstOrDefault();
                                            if (objQMS015 != null)
                                            {
                                                objQMS020.EditedBy = objClsLoginInfo.UserName;
                                                objQMS020.EditedOn = DateTime.Now;
                                                objQMS020.QualityId = objQMS015.QualityId;
                                                objQMS020.QualityIdRev = objQMS015.RevNo;
                                                db.SaveChanges();
                                                objResponseMsg = SaveTPHeader(objQMS020, objQMS015, true);
                                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        objResponseMsg.Key = false;
                                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.DuplicateQID.ToString();
                                    }


                                    if (PTCApplicable.ToLower() == "yes")
                                    {
                                        objQMS020.PTCApplicable = true;
                                        objQMS020.PTCNumber = PTCNumber;
                                    }
                                    else
                                    { objQMS020.PTCApplicable = false; objQMS020.PTCNumber = string.Empty; }
                                }
                            }
                            db.SaveChanges();

                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "Test Plan imported successfully";
                        }
                        catch (Exception ex)
                        {
                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        }
                    }
                    else
                    {
                        objResponseMsg = ErrorExportToExcel(ds);
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Excel file is not valid";
                }
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please enter valid Project no.";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponceMsgWithFileName GenerateHeaderExcel(List<SP_IPI_TEST_PLAN_HEADER_DETAILS_Result> lstdata)
        {
            clsHelper.ResponceMsgWithFileName objResponseMsg = new clsHelper.ResponceMsgWithFileName();
            try
            {
                MemoryStream ms = new MemoryStream();
                using (FileStream fs = System.IO.File.OpenRead(Server.MapPath("~/Resources/IPI/Test Plan Excel Template.xlsx")))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();
                        int i = 2;

                        foreach (var item in lstdata)
                        {
                            excelWorksheet.Cells[i, 1].Value = item.SeamNo;
                            excelWorksheet.Cells[i, 2].Value = item.QualityId;
                            excelWorksheet.Cells[i, 3].Value = (item.QualityIdRev != null) ? "R" + item.QualityIdRev.ToString() : "";
                            excelWorksheet.Cells[i, 4].Value = (item.PTCApplicable ? "Yes" : "No");
                            excelWorksheet.Cells[i, 5].Value = item.PTCNumber;
                            excelWorksheet.Cells[i, 6].Value = "R" + item.RevNo.ToString();
                            excelWorksheet.Cells[i, 7].Value = item.Status;
                            excelWorksheet.Cells[i, 8].Value = item.ReturnRemark;
                            //excelWorksheet.Cells[i, 9].Value = "Edit";
                            i++;
                        }
                        excelPackage.SaveAs(ms);
                    }
                }

                ms.Position = 0;

                string fileName;
                string excelFilePath = Helper.ExcelFilePath(objClsLoginInfo.UserName, out fileName);

                Byte[] bin = ms.ToArray();
                System.IO.File.WriteAllBytes(excelFilePath, bin);

                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error";
                objResponseMsg.FileName = fileName;
                return objResponseMsg;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }

        public clsHelper.ResponceMsgWithFileName ErrorExportToExcel(DataSet ds)
        {
            clsHelper.ResponceMsgWithFileName objResponseMsg = new clsHelper.ResponceMsgWithFileName();
            try
            {
                MemoryStream ms = new MemoryStream();
                using (FileStream fs = System.IO.File.OpenRead(Server.MapPath("~/Resources/IPI/Test Plan Excel Template.xlsx")))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();
                        int i = 2;
                        foreach (DataRow item in ds.Tables[0].Rows)
                        {
                            //Seam No.
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(8)))
                            {
                                excelWorksheet.Cells[i, 1].Value = item.Field<string>(0) + " (Note: " + item.Field<string>(8).ToString() + " )";
                                excelWorksheet.Cells[i, 1].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 1].Value = item.Field<string>(0); }

                            //Quality Id
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(9)))
                            {
                                excelWorksheet.Cells[i, 2].Value = item.Field<string>(1) + " (Note: " + item.Field<string>(9).ToString() + " )";
                                excelWorksheet.Cells[i, 2].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 2].Value = item.Field<string>(1); }

                            //Quality ID rev
                            excelWorksheet.Cells[i, 3].Value = item.Field<string>(2);

                            //PTC applicable
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(10)))
                            {
                                excelWorksheet.Cells[i, 4].Value = item.Field<string>(3) + " (Note: " + item.Field<string>(10).ToString() + " )";
                                excelWorksheet.Cells[i, 4].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 4].Value = item.Field<string>(3); }

                            //PTC Number
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(11)))
                            {
                                excelWorksheet.Cells[i, 5].Value = item.Field<string>(4) + " (Note: " + item.Field<string>(11).ToString() + " )";
                                excelWorksheet.Cells[i, 5].Style.Font.Color.SetColor(Color.Red);
                            }
                            else { excelWorksheet.Cells[i, 5].Value = item.Field<string>(4); }
                            //test plan rev
                            excelWorksheet.Cells[i, 6].Value = item.Field<string>(5);
                            //test plan status
                            excelWorksheet.Cells[i, 7].Value = item.Field<string>(6);
                            //Return remarks
                            excelWorksheet.Cells[i, 8].Value = item.Field<string>(7);
                            ////Action
                            //excelWorksheet.Cells[i, 9].Value = item.Field<string>(8);
                            //if (!string.IsNullOrWhiteSpace(item.Field<string>(13)))
                            //{
                            //    excelWorksheet.Row(i).Style.Font.Color.SetColor(Color.Red);
                            //}
                            i++;
                        }
                        excelPackage.SaveAs(ms);
                    }
                }

                ms.Position = 0;

                string fileName;
                string excelFilePath = Helper.ExcelFilePath(objClsLoginInfo.UserName, out fileName);

                Byte[] bin = ms.ToArray();
                System.IO.File.WriteAllBytes(excelFilePath, bin);

                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error";
                objResponseMsg.FileName = fileName;
                return objResponseMsg;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }

        public DataSet ToChechValidation(ExcelPackage package, QMS020 objImport, out bool isError)
        {
            ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();
            DataTable dtHeaderExcel = new DataTable();
            isError = false;
            int notesIndex = 0;
            try
            {
                #region Read Lines data from excel 
                foreach (var firstRowCell in excelWorksheet.Cells[1, 1, 1, excelWorksheet.Dimension.End.Column])
                {
                    dtHeaderExcel.Columns.Add(firstRowCell.Text);
                }

                for (var rowNumber = 2; rowNumber <= excelWorksheet.Dimension.End.Row; rowNumber++)
                {
                    var row = excelWorksheet.Cells[rowNumber, 1, rowNumber, excelWorksheet.Dimension.End.Column];
                    var newRow = dtHeaderExcel.NewRow();
                    if (excelWorksheet.Cells[rowNumber, 1].Value != null && excelWorksheet.Cells[rowNumber, 6].Value != null
                        && excelWorksheet.Cells[rowNumber, 7].Value != null)
                    {
                        foreach (var cell in row)
                        {
                            newRow[cell.Start.Column - 1] = cell.Text;
                        }
                    }
                    else
                    {
                        notesIndex = rowNumber + 1;
                        break;
                    }

                    dtHeaderExcel.Rows.Add(newRow);
                }
                #endregion

                #region Validation for Lines
                dtHeaderExcel.Columns.Add("SeamNoErrorMsg", typeof(string));//column 8
                dtHeaderExcel.Columns.Add("QIDErrorMsg", typeof(string));//column 9
                dtHeaderExcel.Columns.Add("PTCApplicableErrorMsg", typeof(string));//column 10
                dtHeaderExcel.Columns.Add("PTCNumberErrorMsg", typeof(string));//column 11
                dtHeaderExcel.Columns.Add("StatusErrorMsg", typeof(string));//column 12
                var lstYN = clsImplementationEnum.getyesno().ToList();
                List<string> lstYesNo = (from lst in lstYN
                                         select lst.ToString().ToLower()).ToList();

                foreach (DataRow item in dtHeaderExcel.Rows)
                {
                    string SeamNo = item.Field<string>("SeamNo");
                    string QID = item.Field<string>("QID");
                    string PTCApplicable = item.Field<string>("PTC Applicable");
                    string PTCNumber = item.Field<string>("PTC Number");
                    string Action = item.Field<string>("Action");
                    string errorMessage = string.Empty;
                    string approvedString = clsImplementationEnum.TestPlanStatus.Approved.GetStringValue();
                    string Draft = clsImplementationEnum.TestPlanStatus.Draft.GetStringValue();
                    string Returned = clsImplementationEnum.TestPlanStatus.Returned.GetStringValue();

                    QMS020 objQMS020 = new QMS020();
                    objQMS020 = db.QMS020.Where(x => x.SeamNo.Trim() == SeamNo.Trim()
                                                    && x.QualityProject.Trim() == objImport.QualityProject.Trim() && x.Project.Trim() == objImport.Project.Trim() && x.BU.Trim() == objImport.BU.Trim() && x.Location == objImport.Location).FirstOrDefault();
                    if (objQMS020 != null ? ((objQMS020.Status == clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue() || objQMS020.Status == clsImplementationEnum.TestPlanStatus.Returned.GetStringValue()) && objQMS020.RevNo == 0) : false)
                    {
                        List<string> BoolenList = new List<string>();
                        BoolenList.Add("yes");
                        BoolenList.Add("no");

                        List<string> lstQproj = new List<string>();
                        lstQproj.Add(objImport.QualityProject);
                        lstQproj.AddRange(db.QMS017.Where(c => c.BU == objImport.BU && c.Location == objImport.Location && c.IQualityProject == objImport.QualityProject).Select(x => x.QualityProject).ToList());
                        var lstQualityIds1 = (from a in db.QMS015_Log
                                              where lstQproj.Contains(a.QualityProject) && (a.QualityIdApplicableFor == "Seam")
                                                    && a.BU == objImport.BU && a.Location == objImport.Location
                                                    && a.Status == approvedString && a.QualityId == QID
                                              select a.QualityId).Distinct().ToList();

                        if (string.IsNullOrEmpty(SeamNo))
                        {
                            item["SeamNoErrorMsg"] = "Seam No is required";
                            isError = true;
                        }

                        //if (objQMS020.RevNo == 0 && (objQMS020.Status == Draft && objQMS020.Status == Returned))
                        //{
                        //    item["StatusErrorMsg"] = "Test Plan is in " + objQMS020.Status;
                        //    isError = true;
                        //}
                        //Quality ID
                        //if (string.IsNullOrEmpty(QID))
                        //{
                        //    item["SeamNoErrorMsg"] = "Quality Id is required";
                        //    isError = true;
                        //}
                        //else
                        //{
                        if (!string.IsNullOrWhiteSpace(QID))
                        {
                            if (!lstQualityIds1.Contains(QID))
                            {
                                item["QIDErrorMsg"] = "Quality Id is not valid";
                                isError = true;
                            }
                        }
                        //}
                        //PTC Applicable
                        if (!string.IsNullOrWhiteSpace(PTCApplicable))
                        {
                            if (!BoolenList.Contains(PTCApplicable.ToLower()))
                            {
                                item["PTCApplicableErrorMsg"] = "PTC Applicable is not valid";
                                isError = true;
                            }

                            if (PTCApplicable.ToLower() == clsImplementationEnum.yesno.Yes.GetStringValue().ToLower())
                            {
                                if (string.IsNullOrWhiteSpace(PTCNumber))
                                {
                                    item["PTCNumberErrorMsg"] = "PTC Number is not valid";
                                    isError = true;
                                }
                                else
                                {
                                    //PTC Number
                                    if (PTCNumber.Length > 12)
                                    {
                                        item["PTCNumberErrorMsg"] = "PTC Number  have maximum limit of 12 characters";
                                        isError = true;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (objQMS020 == null)
                        {
                            item["SeamNoErrorMsg"] = "Seam No is not valid";
                            isError = true;
                        }
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            DataSet ds = new DataSet();
            ds.Tables.Add(dtHeaderExcel);
            return ds;
        }

        [SessionExpireFilter]
        [UserPermissions]
        public ActionResult DisplaySWP()
        {
            ViewBag.IsDisplayOnly = true;
            return View("Index");
        }
        public ActionResult DisplayHeader(int headerID)
        {
            ViewBag.IsDisplayOnly = true;
            QMS020 objQMS020 = db.QMS020.Where(x => x.HeaderId == headerID).FirstOrDefault();
            ViewBag.BU = db.COM002.Where(a => a.t_dimx == objQMS020.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            ViewBag.Project = db.COM001.Where(i => i.t_cprj == objQMS020.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objQMS020.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            SWP010 objSWP010 = db.SWP010.Where(x => x.QualityProject == objQMS020.QualityProject && x.Project == objQMS020.Project && x.Location == objQMS020.Location && x.BU == objQMS020.BU).FirstOrDefault();
            if (objSWP010 != null)
                ViewBag.SWPHeaderId = objSWP010.HeaderId;
            else
                ViewBag.SWPHeaderId = 0;
            ViewBag.HeaderID = headerID;

            Session["TP_SWP_SeamList"] = null;
            Session["TP_SWP_PageSize"] = null;
            Session["TP_SWP_Search"] = null;

            return View("AddHeader", objQMS020);
        }
        public class ResponceMsgWithHeaderID : clsHelper.ResponseMsg
        {
            public string Status;
            public int HeaderId;
            public int Sequence;
            public string Stage;
            public string Project;
            public string BU;
            public string Location;
            public string ProjectCode;
            public string BUCode;
            public string LocationCode;
            public int TPHeaderId;
            public int SWPHeaderId;
        }

        #region Add/Delete Stage in Multiple Seams

        public ActionResult LoadMultipleAddDeletePopupPartial(string QualityProject, string BU, string Location, string ActionType, int lineid = 0)
        {
            if (lineid > 0)
            {
                QMS021 obj = db.QMS021.Where(x => x.LineId == lineid).FirstOrDefault();
                if (obj != null)
                {
                    ViewBag.StageCode = obj.StageCode;
                    ViewBag.StageCodeDesc = Manager.GetStageCodeDescriptionFromCode(obj.StageCode, obj.BU, obj.Location);
                    ViewBag.StageSequance = obj.StageSequance;
                    ViewBag.SeamNo = obj.SeamNo;
                    QualityProject = obj.QualityProject;
                    Location = obj.Location;
                    BU = obj.BU;
                }
            }
            ViewBag.QualityProject = QualityProject;
            ViewBag.Location = Location;
            ViewBag.BU = BU;
            ViewBag.ActionType = ActionType;

            var lstAcceptanceStandard = Manager.GetSubCatagories("Acceptance Standard", BU, Location, true).Select(i => new CategoryData { Value = i.Description, Code = i.Description, CategoryDescription = i.Description }).ToList();
            ViewBag.AcceptanceStandard = lstAcceptanceStandard;

            var lstApplicableSpecification = Manager.GetSubCatagories("Applicable Specification", BU, Location, true).Select(i => new CategoryData { Value = i.Description, Code = i.Description, CategoryDescription = i.Description }).ToList();
            ViewBag.ApplicableSpecification = lstApplicableSpecification;

            var lstInspectionExtent = Manager.GetSubCatagories("Inspection Extent", BU, Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            ViewBag.InspectionExtent = lstInspectionExtent;



            return PartialView("_MultipleAddDeletePopupPartial");
        }

        [HttpPost]
        public ActionResult MultipleAddDelete(string QualityProject, string BU, string Location, string ActionType, string FromSeam, string ToSeam, string StageCode, int SeqNo, string AcceptanceStandard = "", string ApplicableSpecification = "", string InspectionExtent = "", string Remarks = "", List<SeamMultipleAddandDeleteEntity> testPlanDeleteEntity = null)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (testPlanDeleteEntity != null && ActionType.Equals("Delete"))
                {
                    string seamno = string.Empty, seamDeleted = string.Empty, seamExists = string.Empty;
                    int countDeleted = 0, seamCnt = 0;
                    int? count = null;

                    foreach (var item in testPlanDeleteEntity)
                    {
                        var TotalAffectedData = db.SP_IPI_MULTIPLE_ADD_DELETE(ActionType, QualityProject, BU, Location, item.SeamNo, item.SeamNo, item.StageCode, item.StageSequance, AcceptanceStandard, ApplicableSpecification, InspectionExtent, Remarks, objClsLoginInfo.UserName).ToList();
                        count = TotalAffectedData.Count > 0 ? TotalAffectedData.Select(x => x.sCount).FirstOrDefault() : 0;
                        if (count > 0)
                        {
                            seamDeleted += "<span class=\"badge badge-success\">" + item.SeamNo.ToString() + "</span> ";
                            countDeleted += 1;
                        }
                        else
                        {
                            seamExists += "<span class=\"badge badge-dark\">" + item.SeamNo.ToString() + "</span> ";
                            seamCnt += 1;
                        }
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = countDeleted > 0 ? countDeleted + " Seams affected.<br />" + seamDeleted : "";
                    objResponseMsg.Value += seamCnt > 0 ? "<br />Stage already offered in following seam, it cannot be deleted in " + seamCnt + " Seam(s).<br/>" + seamExists : "";
                }
                else
                {
                    var TotalAffectedData = db.SP_IPI_MULTIPLE_ADD_DELETE(ActionType, QualityProject, BU, Location, FromSeam, ToSeam, StageCode, SeqNo, AcceptanceStandard, ApplicableSpecification, InspectionExtent, Remarks, objClsLoginInfo.UserName).ToList();
                    int? count = TotalAffectedData.Count > 0 ? TotalAffectedData.Select(x => x.sCount).FirstOrDefault() : 0;
                    string seamno = string.Join(", ", TotalAffectedData.Select(x => x.seamno).Distinct());

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = (TotalAffectedData.Where(x => x.seamno != "").Count() > 0) ? string.Join(",", "Stage already offered in following seam, it cannot be " + (ActionType == "Delete" ? "deleted" : "added") + " in Seam(s): <br />" + "<span class=\"badge badge-dark\">" + seamno + "<span> ") : count + " Seams affected.<br /><span class=\"badge badge-success\">" + seamno + "<span> ";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.TestPlanMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult MultipleAdd(string QualityProject, string BU, string Location, string ActionType, string StageCode, int SeqNo, string AcceptanceStandard = "", string ApplicableSpecification = "", string InspectionExtent = "", string Remarks = "", List<string> seamNoArry = null)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();

            try
            {
                if (seamNoArry != null && ActionType.Equals("Add"))
                {
                    string seamno = string.Empty, seamAdd = string.Empty, seamExists = string.Empty;
                    int countAdd = 0, seamCnt = 0;
                    int? count = null;

                    foreach (string seamNo in seamNoArry)
                    {
                        var TotalAffectedData = db.SP_IPI_MULTIPLE_ADD_DELETE(ActionType, QualityProject, BU, Location, seamNo, seamNo, StageCode, SeqNo, AcceptanceStandard, ApplicableSpecification, InspectionExtent, Remarks, objClsLoginInfo.UserName).ToList();
                        count = TotalAffectedData.Count > 0 ? TotalAffectedData.Select(x => x.sCount).FirstOrDefault() : 0;
                        if (count > 0)
                        {
                            seamAdd += "<span class=\"badge badge-success\">" + seamNo + "</span>";
                            countAdd += 1;
                        }
                        else
                        {
                            seamExists += "<span class=\"badge badge-dark\">" + seamNo + "</span>";
                            seamCnt += 1;
                        }
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = countAdd > 0 ? countAdd + " Seams affected.<br />" + seamAdd + "<br />" : "";
                    objResponseMsg.Value += seamCnt > 0 ? seamCnt + " Seams not effected.<br />" + seamExists : "";
                    //objResponseMsg.Value += seamCnt > 0 ? "Stage already offered in following seam, it cannot be " + (ActionType == "Add" ? "Add" : "Deleted") + " in " + seamCnt + " Seam(s)" : "";

                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                objResponseMsg.Key = false;
                objResponseMsg.Value = "Seam couldn't be added because seam not avaliable";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.TestPlanMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSeamsbyQualityProject(string term, string QualityProject, string ActionType)
        {
            List<string> lstSeamNo = new List<string>();
            List<ddlValue> lstarrSeamNo = new List<ddlValue>();
            try
            {
                if (!string.IsNullOrWhiteSpace(term))
                {
                    lstSeamNo = (from a in db.QMS020
                                 where a.SeamNo.Contains(term) && a.QualityProject == QualityProject
                                 select a.SeamNo).Distinct().OrderBy(x => x.ToString()).ToList();
                }
                else
                {
                    lstSeamNo = (from a in db.QMS020
                                 where a.QualityProject == QualityProject
                                 select a.SeamNo).Distinct().OrderBy(x => x.ToString()).Take(10).ToList();
                }

                string[] arr = lstSeamNo.ToArray();
                Manager.AlphaNumericalSort(arr);

                foreach (var a in arr)
                {
                    lstarrSeamNo.Add(new ddlValue() { Text = a, Value = a });
                }
                if (lstSeamNo.Count > 0)
                {
                    if (ActionType != "Delete")
                    {
                        lstarrSeamNo.Insert(0, new ddlValue() { Text = "All", Value = "All" });
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(lstarrSeamNo, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetAllStages(string term, string Location)
        {
            List<ddlValue> lstStageCode = new List<ddlValue>();
            List<ddlValue> lstarrSeamNo = new List<ddlValue>();
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstStageCode = (from a in db.QMS002
                                where (a.StageCode.Contains(term) || a.StageDesc.Contains(term)) && a.StageCode != null && a.StageCode != "" && a.Location == Location
                                select new ddlValue { Text = a.StageCode + "-" + a.StageDesc, Value = a.StageCode, id = a.SequenceNo.ToString() }).OrderBy(x => x.Value).Distinct().Take(10).ToList();
            }
            else
            {
                lstStageCode = (from a in db.QMS002
                                where a.StageCode != null && a.StageCode != "" && a.Location == Location
                                select new ddlValue { Text = a.StageCode + "-" + a.StageDesc, Value = a.StageCode, id = a.SequenceNo.ToString() }).OrderBy(x => x.Value).Distinct().Take(10).ToList();
            }

            return Json(lstStageCode, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetSeamNoForAdd(string qualityproject, string location, string seamCategorys)
        {

            string[] splitSeamCategory = seamCategorys.Split('-');
            string seamCategory = splitSeamCategory[0].Trim();
            string weldType = splitSeamCategory[1].Trim();

            var result = db.Database.SqlQuery<string>("SELECT SeamNo = STUFF((SELECT DISTINCT ',' + Q20.SeamNo FROM QMS020 AS Q20 JOIN QMS021 AS Q21 ON Q21.HeaderId = Q20.HeaderId WHERE Q20.qualityproject = '" + qualityproject + "' AND Q20.Status in ('Draft','Returned') AND Q21.StageStatus<>'Deleted' AND Q20.Location in('" + location + "') AND Q20.SeamNo IN (SELECT Q12.SeamNo FROM QMS012 AS Q12 WHERE Q12.seamCategory = '" + seamCategory + "' AND Q12.WeldType='" + weldType + "' ) FOR XML PATH ('')),1,1,'')");
            if (result != null)
            {
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            return Json("", JsonRequestBehavior.AllowGet);
        }

        #endregion

        [HttpPost]
        public ActionResult GetNextPrevQualityProject(string qualityProject, string location, string option)
        {
            List<Projects> lstQualityProject = new List<Projects>();
            lstQualityProject = db.QMS020.Select(i => new Projects { Value = i.QualityProject, projectCode = i.QualityProject, Text = i.Location }).Distinct().OrderBy(x => x.Value).ThenBy(x => x.Text).ToList();

            ResponceMsgWithHeaderID objresponse = new ResponceMsgWithHeaderID();
            List<Projects> lstprojects = lstQualityProject;
            int projindexno = 0;
            int minproj = 0;
            int maxproj = 0;
            QMS020 qms020 = new QMS020();

            try
            {
                if (!string.IsNullOrEmpty(qualityProject))
                {
                    projindexno = lstprojects.Select((item, index) => new { index, item }).Where(x => x.item.projectCode.ToLower() == qualityProject.ToLower() && x.item.Text.ToLower() == location.ToLower()).Select(x => x.index).FirstOrDefault();
                    minproj = lstprojects.Select((item, index) => new { index, item }).Min(x => x.index);
                    maxproj = lstprojects.Select((item, index) => new { index, item }).Max(x => x.index);

                    switch (option)
                    {
                        case "PrevProject":
                            ViewBag.Title = "PrevProject";
                            projindexno = (projindexno - 1);
                            qualityProject = lstprojects.Select((item, index) => new { index, item }).Where(x => x.index == projindexno).FirstOrDefault().item.projectCode;
                            location = lstprojects.Select((item, index) => new { index, item }).Where(x => x.index == projindexno).FirstOrDefault().item.Text;
                            qms020 = db.QMS020.Where(i => i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase) && i.Location == location).FirstOrDefault();

                            break;
                        case "NextProject":
                            ViewBag.Title = "NextProject";
                            projindexno = (projindexno + 1);
                            qualityProject = lstprojects.Select((item, index) => new { index, item }).Where(x => x.index == projindexno).FirstOrDefault().item.projectCode;
                            location = lstprojects.Select((item, index) => new { index, item }).Where(x => x.index == projindexno).FirstOrDefault().item.Text;
                            qms020 = db.QMS020.Where(i => i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase) && i.Location == location).FirstOrDefault();

                            break;
                        default:
                            ViewBag.Title = "";
                            break;
                    }


                    objresponse.Project = qms020.QualityProject;
                    objresponse.Location = qms020.Location;
                    objresponse.HeaderId = qms020.HeaderId;
                    objresponse.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objresponse.Key = false;
                objresponse.Value = "Error Occures Please try again";
            }

            return Json(objresponse, JsonRequestBehavior.AllowGet);
        }


        public class SeamMultipleAddandDeleteEntity
        {

            public int HeaderID { get; set; }
            public string SeamNo { get; set; }
            public string QualityId { get; set; }
            public string QulityDesc { get; set; }
            public int StageSequance { get; set; }
            public string StageCode { get; set; }
            public int RevNo { get; set; }
            public int? TotalCount { get; set; }
        }
        //public class TestPlanDeleteEntity
        //{
        //    public int LineId { get; set; }
        //    public int HeaderID { get; set; }
        //    public string SeamNo { get; set; }
        //    public string QualityId { get; set; }
        //    public string QulityDesc { get; set; }
        //    public int StageSequance { get; set; }
        //    public string StageCode { get; set; }
        //    public int? TotalCount { get; set; }
        //}

    }
}