﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Information;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.IPI.Controllers
{
    public class MaintainSeamICLController : clsBase
    {
        // GET: IPI/TestPlan

        [SessionExpireFilter]
        [AllowAnonymous]
        //[UserPermissions]
        #region Index Page
        public ActionResult Index()
        {
            return View();
        }

        //index data
        [HttpPost]
        public JsonResult LoadTPHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += " 1=1 and status in('" + clsImplementationEnum.TestPlanStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.TestPlanStatus.Returned.GetStringValue() + "')";
                }
                else
                {
                    strWhere += " 1=1";
                }

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                string[] columnName = { "QualityProject", "Location", "BU", "Project" };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                //strWhere += "1=1 and Location='" + objClsLoginInfo.Location + "'"; ;
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.Location, "", "Location");
                var lstResult = db.SP_IPI_GET_SEAM_ICL_INDEXRESULT
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.QualityProject),
                            Convert.ToString(uc.Project),
                            Convert.ToString(uc.BU),
                            Convert.ToString(uc.Location),
                            "<center><a  href='" + WebsiteURL + "/IPI/MaintainSeamICL/AddHeader?HeaderID="+uc.HeaderId+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a><a class='btn btn-xs' href='javascript:void(0)' onclick=ShowTimeline('/IPI/MaintainSeamICL/ShowTimeline?HeaderID="+Convert.ToInt32(uc.HeaderId)+"');><i style = '' class='iconspace fa fa-clock-o'></i></a></center>",
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Header Details
        [SessionExpireFilter]
        public ActionResult AddHeader(int headerID)
        {
            ViewBag.HeaderID = headerID;
            return View();
        }

        //maintain form (header) 
        [HttpPost]
        public ActionResult LoadTPHeaderForm(int headerID)
        {
            QMS030 objQMS030 = new QMS030();
            if (headerID > 0)
            {
                objQMS030 = db.QMS030.Where(x => x.HeaderId == headerID).FirstOrDefault();
                objQMS030.BU = db.COM002.Where(a => a.t_dimx == objQMS030.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objQMS030.Project = db.COM001.Where(i => i.t_cprj == objQMS030.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objQMS030.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objQMS030.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();

                //var lstQualityIds1 = (from a in db.QMS015_Log
                //                      where a.QualityProject == objQMS020.QualityProject && (a.QualityIdApplicableFor == "Seam" || a.QualityIdApplicableFor == "Seam")
                //                      select new CategoryData { Value = a.QualityId, Code = a.QualityId, CategoryDescription = a.QualityId + " - " + a.QualityIdDesc }).Distinct().ToList();

                //ViewBag.QualityID = lstQualityIds1;
                //var lstQID15 = db.QMS015_Log.Where(i => i.QualityProject == objQMS020.QualityProject && i.QualityIdApplicableFor == "Seam").Select(i => i.QualityId).Distinct().ToList();
                //var lstQID17 = db.QMS017_Log.Where(i => i.IQualityProject == objQMS020.QualityProject).Select(i => i.QualityId).Distinct().ToList();
                //ViewBag.lstQualityId = lstQID15;//.Union(lstQID17);
                //ViewBag.lstPTCApplicable = clsImplementationEnum.getyesno().ToArray();
            }

            return PartialView("_LoadTPHeaderForm", objQMS030);
        }

        //maintain data
        [HttpPost]
        public ActionResult GetTestPlanData(JQueryDataTableParamModel param, string qProj, string loc)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = string.Empty;
                //string whereCondition = "1=1";
                string strSortOrder = string.Empty;


                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    whereCondition += " 1=1  and qms30.Status in('" + clsImplementationEnum.TestPlanStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.TestPlanStatus.Returned.GetStringValue() + "')";
                }
                else
                {
                    whereCondition += " 1=1";
                }
                if (!string.IsNullOrEmpty(qProj))
                {
                    whereCondition += " AND UPPER(qms30.QualityProject) = '" + qProj.ToUpper() + "'";
                }
                if (!string.IsNullOrEmpty(loc))
                {
                    whereCondition += " AND qms30.Location in('" + loc.Split('-')[0].Trim() + "')";
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                string[] columnName = { "qms30.QualityProject", "qms30.SeamNo", "qms30.Location", "qms30.PTCApplicable", "qms30.PTCNumber", "qms30.Status", "qms30.Status", "c20.QualityId", "c20.QualityIdRev" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var lstResult = db.SP_IPI_SEAM__ICL_HEADER_DETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                //string ApprovedStatus = clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue();
                //var lstQualityIds1 = (from a in db.QMS015_Log
                //                      where a.QualityProject == qProj && a.QualityIdApplicableFor == "Seam" && a.Status == ApprovedStatus
                //                      select new { Code = a.QualityId, Desc = a.QualityIdDesc }).Distinct().ToList();

                List<SelectListItem> BoolenList = new List<SelectListItem>() { new SelectListItem { Text = "Yes", Value = "True" }, new SelectListItem { Text = "No", Value = "False" } };
                NDEModels objNDEModels = new NDEModels();
                //var data = (from uc in lstResult
                //            select new[]
                //            {
                //                    Convert.ToString(uc.HeaderId),
                //                    Helper.GenerateHidden(uc.HeaderId, "HeaderId", Convert.ToString(uc.HeaderId)),
                //                    Helper.GenerateHidden(uc.HeaderId,"HasLines",Convert.ToString(uc.ISLines)),
                //                    Convert.ToString(uc.SeamNo),
                //                    //GenerateAutoComplete(uc.HeaderId, "txtQualityId", Convert.ToString(uc.QualityId),"UpdateData(this, "+ uc.HeaderId +");",false,"","QualityId",(string.Equals(uc.Status,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false))+""+Helper.GenerateHidden(uc.HeaderId,"QualityId",uc.QualityId),
                //                    Convert.ToString(uc.RevNo),
                //                    Helper.GenerateDropdown(uc.HeaderId, "PTCApplicable", new SelectList(BoolenList, "Value", "Text", Convert.ToString(uc.PTCApplicable)),"","UpdateData(this, "+ uc.HeaderId +");",string.Equals(uc.Status,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue())?true:false),
                //                    //GetPTCNumber(uc.PTCNumber,uc.Status, uc.HeaderId,uc.PTCApplicable),
                //                    Helper.GenerateHTMLTextboxOnChanged(uc.HeaderId, "PTCNumber", Convert.ToString(uc.PTCNumber),"UpdateData(this, "+ uc.HeaderId +");", ( string.Equals(uc.Status,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue()) || !uc.PTCApplicable )?true:false, "", false, "12"),
                //                    Convert.ToString(uc.Status),
                //                    Convert.ToString(uc.TPRevNo),
                //                    (( (uc.Status == clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue() && uc.RevNo == 0)  ? HTMLActionString(uc.HeaderId, uc.Status, "Delete", "Delete QualityID", "fa fa-trash-o", "DeleteQualtiyId(" + uc.HeaderId + ");") : "")
                //                       +"   "+ (!string.IsNullOrEmpty(uc.Status)?(!string.Equals(uc.Status,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?HTMLActionString(uc.HeaderId,uc.Status,"CopyQualityID","Copy Test Plan","fa fa-files-o","CopyTestPlanData("+uc.HeaderId+");"):string.Empty):string.Empty)
                //                       +"   "+(!string.IsNullOrEmpty(uc.Status)?(string.Equals(uc.Status,clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue(),StringComparison.OrdinalIgnoreCase) || uc.RevNo>0 ? HTMLActionString(uc.HeaderId,uc.Status,"HistoryTestPlan","History","fa fa-history","HistoryTestPlan(\""+ uc.HeaderId +"\");"):string.Empty):string.Empty)
                //                       +"   "+"<a class='btn btn-xs' href='javascript:void(0)' onclick=ShowTimeline('/IPI/MaintainSeamICL/ShowTimeline?HeaderID="+Convert.ToInt32(uc.HeaderId)+"');><i style = 'margin-left:5px;' class='fa fa-clock-o'></i></a>")

                //            }).ToList();

                //data.Insert(0, newRecord);
                var data = (from uc in lstResult
                            select new[]
                            {
                                    Convert.ToString(uc.HeaderId),
                                    Helper.GenerateHidden(uc.HeaderId, "HeaderId", Convert.ToString(uc.HeaderId)),
                                    Helper.GenerateHidden(uc.HeaderId,"HasLines",Convert.ToString(uc.ISLines)),
                                    Convert.ToString(uc.SeamNo),
                                    //GenerateAutoComplete(uc.HeaderId, "txtQualityId", Convert.ToString(uc.QualityId),"UpdateData(this, "+ uc.HeaderId +");",false,"","QualityId",(string.Equals(uc.Status,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false))+""+Helper.GenerateHidden(uc.HeaderId,"QualityId",uc.QualityId),
                                    Convert.ToString(uc.RevNo),
                                    Convert.ToString(uc.QualityId),
                                    Convert.ToString(!uc.QualityIdRev.HasValue ? "" : "R"+uc.QualityIdRev),
                                    uc.PTCApplicable?"Yes":"No",
                                    //Helper.GenerateDropdown(uc.HeaderId, "PTCApplicable", new SelectList(BoolenList, "Value", "Text", Convert.ToString(uc.PTCApplicable)),"","UpdateData(this, "+ uc.HeaderId +");",string.Equals(uc.Status,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue())?true:false),
                                    //GetPTCNumber(uc.PTCNumber,uc.Status, uc.HeaderId,uc.PTCApplicable),
                                    Convert.ToString(uc.PTCNumber),
                                    //Helper.GenerateHTMLTextboxOnChanged(uc.HeaderId, "PTCNumber", Convert.ToString(uc.PTCNumber),"UpdateData(this, "+ uc.HeaderId +");", ( string.Equals(uc.Status,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue()) || !uc.PTCApplicable )?true:false, "", false, "12"),
                                    Convert.ToString(uc.Status),
                                    Convert.ToString(uc.TPRevNo),
                                    Convert.ToString(uc.ReturnRemark),
                                    "<nobr><center>"+
                                    Manager.generateSeamHistoryButton(uc.Project.Split('-')[0].Trim().ToString(),uc.QualityProject, uc.SeamNo,"Seam Details") +" "+
                                    (/*( (uc.Status == clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue() && uc.RevNo == 0)  ? HTMLActionString(uc.HeaderId, uc.Status, "Delete", "Delete QualityID", "fa fa-trash-o", "DeleteQualtiyId(" + uc.HeaderId + ");") : HTMLActionString(uc.HeaderId, uc.Status, "Delete", "Delete QualityID", "fa fa-trash-o", "",true))*/
                                       (!string.IsNullOrEmpty(uc.Status)?(string.Equals(uc.Status,clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue(),StringComparison.OrdinalIgnoreCase) || uc.RevNo>0 ? HTMLActionString(uc.HeaderId,uc.Status,"HistoryTestPlan","History","fa fa-history","HistoryTestPlan(\""+ uc.HeaderId +"\");"):HTMLActionString(uc.HeaderId,uc.Status,"HistoryTestPlan","History","fa fa-history","",true)):HTMLActionString(uc.HeaderId,uc.Status,"HistoryTestPlan","History","fa fa-history","",true))
                                       +"<a  href='javascript:void(0)' onclick=ShowTimeline('/IPI/MaintainSeamICL/ShowTimeline?HeaderID="+Convert.ToInt32(uc.HeaderId)+"');><i title='View Timeline' class='iconspace fa fa-clock-o'></i></a>"
                                       +"<a  href='javascript:void(0)' onclick=CopyProtocol("+ Convert.ToInt32(uc.HeaderId)+",'Copy');><i title='Copy Protocol Within Project' class='iconspace fa fa-files-o'></i></a>")
                                       +"</center></nobr>"

                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string HTMLAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string maxlength = "", string validate = "")
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string FieldValidate = !string.IsNullOrEmpty(validate) ? "validate='" + validate + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onchange='" + onBlurMethod + "'" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' hdElement='" + hdElementId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + (disabled ? "disabled" : "") + " " + MaxCharcters + " " + FieldValidate + " />";

            return strAutoComplete;
        }

        public string HTMLAutoCompleteOnBlur(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string maxlength = "", string validate = "")
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string FieldValidate = !string.IsNullOrEmpty(validate) ? "validate='" + validate + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' hdElement='" + hdElementId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + (disabled ? "disabled" : "") + " " + MaxCharcters + " " + FieldValidate + " />";

            return strAutoComplete;
        }

        //partial for header tab(maintain page)
        [HttpPost]
        public ActionResult LoadTPHeaderDataPartial(string status, string qualityProject, string location)
        {
            ViewBag.Status = status;
            ViewBag.qualityProject = qualityProject;
            ViewBag.Location = location;
            var lstQualityIds1 = (from a in db.QMS015_Log
                                  where a.QualityProject == qualityProject && (a.QualityIdApplicableFor == "Seam")
                                  select new CategoryData { Value = a.QualityId, Code = a.QualityId, CategoryDescription = a.QualityId + " - " + a.QualityIdDesc }).Distinct().ToList();

            ViewBag.QualityID = lstQualityIds1;

            return PartialView("_GetHeaderGridDataPartial");
        }
        #endregion

        #region Line Details
        [SessionExpireFilter]
        [AllowAnonymous]
        //[UserPermissions]
        //load partial for test plan stages
        [HttpPost]
        public ActionResult LoadTestPlanLineData(int headerId)
        {
            NDEModels objNDEModels = new NDEModels();
            QMS030 objQMS030 = db.QMS030.Where(x => x.HeaderId == headerId).FirstOrDefault();
            if (objQMS030 != null)
            {
                var lstAcceptanceStandard = Manager.GetSubCatagories("Acceptance Standard", objQMS030.BU, objQMS030.Location).Select(i => new CategoryData { Value = i.Description, Code = i.Description, CategoryDescription = i.Description }).ToList();
                var lstApplicableSpecification = Manager.GetSubCatagories("Applicable Specification", objQMS030.BU, objQMS030.Location).Select(i => new CategoryData { Value = i.Description, Code = i.Description, CategoryDescription = i.Description }).ToList();
                var lstInspectionExtent = Manager.GetSubCatagories("Inspection Extent", objQMS030.BU, objQMS030.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
                string StageTypeExludedinOfferDropdown = clsImplementationEnum.ConfigParameter.StageTypeExludedinOfferDropdown.GetStringValue();
                var lstStages = Manager.GetConfigValue(StageTypeExludedinOfferDropdown).ToUpper().Split(',').ToArray();
                ViewBag.StagesCode = db.QMS002.Where(i => i.BU.Equals(objQMS030.BU, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(objQMS030.Location, StringComparison.OrdinalIgnoreCase) && !lstStages.Contains(i.StageType)).OrderBy(o => o.SequenceNo).Select(i => new CategoryData { Value = i.StageCode, Code = i.StageCode, CategoryDescription = i.StageCode + "-" + i.StageDesc }).ToList();
                ViewBag.AcceptanceStandard = lstAcceptanceStandard;
                ViewBag.ApplicableSpecification = lstApplicableSpecification;
                ViewBag.InspectionExtent = lstInspectionExtent;
                ViewBag.lstProtocolType = Manager.GetSubCatagories("Protocol Type", objQMS030.BU, objQMS030.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
                //ViewBag.StagesCode = db.QMS002.Where(i => i.BU.Equals(objQMS020.BU, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(objQMS020.Location, StringComparison.OrdinalIgnoreCase)).Select(i => new CategoryData { Value = i.StageCode, Code = i.StageCode, CategoryDescription = i.StageCode + "-" + i.StageDesc }).ToList();
                //var lstAcceptanceStd = Manager.GetSubCatagories("Acceptance Standard", objQMS020.BU, objQMS020.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
                //var lstApplicableSpeci= Manager.GetSubCatagories("Applicable Specification", objQMS020.BU, objQMS020.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
                //var lstInspectionExt = Manager.GetSubCatagories("Inspection Extent", objQMS020.BU, objQMS020.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
                //ViewBag.AcceptanceStandard = lstAcceptanceStd;
                //ViewBag.ApplicableSpecification = lstApplicableSpeci;
                //ViewBag.InspectionExtent = lstInspectionExt;
            }

            return PartialView("_LoadTPLinesDetailsForm", objQMS030);
        }

        [HttpPost]
        public ActionResult DeleteLine(int LineID)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                //only status will updated as Deleted(No Hard Delete)
                QMS031 objQMS031 = db.QMS031.Where(x => x.LineId == LineID).FirstOrDefault();
                string headerStatus = string.Empty;
                if (objQMS031 != null)
                {
                    headerStatus = objQMS031.QMS030.Status;
                    if (objQMS031.QMS030.Status == clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue())
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.HeaderStatus = clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue();
                        objResponseMsg.Remarks = "This data is already submitted, it cannot be delete now!!";
                    }
                    else
                    {
                        if (objQMS031.QMS030.Status == clsImplementationEnum.TestPlanStatus.Approved.GetStringValue())
                        {
                            objQMS031.QMS030.Status = clsImplementationEnum.TestPlanStatus.Draft.GetStringValue();
                            objQMS031.QMS030.RevNo = objQMS031.QMS030.RevNo + 1;
                            objQMS031.QMS030.EditedBy = objClsLoginInfo.UserName;
                            objQMS031.QMS030.EditedOn = DateTime.Now;
                            objQMS031.QMS030.SubmittedBy = null;
                            objQMS031.QMS030.SubmittedOn = null;
                            objQMS031.QMS030.ApprovedBy = null;
                            objQMS031.QMS030.ApprovedOn = null;
                        }
                        if (objQMS031.QMS030.RevNo == 0 && (objQMS031.QMS030.Status == clsImplementationEnum.TestPlanStatus.Draft.GetStringValue() || objQMS031.QMS030.Status == clsImplementationEnum.TestPlanStatus.Returned.GetStringValue()))
                        {
                            db.QMS031.Remove(objQMS031);
                        }
                        else
                        {
                            objQMS031.StageStatus = clsImplementationEnum.TestPlanStatus.Deleted.GetStringValue();
                            objQMS031.EditedBy = objClsLoginInfo.UserName;
                            objQMS031.EditedOn = DateTime.Now;
                        }
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.HeaderStatus = headerStatus;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //editable grid for stages
        [HttpPost]
        public JsonResult LoadTestPlanLineGridData(JQueryDataTableParamModel param)
        {
            try
            {
                NDEModels objNDEModels = new NDEModels();
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "";
                strWhere += Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();
                //string[] arrayCTQStatus = { clsImplementationEnum.CTQStatus.DRAFT.GetStringValue(), clsImplementationEnum.CTQStatus.Returned.GetStringValue() };

                string[] columnName = { "StageCode", "StageSequance", "AcceptanceStandard", "ApplicableSpecification", "InspectionExtent", "StageStatus", "Remarks", "LineId", };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);

                string strStatus = param.MTStatus;
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {

                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_IPI_SEAM_ICL_LINE_DETAILS
                                (
                               StartIndex,
                               EndIndex,
                               strSortOrder,
                               strWhere
                                ).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                        Helper.GenerateHidden(newRecordId, "HeaderId", param.CTQHeaderId),
                                        GenerateAutoCompleteonBlur(newRecordId,"txtStageCode","","getSequence(this);",false,"","StageCode")+""+Helper.GenerateHidden(newRecordId,"StageCode"),
                                        Helper.GenerateTextbox(newRecordId,"StageSequance","","CheckDuplicate(this);"),
                                        //"",
                                        //"",
                                        //"",
                                        //"",
                                        GenerateAutoComplete(newRecordId, "AcceptanceStandard","","",false,"","AcceptanceStandard")+""+Helper.GenerateHidden(newRecordId,"AcceptanceStandard"),
                                        GenerateAutoComplete(newRecordId, "ApplicableSpecification","","",false,"","ApplicableSpecification")+""+Helper.GenerateHidden(newRecordId,"ApplicableSpecification"),
                                        GenerateAutoComplete(newRecordId, "txtInspectionExtent","","",false,"","InspectionExtent")+""+Helper.GenerateHidden(newRecordId,"InspectionExtent"),
                                        Helper.GenerateTextbox(newRecordId,"Remarks"),
                                        GenerateAutoComplete(newRecordId, "txtProtocolType","","",false,"","ProtocolType")+""+Helper.GenerateHidden(newRecordId,"ProtocolType"),
                                         clsImplementationEnum.TestPlanStatus.Added.GetStringValue(),
                                        Helper.GenerateGridButton(newRecordId, "Add", "Add Rocord", "fa fa-plus", "SaveNewStage();" ),
                                        ""
                                        };
                int hID = Convert.ToInt32(param.CTQHeaderId);
                QMS030 objQMS030 = db.QMS030.FirstOrDefault(c => c.HeaderId == hID);
                int maxOfferedSequenceNo = (new clsManager()).GetMaxOfferedSequenceNo(objQMS030.QualityProject, objQMS030.BU, objQMS030.Location, objQMS030.SeamNo);
                bool isLTFPSProject = Manager.IsLTFPSProject(objQMS030.Project, objQMS030.BU, objQMS030.Location);
                var data = (from uc in lstResult
                            select new[]
                            {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.StageCode)+""+Helper.GenerateHidden(uc.LineId,"StageCode",uc.StageCode.Split('-')[0]),
                                Convert.ToString(uc.StageSequance),
                                Convert.ToString(uc.AcceptanceStandard),
                                Convert.ToString(uc.ApplicableSpecification),
                                !string.IsNullOrWhiteSpace(uc.InspectionExtent)?Convert.ToString(uc.InspectionExtent.Split('-')[1].Trim()):"",
                                Convert.ToString(uc.Remarks),
                                //uc.StageSequance <= maxOfferedSequenceNo ? uc.AcceptanceStandard : GenerateAutoComplete(uc.LineId, "AcceptanceStandard", uc.AcceptanceStandard, "UpdateLineDetails(this, "+ uc.LineId +");",false,"","",(string.Equals(strStatus,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false)|| string.Equals(uc.StageStatus, clsImplementationEnum.TestPlanStatus.Deleted.GetStringValue())  ? true : false),
                                //uc.StageSequance <= maxOfferedSequenceNo ? uc.ApplicableSpecification : GenerateAutoComplete(uc.LineId, "ApplicableSpecification", uc.ApplicableSpecification, "UpdateLineDetails(this, "+ uc.LineId +");",false,"","",(string.Equals(strStatus,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false)|| string.Equals(uc.StageStatus, clsImplementationEnum.TestPlanStatus.Deleted.GetStringValue())  ? true : false),
                                //uc.StageSequance <= maxOfferedSequenceNo ? uc.InspectionExtent : GenerateAutoComplete(uc.LineId, "txtInspectionExtent", objNDEModels.GetCategory(uc.InspectionExtent).CategoryDescription,"UpdateLineDetails(this, "+ uc.LineId +");",false,"","InspectionExtent",(string.Equals(strStatus,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false) || string.Equals(uc.StageStatus, clsImplementationEnum.TestPlanStatus.Deleted.GetStringValue())  ? true : false)+""+Helper.GenerateHidden(uc.LineId,"InspectionExtent",uc.InspectionExtent),
                                //uc.StageSequance <= maxOfferedSequenceNo ? uc.Remarks : Helper.GenerateHTMLTextbox(uc.LineId, "Remarks", Convert.ToString( uc.Remarks), "UpdateLineDetails(this, " + uc.LineId + ");", (string.Equals(strStatus, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue())  ? true : false) || string.Equals(uc.StageStatus, clsImplementationEnum.TestPlanStatus.Deleted.GetStringValue())  ? true : false,"",(string.Equals(strStatus, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue())  ? true : false) || string.Equals(uc.StageStatus, clsImplementationEnum.TestPlanStatus.Deleted.GetStringValue())  ? true : false),
                                ////Convert.ToString(uc.Remarks),
                                //"<i title=\"Attach Protocol\" id=\"btnAttachProtocol\" name=\"btnAttachProtocol\" style=\"cursor:pointer; color: green; \" class=\"glyphicon glyphicon-paperclip\"  onClick=\"LinkedProtocol("+ uc.LineId +", 'attach')\" ></i>&nbsp;&nbsp;",
                                (string.Equals(strStatus,clsImplementationEnum.ICLSeamStatus.SendForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(uc.StageStatus,clsImplementationEnum.ICLSeamStatus.Deleted.GetStringValue(),StringComparison.OrdinalIgnoreCase) || Manager.IsStageOffered(objQMS030.QualityProject, objQMS030.BU, objQMS030.Location, objQMS030.SeamNo, uc.StageCode.Split('-')[0].Trim(), uc.StageSequance.Value) || (isLTFPSProject && isStageLinkedInLTFPS(uc.LineId)) || !isProtocolApplicable(uc.StageCode, uc.Location,uc.BU)) ? uc.ProtocolTypeDescription : GenerateAutoComplete(uc.LineId,"txtProtocolType", uc.ProtocolTypeDescription,"",false,"","ProtocolType")+""+Helper.GenerateHidden(uc.LineId,"ProtocolType", uc.ProtocolType)+"<a Title='Link Existing Protocol' onclick='LinkExistingProtocol("+uc.LineId+");'><nobr>Link Protocol</nobr></a>",
                                Convert.ToString(uc.StageStatus),
                                "<nobr><center>"+
                                ((string.Equals(uc.StageStatus,clsImplementationEnum.ICLSeamStatus.Deleted.GetStringValue(),StringComparison.OrdinalIgnoreCase)|| Manager.IsStageOffered(objQMS030.QualityProject, objQMS030.BU, objQMS030.Location, objQMS030.SeamNo, uc.StageCode.Split('-')[0].Trim(), uc.StageSequance.Value) || (isLTFPSProject && isStageLinkedInLTFPS(uc.LineId)) ||IsLineAddedByInspector(uc.CreatedBy,uc.BU,uc.Location)  ) ? HTMLActionString(uc.HeaderId,uc.StageStatus,"Delete","Delete Record","fa fa-trash-o","",true):string.Equals(strStatus,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) ? HTMLActionString(uc.HeaderId,uc.StageStatus,"Delete","Delete Record","fa fa-trash-o","",true) : HTMLActionString(uc.HeaderId,uc.StageStatus,"Delete","Delete Record","fa fa-trash-o","DeleteRecord("+ uc.LineId +", \""+ strStatus +"\");")) +"<a  href='javascript:void(0)' onclick=ShowTimeline('/IPI/MaintainSeamICL/ShowTimeline?HeaderID="+Convert.ToInt32(uc.HeaderId)+"&LineId="+Convert.ToInt32(uc.LineId)+"');><i style='' class='iconspace fa fa-clock-o'></i></a>"+
                                MaintainIPIActionbutton(uc.HeaderId, uc.LineId,uc.QualityProject,uc.Project.Split('-')[0].Trim(),uc.SeamNo, uc.BU.Split('-')[0].Trim(), uc.Location.Split('-')[0].Trim(),string.IsNullOrWhiteSpace(uc.StageCode)?"": uc.StageCode.Split('-')[0].Trim(),uc.StageCode,uc.StageSequance,uc.StageType)
                                //+ "<a class='btn btn-xs' href='javascript:void(0)' onclick=ShowProtocol('/PROTOCOL/"+ uc.ProtocolType +"/Linkage/"+ Convert.ToInt32(uc.ProtocolId) +"?m=1');><i style='margin-left:5px;' class='fa fa-file-text'></i></a>",
                              
                             + ((Manager.IsStageOffered(objQMS030.QualityProject, objQMS030.BU, objQMS030.Location, objQMS030.SeamNo, uc.StageCode.Split('-')[0].Trim(), uc.StageSequance.Value) || (isLTFPSProject && isStageLinkedInLTFPS(uc.LineId))) ?
                                        HTMLActionString(uc.LineId,uc.StageStatus,"ViewProtocol","View Protocol","fa fa-file-text", uc.ProtocolType == clsImplementationEnum.ProtocolType.Other_Files.GetStringValue() ? "OpenAttachmentPopUp("+uc.LineId+", false, \"QMS031/" + uc.LineId+"\")" : "ShowProtocol(\"" + WebsiteURL + "/PROTOCOL/" + uc.ProtocolType + "/Linkage/" + Convert.ToInt32(uc.ProtocolId) + "\")", uc.ProtocolType == clsImplementationEnum.ProtocolType.Other_Files.GetStringValue() ? false : uc.ProtocolId == null)

                                    :HTMLActionStringWithoutStatus(uc.LineId,"ViewProtocol","View Protocol","fa fa-file-text",string.Equals(objQMS030.Status,clsImplementationEnum.ICLSeamStatus.Approved.GetStringValue(),StringComparison.OrdinalIgnoreCase) ? "AskForICLRevision("+ uc.HeaderId +","+ uc.LineId +",\"" + WebsiteURL + "/PROTOCOL/"+ uc.ProtocolType +"/Linkage/"+ Convert.ToInt32(uc.ProtocolId) + "\" ,\""+uc.ProtocolType+"\")" :
                                                                                                                            uc.ProtocolType == clsImplementationEnum.ProtocolType.Other_Files.GetStringValue() ? "ShowOtherFiles("+uc.LineId+","+((string.Equals(objQMS030.Status,clsImplementationEnum.ICLSeamStatus.SendForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(uc.StageStatus,clsImplementationEnum.ICLSeamStatus.Deleted.GetStringValue(),StringComparison.OrdinalIgnoreCase) || Manager.IsStageOffered(objQMS030.QualityProject, objQMS030.BU, objQMS030.Location, objQMS030.SeamNo, uc.StageCode.Split('-')[0].Trim(), uc.StageSequance.Value) || (isLTFPSProject && isStageLinkedInLTFPS(uc.LineId))) ? "0" : "1") + ")":"ShowProtocol(\"" + WebsiteURL + "/PROTOCOL/"+ uc.ProtocolType +"/Linkage/"+ Convert.ToInt32(uc.ProtocolId) + ((string.Equals(objQMS030.Status,clsImplementationEnum.ICLSeamStatus.SendForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(uc.StageStatus,clsImplementationEnum.ICLSeamStatus.Deleted.GetStringValue(),StringComparison.OrdinalIgnoreCase) || Manager.IsStageOffered(objQMS030.QualityProject, objQMS030.BU, objQMS030.Location, objQMS030.SeamNo, uc.StageCode.Split('-')[0].Trim(), uc.StageSequance.Value) || (isLTFPSProject && isStageLinkedInLTFPS(uc.LineId))) ? "" : "?m=1") + "\")",uc.ProtocolType == clsImplementationEnum.ProtocolType.Other_Files.GetStringValue() ? false : uc.ProtocolId == null ))
                                //+ HTMLActionStringWithoutStatus(uc.LineId,"ViewProtocol","View Protocol","fa fa-file-text",string.Equals(objQMS030.Status,clsImplementationEnum.ICLSeamStatus.Approved.GetStringValue(),StringComparison.OrdinalIgnoreCase) ? "AskForICLRevision("+ uc.HeaderId +","+ uc.LineId +",\"" + WebsiteURL + "/PROTOCOL/"+ uc.ProtocolType +"/Linkage/"+ Convert.ToInt32(uc.ProtocolId) + "\")" : "ShowProtocol(\"" + WebsiteURL + "/PROTOCOL/"+ uc.ProtocolType +"/Linkage/"+ Convert.ToInt32(uc.ProtocolId) + ((string.Equals(objQMS030.Status,clsImplementationEnum.ICLSeamStatus.SendForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(uc.StageStatus,clsImplementationEnum.ICLSeamStatus.Deleted.GetStringValue(),StringComparison.OrdinalIgnoreCase) || uc.StageSequance <= maxOfferedSequenceNo || isStageLinkedInLTFPS(uc.LineId)) ? "" : "?m=1") + "\")",uc.ProtocolType == clsImplementationEnum.ProtocolType.Other_Files.GetStringValue() ? false : uc.ProtocolId == null )
                                +"</center></nobr>"
                           }).ToList();
                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult LoadLinkProtocolPartial(int LineId, bool isSeam = true)
        {
            ViewBag.LineId = LineId;
            ViewBag.isSeam = isSeam;
            return PartialView("_LinkProtocolPartial");
        }

        [HttpPost]
        public JsonResult LoadLinkProtocolGridData(JQueryDataTableParamModel param)
        {
            try
            {
                NDEModels objNDEModels = new NDEModels();
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = " 1=1 ";

                string[] columnName = { "ProtocolType", "Description" };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);


                int LineId = Convert.ToInt32(param.Headerid);
                if (param.Flag)
                {
                    QMS031 objQMS031 = db.QMS031.FirstOrDefault(c => c.LineId == LineId);
                    if (objQMS031 != null)
                    {
                        strWhere += " AND StageCode='" + objQMS031.StageCode + "' AND BU='" + objQMS031.BU + "' AND Location='" + objQMS031.Location + "'";
                    }
                }
                else
                {
                    QMS036 objQMS036 = db.QMS036.FirstOrDefault(c => c.LineId == LineId);
                    if (objQMS036 != null)
                    {
                        strWhere += " AND StageCode='" + objQMS036.StageCode + "' AND BU='" + objQMS036.BU + "' AND Location='" + objQMS036.Location + "'";
                    }
                }


                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_IPI_MAINTAIN_PROTOCOLS_INDEX_DATA
                                (
                                   StartIndex,
                                   EndIndex,
                                   strSortOrder,
                                   strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                            {
                                Convert.ToString(uc.ProtocolDescription),
                                Convert.ToString(uc.Description),
                                "<nobr><center>"
                                + "<input class=\"btn submit\" id=\"btnViewProtocol\" onclick=\"ViewProtocolDetails('/PROTOCOL/"+ uc.ProtocolType +"/Details/"+ uc.ProtocolHeaderId +"');\" value=\"View\" style=\"margin-right: 5px;\" type=\"button\">"
                                + "<input class=\"btn save\" id=\"btnLinkProtocol\" onclick=\"LinkProtocol("+ LineId +",'"+ uc.ProtocolType +"','"+ uc.ProtocolHeaderId +"','"+param.Flag+"');\" value=\"Link\" type=\"button\">"
                                 + "</center></nobr>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult LinkProtocol(int LineID)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                QMS031 objQMS031 = db.QMS031.Where(x => x.LineId == LineID).FirstOrDefault();
                if (objQMS031 != null)
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol linked successfully.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #region Chemical/pmi/ferrite/hardness button
        public string MaintainIPIActionbutton(int headerid, int lineid, string qproject, string project, string SeamNo, string bu, string loc, string stage, string stagedesc, int? stageseq, string stagetype = "")
        {
            string strButtons = Helper.GenerateActionIcon(lineid, "", "", "fa fa-flask", "", "", true);

            var objTP = db.QMS021.Where(x => x.QualityProject == qproject && x.Project == project && x.SeamNo == SeamNo && x.Location == loc && x.BU == bu
                           && x.StageCode == stage && x.StageSequance == stageseq).FirstOrDefault();

            if (objTP != null)
            {
                if (stagetype == clsImplementationEnum.SeamStageType.Chemical.GetStringValue() || stagetype == clsImplementationEnum.SeamStageType.PMI.GetStringValue())
                {
                    QMS022 objQMS022 = objTP != null ? db.QMS022.Where(i => i.TPLineId == objTP.LineId).OrderByDescending(x => x.TPRevNo).FirstOrDefault() : null;
                    if (objQMS022 != null)
                    {
                        strButtons = strButtons = Helper.GenerateActionIcon(lineid, "Sampling Plan", "Sampling Plan", "fa fa-flask",
                                 "LoadIPIMaintainTestPlanSamplingPlan(" + objTP.HeaderId + "," + objTP.LineId + ",'" + qproject + "','" + project + "','" + bu + "','" + loc + "','" + objTP.SeamNo + "'," + objTP.RevNo + ",'" + stage + "','" + 0 + "','true','false','Maintain Sampling Plan')", "", false);
                    }
                    else
                    {
                        List<string> lstQproj = new List<string>();
                        lstQproj.Add(qproject);
                        lstQproj.AddRange(db.QMS017.Where(c => c.BU == bu && c.Location == loc && c.IQualityProject == qproject).Select(x => x.QualityProject).ToList());

                        var QualityId = db.QMS020.FirstOrDefault(x => x.QualityProject == qproject && x.SeamNo == SeamNo && x.Location == loc).QualityId;
                        var Qrev = db.QMS016.FirstOrDefault(x => x.QualityId == QualityId && lstQproj.Contains(x.QualityProject) && x.Location == loc && x.StageCode == stage).RevNo;
                        if (!string.IsNullOrWhiteSpace(QualityId))
                        {
                            if (stagetype == clsImplementationEnum.SeamStageType.Chemical.GetStringValue() || stagetype == clsImplementationEnum.SeamStageType.PMI.GetStringValue())
                            {
                                strButtons = strButtons = Helper.GenerateActionIcon(lineid, "Sampling Plan", "Sampling Plan", "fa fa-flask",
                                         "LoadIPIMaintainSamplingPlan(" + headerid + "," + lineid + ",'" + qproject + "','" + project + "','" + bu + "','" + loc + "','" + QualityId + "'," + Qrev + ",'" + stage + "','" + 0 + "','false','false','Maintain Sampling Plan')", "", false);
                            }
                        }
                    }
                }
                else if (stagetype == clsImplementationEnum.SeamStageType.Hardness.GetStringValue())
                {
                    if (objTP.HardnessRequiredValue != null && objTP.HardnessUnit != null)
                    {
                        strButtons = Helper.GenerateActionIcon(lineid, "Maintain Hardness required value", "Maintain Hardness required value", "fa fa-flask",
                                   "LoadTestPlanHardnessFerritePartial(" + objTP.LineId + ",'false','Maintain Hardness required value')", "", false);
                    }
                    else
                    {
                        List<string> lstQproj = new List<string>();
                        lstQproj.Add(qproject);
                        lstQproj.AddRange(db.QMS017.Where(c => c.BU == bu && c.Location == loc && c.IQualityProject == qproject).Select(x => x.QualityProject).ToList());

                        var QualityId = objTP.QMS020.QualityId;
                        var Qrev = db.QMS016.FirstOrDefault(x => x.QualityId == QualityId && lstQproj.Contains(x.QualityProject) && x.Location == loc && x.StageCode == stage).RevNo;
                        if (!string.IsNullOrWhiteSpace(QualityId))
                        {

                            strButtons = Helper.GenerateActionIcon(lineid, "Maintain Hardness required value", "Maintain Hardness required value", "fa fa-flask",
                               "LoadHardnessFerritein_TP_ICL_Partial(0,'" + qproject + "','" + project + "','" + bu + "','" + loc + "','" + QualityId + "'," + Qrev + ",'" + stage + "','false','Maintain Hardness required value')", "", false);
                        }
                    }
                }
                else if (stagetype == clsImplementationEnum.SeamStageType.Ferrite.GetStringValue())
                {
                    if (objTP.FerriteMaxValue != null && objTP.FerriteMinValue != null && objTP.FerriteUnit != null)
                    {
                        strButtons = Helper.GenerateActionIcon(lineid, "Maintain ferrite test required range", "Maintain ferrite test required range", "fa fa-flask",
                           "LoadTestPlanHardnessFerritePartial(" + objTP.LineId + ",'false','Maintain ferrite test required range')", "", false);
                    }
                    else
                    {
                        List<string> lstQproj = new List<string>();
                        lstQproj.Add(qproject);
                        lstQproj.AddRange(db.QMS017.Where(c => c.BU == bu && c.Location == loc && c.IQualityProject == qproject).Select(x => x.QualityProject).ToList());

                        var QualityId = objTP.QMS020.QualityId;
                        var Qrev = db.QMS016.FirstOrDefault(x => x.QualityId == QualityId && lstQproj.Contains(x.QualityProject) && x.Location == loc && x.StageCode == stage).RevNo;
                        if (!string.IsNullOrWhiteSpace(QualityId))
                        {
                            strButtons = Helper.GenerateActionIcon(lineid, "Maintain ferrite test required range", "Maintain ferrite test required range", "fa fa-flask",
                               "LoadHardnessFerritein_TP_ICL_Partial(0,'" + qproject + "','" + project + "','" + bu + "','" + loc + "','" + QualityId + "'," + Qrev + ",'" + stage + "','false','Maintain ferrite test required range')", "", false);
                        }
                    }
                }
            }

            return strButtons;
        }
        #endregion

        [HttpPost]
        public ActionResult AttachedProtocol(int LineId, string ProtocolType, string ProtocolNo, string ProtocolHeaderId = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (LineId > 0)
                {
                    int? pHeaderId = null;
                    if (!string.IsNullOrWhiteSpace(Convert.ToString(ProtocolHeaderId)))
                    {
                        pHeaderId = Convert.ToInt32(ProtocolHeaderId);
                    }

                    QMS031 objQMS031 = db.QMS031.Where(x => x.LineId == LineId).FirstOrDefault();

                    if (Convert.ToString(objQMS031.ProtocolType) == ProtocolType)
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "nochanged";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        clsHelper.ProtocolHeaderData headerData = new clsHelper.ProtocolHeaderData();
                        headerData.QualityProject = objQMS031.QualityProject;
                        headerData.SeamNo = objQMS031.SeamNo;
                        headerData.Project = objQMS031.Project;
                        headerData.BU = objQMS031.BU;
                        headerData.Location = objQMS031.Location;
                        headerData.StageCode = objQMS031.StageCode;
                        headerData.StageSequence = objQMS031.StageSequance.Value;
                        headerData.ICLRevNo = objQMS031.QMS030.RevNo;

                        int headerId = 0;
                        if (!string.IsNullOrWhiteSpace(ProtocolType) && ProtocolType != clsImplementationEnum.ProtocolType.Other_Files.GetStringValue() && string.IsNullOrWhiteSpace(objQMS031.ProtocolType))
                        {
                            #region Code for attach protocol
                            string role = clsImplementationEnum.ProtocolRoleType.R_Requiredvalue_for_QC.GetStringValue();
                            headerId = Manager.LinkProtocolWithStage(ProtocolType, ProtocolNo, headerData, pHeaderId, role);

                            #endregion
                        }
                        else if (string.IsNullOrWhiteSpace(ProtocolType) && !string.IsNullOrWhiteSpace(objQMS031.ProtocolType))
                        {
                            if (objQMS031.ProtocolType == clsImplementationEnum.ProtocolType.Other_Files.GetStringValue())
                            {
                                #region Code for remove all attached files

                                string deletefolderPath = "QMS031//" + objQMS031.LineId;
                                Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                                _objFUC.DeleteAllFileOnFCSServerAsync(objQMS031.LineId, deletefolderPath, deletefolderPath, DESServices.CommonService.GetUseIPConfig);
                                //var lstDocs = (new clsFileUpload()).GetDocuments(deletefolderPath, true).Select(x => x.Name).ToList();
                                //foreach (var filename in lstDocs)
                                //{
                                //    (new clsFileUpload()).DeleteFileAsync(deletefolderPath, filename.ToString());
                                //}
                                headerId = -1;
                                #endregion
                            }
                            else
                            {
                                #region Code for detach old protocol

                                if (objQMS031.ProtocolId.HasValue)
                                {
                                    if (Manager.DelinkProtocolWithStage(objQMS031.ProtocolId.Value, objQMS031.ProtocolType))
                                        headerId = -1;
                                }
                                #endregion
                            }
                        }
                        else if (!string.IsNullOrWhiteSpace(ProtocolType) && !string.IsNullOrWhiteSpace(objQMS031.ProtocolType))
                        {

                            if (objQMS031.ProtocolType == clsImplementationEnum.ProtocolType.Other_Files.GetStringValue())
                            {
                                #region Code for remove all attached files

                                string deletefolderPath = "QMS031//" + objQMS031.LineId;
                                Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                                _objFUC.DeleteAllFileOnFCSServerAsync(objQMS031.LineId, deletefolderPath, deletefolderPath, DESServices.CommonService.GetUseIPConfig);
                                //var lstDocs = (new clsFileUpload()).GetDocuments(deletefolderPath, true).Select(x => x.Name).ToList();
                                //foreach (var filename in lstDocs)
                                //{
                                //    (new clsFileUpload()).DeleteFileAsync(deletefolderPath, filename.ToString());
                                //}
                                #endregion
                            }
                            else
                            {
                                #region Code for detach old protocol

                                if (objQMS031.ProtocolId.HasValue)
                                {
                                    Manager.DelinkProtocolWithStage(objQMS031.ProtocolId.Value, objQMS031.ProtocolType);
                                }
                                #endregion
                            }

                            if (ProtocolType != clsImplementationEnum.ProtocolType.Other_Files.GetStringValue())
                            {
                                #region Code for attach new protocol
                                string role = clsImplementationEnum.ProtocolRoleType.R_Requiredvalue_for_QC.GetStringValue();
                                headerId = Manager.LinkProtocolWithStage(ProtocolType, ProtocolNo, headerData, pHeaderId, role);

                                #endregion
                            }

                        }

                        if (Convert.ToString(ProtocolType) == clsImplementationEnum.ProtocolType.Other_Files.GetStringValue())
                        {
                            headerId = -2;
                        }

                        if (headerId != 0)
                        {
                            #region Update Seam ICL Data With Attched / Detached Protocol

                            if (objQMS031.QMS030.Status == clsImplementationEnum.ICLSeamStatus.Approved.GetStringValue())
                            {
                                objQMS031.QMS030.RevNo = Convert.ToInt32(objQMS031.QMS030.RevNo) + 1;
                                objQMS031.QMS030.Status = clsImplementationEnum.ICLSeamStatus.Draft.GetStringValue();
                                objQMS031.QMS030.EditedBy = objClsLoginInfo.UserName;
                                objQMS031.QMS030.EditedOn = DateTime.Now;
                                objQMS031.QMS030.SubmittedBy = null;
                                objQMS031.QMS030.SubmittedOn = null;
                                objQMS031.QMS030.ApprovedBy = null;
                                objQMS031.QMS030.ApprovedOn = null;
                            }
                            objQMS031.StageStatus = clsImplementationEnum.ICLSeamStatus.Modified.GetStringValue();
                            objQMS031.EditedBy = objClsLoginInfo.UserName;
                            objQMS031.EditedOn = DateTime.Now;
                            if (headerId > 0)
                            {
                                objQMS031.ProtocolId = headerId;
                                objQMS031.ProtocolType = ProtocolType;
                                objQMS031.ProtocolLinkedBy = objClsLoginInfo.UserName;
                                objQMS031.ProtocolLinkedOn = DateTime.Now;
                                objResponseMsg.Value = "protocol successfully attached";
                            }
                            else
                            {
                                if (headerId == -2)
                                {
                                    objQMS031.ProtocolId = null;
                                    objQMS031.ProtocolType = ProtocolType;
                                    objQMS031.ProtocolLinkedBy = objClsLoginInfo.UserName;
                                    objQMS031.ProtocolLinkedOn = DateTime.Now;
                                    objResponseMsg.Value = "you can attach protocol as form of files.";
                                }
                                else
                                {
                                    objQMS031.ProtocolId = null;
                                    objQMS031.ProtocolType = null;
                                    objQMS031.ProtocolLinkedBy = null;
                                    objQMS031.ProtocolLinkedOn = null;
                                    objResponseMsg.Value = "protocol successfully detached";
                                }
                            }

                            db.SaveChanges();

                            objResponseMsg.Key = true;
                            objResponseMsg.HeaderStatus = objQMS031.QMS030.Status;
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

                            #endregion
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Link protocol fail...!";
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Link protocol fail...!";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error Occured, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        public bool CheckDuplicateStage(int headerID, string stageCode, int stageSequence)
        {
            return db.QMS031.Any(c => c.HeaderId == headerID && c.StageCode == stageCode && c.StageSequance == stageSequence);
        }

        //save stages (for editable Grid)
        [HttpPost]
        public ActionResult SaveNewStages(FormCollection fc)
        {
            int newRowIndex = 0;
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int headerId = Convert.ToInt32(fc["HeaderId" + newRowIndex]);
                bool key = true;
                QMS030 objQMS030 = db.QMS030.Where(x => x.HeaderId == headerId).FirstOrDefault();
                int maxOfferedSequenceNo = (new clsManager()).GetMaxOfferedSequenceNo(objQMS030.QualityProject, objQMS030.BU, objQMS030.Location, objQMS030.SeamNo);
                if (Convert.ToInt32(fc["StageSequance" + newRowIndex]) <= maxOfferedSequenceNo)
                    key = false;
                else
                    key = true;
                if (objQMS030.Status == clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Status = objQMS030.Status;
                    objResponseMsg.Remarks = objResponseMsg.Remarks = clsImplementationMessage.CommonMessages.AlreadySubmitted;
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                if (!key)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Sequence no should be greater then " + maxOfferedSequenceNo;
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                if (CheckDuplicateStage(headerId, fc["StageCode" + newRowIndex], Convert.ToInt32(fc["StageSequance" + newRowIndex])))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.TestPlanMessages.DuplicateStage;
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                QMS031 objQMS031 = new QMS031();
                //QMS030 objQMS030 = new QMS030();

                string addedStatus = clsImplementationEnum.TestPlanStatus.Added.GetStringValue();
                string modifiedStatus = clsImplementationEnum.TestPlanStatus.Modified.GetStringValue();
                string deletedStatus = clsImplementationEnum.TestPlanStatus.Deleted.GetStringValue();

                objQMS030 = db.QMS030.Where(x => x.HeaderId == headerId).FirstOrDefault();
                objQMS031.QualityProject = objQMS030.QualityProject;
                objQMS031.Project = objQMS030.Project;
                objQMS031.HeaderId = objQMS030.HeaderId;
                objQMS031.BU = objQMS030.BU;
                objQMS031.Location = objQMS030.Location;
                objQMS031.SeamNo = objQMS030.SeamNo;
                objQMS031.StageCode = fc["StageCode" + newRowIndex];
                objQMS031.StageSequance = Convert.ToInt32(fc["StageSequance" + newRowIndex]);
                objQMS031.AcceptanceStandard = fc["AcceptanceStandard" + newRowIndex].Replace(",", ""); ;
                objQMS031.ApplicableSpecification = fc["ApplicableSpecification" + newRowIndex].Replace(",", ""); ;
                objQMS031.InspectionExtent = fc["InspectionExtent" + newRowIndex];
                objQMS031.Remarks = fc["Remarks" + newRowIndex];

                objQMS031.HeaderId = objQMS030.HeaderId;
                if (objQMS030.Status == clsImplementationEnum.ICLSeamStatus.Approved.GetStringValue())
                {
                    objQMS030.RevNo = Convert.ToInt32(objQMS030.RevNo) + 1;
                    objQMS030.Status = clsImplementationEnum.ICLSeamStatus.Draft.GetStringValue();
                    objQMS030.EditedBy = objClsLoginInfo.UserName;
                    objQMS030.EditedOn = DateTime.Now;
                    objQMS030.SubmittedBy = null;
                    objQMS030.SubmittedOn = null;
                    objQMS030.ApprovedBy = null;
                    objQMS030.ApprovedOn = null;
                }

                //objQMS031.RevNo = Convert.ToInt32(fc["RevNo" + newRowIndex]);
                objQMS031.StageStatus = addedStatus;
                objQMS031.CreatedBy = objClsLoginInfo.UserName;
                objQMS031.CreatedOn = DateTime.Now;
                db.QMS031.Add(objQMS031);
                db.SaveChanges();


                objResponseMsg.Key = true;
                objResponseMsg.HeaderId = objQMS031.LineId;
                objResponseMsg.Value = clsImplementationMessage.TestPlanMessages.LineInsert;
                objResponseMsg.Status = objQMS030.Status;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.HeaderId = 0;
                objResponseMsg.Value = clsImplementationMessage.TestPlanMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        [HttpPost]
        public ActionResult GetQualityProjectDetails(string term)
        {
            List<Projects> lstQualityProject = new List<Projects>();
            lstQualityProject = db.QMS030.Where(i => term == "" ? true : i.QualityProject.Contains(term)
                                                    ).Select(i => new Projects { Value = i.QualityProject, projectCode = i.QualityProject, Text = i.Location }).Distinct().Take(10).ToList();

            return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult FetchQualityProjectwiseDetails(string qualityProject, string location)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                var objQMS030 = db.QMS030.Where(i => i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase) && i.Location == location).FirstOrDefault();
                if (objQMS030 != null)
                {
                    objResponseMsg.ProjectCode = objQMS030.Project;
                    objResponseMsg.BUCode = objQMS030.BU;
                    objResponseMsg.LocationCode = objQMS030.Location;

                    objResponseMsg.Project = db.COM001.Where(i => i.t_cprj.Equals(objQMS030.Project.Trim(), StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + "-" + i.t_dsca).FirstOrDefault();// project;
                    objResponseMsg.BU = db.COM002.Where(x => x.t_dimx.Trim() == objQMS030.BU.Trim()).Select(x => x.t_desc).FirstOrDefault();
                    objResponseMsg.Location = db.COM002.Where(x => x.t_dimx.Trim() == objQMS030.Location.Trim()).Select(x => x.t_desc).FirstOrDefault();
                    objResponseMsg.HeaderId = objQMS030.HeaderId;
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult LoadResequenceData(int headerId)
        {
            QMS030 objQMS030 = new QMS030();
            objQMS030 = db.QMS030.Where(x => x.HeaderId == headerId).FirstOrDefault();
            return PartialView("_LoadResequenceData", objQMS030);
        }
        [HttpPost]
        public JsonResult LoadResequenceGridData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "";
                int HeaderId = Convert.ToInt32(param.Headerid);
                QMS030 objQMS030 = db.QMS030.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                strWhere += Manager.MakeWhereConditionForCTQHeaderLines(param.Headerid).ToString();
                string[] columnName = { "StageCode", "StageSequance", "NewSequance", "Status", "LineId" };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                int maxOfferedSequenceNo = (new clsManager()).GetMaxOfferedSequenceNo(objQMS030.QualityProject, objQMS030.BU, objQMS030.Location, objQMS030.SeamNo);
                bool isLTFPSProject = Manager.IsLTFPSProject(objQMS030.Project, objQMS030.BU, objQMS030.Location);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_IPI_ICL_RESEQUENCING_DETAILS
                                (
                               StartIndex,
                               EndIndex,
                               strSortOrder,
                               strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.StageCode),
                                Convert.ToString(uc.StageSequance),
                                (Manager.IsStageOffered(objQMS030.QualityProject, objQMS030.BU, objQMS030.Location, objQMS030.SeamNo, uc.StageCode.Split('-')[0].Trim(), uc.StageSequance.Value) || (isLTFPSProject && isStageLinkedInLTFPS(uc.LineId)))?Convert.ToString(uc.NewSequence): getNewSequence(uc.NewSequence),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId),
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdateSequence(int lineId, int header, int newsequence, string fromSeam, string toSeam)
        {
            //clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                QMS031 objQMS031 = db.QMS031.Find(lineId);
                if (newsequence != 0)
                {

                    var TotalAffectedData = db.SP_IPI_SEAM_ICL_UPDATE_STAGE_SEQUENCE(objQMS031.QualityProject, objQMS031.BU, objQMS031.Location, fromSeam, toSeam, objQMS031.StageCode, objQMS031.StageSequance, newsequence, objClsLoginInfo.UserName).ToList();
                    int? count = TotalAffectedData.Count > 0 ? TotalAffectedData.Select(x => x.sCount).FirstOrDefault() : 0;
                    string seamno = string.Join(", ", TotalAffectedData.Select(x => x.seamno).Distinct());
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = count + " Seams affected.";
                    objResponseMsg.Status = objQMS031.QMS030.Status;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Stage sequence should be greater than 0";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public string getNewSequence(int? newSequance)
        {
            if (newSequance != 0)
            {
                return "<input type='text' name='txtNewSequence'  onkeypress = 'return isNumber(event)' class='form-control input-sm input-small input-inline' value='" + newSequance + "' />";
            }
            else
            {
                return "<input type='text' name='txtNewSequence'  onkeypress = 'return isNumber(event)' class='form-control input-sm input-small input-inline' value='" + "" + "' />";
            }
        }

        #region History
        [HttpPost]
        public ActionResult GetTestPlanHistoryDetails(int Id) //partial for CTQ History Lines Details
        {
            QMS030_Log objQMS030Log = new QMS030_Log();
            objQMS030Log = db.QMS030_Log.Where(x => x.HeaderId == Id).FirstOrDefault();
            return PartialView("_TPHistoryPartial", objQMS030Log);
        }

        public ActionResult ViewLogDetails(int? Id)
        {
            QMS030_Log objQMS030 = new QMS030_Log();
            if (Id > 0)
            {
                objQMS030 = db.QMS030_Log.Where(i => i.Id == Id).FirstOrDefault();
                objQMS030.BU = db.COM002.Where(a => a.t_dimx == objQMS030.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objQMS030.Project = db.COM001.Where(i => i.t_cprj == objQMS030.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objQMS030.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objQMS030.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                ViewBag.Action = "edit";
            }
            return View(objQMS030);
        }


        [HttpPost]
        public JsonResult LoadTestPlanHeaderHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;


                strWhere += "1=1 and HeaderId=" + param.Headerid;
                string[] columnName = { "QualityProject", "Location", "PTCApplicable", "PTCNumber", "Status" };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_IPI_SEAM_ICL_HISTORY_HEADER_DETAILS
                                (StartIndex, EndIndex, strSortOrder, strWhere).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.QualityProject),
                            Convert.ToString(uc.Location),
                            Convert.ToString(uc.SeamNo),
                            Convert.ToString(uc.RevNo),
                            Convert.ToString(uc.PTCApplicable),
                            Convert.ToString(uc.PTCNumber),
                            Convert.ToString(uc.TPRevNo),
                            Convert.ToString(uc.Status),
                            "<center><a class='btn btn-xs green' target='_blank' href='" + WebsiteURL + "/IPI/MaintainSeamICL/ViewLogDetails?ID="+uc.Id+"'>View<i style='' class='iconspace fa fa-eye'></i></a><a class='btn btn-xs' href='javascript:void(0)' onclick=ShowTimeline('/IPI/MaintainSeamICL/ShowLogTimeline?id="+Convert.ToInt32(uc.Id)+"');><i style='' class='iconspace fa fa-clock-o'></i></a></center>",
                            }).ToList();



                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadTestPlanLineHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "";
                strWhere += "1=1 and RefId=" + param.Headerid;

                string[] columnName = { "StageCode", "StageSequance", "AcceptanceStandard", "ApplicableSpecification", "InspectionExtent", "StageStatus", "Remarks", "LineId", };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_IPI_SEAM_ICL_HISTORY_LINE_DETAILS
                                (
                               StartIndex,
                               EndIndex,
                               strSortOrder,
                               strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.StageCode),
                                Convert.ToString(uc.StageSequance),
                                Convert.ToString(uc.AcceptanceStandard),
                                Convert.ToString(uc.ApplicableSpecification),
                                Convert.ToString(uc.InspectionExtent),
                                 Convert.ToString(uc.StageStatus),
                                Convert.ToString(uc.Remarks),
                              MaintainIPIActionbutton(uc.HeaderId, uc.LineId,uc.QualityProject,uc.Project.Split('-')[0].Trim(),uc.SeamNo, uc.BU.Split('-')[0].Trim(), uc.Location.Split('-')[0].Trim(),uc.StageCode.Split('-')[0].Trim(),uc.StageCode,uc.StageSequance,uc.StageType)
                             +HTMLActionStringWithoutStatus(uc.LineId,"ViewProtocol","View Protocol","fa fa-file-text",uc.ProtocolType == clsImplementationEnum.ProtocolType.Other_Files.GetStringValue() ? "OpenAttachmentPopUp("+uc.LineId+", false, \"QMS031/" + uc.LineId+"\")" :"ShowProtocol(\"" + WebsiteURL + "/PROTOCOL/"+ uc.ProtocolType +"/Linkage/"+ Convert.ToInt32(uc.ProtocolId) + "\")",uc.ProtocolType == clsImplementationEnum.ProtocolType.Other_Files.GetStringValue() ? false : uc.ProtocolId == null)
                           }).ToList();


                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Copy
        [HttpPost]
        public ActionResult GetCopyDetails(int HeaderID)
        {
            var objQMS030 = db.QMS030.Where(i => i.HeaderId == HeaderID).FirstOrDefault();

            ViewBag.QualityProject = objQMS030.QualityProject;
            ViewBag.SeamNo = objQMS030.SeamNo;

            ViewBag.Seams = db.QMS030.Where(i => i.QualityProject.Equals(objQMS030.QualityProject, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(objQMS030.Location, StringComparison.OrdinalIgnoreCase) && i.BU.Equals(objQMS030.BU, StringComparison.OrdinalIgnoreCase)).OrderBy(i => i.SeamNo).Select(i => i.SeamNo).ToList();

            return PartialView("_LoadTPCopyDetailPartial");
        }
        [HttpPost]
        public ActionResult copyDetails(int strheaderID, string fromSeam, string toSeam)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objQMS030 = db.QMS030.Where(i => i.HeaderId == strheaderID).FirstOrDefault();

                List<QMS030> allSeams = db.QMS030.Where(i => i.QualityProject.Equals(objQMS030.QualityProject, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(objQMS030.Location, StringComparison.OrdinalIgnoreCase) && i.BU.Equals(objQMS030.BU, StringComparison.OrdinalIgnoreCase)).OrderBy(i => i.SeamNo).ToList();

                List<string> newSeamList = new List<string>();
                bool start = false;

                foreach (QMS030 seam in allSeams)
                {
                    if (seam.SeamNo == fromSeam)
                    {
                        start = true;
                    }

                    if (start)
                        newSeamList.Add(seam.SeamNo);

                    if (seam.SeamNo == toSeam)
                    {
                        break;
                    }
                }


                //int h1 = (from a in db.QMS020
                //          where a.SeamNo == fromSeam
                //          select a.HeaderId).FirstOrDefault();

                //int h2 = (from a in db.QMS020
                //          where a.SeamNo == toSeam
                //          select a.HeaderId).FirstOrDefault();

                //var approveString = clsImplementationEnum.TestPlanStatus.Approved.GetStringValue();
                //var lstcopiedseam = (from a in db.QMS020
                //                     where a.HeaderId >= h1 && a.HeaderId <= h2 && a.QualityId != null && a.Status != approveString && a.HeaderId != strheaderID
                //                     select a.SeamNo).ToList();

                //var lstuncopiedseam = (from a in db.QMS020
                //                       where a.HeaderId >= h1 && a.HeaderId <= h2 && a.QualityId == null && a.Status == approveString && a.HeaderId != strheaderID
                //                       select a.SeamNo).ToList();

                //if (h1 > h2)
                //{
                //    objResponseMsg.Key = false;
                //    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                //}
                //else
                //{
                //var strlstseam = string.Join(",", (from a in db.QMS020
                //                                   where a.HeaderId >= h1 && a.HeaderId <= h2
                //                                   select a.SeamNo).ToList());

                var strlstseam = string.Join(",", newSeamList);

                string currentUser = objClsLoginInfo.UserName;
                //var Result = db.SP_IPI_TEST_PLAN_COPY(currentUser, strheaderID, strlstseam).FirstOrDefault();
                var Result = 0;
                if (Convert.ToInt32(Result) > 0)
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = Convert.ToString(Result) + " Seam data is copied successfully";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Seams found for copy";
                }

                //}
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Transfer
        [HttpPost]
        public ActionResult LoadTPTransferPartial(string qualityProject, string location)
        {
            ViewBag.QualityProject = qualityProject;
            ViewBag.Location = location;//db.COM002.Where(a => a.t_dimx == location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            return PartialView("_LoadTPTransferDetailPartial");
        }

        //[HttpPost]
        //public ActionResult tranferDetails(int strheaderID, string fromSeam, string toSeam, string fromQProj, string toQProj)
        //{
        //    string currentUser = objClsLoginInfo.UserName;
        //    clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
        //    var approveString = clsImplementationEnum.TestPlanStatus.Approved.GetStringValue();
        //    int h1 = (from a in db.QMS020
        //              where a.SeamNo == fromSeam
        //              select a.HeaderId).FirstOrDefault();

        //    int h2 = (from a in db.QMS020
        //              where a.SeamNo == toSeam
        //              select a.HeaderId).FirstOrDefault();

        //    int q1 = (from a in db.QMS020
        //              where a.QualityProject == fromQProj
        //              select a.HeaderId).FirstOrDefault();

        //    int q2 = (from a in db.QMS020
        //              where a.QualityProject == toQProj
        //              select a.HeaderId).FirstOrDefault();
        //    var lstCopiedseam = string.Join(",", (from a in db.QMS020
        //                                          where a.HeaderId >= h1 && a.HeaderId <= h2 && a.QualityId != null && a.Status != approveString && a.HeaderId != strheaderID
        //                                          select a.SeamNo).ToList());
        //    var strlstseam = string.Join(",", lstCopiedseam);

        //    var lstunCopiedseam = string.Join(",", (from a in db.QMS020
        //                                            where a.HeaderId >= h1 && a.HeaderId <= h2 && a.QualityId == null && a.Status == approveString && a.HeaderId != strheaderID
        //                                            select a.SeamNo).ToList());

        //    var lstQualityProject = (from a in db.QMS020
        //                             where a.HeaderId >= q1 && a.HeaderId <= q2 && a.Location.Contains(objClsLoginInfo.Location)
        //                             select a.QualityProject).ToList();
        //    var strQualityProject = string.Join(",", lstQualityProject);
        //    try
        //    {
        //        if ((h2 >= h1) && (q2 >= q1))
        //        {
        //            var result = db.SP_IPI_TEST_PLAN_TRANSFER(currentUser, strheaderID, strlstseam, strQualityProject).FirstOrDefault();// FirstOrDefault(); //(currentUser, strheaderID, fromSeam, toSeam, Convert.ToString(fromQProj), Convert.ToString(toQProj)).FirstOrDefault();
        //            if (result.Result1.ToString() == "IS")
        //            {
        //                objResponseMsg.Key = true;
        //                objResponseMsg.Value = lstCopiedseam.Count() + " seams data transfered successfully and system has skipped " + lstunCopiedseam.Count() + " seams";
        //                // objResponseMsg.Value = clsImplementationMessage.CommonMessages.Transfer.ToString();
        //            }
        //            else
        //            {
        //                objResponseMsg.Key = false;
        //                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
        //                //objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
        //            }
        //        }
        //        else
        //        {
        //            objResponseMsg.Key = false;
        //            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
        //        }
        //    }

        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        objResponseMsg.Key = false;
        //        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
        //    }
        //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //}

        #endregion

        #region Copy/Transfer Protocol

        [HttpPost]
        public ActionResult LoadCopyProtololPartial(int headerId, string Type)
        {
            QMS030 objQMS030 = db.QMS030.FirstOrDefault(c => c.HeaderId == headerId);

            ViewBag.QualityProject = objQMS030.QualityProject;
            ViewBag.SeamNo = objQMS030.SeamNo;
            ViewBag.Seams = db.QMS030.Where(i => i.Project.Equals(objQMS030.Project, StringComparison.OrdinalIgnoreCase) && i.QualityProject.Equals(objQMS030.QualityProject, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(objQMS030.Location, StringComparison.OrdinalIgnoreCase) && i.BU.Equals(objQMS030.BU, StringComparison.OrdinalIgnoreCase)).OrderBy(i => i.SeamNo).Select(i => i.SeamNo).ToList();
            ViewBag.Type = Type;

            return PartialView("_CopyProtocolPartial");
        }

        [HttpPost]
        public ActionResult LoadQualityprojectwiseProtocolPartial()
        {
            var objQMS030 = db.QMS030.ToList();
            ViewBag.QualityProject = db.QMS030.OrderBy(i => i.QualityProject).Select(i => i.QualityProject).ToList();
            return PartialView("_CopyQualityprojectwiseProtocolPartial");
        }

        [HttpPost]
        public ActionResult GetQualityProjectForTransfer(string term)
        {
            object lstQualityProject;

            if (!string.IsNullOrEmpty(term))
            {
                lstQualityProject = (from a in db.QMS030
                                     where a.QualityProject.Contains(term) && a.Location.Contains(objClsLoginInfo.Location)
                                     orderby a.QualityProject
                                     select new { Value = a.QualityProject, projectCode = a.QualityProject }).Distinct().ToList();
            }
            else
            {
                lstQualityProject = (from a in db.QMS030
                                     where a.Location.Contains(objClsLoginInfo.Location)
                                     orderby a.QualityProject
                                     select new { Value = a.QualityProject, projectCode = a.QualityProject }).Distinct().Take(10).ToList();

            }

            return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSeamNoForCopy(string term, string qualityproject)
        {
            object lstQualityProject;

            lstQualityProject = (from a in db.QMS030
                                 where a.QualityProject == qualityproject && a.Location.Contains(objClsLoginInfo.Location) && (term == "" || a.SeamNo.Contains(term))
                                 orderby a.SeamNo
                                 select new { Value = a.SeamNo, Text = a.SeamNo }).Distinct().ToList();
            return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CopyProtocol(int headerId, string fromSeam, string toSeam)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS030 objQMS030 = db.QMS030.FirstOrDefault(c => c.HeaderId == headerId);
                List<QMS031> SrclstQMS031 = objQMS030.QMS031.Where(c => c.ProtocolType != null).ToList();

                #region Get Destination Seams List For Copy

                List<QMS030> allSeams = db.QMS030.Where(i => i.Project == objQMS030.Project && i.QualityProject == objQMS030.QualityProject && i.Location == objQMS030.Location && i.BU == objQMS030.BU).OrderBy(i => i.SeamNo).ToList();
                List<string> newSeamList = new List<string>();
                bool start = false;
                foreach (QMS030 seam in allSeams)
                {
                    if (seam.SeamNo == fromSeam)
                    {
                        start = true;
                    }
                    if (start)
                        newSeamList.Add(seam.SeamNo);
                    if (seam.SeamNo == toSeam)
                    {
                        break;
                    }
                }
                #endregion

                if (SrclstQMS031.Count > 0)
                {
                    foreach (var seam in newSeamList)
                    {
                        string role = clsImplementationEnum.ProtocolRoleType.R_Requiredvalue_for_QC.GetStringValue();
                        List<SP_IPI_PROTOCOL_COPY_PROTOCOL_Result> sReturnValue = db.SP_IPI_PROTOCOL_COPY_PROTOCOL(objQMS030.QualityProject, objQMS030.Project, objQMS030.BU, objQMS030.Location, objQMS030.SeamNo, seam + "," + seam, objClsLoginInfo.UserName, role).ToList();
                        #region Copy Other files
                        foreach (var item in sReturnValue)
                        {
                            if (!string.IsNullOrEmpty(item.SourceFilePath) && item.TotalSeams > 0)
                            {
                                string srcFolderPath = item.SourceFilePath;
                                string newfolderPath = item.DestinationFilePath;
                                (new clsFileUpload()).CopyFolderContentsAsync(srcFolderPath, newfolderPath);
                            }
                        }
                        #endregion
                    }

                    if (newSeamList.Count > 0)
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = newSeamList.Count.ToString() + " Seams has been updated.";
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No any Seam available for copy protocol.";
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Protocol not maintained in source seam.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult TransferProtocol(int headerId, string fromSourceSeam, string toSourceSeam, string fromDestQualityProject, string toDestQualityProject)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS030 objQMS030 = db.QMS030.FirstOrDefault(c => c.HeaderId == headerId);

                #region Get Destination Seams List For Copy

                List<QMS030> allSeams = db.QMS030.Where(i => i.Project == objQMS030.Project && i.QualityProject == objQMS030.QualityProject && i.Location == objQMS030.Location && i.BU == objQMS030.BU).OrderBy(i => i.SeamNo).ToList();
                List<string> newSeamList = new List<string>();
                bool start = false;
                foreach (QMS030 seam in allSeams)
                {
                    if (seam.SeamNo == fromSourceSeam)
                    {
                        start = true;
                    }
                    if (start)
                        newSeamList.Add(seam.SeamNo);
                    if (seam.SeamNo == toSourceSeam)
                    {
                        break;
                    }
                }
                #endregion

                #region Get Destinatino QMS project for transfer

                List<string> allQMSProjects = (from a in db.QMS030
                                               where a.Location.Contains(objClsLoginInfo.Location)
                                               orderby a.QualityProject
                                               select a.QualityProject).Distinct().ToList();

                List<string> newQMSProjectList = new List<string>();

                start = false;
                foreach (string proj in allQMSProjects)
                {
                    if (proj == fromDestQualityProject)
                    {
                        start = true;
                    }
                    if (start)
                        newQMSProjectList.Add(proj);
                    if (proj == toDestQualityProject)
                    {
                        break;
                    }
                }


                #endregion
                foreach (var seam in newSeamList)
                {
                    string role = clsImplementationEnum.ProtocolRoleType.R_Requiredvalue_for_QC.GetStringValue();
                    List<SP_IPI_PROTOCOL_TRANSFER_PROTOCOL_Result> sReturnValue = db.SP_IPI_PROTOCOL_TRANSFER_PROTOCOL(objQMS030.QualityProject, objQMS030.Project, objQMS030.BU, objQMS030.Location, seam + "," + seam, string.Join(",", newQMSProjectList.Distinct()), objClsLoginInfo.UserName, role).ToList();
                    #region Copy Other files
                    foreach (var item in sReturnValue)
                    {
                        if (!string.IsNullOrEmpty(item.SourceFilePath) && item.TotalSeams > 0)
                        {
                            string srcFolderPath = item.SourceFilePath;
                            string newfolderPath = item.DestinationFilePath;
                            (new clsFileUpload()).CopyFolderContentsAsync(srcFolderPath, newfolderPath);
                        }
                    }
                    #endregion
                }
                if (newSeamList.Count > 0)
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = newSeamList.Count.ToString() + " Seams has been updated.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No any Seam available for copy protocol.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CopyProtocolQualityProjectForTransfer(string sQProject, string sSeam, string fSeam, string tSeam, string fromDestQualityProject, string toDestQualityProject)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS030 objQMS030 = db.QMS030.FirstOrDefault(c => c.QualityProject == sQProject && c.SeamNo == sSeam);
                List<QMS031> SrclstQMS031 = objQMS030.QMS031.Where(c => c.ProtocolType != null).ToList();

                #region Get Destination Seams List For Copy

                List<QMS030> allSeams = db.QMS030.Where(i => i.QualityProject == toDestQualityProject && i.Location == objClsLoginInfo.Location).OrderBy(i => i.SeamNo).ToList();
                List<string> newSeamList = new List<string>();
                bool start = false;
                foreach (QMS030 seam in allSeams)
                {
                    if (seam.SeamNo == fSeam)
                    {
                        start = true;
                    }
                    if (start)
                        newSeamList.Add(seam.SeamNo);
                    if (seam.SeamNo == tSeam)
                    {
                        break;
                    }
                }
                #endregion

                if (SrclstQMS031.Count > 0)
                {
                    foreach (var seam in newSeamList)
                    {
                        string role = clsImplementationEnum.ProtocolRoleType.R_Requiredvalue_for_QC.GetStringValue();
                        List<SP_IPI_PROTOCOL_COPY_PROTOCOL_TO_OTHER_SEAM_Result> sReturnValue = db.SP_IPI_PROTOCOL_COPY_PROTOCOL_TO_OTHER_SEAM(objQMS030.QualityProject, objQMS030.Project, objQMS030.BU, objQMS030.Location, objQMS030.SeamNo, seam + "," + seam, toDestQualityProject, objClsLoginInfo.UserName, role).ToList();
                        #region Copy Other files
                        foreach (var item in sReturnValue)
                        {
                            if (!string.IsNullOrEmpty(item.SourceFilePath) && item.TotalSeams > 0)
                            {
                                string srcFolderPath = item.SourceFilePath;
                                string newfolderPath = item.DestinationFilePath;
                                (new clsFileUpload()).CopyFolderContentsAsync(srcFolderPath, newfolderPath);
                            }
                        }
                        #endregion
                    }

                    if (newSeamList.Count > 0)
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = newSeamList.Count.ToString() + " Seams has been updated.";
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No any Seam available for copy protocol.";
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Protocol not maintained in source seam.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion
        #region insert,update,delete,send for approval (for header and stages)
        //update header data
        [HttpPost]
        public ActionResult UpdateData(int headerId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    //if (columnName == "QualityId")
                    //{
                    //    QMS020 objQMS020 = new QMS020();
                    //    objQMS020 = db.QMS020.Where(x => x.HeaderId == headerId).FirstOrDefault();
                    //    QMS015_Log objQMS015 = new QMS015_Log();
                    //    if (objQMS020 != null)
                    //    {
                    //        if (objQMS020.QualityId != columnValue)
                    //        {
                    //            objQMS015 = db.QMS015_Log.Where(x => x.QualityId == columnValue).FirstOrDefault();
                    //            if (objQMS015 != null)
                    //            {
                    //                objQMS020.QualityId = columnValue;
                    //                objQMS020.QualityIdRev = objQMS015.RevNo;
                    //                db.SaveChanges();
                    //                objResponseMsg = SaveTPHeader(objQMS020, true);
                    //                objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                    //            }
                    //        }
                    //        else
                    //        {
                    //            objResponseMsg.Key = false;
                    //            objResponseMsg.Value = clsImplementationMessage.CommonMessages.DuplicateQID.ToString();
                    //        }
                    //    }
                    //}
                    //else
                    //{
                    if (columnName == "PTCApplicable")
                    {
                        if (columnValue == "True")
                        {
                            columnValue = "1";
                        }
                        else
                        {
                            columnValue = "0";
                        }
                    }
                    //sp contains revision code
                    db.SP_IPI_SEAM_ICL_HEADER_UPDATE(headerId, columnName, columnValue);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                    //}
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //insert stages for quality id from QMS016_log
        public clsHelper.ResponseMsg SaveTPHeader(QMS020 objSrcQMS020, bool IsEdited)
        {
            var ApproveString = clsImplementationEnum.TestPlanStatus.Approved.GetStringValue();
            if (objSrcQMS020.Status == clsImplementationEnum.ICLSeamStatus.Approved.GetStringValue())
            {
                objSrcQMS020.RevNo = Convert.ToInt32(objSrcQMS020.RevNo) + 1;
                objSrcQMS020.Status = clsImplementationEnum.ICLSeamStatus.Draft.GetStringValue();
                objSrcQMS020.EditedBy = objClsLoginInfo.UserName;
                objSrcQMS020.EditedOn = DateTime.Now;
            }
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                QMS016_Log objSRCQMS016_Log = new QMS016_Log();
                var lstQMS21 = db.QMS021.Where(x => x.HeaderId == objSrcQMS020.HeaderId).ToList();

                var objQMS16 = db.QMS016_Log.Where(x => x.QualityId == objSrcQMS020.QualityId && x.Project == objSrcQMS020.Project && x.QualityProject == objSrcQMS020.QualityProject).Select(x => x.HeaderId).Distinct().FirstOrDefault();
                if (IsEdited)
                {
                    List<QMS021> lstDesQMS021 = db.QMS021.Where(x => x.HeaderId == objSrcQMS020.HeaderId).ToList();
                    if (lstDesQMS021 != null && lstDesQMS021.Count > 0)
                    {
                        db.QMS021.RemoveRange(lstDesQMS021);
                    }
                    db.SaveChanges();
                    QMS002 objQMS002 = new QMS002();
                    List<QMS016_Log> lstSrcQMS016_Log = db.QMS016_Log.Where(x => x.HeaderId == objQMS16 && x.StageStatus == ApproveString).ToList();
                    if (lstSrcQMS016_Log != null)
                    {
                        db.QMS021.AddRange(
                                       lstSrcQMS016_Log.Select(x =>
                                       new QMS021
                                       {
                                           HeaderId = objSrcQMS020.HeaderId,
                                           QualityProject = objSrcQMS020.QualityProject,
                                           Project = objSrcQMS020.Project,
                                           BU = objSrcQMS020.BU,
                                           Location = objSrcQMS020.Location,
                                           SeamNo = objSrcQMS020.SeamNo,
                                           RevNo = objSrcQMS020.RevNo,
                                           StageCode = x.StageCode,
                                           StageSequance = x.StageSequance,//db.QMS016.Where(i => i.StageCode == x.StageCode).FirstOrDefault().SequenceNo,
                                           StageStatus = clsImplementationEnum.TestPlanStatus.Added.GetStringValue(),
                                           AcceptanceStandard = x.AcceptanceStandard,
                                           ApplicableSpecification = x.ApplicableSpecification,
                                           InspectionExtent = x.InspectionExtent,
                                           Remarks = x.Remarks,
                                           CreatedBy = objClsLoginInfo.UserName,
                                           CreatedOn = DateTime.Now,
                                       })
                                     );
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.TestPlanMessages.LineUpdate.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                    }
                }
                else
                {
                    List<QMS016_Log> lstSrcQMS016_Log = db.QMS016_Log.Where(x => x.HeaderId == objQMS16 && x.StageStatus == ApproveString).ToList();
                    if (lstSrcQMS016_Log != null)
                    {
                        db.QMS021.AddRange(
                                        lstSrcQMS016_Log.Select(x =>
                                        new QMS021
                                        {
                                            HeaderId = objSrcQMS020.HeaderId,
                                            QualityProject = objSrcQMS020.QualityProject,
                                            Project = objSrcQMS020.Project,
                                            BU = objSrcQMS020.BU,
                                            Location = objSrcQMS020.Location,
                                            SeamNo = objSrcQMS020.SeamNo,
                                            RevNo = objSrcQMS020.RevNo,
                                            StageCode = x.StageCode,
                                            StageSequance = x.StageSequance,// db.QMS002.Where(i => i.StageCode == x.StageCode).FirstOrDefault().SequenceNo,
                                            StageStatus = clsImplementationEnum.TestPlanStatus.Added.GetStringValue(),
                                            AcceptanceStandard = x.AcceptanceStandard,
                                            ApplicableSpecification = x.ApplicableSpecification,
                                            InspectionExtent = x.InspectionExtent,
                                            Remarks = x.Remarks,
                                            CreatedBy = objClsLoginInfo.UserName,
                                            CreatedOn = DateTime.Now,
                                        })
                                      );
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.TestPlanMessages.HeaderInsert.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                    }
                }

                objResponseMsg.HeaderId = objSrcQMS020.HeaderId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return objResponseMsg;
        }

        //update line/stage data
        [HttpPost]
        public ActionResult UpdateLineData(int headerId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string tableName = string.Empty;
                //tableName = "QMS021";
                QMS030 objQMS030 = db.QMS031.Where(c => c.LineId == headerId).FirstOrDefault().QMS030;
                if (objQMS030.Status != clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue())
                {
                    if (!string.IsNullOrEmpty(columnName))
                    {
                        var dbResult = db.SP_IPI_SEAMICL_LINE_UPDATE(headerId, columnName, columnValue).FirstOrDefault();
                        if (dbResult.Result.ToString() == "IS")
                        {
                            //QMS030 objQMS030 = db.QMS031.Where(c => c.LineId == headerId).FirstOrDefault().QMS030;
                            if (string.Equals(objQMS030.Status, clsImplementationEnum.TestPlanStatus.Approved.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                            {
                                objQMS030.Status = clsImplementationEnum.TestPlanStatus.Draft.GetStringValue();
                                objQMS030.RevNo = objQMS030.RevNo + 1;
                                objQMS030.EditedBy = objClsLoginInfo.UserName;
                                objQMS030.EditedOn = DateTime.Now;
                                objQMS030.SubmittedBy = null;
                                objQMS030.SubmittedOn = null;
                                objQMS030.ApprovedBy = null;
                                objQMS030.ApprovedOn = null;
                                //if (objQMS030.QMS031.Any())
                                //{
                                //    objQMS030.QMS031.ToList().ForEach(i => i.RevNo = objQMS030.RevNo);
                                //}
                                db.SaveChanges();
                            }

                            objResponseMsg.HeaderStatus = objQMS030.Status;
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                        }
                    }
                }
                else
                {
                    objResponseMsg.Key = true; objResponseMsg.HeaderStatus = objQMS030.Status;
                    objResponseMsg.Remarks = clsImplementationMessage.CommonMessages.AlreadySubmitted;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //delete from specific header (quality id,ptc number,ptc applicable)
        [HttpPost]
        public ActionResult DeleteQualtiyId(int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (headerId > 0)
                {
                    QMS030 objQMS030 = new QMS030();
                    QMS031 objQMS031 = new QMS031();
                    objQMS030 = db.QMS030.Where(x => x.HeaderId == headerId).FirstOrDefault();
                    if (objQMS030.Status != clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue())
                    {
                        List<QMS031> lstStages = db.QMS031.Where(x => x.HeaderId == headerId).ToList();
                        if (lstStages != null && lstStages.Count > 0)
                        {
                            db.QMS031.RemoveRange(lstStages);
                        }
                        db.SaveChanges();
                        objQMS030.PTCApplicable = false;
                        objQMS030.PTCNumber = "";
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Remarks = clsImplementationMessage.CommonMessages.AlreadySubmitted;
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Remarks = clsImplementationMessage.CommonMessages.DuplicateQID.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SendForApproval(string strHeader)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();

            List<QMS030> lstQualityIds = new List<QMS030>();
            List<string> lstQIDWithoutLines = new List<string>();
            List<string> lstQIDNotDraft = new List<string>();
            List<string> lstQIDSubmitted = new List<string>();
            List<string> lstNotApplicableForParallel = new List<string>();
            List<string> lstRequireProtocol = new List<string>();
            List<string> lstStagesForProtocol = new List<string>();
            List<string> lstTPIStagesSkipped = new List<string>();
            List<string> lstTPIStagesforSeamSkipped = new List<string>();
            string deleted = clsImplementationEnum.TestPlanStatus.Deleted.GetStringValue();
            int s1 = 0;
            try
            {
                if (!string.IsNullOrEmpty(strHeader))
                {
                    int[] headerIds = Array.ConvertAll(strHeader.Split(','), s => int.Parse(s));
                    lstQualityIds = db.QMS030.Where(i => headerIds.Contains(i.HeaderId)).ToList();

                    foreach (var item in lstQualityIds)
                    {
                        var qms30 = db.QMS030.Where(i => i.HeaderId == item.HeaderId).FirstOrDefault();
                        if (qms30 != null)
                        {
                            if (string.Equals(qms30.Status, clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue()) || string.Equals(qms30.Status, clsImplementationEnum.QualityIdHeaderStatus.Returned.GetStringValue()))
                            {
                                var lstQMS031 = db.QMS031.Where(i => i.HeaderId == qms30.HeaderId && i.StageStatus != deleted).ToList();
                                if (lstQMS031.Count() > 0)
                                {
                                    if (Manager.IsApplicableForParallelStageForSeamICL(qms30.Project, qms30.QualityProject, qms30.BU, qms30.Location, qms30.SeamNo))
                                    {
                                        List<string> lstRequireStages = new List<string>();

                                        if (Manager.IsSeamNP(qms30.QualityProject, qms30.Project, qms30.Location, qms30.SeamNo) ? true : !Manager.IsProtocolRequiredForStage(qms30, out lstRequireStages))
                                        {
                                            bool isStageExists = true;

                                            UpdateTPIStagesInICL(item.HeaderId);
                                            var lstQMS025 = (from dbo in db.QMS025
                                                             where dbo.Location == qms30.Location && dbo.Project == qms30.Project && dbo.QualityProject == qms30.QualityProject && dbo.BU == qms30.BU && dbo.Project == qms30.Project
                                                                   && (dbo.FirstTPIAgency != null && dbo.FirstTPIAgency != "") && (dbo.FirstTPIIntervention != null && dbo.FirstTPIIntervention != "")
                                                             select dbo.Stage.Trim()).ToList();

                                            List<string> stageType = new List<string>() { "PT", "MT", "RT", "UT" };

                                            foreach (var lst in lstQMS031.Where(x => x.StageStatus != deleted).Select(x => x.StageCode.Trim()))
                                            {
                                                var sType = db.QMS002.Where(x => x.Location == qms30.Location && x.BU == qms30.BU && x.StageCode == lst).Select(x => x.StageType).FirstOrDefault();
                                                if (stageType.Contains(sType))
                                                {
                                                    if (!lstQMS025.Contains(lst))
                                                    {
                                                        lstTPIStagesSkipped.Add(lst);
                                                        isStageExists = false;
                                                        lstTPIStagesforSeamSkipped.Add(qms30.SeamNo);
                                                    }
                                                }
                                            }
                                            if (isStageExists)
                                            {
                                                qms30.Status = clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue();
                                                qms30.SubmittedBy = objClsLoginInfo.UserName;
                                                qms30.SubmittedOn = DateTime.Now;
                                                qms30.ReturnedBy = null;
                                                qms30.ReturnedOn = null;
                                                qms30.ReturnRemark = null;
                                                db.QMS031.Where(i => i.HeaderId == qms30.HeaderId).ToList().ForEach(i => { i.SubmittedBy = objClsLoginInfo.UserName; i.SubmittedOn = DateTime.Now; });
                                                db.SaveChanges();



                                                lstQIDSubmitted.Add(qms30.SeamNo);
                                            }
                                        }
                                        else
                                        {
                                            lstStagesForProtocol.AddRange(lstRequireStages);
                                            if (!qms30.SeamNo.Contains("AW") || !qms30.SeamNo.Contains("NP") || !qms30.SeamNo.Contains("TW"))
                                            {
                                                s1 = s1 + 1;
                                            }
                                            lstRequireProtocol.Add(qms30.SeamNo);
                                        }
                                    }
                                    else
                                    {
                                        lstNotApplicableForParallel.Add(qms30.SeamNo);
                                    }



                                }
                                else
                                {
                                    lstQIDWithoutLines.Add(qms30.SeamNo);
                                }
                            }
                            else
                            {
                                lstQIDNotDraft.Add(qms30.SeamNo);
                            }
                        }
                    }
                    objResponseMsg.Key = true;
                    if (lstQIDSubmitted.Any())
                    {
                        #region Send Notification
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.QC2.GetStringValue(), lstQualityIds.FirstOrDefault().Project, lstQualityIds.FirstOrDefault().BU, lstQualityIds.FirstOrDefault().Location, "ICL for Seam: Project " + lstQualityIds.FirstOrDefault().QualityProject + "  has been submitted for your Approval", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/ApproveICLSeam/Viewdetails?headerid=" + lstQualityIds.FirstOrDefault().HeaderId);
                        #endregion

                        objResponseMsg.Value = string.Format(clsImplementationMessage.ICLSeamMessages.SentForApproval, lstQIDSubmitted.Count);
                    }
                    if (lstQIDWithoutLines.Any())
                    {
                        objResponseMsg.WithoutLines = string.Format("No Stage(s) Exist(s) For SeamNo :{0}.Please Add Stages.", string.Join(",", lstQIDWithoutLines));
                    }
                    if (lstQIDNotDraft.Any())
                    {
                        objResponseMsg.NotDraft = string.Format("SeamNo(s) {0} have been skipped as SeamNo(s) {0} are either Sent For Approval Or Approved", string.Join(",", lstQIDNotDraft));
                    }
                    if (lstNotApplicableForParallel.Any())
                    {
                        objResponseMsg.NotApplicableForParallel = string.Format("Seam {0} have been skipped as PT/MT cannot be the parallel stages with RT/UT", string.Join(",", lstNotApplicableForParallel));
                    }
                    if (lstRequireProtocol.Any())
                    {
                        if (s1 > 0)
                        {
                            objResponseMsg.RequiredProtocol = string.Format("Seam {0} have been skipped as protocol not maintained/attached/linked for stage(s) : {1}", string.Join(",", lstRequireProtocol), string.Join(", ", lstStagesForProtocol.Distinct()));
                        }
                        


                    }
                    if (lstTPIStagesSkipped.Any())
                    {
                        objResponseMsg.TPISkipped = string.Format("Seam {0} have been skipped as TPI Agency &  Intervention not maintained for stage(s) : {1}", string.Join(",", lstTPIStagesforSeamSkipped.Distinct()), string.Join(", ", lstTPIStagesSkipped.Distinct()));
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please select record";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public clsHelper.ResponseMsg UpdateTPIStagesInICL(int strHeaderId)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                string deleted = clsImplementationEnum.TestPlanStatus.Deleted.GetStringValue();
                QMS031 objQMS031 = new QMS031();
                var lstQMS031 = (from dbo in db.QMS031
                                 where dbo.HeaderId == strHeaderId && dbo.StageStatus != deleted
                                 select dbo.StageCode.Trim()).Distinct().ToArray();
                for (int j = 0; j < lstQMS031.Length; j++)
                {
                    string strStage = lstQMS031[j];
                    var obj031 = db.QMS031.Where(x => x.StageCode == strStage && x.HeaderId == strHeaderId).ToList();
                    foreach (var newobjQMS031 in obj031)
                    {
                        QMS025 objQMS025 = db.QMS025.Where(a => a.Location == newobjQMS031.Location && a.BU == newobjQMS031.BU && a.Project == newobjQMS031.Project && a.QualityProject == newobjQMS031.QualityProject && strStage.Contains(a.Stage) && (a.FirstTPIAgency != null && a.FirstTPIAgency != "") && (a.FirstTPIIntervention != null && a.FirstTPIIntervention != "")).FirstOrDefault();

                        if (objQMS025 != null)
                        {
                            newobjQMS031.FirstTPIAgency = objQMS025.FirstTPIAgency;
                            newobjQMS031.FirstTPIIntervention = objQMS025.FirstTPIIntervention;
                            newobjQMS031.SecondTPIAgency = objQMS025.SecondTPIAgency;
                            newobjQMS031.SecondTPIIntervention = objQMS025.SecondTPIIntervention;
                            newobjQMS031.ThirdTPIAgency = objQMS025.ThirdTPIAgency;
                            newobjQMS031.ThirdTPIIntervention = objQMS025.ThirdTPIIntervention;
                            newobjQMS031.ForthTPIAgency = objQMS025.ForthTPIAgency;
                            newobjQMS031.ForthTPIIntervention = objQMS025.ForthTPIIntervention;
                            newobjQMS031.FifthTPIAgency = objQMS025.FifthTPIAgency;
                            newobjQMS031.FifthTPIIntervention = objQMS025.FifthTPIIntervention;
                            newobjQMS031.SixthTPIAgency = objQMS025.SixthTPIAgency;
                            newobjQMS031.SixthTPIIntervention = objQMS025.SixthTPIIntervention;
                            newobjQMS031.EditedBy = objClsLoginInfo.UserName;
                            newobjQMS031.EditedOn = DateTime.Now;
                            db.SaveChanges();

                            /*  #region Update Inspection Agency In Protocol
                              if (newobjQMS031.ProtocolId.HasValue)
                              {
                                  List<string> lstInspectionAgency = new List<string>();
                                  if (!string.IsNullOrWhiteSpace(Convert.ToString(newobjQMS031.FirstTPIAgency)))
                                      lstInspectionAgency.Add(Convert.ToString(newobjQMS031.FirstTPIAgency));
                                  if (!string.IsNullOrWhiteSpace(Convert.ToString(newobjQMS031.SecondTPIAgency)))
                                      lstInspectionAgency.Add(Convert.ToString(newobjQMS031.SecondTPIAgency));

                                  string strInspectionAgency = string.Join(", ", lstInspectionAgency);

                                  string PRLTableName = Manager.GetLinkedProtocolTableName(newobjQMS031.ProtocolType);
                                  db.Database.ExecuteSqlCommand("UPDATE " + PRLTableName + " SET InspectionAgency='" + strInspectionAgency + "' WHERE HeaderId = " + newobjQMS031.ProtocolId.Value);
                              }
                              #endregion
                              */

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return objResponseMsg;
        }
        #endregion

        #region old code
        [HttpPost]
        public JsonResult LoadTestPlanLineGridData_Old(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "";
                strWhere += Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();
                //string[] arrayCTQStatus = { clsImplementationEnum.CTQStatus.DRAFT.GetStringValue(), clsImplementationEnum.CTQStatus.Returned.GetStringValue() };

                string[] columnName = { "StageCode", "StageSequance", "AcceptanceStandard", "ApplicableSpecification", "InspectionExtent", "RevNo", "StageStatus", "Remarks", "LineId", };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);

                var lstResult = db.SP_IPI_TEST_PLAN_LINEDETAILS
                                (
                               StartIndex,
                               EndIndex,
                               "",
                               strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                //Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.StageCode),
                                Convert.ToString(uc.StageSequance),
                                Convert.ToString(uc.AcceptanceStandard),
                                Convert.ToString(uc.ApplicableSpecification),
                                Convert.ToString(uc.InspectionExtent),
                                Convert.ToString(uc.RevNo),
                                Convert.ToString(uc.StageStatus),
                                Convert.ToString(uc.Remarks),
                                Convert.ToString(uc.LineId),
                           }).ToList();


                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        //save test plan Stages (before editable grid)
        [HttpPost]
        public ActionResult SaveTestPlanLines(FormCollection fc, QMS021 qms021)
        {
            QMS021 objQMS021 = new QMS021();
            QMS020 objQMS020 = new QMS020();

            string addedStatus = clsImplementationEnum.TestPlanStatus.Added.GetStringValue();
            string modifiedStatus = clsImplementationEnum.TestPlanStatus.Modified.GetStringValue();
            string deletedStatus = clsImplementationEnum.TestPlanStatus.Deleted.GetStringValue();

            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                int headerID = qms021.HeaderId;
                bool IsEdited = false;
                objQMS020 = db.QMS020.Where(x => x.HeaderId == headerID).FirstOrDefault();
                if (qms021.LineId > 0)
                {
                    objQMS021 = db.QMS021.Where(x => x.LineId == qms021.LineId).FirstOrDefault();
                    IsEdited = true;
                }
                objQMS021.QualityProject = objQMS020.QualityProject;
                objQMS021.Project = objQMS020.Project;
                objQMS021.BU = objQMS020.BU;
                objQMS021.Location = objQMS020.Location;
                objQMS021.SeamNo = objQMS020.SeamNo;
                objQMS021.StageCode = qms021.StageCode;
                objQMS021.StageSequance = qms021.StageSequance;
                objQMS021.AcceptanceStandard = qms021.AcceptanceStandard;
                objQMS021.ApplicableSpecification = qms021.ApplicableSpecification;
                objQMS021.InspectionExtent = qms021.InspectionExtent;
                objQMS021.Remarks = qms021.Remarks;
                objQMS021.HeaderId = objQMS020.HeaderId;
                if (objQMS020.Status == clsImplementationEnum.ICLSeamStatus.Approved.GetStringValue())
                {
                    objQMS020.RevNo = Convert.ToInt32(objQMS020.RevNo) + 1;
                    objQMS020.Status = clsImplementationEnum.ICLSeamStatus.Draft.GetStringValue();
                    objQMS020.EditedBy = objClsLoginInfo.UserName;
                    objQMS020.EditedOn = DateTime.Now;
                }
                if (IsEdited)
                {
                    objQMS021.RevNo = objQMS020.RevNo;
                    objQMS021.StageStatus = modifiedStatus;
                    objQMS021.EditedBy = objClsLoginInfo.UserName;
                    objQMS021.EditedOn = DateTime.Now;
                    objQMS020.EditedBy = objClsLoginInfo.UserName;
                    objQMS020.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.TestPlanMessages.LineUpdate;
                }
                else
                {
                    objQMS021.RevNo = objQMS020.RevNo;
                    objQMS021.StageStatus = addedStatus;
                    objQMS021.CreatedBy = objClsLoginInfo.UserName;
                    objQMS021.CreatedOn = DateTime.Now;
                    db.QMS021.Add(objQMS021);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.TestPlanMessages.LineInsert;
                }
                objResponseMsg.Status = objQMS020.Status;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.TestPlanMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //public ActionResult LoadTestPlanLineData_old(int headerId, int lineId)
        //{
        //    QMS020 objQMS020 = new QMS020();
        //    QMS021 objQMS021 = new QMS021();
        //    NDEModels objNDEModels = new NDEModels();
        //    objQMS020 = db.QMS020.Where(x => x.HeaderId == headerId).FirstOrDefault();
        //    ViewBag.Stages = db.QMS002.Where(i => i.BU.Equals(objQMS020.BU, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(objQMS020.Location, StringComparison.OrdinalIgnoreCase)).Select(i => new CategoryData { Value = i.StageCode, Code = i.StageCode, CategoryDescription = i.StageCode + "-" + i.StageDesc }).ToList();
        //    var lstAcceptanceStandard = Manager.GetSubCatagories("Acceptance Standard", objQMS020.BU, objQMS020.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
        //    var lstApplicableSpecification = Manager.GetSubCatagories("Applicable Specification", objQMS020.BU, objQMS020.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
        //    var lstInspectionExtent = Manager.GetSubCatagories("Inspection Extent", objQMS020.BU, objQMS020.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
        //    ViewBag.AcceptanceStandard = lstAcceptanceStandard;
        //    ViewBag.ApplicableSpecification = lstApplicableSpecification;
        //    ViewBag.InspectionExtent = lstInspectionExtent;

        //    if (lineId > 0)
        //    {
        //        objQMS021 = db.QMS021.Where(x => x.LineId == lineId).FirstOrDefault();
        //        ViewBag.StageSelected = db.QMS002.Where(i => i.StageCode.Equals(objQMS021.StageCode, StringComparison.OrdinalIgnoreCase)).Select(i => i.StageCode + "-" + i.StageDesc).FirstOrDefault();
        //        ViewBag.AcceptanceStandardSelected = objNDEModels.GetCategory(objQMS021.AcceptanceStandard).CategoryDescription;
        //        ViewBag.ApplicableSpecificationSelected = objNDEModels.GetCategory(objQMS021.ApplicableSpecification).CategoryDescription;
        //        ViewBag.InspectionExtentSelected = objNDEModels.GetCategory(objQMS021.InspectionExtent).CategoryDescription;
        //        ViewBag.Action = "line edit";
        //    }


        //    ViewBag.QProject = objQMS020.QualityProject;
        //    ViewBag.BU = db.COM002.Where(a => a.t_dimx == objQMS020.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
        //    ViewBag.Project = db.COM001.Where(i => i.t_cprj == objQMS020.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
        //    ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objQMS020.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
        //    ViewBag.SeamNo = objQMS020.SeamNo;
        //    ViewBag.RevNo = objQMS020.RevNo;
        //    return PartialView("_LoadTPLinesDetailsForm", objQMS021);
        //}

        [HttpPost]
        public ActionResult UpdateTPHeader(int headerId, string qualityProject, string strQID, string changeText, string strFrom)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS020 objQMS020 = new QMS020();
                objQMS020 = db.QMS020.Where(x => x.HeaderId == headerId).FirstOrDefault();
                if (objQMS020 != null)
                {
                    if (strFrom.ToUpper() == "A")
                    {
                        if (objQMS020.QualityId != changeText)
                        {
                            strQID = changeText;
                            objQMS020.QualityId = changeText;
                            db.SaveChanges();
                            objResponseMsg = SaveTPHeader(objQMS020, true);
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                        }
                    }
                    if (strFrom.ToUpper() == "B")
                    {
                        objQMS020.PTCApplicable = Convert.ToBoolean(changeText);
                        db.SaveChanges();
                    }
                    if (strFrom.ToUpper() == "C")
                    {
                        objQMS020.PTCNumber = changeText;
                        db.SaveChanges();
                    }
                    if (strFrom.ToUpper() == "B" || strFrom.ToUpper() == "C")
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.TestPlanMessages.HeaderUpdate;
                    }
                    if (strFrom.ToUpper() == "A")
                    {
                        if (objResponseMsg.Key == false)
                        {
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.DuplicateQID.ToString();
                        }
                        else
                        {
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public string GetPTCApplicable(bool? ptcApplicable, string status)
        {
            string[] ptcYesNo = clsImplementationEnum.getyesno().ToArray();
            string htmlOption = "<option value=''>Select PTC Applicable</option>";
            string strSelectedValue;
            if (ptcApplicable == true)
            {
                strSelectedValue = "Yes";
            }
            else
            {
                strSelectedValue = "No";
            }
            for (int i = 0; i < ptcYesNo.Length; i++)
            {
                if (ptcApplicable != null && strSelectedValue == "Yes")
                {
                    if (ptcYesNo[i] == "Yes")
                    {
                        htmlOption += "<option selected value='" + ptcYesNo[i] + "'>" + ptcYesNo[i] + "</option>";
                    }
                    else
                    {
                        htmlOption += "<option value='" + ptcYesNo[i] + "'>" + ptcYesNo[i] + "</option>";
                    }
                }
                else
                {
                    if (ptcYesNo[i] == "No")
                    {
                        htmlOption += "<option selected value='" + ptcYesNo[i] + "'>" + ptcYesNo[i] + "</option>";
                    }
                    else
                    {
                        htmlOption += "<option value='" + ptcYesNo[i] + "'>" + ptcYesNo[i] + "</option>";
                    }
                }
            }
            if (status != clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue())
            {
                return "<select name='ddlptcApplicable' class='form-control input-sm input-small input-inline'>" + htmlOption + "</select>";
            }
            else
            {
                return "<select name='ddlptcApplicable' disabled class='form-control input-sm input-small input-inline'>" + htmlOption + "</select>";
            }
        }

        public string GetPTCNumber_old(string ptcNumber, string status)
        {
            if (ptcNumber != null)
            {
                return "<input type='text' readonly name='txtptcNumber' class='form-control input-sm input-small input-inline' value='" + ptcNumber + "' />";
            }
            else
            {
                return "<input type='text' readonly name='txtptcNumber' class='form-control input-sm input-small input-inline' value='" + ptcNumber + "' />";
            }
        }
        [HttpPost]
        public ActionResult DeleteQID(string strHeader)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            QMS020 objQMS020 = new QMS020();
            List<QMS020> lstQMS020 = new List<QMS020>();
            List<QMS021> lstQMS021 = new List<QMS021>();

            int inQIDDeleted = 0, inQIDWithLines = 0;

            try
            {
                if (!string.IsNullOrEmpty(strHeader))
                {
                    int[] headerIds = Array.ConvertAll(strHeader.Split(','), i => Convert.ToInt32(i));
                    foreach (var headerId in headerIds)
                    {
                        if (headerId > 0)
                        {
                            objQMS020 = db.QMS020.Where(i => i.HeaderId == headerId).FirstOrDefault();
                            #region Revision exists
                            if (objQMS020.RevNo > 0)
                            {
                                var lstLines = db.QMS021.Where(i => i.HeaderId == headerId).ToList();
                                if (lstLines.Any())
                                    db.QMS021.RemoveRange(lstLines);

                                //var lstIdenticalProject = db.QMS017.Where(i => i.HeaderId == headerId).ToList();
                                //if (lstIdenticalProject.Any())
                                //    db.QMS017.RemoveRange(lstIdenticalProject);
                                //db.QMS020.Remove(objQMS020);

                                db.SaveChanges();

                                #region Restoring Previous Version

                                var histHeader = db.QMS020_log.OrderByDescending(x => x.RevNo).FirstOrDefault();
                                var histLines = db.QMS021_log.Where(i => i.RefId == histHeader.Id && i.HeaderId == headerId).ToList();

                                objQMS020.HeaderId = histHeader.HeaderId;
                                objQMS020.QualityProject = histHeader.QualityProject;
                                objQMS020.Project = histHeader.Project;
                                objQMS020.BU = histHeader.BU;
                                objQMS020.Location = histHeader.Location;
                                objQMS020.QualityId = histHeader.QualityId;
                                objQMS020.QualityIdRev = histHeader.QualityIdRev;
                                objQMS020.PTCNumber = histHeader.PTCNumber;
                                objQMS020.PTCApplicable = histHeader.PTCApplicable;
                                objQMS020.RevNo = histHeader.RevNo + 1;
                                objQMS020.Status = clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue();
                                objQMS020.CreatedBy = objClsLoginInfo.UserName;
                                objQMS020.CreatedOn = DateTime.Now;

                                db.SaveChanges();

                                db.QMS021.AddRange(db.QMS021_log.Where(i => i.RefId == histHeader.Id && headerId == histHeader.HeaderId).Select(i => new QMS021
                                {
                                    LineId = i.LineId,
                                    HeaderId = histHeader.HeaderId,
                                    QualityProject = histHeader.QualityProject,
                                    Project = histHeader.Project,
                                    BU = histHeader.BU,
                                    Location = histHeader.Location,
                                    RevNo = objQMS020.RevNo,
                                    StageCode = i.StageCode,
                                    StageSequance = i.StageSequance,
                                    StageStatus = clsImplementationEnum.QualityIdStageStatus.ADDED.GetStringValue(),
                                    AcceptanceStandard = i.AcceptanceStandard,
                                    ApplicableSpecification = i.ApplicableSpecification,
                                    InspectionExtent = i.InspectionExtent,
                                    CreatedBy = objClsLoginInfo.UserName,
                                    CreatedOn = DateTime.Now
                                }).ToList());

                                //var histIdenticalProj = db.QMS017_Log.Where(i => i.RefId == histHeader.Id && headerId == histHeader.HeaderId).ToList();
                                //if (histIdenticalProj.Any())
                                //{
                                //    db.QMS017.AddRange(histIdenticalProj.Select(i => new QMS017
                                //    {
                                //        LineId = i.LineId,
                                //        HeaderId = objQMS020.HeaderId,
                                //        QualityProject = i.QualityProject,
                                //        Project = i.Project,
                                //        BU = i.BU,
                                //        Location = i.Location,
                                //        //QualityId = i.QualityId,
                                //        RevNo = objQMS020.RevNo,
                                //        IQualityProject = i.IQualityProject,
                                //        CreatedBy = objClsLoginInfo.UserName,
                                //        CreatedOn = DateTime.Now
                                //    }));
                                //}
                                #endregion

                                db.SaveChanges();
                                inQIDDeleted++;

                            }
                            #endregion
                            else
                            {
                                var lstLines = db.QMS021.Where(i => i.HeaderId == headerId).ToList();
                                if (lstLines.Any())
                                {
                                    List<QMS021> lstStages = db.QMS021.Where(x => x.HeaderId == headerId).ToList();
                                    if (lstStages != null)
                                    {
                                        db.QMS021.RemoveRange(lstLines);

                                        db.QMS020.Remove(objQMS020);
                                        db.SaveChanges();
                                        inQIDDeleted++;
                                    }
                                    else
                                    {
                                        inQIDWithLines++;
                                    }
                                }
                                else
                                {

                                    db.QMS020.Remove(objQMS020);
                                    db.SaveChanges();
                                    inQIDDeleted++;
                                }

                                //if (hasLines)
                                //{
                                //    var lstLines = db.QMS021.Where(i => i.HeaderId == headerId).ToList();
                                //    if (lstLines.Any())
                                //        db.QMS021.RemoveRange(lstLines);
                                //    //lstQMS021.AddRange(lstLines);
                                //    db.QMS020.Remove(objQMS020);
                                //    db.SaveChanges();
                                //    inQIDDeleted++;
                                //}
                                //else
                                //{
                                //    var lstLines = db.QMS021.Where(i => i.HeaderId == headerId).ToList();
                                //    if (lstLines.Any())
                                //    { }
                                //    else
                                //    { }
                                //    db.QMS020.Remove(objQMS020);
                                //    db.SaveChanges();
                                //    //inQIDDeleted++;
                                //}
                                //db.QMS020.Remove(objQMS020);
                                //db.SaveChanges();
                            }
                        }
                    }

                    if (inQIDWithLines > 0)
                    {
                        objResponseMsg.Status = string.Format("{0} QID(s) is/are having lines.", inQIDWithLines);
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = string.Format(clsImplementationMessage.QualityIDMessage.Delete, inQIDDeleted);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }


            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult LoadTPHeaderData_old(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += " 1=1 and status in('" + clsImplementationEnum.TestPlanStatus.Draft.GetStringValue() + "')";
                }
                else
                {
                    strWhere += " 1=1";
                }
                string[] columnName = { "ST2.QualityProject", "ST2.Location", "ST2.QualityId", "ST2.PTCApplicable", "ST2.PTCNumber", "ST2.Status" };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.Location, "ST2.BU", "ST2.Location");
                var lstResult = db.SP_TPlan_Get_HeaderList
                                (
                                StartIndex, EndIndex, "", strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.QualityProject),
                            Convert.ToString(uc.Location),
                            Convert.ToString(uc.SeamNo),
                            GetQID(uc.QualityIds,uc.Status,uc.QualityId),
                            GetPTCApplicable(uc.PTCApplicable,uc.Status),
                           // GetPTCNumber(uc.PTCNumber,uc.Status),
                            Convert.ToString(uc.Status),
                            Convert.ToString(uc.HeaderId),
                            Convert.ToString(uc.QualityId),
                            Convert.ToString(uc.PTCApplicable),

                            "<center><a class='btn btn-xs green' href='" + WebsiteURL + "/IPI/TestPlan/AddHeader?HeaderID="+uc.HeaderId+"'>View<i style='' class='iconspace fa fa-eye'></i></a></center>",
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public string GetQID(string strQID, string status, string selectedQID)
        {
            string[] qualityID = null;
            string htmlOption = "<option value=''> Select Quality Id </option>";
            string htmlValue = null;
            if (!string.IsNullOrWhiteSpace(strQID))
            {
                qualityID = strQID.Split(',');
                for (int i = 0; i < qualityID.Length; i++)
                {
                    htmlValue = qualityID[i].Split('|')[0];
                    if (selectedQID != null)
                    {
                        if (htmlValue.Trim() == selectedQID.Trim())
                        {
                            htmlOption += "<option selected value=" + htmlValue + ">" + qualityID[i] + "</option>";
                        }
                        else
                        {
                            htmlOption += "<option value='" + htmlValue + "'>" + qualityID[i] + "</option>";
                        }
                    }
                    else
                    {
                        htmlOption += "<option value='" + htmlValue + "'>" + qualityID[i] + "</option>";
                    }
                }
                if (status != clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue())
                {
                    return "<select name='txtQID'  class='form-control input-sm input-small input-inline'>" + htmlOption + "</select>";
                }
                else
                {
                    return "<select name='txtQID' disabled class='form-control input-sm input-small input-inline'>" + htmlOption + "</select>";
                }
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(selectedQID))
                {
                    htmlValue = selectedQID.Split('|')[0];
                    htmlOption += "<option selected value='" + htmlValue + "'>" + selectedQID + "</option>";
                }
                if (status != clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue())
                {
                    return "<select name='txtQID' class='form-control input-sm input-small input-inline'>" + htmlOption + "</select>";
                }
                else { return "<select name='txtQID' disabled class='form-control input-sm input-small input-inline'>" + htmlOption + "</select>"; }
            }
        }
        [HttpPost]
        public ActionResult SendForApproval_old(int strHeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS020 objQMS020 = db.QMS020.Where(u => u.HeaderId == strHeaderId).SingleOrDefault();
                if (objQMS020 != null)
                {
                    objQMS020.Status = clsImplementationEnum.CommonStatus.SendForApprovel.GetStringValue();
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.sentforApprove.ToString();
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region autocomplete,datatable html functions,common functions

        public string GenerateAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string maxlength = "")
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onchange='" + onBlurMethod + "'" : "";

            strAutoComplete = "<input type='text' dataid='" + rowId + "' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' hdElement='" + hdElementId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + "  " + MaxCharcters + "  " + (disabled ? "disabled" : "") + " />";

            return strAutoComplete;
        }

        public string GenerateAutoCompleteonBlur(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string maxlength = "")
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' hdElement='" + hdElementId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + "  " + MaxCharcters + "  " + (disabled ? "disabled" : "") + " />";

            return strAutoComplete;
        }

        [HttpPost]
        public bool checkStageExist(string stagecode, int header)
        {
            bool Flag = false;
            if (!string.IsNullOrWhiteSpace(stagecode))
            {
                QMS021 objCTQ001 = db.QMS021.Where(x => x.StageCode == stagecode && x.HeaderId == header).FirstOrDefault();
                if (objCTQ001 != null)
                {
                    Flag = true;
                }
            }
            return Flag;
        }

        [HttpPost]
        public bool GetSquence(string stagecode, int seq, int headerId)
        {
            bool Flag = false;
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            QMS002 objQMS002 = new QMS002();
            string strStage = stagecode;
            if (!string.IsNullOrWhiteSpace(stagecode))
            {
                QMS021 QMS021 = db.QMS021.Where(i => i.StageCode == stagecode && i.StageSequance == seq && i.HeaderId == headerId).FirstOrDefault();
                if (QMS021 != null)
                {
                    Flag = true;
                }
            }
            return Flag;
        }

        [HttpPost]
        public ActionResult GetStageSquence(string stagecode, int headerId)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            objResponseMsg.Key = false;
            string strStage = stagecode.Trim();
            if (!string.IsNullOrWhiteSpace(stagecode))
            {
                QMS030 objQMS30 = db.QMS030.Where(i => i.HeaderId == headerId).FirstOrDefault();
                QMS002 objQMS002 = db.QMS002.Where(i => i.StageCode == stagecode && i.Location == objQMS30.Location && i.BU == objQMS30.BU).FirstOrDefault();

                if (objQMS002 != null)
                {
                    objResponseMsg.Sequence = Convert.ToInt32(objQMS002.SequenceNo);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = objQMS002.IsProtocolApplicable.ToString();
                }
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CheckDuplicateStages(string stagecode, int headerId, int stageseq)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();

            if (CheckDuplicateStage(headerId, stagecode, stageseq))
            {
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.TestPlanMessages.DuplicateStage;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult CheckSequenceNo(string stagecode, int headerId, int stageseq)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            QMS030 objQMS030 = db.QMS030.Where(x => x.HeaderId == headerId).FirstOrDefault();
            int maxOfferedSequenceNo = (new clsManager()).GetMaxOfferedSequenceNo(objQMS030.QualityProject, objQMS030.BU, objQMS030.Location, objQMS030.SeamNo);
            if (stageseq <= maxOfferedSequenceNo)
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Sequence no should be greater then " + maxOfferedSequenceNo;
            }
            else
                objResponseMsg.Key = true;
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        //    objResponseMsg.Sequence = Convert.ToInt32(Sequence);
        //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        public ActionResult ReviseHeader(int headerid)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS030 objQMS030 = db.QMS030.Where(x => x.HeaderId == headerid).FirstOrDefault();
                if (objQMS030 != null)
                {
                    if (objQMS030.Status == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue())
                    {
                        objQMS030.RevNo = Convert.ToInt32(objQMS030.RevNo) + 1;
                        objQMS030.Status = clsImplementationEnum.UTCTQStatus.DRAFT.GetStringValue();
                        objQMS030.EditedBy = objClsLoginInfo.UserName;
                        objQMS030.EditedOn = DateTime.Now;
                        objQMS030.SubmittedBy = null;
                        objQMS030.SubmittedOn = null;
                        objQMS030.ApprovedBy = null;
                        objQMS030.ApprovedOn = null;
                        db.SaveChanges();
                    }
                    QMS031 objQMS031 = db.QMS031.Where(x => x.HeaderId == headerid).FirstOrDefault();
                    if (objQMS031 != null)
                        if (objQMS031.StageStatus == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue())
                        {
                            //objQMS021.RevNo = Convert.ToInt32(objQMS020.RevNo) + 1;
                            objQMS031.StageStatus = clsImplementationEnum.TestPlanStatus.Added.GetStringValue();
                            objQMS031.EditedBy = objClsLoginInfo.UserName;
                            objQMS031.EditedOn = DateTime.Now;
                            db.SaveChanges();
                        }
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult ReviseICLHeader(int headerid, int? LineId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                QMS030 objQMS030 = db.QMS030.Where(x => x.HeaderId == headerid).FirstOrDefault();
                if (objQMS030 != null)
                {
                    if (objQMS030.Status == clsImplementationEnum.ICLSeamStatus.Approved.GetStringValue())
                    {
                        objQMS030.RevNo = Convert.ToInt32(objQMS030.RevNo) + 1;
                        objQMS030.Status = clsImplementationEnum.ICLSeamStatus.Draft.GetStringValue();
                        objQMS030.EditedBy = objClsLoginInfo.UserName;
                        objQMS030.EditedOn = DateTime.Now;
                        objQMS030.SubmittedBy = null;
                        objQMS030.SubmittedOn = null;
                        objQMS030.ApprovedBy = null;
                        objQMS030.ApprovedOn = null;
                    }

                    if (LineId != null)
                    {
                        QMS031 objQMS031 = objQMS030.QMS031.Where(c => c.LineId == LineId).FirstOrDefault();
                        if (objQMS031 != null)
                        {
                            objQMS031.StageStatus = clsImplementationEnum.ICLSeamStatus.Modified.GetStringValue();
                        }
                    }

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderStatus = objQMS030.Status;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();

                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult GetSeamResultforCopy(string term)
        {
            var lstSeam2 = (from a in db.QMS020
                            where a.SeamNo.Contains(term)
                            orderby a.HeaderId
                            select new { seamNo = a.SeamNo, seamDesc = a.SeamNo }).Distinct().ToList();
            return Json(lstSeam2, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSeamResultforTransfer(string term, string qualityProject)
        {
            var lstSeam2 = (from a in db.QMS020
                            where a.SeamNo.Contains(term) && a.QualityProject.Contains(qualityProject)
                            orderby a.HeaderId
                            select new { seamNo = a.SeamNo, seamDesc = a.SeamNo }).Distinct().ToList();
            return Json(lstSeam2, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetQualityProjectResult(string term)
        {
            var lstqualityProject = (from a in db.QMS020
                                     where a.QualityProject.Contains(term) && a.Location.Contains(objClsLoginInfo.Location)
                                     orderby a.HeaderId
                                     select new { code = a.QualityProject, desc = a.QualityProject }).Distinct().ToList();
            return Json(lstqualityProject, JsonRequestBehavior.AllowGet);
        }

        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", bool isDisable = false)
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (!string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
            {
                if (isDisable)
                {
                    htmlControl = "<i id='" + inputID + "' name='" + inputID + "'  Title='" + buttonTooltip + "' class='disabledicon " + className + "' ></i>";
                }
                else
                {
                    htmlControl = "<i id='" + inputID + "' name='" + inputID + "' Title='" + buttonTooltip + "' class='iconspace " + className + "'" + onClickEvent + " ></i>";
                }
            }

            return htmlControl;
        }

        public string HTMLActionStringWithoutStatus(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", bool isDisable = false)
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (isDisable)
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "'  Title='" + buttonTooltip + "' class='disabledicon " + className + "' ></i>";
            }
            else
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "'  Title='" + buttonTooltip + "' class='iconspace " + className + "'" + onClickEvent + " ></i>";
            }
            return htmlControl;
        }

        public string GetPTCNumber(string ptcNumber, string status, int headerid, bool ptcapplicable)
        {
            if (ptcNumber != null)
            {
                if (ptcapplicable)
                {
                    return Helper.GenerateTextbox(headerid, "PTCNumber", ptcNumber, "UpdateData(this, " + headerid + ");", string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()) ? true : false);
                }
                else
                {
                    return Helper.GenerateTextbox(headerid, "PTCNumber", ptcNumber, "UpdateData(this, " + headerid + ");", true);
                }
            }
            else
            {
                return "<input type='text' readonly name='txtptcNumber' class='form-control input-sm input-small input-inline' value='" + ptcNumber + "' />";
            }
        }
        public bool isStageLinkedInLTFPS(int LineID)
        {
            var objQMS031 = db.QMS031.Where(x => x.LineId == LineID).FirstOrDefault();
            string deleted = clsImplementationEnum.LTFPSLineStatus.Deleted.GetStringValue();
            if (objQMS031 != null)
            {
                if (db.LTF002.Any(x => x.QualityProject.Equals(objQMS031.QualityProject) && x.Location.Equals(objQMS031.Location) && x.BU.Equals(objQMS031.BU) && x.SeamNo.Equals(objQMS031.SeamNo) && x.Stage.Equals(objQMS031.StageCode) && x.StageSequence == objQMS031.StageSequance && x.LineStatus != deleted))
                    return true;
            }
            return false;
        }

        public bool isProtocolApplicable(string StageCode, string Location, string BU)
        {
            try
            {
                StageCode = StageCode.Split('-')[0];
                return db.QMS002.FirstOrDefault(f => f.StageCode == StageCode && f.Location == Location && f.BU == BU).IsProtocolApplicable;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            if (LineId > 0)
            {
                QMS031 objQMS031 = db.QMS031.Where(x => x.LineId == LineId).FirstOrDefault();
                model.ApprovedBy = objQMS031.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objQMS031.ApprovedBy) : null;
                model.ApprovedOn = objQMS031.ApprovedOn;
                model.SubmittedBy = objQMS031.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objQMS031.SubmittedBy) : null;
                model.SubmittedOn = objQMS031.SubmittedOn;
                model.CreatedBy = objQMS031.CreatedBy != null ? Manager.GetUserNameFromPsNo(objQMS031.CreatedBy) : null;
                model.CreatedOn = objQMS031.CreatedOn;
                model.EditedBy = objQMS031.EditedBy != null ? Manager.GetUserNameFromPsNo(objQMS031.EditedBy) : null;
                model.EditedOn = objQMS031.EditedOn;
            }
            else
            {
                QMS030 objQMS030 = db.QMS030.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.ApprovedBy = objQMS030.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objQMS030.ApprovedBy) : null;
                model.ApprovedOn = objQMS030.ApprovedOn;
                model.SubmittedBy = objQMS030.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objQMS030.SubmittedBy) : null;
                model.SubmittedOn = objQMS030.SubmittedOn;
                model.CreatedBy = objQMS030.CreatedBy != null ? Manager.GetUserNameFromPsNo(objQMS030.CreatedBy) : null;
                model.CreatedOn = objQMS030.CreatedOn;
                model.EditedBy = objQMS030.EditedBy != null ? Manager.GetUserNameFromPsNo(objQMS030.EditedBy) : null;
                model.EditedOn = objQMS030.EditedOn;
                model.ReturnedBy = objQMS030.ReturnedBy != null ? Manager.GetUserNameFromPsNo(objQMS030.ReturnedBy) : null;
                model.ReturnedOn = objQMS030.ReturnedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress2.cshtml", model);
        }
        public ActionResult ShowLogTimeline(int id, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            if (LineId > 0)
            {

            }
            else
            {
                QMS030_Log objQMS030log = db.QMS030_Log.Where(x => x.Id == id).FirstOrDefault();
                model.ApprovedBy = objQMS030log.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objQMS030log.ApprovedBy) : null;
                model.ApprovedOn = objQMS030log.ApprovedOn;
                model.SubmittedBy = objQMS030log.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objQMS030log.SubmittedBy) : null;
                model.SubmittedOn = objQMS030log.SubmittedOn;
                model.CreatedBy = objQMS030log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objQMS030log.CreatedBy) : null;
                model.CreatedOn = objQMS030log.CreatedOn;
                model.EditedBy = objQMS030log.EditedBy != null ? Manager.GetUserNameFromPsNo(objQMS030log.EditedBy) : null;
                model.EditedOn = objQMS030log.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress2.cshtml", model);
        }
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.MAINTAININDEX.GetStringValue())
                {
                    var lst = db.SP_IPI_GET_SEAM_ICL_INDEXRESULT(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_IPI_SEAM__ICL_HEADER_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      SeamNo = li.SeamNo,
                                      RevNo = li.RevNo,
                                      TPRevNo = li.TPRevNo,
                                      PTCApplicable = li.PTCApplicable,
                                      PTCNumber = li.PTCNumber,
                                      Status = li.Status,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_IPI_SEAM_ICL_LINE_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      SeamNo = li.SeamNo,
                                      StageCode = li.StageCode,
                                      StageSequance = li.StageSequance,
                                      StageStatus = li.StageStatus,
                                      Parallel = li.Parallel,
                                      FirstTPIAgency = li.FirstTPIAgency,
                                      FirstTPIIntervention = li.FirstTPIIntervention,
                                      SecondTPIAgency = li.SecondTPIAgency,
                                      SecondTPIIntervention = li.SecondTPIIntervention,
                                      ThirdTPIAgency = li.ThirdTPIAgency,
                                      ThirdTPIIntervention = li.ThirdTPIIntervention,
                                      ForthTPIAgency = li.ForthTPIAgency,
                                      ForthTPIIntervention = li.ForthTPIIntervention,
                                      FifthTPIAgency = li.FifthTPIAgency,
                                      FifthTPIIntervention = li.FifthTPIIntervention,
                                      SixthTPIAgency = li.SixthTPIAgency,
                                      SixthTPIIntervention = li.SixthTPIIntervention,
                                      AcceptanceStandard = li.AcceptanceStandard,
                                      ApplicableSpecification = li.ApplicableSpecification,
                                      InspectionExtent = li.InspectionExtent,
                                      Remarks = li.Remarks,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {
                    var lst = db.SP_IPI_SEAM_ICL_HISTORY_HEADER_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      SeamNo = li.SeamNo,
                                      RevNo = li.RevNo,
                                      TPRevNo = li.TPRevNo,
                                      PTCApplicable = li.PTCApplicable,
                                      PTCNumber = li.PTCNumber,
                                      Status = li.Status,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else
                {
                    var lst = db.SP_IPI_SEAM_ICL_HISTORY_LINE_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      SeamNo = li.SeamNo,
                                      StageCode = li.StageCode,
                                      StageSequance = li.StageSequance,
                                      StageStatus = li.StageStatus,
                                      Parallel = li.Parallel,
                                      FirstTPIAgency = li.FirstTPIAgency,
                                      FirstTPIIntervention = li.FirstTPIIntervention,
                                      SecondTPIAgency = li.SecondTPIAgency,
                                      SecondTPIIntervention = li.SecondTPIIntervention,
                                      ThirdTPIAgency = li.ThirdTPIAgency,
                                      ThirdTPIIntervention = li.ThirdTPIIntervention,
                                      ForthTPIAgency = li.ForthTPIAgency,
                                      ForthTPIIntervention = li.ForthTPIIntervention,
                                      FifthTPIAgency = li.FifthTPIAgency,
                                      FifthTPIIntervention = li.FifthTPIIntervention,
                                      SixthTPIAgency = li.SixthTPIAgency,
                                      SixthTPIIntervention = li.SixthTPIIntervention,
                                      AcceptanceStandard = li.AcceptanceStandard,
                                      ApplicableSpecification = li.ApplicableSpecification,
                                      InspectionExtent = li.InspectionExtent,
                                      Remarks = li.Remarks,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        public bool IsLineAddedByInspector(string createdby, string bu, string loc)
        {
            bool flag = false;
            string[] lstrole = { "QC1", "QC2", "QC3" };
            var role = (from a in db.ATH001
                        join b in db.ATH004 on a.Role equals b.Id
                        where a.Employee.Equals(createdby, StringComparison.OrdinalIgnoreCase) && a.BU == bu && a.Location == loc && lstrole.Contains(b.Role)
                        select b).Distinct().ToList();

            if (role.Count == 0)
            {
                flag = true;
            }
            return flag;
        }

        #region Add/Delete Stage in Multiple Seams

        public ActionResult LoadMultipleAddDeletePopupPartial(string QualityProject, string Location, string ActionType)
        {
            var pBU = db.QMS010.Where(x => x.QualityProject == QualityProject).First().BU;
            ViewBag.LocationFullName = Location;
            Location = Location.Split('-')[0].Trim();
            ViewBag.QualityProject = QualityProject;
            ViewBag.Location = Location;
            ViewBag.BU = pBU;
            ViewBag.ActionType = ActionType;

            var lstAcceptanceStandard = Manager.GetSubCatagories("Acceptance Standard", pBU, Location, true).Select(i => new CategoryData { Value = i.Description, Code = i.Description, CategoryDescription = i.Description }).ToList();
            ViewBag.AcceptanceStandard = lstAcceptanceStandard;

            var lstApplicableSpecification = Manager.GetSubCatagories("Applicable Specification", pBU, Location, true).Select(i => new CategoryData { Value = i.Description, Code = i.Description, CategoryDescription = i.Description }).ToList();
            ViewBag.ApplicableSpecification = lstApplicableSpecification;

            var lstInspectionExtent = Manager.GetSubCatagories("Inspection Extent", pBU, Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            ViewBag.InspectionExtent = lstInspectionExtent;

            return PartialView("_MultipleAddDeletePopupPartial");
        }

        [HttpPost]
        public ActionResult MultipleAddDelete(string QualityProject, string Location, string ActionType, string FromSeam, string ToSeam, string StageCode, int SeqNo, string AcceptanceStandard = "", string ApplicableSpecification = "", string InspectionExtent = "", string Remarks = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();

            Location = Location.Split('-')[0].Trim();
            var pBU = db.QMS010.Where(x => x.QualityProject == QualityProject).First().BU;
            try
            {
                var TotalAffectedData = db.SP_IPI_SEAM_ICL_MULTIPLE_ADD_DELETE(ActionType, QualityProject, pBU, Location, FromSeam, ToSeam, StageCode, SeqNo, AcceptanceStandard, ApplicableSpecification, InspectionExtent, Remarks, objClsLoginInfo.UserName).ToList();
                int? count = TotalAffectedData.Count > 0 ? TotalAffectedData.Select(x => x.sCount).FirstOrDefault() : 0;
                string seamno = string.Join(", ", TotalAffectedData.Select(x => x.seamno).Distinct());
                objResponseMsg.Key = true;
                objResponseMsg.Value = count + " Seams affected.";
                //objResponseMsg.Value = (TotalAffectedData.Where(x => x.seamno != "").Count() > 0) ? string.Join(",", "Stage already offered in following seam, it cannot be " + (ActionType == "Delete" ? "deleted" : "added") + " in Seam(s): <br />" + seamno) : count + " Seams affected.";

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.TestPlanMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSeamsbyQualityProject(string term, string QualityProject, string Location, string ActionType)
        {
            List<string> lstSeamNo = new List<string>();
            List<ddlValue> lstarrSeamNo = new List<ddlValue>();
            try
            {
                if (!string.IsNullOrWhiteSpace(term))
                {
                    lstSeamNo = (from a in db.QMS030
                                 where a.SeamNo.Contains(term) && a.QualityProject == QualityProject && a.Location == Location
                                 select a.SeamNo).OrderBy(x => x.ToString()).ToList();
                }
                else
                {
                    lstSeamNo = (from a in db.QMS030
                                 where a.QualityProject == QualityProject && a.Location == Location
                                 select a.SeamNo).OrderBy(x => x.ToString()).Take(10).ToList();
                }

                string[] arr = lstSeamNo.ToArray();
                Manager.AlphaNumericalSort(arr);

                foreach (var a in arr)
                {
                    lstarrSeamNo.Add(new ddlValue() { Text = a, Value = a });
                }
                if (lstSeamNo.Count > 0)
                {
                    if (ActionType != "Delete")
                    {
                        lstarrSeamNo.Insert(0, new ddlValue() { Text = "All", Value = "All" });
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(lstarrSeamNo, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetAllStages(string term, string Location, string BU)
        {
            List<ddlValue> lstStageCode = new List<ddlValue>();
            List<ddlValue> lstarrSeamNo = new List<ddlValue>();

            string StageTypeExludedinOfferDropdown = clsImplementationEnum.ConfigParameter.StageTypeExludedinOfferDropdown.GetStringValue();
            var lstStages = Manager.GetConfigValue(StageTypeExludedinOfferDropdown).ToUpper().Split(',').ToArray();

            if (!string.IsNullOrWhiteSpace(term))
            {
                lstStageCode = (from a in db.QMS002
                                where (a.StageCode.Contains(term) || a.StageDesc.Contains(term)) && a.StageCode != null && a.StageCode != "" && a.Location == Location && a.BU == BU && !lstStages.Contains(a.StageType)
                                select new ddlValue { Text = a.StageCode + "-" + a.StageDesc, Value = a.StageCode, id = a.SequenceNo.ToString() }).OrderBy(x => x.Value).Distinct().Take(10).ToList();
            }
            else
            {
                lstStageCode = (from a in db.QMS002
                                where a.StageCode != null && a.StageCode != "" && a.Location == Location && a.BU == BU && !lstStages.Contains(a.StageType)
                                select new ddlValue { Text = a.StageCode + "-" + a.StageDesc, Value = a.StageCode, id = a.SequenceNo.ToString() }).OrderBy(x => x.Value).Distinct().Take(10).ToList();
            }

            return Json(lstStageCode, JsonRequestBehavior.AllowGet);
        }

        #endregion

        public class ResponceMsgWithHeaderID : clsHelper.ResponseMsg
        {
            public string Status;
            public int HeaderId;
            public int Sequence;
            public string Stage;
            public string Project;
            public string BU;
            public string Location;
            public string ProjectCode;
            public string BUCode;
            public string LocationCode;
            public int TPHeaderId;
            public int SWPHeaderId;
        }

    }
}