﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace IEMQS.Areas.IPI.Controllers
{
    public class MaintainICLSeamController : clsBase
    {
        // GET: IPI/MaintainICLSeam

        [SessionExpireFilter]
        [AllowAnonymous]
        //[UserPermissions]
        #region Index Page
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadICLSeamHeaderDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetHeaderGridDataPartial");
        }

        [HttpPost]
        public JsonResult LoadICLSeamHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms30.BU", "qms30.Location");
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or qms30.QualityProject like '%" + param.sSearch + "%'or (qms30.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%')";
                }

                //  var lstHeader = db.SP_IPI_GetQualityIDHeader(startIndex, endIndex, "", whereCondition).ToList();
                var lstHeader = db.SP_IPI_GET_SEAM_ICL_GROUP(startIndex, endIndex, "", whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstHeader
                          select new[] {
                        Convert.ToString(a.ROW_NO),
                        a.QualityProject,
                        a.Project,
                        a.BU,
                        a.Location,
                        Convert.ToString(a.HeaderId)
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Header Details
        [SessionExpireFilter]
        public ActionResult AddHeader(int id)
        {
            QMS030 objQMS030 = new QMS030();
            if (id > 0)
            {
                objQMS030 = db.QMS030.Where(x => x.HeaderId == id).FirstOrDefault();
                var result = db.SP_GET_BU_LOCATION_PROJECT_CONTRACT_CUSTOMER_DESCRIPTION(objQMS030.BU, objQMS030.Location, objQMS030.Project, null, null).FirstOrDefault();
                objQMS030.BU = result.BU;
                objQMS030.Project = result.Project;
                objQMS030.Location = result.Location;
                //objQMS030.BU = db.COM002.Where(a => a.t_dimx == objQMS030.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                //objQMS030.Project = db.COM001.Where(i => i.t_cprj == objQMS030.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                //objQMS030.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objQMS030.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            }
            return View(objQMS030);
        }

        [HttpPost]
        public ActionResult GetICLHeaderDataPartial(string status, int headerId)
        {
            ViewBag.Status = status;
            ViewBag.HeaderId = headerId;
            return PartialView("_GetICLHeaderDataPartial");
        }
        [HttpPost]
        public JsonResult GetICLSeamHeaderData(JQueryDataTableParamModel param, string status, int headerId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1 and HeaderId = " + headerId;
                string strSortOrder = string.Empty;

                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and qms30.Status in('" + clsImplementationEnum.ICLSeamStatus.Draft.GetStringValue() + "')";
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms30.BU", "qms30.Location");
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += " and (qms30.Status like '%" + param.sSearch + "%'or qms30.SeamNo like '%" + param.sSearch + "%'or qms30.PTCNumber like '%" + param.sSearch + "%')";
                }

                var lstHeader = db.SP_IPI_ICL_HEADER_DETAILS(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstHeader
                          select new[] {
                       "",
                       Convert.ToString(a.HeaderId),
                       Convert.ToString(a.SeamNo),
                       Convert.ToString(a.RevNo),
                       Convert.ToString(a.TPRevNo),
                       a.PTCApplicable==true?"Yes":"No",
                       a.PTCNumber,
                       a.Status
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult LoadICLSeamHeaderForm(int headerID)
        {
            QMS030 objQMS030 = new QMS030();
            if (headerID > 0)
            {
                objQMS030 = db.QMS030.Where(x => x.HeaderId == headerID).FirstOrDefault();
                objQMS030.BU = db.COM002.Where(a => a.t_dimx == objQMS030.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objQMS030.Project = db.COM001.Where(i => i.t_cprj == objQMS030.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objQMS030.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objQMS030.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            }

            return PartialView("_LoadICLSeamHeaderForm", objQMS030);
        }

        [HttpPost]
        public bool checkStageExist(string stagecode, int header)
        {
            bool Flag = false;
            if (!string.IsNullOrWhiteSpace(stagecode))
            {
                QMS031 objQMS031 = db.QMS031.Where(x => x.StageCode == stagecode && x.HeaderId == header).FirstOrDefault();
                if (objQMS031 != null)
                {
                    Flag = true;
                }
            }
            return Flag;
        }
        #endregion

        #region Line Details
        //[HttpPost]
        //public ActionResult GetQualityIDLinesPartial(int headerId)
        //{
        //    QMS031 objQMS031 = new QMS031();
        //    QMS030 objQMS030 = new QMS030();
        //    if (headerId > 0)
        //        objQMS030 = db.QMS030.Where(i => i.HeaderId == headerId).FirstOrDefault();

        //    var lstAcceptanceStandard = Manager.GetSubCatagories("Acceptance Standard", objQMS015.BU, objQMS015.Location).Select(i => new CategoryData { Value = i.Description, Code = i.Description, CategoryDescription = i.Description }).ToList();
        //    var lstApplicableSpecification = Manager.GetSubCatagories("Applicable Specification", objQMS015.BU, objQMS015.Location).Select(i => new CategoryData { Value = i.Description, Code = i.Description, CategoryDescription = i.Description }).ToList();
        //    var lstInspectionExtent = Manager.GetSubCatagories("Inspection Extent", objQMS015.BU, objQMS015.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
        //    NDEModels objNDEModels = new NDEModels();
        //    ViewBag.Stages = db.QMS002.Where(i => i.BU.Equals(objQMS015.BU, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(objQMS015.Location, StringComparison.OrdinalIgnoreCase)).Select(i => new CategoryData { Value = i.StageCode, Code = i.StageCode, CategoryDescription = i.StageCode + "-" + i.StageDesc }).ToList();
        //    ViewBag.AcceptanceStandard = lstAcceptanceStandard;
        //    ViewBag.ApplicableSpecification = lstApplicableSpecification;
        //    ViewBag.InspectionExtent = lstInspectionExtent;
        //    ViewBag.HeaderStatus = objQMS015.Status;
        //    ViewBag.QualityIdName = objQMS015.QualityId;

        //    {
        //        objQMS031.HeaderId = headerId;
        //        objQMS031.RevNo = objQMS015.RevNo;
        //    }
        //    return PartialView("_GetQualityIDLinesPartial", objQMS031);
        //}

        [HttpPost]
        public ActionResult LoadICLStagesDataTable(JQueryDataTableParamModel param, int refHeaderId)
        {
            try
            {
                string status = db.QMS030.Where(x => x.HeaderId == refHeaderId).Select(x => x.Status).FirstOrDefault();
                bool isEditable = string.Equals(status, clsImplementationEnum.ICLSeamStatus.SendForApproval.GetStringValue(), StringComparison.OrdinalIgnoreCase) ? true : false;
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;
                string strSortOrder = string.Empty;
                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms31.BU", "qms31.Location");
                whereCondition += " AND  qms31.HeaderId = " + refHeaderId;
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    string[] columnName = { "StageCode", "StageSequance", "AcceptanceStandard", "ApplicableSpecification", "InspectionExtent", "RevNo", "StageStatus", "Remarks", "LineId" };
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstStages = db.SP_IPI_ICL_LINE_DETAILS(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstStages.Select(i => i.TotalCount).FirstOrDefault();

                List<SelectListItem> BoolenList = new List<SelectListItem>() { new SelectListItem { Text = "Yes", Value = "Yes" }, new SelectListItem { Text = "No", Value = "No" } };

                var Location = (from a in db.COM002
                                where a.t_dtyp == 1 && a.t_dimx != ""
                                select new { a.t_dimx, Desc = a.t_desc }).ToList();

                int newRecordId = 0;
                NDEModels objNDEModels = new NDEModels();
                var newRecord = new[] {
                          Helper.GenerateHidden(newRecordId, "HeaderId", param.CTQHeaderId),
                                        GenerateAutoCompleteonBlur(newRecordId,"txtStageCode","","getSequence(this);",false,"","StageCode")+""+Helper.GenerateHidden(newRecordId,"StageCode"),
                                        Helper.GenerateTextbox(newRecordId,"StageSequance","","CheckDuplicate(this);"),
                                        GenerateAutoComplete(newRecordId, "AcceptanceStandard","","",false,"","AcceptanceStandard")+""+Helper.GenerateHidden(newRecordId,"AcceptanceStandard"),
                                        GenerateAutoComplete(newRecordId, "ApplicableSpecification","","",false,"","ApplicableSpecification")+""+Helper.GenerateHidden(newRecordId,"ApplicableSpecification"),
                                        GenerateAutoComplete(newRecordId, "txtInspectionExtent","","",false,"","InspectionExtent")+""+Helper.GenerateHidden(newRecordId,"InspectionExtent"),
                                        Helper.GenerateTextbox(newRecordId,"Remarks"),
                                         clsImplementationEnum.TestPlanStatus.Added.GetStringValue(),
                                        Helper.GenerateGridButton(newRecordId, "Add", "Add Rocord", "fa fa-plus", "SaveNewStage();" ),
                                    //Helper.GenerateHidden(newRecordId, "HeaderId", Convert.ToString(refHeaderId)),
                                    //Helper.GenerateHidden(newRecordId, "LineId", ""),
                                    //HTMLAutoComplete(newRecordId,"txtStageCode","","CheckDuplicateStages(this,"+ refHeaderId +",0);",false,"","StageCode")+""+Helper.GenerateHidden(newRecordId,"StageCode"),//Helper.GenerateTextbox(newRecordId, "StageCode"),
                                    //Helper.GenerateTextbox(newRecordId,"StageSequance","","getSequence(this); "),
                                    //HTMLAutoComplete(newRecordId, "AcceptanceStandard","","",false,"","AcceptanceStandard")+""+Helper.GenerateHidden(newRecordId,"AcceptanceStandard"),
                                    //HTMLAutoComplete(newRecordId, "tApplicableSpecification","","",false,"","ApplicableSpecification")+""+Helper.GenerateHidden(newRecordId,"ApplicableSpecification"),
                                    //HTMLAutoComplete(newRecordId, "txtInspectionExtent","","",false,"","InspectionExtent")+""+Helper.GenerateHidden(newRecordId,"InspectionExtent"),
                                    //HTMLAutoComplete(newRecordId, "Remarks","","SaveLines(1);"),
                                    //clsImplementationEnum.QualityIdStageStatus.ADDED.GetStringValue(),
                                    //Helper.GenerateGridButton(newRecordId, "Add", "Add Rocord", "fa fa-plus", "SaveLines(0);" ),
                                };

                string strStatus = "";
                var data = (from uc in lstStages
                            select new[]
                            {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.StageCode),
                                Convert.ToString(uc.StageSequance),
                                GenerateAutoComplete(uc.LineId, "AcceptanceStandard", uc.AcceptanceStandard, "UpdateLineDetails(this, "+ uc.LineId +");",false,"","",(string.Equals(strStatus,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false)|| string.Equals(uc.StageStatus, clsImplementationEnum.TestPlanStatus.Deleted.GetStringValue())  ? true : false),
                                GenerateAutoComplete(uc.LineId, "ApplicableSpecification", uc.ApplicableSpecification, "UpdateLineDetails(this, "+ uc.LineId +");",false,"","",(string.Equals(strStatus,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false)|| string.Equals(uc.StageStatus, clsImplementationEnum.TestPlanStatus.Deleted.GetStringValue())  ? true : false),
                                GenerateAutoComplete(uc.LineId, "txtInspectionExtent", objNDEModels.GetCategory(uc.InspectionExtent).CategoryDescription,"UpdateLineDetails(this, "+ uc.LineId +");",false,"","InspectionExtent",(string.Equals(strStatus,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false) || string.Equals(uc.StageStatus, clsImplementationEnum.TestPlanStatus.Deleted.GetStringValue())  ? true : false)+""+Helper.GenerateHidden(uc.LineId,"InspectionExtent",uc.InspectionExtent),
                                Helper.GenerateTextbox(uc.LineId, "Remarks", Convert.ToString( uc.Remarks), "UpdateLineDetails(this, " + uc.LineId + ");", (string.Equals(strStatus, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue())  ? true : false) || string.Equals(uc.StageStatus, clsImplementationEnum.TestPlanStatus.Deleted.GetStringValue())  ? true : false),
                                //Convert.ToString(uc.Remarks),
                                Convert.ToString(uc.StageStatus),
                                string.Equals(uc.StageStatus,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) ? "" : HTMLActionString(uc.HeaderId,uc.StageStatus,"Delete","Delete Record","fa fa-trash-o","DeleteRecord("+ uc.LineId +");")+"<a class='btn btn-xs' href='javascript:void(0)' onclick=ShowTimeline('/IPI/ApproveTestPlan/ShowTimeline?HeaderID="+Convert.ToInt32(uc.HeaderId)+"&LineId="+Convert.ToInt32(uc.LineId)+"');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a>",


                               // Helper.GenerateHidden(uc.LineId, "HeaderId", Convert.ToString(uc.HeaderId)),
                               //Helper.GenerateHidden(uc.LineId,"LineId",Convert.ToString(uc.LineId)),
                               //uc.StageCode,
                               //Convert.ToString( uc.StageSequance),
                               //HTMLAutoComplete(uc.LineId, "AcceptanceStandard", uc.AcceptanceStandard, "UpdateICLStage(this, "+ uc.LineId +");",false,"","",isEditable),
                               //HTMLAutoComplete(uc.LineId, "txtApplicableSpecification", uc.ApplicableSpecification,"UpdateICLStage(this, "+ uc.LineId +");",false,"","ApplicableSpecification",false),
                               ////HTMLAutoComplete(uc.LineId, "ApplicableSpecification", uc.ApplicableSpecification, "UpdateICLStage(this, "+ uc.LineId +");",false,"","",isEditable),
                               //HTMLAutoComplete(uc.LineId, "txtInspectionExtent", uc.InspectionExtent,"UpdateICLStage(this, "+ uc.LineId +");",false,"","InspectionExtent",isEditable)+""+Helper.GenerateHidden(uc.LineId,"InspectionExtent",uc.InspectionExtent),
                               //Helper.GenerateHTMLTextbox(uc.LineId, "Remarks", Convert.ToString(uc.Remarks),"UpdateICLStage(this, "+ uc.LineId +");",false,"",isEditable),
                               //uc.StageStatus,
                               //""
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstStages.Count > 0 ? lstStages.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstStages.Count > 0 ? lstStages.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public string GenerateAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string maxlength = "")
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onchange='" + onBlurMethod + "'" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' hdElement='" + hdElementId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + "  " + MaxCharcters + "  " + (disabled ? "disabled" : "") + " />";

            return strAutoComplete;
        }

        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (!string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            }

            return htmlControl;
        }
        public string GenerateAutoCompleteonBlur(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string maxlength = "")
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' hdElement='" + hdElementId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + "  " + MaxCharcters + "  " + (disabled ? "disabled" : "") + " />";

            return strAutoComplete;
        }

        public string HTMLAutoComplete(int rowId, string columnName, string columnValue = "", string onChangeMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false)
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string onBlurEvent = !string.IsNullOrEmpty(onChangeMethod) ? "onChange='" + onChangeMethod + "'" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' hdElement='" + hdElementId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + (disabled ? "disabled" : "") + " />";

            return strAutoComplete;
        }

        [SessionExpireFilter]
        [AllowAnonymous]
        //[UserPermissions]

        [HttpPost]
        public ActionResult LoadICLSeamLineData(int headerId, int lineId)
        {
            QMS030 objQMS030 = new QMS030();
            QMS031 objQMS031 = new QMS031();
            NDEModels objNDEModels = new NDEModels();

            objQMS030 = db.QMS030.Where(x => x.HeaderId == headerId).FirstOrDefault();
            ViewBag.Stages = db.QMS002.Where(i => i.BU.Equals(objQMS030.BU, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(objQMS030.Location, StringComparison.OrdinalIgnoreCase)).Select(i => new CategoryData { Value = i.StageCode, Code = i.StageCode, CategoryDescription = i.StageCode + "-" + i.StageDesc }).ToList();
            var lstAcceptanceStandard = Manager.GetSubCatagories("Acceptance Standard", objQMS030.BU, objQMS030.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
            var lstApplicableSpecification = Manager.GetSubCatagories("Applicable Specification", objQMS030.BU, objQMS030.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
            var lstInspectionExtent = Manager.GetSubCatagories("Inspection Extent", objQMS030.BU, objQMS030.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();

            ViewBag.AcceptanceStandard = lstAcceptanceStandard;
            ViewBag.ApplicableSpecification = lstApplicableSpecification;
            ViewBag.InspectionExtent = lstInspectionExtent;

            if (lineId > 0)
            {
                objQMS031 = db.QMS031.Where(x => x.LineId == lineId).FirstOrDefault();
                ViewBag.StageSelected = db.QMS002.Where(i => i.StageCode.Equals(objQMS031.StageCode, StringComparison.OrdinalIgnoreCase)).Select(i => i.StageCode + "-" + i.StageDesc).FirstOrDefault();
                ViewBag.AcceptanceStandardSelected = objNDEModels.GetCategory(objQMS031.AcceptanceStandard).CategoryDescription;
                ViewBag.ApplicableSpecificationSelected = objNDEModels.GetCategory(objQMS031.ApplicableSpecification).CategoryDescription;
                ViewBag.InspectionExtentSelected = objNDEModels.GetCategory(objQMS031.InspectionExtent).CategoryDescription;
                ViewBag.Action = "line edit";
            }


            ViewBag.QProject = objQMS030.QualityProject;
            ViewBag.BU = db.COM002.Where(a => a.t_dimx == objQMS030.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            ViewBag.Project = db.COM001.Where(i => i.t_cprj == objQMS030.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objQMS030.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            ViewBag.SeamNo = objQMS030.SeamNo;
            ViewBag.RevNo = objQMS030.RevNo;
            return PartialView("_LoadICLSeamLinesDetailsForm", objQMS031);
        }

        //[HttpPost]
        //public ActionResult GetSquence(string stagecode)
        //{
        //    ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
        //    QMS002 objQMS002 = new QMS002();
        //    string strStage = stagecode;
        //    var Sequence = db.QMS002.Where(i => i.StageCode == stagecode).FirstOrDefault().SequenceNo;
        //    objResponseMsg.Sequence = Convert.ToInt32(Sequence);
        //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //}
        [HttpPost]
        public ActionResult UpdateICLStage(int lineId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            QMS031 objQMS031 = new QMS031();
            try
            {
                if (lineId > 0)
                {
                    ///  objQMS031 = db.QMS031.Where(i => i.LineId == lineId).FirstOrDefault();
                    if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                    {
                        string str = db.SP_IPI_SEAM_ICL_LINE_UPDATE(lineId, columnName, columnValue, objClsLoginInfo.UserName).FirstOrDefault();
                        // var qms30 = db.QMS030.Where(i => i.HeaderId == objQMS031.HeaderId).FirstOrDefault();
                        // if (string.Equals(qms30.Status, clsImplementationEnum.ICLSeamStatus.Approved.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                        // {
                        //     qms30.Status = clsImplementationEnum.ICLSeamStatus.Draft.GetStringValue();                           
                        //     qms30.RevNo += 1;
                        // }
                        // objQMS031.EditedBy = objClsLoginInfo.UserName;
                        // objQMS031.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Saved Successfully";
                        //objResponseMsg.HeaderId = qms30.HeaderId;
                        //objResponseMsg.HeaderStatus = qms30.Status;
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Column name or value is null";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public bool GetSquence(string stagecode, int seq, int headerId)
        {
            bool Flag = false;
            string strStage = stagecode;
            if (!string.IsNullOrWhiteSpace(stagecode))
            {
                QMS031 objQMS031 = db.QMS031.Where(i => i.StageCode == stagecode && i.StageSequance == seq && i.HeaderId == headerId).FirstOrDefault();
                if (objQMS031 != null)
                {
                    Flag = true;
                }
            }
            return Flag;
        }

        [HttpPost]
        public ActionResult SaveICLSeamLines(FormCollection fc, QMS031 qms031)
        {
            QMS031 objQMS031 = new QMS031();
            QMS030 objQMS030 = new QMS030();

            string addedStatus = clsImplementationEnum.ICLSeamStatus.Added.GetStringValue();
            string modifiedStatus = clsImplementationEnum.ICLSeamStatus.Modified.GetStringValue();
            string deletedStatus = clsImplementationEnum.ICLSeamStatus.Deleted.GetStringValue();

            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                int headerID = qms031.HeaderId;
                bool IsEdited = false;
                objQMS030 = db.QMS030.Where(x => x.HeaderId == headerID).FirstOrDefault();
                if (qms031.LineId > 0)
                {
                    objQMS031 = db.QMS031.Where(x => x.LineId == qms031.LineId).FirstOrDefault();
                    IsEdited = true;
                }
                objQMS031.QualityProject = objQMS030.QualityProject;
                objQMS031.Project = objQMS030.Project;
                objQMS031.BU = objQMS030.BU;
                objQMS031.Location = objQMS030.Location;
                objQMS031.SeamNo = objQMS030.SeamNo;
                objQMS031.StageCode = qms031.StageCode;
                objQMS031.StageSequance = qms031.StageSequance;
                objQMS031.AcceptanceStandard = qms031.AcceptanceStandard;
                objQMS031.ApplicableSpecification = qms031.ApplicableSpecification;
                objQMS031.InspectionExtent = qms031.InspectionExtent;
                objQMS031.Remarks = qms031.Remarks;
                objQMS031.HeaderId = objQMS030.HeaderId;
                if (objQMS030.Status == clsImplementationEnum.ICLSeamStatus.Approved.GetStringValue())
                {
                    objQMS030.RevNo = Convert.ToInt32(objQMS030.RevNo) + 1;
                    objQMS030.Status = clsImplementationEnum.ICLSeamStatus.Draft.GetStringValue();
                    objQMS030.EditedBy = objClsLoginInfo.UserName;
                    objQMS030.EditedOn = DateTime.Now;
                }
                if (IsEdited)
                {

                    //objQMS031. = objQMS030.RevNo;
                    objQMS031.StageStatus = modifiedStatus;
                    objQMS031.EditedBy = objClsLoginInfo.UserName;
                    objQMS031.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.ICLSeamMessages.LineUpdate;
                }
                else
                {
                    //objQMS031.RevNo = objQMS030.RevNo;
                    objQMS031.StageStatus = addedStatus;
                    objQMS031.CreatedBy = objClsLoginInfo.UserName;
                    objQMS031.CreatedOn = DateTime.Now;
                    db.QMS031.Add(objQMS031);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.ICLSeamMessages.LineInsert;
                }
                objResponseMsg.Status = objQMS030.Status;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ICLSeamMessages.Error.ToString();
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteLine(int LineID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {

                QMS031 objQMS031 = db.QMS031.Where(x => x.LineId == LineID).FirstOrDefault();
                if (objQMS031 != null)
                {
                    objQMS031.StageStatus = clsImplementationEnum.ICLSeamStatus.Deleted.GetStringValue();
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details successfully deleted.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details not available for delete.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteICLStage(int headerId, int lineId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            QMS030 objQMS030 = new QMS030();
            QMS031 objQMS031 = new QMS031();
            try
            {
                if (headerId > 0 && lineId > 0)
                {
                    objQMS030 = db.QMS030.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    objQMS031 = db.QMS031.Where(i => i.HeaderId == headerId && i.LineId == lineId).FirstOrDefault();

                    if (objQMS031 != null)
                    {
                        if (objQMS030.Status == clsImplementationEnum.ICLSeamStatus.Draft.GetStringValue() && objQMS030.RevNo == 0)
                        {
                            db.QMS031.Remove(objQMS031);
                        }
                        else
                        {
                            objQMS031.StageStatus = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
                            // objQMS031.DeletedBy = objClsLoginInfo.UserName;
                            //objQMS031.DeletedOn = DateTime.Now;

                            if (string.Equals(objQMS030.Status, clsImplementationEnum.ICLSeamStatus.Approved.GetStringValue()))
                            {
                                var lstLines = db.QMS031.Where(i => i.HeaderId == headerId).ToList();
                                // if (lstLines.Any())
                                //lstLines.ForEach(i => i.RevNo = objQMS030.RevNo + 1);

                                objQMS030.Status = clsImplementationEnum.ICLSeamStatus.Draft.GetStringValue();
                                objQMS030.RevNo += 1;
                            }
                        }
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "ICL Stage Deleted Successfully";
                        objResponseMsg.HeaderId = objQMS030.HeaderId;
                        objResponseMsg.HeaderStatus = objQMS030.Status;
                        objResponseMsg.RevNo = Convert.ToString(objQMS030.RevNo);
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }


            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SendForApproval_Old(int strHeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS030 objQMS030 = db.QMS030.Where(u => u.HeaderId == strHeaderId).SingleOrDefault();
                if (objQMS030 != null)
                {
                    objQMS030.Status = clsImplementationEnum.ICLSeamStatus.SendForApproval.GetStringValue();
                    objQMS030.SubmittedBy = objClsLoginInfo.UserName;
                    objQMS030.SubmittedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details has been successfully sent for approval.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details not available for approval.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SendForApproval(string strHeader)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();

            List<QMS030> lstIClIds = new List<QMS030>();
            List<int> lstIClIdWithoutLines = new List<int>();
            List<int> lstIClIdNotDraft = new List<int>();
            List<int> lstIClIdSubmitted = new List<int>();
            try
            {
                if (!string.IsNullOrEmpty(strHeader))
                {
                    int[] headerIds = Array.ConvertAll(strHeader.Split(','), s => int.Parse(s));
                    lstIClIds = db.QMS030.Where(i => headerIds.Contains(i.HeaderId)).ToList();

                    foreach (var item in lstIClIds)
                    {
                        var qms30 = db.QMS030.Where(i => i.HeaderId == item.HeaderId).FirstOrDefault();
                        if (qms30 != null)
                        {
                            if (string.Equals(qms30.Status, clsImplementationEnum.ICLSeamStatus.Draft.GetStringValue()))
                            {
                                if (db.QMS031.Where(i => i.HeaderId == qms30.HeaderId).Any())
                                {
                                    qms30.Status = clsImplementationEnum.ICLSeamStatus.SendForApproval.GetStringValue();
                                    qms30.SubmittedBy = objClsLoginInfo.UserName;
                                    qms30.SubmittedOn = DateTime.Now;

                                    db.QMS031.Where(i => i.HeaderId == qms30.HeaderId).ToList().ForEach(i => { i.SubmittedBy = objClsLoginInfo.UserName; i.SubmittedOn = DateTime.Now; });
                                    db.SaveChanges();
                                    lstIClIdSubmitted.Add(qms30.HeaderId);
                                }
                                else
                                {
                                    lstIClIdWithoutLines.Add(qms30.HeaderId);
                                }
                            }
                            else
                            {
                                lstIClIdNotDraft.Add(qms30.HeaderId);
                            }
                        }
                    }
                    objResponseMsg.Key = true;
                    if (lstIClIdSubmitted.Any())
                    {
                        objResponseMsg.Value = string.Format("{0} Inspection Checklist is sent for approval successfully", lstIClIdSubmitted.Count);
                    }
                    if (lstIClIdWithoutLines.Any())
                    {
                        objResponseMsg.WithoutLines = string.Format("No Stage(s) Exist(s) For Inspection Checklist :{0}.Please Add Stages.", string.Join(",", lstIClIdWithoutLines));
                    }
                    if (lstIClIdNotDraft.Any())
                    {
                        objResponseMsg.NotDraft = string.Format("Inspection Checklist(s) {0} have been skipped as Inspection Checklist(s) {0} are either Sent For Approval Or Approved", string.Join(",", lstIClIdNotDraft));
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please Select Inspection Checklist for sending for approval";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult LoadICLSeamLineGridData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "";
                strWhere += Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();

                string[] columnName = { "StageCode", "StageSequance", "AcceptanceStandard", "ApplicableSpecification", "InspectionExtent", "RevNo", "StageStatus", "Remarks", "LineId" };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);

                var lstResult = db.SP_IPI_ICL_LINE_DETAILS
                                (
                               StartIndex,
                               EndIndex,
                               "",
                               strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                //Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.StageCode),
                                Convert.ToString(uc.StageSequance),
                                Convert.ToString(uc.AcceptanceStandard),
                                Convert.ToString(uc.ApplicableSpecification),
                                Convert.ToString(uc.InspectionExtent),
                                Convert.ToString(uc.Parallel),
                                 Convert.ToString(uc.StageStatus),
                                Convert.ToString(uc.Remarks),
                                Convert.ToString(uc.LineId),
                           }).ToList();


                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult LoadResequenceData(int headerId)
        {
            QMS030 objQMS030 = new QMS030();
            objQMS030 = db.QMS030.Where(x => x.HeaderId == headerId).FirstOrDefault();
            return PartialView("_LoadResequenceData", objQMS030);
        }
        [HttpPost]
        public JsonResult LoadResequenceGridData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "";
                strWhere += Manager.MakeWhereConditionForCTQHeaderLines(param.Headerid).ToString();

                string[] columnName = { "StageCode", "StageSequance", "NewSequance", "Status", "LineId" };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                var lstResult = db.SP_IPI_ICL_RESEQUENCING_DETAILS
                                (
                               StartIndex,
                               EndIndex,
                               "",
                               strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.StageCode),
                                Convert.ToString(uc.StageSequance),
                                getNewSequence(uc.NewSequence),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId),
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdateSequence(int lineId, int header, int newsequence)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS030 objQMS030 = new QMS030();
                QMS031 objQMS031 = new QMS031();
                objQMS030 = db.QMS030.Where(x => x.HeaderId == header).FirstOrDefault();
                objQMS031 = db.QMS031.Where(x => x.LineId == lineId).FirstOrDefault();
                if (objQMS031 != null)
                {
                    if (newsequence != 0)
                    {
                        if (objQMS030.Status == clsImplementationEnum.ICLSeamStatus.Approved.GetStringValue())
                        {
                            objQMS030.RevNo = Convert.ToInt32(objQMS030.RevNo) + 1;
                            objQMS030.Status = clsImplementationEnum.ICLSeamStatus.Draft.GetStringValue();
                            objQMS030.EditedBy = objClsLoginInfo.UserName;
                            objQMS030.EditedOn = DateTime.Now;
                        }
                        objQMS031.StageSequance = newsequence;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Stage Sequence has been updated for Stage " + objQMS031.StageCode;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public string getNewSequence(int? newSequance)
        {
            if (newSequance != 0)
            {
                return "<input type='text' name='txtNewSequence'  onkeypress = 'return isNumber(event)' class='form-control input-sm input-small input-inline' value='" + newSequance + "' />";
            }
            else
            {
                return "<input type='text' name='txtNewSequence'  onkeypress = 'return isNumber(event)' class='form-control input-sm input-small input-inline' value='" + "" + "' />";
            }
        }
        [HttpPost]
        public ActionResult GetSeamICLLinesPartial(int headerId)
        {
            QMS030 objQMS030 = new QMS030();
            if (headerId > 0)
                objQMS030 = db.QMS030.Where(i => i.HeaderId == headerId).FirstOrDefault();

            var lstAcceptanceStandard = Manager.GetSubCatagories("Acceptance Standard", objQMS030.BU, objQMS030.Location).Select(i => new CategoryData { Value = i.Description, Code = i.Description, CategoryDescription = i.Description }).ToList();
            var lstApplicableSpecification = Manager.GetSubCatagories("Applicable Specification", objQMS030.BU, objQMS030.Location).Select(i => new CategoryData { Value = i.Description, Code = i.Description, CategoryDescription = i.Description }).ToList();
            var lstInspectionExtent = Manager.GetSubCatagories("Inspection Extent", objQMS030.BU, objQMS030.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            NDEModels objNDEModels = new NDEModels();
            ViewBag.Stages = db.QMS002.Where(i => i.BU.Equals(objQMS030.BU, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(objQMS030.Location, StringComparison.OrdinalIgnoreCase)).Select(i => new CategoryData { Value = i.StageCode, Code = i.StageCode, CategoryDescription = i.StageCode + "-" + i.StageDesc }).ToList();
            ViewBag.AcceptanceStandard = lstAcceptanceStandard;
            ViewBag.ApplicableSpecification = lstApplicableSpecification;
            ViewBag.InspectionExtent = lstInspectionExtent;
            ViewBag.HeaderStatus = objQMS030.Status;
            ViewBag.SeamNo = objQMS030.SeamNo;

            return View("_GetSeamICLLinesPartial", objQMS030);
        }
        [HttpPost]
        public ActionResult CheckDuplicateStages(int? headerId, int? lineId, string stageCode)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            QMS030 objQMS030 = new QMS030();
            try
            {
                if (headerId > 0)
                    objQMS030 = db.QMS030.Where(i => i.HeaderId == headerId).FirstOrDefault();

                int rowId = lineId.HasValue ? lineId.Value : 0;

                if (isICLStageExist(objQMS030.QualityProject, objQMS030.BU, objQMS030.Location, objQMS030.SeamNo, stageCode, rowId))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Stage Already Exists";
                }
                else
                {
                    objResponseMsg.Key = true;
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public bool isICLStageExist(string qualityProj, string bu, string location, string SeamNo, string stageCode, int lineId)
        {
            bool isQualityIdStageExist = false;
            if (!string.IsNullOrEmpty(qualityProj) && !string.IsNullOrEmpty(bu) && !string.IsNullOrEmpty(location) && !string.IsNullOrEmpty(stageCode))
            {
                var lstQMS031 = db.QMS031.Where(i => i.QualityProject.Equals(qualityProj, StringComparison.OrdinalIgnoreCase) &&
                                                   i.SeamNo.Equals(SeamNo, StringComparison.OrdinalIgnoreCase) &&
                                                   i.BU.Equals(bu, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) &&
                                                   i.StageCode.Equals(stageCode, StringComparison.OrdinalIgnoreCase) && i.LineId != lineId).ToList();
                if (lstQMS031.Any())
                    isQualityIdStageExist = true;
            }

            return isQualityIdStageExist;
        }

        #endregion

        #region History
        [HttpPost]
        public ActionResult GetICLSeamHistoryDetails(int Id)
        {
            QMS030_Log objQMS030Log = new QMS030_Log();
            objQMS030Log = db.QMS030_Log.Where(x => x.HeaderId == Id).FirstOrDefault();
            return PartialView("_ICLSeamHistoryPartial", objQMS030Log);
        }

        public ActionResult ViewLogDetails(int? Id)
        {
            QMS030_Log objQMS030 = new QMS030_Log();
            if (Id > 0)
            {
                objQMS030 = db.QMS030_Log.Where(i => i.Id == Id).FirstOrDefault();
                objQMS030.BU = db.COM002.Where(a => a.t_dimx == objQMS030.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objQMS030.Project = db.COM001.Where(i => i.t_cprj == objQMS030.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objQMS030.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objQMS030.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                ViewBag.Action = "edit";
            }
            return View(objQMS030);
        }

        [HttpPost]
        public JsonResult LoadICLHeaderHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;

                strWhere += "1=1 and HeaderId=" + param.Headerid;
                string[] columnName = { "QualityProject", "BU", "SeamNo", "Location", "CreatedBy", "Status" };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);

                var lstResult = db.SP_IPI_ICL_HEADER_HISTORY_DETAILS
                                (
                                StartIndex, EndIndex, "", strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.QualityProject),
                             Convert.ToString(uc.BU),
                            Convert.ToString(uc.SeamNo),
                            Convert.ToString(uc.CreatedBy),
                             Convert.ToString(uc.Location),
                              Convert.ToString(uc.Status),
                            "<center><a class='btn btn-xs green' href='/IPI/MaintainICLSeam/ViewLogDetails?Id="+uc.Id+"'>View<i style='margin-left:5px;' class='fa fa-eye'></i></a></center>",
                            }).ToList();



                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadICLLineHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "";
                strWhere += "1=1 and RefId=" + param.Headerid;

                string[] columnName = { "StageCode", "StageSequance", "AcceptanceStandard", "ApplicableSpecification", "InspectionExtent", "RevNo", "StageStatus", "Remarks", "LineId", };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);

                var lstResult = db.SP_IPI_ICL_LINE_HISTORY_DETAILS
                                (
                               StartIndex,
                               EndIndex,
                               "",
                               strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                //Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.StageCode),
                                Convert.ToString(uc.StageSequance),
                                Convert.ToString(uc.AcceptanceStandard),
                                Convert.ToString(uc.ApplicableSpecification),
                                Convert.ToString(uc.InspectionExtent),
                                Convert.ToString(uc.Parallel),
                                 Convert.ToString(uc.StageStatus),
                                Convert.ToString(uc.Remarks),
                           }).ToList();


                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        public class ResponceMsgWithHeaderID : clsHelper.ResponseMsg
        {
            public string Status;
            public int HeaderId;
            public int Sequence;
            public string Stage;
        }
    }
}