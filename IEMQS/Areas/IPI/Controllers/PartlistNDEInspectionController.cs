﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace IEMQS.Areas.IPI.Controllers
{
    public class PartlistNDEInspectionController : clsBase
    {
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult GetNDEOffers()
        {
            return View();
        }
        public ActionResult ShowTimelineNDEInspectionPartial(int requestId)
        {
            TimelineViewModel model = new TimelineViewModel();
            QMS065 objQMS065 = db.QMS065.FirstOrDefault(x => x.RequestId == requestId);
            model.OfferedBy = Manager.GetPsidandDescription(objQMS065.OfferedBy);
            model.OfferedOn = objQMS065.OfferedOn;
            model.RequestGeneratedBy = Manager.GetPsidandDescription(objQMS065.RequestGeneratedBy);
            model.RequestGeneratedOn = objQMS065.RequestGeneratedOn;
            model.ConfirmedBy = Manager.GetPsidandDescription(objQMS065.ConfirmedBy);
            model.ConfirmedOn = objQMS065.ConfirmedOn;
            return PartialView("_ShowTimelineNDEInspectionPartial", model);
        }
        [HttpPost]
        public ActionResult LoadPartlistNDEOfferDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_LoadPartlistNDEOfferDataPartial");
        }

        [HttpPost]
        public JsonResult LoadPartlistNDEOfferData(JQueryDataTableParamModel param, string qualityProject, string partNumber)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;

                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and (RequestGeneratedBy in('') or RequestGeneratedBy is null) and QCIsReturned IS NULL ";
                }
                else if (param.CTQCompileStatus.ToUpper() == "RETURNNDEREQUEST")
                {
                    strWhere += "1=1 and OfferedBy is not null and OfferedOn is not null and RequestGeneratedBy is null and QCIsReturned=1 ";
                }
                else
                {
                    strWhere += "1=1 ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                //"qms065.Project+' - '+com1.t_dsca",
                                                "QualityProject",
                                                "Shop",
                                                "ShopLocation",
                                                "ShopRemark",
                                                //"qms065.Project+' - '+com1.t_dsca",
                                                //"ltrim(rtrim(qms065.BU))+' - '+bcom2.t_desc",
                                                //"ltrim(rtrim(qms065.Location))+' - '+lcom2.t_desc",
                                                "PartAssemblyFindNumber",
                                                "PartAssemblyChildItemCode",
                                                "TestResult",
                                                "qms065.CreatedBy +' - '+com003c.t_name",
                                                "(dbo.GET_IPI_STAGECODE_DESCRIPTION(qms065.StageCode,ltrim(rtrim(qms065.BU)),ltrim(rtrim(qms065.Location))))",
                                                "StageSequence",
                                                "IterationNo",
                                                "NDETechniqueNo",
                                                "NDETechniqueRevisionNo",
                                                "ManufacturingCode",
                                                "qms065.RequestGeneratedBy +' - '+com003o.t_name",
                                                "qms065.ConfirmedBy +' - '+com003cb.t_name",
                                                "qms065.OfferedBy +' - '+com003ob.t_name",
                                                "qms065.ReturnedBy +' - '+com003rb.t_name",
                                                "qms065.RejectedBy +' - '+com003rjb.t_name"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms065.BU", "qms065.Location");
                if (!string.IsNullOrEmpty(qualityProject) && !string.IsNullOrEmpty(partNumber))
                {
                    strWhere += (partNumber.Equals("ALL") ? " AND QualityProject='" + qualityProject + "' " : " AND QualityProject='" + qualityProject + "' AND PartAssemblyFindNumber='" + partNumber + "' ");
                }
                var lstResult = db.SP_FETCH_IPI_NDE_PARTLIST_OFFERED_RESULT
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.QualityProject),
                               //Convert.ToString(uc.Project),
                               //Convert.ToString(uc.BU),
                               //Convert.ToString(uc.Location),
                               Convert.ToString(uc.PartAssemblyFindNumber),
                               Convert.ToString(uc.PartAssemblyChildItemCode),
                               Convert.ToString(uc.OfferedQuantity),
                                Convert.ToString(uc.StageCodeDesc),
                               Convert.ToString(uc.StageSequence),
                               Convert.ToString(uc.IterationNo),
                                Convert.ToString(uc.RequestNo),
                               Convert.ToString(uc.RequestNoSequence),
                               Convert.ToString(uc.RequestStatus),
                               //Convert.ToString(uc.PartAssemblyChildItemCode),
                               Convert.ToString(uc.TestResult),
                               //Convert.ToString(uc.ManufacturingCode),
                               Convert.ToString(uc.Shop),
                               Convert.ToString(uc.ShopLocation),
                               Convert.ToString(uc.ShopRemark),
                               //Convert.ToString(uc.NDETechniqueNo),
                               //Convert.ToString(uc.NDETechniqueRevisionNo),
                               //Convert.ToString(uc.CreatedBy),
                               //Convert.ToString(uc.CreatedOn),
                               Convert.ToString(uc.OfferedBy),
                               Convert.ToString(uc.OfferedOn),
                               //Convert.ToString(uc.RequestGeneratedBy),
                               //Convert.ToString(uc.RequestGeneratedOn),
                               //Convert.ToString(uc.ConfirmedBy),
                               //Convert.ToString(uc.ConfirmedOn),
                               //Convert.ToString(uc.ReturnedBy),
                               //Convert.ToString(uc.ReturnedOn),
                               //Convert.ToString(uc.RejectedBy),
                               //Convert.ToString(uc.RejectedOn),
                               (param.CTQCompileStatus.ToUpper() == "RETURNNDEREQUEST" ? "<center style=\"white-space: nowrap;\" class=\"clsDisplayInlineEle\"><a   href=\"" + WebsiteURL + "/IPI/PartlistOfferInspection/ReturnRequestDetails/"+uc.RequestId +"/?from=returnnderequest\" Title=\"View Details\" ><i style=\"margin - left:5px;\" class=\"fa fa-eye\"></i></a></center>"   : "<center style=\"white-space:nowrap;\"><a title=\"View Details\" class='' href='" + WebsiteURL + "/IPI/PartlistNDEInspection/ViewOfferDetails?RequestId="+Convert.ToInt32(uc.RequestId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a>"+Manager.generateIPIGridButtons(uc.QualityProject,uc.Project,uc.BU,uc.Location,uc.PartAssemblyFindNumber,"",uc.PartAssemblyChildItemCode,uc.StageCode,Convert.ToString(uc.StageSequence),"Part Details",65,"","Part Details","Part Request Details")+"&nbsp"+((string.IsNullOrWhiteSpace(uc.RequestGeneratedBy) && uc.QCIsReturned == null )?"<i style=\"cursor:pointer;margin-left:10px;\" title=\"Generate Request\" onclick=\"GenerateRequest("+uc.RequestId+")\" class=\"fa fa-external-link\" aria-hidden=\"true\"></i>":"<i style=\"cursor:pointer;margin-left:10px;opacity:0.3;\" title=\"Generate Request\"  class=\"fa fa-external-link\" aria-hidden=\"true\"></i>")+"&nbsp;&nbsp;<a  href=\"javascript: void(0);\" Title=\"View Timeline\" onclick=\"ShowTimeline("+ uc.RequestId +")\"><i style=\"margin - left:5px;\" class=\"fa fa-clock-o\"></i></a></center>")
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult ViewOfferDetails(int RequestId = 0)
        {
            if (RequestId > 0)
            {
                QMS065 objQMS065 = db.QMS065.Where(x => x.RequestId == RequestId).FirstOrDefault();
                if (objQMS065 != null)
                {
                    var listQMS065 = JsonConvert.SerializeObject(objQMS065, Formatting.None,
                                                            new JsonSerializerSettings()
                                                            {
                                                                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                                                            });
                    ViewBag.QMS065 = listQMS065;

                    return View(objQMS065);
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
            }

        }

        [HttpPost]
        public ActionResult LoadPartlistNDEOfferInspection(string strModel)
        {
            QMS065 qms065 = JsonConvert.DeserializeObject<QMS065>(strModel);

            string stageCodeDesc = db.Database.SqlQuery<string>("select  dbo.GET_IPI_STAGECODE_DESCRIPTION('" + qms065.StageCode + "','" + qms065.BU + "','" + qms065.Location + "')").FirstOrDefault();
            qms065.StageCode = stageCodeDesc;
            ViewBag.IsLTFPS = Manager.IsLTFPSProject(qms065.Project, qms065.BU, qms065.Location);

            var projectDetails = db.COM001.Where(x => x.t_cprj == qms065.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
            ViewBag.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
            ViewBag.BU = qms065.BU;
            ViewBag.Location = qms065.Location;
            qms065.OfferedBy = Manager.GetPsidandDescription(qms065.OfferedBy);
            qms065.RequestGeneratedBy = Manager.GetPsidandDescription(qms065.RequestGeneratedBy);
            qms065.ReturnedBy = Manager.GetPsidandDescription(qms065.ReturnedBy);
            qms065.ConfirmedBy = Manager.GetPsidandDescription(qms065.ConfirmedBy);
            qms065.RejectedBy = Manager.GetPsidandDescription(qms065.RejectedBy);
            List<GLB002> lstCategory = Manager.GetSubCatagories("TPI Agency", qms065.BU, qms065.Location, null);
            List<GLB002> lstCategoryTPI = Manager.GetSubCatagories("TPI Intervention", qms065.BU, qms065.Location, null);
            qms065.TPIAgency1 = GetCategoryDescriptionFromList(lstCategory, qms065.TPIAgency1);
            qms065.TPIAgency2 = GetCategoryDescriptionFromList(lstCategory, qms065.TPIAgency2);
            qms065.TPIAgency3 = GetCategoryDescriptionFromList(lstCategory, qms065.TPIAgency3);
            qms065.TPIAgency4 = GetCategoryDescriptionFromList(lstCategory, qms065.TPIAgency4);
            qms065.TPIAgency5 = GetCategoryDescriptionFromList(lstCategory, qms065.TPIAgency5);
            qms065.TPIAgency6 = GetCategoryDescriptionFromList(lstCategory, qms065.TPIAgency6);
            qms065.TPIAgency1Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms065.TPIAgency1Intervention);
            qms065.TPIAgency2Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms065.TPIAgency2Intervention);
            qms065.TPIAgency3Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms065.TPIAgency3Intervention);
            qms065.TPIAgency4Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms065.TPIAgency4Intervention);
            qms065.TPIAgency5Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms065.TPIAgency5Intervention);
            qms065.TPIAgency6Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms065.TPIAgency6Intervention);
            var BUDescription = (from a in db.COM002
                                 where a.t_dtyp == 2 && a.t_dimx == qms065.BU
                                 select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
            qms065.BU = Convert.ToString(BUDescription.BUDesc);

            var locDescription = (from a in db.COM002
                                  where a.t_dtyp == 1 && a.t_dimx == qms065.Location
                                  select new { Location = a.t_dimx + "-" + a.t_desc }).FirstOrDefault();
            qms065.Location = locDescription.Location;

            ViewBag.IsQCReturned = (qms065.QCIsReturned == true ? "Yes" : "No");

            return PartialView("_LoadPartlistNDEOfferInspection", qms065);
        }

        public string GetCategoryDescriptionFromList(List<GLB002> lst, string code)
        {
            if (!string.IsNullOrWhiteSpace(code))
                return lst.Where(x => x.Code == code).Select(s => s.Code + " - " + s.Description).FirstOrDefault();
            else
                return null;
        }

        [HttpPost]
        public ActionResult QCReturnRequest(int requestId, string ReturnRemarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (!(new SeamListNDEInspectionController()).IsApplicableForNDERequestGenerate(requestId, clsImplementationEnum.InspectionFor.PART.GetStringValue()))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "This request has been already attended";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                QMS065 objQMS065 = db.QMS065.Where(x => x.RequestId == requestId).FirstOrDefault();

                objQMS065.QCIsReturned = true;
                objQMS065.QCReturnedRemarks = ReturnRemarks;
                objQMS065.QCReturnedBy = objClsLoginInfo.UserName;
                objQMS065.QCReturnedOn = DateTime.Now;
                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Request has been returned successfully"; ;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GenerateRequest(QMS065 qms065, string AutoUserPSNo = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (!(new SeamListNDEInspectionController()).IsApplicableForNDERequestGenerate(qms065.RequestId, clsImplementationEnum.InspectionFor.PART.GetStringValue()))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "This request has been already attended";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                if (string.IsNullOrWhiteSpace(AutoUserPSNo))
                    AutoUserPSNo = objClsLoginInfo.UserName;

                QMS065 objQMS065 = db.QMS065.Where(x => x.RequestId == qms065.RequestId).FirstOrDefault();
                if (objQMS065 != null)
                {
                    objQMS065.RequestGeneratedBy = AutoUserPSNo;
                    objQMS065.RequestGeneratedOn = DateTime.Now;
                    bool isLTFPS = Manager.IsLTFPSProject(objQMS065.Project, objQMS065.BU, objQMS065.Location);
                    bool isConfirmationRequired = Manager.IsNDERequestConfirmationRequiredOnFirstInteration(objQMS065.Project, objQMS065.BU, objQMS065.Location);

                    if (!isConfirmationRequired && objQMS065.IterationNo == 1)
                    {
                        objQMS065.ConfirmedBy = Manager.GetSystemUserPSNo();
                        objQMS065.ConfirmedOn = DateTime.Now;
                        if (isLTFPS)
                        {
                            objQMS065.LTF002.InspectionStatus = clsImplementationEnum.SeamListInspectionStatus.OfferedToNDE.GetStringValue();
                        }
                        else
                        {
                            objQMS065.QMS045.InspectionStatus = clsImplementationEnum.SeamListInspectionStatus.OfferedToNDE.GetStringValue();
                        }
                        db.SaveChanges();

                        #region Send Notification
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE3.GetStringValue(), objQMS065.Project, objQMS065.BU, objQMS065.Location, "NDT Request " + objQMS065.RequestNo + " for Stage: " + objQMS065.StageCode + " of Part: " + objQMS065.PartAssemblyFindNumber + " & Project No: " + objQMS065.QualityProject + " has been confirmed and ready for your further action", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/PartlistNDEInspection/NDEViewDetails?RequestId=" + objQMS065.RequestId);
                        #endregion
                    }
                    else
                    {
                        if (isLTFPS)
                        {
                            objQMS065.LTF002.InspectionStatus = clsImplementationEnum.SeamListInspectionStatus.PendingForConfirmation.GetStringValue();
                        }
                        else
                        {
                            objQMS065.QMS045.InspectionStatus = clsImplementationEnum.SeamListInspectionStatus.PendingForConfirmation.GetStringValue();
                        }
                        db.SaveChanges();

                        #region Send Notification
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PROD3.GetStringValue(), objQMS065.Project, objQMS065.BU, objQMS065.Location, "NDT Request " + objQMS065.RequestNo + " for Stage: " + objQMS065.StageCode + " of Part: " + objQMS065.PartAssemblyFindNumber + " & Project No: " + objQMS065.QualityProject + " has been submitted for your Confirmation", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/PartlistNDEInspection/ShopOfferDetails?RequestId=" + objQMS065.RequestId);
                        #endregion
                    }




                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Request Number: " + objQMS065.RequestNo + " has been generated";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "NDE offer not available.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult ShopView()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadPartlistNDEShopOfferDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_LoadPartlistNDEShopOfferDataPartial");
        }

        [HttpPost]
        public JsonResult LoadPartlistShopNDEOfferData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;

                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and (qms065.RequestGeneratedBy != '' or qms065.RequestGeneratedBy is not null) and (qms065.ConfirmedBy in('') or qms065.ConfirmedBy is null) ";
                }
                else
                {
                    strWhere += "1=1 ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                //"qms065.Project+' - '+com1.t_dsca",
                                                "QualityProject",
                                                "Shop",
                                                "ShopLocation",
                                                "ShopRemark",
                                                //"qms065.Project+' - '+com1.t_dsca",
                                                //"ltrim(rtrim(qms065.BU))+' - '+bcom2.t_desc",
                                                //"ltrim(rtrim(qms065.Location))+' - '+lcom2.t_desc",
                                                "PartAssemblyFindNumber",
                                                "PartAssemblyChildItemCode",
                                                "TestResult",
                                                "qms065.CreatedBy +' - '+com003c.t_name",
                                                "(dbo.GET_IPI_STAGECODE_DESCRIPTION(qms065.StageCode,ltrim(rtrim(qms065.BU)),ltrim(rtrim(qms065.Location))))",
                                                "StageSequence",
                                                "IterationNo",
                                                "NDETechniqueNo",
                                                "NDETechniqueRevisionNo",
                                                "ManufacturingCode",
                                                "qms065.RequestGeneratedBy +' - '+com003o.t_name",
                                                "qms065.ConfirmedBy +' - '+com003cb.t_name",
                                                "qms065.OfferedBy +' - '+com003ob.t_name",
                                                "qms065.ReturnedBy +' - '+com003rb.t_name",
                                                "qms065.RejectedBy +'' - ''+com003rjb.t_name",
                                                "RequestNo",
                                                "RequestStatus",
                                                "RequestNoSequence"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms065.BU", "qms065.Location");
                var lstResult = db.SP_FETCH_IPI_NDE_PARTLIST_OFFERED_RESULT
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                              Convert.ToString(uc.QualityProject),
                               //Convert.ToString(uc.Project),
                               //Convert.ToString(uc.BU),
                               //Convert.ToString(uc.Location),
                               Convert.ToString(uc.PartAssemblyFindNumber),
                               Convert.ToString(uc.PartAssemblyChildItemCode),
                               Convert.ToString(uc.OfferedQuantity),
                                Convert.ToString(uc.StageCodeDesc),
                               Convert.ToString(uc.StageSequence),
                               Convert.ToString(uc.IterationNo),
                                Convert.ToString(uc.RequestNo),
                               Convert.ToString(uc.RequestNoSequence),
                               Convert.ToString(uc.RequestStatus),
                               //Convert.ToString(uc.PartAssemblyChildItemCode),
                               Convert.ToString(uc.TestResult),
                               //Convert.ToString(uc.ManufacturingCode),
                               Convert.ToString(uc.Shop),
                               Convert.ToString(uc.ShopLocation),
                               Convert.ToString(uc.ShopRemark),
                               //Convert.ToString(uc.NDETechniqueNo),
                               //Convert.ToString(uc.NDETechniqueRevisionNo),
                               //Convert.ToString(uc.CreatedBy),
                               //Convert.ToString(uc.CreatedOn),
                               Convert.ToString(uc.OfferedBy),
                               Convert.ToString(uc.OfferedOn),
                               Convert.ToString(uc.RequestGeneratedBy),
                               Convert.ToString(uc.RequestGeneratedOn),
                               //Convert.ToString(uc.ConfirmedBy),
                               //Convert.ToString(uc.ConfirmedOn),
                               //Convert.ToString(uc.ReturnedBy),
                               //Convert.ToString(uc.ReturnedOn),
                               //Convert.ToString(uc.RejectedBy),
                               //Convert.ToString(uc.RejectedOn),
                               "<center style=\"white-space:nowrap;\"><a title=\"View Details\" class='' href='" + WebsiteURL + "/IPI/PartlistNDEInspection/ShopOfferDetails?RequestId="+Convert.ToInt32(uc.RequestId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a>"+Manager.generateIPIGridButtons(uc.QualityProject,uc.Project,uc.BU,uc.Location,uc.PartAssemblyFindNumber,"",uc.PartAssemblyChildItemCode,uc.StageCode,Convert.ToString(uc.StageSequence),"Part Details",65,"","Part Details","Part Request Details")+"&nbsp"+((string.IsNullOrWhiteSpace(uc.ConfirmedBy) && !string.IsNullOrWhiteSpace(uc.RequestGeneratedBy))?"<i style=\"cursor:pointer;margin-left:10px;\" title=\"Confirm Request\" onclick=\"ConfirmRequest("+uc.RequestId+")\" class=\"fa fa-check-square-o\" aria-hidden=\"true\"></i>":"<i style=\"cursor:pointer;margin-left:10px;opacity: 0.3;\" title=\"Confirm Request\"  class=\"fa fa-check-square-o\" aria-hidden=\"true\"></i>")+"&nbsp;&nbsp;<a  href=\"javascript: void(0);\" Title=\"View Timeline\" onclick=\"ShowTimeline("+ uc.RequestId +")\"><i style=\"margin - left:5px;\" class=\"fa fa-clock-o\"></i></a></center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult ShopOfferDetails(int RequestId = 0, string from = "")
        {
            ViewBag.fromURL = from;
            if (RequestId > 0)
            {
                QMS065 objQMS065 = db.QMS065.Where(x => x.RequestId == RequestId).FirstOrDefault();
                if (objQMS065 != null)
                {
                    var listQMS065 = JsonConvert.SerializeObject(objQMS065, Formatting.None,
                                                            new JsonSerializerSettings()
                                                            {
                                                                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                                                            });
                    ViewBag.QMS065 = listQMS065;
                    return View(objQMS065);
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
            }

        }
        [HttpPost]
        public ActionResult LoadPartlistShopNDEOfferInspection(string strModel, string UOM = "")
        {
            QMS065 qms065 = JsonConvert.DeserializeObject<QMS065>(strModel);

            string stageCodeDesc = db.Database.SqlQuery<string>("select  dbo.GET_IPI_STAGECODE_DESCRIPTION('" + qms065.StageCode + "','" + qms065.BU + "','" + qms065.Location + "')").FirstOrDefault();
            qms065.StageCode = stageCodeDesc;

            ViewBag.IsLTFPS = Manager.IsLTFPSProject(qms065.Project, qms065.BU, qms065.Location);

            var projectDetails = db.COM001.Where(x => x.t_cprj == qms065.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
            ViewBag.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
            ViewBag.BU = qms065.BU;
            ViewBag.Location = qms065.Location;
            List<GLB002> lstCategory = Manager.GetSubCatagories("TPI Agency", qms065.BU, qms065.Location, null);
            List<GLB002> lstCategoryTPI = Manager.GetSubCatagories("TPI Intervention", qms065.BU, qms065.Location, null);
            qms065.TPIAgency1 = GetCategoryDescriptionFromList(lstCategory, qms065.TPIAgency1);
            qms065.TPIAgency2 = GetCategoryDescriptionFromList(lstCategory, qms065.TPIAgency2);
            qms065.TPIAgency3 = GetCategoryDescriptionFromList(lstCategory, qms065.TPIAgency3);
            qms065.TPIAgency4 = GetCategoryDescriptionFromList(lstCategory, qms065.TPIAgency4);
            qms065.TPIAgency5 = GetCategoryDescriptionFromList(lstCategory, qms065.TPIAgency5);
            qms065.TPIAgency6 = GetCategoryDescriptionFromList(lstCategory, qms065.TPIAgency6);
            qms065.TPIAgency1Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms065.TPIAgency1Intervention);
            qms065.TPIAgency2Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms065.TPIAgency2Intervention);
            qms065.TPIAgency3Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms065.TPIAgency3Intervention);
            qms065.TPIAgency4Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms065.TPIAgency4Intervention);
            qms065.TPIAgency5Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms065.TPIAgency5Intervention);
            qms065.TPIAgency6Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms065.TPIAgency6Intervention);

            qms065.OfferedBy = Manager.GetPsidandDescription(qms065.OfferedBy);
            qms065.RequestGeneratedBy = Manager.GetPsidandDescription(qms065.RequestGeneratedBy);
            qms065.ReturnedBy = Manager.GetPsidandDescription(qms065.ReturnedBy);
            qms065.ConfirmedBy = Manager.GetPsidandDescription(qms065.ConfirmedBy);
            qms065.RejectedBy = Manager.GetPsidandDescription(qms065.RejectedBy);
            var BUDescription = (from a in db.COM002
                                 where a.t_dtyp == 2 && a.t_dimx == qms065.BU
                                 select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
            qms065.BU = Convert.ToString(BUDescription.BUDesc);

            var locDescription = (from a in db.COM002
                                  where a.t_dtyp == 1 && a.t_dimx == qms065.Location
                                  select new { Location = a.t_dimx + "-" + a.t_desc }).FirstOrDefault();

            ViewBag.fromURL = Convert.ToString(UOM);  // used UOM for from URL
            return PartialView("_LoadPartlistShopNDEOfferInspection", qms065);
        }

        [HttpPost]
        public ActionResult RequestConfirm(QMS065 qms065)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (!(new SeamListNDEInspectionController()).IsApplicableForNDERequestConfirmation(qms065.RequestId, clsImplementationEnum.InspectionFor.PART.GetStringValue()))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "This request has been already confirmed";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                QMS065 objQMS065 = db.QMS065.Where(x => x.RequestId == qms065.RequestId).FirstOrDefault();
                if (objQMS065 != null)
                {
                    objQMS065.ConfirmedBy = objClsLoginInfo.UserName;
                    objQMS065.ConfirmedOn = DateTime.Now;
                    bool isLTFPS = Manager.IsLTFPSProject(objQMS065.Project, objQMS065.BU, objQMS065.Location);
                    if (isLTFPS)
                    {
                        objQMS065.LTF002.InspectionStatus = clsImplementationEnum.SeamListInspectionStatus.OfferedToNDE.GetStringValue();
                    }
                    else
                    {
                        objQMS065.QMS045.InspectionStatus = clsImplementationEnum.SeamListInspectionStatus.OfferedToNDE.GetStringValue();
                    }
                    db.SaveChanges();

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE3.GetStringValue(), objQMS065.Project, objQMS065.BU, objQMS065.Location, "NDT Request " + objQMS065.RequestNo + " for Stage: " + objQMS065.StageCode + " of Part: " + objQMS065.PartAssemblyFindNumber + " & Project No: " + objQMS065.QualityProject + " has been confirmed and ready for your further action", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/PartlistNDEInspection/NDEViewDetails?RequestId=" + objQMS065.RequestId);
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Request confirmed Successfully";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "NDE offer not available.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult NDEView()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadPartlistNDEDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_LoadPartlistNDEDataPartial");
        }

        [HttpPost]
        public JsonResult LoadPartlistNDEData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;

                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and (qms065.RequestGeneratedBy != '' or qms065.RequestGeneratedBy is not null) and (qms065.ConfirmedBy != '' or qms065.ConfirmedBy is not null) and (qms065.TestResult is null or qms065.TestResult = '') ";
                }
                else
                {
                    strWhere += "1=1 ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                //"qms065.Project+' - '+com1.t_dsca",
                                                "QualityProject",
                                                "Shop",
                                                "ShopLocation",
                                                "ShopRemark",
                                                //"qms065.Project+' - '+com1.t_dsca",
                                                //"ltrim(rtrim(qms065.BU))+' - '+bcom2.t_desc",
                                                //"ltrim(rtrim(qms065.Location))+' - '+lcom2.t_desc",
                                                "PartAssemblyFindNumber",
                                                "PartAssemblyChildItemCode",
                                                "TestResult",
                                                "qms065.CreatedBy +' - '+com003c.t_name",
                                                "(dbo.GET_IPI_STAGECODE_DESCRIPTION(qms065.StageCode,ltrim(rtrim(qms065.BU)),ltrim(rtrim(qms065.Location))))",
                                                "StageSequence",
                                                "IterationNo",
                                                "NDETechniqueNo",
                                                "NDETechniqueRevisionNo",
                                                "ManufacturingCode",
                                                "qms065.RequestGeneratedBy +' - '+com003o.t_name",
                                                "qms065.ConfirmedBy +' - '+com003cb.t_name",
                                                "qms065.OfferedBy +' - '+com003ob.t_name",
                                                "qms065.ReturnedBy +' - '+com003rb.t_name",
                                                "qms065.RejectedBy +'' - ''+com003rjb.t_name",
                                                "RequestNo",
                                                "RequestStatus",
                                                "RequestNoSequence"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms065.BU", "qms065.Location");
                var lstResult = db.SP_FETCH_IPI_NDE_PARTLIST_OFFERED_RESULT
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.QualityProject),
                               //Convert.ToString(uc.Project),
                               //Convert.ToString(uc.BU),
                               //Convert.ToString(uc.Location),
                               Convert.ToString(uc.PartAssemblyFindNumber),
                               Convert.ToString(uc.PartAssemblyChildItemCode),
                               Convert.ToString(uc.OfferedQuantity),
                                Convert.ToString(uc.StageCodeDesc),
                               Convert.ToString(uc.StageSequence),
                               Convert.ToString(uc.IterationNo),
                                Convert.ToString(uc.RequestNo),
                               Convert.ToString(uc.RequestNoSequence),
                               Convert.ToString(uc.RequestStatus),
                               //Convert.ToString(uc.PartAssemblyChildItemCode),
                               Convert.ToString(uc.TestResult),
                               //Convert.ToString(uc.ManufacturingCode),
                               Convert.ToString(uc.Shop),
                               Convert.ToString(uc.ShopLocation),
                               Convert.ToString(uc.ShopRemark),
                               //Convert.ToString(uc.NDETechniqueNo),
                               //Convert.ToString(uc.NDETechniqueRevisionNo),
                               //Convert.ToString(uc.CreatedBy),
                               //Convert.ToString(uc.CreatedOn),
                               Convert.ToString(uc.OfferedBy),
                               Convert.ToString(uc.OfferedOn),
                               Convert.ToString(uc.RequestGeneratedBy),
                               Convert.ToString(uc.RequestGeneratedOn),
                               Convert.ToString(uc.ConfirmedBy),
                               Convert.ToString(uc.ConfirmedOn),
                               //Convert.ToString(uc.ReturnedBy),
                               //Convert.ToString(uc.ReturnedOn),
                               //Convert.ToString(uc.RejectedBy),
                               //Convert.ToString(uc.RejectedOn),
                               "<center style=\"white-space:nowrap;\"><a target=\"_blank\" title=\"View Details\" class='' href='" + WebsiteURL + "/IPI/PartlistNDEInspection/NDEViewDetails?RequestId="+Convert.ToInt32(uc.RequestId)+"'><i class='fa fa-eye'></i></a>"+Manager.generateIPIGridButtons(uc.QualityProject,uc.Project,uc.BU,uc.Location,uc.PartAssemblyFindNumber,"",uc.PartAssemblyChildItemCode,uc.StageCode,Convert.ToString(uc.StageSequence),"Part Details",65,"","Part Details","Part Request Details")+"&nbsp;&nbsp;<a  href=\"javascript: void(0);\" Title=\"View Timeline\" onclick=\"ShowTimeline("+ uc.RequestId +")\"><i style=\"margin - left:5px;\" class=\"fa fa-clock-o\"></i></a></center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult NDEViewDetails(int RequestId = 0)
        {
            if (RequestId > 0)
            {
                QMS065 objQMS065 = db.QMS065.Where(x => x.RequestId == RequestId).FirstOrDefault();
                if (objQMS065 != null)
                {
                    var listQMS065 = JsonConvert.SerializeObject(objQMS065, Formatting.None,
                                                            new JsonSerializerSettings()
                                                            {
                                                                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                                                            });
                    ViewBag.QMS065 = listQMS065;
                    return View(objQMS065);
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
            }

        }

        [HttpPost]
        public ActionResult LoadPartlistNDEInspection(string strModel)
        {
            QMS065 qms065 = JsonConvert.DeserializeObject<QMS065>(strModel);

            ViewBag.StageCode = qms065.StageCode;
            string stageCodeDesc = db.Database.SqlQuery<string>("select  dbo.GET_IPI_STAGECODE_DESCRIPTION('" + qms065.StageCode + "','" + qms065.BU + "','" + qms065.Location + "')").FirstOrDefault();
            qms065.StageCode = stageCodeDesc;
            ViewBag.IsLTFPS = Manager.IsLTFPSProject(qms065.Project, qms065.BU, qms065.Location);
            var projectDetails = db.COM001.Where(x => x.t_cprj == qms065.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
            ViewBag.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
            ViewBag.BU = qms065.BU;
            ViewBag.Location = qms065.Location;


            if (!string.IsNullOrWhiteSpace(qms065.NDETechniqueNo))
            {
                string techName = qms065.NDETechniqueNo.Split('/')[1].ToString();
                string strURL = string.Empty;
                switch (techName.ToUpper())
                {
                    case "ASCAN":
                        QMS090 objQMS090 = db.QMS090.Where(x => x.RefHeaderId == qms065.RequestId).FirstOrDefault();
                        strURL = WebsiteURL + "/IPI/PartlistNDEInspection/AscanUTViewDetails?HeaderId=" + Convert.ToInt32(objQMS090.HeaderId) + "";
                        ViewBag.strURL = strURL;
                        break;
                    case "TOFD":
                        QMS092 objQMS092 = db.QMS092.Where(x => x.RefHeaderId == qms065.RequestId).FirstOrDefault();
                        strURL = WebsiteURL + "/IPI/PartlistNDEInspection/TOFDUTViewDetails?HeaderId=" + Convert.ToInt32(objQMS092.HeaderId) + "";
                        ViewBag.strURL = strURL;
                        break;
                    case "PAUT":
                        QMS094 objQMS094 = db.QMS094.Where(x => x.RefHeaderId == qms065.RequestId).FirstOrDefault();
                        strURL = WebsiteURL + "/IPI/PartlistNDEInspection/PAUTViewDetails?HeaderId=" + Convert.ToInt32(objQMS094.HeaderId) + "";
                        ViewBag.strURL = strURL;
                        break;
                    default:
                        break;
                }
            }

            List<GLB002> lstCategory = Manager.GetSubCatagories("TPI Agency", qms065.BU, qms065.Location, null);
            List<GLB002> lstCategoryTPI = Manager.GetSubCatagories("TPI Intervention", qms065.BU, qms065.Location, null);
            qms065.TPIAgency1 = GetCategoryDescriptionFromList(lstCategory, qms065.TPIAgency1);
            qms065.TPIAgency2 = GetCategoryDescriptionFromList(lstCategory, qms065.TPIAgency2);
            qms065.TPIAgency3 = GetCategoryDescriptionFromList(lstCategory, qms065.TPIAgency3);
            qms065.TPIAgency4 = GetCategoryDescriptionFromList(lstCategory, qms065.TPIAgency4);
            qms065.TPIAgency5 = GetCategoryDescriptionFromList(lstCategory, qms065.TPIAgency5);
            qms065.TPIAgency6 = GetCategoryDescriptionFromList(lstCategory, qms065.TPIAgency6);
            qms065.TPIAgency1Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms065.TPIAgency1Intervention);
            qms065.TPIAgency2Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms065.TPIAgency2Intervention);
            qms065.TPIAgency3Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms065.TPIAgency3Intervention);
            qms065.TPIAgency4Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms065.TPIAgency4Intervention);
            qms065.TPIAgency5Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms065.TPIAgency5Intervention);
            qms065.TPIAgency6Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, qms065.TPIAgency6Intervention);

            qms065.OfferedBy = Manager.GetPsidandDescription(qms065.OfferedBy);
            qms065.RequestGeneratedBy = Manager.GetPsidandDescription(qms065.RequestGeneratedBy);
            qms065.ReturnedBy = Manager.GetPsidandDescription(qms065.ReturnedBy);
            qms065.ConfirmedBy = Manager.GetPsidandDescription(qms065.ConfirmedBy);
            qms065.RejectedBy = Manager.GetPsidandDescription(qms065.RejectedBy);
            var BUDescription = (from a in db.COM002
                                 where a.t_dtyp == 2 && a.t_dimx == qms065.BU
                                 select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
            qms065.BU = Convert.ToString(BUDescription.BUDesc);

            var locDescription = (from a in db.COM002
                                  where a.t_dtyp == 1 && a.t_dimx == qms065.Location
                                  select new { Location = a.t_dimx + "-" + a.t_desc }).FirstOrDefault();
            qms065.Location = Convert.ToString(locDescription.Location);

            if (Convert.ToBoolean(ViewBag.IsLTFPS))
            {
                ViewBag.InspectionStatus = qms065.LTF002.InspectionStatus;
                int LTFPSHeaderId = Convert.ToInt32(qms065.LTFPSHeaderId);
                ViewBag.DocumentRequired = qms065.LTF002.DocumentRequired;
                LTF002 objLTF002 = db.LTF002.Where(x => x.LineId == LTFPSHeaderId).FirstOrDefault();
                if (objLTF002 != null)
                {
                    if (!string.IsNullOrWhiteSpace(objLTF002.RefDocument) && (objLTF002.DocumentRequired))
                    {
                        ViewBag.IsRevAdd = true;
                    }
                    else
                    {
                        ViewBag.IsRevAdd = false;
                    }
                }
                else
                {
                    ViewBag.IsRevAdd = false;
                }
            }
            else
            {
                ViewBag.DocumentRequired = false;
                ViewBag.InspectionStatus = qms065.QMS045.InspectionStatus;
            }
            return PartialView("_LoadPartlistNDEInspection", qms065);
        }

        [HttpPost]
        public ActionResult GetNDETechniques(string project, string qualityProject, string BU, string Location, string stageCode)
        {
            ViewBag.Project = project;
            ViewBag.QualityProject = qualityProject;
            ViewBag.BU = BU;
            ViewBag.Location = Location;
            ViewBag.StageCode = stageCode;
            return PartialView("_GetNDETechniques");
        }

        [HttpPost]
        public ActionResult GetNDEAutoTechniques(string term, string project, string qualityProject, string BU, string Location, string stageCode)
        {
            var lstTechniques = Manager.getNDETechniques(project, qualityProject, stageCode, BU, Location);
            return Json(lstTechniques, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveNDETechnique(QMS065 qms065, string TechName, bool IsSeam = false)
        {
            TechniqueResponseMsg objResponseMsg = new TechniqueResponseMsg();
            try
            {
                string strURL = string.Empty;

                if (IsSeam)
                {
                    #region SEAM
                    QMS060 objQMS060 = db.QMS060.Where(x => x.RequestId == qms065.RequestId).FirstOrDefault();
                    string oldTechNo = objQMS060.NDETechniqueNo;
                    int? oldRev = objQMS060.NDETechniqueRevisionNo;
                    if (objQMS060 != null)
                    {
                        objQMS060.NDETechniqueNo = qms065.NDETechniqueNo;
                        objQMS060.NDETechniqueRevisionNo = qms065.NDETechniqueRevisionNo;
                        string strApproveStatus = clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue();
                        switch (TechName.ToUpper())
                        {
                            case "PT":
                                break;
                            case "MT":
                                break;
                            case "RT":
                                break;
                            case "ASCAN UT":
                                if (!string.IsNullOrWhiteSpace(oldTechNo))
                                {
                                    //QMS080 objDelQMS080 = db.QMS080.Where(x => x.UTTechNo == oldTechNo && x.RevNo == oldRev).FirstOrDefault(); 
                                    QMS080 objDelQMS080 = db.QMS080.Where(x => x.RefHeaderId == qms065.RequestId).FirstOrDefault();
                                    if (objDelQMS080 != null)
                                    {
                                        List<QMS081> lstDelQMS081 = db.QMS081.Where(x => x.HeaderId == objDelQMS080.HeaderId).ToList();
                                        if (lstDelQMS081 != null && lstDelQMS081.Count > 0)
                                        {
                                            db.QMS081.RemoveRange(lstDelQMS081);
                                        }
                                        db.QMS080.Remove(objDelQMS080);
                                        db.SaveChanges();
                                    }
                                }

                                #region ASCAN UT

                                NDE010_Log objNDE010_Log = db.NDE010_Log.Where(x => x.UTTechNo == qms065.NDETechniqueNo && x.RevNo == qms065.NDETechniqueRevisionNo && x.Status == strApproveStatus && x.QualityProject == objQMS060.QualityProject && x.Location == objQMS060.Location && x.BU == objQMS060.BU).FirstOrDefault();
                                if (objNDE010_Log != null)
                                {
                                    QMS080 objQMS080 = db.QMS080.Add(new QMS080
                                    {
                                        RefHeaderId = objQMS060.RequestId,
                                        QualityProject = objQMS060.QualityProject,
                                        Project = objQMS060.Project,
                                        BU = objQMS060.BU,
                                        Location = objQMS060.Location,
                                        SeamNo = objQMS060.SeamNo,
                                        StageCode = objQMS060.StageCode,
                                        StageSequence = objQMS060.StageSequence,
                                        IterationNo = objQMS060.IterationNo,
                                        RequestNo = objQMS060.RequestNo,
                                        RequestNoSequence = objQMS060.RequestNoSequence,
                                        UTTechNo = objQMS060.NDETechniqueNo,
                                        RevNo = objQMS060.NDETechniqueRevisionNo,
                                        Status = objNDE010_Log.Status,
                                        UTProcedureNo = objNDE010_Log.UTProcedureNo,
                                        RecordableIndication = objNDE010_Log.RecordableIndication,
                                        ScanTechnique = objNDE010_Log.ScanTechnique,
                                        DGSDiagram = Convert.ToBoolean(objNDE010_Log.DGSDiagram),
                                        BasicCalibrationBlock = objNDE010_Log.BasicCalibrationBlock,
                                        SimulationBlock1 = objNDE010_Log.SimulationBlock1,
                                        SimulationBlock2 = objNDE010_Log.SimulationBlock2,
                                        CouplantUsed = objNDE010_Log.CouplantUsed,
                                        ScanPlan = objNDE010_Log.ScanPlan,
                                        ScanningSensitivity = objNDE010_Log.ScanningSensitivity,
                                        PlannerRemarks = objNDE010_Log.PlannerRemarks,
                                        ApproverRemarks = objNDE010_Log.ApproverRemarks,
                                        CreatedBy = objClsLoginInfo.UserName,
                                        CreatedOn = DateTime.Now,
                                    });
                                    db.SaveChanges();
                                    List<NDE011_Log> lstNDE011_Log = db.NDE011_Log.Where(x => x.RefId == objNDE010_Log.Id).ToList();
                                    if (lstNDE011_Log != null && lstNDE011_Log.Count > 0)
                                    {
                                        List<QMS081> lstQMS081 = new List<QMS081>();
                                        foreach (NDE011_Log objNDE011_Log in lstNDE011_Log)
                                        {
                                            QMS081 objQMS081 = new QMS081();
                                            objQMS081.HeaderId = objQMS080.HeaderId;
                                            objQMS081.RefHeaderId = objQMS080.RefHeaderId;
                                            objQMS081.QualityProject = objQMS080.QualityProject;
                                            objQMS081.Project = objQMS080.Project;
                                            objQMS081.BU = objQMS080.BU;
                                            objQMS081.Location = objQMS080.Location;
                                            objQMS081.SeamNo = objQMS080.SeamNo;
                                            objQMS081.StageCode = objQMS080.StageCode;
                                            objQMS081.StageSequence = objQMS080.StageSequence;
                                            objQMS081.IterationNo = objQMS080.IterationNo;
                                            objQMS081.RequestNo = objQMS080.RequestNo;
                                            objQMS081.RequestNoSequence = objQMS080.RequestNoSequence;
                                            objQMS081.UTTechNo = objQMS080.UTTechNo;
                                            objQMS081.RevNo = objQMS080.RevNo;
                                            objQMS081.SearchUnit = objNDE011_Log.SearchUnit;
                                            objQMS081.ReferenceReflector = objNDE011_Log.ReferenceReflector;
                                            objQMS081.CableType = objNDE011_Log.CableType;
                                            objQMS081.CableLength = objNDE011_Log.CableLength;
                                            objQMS081.CreatedBy = objClsLoginInfo.UserName;
                                            objQMS081.CreatedOn = DateTime.Now;
                                            lstQMS081.Add(objQMS081);
                                        }
                                        if (lstQMS081 != null && lstQMS081.Count > 0)
                                        {
                                            db.QMS081.AddRange(lstQMS081);
                                            db.SaveChanges();
                                        }
                                    }
                                    strURL = WebsiteURL + "/IPI/MaintainNDESeamTechnique/AscanUTHeader?id=" + Convert.ToInt32(objQMS080.HeaderId) + "";
                                }

                                #endregion

                                break;
                            case "TOFD UT":

                                if (!string.IsNullOrWhiteSpace(oldTechNo))
                                {
                                    //QMS082 objDelQMS082 = db.QMS082.Where(x => x.UTTechNo == oldTechNo && x.RevNo == oldRev).FirstOrDefault();
                                    QMS082 objDelQMS082 = db.QMS082.Where(x => x.RefHeaderId == qms065.RequestId).FirstOrDefault();
                                    if (objDelQMS082 != null)
                                    {
                                        List<QMS083> lstDelQMS083 = db.QMS083.Where(x => x.HeaderId == objDelQMS082.HeaderId).ToList();
                                        if (lstDelQMS083 != null && lstDelQMS083.Count > 0)
                                        {
                                            db.QMS083.RemoveRange(lstDelQMS083);
                                        }
                                        db.QMS082.Remove(objDelQMS082);
                                        db.SaveChanges();
                                    }
                                }

                                #region TOFD UT
                                NDE012_Log objNDE012_Log = db.NDE012_Log.Where(x => x.UTTechNo == qms065.NDETechniqueNo && x.RevNo == qms065.NDETechniqueRevisionNo && x.Status == strApproveStatus && x.QualityProject == objQMS060.QualityProject && x.Location == objQMS060.Location && x.BU == objQMS060.BU).FirstOrDefault();
                                if (objNDE012_Log != null)
                                {
                                    QMS082 objQMS082 = db.QMS082.Add(new QMS082
                                    {
                                        RefHeaderId = objQMS060.RequestId,
                                        QualityProject = objQMS060.QualityProject,
                                        Project = objQMS060.Project,
                                        BU = objQMS060.BU,
                                        Location = objQMS060.Location,
                                        SeamNo = objQMS060.SeamNo,
                                        StageCode = objQMS060.StageCode,
                                        StageSequence = objQMS060.StageSequence,
                                        IterationNo = objQMS060.IterationNo,
                                        RequestNo = objQMS060.RequestNo,
                                        RequestNoSequence = objQMS060.RequestNoSequence,
                                        UTTechNo = objQMS060.NDETechniqueNo,
                                        RevNo = objQMS060.NDETechniqueRevisionNo,
                                        Status = objNDE012_Log.Status,
                                        UTProcedureNo = objNDE012_Log.UTProcedureNo,
                                        CouplantUsed = objNDE012_Log.CouplantUsed,
                                        ReferenceReflectorSize = objNDE012_Log.ReferenceReflectorSize,
                                        ReferenceReflectorDistance = objNDE012_Log.ReferenceReflectorDistance,
                                        CalibrationBlockID = objNDE012_Log.CalibrationBlockID,
                                        CalibrationBlockIDThickness = objNDE012_Log.CalibrationBlockIDThickness,
                                        ScanningDetail = objNDE012_Log.ScanningDetail,
                                        EncoderType = objNDE012_Log.EncoderType,
                                        ScannerType = objNDE012_Log.ScannerType,
                                        ScannerMake = objNDE012_Log.ScannerMake,
                                        CableType = objNDE012_Log.CableType,
                                        CableLength = objNDE012_Log.CableLength,
                                        ScanPlan = objNDE012_Log.ScanPlan,
                                        PlannerRemarks = objNDE012_Log.PlannerRemarks,
                                        ApproverRemarks = objNDE012_Log.ApproverRemarks,
                                        CreatedBy = objClsLoginInfo.UserName,
                                        CreatedOn = DateTime.Now

                                    });
                                    db.SaveChanges();
                                    List<NDE013_Log> lstNDE013_Log = db.NDE013_Log.Where(x => x.RefId == objNDE012_Log.Id).ToList();
                                    if (lstNDE013_Log != null && lstNDE013_Log.Count > 0)
                                    {
                                        List<QMS083> lstQMS083 = new List<QMS083>();
                                        foreach (NDE013_Log objNDE013_Log in lstNDE013_Log)
                                        {
                                            QMS083 objQMS083 = new QMS083();
                                            objQMS083.HeaderId = objQMS082.HeaderId;
                                            objQMS083.RefHeaderId = objQMS082.RefHeaderId;
                                            objQMS083.QualityProject = objQMS082.QualityProject;
                                            objQMS083.Project = objQMS082.Project;
                                            objQMS083.BU = objQMS082.BU;
                                            objQMS083.Location = objQMS082.Location;
                                            objQMS083.SeamNo = objQMS082.SeamNo;
                                            objQMS083.StageCode = objQMS082.StageCode;
                                            objQMS083.StageSequence = objQMS082.StageSequence;
                                            objQMS083.IterationNo = objQMS082.IterationNo;
                                            objQMS083.RequestNo = objQMS082.RequestNo;
                                            objQMS083.RequestNoSequence = objQMS082.RequestNoSequence;
                                            objQMS083.UTTechNo = objQMS082.UTTechNo;
                                            objQMS083.RevNo = objQMS082.RevNo;
                                            objQMS083.ScanNo = objNDE013_Log.ScanNo;
                                            objQMS083.RevNo = objNDE013_Log.RevNo;
                                            objQMS083.SearchUnit = objNDE013_Log.SearchUnit;
                                            objQMS083.ScanType = objNDE013_Log.ScanType;
                                            objQMS083.PCS = objNDE013_Log.PCS;
                                            objQMS083.CreatedBy = objClsLoginInfo.UserName;
                                            objQMS083.CreatedOn = DateTime.Now;
                                            lstQMS083.Add(objQMS083);
                                        }
                                        if (lstQMS083 != null && lstQMS083.Count > 0)
                                        {
                                            db.QMS083.AddRange(lstQMS083);
                                            db.SaveChanges();
                                        }
                                    }
                                    strURL = WebsiteURL + "/IPI/MaintainNDESeamTechnique/TOFDUTHeader?id=" + Convert.ToInt32(objQMS082.HeaderId) + "";
                                }
                                #endregion

                                break;
                            case "PAUT":

                                //QMS084 objDelQMS084 = db.QMS084.Where(x => x.UTTechNo == oldTechNo && x.RevNo == oldRev).FirstOrDefault();
                                QMS084 objDelQMS084 = db.QMS084.Where(x => x.RefHeaderId == qms065.RequestId).FirstOrDefault();

                                if (objDelQMS084 != null)
                                {
                                    List<QMS085> lstDelQMS085 = db.QMS085.Where(x => x.HeaderId == objDelQMS084.HeaderId).ToList();
                                    if (lstDelQMS085 != null && lstDelQMS085.Count > 0)
                                    {
                                        int[] arrayLineIds = lstDelQMS085.Select(x => x.LineId).ToArray();
                                        if (arrayLineIds.Length > 0)
                                        {
                                            List<QMS086> lstDelQMS086 = db.QMS086.Where(x => arrayLineIds.Contains(x.LineId)).ToList();
                                            if (lstDelQMS086 != null && lstDelQMS086.Count > 0)
                                            {
                                                db.QMS086.RemoveRange(lstDelQMS086);
                                            }
                                        }
                                        db.QMS085.RemoveRange(lstDelQMS085);
                                    }
                                    db.QMS084.Remove(objDelQMS084);
                                    db.SaveChanges();
                                }

                                #region PAUT
                                NDE014_Log objNDE014_Log = db.NDE014_Log.Where(x => x.UTTechNo == qms065.NDETechniqueNo && x.RevNo == qms065.NDETechniqueRevisionNo && x.Status == strApproveStatus && x.QualityProject == objQMS060.QualityProject && x.Location == objQMS060.Location && x.BU == objQMS060.BU).FirstOrDefault();

                                if (objNDE014_Log != null)
                                {
                                    QMS084 objQMS084 = db.QMS084.Add(new QMS084
                                    {
                                        RefHeaderId = objQMS060.RequestId,
                                        QualityProject = objQMS060.QualityProject,
                                        Project = objQMS060.Project,
                                        BU = objQMS060.BU,
                                        Location = objQMS060.Location,
                                        SeamNo = objQMS060.SeamNo,
                                        StageCode = objQMS060.StageCode,
                                        StageSequence = objQMS060.StageSequence,
                                        IterationNo = objQMS060.IterationNo,
                                        RequestNo = objQMS060.RequestNo,
                                        RequestNoSequence = objQMS060.RequestNoSequence,
                                        UTTechNo = objQMS060.NDETechniqueNo,
                                        RevNo = objQMS060.NDETechniqueRevisionNo,
                                        Status = objNDE014_Log.Status,
                                        UTProcedureNo = objNDE014_Log.UTProcedureNo,
                                        CouplantUsed = objNDE014_Log.CouplantUsed,
                                        ReferenceReflectorSize = objNDE014_Log.ReferenceReflectorSize,
                                        ReferenceReflectorDistance = objNDE014_Log.ReferenceReflectorDistance,
                                        CalibrationBlockID = objNDE014_Log.CalibrationBlockID,
                                        CalibrationBlockIDThickness = objNDE014_Log.CalibrationBlockIDThickness,
                                        EncoderType = objNDE014_Log.EncoderType,
                                        ScannerType = objNDE014_Log.ScannerType,
                                        ScannerMake = objNDE014_Log.ScannerMake,
                                        CableType = objNDE014_Log.CableType,
                                        CableLength = objNDE014_Log.CableLength,
                                        ScanPlan = objNDE014_Log.ScanPlan,
                                        PlannerRemarks = objNDE014_Log.PlannerRemarks,
                                        ApproverRemarks = objNDE014_Log.ApproverRemarks,
                                        SimulationBlockID = objNDE014_Log.SimulationBlockID,
                                        SimulationBlockIDThickness = objNDE014_Log.SimulationBlockIDThickness,
                                        CreatedBy = objClsLoginInfo.UserName,
                                        CreatedOn = DateTime.Now
                                    });
                                    db.SaveChanges();

                                    List<NDE015_Log> lstNDE015_Log = db.NDE015_Log.Where(x => x.RefId == objNDE014_Log.Id).ToList();
                                    if (lstNDE015_Log != null && lstNDE015_Log.Count > 0)
                                    {
                                        List<QMS085> lstQMS085 = new List<QMS085>();
                                        foreach (NDE015_Log objNDE015_Log in lstNDE015_Log)
                                        {
                                            QMS085 objQMS085 = db.QMS085.Add(new QMS085()
                                            {
                                                HeaderId = objQMS084.HeaderId,
                                                RefHeaderId = objQMS084.RefHeaderId,
                                                QualityProject = objQMS084.QualityProject,
                                                Project = objQMS084.Project,
                                                BU = objQMS084.BU,
                                                Location = objQMS084.Location,
                                                SeamNo = objQMS084.SeamNo,
                                                StageCode = objQMS084.StageCode,
                                                StageSequence = objQMS084.StageSequence,
                                                IterationNo = objQMS084.IterationNo,
                                                RequestNo = objQMS084.RequestNo,
                                                RequestNoSequence = objQMS084.RequestNoSequence,
                                                UTTechNo = objQMS084.UTTechNo,
                                                RevNo = objQMS084.RevNo,
                                                ZoneNo = objNDE015_Log.ZoneNo,
                                                Probe = objNDE015_Log.Probe,
                                                Wedge = objNDE015_Log.Wedge,
                                                NearRefractedArea = objNDE015_Log.NearRefractedArea,
                                                Skip = objNDE015_Log.Skip,
                                                ScanType = objNDE015_Log.ScanType,
                                                CreatedBy = objClsLoginInfo.UserName,
                                                CreatedOn = DateTime.Now
                                                // lstQMS085.Add(objQMS085);
                                            });
                                            db.SaveChanges();
                                            List<NDE016_Log> lstNDE016_Log = db.NDE016_Log.Where(x => x.RefLineLogId == objNDE015_Log.Id && x.RefId == objNDE015_Log.RefId).ToList();
                                            List<QMS086> lstQMS086 = new List<QMS086>();
                                            foreach (NDE016_Log objNDE016_Log in lstNDE016_Log)
                                            {
                                                QMS086 objQMS086 = new QMS086();
                                                objQMS086.LineId = objQMS085.LineId;
                                                objQMS086.HeaderId = objQMS084.HeaderId;
                                                objQMS086.RefHeaderId = objQMS060.HeaderId;
                                                objQMS086.QualityProject = objQMS060.QualityProject;
                                                objQMS086.Project = objQMS060.Project;
                                                objQMS086.BU = objQMS060.BU;
                                                objQMS086.Location = objQMS060.Location;
                                                objQMS086.SeamNo = objQMS060.SeamNo;
                                                objQMS086.StageCode = objQMS060.StageCode;
                                                objQMS086.StageSequence = objQMS060.StageSequence;
                                                objQMS086.IterationNo = objQMS060.IterationNo;
                                                objQMS086.RequestNo = objQMS060.RequestNo;
                                                objQMS086.RequestNoSequence = objQMS060.RequestNoSequence;
                                                objQMS086.UTTechNo = objQMS060.NDETechniqueNo;
                                                objQMS086.RevNo = objQMS060.NDETechniqueRevisionNo;
                                                objQMS086.ZoneNo = objNDE016_Log.ZoneNo;
                                                objQMS086.GroupNo = objNDE016_Log.GroupNo;
                                                objQMS086.Angle = objNDE016_Log.Angle;
                                                objQMS086.DepthCoverage = objNDE016_Log.DepthCoverage;
                                                objQMS086.FocalDepth = objNDE016_Log.FocalDepth;
                                                objQMS086.TCGIndicationAmplitude = objNDE016_Log.TCGIndicationAmplitude;
                                                objQMS086.StartElement = objNDE016_Log.StartElement;
                                                objQMS086.NumberofElements = objNDE016_Log.NoofElements;
                                                objQMS086.AngularIncrementalChange = objNDE016_Log.AngularIncrementalChange;
                                                objQMS086.FocalPlane = objNDE016_Log.FocalPlane;
                                                objQMS086.CreatedBy = objClsLoginInfo.UserName;
                                                objQMS086.CreatedOn = DateTime.Now;

                                                lstQMS086.Add(objQMS086);
                                            }
                                            if (lstQMS086 != null && lstQMS086.Count > 0)
                                            {
                                                db.QMS086.AddRange(lstQMS086);
                                                db.SaveChanges();
                                            }
                                        }
                                    }
                                    strURL = WebsiteURL + "/IPI/MaintainNDESeamTechnique/PAUTHeader?id=" + Convert.ToInt32(objQMS084.HeaderId) + "";
                                }
                                #endregion

                                break;
                            default:
                                break;
                        }
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "NDE Technique Maintain successfully.";
                        objResponseMsg.TechNo = objQMS060.NDETechniqueNo;
                        objResponseMsg.TechRev = Convert.ToInt32(objQMS060.NDETechniqueRevisionNo);
                        objResponseMsg.TechName = TechName;
                        objResponseMsg.RedirectURL = strURL;
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "NDE offer not available.";
                    }
                    #endregion
                }
                else
                {
                    #region PART
                    QMS065 objQMS065 = db.QMS065.Where(x => x.RequestId == qms065.RequestId).FirstOrDefault();
                    string oldTechNo = objQMS065.NDETechniqueNo;
                    int? oldRev = objQMS065.NDETechniqueRevisionNo;
                    if (objQMS065 != null)
                    {
                        objQMS065.NDETechniqueNo = qms065.NDETechniqueNo;
                        objQMS065.NDETechniqueRevisionNo = qms065.NDETechniqueRevisionNo;
                        string strApproveStatus = clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue();
                        switch (TechName.ToUpper())
                        {
                            case "PT":
                                break;
                            case "MT":
                                break;
                            case "RT":
                                break;
                            case "ASCAN UT":
                                if (!string.IsNullOrWhiteSpace(oldTechNo))
                                {
                                    //QMS090 objDelQMS090 = db.QMS090.Where(x => x.UTTechNo == oldTechNo && x.RevNo == oldRev).FirstOrDefault(); 
                                    QMS090 objDelQMS090 = db.QMS090.Where(x => x.RefHeaderId == qms065.RequestId).FirstOrDefault();
                                    if (objDelQMS090 != null)
                                    {
                                        List<QMS091> lstDelQMS091 = db.QMS091.Where(x => x.HeaderId == objDelQMS090.HeaderId).ToList();
                                        if (lstDelQMS091 != null && lstDelQMS091.Count > 0)
                                        {
                                            db.QMS091.RemoveRange(lstDelQMS091);
                                        }
                                        db.QMS090.Remove(objDelQMS090);
                                        db.SaveChanges();
                                    }
                                }

                                #region ASCAN UT

                                NDE010_Log objNDE010_Log = db.NDE010_Log.Where(x => x.UTTechNo == qms065.NDETechniqueNo && x.RevNo == qms065.NDETechniqueRevisionNo && x.Status == strApproveStatus).FirstOrDefault();
                                if (objNDE010_Log != null)
                                {
                                    QMS090 objQMS090 = db.QMS090.Add(new QMS090
                                    {
                                        RefHeaderId = objQMS065.RequestId,
                                        QualityProject = objQMS065.QualityProject,
                                        Project = objQMS065.Project,
                                        BU = objQMS065.BU,
                                        Location = objQMS065.Location,
                                        PartNo = objQMS065.PartAssemblyFindNumber,
                                        //ParentPartNo = objQMS065.
                                        ChildPartNo = objQMS065.PartAssemblyChildItemCode,
                                        StageCode = objQMS065.StageCode,
                                        StageSequence = objQMS065.StageSequence,
                                        IterationNo = objQMS065.IterationNo,
                                        RequestNo = objQMS065.RequestNo,
                                        RequestNoSequence = objQMS065.RequestNoSequence,
                                        UTTechNo = objQMS065.NDETechniqueNo,
                                        RevNo = objQMS065.NDETechniqueRevisionNo,
                                        Status = objNDE010_Log.Status,
                                        UTProcedureNo = objNDE010_Log.UTProcedureNo,
                                        RecordableIndication = objNDE010_Log.RecordableIndication,
                                        ScanTechnique = objNDE010_Log.ScanTechnique,
                                        DGSDiagram = Convert.ToBoolean(objNDE010_Log.DGSDiagram),
                                        BasicCalibrationBlock = objNDE010_Log.BasicCalibrationBlock,
                                        SimulationBlock1 = objNDE010_Log.SimulationBlock1,
                                        SimulationBlock2 = objNDE010_Log.SimulationBlock2,
                                        CouplantUsed = objNDE010_Log.CouplantUsed,
                                        ScanPlan = objNDE010_Log.ScanPlan,
                                        ScanningSensitivity = objNDE010_Log.ScanningSensitivity,
                                        PlannerRemarks = objNDE010_Log.PlannerRemarks,
                                        ApproverRemarks = objNDE010_Log.ApproverRemarks,
                                        CreatedBy = objClsLoginInfo.UserName,
                                        CreatedOn = DateTime.Now,
                                    });
                                    db.SaveChanges();
                                    List<NDE011_Log> lstNDE011_Log = db.NDE011_Log.Where(x => x.RefId == objNDE010_Log.Id).ToList();
                                    if (lstNDE011_Log != null && lstNDE011_Log.Count > 0)
                                    {
                                        List<QMS091> lstQMS091 = new List<QMS091>();
                                        foreach (NDE011_Log objNDE011_Log in lstNDE011_Log)
                                        {
                                            QMS091 objQMS091 = new QMS091();

                                            objQMS091.HeaderId = objQMS090.HeaderId;
                                            objQMS091.RefHeaderId = objQMS090.RefHeaderId;
                                            objQMS091.QualityProject = objQMS090.QualityProject;
                                            objQMS091.Project = objQMS090.Project;
                                            objQMS091.BU = objQMS090.BU;
                                            objQMS091.Location = objQMS090.Location;
                                            objQMS091.PartNo = objQMS090.PartNo;
                                            objQMS091.ParentPartNo = objQMS090.ParentPartNo;
                                            objQMS091.ChildPartNo = objQMS090.ChildPartNo;
                                            objQMS091.StageCode = objQMS090.StageCode;
                                            objQMS091.StageSequence = objQMS090.StageSequence;
                                            objQMS091.IterationNo = objQMS090.IterationNo;
                                            objQMS091.RequestNo = objQMS090.RequestNo;
                                            objQMS091.RequestNoSequence = objQMS090.RequestNoSequence;
                                            objQMS091.UTTechNo = objQMS090.UTTechNo;
                                            objQMS091.RevNo = objQMS090.RevNo;
                                            objQMS091.SearchUnit = objNDE011_Log.SearchUnit;
                                            objQMS091.ReferenceReflector = objNDE011_Log.ReferenceReflector;
                                            objQMS091.CableType = objNDE011_Log.CableType;
                                            objQMS091.CableLength = objNDE011_Log.CableLength;
                                            objQMS091.CreatedBy = objClsLoginInfo.UserName;
                                            objQMS091.CreatedOn = DateTime.Now;
                                            lstQMS091.Add(objQMS091);
                                        }
                                        if (lstQMS091 != null && lstQMS091.Count > 0)
                                        {
                                            db.QMS091.AddRange(lstQMS091);
                                            db.SaveChanges();
                                        }
                                    }
                                    strURL = WebsiteURL + "/IPI/PartlistNDEInspection/AscanUTViewDetails?HeaderId=" + Convert.ToInt32(objQMS090.HeaderId) + "";
                                }

                                #endregion

                                break;
                            case "TOFD UT":

                                if (!string.IsNullOrWhiteSpace(oldTechNo))
                                {
                                    //QMS092 objDelQMS092 = db.QMS092.Where(x => x.UTTechNo == oldTechNo && x.RevNo == oldRev).FirstOrDefault();
                                    QMS092 objDelQMS092 = db.QMS092.Where(x => x.RefHeaderId == qms065.RequestId).FirstOrDefault();
                                    if (objDelQMS092 != null)
                                    {
                                        List<QMS093> lstDelQMS093 = db.QMS093.Where(x => x.HeaderId == objDelQMS092.HeaderId).ToList();
                                        if (lstDelQMS093 != null && lstDelQMS093.Count > 0)
                                        {
                                            db.QMS093.RemoveRange(lstDelQMS093);
                                        }
                                        db.QMS092.Remove(objDelQMS092);
                                        db.SaveChanges();
                                    }
                                }

                                #region TOFD UT
                                NDE012_Log objNDE012_Log = db.NDE012_Log.Where(x => x.UTTechNo == qms065.NDETechniqueNo && x.RevNo == qms065.NDETechniqueRevisionNo && x.Status == strApproveStatus).FirstOrDefault();
                                if (objNDE012_Log != null)
                                {
                                    QMS092 objQMS092 = db.QMS092.Add(new QMS092
                                    {
                                        RefHeaderId = objQMS065.RequestId,
                                        QualityProject = objQMS065.QualityProject,
                                        Project = objQMS065.Project,
                                        BU = objQMS065.BU,
                                        Location = objQMS065.Location,
                                        PartNo = objQMS065.PartAssemblyFindNumber,
                                        //ParentPartNo = objQMS0
                                        ChildPartNo = objQMS065.PartAssemblyChildItemCode,
                                        StageCode = objQMS065.StageCode,
                                        StageSequence = objQMS065.StageSequence,
                                        IterationNo = objQMS065.IterationNo,
                                        RequestNo = objQMS065.RequestNo,
                                        RequestNoSequence = objQMS065.RequestNoSequence,
                                        UTTechNo = objQMS065.NDETechniqueNo,
                                        RevNo = objQMS065.NDETechniqueRevisionNo,
                                        Status = objNDE012_Log.Status,
                                        UTProcedureNo = objNDE012_Log.UTProcedureNo,
                                        CouplantUsed = objNDE012_Log.CouplantUsed,
                                        ReferenceReflectorSize = objNDE012_Log.ReferenceReflectorSize,
                                        ReferenceReflectorDistance = objNDE012_Log.ReferenceReflectorDistance,
                                        CalibrationBlockID = objNDE012_Log.CalibrationBlockID,
                                        CalibrationBlockIDThickness = objNDE012_Log.CalibrationBlockIDThickness,
                                        ScanningDetail = objNDE012_Log.ScanningDetail,
                                        EncoderType = objNDE012_Log.EncoderType,
                                        ScannerType = objNDE012_Log.ScannerType,
                                        ScannerMake = objNDE012_Log.ScannerMake,
                                        CableType = objNDE012_Log.CableType,
                                        CableLength = objNDE012_Log.CableLength,
                                        ScanPlan = objNDE012_Log.ScanPlan,
                                        PlannerRemarks = objNDE012_Log.PlannerRemarks,
                                        ApproverRemarks = objNDE012_Log.ApproverRemarks,
                                        CreatedBy = objClsLoginInfo.UserName,
                                        CreatedOn = DateTime.Now

                                    });
                                    db.SaveChanges();
                                    List<NDE013_Log> lstNDE013_Log = db.NDE013_Log.Where(x => x.RefId == objNDE012_Log.Id).ToList();
                                    if (lstNDE013_Log != null && lstNDE013_Log.Count > 0)
                                    {
                                        List<QMS093> lstQMS093 = new List<QMS093>();
                                        foreach (NDE013_Log objNDE013_Log in lstNDE013_Log)
                                        {
                                            QMS093 objQMS093 = new QMS093();
                                            objQMS093.HeaderId = objQMS092.HeaderId;
                                            objQMS093.RefHeaderId = objQMS092.RefHeaderId;
                                            objQMS093.QualityProject = objQMS092.QualityProject;
                                            objQMS093.Project = objQMS092.Project;
                                            objQMS093.BU = objQMS092.BU;
                                            objQMS093.Location = objQMS092.Location;
                                            objQMS093.PartNo = objQMS092.PartNo;
                                            objQMS093.ParentPartNo = objQMS092.ParentPartNo;
                                            objQMS093.ChildPartNo = objQMS092.ChildPartNo;
                                            objQMS093.StageCode = objQMS092.StageCode;
                                            objQMS093.StageSequence = objQMS092.StageSequence;
                                            objQMS093.IterationNo = objQMS092.IterationNo;
                                            objQMS093.RequestNo = objQMS092.RequestNo;
                                            objQMS093.RequestNoSequence = objQMS092.RequestNoSequence;
                                            objQMS093.UTTechNo = objQMS092.UTTechNo;
                                            objQMS093.RevNo = objQMS092.RevNo;
                                            objQMS093.ScanNo = objNDE013_Log.ScanNo;
                                            objQMS093.RefRevNo = objNDE013_Log.RevNo;
                                            objQMS093.SearchUnit = objNDE013_Log.SearchUnit;
                                            objQMS093.ScanType = objNDE013_Log.ScanType;
                                            objQMS093.PCS = objNDE013_Log.PCS;
                                            objQMS093.CreatedBy = objClsLoginInfo.UserName;
                                            objQMS093.CreatedOn = DateTime.Now;
                                            lstQMS093.Add(objQMS093);
                                        }
                                        if (lstQMS093 != null && lstQMS093.Count > 0)
                                        {
                                            db.QMS093.AddRange(lstQMS093);
                                            db.SaveChanges();
                                        }
                                    }
                                    strURL = WebsiteURL + "/IPI/PartlistNDEInspection/TOFDUTViewDetails?HeaderId=" + Convert.ToInt32(objQMS092.HeaderId) + "";
                                }
                                #endregion

                                break;
                            case "PAUT":

                                //QMS094 objDelQMS094 = db.QMS094.Where(x => x.UTTechNo == oldTechNo && x.RevNo == oldRev).FirstOrDefault();
                                QMS094 objDelQMS094 = db.QMS094.Where(x => x.RefHeaderId == qms065.RequestId).FirstOrDefault();

                                if (objDelQMS094 != null)
                                {
                                    List<QMS095> lstDelQMS095 = db.QMS095.Where(x => x.HeaderId == objDelQMS094.HeaderId).ToList();
                                    if (lstDelQMS095 != null && lstDelQMS095.Count > 0)
                                    {
                                        int[] arrayLineIds = lstDelQMS095.Select(x => x.LineId).ToArray();
                                        if (arrayLineIds.Length > 0)
                                        {
                                            List<QMS096> lstDelQMS096 = db.QMS096.Where(x => arrayLineIds.Contains(x.LineId)).ToList();
                                            if (lstDelQMS096 != null && lstDelQMS096.Count > 0)
                                            {
                                                db.QMS096.RemoveRange(lstDelQMS096);
                                            }
                                        }
                                        db.QMS095.RemoveRange(lstDelQMS095);
                                    }
                                    db.QMS094.Remove(objDelQMS094);
                                    db.SaveChanges();
                                }

                                #region PAUT
                                NDE014_Log objNDE014_Log = db.NDE014_Log.Where(x => x.UTTechNo == qms065.NDETechniqueNo && x.RevNo == qms065.NDETechniqueRevisionNo && x.Status == strApproveStatus).FirstOrDefault();

                                if (objNDE014_Log != null)
                                {
                                    QMS094 objQMS094 = db.QMS094.Add(new QMS094
                                    {
                                        RefHeaderId = objQMS065.RequestId,
                                        QualityProject = objQMS065.QualityProject,
                                        Project = objQMS065.Project,
                                        BU = objQMS065.BU,
                                        Location = objQMS065.Location,
                                        PartNo = objQMS065.PartAssemblyFindNumber,
                                        //ParentPartNo = objQMS0
                                        ChildPartNo = objQMS065.PartAssemblyChildItemCode,
                                        StageCode = objQMS065.StageCode,
                                        StageSequence = objQMS065.StageSequence,
                                        IterationNo = objQMS065.IterationNo,
                                        RequestNo = objQMS065.RequestNo,
                                        RequestNoSequence = objQMS065.RequestNoSequence,
                                        UTTechNo = objQMS065.NDETechniqueNo,
                                        RevNo = objQMS065.NDETechniqueRevisionNo,
                                        Status = objNDE014_Log.Status,
                                        UTProcedureNo = objNDE014_Log.UTProcedureNo,
                                        CouplantUsed = objNDE014_Log.CouplantUsed,
                                        ReferenceReflectorSize = objNDE014_Log.ReferenceReflectorSize,
                                        ReferenceReflectorDistance = objNDE014_Log.ReferenceReflectorDistance,
                                        CalibrationBlockID = objNDE014_Log.CalibrationBlockID,
                                        CalibrationBlockIDThickness = objNDE014_Log.CalibrationBlockIDThickness,
                                        EncoderType = objNDE014_Log.EncoderType,
                                        ScannerType = objNDE014_Log.ScannerType,
                                        ScannerMake = objNDE014_Log.ScannerMake,
                                        CableType = objNDE014_Log.CableType,
                                        CableLength = objNDE014_Log.CableLength,
                                        ScanPlan = objNDE014_Log.ScanPlan,
                                        PlannerRemarks = objNDE014_Log.PlannerRemarks,
                                        ApproverRemarks = objNDE014_Log.ApproverRemarks,
                                        SimulationBlockID = objNDE014_Log.SimulationBlockID,
                                        SimulationBlockIDThickness = objNDE014_Log.SimulationBlockIDThickness,
                                        CreatedBy = objClsLoginInfo.UserName,
                                        CreatedOn = DateTime.Now
                                    });
                                    db.SaveChanges();

                                    List<NDE015_Log> lstNDE015_Log = db.NDE015_Log.Where(x => x.RefId == objNDE014_Log.Id).ToList();
                                    if (lstNDE015_Log != null && lstNDE015_Log.Count > 0)
                                    {
                                        List<QMS095> lstQMS095 = new List<QMS095>();
                                        foreach (NDE015_Log objNDE015_Log in lstNDE015_Log)
                                        {
                                            QMS095 objQMS095 = db.QMS095.Add(new QMS095()
                                            {
                                                HeaderId = objQMS094.HeaderId,
                                                RefHeaderId = objQMS094.RefHeaderId,
                                                QualityProject = objQMS094.QualityProject,
                                                Project = objQMS094.Project,
                                                BU = objQMS094.BU,
                                                Location = objQMS094.Location,
                                                PartNo = objQMS094.PartNo,
                                                ParentPartNo = objQMS094.ParentPartNo,
                                                ChildPartNo = objQMS094.ChildPartNo,
                                                StageCode = objQMS094.StageCode,
                                                StageSequence = objQMS094.StageSequence,
                                                IterationNo = objQMS094.IterationNo,
                                                RequestNo = objQMS094.RequestNo,
                                                RequestNoSequence = objQMS094.RequestNoSequence,
                                                UTTechNo = objQMS094.UTTechNo,
                                                RevNo = objQMS094.RevNo,
                                                ZoneNo = objNDE015_Log.ZoneNo,
                                                Probe = objNDE015_Log.Probe,
                                                Wedge = objNDE015_Log.Wedge,
                                                NearRefractedArea = objNDE015_Log.NearRefractedArea,
                                                Skip = objNDE015_Log.Skip,
                                                ScanType = objNDE015_Log.ScanType,
                                                CreatedBy = objClsLoginInfo.UserName,
                                                CreatedOn = DateTime.Now
                                                // lstQMS095.Add(objQMS095);
                                            });
                                            db.SaveChanges();
                                            List<NDE016_Log> lstNDE016_Log = db.NDE016_Log.Where(x => x.RefLineLogId == objNDE015_Log.Id && x.RefId == objNDE015_Log.RefId).ToList();
                                            List<QMS096> lstQMS096 = new List<QMS096>();
                                            foreach (NDE016_Log objNDE016_Log in lstNDE016_Log)
                                            {
                                                QMS096 objQMS096 = new QMS096();
                                                objQMS096.LineId = objQMS095.LineId;
                                                objQMS096.HeaderId = objQMS094.HeaderId;
                                                objQMS096.RefHeaderId = objQMS065.HeaderId;
                                                objQMS096.QualityProject = objQMS065.QualityProject;
                                                objQMS096.Project = objQMS065.Project;
                                                objQMS096.BU = objQMS065.BU;
                                                objQMS096.Location = objQMS065.Location;
                                                objQMS096.PartNo = objQMS065.PartAssemblyFindNumber;
                                                // objQMS096.ParentPartNo = objQMS065.
                                                objQMS096.ChildPartNo = objQMS065.PartAssemblyChildItemCode;
                                                objQMS096.StageCode = objQMS065.StageCode;
                                                objQMS096.StageSequence = objQMS065.StageSequence;
                                                objQMS096.IterationNo = objQMS065.IterationNo;
                                                objQMS096.RequestNo = objQMS065.RequestNo;
                                                objQMS096.RequestNoSequence = objQMS065.RequestNoSequence;
                                                objQMS096.UTTechNo = objQMS065.NDETechniqueNo;
                                                objQMS096.RevNo = objQMS065.NDETechniqueRevisionNo;
                                                objQMS096.ZoneNo = objNDE016_Log.ZoneNo;
                                                objQMS096.GroupNo = objNDE016_Log.GroupNo;
                                                objQMS096.Angle = objNDE016_Log.Angle;
                                                objQMS096.DepthCoverage = objNDE016_Log.DepthCoverage;
                                                objQMS096.FocalDepth = objNDE016_Log.FocalDepth;
                                                objQMS096.TCGIndicationAmplitude = objNDE016_Log.TCGIndicationAmplitude;
                                                objQMS096.StartElement = objNDE016_Log.StartElement;
                                                objQMS096.NumberofElements = objNDE016_Log.NoofElements;
                                                objQMS096.AngularIncrementalChange = objNDE016_Log.AngularIncrementalChange;
                                                objQMS096.FocalPlane = objNDE016_Log.FocalPlane;
                                                objQMS096.CreatedBy = objClsLoginInfo.UserName;
                                                objQMS096.CreatedOn = DateTime.Now;

                                                lstQMS096.Add(objQMS096);
                                            }
                                            if (lstQMS096 != null && lstQMS096.Count > 0)
                                            {
                                                db.QMS096.AddRange(lstQMS096);
                                                db.SaveChanges();
                                            }
                                        }
                                    }
                                    strURL = WebsiteURL + "/IPI/PartlistNDEInspection/PAUTViewDetails?HeaderId=" + Convert.ToInt32(objQMS094.HeaderId) + "";
                                }
                                #endregion

                                break;
                            default:
                                break;
                        }
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "NDE Technique Maintain successfully.";
                        objResponseMsg.TechNo = objQMS065.NDETechniqueNo;
                        objResponseMsg.TechRev = Convert.ToInt32(objQMS065.NDETechniqueRevisionNo);
                        objResponseMsg.TechName = TechName;
                        objResponseMsg.RedirectURL = strURL;
                        objResponseMsg.TechName = TechName;
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "NDE offer not available.";
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveClearedQuantity(int RequestId, string ltfpsDocRev = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                bool allclear = false;
                bool parentReadyToOffer = false;
                string ApprovedStatus = clsImplementationEnum.ICLPartStatus.Approved.GetStringValue();
                QMS065 objQMS065 = db.QMS065.Where(x => x.RequestId == RequestId).FirstOrDefault();
                if (objQMS065 != null)
                {
                    objResponseMsg = Manager.IsApplicableForAttend(objQMS065.RequestId, clsImplementationEnum.InspectionFor.PART.GetStringValue(), "NDE");
                    if (!objResponseMsg.Key)
                    {
                        return Json(objResponseMsg);
                    }

                    bool isLTFPS = Manager.IsLTFPSProject(objQMS065.Project, objQMS065.BU, objQMS065.Location);

                    if (objQMS065.OfferedQuantity > 0)
                    {
                        objQMS065.TestResult = clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue();
                        db.SaveChanges();
                        if (isLTFPS)
                        {
                            LTF002 objLTF002 = db.LTF002.Where(x => x.LineId == objQMS065.LTFPSHeaderId).FirstOrDefault();
                            string strInspectionStatus = GetLTFPSInspectionStatus(objLTF002.LineId, 1); // Quantity Field not available in LTF002 So put 1 as per given required
                            if (!string.IsNullOrEmpty(strInspectionStatus))
                            {
                                objLTF002.LNTStatus = clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue();
                                //objLTF002.l = DateTime.Now;
                                objLTF002.InspectionStatus = strInspectionStatus;
                                objLTF002.LNTInspectorResultOn = DateTime.Now;
                                if (!string.IsNullOrWhiteSpace(ltfpsDocRev))
                                {
                                    objLTF002.DocRevNo = ltfpsDocRev;
                                }
                                allclear = true;
                                // List<List<LTF002>> groups = db.LTF002.Where(x => x.PartNo == objLTF002.PartNo && x.QualityProject == objLTF002.QualityProject && x.Location == objLTF002.Location && x.OperationNo > objLTF002.OperationNo).OrderBy(x => x.OperationNo).GroupBy(c => c.OperationNo)
                                //.Select(group => group.ToList())
                                //.ToList();

                                // if (groups != null && groups.Count > 0)
                                // {
                                //     List<LTF002> lstLTF002 = groups.FirstOrDefault().ToList();
                                //     if (lstLTF002 != null && lstLTF002.Count > 0)
                                //     {
                                //         lstLTF002.ForEach(x =>
                                //         {
                                //             x.InspectionStatus = x.InspectionStatus == null ? clsImplementationEnum.PartlistOfferInspectionStatus.READY_TO_OFFER.GetStringValue() : x.InspectionStatus;
                                //             // x.BalanceQuantity = x.InspectionStatus == null ? objQMS065.OfferedQuantity : 1 + objQMS065.OfferedQuantity;
                                //         }
                                //         );
                                //     }
                                // }
                            }
                            objLTF002.EditedOn = DateTime.Now;
                            objLTF002.EditedBy = objClsLoginInfo.UserName;
                        }
                        else
                        {
                            QMS045 objQMS045 = db.QMS045.Where(x => x.HeaderId == objQMS065.HeaderId).FirstOrDefault();
                            if (objQMS045.BalanceQuantity > 0)
                            {
                                objQMS045.L_TInspectionResult = clsImplementationEnum.PartlistOfferInspectionStatus.PARTIALLY_CLEARED.GetStringValue();
                                objQMS045.InspectionStatus = clsImplementationEnum.PartlistOfferInspectionStatus.PARTIALLY_OFFERED.GetStringValue();
                                objQMS045.LNTInspectorResultOn = DateTime.Now;
                                //No change in Inspection Status
                            }
                            else
                            {
                                string strInspectionStatus = GetInspectionStatus(objQMS045.HeaderId, (objQMS045.Quantity == null ? 0 : Convert.ToInt32(objQMS045.Quantity)));
                                if (!string.IsNullOrEmpty(strInspectionStatus))
                                {
                                    objQMS045.L_TInspectionResult = clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue();
                                    objQMS045.LNTInspectorResultOn = DateTime.Now;
                                    objQMS045.InspectionStatus = strInspectionStatus;
                                    //bool IsTPIOnlineApproval = db.QMS010.Where(x => x.QualityProject == objQMS045.QualityProject).Select(x => x.TPIOnlineApproval).FirstOrDefault();
                                    //if (!IsTPIOnlineApproval)
                                    //{
                                    //objQMS045.InspectionStatus = strInspectionStatus;
                                    //List<List<QMS045>> groups = db.QMS045.OrderBy(x => x.StageSequence).Where(x => x.StageSequence != objQMS045.StageSequence && x.InspectionStatus == null).GroupBy(c => c.StageSequence)
                                    //   .Select(group => group.ToList())
                                    //   .ToList();
                                    //List<QMS045> lstQMS045 = groups.FirstOrDefault().ToList();
                                    //if (lstQMS045 != null && lstQMS045.Count > 0)
                                    //{
                                    //    lstQMS045.ForEach(x => x.InspectionStatus = clsImplementationEnum.PartlistOfferInspectionStatus.READY_TO_OFFER.GetStringValue());
                                    //}
                                    //}

                                    List<List<QMS045>> groups = db.QMS045.Where(x => x.PartNo == objQMS045.PartNo && x.ParentPartNo == objQMS045.ParentPartNo && x.QualityProject == objQMS045.QualityProject && x.Location == objQMS045.Location && x.StageSequence > objQMS045.StageSequence).OrderBy(x => x.StageSequence).GroupBy(c => c.StageSequence)
                                   .Select(group => group.ToList())
                                   .ToList();

                                    if (groups != null && groups.Count > 0)
                                    {
                                        List<QMS045> lstQMS045 = groups.FirstOrDefault().ToList();
                                        if (lstQMS045 != null && lstQMS045.Count > 0)
                                        {
                                            lstQMS045.ForEach(x =>
                                            {
                                                x.InspectionStatus = x.InspectionStatus == null ? clsImplementationEnum.PartlistOfferInspectionStatus.READY_TO_OFFER.GetStringValue() : x.InspectionStatus;
                                                x.BalanceQuantity = x.InspectionStatus == null ? objQMS065.OfferedQuantity : (x.BalanceQuantity == null ? 0 : x.BalanceQuantity) + objQMS065.OfferedQuantity;
                                            }
                                            );
                                        }
                                    }

                                    parentReadyToOffer = true;

                                }
                            }
                            objQMS045.EditedOn = DateTime.Now;
                            objQMS045.EditedBy = objClsLoginInfo.UserName;
                        }
                    }
                    objQMS065.RequestStatus = clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue();
                    objQMS065.InspectedBy = objClsLoginInfo.UserName;
                    objQMS065.InspectedOn = DateTime.Now;

                    db.SaveChanges();

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PROD3.GetStringValue(), objQMS065.Project, objQMS065.BU, objQMS065.Location, "NDT Request " + objQMS065.RequestNo + " for Stage: " + objQMS065.StageCode + " of Part: " + objQMS065.PartAssemblyFindNumber + " & Project No: " + objQMS065.QualityProject + " has been Cleared", clsImplementationEnum.NotificationType.Information.GetStringValue());
                    #endregion

                    //if (parentReadyToOffer)
                    //{
                        #region Code for make parent part ready to offer 
                        //QMS045 objQMS045 = objQMS065.QMS045;
                        //if (Manager.IsPARTFullyCleared(objQMS045.Project, objQMS045.QualityProject, objQMS045.BU, objQMS045.Location, objQMS045.PartNo))
                        //{
                        //    // get seams which are associated with this parts and ready to offer
                        //    List<string> SeamList = db.QMS012_Log.Where(c => c.QualityProject == objQMS045.QualityProject && c.Project == objQMS045.Project && c.Status == ApprovedStatus).ToList().Where(w => w.Position.Split(',').Contains(objQMS045.PartNo)).Select(s => s.SeamNo).Distinct().ToList();
                        //    foreach (var seam in SeamList)
                        //    {
                        //        //QMS012_Log objQMS012 = db.QMS012_Log.FirstOrDefault(c => c.Project == objQMS045.Project && c.QualityProject == objQMS045.QualityProject && c.SeamNo == seam && c.Status == ApprovedStatus);
                        //        //if (Manager.IsChildPartCleared(objQMS045.Project, objQMS045.QualityProject, objQMS045.BU, objQMS045.Location, objQMS012.AssemblyNo))
                        //        //{
                        //        QMS030 objQMS030 = db.QMS030.FirstOrDefault(c => c.Project == objQMS045.Project && c.QualityProject == objQMS045.QualityProject && c.SeamNo == seam && c.Status == ApprovedStatus);
                        //        if (objQMS030 != null)
                        //        {
                        //            Manager.ReadyToOffer_SEAM(objQMS030);
                        //        }
                        //        //}
                        //    }

                        //    QMS035 objQMS035 = db.QMS035.FirstOrDefault(c => c.Project == objQMS045.Project && c.QualityProject == objQMS045.QualityProject && c.BU == objQMS045.BU && c.Location == objQMS045.Location && c.ChildPartNo == objQMS045.ParentPartNo && c.Status == ApprovedStatus);
                        //    if (objQMS035 != null)
                        //    {
                        //        Manager.ReadyToOffer_PART(objQMS035);
                        //    }
                        //    else
                        //    {
                        //        List<QMS012> lstQMS012 = db.QMS012.Where(c => c.Project == objQMS045.Project && c.QualityProject == objQMS045.QualityProject && c.AssemblyNo == objQMS045.ParentPartNo).ToList();
                        //        foreach (var objQMS012 in lstQMS012)
                        //        {
                        //            //if (Manager.IsChildPartCleared(objQMS045.Project, objQMS045.QualityProject, objQMS045.BU, objQMS045.Location, objQMS012.AssemblyNo))
                        //            //{
                        //            QMS030 objQMS030 = db.QMS030.FirstOrDefault(c => c.Project == objQMS045.Project && c.QualityProject == objQMS045.QualityProject && c.SeamNo == objQMS012.SeamNo && c.Status == ApprovedStatus);
                        //            if (objQMS030 != null)
                        //            {
                        //                Manager.ReadyToOffer_SEAM(objQMS030);
                        //            }
                        //            //}
                        //        }
                        //    }

                        //}

                        #endregion
                    //}

                    if (allclear)
                    {
                        var result = db.SP_LTFPS_RELEASE_READY_TO_OFFER(objQMS065.LTF002.HeaderId);
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Offer cleared successfully.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "NDE offer not available.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveRejectQuantity(int RequestId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS065 objQMS065 = db.QMS065.Where(x => x.RequestId == RequestId).FirstOrDefault();
                QMS036 objQMS036 = db.QMS036.Where(x =>
                                                        x.QualityProject == objQMS065.QualityProject &&
                                                        x.Project == objQMS065.Project &&
                                                        x.BU == objQMS065.BU &&
                                                        x.Location == objQMS065.Location &&
                                                        x.PartNo == objQMS065.PartAssemblyFindNumber &&
                                                        // x.ParentPartNo == objQMS065.ParentPartNo &&
                                                        x.ChildPartNo == objQMS065.PartAssemblyChildItemCode &&
                                                        x.StageCode == objQMS065.StageCode).FirstOrDefault();
                if (objQMS065 != null)
                {
                    objResponseMsg = Manager.IsApplicableForAttend(objQMS065.RequestId, clsImplementationEnum.InspectionFor.PART.GetStringValue(), "NDE");
                    if (!objResponseMsg.Key)
                    {
                        return Json(objResponseMsg);
                    }

                    if (objQMS065.OfferedQuantity > 0)
                    {
                        bool isLTFPS = Manager.IsLTFPSProject(objQMS065.Project, objQMS065.BU, objQMS065.Location);
                        objQMS065.TestResult = clsImplementationEnum.PartlistOfferInspectionStatus.REJECTED.GetStringValue();
                        objQMS065.RejectedBy = objClsLoginInfo.UserName;
                        objQMS065.RejectedOn = DateTime.Now;
                        db.SaveChanges();

                        if (isLTFPS)
                        {
                            LTF002 objLTF002 = db.LTF002.Where(x => x.LineId == objQMS065.LTFPSHeaderId).FirstOrDefault();
                            //if (objLTF002.LNTStatus == null)
                            //{
                            objLTF002.LNTStatus = clsImplementationEnum.PartlistOfferInspectionStatus.REJECTED.GetStringValue();
                            objLTF002.LNTInspectorResultOn = DateTime.Now;
                            //}
                            objLTF002.InspectionStatus = clsImplementationEnum.SeamListInspectionStatus.ReworkNotAttached.GetStringValue();
                            objLTF002.EditedOn = DateTime.Now;
                            objLTF002.EditedBy = objClsLoginInfo.UserName;
                        }
                        else
                        {
                            QMS045 objQMS045 = db.QMS045.Where(x => x.HeaderId == objQMS065.HeaderId).FirstOrDefault();

                            //Commented by Dharmesh For Issue related this Status
                            //  objQMS045.InspectionStatus = clsImplementationEnum.PartlistOfferInspectionStatus.PARTIALLY_REWORK.GetStringValue();
                            if (objQMS045.L_TInspectionResult == null)
                            {
                                objQMS045.L_TInspectionResult = clsImplementationEnum.PartlistOfferInspectionStatus.PARTIALLY_REJECTED.GetStringValue();
                                objQMS045.LNTInspectorResultOn = DateTime.Now;
                            }

                            objQMS045.EditedOn = DateTime.Now;
                            objQMS045.EditedBy = objClsLoginInfo.UserName;

                            bool flag = AddPendingQuantitiesDetails(objQMS065, objQMS036, clsImplementationEnum.PartlistOfferInspectionStatus.REJECTED.GetStringValue());
                        }

                    }
                    objQMS065.RequestStatus = clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue();
                    objQMS065.InspectedBy = objClsLoginInfo.UserName;
                    objQMS065.InspectedOn = DateTime.Now;

                    db.SaveChanges();

                    #region Send Notification
                    QMS065 objQMS065Noti = db.QMS065.Where(x => x.RequestNo == objQMS065.RequestNo && x.HeaderId == objQMS065.HeaderId).OrderByDescending(o => o.CreatedOn).FirstOrDefault();
                    if (objQMS065Noti != null)
                    {
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PROD3.GetStringValue(), objQMS065.Project, objQMS065.BU, objQMS065.Location, "NDT Request " + objQMS065.RequestNo + " for Stage: " + objQMS065.StageCode + " of Part: " + objQMS065.PartAssemblyFindNumber + " & Project No: " + objQMS065.QualityProject + " has been Rejected", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/PartlistNDEInspection/ShopOfferDetails?RequestId=" + objQMS065Noti.RequestId);
                    }
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Offer rejected successfully.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "NDE offer not available.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReturnedQuantity(int RequestId, string remark)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS065 objQMS065 = db.QMS065.Where(x => x.RequestId == RequestId).FirstOrDefault();
                QMS036 objQMS036 = db.QMS036.Where(x =>
                                                        x.QualityProject == objQMS065.QualityProject &&
                                                        x.Project == objQMS065.Project &&
                                                        x.BU == objQMS065.BU &&
                                                        x.Location == objQMS065.Location &&
                                                        x.PartNo == objQMS065.PartAssemblyFindNumber &&
                                                        // x.ParentPartNo == objQMS055.ParentPartNo &&
                                                        x.ChildPartNo == objQMS065.PartAssemblyChildItemCode &&
                                                        x.StageCode == objQMS065.StageCode).FirstOrDefault();
                if (objQMS065 != null)
                {
                    objResponseMsg = Manager.IsApplicableForAttend(objQMS065.RequestId, clsImplementationEnum.InspectionFor.PART.GetStringValue(), "NDE");
                    if (!objResponseMsg.Key)
                    {
                        return Json(objResponseMsg);
                    }

                    objQMS065.RequestStatus = clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue();
                    objQMS065.ReturnRemark = remark;
                    objQMS065.TestResult = clsImplementationEnum.PartlistOfferInspectionStatus.RETURNED.GetStringValue();
                    objQMS065.ReturnedBy = objClsLoginInfo.UserName;
                    objQMS065.ReturnedOn = DateTime.Now;
                    db.SaveChanges();
                    if (objQMS065.OfferedQuantity > 0)
                    {
                        bool flag = AddPendingQuantitiesDetails(objQMS065, objQMS036, clsImplementationEnum.PartlistOfferInspectionStatus.RETURNED.GetStringValue());
                    }
                    objQMS065.InspectedBy = objClsLoginInfo.UserName;
                    objQMS065.InspectedOn = DateTime.Now;

                    bool isLTFPS = Manager.IsLTFPSProject(objQMS065.Project, objQMS065.BU, objQMS065.Location);
                    if (isLTFPS)
                    {
                        LTF002 objLTF002 = db.LTF002.Where(x => x.LineId == objQMS065.LTFPSHeaderId).FirstOrDefault();
                        objLTF002.EditedOn = DateTime.Now;
                        objLTF002.EditedBy = objClsLoginInfo.UserName;
                    }
                    else
                    {
                        QMS045 objQMS045 = db.QMS045.Where(x => x.HeaderId == objQMS065.HeaderId).FirstOrDefault();
                        objQMS045.EditedOn = DateTime.Now;
                        objQMS045.EditedBy = objClsLoginInfo.UserName;
                    }
                    db.SaveChanges();

                    #region Send Notification
                    QMS065 objQMS065Noti = db.QMS065.Where(x => x.RequestNo == objQMS065.RequestNo && x.HeaderId == objQMS065.HeaderId).OrderByDescending(o => o.CreatedOn).FirstOrDefault();
                    if (objQMS065Noti != null)
                    {
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PROD3.GetStringValue(), objQMS065.Project, objQMS065.BU, objQMS065.Location, "NDT Request " + objQMS065.RequestNo + " for Stage: " + objQMS065.StageCode + " of Part: " + objQMS065.PartAssemblyFindNumber + " & Project No: " + objQMS065.QualityProject + " has been Returned", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/PartlistNDEInspection/ShopOfferDetails?RequestId=" + objQMS065.RequestId);
                    }
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Offer returned successfully.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Requested offer not available.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public bool AddPendingQuantitiesDetails(QMS065 qms065, QMS036 objQMS036, string Status, bool isTPI = false)
        {
            bool flag = true;
            int IterationNo = 0;
            int ReqSeqNo = 0;
            int OfferedQuantity = 0;
            bool? isQCReturned = null;

            if (Status == clsImplementationEnum.PartlistOfferInspectionStatus.REJECTED.GetStringValue())
            {
                IterationNo = Convert.ToInt32(qms065.IterationNo + 1);
                ReqSeqNo = Convert.ToInt32(qms065.RequestNoSequence);
                OfferedQuantity = Convert.ToInt32(qms065.OfferedQuantity);
            }
            else if (Status == clsImplementationEnum.PartlistOfferInspectionStatus.RETURNED.GetStringValue())
            {
                IterationNo = Convert.ToInt32(qms065.IterationNo);
                ReqSeqNo = Convert.ToInt32(qms065.RequestNoSequence + 1);
                OfferedQuantity = Convert.ToInt32(qms065.OfferedQuantity);
                isQCReturned = true;
            }

            QMS065 objQMS065 = db.QMS065.Add(new QMS065
            {
                HeaderId = qms065.HeaderId,
                QualityProject = qms065.QualityProject,
                Project = qms065.Project,
                BU = qms065.BU,
                Location = qms065.Location,
                PartAssemblyFindNumber = qms065.PartAssemblyFindNumber,
                PartAssemblyChildItemCode = qms065.PartAssemblyChildItemCode,
                ChildItemCodeDesc = qms065.ChildItemCodeDesc,
                StageCode = qms065.StageCode,
                StageSequence = qms065.StageSequence,
                IterationNo = IterationNo,
                RequestNo = qms065.RequestNo,
                RequestNoSequence = ReqSeqNo,
                RequestStatus = clsImplementationEnum.NDESeamRequestStatus.NotAttended.GetStringValue(),
                ManufacturingCode = db.QMS010.Where(x => x.QualityProject == qms065.QualityProject).Select(x => x.ManufacturingCode).FirstOrDefault(),
                Shop = qms065.Shop,
                ShopLocation = qms065.ShopLocation,
                ShopRemark = qms065.ShopRemark,
                //RequestGeneratedBy = qms065.RequestGeneratedBy,
                //RequestGeneratedOn = qms065.RequestGeneratedOn,
                OfferedBy = qms065.OfferedBy,
                OfferedOn = qms065.OfferedOn,
                ConfirmedBy = isTPI ? qms065.ConfirmedBy : null,
                ConfirmedOn = isTPI ? qms065.ConfirmedOn : null,
                TestResult = isTPI ? "Returned by TPI" : null,
                TPIAgency1 = objQMS036 != null ? objQMS036.FirstTPIAgency : null,
                TPIAgency1Intervention = objQMS036 != null ? objQMS036.FirstTPIIntervention : null,
                TPIAgency2 = objQMS036 != null ? objQMS036.SecondTPIAgency : null,
                TPIAgency2Intervention = objQMS036 != null ? objQMS036.SecondTPIIntervention : null,
                TPIAgency3 = objQMS036 != null ? objQMS036.ThirdTPIAgency : null,
                TPIAgency3Intervention = objQMS036 != null ? objQMS036.ThirdTPIIntervention : null,
                TPIAgency4 = objQMS036 != null ? objQMS036.FourthTPIAgency : null,
                TPIAgency4Intervention = objQMS036 != null ? objQMS036.FourthTPIIntervention : null,
                TPIAgency5 = objQMS036 != null ? objQMS036.FifthTPIAgency : null,
                TPIAgency5Intervention = objQMS036 != null ? objQMS036.FifthTPIIntervention : null,
                TPIAgency6 = objQMS036 != null ? objQMS036.SixthTPIAgency : null,
                TPIAgency6Intervention = objQMS036 != null ? objQMS036.SixthTPIIntervention : null,
                OfferedQuantity = OfferedQuantity,
                CreatedBy = objClsLoginInfo.UserName,
                CreatedOn = DateTime.Now,
                NDETechniqueNo = null,
                NDETechniqueRevisionNo = null,
                LTFPSHeaderId = qms065.LTFPSHeaderId,
                QCIsReturned = isQCReturned
            });
            db.SaveChanges();
            return flag;
        }

        public string GetInspectionStatus(int refHeaderID, int totalQty, bool IsTPI = false)
        {
            string strInspectionStatus = string.Empty;
            int sumClearedQuantities = 0;
            string testResult = clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue();
            List<QMS065> lstQMS065 = db.QMS065.Where(x => x.HeaderId == refHeaderID && x.TestResult == testResult).ToList();
            if (lstQMS065 != null && lstQMS065.Count > 0)
            {
                sumClearedQuantities = Convert.ToInt32(lstQMS065.Sum(x => x.OfferedQuantity));
                if (totalQty == sumClearedQuantities)
                {
                    strInspectionStatus = clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue();
                }
            }
            return strInspectionStatus;
        }

        public string GetLTFPSInspectionStatus(int refLTFPSId, int totalQty, bool IsTPI = false)
        {
            string strInspectionStatus = string.Empty;
            int sumClearedQuantities = 0;
            string testResult = clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue();
            List<QMS065> lstQMS065 = db.QMS065.Where(x => x.LTFPSHeaderId == refLTFPSId && x.TestResult == testResult).ToList();
            if (lstQMS065 != null && lstQMS065.Count > 0)
            {
                sumClearedQuantities = Convert.ToInt32(lstQMS065.Sum(x => x.OfferedQuantity));
                if (totalQty == sumClearedQuantities)
                {
                    strInspectionStatus = clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue();
                }
            }
            return strInspectionStatus;
        }

        #region Commented Grid

        //[SessionExpireFilter]
        //public ActionResult AscanUTDetails()
        //{
        //    return View();
        //}

        //[HttpPost]
        //public ActionResult LoadPartlistAscanUTDataPartial(string status)
        //{
        //    ViewBag.Status = status;
        //    return PartialView("_LoadPartlistAscanUTDataPartial");
        //}

        //[HttpPost]
        //public JsonResult LoadPartlistNDEAscanUTData(JQueryDataTableParamModel param)
        //{
        //    try
        //    {
        //        var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
        //        var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
        //        int StartIndex = param.iDisplayStart + 1;
        //        int EndIndex = param.iDisplayStart + param.iDisplayLength;

        //        string strWhere = string.Empty;

        //        if (param.CTQCompileStatus.ToUpper() == "PENDING")
        //        {
        //            strWhere += "1=1";
        //        }
        //        else
        //        {
        //            strWhere += "1=1";
        //        }
        //        if (!string.IsNullOrWhiteSpace(param.sSearch))
        //        {
        //            string[] arrayLikeCon = {
        //                                        "qms090.Project+' - '+com1.t_dsca",
        //                                        "QualityProject"
        //                                    };
        //            strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
        //        }
        //        strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms090.BU", "qms090.Location");
        //        var lstResult = db.SP_FETCH_IPI_NDE_PARTLIST_ASCANUT_PARAMETERS_HEADER_RESULT
        //                        (
        //                        StartIndex, EndIndex, "", strWhere
        //                        ).ToList();

        //        var data = (from uc in lstResult
        //                    select new[]
        //                   {
        //                        Convert.ToString(uc.QualityProject),
        //                        Convert.ToString(uc.Project),
        //                        Convert.ToString(uc.BU),
        //                        Convert.ToString(uc.Location),
        //                        Convert.ToString(uc.PartNo),
        //                        Convert.ToString(uc.ParentPartNo),
        //                        Convert.ToString(uc.ChildPartNo),
        //                        Convert.ToString(uc.StageCode),
        //                        Convert.ToString(uc.StageSequence),
        //                        Convert.ToString(uc.IterationNo),
        //                        Convert.ToString(uc.RequestNo),
        //                        Convert.ToString(uc.RequestNoSequence),
        //                        Convert.ToString(uc.UTTechNo),
        //                        Convert.ToString(uc.RevNo),
        //                        Convert.ToString(uc.Status),
        //                        Convert.ToString(uc.UTProcedureNo),
        //                        Convert.ToString(uc.RecordableIndication),
        //                        Convert.ToString(uc.ScanTechnique),
        //                        Convert.ToString(uc.DGSDiagram),
        //                        Convert.ToString(uc.BasicCalibrationBlock),
        //                        Convert.ToString(uc.SimulationBlock1),
        //                        Convert.ToString(uc.SimulationBlock2),
        //                        Convert.ToString(uc.CouplantUsed),
        //                        Convert.ToString(uc.ScanPlan),
        //                        Convert.ToString(uc.ScanningSensitivity),
        //                        Convert.ToString(uc.PlannerRemarks),
        //                        Convert.ToString(uc.ApproverRemarks),
        //                        Convert.ToString(uc.TestInstrument),
        //                        Convert.ToString(uc.ExaminationDate),
        //                        Convert.ToString(uc.NDTDuration),
        //                        Convert.ToString(uc.Rejectcontrol),
        //                        Convert.ToString(uc.Damping),
        //                        Convert.ToString(uc.RestrictedAccessAreaInaccessibleWelds),
        //                        Convert.ToString(uc.TransferCorrection),
        //                        Convert.ToString(uc.TransferCorrectiondB),
        //                        Convert.ToString(uc.Noofpoints),
        //                        Convert.ToString(uc.AfterTestCalibrationrechecked),
        //                        Convert.ToString(uc.AnySpecialAccessoriesUsed),
        //                        Convert.ToString(uc.AnyRecordableIndication),
        //                        Convert.ToString(uc.NDTremarks),
        //                        Convert.ToString(uc.CreatedBy),
        //                        Convert.ToString(uc.CreatedOn),
        //                        Convert.ToString(uc.EditedBy),
        //                        Convert.ToString(uc.EditedOn),
        //                       "<center><a class='btn btn-xs green' href='/IPI/PartlistNDEInspection/AscanUTViewDetails?HeaderId="+Convert.ToInt32(uc.HeaderId)+"'>View<i style='margin-left:5px;' class='fa fa-eye'></i></a></center>"
        //                   }).ToList();

        //        return Json(new
        //        {
        //            sEcho = Convert.ToInt32(param.sEcho),
        //            iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
        //            iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
        //            aaData = data
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new
        //        {
        //            sEcho = param.sEcho,
        //            iTotalRecords = 0,
        //            iTotalDisplayRecords = 0,
        //            aaData = ""
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //}

        #endregion

        #region Ascan UT

        [SessionExpireFilter]
        public ActionResult AscanUTViewDetails(int HeaderId = 0)
        {
            if (HeaderId > 0)
            {
                QMS090 objQMS090 = db.QMS090.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                if (objQMS090 != null)
                {
                    ViewBag.HeaderID = HeaderId;
                    return View(objQMS090);
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
            }
        }

        [HttpPost]
        public ActionResult LoadAscanUTFormPartial(int HeaderID)
        {
            QMS090 objQMS090 = new QMS090();
            if (HeaderID > 0)
            {
                NDEModels objNDEModels = new NDEModels();
                objQMS090 = db.QMS090.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
                objQMS090.ScanTechnique = objNDEModels.GetCategory("Scan Technique", objQMS090.ScanTechnique, objQMS090.BU, objQMS090.Location, true).CategoryDescription;
                objQMS090.CouplantUsed = objNDEModels.GetCategory("Couplant", objQMS090.CouplantUsed, objQMS090.BU, objQMS090.Location, true).CategoryDescription;
                objQMS090.SimulationBlock1 = objNDEModels.GetCategory("Simulation Block", objQMS090.SimulationBlock1, objQMS090.BU, objQMS090.Location, true).CategoryDescription;
                objQMS090.ScanningSensitivity = objNDEModels.GetCategory("Scanning Sensitivity", objQMS090.ScanningSensitivity, objQMS090.BU, objQMS090.Location, true).CategoryDescription;
                objQMS090.BasicCalibrationBlock = objNDEModels.GetCategory("Basic Calibration Block", objQMS090.BasicCalibrationBlock, objQMS090.BU, objQMS090.Location, true).CategoryDescription;
                objQMS090.RecordableIndication = objNDEModels.GetCategory("Recordable Indication At Reference Gain", objQMS090.RecordableIndication, objQMS090.BU, objQMS090.Location, true).CategoryDescription;

                string strBU = db.COM001.Where(i => i.t_cprj == objQMS090.Project).FirstOrDefault().t_entu;
                var BUDescription = (from a in db.COM002
                                     where a.t_dtyp == 2 && a.t_dimx == strBU
                                     select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
                objQMS090.BU = BUDescription.BUDesc;
                var locDescription = (from a in db.COM002
                                      where a.t_dtyp == 1 && a.t_dimx == objQMS090.Location
                                      select new { Location = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
                objQMS090.Location = locDescription.Location;

                List<string> lstyesno = clsImplementationEnum.getyesno().ToList();

                ViewBag.lstyesno = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();
                var projectDetails = db.COM001.Where(x => x.t_cprj == objQMS090.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                ViewBag.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;

                if (ISRequestAttended(objQMS090.RefHeaderId))
                {
                    ViewBag.isDisabled = "disabled";
                }
            }
            return PartialView("_LoadAscanUTFormPartial", objQMS090);
        }

        [HttpPost]
        public ActionResult GetAscanUTLinesForm(int LineId)
        {
            QMS091 objQMS091 = db.QMS091.Where(x => x.LineId == LineId).FirstOrDefault();
            var projectDetails = db.COM001.Where(x => x.t_cprj == objQMS091.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
            string projectDesc = projectDetails.t_cprj + " - " + projectDetails.t_dsca;

            string BU = db.COM001.Where(i => i.t_cprj == objQMS091.Project).FirstOrDefault().t_entu;
            var BUDescription = (from a in db.COM002
                                 where a.t_dtyp == 2 && a.t_dimx == objQMS091.BU
                                 select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();

            var locDescription = (from a in db.COM002
                                  where a.t_dtyp == 1 && a.t_dimx == objQMS091.Location
                                  select new { Location = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
            ViewBag.Location = locDescription.Location;

            ViewBag.Project = projectDesc;
            ViewBag.BU = BUDescription.BUDesc;
            return PartialView("_GetAscanUTLinesForm", objQMS091);
        }

        [HttpPost]
        public JsonResult LoadAscanUTLineData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {

                }
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName);
                var lstResult = db.SP_FETCH_IPI_NDE_PARTLIST_ASCANUT_PARAMETERS_LINES_RESULT
                                (
                               StartIndex,
                               EndIndex,
                               "",
                               strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.QualityProject),
                                Convert.ToString(uc.Project),
                                Convert.ToString(uc.BU),
                                Convert.ToString(uc.Location),
                                Convert.ToString(uc.PartNo),
                                Convert.ToString(uc.ParentPartNo),
                                Convert.ToString(uc.ChildPartNo),
                                Convert.ToString(uc.StageCode),
                                Convert.ToString(uc.StageSequence),
                                Convert.ToString(uc.IterationNo),
                                Convert.ToString(uc.RequestNo),
                                Convert.ToString(uc.RequestNoSequence),
                                Convert.ToString(uc.UTTechNo),
                                Convert.ToString(uc.RevNo),
                                Convert.ToString(uc.SearchUnit),
                                Convert.ToString(uc.ReferenceReflector),
                                Convert.ToString(uc.CableType),
                                Convert.ToString(uc.CableLength),
                                Convert.ToString(uc.ReferencegaindB),
                                Convert.ToString(uc.Range),
                                Convert.ToString(uc.AmpInd1),
                                Convert.ToString(uc.BPInd1),
                                Convert.ToString(uc.AmpInd2),
                                Convert.ToString(uc.BPInd2),
                                Convert.ToString(uc.AmpInd3),
                                Convert.ToString(uc.BPInd3),
                                Convert.ToString(uc.AmpInd4),
                                Convert.ToString(uc.BPInd4),
                                Convert.ToString(uc.AmpInd5),
                                Convert.ToString(uc.BPInd5),
                                Convert.ToString(uc.AmpInd6),
                                Convert.ToString(uc.BPInd6),
                                Convert.ToString(uc.CreatedBy),
                                Convert.ToString(uc.CreatedOn),
                                Convert.ToString(uc.EditedBy),
                                Convert.ToString(uc.EditedOn),
                                param.IsVisible?
                                "<center><i  data-modal=\"\" id=\"btnEdit\" name=\"btnAction\" Title=\"Edit Record\" class=\"disabledicon fa fa-pencil-square-o\"></i></center>" :
                                "<center><i  data-modal=\"\" id=\"btnEdit\" name=\"btnAction\" style=\"cursor:pointer;\" Title=\"Edit Record\" class=\"fa fa-pencil-square-o\"  onclick=\"GetAscanUTLinesForm("+uc.LineId+")\" ></i>&nbsp;&nbsp;</center>"//Convert.ToString(uc.LineId)
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = 0,
                    iTotalDisplayRecords = 0,
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdateAscanHeader(QMS090 qms090)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (qms090.HeaderId > 0)
                {
                    QMS090 objQMS090 = db.QMS090.Where(x => x.HeaderId == qms090.HeaderId).FirstOrDefault();
                    objQMS090.TestInstrument = qms090.TestInstrument;
                    objQMS090.ExaminationDate = qms090.ExaminationDate;
                    objQMS090.NDTDuration = qms090.NDTDuration;
                    objQMS090.Rejectcontrol = qms090.Rejectcontrol;
                    objQMS090.Damping = qms090.Damping;
                    objQMS090.RestrictedAccessAreaInaccessibleWelds = qms090.RestrictedAccessAreaInaccessibleWelds;
                    objQMS090.TransferCorrection = qms090.TransferCorrection;
                    objQMS090.TransferCorrectiondB = qms090.TransferCorrectiondB;
                    objQMS090.Noofpoints = qms090.Noofpoints;
                    objQMS090.AveragedB = qms090.AveragedB;
                    objQMS090.AfterTestCalibrationrechecked = qms090.AfterTestCalibrationrechecked;
                    objQMS090.AnySpecialAccessoriesUsed = qms090.AnySpecialAccessoriesUsed;
                    objQMS090.AnyRecordableIndication = qms090.AnyRecordableIndication;
                    objQMS090.NDTremarks = qms090.NDTremarks;
                    objQMS090.EditedBy = objClsLoginInfo.UserName;
                    objQMS090.EditedOn = DateTime.Now;
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Document update successfully.";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveAscanUTLines(QMS091 qms091)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS091 objQMS091 = db.QMS091.Where(x => x.LineId == qms091.LineId).FirstOrDefault();
                if (objQMS091 != null)
                {
                    objQMS091.ReferencegaindB = qms091.ReferencegaindB;
                    objQMS091.Range = qms091.Range;
                    objQMS091.AmpInd1 = qms091.AmpInd1;
                    objQMS091.BPInd1 = qms091.BPInd1;
                    objQMS091.AmpInd2 = qms091.AmpInd2;
                    objQMS091.BPInd2 = qms091.BPInd2;
                    objQMS091.AmpInd3 = qms091.AmpInd3;
                    objQMS091.BPInd3 = qms091.BPInd3;
                    objQMS091.AmpInd4 = qms091.AmpInd4;
                    objQMS091.BPInd4 = qms091.BPInd4;
                    objQMS091.AmpInd5 = qms091.AmpInd5;
                    objQMS091.BPInd5 = qms091.BPInd5;
                    objQMS091.AmpInd6 = qms091.AmpInd6;
                    objQMS091.BPInd6 = qms091.BPInd6;
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Line update successfully.";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Commented Grid

        //[SessionExpireFilter]
        //public ActionResult TOFDUTDetails()
        //{
        //    return View();
        //}

        //[HttpPost]
        //public ActionResult LoadPartlistTOFDUTDataPartial(string status)
        //{
        //    ViewBag.Status = status;
        //    return PartialView("_LoadPartlistTOFDUTDataPartial");
        //}

        //[HttpPost]
        //public JsonResult LoadPartlistNDETOFDUTData(JQueryDataTableParamModel param)
        //{
        //    try
        //    {
        //        var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
        //        var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
        //        int StartIndex = param.iDisplayStart + 1;
        //        int EndIndex = param.iDisplayStart + param.iDisplayLength;

        //        string strWhere = string.Empty;

        //        if (param.CTQCompileStatus.ToUpper() == "PENDING")
        //        {
        //            strWhere += "1=1";
        //        }
        //        else
        //        {
        //            strWhere += "1=1";
        //        }
        //        if (!string.IsNullOrWhiteSpace(param.sSearch))
        //        {
        //            string[] arrayLikeCon = {
        //                                        "qms092.Project+' - '+com1.t_dsca",
        //                                        "QualityProject"
        //                                    };
        //            strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
        //        }
        //        strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms092.BU", "qms092.Location");
        //        var lstResult = db.SP_FETCH_IPI_TOFD_TEST_HEADER_RESULT
        //                        (
        //                        StartIndex, EndIndex, "", strWhere
        //                        ).ToList();

        //        var data = (from uc in lstResult
        //                    select new[]
        //                   {
        //                        Convert.ToString(uc.QualityProject),
        //                        Convert.ToString(uc.Project),
        //                        Convert.ToString(uc.BU),
        //                        Convert.ToString(uc.Location),
        //                        Convert.ToString(uc.PartNo),
        //                        Convert.ToString(uc.ParentPartNo),
        //                        Convert.ToString(uc.ChildPartNo),
        //                        Convert.ToString(uc.StageCode),
        //                        Convert.ToString(uc.StageSequence),
        //                        Convert.ToString(uc.IterationNo),
        //                        Convert.ToString(uc.RequestNo),
        //                        Convert.ToString(uc.RequestNoSequence),
        //                        Convert.ToString(uc.UTTechNo),
        //                        Convert.ToString(uc.RevNo),
        //                        Convert.ToString(uc.Status),
        //                        Convert.ToString(uc.UTProcedureNo),
        //                        Convert.ToString(uc.CouplantUsed),
        //                        Convert.ToString(uc.ReferenceReflectorSize),
        //                        Convert.ToString(uc.ReferenceReflectorDistance),
        //                        Convert.ToString(uc.CalibrationBlockID),
        //                        Convert.ToString(uc.CalibrationBlockIDThickness),
        //                        Convert.ToString(uc.ScanningDetail),
        //                        Convert.ToString(uc.EncoderType),
        //                        Convert.ToString(uc.ScannerType),
        //                        Convert.ToString(uc.ScannerMake),
        //                        Convert.ToString(uc.CableType),
        //                        Convert.ToString(uc.CableLength),
        //                        Convert.ToString(uc.ScanPlan),
        //                        Convert.ToString(uc.PlannerRemarks),
        //                        Convert.ToString(uc.ApproverRemarks),
        //                        Convert.ToString(uc.TestInstrument),
        //                        Convert.ToString(uc.ExaminationDate),
        //                        Convert.ToString(uc.NDTDuration),
        //                        Convert.ToString(uc.DataSamplingSpacing),
        //                        Convert.ToString(uc.DataStorageLocation),
        //                        Convert.ToString(uc.ScanningStartLocation),
        //                        Convert.ToString(uc.EncoderCalibration),
        //                        Convert.ToString(uc.Software),
        //                        Convert.ToString(uc.Version),
        //                        Convert.ToString(uc.ScanningSurface),
        //                        Convert.ToString(uc.FinalDisplayProcessingLevels),
        //                        Convert.ToString(uc.CompAscanUT),
        //                        Convert.ToString(uc.AfterTestCalibrationrechecked),
        //                        Convert.ToString(uc.AnySpecialAccessoriesUsed),
        //                        Convert.ToString(uc.AnyRecordableIndication),
        //                        Convert.ToString(uc.NDTremarks),
        //                        Convert.ToString(uc.CreatedBy),
        //                        Convert.ToString(uc.CreatedOn),
        //                        Convert.ToString(uc.EditedBy),
        //                        Convert.ToString(uc.EditedOn),
        //                       "<center><a class='btn btn-xs green' href='/IPI/PartlistNDEInspection/TOFDUTViewDetails?HeaderId="+Convert.ToInt32(uc.HeaderId)+"'>View<i style='margin-left:5px;' class='fa fa-eye'></i></a></center>"
        //                   }).ToList();

        //        return Json(new
        //        {
        //            sEcho = Convert.ToInt32(param.sEcho),
        //            iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
        //            iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
        //            aaData = data
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new
        //        {
        //            sEcho = param.sEcho,
        //            iTotalRecords = 0,
        //            iTotalDisplayRecords = 0,
        //            aaData = ""
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //}

        #endregion

        #region TOFD UT

        [SessionExpireFilter]
        public ActionResult TOFDUTViewDetails(int HeaderId = 0)
        {
            if (HeaderId > 0)
            {
                QMS092 objQMS092 = db.QMS092.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                if (objQMS092 != null)
                {
                    ViewBag.HeaderID = HeaderId;
                    return View(objQMS092);
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
            }
        }

        [HttpPost]
        public ActionResult LoadTOFDUTFormPartial(int HeaderID)
        {
            QMS092 objQMS092 = new QMS092();
            if (HeaderID > 0)
            {
                NDEModels objNDEModels = new NDEModels();
                objQMS092 = db.QMS092.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
                objQMS092.CouplantUsed = objNDEModels.GetCategory("Couplant", objQMS092.CouplantUsed, objQMS092.BU, objQMS092.Location, true).CategoryDescription;
                objQMS092.CableType = objNDEModels.GetCategory("Cable Type", objQMS092.CableType, objQMS092.BU, objQMS092.Location, true).CategoryDescription;
                objQMS092.CableLength = objNDEModels.GetCategory("Cable Length", objQMS092.CableLength, objQMS092.BU, objQMS092.Location, true).CategoryDescription;
                objQMS092.ScanningDetail = objNDEModels.GetCategory("Scanning Detail", objQMS092.ScanningDetail, objQMS092.BU, objQMS092.Location, true).CategoryDescription;
                objQMS092.ReferenceReflectorSize = objNDEModels.GetCategory("Reference Reflector Size", objQMS092.ReferenceReflectorSize, objQMS092.BU, objQMS092.Location, true).CategoryDescription;



                string strBU = db.COM001.Where(i => i.t_cprj == objQMS092.Project).FirstOrDefault().t_entu;
                var BUDescription = (from a in db.COM002
                                     where a.t_dtyp == 2 && a.t_dimx == strBU
                                     select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
                objQMS092.BU = BUDescription.BUDesc;
                var locDescription = (from a in db.COM002
                                      where a.t_dtyp == 1 && a.t_dimx == objQMS092.Location
                                      select new { Location = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
                objQMS092.Location = locDescription.Location;

                var projectDetails = db.COM001.Where(x => x.t_cprj == objQMS092.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                ViewBag.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;

                List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
                ViewBag.lstyesno = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();
                if (ISRequestAttended(objQMS092.RefHeaderId))
                {
                    ViewBag.isDisabled = "disabled";
                }
            }
            return PartialView("_LoadTOFDUTFormPartial", objQMS092);
        }

        [HttpPost]
        public ActionResult GetTOFDUTLinesForm(int LineId)
        {
            QMS093 objQMS093 = db.QMS093.Where(x => x.LineId == LineId).FirstOrDefault();
            var projectDetails = db.COM001.Where(x => x.t_cprj == objQMS093.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
            string projectDesc = projectDetails.t_cprj + " - " + projectDetails.t_dsca;

            string BU = db.COM001.Where(i => i.t_cprj == objQMS093.Project).FirstOrDefault().t_entu;
            var BUDescription = (from a in db.COM002
                                 where a.t_dtyp == 2 && a.t_dimx == objQMS093.BU
                                 select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();

            var locDescription = (from a in db.COM002
                                  where a.t_dtyp == 1 && a.t_dimx == objQMS093.Location
                                  select new { Location = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
            ViewBag.Location = locDescription.Location;

            ViewBag.Project = projectDesc;
            ViewBag.BU = BUDescription.BUDesc;
            return PartialView("_GetTOFDUTLinesForm", objQMS093);
        }

        [HttpPost]
        public JsonResult LoadTOFDUTLineData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {

                }
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName);
                var lstResult = db.SP_FETCH_IPI_TOFD_TEST_LINES_RESULT
                                (
                               StartIndex,
                               EndIndex,
                               "",
                               strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.QualityProject),
                                Convert.ToString(uc.Project),
                                Convert.ToString(uc.BU),
                                Convert.ToString(uc.Location),
                                Convert.ToString(uc.PartNo),
                                Convert.ToString(uc.ParentPartNo),
                                Convert.ToString(uc.ChildPartNo),
                                Convert.ToString(uc.StageCode),
                                Convert.ToString(uc.StageSequence),
                                Convert.ToString(uc.IterationNo),
                                Convert.ToString(uc.RequestNo),
                                Convert.ToString(uc.RequestNoSequence),
                                Convert.ToString(uc.UTTechNo),
                                Convert.ToString(uc.RevNo),
                                Convert.ToString(uc.ScanNo),
                                Convert.ToString(uc.RefRevNo),
                                Convert.ToString(uc.SearchUnit),
                                Convert.ToString(uc.ScanType),
                                Convert.ToString(uc.PCS),
                                Convert.ToString(uc.ReferencegaindB),
                                Convert.ToString(uc.CreatedBy),
                                Convert.ToString(uc.CreatedOn),
                                Convert.ToString(uc.EditedBy),
                                Convert.ToString(uc.EditedOn),
                                param.IsVisible?
                                "<center><i  data-modal=\"\" id=\"btnEdit\" name=\"btnAction\" Title=\"Edit Record\" class=\"disabledicon fa fa-pencil-square-o\"   ></i></center>" :
                                "<center><i  data-modal=\"\" id=\"btnEdit\" name=\"btnAction\" style=\"cursor:pointer;\" Title=\"Edit Record\" class=\"iconspace fa fa-pencil-square-o\"  onclick=\"GetTOFDUTLinesForm("+uc.LineId+")\" ></i></center>"//Convert.ToString(uc.LineId)
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = 0,
                    iTotalDisplayRecords = 0,
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdateTOFDHeader(QMS092 qms092)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (qms092.HeaderId > 0)
                {
                    QMS092 objQMS092 = db.QMS092.Where(x => x.HeaderId == qms092.HeaderId).FirstOrDefault();
                    objQMS092.TestInstrument = qms092.TestInstrument;
                    objQMS092.ExaminationDate = qms092.ExaminationDate;
                    objQMS092.NDTDuration = qms092.NDTDuration;
                    objQMS092.DataSamplingSpacing = qms092.DataSamplingSpacing;
                    objQMS092.DataStorageLocation = qms092.DataStorageLocation;
                    objQMS092.ScanningStartLocation = qms092.ScanningStartLocation;
                    objQMS092.EncoderCalibration = qms092.EncoderCalibration;
                    objQMS092.Software = qms092.Software;
                    objQMS092.Version = qms092.Version;
                    objQMS092.ScanningSurface = qms092.ScanningSurface;
                    objQMS092.FinalDisplayProcessingLevels = qms092.FinalDisplayProcessingLevels;
                    objQMS092.AfterTestCalibrationrechecked = qms092.AfterTestCalibrationrechecked;
                    objQMS092.AnySpecialAccessoriesUsed = qms092.AnySpecialAccessoriesUsed;
                    objQMS092.AnyRecordableIndication = qms092.AnyRecordableIndication;
                    objQMS092.NDTremarks = qms092.NDTremarks;
                    objQMS092.EditedBy = objClsLoginInfo.UserName;
                    objQMS092.EditedOn = DateTime.Now;
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Document update successfully.";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveTOFDLines(QMS093 qms093)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS093 objQMS093 = db.QMS093.Where(x => x.LineId == qms093.LineId).FirstOrDefault();
                if (objQMS093 != null)
                {
                    objQMS093.ReferencegaindB = qms093.ReferencegaindB;
                    objQMS093.EditedBy = objClsLoginInfo.UserName;
                    objQMS093.EditedOn = DateTime.Now;
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Line update successfully.";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Commented Grid

        [SessionExpireFilter]
        public ActionResult PAUTDetails()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadPartlistPAUTDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_LoadPartlistPAUTDataPartial");
        }

        [HttpPost]
        public JsonResult LoadPartlistNDEPAUTData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;

                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1";
                }
                else
                {
                    strWhere += "1=1";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "qms094.Project+' - '+com1.t_dsca",
                                                "QualityProject"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms094.BU", "qms094.Location");
                var lstResult = db.SP_FETCH_IPI_PAUT_TEST_HEADER_RESULT
                                (
                                StartIndex, EndIndex, "", strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.QualityProject),
                                Convert.ToString(uc.Project),
                                Convert.ToString(uc.BU),
                                Convert.ToString(uc.Location),
                                Convert.ToString(uc.PartNo),
                                Convert.ToString(uc.ParentPartNo),
                                Convert.ToString(uc.ChildPartNo),
                                Convert.ToString(uc.StageCode),
                                Convert.ToString(uc.StageSequence),
                                Convert.ToString(uc.IterationNo),
                                Convert.ToString(uc.RequestNo),
                                Convert.ToString(uc.RequestNoSequence),
                                Convert.ToString(uc.UTTechNo),
                                Convert.ToString(uc.RevNo),
                                Convert.ToString(uc.Status),
                                Convert.ToString(uc.UTProcedureNo),
                                Convert.ToString(uc.CouplantUsed),
                                Convert.ToString(uc.ReferenceReflectorSize),
                                Convert.ToString(uc.ReferenceReflectorDistance),
                                Convert.ToString(uc.CalibrationBlockID),
                                Convert.ToString(uc.CalibrationBlockIDThickness),
                                Convert.ToString(uc.SimulationBlockID),
                                Convert.ToString(uc.SimulationBlockIDThickness),
                                Convert.ToString(uc.EncoderType),
                                Convert.ToString(uc.ScannerType),
                                Convert.ToString(uc.ScannerMake),
                                Convert.ToString(uc.CableType),
                                Convert.ToString(uc.CableLength),
                                Convert.ToString(uc.ScanPlan),
                                Convert.ToString(uc.PlannerRemarks),
                                Convert.ToString(uc.ApproverRemarks),
                                Convert.ToString(uc.TestInstrument),
                                Convert.ToString(uc.ExaminationDate),
                                Convert.ToString(uc.NDTDuration),
                                Convert.ToString(uc.DataSamplingSpacing),
                                Convert.ToString(uc.DataStorageLocation),
                                Convert.ToString(uc.ScanningStartLocation),
                                Convert.ToString(uc.EncoderCalibration),
                                Convert.ToString(uc.Software),
                                Convert.ToString(uc.Version),
                                Convert.ToString(uc.ScanningSurface),
                                Convert.ToString(uc.FinalDisplayProcessingLevels),
                                Convert.ToString(uc.CompAscanUT),
                                Convert.ToString(uc.AfterTestCalibrationrechecked),
                                Convert.ToString(uc.AnySpecialAccessoriesUsed),
                                Convert.ToString(uc.AnyRecordableIndication),
                                Convert.ToString(uc.NDTremarks),
                                Convert.ToString(uc.CreatedBy),
                                Convert.ToString(uc.CreatedOn),
                                Convert.ToString(uc.EditedBy),
                                Convert.ToString(uc.EditedOn),
                               "<center><a class='btn btn-xs green' href='" + WebsiteURL + "/IPI/PartlistNDEInspection/PAUTViewDetails?HeaderId="+Convert.ToInt32(uc.HeaderId)+"'>View<i style='margin-left:5px;' class='fa fa-eye'></i></a></center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = 0,
                    iTotalDisplayRecords = 0,
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region PAUT

        [SessionExpireFilter]
        public ActionResult PAUTViewDetails(int HeaderId = 0)
        {
            if (HeaderId > 0)
            {
                QMS094 objQMS094 = db.QMS094.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                if (objQMS094 != null)
                {
                    ViewBag.HeaderID = HeaderId;
                    return View(objQMS094);
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
            }
        }

        [HttpPost]
        public ActionResult LoadPAUTFormPartial(int HeaderID)
        {
            QMS094 objQMS094 = new QMS094();
            if (HeaderID > 0)
            {
                NDEModels objNDEModels = new NDEModels();
                objQMS094 = db.QMS094.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
                objQMS094.CouplantUsed = objNDEModels.GetCategory("Couplant", objQMS094.CouplantUsed, objQMS094.BU, objQMS094.Location, true).CategoryDescription;
                objQMS094.ReferenceReflectorSize = objNDEModels.GetCategory("Reflector Size", objQMS094.ReferenceReflectorSize, objQMS094.BU, objQMS094.Location, true).CategoryDescription;
                objQMS094.CableType = objNDEModels.GetCategory("Cable Type", objQMS094.CableType, objQMS094.BU, objQMS094.Location, true).CategoryDescription;
                objQMS094.CableLength = objNDEModels.GetCategory("Cable Length", objQMS094.CableLength, objQMS094.BU, objQMS094.Location, true).CategoryDescription;
                //  objQMS094.ScanningDetail = objNDEModels.GetCategory("Scanning Detail", objQMS094.ScanningTechnique, objQMS094.BU, objQMS094.Location, true).CategoryDescription;


                string strBU = db.COM001.Where(i => i.t_cprj == objQMS094.Project).FirstOrDefault().t_entu;
                var BUDescription = (from a in db.COM002
                                     where a.t_dtyp == 2 && a.t_dimx == strBU
                                     select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
                objQMS094.BU = BUDescription.BUDesc;
                var locDescription = (from a in db.COM002
                                      where a.t_dtyp == 1 && a.t_dimx == objQMS094.Location
                                      select new { Location = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
                objQMS094.Location = locDescription.Location;

                var projectDetails = db.COM001.Where(x => x.t_cprj == objQMS094.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                ViewBag.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
                ViewBag.lstyesno = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();
                if (ISRequestAttended(objQMS094.RefHeaderId))
                {
                    ViewBag.isDisabled = "disabled";
                }
            }
            return PartialView("_LoadPAUTFormPartial", objQMS094);
        }

        [HttpPost]
        public JsonResult LoadPAUTLineData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {

                }
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName);
                var lstResult = db.SP_FETCH_IPI_PAUT_TEST_LINES_RESULT
                                (
                               StartIndex,
                               EndIndex,
                               "",
                               strWhere
                                ).ToList();

                List<Tuple<string, string>> lstScanningSurface = new List<Tuple<string, string>>
                {
                    Tuple.Create("Outside", "Outside"),
                    Tuple.Create("Inside", "Inside"),
                    Tuple.Create("Both", "Both")
                };
                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.ZoneNo),
                                Convert.ToString(uc.Probe),
                                Convert.ToString(uc.Wedge),
                                Convert.ToString(uc.NearRefractedArea),
                                Convert.ToString(uc.Skip),
                                Convert.ToString(uc.ScanType),
                                param.IsVisible? uc.ScannedSpots:"<input type='text' name='txtScannedSpots' maxlength = \"40\" class='form-control input-sm input-small input-inline clsEdit' value='"+Convert.ToString(uc.ScannedSpots)+"' spara='"+uc.LineId+"' id='txtScannedSpots_"+uc.LineId+"' />",//Convert.ToString(uc.ScannedSpots),
                                param.IsVisible? uc.ScannedLength:"<input type='text' name='txtScannedLength' maxlength = \"40\" class='form-control input-sm input-small input-inline clsEdit' value='"+Convert.ToString(uc.ScannedLength)+"' spara='"+uc.LineId+"' id='txtScannedLength_"+uc.LineId+"' />",// Convert.ToString(uc.ScannedLength),
                                param.IsVisible?uc.ScanningSurface:  Manager.MakeHTMLDropDownControl("form-control clsEdit","txtScanningSurface","txtScanningSurface_"+uc.LineId+"",uc.ScanningSurface,lstScanningSurface,true,uc.LineId),
                               //"<input type='text' name='txtScanningSurface' maxlength = \"15\" class='form-control input-sm input-small input-inline clsEdit' value='"+Convert.ToString(uc.ScanningSurface)+"' spara='"+uc.LineId+"' id='txtScanningSurface_"+uc.LineId+"' />", //Convert.ToString(uc.ScanningSurface),
                                param.IsVisible?Convert.ToString(uc.ReferencegaindB): "<input type='text' name='txtReferencegaindB' class='form-control input-sm input-small input-inline clsEdit' value='"+Convert.ToString(uc.ReferencegaindB)+"' spara='"+uc.LineId+"' id='txtReferencegaindB_"+uc.LineId+"' />", //Convert.ToString(uc.ReferencegaindB),
                                param.IsVisible?Convert.ToString(uc.TCGIndicationAmplitude):  "<input type='text' name='txtTCGIndicationAmplitude' class='form-control input-sm input-small input-inline clsEdit' value='"+Convert.ToString(uc.TCGIndicationAmplitude)+"' spara='"+uc.LineId+"' id='txtTCGIndicationAmplitude_"+uc.LineId+"' />",//Convert.ToString(uc.TCGIndicationAmplitude),
                                Convert.ToString(uc.CreatedBy),
                                Convert.ToString(uc.CreatedOn),
                                Convert.ToString(uc.EditedBy),
                                Convert.ToString(uc.EditedOn),
                                "<center><i  data-modal=\"\" id=\"btnView\" name=\"btnAction\" style=\"cursor:pointer;\" Title=\"Subline Details\" class=\"fa fa-plus\"  onclick=\"GetPAUTSubLinesGrid("+uc.LineId+")\" ></i>&nbsp;&nbsp;</center>"//Convert.ToString(uc.LineId)
                                //"<center><a class='btn btn-xs green' href='/IPI/PartlistNDEInspection/PAUTLineViewDetails?LineId="+Convert.ToInt32(uc.LineId)+"'>View<i style='margin-left:5px;' class='fa fa-eye'></i></a></center>"
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = 0,
                    iTotalDisplayRecords = 0,
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]
        public ActionResult PAUTLineViewDetails(int LineId = 0)
        {
            ViewBag.LineId = LineId;
            return View();
        }

        [HttpPost]
        public ActionResult LoadPAUTLineFormPartial(int LineId, bool IsDisabled)
        {
            ViewBag.LineId = LineId;
            ViewBag.IsDisabled = IsDisabled;
            return PartialView("_LoadPAUTLineFormPartial");
        }

        [HttpPost]
        public JsonResult LoadPAUTSubLineData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "qms096.LineId in('" + param.CTQHeaderId + "')";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {

                }
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName);
                var lstResult = db.SP_FETCH_IPI_PAUT_TEST_SUB_LINES_RESULT
                                (
                               StartIndex,
                               EndIndex,
                               "",
                               strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.ROW_NO),
                               param.IsVisible? Convert.ToString(uc.GroupNo):"<input type='text' name='txtGroupNo' class='form-control input-sm input-small input-inline clsSubEdit' value='"+Convert.ToString(uc.GroupNo)+"' spara='"+uc.SubLineId+"' id='txtGroupNo_"+uc.SubLineId+"' />",
                               param.IsVisible? Convert.ToString(uc.Angle):"<input type='text' name='txtAngle' class='form-control input-sm input-small input-inline clsSubEdit' value='"+Convert.ToString(uc.Angle)+"' spara='"+uc.SubLineId+"' id='txtAngle_"+uc.SubLineId+"' />",//Convert.ToString(uc.Angle),
                               param.IsVisible? Convert.ToString(uc.DepthCoverage):"<input type='text' name='txtDepthCoverage' class='form-control input-sm input-small input-inline clsSubEdit' value='"+Convert.ToString(uc.DepthCoverage)+"' spara='"+uc.SubLineId+"' id='txtDepthCoverage_"+uc.SubLineId+"' />",//Convert.ToString(uc.DepthCoverage),
                               param.IsVisible? Convert.ToString(uc.FocalDepth):"<input type='text' name='txtFocalDepth' class='form-control input-sm input-small input-inline clsSubEdit' value='"+Convert.ToString(uc.FocalDepth)+"' spara='"+uc.SubLineId+"' id='txtFocalDepth_"+uc.SubLineId+"' />",//Convert.ToString(uc.FocalDepth),
                               param.IsVisible? Convert.ToString(uc.TCGIndicationAmplitude):"<input type='text' name='txtTCGIndicationAmplitude' class='form-control input-sm input-small input-inline clsSubEdit' value='"+Convert.ToString(uc.TCGIndicationAmplitude)+"' spara='"+uc.SubLineId+"' id='txtTCGIndicationAmplitude_"+uc.SubLineId+"' />",//Convert.ToString(uc.TCGIndicationAmplitude),
                               param.IsVisible? Convert.ToString(uc.StartElement):"<input type='text' name='txtStartElement' class='form-control input-sm input-small input-inline clsSubEdit' value='"+Convert.ToString(uc.StartElement)+"' spara='"+uc.SubLineId+"' id='txtStartElement_"+uc.SubLineId+"' />",//Convert.ToString(uc.StartElement),
                               param.IsVisible? Convert.ToString(uc.NumberofElements):"<input type='text' name='txtNumberofElements' class='form-control input-sm input-small input-inline clsSubEdit' value='"+Convert.ToString(uc.NumberofElements)+"' spara='"+uc.SubLineId+"' id='txtNumberofElements_"+uc.SubLineId+"' />",//Convert.ToString(uc.NumberofElements),
                               param.IsVisible? Convert.ToString(uc.AngularIncrementalChange):"<input type='text' name='txtAngularIncrementalChange' class='form-control input-sm input-small input-inline clsSubEdit' value='"+Convert.ToString(uc.AngularIncrementalChange)+"' spara='"+uc.SubLineId+"' id='txtAngularIncrementalChange_"+uc.SubLineId+"' />",//Convert.ToString(uc.AngularIncrementalChange),
                               param.IsVisible? Convert.ToString(uc.FocalPlane):"<input type='text' name='txtFocalPlane' class='form-control input-sm input-small input-inline clsSubEdit' value='"+Convert.ToString(uc.FocalPlane)+"' spara='"+uc.SubLineId+"' id='txtFocalPlane_"+uc.SubLineId+"' />",//Convert.ToString(uc.FocalPlane),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn),
                               Convert.ToString(uc.EditedBy),
                               Convert.ToString(uc.EditedOn),
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = 0,
                    iTotalDisplayRecords = 0,
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetPAUTSubLinesForm(int HeaderId, int LineId, string location)
        {
            QMS096 objQMS096 = new QMS096();
            return PartialView("_GetPAUTSubLinesForm", objQMS096);
        }

        [HttpPost]
        public ActionResult UpdatePAUTHeader(QMS094 qms094)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS094 objQMS094 = db.QMS094.Where(x => x.HeaderId == qms094.HeaderId).FirstOrDefault();
                if (objQMS094 != null)
                {
                    objQMS094.TestInstrument = qms094.TestInstrument;
                    objQMS094.ExaminationDate = qms094.ExaminationDate;
                    objQMS094.NDTDuration = qms094.NDTDuration;
                    objQMS094.DataSamplingSpacing = qms094.DataSamplingSpacing;
                    objQMS094.DataStorageLocation = qms094.DataStorageLocation;
                    objQMS094.ScanningStartLocation = qms094.ScanningSurface;
                    objQMS094.EncoderCalibration = qms094.EncoderCalibration;
                    objQMS094.Software = qms094.Software;
                    objQMS094.Version = qms094.Version;
                    objQMS094.ScanningSurface = qms094.ScanningSurface;
                    objQMS094.FinalDisplayProcessingLevels = qms094.FinalDisplayProcessingLevels;
                    objQMS094.CompAscanUT = qms094.CompAscanUT;
                    objQMS094.AfterTestCalibrationrechecked = qms094.AfterTestCalibrationrechecked;
                    objQMS094.AnySpecialAccessoriesUsed = qms094.AnySpecialAccessoriesUsed;
                    objQMS094.AnyRecordableIndication = qms094.AnyRecordableIndication;
                    objQMS094.NDTremarks = qms094.NDTremarks;
                    objQMS094.EditedBy = objClsLoginInfo.UserName;
                    objQMS094.EditedOn = DateTime.Now;
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Document update successfully.";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SavePAUTLine(QMS095 qms095)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS095 objQMS095 = db.QMS095.Where(x => x.LineId == qms095.LineId).FirstOrDefault();
                if (objQMS095 != null)
                {
                    objQMS095.ScannedSpots = qms095.ScannedSpots;
                    objQMS095.ScannedLength = qms095.ScannedLength;
                    objQMS095.ScanningSurface = qms095.ScanningSurface;
                    objQMS095.ReferencegaindB = qms095.ReferencegaindB;
                    objQMS095.TCGIndicationAmplitude = qms095.TCGIndicationAmplitude;
                    objQMS095.EditedBy = objClsLoginInfo.UserName;
                    objQMS095.EditedOn = DateTime.Now;
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Line update successfully.";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SavePAUTSubLine(QMS096 qms096)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS096 objQMS096 = db.QMS096.Where(x => x.SubLineId == qms096.SubLineId).FirstOrDefault();
                if (objQMS096 != null)
                {
                    objQMS096.GroupNo = qms096.GroupNo;
                    objQMS096.Angle = qms096.Angle;
                    objQMS096.DepthCoverage = qms096.DepthCoverage;
                    objQMS096.FocalDepth = qms096.FocalDepth;
                    objQMS096.TCGIndicationAmplitude = qms096.TCGIndicationAmplitude;
                    objQMS096.StartElement = qms096.StartElement;
                    objQMS096.NumberofElements = qms096.NumberofElements;
                    objQMS096.AngularIncrementalChange = qms096.AngularIncrementalChange;
                    objQMS096.FocalPlane = qms096.FocalPlane;
                    objQMS096.EditedBy = objClsLoginInfo.UserName;
                    objQMS096.EditedOn = DateTime.Now;
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Subline update successfully.";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;

                var lst = db.SP_FETCH_IPI_NDE_PARTLIST_OFFERED_RESULT(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Data Found";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                var newlst = (from li in lst
                              select new
                              {
                                  QualityProject = li.QualityProject,
                                  Project = li.Project,
                                  BU = li.BU,
                                  Location = li.Location,
                                  PartAssemblyFindNumber = li.PartAssemblyFindNumber,
                                  PartAssemblyChildItemCode = li.PartAssemblyChildItemCode,
                                  ChildItemCodeDesc = li.ChildItemCodeDesc,
                                  StageCode = li.StageCode,
                                  StageSequence = li.StageSequence,
                                  IterationNo = li.IterationNo,
                                  RequestNo = li.RequestNo,
                                  RequestNoSequence = li.RequestNoSequence,
                                  RequestStatus = li.RequestStatus,
                                  ManufacturingCode = li.ManufacturingCode,
                                  TPIAgency1 = li.TPIAgency1,
                                  TPIAgency1Intervention = li.TPIAgency1Intervention,
                                  TPIAgency2 = li.TPIAgency2,
                                  TPIAgency2Intervention = li.TPIAgency2Intervention,
                                  TPIAgency3 = li.TPIAgency3,
                                  TPIAgency3Intervention = li.TPIAgency3Intervention,
                                  TPIAgency4 = li.TPIAgency4,
                                  TPIAgency4Intervention = li.TPIAgency4Intervention,
                                  TPIAgency5 = li.TPIAgency5,
                                  TPIAgency5Intervention = li.TPIAgency5Intervention,
                                  TPIAgency6 = li.TPIAgency6,
                                  TPIAgency6Intervention = li.TPIAgency6Intervention,
                                  Shop = li.Shop,
                                  ShopLocation = li.ShopLocation,
                                  ShopRemark = li.ShopRemark,
                                  CreatedBy = li.CreatedBy,
                                  CreatedOn = li.CreatedOn,
                                  EditedBy = li.EditedBy,
                                  EditedOn = li.EditedOn,
                                  RequestGeneratedBy = li.RequestGeneratedBy,
                                  RequestGeneratedOn = li.RequestGeneratedOn,
                                  ConfirmedBy = li.ConfirmedBy,
                                  ConfirmedOn = li.ConfirmedOn,
                                  RequestId = li.RequestId,
                                  OfferedQuantity = li.OfferedQuantity,
                                  ProductionDrawingNo = li.ProductionDrawingNo,
                                  NDETechniqueNo = li.NDETechniqueNo,
                                  NDETechniqueRevisionNo = li.NDETechniqueRevisionNo,
                                  TestResult = li.TestResult,
                                  ReturnRemark = li.ReturnRemark,
                                  OfferedBy = li.OfferedBy,
                                  OfferedOn = li.OfferedOn,
                                  ReturnedBy = li.ReturnedBy,
                                  ReturnedOn = li.ReturnedOn,
                                  RejectedBy = li.RejectedBy,
                                  RejectedOn = li.RejectedOn,
                                  StageCodeDesc = li.StageCodeDesc
                              }).ToList();

                strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        public bool ISRequestAttended(int? reqId)
        {
            bool flag = false;
            string attended = clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue();
            var attendedreq = db.QMS065.Where(x => x.RequestId == reqId && x.RequestStatus == attended).FirstOrDefault();
            if (attendedreq != null)
            { flag = true; }

            return flag;
        }
    }

    public class TechniqueResponseMsg : clsHelper.ResponseMsg
    {
        public int TechRequestID { get; set; }
        public string RedirectURL { get; set; }
        public string TechNo { get; set; }
        public string TechName { get; set; }
        public int TechRev { get; set; }
    }
}