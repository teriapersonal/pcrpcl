﻿using IEMQS.Areas.IPI.Models;
using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace IEMQS.Areas.IPI.Controllers
{
    public class MaintainSeamListOfferInspectionController : clsBase
    {
        [SessionExpireFilter]
        public ActionResult Index(string tab)
        {
            ViewBag.Tab = tab;
            ViewBag.Location = objClsLoginInfo.Location;
            return View();
        }
        public ActionResult ShowTimelineOfferInspectionPartial(int headerId)
        {
            TimelineViewModel model = new TimelineViewModel();
            QMS040 objQMS040 = db.QMS040.FirstOrDefault(x => x.HeaderId == headerId);
            model.CreatedBy = Manager.GetPsidandDescription(objQMS040.CreatedBy);
            model.CreatedOn = objQMS040.CreatedOn;
            model.EditedBy = Manager.GetPsidandDescription(objQMS040.EditedBy);
            model.EditedOn = objQMS040.EditedOn;
            return PartialView("_ShowTimelineOfferInspectionPartial", model);
        }


        public ActionResult ShowTimelineNonNDEInspectionPartial(int requestId)
        {
            TimelineViewModel model = new TimelineViewModel();
            QMS050 objQMS050 = db.QMS050.FirstOrDefault(x => x.RequestId == requestId);
            model.CreatedBy = Manager.GetPsidandDescription(objQMS050.CreatedBy);
            model.CreatedOn = objQMS050.CreatedOn;
            model.EditedBy = Manager.GetPsidandDescription(objQMS050.EditedBy);
            model.EditedOn = objQMS050.EditedOn;
            model.OfferedBy = Manager.GetPsidandDescription(objQMS050.OfferedBy);
            model.OfferedOn = objQMS050.OfferedOn;
            model.InspectedBy = Manager.GetPsidandDescription(objQMS050.InspectedBy);
            model.InspectedOn = objQMS050.InspectedOn;
            model.OfferedtoCustomerBy = Manager.GetPsidandDescription(objQMS050.OfferedtoCustomerBy);
            model.OfferedtoCustomerOn = objQMS050.OfferedtoCustomerOn;
            model.TPIResultBy = Manager.GetPsidandDescription(objQMS050.EditedBy);
            model.TPIResultOn = objQMS050.TPIAgency1ResultOn;
            return PartialView("_ShowTimelineNonNDEInspectionPartial", model);
        }

        public ActionResult GetSeamListOfferDataPartial(string status, string qualityProject, string seamNumber)
        {
            ViewBag.Status = status;
            ViewBag.QualityProject = qualityProject;
            ViewBag.SeamNumber = seamNumber;

            if (status.ToLower() == "reoffer")
            {
                ViewBag.Status = "Pending";
                return PartialView("_GetReOfferDataPartial");
            }
            else if (status.ToLower() == "regenerate")
            {
                ViewBag.Status = "Regenerate";
                return PartialView("_GetShopSeamTestDataPartial");
            }
            else if (status.ToLower() == "returnnderequest")
            {
                ViewBag.Status = "ReturnNDERequest";
                return PartialView("_GetShopRequestReturnDataPartial");
            }
            else
            {
                return PartialView("_GetSeamListOfferDataPartial");
            }
        }

        public ActionResult GroupOfferPartial(string qProject)
        {
            string whereCondition = "1=1";
            whereCondition += " and qms.InspectionStatus in('" + clsImplementationEnum.SeamListInspectionStatus.ReadyToOffer.GetStringValue() + "') and qms.QualityProject='" + qProject + "' "; //and (dbo.CHECK_AVAIALBLE_NDE_STAGE(qms.StageCode, qms.BU, qms.Location) = 0)";
            whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms.BU", "qms.Location");
            List<SP_IPI_SEAMLISTOFFERINSPECTION_Result> lstReadyToOfferSeam = db.SP_IPI_SEAMLISTOFFERINSPECTION(1, int.MaxValue, "", whereCondition).ToList();
            List<string> lstDistinctQualityProject = lstReadyToOfferSeam.Select(x => x.QualityProject).Distinct().ToList();
            string chemical = clsImplementationEnum.SeamStageType.Chemical.GetStringValue().ToUpper();
            List<string> lstDistinctStages = (from stg in lstReadyToOfferSeam
                                              join q02 in db.QMS002 on stg.StageCode.Trim() equals q02.StageCode.Trim()
                                              where q02.StageType.ToUpper() != chemical
                                              select stg.StageCode).Distinct().ToList();

            List<BULocWiseCategoryModel> lstQualityProject = lstDistinctQualityProject.Select(x => new BULocWiseCategoryModel { CatDesc = (x.ToString()), CatID = x.ToString() }).ToList();
            ViewBag.lstQualityProject = new JavaScriptSerializer().Serialize(lstQualityProject);
            List<BULocWiseCategoryModel> lstStages = lstDistinctStages.Select(x => new BULocWiseCategoryModel { CatDesc = (x.ToString()), CatID = x.ToString() }).ToList();
            ViewBag.lstStages = new JavaScriptSerializer().Serialize(lstStages);



            return PartialView("_GroupOfferPartial");
        }
        public ActionResult loadSeamListGroupOfferDataTable(JQueryDataTableParamModel param, string qualityProject, string stageCode)
        {
            try
            {
                string BU = db.QMS010.Where(x => x.QualityProject == qualityProject && x.Location == objClsLoginInfo.Location).Select(x => x.BU).FirstOrDefault();
                var IsProtocolApplicable = db.QMS002.Where(x => x.Location == objClsLoginInfo.Location && x.BU == BU && x.StageCode == stageCode).Select(x => x.IsProtocolApplicable).FirstOrDefault();

                List<GLB002> lstGLB002 = Manager.GetSubCatagories("Shop", BU, objClsLoginInfo.Location).ToList();
                List<BULocWiseCategoryModel> lstBULocWiseCategoryModel = lstGLB002.Select(x => new BULocWiseCategoryModel { CatDesc = (x.Code + "-" + x.Description), CatID = x.Code }).ToList();


                string whereCondition = "1=1";
                whereCondition += " and qms.InspectionStatus in('" + clsImplementationEnum.SeamListInspectionStatus.ReadyToOffer.GetStringValue() + "')";
                whereCondition += " and qms.QualityProject in('" + qualityProject + "')";
                whereCondition += " and qms.StageCode in('" + stageCode + "') ";//obs : 16060 : IPI  >> and (dbo.CHECK_AVAIALBLE_NDE_STAGE(qms.StageCode, qms.BU, qms.Location) = 0)";

                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms.BU", "qms.Location");

                string[] columnName = { "qms.SeamNo", "qms.QualityProject", "qms.InspectionStatus", "qms.Location", "qms.StageCode", "qms.LNTInspectorResult", "qms.Remarks" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = Int32.MaxValue; // param.iDisplayStart + param.iDisplayLength;
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstSeamList = db.SP_IPI_SEAMLISTOFFERINSPECTION(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                string allowseamcategorygroupoffer = clsImplementationEnum.ConfigParameter.AllowSeamCategoryGroupOffer.GetStringValue();

                lstSeamList.ForEach(x =>
                {
                    x.Project = x.Project.Split('-')[0];
                });
                var preheat = clsImplementationEnum.HeatTreatment.Pre.GetStringValue();
                var postheat = clsImplementationEnum.HeatTreatment.Post.GetStringValue();
                var PrePostWeldHeadTreatment = db.QMS002.Where(x => x.StageCode == stageCode && x.BU == BU && x.Location == objClsLoginInfo.Location).Select(x => x.PrePostWeldHeadTreatment).FirstOrDefault();
                bool ispreheat = (PrePostWeldHeadTreatment == clsImplementationEnum.HeatTreatment.Pre.GetStringValue());
                bool ispostheat = (PrePostWeldHeadTreatment == clsImplementationEnum.HeatTreatment.Post.GetStringValue());

                var lstAllowSeam = Manager.GetConfigValue(allowseamcategorygroupoffer).ToUpper().Split(',').ToArray();

                var lstHeader = (from lst in lstSeamList
                                 join seam in db.QMS012 on new { lst.QualityProject, lst.Project, lst.SeamNo } equals new { seam.QualityProject, seam.Project, seam.SeamNo }
                                 where (ispreheat ? lstAllowSeam.Contains(seam.SeamCategory.Trim().ToUpper()) : true)
                                 select lst).ToList();
                var obj = lstHeader.FirstOrDefault();
                bool IsSTAGEDEPARTMENTNDE = false;
                bool? IsWelderApplicable = false;
                bool? IsPTMTApplicable = false;
                List<GLB002> lstGLB002SurfaceCondition = null;
                List<BULocWiseCategoryModel> lstBULocWiseSurfaceCondition = null;
                string SurfaceCondition = string.Empty;
                string SurfaceConditionCode = string.Empty;
                if (obj != null)
                {
                    IsSTAGEDEPARTMENTNDE = db.Database.SqlQuery<bool>("SELECT dbo.FUN_GET_IPI_ISSTAGEDEPARTMENTNDE('" + obj.HeaderId + "')").FirstOrDefault();
                    string location = obj.Location.Split('-')[0].Trim();
                    string bu = obj.BU.Split('-')[0].Trim();
                    var objQMS002 = db.QMS002.Where(x => x.Location.Trim() == location && x.BU.Trim() == bu && x.StageCode == obj.StageCode).FirstOrDefault();
                    IsWelderApplicable = objQMS002?.IsWelderApplicable;
                    var ReinforcementNotApplicableFor = clsImplementationEnum.ConfigParameter.ReinforcementNotApplicableFor.GetStringValue();
                    var lstNotApplicableFor = Manager.GetConfigValue(ReinforcementNotApplicableFor).ToUpper().Split(',').ToArray();
                    NDEModels objNDEModels = new NDEModels();
                    IsPTMTApplicable = objQMS002 != null ? (lstNotApplicableFor.Contains(objQMS002.StageType) ? true : false) : false;
                    lstGLB002SurfaceCondition = Manager.GetSubCatagories("Surface Condition", obj.BU.Split('-')[0], obj.Location.Split('-')[0]).ToList();
                    lstBULocWiseSurfaceCondition = lstGLB002SurfaceCondition.Select(x => new BULocWiseCategoryModel { CatDesc = (x.Description), CatID = x.Code }).ToList();
                    SurfaceCondition = objQMS002 != null ? objNDEModels.GetCategory("Surface Condition", objQMS002.DefaultSurfaceCondition, objQMS002.BU, objQMS002.Location, true).CategoryDescription : string.Empty;
                    SurfaceConditionCode = objQMS002 != null ? objQMS002.DefaultSurfaceCondition : string.Empty;
                }

                int? totalRecords = lstHeader.Count; //lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                var res = from h in lstHeader
                          select new[] {
                           Convert.ToString(h.HeaderId),
                           h.QualityProject,
                           Convert.ToString( h.SeamNo),
                           Convert.ToString(h.StageCodeDescription),
                           Convert.ToString(h.StageSequence),
                           Convert.ToString(h.HeaderId),
                 };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition,
                    IsSTAGEDEPARTMENTNDE = IsSTAGEDEPARTMENTNDE,
                    IsWelderApplicable = IsWelderApplicable,
                    IsPTMTApplicable = IsPTMTApplicable,
                    lstSurfaceCondition = new JavaScriptSerializer().Serialize(lstBULocWiseSurfaceCondition),
                    protocolAppplicable = IsProtocolApplicable ? "Yes" : "No",
                    lstSubCatagories = new JavaScriptSerializer().Serialize(lstBULocWiseCategoryModel),
                    SurfaceCondition = SurfaceCondition,
                    SurfaceConditionCode = SurfaceConditionCode
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = "",

                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult GroupOfferSeamlist(FormCollection fc, SeamListSendOffer objSeamListSendOffer, string strHeaderIds, string isProtocolApplicable)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                List<int> arrayHeaderIds = strHeaderIds.Split(',').Select(int.Parse).ToList();
                //if (hasAttachments)
                //{
                //    string[] folderPaths = new string[arrayHeaderIds.Count()];
                //    int i = 0;
                //    foreach (var headerid in arrayHeaderIds)
                //    {
                //        folderPaths[i] = "QMS050/" + headerid + "/" + 1 + "/" + 1;
                //        i++;
                //    }
                //    try
                //    {
                //        Manager.ManageDocumentsOnMultipleDestination(folderPaths, hasAttachments, Attach, objClsLoginInfo.UserName);
                //    }
                //    catch
                //    {
                //    }
                //}
                string response = "";
                List<string> OfferedSeams = new List<string>();
                List<string> SkipSeams = new List<string>();
                List<string> SkipWelderNotMaintained = new List<string>();
                List<WPS025> lstWPS025 = new List<WPS025>();
                List<WelderStamlList> lstDefaultWelders = new List<WelderStamlList>();
                int id = arrayHeaderIds[0];
                QMS040 obj = db.QMS040.Where(x => x.HeaderId == id).FirstOrDefault();

                bool IsSTAGEDEPARTMENTNDE = db.Database.SqlQuery<bool>("SELECT dbo.FUN_GET_IPI_ISSTAGEDEPARTMENTNDE('" + obj.HeaderId + "')").FirstOrDefault();
                //if (IsSTAGEDEPARTMENTNDE)
                //{
                var lstSeams = db.QMS040.Where(x => arrayHeaderIds.Contains(x.HeaderId)).Select(x => x.SeamNo).ToList();

                // copy first seam welder and break the loop
                foreach (var item in lstSeams)
                {
                    lstWPS025 = db.WPS025.Where(x => x.SeamNo == item
                                   && x.QualityProject == obj.QualityProject && x.Project == obj.Project
                                   && x.BU == obj.BU).OrderBy(o => o.PrintedOn).ToList();
                    if (lstWPS025.Count > 0)
                    {
                        break;
                    }
                }
                lstDefaultWelders = lstWPS025.Select(x => new WelderStamlList { WelderStamp = x.WelderStamp, WeldingProcess = x.WeldingProcess, QualityProject = x.QualityProject, SeamNo = x.SeamNo }).Distinct().ToList();
                //}
                foreach (var headerid in arrayHeaderIds)
                {
                    bool ApplicableForOffer = true;
                    bool IsWelderMaintained = true;

                    QMS040 objQMS040 = db.QMS040.Where(x => x.HeaderId == headerid).FirstOrDefault();

                    #region Check For Any Stage Approved or not in ICL

                    var stageStatus = clsImplementationEnum.TestPlanStatus.Approved.GetStringValue();
                    var lstTPStages = db.QMS021.Where(x => x.QualityProject == objQMS040.QualityProject && x.Project == objQMS040.Project
                                    && x.Location == objQMS040.Location && x.BU == objQMS040.BU && x.SeamNo == objQMS040.SeamNo
                                    && (x.StageSequance < objQMS040.StageSequence || (x.StageCode == objQMS040.StageCode && x.StageSequance == objQMS040.StageSequence)) && !x.StageStatus.Equals(stageStatus, StringComparison.OrdinalIgnoreCase)).ToList();
                    if (lstTPStages.Any())
                    {
                        ApplicableForOffer = false;
                    }
                    else
                    {
                        var lstICLStages = db.QMS031.Where(x => x.QualityProject == objQMS040.QualityProject && x.Project == objQMS040.Project
                                    && x.Location == objQMS040.Location && x.BU == objQMS040.BU && x.SeamNo == objQMS040.SeamNo
                                    && (x.StageSequance < objQMS040.StageSequence || (x.StageCode == objQMS040.StageCode && x.StageSequance == objQMS040.StageSequence)) && x.StageSequance <= objQMS040.StageSequence
                                    && !x.StageStatus.Equals(stageStatus, StringComparison.OrdinalIgnoreCase)).ToList();
                        if (lstICLStages.Any())
                        {
                            ApplicableForOffer = false;
                        }
                        else
                            objResponseMsg.Key = true;
                    }
                    #endregion

                    //#region Check For Protocol Filled or not by Production Team
                    //if (ApplicableForOffer && objQMS040.ProtocolId.HasValue && !Manager.IsProtocolFilledByProduction(objQMS040.ProtocolId.Value, objQMS040.ProtocolType, objQMS040.BU, objQMS040.Location))
                    //{
                    //    ApplicableForOffer = false;
                    //}
                    //#endregion

                    if (ApplicableForOffer)
                    {
                        #region Capture welder For NDE Stage
                        if (IsSTAGEDEPARTMENTNDE)
                        {
                            //CopyWelderWeldLengthFromPreviousNDEStage(objQMS040.HeaderId, false);
                            //#region Fetch data from request (16014 :While offering stage, system should copy offer data from previous stage to new stage.)
                            //var objPreviousStageSeq = (from qms40 in db.QMS040
                            //                           join qms002 in db.QMS002 on new { qms40.StageCode, qms40.BU, qms40.Location } equals new { qms002.StageCode, qms002.BU, qms002.Location }
                            //                           where qms40.QualityProject == objQMS040.QualityProject && qms40.Project == objQMS040.Project && qms40.SeamNo == objQMS040.SeamNo
                            //                                 && qms40.Location == objQMS040.Location && qms40.BU == objQMS040.BU
                            //                                 && qms40.StageSequence <= objQMS040.StageSequence && qms40.HeaderId != objQMS040.HeaderId
                            //                                 && qms002.StageDepartment == "NDE"
                            //                           orderby qms40.StageSequence descending
                            //                           select qms40).FirstOrDefault();
                            //NDEModels objNDEModels = new NDEModels();
                            //string SurfaceCondition = string.Empty, ShopDesc = string.Empty;
                            //if (objPreviousStageSeq != null)
                            //{
                            //    bool IsSTAGENDE = db.Database.SqlQuery<bool>("SELECT dbo.FUN_GET_IPI_ISSTAGEDEPARTMENTNDE('" + objPreviousStageSeq.HeaderId + "')").FirstOrDefault();
                            //    if (IsSTAGENDE)
                            //    {
                            //        QMS060 objQMS060 = db.QMS060
                            //                            .Where(x => x.HeaderId == objPreviousStageSeq.HeaderId)
                            //                            .OrderByDescending(x => x.OfferedOn)
                            //                            .FirstOrDefault();
                            //        if (objQMS060 != null)
                            //        {
                            //            SurfaceCondition = objNDEModels.GetCategory("Surface Condition", objQMS060.SurfaceCondition, objQMS060.BU, objQMS060.Location, true).CategoryDescription;
                            //            if (!string.IsNullOrWhiteSpace(objQMS060.SurfaceCondition))
                            //            {
                            //                objSeamListSendOffer.SurfaceCondition = objQMS060.SurfaceCondition;
                            //            }
                            //            if (objQMS060.WeldThicknessReinforcement != null && objQMS060.WeldThicknessReinforcement.HasValue)
                            //            {
                            //                objSeamListSendOffer.WeldThicknessReinforcement = objQMS060.WeldThicknessReinforcement;
                            //            }
                            //        }
                            //    }
                            //    else
                            //    {
                            //        QMS050 objQMS050 = db.QMS050
                            //                            .Where(x => x.HeaderId == objPreviousStageSeq.HeaderId)
                            //                            .OrderByDescending(x => x.OfferedOn)
                            //                            .FirstOrDefault();
                            //        if (objQMS050 != null)
                            //        {
                            //            ShopDesc = objNDEModels.GetCategory("Shop", objQMS050.Shop, objQMS050.BU, objQMS050.Location).CategoryDescription;
                            //            objSeamListSendOffer.Shop = objQMS050.Shop;
                            //            objSeamListSendOffer.Location = objQMS050.ShopLocation;
                            //            objSeamListSendOffer.ShopRemark = objQMS050.ShopRemark;
                            //        }
                            //    }
                            //}
                            //var objQMS002 = db.QMS002.Where(x => x.Location == objQMS040.Location && x.BU == objQMS040.BU && x.StageCode == objQMS040.StageCode).FirstOrDefault();
                            //var ReinforcementNotApplicableFor = clsImplementationEnum.ConfigParameter.ReinforcementNotApplicableFor.GetStringValue();
                            //var lstNotApplicableFor = Manager.GetConfigValue(ReinforcementNotApplicableFor).ToUpper().Split(',').ToArray();

                            //ViewBag.IsWelderApplicable = objQMS002 != null ? objQMS002.IsWelderApplicable : false;
                            //ViewBag.IsPTMT = objQMS002 != null ? (lstNotApplicableFor.Contains(objQMS002.StageType) ? true : false) : false;

                            //ViewBag.ShopDesc = ShopDesc;
                            //ViewBag.SurfaceConditionCode = objQMS002 != null ? objQMS002.DefaultSurfaceCondition : string.Empty;
                            //ViewBag.SurfaceCondition = objQMS002 != null ? objNDEModels.GetCategory("Surface Condition", objQMS002.DefaultSurfaceCondition, objQMS002.BU, objQMS002.Location, true).CategoryDescription : SurfaceCondition;
                            //ViewBag.DefaultSurfaceCondition = !string.IsNullOrWhiteSpace(objQMS002.DefaultSurfaceCondition);
                            //#endregion

                            if (!db.QMS061.Any(x => x.HeaderId == objQMS040.HeaderId))
                            {
                                QMS012 objQMS012 = db.QMS012.Where(x => x.QualityProject == objQMS040.QualityProject
                                                                && x.Project == objQMS040.Project
                                                                && x.SeamNo == objQMS040.SeamNo).FirstOrDefault();

                                List<WelderStamlList> lstSeamWiseWelders = new List<WelderStamlList>();
                                List<WPS025> lstToken = new List<WPS025>();
                                lstSeamWiseWelders = db.WPS025.Where(x => x.SeamNo == objQMS040.SeamNo
                                                                    && x.QualityProject == obj.QualityProject
                                                                    && x.Project == obj.Project
                                                                    && x.BU == obj.BU).OrderBy(o => o.PrintedOn).Select(x => new WelderStamlList { WelderStamp = x.WelderStamp, WeldingProcess = x.WeldingProcess }).Distinct().ToList();
                                // if welder not available for seam then copy(current seam rest record will be same) above capture welder from wps025 & 26(lstFecthWelders)
                                if (lstSeamWiseWelders.Count == 0)
                                {
                                    foreach (var item in lstDefaultWelders)
                                    {
                                        WPS025 objSrcwps025 = db.WPS025.Where(x => x.WelderStamp == item.WelderStamp && x.WeldingProcess == item.WeldingProcess && x.SeamNo == item.SeamNo
                                                                    && x.QualityProject == obj.QualityProject).FirstOrDefault();

                                        if (objSrcwps025 != null)
                                        {
                                            WPS026 objsrcWPS026 = db.WPS026.Where(x => x.Id == objSrcwps025.RefTokenId).FirstOrDefault();
                                            if (objsrcWPS026 != null)
                                            {
                                                WPS026 objWPS026 = new WPS026();
                                                objWPS026.QualityProject = objsrcWPS026.QualityProject;
                                                objWPS026.Project = objsrcWPS026.Project;
                                                objWPS026.BU = objsrcWPS026.BU;
                                                objWPS026.Location = objsrcWPS026.Location;
                                                objWPS026.TokenNumber = objsrcWPS026.TokenNumber;
                                                objWPS026.SeamNo = objQMS040.SeamNo;
                                                objWPS026.WPSNumber = objsrcWPS026.WPSNumber;
                                                objWPS026.WeldingProcess = objsrcWPS026.WeldingProcess;
                                                objWPS026.WPSRevNo = objsrcWPS026.WPSRevNo;
                                                objWPS026.WeldLayer = objsrcWPS026.WeldLayer;
                                                objWPS026.AWSClass = objsrcWPS026.AWSClass;
                                                objWPS026.FillerMetalSize = objsrcWPS026.FillerMetalSize;
                                                objWPS026.FluxAWSClass = objsrcWPS026.FluxAWSClass;
                                                objWPS026.FluxItem = objsrcWPS026.FluxItem;
                                                objWPS026.ConsumableItem = objsrcWPS026.ConsumableItem;
                                                objWPS026.CreatedBy = objsrcWPS026.CreatedBy;
                                                objWPS026.CreatedOn = objsrcWPS026.CreatedOn;
                                                objWPS026.EditedBy = objsrcWPS026.EditedBy;
                                                objWPS026.EditedOn = objsrcWPS026.EditedOn;
                                                objWPS026.PrintedBy = objsrcWPS026.PrintedBy;
                                                objWPS026.PrintedOn = objsrcWPS026.PrintedOn;
                                                objWPS026.ValidUpto = objsrcWPS026.ValidUpto;
                                                objWPS026.EU = objsrcWPS026.EU;
                                                objWPS026.WelderPSNO = objsrcWPS026.WelderPSNO;
                                                objWPS026.FluxItemCategory = objsrcWPS026.FluxItemCategory;
                                                objWPS026.ConsumableItemCategory = objsrcWPS026.ConsumableItemCategory;
                                                objWPS026.UserLocation = objsrcWPS026.UserLocation;
                                                objWPS026.WPPNumber = objsrcWPS026.WPPNumber;
                                                objWPS026.TokenFor = objsrcWPS026.TokenFor;
                                                objWPS026.Shop = objsrcWPS026.Shop;
                                                objWPS026.Station = objsrcWPS026.Station;
                                                objWPS026.FluxQty = objsrcWPS026.FluxQty;
                                                objWPS026.WelderStamp = objsrcWPS026.WelderStamp;
                                                db.WPS026.Add(objWPS026);
                                                db.SaveChanges();

                                                WPS025 objwps025 = new WPS025();
                                                objwps025.SeamNo = objQMS040.SeamNo;
                                                objwps025.RefTokenId = objWPS026.Id;
                                                objwps025.QualityProject = objSrcwps025.QualityProject;
                                                objwps025.Project = objSrcwps025.Project;
                                                objwps025.BU = objSrcwps025.BU;
                                                objwps025.Location = objSrcwps025.Location;
                                                objwps025.WeldingProcess = objSrcwps025.WeldingProcess;
                                                objwps025.WelderPSNO = objSrcwps025.WelderPSNO;
                                                objwps025.WPSNumber = objSrcwps025.WPSNumber;
                                                objwps025.WPSRevNo = objSrcwps025.WPSRevNo;
                                                objwps025.WeldLayer = objSrcwps025.WeldLayer;
                                                objwps025.JointType = objSrcwps025.JointType;
                                                objwps025.PrintedBy = objSrcwps025.PrintedBy;
                                                objwps025.PrintedOn = objSrcwps025.PrintedOn;
                                                objwps025.ValidUpto = objSrcwps025.ValidUpto;
                                                objwps025.WelderStamp = objSrcwps025.WelderStamp;

                                                lstToken.Add(objwps025);
                                            }
                                        }
                                    }
                                    if (lstToken.Count > 0)
                                    {
                                        db.WPS025.AddRange(lstToken);
                                        db.SaveChanges();
                                        lstSeamWiseWelders = lstToken.Select(x => new WelderStamlList { WelderStamp = x.WelderStamp, WeldingProcess = x.WeldingProcess }).Distinct().ToList();
                                    }
                                }
                                //else fetch own welder from wps025 (lstFecthWelders)
                                decimal? weldlength = 0;
                                bool isOverlay = Manager.IsOverlaySeam(objQMS040.QualityProject, objQMS040.SeamNo);
                                if (lstSeamWiseWelders.Count() > 0)
                                {
                                    weldlength = objQMS012.SeamLengthArea / Convert.ToInt32(lstSeamWiseWelders.Count());
                                }
                                else
                                {
                                    if (Manager.IsWelderRequiredInNDEStageOffer(objQMS040.BU, objQMS040.Location, objQMS040.StageCode))
                                    {
                                        IsWelderMaintained = false;
                                        SkipWelderNotMaintained.Add(objQMS040.SeamNo);
                                    }
                                }
                                List<QMS061> lstQMS061 = new List<QMS061>();
                                List<string> lstaddedWelders = new List<string>();
                                foreach (var item in lstSeamWiseWelders)
                                {
                                    if (!lstaddedWelders.Contains(item.WelderStamp))
                                    {
                                        QMS061 objQMS061 = new QMS061
                                        {
                                            HeaderId = Convert.ToInt32(objQMS040.HeaderId),
                                            RefRequestId = null,
                                            QualityProject = objQMS040.QualityProject,
                                            Project = objQMS040.Project,
                                            BU = objQMS040.BU,
                                            Location = objQMS040.Location,
                                            SeamNo = objQMS040.SeamNo,
                                            StageCode = objQMS040.StageCode,
                                            StageSequence = objQMS040.StageSequence,
                                            IterationNo = null,
                                            RequestNo = null,
                                            RequestNoSequence = null,
                                            welderstamp = item.WelderStamp,
                                            WeldingProcess = item.WeldingProcess,
                                            CreatedBy = objClsLoginInfo.UserName,
                                            CreatedOn = DateTime.Now,
                                            Weldlength = isOverlay ? null : weldlength,
                                            Weldarea = isOverlay ? weldlength : null,
                                        };
                                        lstQMS061.Add(objQMS061);
                                        lstaddedWelders.Add(item.WelderStamp);
                                    }
                                }
                                if (lstQMS061.Count > 0)
                                {
                                    db.QMS061.AddRange(lstQMS061);
                                    db.SaveChanges();
                                }
                            }
                        }

                        #endregion

                        if (!IsSTAGEDEPARTMENTNDE && Manager.IsWeldVisualStage(objQMS040.BU, objQMS040.Location, objQMS040.StageCode))
                        {
                            if (!db.WPS025.Any(c => c.QualityProject == objQMS040.QualityProject && c.SeamNo == objQMS040.SeamNo) && lstDefaultWelders.Count == 0)
                            {
                                IsWelderMaintained = false;
                            }
                            else
                            {
                                #region copy default welders
                                List<WPS025> lstToken = new List<WPS025>();
                                foreach (var item in lstDefaultWelders)
                                {
                                    WPS025 objSrcwps025 = db.WPS025.Where(x => x.WelderStamp == item.WelderStamp && x.WeldingProcess == item.WeldingProcess && x.SeamNo == item.SeamNo
                                                                && x.QualityProject == item.QualityProject).FirstOrDefault();

                                    if (objSrcwps025 != null)
                                    {
                                        WPS026 objsrcWPS026 = db.WPS026.Where(x => x.Id == objSrcwps025.RefTokenId).FirstOrDefault();
                                        if (objsrcWPS026 != null)
                                        {
                                            WPS026 objWPS026 = new WPS026();
                                            objWPS026.QualityProject = objsrcWPS026.QualityProject;
                                            objWPS026.Project = objsrcWPS026.Project;
                                            objWPS026.BU = objsrcWPS026.BU;
                                            objWPS026.Location = objsrcWPS026.Location;
                                            objWPS026.TokenNumber = objsrcWPS026.TokenNumber;
                                            objWPS026.SeamNo = objQMS040.SeamNo;
                                            objWPS026.WPSNumber = objsrcWPS026.WPSNumber;
                                            objWPS026.WeldingProcess = objsrcWPS026.WeldingProcess;
                                            objWPS026.WPSRevNo = objsrcWPS026.WPSRevNo;
                                            objWPS026.WeldLayer = objsrcWPS026.WeldLayer;
                                            objWPS026.AWSClass = objsrcWPS026.AWSClass;
                                            objWPS026.FillerMetalSize = objsrcWPS026.FillerMetalSize;
                                            objWPS026.FluxAWSClass = objsrcWPS026.FluxAWSClass;
                                            objWPS026.FluxItem = objsrcWPS026.FluxItem;
                                            objWPS026.ConsumableItem = objsrcWPS026.ConsumableItem;
                                            objWPS026.CreatedBy = objsrcWPS026.CreatedBy;
                                            objWPS026.CreatedOn = objsrcWPS026.CreatedOn;
                                            objWPS026.EditedBy = objsrcWPS026.EditedBy;
                                            objWPS026.EditedOn = objsrcWPS026.EditedOn;
                                            objWPS026.PrintedBy = objsrcWPS026.PrintedBy;
                                            objWPS026.PrintedOn = objsrcWPS026.PrintedOn;
                                            objWPS026.ValidUpto = objsrcWPS026.ValidUpto;
                                            objWPS026.EU = objsrcWPS026.EU;
                                            objWPS026.WelderPSNO = objsrcWPS026.WelderPSNO;
                                            objWPS026.FluxItemCategory = objsrcWPS026.FluxItemCategory;
                                            objWPS026.ConsumableItemCategory = objsrcWPS026.ConsumableItemCategory;
                                            objWPS026.UserLocation = objsrcWPS026.UserLocation;
                                            objWPS026.WPPNumber = objsrcWPS026.WPPNumber;
                                            objWPS026.TokenFor = objsrcWPS026.TokenFor;
                                            objWPS026.Shop = objsrcWPS026.Shop;
                                            objWPS026.Station = objsrcWPS026.Station;
                                            objWPS026.FluxQty = objsrcWPS026.FluxQty;
                                            db.WPS026.Add(objWPS026);
                                            db.SaveChanges();

                                            WPS025 objwps025 = new WPS025();
                                            objwps025.SeamNo = objQMS040.SeamNo;
                                            objwps025.RefTokenId = objWPS026.Id;
                                            objwps025.QualityProject = objSrcwps025.QualityProject;
                                            objwps025.Project = objSrcwps025.Project;
                                            objwps025.BU = objSrcwps025.BU;
                                            objwps025.Location = objSrcwps025.Location;
                                            objwps025.WeldingProcess = objSrcwps025.WeldingProcess;
                                            objwps025.WelderPSNO = objSrcwps025.WelderPSNO;
                                            objwps025.WPSNumber = objSrcwps025.WPSNumber;
                                            objwps025.WPSRevNo = objSrcwps025.WPSRevNo;
                                            objwps025.WeldLayer = objSrcwps025.WeldLayer;
                                            objwps025.JointType = objSrcwps025.JointType;
                                            objwps025.PrintedBy = objSrcwps025.PrintedBy;
                                            objwps025.PrintedOn = objSrcwps025.PrintedOn;
                                            objwps025.ValidUpto = objSrcwps025.ValidUpto;
                                            objwps025.WelderStamp = objSrcwps025.WelderStamp;

                                            lstToken.Add(objwps025);
                                        }
                                    }
                                }
                                if (lstToken.Count > 0)
                                {
                                    db.WPS025.AddRange(lstToken);
                                    db.SaveChanges();
                                }
                                #endregion
                            }
                        }

                        if (IsWelderMaintained)
                        {
                            response = db.SP_IPI_GENREATESEAMTESTREQUEST(headerid, objClsLoginInfo.UserName, objSeamListSendOffer.Shop, objSeamListSendOffer.Location, objSeamListSendOffer.ShopRemark, objSeamListSendOffer.SurfaceCondition, objSeamListSendOffer.WeldThicknessReinforcement, null, null).FirstOrDefault();
                            var iter = db.QMS040.Where(x => x.HeaderId == headerid).FirstOrDefault();

                            #region Send Notification    
                            if (IsSTAGEDEPARTMENTNDE)
                            {
                                QMS060 objQMS060 = iter.QMS060.OrderByDescending(o => o.CreatedOn).FirstOrDefault();
                                if (objQMS060 != null)
                                {
                                    objQMS060.IsGroupOffered = true; db.SaveChanges();
                                    if (Manager.IsNDERequestAutoGenerate(objQMS060.Project, objQMS060.BU, objQMS060.Location))
                                    {
                                        (new SeamListNDEInspectionController()).RespondeGenerateRequest(objQMS060.RequestId, Manager.GetSystemUserPSNo());
                                    }
                                    else
                                    {
                                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), iter.Project, iter.BU, iter.Location, "Stage: " + iter.StageCode + " of Seam: " + iter.SeamNo + " & Project No: " + iter.QualityProject + " has been offered for Inspection", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/SeamListNDEInspection/QCSeamTestDetails/" + objQMS060.RequestId);
                                    }
                                }
                            }
                            else
                            {
                                QMS050 objQMS050 = iter.QMS050.OrderByDescending(o => o.CreatedOn).FirstOrDefault();
                                if (objQMS050 != null)
                                {
                                    objQMS050.IsGroupOffered = true; db.SaveChanges();
                                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), iter.Project, iter.BU, iter.Location, "Stage: " + iter.StageCode + " of Seam: " + iter.SeamNo + " & Project No: " + iter.QualityProject + " has been offered for Inspection", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/MaintainSeamListOfferInspection/ApproveSeamListInspection/" + objQMS050.RequestId);
                                }
                            }
                            OfferedSeams.Add(objQMS040.SeamNo);
                            #endregion
                        }
                        else
                        {
                            SkipWelderNotMaintained.Add(objQMS040.SeamNo);
                        }

                    }
                    else
                    {
                        SkipSeams.Add(objQMS040.SeamNo);
                    }
                }

                objResponseMsg.Key = true;

                if (OfferedSeams.Count > 0)
                {
                    objResponseMsg.Value = "Seam Offered: " + string.Join(",", OfferedSeams) + ".";
                }
                if (SkipSeams.Count > 0)
                {
                    objResponseMsg.RequiredProtocol = "Seam Skipped: " + string.Join(",", SkipSeams) + " because of required protocol not filled or any of stages in ICL is not approved yet. Please try to offer individual for check actual reason for particular seam.";
                }
                if (SkipWelderNotMaintained.Count > 0)
                {
                    objResponseMsg.dataValue = "Seam Skipped: " + string.Join(",", SkipWelderNotMaintained) + ",Please add Welders in Welder wise Parameter Slip for particular seam.";
                }
                return Json(objResponseMsg);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error Occures Please try again";
                return Json(objResponseMsg);
            }

        }

        public ActionResult GroupReOfferPartial()
        {
            string whereCondition = "1=1";
            whereCondition += " and TestResult IS NULL and OfferedBy IS NULL and qms.StageCode IS NOT NULL and (dbo.CHECK_AVAIALBLE_NDE_STAGE(qms.StageCode, qms.BU, qms.Location) = 0)";
            whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms.BU", "qms.Location");
            List<SP_IPI_SEAMLISTOFFERINSPECTION_DETAILS_Result> lstReofferStage = db.SP_IPI_SEAMLISTOFFERINSPECTION_DETAILS(1, int.MaxValue, "", whereCondition).ToList();
            List<string> lstDistinctQualityProject = lstReofferStage.Select(x => x.QualityProject).Distinct().ToList();
            List<string> lstDistinctStages = lstReofferStage.Select(x => x.StageCode).Distinct().ToList();
            List<string> lstNonchemicalStages = new List<string>();
            foreach (var item in lstDistinctStages)
            {
                if (!string.IsNullOrWhiteSpace(item))
                {
                    lstNonchemicalStages.Add(item.Split('-')[0]);
                }
            }
            string StageTypeExludedinOfferDropdown = clsImplementationEnum.ConfigParameter.StageTypeExludedinOfferDropdown.GetStringValue();
            var lstExludedinOfferDropdown = Manager.GetConfigValue(StageTypeExludedinOfferDropdown).ToUpper().Split(',').ToArray();

            var lstStageCodes = (from lst in db.QMS002
                                 where lstNonchemicalStages.Contains(lst.StageCode) && !lstExludedinOfferDropdown.Contains(lst.StageType)
                                 select lst.StageCode).ToList();

            List<BULocWiseCategoryModel> lstQualityProject = lstDistinctQualityProject.Select(x => new BULocWiseCategoryModel { CatDesc = x.ToString().Split('-')[0], CatID = x.ToString().Split('-')[0] }).ToList();
            ViewBag.lstQualityProject = new JavaScriptSerializer().Serialize(lstQualityProject);
            List<BULocWiseCategoryModel> lstStages = lstStageCodes.Select(x => new BULocWiseCategoryModel { CatDesc = x.ToString().Split('-')[0], CatID = x.ToString().Split('-')[0] }).ToList();
            ViewBag.lstStages = new JavaScriptSerializer().Serialize(lstStages);
            return PartialView("_GroupReOfferPartial");
        }
        public ActionResult loadSeamListGroupReOfferDataTable(JQueryDataTableParamModel param, string qualityProject, string stageCode)
        {
            try
            {
                string BU = db.QMS010.Where(x => x.QualityProject == qualityProject && x.Location == objClsLoginInfo.Location).Select(x => x.BU).FirstOrDefault();
                var IsProtocolApplicable = db.QMS002.Where(x => x.Location == objClsLoginInfo.Location && x.BU == BU && x.StageCode == stageCode).Select(x => x.IsProtocolApplicable).FirstOrDefault();
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                whereCondition += " and TestResult IS NULL and OfferedBy IS NULL "; // and (dbo.CHECK_AVAIALBLE_NDE_STAGE(qms.StageCode, qms.BU, qms.Location) = 0)";
                whereCondition += " and qms.QualityProject in('" + qualityProject + "')";
                whereCondition += " and qms.StageCode in('" + stageCode + "') ";

                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms.BU", "qms.Location");
                string[] columnName = { "qms.SeamNo", "qms.QualityProject", "dbo.GET_PROJECTDESC_BY_CODE(qms.Project)", "qms.OfferedBy", "qms.CreatedBy", "qms.Location", "dbo.GET_IPI_STAGECODE_DESCRIPTION(qms.StageCode, qms.BU, qms.Location) ", "qms.RequestNo", "qms.InspectedBy", "qms.QCRemarks", "qms.OfferedtoCustomerBy", "qms.Shop", "qms.ShopLocation", "qms.ShopRemark" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = string.Empty; ;
                sortDirection = Convert.ToString(Request["sSortDir_0"]); // asc or desc
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstHeader = db.SP_IPI_SEAMLISTOFFERINSPECTION_DETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                var res = from h in lstHeader
                          select new[] {
                           Convert.ToString(h.RequestId),
                           h.QualityProject,
                           Convert.ToString( h.SeamNo),
                           Convert.ToString(h.StageCode),
                           Convert.ToString(h.StageSequence),
                           Convert.ToString(h.HeaderId+"/"+h.IterationNo+"/"+h.RequestNoSequence),
                 };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition,
                    protocolAppplicable = IsProtocolApplicable ? "Yes" : "No",
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = "",

                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult GroupReOfferSeamlist(string strRequestIds, string isProtocolApplicable)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<int> arrayRequestIds = strRequestIds.Split(',').Select(int.Parse).ToList();
                //if (hasAttachments)
                //{
                //    string[] folderPaths = new string[arrayRequestIds.Count()];
                //    int i = 0;
                //    foreach (var requestId in arrayRequestIds)
                //    {
                //        var objQMS050 = db.QMS050.Where(x => x.RequestId == requestId).FirstOrDefault();

                //        folderPaths[i] = "QMS050/" + objQMS050.HeaderId + "/" + objQMS050.IterationNo + "/" + objQMS050.RequestNoSequence;
                //        i++;
                //    }
                //    try
                //    {
                //        Manager.ManageDocumentsOnMultipleDestination(folderPaths, hasAttachments, Attach, objClsLoginInfo.UserName);
                //    }
                //    catch
                //    {
                //    }
                //}

                List<QMS050> lstReOfferRequests = (from li in db.QMS050
                                                   where arrayRequestIds.Contains(li.RequestId)
                                                   select li).ToList();
                lstReOfferRequests.ToList().ForEach(x =>
                {
                    x.OfferedBy = objClsLoginInfo.UserName;
                    x.OfferedOn = DateTime.Now;
                    if (Manager.IsLTFPSProject(x.Project, x.BU, x.Location))
                    {
                        x.LTF002.InspectionStatus = clsImplementationEnum.SeamListInspectionStatus.UnderInspection.GetStringValue();
                    }
                    else
                    {
                        x.QMS040.InspectionStatus = clsImplementationEnum.SeamListInspectionStatus.UnderInspection.GetStringValue();
                    }
                });
                db.SaveChanges();

                foreach (var requestId in arrayRequestIds)
                {
                    var objQMS050 = db.QMS050.Where(x => x.RequestId == requestId).FirstOrDefault();
                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), objQMS050.Project, objQMS050.BU, objQMS050.Location, "Stage: " + objQMS050.StageCode + " of Seam: " + objQMS050.SeamNo + " & Project No: " + objQMS050.QualityProject + " has been re offered for Inspection", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/MaintainSeamListOfferInspection/ApproveSeamListInspection/" + objQMS050.RequestId);
                    #endregion                 
                }
                objResponseMsg.Key = true;
                return Json(objResponseMsg);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error Occures Please try again";
                return Json(objResponseMsg);
            }
        }
        public ActionResult loadSeamListOfferDataTable(JQueryDataTableParamModel param, string status, string qualityProject, string seamNumber)
        {
            try
            {
                string whereCondition = "1=1";
                string _urlFrom = "all";

                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and qms.InspectionStatus in('" + clsImplementationEnum.SeamListInspectionStatus.ReadyToOffer.GetStringValue() + "')";
                    _urlFrom = "offer";
                }
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms.BU", "qms.Location");

                string[] columnName = { "qms.SeamNo", "qms.QualityProject", "qms.Project+'-'+com1.t_dsca", "qms.InspectionStatus", "qms.Location", "qms.StageCode", "qms.LNTInspectorResult", "qms.Remarks" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                if (!string.IsNullOrEmpty(qualityProject) && !string.IsNullOrEmpty(seamNumber))
                {
                    whereCondition += (seamNumber.Equals("ALL") ? " AND qms.QualityProject = '" + qualityProject + "' " : " AND qms.QualityProject = '" + qualityProject + "' AND qms.SeamNo ='" + seamNumber + "'");
                }
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstHeader = db.SP_IPI_SEAMLISTOFFERINSPECTION(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                var res = from h in lstHeader
                          select new[] {
                           Convert.ToString( h.SeamNo) +  (status.ToUpper() == "ALL" && IsPendingForInspection(h.QualityProject, h.BU.Split('-')[0], h.Location.Split('-')[0], h.SeamNo) ? "<a  href=\"javascript: void(0);\" Title=\"View Pending Inspection Data\" onclick=\"GetIPIPendingInspectionDataForSeam('"+ h.Project.Split('-')[0] +"','"+ h.QualityProject +"','"+ h.BU.Split('-')[0] +"','"+ h.Location.Split('-')[0] +"','"+ h.SeamNo+ "')\"><i class=\"fa fa-exclamation-circle\" style=\"margin-left:5px; color: red; font-size: 20px;\"></i></a>" : ""),
                           Convert.ToString(h.StageCodeDescription),
                           Convert.ToString(h.StageSequence),
                           Convert.ToString(h.IterationNo),
                           Convert.ToString( h.InspectionStatus),
                           Convert.ToString(h.LNTInspectorResult),
                           h.QualityProject,
                           Convert.ToString(h.HeaderId),
                           //Convert.ToString( h.CreatedBy),
                           //Convert.ToString(h.CreatedOn),
                           //Convert.ToString( h.Location),
                           //Convert.ToString(h.Project),
                           //Convert.ToString(h.BU),
                           //Convert.ToString(h.Remarks),
                           //h.SeamCleared==true?"Yes" :"No",
                           "<center style=\"white-space: nowrap;\" class=\"clsDisplayInlineEle\"><a   href=\""+ WebsiteURL + "/IPI/MaintainSeamListOfferInspection/SeamListOfferInspection/"+h.HeaderId+"?from="+ _urlFrom +"\" Title=\"View Details\" ><i style=\"margin - left:5px;\" class=\"fa fa-eye\"></i></a>&nbsp;&nbsp;"
                           + GenerateReportIcon(h.HeaderId)
                           + (h.InspectionStatus == clsImplementationEnum.SeamListInspectionStatus.ReadyToOffer.GetStringValue() ? "<a  href=\"javascript: void(0);\" Title=\"Offer Seam\" onclick=\"GenerateOffer(" + h.HeaderId + ",'" + ""+h.SeamNo.Trim()+"" + "','" + h.InspectionStatus + "')\"><i style=\"margin - left:5px;\" class=\"fa fa-paper-plane\"></i></a>&nbsp;" : "<a Title=\"This stage is not ready for offer\" ><i style=\"opacity:0.3;\" class=\"fa fa-paper-plane\"></i></a>&nbsp;")
                           + Manager.generateIPIGridButtons(h.QualityProject,h.Project,h.BU,h.Location,h.SeamNo,"","",h.StageCode,Convert.ToString(h.StageSequence),"Seam Details",(Convert.ToBoolean(h.isNDEStage)?60:50),"true","Seam Details","Seam Request Details")
                           //+ (h.ProtocolId.HasValue ? "&nbsp;&nbsp;<a onclick=\"OpenProtocol('" + WebsiteURL + "/PROTOCOL/"+h.ProtocolType+"/Linkage/"+h.ProtocolId + ((h.InspectionStatus == null || string.Equals(h.InspectionStatus,clsImplementationEnum.SeamListInspectionStatus.ReadyToOffer.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(h.InspectionStatus,clsImplementationEnum.SeamListInspectionStatus.Rework.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(h.InspectionStatus,clsImplementationEnum.SeamListInspectionStatus.Returned.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(h.InspectionStatus,clsImplementationEnum.SeamListInspectionStatus.Rejected.GetStringValue(),StringComparison.OrdinalIgnoreCase)) ? "?m=1', '"+ h.ProtocolType +"',"+ h.ProtocolId +");" : "', '"+ h.ProtocolType +"',"+ h.ProtocolId +");" ) +"\" Title=\"View Protocol Details\" ><i style=\"margin - left:5px;\" class=\"fa fa-file-text\"></i></a>" : "&nbsp;&nbsp;<a Title=\"No Protocol Attached\" ><i style=\"opacity:0.3;\" class=\"fa fa-file-text\"></i></a>")
                           + (Convert.ToString(h.ProtocolType) == clsImplementationEnum.ProtocolType.Other_Files.GetStringValue() ? ( "&nbsp;&nbsp;<a onclick=\"OpenProtocolPopUp("+ h.HeaderId +");\" Title=\"View Protocol Files\" ><i style=\"margin - left:5px;\" class=\"fa fa-file-text\"></i></a>"  ) : ((h.ProtocolId.HasValue ? "&nbsp;&nbsp;<a onclick=\"OpenProtocol('" + WebsiteURL + "/PROTOCOL/"+h.ProtocolType+"/Linkage/"+h.ProtocolId + ((h.InspectionStatus == null || string.Equals(h.InspectionStatus,clsImplementationEnum.SeamListInspectionStatus.ReadyToOffer.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(h.InspectionStatus,clsImplementationEnum.SeamListInspectionStatus.Rework.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(h.InspectionStatus,clsImplementationEnum.SeamListInspectionStatus.Returned.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(h.InspectionStatus,clsImplementationEnum.SeamListInspectionStatus.Rejected.GetStringValue(),StringComparison.OrdinalIgnoreCase)) ? "?m=1', '"+ h.ProtocolType +"',"+ h.ProtocolId +");" : "', '"+ h.ProtocolType +"',"+ h.ProtocolId +");" ) +"\" Title=\"View Protocol Details\" ><i style=\"margin - left:5px;\" class=\"fa fa-file-text\"></i></a>" : "&nbsp;&nbsp;<a Title=\"No Protocol Attached\" ><i style=\"opacity:0.3;\" class=\"fa fa-file-text\"></i></a>")) )     
                           //+ (h.ProtocolId.HasValue ? "&nbsp;&nbsp;<a href=\"javascript: void(0);\"  onclick=\"ViewProtocol("+ h.HeaderId+",'"+h.InspectionStatus+"','/PROTOCOL/"+h.ProtocolType+"/Linkage/"+h.ProtocolId + "','"+ ((h.InspectionStatus == null || string.Equals(h.InspectionStatus,clsImplementationEnum.SeamListInspectionStatus.ReadyToOffer.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(h.InspectionStatus,clsImplementationEnum.SeamListInspectionStatus.Rework.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(h.InspectionStatus,clsImplementationEnum.SeamListInspectionStatus.Returned.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(h.InspectionStatus,clsImplementationEnum.SeamListInspectionStatus.Rejected.GetStringValue(),StringComparison.OrdinalIgnoreCase)) ? "?m=1" : "" ) +"');\" Title=\"View Protocol Details\" ><i style=\"margin - left:5px;\" class=\"fa fa-file-text\"></i></a>" : "&nbsp;&nbsp;<a Title=\"No Protocol Attached\" ><i style=\"opacity:0.3;\" class=\"fa fa-file-text\"></i></a>")
                           +"&nbsp;&nbsp;<a  href=\"javascript: void(0);\" Title=\"View Timeline\" onclick=\"ShowTimeline("+ h.HeaderId +")\"><i style=\"margin - left:5px;\" class=\"fa fa-clock-o\"></i></a> </center>"

            };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = "",

                }, JsonRequestBehavior.AllowGet);
            }
        }

        // ONLY USED in MOBILE APPLICATION
        [HttpPost]
        public ActionResult AttendOfferedSeamListMobile(JQueryDataTableParamModel param, string status, string UserName)
        {
            try
            {
                int? StartIndex = (param.iDisplayStart * param.iDisplayLength) + 1;
                int? EndIndex = (param.iDisplayStart * param.iDisplayLength) + param.iDisplayLength;

                string whereCondition = "1=1";
                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and (TestResult IS NULL or TestResult='Returned by TPI') and OfferedBy IS NOT NULL";

                }
                if (status.ToUpper() == "HARDNESS" || status.ToUpper() == "FERRITE" || status.ToUpper() == "PMI")
                {
                    whereCondition += " and (TestResult IS NULL or TestResult='Returned by TPI') and OfferedBy IS NOT NULL and q02.StageType='" + status + "'";
                }
                else if (status.ToUpper() == "TPI")
                {
                    whereCondition += " and TestResult = 'Cleared' and OfferedtoCustomerBy IS NULL and  1 = (select TPIOnlineApproval from QMS010 qms10 where qms10.QualityProject = qms.QualityProject AND qms10.BU = qms.BU AND qms10.Location = qms.Location) ";
                }
                whereCondition += Manager.MakeDefaultWhere(UserName, "qms.BU", "qms.Location");
                //TestResult ="Cleared" and offertocustmerby is null and  

                string qualityProject = param.QualityProject;
                string seamNumber = param.SeamNo;

                //if (!string.IsNullOrEmpty(qualityProject) && !string.IsNullOrEmpty(seamNumber))
                //{
                //    whereCondition += (seamNumber.Equals("ALL") ? " AND qms.QualityProject = '" + qualityProject + "' " : " AND qms.QualityProject = '" + qualityProject + "' AND qms.SeamNo ='" + seamNumber + "'");
                //}

                string strWhere = whereCondition;
                if (!string.IsNullOrEmpty(qualityProject))
                {
                    whereCondition += " AND qms.QualityProject in (SELECT Value FROM [dbo].[fn_Split]('" + qualityProject + "',','))";
                }

                string sortColumnName = "QualityProject";
                string sortDirection = "asc"; ;
                string strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";

                var lstHeader = db.SP_IPI_SEAMLISTOFFERINSPECTION_DETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    TotalCount = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    data_list = lstHeader
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                    TotalCount = 0,
                    data_list = new List<string>()
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public bool IsPendingForInspection(string qualityProject, string BU, string Location, string seamNo)
        {
            if (db.QMS040.Any(c => c.QualityProject == qualityProject && c.BU == BU && c.Location == Location && c.SeamNo == seamNo && c.InspectionStatus != null))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public string GenerateReportIcon(int HeaderId)
        {
            string ActionHTML = string.Empty;
            string ReportType = string.Empty;

            bool isRequestExists = db.QMS040.FirstOrDefault(c => c.HeaderId == HeaderId).QMS060.Any();

            if (isRequestExists)
            {
                QMS060 objQMS060 = db.QMS040.FirstOrDefault(c => c.HeaderId == HeaderId).QMS060.OrderByDescending(d => d.RequestId).FirstOrDefault();

                QMS002 objQMS002 = db.QMS002.FirstOrDefault(c => c.BU == objQMS060.BU && c.Location == objQMS060.Location && c.StageCode == objQMS060.StageCode);
                if (objQMS002 != null)
                {
                    if (objQMS002.StageType == "RT" || objQMS002.StageType == "UT")
                    {
                        if (objQMS002.StageType == "RT")
                        {
                            ReportType = "RT";
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(Convert.ToString(objQMS060.NDETechniqueNo)))
                                ReportType = objQMS060.NDETechniqueNo.Split('/')[1].ToString();
                        }
                        if (!string.IsNullOrWhiteSpace(ReportType))
                            ActionHTML = "onclick=\"OpenNDEReport('" + ReportType + "','" + objQMS060.QualityProject + "','" + objQMS002.StageType + "','" + objQMS060.SeamNo.Replace("\\", "\\\\") + "','" + objQMS060.StageCode + "','" + objQMS060.StageSequence + "','" + objQMS060.IterationNo + "','" + objQMS060.RequestNo + "')\"";
                    }
                }
            }

            if (!string.IsNullOrWhiteSpace(ActionHTML))
            {
                return "<a " + ActionHTML + " Title=\"View Report\" ><i style=\"margin-left:5px;\" class=\"fa fa-print\"></i></a>&nbsp;";
            }
            else
            {
                return "<a Title=\"Report Not Available\" ><i style=\"cursor: pointer; opacity:0.3; margin-left:5px;\" class=\"fa fa-print\"></i></a>&nbsp;";
            }

        }

        public ActionResult GetSeamICLLineId(int OfferHeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS040 objQMS040 = db.QMS040.FirstOrDefault(c => c.HeaderId == OfferHeaderId);
                QMS031 objQMS031 = db.QMS031.FirstOrDefault(c => c.Project == objQMS040.Project && c.QualityProject == objQMS040.QualityProject && c.BU == objQMS040.BU && c.Location == objQMS040.Location && c.SeamNo == objQMS040.SeamNo && c.StageCode == objQMS040.StageCode && c.StageSequance == objQMS040.StageSequence);

                if (objQMS031 != null)
                {
                    objResponseMsg.Value = objQMS031.LineId.ToString();
                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Value = "ICL Line not found.";
                    objResponseMsg.Key = false;
                }

                return Json(objResponseMsg);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error Occures Please try again";
                return Json(objResponseMsg);
            }
        }

        public ActionResult IsProtocolFileRequired(int OfferHeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS040 objQMS040 = db.QMS040.FirstOrDefault(c => c.HeaderId == OfferHeaderId);
                if (Convert.ToString(objQMS040.ProtocolType) == clsImplementationEnum.ProtocolType.Other_Files.GetStringValue())
                {
                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Key = false;
                }

                return Json(objResponseMsg);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error Occures Please try again";
                return Json(objResponseMsg);
            }
        }



        [HttpPost]
        public JsonResult LoadFilteredSeamlistData(JQueryDataTableParamModel param, string qualityProject, string project, string bu, string location, string SeamNo)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;

                strWhere = "1=1";
                strWhere += string.Format(" and qms.QualityProject in('{0}') and qms.Project in('{1}') and qms.BU in('{2}') and qms.Location in('{3}') and  qms.SeamNo in('{4}')", qualityProject, project, bu, location, SeamNo);

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = {  "InspectionStatus",
                                                "[L&TInspectionResult]",
                                                 "(dbo.GET_IPI_STAGECODE_DESCRIPTION(qms.StageCode,ltrim(rtrim(qms.BU)),ltrim(rtrim(qms.Location))))",
                                                "StageSequence",
                                                //"IterationNo",
                                             };
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                // strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms045.BU", "qms045.Location");
                var lstResult = db.SP_IPI_SEAMLISTOFFERINSPECTION
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.StageCodeDescription),
                               Convert.ToString(uc.StageSequence),
                               Convert.ToString(uc.IterationNo),
                               Convert.ToString(uc.InspectionStatus),
                               Convert.ToString(uc.LNTInspectorResult),
                               uc.AcceptanceStandard,
                               uc.ApplicableSpecification,
                               uc.InspectionExtent,
                               uc.Remark1,
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.isNDEStage),
                               Convert.ToString(uc.StageCode),
                               Convert.ToString(uc.StageSequence)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = 0,
                    iTotalDisplayRecords = 0,
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult loadSeamListOfferInspectionDataTable(JQueryDataTableParamModel param, string status)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                string _urlFrom = "all";

                string whereCondition = "1=1";
                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and (TestResult IS NULL or TestResult='Returned by TPI') and OfferedBy IS NOT NULL";
                    _urlFrom = "attend";
                }
                if (status.ToUpper() == "HARDNESS" || status.ToUpper() == "FERRITE" || status.ToUpper() == "PMI" || status.ToUpper() == "CHEMICAL")
                {
                    whereCondition += " and (TestResult IS NULL or TestResult='Returned by TPI') and OfferedBy IS NOT NULL and q02.StageType='" + status + "'";
                    _urlFrom = status.ToLower();
                }
                else if (status.ToUpper() == "TPI")
                {
                    whereCondition += " and TestResult = 'Cleared' and OfferedtoCustomerBy IS NULL and  1 = (select top 1  TPIOnlineApproval from QMS010 qms10 where qms10.QualityProject = qms.QualityProject AND qms10.BU = qms.BU AND qms10.Location = qms.Location) ";
                    _urlFrom = "offertpi";
                }
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms.BU", "qms.Location");
                //TestResult ="Cleared" and offertocustmerby is null and  
                string[] columnName = { "qms.SeamNo", "dbo.GET_PROJECTDESC_BY_CODE(qms.Project)", "qms.OfferedBy", "qms.CreatedBy", "qms.Location", "dbo.GET_IPI_STAGECODE_DESCRIPTION(qms.StageCode, qms.BU, qms.Location) ", "qms.RequestNo", "qms.InspectedBy", "qms.QCRemarks", "qms.OfferedtoCustomerBy", "qms.Shop", "qms.ShopLocation", "qms.ShopRemark" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string qualityProject = param.QualityProject;
                string seamNumber = param.SeamNo;

                //if (!string.IsNullOrEmpty(qualityProject) && !string.IsNullOrEmpty(seamNumber))
                //{
                //    whereCondition += (seamNumber.Equals("ALL") ? " AND qms.QualityProject = '" + qualityProject + "' " : " AND qms.QualityProject = '" + qualityProject + "' AND qms.SeamNo ='" + seamNumber + "'");
                //}
                string strWhere = whereCondition;
                if (!string.IsNullOrEmpty(qualityProject))
                {
                    whereCondition += " AND qms.QualityProject in (SELECT Value FROM [dbo].[fn_Split]('" + qualityProject + "',','))";
                }

                //[19-10-2020-60278 ] Jagruti - JayamSoft
                if (!string.IsNullOrEmpty(param.dtRange))
                {
                    string dtsearchfilter = param.dtRange;
                    clsManager objmanger = new clsManager();
                    String[] slit = { "to" };
                    string[] date = dtsearchfilter.Split(slit, StringSplitOptions.RemoveEmptyEntries);
                    string datefrom = "CONVERT(date, '" + objmanger.getDateTimeyyyyMMdd(date[0]) + "')";
                    string dateto = "CONVERT(date, '" + objmanger.getDateTimeyyyyMMdd(date[1]) + "')";
                    whereCondition += " AND (CONVERT(date, OfferedOn) >= " + datefrom + " and CONVERT(date, OfferedOn) <= " + dateto + ")";
                }

                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = string.Empty; ;
                sortDirection = Convert.ToString(Request["sSortDir_0"]); // asc or desc
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstHeader = db.SP_IPI_SEAMLISTOFFERINSPECTION_DETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                var res = from h in lstHeader
                          select new[] {
                           h.QualityProject,
                           Convert.ToString(h.Project),
                           Convert.ToString(h.RequestId),
                           Convert.ToString(h.HeaderId),
                           Convert.ToString( h.SeamNo),
                           Convert.ToString(h.StageCode),
                           Convert.ToString(h.StageSequence),
                           Convert.ToString(h.IterationNo),
                           Convert.ToString(h.TestResult),
                           Convert.ToString(h.OfferedBy),
                           h.OfferedOn != null ? Convert.ToDateTime(h.OfferedOn).ToString("dd/MM/yyyy hh:mm tt") : "",
                           Convert.ToString(h.BU),
                           Convert.ToString( h.Location),
                           Convert.ToString(h.RequestNo),
                           Convert.ToString(h.RequestNoSequence),
                           Convert.ToString(h.InspectedBy),
                           h.InspectedOn != null ? Convert.ToDateTime(h.InspectedOn).ToString("dd/MM/yyyy hh:mm tt") : "",
                           Convert.ToString(h.QCRemarks),
                           Convert.ToString(h.OfferedtoCustomerBy),
                           h.OfferedtoCustomerOn != null ? Convert.ToDateTime(h.OfferedtoCustomerOn).ToString("dd/MM/yyyy hh:mm tt") : "",
                           Convert.ToString(h.Shop),
                           Convert.ToString(h.ShopLocation),
                           Convert.ToString(h.ShopRemark),
                          "<center style=\"white-space: nowrap;\" class=\"clsDisplayInlineEle\"><a target=\"_blank\" href=\"" + WebsiteURL + "/IPI/MaintainSeamListOfferInspection/ApproveSeamListInspection/"+h.RequestId+"?from="+ _urlFrom +"\" Title=\"View Details\" ><i style=\"margin - left:5px;\" class=\"fa fa-eye\"></i></a>&nbsp;"+Manager.generateIPIGridButtons(h.QualityProject,h.Project,h.BU,h.Location,h.SeamNo,"","",h.StageCode,Convert.ToString(h.StageSequence),"Seam Details",50,"true","Seam Details","Seam Request Details")
                          //+ (h.ProtocolId.HasValue ? "&nbsp;&nbsp;<a target=\"_blank\"  href=\"" + WebsiteURL + "/PROTOCOL/"+h.ProtocolType+"/Linkage/"+h.ProtocolId + (((h.TestResult==null || h.TestResult=="Returned by TPI") && h.OfferedBy != null && status.ToUpper() != "TPI") ? "?m=1" : "") + "\" Title=\"View Protocol Details\" ><i style=\"margin - left:5px;\" class=\"fa fa-file-text\"></i></a>" : "&nbsp;&nbsp;<a Title=\"No Protocol Attached\" ><i style=\"opacity:0.3;\" class=\"fa fa-file-text\"></i></a>")
                          + (Convert.ToString(h.ProtocolType) == clsImplementationEnum.ProtocolType.Other_Files.GetStringValue() ? ( "&nbsp;&nbsp;<a onclick=\"OpenProtocolPopUp("+ h.HeaderId +");\" Title=\"View Protocol Files\" ><i style=\"margin - left:5px;\" class=\"fa fa-file-text\"></i></a>"  ) : (h.ProtocolId.HasValue ? "&nbsp;&nbsp;<a target=\"_blank\"  href=\"" + WebsiteURL + "/PROTOCOL/"+h.ProtocolType+"/Linkage/"+h.ProtocolId + (((h.TestResult==null || h.TestResult=="Returned by TPI") && h.OfferedBy != null && status.ToUpper() != "TPI") ? "?m=1" : "") + "\" Title=\"View Protocol Details\" ><i style=\"margin - left:5px;\" class=\"fa fa-file-text\"></i></a>" : "&nbsp;&nbsp;<a Title=\"No Protocol Attached\" ><i style=\"opacity:0.3;\" class=\"fa fa-file-text\"></i></a>") )
                          + "&nbsp;&nbsp;<a  href=\"javascript: void(0);\" Title=\"View Timeline\" onclick=\"ShowTimeline("+ h.RequestId +")\"><i style=\"margin - left:5px;\" class=\"fa fa-clock-o\"></i></a></center>"

                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition,
                    strWhere = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        // ONLY USED IN MOBILE APPLICATION
        [HttpPost]
        public JsonResult AutoCompleteSeamNoListByQualityProject(string QualityProject, string SeamNo)
        {
            try
            {

                IEnumerable<string> lstSeamNo = (from qms in db.QMS050
                                                 join qms001 in db.QMS001
                                                 on new { qms.Project, qms.BU, qms.Location }
                                                 equals new { qms001.Project, qms001.BU, qms001.Location }
                                                 where qms.QualityProject == QualityProject && qms.SeamNo.Contains(SeamNo)
                                                 select qms.SeamNo).Distinct().ToList();

                return Json(new
                {
                    Key = true,
                    Value = "Success",
                    data = lstSeamNo
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Key = false,
                    Value = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #region Group Clearance Task
        //Created BY : Ajay Chauhan
        //Date : 29/12/2017 : 
        //Task : Group Clearance Task
        public ActionResult GroupClearancePartial()
        {
            string whereCondition = "1=1";
            whereCondition += " and (TestResult IS NULL or TestResult='Returned by TPI') and OfferedBy IS NOT NULL";
            whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms.BU", "qms.Location");

            List<SP_IPI_SEAMLISTOFFERINSPECTION_DETAILS_Result> lstClearanceSeam = db.SP_IPI_SEAMLISTOFFERINSPECTION_DETAILS(1, int.MaxValue, "", whereCondition).ToList();
            List<string> lstDistinctQualityProject = lstClearanceSeam.Select(x => x.QualityProject).Distinct().ToList();
            List<lststages> lstDistinctStages = lstClearanceSeam.Select(x => new lststages { StageCode = x.StageCode, BU = x.BU, Location = x.Location }).Distinct().ToList();
            List<BULocWiseCategoryModel> lstQualityProject = lstDistinctQualityProject.Select(x => new BULocWiseCategoryModel { CatDesc = (x.ToString()), CatID = x.ToString() }).ToList();
            ViewBag.lstQualityProject = new JavaScriptSerializer().Serialize(lstQualityProject);

            lstDistinctStages.ForEach(x =>
            {
                x.StageDesc = x.StageCode;
                x.Location = !string.IsNullOrWhiteSpace(x.Location) ? x.Location.Split('-')[0] : "";
                x.StageCode = !string.IsNullOrWhiteSpace(x.StageCode) ? x.StageCode.Split('-')[0] : "";
                x.BU = !string.IsNullOrWhiteSpace(x.BU) ? x.BU.Split('-')[0] : "";
            });


            var lstStages = lstDistinctStages.Where(c => c.StageCode != null || c.StageCode != "")
                            .Select(x => new { StageDesc = (x.StageCode), StageCode = x.StageCode, BU = x.BU, Location = x.Location }).ToList();

            //string StageTypeExludedinOfferDropdown = clsImplementationEnum.ConfigParameter.StageTypeExludedinOfferDropdown.GetStringValue();
            //var lstStagetypes = Manager.GetConfigValue(StageTypeExludedinOfferDropdown).ToUpper().Split(',').ToArray();
            var lstClearStages = (from stg in lstStages
                                  join q02 in db.QMS002 on new { stg.StageCode, stg.BU, stg.Location } equals new { q02.StageCode, q02.BU, q02.Location }
                                  //where !lstStagetypes.Contains(q02.StageType.ToUpper())
                                  select new { CatDesc = stg.StageDesc, CatID = q02.StageCode }).Distinct().ToList();
            ViewBag.lstStages = new JavaScriptSerializer().Serialize(lstClearStages);


            return PartialView("_GroupClearancePartial");
        }

        public ActionResult loadSeamListGroupClearanceDataTable(JQueryDataTableParamModel param, string qualityProject, string stageCode)
        {
            try
            {
                string BU = db.QMS010.Where(x => x.QualityProject == qualityProject && x.Location == objClsLoginInfo.Location).Select(x => x.BU).FirstOrDefault();
                var IsProtocolApplicable = db.QMS002.Where(x => x.Location == objClsLoginInfo.Location && x.BU == BU && x.StageCode == stageCode).Select(x => x.IsProtocolApplicable).FirstOrDefault();

                List<GLB002> lstGLB002 = Manager.GetSubCatagories("Shop", BU, objClsLoginInfo.Location).ToList();
                List<BULocWiseCategoryModel> lstBULocWiseCategoryModel = lstGLB002.Select(x => new BULocWiseCategoryModel { CatDesc = (x.Code + "-" + x.Description), CatID = x.Code }).ToList();

                string whereCondition = "1=1";
                whereCondition += " and (TestResult IS NULL or TestResult='Returned by TPI') and OfferedBy IS NOT NULL";
                whereCondition += " and qms.QualityProject in('" + qualityProject + "')";
                whereCondition += " and qms.StageCode in('" + stageCode + "')";

                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms.BU", "qms.Location");

                string[] columnName = { "qms.SeamNo", "dbo.GET_PROJECTDESC_BY_CODE(qms.Project)", "qms.OfferedBy", "qms.CreatedBy", "qms.Location", "dbo.GET_IPI_STAGECODE_DESCRIPTION(qms.StageCode, qms.BU, qms.Location) ", "qms.RequestNo", "qms.InspectedBy", "qms.QCRemarks", "qms.OfferedtoCustomerBy", "qms.Shop", "qms.ShopLocation", "qms.ShopRemark" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstSeamList = db.SP_IPI_SEAMLISTOFFERINSPECTION_DETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                var preheat = clsImplementationEnum.HeatTreatment.Pre.GetStringValue();
                var postheat = clsImplementationEnum.HeatTreatment.Post.GetStringValue();
                var objHeatTreatment = db.QMS002.Where(x => x.StageCode == stageCode && x.BU == BU && x.Location == objClsLoginInfo.Location).FirstOrDefault();
                bool ispreheat = (objHeatTreatment.PrePostWeldHeadTreatment == clsImplementationEnum.HeatTreatment.Pre.GetStringValue());
                bool ispostheat = (objHeatTreatment.PrePostWeldHeadTreatment == clsImplementationEnum.HeatTreatment.Post.GetStringValue());
                bool isGroundspot = (objHeatTreatment.IsGroundSpot) == true ? true : false;

                var lstStagetypes = Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.StageTypeExludedinOfferDropdown.GetStringValue()).ToUpper().Split(',').ToArray();
                bool isQAStage = lstStagetypes.Contains(objHeatTreatment.StageType.ToUpper());

                string allowseamcategorygroupoffer = clsImplementationEnum.ConfigParameter.AllowSeamCategoryGroupOffer.GetStringValue();
                lstSeamList.ForEach(x =>
                {
                    x.Project = x.Project.Split('-')[0];
                });
                var lstAllowSeam = Manager.GetConfigValue(allowseamcategorygroupoffer).ToUpper().Split(',').ToArray();
                var lstHeader = (from lst in lstSeamList
                                 join seam in db.QMS012 on new { lst.QualityProject, lst.Project, lst.SeamNo } equals new { seam.QualityProject, seam.Project, seam.SeamNo }
                                 where (isGroundspot ? false : (ispreheat && !isQAStage ? lstAllowSeam.Contains(seam.SeamCategory.Trim().ToUpper()) : true))
                                 select lst).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                var res = from h in lstHeader
                          select new[] {
                            Convert.ToString(h.RequestId),
                            h.QualityProject,
                            Convert.ToString(h.Project),
                            Convert.ToString(h.RequestId),
                            Convert.ToString(h.HeaderId),
                            Convert.ToString( h.SeamNo),
                            Convert.ToString(h.StageCode),
                            Convert.ToString(h.StageSequence),
                            Convert.ToString(h.IterationNo),
                            Convert.ToString(h.TestResult),
                            Convert.ToString(h.OfferedBy),
                            h.OfferedOn != null ? Convert.ToDateTime(h.OfferedOn).ToString("dd/MM/yyyy") : "",
                            Convert.ToString(h.BU),
                            Convert.ToString( h.Location),
                            Convert.ToString(h.RequestNo),
                            Convert.ToString(h.RequestNoSequence),
                            Convert.ToString(h.InspectedBy),
                            h.InspectedOn != null ? Convert.ToDateTime(h.InspectedOn).ToString("dd/MM/yyyy") : "",
                            Convert.ToString(h.QCRemarks),
                            Convert.ToString(h.OfferedtoCustomerBy),
                            h.OfferedtoCustomerOn != null ? Convert.ToDateTime(h.OfferedtoCustomerOn).ToString("dd/MM/yyyy") : "",
                            Convert.ToString(h.Shop),
                            Convert.ToString(h.ShopLocation),
                            Convert.ToString(h.ShopRemark),
                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = "",

                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SeamListGroupClearanceRequest(string strRequestIds, string type, string qcremarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                ViewBag.RequestId = strRequestIds;
                List<int> arrayRequestIds = strRequestIds.Split(',').Select(int.Parse).ToList();

                string StageTypeExludedinOfferDropdown = clsImplementationEnum.ConfigParameter.StageTypeExludedinOfferDropdown.GetStringValue();
                var lstStagetypes = Manager.GetConfigValue(StageTypeExludedinOfferDropdown).ToUpper().Split(',').ToArray();
                var lstClearStages = (from q02 in db.QMS002
                                      where lstStagetypes.Contains(q02.StageType.ToUpper())
                                      select new { CatID = q02.StageCode, CatStageType = q02.StageType }).Distinct().ToList();
                int requestId = arrayRequestIds.FirstOrDefault();
                QMS050 ObjCheckQMS050Data = db.QMS050.Where(x => x.RequestId == requestId).FirstOrDefault();

                objResponseMsg.CheckStage = lstClearStages.Where(x => x.CatID == ObjCheckQMS050Data.StageCode).Count() > 0 ? true : false;
                objResponseMsg.CheckStageType = lstClearStages.Where(x => x.CatID == ObjCheckQMS050Data.StageCode).Select(x => x.CatStageType.ToUpper()).FirstOrDefault() ?? "";

                if (!objResponseMsg.CheckStage.Value)
                {
                    foreach (var headerid in arrayRequestIds)
                    {
                        QMS050 objQMS050 = db.QMS050.FirstOrDefault(x => x.RequestId == headerid);
                        int? isTPIOnline = db.SP_IPI_SEAMQTESTQCRESPOND(headerid, type, qcremarks, objClsLoginInfo.UserName, false).FirstOrDefault();

                        #region Send Notification
                        if (type.ToLower() == clsImplementationEnum.SeamListInspectionStatus.Cleared.GetStringValue().ToLower())
                        {
                            (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PROD3.GetStringValue(), objQMS050.Project, objQMS050.BU, objQMS050.Location, "Stage: " + objQMS050.StageCode + " of Seam: " + objQMS050.SeamNo + " & Project No: " + objQMS050.QualityProject + " has been Cleared", clsImplementationEnum.NotificationType.Information.GetStringValue());
                        }
                        #endregion

                        if (isTPIOnline.HasValue && isTPIOnline == 0)
                        {
                            var objQMS030 = db.QMS030.Where(z => z.QualityProject == objQMS050.QualityProject && z.Project == objQMS050.Project && z.BU == objQMS050.BU && z.Location == objQMS050.Location && z.SeamNo == objQMS050.SeamNo).FirstOrDefault();
                            if (objQMS030 != null)
                            {
                                Manager.ReadyToOffer_SEAM(objQMS030);
                            }

                            #region Code for make ready to offer parent part
                            //if (type == clsImplementationEnum.SeamListInspectionStatus.Cleared.GetStringValue() && !Manager.IsLTFPSProject(objQMS050.Project, objQMS050.BU, objQMS050.Location))
                            //{
                            //    string ApprovedStatus = clsImplementationEnum.ICLPartStatus.Approved.GetStringValue();
                            //    if (Manager.IsSEAMFullyCleared(objQMS050.Project, objQMS050.QualityProject, objQMS050.BU, objQMS050.Location, objQMS050.SeamNo))
                            //    {
                            //        QMS012 objQMS012 = db.QMS012.FirstOrDefault(c => c.Project == objQMS050.Project && c.QualityProject == objQMS050.QualityProject && c.SeamNo == objQMS050.SeamNo);
                            //        QMS035 objQMS035 = db.QMS035.FirstOrDefault(c => c.Project == objQMS050.Project && c.QualityProject == objQMS050.QualityProject && c.BU == objQMS050.BU && c.Location == objQMS050.Location && c.ChildPartNo == objQMS012.AssemblyNo && c.Status == ApprovedStatus);
                            //        if (objQMS035 != null)
                            //        {
                            //            Manager.ReadyToOffer_PART(objQMS035);
                            //        }
                            //    }
                            //}
                            #endregion

                        }

                    }
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Seam Successfully " + type;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error Occured, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SeamListGroupReturnRequest(string strRequestIds, string type, string QCRemarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<int> arrayRequestIds = strRequestIds.Split(',').Select(int.Parse).ToList();
                foreach (var headerid in arrayRequestIds)
                {
                    QMS050 objQMS050 = db.QMS050.FirstOrDefault(x => x.RequestId == headerid);
                    int? isTPIOnline = db.SP_IPI_SEAMQTESTQCRESPOND(headerid, type, QCRemarks, objClsLoginInfo.UserName, false).FirstOrDefault();

                    string NewfolderPath = "";
                    string OldfolderPath = "";
                    int iTableId = 0;
                    if (objQMS050.LTFPSHeaderId > 0)
                    {
                        OldfolderPath = "QMS050_LTFPS//" + objQMS050.LTFPSHeaderId + "//" + objQMS050.IterationNo + "//" + objQMS050.RequestNoSequence;
                        if (type == clsImplementationEnum.SeamListInspectionStatus.Rejected.GetStringValue())
                            NewfolderPath = "QMS050_LTFPS//" + objQMS050.LTFPSHeaderId + "//" + (objQMS050.IterationNo + 1) + "//" + objQMS050.RequestNoSequence;
                        else if (type == clsImplementationEnum.SeamListInspectionStatus.Returned.GetStringValue())
                            NewfolderPath = "QMS050_LTFPS//" + objQMS050.LTFPSHeaderId + "//" + objQMS050.IterationNo + "//" + (objQMS050.RequestNoSequence + 1);
                        iTableId = Convert.ToInt32(objQMS050.LTFPSHeaderId);
                    }
                    else
                    {
                        OldfolderPath = "QMS050//" + objQMS050.HeaderId.Value + "//" + objQMS050.IterationNo + "//" + objQMS050.RequestNoSequence;
                        if (type == clsImplementationEnum.SeamListInspectionStatus.Rejected.GetStringValue())
                            NewfolderPath = "QMS050//" + objQMS050.HeaderId + "//" + (objQMS050.IterationNo + 1) + "//" + objQMS050.RequestNoSequence;
                        else if (type == clsImplementationEnum.SeamListInspectionStatus.Returned.GetStringValue())
                            NewfolderPath = "QMS050//" + objQMS050.HeaderId + "//" + objQMS050.IterationNo + "//" + (objQMS050.RequestNoSequence + 1);

                        iTableId = Convert.ToInt32(objQMS050.HeaderId);
                    }

                    //(new clsFileUpload()).CopyFolderContentsAsync(OldfolderPath, NewfolderPath);
                    Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                    _objFUC.CopyDataOnFCSServerAsync(OldfolderPath, NewfolderPath, OldfolderPath, iTableId, NewfolderPath, iTableId, DESServices.CommonService.GetUseIPConfig, DESServices.CommonService.objClsLoginInfo.UserName, 0, false);

                    #region Send Notification
                    QMS050 objQMS050Noti = db.QMS050.Where(x => x.RequestNo == objQMS050.RequestNo && x.HeaderId == objQMS050.HeaderId && x.TestResult == null).OrderByDescending(o => o.IterationNo).FirstOrDefault();
                    if (objQMS050Noti != null && type.ToLower() == clsImplementationEnum.SeamListInspectionStatus.Rejected.GetStringValue().ToLower())
                    {
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PROD3.GetStringValue(), objQMS050.Project, objQMS050.BU, objQMS050.Location, "Stage: " + objQMS050.StageCode + " of Seam: " + objQMS050.SeamNo + " & Project No: " + objQMS050.QualityProject + " has been Rejected", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/MaintainSeamListOfferInspection/SendReOfferView/" + objQMS050Noti.RequestId);
                    }
                    else if (objQMS050Noti != null && type.ToLower() == clsImplementationEnum.SeamListInspectionStatus.Returned.GetStringValue().ToLower())
                    {
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PROD3.GetStringValue(), objQMS050.Project, objQMS050.BU, objQMS050.Location, "Stage: " + objQMS050.StageCode + " of Seam: " + objQMS050.SeamNo + " & Project No: " + objQMS050.QualityProject + " has been Returned", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/MaintainSeamListOfferInspection/SendReOfferView/" + objQMS050Noti.RequestId);
                    }
                    #endregion

                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Seam Successfully " + type;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error Occured, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion
        [HttpPost]
        public JsonResult LoadFilteredReqSeamData(JQueryDataTableParamModel param, string qualityProject, string project, string bu, string location, string SeamNo, string stageCode, int stageSeq)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[0]) + "," + Convert.ToString(Request["sColumns"].Split(',')[1]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                strWhere = "1=1";
                strWhere += string.Format(" and QualityProject in('{0}') and qms.Project in('{1}') and qms.BU in('{2}') and qms.Location in('{3}') and qms.SeamNo in('{4}') and qms.StageCode in('{5}') and qms.StageSequence in({6})", qualityProject, project, bu, location, SeamNo, stageCode, stageSeq);


                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = { "qms.SeamNo", "dbo.GET_PROJECTDESC_BY_CODE(qms.Project)", "qms.OfferedBy", "qms.CreatedBy", "qms.Location", "dbo.GET_IPI_STAGECODE_DESCRIPTION(qms.StageCode, qms.BU, qms.Location) ", "qms.RequestNo", "qms.InspectedBy", "qms.QCRemarks", "qms.OfferedtoCustomerBy", "qms.Shop", "qms.ShopLocation", "qms.ShopRemark" };
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                var lstResult = db.SP_IPI_SEAMLISTOFFERINSPECTION_DETAILS
                                (
                                StartIndex, EndIndex, "", strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.RequestNo),
                               Convert.ToString(uc.RequestNoSequence),
                               Convert.ToString(uc.IterationNo),
                               Convert.ToString(uc.TestResult),
                               Convert.ToString(uc.Shop),
                               Convert.ToString(uc.ShopLocation),
                               Convert.ToString(uc.ShopRemark),
                               Convert.ToString(uc.OfferedBy),
                               Convert.ToString(uc.OfferedOn),
                               Convert.ToString(uc.InspectedBy),
                               Convert.ToString(uc.InspectedOn),
                               Convert.ToString(uc.QCRemarks),
                               Convert.ToString(uc.OfferedtoCustomerBy),
                               Convert.ToString(uc.OfferedtoCustomerOn),
                               Convert.ToString(uc.TPIAgency1),
                               Convert.ToString(uc.TPIAgency1Intervention),
                               Convert.ToString(uc.TPIAgency1Result),
                               Convert.ToString(uc.TPIAgency1ResultOn),
                               Convert.ToString(uc.TPIAgency2),
                               Convert.ToString(uc.TPIAgency2Intervention),
                               Convert.ToString(uc.TPIAgency2Result),
                               Convert.ToString(uc.TPIAgency2ResultOn),
                               Convert.ToString(uc.TPIAgency3),
                               Convert.ToString(uc.TPIAgency3Intervention),
                               Convert.ToString(uc.TPIAgency3Result),
                               Convert.ToString(uc.TPIAgency3ResultOn),
                               Convert.ToString(uc.TPIAgency4),
                               Convert.ToString(uc.TPIAgency4Intervention),
                               Convert.ToString(uc.TPIAgency4Result),
                               Convert.ToString(uc.TPIAgency4ResultOn),
                               Convert.ToString(uc.TPIAgency5),
                               Convert.ToString(uc.TPIAgency5Intervention),
                               Convert.ToString(uc.TPIAgency5Result),
                               Convert.ToString(uc.TPIAgency5ResultOn),
                               Convert.ToString(uc.TPIAgency6),
                               Convert.ToString(uc.TPIAgency6Intervention),
                               Convert.ToString(uc.TPIAgency6Result),
                               Convert.ToString(uc.TPIAgency6ResultOn),
                               Convert.ToString(uc.Welder1),
                               Convert.ToString(uc.Welder2),
                               Convert.ToString(uc.Welder3),
                               Convert.ToString(uc.Welder4),
                               Convert.ToString(uc.Welder5),
                               Convert.ToString(uc.Welder6),
                               Convert.ToString(uc.Welder7),
                               Convert.ToString(uc.Welder8),
                               Convert.ToString(uc.Welder9),
                               Convert.ToString(uc.Welder10),
                               Convert.ToString(uc.Welder11),
                               Convert.ToString(uc.Welder12),
                                Convert.ToString(uc.Welder13),
                                Convert.ToString(uc.Welder14),
                                Convert.ToString(uc.Welder15),
                                Convert.ToString(uc.Welder16),
                                Convert.ToString(uc.Welder17),
                                Convert.ToString(uc.Welder18),
                                Convert.ToString(uc.Welder19),
                                Convert.ToString(uc.Welder20),
                                Convert.ToString(uc.Welder21),
                                Convert.ToString(uc.Welder22),
                                Convert.ToString(uc.Welder23),
                                Convert.ToString(uc.Welder24),
                               Convert.ToString(uc.WeldingProcess),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = 0,
                    iTotalDisplayRecords = 0,
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetSeamListOfferInspectionDataPartial(string status, string qualityProject = "", string seamNumber = "")
        {
            ViewBag.Status = status;
            ViewBag.QualityProject = qualityProject;
            ViewBag.SeamNumber = seamNumber;
            return PartialView("_GetSeamListOfferInspectionDataPartial");
        }
        [SessionExpireFilter]
        public ActionResult SendOfferPartial(int? headerId)
        {
            QMS040 objQMS040 = db.QMS040.Where(x => x.HeaderId == headerId).FirstOrDefault();
            var IsProtocolApplicable = db.QMS002.Where(x => x.Location == objQMS040.Location && x.BU == objQMS040.BU && x.StageCode == objQMS040.StageCode).Select(x => x.IsProtocolApplicable).FirstOrDefault();
            bool IsSeamNP = Manager.IsSeamNP(objQMS040.QualityProject, objQMS040.Project, objQMS040.Location, objQMS040.SeamNo);
            if (IsProtocolApplicable && !IsSeamNP)
                ViewBag.ProtocolApplicable = "Yes";
            else
                ViewBag.ProtocolApplicable = "No";


            if (Convert.ToString(objQMS040.ProtocolType) == clsImplementationEnum.ProtocolType.Other_Files.GetStringValue() && !IsSeamNP)
            {
                ViewBag.ProtocolFileRequired = "Yes";
            }
            else
            {
                ViewBag.ProtocolFileRequired = "No";
            }

            List<GLB002> lstGLB002 = Manager.GetSubCatagories("Shop", objQMS040.BU, objQMS040.Location).ToList();
            List<BULocWiseCategoryModel> lstBULocWiseCategoryModel = lstGLB002.Select(x => new BULocWiseCategoryModel { CatDesc = (x.Code + "-" + x.Description), CatID = x.Code }).ToList();
            ViewBag.lstSubCatagories = new JavaScriptSerializer().Serialize(lstBULocWiseCategoryModel);

            List<GLB002> lstGLB002SurfaceCondition = Manager.GetSubCatagories("Surface Condition", objQMS040.BU, objQMS040.Location).ToList();
            List<BULocWiseCategoryModel> lstBULocWiseSurfaceCondition = lstGLB002SurfaceCondition.Select(x => new BULocWiseCategoryModel { CatDesc = (x.Description), CatID = x.Code }).ToList();
            ViewBag.lstSurfaceCondition = new JavaScriptSerializer().Serialize(lstBULocWiseSurfaceCondition);

            SeamListSendOffer objSeamListSendOffer = new SeamListSendOffer();
            objSeamListSendOffer.HeaderId = objQMS040.HeaderId;
            bool IsSTAGEDEPARTMENTNDE = db.Database.SqlQuery<bool>("SELECT dbo.FUN_GET_IPI_ISSTAGEDEPARTMENTNDE('" + objQMS040.HeaderId + "')").FirstOrDefault();

            objQMS040.CreatedBy = Manager.GetPsidandDescription(objQMS040.CreatedBy);
            ViewBag.IsSTAGEDEPARTMENTNDE = IsSTAGEDEPARTMENTNDE;


            var iter = db.QMS040.Where(x => x.HeaderId == objSeamListSendOffer.HeaderId).FirstOrDefault();

            if (IsSTAGEDEPARTMENTNDE)
            {
                objSeamListSendOffer.folderPath = "QMS060//" + objSeamListSendOffer.HeaderId + "//" + iter.IterationNo + "//" + 1;
            }
            else
                objSeamListSendOffer.folderPath = "QMS050//" + objSeamListSendOffer.HeaderId + "//" + iter.IterationNo + "//" + 1;

            #region Fetch data from request (16014 :While offering stage, system should copy offer data from previous stage to new stage.)
            var objPreviousStageSeq = (from qms40 in db.QMS040
                                       where qms40.QualityProject == objQMS040.QualityProject && qms40.Project == objQMS040.Project && qms40.SeamNo == objQMS040.SeamNo
                                             && qms40.Location == objQMS040.Location && qms40.BU == objQMS040.BU
                                             && qms40.StageSequence <= objQMS040.StageSequence && qms40.HeaderId != objQMS040.HeaderId
                                       orderby qms40.StageSequence descending
                                       select qms40).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            string SurfaceCondition = string.Empty, ShopDesc = string.Empty;
            if (objPreviousStageSeq != null)
            {
                bool IsSTAGENDE = db.Database.SqlQuery<bool>("SELECT dbo.FUN_GET_IPI_ISSTAGEDEPARTMENTNDE('" + objPreviousStageSeq.HeaderId + "')").FirstOrDefault();
                if (IsSTAGENDE)
                {
                    QMS060 objQMS060 = db.QMS060
                                        .Where(x => x.HeaderId == objPreviousStageSeq.HeaderId)
                                        .OrderByDescending(x => x.OfferedOn)
                                        .FirstOrDefault();
                    if (objQMS060 != null)
                    {
                        ShopDesc = objNDEModels.GetCategory("Shop", objQMS060.Shop, objQMS060.BU, objQMS060.Location).CategoryDescription;
                        objSeamListSendOffer.Shop = objQMS060.Shop;
                        objSeamListSendOffer.Location = objQMS060.ShopLocation;
                        objSeamListSendOffer.ShopRemark = objQMS060.ShopRemark;
                        SurfaceCondition = objNDEModels.GetCategory("Surface Condition", objQMS060.SurfaceCondition, objQMS060.BU, objQMS060.Location, true).CategoryDescription;
                        objSeamListSendOffer.SurfaceCondition = objQMS060.SurfaceCondition;
                        objSeamListSendOffer.WeldThicknessReinforcement = objQMS060.WeldThicknessReinforcement;
                    }
                }
                else
                {
                    QMS050 objQMS050 = db.QMS050
                                        .Where(x => x.HeaderId == objPreviousStageSeq.HeaderId)
                                        .OrderByDescending(x => x.OfferedOn)
                                        .FirstOrDefault();
                    if (objQMS050 != null)
                    {
                        ShopDesc = objNDEModels.GetCategory("Shop", objQMS050.Shop, objQMS050.BU, objQMS050.Location).CategoryDescription;
                        objSeamListSendOffer.Shop = objQMS050.Shop;
                        objSeamListSendOffer.Location = objQMS050.ShopLocation;
                        objSeamListSendOffer.ShopRemark = objQMS050.ShopRemark;
                    }
                }
            }
            var objQMS002 = db.QMS002.Where(x => x.Location == objQMS040.Location && x.BU == objQMS040.BU && x.StageCode == objQMS040.StageCode).FirstOrDefault();
            var ReinforcementNotApplicableFor = clsImplementationEnum.ConfigParameter.ReinforcementNotApplicableFor.GetStringValue();
            var lstNotApplicableFor = Manager.GetConfigValue(ReinforcementNotApplicableFor).ToUpper().Split(',').ToArray();

            ViewBag.IsWelderApplicable = objQMS002 != null ? objQMS002.IsWelderApplicable : false;
            ViewBag.IsPTMT = objQMS002 != null ? (lstNotApplicableFor.Contains(objQMS002.StageType) ? true : false) : false;

            ViewBag.ShopDesc = ShopDesc;
            ViewBag.SurfaceConditionCode = objQMS002 != null ? objQMS002.DefaultSurfaceCondition : string.Empty;
            ViewBag.SurfaceCondition = objQMS002 != null ? objNDEModels.GetCategory("Surface Condition", objQMS002.DefaultSurfaceCondition, objQMS002.BU, objQMS002.Location, true).CategoryDescription : SurfaceCondition;
            ViewBag.DefaultSurfaceCondition = !string.IsNullOrWhiteSpace(objQMS002.DefaultSurfaceCondition);
            #endregion
            ViewBag.IsOverlay = Manager.IsOverlaySeam(objQMS040.QualityProject, objQMS040.SeamNo);

            return PartialView("_SendOfferPartial", objSeamListSendOffer);
        }
        [SessionExpireFilter]
        public ActionResult SeamListApproval(string tab)
        {
            ViewBag.Tab = tab;
            ViewBag.Location = objClsLoginInfo.Location;
            return View();
        }
        [SessionExpireFilter]
        public ActionResult SeamListOfferInspection(int id, string from = "")
        {
            SP_IPI_SEAMLISTOFFERINSPECTION_Result result = db.SP_IPI_SEAMLISTOFFERINSPECTION(null, null, "", " qms.HeaderId = " + id).FirstOrDefault();
            ViewBag.SeamCleared = result.SeamCleared == true ? "Yes" : "No";
            ViewBag.CreatedOn = Convert.ToString(result.CreatedOn);
            ViewBag.fromURL = from;

            QMS040 objQMS040 = db.QMS040.Where(c => c.HeaderId == id).FirstOrDefault();

            ViewBag.IsOverlay = Manager.IsOverlaySeam(objQMS040.QualityProject, objQMS040.SeamNo);
            ViewBag.IsWeldLengthRequired = (!Manager.IsNDERequestConfirmationRequiredOnFirstInteration(objQMS040.Project, objQMS040.BU, objQMS040.Location) && Manager.IsNDERequest(objQMS040.StageCode, objQMS040.BU, objQMS040.Location));
            ViewBag.IsGroundSpot = db.QMS002.Where(x => x.StageCode == objQMS040.StageCode && x.BU == objQMS040.BU && x.Location == objQMS040.Location).Select(x => x.IsGroundSpot).FirstOrDefault();
            ViewBag.IsSTAGEDEPARTMENTNDE = db.Database.SqlQuery<bool>("SELECT dbo.FUN_GET_IPI_ISSTAGEDEPARTMENTNDE('" + id + "')").FirstOrDefault();

            if (!string.IsNullOrWhiteSpace(Convert.ToString(objQMS040.ProtocolType)))
            {
                if (objQMS040.ProtocolId.HasValue)
                {
                    ViewBag.ProtocolURL = WebsiteURL + "/PROTOCOL/" + result.ProtocolType + "/Linkage/" + result.ProtocolId + ((string.Equals(objQMS040.InspectionStatus, clsImplementationEnum.SeamListInspectionStatus.ReadyToOffer.GetStringValue(), StringComparison.OrdinalIgnoreCase) || string.Equals(objQMS040.InspectionStatus, clsImplementationEnum.SeamListInspectionStatus.Rework.GetStringValue(), StringComparison.OrdinalIgnoreCase) || string.Equals(objQMS040.InspectionStatus, clsImplementationEnum.SeamListInspectionStatus.Returned.GetStringValue(), StringComparison.OrdinalIgnoreCase) || string.Equals(objQMS040.InspectionStatus, clsImplementationEnum.SeamListInspectionStatus.Rejected.GetStringValue(), StringComparison.OrdinalIgnoreCase)) ? "?m=1" : "");
                }
                else
                {
                    QMS031 objQMS031 = db.QMS031.FirstOrDefault(c => c.Project == objQMS040.Project && c.QualityProject == objQMS040.QualityProject && c.BU == objQMS040.BU && c.Location == objQMS040.Location && c.SeamNo == objQMS040.SeamNo && c.StageCode == objQMS040.StageCode && c.StageSequance == objQMS040.StageSequence);
                    if (objQMS031 != null)
                    {
                        ViewBag.ProtocolURL = "OpenAttachmentPopUp(" + objQMS031.LineId + ",false,'QMS031/" + objQMS031.LineId + "');";
                    }
                }
            }

            #region Get Welder Stamps
            string[] arrWelders = new string[24];
            clsHelper.Welders objWelderDetails = GetWelderStamps(objQMS040.HeaderId);
            int i = 0;
            foreach (var item in objWelderDetails.WelderStamps)
            {
                arrWelders[i] = item;
                i = i + 1;
                if (i > 23)
                {
                    break;
                }
            }
            ViewBag.Welders = arrWelders;
            ViewBag.WeldingProcess = objWelderDetails.WeldingProcess;
            #endregion
            if (Helper.IsChemstage(objQMS040.StageCode, objQMS040.BU, objQMS040.Location))
            {

                ViewBag.IsChemStage = true;
            }
            else
            {
                ViewBag.IsChemStage = false;
            }
            return View(result);
        }

        [HttpPost]
        public ActionResult CopySeamDetails(string QualityProject, string StageCode, string ProtocolType)
        {
            string Readytooffer = clsImplementationEnum.TravelerHeaderStatus.Readytooffer.GetStringValue();
            ViewBag.QualityProjectSeamList = db.QMS040.Where(m =>
                                                    m.QualityProject.ToLower() == QualityProject.ToLower() &&
                                                    m.InspectionStatus.ToLower() == Readytooffer.ToLower() &&
                                                    m.IterationNo == 1 &&
                                                    m.StageCode.ToLower() == StageCode.ToLower() &&
                                                    m.ProtocolType.ToLower() == ProtocolType.ToLower()
                                                    ).OrderBy(i => i.SeamNo).Select(m => new { Text = m.SeamNo, Value = m.SeamNo }).Distinct().ToList();

            return PartialView("_CopySeamPopupPartial");
        }

        [HttpPost]
        public ActionResult CopyProtocol(int headerId, string fromSeam, string toSeam)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS040 objQMS040 = db.QMS040.Where(c => c.HeaderId == headerId).FirstOrDefault();
                string Readytooffer = clsImplementationEnum.TravelerHeaderStatus.Readytooffer.GetStringValue();
                #region Get Destination Seams List For Copy

                List<QMS040> allProtocols = db.QMS040.Where(i =>
                                                            i.IterationNo == 1 &&
                                                            i.StageCode == objQMS040.StageCode &&
                                                            i.QualityProject == objQMS040.QualityProject &&
                                                            i.InspectionStatus.ToLower() == Readytooffer.ToLower() &&
                                                            i.ProtocolType == objQMS040.ProtocolType
                                                            ).OrderBy(i => i.SeamNo).ToList();
                List<int?> newprototocolList = new List<int?>();
                bool start = false;
                foreach (QMS040 Protocols in allProtocols)
                {
                    if (Protocols.SeamNo == fromSeam)
                    {
                        start = true;
                    }
                    if (start)
                        newprototocolList.Add(Protocols.ProtocolId);
                    if (Protocols.SeamNo == toSeam)
                    {
                        break;
                    }
                }
                #endregion
                foreach (var ToProtocol in newprototocolList)
                {
                    string role = clsImplementationEnum.ProtocolRoleType.A_Actualvalue_for_PROD.GetStringValue();
                    #region Copy Other files
                    db.SP_IPI_PROTOCOL_COPY_PROTOCOL_PRODUCTION_DATA(objQMS040.ProtocolType, objQMS040.ProtocolId, ToProtocol, objClsLoginInfo.UserName, role);
                    #endregion
                }

                if (newprototocolList.Count > 0)
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = newprototocolList.Count.ToString() + " Protocols has been updated.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No any Protocols available for copy protocol.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        public ActionResult ReturnRequestDetails(int id, string from)
        {
            QMS060 objQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == id);
            var objQMS002 = db.QMS002.Where(x => x.Location == objQMS060.Location && x.BU == objQMS060.BU && x.StageCode == objQMS060.StageCode).FirstOrDefault();
            ViewBag.IsLTFPS = Manager.IsLTFPSProject(objQMS060.Project, objQMS060.BU, objQMS060.Location);

            List<GLB002> lstGLB002 = Manager.GetSubCatagories("Shop", objQMS060.BU, objQMS060.Location).ToList();
            List<BULocWiseCategoryModel> lstBULocWiseCategoryModel = lstGLB002.Select(x => new BULocWiseCategoryModel { CatDesc = (x.Code + " - " + x.Description), CatID = x.Code }).ToList();
            ViewBag.lstSubCatagories = new JavaScriptSerializer().Serialize(lstBULocWiseCategoryModel);

            List<GLB002> lstGLB002SurfaceCondition = Manager.GetSubCatagories("Surface Condition", objQMS060.BU, objQMS060.Location).ToList();
            List<BULocWiseCategoryModel> lstBULocWiseSurfaceCondition = lstGLB002SurfaceCondition.Select(x => new BULocWiseCategoryModel { CatDesc = (x.Description), CatID = x.Code }).ToList();
            ViewBag.lstSurfaceCondition = new JavaScriptSerializer().Serialize(lstBULocWiseSurfaceCondition);

            ViewBag.Shop = GetCategoryDescriptionFromList(lstGLB002, objQMS060.Shop);
            string SurfaceCondition = GetCategoryDescriptionOnly(lstGLB002SurfaceCondition, objQMS060.SurfaceCondition);
            ViewBag.SurfaceCondition = objQMS002.DefaultSurfaceCondition != null ? GetCategoryDescriptionOnly(lstGLB002SurfaceCondition, objQMS002.DefaultSurfaceCondition) : SurfaceCondition;
            ViewBag.DefaultSurfaceCondition = !string.IsNullOrWhiteSpace(objQMS002.DefaultSurfaceCondition);
            ViewBag.SurfaceConditionCode = objQMS002.DefaultSurfaceCondition != null ? objQMS002.DefaultSurfaceCondition : string.Empty;
            objQMS060.Project = Manager.GetProjectAndDescription(objQMS060.Project);
            objQMS060.StageCode = Manager.GetStageCodeDescriptionFromCode(objQMS060.StageCode, objQMS060.BU, objQMS060.Location);
            objQMS060.SeamNo = db.QMS012.Where(x => x.QualityProject == objQMS060.QualityProject && x.SeamNo == objQMS060.SeamNo).Select(x => x.SeamNo + "-" + x.SeamDescription).FirstOrDefault();
            List<GLB002> lstCategory = Manager.GetSubCatagories("TPI Agency", objQMS060.BU, objQMS060.Location, null);
            List<GLB002> lstCategoryTPI = Manager.GetSubCatagories("TPI Intervention", objQMS060.BU, objQMS060.Location, null);
            objQMS060.TPIAgency1 = GetCategoryDescriptionFromList(lstCategory, objQMS060.TPIAgency1);
            objQMS060.TPIAgency2 = GetCategoryDescriptionFromList(lstCategory, objQMS060.TPIAgency2);
            objQMS060.TPIAgency3 = GetCategoryDescriptionFromList(lstCategory, objQMS060.TPIAgency3);
            objQMS060.TPIAgency4 = GetCategoryDescriptionFromList(lstCategory, objQMS060.TPIAgency4);
            objQMS060.TPIAgency5 = GetCategoryDescriptionFromList(lstCategory, objQMS060.TPIAgency5);
            objQMS060.TPIAgency6 = GetCategoryDescriptionFromList(lstCategory, objQMS060.TPIAgency6);
            objQMS060.TPIAgency1Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS060.TPIAgency1Intervention);
            objQMS060.TPIAgency2Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS060.TPIAgency2Intervention);
            objQMS060.TPIAgency3Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS060.TPIAgency3Intervention);
            objQMS060.TPIAgency4Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS060.TPIAgency4Intervention);
            objQMS060.TPIAgency5Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS060.TPIAgency5Intervention);
            objQMS060.TPIAgency6Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS060.TPIAgency6Intervention);
            objQMS060.Location = db.COM002.Where(x => x.t_dimx == objQMS060.Location).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
            objQMS060.BU = db.COM002.Where(x => x.t_dimx == objQMS060.BU).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
            objQMS060.OfferedBy = Manager.GetPsidandDescription(objQMS060.OfferedBy);
            objQMS060.QCReturnedBy = Manager.GetPsidandDescription(objQMS060.QCIsReturned == true ? objQMS060.QCReturnedBy : (objQMS060.NDEIsReturned == true ? objQMS060.NDEReturnedBy : ""));
            objQMS060.QCReturnedOn = objQMS060.QCIsReturned == true ? objQMS060.QCReturnedOn : (objQMS060.NDEIsReturned == true ? objQMS060.NDEReturnedOn : null);
            objQMS060.QCReturnedRemarks = objQMS060.QCIsReturned == true ? objQMS060.QCReturnedRemarks : (objQMS060.NDEIsReturned == true ? objQMS060.NDEReturnedRemarks : "");
            ViewBag.IsQCReturned = (objQMS060.QCIsReturned == true ? "Yes" : "No");
            ViewBag.IsNDEReturned = (objQMS060.NDEIsReturned == true ? "Yes" : "No");
            ViewBag.IsPTMTCellRequired = (objQMS060.IsPTMTCellRequired == true ? "Yes" : "No");
            ViewBag.fromURL = from;

            var ReinforcementNotApplicableFor = clsImplementationEnum.ConfigParameter.ReinforcementNotApplicableFor.GetStringValue();
            var lstNotApplicableFor = Manager.GetConfigValue(ReinforcementNotApplicableFor).ToUpper().Split(',').ToArray();
            ViewBag.IsWelderApplicable = objQMS002 != null ? objQMS002.IsWelderApplicable : false;
            ViewBag.IsPTMT = objQMS002 != null ? (lstNotApplicableFor.Contains(objQMS002.StageType) ? true : false) : false;

            //if (string.IsNullOrEmpty(objQMS060.Part3thickness))
            //{
            //    var thicknessposition = objQMS060.OverlayThickness.Split(',');
            //    if (!string.IsNullOrEmpty(objQMS060.OverlayThickness))
            //    {
            //        if (thicknessposition.Length >= 6)
            //        {
            //            objQMS060.Part3thickness = thicknessposition[5].ToString();
            //        }
            //    }
            //}

            #region Get Welder Stamps
            if (objQMS060.Welder1 == null)
            {
                int _Count = 1;
                clsHelper.Welders objWelderDetails = new clsHelper.Welders();
                if (ViewBag.IsLTFPS)
                {
                    objWelderDetails = (new SeamListNDEInspectionController()).GetWelderStamps(objQMS060.LTF002.LineId, true);
                }
                else
                {
                    objWelderDetails = (new SeamListNDEInspectionController()).GetWelderStamps(objQMS060.QMS040.HeaderId, false);
                }

                foreach (var item in objWelderDetails.WelderStamps)
                {
                    #region maintain welder weld length
                    switch (_Count)
                    {
                        case 1:
                            objQMS060.Welder1 = item;
                            break;
                        case 2:
                            objQMS060.Welder2 = item;
                            break;
                        case 3:
                            objQMS060.Welder3 = item;
                            break;
                        case 4:
                            objQMS060.Welder4 = item;
                            break;
                        case 5:
                            objQMS060.Welder5 = item;
                            break;
                        case 6:
                            objQMS060.Welder6 = item;
                            break;
                        case 7:
                            objQMS060.Welder7 = item;
                            break;
                        case 8:
                            objQMS060.Welder8 = item;
                            break;
                        case 9:
                            objQMS060.Welder9 = item;
                            break;
                        case 10:
                            objQMS060.Welder10 = item;
                            break;
                        case 11:
                            objQMS060.Welder11 = item;
                            break;
                        case 12:
                            objQMS060.Welder12 = item;
                            break;
                        case 13:
                            objQMS060.Welder13 = item;
                            break;
                        case 14:
                            objQMS060.Welder14 = item;
                            break;
                        case 15:
                            objQMS060.Welder15 = item;
                            break;
                        case 16:
                            objQMS060.Welder16 = item;
                            break;
                        case 17:
                            objQMS060.Welder17 = item;
                            break;
                        case 18:
                            objQMS060.Welder18 = item;
                            break;
                        case 19:
                            objQMS060.Welder19 = item;
                            break;
                        case 20:
                            objQMS060.Welder20 = item;
                            break;
                        case 21:
                            objQMS060.Welder21 = item;
                            break;
                        case 22:
                            objQMS060.Welder22 = item;
                            break;
                        case 23:
                            objQMS060.Welder23 = item;
                            break;
                        case 24:
                            objQMS060.Welder24 = item;
                            break;
                        default:
                            break;
                    }
                    _Count++;
                    #endregion
                }
                objQMS060.WeldingProcess = objWelderDetails.WeldingProcess;
            }
            #endregion

            return View(objQMS060);
        }

        public bool IsApplicableForNDEReOfferReturnedRequest(int RequestId, string InspectionFor)
        {
            bool isApplicable = false;

            if (InspectionFor == clsImplementationEnum.InspectionFor.SEAM.GetStringValue())
            {
                QMS060 objQMS060 = db.QMS060.Where(x => x.RequestId == RequestId).FirstOrDefault();
                if (objQMS060.RequestStatus == clsImplementationEnum.NDESeamRequestStatus.NotAttended.GetStringValue() && (objQMS060.QCIsReturned == true || objQMS060.NDEIsReturned == true))
                {
                    isApplicable = true;
                }
            }
            else
            {
                QMS065 objQMS065 = db.QMS065.Where(x => x.RequestId == RequestId).FirstOrDefault();
                if (objQMS065.RequestStatus == clsImplementationEnum.NDESeamRequestStatus.NotAttended.GetStringValue() && objQMS065.QCIsReturned == true)
                {
                    isApplicable = true;
                }
            }

            return isApplicable;
        }

        [HttpPost]
        public ActionResult ReOfferNDERequest(int RequestId, string Shop, string ShopLocation, string SurfaceCondition, string WeldThicknessReinforcement, string ShopRemark, string Part3thickness,string Part6thickness, string Part6Material)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (!IsApplicableForNDEReOfferReturnedRequest(RequestId, clsImplementationEnum.InspectionFor.SEAM.GetStringValue()))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "This request has been already offered";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                QMS060 objQMS060 = db.QMS060.Where(x => x.RequestId == RequestId).FirstOrDefault();
                //if (objQMS060 != null)
                //{
                //    if (string.IsNullOrEmpty(objQMS060.Part3thickness))
                //    {
                //        objResponseMsg.Key = true;
                //        var thicknessposition = objQMS060.OverlayThickness.Split(',');
                //        string thicknessposition6 = "";
                //        if (!string.IsNullOrEmpty(objQMS060.OverlayThickness))
                //        {
                //            if (thicknessposition.Length >= 6)
                //            {
                //                thicknessposition6 = thicknessposition[5].ToString();
                //                if (objQMS060.Part3Material != null)
                //                {
                //                    objResponseMsg.Key = false;
                //                    objResponseMsg.Value = "You can't Re-offer this stage without maintain overlay material";
                //                }
                //                else
                //                {
                //                    objResponseMsg.Key = true;
                //                }
                //            }
                //            else
                //            {
                //                objResponseMsg.Key = true;
                //            }
                //        }
                //        else
                //        {
                //            objResponseMsg.Key = true;
                //        }
                //    }
                //    else
                //    {
                //        if (string.IsNullOrEmpty(objQMS060.Part3Material))
                //        {
                //            objResponseMsg.Key = false;
                //            objResponseMsg.Value = "You can't Re-offer this stage without maintain overlay material";
                //        }
                //        else
                //        {
                //            objResponseMsg.Key = true;
                //        }

                //    }
                //}

                bool isLTFPSProject = Manager.IsLTFPSProject(objQMS060.Project, objQMS060.BU, objQMS060.Location);
                string parallelRequestId = RequestId.ToString();

                if (objQMS060.NDEIsReturned != true)
                {
                    if (isLTFPSProject)
                    {
                        parallelRequestId = db.Database.SqlQuery<string>("SELECT dbo.FUN_GET_LTFPS_PARALLEL_SEAM_NDE_REQUEST('" + objQMS060.RequestId + "')").FirstOrDefault();
                    }
                    else
                    {
                        parallelRequestId = db.Database.SqlQuery<string>("SELECT dbo.FUN_GET_IPI_PARALLEL_SEAM_NDE_REQUEST('" + objQMS060.RequestId + "')").FirstOrDefault();
                    }
                }

                List<int> parallelId = new List<int>();

                if (objQMS060.IterationNo > 1)
                {
                    parallelId.Add(RequestId);
                }
                else
                {
                    parallelId = parallelRequestId.Split(',').Select(Int32.Parse).ToList();
                }
                //if (objResponseMsg.Key)
                //{
                foreach (var reqId in parallelId)
                {
                    QMS060 objDestQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == reqId);
                    if (objDestQMS060.QCIsReturned == true)
                    {
                        objDestQMS060.QCIsReturned = null;
                    }
                    else if (objDestQMS060.NDEIsReturned == true)
                    {
                        objDestQMS060.NDEIsReturned = null;
                        if (isLTFPSProject)
                        {
                            objDestQMS060.LTF002.InspectionStatus = clsImplementationEnum.SeamListInspectionStatus.OfferedToNDE.GetStringValue();
                        }
                        else
                        {
                            objDestQMS060.QMS040.InspectionStatus = clsImplementationEnum.SeamListInspectionStatus.OfferedToNDE.GetStringValue();
                        }
                    }

                    objDestQMS060.Shop = Shop;
                    objDestQMS060.ShopLocation = ShopLocation;
                    objDestQMS060.SurfaceCondition = SurfaceCondition;
                    objDestQMS060.WeldThicknessReinforcement = !string.IsNullOrWhiteSpace(WeldThicknessReinforcement) ? Convert.ToInt32(WeldThicknessReinforcement) : (int?)null;
                    objDestQMS060.ShopRemark = ShopRemark;
                    //if (!string.IsNullOrEmpty(Part3thickness))
                    //{
                    //    objQMS060.Part3thickness = Part3thickness;
                    //}
                    objDestQMS060.OfferedBy = objClsLoginInfo.UserName;
                    objDestQMS060.OfferedOn = DateTime.Now;
                    objDestQMS060.RequestGeneratedOn = (objDestQMS060.RequestGeneratedOn != null ? DateTime.Now : (DateTime?)null);
                    objDestQMS060.ConfirmedOn = (objDestQMS060.ConfirmedOn != null ? DateTime.Now : (DateTime?)null);
                    objDestQMS060.Part6Material = Part6Material;
                    objDestQMS060.Part6thickness = Part6thickness;

                    db.SaveChanges();

                    #region Maintain Return Request History
                    QMS062 objQMS062 = new QMS062();
                    objQMS062.RequestId = objDestQMS060.RequestId;
                    objQMS062.OfferedBy = objClsLoginInfo.UserName;
                    objQMS062.OfferedOn = DateTime.Now;
                    objQMS062.OfferedRemarks = objDestQMS060.ShopRemark;
                    objQMS062.CreatedBy = objClsLoginInfo.UserName;
                    objQMS062.CreatedOn = DateTime.Now;
                    db.QMS062.Add(objQMS062);
                    db.SaveChanges();
                    #endregion

                }


                objResponseMsg.Key = true;
                objResponseMsg.Value = (parallelId.Count > 1 ? "This stage is having parallel stage. So both will get offered" : "The stage " + objQMS060.StageCode + "  has been offered");

                #region Send Notification
                if (objQMS060.RequestGeneratedBy != null)
                {
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE3.GetStringValue(), objQMS060.Project, objQMS060.BU, objQMS060.Location, "NDT Request " + objQMS060.RequestNo + " for Stage: " + objQMS060.StageCode + " of Seam: " + objQMS060.SeamNo + " & Project No: " + objQMS060.QualityProject + " has been offered and ready for your further action", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/SeamListNDEInspection/NDESeamTestDetails/" + objQMS060.RequestId);
                }
                else
                {
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), objQMS060.Project, objQMS060.BU, objQMS060.Location, "Stage: " + objQMS060.StageCode + " of Seam: " + objQMS060.SeamNo + " & Project No: " + objQMS060.QualityProject + " has been offered for Inspection", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/SeamListNDEInspection/QCSeamTestDetails/" + objQMS060.RequestId);
                }
                #endregion
                //}
                return Json(objResponseMsg);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error Occures Please try again";
                return Json(objResponseMsg);
            }

        }

        [HttpPost]
        public ActionResult CaptureWeldersBeforeOffer(int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            List<string> _weldingProcess = new List<string>();
            List<WPS025> lstWPS025 = new List<WPS025>();
            QMS040 objQMS040 = db.QMS040.FirstOrDefault(x => x.HeaderId == headerId);
            List<QMS061> lstWelderList = db.QMS061.Where(x => x.HeaderId == headerId).ToList();

            List<string> lstAssigedWeldwers = lstWelderList.Select(y => y.welderstamp).ToList();

            //lstWPS025 = db.WPS025.Where(x => x.BU == objQMS040.BU && x.Location == objQMS040.Location && x.QualityProject == objQMS040.QualityProject && x.Project == objQMS040.Project && x.SeamNo == objQMS040.SeamNo).OrderByDescending(o => o.PrintedOn).ToList();
            lstWPS025 = db.WPS025.Where(x => x.BU == objQMS040.BU && x.QualityProject == objQMS040.QualityProject && x.Project == objQMS040.Project && x.SeamNo == objQMS040.SeamNo).OrderBy(o => o.PrintedOn).ToList();

            if (lstWPS025.Count > 0)
            {
                List<QMS061> lstQMS061 = new List<QMS061>();
                List<string> lstWelders = new List<string>();
                int count = 1;
                //if (lstWelderList.Count > 0)
                //{
                //    count = lstWelderList.Count + 1;
                //}
                foreach (var wps25 in lstWPS025)
                {
                    if (!lstWelders.Contains(wps25.WelderStamp) && !lstWelderList.Select(y => y.welderstamp).Contains(wps25.WelderStamp))
                    {
                        QMS061 objQMS061 = new QMS061
                        {
                            HeaderId = Convert.ToInt32(objQMS040.HeaderId),
                            RefRequestId = null,
                            QualityProject = objQMS040.QualityProject,
                            Project = objQMS040.Project,
                            BU = objQMS040.BU,
                            Location = objQMS040.Location,
                            SeamNo = objQMS040.SeamNo,
                            StageCode = objQMS040.StageCode,
                            StageSequence = objQMS040.StageSequence,
                            IterationNo = null,
                            RequestNo = null,
                            RequestNoSequence = null,
                            welderstamp = wps25.WelderStamp,
                            WeldingProcess = wps25.WeldingProcess,
                            CreatedBy = objClsLoginInfo.UserName,
                            CreatedOn = DateTime.Now
                        };
                        lstQMS061.Add(objQMS061);
                        lstWelders.Add(wps25.WelderStamp);
                        _weldingProcess.Add(wps25.WeldingProcess);
                        count++;
                    }
                    else
                    {
                        if (lstWelderList.Select(y => y.welderstamp).Contains(wps25.WelderStamp))
                        {
                            QMS061 existingWelder = lstWelderList.FirstOrDefault(x => x.welderstamp == wps25.WelderStamp);
                            if (existingWelder != null && !existingWelder.WeldingProcess.Split('+').Contains(wps25.WeldingProcess))
                                existingWelder.WeldingProcess = existingWelder.WeldingProcess + "+" + wps25.WeldingProcess;
                        }
                        else if (lstQMS061.Select(y => y.welderstamp).Contains(wps25.WelderStamp))
                        {
                            lstQMS061.Where(w => w.welderstamp == wps25.WelderStamp && !w.WeldingProcess.Split('+').Contains(wps25.WeldingProcess)).ToList().ForEach(x => x.WeldingProcess = (x.WeldingProcess + "+" + wps25.WeldingProcess));
                        }

                        _weldingProcess.Add(wps25.WeldingProcess);

                    }
                }
                db.QMS061.AddRange(lstQMS061);
                db.SaveChanges();

                objResponseMsg.Remarks = string.Join("+", _weldingProcess.Distinct());
                objResponseMsg.Status = string.Join(",", lstWelders);
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Welders Captured Successfully";
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please add Welders in Welder wise Parameter Slip";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.Welders GetWelderStamps(int headerId, bool IsLTFPS = false)
        {
            clsHelper.Welders objResponseMsg = new clsHelper.Welders();
            List<QMS061> lstQMS061 = new List<QMS061>();

            CopyWelderWeldLengthFromPreviousNDEStage(headerId, IsLTFPS);

            bool isAlreadyMaintained = false;

            if (IsLTFPS)
            {
                lstQMS061 = db.QMS061.Where(c => c.LTFPSHeaderId == headerId).ToList();
            }
            else
            {
                lstQMS061 = db.QMS061.Where(c => c.HeaderId == headerId).ToList();
            }
            if (lstQMS061.Count > 0)
            {
                isAlreadyMaintained = true;
            }

            List<string> _weldingProcess = new List<string>();
            List<string> lstWelders = new List<string>();

            if (!isAlreadyMaintained)
            {
                #region Get Welders from Parameter slip

                List<WPS025> lstWPS025 = new List<WPS025>();
                if (IsLTFPS)
                {
                    LTF002 objLTF002 = db.LTF002.FirstOrDefault(x => x.LineId == headerId);
                    lstWPS025 = db.WPS025.Where(x => x.BU == objLTF002.BU && x.Location == objLTF002.Location && x.QualityProject == objLTF002.QualityProject && x.Project == objLTF002.Project && x.SeamNo == objLTF002.SeamNo).OrderBy(o => o.PrintedOn).ToList();
                }
                else
                {
                    QMS040 objQMS040 = db.QMS040.FirstOrDefault(x => x.HeaderId == headerId);
                    //lstWPS025 = db.WPS025.Where(x => x.BU == objQMS040.BU && x.Location == objQMS040.Location && x.QualityProject == objQMS040.QualityProject && x.Project == objQMS040.Project && x.SeamNo == objQMS040.SeamNo).OrderByDescending(o => o.PrintedOn).ToList();
                    lstWPS025 = db.WPS025.Where(x => x.BU == objQMS040.BU && x.QualityProject == objQMS040.QualityProject && x.Project == objQMS040.Project && x.SeamNo == objQMS040.SeamNo).OrderBy(o => o.PrintedOn).ToList();
                }
                foreach (var wps25 in lstWPS025)
                {
                    if (!lstWelders.Contains(wps25.WelderStamp))
                    {
                        lstWelders.Add(wps25.WelderStamp);
                    }

                    if (!_weldingProcess.Contains(wps25.WeldingProcess))
                    {
                        _weldingProcess.Add(wps25.WeldingProcess);
                    }

                }
                #endregion
            }
            else
            {
                #region Get Welders from maintain welder weld length

                foreach (QMS061 objQMS061 in lstQMS061)
                {
                    if (!lstWelders.Contains(objQMS061.welderstamp))
                    {
                        lstWelders.Add(objQMS061.welderstamp);
                    }

                    foreach (var wp in objQMS061.WeldingProcess.Split('+'))
                    {
                        if (!string.IsNullOrWhiteSpace(wp) && !_weldingProcess.Contains(wp))
                        {
                            _weldingProcess.Add(wp);
                        }
                    }

                }
                #endregion
            }

            objResponseMsg.WelderStamps = lstWelders;
            objResponseMsg.WeldingProcess = string.Join("+", _weldingProcess.Distinct());
            objResponseMsg.Key = true;
            objResponseMsg.Value = "Welders Captured Successfully";

            return objResponseMsg;
        }

        // ALSO USED in MOBILE APPLICATION
        [HttpPost]
        public ActionResult GetSeamAdditionalDetails(int headerId, bool isLTFPS = false, string APIRequestFrom = "", string UserName = "")
        {
            SeamAdditionalData objResponse = new SeamAdditionalData();
            QMS040 objQMS040 = new QMS040();
            LTF002 objLTF002 = new LTF002();
            QMS031 objQMS031 = null;
            QMS012 objQMS012 = null;
            if (isLTFPS)
            {
                objLTF002 = db.LTF002.FirstOrDefault(c => c.LineId == headerId);
                objResponse.ManufacturingCode = db.QMS010.FirstOrDefault(c => c.BU == objLTF002.BU && c.Location == objLTF002.Location && c.QualityProject == objLTF002.QualityProject).ManufacturingCode;
                if (!string.IsNullOrWhiteSpace(objLTF002.Stage))
                {
                    objQMS031 = db.QMS031.FirstOrDefault(c => c.BU == objLTF002.BU && c.Location == objLTF002.Location && c.QualityProject == objLTF002.QualityProject
                                                            && c.StageCode == objLTF002.Stage && c.SeamNo == objLTF002.SeamNo);
                }
                objQMS012 = db.QMS012.FirstOrDefault(c => c.QualityProject == objLTF002.QualityProject && c.SeamNo == objLTF002.SeamNo);
            }
            else
            {
                objQMS040 = db.QMS040.FirstOrDefault(c => c.HeaderId == headerId);
                objResponse.ManufacturingCode = db.QMS010.FirstOrDefault(c => c.BU == objQMS040.BU && c.Location == objQMS040.Location && c.QualityProject == objQMS040.QualityProject).ManufacturingCode;
                if (!string.IsNullOrWhiteSpace(objQMS040.StageCode))
                {
                    objQMS031 = db.QMS031.FirstOrDefault(c => c.BU == objQMS040.BU && c.Location == objQMS040.Location && c.QualityProject == objQMS040.QualityProject
                                                           && c.StageCode == objQMS040.StageCode && c.StageSequance == objQMS040.StageSequence && c.SeamNo == objQMS040.SeamNo);
                }
                objQMS012 = db.QMS012.FirstOrDefault(c => c.QualityProject == objQMS040.QualityProject && c.SeamNo == objQMS040.SeamNo);
            }

            if (objQMS031 != null)
            {
                objResponse.AcceptanceStandard = objQMS031.AcceptanceStandard;
                objResponse.ApplicationSpecification = objQMS031.ApplicableSpecification;
                objResponse.InspectionExtent = objQMS031.InspectionExtent;
                objResponse.WERemarks = objQMS031.Remarks;
            }

            if (objQMS012 != null)
            {
                objResponse.SeamNo = objQMS012.SeamNo;
                objResponse.SeamDescription = objQMS012.SeamDescription;

                if (Manager.IsOverlaySeam(objQMS012.QualityProject, objQMS012.SeamNo))
                    objResponse.OverlayArea = objQMS012.SeamLengthArea;
                else
                    objResponse.SeamLength = objQMS012.SeamLengthArea;

                objResponse.JointType = objQMS012.WeldType;
                if (objQMS012.Position != null)
                {
                    foreach (string position in objQMS012.Position.Split(','))
                    {
                        if (string.IsNullOrEmpty(objResponse.Part1Position))
                        {
                            objResponse.Part1Position = position.Trim();
                            continue;
                        }
                        if (string.IsNullOrEmpty(objResponse.Part2Position))
                        {
                            objResponse.Part2Position = position.Trim();
                            continue;
                        }
                        if (string.IsNullOrEmpty(objResponse.Part3Position))
                        {
                            objResponse.Part3Position = position.Trim();
                            continue;
                        }
                        if (string.IsNullOrEmpty(objResponse.Part4Position))
                        {
                            objResponse.Part4Position = position.Trim();
                            continue;
                        }
                        if (string.IsNullOrEmpty(objResponse.Part5Position))
                        {
                            objResponse.Part5Position = position.Trim();
                            continue;
                        }
                        if (string.IsNullOrEmpty(objResponse.Part6Position))
                        {
                            objResponse.Part6Position = position.Trim();
                            continue;
                        }

                    }
                }
                //string position3 = "", position4 = "", position5 = "";
                if (objQMS012.Thickness != null)
                {
                    foreach (string thickness in objQMS012.Thickness.Split(','))
                    {
                        if (string.IsNullOrEmpty(objResponse.Part1Thickness))
                        {
                            objResponse.Part1Thickness = thickness.Trim();
                            continue;
                        }
                        if (string.IsNullOrEmpty(objResponse.Part2Thickness))
                        {
                            objResponse.Part2Thickness = thickness.Trim();
                            continue;
                        }
                        if (string.IsNullOrEmpty(objResponse.Part3Thickness))
                        {
                            objResponse.Part3Thickness = thickness.Trim();
                            continue;
                        }
                        if (string.IsNullOrEmpty(objResponse.Part4Thickness))
                        {
                            objResponse.Part4Thickness = thickness.Trim();
                            continue;
                        }
                        if (string.IsNullOrEmpty(objResponse.Part5Thickness))
                        {
                            objResponse.Part5Thickness = thickness.Trim();
                            continue;
                        }
                        if (string.IsNullOrEmpty(objResponse.Part6Thickness))
                        {
                            objResponse.Part6Thickness = thickness.Trim();
                            continue;
                        }

                        //if (string.IsNullOrEmpty(position3))
                        //{
                        //    position3 = thickness.Trim();
                        //    continue;
                        //}
                        //if (string.IsNullOrEmpty(position4))
                        //{
                        //    position4 = thickness.Trim();
                        //    continue;
                        //}
                        //if (string.IsNullOrEmpty(position5))
                        //{
                        //    position5 = thickness.Trim();
                        //    continue;
                        //}

                    }
                }
                if (objQMS012.Material != null)
                {
                    foreach (string material in objQMS012.Material.Split(','))
                    {
                        if (string.IsNullOrEmpty(objResponse.Part1Materials))
                        {
                            objResponse.Part1Materials = material.Trim();
                            continue;
                        }
                        if (string.IsNullOrEmpty(objResponse.Part2Materials))
                        {
                            objResponse.Part2Materials = material.Trim();
                            continue;
                        }
                        if (string.IsNullOrEmpty(objResponse.Part3Materials))
                        {
                            objResponse.Part3Materials = material.Trim();
                            continue;
                        }
                        if (string.IsNullOrEmpty(objResponse.Part4Materials))
                        {
                            objResponse.Part4Materials = material.Trim();
                            continue;
                        }
                        if (string.IsNullOrEmpty(objResponse.Part5Materials))
                        {
                            objResponse.Part5Materials = material.Trim();
                            continue;
                        }
                        if (string.IsNullOrEmpty(objResponse.Part6Materials))
                        {
                            objResponse.Part6Materials = material.Trim();
                            continue;
                        }


                    }
                }
            }

            return Json(objResponse, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public ActionResult ApproveSeamListInspection(int id, string from = "")
        {
            QMS050 objQMS050 = db.QMS050.FirstOrDefault(x => x.RequestId == id);
            int _headerId = 0;

            if (!string.IsNullOrWhiteSpace(Convert.ToString(objQMS050.ProtocolType)))
            {
                if (objQMS050.ProtocolId.HasValue)
                {
                    ViewBag.ProtocolURL = WebsiteURL + "/PROTOCOL/" + objQMS050.ProtocolType + "/Linkage/" + objQMS050.ProtocolId + (((objQMS050.TestResult == null || objQMS050.TestResult == "Returned by TPI") && objQMS050.OfferedBy != null) ? "?m=1" : "");
                    ViewBag.ProtocolTypeDescription = GetCategoryDescriptionOnly(Manager.GetSubCatagories("Protocol Type", objQMS050.BU, objQMS050.Location, null), objQMS050.ProtocolType);
                }
                else
                {
                    QMS031 objQMS031 = db.QMS031.FirstOrDefault(c => c.Project == objQMS050.Project && c.QualityProject == objQMS050.QualityProject && c.BU == objQMS050.BU && c.Location == objQMS050.Location && c.SeamNo == objQMS050.SeamNo && c.StageCode == objQMS050.StageCode && c.StageSequance == objQMS050.StageSequence);
                    if (objQMS031 != null)
                    {
                        ViewBag.ProtocolURL = "OpenAttachmentPopUp(" + objQMS031.LineId + ",false,'QMS031/" + objQMS031.LineId + "');";
                        ViewBag.ProtocolTypeDescription = "";
                    }
                }
            }

            string stagetype = Manager.FetchStageType(objQMS050.StageCode, objQMS050.BU, objQMS050.Location);
            ViewBag.IsChemStage = stagetype == clsImplementationEnum.SeamStageType.Chemical.GetStringValue();
            ViewBag.IsHardnessStage = stagetype == clsImplementationEnum.SeamStageType.Hardness.GetStringValue();
            ViewBag.IsPMIStage = stagetype == clsImplementationEnum.SeamStageType.PMI.GetStringValue();
            ViewBag.IsFerriteStage = stagetype == clsImplementationEnum.SeamStageType.Ferrite.GetStringValue();

            ViewBag.IsGroundSpot = db.QMS002.Where(x => x.StageCode == objQMS050.StageCode && x.BU == objQMS050.BU && x.Location == objQMS050.Location).Select(x => x.IsGroundSpot).FirstOrDefault();
            ViewBag.IsLTFPS = Manager.IsLTFPSProject(objQMS050.Project, objQMS050.BU, objQMS050.Location);
            ViewBag.isTPIOnline = db.QMS010.Where(x => x.QualityProject == objQMS050.QualityProject).Select(x => x.TPIOnlineApproval).FirstOrDefault();
            objQMS050.Project = Manager.GetProjectAndDescription(objQMS050.Project);
            ViewBag.StageCode = Manager.GetStageCodeDescriptionFromCode(objQMS050.StageCode, objQMS050.BU, objQMS050.Location);
            List<GLB002> lstCategory = Manager.GetSubCatagories("TPI Agency", objQMS050.BU, objQMS050.Location, null);
            List<GLB002> lstCategoryTPI = Manager.GetSubCatagories("TPI Intervention", objQMS050.BU, objQMS050.Location, null);
            objQMS050.TPIAgency1 = GetCategoryDescriptionFromList(lstCategory, objQMS050.TPIAgency1);
            objQMS050.TPIAgency2 = GetCategoryDescriptionFromList(lstCategory, objQMS050.TPIAgency2);
            objQMS050.TPIAgency3 = GetCategoryDescriptionFromList(lstCategory, objQMS050.TPIAgency3);
            objQMS050.TPIAgency4 = GetCategoryDescriptionFromList(lstCategory, objQMS050.TPIAgency4);
            objQMS050.TPIAgency5 = GetCategoryDescriptionFromList(lstCategory, objQMS050.TPIAgency5);
            objQMS050.TPIAgency6 = GetCategoryDescriptionFromList(lstCategory, objQMS050.TPIAgency6);
            objQMS050.TPIAgency1Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS050.TPIAgency1Intervention);
            objQMS050.TPIAgency2Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS050.TPIAgency2Intervention);
            objQMS050.TPIAgency3Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS050.TPIAgency3Intervention);
            objQMS050.TPIAgency4Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS050.TPIAgency4Intervention);
            objQMS050.TPIAgency5Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS050.TPIAgency5Intervention);
            objQMS050.TPIAgency6Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS050.TPIAgency6Intervention);
            //objQMS050.Location = db.COM002.Where(x => x.t_dimx == objQMS050.Location).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
            objQMS050.CreatedBy = Manager.GetPsidandDescription(objQMS050.CreatedBy);
            objQMS050.OfferedBy = Manager.GetPsidandDescription(objQMS050.OfferedBy);
            objQMS050.InspectedBy = Manager.GetPsidandDescription(objQMS050.InspectedBy);
            objQMS050.OfferedtoCustomerBy = Manager.GetPsidandDescription(objQMS050.OfferedtoCustomerBy);
            objQMS050.Location = db.COM002.Where(x => x.t_dimx == objQMS050.Location).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
            objQMS050.BU = db.COM002.Where(x => x.t_dimx == objQMS050.BU).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
            objQMS050.TestResult = string.IsNullOrWhiteSpace(objQMS050.TestResult) ? "Pending" : objQMS050.TestResult;
            ViewBag.remark = GetPreviousRequestRemark(objQMS050.RequestNo, objQMS050.RequestId);
            string strReturned = clsImplementationEnum.SeamListInspectionStatus.Returned.GetStringValue();
            string strRejected = clsImplementationEnum.SeamListInspectionStatus.Rejected.GetStringValue();
            ViewBag.GroundSpot = db.QMS050.Where(x => (x.RequestNo == objQMS050.RequestNo && x.RequestId < objQMS050.RequestId) && (x.TestResult == strReturned || x.TestResult == strRejected)).OrderByDescending(x => x.RequestId).Select(x => x.GroundSpot).FirstOrDefault();

            ViewBag.fromURL = from;

            if (ViewBag.IsLTFPS)
            {
                ViewBag.DocRevNumber = objQMS050.LTF002.DocRevNo;
                ViewBag.DocumentRequired = objQMS050.LTF002.DocumentRequired;
                _headerId = objQMS050.LTFPSHeaderId.Value;
            }
            else
            {
                ViewBag.DocumentRequired = false;
                _headerId = objQMS050.HeaderId.Value;
            }
            #region Get Welder Stamps


            string[] arrWelders = new string[24];
            clsHelper.Welders objWelderDetails = GetWelderStamps(_headerId, ViewBag.IsLTFPS);
            int i = 0;
            foreach (var item in objWelderDetails.WelderStamps)
            {
                arrWelders[i] = item;
                i = i + 1;
                if (i > 23)
                {
                    break;
                }
            }
            ViewBag.Welders = arrWelders;
            ViewBag.WeldingProcess = objWelderDetails.WeldingProcess;
            #endregion

            return View(objQMS050);
        }


        // ONLY USED IN MOBILE APPLICATION
        [HttpPost]
        public ActionResult AttendSeamOfferedViewMoreMobile(int id, string from = "", string APIRequestFrom = "", string UserName = "")
        {
            try
            {
                db.Configuration.ProxyCreationEnabled = false;
                QMS050 objQMS050 = db.QMS050.FirstOrDefault(x => x.RequestId == id);
                db.Entry(objQMS050).State = System.Data.Entity.EntityState.Detached;

                int _headerId = 0;

                //if (!string.IsNullOrWhiteSpace(Convert.ToString(objQMS050.ProtocolType)))
                //{
                //    if (objQMS050.ProtocolId.HasValue)
                //    {
                //        ViewBag.ProtocolURL = WebsiteURL + "/PROTOCOL/" + objQMS050.ProtocolType + "/Linkage/" + objQMS050.ProtocolId + (((objQMS050.TestResult == null || objQMS050.TestResult == "Returned by TPI") && objQMS050.OfferedBy != null) ? "?m=1" : "");
                //        ViewBag.ProtocolTypeDescription = GetCategoryDescriptionOnly(Manager.GetSubCatagories("Protocol Type", objQMS050.BU, objQMS050.Location, null), objQMS050.ProtocolType);
                //    }
                //    else
                //    {
                //        QMS031 objQMS031 = db.QMS031.FirstOrDefault(c => c.Project == objQMS050.Project && c.QualityProject == objQMS050.QualityProject && c.BU == objQMS050.BU && c.Location == objQMS050.Location && c.SeamNo == objQMS050.SeamNo && c.StageCode == objQMS050.StageCode && c.StageSequance == objQMS050.StageSequence);
                //        if (objQMS031 != null)
                //        {
                //            ViewBag.ProtocolURL = "OpenAttachmentPopUp(" + objQMS031.LineId + ",false,'QMS031/" + objQMS031.LineId + "');";
                //            ViewBag.ProtocolTypeDescription = "";
                //        }
                //    }
                //}

                string stagetype = Manager.FetchStageType(objQMS050.StageCode, objQMS050.BU, objQMS050.Location);
                var IsChemStage = stagetype == clsImplementationEnum.SeamStageType.Chemical.GetStringValue();
                var IsHardnessStage = stagetype == clsImplementationEnum.SeamStageType.Hardness.GetStringValue();
                var IsPMIStage = stagetype == clsImplementationEnum.SeamStageType.PMI.GetStringValue();
                var IsFerriteStage = stagetype == clsImplementationEnum.SeamStageType.Ferrite.GetStringValue();

                var IsGroundSpot = db.QMS002.Where(x => x.StageCode == objQMS050.StageCode && x.BU == objQMS050.BU && x.Location == objQMS050.Location).Select(x => x.IsGroundSpot).FirstOrDefault();
                var IsLTFPS = Manager.IsLTFPSProject(objQMS050.Project, objQMS050.BU, objQMS050.Location);
                var isTPIOnline = db.QMS010.Where(x => x.QualityProject == objQMS050.QualityProject).Select(x => x.TPIOnlineApproval).FirstOrDefault();
                objQMS050.Project = Manager.GetProjectAndDescription(objQMS050.Project);
                var StageCode = Manager.GetStageCodeDescriptionFromCode(objQMS050.StageCode, objQMS050.BU, objQMS050.Location);
                List<GLB002> lstCategory = Manager.GetSubCatagories("TPI Agency", objQMS050.BU, objQMS050.Location, null);
                List<GLB002> lstCategoryTPI = Manager.GetSubCatagories("TPI Intervention", objQMS050.BU, objQMS050.Location, null);
                objQMS050.TPIAgency1 = GetCategoryDescriptionFromList(lstCategory, objQMS050.TPIAgency1);
                objQMS050.TPIAgency2 = GetCategoryDescriptionFromList(lstCategory, objQMS050.TPIAgency2);
                objQMS050.TPIAgency3 = GetCategoryDescriptionFromList(lstCategory, objQMS050.TPIAgency3);
                objQMS050.TPIAgency4 = GetCategoryDescriptionFromList(lstCategory, objQMS050.TPIAgency4);
                objQMS050.TPIAgency5 = GetCategoryDescriptionFromList(lstCategory, objQMS050.TPIAgency5);
                objQMS050.TPIAgency6 = GetCategoryDescriptionFromList(lstCategory, objQMS050.TPIAgency6);
                objQMS050.TPIAgency1Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS050.TPIAgency1Intervention);
                objQMS050.TPIAgency2Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS050.TPIAgency2Intervention);
                objQMS050.TPIAgency3Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS050.TPIAgency3Intervention);
                objQMS050.TPIAgency4Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS050.TPIAgency4Intervention);
                objQMS050.TPIAgency5Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS050.TPIAgency5Intervention);
                objQMS050.TPIAgency6Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS050.TPIAgency6Intervention);
                //objQMS050.Location = db.COM002.Where(x => x.t_dimx == objQMS050.Location).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
                objQMS050.CreatedBy = Manager.GetPsidandDescription(objQMS050.CreatedBy);
                objQMS050.OfferedBy = Manager.GetPsidandDescription(objQMS050.OfferedBy);
                objQMS050.InspectedBy = Manager.GetPsidandDescription(objQMS050.InspectedBy);
                objQMS050.OfferedtoCustomerBy = Manager.GetPsidandDescription(objQMS050.OfferedtoCustomerBy);
                objQMS050.Location = db.COM002.Where(x => x.t_dimx == objQMS050.Location).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
                objQMS050.BU = db.COM002.Where(x => x.t_dimx == objQMS050.BU).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
                objQMS050.TestResult = string.IsNullOrWhiteSpace(objQMS050.TestResult) ? "Pending" : objQMS050.TestResult;
                var remark = GetPreviousRequestRemark(objQMS050.RequestNo, objQMS050.RequestId);
                string strReturned = clsImplementationEnum.SeamListInspectionStatus.Returned.GetStringValue();
                string strRejected = clsImplementationEnum.SeamListInspectionStatus.Rejected.GetStringValue();
                var GroundSpot = db.QMS050.Where(x => (x.RequestNo == objQMS050.RequestNo && x.RequestId < objQMS050.RequestId) && (x.TestResult == strReturned || x.TestResult == strRejected)).OrderByDescending(x => x.RequestId).Select(x => x.GroundSpot).FirstOrDefault();

                var DocRevNumber = "";
                var DocumentRequired = false;
                if (IsLTFPS)
                {
                    var objLTF002 = db.LTF002.Where(x => x.LineId == objQMS050.LTFPSHeaderId).Select(x => new { x.DocRevNo, x.DocumentRequired }).FirstOrDefault();
                    if (objLTF002 != null)
                    {
                        DocRevNumber = objLTF002.DocRevNo;
                        DocumentRequired = objLTF002.DocumentRequired;
                    }
                    _headerId = objQMS050.LTFPSHeaderId.Value;
                }
                else
                {
                    DocumentRequired = false;
                    _headerId = objQMS050.HeaderId.Value;
                }
                #region Get Welder Stamps


                string[] arrWelders = new string[24];
                clsHelper.Welders objWelderDetails = GetWelderStamps(_headerId, IsLTFPS);
                int i = 0;
                foreach (var item in objWelderDetails.WelderStamps)
                {
                    arrWelders[i] = !string.IsNullOrEmpty(item) ? item : "";
                    i = i + 1;
                    if (i > 23)
                    {
                        break;
                    }
                }

                var WeldingProcess = objWelderDetails.WeldingProcess;
                #endregion

                var TempAdditionalDetails = new Object();
                try
                {
                    var AdditionalDetails = GetSeamAdditionalDetails(Convert.ToInt32(objQMS050.HeaderId), IsLTFPS, APIRequestFrom, UserName);
                    TempAdditionalDetails = AdditionalDetails != null && ((System.Web.Mvc.JsonResult)AdditionalDetails).Data != null ? ((System.Web.Mvc.JsonResult)AdditionalDetails).Data : null;
                }
                catch (Exception ex)
                {
                    TempAdditionalDetails = null;
                }

                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    QMS050 = objQMS050,
                    lstWelders = arrWelders,
                    WeldingProcess = WeldingProcess,
                    AdditionalDetails = TempAdditionalDetails,
                    IsLTFPS,
                    IsChemStage,
                    IsHardnessStage,
                    IsFerriteStage,
                    IsPMIStage,
                    StageCode,
                    IsGroundSpot,
                    GroundSpot,
                    isTPIOnline,
                    DocRevNumber,
                    DocumentRequired,
                    remark,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }

        }
        public string GetCategoryDescriptionFromList(List<GLB002> lst, string code)
        {
            if (!string.IsNullOrWhiteSpace(code))
                return lst.Where(x => x.Code == code).Select(s => s.Code + " - " + s.Description).FirstOrDefault();
            else
                return null;
        }

        public string GetCategoryDescriptionOnly(List<GLB002> lst, string code)
        {
            if (!string.IsNullOrWhiteSpace(code))
                return lst.Where(x => x.Code == code).Select(s => s.Description).FirstOrDefault();
            else
                return null;
        }
        [SessionExpireFilter]
        public ActionResult ReOfferView()
        {
            return View();
        }
        public ActionResult GetReOfferDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetReOfferDataPartial");
        }
        // ALSO USED IN Mobile Application
        [HttpPost]
        public ActionResult ReOfferToQC(int requestId, string APIRequestFrom = "", string UserName = "")//, bool hasAttachments, Dictionary<string, string> Attach)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS050 objQMS050 = db.QMS050.FirstOrDefault(x => x.RequestId == requestId);
                if (Helper.IsChemstage(objQMS050.StageCode, objQMS050.BU, objQMS050.Location) ? db.QMS051.Any(x => x.HeaderId == objQMS050.HeaderId && x.RefRequestId == objQMS050.RequestId) : true)
                {
                    bool isLTFPSProject = Manager.IsLTFPSProject(objQMS050.Project, objQMS050.BU, objQMS050.Location);

                    //var folderPath = "";
                    //if (isLTFPSProject)
                    //{
                    //    folderPath = "QMS050_LTFPS/" + objQMS050.LTFPSHeaderId + "/" + objQMS050.IterationNo + "/" + objQMS050.RequestNoSequence;
                    //}
                    //else
                    //{
                    //    folderPath = "QMS050/" + objQMS050.HeaderId + "/" + objQMS050.IterationNo + "/" + objQMS050.RequestNoSequence;
                    //}
                    //try
                    //{
                    //    Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                    //}
                    //catch
                    //{
                    //}
                    bool IsProtocolApplicable = db.QMS002.Where(x => x.BU == objQMS050.BU && x.Location == objQMS050.Location && x.StageCode == objQMS050.StageCode).Select(s => s.IsProtocolApplicable).FirstOrDefault();

                    //if (IsProtocolApplicable)
                    //{
                    //    try
                    //    {
                    //        var existing = (new clsFileUpload()).GetDocuments(folderPath);
                    //        if (!existing.Any())
                    //        {
                    //            objResponseMsg.Key = false;
                    //            objResponseMsg.Value = "Attachment(s) is Required";
                    //            return Json(objResponseMsg);
                    //        }
                    //    }
                    //    catch (Exception)
                    //    {
                    //        objResponseMsg.Key = false;
                    //        objResponseMsg.Value = "File server connection not established, Please try again or contact administrator";
                    //        return Json(objResponseMsg);
                    //    }
                    //}

                    objQMS050.OfferedBy = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;

                    objQMS050.OfferedOn = DateTime.Now;
                    //db.SaveChanges();


                    if (isLTFPSProject)
                    {
                        objQMS050.LTF002.InspectionStatus = clsImplementationEnum.SeamListInspectionStatus.UnderInspection.GetStringValue();
                    }
                    else
                    {
                        objQMS050.QMS040.InspectionStatus = clsImplementationEnum.SeamListInspectionStatus.UnderInspection.GetStringValue();
                    }

                    //QMS040 objQMS040 = db.QMS040.FirstOrDefault(x => x.HeaderId == objQMS050.HeaderId);
                    //objQMS040.InspectionStatus = clsImplementationEnum.SeamListInspectionStatus.UnderInspection.GetStringValue();
                    db.SaveChanges();

                    if (objQMS050.ProtocolId.HasValue)
                    {
                        string PRLTableName = Manager.GetLinkedProtocolTableName(objQMS050.ProtocolType);
                        db.Database.ExecuteSqlCommand("UPDATE " + PRLTableName + " SET OfferDate=GETDATE() WHERE HeaderId = " + objQMS050.ProtocolId);
                    }

                    #region Send Notification
                   (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), objQMS050.Project, objQMS050.BU, objQMS050.Location, "Stage: " + objQMS050.StageCode + " of Seam: " + objQMS050.SeamNo + " & Project No: " + objQMS050.QualityProject + " has been re offered for Inspection", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/MaintainSeamListOfferInspection/ApproveSeamListInspection/" + objQMS050.RequestId, null, APIRequestFrom, UserName);
                    #endregion

                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please Maintain Chemical Test Details";
                }
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error Occured, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]
        public ActionResult SendReOfferView(int id, string from = "")
        {
            QMS050 objQMS050 = db.QMS050.FirstOrDefault(x => x.RequestId == id);
            ViewBag.IsLTFPS = Manager.IsLTFPSProject(objQMS050.Project, objQMS050.BU, objQMS050.Location);
            bool IsSeamNP = Manager.IsSeamNP(objQMS050.QualityProject, objQMS050.Project, objQMS050.Location, objQMS050.SeamNo);

            var IsProtocolApplicable = db.QMS002.Where(x => x.Location == objQMS050.Location && x.BU == objQMS050.BU && x.StageCode == objQMS050.StageCode).Select(x => x.IsProtocolApplicable).FirstOrDefault();
            if (IsProtocolApplicable && !IsSeamNP)
                ViewBag.ProtocolApplicable = "Yes";
            else
                ViewBag.ProtocolApplicable = "No";

            if (!string.IsNullOrWhiteSpace(Convert.ToString(objQMS050.ProtocolType)))
            {
                if (objQMS050.ProtocolId.HasValue)
                {
                    ViewBag.ProtocolURL = WebsiteURL + "/PROTOCOL/" + objQMS050.ProtocolType + "/Linkage/" + objQMS050.ProtocolId + "?m=1";
                    ViewBag.ProtocolTypeDescription = GetCategoryDescriptionOnly(Manager.GetSubCatagories("Protocol Type", objQMS050.BU, objQMS050.Location, null), objQMS050.ProtocolType);
                }
                else
                {
                    QMS031 objQMS031 = db.QMS031.FirstOrDefault(c => c.Project == objQMS050.Project && c.QualityProject == objQMS050.QualityProject && c.BU == objQMS050.BU && c.Location == objQMS050.Location && c.SeamNo == objQMS050.SeamNo && c.StageCode == objQMS050.StageCode && c.StageSequance == objQMS050.StageSequence);
                    if (objQMS031 != null)
                    {
                        ViewBag.ProtocolURL = "OpenAttachmentPopUp(" + objQMS031.LineId + ",false,'QMS031/" + objQMS031.LineId + "');";
                        ViewBag.ProtocolTypeDescription = "";
                    }
                }
            }
            if (Helper.IsChemstage(objQMS050.StageCode, objQMS050.BU, objQMS050.Location))
            {

                ViewBag.IsChemStage = true;
            }
            else
            {
                ViewBag.IsChemStage = false;
            }
            string strReturned = clsImplementationEnum.SeamListInspectionStatus.Returned.GetStringValue();
            string strRejected = clsImplementationEnum.SeamListInspectionStatus.Rejected.GetStringValue();
            bool? IsGroundSpot = db.QMS002.Where(x => x.StageCode == objQMS050.StageCode && x.BU == objQMS050.BU && x.Location == objQMS050.Location).Select(x => x.IsGroundSpot).FirstOrDefault();
            ViewBag.IsGroundSpot = IsGroundSpot;
            ViewBag.GroundSpot = IsGroundSpot == true ? db.QMS050.Where(x => (x.RequestNo == objQMS050.RequestNo && x.RequestId < objQMS050.RequestId) && (x.TestResult == strReturned || x.TestResult == strRejected)).OrderByDescending(x => x.RequestId).Select(x => x.GroundSpot).FirstOrDefault() : 0;

            objQMS050.Project = Manager.GetProjectAndDescription(objQMS050.Project);
            ViewBag.StageCode = Manager.GetStageCodeDescriptionFromCode(objQMS050.StageCode, objQMS050.BU, objQMS050.Location);
            List<GLB002> lstCategory = Manager.GetSubCatagories("TPI Agency", objQMS050.BU, objQMS050.Location, null);
            List<GLB002> lstCategoryTPI = Manager.GetSubCatagories("TPI Intervention", objQMS050.BU, objQMS050.Location, null);
            objQMS050.TPIAgency1 = GetCategoryDescriptionFromList(lstCategory, objQMS050.TPIAgency1);
            objQMS050.TPIAgency2 = GetCategoryDescriptionFromList(lstCategory, objQMS050.TPIAgency2);
            objQMS050.TPIAgency3 = GetCategoryDescriptionFromList(lstCategory, objQMS050.TPIAgency3);
            objQMS050.TPIAgency4 = GetCategoryDescriptionFromList(lstCategory, objQMS050.TPIAgency4);
            objQMS050.TPIAgency5 = GetCategoryDescriptionFromList(lstCategory, objQMS050.TPIAgency5);
            objQMS050.TPIAgency6 = GetCategoryDescriptionFromList(lstCategory, objQMS050.TPIAgency6);
            objQMS050.TPIAgency1Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS050.TPIAgency1Intervention);
            objQMS050.TPIAgency2Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS050.TPIAgency2Intervention);
            objQMS050.TPIAgency3Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS050.TPIAgency3Intervention);
            objQMS050.TPIAgency4Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS050.TPIAgency4Intervention);
            objQMS050.TPIAgency5Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS050.TPIAgency5Intervention);
            objQMS050.TPIAgency6Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS050.TPIAgency6Intervention);
            objQMS050.CreatedBy = Manager.GetPsidandDescription(objQMS050.CreatedBy);
            objQMS050.OfferedBy = Manager.GetPsidandDescription(objQMS050.OfferedBy);
            objQMS050.InspectedBy = Manager.GetPsidandDescription(objQMS050.InspectedBy);
            objQMS050.OfferedtoCustomerBy = Manager.GetPsidandDescription(objQMS050.OfferedtoCustomerBy);
            objQMS050.Location = db.COM002.Where(x => x.t_dimx == objQMS050.Location).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
            objQMS050.BU = db.COM002.Where(x => x.t_dimx == objQMS050.BU).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
            objQMS050.TestResult = string.IsNullOrWhiteSpace(objQMS050.TestResult) ? "Pending" : objQMS050.TestResult;
            ViewBag.remark = GetPreviousRequestRemark(objQMS050.RequestNo, objQMS050.RequestId);
            ViewBag.fromURL = from;



            return View(objQMS050);
        }

        // ONLY USED IN Mobile Application
        [HttpPost]
        public ActionResult SendReOfferViewMobile(int id, string APIRequestFrom = "", string UserName = "")
        {
            try
            {
                var lstWelders = new List<string>();

                bool ProtocolApplicable = false;
                bool IsChemStage = false;
                string ProtocolTypeDescription = "";
                db.Configuration.ProxyCreationEnabled = false;
                QMS050 objQMS050 = db.QMS050.Where(x => x.RequestId == id).Select(y => y).FirstOrDefault();
                db.Entry(objQMS050).State = System.Data.Entity.EntityState.Detached;

                bool IsLTFPS = Manager.IsLTFPSProject(objQMS050.Project, objQMS050.BU, objQMS050.Location);
                bool IsSeamNP = Manager.IsSeamNP(objQMS050.QualityProject, objQMS050.Project, objQMS050.Location, objQMS050.SeamNo);

                var IsProtocolApplicable = db.QMS002.Where(x => x.Location == objQMS050.Location && x.BU == objQMS050.BU && x.StageCode == objQMS050.StageCode).Select(x => x.IsProtocolApplicable).FirstOrDefault();
                if (IsProtocolApplicable && !IsSeamNP)
                    ProtocolApplicable = false;
                else
                    ProtocolApplicable = true;

                if (!string.IsNullOrWhiteSpace(Convert.ToString(objQMS050.ProtocolType)))
                {
                    if (objQMS050.ProtocolId.HasValue)
                    {
                        ProtocolTypeDescription = GetCategoryDescriptionOnly(Manager.GetSubCatagories("Protocol Type", objQMS050.BU, objQMS050.Location, null), objQMS050.ProtocolType);
                    }
                    else
                    {
                        QMS031 objQMS031 = db.QMS031.FirstOrDefault(c => c.Project == objQMS050.Project && c.QualityProject == objQMS050.QualityProject && c.BU == objQMS050.BU && c.Location == objQMS050.Location && c.SeamNo == objQMS050.SeamNo && c.StageCode == objQMS050.StageCode && c.StageSequance == objQMS050.StageSequence);
                        if (objQMS031 != null)
                        {
                            ProtocolTypeDescription = "";
                        }
                    }
                }
                if (Helper.IsChemstage(objQMS050.StageCode, objQMS050.BU, objQMS050.Location))
                {

                    IsChemStage = true;
                }
                else
                {
                    IsChemStage = false;
                }
                string strReturned = clsImplementationEnum.SeamListInspectionStatus.Returned.GetStringValue();
                string strRejected = clsImplementationEnum.SeamListInspectionStatus.Rejected.GetStringValue();
                bool? IsGroundSpot = db.QMS002.Where(x => x.StageCode == objQMS050.StageCode && x.BU == objQMS050.BU && x.Location == objQMS050.Location).Select(x => x.IsGroundSpot).FirstOrDefault();

                double? GroundSpot = IsGroundSpot == true ? db.QMS050.Where(x => (x.RequestNo == objQMS050.RequestNo && x.RequestId < objQMS050.RequestId) && (x.TestResult == strReturned || x.TestResult == strRejected)).OrderByDescending(x => x.RequestId).Select(x => x.GroundSpot).FirstOrDefault() : 0;

                objQMS050.Project = Manager.GetProjectAndDescription(objQMS050.Project);
                string StageCode = Manager.GetStageCodeDescriptionFromCode(objQMS050.StageCode, objQMS050.BU, objQMS050.Location);
                List<GLB002> lstCategory = Manager.GetSubCatagories("TPI Agency", objQMS050.BU, objQMS050.Location, null);
                List<GLB002> lstCategoryTPI = Manager.GetSubCatagories("TPI Intervention", objQMS050.BU, objQMS050.Location, null);
                objQMS050.TPIAgency1 = GetCategoryDescriptionFromList(lstCategory, objQMS050.TPIAgency1);
                objQMS050.TPIAgency2 = GetCategoryDescriptionFromList(lstCategory, objQMS050.TPIAgency2);
                objQMS050.TPIAgency3 = GetCategoryDescriptionFromList(lstCategory, objQMS050.TPIAgency3);
                objQMS050.TPIAgency4 = GetCategoryDescriptionFromList(lstCategory, objQMS050.TPIAgency4);
                objQMS050.TPIAgency5 = GetCategoryDescriptionFromList(lstCategory, objQMS050.TPIAgency5);
                objQMS050.TPIAgency6 = GetCategoryDescriptionFromList(lstCategory, objQMS050.TPIAgency6);
                objQMS050.TPIAgency1Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS050.TPIAgency1Intervention);
                objQMS050.TPIAgency2Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS050.TPIAgency2Intervention);
                objQMS050.TPIAgency3Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS050.TPIAgency3Intervention);
                objQMS050.TPIAgency4Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS050.TPIAgency4Intervention);
                objQMS050.TPIAgency5Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS050.TPIAgency5Intervention);
                objQMS050.TPIAgency6Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS050.TPIAgency6Intervention);
                objQMS050.CreatedBy = Manager.GetPsidandDescription(objQMS050.CreatedBy);
                objQMS050.OfferedBy = Manager.GetPsidandDescription(objQMS050.OfferedBy);
                objQMS050.InspectedBy = Manager.GetPsidandDescription(objQMS050.InspectedBy);
                objQMS050.OfferedtoCustomerBy = Manager.GetPsidandDescription(objQMS050.OfferedtoCustomerBy);
                objQMS050.Location = db.COM002.Where(x => x.t_dimx == objQMS050.Location).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
                objQMS050.BU = db.COM002.Where(x => x.t_dimx == objQMS050.BU).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
                objQMS050.TestResult = string.IsNullOrWhiteSpace(objQMS050.TestResult) ? "Pending" : objQMS050.TestResult;
                string remark = GetPreviousRequestRemark(objQMS050.RequestNo, objQMS050.RequestId);

                lstWelders.Add(!string.IsNullOrEmpty(objQMS050.Welder1) ? objQMS050.Welder1 : "");
                lstWelders.Add(!string.IsNullOrEmpty(objQMS050.Welder2) ? objQMS050.Welder2 : "");
                lstWelders.Add(!string.IsNullOrEmpty(objQMS050.Welder3) ? objQMS050.Welder3 : "");
                lstWelders.Add(!string.IsNullOrEmpty(objQMS050.Welder4) ? objQMS050.Welder4 : "");
                lstWelders.Add(!string.IsNullOrEmpty(objQMS050.Welder5) ? objQMS050.Welder5 : "");
                lstWelders.Add(!string.IsNullOrEmpty(objQMS050.Welder6) ? objQMS050.Welder6 : "");
                lstWelders.Add(!string.IsNullOrEmpty(objQMS050.Welder7) ? objQMS050.Welder7 : "");
                lstWelders.Add(!string.IsNullOrEmpty(objQMS050.Welder8) ? objQMS050.Welder8 : "");
                lstWelders.Add(!string.IsNullOrEmpty(objQMS050.Welder9) ? objQMS050.Welder9 : "");
                lstWelders.Add(!string.IsNullOrEmpty(objQMS050.Welder10) ? objQMS050.Welder10 : "");
                lstWelders.Add(!string.IsNullOrEmpty(objQMS050.Welder11) ? objQMS050.Welder11 : "");
                lstWelders.Add(!string.IsNullOrEmpty(objQMS050.Welder12) ? objQMS050.Welder12 : "");
                lstWelders.Add(!string.IsNullOrEmpty(objQMS050.Welder13) ? objQMS050.Welder13 : "");
                lstWelders.Add(!string.IsNullOrEmpty(objQMS050.Welder14) ? objQMS050.Welder14 : "");
                lstWelders.Add(!string.IsNullOrEmpty(objQMS050.Welder15) ? objQMS050.Welder15 : "");
                lstWelders.Add(!string.IsNullOrEmpty(objQMS050.Welder16) ? objQMS050.Welder16 : "");
                lstWelders.Add(!string.IsNullOrEmpty(objQMS050.Welder17) ? objQMS050.Welder17 : "");
                lstWelders.Add(!string.IsNullOrEmpty(objQMS050.Welder18) ? objQMS050.Welder18 : "");
                lstWelders.Add(!string.IsNullOrEmpty(objQMS050.Welder19) ? objQMS050.Welder19 : "");
                lstWelders.Add(!string.IsNullOrEmpty(objQMS050.Welder20) ? objQMS050.Welder20 : "");
                lstWelders.Add(!string.IsNullOrEmpty(objQMS050.Welder21) ? objQMS050.Welder21 : "");
                lstWelders.Add(!string.IsNullOrEmpty(objQMS050.Welder22) ? objQMS050.Welder22 : "");
                lstWelders.Add(!string.IsNullOrEmpty(objQMS050.Welder23) ? objQMS050.Welder23 : "");
                lstWelders.Add(!string.IsNullOrEmpty(objQMS050.Welder24) ? objQMS050.Welder24 : "");

                var TempAdditionalDetails = new Object();
                try
                {
                    var AdditionalDetails = GetSeamAdditionalDetails(Convert.ToInt32(objQMS050.HeaderId), IsLTFPS, APIRequestFrom, UserName);
                    TempAdditionalDetails = AdditionalDetails != null && ((System.Web.Mvc.JsonResult)AdditionalDetails).Data != null ? ((System.Web.Mvc.JsonResult)AdditionalDetails).Data : null;
                }
                catch (Exception ex)
                {
                    TempAdditionalDetails = null;
                }

                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    QMS050 = objQMS050,
                    lstWelders = lstWelders,
                    AdditionalDetails = TempAdditionalDetails,
                    IsLTFPS,
                    IsSeamNP,
                    ProtocolApplicable,
                    ProtocolTypeDescription,
                    IsChemStage,
                    IsGroundSpot,
                    GroundSpot,
                    StageCode,
                    remark,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult loadSeamListReOfferDataTable(JQueryDataTableParamModel param, string status, string qualityProject, string seamNumber)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and TestResult IS NULL and OfferedBy IS NULL";
                }
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms.BU", "qms.Location");
                string[] columnName = { "qms.SeamNo", "qms.QualityProject", "dbo.GET_PROJECTDESC_BY_CODE(qms.Project)", "qms.OfferedBy", "qms.CreatedBy", "qms.Location", "dbo.GET_IPI_STAGECODE_DESCRIPTION(qms.StageCode, qms.BU, qms.Location) ", "qms.RequestNo", "qms.InspectedBy", "qms.QCRemarks", "qms.OfferedtoCustomerBy", "qms.Shop", "qms.ShopLocation", "qms.ShopRemark" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                if (!string.IsNullOrEmpty(qualityProject) && !string.IsNullOrEmpty(seamNumber))
                {
                    whereCondition += (seamNumber.Equals("ALL") ? " AND qms.QualityProject = '" + qualityProject + "' " : " AND qms.QualityProject = '" + qualityProject + "' AND qms.SeamNo ='" + seamNumber + "'");
                }

                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = string.Empty; ;
                sortDirection = Convert.ToString(Request["sSortDir_0"]); // asc or desc
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstHeader = db.SP_IPI_SEAMLISTOFFERINSPECTION_DETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                var res = from h in lstHeader
                          select new[] {
                           Convert.ToString( h.SeamNo),
                           Convert.ToString(h.StageCode),
                           Convert.ToString(h.StageSequence),
                           Convert.ToString(h.IterationNo),
                           Convert.ToString(h.TestResult),
                           //Convert.ToString(h.OfferedBy),
                           //h.OfferedOn != null ? Convert.ToDateTime(h.OfferedOn).ToString("dd/MM/yyyy") : "",
                           //Convert.ToString( h.CreatedBy),
                           //Convert.ToDateTime(h.CreatedOn).ToString("dd/MM/yyyy"),
                           //Convert.ToString( h.Location),
                           Convert.ToString(h.Project),
                           Convert.ToString(h.BU),
                           Convert.ToString(h.StageCode),
                           Convert.ToString(h.StageSequence),
                           Convert.ToString(h.IterationNo),
                           Convert.ToString(h.RequestNo),
                           Convert.ToString(h.RequestNoSequence),
                           Convert.ToString(h.InspectedBy),
                           Convert.ToDateTime(h.InspectedOn).ToString("dd/MM/yyyy"),
                           Convert.ToString(h.QCRemarks),
                           Convert.ToString(h.OfferedtoCustomerBy),
                           Convert.ToDateTime(h.OfferedtoCustomerOn).ToString("dd/MM/yyyy"),
                           Convert.ToString(h.Shop),
                           Convert.ToString(h.ShopLocation),
                           Convert.ToString(h.ShopRemark),
                           h.QualityProject,
                           Convert.ToString(h.RequestId),
                           Convert.ToString(h.HeaderId),
                           "<center style=\"white-space: nowrap;\" class=\"clsDisplayInlineEle\"><a   href=\"" +WebsiteURL + "/IPI/MaintainSeamListOfferInspection/SendReOfferView/"+h.RequestId+"?from=reoffer\" Title=\"View Details\" ><i style=\"margin - left:5px;\" class=\"fa fa-eye\"></i></a>&nbsp;"+Manager.generateIPIGridButtons(h.QualityProject,h.Project,h.BU,h.Location,h.SeamNo,"","",h.StageCode,Convert.ToString(h.StageSequence),"Seam Details",50,"true","Seam Details","Seam Request Details")
                           + (h.ProtocolId.HasValue ? "&nbsp;&nbsp;<a target=\"_blank\"  href=\"" + WebsiteURL + "/PROTOCOL/"+h.ProtocolType+"/Linkage/"+h.ProtocolId + "?m=1" +"\" Title=\"View Protocol Details\" ><i style=\"margin - left:5px;\" class=\"fa fa-file-text\"></i></a>" : "&nbsp;&nbsp;<a Title=\"No Protocol Attached\" ><i style=\"opacity:0.3;\" class=\"fa fa-file-text\"></i></a>")
                           + "&nbsp;&nbsp;<a  href=\"javascript: void(0);\" Title=\"View Timeline\" onclick=\"ShowTimeline("+ h.RequestId +")\"><i style=\"margin - left:5px;\" class=\"fa fa-clock-o\"></i></a></center>"

                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        // ONLY USED IN Mobile Application
        [HttpPost]
        public ActionResult loadSeamListReOfferDataTableMobile(JQueryDataTableParamModel param, string status, string qualityProject, string seamNumber, string UserName)
        {
            try
            {
                int? StartIndex = (param.iDisplayStart * param.iDisplayLength) + 1;
                int? EndIndex = (param.iDisplayStart * param.iDisplayLength) + param.iDisplayLength;

                string whereCondition = "1=1";
                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and TestResult IS NULL and OfferedBy IS NULL";
                }
                whereCondition += Manager.MakeDefaultWhere(UserName, "qms.BU", "qms.Location");
                string[] columnName = { "qms.SeamNo", "qms.QualityProject", "dbo.GET_PROJECTDESC_BY_CODE(qms.Project)", "qms.OfferedBy", "qms.CreatedBy", "qms.Location", "dbo.GET_IPI_STAGECODE_DESCRIPTION(qms.StageCode, qms.BU, qms.Location) ", "qms.RequestNo", "qms.InspectedBy", "qms.QCRemarks", "qms.OfferedtoCustomerBy", "qms.Shop", "qms.ShopLocation", "qms.ShopRemark" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                if (!string.IsNullOrEmpty(qualityProject) && !string.IsNullOrEmpty(seamNumber))
                {
                    whereCondition += (seamNumber.Equals("ALL") ? " AND qms.QualityProject = '" + qualityProject + "' " : " AND qms.QualityProject = '" + qualityProject + "' AND qms.SeamNo ='" + seamNumber + "'");
                }

                string strSortOrder = " Order By StageSequence asc ";

                var lstHeader = db.SP_IPI_SEAMLISTOFFERINSPECTION_DETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    TotalCount = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    data_list = lstHeader
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                    TotalCount = 0,
                    data_list = new List<string>()
                }, JsonRequestBehavior.AllowGet);
            }
        }

        // ALSO USED in Mobile Application
        [HttpPost]
        public ActionResult RequestSeamListOffered(FormCollection fc, SeamListSendOffer objSeamListSendOffer, string IsSTAGEDEPARTMENTNDE, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var iter = db.QMS040.Where(x => x.HeaderId == objSeamListSendOffer.HeaderId).FirstOrDefault();

                objResponseMsg = Manager.IsApplicableForOffer(iter.HeaderId, clsImplementationEnum.InspectionFor.SEAM.GetStringValue(), false);

                if (!objResponseMsg.Key)
                {
                    return Json(objResponseMsg);
                }

                //if (IsSTAGEDEPARTMENTNDE.ToLower() == "true")
                //{
                //    folderPath = "QMS060/" + objSeamListSendOffer.HeaderId + "/" + iter.IterationNo + "/" + 1;
                //}
                //else
                //    folderPath = "QMS050/" + objSeamListSendOffer.HeaderId + "/" + iter.IterationNo + "/" + 1;
                //try
                //{
                //    Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                //}
                //catch
                //{
                //}
                bool IsProtocolApplicable = db.QMS002.Where(x => x.BU == iter.BU && x.Location == iter.Location && x.StageCode == iter.StageCode).Select(s => s.IsProtocolApplicable).FirstOrDefault();

                //if (IsProtocolApplicable)
                //{
                //    try
                //    {
                //        var existing = (new clsFileUpload()).GetDocuments(folderPath);
                //        if (!existing.Any())
                //        {
                //            objResponseMsg.Key = false;
                //            objResponseMsg.Value = "Attachment(s) is Required";
                //            return Json(objResponseMsg);
                //        }
                //    }
                //    catch (Exception)
                //    {
                //        objResponseMsg.Key = false;
                //        objResponseMsg.Value = "File server connection not established, Please try again or contact administrator";
                //        return Json(objResponseMsg);
                //    }
                //}
                string tempUserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                string response = db.SP_IPI_GENREATESEAMTESTREQUEST(objSeamListSendOffer.HeaderId, tempUserName, objSeamListSendOffer.Shop, objSeamListSendOffer.Location, objSeamListSendOffer.ShopRemark, objSeamListSendOffer.SurfaceCondition, objSeamListSendOffer.WeldThicknessReinforcement, objSeamListSendOffer.OverlayThickness, objSeamListSendOffer.OverlayMaterials).FirstOrDefault();

                #region Send Notification
                if (IsSTAGEDEPARTMENTNDE.ToLower() == "true")
                {
                    QMS060 objQMS060 = iter.QMS060.OrderByDescending(o => o.CreatedOn).FirstOrDefault();
                    if (objQMS060 != null)
                    {
                        if (Manager.IsNDERequestAutoGenerate(objQMS060.Project, objQMS060.BU, objQMS060.Location))
                        {
                            (new SeamListNDEInspectionController()).RespondeGenerateRequest(objQMS060.RequestId, Manager.GetSystemUserPSNo(), APIRequestFrom, UserName);
                        }
                        else
                        {
                            (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), iter.Project, iter.BU, iter.Location, "Stage: " + iter.StageCode + " of Seam: " + iter.SeamNo + " & Project No: " + iter.QualityProject + " has been offered for Inspection", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/SeamListNDEInspection/QCSeamTestDetails/" + objQMS060.RequestId, null, APIRequestFrom, UserName);
                        }
                    }
                }
                else
                {
                    QMS050 objQMS050 = iter.QMS050.OrderByDescending(o => o.CreatedOn).FirstOrDefault();
                    QMS050 reqDetail = db.QMS050.Where(x => x.HeaderId == iter.HeaderId).OrderByDescending(x => x.HeaderId).FirstOrDefault();
                    if (reqDetail != null)
                    {
                        string StageType = Manager.FetchStageType(iter.StageCode, iter.BU, iter.Location);
                        if (StageType == clsImplementationEnum.SeamStageType.Chemical.GetStringValue())
                        {
                            var chemDetail = db.QMS051.Where(x => x.HeaderId == iter.HeaderId).ToList();
                            if (chemDetail.Count > 0)
                            {
                                chemDetail.ForEach(x =>
                                {
                                    x.RefRequestId = reqDetail.RequestId;
                                    x.RequestNo = reqDetail.RequestNo;
                                    x.RequestNoSequence = reqDetail.RequestNoSequence;
                                });
                                db.SaveChanges();
                            }
                        }
                        if (StageType == clsImplementationEnum.SeamStageType.Hardness.GetStringValue())
                        {
                            db.QMS052.Add(new QMS052
                            {
                                HeaderId = iter.HeaderId,
                                RefRequestId = reqDetail.RequestId,
                                CreatedBy = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName,
                                CreatedOn = DateTime.Now
                            });
                            db.SaveChanges();
                        }
                        if (StageType == clsImplementationEnum.SeamStageType.Ferrite.GetStringValue())
                        {
                            db.QMS053_1.Add(new QMS053_1
                            {
                                HeaderId = iter.HeaderId,
                                RefRequestId = reqDetail.RequestId,
                                CreatedBy = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName,
                                CreatedOn = DateTime.Now
                            });
                            db.SaveChanges();
                        }
                        if (StageType == clsImplementationEnum.SeamStageType.PMI.GetStringValue())
                        {
                            string[] arrElement = new string[6];

                            var objTestPlan = db.QMS021.Where(x => x.QualityProject == iter.QualityProject && x.SeamNo == iter.SeamNo && x.Location == iter.Location && x.StageCode == iter.StageCode && x.StageSequance == iter.StageSequence).FirstOrDefault();
                            if (objTestPlan != null)
                            {
                                var lstQMS023 = objTestPlan != null ? db.QMS023.Where(i => i.TPLineId == objTestPlan.LineId).OrderByDescending(x => x.TPRevNo).ToList() : null;
                                if (lstQMS023.Count() > 0 && lstQMS023 != null)
                                {
                                    int i = 0;
                                    #region Fetch element data from Test Plan(QMS022/QMS023)
                                    foreach (var item in lstQMS023)
                                    {
                                        arrElement[i] = item.Element;
                                        i = i + 1;
                                        if (i == 5)
                                            break;
                                    }
                                    #endregion
                                }
                                else
                                {
                                    #region Fecth from quality id (QMS018/QMS019)
                                    List<string> lstQproj = new List<string>();
                                    lstQproj.Add(iter.QualityProject);
                                    lstQproj.AddRange(db.QMS017.Where(c => c.BU == iter.BU && c.Location == iter.Location && c.IQualityProject == iter.QualityProject).Select(x => x.QualityProject).ToList());

                                    var QualityId = db.QMS020.FirstOrDefault(x => x.QualityProject == iter.QualityProject && x.SeamNo == iter.SeamNo && x.Location == iter.Location).QualityId;
                                    var Qrev = QualityId != null ? db.QMS015.FirstOrDefault(x => x.QualityId == QualityId && lstQproj.Contains(x.QualityProject) && x.Location == iter.Location).RevNo : 0;

                                    if (!string.IsNullOrWhiteSpace(QualityId))
                                    {

                                        var lstQMS019 = db.QMS019.Where(i => lstQproj.Contains(i.QualityProject) //&& i.Project == objQMS040.Project
                                                                        && i.BU == iter.BU && i.Location == iter.Location && i.QualityId == QualityId && i.QualityIdRev == Qrev
                                                                        && i.StageCode == iter.StageCode
                                                                    ).ToList();
                                        NDEModels objNDEModels = new NDEModels();

                                        if (lstQMS019.Count() > 0)
                                        {
                                            int i = 0;
                                            foreach (var item in lstQMS019)
                                            {
                                                arrElement[i] = item.Element;
                                                i = i + 1;
                                                if (i == 5)
                                                    break;
                                            }
                                        }
                                    }
                                    #endregion
                                }
                            }

                            db.QMS054.Add(new QMS054
                            {
                                HeaderId = iter.HeaderId,
                                RefRequestId = reqDetail.RequestId,
                                Element1 = arrElement[0],
                                Element2 = arrElement[1],
                                Element3 = arrElement[2],
                                Element4 = arrElement[3],
                                Element5 = arrElement[4],
                                Element6 = arrElement[5],
                                CreatedBy = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName,
                                CreatedOn = DateTime.Now
                            });
                            db.SaveChanges();
                        }
                    }

                    if (objQMS050 != null)
                    {
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), iter.Project, iter.BU, iter.Location, "Stage: " + iter.StageCode + " of Seam: " + iter.SeamNo + " & Project No: " + iter.QualityProject + " has been offered for Inspection", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/MaintainSeamListOfferInspection/ApproveSeamListInspection/" + objQMS050.RequestId, null, APIRequestFrom, UserName);
                    }
                }
                #endregion

                objResponseMsg.Key = true;
                objResponseMsg.Value = response;
                return Json(objResponseMsg);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error Occures Please try again";
                return Json(objResponseMsg);
            }

        }
        // ALSO USED in Mobile Application
        [HttpPost]
        public ActionResult CheckSeamListOffered(int? headerId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS040 objQMS040 = db.QMS040.Where(x => x.HeaderId == headerId).FirstOrDefault();
                QMS012 objQMS012 = null;
                if (objQMS040 != null)
                {
                    if (Helper.IsChemstage(objQMS040.StageCode, objQMS040.BU, objQMS040.Location) ? db.QMS051.Any(x => x.HeaderId == headerId) : true)
                    {
                        CopyWelderWeldLengthFromPreviousNDEStage(Convert.ToInt32(headerId), false, APIRequestFrom, UserName);

                        #region Check For Offer this stage without maintain overlay thickness material  NDE stage

                        //bool IsSTAGEDEPARTMENTNDE = db.Database.SqlQuery<bool>("SELECT dbo.FUN_GET_IPI_ISSTAGEDEPARTMENTNDE('" + objQMS040.HeaderId + "')").FirstOrDefault();
                        //objQMS012 = db.QMS012.FirstOrDefault(c => c.QualityProject == objQMS040.QualityProject && c.SeamNo == objQMS040.SeamNo);
                        //if (objQMS012 != null)
                        //{
                        //    objResponseMsg.Key = true;
                        //    string thicknessposition6 = "";
                        //    var thicknessposition = objQMS012.Thickness.Split(',');
                        //    if (!string.IsNullOrEmpty(objQMS012.Thickness))
                        //    {
                        //        if (thicknessposition.Length >= 6)
                        //        {
                        //            thicknessposition6 = thicknessposition[5].ToString();
                        //        }
                        //    }
                        //    if (!string.IsNullOrEmpty(thicknessposition6))
                        //    {
                        //        if (IsSTAGEDEPARTMENTNDE)
                        //        {
                        //            if (string.IsNullOrEmpty(objQMS040.Material))
                        //            {
                        //                objResponseMsg.Key = false;
                        //                objResponseMsg.Value = "You can't offer this stage without maintain overlay material";
                        //            }
                        //            else
                        //            {
                        //                objResponseMsg.Key = true;
                        //            }
                        //        }
                        //    }
                        //}

                        #endregion

                        #region Check For Any Stage Approved or not in ICL
                        //if (objResponseMsg.Key)
                        //{
                        var stageStatus = clsImplementationEnum.TestPlanStatus.Approved.GetStringValue();
                        var lstTPStages = db.QMS021.Where(x => x.QualityProject == objQMS040.QualityProject && x.Project == objQMS040.Project
                                        && x.Location == objQMS040.Location && x.BU == objQMS040.BU && x.SeamNo == objQMS040.SeamNo
                                        && (x.StageSequance <= objQMS040.StageSequence || (x.StageCode == objQMS040.StageCode && x.StageSequance == objQMS040.StageSequence)) && !x.StageStatus.Equals(stageStatus, StringComparison.OrdinalIgnoreCase)).ToList();
                        if (lstTPStages.Any())
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Please Approve sequence " + lstTPStages[0].StageSequance + " of Stage " + lstTPStages[0].StageCode + " in Test Plan";
                        }
                        else
                        {
                            var lstICLStages = db.QMS031.Where(x => x.QualityProject == objQMS040.QualityProject && x.Project == objQMS040.Project
                                        && x.Location == objQMS040.Location && x.BU == objQMS040.BU && x.SeamNo == objQMS040.SeamNo
                                        && (x.StageSequance <= objQMS040.StageSequence || (x.StageCode == objQMS040.StageCode && x.StageSequance == objQMS040.StageSequence)) && x.StageSequance <= objQMS040.StageSequence
                                        && !x.StageStatus.Equals(stageStatus, StringComparison.OrdinalIgnoreCase)).ToList();
                            if (lstICLStages.Any())
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = "Please Approve sequence " + lstICLStages[0].StageSequance + " of Stage " + lstICLStages[0].StageCode + " in ICL Seam";
                            }
                            else
                                objResponseMsg.Key = true;
                        }
                        //}
                        #endregion

                        //#region Check For Any Stage Approved or not in ICL
                        ////if (objResponseMsg.Key)
                        ////{
                        //var stageStatus = clsImplementationEnum.TestPlanStatus.Approved.GetStringValue();
                        //var lstTPStages = db.QMS021.Where(x => x.QualityProject == objQMS040.QualityProject && x.Project == objQMS040.Project
                        //                && x.Location == objQMS040.Location && x.BU == objQMS040.BU && x.SeamNo == objQMS040.SeamNo
                        //                && (x.StageSequance <= objQMS040.StageSequence || (x.StageCode == objQMS040.StageCode && x.StageSequance == objQMS040.StageSequence)) && !x.StageStatus.Equals(stageStatus, StringComparison.OrdinalIgnoreCase)).ToList();
                        //if (lstTPStages.Any())
                        //{
                        //    objResponseMsg.Key = false;
                        //    objResponseMsg.Value = "Please Approve sequence " + lstTPStages[0].StageSequance + " of Stage " + lstTPStages[0].StageCode + " in Test Plan";
                        //}
                        //else
                        //{
                        //    var lstICLStages = db.QMS031.Where(x => x.QualityProject == objQMS040.QualityProject && x.Project == objQMS040.Project
                        //                && x.Location == objQMS040.Location && x.BU == objQMS040.BU && x.SeamNo == objQMS040.SeamNo
                        //                && (x.StageSequance <= objQMS040.StageSequence || (x.StageCode == objQMS040.StageCode && x.StageSequance == objQMS040.StageSequence)) && x.StageSequance <= objQMS040.StageSequence
                        //                && !x.StageStatus.Equals(stageStatus, StringComparison.OrdinalIgnoreCase)).ToList();
                        //    if (lstICLStages.Any())
                        //    {
                        //        objResponseMsg.Key = false;
                        //        objResponseMsg.Value = "Please Approve sequence " + lstICLStages[0].StageSequance + " of Stage " + lstICLStages[0].StageCode + " in ICL Seam";
                        //    }
                        //    else
                        //        objResponseMsg.Key = true;
                        //}
                        ////}
                        //#endregion

                        #region Check For PRotocol Filled or not by Production Team
                        if (objResponseMsg.Key && objQMS040.ProtocolId.HasValue && !Manager.IsProtocolFilledByProduction(objQMS040.ProtocolId.Value, objQMS040.ProtocolType, objQMS040.BU, objQMS040.Location))
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "You can't offer stage without fill protocol data";
                        }
                        #endregion

                        //#region Check HBOM For Is Seam Applicable For Offer
                        //if (objResponseMsg.Key)
                        //{
                        //    int maxOfferedSequenceNo = Manager.GetMaxOfferedSequenceNo(objQMS040.QualityProject, objQMS040.BU, objQMS040.Location, objQMS040.SeamNo);
                        //    if (maxOfferedSequenceNo == 0)
                        //    {
                        //        QMS012 objQMS012 = db.QMS012.FirstOrDefault(c => c.QualityProject == objQMS040.QualityProject && c.Project == objQMS040.Project && c.SeamNo == objQMS040.SeamNo);
                        //        if (objQMS012 != null)
                        //        {
                        //            if (!Manager.IsPartApplicableForRTO(objQMS012.Project, objQMS012.QualityProject, objQMS012.AssemblyNo))
                        //            {
                        //                objResponseMsg.Key = false;
                        //                objResponseMsg.Value = "The child of this assembly " + objQMS012.AssemblyNo + " are not cleared";
                        //            }
                        //        }
                        //    }
                        //}
                        //#endregion


                        #region Check For Token Generated or Not For Weld Visual Stage
                        if (objResponseMsg.Key)
                        {
                            //var lstAllowSeam = Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.AllowSeamCategoryGroupOffer.GetStringValue()).ToUpper().Split(',').ToArray();
                            //string SeamCategory = db.QMS012.FirstOrDefault(c => c.QualityProject == objQMS040.QualityProject && c.Project == objQMS040.Project && c.SeamNo == objQMS040.SeamNo).SeamCategory;

                            if (/*!lstAllowSeam.Contains(SeamCategory.Trim().ToUpper()) &&*/ Manager.IsWeldVisualStage(objQMS040.BU, objQMS040.Location, objQMS040.StageCode) && !db.WPS025.Any(x => x.QualityProject == objQMS040.QualityProject && x.Project == objQMS040.Project && x.SeamNo == objQMS040.SeamNo))
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = "You can't offer this stage without generate token / print slip";
                            }
                        }

                        #endregion


                        #region Check For Maintain Weld Length
                        if (objResponseMsg.Key && Manager.IsWelderRequiredInNDEStageOffer(objQMS040.BU, objQMS040.Location, objQMS040.StageCode) && (!Manager.IsNDERequestConfirmationRequiredOnFirstInteration(objQMS040.Project, objQMS040.BU, objQMS040.Location) && Manager.IsNDERequest(objQMS040.StageCode, objQMS040.BU, objQMS040.Location)))
                        {

                            List<QMS061> lstQMS061 = db.QMS061.Where(x => x.HeaderId == objQMS040.HeaderId).ToList();
                            bool isOverlay = Manager.IsOverlaySeam(objQMS040.QualityProject, objQMS040.SeamNo);
                            string message = string.Empty;

                            //decimal decValue = 0;
                            //QMS012 objQMS012 = db.QMS012.FirstOrDefault(c => c.QualityProject == objQMS040.QualityProject && c.SeamNo == objQMS040.SeamNo);
                            //if (objQMS012.SeamLengthArea != null)
                            //    decValue = Convert.ToDecimal(objQMS012.SeamLengthArea);

                            decimal? sum = 0;
                            if (isOverlay)
                            {
                                sum = lstQMS061.Sum(x => x.Weldarea);
                                if (sum <= 0)
                                {
                                    message = "Total weld area should be greater than 0.";
                                    objResponseMsg.Key = false;
                                    objResponseMsg.Value = message;
                                }
                                else
                                {
                                    objResponseMsg.Key = true;
                                    objResponseMsg.Value = message;
                                }
                            }
                            else
                            {
                                sum = lstQMS061.Sum(x => x.Weldlength);
                                if (sum <= 0)
                                {
                                    message = "Total weld length should be greater than 0.";
                                    objResponseMsg.Key = false;
                                    objResponseMsg.Value = message;
                                }
                                else
                                {
                                    objResponseMsg.Key = true;
                                    objResponseMsg.Value = message;
                                }
                            }
                        }
                        #endregion

                        #region Check for additional welder exists in token/parameter slip 

                        if (objResponseMsg.Key)
                        {
                            List<QMS061> lstQMS061 = db.QMS061.Where(x => x.HeaderId == objQMS040.HeaderId).ToList();
                            if (lstQMS061.Count > 0)
                            {
                                List<WPS025> lstWPS025 = db.WPS025.Where(x => x.QualityProject == objQMS040.QualityProject && x.Project == objQMS040.Project && x.SeamNo == objQMS040.SeamNo).OrderBy(o => o.PrintedOn).ToList();

                                List<string> lstProcess = new List<string>();
                                foreach (var weldingProcess in lstQMS061.Select(s => s.WeldingProcess).Distinct().ToList())
                                {
                                    foreach (var wp in weldingProcess.Split('+'))
                                    {
                                        if (!string.IsNullOrWhiteSpace(wp))
                                        {
                                            lstProcess.Add(wp);
                                        }
                                    }
                                }

                                if (lstQMS061.Select(s => s.welderstamp).Distinct().Count() != lstWPS025.Select(s => s.WelderStamp).Distinct().Count() || lstProcess.Distinct().Count() != lstWPS025.Select(s => s.WeldingProcess).Distinct().Count())
                                {
                                    objResponseMsg.Key = false;
                                    objResponseMsg.Value = "Please Maintain Weld Length";
                                }
                            }
                        }

                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Please Maintain Chemical Test Details";
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please Try Again";
                }

                return Json(objResponseMsg);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error Occures Please try again";
                return Json(objResponseMsg);
            }
        }

        [HttpPost]
        public ActionResult CopyWelderWeldLengthFromPreviousNDEStage(int HeaderId, bool IsLTFPS = false, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (IsLTFPS)
                {
                    if (!db.QMS061.Any(c => c.LTFPSHeaderId == HeaderId))
                    {
                        #region Get Welders For LTFPS

                        LTF002 objLTF002 = db.LTF002.FirstOrDefault(c => c.LineId == HeaderId);
                        bool IsSTAGEDEPARTMENTNDE = db.Database.SqlQuery<bool>("SELECT dbo.FUN_GET_LTFPS_ISSTAGEDEPARTMENTNDE('" + objLTF002.LineId + "')").FirstOrDefault();
                        if (IsSTAGEDEPARTMENTNDE)
                        {
                            List<QMS061> lstQMS061 = new List<QMS061>();

                            QMS031 objQMS031 = db.QMS031.FirstOrDefault(c => c.QualityProject == objLTF002.QualityProject && c.Project == objLTF002.Project && c.BU == objLTF002.BU && c.Location == objLTF002.Location && c.SeamNo == objLTF002.SeamNo && c.StageCode == objLTF002.Stage);
                            List<QMS061> lstSrcQMS061 = db.Database.SqlQuery<QMS061>(@"SELECT * FROM dbo.FN_GET_WELDERS_FROM_PREV_STAGE('" + objLTF002.QualityProject + "','" + objLTF002.Project + "','" + objLTF002.BU + "','" + objLTF002.Location + "','" + objLTF002.SeamNo + "'," + objQMS031.StageSequance + ")").ToList();

                            foreach (var objQMS061 in lstSrcQMS061)
                            {
                                QMS061 objNewQMS061 = new QMS061
                                {
                                    LTFPSHeaderId = objLTF002.LineId,
                                    HeaderId = null,
                                    RefRequestId = null,
                                    QualityProject = objLTF002.QualityProject,
                                    Project = objLTF002.Project,
                                    BU = objLTF002.BU,
                                    Location = objLTF002.Location,
                                    SeamNo = objLTF002.SeamNo,
                                    StageCode = objLTF002.Stage,
                                    StageSequence = objQMS031.StageSequance,
                                    IterationNo = null,
                                    RequestNo = null,
                                    RequestNoSequence = null,
                                    welderstamp = objQMS061.welderstamp,
                                    WeldingProcess = objQMS061.WeldingProcess,
                                    Weldlength = objQMS061.Weldlength,
                                    Weldarea = objQMS061.Weldarea,
                                    CreatedBy = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName,
                                    CreatedOn = DateTime.Now
                                };
                                lstQMS061.Add(objNewQMS061);
                            }
                            if (lstQMS061.Count > 0)
                            {
                                db.QMS061.AddRange(lstQMS061);
                                db.SaveChanges();
                            }
                        }
                        #endregion
                    }
                }
                else
                {
                    if (!db.QMS061.Any(c => c.HeaderId == HeaderId))
                    {
                        #region Get Welders For IPI

                        QMS040 objQMS040 = db.QMS040.FirstOrDefault(c => c.HeaderId == HeaderId);
                        bool IsSTAGEDEPARTMENTNDE = db.Database.SqlQuery<bool>("SELECT dbo.FUN_GET_IPI_ISSTAGEDEPARTMENTNDE('" + objQMS040.HeaderId + "')").FirstOrDefault();
                        if (IsSTAGEDEPARTMENTNDE)
                        {
                            List<QMS061> lstQMS061 = new List<QMS061>();
                            List<QMS061> lstSrcQMS061 = db.Database.SqlQuery<QMS061>(@"SELECT * FROM dbo.FN_GET_WELDERS_FROM_PREV_STAGE('" + objQMS040.QualityProject + "','" + objQMS040.Project + "','" + objQMS040.BU + "','" + objQMS040.Location + "','" + objQMS040.SeamNo + "'," + objQMS040.StageSequence + ")").ToList();

                            foreach (var objQMS061 in lstSrcQMS061)
                            {
                                QMS061 objNewQMS061 = new QMS061
                                {
                                    LTFPSHeaderId = null,
                                    HeaderId = objQMS040.HeaderId,
                                    RefRequestId = null,
                                    QualityProject = objQMS040.QualityProject,
                                    Project = objQMS040.Project,
                                    BU = objQMS040.BU,
                                    Location = objQMS040.Location,
                                    SeamNo = objQMS040.SeamNo,
                                    StageCode = objQMS040.StageCode,
                                    StageSequence = objQMS040.StageSequence,
                                    IterationNo = null,
                                    RequestNo = null,
                                    RequestNoSequence = null,
                                    welderstamp = objQMS061.welderstamp,
                                    WeldingProcess = objQMS061.WeldingProcess,
                                    Weldlength = objQMS061.Weldlength,
                                    Weldarea = objQMS061.Weldarea,
                                    CreatedBy = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName,
                                    CreatedOn = DateTime.Now
                                };
                                lstQMS061.Add(objNewQMS061);
                            }
                            if (lstQMS061.Count > 0)
                            {
                                db.QMS061.AddRange(lstQMS061);
                                db.SaveChanges();
                            }
                        }
                        #endregion
                    }
                }



                objResponseMsg.Key = true;
                return Json(objResponseMsg);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error Occures Please try again";
                return Json(objResponseMsg);
            }
        }

        // ALSO USED in Mobile Application
        [HttpPost]
        public ActionResult RespondTestDetailRequest(int requestId, string type, string qcRemark, string docRevNo, bool IsRequireReworkLTFPS = false, double GroundSpot = 0, string APIRequestFrom = "", string UserName = "")
        {
            bool isOTCApplicable = false;
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                bool flag = true;
                string msg = string.Empty;
                QMS050 objQMS050 = db.QMS050.FirstOrDefault(x => x.RequestId == requestId);
                string stagetype = Manager.FetchStageType(objQMS050.StageCode, objQMS050.BU, objQMS050.Location);

                if (type == "OTC")
                {
                    objQMS050.OfferedtoCustomerBy = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                    objQMS050.OfferedtoCustomerOn = DateTime.Now;
                    objQMS050.InspectedBy = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                    objQMS050.InspectedOn = DateTime.Now;
                    objQMS050.TestResult = clsImplementationEnum.SeamListInspectionStatus.Cleared.GetStringValue();
                    if (stagetype == clsImplementationEnum.SeamStageType.Chemical.GetStringValue())
                    {
                        var lstChemDet = db.QMS051.Where(x => x.RefRequestId == requestId).ToList();
                        lstChemDet.ForEach(x =>
                        {
                            x.Inspectedby = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                            x.InspectedOn = DateTime.Now;
                        });
                    }
                    msg = "Seam Successfully Offer to TPI";
                    flag = true;
                    db.SaveChanges();

                    var response = new { Key = flag, Value = msg, isOtc = isOTCApplicable };
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    bool isMaintain = true;
                    QMS052 objQMS052 = db.QMS052.Where(x => x.RefRequestId == requestId).FirstOrDefault();
                    if (type == clsImplementationEnum.SeamListInspectionStatus.Cleared.GetStringValue() || type == clsImplementationEnum.SeamListInspectionStatus.Rejected.GetStringValue())
                    {
                        if (stagetype == clsImplementationEnum.SeamStageType.Chemical.GetStringValue())
                        {
                            if (!isManintainedElementResult(requestId))
                            {
                                isMaintain = false;
                                msg = "Please Maintain Instrument/Element Results";
                                flag = false;
                            }
                        }
                        if (stagetype == clsImplementationEnum.SeamStageType.PMI.GetStringValue())
                        {
                            if (!isMaintainedPMIResult(requestId))
                            {
                                isMaintain = false;
                                msg = "Please Maintain  Instrument/PMI Test Details";
                                flag = false;
                            }
                        }
                    }
                    if (type == clsImplementationEnum.SeamListInspectionStatus.Cleared.GetStringValue())
                    {
                        #region Validate results for Chem/PMI/Hardness/Ferrite

                        if (stagetype == clsImplementationEnum.SeamStageType.Hardness.GetStringValue())
                        {
                            var objTestPlan = db.QMS021.Where(x => x.QualityProject == objQMS050.QualityProject && x.SeamNo == objQMS050.SeamNo && x.Location == objQMS050.Location && x.StageCode == objQMS050.StageCode && x.StageSequance == objQMS050.StageSequence).FirstOrDefault();
                            if (objTestPlan != null ? (objTestPlan.HardnessRequiredValue != null && objTestPlan.HardnessUnit != null) : false)
                            {
                                if (!(objTestPlan.HardnessRequiredValue.HasValue) && !db.QMS052_1.Any(x => x.HardnessId == objQMS052.HardnessId))
                                {
                                    isMaintain = false;
                                    msg = "Please Maintain Hardness Test Details";
                                    flag = false;
                                }
                                if (db.QMS052.FirstOrDefault(x => x.HardnessId == objQMS052.HardnessId) != null)
                                {
                                    if (string.IsNullOrWhiteSpace(objQMS052.Instrument))
                                    {
                                        isMaintain = false;
                                        msg = "Please Maintain Instrument";
                                        flag = false;
                                    }
                                }
                            }
                            else
                            {
                                List<string> lstQproj = new List<string>();
                                lstQproj.Add(objQMS050.QualityProject);
                                lstQproj.AddRange(db.QMS017.Where(c => c.BU == objQMS050.BU && c.Location == objQMS050.Location && c.IQualityProject == objQMS050.QualityProject).Select(x => x.QualityProject).ToList());
                                int? objHardnessRequiredValue = 0;
                                var QualityId = db.QMS020.FirstOrDefault(x => x.QualityProject == objQMS050.QualityProject && x.SeamNo == objQMS050.SeamNo && x.Location == objQMS050.Location).QualityId;
                                if (!string.IsNullOrWhiteSpace(QualityId))
                                {
                                    objHardnessRequiredValue = (from a in db.QMS015_Log
                                                                join b in db.QMS016_Log on a.Id equals b.RefId
                                                                where a.Status == "Approved" && lstQproj.Contains(a.QualityProject) && b.StageCode == objQMS050.StageCode && b.QualityId == QualityId
                                                                select b.HardnessRequiredValue).FirstOrDefault();
                                    if (objHardnessRequiredValue.HasValue && !db.QMS052_1.Any(x => x.HardnessId == objQMS052.HardnessId))
                                    {
                                        isMaintain = false;
                                        msg = "Please Maintain Hardness Test Details";
                                        flag = false;
                                    }
                                    if (db.QMS052.FirstOrDefault(x => x.HardnessId == objQMS052.HardnessId) != null)
                                    {
                                        if (string.IsNullOrWhiteSpace(objQMS052.Instrument))
                                        {
                                            isMaintain = false;
                                            msg = "Please Maintain Instrument";
                                            flag = false;
                                        }
                                    }
                                }
                            }
                        }
                        if (stagetype == clsImplementationEnum.SeamStageType.Ferrite.GetStringValue())
                        {
                            var objTestPlan = db.QMS021.Where(x => x.QualityProject == objQMS050.QualityProject && x.SeamNo == objQMS050.SeamNo && x.Location == objQMS050.Location && x.StageCode == objQMS050.StageCode && x.StageSequance == objQMS050.StageSequence).FirstOrDefault();
                            if (objTestPlan != null ? (objTestPlan.FerriteMaxValue != null && objTestPlan.FerriteMinValue != null && objTestPlan.FerriteUnit != null) : false)
                            {
                                if (objTestPlan.FerriteMinValue.HasValue)
                                {
                                    if (!IsMaintainedFerriteTestResult(requestId))
                                    {
                                        isMaintain = false;
                                        msg = "Please maintain Instrument/Ferrite Test Details";
                                        flag = false;
                                    }
                                }
                            }
                            else
                            {
                                List<string> lstQproj = new List<string>();
                                lstQproj.Add(objQMS050.QualityProject);
                                lstQproj.AddRange(db.QMS017.Where(c => c.BU == objQMS050.BU && c.Location == objQMS050.Location && c.IQualityProject == objQMS050.QualityProject).Select(x => x.QualityProject).ToList());

                                var QualityId = db.QMS020.FirstOrDefault(x => x.QualityProject == objQMS050.QualityProject && x.SeamNo == objQMS050.SeamNo && x.Location == objQMS050.Location).QualityId;
                                double? objFerriteRequiredValue = 0;
                                if (!string.IsNullOrWhiteSpace(QualityId))
                                {
                                    objFerriteRequiredValue = (from a in db.QMS015_Log
                                                               join b in db.QMS016_Log on a.Id equals b.RefId
                                                               where a.Status == "Approved" && lstQproj.Contains(a.QualityProject) && b.StageCode == objQMS050.StageCode && b.QualityId == QualityId
                                                               select b.FerriteMinValue).FirstOrDefault();
                                    if (objFerriteRequiredValue.HasValue)
                                    {
                                        if (!IsMaintainedFerriteTestResult(requestId))
                                        {
                                            isMaintain = false;
                                            msg = "Please Maintain Intrument/Ferrite Test Details";
                                            flag = false;
                                        }
                                    }
                                }

                            }
                        }
                        #endregion
                    }
                    if (!isMaintain)
                    {
                        var responsemessage = new { Key = flag, Value = msg, isMaintained = isMaintain };
                        return Json(responsemessage, JsonRequestBehavior.AllowGet);
                    }
                    objQMS050.GroundSpot = GroundSpot;
                    //Added By Deval Barot on 19/03/2020
                    string TPIAgency1 = objQMS050.TPIAgency1;
                    string TPIAgency2 = objQMS050.TPIAgency2;
                    string TPIAgency3 = objQMS050.TPIAgency3;
                    string TPIAgency4 = objQMS050.TPIAgency4;
                    string TPIAgency5 = objQMS050.TPIAgency5;
                    string TPIAgency6 = objQMS050.TPIAgency6;
                    if (TPIAgency1 != null || TPIAgency2 != null || TPIAgency3 != null || TPIAgency4 != null || TPIAgency5 != null || TPIAgency6 != null)
                    {
                        objQMS050.IsTPIPending = true;
                    }
                    //End Here
                    objResponseMsg = Manager.IsApplicableForAttend(objQMS050.RequestId, clsImplementationEnum.InspectionFor.SEAM.GetStringValue(), "QC");
                    if (!objResponseMsg.Key)
                    {
                        return Json(objResponseMsg);
                    }

                    if (Manager.IsLTFPSProject(objQMS050.Project, objQMS050.BU, objQMS050.Location) && !string.IsNullOrWhiteSpace(docRevNo))
                    {
                        objQMS050.LTF002.DocRevNo = docRevNo;
                    }
                    db.SaveChanges();
                    string TempUserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                    int? isTPIOnline = db.SP_IPI_SEAMQTESTQCRESPOND(requestId, type, qcRemark, TempUserName, IsRequireReworkLTFPS).FirstOrDefault();
                    if (isTPIOnline == 1)
                    {
                        isOTCApplicable = true;
                    }
                    else
                    {
                        var objQMS030 = db.QMS030.Where(z => z.QualityProject == objQMS050.QualityProject && z.Project == objQMS050.Project && z.BU == objQMS050.BU && z.Location == objQMS050.Location && z.SeamNo == objQMS050.SeamNo).FirstOrDefault();
                        if (objQMS030 != null)
                        {
                            Manager.ReadyToOffer_SEAM(objQMS030);
                        }
                        #region Code for make ready to offer parent part
                        //if (type == clsImplementationEnum.SeamListInspectionStatus.Cleared.GetStringValue() && !Manager.IsLTFPSProject(objQMS050.Project, objQMS050.BU, objQMS050.Location))
                        //{
                        //    string ApprovedStatus = clsImplementationEnum.ICLPartStatus.Approved.GetStringValue();
                        //    if (Manager.IsSEAMFullyCleared(objQMS050.Project, objQMS050.QualityProject, objQMS050.BU, objQMS050.Location, objQMS050.SeamNo))
                        //    {
                        //        QMS012 objQMS012 = db.QMS012.FirstOrDefault(c => c.Project == objQMS050.Project && c.QualityProject == objQMS050.QualityProject && c.SeamNo == objQMS050.SeamNo);
                        //        QMS035 objQMS035 = db.QMS035.FirstOrDefault(c => c.Project == objQMS050.Project && c.QualityProject == objQMS050.QualityProject && c.BU == objQMS050.BU && c.Location == objQMS050.Location && c.ChildPartNo == objQMS012.AssemblyNo && c.Status == ApprovedStatus);
                        //        if (objQMS035 != null)
                        //        {
                        //            Manager.ReadyToOffer_PART(objQMS035);
                        //        }

                        //    }
                        //}
                        #endregion
                    }
                    int NewRequestId = db.QMS050.Where(x => x.HeaderId == objQMS050.HeaderId).OrderByDescending(x => x.RequestId).Select(x => x.RequestId).FirstOrDefault();

                    string NewfolderPath = "";
                    string OldfolderPath = "";
                    if (type == clsImplementationEnum.SeamListInspectionStatus.Rejected.GetStringValue())
                    {
                        int iTableId = 0;
                        objQMS050.GroundSpot = GroundSpot;
                        if (objQMS050.LTFPSHeaderId > 0)
                        {
                            OldfolderPath = "QMS050_LTFPS//" + objQMS050.LTFPSHeaderId + "//" + objQMS050.IterationNo + "//" + objQMS050.RequestNoSequence;
                            NewfolderPath = "QMS050_LTFPS//" + objQMS050.LTFPSHeaderId + "//" + (objQMS050.IterationNo + 1) + "//" + objQMS050.RequestNoSequence;
                            iTableId = Convert.ToInt32(objQMS050.LTFPSHeaderId);
                        }
                        else
                        {

                            OldfolderPath = "QMS050//" + objQMS050.HeaderId.Value + "//" + objQMS050.IterationNo + "//" + objQMS050.RequestNoSequence;
                            NewfolderPath = "QMS050//" + objQMS050.HeaderId + "//" + (objQMS050.IterationNo + 1) + "//" + objQMS050.RequestNoSequence;

                            iTableId = Convert.ToInt32(objQMS050.HeaderId);

                            if (stagetype == clsImplementationEnum.SeamStageType.Chemical.GetStringValue())
                            { CopyChemTestDetails(objQMS050.HeaderId, requestId, APIRequestFrom, UserName); }
                            else if (stagetype == clsImplementationEnum.SeamStageType.PMI.GetStringValue())
                            {
                                var QMS054 = db.QMS054.Where(x => x.RefRequestId == requestId).FirstOrDefault();
                                db.QMS054.Add(new QMS054
                                {
                                    HeaderId = QMS054.HeaderId,
                                    RefRequestId = NewRequestId,
                                    Element1 = QMS054.Element1,
                                    Element2 = QMS054.Element2,
                                    Element3 = QMS054.Element3,
                                    Element4 = QMS054.Element4,
                                    Element5 = QMS054.Element5,
                                    Element6 = QMS054.Element6,
                                    CreatedBy = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName,
                                    CreatedOn = DateTime.Now
                                });
                                db.SaveChanges();
                            }
                            //(new clsFileUpload()).CopyFolderContentsAsync(OldfolderPath, NewfolderPath);
                            Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                            _objFUC.CopyDataOnFCSServerAsync(OldfolderPath,NewfolderPath,OldfolderPath, iTableId,NewfolderPath, iTableId,DESServices.CommonService.GetUseIPConfig,DESServices.CommonService.objClsLoginInfo.UserName,0,false);
                        }
                    }
                    else if (type == clsImplementationEnum.SeamListInspectionStatus.Returned.GetStringValue())
                    {
                        int iTableId = 0;
                        if (objQMS050.LTFPSHeaderId > 0)
                        {
                            OldfolderPath = "QMS050_LTFPS//" + objQMS050.LTFPSHeaderId + "//" + objQMS050.IterationNo + "//" + objQMS050.RequestNoSequence;
                            NewfolderPath = "QMS050_LTFPS//" + objQMS050.LTFPSHeaderId + "//" + objQMS050.IterationNo + "//" + (objQMS050.RequestNoSequence + 1);

                            iTableId = Convert.ToInt32(objQMS050.LTFPSHeaderId);
                        }
                        else
                        {
                            OldfolderPath = "QMS050//" + objQMS050.HeaderId.Value + "//" + objQMS050.IterationNo + "//" + objQMS050.RequestNoSequence;
                            NewfolderPath = "QMS050//" + objQMS050.HeaderId + "//" + objQMS050.IterationNo + "//" + (objQMS050.RequestNoSequence + 1);

                            iTableId = Convert.ToInt32(objQMS050.HeaderId);

                            if (stagetype == clsImplementationEnum.SeamStageType.Chemical.GetStringValue())
                            {
                                CopyChemTestDetails(objQMS050.HeaderId, requestId, APIRequestFrom, UserName);
                            }
                            else if (stagetype == clsImplementationEnum.SeamStageType.PMI.GetStringValue())
                            {
                                var QMS054 = db.QMS054.Where(x => x.RefRequestId == requestId).FirstOrDefault();
                                db.QMS054.Add(new QMS054
                                {
                                    HeaderId = QMS054.HeaderId,
                                    RefRequestId = NewRequestId,
                                    Element1 = QMS054.Element1,
                                    Element2 = QMS054.Element2,
                                    Element3 = QMS054.Element3,
                                    Element4 = QMS054.Element4,
                                    Element5 = QMS054.Element5,
                                    Element6 = QMS054.Element6,
                                    CreatedBy = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName,
                                    CreatedOn = DateTime.Now
                                });
                                db.SaveChanges();
                            }
                        }
                        //(new clsFileUpload()).CopyFolderContentsAsync(OldfolderPath, NewfolderPath);
                        Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                        _objFUC.CopyDataOnFCSServerAsync(OldfolderPath, NewfolderPath, OldfolderPath, iTableId, NewfolderPath, iTableId, DESServices.CommonService.GetUseIPConfig, DESServices.CommonService.objClsLoginInfo.UserName, 0, false);
                    }

                    #region Send Notification
                    if (type.ToLower() == clsImplementationEnum.SeamListInspectionStatus.Cleared.GetStringValue().ToLower())
                    {
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PROD3.GetStringValue(), objQMS050.Project, objQMS050.BU, objQMS050.Location, "Stage: " + objQMS050.StageCode + " of Seam: " + objQMS050.SeamNo + " & Project No: " + objQMS050.QualityProject + " has been Cleared", clsImplementationEnum.NotificationType.Information.GetStringValue(), null, null, APIRequestFrom, UserName);
                    }
                    else if (type.ToLower() == clsImplementationEnum.SeamListInspectionStatus.Rejected.GetStringValue().ToLower())
                    {
                        QMS050 objQMS050Noti = db.QMS050.Where(x => x.RequestNo == objQMS050.RequestNo && x.HeaderId == objQMS050.HeaderId && x.TestResult == null).OrderByDescending(o => o.IterationNo).FirstOrDefault();
                        if (objQMS050Noti != null)
                        {
                            (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PROD3.GetStringValue(), objQMS050.Project, objQMS050.BU, objQMS050.Location, "Stage: " + objQMS050.StageCode + " of Seam: " + objQMS050.SeamNo + " & Project No: " + objQMS050.QualityProject + " has been Rejected", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/MaintainSeamListOfferInspection/SendReOfferView/" + objQMS050Noti.RequestId, null, APIRequestFrom, UserName);
                        }
                    }
                    else if (type.ToLower() == clsImplementationEnum.SeamListInspectionStatus.Returned.GetStringValue().ToLower())
                    {
                        QMS050 objQMS050Noti = db.QMS050.Where(x => x.RequestNo == objQMS050.RequestNo && x.HeaderId == objQMS050.HeaderId && x.TestResult == null).OrderByDescending(o => o.IterationNo).FirstOrDefault();
                        if (objQMS050Noti != null)
                        {
                            (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PROD3.GetStringValue(), objQMS050.Project, objQMS050.BU, objQMS050.Location, "Stage: " + objQMS050.StageCode + " of Seam: " + objQMS050.SeamNo + " & Project No: " + objQMS050.QualityProject + " has been Returned", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/MaintainSeamListOfferInspection/SendReOfferView/" + objQMS050Noti.RequestId, null, APIRequestFrom, UserName);
                        }
                    }
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Seam Successfully " + type;
                    msg = "Seam Successfully " + type;
                    flag = true;

                    var response = new { Key = flag, Value = msg, isOtc = isOTCApplicable };
                    return Json(response, JsonRequestBehavior.AllowGet);

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error Occured, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        // ALSO USED in MOBILE APPLICATION
        [HttpPost]
        public ActionResult CopyChemTestDetails(int? headerid, int prevRequestId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int NewRequestId = db.QMS050.Where(x => x.HeaderId == headerid).OrderByDescending(x => x.RequestId).Select(x => x.RequestId).FirstOrDefault();
                List<QMS051> existingChemTestDetails = db.QMS051.Where(x => x.RefRequestId == prevRequestId && x.HeaderId == headerid).ToList();
                db.QMS051.AddRange(existingChemTestDetails.Select(i => new QMS051
                {
                    HeaderId = i.HeaderId,
                    RefRequestId = NewRequestId,
                    QualityProject = i.QualityProject,
                    Project = i.Project,
                    BU = i.BU,
                    Location = i.Location,
                    SeamNo = i.SeamNo,
                    StageCode = i.StageCode,
                    StageSequence = i.StageSequence,
                    IterationNo = i.IterationNo,
                    RequestNo = i.RequestNo,
                    RequestNoSequence = i.RequestNoSequence,
                    WeldingProcess = i.WeldingProcess,
                    Sample = i.Sample,
                    Position = i.Position,
                    Welder1 = i.Welder1,
                    Welder2 = i.Welder2,
                    Welder3 = i.Welder3,
                    Welder4 = i.Welder4,
                    Welder5 = i.Welder5,
                    Welder6 = i.Welder6,
                    Welder7 = i.Welder7,
                    Welder8 = i.Welder8,
                    Welder9 = i.Welder9,
                    Welder10 = i.Welder10,
                    Welder11 = i.Welder11,
                    Welder12 = i.Welder12,
                    Welder13 = i.Welder13,
                    Welder14 = i.Welder14,
                    Welder15 = i.Welder15,
                    Welder16 = i.Welder16,
                    Welder17 = i.Welder17,
                    Welder18 = i.Welder18,
                    Welder19 = i.Welder19,
                    Welder20 = i.Welder20,
                    Welder21 = i.Welder21,
                    Welder22 = i.Welder22,
                    Welder23 = i.Welder23,
                    Welder24 = i.Welder24,
                    Element1 = i.Element1,
                    ExaminationResult1 = null,
                    Element2 = i.Element2,
                    ExaminationResult2 = null,
                    Element3 = i.Element3,
                    ExaminationResult3 = null,
                    Element4 = i.Element4,
                    ExaminationResult4 = null,
                    Element5 = i.Element5,
                    ExaminationResult5 = null,
                    Element6 = i.Element6,
                    ExaminationResult6 = null,
                    Element7 = i.Element7,
                    ExaminationResult7 = null,
                    Element8 = i.Element8,
                    ExaminationResult8 = null,
                    Element9 = i.Element9,
                    ExaminationResult9 = null,
                    Element10 = i.Element10,
                    ExaminationResult10 = null,
                    Element11 = i.Element11,
                    ExaminationResult11 = null,
                    Element12 = i.Element12,
                    ExaminationResult12 = null,
                    CreatedBy = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                }));
                db.SaveChanges();
                objResponseMsg.dataKey = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.dataKey = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        public bool isManintainedElementResult(int reqid)
        {
            bool isManintained = true;

            var lstElements = db.QMS051.Where(x => x.RefRequestId == reqid).ToList();


            if (lstElements.Count > 0)
            {
                if (lstElements.Any(x => x.Instrument == "" || x.Instrument == null))
                {
                    return false;
                }
                foreach (var item in lstElements)
                {
                    if (!isManintained)
                    {
                        break;
                    }

                    if (!string.IsNullOrWhiteSpace(item.Element1))
                    {
                        if (item.ExaminationResult1 == null || !item.ExaminationResult1.HasValue)
                        {
                            isManintained = false;
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(item.Element2))
                    {
                        if (item.ExaminationResult2 == null || !item.ExaminationResult2.HasValue)
                        {
                            isManintained = false;
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(item.Element3))
                    {
                        if (item.ExaminationResult3 == null || !item.ExaminationResult3.HasValue)
                        {
                            isManintained = false;
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(item.Element4))
                    {
                        if (item.ExaminationResult4 == null || !item.ExaminationResult4.HasValue)
                        {
                            isManintained = false;
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(item.Element5))
                    {
                        if (item.ExaminationResult5 == null || !item.ExaminationResult5.HasValue)
                        {
                            isManintained = false;
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(item.Element6))
                    {
                        if (item.ExaminationResult6 == null || !item.ExaminationResult6.HasValue)
                        {
                            isManintained = false;
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(item.Element7))
                    {
                        if (item.ExaminationResult7 == null || !item.ExaminationResult7.HasValue)
                        {
                            isManintained = false;
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(item.Element8))
                    {
                        if (item.ExaminationResult8 == null || !item.ExaminationResult8.HasValue)
                        {
                            isManintained = false;
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(item.Element9))
                    {
                        if (item.ExaminationResult9 == null || !item.ExaminationResult9.HasValue)
                        {
                            isManintained = false;
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(item.Element10))
                    {
                        if (item.ExaminationResult10 == null || !item.ExaminationResult10.HasValue)
                        {
                            isManintained = false;
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(item.Element11))
                    {
                        if (item.ExaminationResult11 == null || !item.ExaminationResult11.HasValue)
                        {
                            isManintained = false;
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(item.Element12))
                    {
                        if (item.ExaminationResult12 == null || !item.ExaminationResult12.HasValue)
                        {
                            isManintained = false;
                        }
                    }

                }
            }
            return isManintained;
        }

        public bool isMaintainedPMIResult(int reqid)
        {
            bool isManintained = true;
            try
            {
                QMS050 objQMS050 = db.QMS050.FirstOrDefault(x => x.RequestId == reqid);
                var objTestPlan = db.QMS021.Where(x => x.QualityProject == objQMS050.QualityProject && x.SeamNo == objQMS050.SeamNo && x.Location == objQMS050.Location && x.StageCode == objQMS050.StageCode && x.StageSequance == objQMS050.StageSequence).FirstOrDefault();
                if (objTestPlan != null)
                {
                    var lstQMS023 = objTestPlan != null ? db.QMS023.Where(i => i.TPLineId == objTestPlan.LineId).OrderByDescending(x => x.TPRevNo).ToList() : null;
                    if (lstQMS023.Count() == 0 || lstQMS023 == null)
                    {
                        return true;
                    }
                    else
                    {
                        if (lstQMS023.Count() == 0)
                        {
                            List<string> lstQproj = new List<string>();
                            lstQproj.Add(objQMS050.QualityProject);
                            lstQproj.AddRange(db.QMS017.Where(c => c.BU == objQMS050.BU && c.Location == objQMS050.Location && c.IQualityProject == objQMS050.QualityProject).Select(x => x.QualityProject).ToList());
                            var QualityId = db.QMS020.FirstOrDefault(x => x.QualityProject == objQMS050.QualityProject && x.SeamNo == objQMS050.SeamNo && x.Location == objQMS050.Location).QualityId;
                            var objRev = db.QMS015.FirstOrDefault(x => x.QualityId == QualityId && lstQproj.Contains(x.QualityProject) && x.Location == objQMS050.Location);
                            int? Qrev = QualityId != null ? ((objRev != null) ? objRev.RevNo : 0) : 0;

                            if (!string.IsNullOrWhiteSpace(QualityId) && objRev != null)
                            {

                                var lstQMS019 = db.QMS019.Where(i => lstQproj.Contains(i.QualityProject) //&& i.Project == objQMS040.Project
                                                                && i.BU == objQMS050.BU && i.Location == objQMS050.Location && i.QualityId == QualityId && i.QualityIdRev == Qrev
                                                                && i.StageCode == objQMS050.StageCode
                                                            ).ToList();

                                if (lstQMS019.Count() == 0)
                                {
                                    return true;
                                }
                            }
                        }
                    }
                }
                var objQMS054 = db.QMS054.Where(x => x.RefRequestId == reqid).FirstOrDefault();
                if (objQMS054 == null)
                {
                    return false;
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(objQMS054.Instrument))
                        return false;
                }
                var lstElements = db.QMS054_1.Where(x => x.PMIId == objQMS054.PMIId).ToList();

                if (lstElements.Count == 0)
                    return false;

                foreach (var item in lstElements)
                {
                    if (!isManintained)
                    {
                        break;
                    }

                    if (!string.IsNullOrWhiteSpace(objQMS054.Element1))
                    {
                        if ((item.InsideReading1 == null || !item.InsideReading1.HasValue) || (item.OutsideReading1 == null || !item.OutsideReading1.HasValue))
                        {
                            isManintained = false;
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(objQMS054.Element2))
                    {
                        if ((item.InsideReading2 == null || !item.InsideReading2.HasValue) || (item.OutsideReading2 == null || !item.OutsideReading2.HasValue))
                        {
                            isManintained = false;
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(objQMS054.Element3))
                    {
                        if ((item.InsideReading3 == null || !item.InsideReading3.HasValue) || (item.OutsideReading3 == null || !item.OutsideReading3.HasValue))
                        {
                            isManintained = false;
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(objQMS054.Element4))
                    {
                        if ((item.InsideReading4 == null || !item.InsideReading4.HasValue) || (item.OutsideReading4 == null || !item.OutsideReading4.HasValue))
                        {
                            isManintained = false;
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(objQMS054.Element5))
                    {
                        if ((item.InsideReading5 == null || !item.InsideReading5.HasValue) || (item.OutsideReading5 == null || !item.OutsideReading5.HasValue))
                        {
                            isManintained = false;
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(objQMS054.Element6))
                    {
                        if ((item.InsideReading6 == null || !item.InsideReading6.HasValue) || (item.OutsideReading6 == null || !item.OutsideReading6.HasValue))
                        {
                            isManintained = false;
                        }
                    }
                }
                return isManintained;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return false;
            }
        }

        public bool IsMaintainedFerriteTestResult(int RequestId)
        {
            bool IsMaintained = true;
            var lstElements = db.QMS053.Where(x => x.RefRequestId == RequestId).ToList();

            QMS050 objQMS050 = db.QMS050.Where(x => x.RequestId == RequestId).FirstOrDefault();
            //if (objQMS050 != null)
            //{
            //    List<string> lstWeldingProcess = GetWeldingProcessForFerriteTest(objQMS050.QualityProject, objQMS050.Project, objQMS050.SeamNo, objQMS050.BU);
            //    if (lstWeldingProcess.Count > 0)
            //    {
            //        foreach (var item in lstWeldingProcess)
            //        {
            //            if (!lstElements.Any(x => x.WeldingProcess == item))
            //            {
            //                IsMaintained = false;
            //                break;
            //            }
            //        }
            //    }
            //}
            if (lstElements.Count == 0)
            {
                IsMaintained = false;
                return IsMaintained;
            }
            if (lstElements.Count > 0 && IsMaintained)
            {
                var objQMS053_1 = db.QMS053_1.Where(x => x.RefRequestId == RequestId).FirstOrDefault();
                if (objQMS053_1 != null)
                {
                    if (string.IsNullOrWhiteSpace(objQMS053_1.Instrument))
                    {
                        return false;
                    }
                }
                foreach (var item in lstElements)
                {
                    if (item.Reading1 == null || !item.Reading1.HasValue)
                    {
                        IsMaintained = false;
                    }
                    else if (item.Reading2 == null || !item.Reading2.HasValue)
                    {
                        IsMaintained = false;
                    }
                    else if (item.Reading3 == null || !item.Reading3.HasValue)
                    {
                        IsMaintained = false;
                    }
                    //else if (item.Reading4 == null || !item.Reading4.HasValue)
                    //{
                    //    IsMaintained = false;
                    //}
                    //else if (item.Reading5 == null || !item.Reading5.HasValue)
                    //{
                    //    IsMaintained = false;
                    //}
                    //else if (item.Reading6 == null || !item.Reading6.HasValue)
                    //{
                    //    IsMaintained = false;
                    //}
                    //else if (string.IsNullOrWhiteSpace(item.Remarks))
                    //{
                    //    IsMaintained = false;
                    //}

                    if (!IsMaintained)
                    {
                        break;
                    }
                }
            }
            return IsMaintained;
        }

        [SessionExpireFilter]
        public ActionResult TPIView()
        {
            return View();
        }
        public ActionResult TPIDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_TPIDataPartial");
        }
        [SessionExpireFilter]
        public ActionResult TPITestDetails(int id)
        {
            QMS050 objQMS050 = db.QMS050.FirstOrDefault(x => x.RequestId == id);
            ViewBag.IsLTFPS = Manager.IsLTFPSProject(objQMS050.Project, objQMS050.BU, objQMS050.Location);

            if (!string.IsNullOrWhiteSpace(Convert.ToString(objQMS050.ProtocolType)))
            {
                if (objQMS050.ProtocolId.HasValue)
                {
                    ViewBag.ProtocolURL = WebsiteURL + "/PROTOCOL/" + objQMS050.ProtocolType + "/Linkage/" + objQMS050.ProtocolId;
                    ViewBag.ProtocolTypeDescription = GetCategoryDescriptionOnly(Manager.GetSubCatagories("Protocol Type", objQMS050.BU, objQMS050.Location, null), objQMS050.ProtocolType);
                }
                else
                {
                    QMS031 objQMS031 = db.QMS031.FirstOrDefault(c => c.Project == objQMS050.Project && c.QualityProject == objQMS050.QualityProject && c.BU == objQMS050.BU && c.Location == objQMS050.Location && c.SeamNo == objQMS050.SeamNo && c.StageCode == objQMS050.StageCode && c.StageSequance == objQMS050.StageSequence);
                    if (objQMS031 != null)
                    {
                        ViewBag.ProtocolURL = "OpenAttachmentPopUp(" + objQMS031.LineId + ",false,'QMS031/" + objQMS031.LineId + "');";
                        ViewBag.ProtocolTypeDescription = "";
                    }
                }
            }

            objQMS050.Project = Manager.GetProjectAndDescription(objQMS050.Project);
            ViewBag.StageCode = Manager.GetStageCodeDescriptionFromCode(objQMS050.StageCode, objQMS050.BU, objQMS050.Location);
            List<GLB002> lstCategory = Manager.GetSubCatagories("TPI Agency", objQMS050.BU, objQMS050.Location, null);
            List<GLB002> lstCategoryTPI = Manager.GetSubCatagories("TPI Intervention", objQMS050.BU, objQMS050.Location, null);
            objQMS050.TPIAgency1 = GetCategoryDescriptionFromList(lstCategory, objQMS050.TPIAgency1);
            objQMS050.TPIAgency2 = GetCategoryDescriptionFromList(lstCategory, objQMS050.TPIAgency2);
            objQMS050.TPIAgency3 = GetCategoryDescriptionFromList(lstCategory, objQMS050.TPIAgency3);
            objQMS050.TPIAgency4 = GetCategoryDescriptionFromList(lstCategory, objQMS050.TPIAgency4);
            objQMS050.TPIAgency5 = GetCategoryDescriptionFromList(lstCategory, objQMS050.TPIAgency5);
            objQMS050.TPIAgency6 = GetCategoryDescriptionFromList(lstCategory, objQMS050.TPIAgency6);
            objQMS050.TPIAgency1Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS050.TPIAgency1Intervention);
            objQMS050.TPIAgency2Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS050.TPIAgency2Intervention);
            objQMS050.TPIAgency3Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS050.TPIAgency3Intervention);
            objQMS050.TPIAgency4Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS050.TPIAgency4Intervention);
            objQMS050.TPIAgency5Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS050.TPIAgency5Intervention);
            objQMS050.TPIAgency6Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS050.TPIAgency6Intervention);
            objQMS050.CreatedBy = Manager.GetPsidandDescription(objQMS050.CreatedBy);
            objQMS050.OfferedBy = Manager.GetPsidandDescription(objQMS050.OfferedBy);
            objQMS050.InspectedBy = Manager.GetPsidandDescription(objQMS050.InspectedBy);
            objQMS050.OfferedtoCustomerBy = Manager.GetPsidandDescription(objQMS050.OfferedtoCustomerBy);
            objQMS050.Location = db.COM002.Where(x => x.t_dimx == objQMS050.Location).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
            objQMS050.BU = db.COM002.Where(x => x.t_dimx == objQMS050.BU).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
            objQMS050.TestResult = string.IsNullOrWhiteSpace(objQMS050.TestResult) ? "Pending" : objQMS050.TestResult;
            ViewBag.remark = GetPreviousRequestRemark(objQMS050.RequestNo, objQMS050.RequestId);

            return View(objQMS050);
        }
        public ActionResult loadTPIOfferDataTable(JQueryDataTableParamModel param, string status)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and  OfferedtoCustomerBy IS NOT NULL and TPIAgency1Result IS NULL and TPIAgency2Result IS NULL and TPIAgency3Result IS NULL and TPIAgency4Result IS NULL and TPIAgency5Result IS NULL and TPIAgency6Result IS NULL";
                }
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms.BU", "qms.Location");
                string[] columnName = { "qms.SeamNo", "dbo.GET_PROJECTDESC_BY_CODE(qms.Project)", "qms.OfferedBy", "qms.CreatedBy", "qms.Location", "dbo.GET_IPI_STAGECODE_DESCRIPTION(qms.StageCode, qms.BU, qms.Location) ", "qms.RequestNo", "qms.InspectedBy", "qms.QCRemarks", "qms.OfferedtoCustomerBy", "qms.Shop", "qms.ShopLocation", "qms.ShopRemark" };
                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = string.Empty; ;
                sortDirection = Convert.ToString(Request["sSortDir_0"]); // asc or desc
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstHeader = db.SP_IPI_SEAMLISTOFFERINSPECTION_DETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                var res = from h in lstHeader
                          select new[] {
                           h.QualityProject,
                           Convert.ToString(h.Project),
                           Convert.ToString(h.RequestId),
                           Convert.ToString(h.HeaderId),
                           Convert.ToString( h.SeamNo),
                           Convert.ToString(h.StageCode),
                           Convert.ToString(h.StageSequence),
                           Convert.ToString(h.IterationNo),
                           Convert.ToString(h.RequestNo),
                           Convert.ToString(h.RequestNoSequence),
                           Convert.ToString(h.TestResult),
                           Convert.ToString(h.OfferedBy),
                           h.OfferedOn != null ? Convert.ToDateTime(h.OfferedOn).ToString("dd/MM/yyyy") : "",
                           //Convert.ToString( h.CreatedBy),
                           //Convert.ToDateTime(h.CreatedOn).ToString("dd/MM/yyyy"),
                           Convert.ToString( h.Location),
                           Convert.ToString(h.BU),
                           Convert.ToString(h.InspectedBy),
                           h.InspectedOn != null ? Convert.ToDateTime(h.InspectedOn).ToString("dd/MM/yyyy") : "",
                           Convert.ToString(h.QCRemarks),
                           Convert.ToString(h.OfferedtoCustomerBy),
                           h.OfferedtoCustomerOn != null ? Convert.ToDateTime(h.OfferedtoCustomerOn).ToString("dd/MM/yyyy") : "",
                           Convert.ToString(h.Shop),
                           Convert.ToString(h.ShopLocation),
                           Convert.ToString(h.ShopRemark),
                          "<center style=\"white-space: nowrap;\" class=\"clsDisplayInlineEle\"><a   href=\"" + WebsiteURL + "/IPI/MaintainSeamListOfferInspection/TPITestDetails/"+h.RequestId+"\" Title=\"View Details\" ><i style=\"margin - left:5px;\" class=\"fa fa-eye\"></i></a>&nbsp;"+Manager.generateIPIGridButtons(h.QualityProject,h.Project,h.BU,h.Location,h.SeamNo,"","",h.StageCode,Convert.ToString(h.StageSequence),"Seam Details",50,"true","Seam Details","Seam Request Details")
                          + (h.ProtocolId.HasValue ? "&nbsp;&nbsp;<a target=\"_blank\"  href=\"" + WebsiteURL + "/PROTOCOL/"+h.ProtocolType+"/Linkage/"+h.ProtocolId +"\" Title=\"View Protocol Details\" ><i style=\"margin - left:5px;\" class=\"fa fa-file-text\"></i></a>" : "&nbsp;&nbsp;<a Title=\"No Protocol Attached\" ><i style=\"opacity:0.3;\" class=\"fa fa-file-text\"></i></a>")
                          +"&nbsp;&nbsp;<a  href=\"javascript: void(0);\" Title=\"View Timeline\" onclick=\"ShowTimeline("+ h.RequestId +")\"><i style=\"margin - left:5px;\" class=\"fa fa-clock-o\"></i></a></center>"

                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition

                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public string GetPreviousRequestRemark(long? requestNo, int requestId)
        {
            string strReturned = clsImplementationEnum.SeamListInspectionStatus.Returned.GetStringValue();
            string strRejected = clsImplementationEnum.SeamListInspectionStatus.Rejected.GetStringValue();
            return db.QMS050.Where(x => (x.RequestNo == requestNo && x.RequestId < requestId) && (x.TestResult == strReturned || x.TestResult == strRejected)).OrderByDescending(x => x.RequestId).Select(x => x.QCRemarks).FirstOrDefault();
        }

        [HttpPost]
        public ActionResult LTFPSDocRevNoValidation(int requestId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            objResponseMsg.Key = false;
            QMS050 objQMS050 = db.QMS050.FirstOrDefault(x => x.RequestId == requestId);
            if (Manager.IsLTFPSProject(objQMS050.Project, objQMS050.BU, objQMS050.Location) && !string.IsNullOrWhiteSpace(objQMS050.LTF002.RefDocument))
            {
                objResponseMsg.Key = true;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RespondTPITestDetailRequest(int requestId, string type)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS050 objQMS050 = db.QMS050.FirstOrDefault(x => x.RequestId == requestId);
                db.SP_IPI_SEAMTESTTPIRESPOND(requestId, type, objClsLoginInfo.UserName);

                string NewfolderPath = "";
                string OldfolderPath = "";

                if (type == clsImplementationEnum.SeamListInspectionStatus.Returned.GetStringValue())
                {
                    int iTableId = 0;
                    if (objQMS050.LTFPSHeaderId > 0)
                    {
                        OldfolderPath = "QMS050_LTFPS//" + objQMS050.LTFPSHeaderId.Value + "//" + objQMS050.IterationNo + "//" + objQMS050.RequestNoSequence;
                        NewfolderPath = "QMS050_LTFPS//" + objQMS050.LTFPSHeaderId.Value + "//" + objQMS050.IterationNo + "//" + (objQMS050.RequestNoSequence + 1);
                        iTableId = Convert.ToInt32(objQMS050.LTFPSHeaderId);
                    }
                    else
                    {
                        OldfolderPath = "QMS050//" + objQMS050.HeaderId.Value + "//" + objQMS050.IterationNo + "//" + objQMS050.RequestNoSequence;
                        NewfolderPath = "QMS050//" + objQMS050.HeaderId + "//" + objQMS050.IterationNo + "//" + (objQMS050.RequestNoSequence + 1);
                        iTableId = Convert.ToInt32(objQMS050.HeaderId);
                    }
                    //(new clsFileUpload()).CopyFolderContentsAsync(OldfolderPath, NewfolderPath);
                    Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                    _objFUC.CopyDataOnFCSServerAsync(OldfolderPath, NewfolderPath, OldfolderPath, iTableId, NewfolderPath, iTableId, DESServices.CommonService.GetUseIPConfig, DESServices.CommonService.objClsLoginInfo.UserName, 0, false);
                }
                var objQMS030 = db.QMS030.Where(z => z.QualityProject == objQMS050.QualityProject && z.Project == objQMS050.Project && z.BU == objQMS050.BU && z.Location == objQMS050.Location && z.SeamNo == objQMS050.SeamNo).FirstOrDefault();
                if (objQMS030 != null)
                {
                    Manager.ReadyToOffer_SEAM(objQMS030);
                }

                #region Code for make ready to offer parent part
                //if (type == clsImplementationEnum.SeamListInspectionStatus.Cleared.GetStringValue() && !Manager.IsLTFPSProject(objQMS050.Project, objQMS050.BU, objQMS050.Location))
                //{
                //    string ApprovedStatus = clsImplementationEnum.ICLPartStatus.Approved.GetStringValue();
                //    if (Manager.IsSEAMFullyCleared(objQMS050.Project, objQMS050.QualityProject, objQMS050.BU, objQMS050.Location, objQMS050.SeamNo))
                //    {
                //        QMS012 objQMS012 = db.QMS012.FirstOrDefault(c => c.Project == objQMS050.Project && c.QualityProject == objQMS050.QualityProject && c.SeamNo == objQMS050.SeamNo);
                //        QMS035 objQMS035 = db.QMS035.FirstOrDefault(c => c.Project == objQMS050.Project && c.QualityProject == objQMS050.QualityProject && c.BU == objQMS050.BU && c.Location == objQMS050.Location && c.ChildPartNo == objQMS012.AssemblyNo && c.Status == ApprovedStatus);
                //        if (objQMS035 != null)
                //        {
                //            Manager.ReadyToOffer_PART(objQMS035);
                //        }
                //    }
                //}
                #endregion

                #region Send Notification
                if (objQMS050 != null)
                {
                    if (type.ToLower() == clsImplementationEnum.SeamListInspectionStatus.Cleared.GetStringValue().ToLower())
                    {
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.QC3.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PROD3.GetStringValue(), objQMS050.Project, objQMS050.BU, objQMS050.Location, "Stage: " + objQMS050.StageCode + " of Seam: " + objQMS050.SeamNo + " & Project No: " + objQMS050.QualityProject + " has been Cleared by TPI", clsImplementationEnum.NotificationType.Information.GetStringValue());
                    }
                    else if (type.ToLower() == clsImplementationEnum.SeamListInspectionStatus.Returned.GetStringValue().ToLower())
                    {
                        QMS050 objQMS050Noti = db.QMS050.Where(x => x.RequestNo == objQMS050.RequestNo && x.HeaderId == objQMS050.HeaderId && x.TestResult == null).OrderByDescending(o => o.IterationNo).FirstOrDefault();
                        if (objQMS050Noti != null)
                        {
                            (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), objQMS050.Project, objQMS050.BU, objQMS050.Location, "Stage: " + objQMS050.StageCode + " of Seam: " + objQMS050.SeamNo + " & Project No: " + objQMS050.QualityProject + " has been Returned by TPI", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/MaintainSeamListOfferInspection/ApproveSeamListInspection/" + objQMS050Noti.RequestId);
                        }
                    }
                }
                #endregion

                objResponseMsg.Key = true;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error Occured, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult LTFPSClear(int requestId, string docRevNo)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS050 objQMS050 = db.QMS050.Where(x => x.RequestId == requestId).FirstOrDefault();
                string strCleared = clsImplementationEnum.SeamListInspectionStatus.Cleared.GetStringValue();
                objQMS050.TestResult = strCleared;
                //TPI Agency1 Update
                if (!string.IsNullOrWhiteSpace(objQMS050.TPIAgency1))
                {
                    objQMS050.TPIAgency1Result = strCleared;
                    objQMS050.TPIAgency1ResultOn = DateTime.Now;
                    objQMS050.EditedBy = objClsLoginInfo.UserName;
                    objQMS050.EditedOn = DateTime.Now;
                }
                //TPI Agency2 Update
                if (!string.IsNullOrWhiteSpace(objQMS050.TPIAgency2))
                {
                    objQMS050.TPIAgency2Result = strCleared;
                    objQMS050.TPIAgency2ResultOn = DateTime.Now;
                    objQMS050.EditedBy = objClsLoginInfo.UserName;
                    objQMS050.EditedOn = DateTime.Now;
                }
                //TPI Agency3 Update
                if (!string.IsNullOrWhiteSpace(objQMS050.TPIAgency3))
                {
                    objQMS050.TPIAgency3Result = strCleared;
                    objQMS050.TPIAgency3ResultOn = DateTime.Now;
                    objQMS050.EditedBy = objClsLoginInfo.UserName;
                    objQMS050.EditedOn = DateTime.Now;
                }
                //TPI Agency4 Update
                if (!string.IsNullOrWhiteSpace(objQMS050.TPIAgency4))
                {
                    objQMS050.TPIAgency4Result = strCleared;
                    objQMS050.TPIAgency4ResultOn = DateTime.Now;
                    objQMS050.EditedBy = objClsLoginInfo.UserName;
                    objQMS050.EditedOn = DateTime.Now;
                }
                //TPI Agency5 Update
                if (!string.IsNullOrWhiteSpace(objQMS050.TPIAgency5))
                {
                    objQMS050.TPIAgency5Result = strCleared;
                    objQMS050.TPIAgency5ResultOn = DateTime.Now;
                    objQMS050.EditedBy = objClsLoginInfo.UserName;
                    objQMS050.EditedOn = DateTime.Now;
                }
                //TPI Agency6 Update
                if (!string.IsNullOrWhiteSpace(objQMS050.TPIAgency6))
                {
                    objQMS050.TPIAgency6Result = strCleared;
                    objQMS050.TPIAgency6ResultOn = DateTime.Now;
                    objQMS050.EditedBy = objClsLoginInfo.UserName;
                    objQMS050.EditedOn = DateTime.Now;
                }
                objQMS050.LTF002.LNTStatus = strCleared;
                objQMS050.LTF002.InspectionStatus = strCleared;
                objQMS050.LTF002.LNTInspectorResultOn = DateTime.Now;
                db.SaveChanges();
                string result = db.SP_LTFPS_RELEASE_READY_TO_OFFER(objQMS050.LTF002.HeaderId).FirstOrDefault();

                if (objQMS050.LTF002.DocumentRequired && !string.IsNullOrWhiteSpace(objQMS050.LTF002.RefDocument) && !string.IsNullOrWhiteSpace(docRevNo))
                {
                    LTF002 objLTF002 = db.LTF002.FirstOrDefault(x => x.LineId == objQMS050.LTFPSHeaderId);
                    objLTF002.DocRevNo = docRevNo;
                    db.SaveChanges();
                }

                #region Send Notification
                (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.QC3.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PROD3.GetStringValue(), objQMS050.Project, objQMS050.BU, objQMS050.Location, "Stage: " + objQMS050.StageCode + " of Seam: " + objQMS050.SeamNo + " & Project No: " + objQMS050.QualityProject + " has been Cleared by TPI", clsImplementationEnum.NotificationType.Information.GetStringValue());
                #endregion
                objResponseMsg.Key = true;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error Occured, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadFilteredNDEReqData(JQueryDataTableParamModel param, string qualityProject, string project, string bu, string location, string SeamNo, string stageCode, int stageSeq)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[0]) + "," + Convert.ToString(Request["sColumns"].Split(',')[1]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                strWhere = "1=1";
                strWhere += string.Format(" and QualityProject in('{0}') and qms.Project in('{1}') and qms.BU in('{2}') and qms.Location in('{3}') and qms.SeamNo in('{4}') and qms.StageCode in('{5}') and qms.StageSequence in({6})", qualityProject, project, bu, location, SeamNo, stageCode, stageSeq);


                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = { "qms.SeamNo", "dbo.GET_PROJECTDESC_BY_CODE(qms.Project)", "qms.OfferedBy", "qms.CreatedBy", "qms.Location", "dbo.GET_IPI_STAGECODE_DESCRIPTION(qms.StageCode, qms.BU, qms.Location) ", "qms.RequestNo", "qms.InspectedBy", "qms.QCRemarks", "qms.OfferedtoCustomerBy", "qms.Shop", "qms.ShopLocation", "qms.ShopRemark" };
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                var lstResult = db.SP_IPI_SEAMLISTNDETEST_DETAILS(StartIndex, EndIndex, "", strWhere).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               GenerateReportLink(uc.RequestId, uc.RequestNo.ToString()),
                               //Convert.ToString(uc.RequestNo),
                               Convert.ToString(uc.RequestNoSequence),
                               GenerateReportLink(uc.RequestId, uc.IterationNo.ToString(), false, true),
                               //Convert.ToString(uc.IterationNo),
                               Convert.ToString(uc.RequestStatus),
                               Convert.ToString(uc.ManufacturingCode),
                               Convert.ToString(uc.Shop),
                               Convert.ToString(uc.ShopLocation),
                               Convert.ToString(uc.ShopRemark),
                               Convert.ToString(uc.NDETechniqueNo),
                               Convert.ToString(uc.NDETechniqueRevisionNo),
                               Convert.ToString(uc.SeamLength),
                               Convert.ToString(uc.OverlayArea),
                               Convert.ToString(uc.OverlayThickness),
                               Convert.ToString(uc.Jointtype),
                               Convert.ToString(uc.SeamStatus),
                               Convert.ToString(uc.SurfaceCondition),
                               Convert.ToString(uc.WeldThicknessReinforcement),
                               Convert.ToString(uc.SpotGenerationtype),
                               Convert.ToString(uc.SpotLength),
                               Convert.ToString(uc.Part1Position),
                               Convert.ToString(uc.Part1thickness),
                               Convert.ToString(uc.Part1Material),
                               Convert.ToString(uc.Part2Position),
                               Convert.ToString(uc.Part2thickness),
                               Convert.ToString(uc.Part2Material),
                               Convert.ToString(uc.Part3Position),
                               Convert.ToString(uc.Part3thickness),
                               Convert.ToString(uc.Part3Material),
                               Convert.ToString(uc.AcceptanceStandard),
                               Convert.ToString(uc.ApplicableSpecification),
                               Convert.ToString(uc.InspectionExtent),
                               Convert.ToString(uc.WeldingEngineeringRemarks),
                               Convert.ToString(uc.TPIAgency1),
                               Convert.ToString(uc.TPIAgency1Intervention),
                               Convert.ToString(uc.TPIAgency2),
                               Convert.ToString(uc.TPIAgency2Intervention),
                               Convert.ToString(uc.TPIAgency3),
                               Convert.ToString(uc.TPIAgency3Intervention),
                               Convert.ToString(uc.TPIAgency4),
                               Convert.ToString(uc.TPIAgency4Intervention),
                               Convert.ToString(uc.TPIAgency5),
                               Convert.ToString(uc.TPIAgency5Intervention),
                               Convert.ToString(uc.TPIAgency6),
                               Convert.ToString(uc.TPIAgency6Intervention),
                               Convert.ToString(uc.Welder1),
                               Convert.ToString(uc.Welder2),
                               Convert.ToString(uc.Welder3),
                               Convert.ToString(uc.Welder4),
                               Convert.ToString(uc.Welder5),
                               Convert.ToString(uc.Welder6),
                               Convert.ToString(uc.Welder7),
                               Convert.ToString(uc.Welder8),
                               Convert.ToString(uc.Welder9),
                               Convert.ToString(uc.Welder10),
                               Convert.ToString(uc.Welder11),
                               Convert.ToString(uc.Welder12),
                               Convert.ToString(uc.Welder13),
                                Convert.ToString(uc.Welder14),
                                Convert.ToString(uc.Welder15),
                                Convert.ToString(uc.Welder16),
                                Convert.ToString(uc.Welder17),
                                Convert.ToString(uc.Welder18),
                                Convert.ToString(uc.Welder19),
                                Convert.ToString(uc.Welder20),
                                Convert.ToString(uc.Welder21),
                                Convert.ToString(uc.Welder22),
                                Convert.ToString(uc.Welder23),
                                Convert.ToString(uc.Welder24),
                               Convert.ToString(uc.WeldingProcess),
                               Convert.ToString(uc.PrintSummary),
                               Convert.ToString(uc.OfferedBy),
                               Convert.ToString(uc.OfferedOn),
                               Convert.ToString(uc.RequestGeneratedBy),
                               Convert.ToString(uc.RequestGeneratedOn),
                               Convert.ToString(uc.ConfirmedBy),
                               Convert.ToString(uc.ConfirmedOn),
                               Convert.ToString(uc.InspectedBy),
                               Convert.ToString(uc.InspectedOn),
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = 0,
                    iTotalDisplayRecords = 0,
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string GenerateReportLink(int RequestId, string LinkTitle, bool isRequestNo = true, bool isIteration = false)
        {
            string ActionHTML = string.Empty;
            string ReportType = string.Empty;

            QMS060 objQMS060 = db.QMS060.FirstOrDefault(c => c.RequestId == RequestId);

            if (objQMS060.RequestStatus == clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue() && objQMS060 != null && !string.IsNullOrWhiteSpace(objQMS060.NDETechniqueNo) && objQMS060.NDETechniqueRevisionNo.HasValue)
            {
                QMS002 objQMS002 = db.QMS002.FirstOrDefault(c => c.BU == objQMS060.BU && c.Location == objQMS060.Location && c.StageCode == objQMS060.StageCode);
                if (objQMS002 != null)
                {
                    if (objQMS002.StageType == "RT" || objQMS002.StageType == "UT")
                    {
                        if (objQMS002.StageType == "RT")
                        {
                            ReportType = "RT";
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(Convert.ToString(objQMS060.NDETechniqueNo)))
                                ReportType = objQMS060.NDETechniqueNo.Split('/')[1].ToString();
                        }
                        if (!string.IsNullOrWhiteSpace(ReportType))
                        {
                            if (isRequestNo)
                            {
                                ActionHTML = "onclick=\"OpenNDEReport('" + ReportType + "','" + objQMS060.QualityProject + "','" + objQMS002.StageType + "','" + objQMS060.SeamNo.Replace("\\", "\\\\") + "','" + objQMS060.StageCode + "','" + objQMS060.StageSequence + "','','" + objQMS060.RequestNo + "')\"";
                            }
                            else if (isIteration)
                            {
                                ActionHTML = "onclick=\"OpenNDEReport('" + ReportType + "','" + objQMS060.QualityProject + "','" + objQMS002.StageType + "','" + objQMS060.SeamNo.Replace("\\", "\\\\") + "','" + objQMS060.StageCode + "','" + objQMS060.StageSequence + "','" + objQMS060.IterationNo + "','" + objQMS060.RequestNo + "')\"";
                            }
                        }
                    }
                }
            }
            else
            {
                ActionHTML = "onclick=\"OpenNDERequestDetails(" + RequestId + ")\"";
            }



            if (!string.IsNullOrWhiteSpace(ActionHTML))
            {
                return "<a " + ActionHTML + " Title=\"View Report\" >" + LinkTitle + "</a>";
            }
            else
            {
                return LinkTitle;
            }

        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    List<SP_IPI_SEAMLISTOFFERINSPECTION_Result> lst = db.SP_IPI_SEAMLISTOFFERINSPECTION(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      SeamNo = li.SeamNo,
                                      StageCode = li.StageCode,
                                      StageCodeDescription = li.StageCodeDescription,
                                      StageSequence = li.StageSequence,
                                      IterationNo = li.IterationNo,
                                      InspectionStatus = li.InspectionStatus,
                                      LNTInspectorResult = li.LNTInspectorResult,
                                      Remarks = li.Remarks,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else
                {
                    List<SP_IPI_SEAMLISTOFFERINSPECTION_DETAILS_Result> lst = db.SP_IPI_SEAMLISTOFFERINSPECTION_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      SeamNo = li.SeamNo,
                                      StageCode = li.StageCode,
                                      StageSequence = li.StageSequence,
                                      IterationNo = li.IterationNo,
                                      RequestNo = li.RequestNo,
                                      RequestNoSequence = li.RequestNoSequence,
                                      TestResult = li.TestResult,
                                      InspectedBy = li.InspectedBy,
                                      InspectedOn = li.InspectedOn,
                                      QCRemarks = li.QCRemarks,
                                      OfferedtoCustomerBy = li.OfferedtoCustomerBy,
                                      OfferedtoCustomerOn = li.OfferedtoCustomerOn,
                                      TPIAgency1 = li.TPIAgency1,
                                      TPIAgency1Intervention = li.TPIAgency1Intervention,
                                      TPIAgency1Result = li.TPIAgency1Result,
                                      TPIAgency1ResultOn = li.TPIAgency1ResultOn,
                                      TPIAgency2 = li.TPIAgency2,
                                      TPIAgency2Intervention = li.TPIAgency2Intervention,
                                      TPIAgency2Result = li.TPIAgency2Result,
                                      TPIAgency2ResultOn = li.TPIAgency2ResultOn,
                                      TPIAgency3 = li.TPIAgency3,
                                      TPIAgency3Intervention = li.TPIAgency3Intervention,
                                      TPIAgency3Result = li.TPIAgency3Result,
                                      TPIAgency3ResultOn = li.TPIAgency3ResultOn,
                                      TPIAgency4 = li.TPIAgency4,
                                      TPIAgency4Intervention = li.TPIAgency4Intervention,
                                      TPIAgency4Result = li.TPIAgency4Result,
                                      TPIAgency4ResultOn = li.TPIAgency4ResultOn,
                                      TPIAgency5 = li.TPIAgency5,
                                      TPIAgency5Intervention = li.TPIAgency5Intervention,
                                      TPIAgency5Result = li.TPIAgency5Result,
                                      TPIAgency5ResultOn = li.TPIAgency5ResultOn,
                                      TPIAgency6 = li.TPIAgency6,
                                      TPIAgency6Intervention = li.TPIAgency6Intervention,
                                      TPIAgency6Result = li.TPIAgency6Result,
                                      TPIAgency6ResultOn = li.TPIAgency6ResultOn,
                                      Shop = li.Shop,
                                      ShopLocation = li.ShopLocation,
                                      ShopRemark = li.ShopRemark,
                                      OfferedBy = li.OfferedBy,
                                      OfferedOn = li.OfferedOn,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// This function is to just seam no. for particular qualityProject
        /// Created By Darshan Dave 08/12/2018
        /// </summary>
        /// <param name="qualityProject"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetSeamNoByQualityProject(string term, string qualityProject)
        {
            try
            {
                var data = db.QMS040.Where(x => x.QualityProject.Equals(qualityProject) && x.SeamNo.Contains(term)).Select(x => x.SeamNo).OrderBy(x => x).Distinct().ToList();
                return Json(new { Data = data });
            }
            catch (Exception ex)
            {
                return Json(new { Error = ex.Message });
            }
        }

        [HttpPost]
        public ActionResult nextprevbyProjectorseam(string seam, string qualityProject, string option)
        {
            ResponsenextorPre objresponse = new ResponsenextorPre();
            List<Projects> lstprojects = listqualityprojects();
            List<seamlist> lstseams = null;
            string forall = "ALL";
            int projindexno = 0;
            int seamindexno = 0;
            int minseam = 0;
            int maxseam = 0;
            int minproj = 0;
            int maxproj = 0;

            try
            {
                if (!string.IsNullOrEmpty(qualityProject) && !string.IsNullOrEmpty(seam))
                {
                    projindexno = lstprojects.Select((item, index) => new { index, item }).Where(x => x.item.projectCode.ToLower() == qualityProject.ToLower()).Select(x => x.index).FirstOrDefault();
                    minproj = lstprojects.Select((item, index) => new { index, item }).Min(x => x.index);
                    maxproj = lstprojects.Select((item, index) => new { index, item }).Max(x => x.index);

                    if (!string.IsNullOrEmpty(option))
                    {
                        switch (option)
                        {
                            case "PrevProject":
                                ViewBag.Title = "PrevProject";
                                projindexno = (projindexno - 1);
                                objresponse.projprevbutton = (projindexno == minproj) ? true : ((lstprojects.Select((item, index) => new { index, item }).Any(x => x.index == projindexno)) ? false : true);
                                objresponse.projnextbutton = (lstprojects.Select((item, index) => new { index, item }).Any(x => x.index == (projindexno + 1))) ? false : true;
                                qualityProject = mainqualityProject(projindexno);
                                break;
                            case "NextProject":
                                ViewBag.Title = "NextProject";
                                projindexno = (projindexno + 1);
                                objresponse.projprevbutton = (lstprojects.Select((item, index) => new { index, item }).Any(x => x.index == (projindexno - 1))) ? false : true;
                                objresponse.projnextbutton = (projindexno == maxproj) ? true : ((lstprojects.Select((item, index) => new { index, item }).Any(x => x.index == projindexno)) ? false : true);
                                qualityProject = mainqualityProject(projindexno);
                                break;
                            default:
                                ViewBag.Title = "NextSeam";
                                break;
                        }
                    }
                    else
                    {
                        objresponse.projprevbutton = (lstprojects.Select((item, index) => new { index, item }).Any(x => x.index == (projindexno - 1))) ? false : true;
                        objresponse.projnextbutton = (lstprojects.Select((item, index) => new { index, item }).Any(x => x.index == (projindexno + 1))) ? false : true;
                    }

                    lstseams = listseamwiseproject(qualityProject);

                    if (lstseams.Select((item, index) => new { index, item }).Any(x => x.item.SeamNo.ToLower() == seam.ToLower()))
                    {
                        seamindexno = lstseams.Select((item, index) => new { index, item }).Where(x => x.item.SeamNo.ToLower() == seam.ToLower()).Select(x => x.index).FirstOrDefault();
                        minseam = lstseams.Select((item, index) => new { index, item }).Min(x => x.index);
                        maxseam = lstseams.Select((item, index) => new { index, item }).Max(x => x.index);
                    }
                    else
                        seam = forall;

                    if (seam.ToLower() == forall.ToLower())
                    {
                        objresponse.seamprevbutton = true;
                        objresponse.seamnextbutton = true;
                    }
                    else
                    {
                        objresponse.seamprevbutton = (lstseams.Select((item, Index) => new { Index, item }).Any(x => x.Index == (seamindexno - 1))) ? false : true;
                        objresponse.seamnextbutton = (lstseams.Select((item, Index) => new { Index, item }).Any(x => x.Index == (seamindexno + 1))) ? false : true;
                    }


                    switch (option)
                    {
                        case "PrevSeam":
                            ViewBag.Title = "PrevSeam";
                            seamindexno = (seamindexno - 1);
                            objresponse.seamprevbutton = (seamindexno == minseam) ? true : ((lstseams.Select((item, index) => new { index, item }).Any(x => x.index == seamindexno)) ? false : true);
                            objresponse.seamnextbutton = (lstseams.Select((item, index) => new { index, item }).Any(x => x.index == (seamindexno + 1))) ? false : true;
                            seam = lstseams.Select((item, index) => new { index, item }).Where(x => x.index == seamindexno).FirstOrDefault().item.SeamNo;
                            break;
                        case "NextSeam":
                            ViewBag.Title = "NextSeam";
                            seamindexno = (seamindexno + 1);
                            objresponse.seamprevbutton = (lstseams.Select((item, index) => new { index, item }).Any(x => x.index == (seamindexno - 1))) ? false : true;
                            objresponse.seamnextbutton = (seamindexno == maxseam) ? true : ((lstseams.Select((item, index) => new { index, item }).Any(x => x.index == seamindexno)) ? false : true);
                            seam = lstseams.Select((item, index) => new { index, item }).Where(x => x.index == seamindexno).FirstOrDefault().item.SeamNo;
                            break;
                        default:
                            ViewBag.Title = "NextSeam";
                            break;
                    }

                    objresponse.project = qualityProject;
                    objresponse.seam = seam;
                    objresponse.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objresponse.Key = false;
                objresponse.Value = "Error Occures Please try again";
            }
            return Json(objresponse, JsonRequestBehavior.AllowGet);
        }

        public List<Projects> listqualityprojects()
        {
            string username = objClsLoginInfo.UserName;
            var lstATHLocation = db.ATH001.Where(x => x.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(x => x.Location).Distinct().ToList();
            var lstCOMLocation = db.COM003.Where(i => i.t_psno.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_loca).Distinct().ToList();

            var lstCOMBU = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.BU).Distinct().ToList();

            lstCOMLocation = lstATHLocation.Union(lstCOMLocation).ToList();

            List<Projects> lstQualityProject = new List<Projects>();

            lstQualityProject = db.QMS010.Where(i => lstCOMLocation.Contains(i.Location)
                                                    && lstCOMBU.Contains(i.BU)
                                                    ).Select(i => new Projects { Value = i.QualityProject, projectCode = i.QualityProject }).OrderBy(x => x.projectCode).Distinct().ToList();
            return lstQualityProject;
        }

        public List<seamlist> listseamwiseproject(string qualityProject)
        {
            List<seamlist> lstseam = new List<seamlist>();
            lstseam = db.QMS040.Where(x => x.QualityProject.Equals(qualityProject)).Select(x => new seamlist { SeamNo = x.SeamNo }).OrderBy(x => x.SeamNo).Distinct().ToList();
            return lstseam;
        }

        public List<stageType> liststagewiseproject(string qualityProject)
        {
            List<stageType> lstseam = new List<stageType>();

            var Query = string.Format("select Distinct Qms002.StageType Type from Qms002 where Qms002.StageCode in (select QMS050.StageCode from QMS050 where QMS050.QualityProject = '{0}')", qualityProject);
            lstseam = db.Database.SqlQuery<stageType>(Query).Select(x => new stageType { Type = x.Type }).OrderBy(x => x.Type).Distinct().ToList();
            // lstseam = db.QMS050.Where(x => x.QualityProject.Equals(qualityProject)).Select(x => new seamlist { SeamNo = x.StageCode }).OrderBy(x => x.SeamNo).Distinct().ToList();
            return lstseam;
        }

        public List<seamlist> liststageCodewiseproject(string qualityProject, string StageCodeType)
        {
            List<seamlist> lstseam = new List<seamlist>();

            var Query = string.Format("select  Distinct Qms002.StageCode SeamNo from Qms002 where Qms002.StageType = '" + StageCodeType + "' and  Qms002.StageCode in (select QMS050.StageCode from QMS050 where QMS050.QualityProject = '{0}')", qualityProject);
            lstseam = db.Database.SqlQuery<seamlist>(Query).Select(x => new seamlist { SeamNo = x.SeamNo }).OrderBy(x => x.SeamNo).ToList();

            // lstseam = db.Database.SqlQuery<QMS002>(Query).Select(x => new seamlist { SeamNo = x.StageType }).OrderBy(x => x.SeamNo).Distinct().ToList();
            // lstseam = db.QMS050.Where(x => x.QualityProject.Equals(qualityProject)).Select(x => new seamlist { SeamNo = x.StageCode }).OrderBy(x => x.SeamNo).Distinct().ToList();
            return lstseam;
        }

        public string mainqualityProject(int indexno = 0)
        {
            string project = string.Empty;
            List<Projects> lstprojects = listqualityprojects();
            project = lstprojects.Select((item, index) => new { index, item }).Where(x => x.index == indexno).FirstOrDefault().item.projectCode;
            return project;
        }

        [HttpPost]
        public JsonResult loadPendingInspectionProject(string whereCondition, string strSortOrder)
        {
            List<ddlValue> lstProject = new List<ddlValue>();
            try
            {
                List<SP_IPI_SEAMLISTOFFERINSPECTION_DETAILS_Result> lst = db.SP_IPI_SEAMLISTOFFERINSPECTION_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                lstProject = lst.Select(x => x.QualityProject).Distinct().Select(x => new ddlValue { id = x, text = x }).Distinct().ToList();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(lstProject, JsonRequestBehavior.AllowGet);
        }

        #region Get Pending Inspection Data
        [HttpPost]
        public ActionResult GetPendingInspectionData(string project, string qualityProject, string bu, string location, string seamno)
        {
            var lstResult = db.SP_IPI_GET_PENDING_INSPECTION_DATA_FOR_SEAM(project, qualityProject, bu, location, seamno).ToList();
            List<ReturnedData> lstSeam = lstResult.Where(x => x.Type.ToLower() == "seam").Select(x => new ReturnedData { Description = x.Name + " - " + x.Description }).ToList();
            List<CategoryData> lstPart = lstResult.Where(x => x.Type.ToLower() == "part").Select(x => new CategoryData { Description = x.Name + " - " + x.Description }).ToList();
            ViewBag.Seam = lstSeam;
            ViewBag.Part = lstPart;
            ViewBag.InspectionType = "SEAM";

            return PartialView("~/Areas/IPI/Views/PartlistOfferInspection/GetPendingInspectionDataPartial.cshtml");
        }
        #endregion

        #region Maintain Hardness Test Details

        public ActionResult LoadHardnessTestDetailsPartial(int HeaderId, int RequestId, string IterationNo, bool IsEditable)
        {
            QMS050 objQMS050 = new QMS050();

            if (RequestId > 0)
            {
                objQMS050 = db.QMS050.Where(x => x.RequestId == RequestId).FirstOrDefault();
            }
            ViewBag.Stage = db.QMS002.Where(x => x.StageCode == objQMS050.StageCode && x.BU == objQMS050.BU && x.Location == objQMS050.Location).Select(x => x.StageCode + "-" + x.StageDesc).FirstOrDefault();
            ViewBag.HeaderId = HeaderId;
            ViewBag.RequestId = RequestId;
            ViewBag.IterationNo = IterationNo;
            ViewBag.IsEditable = IsEditable;

            List<GLB002> lstInstrument = Manager.GetSubCatagories("Instrument Group", objQMS050.BU, objQMS050.Location, null);
            ViewBag.InstrumentList = lstInstrument.AsEnumerable().Select(x => new { CatID = x.Code, CatDesc = x.Description, id = x.Code, text = x.Description }).ToList();

            ViewBag.HardnessId = 0;
            ViewBag.Instrument = string.Empty;
            ViewBag.InstrumentCode = string.Empty;
            QMS052 objQMS052 = db.QMS052.Where(x => x.RefRequestId == RequestId).FirstOrDefault();
            if (objQMS052 == null)
            {
                db.QMS052.Add(new QMS052
                {
                    HeaderId = objQMS050.HeaderId,
                    RefRequestId = objQMS050.RequestId,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                db.SaveChanges();
                objQMS052 = db.QMS052.Where(x => x.RefRequestId == RequestId).FirstOrDefault();
            }

            ViewBag.HardnessId = objQMS052?.HardnessId;
            string InstrumentCode = string.Empty;
            InstrumentCode = objQMS052.Instrument;
            if (!string.IsNullOrWhiteSpace(InstrumentCode))
            {
                string[] arr = objQMS052.Instrument.Split(',');
                ViewBag.InstrumentDesc = string.Join(",", lstInstrument.Where(x => arr.Contains(x.Code)).Select(x => x.Description).ToList());
            }
            ViewBag.InstrumentCode = InstrumentCode;
            int? objHardnessRequiredValue = 0;
            string hardnessUnit = string.Empty;
            var objTestPlan = db.QMS021.Where(x => x.QualityProject == objQMS050.QualityProject && x.SeamNo == objQMS050.SeamNo && x.Location == objQMS050.Location && x.StageCode == objQMS050.StageCode && x.StageSequance == objQMS050.StageSequence).FirstOrDefault();
            if (objTestPlan != null ? (objTestPlan.HardnessRequiredValue != null && objTestPlan.HardnessUnit != null) : false)
            {
                objHardnessRequiredValue = objTestPlan.HardnessRequiredValue;
                hardnessUnit = objTestPlan.HardnessUnit;
            }
            else
            {
                List<string> lstQproj = new List<string>();
                lstQproj.Add(objQMS050.QualityProject);
                lstQproj.AddRange(db.QMS017.Where(c => c.BU == objQMS050.BU && c.Location == objQMS050.Location && c.IQualityProject == objQMS050.QualityProject).Select(x => x.QualityProject).ToList());
                var QualityId = db.QMS020.FirstOrDefault(x => x.QualityProject == objQMS050.QualityProject && x.SeamNo == objQMS050.SeamNo && x.Location == objQMS050.Location).QualityId;
                if (!string.IsNullOrWhiteSpace(QualityId))
                {
                    var obj = (from a in db.QMS015_Log
                               join b in db.QMS016_Log on a.Id equals b.RefId
                               where a.Status == "Approved" && lstQproj.Contains(a.QualityProject) && b.StageCode == objQMS050.StageCode && b.QualityId == QualityId
                               select b).FirstOrDefault();
                    objHardnessRequiredValue = obj?.HardnessRequiredValue;
                    hardnessUnit = obj?.HardnessUnit;
                }
            }
            ViewBag.HardnessRequiredValue = objHardnessRequiredValue != null ? Convert.ToString(objHardnessRequiredValue) : "";
            ViewBag.HardnessRequiredUnit = hardnessUnit != null ? Convert.ToString(hardnessUnit) : "";
            return PartialView("_LoadHardnessTestDetailsPartial", objQMS050);
        }

        public ActionResult LoadHardnessGridDataPartial(int HardnessId, int RequestId, bool IsEditable)
        {
            ViewBag.HardnessId = HardnessId;
            ViewBag.IsEditable = IsEditable;
            return PartialView("_GetHardnessGridDataPartial");
        }

        [HttpPost]
        public JsonResult LoadHardnessGridData(JQueryDataTableParamModel param, int HardnessId, bool IsEditable)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                #endregion

                strWhereCondition = " 1=1 and HardnessId=" + HardnessId;

                //search Condition 
                string[] columnName = { "SpotNo", "SeamLocation", "InsideReading1", "InsideReading2", "InsideReading3", "InsideReading4", "InsideReading5", "InsideAverage",
                                                                  "OutsideReading1","OutsideReading2" ,"OutsideReading3" ,"OutsideReading4" ,"OutsideReading5","OutsideAverage","Remarks"};

                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                var lstResult = db.SP_IPI_GET_HARDNESS_TEST_DETAIL_DATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();
                int newRecordId = 0;
                var newRecord = new[] {
                            clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                            clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                            Helper.GenerateNumericTextbox(newRecordId, "SpotNo", "","OnBlurSpotNo(this)",false,"","",""),
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                    };

                var data = (from uc in lstResult
                            select new[]{
                            clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                            clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                            IsEditable ? Helper.GenerateTextbox(Convert.ToInt32(uc.LineId), "SpotNo", Convert.ToString(uc.SpotNo),"",true,"","","") : Convert.ToString(uc.SpotNo),
                            IsEditable ? Helper.GenerateTextbox(Convert.ToInt32(uc.LineId), "SeamLocation", uc.SeamLocation,"",true,"","","") : Convert.ToString(uc.SeamLocation),
                            IsEditable ? Helper.GenerateNumericTextbox(uc.LineId, "InsideReading1_",Convert.ToString(uc.InsideReading1), "UpdateHardnessData(this, " + uc.LineId + ",false);",false,"","","clsReading") : Convert.ToString(uc.InsideReading1),
                            IsEditable ? Helper.GenerateNumericTextbox(uc.LineId, "InsideReading2_", Convert.ToString(uc.InsideReading2), "UpdateHardnessData(this, " + uc.LineId + ",false);",false,"","","clsReading") : Convert.ToString(uc.InsideReading2),
                            IsEditable ? Helper.GenerateNumericTextbox(uc.LineId, "InsideReading3_", Convert.ToString(uc.InsideReading3), "UpdateHardnessData(this, " + uc.LineId + ",false);",false,"","","clsReading") : Convert.ToString(uc.InsideReading3),
                            IsEditable ? Helper.GenerateNumericTextbox(uc.LineId, "InsideReading4_", Convert.ToString(uc.InsideReading4), "UpdateHardnessData(this, " + uc.LineId + ",false);",false,"","","clsReading") : Convert.ToString(uc.InsideReading4),
                            IsEditable ? Helper.GenerateNumericTextbox(uc.LineId, "InsideReading5_", Convert.ToString(uc.InsideReading5), "UpdateHardnessData(this, " + uc.LineId + ",false);",false,"","","clsReading") : Convert.ToString(uc.InsideReading5),
                            IsEditable ? Helper.GenerateTextbox(uc.LineId, "InsideAverage", Convert.ToString(uc.InsideAverage), "UpdateHardnessData(this, " + uc.LineId + ",false);",true,"","","numeric_decimal") : Convert.ToString(uc.InsideAverage),
                            IsEditable ? Helper.GenerateNumericTextbox(uc.LineId, "OutsideReading1_", Convert.ToString(uc.OutsideReading1), "UpdateHardnessData(this, " + uc.LineId + ",false);",false,"","","clsReading") : Convert.ToString(uc.OutsideReading1),
                            IsEditable ? Helper.GenerateNumericTextbox(uc.LineId, "OutsideReading2_", Convert.ToString(uc.OutsideReading2), "UpdateHardnessData(this, " + uc.LineId + ",false);",false,"","","clsReading") : Convert.ToString(uc.OutsideReading2),
                            IsEditable ? Helper.GenerateNumericTextbox(uc.LineId, "OutsideReading3_", Convert.ToString(uc.OutsideReading3), "UpdateHardnessData(this, " + uc.LineId + ",false);",false,"","","clsReading") : Convert.ToString(uc.OutsideReading3),
                            IsEditable ? Helper.GenerateNumericTextbox(uc.LineId, "OutsideReading4_", Convert.ToString(uc.OutsideReading4), "UpdateHardnessData(this, " + uc.LineId + ",false);",false,"","","clsReading") : Convert.ToString(uc.OutsideReading4),
                            IsEditable ? Helper.GenerateNumericTextbox(uc.LineId, "OutsideReading5_", Convert.ToString(uc.OutsideReading5), "UpdateHardnessData(this, " + uc.LineId + ",false);",false,"","","clsReading") : Convert.ToString(uc.OutsideReading5),
                            IsEditable ? Helper.GenerateTextbox(uc.LineId, "OutsideAverage", Convert.ToString(uc.OutsideAverage), "UpdateHardnessData(this, " + uc.LineId + ",false);",true,"","","numeric_decimal") : Convert.ToString(uc.OutsideAverage),
                            IsEditable ? Helper.GenerateTextbox(uc.LineId, "Remarks",uc.Remarks, "UpdateHardnessData(this, " + uc.LineId + ",false);",false,"","200","") : uc.Remarks,
                            Helper.HTMLActionString(uc.LineId, "Delete", "Delete Record", "fa fa-trash-o", "DeleteHardnessTestDetail("+uc.LineId+");","",IsEditable ? false : true),
                        }).ToList();
                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetInlineEditableRowForHardness(int id, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();
                var result = new List<string>();
                if (id == 0)
                {
                    int newRecordId = 0;
                    var newRecord = new[] {
                            clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                            clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                            Helper.GenerateNumericTextbox(newRecordId, "SpotNo", "","OnBlurSpotNo(this)",false,"","",""),
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                    };
                    data.Add(newRecord);
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult UpdateInstrumentData(int HardnessId, string InstrumentCode)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            //var firstId = data[0];

            try
            {
                var objQMS052 = db.QMS052.Where(x => x.HardnessId == HardnessId).FirstOrDefault();
                if (objQMS052 != null)
                {
                    objQMS052.Instrument = InstrumentCode;
                    objQMS052.EditedBy = objClsLoginInfo.UserName;
                    objQMS052.EditedOn = DateTime.Now;

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Hardness Test Header Data not found";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateHardnessData(int LineId, string ColumnName, string ColumnValue, string InsideAverage, string OutsideAverage)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string tableName = "QMS052_1";
                if (ColumnName.EndsWith("_"))
                    ColumnName = ColumnName.Substring(0, ColumnName.Length - 1);

                if (!string.IsNullOrEmpty(ColumnName))
                {
                    db.SP_COMMON_TABLE_UPDATE(tableName, Convert.ToInt32(LineId), "LineId", ColumnName, ColumnValue, objClsLoginInfo.UserName);

                    if (ColumnName.ToLower().Contains("insidereading"))
                    {
                        db.SP_COMMON_TABLE_UPDATE(tableName, Convert.ToInt32(LineId), "LineId", "InsideAverage", InsideAverage, objClsLoginInfo.UserName);
                    }
                    if (ColumnName.ToLower().Contains("outsidereading"))
                    {
                        db.SP_COMMON_TABLE_UPDATE(tableName, Convert.ToInt32(LineId), "LineId", "OutsideAverage", OutsideAverage, objClsLoginInfo.UserName);
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddHardnessTestDefaultLineData(int HardnessId, string SpotNo)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                List<QMS052_1> objQMS052_1List = new List<QMS052_1>();
                List<string> alreadyExists = new List<string>();
                string[] arrVals = new string[] { "Base Metal", "HAZ", "Weld", "HAZ", "Base Metal" };
                var objQMS052 = db.QMS052.Where(x => x.HardnessId == HardnessId).FirstOrDefault();
                if (objQMS052 != null)
                {
                    for (int i = 0; i < arrVals.Length; i++)
                    {
                        int spt = Convert.ToInt32(SpotNo);
                        var SeamLocation = arrVals[i];
                        if (!db.QMS052_1.Where(x => x.HardnessId == HardnessId && x.SpotNo == spt && x.SeamLocation == SeamLocation).Any())
                        {
                            objQMS052_1List.Add(new QMS052_1()
                            {
                                HardnessId = HardnessId,
                                SpotNo = Convert.ToInt32(SpotNo),
                                SeamLocation = arrVals[i],
                                CreatedBy = objClsLoginInfo.UserName,
                                CreatedOn = DateTime.Now
                            });
                        }
                        else { alreadyExists.Add(SeamLocation); }
                    }

                    if (objQMS052_1List.Count() > 0)
                    {
                        db.QMS052_1.AddRange(objQMS052_1List);
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                    }
                    if (alreadyExists.Count() > 0)
                    {
                        objResponseMsg.dataKey = true;
                        objResponseMsg.dataValue = " Seam Location :" + string.Join(",", alreadyExists.ToList()) + " already exists !!";
                    }
                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Hardness Test Header Data not found";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteHardnessTestDetail(int LineId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objQMS052_1 = db.QMS052_1.Where(x => x.LineId == LineId).FirstOrDefault();
                if (objQMS052_1 != null)
                {
                    db.QMS052_1.Remove(objQMS052_1);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Data not found";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Maintain PMI Test Details

        public ActionResult LoadPMITestDetailsPartial(int HeaderId, int RequestId, string IterationNo, bool IsEditable)
        {
            QMS050 objQMS050 = new QMS050();
            NDEModels objNDEModels = new NDEModels();
            string[] arrqElement = new string[6];
            double[] arrMin = new double[6];
            double[] arrMax = new double[6];
            int j = 0;
            if (RequestId > 0)
            {
                objQMS050 = db.QMS050.Where(x => x.RequestId == RequestId).FirstOrDefault();
            }
            ViewBag.Stage = db.QMS002.Where(x => x.StageCode == objQMS050.StageCode && x.BU == objQMS050.BU && x.Location == objQMS050.Location).Select(x => x.StageCode + "-" + x.StageDesc).FirstOrDefault();
            ViewBag.HeaderId = HeaderId;
            ViewBag.RequestId = RequestId;
            ViewBag.IterationNo = IterationNo;

            string RoletoEnableEditForPMI = clsImplementationEnum.ConfigParameter.RoletoEnableEditForPMI.GetStringValue();
            var roles = Manager.GetConfigValue(RoletoEnableEditForPMI).ToUpper().Split(',').ToArray();
            bool flg = objClsLoginInfo.GetUserRoleList().Where(x => roles.Contains(x)).Any();
            ViewBag.IsEditable = (objQMS050.TestResult == clsImplementationEnum.SeamListInspectionStatus.Cleared.GetStringValue()) ? flg : IsEditable;
            ViewBag.IsDeleteEditable = IsEditable;
            List<GLB002> lstInstrument = Manager.GetSubCatagories("Instrument Group", objQMS050.BU, objQMS050.Location, null);
            ViewBag.InstrumentList = lstInstrument.AsEnumerable().Select(x => new { CatID = x.Code, CatDesc = x.Description, id = x.Code, text = x.Description }).ToList();
            ViewBag.PMIId = 0;
            ViewBag.Instrument = string.Empty;
            ViewBag.InstrumentCode = string.Empty;
            QMS054 objQMS054 = db.QMS054.Where(x => x.RefRequestId == RequestId).FirstOrDefault();


            var objTestPlan = db.QMS021.Where(x => x.QualityProject == objQMS050.QualityProject && x.SeamNo == objQMS050.SeamNo && x.Location == objQMS050.Location && x.StageCode == objQMS050.StageCode && x.StageSequance == objQMS050.StageSequence).FirstOrDefault();
            if (objTestPlan != null)
            {
                var lstQMS023 = objTestPlan != null ? db.QMS023.Where(i => i.TPLineId == objTestPlan.LineId).OrderByDescending(x => x.TPRevNo).ToList() : null;
                if (lstQMS023.Count() > 0 && lstQMS023 != null)
                {
                    #region Fetch element data from Test Plan(QMS022/QMS023)
                    foreach (var item in lstQMS023)
                    {
                        if (objQMS054 == null)
                        {
                            arrqElement[j] = item.Element;
                        }
                        arrMin[j] = Convert.ToDouble(item.ReqMinValue);
                        arrMax[j] = Convert.ToDouble(item.ReqMaxValue);
                        if (j == 5)
                        {
                            break;
                        }
                        j = j + 1;
                        if (j == 6)
                        { break; }
                    }
                    #endregion
                }
                else
                {
                    #region fetch data from QID
                    List<string> lstQproj = new List<string>();
                    lstQproj.Add(objQMS050.QualityProject);
                    lstQproj.AddRange(db.QMS017.Where(c => c.BU == objQMS050.BU && c.Location == objQMS050.Location && c.IQualityProject == objQMS050.QualityProject).Select(x => x.QualityProject).ToList());

                    var QualityId = db.QMS020.FirstOrDefault(x => x.QualityProject == objQMS050.QualityProject && x.SeamNo == objQMS050.SeamNo && x.Location == objQMS050.Location).QualityId;
                    var Qrev = QualityId != null ? db.QMS015.FirstOrDefault(x => x.QualityId == QualityId && lstQproj.Contains(x.QualityProject) && x.Location == objQMS050.Location).RevNo : 0;

                    if (!string.IsNullOrWhiteSpace(QualityId))
                    {
                        var lstQMS019 = db.QMS019.Where(i => lstQproj.Contains(i.QualityProject) //&& i.Project == objQMS040.Project
                                                        && i.BU == objQMS050.BU && i.Location == objQMS050.Location && i.QualityId == QualityId && i.QualityIdRev == Qrev
                                                        && i.StageCode == objQMS050.StageCode
                                                    ).ToList();

                        if (lstQMS019.Count() > 0)
                        {
                            int i = 0;
                            foreach (var item in lstQMS019)
                            {
                                if (objQMS054 == null)
                                {
                                    arrqElement[i] = item.Element;
                                }
                                arrMin[i] = Convert.ToDouble(item.ReqMinValue);
                                arrMax[i] = Convert.ToDouble(item.ReqMaxValue);
                                i = i + 1;
                            }
                        }

                    }
                    #endregion
                }
            }
            ViewBag.ReqMinValue = arrMin;
            ViewBag.ReqMaxValue = arrMax;

            if (objQMS054 == null)
            {
                db.QMS054.Add(new QMS054
                {
                    HeaderId = objQMS050.HeaderId,
                    RefRequestId = objQMS050.RequestId,
                    Element1 = arrqElement[0],
                    Element2 = arrqElement[1],
                    Element3 = arrqElement[2],
                    Element4 = arrqElement[3],
                    Element5 = arrqElement[4],
                    Element6 = arrqElement[5],
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                db.SaveChanges();
                objQMS054 = db.QMS054.Where(x => x.RefRequestId == RequestId).FirstOrDefault();
            }
            if (objQMS054 != null)
            {
                ViewBag.PMIId = objQMS054.PMIId;
                string InstrumentCode = string.Empty;
                InstrumentCode = objQMS054.Instrument;
                if (!string.IsNullOrWhiteSpace(InstrumentCode))
                {
                    string[] arr = objQMS054.Instrument.Split(',');
                    ViewBag.InstrumentDesc = string.Join(",", lstInstrument.Where(x => arr.Contains(x.Code)).Select(x => x.Description).ToList());
                }
                ViewBag.InstrumentCode = InstrumentCode;
                ViewBag.Element1 = objNDEModels.GetCategory("Element", objQMS054.Element1, objQMS050.BU.Trim(), objQMS050.Location.Trim(), true).CategoryDescription;
                ViewBag.Element2 = objNDEModels.GetCategory("Element", objQMS054.Element2, objQMS050.BU.Trim(), objQMS050.Location.Trim(), true).CategoryDescription;
                ViewBag.Element3 = objNDEModels.GetCategory("Element", objQMS054.Element3, objQMS050.BU.Trim(), objQMS050.Location.Trim(), true).CategoryDescription;
                ViewBag.Element4 = objNDEModels.GetCategory("Element", objQMS054.Element4, objQMS050.BU.Trim(), objQMS050.Location.Trim(), true).CategoryDescription;
                ViewBag.Element5 = objNDEModels.GetCategory("Element", objQMS054.Element5, objQMS050.BU.Trim(), objQMS050.Location.Trim(), true).CategoryDescription;
                ViewBag.Element6 = objNDEModels.GetCategory("Element", objQMS054.Element6, objQMS050.BU.Trim(), objQMS050.Location.Trim(), true).CategoryDescription;
            }



            return PartialView("_GetPMIGridDataPartia", objQMS050);
        }

        [HttpPost]
        public JsonResult LoadPMIGridData(JQueryDataTableParamModel param, int PMIId, bool IsEditable, bool IsDeleteEditable)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                #endregion

                strWhereCondition = " 1=1 and PMIId=" + PMIId;

                //search Condition 
                string[] columnName = {  "InsideReading1", "InsideReading2", "InsideReading3", "InsideReading4", "InsideReading5", "InsideAverage",
                                                                  "OutsideReading1","OutsideReading2" ,"OutsideReading3" ,"OutsideReading4" ,"OutsideReading5","OutsideAverage","Remarks"};

                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                var lstResult = db.SP_IPI_GET_PMI_TEST_DETAIL_DATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();
                int newRecordId = 0;
                var newRecord = new[] {
                            clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                            clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                            Helper.GenerateNumericTextbox(newRecordId, "InsideReading1_" ,"","ValidatePMIData(this, " + newRecordId + ",false,true);",false,"","",""),
                            Helper.GenerateNumericTextbox(newRecordId, "OutsideReading1_","","ValidatePMIData(this, " + newRecordId + ",false,true);",false,"","",""),
                            Helper.GenerateNumericTextbox(newRecordId, "InsideReading2_" ,"","ValidatePMIData(this, " + newRecordId + ",false,true);",false,"","",""),
                            Helper.GenerateNumericTextbox(newRecordId, "OutsideReading2_","","ValidatePMIData(this, " + newRecordId + ",false,true);",false,"","",""),
                            Helper.GenerateNumericTextbox(newRecordId, "InsideReading3_" ,"","ValidatePMIData(this, " + newRecordId + ",false,true);",false,"","",""),
                            Helper.GenerateNumericTextbox(newRecordId, "OutsideReading3_","","ValidatePMIData(this, " + newRecordId + ",false,true);",false,"","",""),
                            Helper.GenerateNumericTextbox(newRecordId, "InsideReading4_" ,"","ValidatePMIData(this, " + newRecordId + ",false,true);",false,"","",""),
                            Helper.GenerateNumericTextbox(newRecordId, "OutsideReading4_","","ValidatePMIData(this, " + newRecordId + ",false,true);",false,"","",""),
                            Helper.GenerateNumericTextbox(newRecordId, "InsideReading5_" ,"","ValidatePMIData(this, " + newRecordId + ",false,true);",false,"","",""),
                            Helper.GenerateNumericTextbox(newRecordId, "OutsideReading5_","","ValidatePMIData(this, " + newRecordId + ",false,true);",false,"","",""),
                            Helper.GenerateNumericTextbox(newRecordId, "InsideReading6_" ,"","ValidatePMIData(this, " + newRecordId + ",false,true);",false,"","","") ,
                            Helper.GenerateNumericTextbox(newRecordId, "OutsideReading6_","","ValidatePMIData(this, " + newRecordId + ",false,true);",false,"","","") ,
                            Helper.GenerateTextbox(newRecordId, "Remarks","","",false,"","200","") ,
                            Helper.HTMLActionString(newRecordId,"Add","Add New Record","fa fa-plus","SaveNewRecord(0);"),
                    };

                var data = (from uc in lstResult
                            select new[]{
                            clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                            clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                            IsEditable ? Helper.GenerateNumericTextbox(uc.LineId, "InsideReading1_",Convert.ToString(uc.InsideReading1), "ValidatePMIData(this, " + uc.LineId + ",true);",false,"","","numeric_decimal") : Convert.ToString(uc.InsideReading1),
                            IsEditable ? Helper.GenerateNumericTextbox(uc.LineId, "OutsideReading1_", Convert.ToString(uc.OutsideReading1), "ValidatePMIData(this, " + uc.LineId + ",true);",false,"","","numeric_decimal") : Convert.ToString(uc.OutsideReading1),
                            IsEditable ? Helper.GenerateNumericTextbox(uc.LineId, "InsideReading2_", Convert.ToString(uc.InsideReading2), "ValidatePMIData(this, " + uc.LineId + ",true);",false,"","","numeric_decimal") : Convert.ToString(uc.InsideReading2),
                            IsEditable ? Helper.GenerateNumericTextbox(uc.LineId, "OutsideReading2_", Convert.ToString(uc.OutsideReading2), "ValidatePMIData(this, " + uc.LineId + ",true);",false,"","","numeric_decimal") : Convert.ToString(uc.OutsideReading2),
                            IsEditable ? Helper.GenerateNumericTextbox(uc.LineId, "InsideReading3_", Convert.ToString(uc.InsideReading3), "ValidatePMIData(this, " + uc.LineId + ",true);",false,"","","numeric_decimal") : Convert.ToString(uc.InsideReading3),
                            IsEditable ? Helper.GenerateNumericTextbox(uc.LineId, "OutsideReading3_", Convert.ToString(uc.OutsideReading3), "ValidatePMIData(this, " + uc.LineId + ",true);",false,"","","numeric_decimal") : Convert.ToString(uc.OutsideReading3),
                            IsEditable ? Helper.GenerateNumericTextbox(uc.LineId, "InsideReading4_", Convert.ToString(uc.InsideReading4), "ValidatePMIData(this, " + uc.LineId + ",true);",false,"","","numeric_decimal") : Convert.ToString(uc.InsideReading4),
                            IsEditable ? Helper.GenerateNumericTextbox(uc.LineId, "OutsideReading4_", Convert.ToString(uc.OutsideReading4), "ValidatePMIData(this, " + uc.LineId + ",true);",false,"","","numeric_decimal") : Convert.ToString(uc.OutsideReading4),
                            IsEditable ? Helper.GenerateNumericTextbox(uc.LineId, "InsideReading5_", Convert.ToString(uc.InsideReading5), "ValidatePMIData(this, " + uc.LineId + ",true);",false,"","","numeric_decimal") : Convert.ToString(uc.InsideReading5),
                            IsEditable ? Helper.GenerateNumericTextbox(uc.LineId, "OutsideReading5_", Convert.ToString(uc.OutsideReading5), "ValidatePMIData(this, " + uc.LineId + ",true);",false,"","","numeric_decimal") : Convert.ToString(uc.OutsideReading5),
                            IsEditable ? Helper.GenerateNumericTextbox(uc.LineId, "InsideReading6_", Convert.ToString(uc.InsideReading6), "ValidatePMIData(this, " + uc.LineId + ",true);",false,"","","numeric_decimal") : Convert.ToString(uc.InsideReading6),
                            IsEditable ? Helper.GenerateNumericTextbox(uc.LineId, "OutsideReading6_", Convert.ToString(uc.OutsideReading6), "ValidatePMIData(this, " + uc.LineId + ",true);",false,"","","numeric_decimal") : Convert.ToString(uc.OutsideReading6),
                            IsEditable ? Helper.GenerateTextbox(uc.LineId, "Remarks",uc.Remarks, "ValidatePMIData(this, " + uc.LineId + ",false);",false,"","200","") : uc.Remarks,
                            Helper.HTMLActionString(uc.LineId, "Delete", "Delete Record", "fa fa-trash-o", "DeleteUtilityGridData("+uc.LineId+",'/IPI/MaintainSeamListOfferInspection/DeletePMITestDetail',null,'tblPMITestLines');","",IsDeleteEditable ? false : true),
                        }).ToList();
                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetInlineEditableRowForPMI(int id, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();
                var result = new List<string>();
                if (id == 0)
                {
                    int newRecordId = 0;
                    var newRecord = new[] {
                            clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                            clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                            Helper.GenerateNumericTextbox(newRecordId, "InsideReading1_" ,"","ValidatePMIData(this, " + newRecordId + ",false,true);",false,"","",""),
                            Helper.GenerateNumericTextbox(newRecordId, "OutsideReading1_","","ValidatePMIData(this, " + newRecordId + ",false,true);",false,"","",""),
                            Helper.GenerateNumericTextbox(newRecordId, "InsideReading2_" ,"","ValidatePMIData(this, " + newRecordId + ",false,true);",false,"","",""),
                            Helper.GenerateNumericTextbox(newRecordId, "OutsideReading2_","","ValidatePMIData(this, " + newRecordId + ",false,true);",false,"","",""),
                            Helper.GenerateNumericTextbox(newRecordId, "InsideReading3_" ,"","ValidatePMIData(this, " + newRecordId + ",false,true);",false,"","",""),
                            Helper.GenerateNumericTextbox(newRecordId, "OutsideReading3_","","ValidatePMIData(this, " + newRecordId + ",false,true);",false,"","",""),
                            Helper.GenerateNumericTextbox(newRecordId, "InsideReading4_" ,"","ValidatePMIData(this, " + newRecordId + ",false,true);",false,"","",""),
                            Helper.GenerateNumericTextbox(newRecordId, "OutsideReading4_","","ValidatePMIData(this, " + newRecordId + ",false,true);",false,"","",""),
                            Helper.GenerateNumericTextbox(newRecordId, "InsideReading5_" ,"","ValidatePMIData(this, " + newRecordId + ",false,true);",false,"","",""),
                            Helper.GenerateNumericTextbox(newRecordId, "OutsideReading5_","","ValidatePMIData(this, " + newRecordId + ",false,true);",false,"","",""),
                            Helper.GenerateNumericTextbox(newRecordId, "InsideReading6_" ,"","ValidatePMIData(this, " + newRecordId + ",false,true);",false,"","","") ,
                            Helper.GenerateNumericTextbox(newRecordId, "OutsideReading6_","","ValidatePMIData(this, " + newRecordId + ",false,true);",false,"","","") ,
                            Helper.GenerateTextbox(newRecordId, "Remarks","","",false,"","200","") ,
                            Helper.HTMLActionString(newRecordId,"Add","Add New Record","fa fa-plus","SaveNewRecord(0);"),
                    };
                    data.Add(newRecord);
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SavePMILineData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                QMS054_1 objQMS054_1 = new QMS054_1();
                if (fc != null)
                {
                    int LineId = Convert.ToInt32(fc["LineId"]);
                    double InsideReading1 = !string.IsNullOrWhiteSpace(fc["InsideReading1_" + LineId]) ? Convert.ToDouble(fc["InsideReading1_" + LineId]) : 0;
                    double? OutsideReading1 = !string.IsNullOrWhiteSpace(fc["OutsideReading1_" + LineId]) ? Convert.ToDouble(fc["OutsideReading1_" + LineId]) : 0;
                    double? InsideReading2 = !string.IsNullOrWhiteSpace(fc["InsideReading2_" + LineId]) ? Convert.ToDouble(fc["InsideReading2_" + LineId]) : 0;
                    double? OutsideReading2 = !string.IsNullOrWhiteSpace(fc["OutsideReading2_" + LineId]) ? Convert.ToDouble(fc["OutsideReading2_" + LineId]) : 0;
                    double? InsideReading3 = !string.IsNullOrWhiteSpace(fc["InsideReading3_" + LineId]) ? Convert.ToDouble(fc["InsideReading3_" + LineId]) : 0;
                    double? OutsideReading3 = !string.IsNullOrWhiteSpace(fc["OutsideReading3_" + LineId]) ? Convert.ToDouble(fc["OutsideReading3_" + LineId]) : 0;
                    double? InsideReading4 = !string.IsNullOrWhiteSpace(fc["InsideReading4_" + LineId]) ? Convert.ToDouble(fc["InsideReading4_" + LineId]) : 0;
                    double? OutsideReading4 = !string.IsNullOrWhiteSpace(fc["OutsideReading4_" + LineId]) ? Convert.ToDouble(fc["OutsideReading4_" + LineId]) : 0;
                    double? InsideReading5 = !string.IsNullOrWhiteSpace(fc["InsideReading5_" + LineId]) ? Convert.ToDouble(fc["InsideReading5_" + LineId]) : 0;
                    double? OutsideReading5 = !string.IsNullOrWhiteSpace(fc["OutsideReading5_" + LineId]) ? Convert.ToDouble(fc["OutsideReading5_" + LineId]) : 0;
                    double? InsideReading6 = !string.IsNullOrWhiteSpace(fc["InsideReading6_" + LineId]) ? Convert.ToDouble(fc["InsideReading6_" + LineId]) : 0;
                    double? OutsideReading6 = !string.IsNullOrWhiteSpace(fc["OutsideReading6_" + LineId]) ? Convert.ToDouble(fc["OutsideReading6_" + LineId]) : 0;

                    objQMS054_1.PMIId = Convert.ToInt32(fc["PMIId"]);
                    objQMS054_1.InsideReading1 = InsideReading1;
                    objQMS054_1.OutsideReading1 = OutsideReading1;
                    objQMS054_1.InsideReading2 = InsideReading2;
                    objQMS054_1.OutsideReading2 = OutsideReading2;
                    objQMS054_1.InsideReading3 = InsideReading3;
                    objQMS054_1.OutsideReading3 = OutsideReading3;
                    objQMS054_1.InsideReading4 = InsideReading4;
                    objQMS054_1.OutsideReading4 = OutsideReading4;
                    objQMS054_1.InsideReading5 = InsideReading5;
                    objQMS054_1.OutsideReading5 = OutsideReading5;
                    objQMS054_1.InsideReading6 = InsideReading6;
                    objQMS054_1.OutsideReading6 = OutsideReading6;
                    objQMS054_1.Remarks = Convert.ToString(fc["Remarks" + LineId]);
                    objQMS054_1.CreatedBy = objClsLoginInfo.UserName;
                    objQMS054_1.CreatedOn = DateTime.Now;
                    db.QMS054_1.Add(objQMS054_1);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.dataValue = ex.ToString();
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public ActionResult UpdatePMIInstrumentData(int PMIId, string InstrumentCode)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                
                    var objQMS054 = db.QMS054.Where(x => x.PMIId == PMIId).FirstOrDefault();
                    //var objQMS054 = db.QMS054.Where(x => x.RefRequestId == item).FirstOrDefault();
                    if (objQMS054 != null)
                    {
                        objQMS054.Instrument = InstrumentCode;
                        objQMS054.EditedBy = objClsLoginInfo.UserName;
                        objQMS054.EditedOn = DateTime.Now;

                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "PMI Test Header Data not found";
                    }
            
                //objResponseMsg.Key = false;
                //objResponseMsg.Value = "PMI Test Header Data not found";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdatePMIData(int LineId, string ColumnName, string ColumnValue, string InsideAverage)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string tableName = "QMS054_1";
                if (ColumnName.EndsWith("_"))
                    ColumnName = ColumnName.Substring(0, ColumnName.Length - 1);

                if (!string.IsNullOrEmpty(ColumnName))
                {
                    db.SP_COMMON_TABLE_UPDATE(tableName, Convert.ToInt32(LineId), "LineId", ColumnName, ColumnValue, objClsLoginInfo.UserName);

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddPMITestDefaultLineData(int PMIId, string SpotNo)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (PMIId > 0)
                {
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "PMI Test Header Data not found";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeletePMITestDetail(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objQMS054_1 = db.QMS054_1.Where(x => x.LineId == Id).FirstOrDefault();
                if (objQMS054_1 != null)
                {
                    db.QMS054_1.Remove(objQMS054_1);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Data not found";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Maintain Ferrite Test Details

        public ActionResult LoadFerriteTestDetailsPartial(int HeaderId, int RequestId, string IterationNo, bool IsEditable)
        {
            QMS050 objQMS050 = new QMS050();
            if (RequestId > 0)
            {
                objQMS050 = db.QMS050.Where(x => x.RequestId == RequestId).FirstOrDefault();
            }
            ViewBag.Stage = db.QMS002.Where(x => x.StageCode == objQMS050.StageCode && x.BU == objQMS050.BU && x.Location == objQMS050.Location).Select(x => x.StageCode + "-" + x.StageDesc).FirstOrDefault();
            //ViewBag.Project = db.COM001.Where(i => i.t_cprj.Equals(objQMS050.Project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + "-" + i.t_dsca).FirstOrDefault();
            //ViewBag.Location = db.COM002.Where(a => a.t_dimx == objQMS050.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            //ViewBag.BU = db.COM002.Where(a => a.t_dimx == objQMS050.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            ViewBag.HeaderId = HeaderId;
            ViewBag.RequestId = RequestId;
            ViewBag.IterationNo = IterationNo;
            ViewBag.IsEditable = IsEditable;

            List<string> lstWeldingProcess = GetWeldingProcessForFerriteTest(objQMS050.QualityProject, objQMS050.Project, objQMS050.SeamNo, objQMS050.BU);
            ViewBag.WeldingProcessForFerriteTest = string.Join(",", lstWeldingProcess);
            //ViewBag.WeldingProcessForFerriteList = lstWeldingProcess.AsEnumerable().Select(x => new { id = x.ToString(), text = x.ToString() }).ToList();
            List<GLB002> lstInstrument = Manager.GetSubCatagories("Instrument Group", objQMS050.BU, objQMS050.Location, null);
            var InstrumentList = lstInstrument.AsEnumerable().Select(x => new { CatID = x.Code, CatDesc = x.Description, id = x.Code, text = x.Description }).ToList();
            QMS053_1 objQMS053_1 = new QMS053_1();
            objQMS053_1 = db.QMS053_1.Where(x => x.HeaderId == HeaderId && x.RefRequestId == RequestId).FirstOrDefault();
            if (objQMS053_1 == null)
            {
                objQMS053_1 = new QMS053_1();
                objQMS053_1.HeaderId = HeaderId;
                objQMS053_1.RefRequestId = RequestId;
                objQMS053_1.CreatedBy = objClsLoginInfo.UserName;
                objQMS053_1.CreatedOn = DateTime.Now;
                db.QMS053_1.Add(objQMS053_1);
                db.SaveChanges();
                objQMS053_1 = db.QMS053_1.Where(x => x.HeaderId == HeaderId && x.RefRequestId == RequestId).FirstOrDefault();
            }
            //if (!string.IsNullOrWhiteSpace(objQMS053_1.ApplicableWeldingProcess))
            //{
            //    ViewBag.WeldingProcessForFerriteTest = objQMS053_1.ApplicableWeldingProcess;
            //}
            ViewBag.FerriteHeaderID = objQMS053_1.FerriteId;
            string Instrument = string.Empty;
            string InstrumentDesc = string.Empty;
            string InstrumentCode = objQMS053_1.Instrument;
            if (!string.IsNullOrWhiteSpace(InstrumentCode))
            {
                string[] arr = objQMS053_1.Instrument.Split(',');
                InstrumentDesc = string.Join(",", lstInstrument.Where(x => arr.Contains(x.Code)).Select(x => x.Description).ToList());
            }
            ViewBag.InstrumentCode = InstrumentCode;
            var objTestPlan = db.QMS021.Where(x => x.QualityProject == objQMS050.QualityProject && x.SeamNo == objQMS050.SeamNo && x.Location == objQMS050.Location && x.StageCode == objQMS050.StageCode && x.StageSequance == objQMS050.StageSequence).FirstOrDefault();
            if (objTestPlan != null ? (objTestPlan.FerriteMaxValue != null && objTestPlan.FerriteMinValue != null && objTestPlan.FerriteUnit != null) : false)
            {
                if (objTestPlan.FerriteMinValue.HasValue)
                {
                    ViewBag.FerriteMinValue = objTestPlan != null ? Convert.ToString(objTestPlan.FerriteMinValue) : "";
                    ViewBag.FerriteMaxValue = objTestPlan != null ? Convert.ToString(objTestPlan.FerriteMaxValue) : "";
                    ViewBag.FerriteUnit = objTestPlan != null ? Convert.ToString(objTestPlan.FerriteUnit) : "";
                }
            }
            else
            {
                List<string> lstQproj = new List<string>();
                lstQproj.Add(objQMS050.QualityProject);
                lstQproj.AddRange(db.QMS017.Where(c => c.BU == objQMS050.BU && c.Location == objQMS050.Location && c.IQualityProject == objQMS050.QualityProject).Select(x => x.QualityProject).ToList());

                var QualityId = db.QMS020.FirstOrDefault(x => x.QualityProject == objQMS050.QualityProject && x.SeamNo == objQMS050.SeamNo && x.Location == objQMS050.Location).QualityId;
                if (!string.IsNullOrWhiteSpace(QualityId))
                {
                    var objTemp = (from a in db.QMS015_Log
                                   join b in db.QMS016_Log on a.Id equals b.RefId
                                   where a.Status == "Approved" && lstQproj.Contains(a.QualityProject) && b.StageCode == objQMS050.StageCode && b.QualityId == QualityId
                                   select b).FirstOrDefault();

                    ViewBag.FerriteMinValue = objTemp != null ? Convert.ToString(objTemp.FerriteMinValue) : "";
                    ViewBag.FerriteMaxValue = objTemp != null ? Convert.ToString(objTemp.FerriteMaxValue) : "";
                    ViewBag.FerriteUnit = objTemp != null ? Convert.ToString(objTemp.FerriteUnit) : "";
                }
            }
            ViewBag.InstrumentList = InstrumentList;
            ViewBag.Instrument = Instrument;
            ViewBag.InstrumentDesc = InstrumentDesc;
            return PartialView("_LoadFerriteTestDetailsPartial", objQMS050);
        }

        // ONLY USED IN MOBILE APPLICATION
        [HttpPost]
        public ActionResult LoadFerriteTestDetailsMobile(int HeaderId, int RequestId, string IterationNo, bool IsEditable, string APIRequestFrom, string UserName)
        {
            QMS050 objQMS050 = new QMS050();
            if (RequestId > 0)
            {
                db.Configuration.ProxyCreationEnabled = false;
                objQMS050 = db.QMS050.Where(x => x.RequestId == RequestId).FirstOrDefault();
                db.Entry(objQMS050).State = System.Data.Entity.EntityState.Detached;
            }
            var Stage = db.QMS002.Where(x => x.StageCode == objQMS050.StageCode && x.BU == objQMS050.BU && x.Location == objQMS050.Location).Select(x => x.StageCode + "-" + x.StageDesc).FirstOrDefault();
            //ViewBag.Project = db.COM001.Where(i => i.t_cprj.Equals(objQMS050.Project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + "-" + i.t_dsca).FirstOrDefault();
            //ViewBag.Location = db.COM002.Where(a => a.t_dimx == objQMS050.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            //ViewBag.BU = db.COM002.Where(a => a.t_dimx == objQMS050.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();


            List<string> lstWeldingProcess = GetWeldingProcessForFerriteTest(objQMS050.QualityProject, objQMS050.Project, objQMS050.SeamNo, objQMS050.BU);
            var WeldingProcessForFerriteTest = string.Join(",", lstWeldingProcess);
            //var WeldingProcessForFerriteList = lstWeldingProcess.AsEnumerable().Select(x => new { id = x.ToString(), text = x.ToString() }).ToList();
            List<GLB002> lstInstrument = Manager.GetSubCatagories("Instrument Group", objQMS050.BU, objQMS050.Location, null);
            var InstrumentList = lstInstrument.AsEnumerable().Select(x => new { CatID = x.Code, CatDesc = x.Description, id = x.Code, text = x.Description }).ToList();
            QMS053_1 objQMS053_1 = new QMS053_1();
            objQMS053_1 = db.QMS053_1.Where(x => x.HeaderId == HeaderId && x.RefRequestId == RequestId).FirstOrDefault();
            if (objQMS053_1 == null)
            {
                objQMS053_1 = new QMS053_1();
                objQMS053_1.HeaderId = HeaderId;
                objQMS053_1.RefRequestId = RequestId;
                objQMS053_1.CreatedBy = UserName;
                objQMS053_1.CreatedOn = DateTime.Now;
                db.QMS053_1.Add(objQMS053_1);
                db.SaveChanges();
                objQMS053_1 = db.QMS053_1.Where(x => x.HeaderId == HeaderId && x.RefRequestId == RequestId).FirstOrDefault();
            }
            //if (!string.IsNullOrWhiteSpace(objQMS053_1.ApplicableWeldingProcess))
            //{
            //    WeldingProcessForFerriteTest = objQMS053_1.ApplicableWeldingProcess;
            //}
            var FerriteHeaderID = objQMS053_1.FerriteId;
            string Instrument = string.Empty;
            string InstrumentDesc = string.Empty;
            string InstrumentCode = objQMS053_1.Instrument;
            if (!string.IsNullOrWhiteSpace(InstrumentCode))
            {
                string[] arr = objQMS053_1.Instrument.Split(',');
                InstrumentDesc = string.Join(",", lstInstrument.Where(x => arr.Contains(x.Code)).Select(x => x.Description).ToList());
            }
            string FerriteMinValue = "";
            string FerriteMaxValue = "";
            string FerriteUnit = "";
            var objTestPlan = db.QMS021.Where(x => x.QualityProject == objQMS050.QualityProject && x.SeamNo == objQMS050.SeamNo && x.Location == objQMS050.Location && x.StageCode == objQMS050.StageCode && x.StageSequance == objQMS050.StageSequence).FirstOrDefault();
            if (objTestPlan != null ? (objTestPlan.FerriteMaxValue != null && objTestPlan.FerriteMinValue != null && objTestPlan.FerriteUnit != null) : false)
            {
                if (objTestPlan.FerriteMinValue.HasValue)
                {
                    FerriteMinValue = objTestPlan != null ? Convert.ToString(objTestPlan.FerriteMinValue) : "";
                    FerriteMaxValue = objTestPlan != null ? Convert.ToString(objTestPlan.FerriteMaxValue) : "";
                    FerriteUnit = objTestPlan != null ? Convert.ToString(objTestPlan.FerriteUnit) : "";
                }
            }
            else
            {
                List<string> lstQproj = new List<string>();
                lstQproj.Add(objQMS050.QualityProject);
                lstQproj.AddRange(db.QMS017.Where(c => c.BU == objQMS050.BU && c.Location == objQMS050.Location && c.IQualityProject == objQMS050.QualityProject).Select(x => x.QualityProject).ToList());

                var QualityId = db.QMS020.FirstOrDefault(x => x.QualityProject == objQMS050.QualityProject && x.SeamNo == objQMS050.SeamNo && x.Location == objQMS050.Location).QualityId;
                if (!string.IsNullOrWhiteSpace(QualityId))
                {
                    var objTemp = (from a in db.QMS015_Log
                                   join b in db.QMS016_Log on a.Id equals b.RefId
                                   where a.Status == "Approved" && lstQproj.Contains(a.QualityProject) && b.StageCode == objQMS050.StageCode && b.QualityId == QualityId
                                   select b).FirstOrDefault();

                    FerriteMinValue = objTemp != null ? Convert.ToString(objTemp.FerriteMinValue) : "";
                    FerriteMaxValue = objTemp != null ? Convert.ToString(objTemp.FerriteMaxValue) : "";
                    FerriteUnit = objTemp != null ? Convert.ToString(objTemp.FerriteUnit) : "";
                }
            }
            // WeldingProcessForFerriteList,
            return Json(new
            {
                res = "1",
                msg = "Success",
                QMS050 = objQMS050,
                Stage,
                WeldingProcessForFerriteTest,
                InstrumentList,
                Instrument,
                InstrumentDesc,
                InstrumentCode,
                FerriteMinValue,
                FerriteMaxValue,
                FerriteUnit,
                FerriteHeaderID
            }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult LoadFerriteGridDataPartial(int HeaderId, int RequestId, bool IsEditable)
        {
            ViewBag.HeaderId = HeaderId;
            ViewBag.RequestId = RequestId;
            ViewBag.IsEditable = IsEditable;

            return PartialView("_GetFerriteGridDataPartial");
        }

        [HttpPost]
        public JsonResult LoadFerriteGridData(JQueryDataTableParamModel param, int HeaderId, int RequestId, bool IsEditable)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                #endregion

                strWhereCondition = " 1=1 and HeaderId = " + HeaderId + " and RefRequestId = " + RequestId;

                //search Condition 
                string[] columnName = { "SpotNo", "WeldingProcess", "Reading1", "Reading2", "Reading3", "Reading4", "Reading5", "Reading6", "Average", "Remarks" };

                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                var lstResult = db.SP_IPI_GET_FERRITE_TEST_DETAIL_DATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();
                int newRecordId = 0;
                var newRecord = new[] {
                            clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                            clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                            Helper.GenerateTextbox(newRecordId, "SpotNo", "","OnBlurSpotNo(this)",false,"","",""),
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                    };

                var data = (from uc in lstResult
                            select new[]{
                            clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                            clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                            IsEditable ? Helper.GenerateTextbox(Convert.ToInt32(uc.FerriteId), "SpotNo", Convert.ToString(uc.SpotNo),"",true,"","","") : Convert.ToString(uc.SpotNo),
                            IsEditable ? Helper.GenerateTextbox(Convert.ToInt32(uc.FerriteId), "WeldingProcess", uc.WeldingProcess,"",true,"","","") : uc.WeldingProcess,
                            IsEditable ? Helper.GenerateNumericTextbox(uc.FerriteId, "Reading1_",Convert.ToString(uc.Reading1), "UpdateFerriteData(this, " + uc.FerriteId + ",true);",false,"","","clsReading") : Convert.ToString(uc.Reading1),
                            IsEditable ? Helper.GenerateNumericTextbox(uc.FerriteId, "Reading2_", Convert.ToString(uc.Reading2), "UpdateFerriteData(this, " + uc.FerriteId + ",true);",false,"","","clsReading") : Convert.ToString(uc.Reading2),
                            IsEditable ? Helper.GenerateNumericTextbox(uc.FerriteId, "Reading3_", Convert.ToString(uc.Reading3), "UpdateFerriteData(this, " + uc.FerriteId + ",true);",false,"","","clsReading") : Convert.ToString(uc.Reading3),
                            IsEditable ? Helper.GenerateNumericTextbox(uc.FerriteId, "Reading4_", Convert.ToString(uc.Reading4), "UpdateFerriteData(this, " + uc.FerriteId + ",true);",false,"","","clsReading") : Convert.ToString(uc.Reading4),
                            IsEditable ? Helper.GenerateNumericTextbox(uc.FerriteId, "Reading5_", Convert.ToString(uc.Reading5), "UpdateFerriteData(this, " + uc.FerriteId + ",true);",false,"","","clsReading") : Convert.ToString(uc.Reading5),
                            IsEditable ? Helper.GenerateNumericTextbox(uc.FerriteId, "Reading6_", Convert.ToString(uc.Reading6), "UpdateFerriteData(this, " + uc.FerriteId + ",true);",false,"","","clsReading") : Convert.ToString(uc.Reading6),
                            IsEditable ? Helper.GenerateNumericTextbox(uc.FerriteId, "Average", Convert.ToString(uc.Average), "UpdateFerriteData(this, " + uc.FerriteId + ",true);",true,"","","") : Convert.ToString(uc.Average),
                            IsEditable ? Helper.GenerateTextbox(uc.FerriteId, "Remarks",uc.Remarks, "UpdateFerriteData(this, " + uc.FerriteId + ",false);",false,"","500","") : uc.Remarks,
                            Helper.HTMLActionString(uc.FerriteId, "Delete", "Delete Record", "fa fa-trash-o", "DeleteUtilityGridData("+uc.FerriteId+",'/IPI/MaintainSeamListOfferInspection/DeleteFerriteTestDetail',null,'tblFerriteTestLines');","",IsEditable ? false : true),
                        }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetInlineEditableRowForFerrite(int id, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();
                var result = new List<string>();
                if (id == 0)
                {
                    int newRecordId = 0;
                    var newRecord = new[] {
                            clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                            clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                            Helper.GenerateTextbox(newRecordId, "SpotNo", "","OnBlurSpotNo(this)",false,"","",""),
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                    };
                    data.Add(newRecord);
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdateFerriteData(int FerriteId, string ColumnName, string ColumnValue, string Average)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string tableName = "QMS053";
                if (ColumnName.EndsWith("_"))
                    ColumnName = ColumnName.Substring(0, ColumnName.Length - 1);

                if (!string.IsNullOrEmpty(ColumnName))
                {
                    db.SP_COMMON_TABLE_UPDATE(tableName, Convert.ToInt32(FerriteId), "FerriteId", ColumnName, ColumnValue, objClsLoginInfo.UserName);

                    if (ColumnName.ToLower().Contains("reading"))
                    {
                        db.SP_COMMON_TABLE_UPDATE(tableName, Convert.ToInt32(FerriteId), "FerriteId", "Average", Average, objClsLoginInfo.UserName);
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddFerriteTestDefaultLineData(int RequestId, string SpotNo, string InstrumentCode)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                List<QMS053> objQMS053List = new List<QMS053>();
                List<string> alreadyExists = new List<string>();
                QMS053_1 objQMS050 = db.QMS053_1.Where(x => x.RefRequestId == RequestId).OrderByDescending(x => x.FerriteId).FirstOrDefault();
                if (objQMS050 != null)
                {
                    //List<string> lstWeldingProcess = !string.IsNullOrWhiteSpace(objQMS050.ApplicableWeldingProcess) ? objQMS050.ApplicableWeldingProcess.Split(',').ToList() : null;//GetWeldingProcessForFerriteTest(objQMS050.QualityProject, objQMS050.Project, objQMS050.SeamNo, objQMS050.BU);
                    List<string> lstWeldingProcess = GetWeldingProcessForFerriteTest(objQMS050.QMS050.QualityProject, objQMS050.QMS050.Project, objQMS050.QMS050.SeamNo, objQMS050.QMS050.BU);
                    if (lstWeldingProcess.Count > 0)
                    {
                        foreach (var weldingprocess in lstWeldingProcess)
                        {
                            int spt = Convert.ToInt32(SpotNo);
                            if (!db.QMS053.Where(x => x.RefRequestId == RequestId && x.SpotNo == spt && x.WeldingProcess == weldingprocess).Any())
                            {
                                objQMS053List.Add(new QMS053()
                                {
                                    HeaderId = objQMS050.HeaderId,
                                    RefRequestId = objQMS050.RefRequestId,
                                    SpotNo = Convert.ToInt32(SpotNo),
                                    WeldingProcess = weldingprocess,
                                    CreatedBy = objClsLoginInfo.UserName,
                                    CreatedOn = DateTime.Now,
                                    Instrument = InstrumentCode
                                });
                            }
                            else { alreadyExists.Add(weldingprocess); }
                        }
                        if (objQMS053List.Count() > 0)
                        {
                            db.QMS053.AddRange(objQMS053List);
                            db.SaveChanges();
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                        }
                        if (alreadyExists.Count() > 0)
                        {
                            objResponseMsg.dataKey = true;
                            objResponseMsg.dataValue = " Welding Process :" + string.Join(",", alreadyExists.ToList()) + " already exists !!";
                        }
                        objResponseMsg.Key = true;

                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Welding Process not found";
                    }
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteFerriteTestDetail(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objQMS053 = db.QMS053.Where(x => x.FerriteId == Id).FirstOrDefault();
                if (objQMS053 != null)
                {
                    db.QMS053.Remove(objQMS053);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Data not found";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public List<string> GetWeldingProcessForFerriteTest(string QualityProject, string Project, string SeamNo, string BU)
        {
            return db.WPS025.Where(x => x.QualityProject == QualityProject && x.Project == Project && x.SeamNo == SeamNo && x.BU == BU)
                             .OrderBy(o => o.PrintedOn)
                             .Select(x => x.WeldingProcess)
                             .Distinct().ToList();
        }

        public ActionResult UpdateInstrumentDatainFerrite(int reqid, string param)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
               // List<int> arrayRequestIds = RequestIds.Split(',').Select(int.Parse).ToList();
                //if (currentPos == "0")
                //{
                //    foreach (var item in arrayRequestIds)
                //    {
                //        QMS053_1 objQMS053_1 = db.QMS053_1.Where(i => i.RefRequestId == item).FirstOrDefault();
                //        if (objQMS053_1 != null)
                //        {
                //            objQMS053_1.Instrument = param;
                //            objQMS053_1.EditedBy = objClsLoginInfo.UserName;
                //            objQMS053_1.EditedOn = DateTime.Now;
                //            db.SaveChanges();
                //            objResponseMsg.Key = true;
                //            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                //        }
                //        else
                //        {
                //            objResponseMsg.Key = false;
                //            objResponseMsg.Value = "Sample Test result data not found";
                //        }
                //    }
                //}
                //else
                //{
                    QMS053_1 objQMS053_1 = db.QMS053_1.Where(i => i.RefRequestId == reqid).FirstOrDefault();
                    if (objQMS053_1 != null)
                    {
                        objQMS053_1.Instrument = param;
                        objQMS053_1.EditedBy = objClsLoginInfo.UserName;
                        objQMS053_1.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Sample Test result data not found";
                    }
               // }


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        // ALSO USED IN MOBILE APPLICATION
        [HttpPost]
        public ActionResult UpdateApplicableWeldingProcessFerrite(int reqid, string param, string APIRequestFrom, string UserName)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS053_1 objQMS053_1 = db.QMS053_1.Where(i => i.RefRequestId == reqid).FirstOrDefault();
                if (objQMS053_1 != null)
                {
                    objQMS053_1.ApplicableWeldingProcess = param;
                    objQMS053_1.EditedBy = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                    objQMS053_1.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Sample Test result data not found";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        [HttpPost]
        public ActionResult UpdateMaterial(int Id, string material, bool IsOffer)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (IsOffer)
                {
                    QMS040 objQMS040 = db.QMS040.Where(x => x.HeaderId == Id).FirstOrDefault();
                    if (objQMS040 != null)
                    {
                        if (!string.IsNullOrEmpty(material))
                        {
                            objQMS040.Material = material;
                        }
                        objQMS040.EditedBy = objClsLoginInfo.UserName;
                        objQMS040.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Sample Test result data not found";
                    }
                }
                else
                {
                    QMS060 objQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == Id);
                    if (objQMS060 != null)
                    {
                        if (!string.IsNullOrEmpty(material))
                        {
                            objQMS060.Part6Material = material;
                        }
                        objQMS060.EditedBy = objClsLoginInfo.UserName;
                        objQMS060.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Sample Test result data not found";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ShowMultipleStagesGroupClearance(string RequestIds, string CheckStageType, string multipleIds)
        {
            System.Threading.Thread.Sleep(1000);


            QMS050 objQMS050 = new QMS050();
            ViewBag.MultipleReqId = RequestIds;
            List<int> arrayRequestIds = RequestIds.Split(',').Select(int.Parse).ToList();
            string[] Ids = multipleIds.Split(',');
            int topRequestId = Convert.ToInt32(Ids[0]);
            int requestId = arrayRequestIds.FirstOrDefault();
            if (CheckStageType == "HARDNESS")
            {

                if (requestId > 0)
                {
                    objQMS050 = db.QMS050.Where(x => x.RequestId == requestId).FirstOrDefault();
                }
                ViewBag.Stage = db.QMS002.Where(x => x.StageCode == objQMS050.StageCode && x.BU == objQMS050.BU && x.Location == objQMS050.Location).Select(x => x.StageCode + "-" + x.StageDesc).FirstOrDefault();
                ViewBag.HeaderId = objQMS050.HeaderId;
                ViewBag.RequestId = objQMS050.RequestId;
                ViewBag.IterationNo = objQMS050.IterationNo;
                ViewBag.IsEditable = true;

                List<GLB002> lstInstrument = Manager.GetSubCatagories("Instrument Group", objQMS050.BU, objQMS050.Location, null);
                ViewBag.InstrumentList = lstInstrument.AsEnumerable().Select(x => new { CatID = x.Code, CatDesc = x.Description, id = x.Code, text = x.Description }).ToList();

                ViewBag.HardnessId = 0;
                ViewBag.Instrument = string.Empty;

                ViewBag.InstrumentCode = string.Empty;
                QMS052 objQMS052 = db.QMS052.Where(x => x.RefRequestId == requestId).FirstOrDefault();
                string InstrumentCodes = string.Empty;
                if (topRequestId != Convert.ToInt32(RequestIds))
                {
                    //InstrumentCodes = objQMS054.Instrument;
                    var testInstrumentData = db.QMS052.FirstOrDefault(a => a.RefRequestId == topRequestId);
                    if (testInstrumentData != null)
                    {
                        InstrumentCodes = testInstrumentData.Instrument;
                    }
                }
                if (objQMS052 == null)
                {
                    db.QMS052.Add(new QMS052
                    {
                        HeaderId = objQMS050.HeaderId,
                        Instrument = InstrumentCodes,
                        RefRequestId = objQMS050.RequestId,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    db.SaveChanges();
                    objQMS052 = db.QMS052.Where(x => x.RefRequestId == requestId).FirstOrDefault();
                }

                ViewBag.HardnessId = objQMS052?.HardnessId;
                string InstrumentCode = string.Empty;
                if (topRequestId == Convert.ToInt32(RequestIds))
                {
                    InstrumentCode = objQMS052.Instrument;
                }
                else
                {
                    var testInstrumentData = db.QMS052.FirstOrDefault(a => a.RefRequestId == topRequestId);
                    if (testInstrumentData != null)
                    {
                        InstrumentCode = testInstrumentData.Instrument;
                    }
                }

                if (!string.IsNullOrWhiteSpace(InstrumentCode))
                {
                    //string[] arr = objQMS052.Instrument.Split(',');
                    string[] arr = InstrumentCode.Split(',');
                    ViewBag.InstrumentDesc = string.Join(",", lstInstrument.Where(x => arr.Contains(x.Code)).Select(x => x.Description).ToList());
                }
                ViewBag.InstrumentCode = InstrumentCode;
                int? objHardnessRequiredValue = 0;
                string hardnessUnit = string.Empty;
                var objTestPlan = db.QMS021.Where(x => x.QualityProject == objQMS050.QualityProject && x.SeamNo == objQMS050.SeamNo && x.Location == objQMS050.Location && x.StageCode == objQMS050.StageCode && x.StageSequance == objQMS050.StageSequence).FirstOrDefault();
                if (objTestPlan != null ? (objTestPlan.HardnessRequiredValue != null && objTestPlan.HardnessUnit != null) : false)
                {
                    objHardnessRequiredValue = objTestPlan.HardnessRequiredValue;
                    hardnessUnit = objTestPlan.HardnessUnit;
                }
                else
                {
                    List<string> lstQproj = new List<string>();
                    lstQproj.Add(objQMS050.QualityProject);
                    lstQproj.AddRange(db.QMS017.Where(c => c.BU == objQMS050.BU && c.Location == objQMS050.Location && c.IQualityProject == objQMS050.QualityProject).Select(x => x.QualityProject).ToList());
                    var QualityId = db.QMS020.FirstOrDefault(x => x.QualityProject == objQMS050.QualityProject && x.SeamNo == objQMS050.SeamNo && x.Location == objQMS050.Location).QualityId;
                    if (!string.IsNullOrWhiteSpace(QualityId))
                    {
                        var obj = (from a in db.QMS015_Log
                                   join b in db.QMS016_Log on a.Id equals b.RefId
                                   where a.Status == "Approved" && lstQproj.Contains(a.QualityProject) && b.StageCode == objQMS050.StageCode && b.QualityId == QualityId
                                   select b).FirstOrDefault();
                        objHardnessRequiredValue = obj?.HardnessRequiredValue;
                        hardnessUnit = obj?.HardnessUnit;
                    }
                }
                ViewBag.HardnessRequiredValue = objHardnessRequiredValue != null ? Convert.ToString(objHardnessRequiredValue) : "";
                ViewBag.HardnessRequiredUnit = hardnessUnit != null ? Convert.ToString(hardnessUnit) : "";

                return PartialView("_LoadHardnessTestDetailsPartial", objQMS050);
            }
            else if (CheckStageType == "FERRITE")
            {

                if (requestId > 0)
                {
                    objQMS050 = db.QMS050.Where(x => x.RequestId == requestId).FirstOrDefault();
                }
                ViewBag.Stage = db.QMS002.Where(x => x.StageCode == objQMS050.StageCode && x.BU == objQMS050.BU && x.Location == objQMS050.Location).Select(x => x.StageCode + "-" + x.StageDesc).FirstOrDefault();
                //ViewBag.Project = db.COM001.Where(i => i.t_cprj.Equals(objQMS050.Project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + "-" + i.t_dsca).FirstOrDefault();
                //ViewBag.Location = db.COM002.Where(a => a.t_dimx == objQMS050.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                //ViewBag.BU = db.COM002.Where(a => a.t_dimx == objQMS050.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                ViewBag.HeaderId = objQMS050.HeaderId;
                ViewBag.RequestId = objQMS050.RequestId;
                ViewBag.IterationNo = objQMS050.IterationNo;
                ViewBag.IsEditable = true;

                List<string> lstWeldingProcess = GetWeldingProcessForFerriteTest(objQMS050.QualityProject, objQMS050.Project, objQMS050.SeamNo, objQMS050.BU);
                ViewBag.WeldingProcessForFerriteTest = string.Join(",", lstWeldingProcess);
                ViewBag.WeldingProcessForFerriteList = lstWeldingProcess.AsEnumerable().Select(x => new { id = x.ToString(), text = x.ToString() }).ToList();
                List<GLB002> lstInstrument = Manager.GetSubCatagories("Instrument Group", objQMS050.BU, objQMS050.Location, null);
                var InstrumentList = lstInstrument.AsEnumerable().Select(x => new { CatID = x.Code, CatDesc = x.Description, id = x.Code, text = x.Description }).ToList();
                QMS053_1 objQMS053_1 = new QMS053_1();
                objQMS053_1 = db.QMS053_1.Where(x => x.HeaderId == objQMS050.HeaderId && x.RefRequestId == objQMS050.RequestId).FirstOrDefault();
                string InstrumentCodes = string.Empty;
                if (topRequestId != Convert.ToInt32(RequestIds))
                {
                    //InstrumentCodes = objQMS054.Instrument;
                    var testInstrumentData = db.QMS053_1.FirstOrDefault(a => a.RefRequestId == topRequestId);
                    if (testInstrumentData != null)
                    {
                        InstrumentCodes = testInstrumentData.Instrument;
                    }
                }

                if (objQMS053_1 == null)
                {
                    objQMS053_1 = new QMS053_1();
                    
                    objQMS053_1.HeaderId = objQMS050.HeaderId;
                    objQMS053_1.RefRequestId = objQMS050.RequestId;
                    objQMS053_1.Instrument = InstrumentCodes;
                    objQMS053_1.CreatedBy = objClsLoginInfo.UserName;
                    objQMS053_1.CreatedOn = DateTime.Now;
                    db.QMS053_1.Add(objQMS053_1);
                    db.SaveChanges();
                    objQMS053_1 = db.QMS053_1.Where(x => x.HeaderId == objQMS050.HeaderId && x.RefRequestId == objQMS050.RequestId).FirstOrDefault();
                }
                if (!string.IsNullOrWhiteSpace(objQMS053_1.ApplicableWeldingProcess))
                {
                    ViewBag.WeldingProcessForFerriteTest = objQMS053_1.ApplicableWeldingProcess;
                }
                ViewBag.FerriteHeaderID = objQMS053_1.FerriteId;
                string Instrument = string.Empty;
                string InstrumentDesc = string.Empty;
                string InstrumentCode = string.Empty;
                if (topRequestId == Convert.ToInt32(RequestIds))
                {
                    InstrumentCode = objQMS053_1.Instrument;
                }
                else
                {
                    var testInstrumentData = db.QMS053_1.FirstOrDefault(a => a.RefRequestId == topRequestId);
                    if (testInstrumentData != null)
                    {
                        InstrumentCode = testInstrumentData.Instrument;
                    }
                }

                if (!string.IsNullOrWhiteSpace(InstrumentCode))
                {
                    //string[] arr = objQMS053_1.Instrument.Split(',');
                    string[] arr = InstrumentCode.Split(',');
                    InstrumentDesc = string.Join(",", lstInstrument.Where(x => arr.Contains(x.Code)).Select(x => x.Description).ToList());
                }
                ViewBag.InstrumentCode = InstrumentCode;
                var objTestPlan = db.QMS021.Where(x => x.QualityProject == objQMS050.QualityProject && x.SeamNo == objQMS050.SeamNo && x.Location == objQMS050.Location && x.StageCode == objQMS050.StageCode && x.StageSequance == objQMS050.StageSequence).FirstOrDefault();
                if (objTestPlan != null ? (objTestPlan.FerriteMaxValue != null && objTestPlan.FerriteMinValue != null && objTestPlan.FerriteUnit != null) : false)
                {
                    if (objTestPlan.FerriteMinValue.HasValue)
                    {
                        ViewBag.FerriteMinValue = objTestPlan != null ? Convert.ToString(objTestPlan.FerriteMinValue) : "";
                        ViewBag.FerriteMaxValue = objTestPlan != null ? Convert.ToString(objTestPlan.FerriteMaxValue) : "";
                        ViewBag.FerriteUnit = objTestPlan != null ? Convert.ToString(objTestPlan.FerriteUnit) : "";
                    }
                }
                else
                {
                    List<string> lstQproj = new List<string>();
                    lstQproj.Add(objQMS050.QualityProject);
                    lstQproj.AddRange(db.QMS017.Where(c => c.BU == objQMS050.BU && c.Location == objQMS050.Location && c.IQualityProject == objQMS050.QualityProject).Select(x => x.QualityProject).ToList());

                    var QualityId = db.QMS020.FirstOrDefault(x => x.QualityProject == objQMS050.QualityProject && x.SeamNo == objQMS050.SeamNo && x.Location == objQMS050.Location).QualityId;
                    if (!string.IsNullOrWhiteSpace(QualityId))
                    {
                        var objTemp = (from a in db.QMS015_Log
                                       join b in db.QMS016_Log on a.Id equals b.RefId
                                       where a.Status == "Approved" && lstQproj.Contains(a.QualityProject) && b.StageCode == objQMS050.StageCode && b.QualityId == QualityId
                                       select b).FirstOrDefault();

                        ViewBag.FerriteMinValue = objTemp != null ? Convert.ToString(objTemp.FerriteMinValue) : "";
                        ViewBag.FerriteMaxValue = objTemp != null ? Convert.ToString(objTemp.FerriteMaxValue) : "";
                        ViewBag.FerriteUnit = objTemp != null ? Convert.ToString(objTemp.FerriteUnit) : "";
                    }
                }
                ViewBag.InstrumentList = InstrumentList;
                ViewBag.Instrument = Instrument;
                ViewBag.InstrumentDesc = InstrumentDesc;
                return PartialView("_LoadFerriteTestDetailsPartial", objQMS050);
            }
            else if (CheckStageType == "PMI")
            {
                NDEModels objNDEModels = new NDEModels();
                string[] arrqElement = new string[6];
                double[] arrMin = new double[6];
                double[] arrMax = new double[6];
                int j = 0;
                if (requestId > 0)
                {
                    objQMS050 = db.QMS050.Where(x => x.RequestId == requestId).FirstOrDefault();
                }
                ViewBag.Stage = db.QMS002.Where(x => x.StageCode == objQMS050.StageCode && x.BU == objQMS050.BU && x.Location == objQMS050.Location).Select(x => x.StageCode + "-" + x.StageDesc).FirstOrDefault();
                ViewBag.HeaderId = objQMS050.HeaderId;
                ViewBag.RequestId = objQMS050.RequestId;
                ViewBag.IterationNo = objQMS050.IterationNo;

                string RoletoEnableEditForPMI = clsImplementationEnum.ConfigParameter.RoletoEnableEditForPMI.GetStringValue();
                var roles = Manager.GetConfigValue(RoletoEnableEditForPMI).ToUpper().Split(',').ToArray();
                bool flg = objClsLoginInfo.GetUserRoleList().Where(x => roles.Contains(x)).Any();
                ViewBag.IsEditable = (objQMS050.TestResult == clsImplementationEnum.SeamListInspectionStatus.Cleared.GetStringValue()) ? flg : true;
                ViewBag.IsDeleteEditable = true;
                List<GLB002> lstInstrument = Manager.GetSubCatagories("Instrument Group", objQMS050.BU, objQMS050.Location, null);
                ViewBag.InstrumentList = lstInstrument.AsEnumerable().Select(x => new { CatID = x.Code, CatDesc = x.Description, id = x.Code, text = x.Description }).ToList();
                ViewBag.PMIId = 0;
                ViewBag.Instrument = string.Empty;
                ViewBag.InstrumentCode = string.Empty;
                QMS054 objQMS054 = db.QMS054.Where(x => x.RefRequestId == requestId).FirstOrDefault();


                var objTestPlan = db.QMS021.Where(x => x.QualityProject == objQMS050.QualityProject && x.SeamNo == objQMS050.SeamNo && x.Location == objQMS050.Location && x.StageCode == objQMS050.StageCode && x.StageSequance == objQMS050.StageSequence).FirstOrDefault();
                if (objTestPlan != null)
                {
                    var lstQMS023 = objTestPlan != null ? db.QMS023.Where(i => i.TPLineId == objTestPlan.LineId).OrderByDescending(x => x.TPRevNo).ToList() : null;
                    if (lstQMS023.Count() > 0 && lstQMS023 != null)
                    {
                        #region Fetch element data from Test Plan(QMS022/QMS023)
                        foreach (var item in lstQMS023)
                        {
                            if (objQMS054 == null)
                            {
                                arrqElement[j] = item.Element;
                            }
                            arrMin[j] = Convert.ToDouble(item.ReqMinValue);
                            arrMax[j] = Convert.ToDouble(item.ReqMaxValue);
                            if (j == 5)
                            {
                                break;
                            }
                            j = j + 1;
                            if (j == 6)
                            { break; }
                        }
                        #endregion
                    }
                    else
                    {
                        #region fetch data from QID
                        List<string> lstQproj = new List<string>();
                        lstQproj.Add(objQMS050.QualityProject);
                        lstQproj.AddRange(db.QMS017.Where(c => c.BU == objQMS050.BU && c.Location == objQMS050.Location && c.IQualityProject == objQMS050.QualityProject).Select(x => x.QualityProject).ToList());

                        var QualityId = db.QMS020.FirstOrDefault(x => x.QualityProject == objQMS050.QualityProject && x.SeamNo == objQMS050.SeamNo && x.Location == objQMS050.Location).QualityId;
                        var Qrev = QualityId != null ? db.QMS015.FirstOrDefault(x => x.QualityId == QualityId && lstQproj.Contains(x.QualityProject) && x.Location == objQMS050.Location).RevNo : 0;

                        if (!string.IsNullOrWhiteSpace(QualityId))
                        {
                            var lstQMS019 = db.QMS019.Where(i => lstQproj.Contains(i.QualityProject) //&& i.Project == objQMS040.Project
                                                            && i.BU == objQMS050.BU && i.Location == objQMS050.Location && i.QualityId == QualityId && i.QualityIdRev == Qrev
                                                            && i.StageCode == objQMS050.StageCode
                                                        ).ToList();

                            if (lstQMS019.Count() > 0)
                            {
                                int i = 0;
                                foreach (var item in lstQMS019)
                                {
                                    if (objQMS054 == null)
                                    {
                                        arrqElement[i] = item.Element;
                                    }
                                    arrMin[i] = Convert.ToDouble(item.ReqMinValue);
                                    arrMax[i] = Convert.ToDouble(item.ReqMaxValue);
                                    i = i + 1;
                                }
                            }

                        }
                        #endregion
                    }
                }
                ViewBag.ReqMinValue = arrMin;
                ViewBag.ReqMaxValue = arrMax;
                string InstrumentCodes = string.Empty;
                if (topRequestId != Convert.ToInt32(RequestIds))
                {
                    //InstrumentCodes = objQMS054.Instrument;
                    var testInstrumentData = db.QMS054.FirstOrDefault(a => a.RefRequestId == topRequestId);
                    if (testInstrumentData != null)
                    {
                        InstrumentCodes = testInstrumentData.Instrument;
                    }
                }
                if (objQMS054 == null)
                {
                    db.QMS054.Add(new QMS054
                    {
                        HeaderId = objQMS050.HeaderId,
                        Instrument = InstrumentCodes,
                        RefRequestId = objQMS050.RequestId,
                        Element1 = arrqElement[0],
                        Element2 = arrqElement[1],
                        Element3 = arrqElement[2],
                        Element4 = arrqElement[3],
                        Element5 = arrqElement[4],
                        Element6 = arrqElement[5],
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });;
                    db.SaveChanges();
                    objQMS054 = db.QMS054.Where(x => x.RefRequestId == requestId).FirstOrDefault();
                }
                if (objQMS054 != null)
                {
                    ViewBag.PMIId = objQMS054.PMIId;
                    string InstrumentCode = string.Empty;
                    if (topRequestId == Convert.ToInt32(RequestIds))
                    {
                        InstrumentCode = objQMS054.Instrument;
                    }
                    else
                    {
                        var testInstrumentData = db.QMS054.FirstOrDefault(a => a.RefRequestId == topRequestId);
                        if (testInstrumentData != null)
                        {
                            InstrumentCode = testInstrumentData.Instrument;
                        }
                    }
                   // InstrumentCode = objQMS054.Instrument;
                    if (!string.IsNullOrWhiteSpace(InstrumentCode))
                    {
                        //string[] arr = objQMS054.Instrument.Split(',');
                        string[] arr = InstrumentCode.Split(',');
                        ViewBag.InstrumentDesc = string.Join(",", lstInstrument.Where(x => arr.Contains(x.Code)).Select(x => x.Description).ToList());
                    }
                    ViewBag.InstrumentCode = InstrumentCode;
                    ViewBag.Element1 = objNDEModels.GetCategory("Element", objQMS054.Element1, objQMS050.BU.Trim(), objQMS050.Location.Trim(), true).CategoryDescription;
                    ViewBag.Element2 = objNDEModels.GetCategory("Element", objQMS054.Element2, objQMS050.BU.Trim(), objQMS050.Location.Trim(), true).CategoryDescription;
                    ViewBag.Element3 = objNDEModels.GetCategory("Element", objQMS054.Element3, objQMS050.BU.Trim(), objQMS050.Location.Trim(), true).CategoryDescription;
                    ViewBag.Element4 = objNDEModels.GetCategory("Element", objQMS054.Element4, objQMS050.BU.Trim(), objQMS050.Location.Trim(), true).CategoryDescription;
                    ViewBag.Element5 = objNDEModels.GetCategory("Element", objQMS054.Element5, objQMS050.BU.Trim(), objQMS050.Location.Trim(), true).CategoryDescription;
                    ViewBag.Element6 = objNDEModels.GetCategory("Element", objQMS054.Element6, objQMS050.BU.Trim(), objQMS050.Location.Trim(), true).CategoryDescription;
                }



                return PartialView("_GetPMIGridDataPartia", objQMS050);
            }
            else
            {
                if (requestId > 0)
                {
                    objQMS050 = db.QMS050.Where(x => x.RequestId == requestId).FirstOrDefault();
                }
                QMS040 objQMS040 = new QMS040();
                string Instrument = string.Empty, InstrumentDesc = string.Empty;
                if (objQMS050.HeaderId > 0)
                {
                    objQMS040 = db.QMS040.Where(i => i.HeaderId == objQMS050.HeaderId).FirstOrDefault();
                }
                List<GLB002> lstInstrument = Manager.GetSubCatagories("Instrument Group", objQMS040.BU, objQMS040.Location, null);
                var InstrumentList = lstInstrument.AsEnumerable().Select(x => new { CatID = x.Code, CatDesc = x.Description, id = x.Code, text = x.Description }).ToList();

                if (objQMS050.HeaderId > 0)
                {
                   
                    if (topRequestId == Convert.ToInt32(RequestIds))
                    {
                        var chemDetail = db.QMS051.Where(i => i.RefRequestId == objQMS050.RequestId).ToList();
                        if (chemDetail.Count > 0)
                        {
                            chemDetail.ForEach(x => x.IterationNo = objQMS050.IterationNo);
                            Instrument = chemDetail.Select(x => x.Instrument).FirstOrDefault();
                            InstrumentDesc = InstrumentList.Count() > 0 ? InstrumentList.Where(x => x.CatID == Instrument).Select(x => x.CatDesc).FirstOrDefault() : "";
                            db.SaveChanges();
                        }
                    }
                    else
                    {
                        var getDataList = db.QMS051.Where(a => a.RefRequestId == topRequestId).ToList();
                        getDataList.ForEach(x => x.IterationNo = objQMS050.IterationNo);
                        Instrument = getDataList.Select(a => a.Instrument).FirstOrDefault();
                        InstrumentDesc = InstrumentList.Count() > 0 ? InstrumentList.Where(x => x.CatID == Instrument).Select(x => x.CatDesc).FirstOrDefault() : "";
                       
                    }
                   
                }


                string InstrumentCode = Instrument;


                if (!string.IsNullOrWhiteSpace(InstrumentCode))
                {
                    string[] arr = Instrument.Split(',');
                    ViewBag.InstrumentDesc = string.Join(",", lstInstrument.Where(x => arr.Contains(x.Code)).Select(x => x.Description).ToList());
                }
                ViewBag.InstrumentCode = InstrumentCode;

                ViewBag.InstrumentList = InstrumentList;
                ViewBag.InstrumentDesc = InstrumentDesc;

                ViewBag.Stage = db.QMS002.Where(x => x.StageCode == objQMS040.StageCode && x.BU == objQMS040.BU && x.Location == objQMS040.Location).Select(x => x.StageCode + "-" + x.StageDesc).FirstOrDefault();
                ViewBag.iteration = objQMS050.IterationNo;
                ViewBag.Project = db.COM001.Where(i => i.t_cprj.Equals(objQMS040.Project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + "-" + i.t_dsca).FirstOrDefault();
                ViewBag.Location = db.COM002.Where(a => a.t_dimx == objQMS040.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                ViewBag.BU = db.COM002.Where(a => a.t_dimx == objQMS040.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                ViewBag.Role = "QC";
                ViewBag.reqid = objQMS050.RequestId;
                ViewBag.isEditable = true;
                ViewBag.isHistory = false;
                return PartialView("~/Areas/IPI/Views/ChemicalAnalysis/_LoadChemicalTestDetailsPartial.cshtml", objQMS040);
            }


        }

        //Added By Deval Barot on 13-03-2020
        [SessionExpireFilter]
        public ActionResult PendingTPI(string tab)
        {
            ViewBag.Tab = tab;
            ViewBag.Location = objClsLoginInfo.Location;
            return View();
        }

        public ActionResult GetSeamListInspectionDataPartial(string status, string qualityProject = "", string seamNumber = "")
        {
            ViewBag.Status = status;
            ViewBag.QualityProject = qualityProject;
            ViewBag.SeamNumber = seamNumber;
            return PartialView("_GetSeamListInspectionDataPartial");
        }

        public ActionResult loadSeamListDataTable(JQueryDataTableParamModel param)
        {
            try
            {
                var HostName = ConfigurationManager.AppSettings["Applicaiton_URL"];
                var SignUrl = ConfigurationManager.AppSettings["SignUrl"];
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                string _urlFrom = "all";
                //var list = new List<QMS050>();

                string whereCondition = "1=1";
                if (param.Status.ToUpper() == "PENDING")
                {
                    //list = db.QMS050.Where(x => x.IsTPIPending == true && x.OfferedtoTPIBy == null && x.TestResult == "Cleared").ToList();
                    whereCondition += " and IsTPIPending = 1 and OfferedtoTPIBy IS NULL and TestResult = 'Cleared'";
                    _urlFrom = "pending";
                    ViewBag.Status = "Pending";
                }
                else if (param.Status.ToUpper() == "RETURN")
                {
                    //list = db.QMS050.Where(x => (x.TPIAgency1Result.Equals("Returned") || x.TPIAgency2Result.Equals("Returned") || x.TPIAgency3Result.Equals("Returned") || x.TPIAgency4Result.Equals("Returned") || x.TPIAgency5Result.Equals("Returned") || x.TPIAgency6Result.Equals("Returned")) && x.TestResult == "Cleared" && x.IsTPIResultMaintained == true).ToList();
                    whereCondition += " and (TPIAgency1Result = 'Returned' or TPIAgency2Result = 'Returned' or TPIAgency3Result = 'Returned' or TPIAgency4Result = 'Returned' or TPIAgency5Result = 'Returned' or TPIAgency6Result = 'Returned') and TestResult = 'Cleared' and IsTPIResultMaintained = 1";
                    _urlFrom = "return";
                    ViewBag.Status = "Return";
                }
                else if (param.Status.ToUpper() == "CLEAR")
                {
                    //list = db.QMS050.Where(x => (x.TPIAgency1Result.Equals("Cleared") || x.TPIAgency2Result.Equals("Cleared") || x.TPIAgency3Result.Equals("Cleared") || x.TPIAgency4Result.Equals("Cleared") || x.TPIAgency5Result.Equals("Cleared") || x.TPIAgency6Result.Equals("Cleared")) && x.TestResult == "Cleared" && x.IsTPIResultMaintained == true).ToList();
                    //whereCondition += " and (TPIAgency1Result = 'Cleared' or TPIAgency2Result = 'Cleared' or TPIAgency3Result = 'Cleared' or TPIAgency4Result = 'Cleared' or TPIAgency5Result = 'Cleared' or TPIAgency6Result = 'Cleared') and TestResult = 'Cleared' and IsTPIResultMaintained = 1 and ISNULL(IsSubmitToQA, 0) = 0 ";
                    whereCondition += " and (not (isnull(TPIAgency1Result,'') = 'Returned' or isnull(TPIAgency2Result,'') = 'Returned' or isnull(TPIAgency3Result,'') = 'Returned' or isnull(TPIAgency4Result,'') = 'Returned' or isnull(TPIAgency5Result,'') = 'Returned' or isnull(TPIAgency6Result,'') = 'Returned')) and TestResult = 'Cleared' and IsTPIResultMaintained = 1 and ISNULL(IsSubmitToQA, 0) = 0 ";
                    _urlFrom = "clear";
                    ViewBag.Status = "Clear";
                }
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms.BU", "qms.Location");
                string[] columnName = { "qms.SeamNo", "dbo.GET_PROJECTDESC_BY_CODE(qms.Project)", "qms.OfferedBy", "qms.CreatedBy", "qms.Location", "dbo.GET_IPI_STAGECODE_DESCRIPTION(qms.StageCode, qms.BU, qms.Location) ", "qms.RequestNo", "qms.InspectedBy", "qms.QCRemarks", "qms.OfferedtoCustomerBy", "qms.Shop", "qms.ShopLocation", "qms.ShopRemark" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string qualityProject = param.QualityProject;
                string seamNumber = param.SeamNo;

                //if (!string.IsNullOrEmpty(qualityProject) && !string.IsNullOrEmpty(seamNumber))
                //{
                //    whereCondition += (seamNumber.Equals("ALL") ? " AND qms.QualityProject = '" + qualityProject + "' " : " AND qms.QualityProject = '" + qualityProject + "' AND qms.SeamNo ='" + seamNumber + "'");
                //}
                string strWhere = whereCondition;
                if (!string.IsNullOrEmpty(qualityProject))
                {
                    whereCondition += " AND qms.QualityProject in (SELECT Value FROM [dbo].[fn_Split]('" + qualityProject + "',','))";
                }

                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = string.Empty; ;
                sortDirection = Convert.ToString(Request["sSortDir_0"]); // asc or desc
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstHeader = db.SP_IPI_SEAMLISTOFFERINSPECTION_DETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                if (param.Status.ToUpper() == "PENDING" || param.Status.ToUpper() == "RETURN")
                {
                    var res = from h in lstHeader
                              select new[] {
                           GenerateHTMLCheckboxWithEvent(Convert.ToInt32(h.HeaderId),"check",false, "", true, "clsCheckbox"),
                           h.QualityProject,
                           Convert.ToString(h.Project),
                           Convert.ToString(h.RequestId),
                           Convert.ToString(h.HeaderId),
                           Convert.ToString( h.SeamNo),
                           Convert.ToString(h.StageCode),
                           Convert.ToString(h.StageSequence),
                           Convert.ToString(h.IterationNo),
                           Convert.ToString(h.TestResult),
                           Convert.ToString(h.OfferedBy),
                           h.OfferedOn != null ? Convert.ToDateTime(h.OfferedOn).ToString("dd/MM/yyyy hh:mm tt") : "",
                           Convert.ToString(h.BU),
                           Convert.ToString( h.Location),
                           Convert.ToString(h.RequestNo),
                           Convert.ToString(h.RequestNoSequence),
                           Convert.ToString(h.InspectedBy),
                           h.InspectedOn != null ? Convert.ToDateTime(h.InspectedOn).ToString("dd/MM/yyyy hh:mm tt") : "",
                           Convert.ToString(h.QCRemarks),
                           Convert.ToString(h.OfferedtoCustomerBy),
                           h.OfferedtoCustomerOn != null ? Convert.ToDateTime(h.OfferedtoCustomerOn).ToString("dd/MM/yyyy hh:mm tt") : "",
                           Convert.ToString(h.Shop),
                           Convert.ToString(h.ShopLocation),
                           Convert.ToString(h.ShopRemark),
                           //h.StageCode.ToLower().Contains("chemical") || h.StageCode.ToLower().Contains("ferrite") || h.StageCode.ToLower().Contains("hardness") || h.StageCode.ToLower().Contains("pmi") ? "<center style=\"white-space: nowrap;\" class=\"clsDisplayInlineEle\"><a target=\"_blank\" Title=\"View Details\" id=\"btnPrint\" name=\"btnPrint\" onclick=\"PrintReport(" + h.RequestId + ", '" + h.HeaderId + "', '" + h.BU + "', '" + h.Location + "', '" + h.QualityProject + "', '" + h.SeamNo + "', '" + h.InspectedOn + "', '" + h.StageCode + "', '" + h.StageSequence + "', '" + h.TestResult + "', '" + objClsLoginInfo.UserName + "');\" ><i style=\"margin - left:5px;\" class=\"fa fa-eye\"></i></a></center>"
                           // :  "<center style=\"white-space: nowrap;\" class=\"clsDisplayInlineEle\"><a target=\"_blank\" Title=\"View Details\" id=\"btnPrint\" name=\"btnPrint\" onclick=\"GetProtocolData(" + h.RequestId + ");\" ><i style=\"margin - left:5px;\" class=\"fa fa-eye\"></i></a></center>"
                           // "<center style=\"white-space: nowrap;\" class=\"clsDisplayInlineEle\"> <a target=\"_blank\" Title=\"View Details\" id=\"btnPrint\" name=\"btnPrint\" onclick=\"GetProtocolData(" + h.RequestId + ");\" ><i style=\"margin - left:5px;\" class=\"fa fa-eye\"></i></a>&nbsp;&nbsp;"
                           //+"&nbsp;&nbsp;"
                           //+(h.StageCode.ToLower().Contains("chemical") || h.StageCode.ToLower().Contains("ferrite") || h.StageCode.ToLower().Contains("hardness") || h.StageCode.ToLower().Contains("pmi") ?"<a target=\"_blank\" Title=\"Print\" id=\"btnPrintR\" name=\"btnPrintR\" onclick=\"PrintReport(" + h.RequestId + ", '" + h.HeaderId + "', '" + h.BU + "', '" + h.Location + "', '" + h.QualityProject + "', '" + h.SeamNo + "', '" + h.InspectedOn + "', '" + h.StageCode + "', '" + h.StageSequence + "', '" + h.TestResult + "', '" + objClsLoginInfo.UserName + "','" + h.StageCode + "');\" ><i style=\"margin - left:5px;\" class=\"fa fa-print\"></i></a>":"<a target=\"_blank\" Title=\"Print\" id=\"btnPrintR\" name=\"btnPrintR\" onclick=\"PrintReport(" + h.RequestId + ", '" + h.ProtocolId + "', '" + h.BU + "', '" + h.Location + "', '" + h.QualityProject + "', '" + h.SeamNo + "', '" + h.InspectedOn + "', '" + h.StageCode + "', '" + h.StageSequence + "', '" + h.TestResult + "', '" + objClsLoginInfo.UserName + "','" + h.ProtocolType + "');\" ><i style=\"margin - left:5px;\" class=\"fa fa-print\"></i></a>")
                           // +"</center>"
                           
                           (Manager.FetchStageType(h.StageCode.Split('-')[0].Trim(), h.BU.Split('-')[0].Trim(), h.Location.Split('-')[0].Trim()).ToLower().Equals("chemical") || Manager.FetchStageType(h.StageCode.Split('-')[0].Trim(), h.BU.Split('-')[0].Trim(), h.Location.Split('-')[0].Trim()).ToLower().Equals("ferrite") || Manager.FetchStageType(h.StageCode.Split('-')[0].Trim(), h.BU.Split('-')[0].Trim(), h.Location.Split('-')[0].Trim()).ToLower().Equals("hardness") || Manager.FetchStageType(h.StageCode.Split('-')[0].Trim(), h.BU.Split('-')[0].Trim(), h.Location.Split('-')[0].Trim()).ToLower().Equals("pmi")) ? "<center style=\"white-space: nowrap;\" class=\"clsDisplayInlineEle\"><a target=\"_blank\" Title=\"View Details\" id=\"btnPrint\" name=\"btnPrint\" onclick=\"OpenQAClearanceReports('" + Manager.FetchStageType(h.StageCode.Split('-')[0].Trim(), h.BU.Split('-')[0].Trim(), h.Location.Split('-')[0].Trim()) + "', '" + h.Location.Split('-')[0].Trim() + "', '" + h.QualityProject + "', '" + h.QualityProject + "',  '" + h.SeamNo + "','" + h.SeamNo + "', '" + Manager.getDateTimeyyyyMMdd(h.InspectedOn.Value.ToShortDateString()) + "', '" + Manager.getDateTimeyyyyMMdd(h.InspectedOn.Value.ToShortDateString()) + "', '" + h.StageCode.Split('-')[0].Trim() + "', '" + h.StageCode.Split('-')[0].Trim() + "', '" + h.StageSequence + "', '" + h.StageSequence + "', '', '', '', '" + h.InspectedBy + "', '', '" + objClsLoginInfo.UserName + "', ' ', ' ');\" ><i style=\"margin - left:5px;\" class=\"fa fa-eye\"></i></a></center>"
                                                        :  "<center style=\"white-space: nowrap;\" class=\"clsDisplayInlineEle\"><a target=\"_blank\" Title=\"View Details\" id=\"btnPrint\" name=\"btnPrint\" onclick=\"OpenProtocolReports('" + h.ProtocolType + "', '" + h.ProtocolId + "', '" + h.HeaderId + "', '" + h.IterationNo + "', '" + h.RequestNoSequence + "');\" ><i style=\"margin - left:5px;\" class=\"fa fa-eye\"></i></a></center>"

                    };
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                        strSortOrder = strSortOrder,
                        whereCondition = whereCondition,
                        strWhere = strWhere
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var res = from h in lstHeader
                              select new[] {
                           GenerateHTMLCheckboxWithEvent(Convert.ToInt32(h.HeaderId),"check",false, "", true, "clsCheckbox"),
                           h.QualityProject,
                           Convert.ToString(h.Project),
                           Convert.ToString(h.RequestId),
                           Convert.ToString(h.HeaderId),
                           Convert.ToString( h.SeamNo),
                           Convert.ToString(h.StageCode),
                           Convert.ToString(h.StageSequence),
                           Convert.ToString(h.IterationNo),
                           Convert.ToString(h.TestResult),
                           Convert.ToString(h.OfferedBy),
                           h.OfferedOn != null ? Convert.ToDateTime(h.OfferedOn).ToString("dd/MM/yyyy hh:mm tt") : "",
                           Convert.ToString(h.BU),
                           Convert.ToString( h.Location),
                           Convert.ToString(h.RequestNo),
                           Convert.ToString(h.RequestNoSequence),
                           Convert.ToString(h.InspectedBy),
                           h.InspectedOn != null ? Convert.ToDateTime(h.InspectedOn).ToString("dd/MM/yyyy hh:mm tt") : "",
                           Convert.ToString(h.QCRemarks),
                           Convert.ToString(h.OfferedtoCustomerBy),
                           h.OfferedtoCustomerOn != null ? Convert.ToDateTime(h.OfferedtoCustomerOn).ToString("dd/MM/yyyy hh:mm tt") : "",
                           Convert.ToString(h.Shop),
                           Convert.ToString(h.ShopLocation),
                           Convert.ToString(h.ShopRemark),
                           //h.StageCode.ToLower().Contains("chemical") || h.StageCode.ToLower().Contains("ferrite") || h.StageCode.ToLower().Contains("hardness") || h.StageCode.ToLower().Contains("pmi") ? "<center style=\"white-space: nowrap;\" class=\"clsDisplayInlineEle\"><a target=\"_blank\" Title=\"View Details\" id=\"btnPrint\" name=\"btnPrint\" onclick=\"PrintReport(" + h.RequestId + ", '" + h.HeaderId + "', '" + h.BU + "', '" + h.Location + "', '" + h.QualityProject + "', '" + h.SeamNo + "', '" + h.InspectedOn + "', '" + h.StageCode + "', '" + h.StageSequence + "', '" + h.TestResult + "', '" + objClsLoginInfo.UserName + "');\" ><i style=\"margin - left:5px;\" class=\"fa fa-eye\"></i></a></center>"
                           // :  "<center style=\"white-space: nowrap;\" class=\"clsDisplayInlineEle\"><a target=\"_blank\" Title=\"View Details\" id=\"btnPrint\" name=\"btnPrint\" onclick=\"GetProtocolData(" + h.RequestId + ");\" ><i style=\"margin - left:5px;\" class=\"fa fa-eye\"></i></a></center>"
                           "<center style=\"white-space: nowrap;\" class=\"clsDisplayInlineEle\"> <a target=\"_blank\" Title=\"View Details\" id=\"btnPrint\" name=\"btnPrint\" onclick=\"GetProtocolData(" + h.RequestId + ");\" ><i style=\"margin - left:5px;\" class=\"fa fa-eye\"></i></a>&nbsp;&nbsp;"
                           +"&nbsp;&nbsp;"
                           +(h.StageCode.ToLower().Contains("chemical") || h.StageCode.ToLower().Contains("ferrite") || h.StageCode.ToLower().Contains("hardness") || h.StageCode.ToLower().Contains("pmi") ?"<a target=\"_blank\" Title=\"Print\" id=\"btnPrintR\" name=\"btnPrintR\" onclick=\"PrintReport(" + h.RequestId + ", '" + h.HeaderId + "', '" + h.BU + "', '" + h.Location + "', '" + h.QualityProject + "', '" + h.SeamNo + "', '" + h.InspectedOn + "', '" + h.StageCode + "', '" + h.StageSequence + "', '" + h.TestResult + "', '" + objClsLoginInfo.UserName + "','" + h.StageCode + "','"+ SignUrl +"','" + HostName + "');\" ><i style=\"margin - left:5px;\" class=\"fa fa-print\"></i></a>":"<a target=\"_blank\" Title=\"Print\" id=\"btnPrintR\" name=\"btnPrintR\" onclick=\"PrintReport(" + h.RequestId + ", '" + h.ProtocolId + "', '" + h.BU + "', '" + h.Location + "', '" + h.QualityProject + "', '" + h.SeamNo + "', '" + h.InspectedOn + "', '" + h.StageCode + "', '" + h.StageSequence + "', '" + h.TestResult + "', '" + objClsLoginInfo.UserName + "','" + h.ProtocolType + "','"+ SignUrl+ "','" + HostName + "');\" ><i style=\"margin - left:5px;\" class=\"fa fa-print\"></i></a>")
                            +"</center>"
                    };
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                        strSortOrder = strSortOrder,
                        whereCondition = whereCondition,
                        strWhere = strWhere
                    }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult UpdateOfferTPI(string RequestId, string TPIAgency, string TPIAgency2, string TPIAgency3, string TPIAgency4, string TPIAgency5, string TPIAgency6, string Status)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            QMS050 objQMS050 = new QMS050();
            List<string> lstRequestId = new List<string>();

            if (!string.IsNullOrWhiteSpace(RequestId))
            {
                lstRequestId = RequestId.Split(',').ToList();
            }

            try
            {
                for (int j = 0; j < lstRequestId.Count; j++)
                {
                    int StoreRequestID = Convert.ToInt32(lstRequestId[j]);
                    objQMS050 = db.QMS050.Where(i => i.RequestId == StoreRequestID).FirstOrDefault();
                    objQMS050.OfferedtoTPIBy = objClsLoginInfo.UserName;
                    objQMS050.OfferedtoTPIOn = DateTime.Now;
                    if (TPIAgency != "")
                    {
                        objQMS050.OfferedTPIAgency = TPIAgency;
                    }
                    if (TPIAgency2 != "")
                    {
                        objQMS050.OfferedTPIAgency2 = TPIAgency2;
                    }
                    if (TPIAgency3 != "")
                    {
                        objQMS050.OfferedTPIAgency3 = TPIAgency3;
                    }
                    if (TPIAgency4 != "")
                    {
                        objQMS050.OfferedTPIAgency4 = TPIAgency4;
                    }
                    if (TPIAgency5 != "")
                    {
                        objQMS050.OfferedTPIAgency5 = TPIAgency5;
                    }
                    if (TPIAgency6 != "")
                    {
                        objQMS050.OfferedTPIAgency6 = TPIAgency6;
                    }

                    objQMS050.TPIAgency1Result = null;
                    objQMS050.TPIAgency1ResultOn = null;
                    objQMS050.TPIAgency1PSNO = null;


                    objQMS050.TPIAgency2Result = null;
                    objQMS050.TPIAgency2ResultOn = null;
                    objQMS050.TPIAgency2PSNO = null;


                    objQMS050.TPIAgency3Result = null;
                    objQMS050.TPIAgency3ResultOn = null;
                    objQMS050.TPIAgency3PSNO = null;

                    objQMS050.TPIAgency4Result = null;
                    objQMS050.TPIAgency4ResultOn = null;
                    objQMS050.TPIAgency4PSNO = null;

                    objQMS050.TPIAgency5Result = null;
                    objQMS050.TPIAgency5ResultOn = null;
                    objQMS050.TPIAgency5PSNO = null;


                    objQMS050.TPIAgency6Result = null;
                    objQMS050.TPIAgency6ResultOn = null;
                    objQMS050.TPIAgency6PSNO = null;
                    objQMS050.IsTPIResultMaintained = false;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    //objResponseMsg.Status = string.Format(clsImplementationMessage.QualityProjectMessage.Update, objQMS050.QualityProject);
                    objResponseMsg.Value = Convert.ToString(objQMS050.RequestId);

                    //if (ViewBag.Status == "Pending")
                    if (Status == "Pending")
                    {
                        #region Send Mail
                        string ID = StoreRequestID.ToString();
                        List<string> emailTo = new List<string>();
                        string emailfrom = "";
                        emailfrom = objClsLoginInfo.UserName;

                        QMS050 objData = db.QMS050.Where(u => u.RequestId == StoreRequestID).SingleOrDefault();

                        string QualityProject = objData.QualityProject;
                        string OfferedTPIAgency = objData.OfferedTPIAgency;

                        QMS010 objQMS010 = db.QMS010.Where(u => u.QualityProject == QualityProject).SingleOrDefault();

                        string TPI1 = objQMS010.TPI1;
                        string TPI2 = objQMS010.TPI2;
                        string TPI3 = objQMS010.TPI3;
                        string TPI4 = objQMS010.TPI4;
                        string TPI5 = objQMS010.TPI5;
                        string TPI6 = objQMS010.TPI6;
                        string TPIAgency1Name = objQMS010.TPIAgency1Name;
                        string TPIAgency2Name = objQMS010.TPIAgency2Name;
                        string TPIAgency3Name = objQMS010.TPIAgency3Name;
                        string TPIAgency4Name = objQMS010.TPIAgency4Name;
                        string TPIAgency5Name = objQMS010.TPIAgency5Name;
                        string TPIAgency6Name = objQMS010.TPIAgency6Name;

                        if (OfferedTPIAgency == TPI1)
                        {
                            emailTo.Add(Manager.GetMailIdFromPsNo(TPIAgency1Name));
                        }
                        if (OfferedTPIAgency == TPI2)
                        {
                            emailTo.Add(Manager.GetMailIdFromPsNo(TPIAgency2Name));
                        }
                        if (OfferedTPIAgency == TPI3)
                        {
                            emailTo.Add(Manager.GetMailIdFromPsNo(TPIAgency3Name));
                        }
                        if (OfferedTPIAgency == TPI4)
                        {
                            emailTo.Add(Manager.GetMailIdFromPsNo(TPIAgency4Name));
                        }
                        if (OfferedTPIAgency == TPI5)
                        {
                            emailTo.Add(Manager.GetMailIdFromPsNo(TPIAgency5Name));
                        }
                        if (OfferedTPIAgency == TPI6)
                        {
                            emailTo.Add(Manager.GetMailIdFromPsNo(TPIAgency6Name));
                        }

                        if (emailTo.Count > 0)
                        {
                            try
                            {
                                Hashtable _ht = new Hashtable();
                                EmailSend _objEmail = new EmailSend();
                                _ht["[FromEmail]"] = Manager.GetUserNameFromPsNo(emailfrom);
                                _ht["[ProjectNo]"] = objData.Project;

                                MAIL001 objTemplateMaster = new MAIL001();
                                objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.IPI.OffertoTPI).SingleOrDefault();
                                //string mail = "deval.barot@rigelnetworks.net";
                                //emailTo.Add(mail);
                                _objEmail.MailToAdd = string.Join(",", emailTo.Distinct());
                                _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                            }
                            catch (Exception ex)
                            {
                                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                            }
                        }
                        #endregion
                    }
                }
                return Json(objResponseMsg);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error Occures Please try again";
                return Json(objResponseMsg);
            }
        }

        [NonAction]
        public static string GenerateHTMLCheckboxWithEvent(int rowId, string columnName, bool columnValue = false, string onClickMethod = "", bool isEnabled = true, string ClassName = "", bool isDisabled = false, string check = "")
        {
            string htmlControl = "";
            string inputID = columnName + "" + rowId.ToString();
            string inputValue = columnValue ? "Checked='Checked'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";
            string cssClass = !string.IsNullOrEmpty(ClassName) ? "class='" + ClassName + "'" : "";

            if (isDisabled)
            {
                if (check != string.Empty && check != null)
                {
                    htmlControl = "<input type='checkbox' id='" + inputID + "' " + inputValue + " name='" + inputID + "' colname='" + columnName + "' disabled='disabled " + cssClass + " />";
                }
                else
                {
                    htmlControl = "<input type='checkbox' id='" + inputID + "' name='" + inputID + "' colname='" + columnName + "' disabled='disabled " + cssClass + " />";
                }
            }
            else
            {
                if (isEnabled)
                {
                    htmlControl = "<input type='checkbox' id='" + inputID + "' " + inputValue + " name='" + inputID + "' colname='" + columnName + "' " + onClickEvent + " " + (!isEnabled ? "disabled='disabled'" : "") + " " + cssClass + " />";
                }
                else
                {
                    htmlControl = columnValue ? "" : "";
                }
            }
            return htmlControl;
        }

        [HttpPost]
        public ActionResult FillAutocompleteForTPI(string term, string bu, string location, bool isAgency, string ID)
        {
            //var lstTPIAgency = Manager.GetSubCatagories("TPI Agency", bu, location)
            //                            .Where(x => (term == "" || (x.Code.ToLower().Contains(term.ToLower()) || x.Description.ToLower().Contains(term.ToLower()))))
            //                            .Select(i => new { Value = i.Code, Text = i.Description }).ToList();

            List<string> lstBU = new List<string>();
            List<string> lstLocation = new List<string>();
            List<string> lstID = new List<string>();
            //List<string> lstAllAgency = new List<string>();

            bu = bu.Trim();
            location = location.Trim();
            ID = ID.Trim();
            if (!string.IsNullOrWhiteSpace(bu))
            {
                lstBU = bu.Split(',').ToList();
            }
            if (!string.IsNullOrWhiteSpace(location))
            {
                lstLocation = location.Split(',').ToList();
            }
            if (!string.IsNullOrWhiteSpace(ID))
            {
                lstID = ID.Split(',').ToList();
            }

            List<GLB002> lstGLB002 = new List<GLB002>();

            lstGLB002 = (from glb002 in db.GLB002
                         join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                         where glb001.Category.Equals("TPI Agency") && glb002.IsActive == true && lstBU.Contains(glb002.BU) && lstLocation.Contains(glb002.Location)
                         select glb002).ToList();

            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Code", typeof(string)));
            dt.Columns.Add(new DataColumn("Description", typeof(string)));

            //Dictionary<object, object> lstAllAgency = new Dictionary<object, object>();

            List<DropDownModel> lstAllAgency = new List<DropDownModel>();

            for (int j = 0; j < lstID.Count; j++)
            {
                int StoreRequestID = Convert.ToInt32(lstID[j]);
                QMS050 objQMS050 = db.QMS050.FirstOrDefault(x => x.RequestId == StoreRequestID);

                List<GLB002> lstCategory = lstGLB002;
                List<string> listOfCodes = new List<string>();

                listOfCodes.Add(objQMS050.TPIAgency1);
                listOfCodes.Add(objQMS050.TPIAgency2);
                listOfCodes.Add(objQMS050.TPIAgency3);
                listOfCodes.Add(objQMS050.TPIAgency4);
                listOfCodes.Add(objQMS050.TPIAgency5);
                listOfCodes.Add(objQMS050.TPIAgency6);

                var lstTest = lstGLB002.Where(x => listOfCodes.Contains(x.Code)).Select(i => new DropDownModel { Value = i.Code, Text = i.Description }).ToList();
                foreach (var item in lstTest)
                {
                    bool AlredyExists = lstAllAgency.Any(i => i.Value == item.Value);
                    if (AlredyExists == false)
                    {
                        lstAllAgency.Add(item);
                    }
                }
            }

            return Json(lstAllAgency, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateSubmitQA(string RequestId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            QMS050 objQMS050 = new QMS050();
            List<string> lstRequestId = new List<string>();

            if (!string.IsNullOrWhiteSpace(RequestId))
            {
                lstRequestId = RequestId.Split(',').ToList();
            }

            try
            {
                for (int j = 0; j < lstRequestId.Count; j++)
                {
                    int StoreRequestId = Convert.ToInt32(lstRequestId[j]);
                    objQMS050 = db.QMS050.Where(i => i.RequestId == StoreRequestId).FirstOrDefault();
                    objQMS050.IsSubmitToQA = true;
                    objQMS050.SubmittedtoQABy = objClsLoginInfo.UserName;
                    objQMS050.SubmittedtoQAOn = DateTime.Now;
                    db.Entry(objQMS050).State = EntityState.Modified;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    //objResponseMsg.Status = string.Format(clsImplementationMessage.QualityProjectMessage.Update, objQMS050.QualityProject);
                    objResponseMsg.Value = Convert.ToString(objQMS050.RequestId);

                    //#region generate pdf
                    string ProjectName = db.QMS050.Where(i => i.RequestId == StoreRequestId).Select(x => x.Project).FirstOrDefault();
                    string StageCode = db.QMS050.Where(i => i.RequestId == StoreRequestId).Select(x => x.StageCode).FirstOrDefault();
                    string SeamNo = db.QMS050.Where(i => i.RequestId == StoreRequestId).Select(x => x.SeamNo).FirstOrDefault();

                }
                return Json(objResponseMsg);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error Occures Please try again";
                return Json(objResponseMsg);
            }
        }

        [SessionExpireFilter]
        public ActionResult TPIClearance(string tab)
        {
            ViewBag.Tab = tab;
            ViewBag.Location = objClsLoginInfo.Location;
            return View();
        }

        public ActionResult GetTPIClearanceDataPartial(string status, string qualityProject = "", string seamNumber = "")
        {
            ViewBag.Status = status;
            ViewBag.QualityProject = qualityProject;
            ViewBag.SeamNumber = seamNumber;
            return PartialView("_GetTPIClearanceDataPartial");
        }

        [HttpPost]
        public ActionResult LoadReportInputFilterPartial(string reporttype)
        {
            //0 - false, 1 - true

            ViewBag.ShowLocation = 0;
            ViewBag.ShowProject = 0;
            ViewBag.ShowSeamNo = 0;
            ViewBag.ShowQID = 0;
            ViewBag.ReportType = reporttype;
            ViewBag.PrintedBy = objClsLoginInfo.UserName;
            var lstStageType = new List<SelectListItem>()
            { new SelectListItem { Text = "PT", Value = "PT" },
              new SelectListItem { Text = "MT", Value = "MT" },
              new SelectListItem { Text = "UT", Value = "UT" },
              new SelectListItem { Text = "RT", Value = "RT" },
            };
            if (reporttype == "Chemical")
            {
                var objATH001 = db.ATH001.Where(i => i.Employee == objClsLoginInfo.UserName).Select(i => new { BU = i.BU, Location = i.Location, Role = i.Role }).ToList();
                var objLocation = objATH001.Select(i => i.Location).Distinct().ToList();
                var listLocation = (from c1 in db.COM002
                                    where c1.t_dtyp == 1 && objLocation.Contains(c1.t_dimx)
                                    select new { id = c1.t_dimx, text = c1.t_desc, CatID = c1.t_dimx, CatDesc = c1.t_desc }
                              ).Distinct().ToList();

                ViewBag.Location = listLocation;
                ViewBag.ShowRPTFromToDate = 1;
                ViewBag.ShowFromToQualityProject = 1;
                ViewBag.ShowFromToSeamNobyQualityProject = 1;
                ViewBag.ShowFromToAllStages = 1;
                ViewBag.ShowLocationAutocomplete = 1;
                ViewBag.ShowRPTSeqn = 1; ViewBag.ShowRPTFromToStageseqinput = 1;
                ViewBag.ShowSeamApprovalStatus = 1;
                ViewBag.WeldingProcess = 1;
                ViewBag.Instrument = 1; ViewBag.ShowTPIinReport = 1;
                ViewBag.ReportStageType = clsImplementationEnum.SeamStageType.Chemical.GetStringValue();
                var lstSeamApprovalStatus = new List<SelectListItem>()
                                            {
                                              new SelectListItem { Text = "Select", Value = "" },
                                              new SelectListItem { Text = "Cleared", Value = "Cleared" },
                                              new SelectListItem { Text = "Returned", Value = "Returned" },
                                              new SelectListItem { Text = "Rejected", Value = "Rejected" }
                                            };

                ViewBag.SeamApprovalStatus = lstSeamApprovalStatus;

                List<CategoryData> lstWeldingProcess = Manager.GetSubCatagorywithoutBULocation("Welding Process").ToList();
                ViewBag.WeldingProcessList = lstWeldingProcess.AsEnumerable().Select(x => new { CatID = x.Value, CatDesc = x.CategoryDescription, id = x.Value, text = x.CategoryDescription }).OrderBy(x => x.CatID).ToList();

                List<CategoryData> lstInstrument = Manager.GetSubCatagorywithoutBULocation("Instrument Group").ToList();
                ViewBag.InstrumentList = lstInstrument.AsEnumerable().Select(x => new { CatID = x.Value, CatDesc = x.CategoryDescription, id = x.Value, text = x.CategoryDescription }).OrderBy(x => x.CatID).ToList();

                ViewBag.ShowInspectionRemark = 1;
            }
            else if (reporttype == "Ferrite")
            {
                var objATH001 = db.ATH001.Where(i => i.Employee == objClsLoginInfo.UserName).Select(i => new { BU = i.BU, Location = i.Location, Role = i.Role }).ToList();
                var objLocation = objATH001.Select(i => i.Location).Distinct().ToList();
                var listLocation = (from c1 in db.COM002
                                    where c1.t_dtyp == 1 && objLocation.Contains(c1.t_dimx)
                                    select new { id = c1.t_dimx, text = c1.t_desc, CatID = c1.t_dimx, CatDesc = c1.t_desc }
                              ).Distinct().ToList();

                ViewBag.Location = listLocation;
                ViewBag.ShowRPTFromToDate = 1;
                ViewBag.ShowFromToQualityProject = 1;
                ViewBag.ShowFromToSeamNobyQualityProject = 1;
                ViewBag.ShowFromToAllStages = 1;
                ViewBag.ShowLocationAutocomplete = 1;
                ViewBag.ShowRPTSeqn = 1; ViewBag.ShowRPTFromToStageseqinput = 1;
                ViewBag.ShowSeamApprovalStatus = 1;
                ViewBag.WeldingProcess = 1;
                ViewBag.Instrument = 1; ViewBag.ShowTPIinReport = 1;
                ViewBag.ReportStageType = clsImplementationEnum.SeamStageType.Ferrite.GetStringValue();
                var lstSeamApprovalStatus = new List<SelectListItem>()
                                            {   new SelectListItem { Text = "Select", Value = "" }, new SelectListItem { Text = "Cleared", Value = "Cleared" },
                                              new SelectListItem { Text = "Returned", Value = "Returned" },
                                              new SelectListItem { Text = "Rejected", Value = "Rejected" }
                                            };

                ViewBag.SeamApprovalStatus = lstSeamApprovalStatus;

                List<CategoryData> lstWeldingProcess = Manager.GetSubCatagorywithoutBULocation("Welding Process").ToList();
                ViewBag.WeldingProcessList = lstWeldingProcess.AsEnumerable().Select(x => new { CatID = x.Value, CatDesc = x.CategoryDescription, id = x.Value, text = x.CategoryDescription }).OrderBy(x => x.CatID).ToList();

                List<CategoryData> lstInstrument = Manager.GetSubCatagorywithoutBULocation("Instrument Group").ToList();
                ViewBag.InstrumentList = lstInstrument.AsEnumerable().Select(x => new { CatID = x.Value, CatDesc = x.CategoryDescription, id = x.Value, text = x.CategoryDescription }).OrderBy(x => x.CatID).ToList();

                ViewBag.ShowInspectionRemark = 1;
            }
            else if (reporttype == "Hardness")
            {
                var objATH001 = db.ATH001.Where(i => i.Employee == objClsLoginInfo.UserName).Select(i => new { BU = i.BU, Location = i.Location, Role = i.Role }).ToList();
                var objLocation = objATH001.Select(i => i.Location).Distinct().ToList();
                var listLocation = (from c1 in db.COM002
                                    where c1.t_dtyp == 1 && objLocation.Contains(c1.t_dimx)
                                    select new { id = c1.t_dimx, text = c1.t_desc, CatID = c1.t_dimx, CatDesc = c1.t_desc }
                              ).Distinct().ToList();

                ViewBag.Location = listLocation;
                ViewBag.ShowRPTFromToDate = 1;
                ViewBag.ShowFromToQualityProject = 1;
                ViewBag.ShowFromToSeamNobyQualityProject = 1;
                ViewBag.ShowFromToAllStages = 1;
                ViewBag.ShowLocationAutocomplete = 1;
                ViewBag.ShowRPTSeqn = 1; ViewBag.ShowRPTFromToStageseqinput = 1;
                ViewBag.ShowSeamApprovalStatus = 1;
                ViewBag.Instrument = 1;
                ViewBag.ShowTPIinReport = 1;
                ViewBag.ReportStageType = clsImplementationEnum.SeamStageType.Hardness.GetStringValue();
                var lstSeamApprovalStatus = new List<SelectListItem>()
                                            {  new SelectListItem { Text = "Select", Value = "" },  new SelectListItem { Text = "Cleared", Value = "Cleared" },
                                              new SelectListItem { Text = "Returned", Value = "Returned" },
                                              new SelectListItem { Text = "Rejected", Value = "Rejected" }
                                            };

                ViewBag.SeamApprovalStatus = lstSeamApprovalStatus;

                List<CategoryData> lstWeldingProcess = Manager.GetSubCatagorywithoutBULocation("Welding Process").ToList();
                ViewBag.WeldingProcessList = lstWeldingProcess.AsEnumerable().Select(x => new { CatID = x.Value, CatDesc = x.CategoryDescription, id = x.Value, text = x.CategoryDescription }).OrderBy(x => x.CatID).ToList();

                List<CategoryData> lstInstrument = Manager.GetSubCatagorywithoutBULocation("Instrument Group").ToList();
                ViewBag.InstrumentList = lstInstrument.AsEnumerable().Select(x => new { CatID = x.Value, CatDesc = x.CategoryDescription, id = x.Value, text = x.CategoryDescription }).OrderBy(x => x.CatID).ToList();

                ViewBag.ShowInspectionRemark = 1;
            }
            else if (reporttype == "PMI")
            {
                var objATH001 = db.ATH001.Where(i => i.Employee == objClsLoginInfo.UserName).Select(i => new { BU = i.BU, Location = i.Location, Role = i.Role }).ToList();
                var objLocation = objATH001.Select(i => i.Location).Distinct().ToList();
                var listLocation = (from c1 in db.COM002
                                    where c1.t_dtyp == 1 && objLocation.Contains(c1.t_dimx)
                                    select new { id = c1.t_dimx, text = c1.t_desc, CatID = c1.t_dimx, CatDesc = c1.t_desc }
                              ).Distinct().ToList();

                ViewBag.Location = listLocation;
                ViewBag.ShowRPTFromToDate = 1;
                ViewBag.ShowFromToQualityProject = 1;
                ViewBag.ShowFromToSeamNobyQualityProject = 1;
                ViewBag.ShowFromToAllStages = 1;
                ViewBag.ShowLocationAutocomplete = 1;
                ViewBag.ShowRPTSeqn = 1; ViewBag.ShowRPTFromToStageseqinput = 1;
                ViewBag.ShowSeamApprovalStatus = 1;
                ViewBag.Instrument = 1; ViewBag.ShowTPIinReport = 1;
                ViewBag.ReportStageType = clsImplementationEnum.SeamStageType.PMI.GetStringValue();
                var lstSeamApprovalStatus = new List<SelectListItem>()
                                            {  new SelectListItem { Text = "Select", Value = "" }, new SelectListItem { Text = "Cleared", Value = "Cleared" },
                                              new SelectListItem { Text = "Returned", Value = "Returned" },
                                              new SelectListItem { Text = "Rejected", Value = "Rejected" }
                                            };

                ViewBag.SeamApprovalStatus = lstSeamApprovalStatus;

                List<CategoryData> lstWeldingProcess = Manager.GetSubCatagorywithoutBULocation("Welding Process").ToList();
                ViewBag.WeldingProcessList = lstWeldingProcess.AsEnumerable().Select(x => new { CatID = x.Value, CatDesc = x.CategoryDescription, id = x.Value, text = x.CategoryDescription }).OrderBy(x => x.CatID).ToList();

                List<CategoryData> lstInstrument = Manager.GetSubCatagorywithoutBULocation("Instrument Group").ToList();
                ViewBag.InstrumentList = lstInstrument.AsEnumerable().Select(x => new { CatID = x.Value, CatDesc = x.CategoryDescription, id = x.Value, text = x.CategoryDescription }).OrderBy(x => x.CatID).ToList();

                ViewBag.ShowInspectionRemark = 1;
            }
            ViewBag.StageType = lstStageType;
            return PartialView("_ReportInputFilterHtmlPartial");
        }

        public ActionResult loadTPIClearanceDataTable(JQueryDataTableParamModel param)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                string _urlFrom = "all";
                //var list = new List<QMS050>();

                string whereCondition = "1=1";
                if (param.Status.ToUpper() == "PENDING")
                {
                    //list = db.QMS050.Where(x => x.IsTPIPending == true && x.OfferedtoTPIBy == null && x.TestResult == "Cleared").ToList();
                    //whereCondition += " and IsTPIResultMaintained = 0 and IsTPIPending = 1 and OfferedtoTPIBy is not null and (TPIAgency1Name = '" + objClsLoginInfo.UserName + "' or TPIAgency2Name = '" + objClsLoginInfo.UserName + "' or TPIAgency3Name = '" + objClsLoginInfo.UserName + "' or TPIAgency4Name = '" + objClsLoginInfo.UserName + "' or TPIAgency5Name = '" + objClsLoginInfo.UserName + "' or TPIAgency6Name = '" + objClsLoginInfo.UserName + "') ";

                    whereCondition += " and IsTPIResultMaintained = 0 and IsTPIPending = 1 and OfferedtoTPIBy is not null and " +
                                           "((TPIAgency1Result is null and OfferedTPIAgency IN (SELECT CASE WHEN  QMS010.TPIAgency1Name = '" + objClsLoginInfo.UserName + "' THEN TPI1 WHEN QMS010.TPIAgency2Name = '" + objClsLoginInfo.UserName + "' THEN TPI2 WHEN QMS010.TPIAgency3Name = '" + objClsLoginInfo.UserName + "' THEN TPI3 WHEN QMS010.TPIAgency4Name = '" + objClsLoginInfo.UserName + "' THEN TPI4 WHEN QMS010.TPIAgency5Name = '" + objClsLoginInfo.UserName + "' THEN TPI5 WHEN QMS010.TPIAgency6Name = '" + objClsLoginInfo.UserName + "' THEN TPI6 END FROM QMS010 where QMS010.Project = QMS.Project And QMS010.BU = qms.BU and QMS010.Location = qms.Location and QMS010.QualityProject = qms.QualityProject )) " +
                                         "Or (TPIAgency2Result is null and OfferedTPIAgency2 IN (SELECT CASE WHEN QMS010.TPIAgency1Name = '" + objClsLoginInfo.UserName + "' THEN TPI1 WHEN QMS010.TPIAgency2Name = '" + objClsLoginInfo.UserName + "' THEN TPI2 WHEN QMS010.TPIAgency3Name = '" + objClsLoginInfo.UserName + "' THEN TPI3 WHEN QMS010.TPIAgency4Name = '" + objClsLoginInfo.UserName + "' THEN TPI4 WHEN QMS010.TPIAgency5Name = '" + objClsLoginInfo.UserName + "' THEN TPI5 WHEN QMS010.TPIAgency6Name = '" + objClsLoginInfo.UserName + "' THEN TPI6 END FROM QMS010 where QMS010.Project = QMS.Project And QMS010.BU = qms.BU and QMS010.Location = qms.Location and QMS010.QualityProject = qms.QualityProject)) " +
                                         "Or (TPIAgency3Result is null and OfferedTPIAgency3 IN (SELECT CASE WHEN QMS010.TPIAgency1Name = '" + objClsLoginInfo.UserName + "' THEN TPI1 WHEN QMS010.TPIAgency2Name = '" + objClsLoginInfo.UserName + "' THEN TPI2 WHEN QMS010.TPIAgency3Name = '" + objClsLoginInfo.UserName + "' THEN TPI3 WHEN QMS010.TPIAgency4Name = '" + objClsLoginInfo.UserName + "' THEN TPI4 WHEN QMS010.TPIAgency5Name = '" + objClsLoginInfo.UserName + "' THEN TPI5 WHEN QMS010.TPIAgency6Name = '" + objClsLoginInfo.UserName + "' THEN TPI6 END FROM QMS010 where QMS010.Project = QMS.Project And QMS010.BU = qms.BU and QMS010.Location = qms.Location and QMS010.QualityProject = qms.QualityProject)) " +
                                         "Or (TPIAgency4Result is null and OfferedTPIAgency4 IN (SELECT CASE WHEN QMS010.TPIAgency1Name = '" + objClsLoginInfo.UserName + "' THEN TPI1 WHEN QMS010.TPIAgency2Name = '" + objClsLoginInfo.UserName + "' THEN TPI2 WHEN QMS010.TPIAgency3Name = '" + objClsLoginInfo.UserName + "' THEN TPI3 WHEN QMS010.TPIAgency4Name = '" + objClsLoginInfo.UserName + "' THEN TPI4 WHEN QMS010.TPIAgency5Name = '" + objClsLoginInfo.UserName + "' THEN TPI5 WHEN QMS010.TPIAgency6Name = '" + objClsLoginInfo.UserName + "' THEN TPI6 END FROM QMS010 where QMS010.Project = QMS.Project And QMS010.BU = qms.BU and QMS010.Location = qms.Location and QMS010.QualityProject = qms.QualityProject)) " +
                                         "Or (TPIAgency5Result is null and OfferedTPIAgency5 IN (SELECT CASE WHEN QMS010.TPIAgency1Name = '" + objClsLoginInfo.UserName + "' THEN TPI1 WHEN QMS010.TPIAgency2Name = '" + objClsLoginInfo.UserName + "' THEN TPI2 WHEN QMS010.TPIAgency3Name = '" + objClsLoginInfo.UserName + "' THEN TPI3 WHEN QMS010.TPIAgency4Name = '" + objClsLoginInfo.UserName + "' THEN TPI4 WHEN QMS010.TPIAgency5Name = '" + objClsLoginInfo.UserName + "' THEN TPI5 WHEN QMS010.TPIAgency6Name = '" + objClsLoginInfo.UserName + "' THEN TPI6 END FROM QMS010 where QMS010.Project = QMS.Project And QMS010.BU = qms.BU and QMS010.Location = qms.Location and QMS010.QualityProject = qms.QualityProject)) " +
                                         "Or (TPIAgency6Result is null and OfferedTPIAgency6 IN (SELECT CASE WHEN QMS010.TPIAgency1Name = '" + objClsLoginInfo.UserName + "' THEN TPI1 WHEN QMS010.TPIAgency2Name = '" + objClsLoginInfo.UserName + "' THEN TPI2 WHEN QMS010.TPIAgency3Name = '" + objClsLoginInfo.UserName + "' THEN TPI3 WHEN QMS010.TPIAgency4Name = '" + objClsLoginInfo.UserName + "' THEN TPI4 WHEN QMS010.TPIAgency5Name = '" + objClsLoginInfo.UserName + "' THEN TPI5 WHEN QMS010.TPIAgency6Name = '" + objClsLoginInfo.UserName + "' THEN TPI6 END FROM QMS010 where QMS010.Project = QMS.Project And QMS010.BU = qms.BU and QMS010.Location = qms.Location and QMS010.QualityProject = qms.QualityProject)))";
                    _urlFrom = "pending";
                    ViewBag.Status = "Pending";
                }
                else
                {
                    whereCondition += " and OfferedtoTPIBy is not null and (OfferedTPIAgency IN (SELECT CASE WHEN QMS010.TPIAgency1Name = '" + objClsLoginInfo.UserName + "' THEN TPI1 WHEN QMS010.TPIAgency2Name = '" + objClsLoginInfo.UserName + "' THEN TPI2 WHEN QMS010.TPIAgency3Name = '" + objClsLoginInfo.UserName + "' THEN TPI3 WHEN QMS010.TPIAgency4Name = '" + objClsLoginInfo.UserName + "' THEN TPI4 WHEN QMS010.TPIAgency5Name = '" + objClsLoginInfo.UserName + "' THEN TPI5 WHEN QMS010.TPIAgency6Name = '" + objClsLoginInfo.UserName + "' THEN TPI6 END FROM QMS010 where QMS010.Project = QMS.Project And QMS010.BU = qms.BU and QMS.Location = qms.Location and QMS010.QualityProject = qms.QualityProject) " +
                                         "Or OfferedTPIAgency2 IN (SELECT CASE WHEN QMS010.TPIAgency1Name = '" + objClsLoginInfo.UserName + "' THEN TPI1 WHEN QMS010.TPIAgency2Name = '" + objClsLoginInfo.UserName + "' THEN TPI2 WHEN QMS010.TPIAgency3Name = '" + objClsLoginInfo.UserName + "' THEN TPI3 WHEN QMS010.TPIAgency4Name = '" + objClsLoginInfo.UserName + "' THEN TPI4 WHEN QMS010.TPIAgency5Name = '" + objClsLoginInfo.UserName + "' THEN TPI5 WHEN QMS010.TPIAgency6Name = '" + objClsLoginInfo.UserName + "' THEN TPI6 END FROM QMS010 where QMS010.Project = QMS.Project And QMS010.BU = qms.BU and QMS010.Location = qms.Location and QMS010.QualityProject = qms.QualityProject) " +
                                         "Or OfferedTPIAgency3 IN (SELECT CASE WHEN QMS010.TPIAgency1Name = '" + objClsLoginInfo.UserName + "' THEN TPI1 WHEN QMS010.TPIAgency2Name = '" + objClsLoginInfo.UserName + "' THEN TPI2 WHEN QMS010.TPIAgency3Name = '" + objClsLoginInfo.UserName + "' THEN TPI3 WHEN QMS010.TPIAgency4Name = '" + objClsLoginInfo.UserName + "' THEN TPI4 WHEN QMS010.TPIAgency5Name = '" + objClsLoginInfo.UserName + "' THEN TPI5 WHEN QMS010.TPIAgency6Name = '" + objClsLoginInfo.UserName + "' THEN TPI6 END FROM QMS010 where QMS010.Project = QMS.Project And QMS010.BU = qms.BU and QMS010.Location = qms.Location and QMS010.QualityProject = qms.QualityProject) " +
                                         "Or OfferedTPIAgency4 IN (SELECT CASE WHEN QMS010.TPIAgency1Name = '" + objClsLoginInfo.UserName + "' THEN TPI1 WHEN QMS010.TPIAgency2Name = '" + objClsLoginInfo.UserName + "' THEN TPI2 WHEN QMS010.TPIAgency3Name = '" + objClsLoginInfo.UserName + "' THEN TPI3 WHEN QMS010.TPIAgency4Name = '" + objClsLoginInfo.UserName + "' THEN TPI4 WHEN QMS010.TPIAgency5Name = '" + objClsLoginInfo.UserName + "' THEN TPI5 WHEN QMS010.TPIAgency6Name = '" + objClsLoginInfo.UserName + "' THEN TPI6 END FROM QMS010 where QMS010.Project = QMS.Project And QMS010.BU = qms.BU and QMS010.Location = qms.Location and QMS010.QualityProject = qms.QualityProject) " +
                                         "Or OfferedTPIAgency5 IN (SELECT CASE WHEN QMS010.TPIAgency1Name = '" + objClsLoginInfo.UserName + "' THEN TPI1 WHEN QMS010.TPIAgency2Name = '" + objClsLoginInfo.UserName + "' THEN TPI2 WHEN QMS010.TPIAgency3Name = '" + objClsLoginInfo.UserName + "' THEN TPI3 WHEN QMS010.TPIAgency4Name = '" + objClsLoginInfo.UserName + "' THEN TPI4 WHEN QMS010.TPIAgency5Name = '" + objClsLoginInfo.UserName + "' THEN TPI5 WHEN QMS010.TPIAgency6Name = '" + objClsLoginInfo.UserName + "' THEN TPI6 END FROM QMS010 where QMS010.Project = QMS.Project And QMS010.BU = qms.BU and QMS010.Location = qms.Location and QMS010.QualityProject = qms.QualityProject) " +
                                         "Or OfferedTPIAgency6 IN (SELECT CASE WHEN QMS010.TPIAgency1Name = '" + objClsLoginInfo.UserName + "' THEN TPI1 WHEN QMS010.TPIAgency2Name = '" + objClsLoginInfo.UserName + "' THEN TPI2 WHEN QMS010.TPIAgency3Name = '" + objClsLoginInfo.UserName + "' THEN TPI3 WHEN QMS010.TPIAgency4Name = '" + objClsLoginInfo.UserName + "' THEN TPI4 WHEN QMS010.TPIAgency5Name = '" + objClsLoginInfo.UserName + "' THEN TPI5 WHEN QMS010.TPIAgency6Name = '" + objClsLoginInfo.UserName + "' THEN TPI6 END FROM QMS010 where QMS010.Project = QMS.Project And QMS010.BU = qms.BU and QMS010.Location = qms.Location and QMS010.QualityProject = qms.QualityProject))";
                    //END FROM QMS010)";
                    _urlFrom = "all";
                    ViewBag.Status = "All";
                }
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms.BU", "qms.Location");
                string[] columnName = { "qms.SeamNo", "dbo.GET_PROJECTDESC_BY_CODE(qms.Project)", "qms.OfferedBy", "qms.CreatedBy", "qms.Location", "dbo.GET_IPI_STAGECODE_DESCRIPTION(qms.StageCode, qms.BU, qms.Location) ", "qms.RequestNo", "qms.InspectedBy", "qms.QCRemarks", "qms.OfferedtoCustomerBy", "qms.Shop", "qms.ShopLocation", "qms.ShopRemark" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string qualityProject = param.QualityProject;
                string seamNumber = param.SeamNo;

                //if (!string.IsNullOrEmpty(qualityProject) && !string.IsNullOrEmpty(seamNumber))
                //{
                //    whereCondition += (seamNumber.Equals("ALL") ? " AND qms.QualityProject = '" + qualityProject + "' " : " AND qms.QualityProject = '" + qualityProject + "' AND qms.SeamNo ='" + seamNumber + "'");
                //}
                string strWhere = whereCondition;
                if (!string.IsNullOrEmpty(qualityProject))
                {
                    whereCondition += " AND qms.QualityProject in (SELECT Value FROM [dbo].[fn_Split]('" + qualityProject + "',','))";
                }

                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = string.Empty; ;
                sortDirection = Convert.ToString(Request["sSortDir_0"]); // asc or desc
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstHeader = db.SP_IPI_TPI_Clearance_Details(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from h in lstHeader
                          select new[] {
                       GenerateHTMLCheckboxWithEvent(Convert.ToInt32(h.HeaderId),"check",false, "", true, "clsCheckbox"),
                       h.QualityProject,
                       Convert.ToString(h.Project),
                       Convert.ToString(h.RequestId),
                       Convert.ToString(h.HeaderId),
                       Convert.ToString( h.SeamNo),
                       Convert.ToString(h.StageCode),
                       Convert.ToString(h.StageSequence),
                       Convert.ToString(h.IterationNo),
                       Convert.ToString(h.TestResult),
                       Convert.ToString(h.OfferedBy),
                       h.OfferedOn != null ? Convert.ToDateTime(h.OfferedOn).ToString("dd/MM/yyyy hh:mm tt") : "",
                       Convert.ToString(h.BU),
                       Convert.ToString( h.Location),
                       Convert.ToString(h.RequestNo),
                       Convert.ToString(h.RequestNoSequence),
                       Convert.ToString(h.InspectedBy),
                       h.InspectedOn != null ? Convert.ToDateTime(h.InspectedOn).ToString("dd/MM/yyyy hh:mm tt") : "",
                       Convert.ToString(h.QCRemarks),
                       Convert.ToString(h.OfferedtoCustomerBy),
                       h.OfferedtoCustomerOn != null ? Convert.ToDateTime(h.OfferedtoCustomerOn).ToString("dd/MM/yyyy hh:mm tt") : "",
                       Convert.ToString(h.Shop),
                       Convert.ToString(h.ShopLocation),
                       Convert.ToString(h.ShopRemark),
                           h.StageType.ToLower().Contains("chemical") || h.StageType.ToLower().Contains("ferrite") || h.StageType.ToLower().Contains("hardness") || h.StageType.ToLower().Contains("pmi") ? "<center style=\"white-space: nowrap;\" class=\"clsDisplayInlineEle\"><a target=\"_blank\" Title=\"View Details\" id=\"btnPrint\" name=\"btnPrint\" onclick=\"OpenQAClearanceReports('" + h.StageType + "', '" + h.Location.Split('-')[0].Trim() + "', '" + h.QualityProject + "', '" + h.QualityProject + "',  '" + h.SeamNo + "','" + h.SeamNo + "', '" + Manager.getDateTimeyyyyMMdd(h.InspectedOn.Value.ToShortDateString()) + "', '" + Manager.getDateTimeyyyyMMdd(h.InspectedOn.Value.ToShortDateString()) + "', '" + h.StageCode.Split('-')[0].Trim() + "', '" + h.StageCode.Split('-')[0].Trim() + "', '" + h.StageSequence + "', '" + h.StageSequence + "', '', '', '', '" + h.InspectedBy + "', '', '" + objClsLoginInfo.UserName + "', ' ', ' ');\" ><i style=\"margin - left:5px;\" class=\"fa fa-eye\"></i></a></center>"
                            :  "<center style=\"white-space: nowrap;\" class=\"clsDisplayInlineEle\"><a target=\"_blank\" Title=\"View Details\" id=\"btnPrint\" name=\"btnPrint\" onclick=\"OpenProtocolReports('" + h.ProtocolType + "', '" + h.ProtocolId + "', '" + h.HeaderId + "', '" + h.IterationNo + "', '" + h.RequestNoSequence + "');\" ><i style=\"margin - left:5px;\" class=\"fa fa-eye\"></i></a></center>"

                };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition,
                    strWhere = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ApproveTPIData(string RequestId, string TPIRemark, DateTime? TPIApproveDate)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            QMS050 objQMS050 = new QMS050();
            List<string> lstRequestId = new List<string>();

            //Get clear Date 
            if (TPIApproveDate != null)
            {
                var dt1 = Session["clearDate"].ToString();
                DateTime dt2 = Convert.ToDateTime(dt1);
                DateTime dt3 = DateTime.Now;

                if (TPIApproveDate > dt3 || TPIApproveDate < dt2)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please select Date between Clear and today Date";
                    return Json(objResponseMsg);
                }
            }
            else
            {
                TPIApproveDate = null;
            }

            if (!string.IsNullOrWhiteSpace(RequestId))
            {
                lstRequestId = RequestId.Split(',').ToList();
            }

            try
            {
                for (int j = 0; j < lstRequestId.Count; j++)
                {
                    int StoreRequestID = Convert.ToInt32(lstRequestId[j]);

                    QMS050 objData = db.QMS050.Where(u => u.RequestId == StoreRequestID).SingleOrDefault();
                    QMS010 objQMS010 = new QMS010();
                    objQMS010 = db.QMS010.Where(i => i.QualityProject == objData.QualityProject).FirstOrDefault();
                    if (objQMS010 != null)
                    {
                        string TPI1 = "";
                        string TPI2 = "";
                        string TPI3 = "";
                        string TPI4 = "";
                        string TPI5 = "";
                        string TPI6 = "";

                        if (objQMS010.TPIAgency1Name == objClsLoginInfo.UserName)
                        {
                            TPI1 = objQMS010.TPI1;
                        }
                        if (objQMS010.TPIAgency2Name == objClsLoginInfo.UserName)
                        {
                            TPI2 = objQMS010.TPI2;
                        }
                        if (objQMS010.TPIAgency3Name == objClsLoginInfo.UserName)
                        {
                            TPI3 = objQMS010.TPI3;
                        }
                        if (objQMS010.TPIAgency4Name == objClsLoginInfo.UserName)
                        {
                            TPI4 = objQMS010.TPI4;
                        }
                        if (objQMS010.TPIAgency5Name == objClsLoginInfo.UserName)
                        {
                            TPI5 = objQMS010.TPI5;
                        }
                        if (objQMS010.TPIAgency6Name == objClsLoginInfo.UserName)
                        {
                            TPI6 = objQMS010.TPI6;
                        }

                        //string TPIAgency1Name = objQMS010.TPIAgency1Name;
                        //string TPIAgency2Name = objQMS010.TPIAgency2Name;
                        //string TPIAgency3Name = objQMS010.TPIAgency3Name;
                        //string TPIAgency4Name = objQMS010.TPIAgency4Name;
                        //string TPIAgency5Name = objQMS010.TPIAgency5Name;
                        //string TPIAgency6Name = objQMS010.TPIAgency6Name;

                        //string TPI1 = objQMS010.TPI1;
                        //string TPI2 = objQMS010.TPI2;
                        //string TPI3 = objQMS010.TPI3;
                        //string TPI4 = objQMS010.TPI4;
                        //string TPI5 = objQMS010.TPI5;
                        //string TPI6 = objQMS010.TPI6;


                        string OfferTPIAgency = objData.OfferedTPIAgency;
                        string OfferTPIAgency2 = objData.OfferedTPIAgency2;
                        string OfferTPIAgency3 = objData.OfferedTPIAgency3;
                        string OfferTPIAgency4 = objData.OfferedTPIAgency4;
                        string OfferTPIAgency5 = objData.OfferedTPIAgency5;
                        string OfferTPIAgency6 = objData.OfferedTPIAgency6;


                        //string TPIAgency1 = objData.TPIAgency1;
                        //string TPIAgency2 = objData.TPIAgency2;
                        //string TPIAgency3 = objData.TPIAgency3;
                        //string TPIAgency4 = objData.TPIAgency4;
                        //string TPIAgency5 = objData.TPIAgency5;
                        //string TPIAgency6 = objData.TPIAgency6;


                        //string TPIAgency1 = objData.TPIAgency1;
                        //string TPIAgency2 = objData.TPIAgency2;
                        //string TPIAgency3 = objData.TPIAgency3;
                        //string TPIAgency4 = objData.TPIAgency4;
                        //string TPIAgency5 = objData.TPIAgency5;
                        //string TPIAgency6 = objData.TPIAgency6;

                        objQMS050 = db.QMS050.Where(i => i.RequestId == StoreRequestID).FirstOrDefault();
                        if (OfferTPIAgency == TPI1 || OfferTPIAgency == TPI2 || OfferTPIAgency == TPI3 || OfferTPIAgency == TPI4 || OfferTPIAgency == TPI5 || OfferTPIAgency == TPI6)
                        {
                            objQMS050.TPIAgency1Result = "Cleared";
                            //objQMS050.TPIAgency1ResultOn = DateTime.Now;
                            objQMS050.TPIAgency1ResultOn = TPIApproveDate;
                            objQMS050.IsTPIResultMaintained = true;
                            objQMS050.TPIAgency1PSNO = objClsLoginInfo.UserName;
                            objQMS050.TPIAgency1ResultRemark = TPIRemark;
                        }
                        if (OfferTPIAgency2 == TPI1 || OfferTPIAgency2 == TPI2 || OfferTPIAgency2 == TPI3 || OfferTPIAgency2 == TPI4 || OfferTPIAgency2 == TPI5 || OfferTPIAgency2 == TPI6)
                        {
                            objQMS050.TPIAgency2Result = "Cleared";
                            objQMS050.TPIAgency2ResultOn = TPIApproveDate;
                            // objQMS050.TPIAgency2ResultOn = DateTime.Now;
                            objQMS050.IsTPIResultMaintained = true;
                            objQMS050.TPIAgency2PSNO = objClsLoginInfo.UserName;
                            objQMS050.TPIAgency2ResultRemark = TPIRemark;
                        }
                        if (OfferTPIAgency3 == TPI1 || OfferTPIAgency3 == TPI2 || OfferTPIAgency3 == TPI3 || OfferTPIAgency3 == TPI4 || OfferTPIAgency3 == TPI5 || OfferTPIAgency3 == TPI6)
                        {
                            objQMS050.TPIAgency3Result = "Cleared";
                            objQMS050.TPIAgency3ResultOn = TPIApproveDate;
                            objQMS050.IsTPIResultMaintained = true;
                            objQMS050.TPIAgency3PSNO = objClsLoginInfo.UserName;
                            objQMS050.TPIAgency3ResultRemark = TPIRemark;
                        }
                        if (OfferTPIAgency4 == TPI1 || OfferTPIAgency4 == TPI2 || OfferTPIAgency4 == TPI3 || OfferTPIAgency4 == TPI4 || OfferTPIAgency4 == TPI5 || OfferTPIAgency4 == TPI6)
                        {
                            objQMS050.TPIAgency4Result = "Cleared";
                            objQMS050.TPIAgency4ResultOn = DateTime.Now;
                            objQMS050.IsTPIResultMaintained = true;
                            objQMS050.TPIAgency4PSNO = objClsLoginInfo.UserName;
                            objQMS050.TPIAgency4ResultRemark = TPIRemark;
                        }
                        if (OfferTPIAgency5 == TPI1 || OfferTPIAgency5 == TPI2 || OfferTPIAgency5 == TPI3 || OfferTPIAgency5 == TPI4 || OfferTPIAgency5 == TPI5 || OfferTPIAgency5 == TPI6)
                        {
                            objQMS050.TPIAgency5Result = "Cleared";
                            objQMS050.TPIAgency5ResultOn = TPIApproveDate;
                            objQMS050.IsTPIResultMaintained = true;
                            objQMS050.TPIAgency1PSNO = objClsLoginInfo.UserName;
                            objQMS050.TPIAgency5ResultRemark = TPIRemark;
                        }
                        if (OfferTPIAgency6 == TPI1 || OfferTPIAgency6 == TPI2 || OfferTPIAgency6 == TPI3 || OfferTPIAgency6 == TPI4 || OfferTPIAgency6 == TPI5 || OfferTPIAgency6 == TPI6)
                        {
                            objQMS050.TPIAgency6Result = "Cleared";
                            objQMS050.TPIAgency6ResultOn = TPIApproveDate;
                            objQMS050.IsTPIResultMaintained = true;
                            objQMS050.TPIAgency6PSNO = objClsLoginInfo.UserName;
                            objQMS050.TPIAgency6ResultRemark = TPIRemark;
                        }
                        if ((objQMS050.OfferedTPIAgency != null && objQMS050.TPIAgency1Result == null) ||
                            (objQMS050.OfferedTPIAgency2 != null && objQMS050.TPIAgency2Result == null) ||
                            (objQMS050.OfferedTPIAgency3 != null && objQMS050.TPIAgency3Result == null) ||
                            (objQMS050.OfferedTPIAgency4 != null && objQMS050.TPIAgency4Result == null) ||
                            (objQMS050.OfferedTPIAgency5 != null && objQMS050.TPIAgency5Result == null) ||
                            (objQMS050.OfferedTPIAgency6 != null && objQMS050.TPIAgency6Result == null))
                        {
                            objQMS050.IsTPIResultMaintained = false;
                        }

                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        //objResponseMsg.Status = string.Format(clsImplementationMessage.QualityProjectMessage.Update, objQMS050.QualityProject);
                        objResponseMsg.Value = Convert.ToString(objQMS050.RequestId);
                    }
                }
                return Json(objResponseMsg);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error Occures Please try again";
                return Json(objResponseMsg);
            }
        }

        public ActionResult ReturnTPIData(string RequestId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            QMS050 objQMS050 = new QMS050();
            List<string> lstRequestId = new List<string>();

            if (!string.IsNullOrWhiteSpace(RequestId))
            {
                lstRequestId = RequestId.Split(',').ToList();
            }

            try
            {
                for (int j = 0; j < lstRequestId.Count; j++)
                {
                    int StoreRequestID = Convert.ToInt32(lstRequestId[j]);

                    QMS050 objData = db.QMS050.Where(u => u.RequestId == StoreRequestID).SingleOrDefault();

                    QMS010 objQMS010 = new QMS010();
                    objQMS010 = db.QMS010.Where(i => i.QualityProject == objData.QualityProject).FirstOrDefault();
                    if (objQMS010 != null)
                    {

                        string TPI1 = "";
                        string TPI2 = "";
                        string TPI3 = "";
                        string TPI4 = "";
                        string TPI5 = "";
                        string TPI6 = "";

                        if (objQMS010.TPIAgency1Name == objClsLoginInfo.UserName)
                        {
                            TPI1 = objQMS010.TPI1;
                        }
                        if (objQMS010.TPIAgency2Name == objClsLoginInfo.UserName)
                        {
                            TPI2 = objQMS010.TPI2;
                        }
                        if (objQMS010.TPIAgency3Name == objClsLoginInfo.UserName)
                        {
                            TPI3 = objQMS010.TPI3;
                        }
                        if (objQMS010.TPIAgency4Name == objClsLoginInfo.UserName)
                        {
                            TPI4 = objQMS010.TPI4;
                        }
                        if (objQMS010.TPIAgency5Name == objClsLoginInfo.UserName)
                        {
                            TPI5 = objQMS010.TPI5;
                        }
                        if (objQMS010.TPIAgency6Name == objClsLoginInfo.UserName)
                        {
                            TPI6 = objQMS010.TPI6;
                        }

                        string OfferTPIAgency = objData.OfferedTPIAgency;
                        string OfferTPIAgency2 = objData.OfferedTPIAgency2;
                        string OfferTPIAgency3 = objData.OfferedTPIAgency3;
                        string OfferTPIAgency4 = objData.OfferedTPIAgency4;
                        string OfferTPIAgency5 = objData.OfferedTPIAgency5;
                        string OfferTPIAgency6 = objData.OfferedTPIAgency6;


                        //string OfferTPIAgency = objData.OfferedTPIAgency;
                        //string TPIAgency1 = objData.TPIAgency1;
                        //string TPIAgency2 = objData.TPIAgency2;
                        //string TPIAgency3 = objData.TPIAgency3;
                        //string TPIAgency4 = objData.TPIAgency4;
                        //string TPIAgency5 = objData.TPIAgency5;
                        //string TPIAgency6 = objData.TPIAgency6;

                        objQMS050 = db.QMS050.Where(i => i.RequestId == StoreRequestID).FirstOrDefault();
                        if (OfferTPIAgency == TPI1 || OfferTPIAgency == TPI2 || OfferTPIAgency == TPI3 || OfferTPIAgency == TPI4 || OfferTPIAgency == TPI5 || OfferTPIAgency == TPI6)
                        {
                            objQMS050.TPIAgency1Result = "Returned";
                            objQMS050.TPIAgency1ResultOn = DateTime.Now;
                            objQMS050.IsTPIResultMaintained = true;
                            objQMS050.TPIAgency1PSNO = objClsLoginInfo.UserName;
                        }
                        if (OfferTPIAgency2 == TPI1 || OfferTPIAgency2 == TPI2 || OfferTPIAgency2 == TPI3 || OfferTPIAgency2 == TPI4 || OfferTPIAgency2 == TPI5 || OfferTPIAgency2 == TPI6)
                        {
                            objQMS050.TPIAgency2Result = "Returned";
                            objQMS050.TPIAgency2ResultOn = DateTime.Now;
                            objQMS050.IsTPIResultMaintained = true;
                            objQMS050.TPIAgency2PSNO = objClsLoginInfo.UserName;
                        }
                        if (OfferTPIAgency3 == TPI1 || OfferTPIAgency3 == TPI2 || OfferTPIAgency3 == TPI3 || OfferTPIAgency3 == TPI4 || OfferTPIAgency3 == TPI5 || OfferTPIAgency3 == TPI6)
                        {
                            objQMS050.TPIAgency3Result = "Returned";
                            objQMS050.TPIAgency3ResultOn = DateTime.Now;
                            objQMS050.IsTPIResultMaintained = true;
                            objQMS050.TPIAgency3PSNO = objClsLoginInfo.UserName;
                        }
                        if (OfferTPIAgency4 == TPI1 || OfferTPIAgency4 == TPI2 || OfferTPIAgency4 == TPI3 || OfferTPIAgency4 == TPI4 || OfferTPIAgency4 == TPI5 || OfferTPIAgency4 == TPI6)
                        {
                            objQMS050.TPIAgency4Result = "Returned";
                            objQMS050.TPIAgency4ResultOn = DateTime.Now;
                            objQMS050.IsTPIResultMaintained = true;
                            objQMS050.TPIAgency4PSNO = objClsLoginInfo.UserName;
                        }
                        if (OfferTPIAgency5 == TPI1 || OfferTPIAgency5 == TPI2 || OfferTPIAgency5 == TPI3 || OfferTPIAgency5 == TPI4 || OfferTPIAgency5 == TPI5 || OfferTPIAgency5 == TPI6)
                        {
                            objQMS050.TPIAgency5Result = "Returned";
                            objQMS050.TPIAgency5ResultOn = DateTime.Now;
                            objQMS050.IsTPIResultMaintained = true;
                            objQMS050.TPIAgency5PSNO = objClsLoginInfo.UserName;
                        }
                        if (OfferTPIAgency6 == TPI1 || OfferTPIAgency6 == TPI2 || OfferTPIAgency6 == TPI3 || OfferTPIAgency6 == TPI4 || OfferTPIAgency6 == TPI5 || OfferTPIAgency6 == TPI6)
                        {
                            objQMS050.TPIAgency6Result = "Returned";
                            objQMS050.TPIAgency6ResultOn = DateTime.Now;
                            objQMS050.IsTPIResultMaintained = true;
                            objQMS050.TPIAgency6PSNO = objClsLoginInfo.UserName;
                        }
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        //objResponseMsg.Status = string.Format(clsImplementationMessage.QualityProjectMessage.Update, objQMS050.QualityProject);
                        objResponseMsg.Value = Convert.ToString(objQMS050.RequestId);
                    }
                }
                return Json(objResponseMsg);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error Occures Please try again";
                return Json(objResponseMsg);
            }
        }

        [HttpPost]
        public ActionResult GetReportType(int RequestId, string HeaderId, string BU, string Location)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            QMS050 objQMS050 = new QMS050();
            QMS002 objQMS002 = new QMS002();

            try
            {
                var strReportType = (from qms050 in db.QMS050
                                         //join qms002 in db.QMS002 on qms050.StageCode equals qms002.StageCode
                                     join qms002 in db.QMS002 on new { qms050.StageCode, qms050.BU, qms050.Location }
                                     equals new { qms002.StageCode, qms002.BU, qms002.Location }
                                     where qms050.RequestId == RequestId //&& qms002.IsProtocolApplicable == true
                                     select new { Value = qms002.StageType }).ToList().Distinct();
                string strReturnType = strReportType.Select(x => x.Value).FirstOrDefault();
                return Json(strReturnType, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error Occures Please try again";
                return Json(objResponseMsg);
            }
        }

        [HttpPost]
        public string GetInspectionAgency(string qproject)
        {
            string Agency = string.Empty;
            string inspectionagency = Manager.GetInspectionAgency(qproject);
            if (!string.IsNullOrWhiteSpace(inspectionagency))
            {
                string[] arr = inspectionagency.Split(',').ToArray();
                Agency = string.Join("#", arr.Where(x => x.ToString() != "ASME_AI").Take(3).ToArray());
            }
            return Agency;
        }

        [HttpPost]
        public ActionResult GetProtocolData(int RequestId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();

            try
            {
                QMS050 objQMS050 = db.QMS050.FirstOrDefault(x => x.RequestId == RequestId);

                if (!string.IsNullOrWhiteSpace(Convert.ToString(objQMS050.ProtocolType)))
                {
                    if (objQMS050.ProtocolId.HasValue)
                    {
                        ViewBag.ProtocolURL = WebsiteURL + "/PROTOCOL/" + objQMS050.ProtocolType + "/Linkage/" + objQMS050.ProtocolId + (((objQMS050.TestResult == null || objQMS050.TestResult == "Returned by TPI") && objQMS050.OfferedBy != null) ? "?m=1" : "");
                    }
                    else
                    {
                        QMS031 objQMS031 = db.QMS031.FirstOrDefault(c => c.Project == objQMS050.Project && c.QualityProject == objQMS050.QualityProject && c.BU == objQMS050.BU && c.Location == objQMS050.Location && c.SeamNo == objQMS050.SeamNo && c.StageCode == objQMS050.StageCode && c.StageSequance == objQMS050.StageSequence);
                        if (objQMS031 != null)
                        {
                            ViewBag.ProtocolURL = "OpenAttachmentPopUp(" + objQMS031.LineId + ",false,'QMS031/" + objQMS031.LineId + "');";
                        }
                    }
                }
                return Json(ViewBag.ProtocolURL, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error Occures Please try again";
                return Json(objResponseMsg);
            }
        }

        [HttpPost]
        public ActionResult GetUploadData(string RequestId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            List<string> lstRequestId = new List<string>();

            if (!string.IsNullOrWhiteSpace(RequestId))
            {
                lstRequestId = RequestId.Split(',').ToList();
            }
            try
            {
                string reportFilename = "";
                for (int j = 0; j < lstRequestId.Count; j++)
                {
                    int StoreRequestId = Convert.ToInt32(lstRequestId[j]);

                    #region generate pdf
                    var objQMS050 = db.QMS050.Where(i => i.RequestId == StoreRequestId).FirstOrDefault();
                    string ProjectName = objQMS050.Project;
                    string StageCode = objQMS050.StageCode;
                    string SeamNo = objQMS050.SeamNo;

                    string filenameformat = SeamNo;
                    string downloadpath = "~/Resources/IPIEmailAttachments/";
                    string Reporturl = "/IPI/QC/CHEMICAL_ANALYSIS_REPORT";
                    var strReportType = (from qms050 in db.QMS050
                                         join qms002 in db.QMS002 on new { qms050.StageCode, qms050.BU, qms050.Location }
                                         equals new { qms002.StageCode, qms002.BU, qms002.Location }
                                         where qms050.RequestId == StoreRequestId //&& qms002.IsProtocolApplicable == true
                                         select new { Value = qms002.StageType }).ToList().Distinct();
                    string strReturnType = strReportType.Select(x => x.Value).FirstOrDefault();

                    if (strReturnType.ToLower().Contains("chemical"))
                    {
                        Reporturl = "/IPI/QC/CHEMICAL_ANALYSIS_REPORT";
                    }
                    else if (strReturnType.ToLower().Contains("ferrite"))
                    {
                        Reporturl = "/IPI/QC/FERRITE_REPORT";
                    }
                    else if (strReturnType.ToLower().Contains("hardness"))
                    {
                        Reporturl = "/IPI/QC/HARDNESS_REPORT";
                    }
                    else if (strReturnType.ToLower().Contains("pmi"))
                    {
                        Reporturl = "/IPI/QC/PMI_TEST_REPORT";
                    }
                    else
                    {
                        Reporturl = "/IPI/PROTOCOL/" + objQMS050.ProtocolType;
                    }

                    List<SSRSParam> reportParams = new List<SSRSParam>();
                    if (strReturnType.ToLower().Contains("chemical") || strReturnType.ToLower().Contains("ferrite") || strReturnType.ToLower().Contains("hardness") || strReturnType.ToLower().Contains("pmi"))
                    {
                        var PrintTPI = GetInspectionAgency(objQMS050.QualityProject);

                        reportParams.Add(new SSRSParam { Param = "Location", Value = Convert.ToString(objQMS050.Location) });
                        reportParams.Add(new SSRSParam { Param = "QualityProject_f", Value = Convert.ToString(objQMS050.QualityProject) });
                        reportParams.Add(new SSRSParam { Param = "QualityProject_t", Value = Convert.ToString(objQMS050.QualityProject) });
                        reportParams.Add(new SSRSParam { Param = "SeamNo_f", Value = Convert.ToString(objQMS050.SeamNo) });
                        reportParams.Add(new SSRSParam { Param = "SeamNo_t", Value = Convert.ToString(objQMS050.SeamNo) });
                        reportParams.Add(new SSRSParam { Param = "InspectionDate_f", Value = Convert.ToString(DBNull.Value) });
                        reportParams.Add(new SSRSParam { Param = "InspectionDate_t" });
                        reportParams.Add(new SSRSParam { Param = "StageCode_f", Value = Convert.ToString(objQMS050.StageCode) });
                        reportParams.Add(new SSRSParam { Param = "StageCode_t", Value = Convert.ToString(objQMS050.StageCode) });
                        reportParams.Add(new SSRSParam { Param = "Seqn_f", Value = Convert.ToString(objQMS050.StageSequence) });
                        reportParams.Add(new SSRSParam { Param = "Seqn_t", Value = Convert.ToString(objQMS050.StageSequence) });
                        reportParams.Add(new SSRSParam { Param = "WeldingProcess", Value = Convert.ToString(DBNull.Value) });
                        reportParams.Add(new SSRSParam { Param = "SeamResult", Value = Convert.ToString(objQMS050.TestResult) });
                        reportParams.Add(new SSRSParam { Param = "TestInstrument", Value = Convert.ToString(DBNull.Value) });
                        reportParams.Add(new SSRSParam { Param = "PrintBy", Value = Convert.ToString(objClsLoginInfo.UserName) });
                        reportParams.Add(new SSRSParam { Param = "InspectionRemark", Value = Convert.ToString(DBNull.Value) });
                        reportParams.Add(new SSRSParam { Param = "PrintTPI1", Value = Convert.ToString(PrintTPI) });
                        reportParams.Add(new SSRSParam { Param = "PrintTPI2", Value = Convert.ToString(PrintTPI) });
                        reportParams.Add(new SSRSParam { Param = "PrintTPI3", Value = Convert.ToString(PrintTPI) });
                    }
                    else
                    {
                        reportParams.Add(new SSRSParam { Param = "HeaderId", Value = Convert.ToString(objQMS050.ProtocolId) });
                    }

                    reportFilename = (new Utility.Controllers.GeneralController()).GeneratePDFfromSSRSreport(Server.MapPath(downloadpath + filenameformat + ".pdf"), Reporturl, reportParams);
                    string folderpath = "/Final MRB" + "/" + ProjectName + "/" + StageCode + "/";

                    FileStream fileStream = System.IO.File.OpenRead(Server.MapPath(downloadpath + filenameformat + ".pdf"));
                    var R = (new clsFileUpload()).FileUpload(filenameformat + ".pdf", folderpath, fileStream, "", "", true);
                    #endregion
                }
                return Json(reportFilename, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error Occures Please try again";
                return Json(objResponseMsg);
            }
        }

        public ActionResult GetTPIAgencyPSNO(int RequestId)
        {
            try
            {
                List<TPIPSNOModel> LstPSNO = new List<TPIPSNOModel>();

                //var query = "select TPIAgency1PSNO,TPIAgency2PSNO,TPIAgency3PSNO,TPIAgency4PSNO,TPIAgency5PSNO,TPIAgency6PSNO from QMS050 where RequestId=3317";
                var Query = string.Format("select TPIAgency1PSNO,TPIAgency2PSNO,TPIAgency3PSNO,TPIAgency4PSNO,TPIAgency5PSNO,TPIAgency6PSNO from QMS050 where RequestId= '{0}'", RequestId);
                var data = db.Database.SqlQuery<TPIPSNOModel>(Query).ToList();
                //var data = db.QMS002.Where(x => x.StageDepartment == "QC").Select(x => x.StageType).OrderBy(x => x).Distinct().ToList();
                return Json(new { Data = data });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //End Here

        #region QA - Final Dossier
        // Redirect for open final dossier view 
        [SessionExpireFilter, UserPermissions]
        public ActionResult FinalDossier()
        {
            ViewBag.Title = "Final Dossier";
            ViewBag.Location = objClsLoginInfo.Location;
            return View();
        }

        // Load tab wise data list like pending/all
        [HttpPost]
        public ActionResult LoadFinalDossierDataGridPartial(string status, string qualityProject, string stageCode, string stageCodeType)
        {
            ViewBag.Status = status;
            ViewBag.QualityProject = qualityProject;
            ViewBag.StageCode = stageCode;
            ViewBag.stageCodeType = stageCodeType;
            return PartialView("_GetFinalDossierGridDataPartial");
        }

        //bind datatable for Data Grid
        [HttpPost]
        public JsonResult LoadFinalDossierDataGridData(JQueryDataTableParamModel param, string stageCodeType)

        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                if (param.Status.ToUpper() == "PENDING")
                {
                    whereCondition += " and IsSubmitToQA=1 and IsCompiledByQA IS NULL ";
                }
                else
                {
                    whereCondition += " and IsSubmitToQA=1 and IsCompiledByQA=1 ";
                }

                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms.BU", "qms.Location");
                string[] columnName = { "qms.SeamNo", "dbo.GET_PROJECTDESC_BY_CODE(qms.Project)", "qms.StageCode", "qms.QualityProject" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string qualityProject = param.QualityProject;
                string stageCode = param.StageCode;
                string stageCodeDesc = stageCodeType;
                string strWhere = whereCondition;
                if (!string.IsNullOrEmpty(qualityProject))
                {
                    whereCondition += " AND qms.QualityProject = '" + qualityProject + "'";
                }
                if (!string.IsNullOrEmpty(stageCode))
                {
                    if (stageCode.ToUpper() != "ALL")
                        whereCondition += " AND qms.StageCode = '" + stageCode + "'";
                }
                if (!string.IsNullOrEmpty(stageCode))
                {
                    if (stageCode.ToUpper() != "ALL")
                        whereCondition += " AND q02.StageType = '" + stageCodeDesc + "'";
                }

                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = string.Empty; ;
                sortDirection = Convert.ToString(Request["sSortDir_0"]); // asc or desc
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstHeader = db.SP_IPI_SEAMLISTOFFERINSPECTION_DETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from h in lstHeader
                          select new[] {
                           Convert.ToString(h.RequestId),
                           Convert.ToString(h.Project),
                           h.QualityProject,
                           Convert.ToString(h.SeamNo),
                           Convert.ToString(h.StageCode),
                           Convert.ToString(h.StageSequence),
                           Convert.ToString(h.IterationNo),
                           Convert.ToString(h.TestResult),
                           ""
                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition,
                    strWhere = strWhere
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetStageByQualityProject(string term, string qualityProject, string StageType)
        {
            try
            {
                // var data = db.QMS050.Where(x => x.QualityProject.Equals(qualityProject) && x.StageCode.Contains(term)).Select(x => x.StageCode).OrderBy(x => x).Distinct().ToList();
                // var Query = "select distinct Qms002.StageType from Qms002 where Qms002.StageCode in (select QMS050.StageCode from QMS050 where QMS050.QualityProject = "+ +")";
                var Query = string.Format("select  Qms002.* from Qms002 where Qms002.StageType = '" + StageType + "' and  Qms002.StageCode in (select QMS050.StageCode from QMS050 where QMS050.QualityProject = '{0}')", qualityProject);
                var data = db.Database.SqlQuery<QMS002>(Query).Select(a => a.StageCode).OrderBy(x => x).Distinct().ToList();
                //var data = db.QMS002.Where(x => x.StageType.Equals(StageType)).Select(x => x.StageCode).OrderBy(x => x).Distinct().ToList();
                return Json(new { Data = data });
            }
            catch (Exception ex)
            {
                return Json(new { Error = ex.Message });
            }
        }

        [HttpPost]
        public ActionResult GetStageCodeByStaygeTYpe(string term, string qualityProject)
        {
            try
            {
                var Query = string.Format("select Qms002.* from Qms002 where Qms002.StageCode in (select QMS050.StageCode from QMS050 where QMS050.QualityProject = '{0}')", qualityProject);
                var data = db.Database.SqlQuery<QMS002>(Query).Select(a => a.StageType).OrderBy(x => x).Distinct().ToList();
                //var data = db.QMS002.Where(x => x.StageDepartment == "QC").Select(x => x.StageType).OrderBy(x => x).Distinct().ToList();
                return Json(new { Data = data });
            }
            catch (Exception ex)
            {
                return Json(new { Error = ex.Message });
            }
        }

        public ActionResult nextprevbyProjectorStage(string stage, string qualityProject, string curStageType, string option)
        {
            ResponsenextorPre objresponse = new ResponsenextorPre();
            List<Projects> lstprojects = listqualityprojects();
            List<seamlist> lststage = null;
            List<stageType> lststageType = null;
            string forall = "ALL";
            //string forall = "";
            int projindexno = 0;
            int stageindexno = 0;
            int minstage = 0;
            int maxstage = 0;
            int minproj = 0;
            int maxproj = 0;

            try
            {
                if (!string.IsNullOrEmpty(qualityProject) && !string.IsNullOrEmpty(stage) && !string.IsNullOrEmpty(curStageType))
                {
                    projindexno = lstprojects.Select((item, index) => new { index, item }).Where(x => x.item.projectCode.ToLower() == qualityProject.ToLower()).Select(x => x.index).FirstOrDefault();
                    minproj = lstprojects.Select((item, index) => new { index, item }).Min(x => x.index);
                    maxproj = lstprojects.Select((item, index) => new { index, item }).Max(x => x.index);

                    if (!string.IsNullOrEmpty(option))
                    {
                        switch (option)
                        {
                            case "PrevProject":
                                ViewBag.Title = "PrevProject";
                                projindexno = (projindexno - 1);
                                objresponse.projprevbutton = (projindexno == minproj) ? true : ((lstprojects.Select((item, index) => new { index, item }).Any(x => x.index == projindexno)) ? false : true);
                                objresponse.projnextbutton = (lstprojects.Select((item, index) => new { index, item }).Any(x => x.index == (projindexno + 1))) ? false : true;
                                qualityProject = mainqualityProject(projindexno);
                                break;
                            case "NextProject":
                                ViewBag.Title = "NextProject";
                                projindexno = (projindexno + 1);
                                objresponse.projprevbutton = (lstprojects.Select((item, index) => new { index, item }).Any(x => x.index == (projindexno - 1))) ? false : true;
                                objresponse.projnextbutton = (projindexno == maxproj) ? true : ((lstprojects.Select((item, index) => new { index, item }).Any(x => x.index == projindexno)) ? false : true);
                                qualityProject = mainqualityProject(projindexno);
                                break;
                            default:
                                ViewBag.Title = "nextStageType";
                                break;
                        }
                    }
                    else
                    {
                        objresponse.projprevbutton = (lstprojects.Select((item, index) => new { index, item }).Any(x => x.index == (projindexno - 1))) ? false : true;
                        objresponse.projnextbutton = (lstprojects.Select((item, index) => new { index, item }).Any(x => x.index == (projindexno + 1))) ? false : true;
                    }
                    //Get Project Wise StageTYpe code.
                    lststageType = liststagewiseproject(qualityProject);

                    if (lststageType.Select((item, index) => new { index, item }).Any(x => x.item.Type.ToLower() == curStageType.ToLower()))
                    {
                        stageindexno = lststageType.Select((item, index) => new { index, item }).Where(x => x.item.Type.ToLower() == curStageType.ToLower()).Select(x => x.index).FirstOrDefault();
                        minstage = lststageType.Select((item, index) => new { index, item }).Min(x => x.index);
                        maxstage = lststageType.Select((item, index) => new { index, item }).Max(x => x.index);
                    }
                    else
                        curStageType = forall;

                    if (curStageType.ToLower() == forall.ToLower())
                    {
                        objresponse.stagetypeprevbutton = true;
                        objresponse.stagetypenextbutton = true;
                    }
                    else
                    {
                        objresponse.stagetypeprevbutton = (lststageType.Select((item, Index) => new { Index, item }).Any(x => x.Index == (stageindexno - 1))) ? false : true;
                        objresponse.stagetypenextbutton = (lststageType.Select((item, Index) => new { Index, item }).Any(x => x.Index == (stageindexno + 1))) ? false : true;
                    }


                    switch (option)
                    {
                        case "PrevStageType":
                            ViewBag.Title = "prevStageType";
                            stageindexno = (stageindexno - 1);
                            objresponse.stagetypeprevbutton = (stageindexno == minstage) ? true : ((lststageType.Select((item, index) => new { index, item }).Any(x => x.index == stageindexno)) ? false : true);
                            objresponse.stagetypenextbutton = (lststageType.Select((item, index) => new { index, item }).Any(x => x.index == (stageindexno + 1))) ? false : true;
                            curStageType = lststageType.Select((item, index) => new { index, item }).Where(x => x.index == stageindexno).FirstOrDefault().item.Type;
                            break;
                        case "nextStageType":
                            ViewBag.Title = "nextStageType";
                            stageindexno = (stageindexno + 1);
                            objresponse.stagetypeprevbutton = (lststageType.Select((item, index) => new { index, item }).Any(x => x.index == (stageindexno - 1))) ? false : true;
                            objresponse.stagetypenextbutton = (stageindexno == maxstage) ? true : ((lststageType.Select((item, index) => new { index, item }).Any(x => x.index == stageindexno)) ? false : true);
                            curStageType = lststageType.Select((item, index) => new { index, item }).Where(x => x.index == stageindexno).FirstOrDefault().item.Type;
                            break;
                        default:
                            ViewBag.Title = "NextStage";
                            break;
                    }
                    //Get Stage Type Wise  Stage Code.

                    //var StageTyepeData = liststageCodewiseproject(qualityProject, curStageType).Distinct().ToList();
                    lststage = liststageCodewiseproject(qualityProject, curStageType);

                    if (lststage.Select((item, index) => new { index, item }).Any(x => x.item.SeamNo.ToLower() == stage.ToLower()))
                    {
                        stageindexno = lststage.Select((item, index) => new { index, item }).Where(x => x.item.SeamNo.ToLower() == curStageType.ToLower()).Select(x => x.index).FirstOrDefault();
                        minstage = lststage.Select((item, index) => new { index, item }).Min(x => x.index);
                        maxstage = lststage.Select((item, index) => new { index, item }).Max(x => x.index);
                    }
                    else
                        stage = forall;

                    if (stage.ToLower() == forall.ToLower())
                    {
                        objresponse.seamprevbutton = true;
                        objresponse.seamnextbutton = true;
                    }
                    else
                    {
                        objresponse.seamprevbutton = (lststage.Select((item, Index) => new { Index, item }).Any(x => x.Index == (stageindexno - 1))) ? false : true;
                        objresponse.seamnextbutton = (lststage.Select((item, Index) => new { Index, item }).Any(x => x.Index == (stageindexno + 1))) ? false : true;
                    }


                    switch (option)
                    {
                        case "PrevStage":
                            ViewBag.Title = "prevStage";
                            stageindexno = (stageindexno - 1);
                            objresponse.seamprevbutton = (stageindexno == minstage) ? true : ((lststage.Select((item, index) => new { index, item }).Any(x => x.index == stageindexno)) ? false : true);
                            objresponse.seamnextbutton = (lststage.Select((item, index) => new { index, item }).Any(x => x.index == (stageindexno + 1))) ? false : true;
                            stage = lststage.Select((item, index) => new { index, item }).Where(x => x.index == stageindexno).FirstOrDefault().item.SeamNo;
                            break;
                        case "NextStage":
                            ViewBag.Title = "NextStage";
                            stageindexno = (stageindexno + 1);
                            objresponse.seamprevbutton = (lststage.Select((item, index) => new { index, item }).Any(x => x.index == (stageindexno - 1))) ? false : true;
                            objresponse.seamnextbutton = (stageindexno == maxstage) ? true : ((lststage.Select((item, index) => new { index, item }).Any(x => x.index == stageindexno)) ? false : true);
                            stage = lststage.Select((item, index) => new { index, item }).Where(x => x.index == stageindexno).FirstOrDefault().item.SeamNo;
                            break;
                        default:
                            ViewBag.Title = "NextStage";
                            break;
                    }


                    objresponse.project = qualityProject;
                    objresponse.seam = stage;
                    objresponse.stageType = curStageType;
                    objresponse.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objresponse.Key = false;
                objresponse.Value = "Error Occures Please try again";
            }
            return Json(objresponse, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult QACompile(string fQualityProject, string fStageCode, string RequestId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            List<string> lstRequestId = new List<string>();
            List<byte[]> lstFileBytes = new List<byte[]>();
            List<RpeortnoClass> lstReportNo = new List<RpeortnoClass>();
            var SignUrl = ConfigurationManager.AppSettings["SignUrl"];
            if (!string.IsNullOrWhiteSpace(RequestId))
            {
                lstRequestId = RequestId.Split(',').ToList();
            }
            try
            {


                var Query1 = "select QMS051_1.Reportnumber,QMS002.StageType,QMS050.StageCode,QMS050.QualityProject,  QMS050.RequestId, null ProtocolId,  " +
                                      "cast(null as int) PageCnt " +
                               "from	QMS050 " +
                                        "INNER JOIN QMS040 ON QMS050.HeaderId = QMS040.HeaderId " +
                                        "INNER JOIN QMS002 ON QMS002.StageCode = QMS050.StageCode And QMS002.BU = QMS050.BU And QMS002.Location = QMS050.Location " +
                                        "INNER JOIN QMS051_1 ON QMS051_1.Project = QMS040.QualityProject and QMS051_1.Stagecode = QMS040.StageCode and QMS051_1.ReportDate = (cast(QMS040.LNTInspectorResultOn as date)) " +
                               "where   QMS050.RequestId in (" + RequestId + ") " +
                                       "and QMS002.StageType  in ('PMI','Hardness','Ferrite','Chemical')" +

                               "union all " +
                               "select  QMS051_1.Reportnumber,QMS002.StageType,QMS050.StageCode,QMS050.QualityProject, null as RequestIds,QMS050.ProtocolId,   " +
                                       "cast(null as int) PageCnt " +
                               "from    QMS050 " +
                                       "INNER JOIN QMS040 ON QMS050.HeaderId = QMS040.HeaderId " +
                                       "INNER JOIN QMS002 ON QMS002.StageCode = QMS050.StageCode And QMS002.BU = QMS050.BU And QMS002.Location = QMS050.Location " +
                                       "INNER JOIN QMS051_1 ON QMS051_1.Project = QMS040.QualityProject and QMS051_1.Stagecode = QMS040.StageCode and QMS051_1.ReportDate = (cast(QMS040.LNTInspectorResultOn as date)) " +
                               "where QMS050.RequestId in (" + RequestId + ") " +
                                        "and QMS002.StageType not in ('PMI','Hardness','Ferrite','Chemical')  " +
                                "order by QMS051_1.Reportnumber,QMS050.QualityProject,QMS050.StageCode,QMS002.StageType ";





                lstReportNo = db.Database.SqlQuery<RpeortnoClass>(Query1).Select(x => new RpeortnoClass { Reportnumber = x.Reportnumber, RequestId = x.RequestId, StageType = x.StageType, StageCode = x.StageCode, QualityProject = x.QualityProject, ProtocolId = x.ProtocolId, PageCnt = x.PageCnt }).OrderBy(x => x.Reportnumber).Distinct().ToList();


                for (int j = 0; j < lstReportNo.Count; j++)
                {

                    string Reporturl = "";

                    string strReturnType = lstReportNo[j].StageType;

                    if (strReturnType.ToLower().Contains("chemical"))
                    {
                        Reporturl = "/IPI/QC/CHEMICAL_ANALYSIS_REPORT";
                    }
                    else if (strReturnType.ToLower().Contains("ferrite"))
                    {
                        Reporturl = "/IPI/QC/FERRITE_REPORT";
                    }
                    else if (strReturnType.ToLower().Contains("hardness"))
                    {
                        Reporturl = "/IPI/QC/HARDNESS_REPORT";
                    }
                    else if (strReturnType.ToLower().Contains("pmi"))
                    {
                        Reporturl = "/IPI/QC/PMI_TEST_REPORT";
                    }
                    else if (lstReportNo[j].ProtocolId != null)
                    {
                        Reporturl = "/IPI/PROTOCOL/" + lstReportNo[j].ProtocolId;
                    }
                    else
                    {
                        Reporturl = string.Empty;
                    }

                    if (!string.IsNullOrWhiteSpace(Reporturl))
                    {
                        List<SSRSParam> reportParams = new List<SSRSParam>();
                        if (strReturnType.ToLower().Contains("chemical") || strReturnType.ToLower().Contains("ferrite") || strReturnType.ToLower().Contains("hardness") || strReturnType.ToLower().Contains("pmi"))
                        {
                            reportParams.Add(new SSRSParam { Param = "Location", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "QualityProject_f", Value = Convert.ToString(lstReportNo[j].QualityProject) });
                            reportParams.Add(new SSRSParam { Param = "QualityProject_t", Value = Convert.ToString(lstReportNo[j].QualityProject) });
                            reportParams.Add(new SSRSParam { Param = "SeamNo_f", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "SeamNo_t", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "InspectionDate_f", Value = Convert.ToString(DBNull.Value) });
                            reportParams.Add(new SSRSParam { Param = "InspectionDate_t" });
                            reportParams.Add(new SSRSParam { Param = "StageCode_f", Value = Convert.ToString(lstReportNo[j].StageCode) });
                            reportParams.Add(new SSRSParam { Param = "StageCode_t", Value = Convert.ToString(lstReportNo[j].StageCode) });
                            reportParams.Add(new SSRSParam { Param = "Seqn_f", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "Seqn_t", Value = "" });
                            if (!strReturnType.ToLower().Contains("hardness") && !strReturnType.ToLower().Contains("pmi"))
                            {
                                reportParams.Add(new SSRSParam { Param = "WeldingProcess", Value = "" });
                            }
                            reportParams.Add(new SSRSParam { Param = "SeamResult", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "TestInstrument", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintBy", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "InspectionRemark", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintTPI1", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintTPI2", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintTPI3", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "RequestId", Value = Convert.ToString(lstReportNo[j].RequestId) });
                            reportParams.Add(new SSRSParam { Param = "TPIDateApproval", Value = "No" });
                            reportParams.Add(new SSRSParam { Param = "SignURL", Value = SignUrl });
                            reportParams.Add(new SSRSParam { Param = "PageCount", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "TotalPage", Value = "" });

                        }
                        else
                        {
                            reportParams.Add(new SSRSParam { Param = "HeaderId", Value = Convert.ToString(lstReportNo[j].ProtocolId) });
                        }

                        byte[] fileBytes = (new Utility.Controllers.GeneralController()).GetPDFBytesfromSSRSreport(Reporturl, reportParams);
                        var reader = new iTextSharp.text.pdf.PdfReader(fileBytes);
                        var total = reader.NumberOfPages;
                        lstReportNo[j].PageCnt = total;

                    }



                }




                for (int j = 0; j < lstReportNo.Count; j++)
                {
                    QMS050 objQMS050 = new QMS050();
                    int PageCount = 0;
                    int _RequestId = Convert.ToInt32(lstReportNo[j].RequestId);
                    int TOtalPage = 0;
                    int MainTotal = 0;
                    var lst = lstReportNo;
                    var data = lstReportNo.FirstOrDefault(a => a.RequestId == lstReportNo[j].RequestId && a.Reportnumber == lstReportNo[j].Reportnumber);

                    var data2 = lstReportNo.Where(a => a.Reportnumber == lstReportNo[j].Reportnumber && a.RequestId < lstReportNo[j].RequestId).Sum(a => a.PageCnt);

                    PageCount = PageCount + Convert.ToInt32(data2);

                    var data3 = lstReportNo.Where(a => a.Reportnumber == lstReportNo[j].Reportnumber).Sum(a => a.PageCnt);
                    MainTotal = Convert.ToInt32(data3);

                    string Reporturl = "";
                    string strReturnType = lstReportNo[j].StageType;

                    if (strReturnType.ToLower().Contains("chemical"))
                    {
                        Reporturl = "/IPI/QC/CHEMICAL_ANALYSIS_REPORT";
                    }
                    else if (strReturnType.ToLower().Contains("ferrite"))
                    {
                        Reporturl = "/IPI/QC/FERRITE_REPORT";
                    }
                    else if (strReturnType.ToLower().Contains("hardness"))
                    {
                        Reporturl = "/IPI/QC/HARDNESS_REPORT";
                    }
                    else if (strReturnType.ToLower().Contains("pmi"))
                    {
                        Reporturl = "/IPI/QC/PMI_TEST_REPORT";
                    }
                    else if (lstReportNo[j].ProtocolId != null)
                    {
                        Reporturl = "/IPI/PROTOCOL/" + lstReportNo[j].ProtocolId;
                    }
                    else
                    {
                        Reporturl = string.Empty;
                    }

                    if (!string.IsNullOrWhiteSpace(Reporturl))
                    {
                        List<SSRSParam> reportParams = new List<SSRSParam>();
                        if (strReturnType.ToLower().Contains("chemical") || strReturnType.ToLower().Contains("ferrite") || strReturnType.ToLower().Contains("hardness") || strReturnType.ToLower().Contains("pmi"))
                        {
                            reportParams.Add(new SSRSParam { Param = "Location", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "QualityProject_f", Value = Convert.ToString(lstReportNo[j].QualityProject) });
                            reportParams.Add(new SSRSParam { Param = "QualityProject_t", Value = Convert.ToString(lstReportNo[j].QualityProject) });
                            reportParams.Add(new SSRSParam { Param = "SeamNo_f", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "SeamNo_t", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "InspectionDate_f", Value = Convert.ToString(DBNull.Value) });
                            reportParams.Add(new SSRSParam { Param = "InspectionDate_t" });
                            reportParams.Add(new SSRSParam { Param = "StageCode_f", Value = Convert.ToString(lstReportNo[j].StageCode) });
                            reportParams.Add(new SSRSParam { Param = "StageCode_t", Value = Convert.ToString(lstReportNo[j].StageCode) });
                            reportParams.Add(new SSRSParam { Param = "Seqn_f", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "Seqn_t", Value = "" });
                            if (!strReturnType.ToLower().Contains("hardness") && !strReturnType.ToLower().Contains("pmi"))
                            {
                                reportParams.Add(new SSRSParam { Param = "WeldingProcess", Value = "" });
                            }
                            reportParams.Add(new SSRSParam { Param = "SeamResult", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "TestInstrument", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintBy", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "InspectionRemark", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintTPI1", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintTPI2", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "PrintTPI3", Value = "" });
                            reportParams.Add(new SSRSParam { Param = "RequestId", Value = Convert.ToString(lstReportNo[j].RequestId) });
                            reportParams.Add(new SSRSParam { Param = "TPIDateApproval", Value = "No" });
                            reportParams.Add(new SSRSParam { Param = "SignURL", Value = SignUrl });
                            reportParams.Add(new SSRSParam { Param = "PageCount", Value = Convert.ToString(PageCount) });
                            reportParams.Add(new SSRSParam { Param = "TotalPage", Value = Convert.ToString(MainTotal) });

                        }
                        else
                        {
                            reportParams.Add(new SSRSParam { Param = "HeaderId", Value = Convert.ToString(lstReportNo[j].ProtocolId) });
                        }

                        byte[] fileBytes = (new Utility.Controllers.GeneralController()).GetPDFBytesfromSSRSreport(Reporturl, reportParams);
                        var reader = new iTextSharp.text.pdf.PdfReader(fileBytes);
                        var total = reader.NumberOfPages;
                        lstReportNo[j].PageCnt = total;
                        if (fileBytes != null)
                        {
                            lstFileBytes.Add(fileBytes);
                        }
                        PageCount++;
                    }
                    objQMS050 = db.QMS050.FirstOrDefault(a => a.RequestId == _RequestId);
                    if (objQMS050 != null)
                    {

                        objQMS050.IsCompiledByQA = true;
                        objQMS050.CompiledBy = objClsLoginInfo.UserName;
                        objQMS050.CompiledOn = DateTime.Now;
                        db.SaveChanges();

                    }

                }



                if (lstFileBytes.Count > 0)
                {
                    // string fileName = fQualityProject;
                    string fileName = fQualityProject + "_" + fStageCode;
                    fileName = string.Join("�", fileName.Split(Path.GetInvalidFileNameChars()));
                    string pdfName = String.Format("{0}_{1}.{2}", fileName, DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"), "pdf");
                    //string pdfName = String.Format("{0}", fileName, DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"), "pdf");
                    string OutFile = Server.MapPath("~/Resources/Download/") + pdfName;
                    //string outFilePath = (new IEMQS.Areas.Utility.Controllers.FileUploadController()).MergedPDFFileWithMultipleFileBytes(lstFileBytes, OutFile);
                    string outFilePath = MergedPDFFileWithMultipleFileBytes(lstFileBytes, OutFile);

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = outFilePath;
                    objResponseMsg.CheckStageType = pdfName;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No file found.";
                }

                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error Occures Please try again";
                return Json(objResponseMsg);
            }
        }

        public FileResult Download(string filePath, string FileName)
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            string fileName = FileName;
            System.IO.File.Delete(filePath);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Pdf, fileName);
        }

        private string MergedPDFFileWithMultipleFileBytes(List<byte[]> lstFileBytes, string fileName, string filetype = "pdf")
        {

            try
            {
                using (var doc = new iTextSharp.text.Document())
                using (FileStream stream = new FileStream(fileName, FileMode.Create))
                using (var pdf = new iTextSharp.text.pdf.PdfCopy(doc, stream))
                {
                    doc.Open();
                    foreach (var fileBytes in lstFileBytes)
                    {
                        try
                        {
                            var reader = new iTextSharp.text.pdf.PdfReader(fileBytes);
                            iTextSharp.text.pdf.PdfImportedPage page = null;

                            for (int i = 0; i < reader.NumberOfPages; i++)
                            {
                                page = pdf.GetImportedPage(reader, i + 1);
                                pdf.AddPage(page);
                            }
                            pdf.FreeReader(reader);
                            reader.Close();
                        }
                        catch (Exception ex)
                        {
                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        }
                    }

                    pdf.Close();
                    stream.Close();
                }
            }
            catch (Exception ex)
            {
                System.IO.File.Delete(fileName);
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return fileName;
        }

        public ActionResult GetClearDateForTPIApproveDate(string RequestId)
        {
            try
            {
                List<string> lstRequestId = new List<string>();
                List<TPIApprovalDate> lstTPIDate = new List<TPIApprovalDate>();
                List<QMS050> GetData = new List<QMS050>();
                if (!string.IsNullOrWhiteSpace(RequestId))
                {
                    lstRequestId = RequestId.Split(',').ToList();
                }

                for (int i = 0; i < lstRequestId.Count; i++)
                {
                    int StoreRequestID = Convert.ToInt32(lstRequestId[i]);
                    var data = db.QMS050.FirstOrDefault(a => a.RequestId == StoreRequestID);
                    if (data != null)
                    {
                        TPIApprovalDate objModel = new TPIApprovalDate();

                        if (data.InspectedOn != null)
                        {
                            // objModel.RequestId = StoreRequestID;
                            objModel.TPIApproveDate = Convert.ToString(data.InspectedOn);
                            lstTPIDate.Add(objModel);
                        }
                    }
                }

                var tpidate = lstTPIDate.OrderByDescending(a => a.TPIApproveDate).FirstOrDefault();
                DateTime dt1 = Convert.ToDateTime(tpidate.TPIApproveDate);
                var dt2 = dt1.ToString("yyyy/MM/dd");
                Session["clearDate"] = dt1;
                //string   dt1 = tpidate.ToString();
                return Json(new { Status = "OK", data = dt2, Message = "" });
            }
            catch (Exception ex)
            {
                return Json(new { Status = "FAIL", data = "", Message = "" });
                throw ex;
            }
        }

        #endregion



        [HttpPost]
        public ActionResult GetTPIRemark(string term, string qualityProject)
        {
            try
            {
                List<string> lstData = new List<string>();

                lstData.Add("Random");
                lstData.Add("Witnessed");
                lstData.Add("Random Witnessed");


                return Json(new { Data = lstData });
            }
            catch (Exception ex)
            {
                return Json(new { Error = ex.Message });
            }
        }
    }

    public class ResponsenextorPre : clsHelper.ResponseMsg
    {
        public string project { get; set; }
        public string seam { get; set; }
        public string stageType { get; set; }
        public bool projnextbutton { get; set; }
        public bool projprevbutton { get; set; }
        public bool seamnextbutton { get; set; }
        public bool seamprevbutton { get; set; }
        public bool stagetypenextbutton { get; set; }
        public bool stagetypeprevbutton { get; set; }
    }

    public class seamlist
    {
        public string SeamNo { get; set; }
    }

    public class stageType
    {
        public string Type { get; set; }
    }

    public class TPIApprovalDate
    {
        public string TPIApproveDate { get; set; }
        //public int RequestId { get; set; }
    }
    public class RpeortnoClass
    {
        public string QualityProject { get; set; }
        public string StageCode { get; set; }
        public string StageType { get; set; }
        public string Reportnumber { get; set; }
        public int RequestId { get; set; }
        public string ProtocolId { get; set; }
        public int? PageCnt { get; set; }

    }

    public class SeamAdditionalData
    {
        public string ManufacturingCode;
        public string AcceptanceStandard;
        public string ApplicationSpecification;
        public string InspectionExtent;
        public string WERemarks;

        public string SeamNo;
        public string SeamDescription;
        public int? SeamLength;
        public int? OverlayArea;
        public string OverlayThickness;
        public string JointType;

        public string Part1Position;
        public string Part1Thickness;
        public string Part1Materials;
        public string Part2Position;
        public string Part2Thickness;
        public string Part2Materials;
        public string Part3Position;
        public string Part3Thickness;
        public string Part3Materials;
        public string Part4Position;
        public string Part4Thickness;
        public string Part4Materials;
        public string Part5Position;
        public string Part5Thickness;
        public string Part5Materials;
        public string Part6Position;
        public string Part6Thickness;
        public string Part6Materials;

    }
    public class WelderStamlList : CategoryData
    {
        public string QualityProject { get; set; }
        public string SeamNo { get; set; }
        public string WelderStamp { get; set; }
        public string WeldingProcess { get; set; }
    }

    public class lststages : CategoryData
    {
        public string StageDesc { get; set; }
        public string StageCode { get; set; }
        public string BU { get; set; }
        public string Location { get; set; }
    }

    public class DropDownModel
    {
        public string Value { get; set; }
        public string Text { get; set; }
    }

    public class TPIPSNOModel
    {
        public string TPIAgency1PSNO { get; set; }
        public string TPIAgency2PSNO { get; set; }
        public string TPIAgency3PSNO { get; set; }
        public string TPIAgency4PSNO { get; set; }
        public string TPIAgency5PSNO { get; set; }
        public string TPIAgency6PSNO { get; set; }
    }
}