﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQS.Models;
using IEMQS.Areas.Utility.Models;
using IEMQS.Areas.NDE.Models;

namespace IEMQS.Areas.IPI.Controllers
{
    public class ApproveQualityIdController : clsBase
    {
        // GET: IPI/ApproveQualityId
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetApproveQualityIDGridDataPartial(string status, string qualityProject, string Location, string BU)
        {
            ViewBag.status = status;
            ViewBag.qualityProject = qualityProject;
            ViewBag.location = Location;
            ViewBag.bu = BU;
            return PartialView("_GetApproveQualityIDGridDataPartial");
        }

        public ActionResult LoadHeaderDataTable(JQueryDataTableParamModel param, string status)
        {
            try
            {
                var role = (from a in db.ATH001
                            join b in db.ATH004 on a.Role equals b.Id
                            where a.Employee.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                            select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;

                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms15.BU", "qms15.Location");
                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and qms15.Status in('" + clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue() + "')";
                }
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or qms15.QualityProject like '%" + param.sSearch + "%' or (qms15.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%')";
                }

                #region Condition For QID Type
                if (role.Where(i => i.RoleDesc.Equals("we3", StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    whereCondition += " and qms15.QualityIdApplicableFor in('" + clsImplementationEnum.QualityIDApplicable.SEAM.GetStringValue() + "')";
                }
                else if (role.Where(i => i.RoleDesc.Equals("qc3", StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    whereCondition += " and qms15.QualityIdApplicableFor in('" + clsImplementationEnum.QualityIDApplicable.PART.GetStringValue() + "', '" + clsImplementationEnum.QualityIDApplicable.ASSEMBLY.GetStringValue() + "')";
                }
                #endregion

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstHeader = db.SP_IPI_GET_QUALITYPROJECT_RESULT(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstHeader
                          select new[] {
                        //Convert.ToString(a.HasLines),
                        Convert.ToString(a.HeaderId),
                        a.QualityProject,
                        a.Project,
                        a.BU,
                        a.Location,
                        //a.BU,
                        //a.Project,
                        //a.QualityId,
                        //string.IsNullOrEmpty(a.QualityIdDesc) ?"":a.QualityIdDesc,
                        //a.CreatedBy,
                        //a.EditedBy,
                        
                        //a.Status,
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult LoadQualityIDDataTable(JQueryDataTableParamModel param, string qualityProj, string status, string location, string bu)
        {
            try
            {
                var role = (from a in db.ATH001
                            join b in db.ATH004 on a.Role equals b.Id
                            where a.Employee.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase) && a.Location == location && a.BU == bu
                            select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms15.BU", "qms15.Location");
                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(qualityProj))
                {
                    whereCondition += " AND UPPER(QualityProject) = '" + qualityProj.ToUpper() + "'";
                }
                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and qms15.Status in('" + clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue() + "')";
                }

                #region Condition For QID Type
                if (role.Where(i => i.RoleDesc.Equals("we3", StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    whereCondition += " and qms15.QualityIdApplicableFor in('" + clsImplementationEnum.QualityIDApplicable.SEAM.GetStringValue() + "')";
                }
                else if (role.Where(i => i.RoleDesc.Equals("qc3", StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    whereCondition += " and qms15.QualityIdApplicableFor in('" + clsImplementationEnum.QualityIDApplicable.PART.GetStringValue() + "', '" + clsImplementationEnum.QualityIDApplicable.ASSEMBLY.GetStringValue() + "')";
                }
                #endregion

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (qms15.QualityId like '%" + param.sSearch
                       + "%' or qms15.QualityIdDesc like '%" + param.sSearch
                       + "%' or qms15.QualityIdApplicableFor like '%" + param.sSearch
                       + "%' or qms15.RevNo like '%" + param.sSearch
                       + "%' or qms15.Status like '%" + param.sSearch
                       + "%' or ecom3.t_psno like '%" + param.sSearch
                       + "%' or ecom3.t_name like '%" + param.sSearch
                       + "%')";
                }
                var lstResult = db.SP_IPI_GetQualityIDHeader(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                List<SelectListItem> BoolenList = new List<SelectListItem>() { new SelectListItem { Text = "Yes", Value = "Yes" }, new SelectListItem { Text = "No", Value = "No" } };

                var Location = (from a in db.COM002
                                where a.t_dtyp == 1 && a.t_dimx != ""
                                select new { a.t_dimx, Desc = a.t_desc }).ToList();


                var data = (from uc in lstResult
                            select new[]
                            {
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.HasLines),
                               Convert.ToString(uc.QualityId),
                               Convert.ToString(uc.QualityIdDesc),
                               Convert.ToString(uc.QualityIdApplicableFor),
                               "R"+Convert.ToString(uc.RevNo),
                               Convert.ToString(uc.Status),
                               "",
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetQualityIDLinesPartial(int headerId)
        {
            QMS016 objQMS016 = new QMS016();
            QMS015 objQMS015 = new QMS015();
            if (headerId > 0)
                objQMS015 = db.QMS015.Where(i => i.HeaderId == headerId).FirstOrDefault();
            ViewBag.QualityIdName = objQMS015.QualityId;
            ViewBag.headerId = headerId;

            return PartialView("_GetQualityIDLinesPartial");
        }

        [HttpPost]
        public ActionResult LoadQualityIDStagesDataTable(JQueryDataTableParamModel param, int refHeaderId)
        {
            try
            {
                NDEModels objNDEModels = new NDEModels();
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;
                string strSortOrder = string.Empty;
                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms16.BU", "qms16.Location");
                whereCondition += " AND  qms16.HeaderId = " + refHeaderId;
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += " and (qms16.StageSequance like '%" + param.sSearch + "%' or qms16.StageCode like '%" + param.sSearch + "%' or qms16.StageStatus like '%" + param.sSearch + "%' or qms16.AcceptanceStandard like '%" + param.sSearch + "%' or qms16.ApplicableSpecification like '%" + param.sSearch + "%' or qms16.InspectionExtent like '%" + param.sSearch + "%' or qms16.Remarks like '%" + param.sSearch + "%' or qms16.QualityId like '%" + param.sSearch + "%')";
                }
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstStages = db.SP_IPI_GetQualityIdLines(startIndex, endIndex, "", whereCondition).ToList();
                int? totalRecords = lstStages.Select(i => i.TotalCount).FirstOrDefault();

                //List<SelectListItem> BoolenList = new List<SelectListItem>() { new SelectListItem { Text = "Yes", Value = "Yes" }, new SelectListItem { Text = "No", Value = "No" } };

                //var Location = (from a in db.COM002
                //                where a.t_dtyp == 1 && a.t_dimx != ""
                //                select new { a.t_dimx, Desc = a.t_desc }).ToList();

                var data = (from uc in lstStages
                            select new[]
                            {
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Convert.ToString(uc.QualityId),
                               uc.StageCode,
                               Convert.ToString(uc.StageSequance),
                               uc.AcceptanceStandard,
                               uc.ApplicableSpecification,
                               objNDEModels.GetInspectionExtent(uc.InspectionExtent, uc.BU.Split('-')[0].Trim(), uc.Location.Split('-')[0].Trim()).CategoryDescription,
                               //uc.InspectionExtent,
                               Convert.ToString(uc.Remarks),
                               uc.StageStatus,
                               Helper.MaintainIPIStagetypewiseGridButton(uc.HeaderId,uc.LineId,uc.QualityProject,uc.Project.Split('-')[0].Trim(),uc.BU.Split('-')[0].Trim(),uc.Location.Split('-')[0].Trim(),uc.QualityId,uc.RevNo,uc.StageCode.Split('-')[0].Trim(),uc.StageCode,uc.StageSequance,false,(new QualityIdController()).StageTypeAction(uc.HeaderId, uc.StageCode.Split('-')[0].Trim(), uc.BU.Split('-')[0].Trim(), uc.Location.Split('-')[0].Trim(),true),true,uc.StageType)
                            //Helper.MaintainIPISamplingPlanGridButton(uc.HeaderId,uc.LineId,uc.RevNo,uc.StageCode,false,(new QualityIdController()).IsSamplingPlanisMaintained(uc.HeaderId, uc.StageCode.Split('-')[0].Trim(),uc.BU.Split('-')[0].Trim(), uc.Location.Split('-')[0].Trim(),true),true)
                           }).ToList();


                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstStages.Count > 0 ? lstStages.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstStages.Count > 0 ? lstStages.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]
        public ActionResult ViewQualityId(int headerId)
        {
            QMS015 objQMS015 = new QMS015();

            if (headerId > 0)
            {
                objQMS015 = db.QMS015.Where(i => i.HeaderId == headerId).FirstOrDefault();
                ViewBag.Project = db.COM001.Where(i => i.t_cprj.Equals(objQMS015.Project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + "-" + i.t_dsca).FirstOrDefault();
                ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objQMS015.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                ViewBag.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objQMS015.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();

                //Identical Project Based On QualityProject
                ViewBag.IdenticalProject = string.Join(",", db.QMS017.Where(i => i.QualityProject == objQMS015.QualityProject).Select(i => i.IQualityProject).ToList());

            }
            else
            {
                objQMS015.HeaderId = 0;
                objQMS015.Status = clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue();
                objQMS015.RevNo = 0;
                ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objClsLoginInfo.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            }

            //if (headerId > 0)
            //    objQMS015 = db.QMS015.Where(i => i.HeaderId == headerId).FirstOrDefault();

            //var location = (from a in db.COM003
            //                join b in db.COM002 on a.t_loca equals b.t_dimx
            //                where b.t_dtyp == 1 && a.t_psno.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
            //                select b.t_dimx + " - " + b.t_desc).FirstOrDefault();

            //ViewBag.Location = location;
            //ViewBag.Project = db.COM001.Where(i => i.t_cprj.Equals(objQMS015.Project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + "-" + i.t_dsca).FirstOrDefault();

            return View(objQMS015);
        }

        public ActionResult LoadQualityIdLinesDataTable(JQueryDataTableParamModel param, int refHeaderId)
        {
            try
            {
                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms16.BU", "qms16.Location");
                //if (status.ToUpper() == "PENDING")
                //{
                //    whereCondition += " and qms15.Status in('" + clsImplementationEnum.QualityIdStageStatus.Draft.GetStringValue() + "')";
                //}
                whereCondition += " AND qms15.HeaderId = " + refHeaderId;
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or qms16.StageCode like '%" + param.sSearch + "%' or qms16.StageStatus like '%" + param.sSearch + "%' or qms16.AcceptanceStandard like '%" + param.sSearch + "%' or qms16.ApplicableSpecification like '%" + param.sSearch + "%' or qms16.InspectionExtent like '%" + param.sSearch + "%' or qms16.Remarks like '%" + param.sSearch + "%' or qms16.QualityId like '%" + param.sSearch + ")";
                }
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstHeader = db.SP_IPI_GetQualityIdLines(startIndex, endIndex, "", whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstHeader
                          select new[] {
                        Convert.ToString(a.HeaderId),
                        Convert.ToString(a.LineId),
                        Convert.ToString(a.QualityId),
                        a.StageCode ,
                        a.StageStatus,
                        a.AcceptanceStandard,
                        a.ApplicableSpecification,
                        a.InspectionExtent,
                        a.Remarks
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition

                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult ApproveQualityId(string strHeaderIds)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            List<string> lstQIDNotSendForApprove = new List<string>();
            List<string> lstQIDAttended = new List<string>();
            try
            {
                if (!string.IsNullOrEmpty(strHeaderIds))
                {
                    int[] headerIds = Array.ConvertAll(strHeaderIds.Split(','), i => Convert.ToInt32(i));
                    foreach (var headerId in headerIds)
                    {
                        var objQMS015 = db.QMS015.Where(i => i.HeaderId == headerId).FirstOrDefault();
                        if (objQMS015.Status == clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue())
                        {
                            if (objQMS015.RevNo > 0)
                            {
                                var stageStatus = clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue();
                                var lstStages = db.QMS016.Where(i => i.HeaderId == headerId).ToList();
                                if (lstStages.Any())
                                {
                                    InsUpdOnTestPlanNEW(objQMS015, lstStages);
                                    //objResponseMsg = InsUpdOnTestPlan(objQMS015.QualityId, lstStages);
                                }
                            }
                            db.SP_IPI_ApproveQualityID(headerId, objClsLoginInfo.UserName);
                            lstQIDAttended.Add(headerId.ToString());
                            #region Send Notification
                            (new clsManager()).SendNotification(objQMS015.QualityIdApplicableFor == clsImplementationEnum.QualityIDApplicable.SEAM.GetStringValue() ? "WE3" : "QC3", objQMS015.Project, objQMS015.BU, objQMS015.Location, "QID: " + objQMS015.QualityId + " has been Approved", clsImplementationEnum.NotificationType.Information.GetStringValue());
                            #endregion
                        }
                        else
                        {
                            lstQIDNotSendForApprove.Add(headerId.ToString());
                        }
                    }
                    objResponseMsg.Value = string.Format(clsImplementationMessage.QualityIDMessage.Approve.ToString(), lstQIDAttended.Count());

                    if (lstQIDNotSendForApprove.Any())
                    {
                        objResponseMsg.Remarks = string.Format("QualityID(s) {0} have been skipped as QualityID(s) has been already attended", lstQIDNotSendForApprove.Count());
                    }
                    objResponseMsg.Key = true;
                    //objResponseMsg.Value = string.Format(clsImplementationMessage.QualityIDMessage.Approve.ToString(), headerIds.Count());
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReturnQualityId(string strHeaderIds, string returnremark)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            List<string> lstQIDNotSendForApprove = new List<string>();
            List<string> lstQIDAttended = new List<string>();
            try
            {
                if (!string.IsNullOrEmpty(strHeaderIds))
                {
                    int[] headerIds = Array.ConvertAll(strHeaderIds.Split(','), i => Convert.ToInt32(i));
                    foreach (var headerId in headerIds)
                    {
                        var objQMS015 = db.QMS015.Where(i => i.HeaderId == headerId).FirstOrDefault();
                        if (objQMS015 != null)
                        {
                            if (objQMS015.Status == clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue())
                            {
                                objQMS015.Status = clsImplementationEnum.QualityIdHeaderStatus.Returned.GetStringValue();
                                objQMS015.ReturnedBy = objClsLoginInfo.UserName;
                                objQMS015.ReturnedOn = DateTime.Now;
                                objQMS015.ReturnRemark = returnremark;
                                db.SaveChanges();
                                lstQIDAttended.Add(headerId.ToString());
                                #region Send Notification
                                (new clsManager()).SendNotification(objQMS015.QualityIdApplicableFor == clsImplementationEnum.QualityIDApplicable.SEAM.GetStringValue() ? "WE3" : "QC3", objQMS015.Project, objQMS015.BU, objQMS015.Location, "QID: " + objQMS015.QualityId + " has been Returned", clsImplementationEnum.NotificationType.Information.GetStringValue());
                                #endregion
                            }
                            else
                            {
                                lstQIDNotSendForApprove.Add(headerId.ToString());
                            }
                        }
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = string.Format(clsImplementationMessage.QualityIDMessage.Returne.ToString(), lstQIDAttended.Count());
                    if (lstQIDNotSendForApprove.Any())
                    {
                        objResponseMsg.Remarks = string.Format("QualityID(s) {0} have been skipped as QualityID(s) {0} has been already attended", string.Join(",", lstQIDNotSendForApprove));
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public void InsUpdOnTestPlanNEW(QMS015 objQMS015, List<QMS016> lstQMS16)
        {

            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            List<QMS020> lstQMS020 = new List<QMS020>();
            List<QMS035> lstQMS035 = new List<QMS035>();
            clsManager mngr = new clsManager();
            List<string> lstQproj = new List<string>();
            lstQproj.Add(objQMS015.QualityProject);
            lstQproj.AddRange(db.QMS017.Where(c => c.BU == objQMS015.BU && c.Location == objQMS015.Location && c.QualityProject == objQMS015.QualityProject).Select(x => x.IQualityProject).ToList());

            #region Check for QID Applicable For and Get data respective tables(QMS020 or QMS035)
            if (objQMS015.QualityIdApplicableFor == clsImplementationEnum.QualityIDApplicable.SEAM.GetStringValue())
            {
                #region old code
                /*
                lstQMS020 = db.QMS020.Where(c => c.QualityId == objQMS015.QualityId && c.BU == objQMS015.BU && c.Location == objQMS015.Location && lstQproj.Contains(c.QualityProject)).ToList();
                foreach (QMS020 item in lstQMS020)
                {
                    // get max offered stage sequence no.
                    int maxOfferedSequenceNo = mngr.GetMaxOfferedSequenceNo(item.QualityProject, item.BU, item.Location, item.SeamNo);
                    bool isUpdated = false;
                    foreach (var qIDStage in lstQMS16)
                    {
                        bool isStageLinkedInLTFPS = (new QualityIdController()).isStageLinkedInLTFPS(qIDStage.LineId);
                        if (!isStageLinkedInLTFPS && maxOfferedSequenceNo < qIDStage.StageSequance)
                        {
                            if (item.QMS021.Any(c => c.StageCode == qIDStage.StageCode && c.StageSequance == qIDStage.StageSequance))
                            {
                                // check for deleted stage
                                if (qIDStage.StageStatus == clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue())
                                {
                                    #region delete stage from test plan
                                    QMS021 deletedStageQMS021 = db.QMS021.Where(c => c.HeaderId == item.HeaderId && c.StageCode == qIDStage.StageCode && c.StageSequance == qIDStage.StageSequance).FirstOrDefault();
                                    if (deletedStageQMS021 != null)
                                    {
                                        deletedStageQMS021.StageStatus = qIDStage.StageStatus;
                                        deletedStageQMS021.EditedBy = qIDStage.EditedBy;
                                        deletedStageQMS021.EditedOn = qIDStage.EditedOn;
                                        db.SaveChanges();
                                        isUpdated = true;
                                    }
                                    #endregion
                                }
                                else
                                {
                                    #region update stage in test plan
                                    QMS021 modifiedStageQMS021 = db.QMS021.Where(c => c.HeaderId == item.HeaderId && c.StageCode == qIDStage.StageCode && c.StageSequance == qIDStage.StageSequance).FirstOrDefault();
                                    if (modifiedStageQMS021 != null)
                                    {
                                        if (qIDStage.StageStatus == clsImplementationEnum.QualityIdStageStatus.MODIFIED.GetStringValue())
                                        {
                                            modifiedStageQMS021.StageSequance = qIDStage.StageSequance;
                                            modifiedStageQMS021.StageStatus = qIDStage.StageStatus;
                                            modifiedStageQMS021.AcceptanceStandard = qIDStage.AcceptanceStandard;
                                            modifiedStageQMS021.ApplicableSpecification = qIDStage.ApplicableSpecification;
                                            modifiedStageQMS021.InspectionExtent = qIDStage.InspectionExtent;
                                            modifiedStageQMS021.Remarks = qIDStage.Remarks;
                                            modifiedStageQMS021.HardnessRequiredValue = qIDStage.HardnessRequiredValue;
                                            modifiedStageQMS021.HardnessUnit = qIDStage.HardnessUnit;
                                            modifiedStageQMS021.FerriteMaxValue = qIDStage.FerriteMaxValue;
                                            modifiedStageQMS021.FerriteMinValue = qIDStage.FerriteMinValue;
                                            modifiedStageQMS021.FerriteUnit = qIDStage.FerriteUnit;
                                            modifiedStageQMS021.EditedBy = qIDStage.EditedBy;
                                            modifiedStageQMS021.EditedOn = qIDStage.EditedOn;

                                            #region Chemical/PMI
                                            string chemical = clsImplementationEnum.SeamStageType.Chemical.GetStringValue();
                                            string PMI = clsImplementationEnum.SeamStageType.PMI.GetStringValue();

                                            if (Manager.FetchStageType(qIDStage.StageCode.ToLower(), qIDStage.BU, qIDStage.Location) == chemical || Manager.FetchStageType(qIDStage.StageCode.ToLower(), qIDStage.BU, qIDStage.Location) == PMI)
                                            {

                                                    #region Chem/Hardness/PMi/Ferrite                    
                                                    List<QMS023> lstDesQMS023 = db.QMS023.Where(x => x.TPLineId == modifiedStageQMS021.LineId).ToList();
                                                    if (lstDesQMS023 != null && lstDesQMS023.Count > 0)
                                                    {
                                                        db.QMS023.RemoveRange(lstDesQMS023);
                                                    }

                                                    List<QMS022> lstDesQMS022 = db.QMS022.Where(x => x.TPLineId == modifiedStageQMS021.LineId).ToList();
                                                    if (lstDesQMS022 != null && lstDesQMS022.Count > 0)
                                                    {
                                                        db.QMS022.RemoveRange(lstDesQMS022);
                                                    }
                                                    db.SaveChanges();
                                                    #endregion

                                                    List<QMS018> lstSamplingplan = db.QMS018.Where(x => x.QIDLineId == qIDStage.LineId && x.QualityIdRev == objQMS015.RevNo).ToList();
                                                    List<QMS022> lstQMS022 = new List<QMS022>();
                                                    foreach (var x in lstSamplingplan)
                                                    {
                                                        QMS022 objQMS022 = new QMS022();
                                                        objQMS022.TPLineId = modifiedStageQMS021.LineId;
                                                        objQMS022.Project = x.Project;
                                                        objQMS022.QualityProject = x.QualityProject;
                                                        objQMS022.BU = x.BU;
                                                        objQMS022.Location = x.Location;
                                                        objQMS022.SeamNo = modifiedStageQMS021.SeamNo;
                                                        objQMS022.TPRevNo = modifiedStageQMS021.QMS020.RevNo;
                                                        objQMS022.StageCode = x.StageCode;
                                                        objQMS022.StageSequence = x.StageSequence;
                                                        objQMS022.Depth = x.Depth;
                                                        objQMS022.Component = x.Component;
                                                        objQMS022.NoOfComponents = x.NoOfComponents;
                                                        objQMS022.Procedure = x.Procedure;
                                                        objQMS022.NoOfProcedures = x.NoOfProcedures;
                                                        objQMS022.Welder = x.Welder;
                                                        objQMS022.NoOfWelders = x.NoOfWelders;
                                                        objQMS022.Position = x.Position;
                                                        objQMS022.NoOfPositions = x.NoOfPositions;
                                                        objQMS022.CreatedBy = objClsLoginInfo.UserName;
                                                        objQMS022.CreatedOn = DateTime.Now;
                                                        lstQMS022.Add(objQMS022);
                                                    }
                                                    db.QMS022.AddRange(lstQMS022);
                                                    db.SaveChanges();

                                                    List<QMS023> lstQMS023 = new List<QMS023>();
                                                    List<QMS019> lstElement = db.QMS019.Where(x => x.QIDLineId == qIDStage.LineId && x.QualityIdRev == objQMS015.RevNo).ToList();
                                                    foreach (var x in lstElement)
                                                    {
                                                        QMS023 objQMS023 = new QMS023();
                                                        objQMS023.TPLineId = modifiedStageQMS021.LineId;
                                                        objQMS023.Project = modifiedStageQMS021.Project;
                                                        objQMS023.QualityProject = modifiedStageQMS021.QualityProject;
                                                        objQMS023.BU = modifiedStageQMS021.BU;
                                                        objQMS023.Location = modifiedStageQMS021.Location;
                                                        objQMS023.SeamNo = modifiedStageQMS021.SeamNo;
                                                        objQMS023.TPRevNo = modifiedStageQMS021.QMS020.RevNo;
                                                        objQMS023.StageCode = x.StageCode;
                                                        objQMS023.StageSequence = x.StageSequence;
                                                        objQMS023.HeaderId = db.QMS022.Where(a => a.TPLineId == objQMS023.TPLineId && a.StageCode == x.StageCode && a.StageSequence == x.StageSequence).Select(a => a.HeaderId).FirstOrDefault();
                                                        objQMS023.Element = x.Element;
                                                        objQMS023.ReqMinValue = x.ReqMinValue;
                                                        objQMS023.ReqMaxValue = x.ReqMaxValue;
                                                        objQMS023.CreatedBy = objClsLoginInfo.UserName;
                                                        objQMS023.CreatedOn = DateTime.Now;
                                                        lstQMS023.Add(objQMS023);
                                                    }
                                                    db.QMS023.AddRange(lstQMS023);
                                                    db.SaveChanges();
                                                 
                                            }
                                            #endregion
                                            db.SaveChanges();
                                            isUpdated = true;
                                        }
                                    }
                                    #endregion
                                }
                            }
                            else
                            {
                                #region add new stage in test plan
                                QMS021 newQMS021Stage = new QMS021();
                                newQMS021Stage.HeaderId = item.HeaderId;
                                newQMS021Stage.QualityProject = item.QualityProject;
                                newQMS021Stage.Project = item.Project;
                                newQMS021Stage.BU = item.BU;
                                newQMS021Stage.Location = item.Location;
                                newQMS021Stage.SeamNo = item.SeamNo;
                                newQMS021Stage.RevNo = item.RevNo;
                                newQMS021Stage.StageCode = qIDStage.StageCode;
                                newQMS021Stage.StageSequance = qIDStage.StageSequance;
                                newQMS021Stage.StageStatus = clsImplementationEnum.QualityIdStageStatus.ADDED.GetStringValue();
                                newQMS021Stage.AcceptanceStandard = qIDStage.AcceptanceStandard;
                                newQMS021Stage.ApplicableSpecification = qIDStage.ApplicableSpecification;
                                newQMS021Stage.InspectionExtent = qIDStage.InspectionExtent;
                                newQMS021Stage.Remarks = qIDStage.Remarks;
                                newQMS021Stage.HardnessRequiredValue = qIDStage.HardnessRequiredValue;
                                newQMS021Stage.HardnessUnit = qIDStage.HardnessUnit;
                                newQMS021Stage.FerriteMaxValue = qIDStage.FerriteMaxValue;
                                newQMS021Stage.FerriteMinValue = qIDStage.FerriteMinValue;
                                newQMS021Stage.FerriteUnit = qIDStage.FerriteUnit;
                                newQMS021Stage.CreatedBy = qIDStage.CreatedBy;
                                newQMS021Stage.CreatedOn = qIDStage.CreatedOn;
                                db.QMS021.Add(newQMS021Stage);
                                db.SaveChanges();
                                #region Chemical/PMI
                                string chemical = clsImplementationEnum.SeamStageType.Chemical.GetStringValue();
                                string PMI = clsImplementationEnum.SeamStageType.PMI.GetStringValue();

                                if (Manager.FetchStageType(qIDStage.StageCode.ToLower(), qIDStage.BU, qIDStage.Location) == chemical || Manager.FetchStageType(qIDStage.StageCode.ToLower(), qIDStage.BU, qIDStage.Location) == PMI)
                                {

                                    #region Chem/Hardness/PMi/Ferrite                    
                                    List<QMS023> lstDesQMS023 = db.QMS023.Where(x => x.TPLineId == newQMS021Stage.LineId).ToList();
                                    if (lstDesQMS023 != null && lstDesQMS023.Count > 0)
                                    {
                                        db.QMS023.RemoveRange(lstDesQMS023);
                                    }

                                    List<QMS022> lstDesQMS022 = db.QMS022.Where(x => x.TPLineId == newQMS021Stage.LineId).ToList();
                                    if (lstDesQMS022 != null && lstDesQMS022.Count > 0)
                                    {
                                        db.QMS022.RemoveRange(lstDesQMS022);
                                    }
                                    db.SaveChanges();
                                    #endregion

                                    List<QMS018> lstSamplingplan = db.QMS018.Where(x => x.QIDLineId == qIDStage.LineId && x.QualityIdRev == objQMS015.RevNo).ToList();
                                    List<QMS022> lstQMS022 = new List<QMS022>();
                                    foreach (var x in lstSamplingplan)
                                    {
                                        QMS022 objQMS022 = new QMS022();
                                        objQMS022.TPLineId = newQMS021Stage.LineId;
                                        objQMS022.Project = newQMS021Stage.Project;
                                        objQMS022.QualityProject = newQMS021Stage.QualityProject;
                                        objQMS022.BU = newQMS021Stage.BU;
                                        objQMS022.Location = newQMS021Stage.Location;
                                        objQMS022.SeamNo = newQMS021Stage.SeamNo;
                                        objQMS022.TPRevNo = newQMS021Stage.QMS020.RevNo;
                                        objQMS022.StageCode = x.StageCode;
                                        objQMS022.StageSequence = x.StageSequence;
                                        objQMS022.Depth = x.Depth;
                                        objQMS022.Component = x.Component;
                                        objQMS022.NoOfComponents = x.NoOfComponents;
                                        objQMS022.Procedure = x.Procedure;
                                        objQMS022.NoOfProcedures = x.NoOfProcedures;
                                        objQMS022.Welder = x.Welder;
                                        objQMS022.NoOfWelders = x.NoOfWelders;
                                        objQMS022.Position = x.Position;
                                        objQMS022.NoOfPositions = x.NoOfPositions;
                                        objQMS022.CreatedBy = objClsLoginInfo.UserName;
                                        objQMS022.CreatedOn = DateTime.Now;
                                        lstQMS022.Add(objQMS022);
                                    }
                                    db.QMS022.AddRange(lstQMS022);
                                    db.SaveChanges();

                                    List<QMS023> lstQMS023 = new List<QMS023>();
                                    List<QMS019> lstElement = db.QMS019.Where(x => x.QIDLineId == qIDStage.LineId && x.QualityIdRev == objQMS015.RevNo).ToList();
                                    foreach (var x in lstElement)
                                    {
                                        QMS023 objQMS023 = new QMS023();
                                        objQMS023.TPLineId = newQMS021Stage.LineId;
                                        objQMS023.Project = x.Project;
                                        objQMS023.QualityProject = x.QualityProject;
                                        objQMS023.BU = x.BU;
                                        objQMS023.Location = x.Location;
                                        objQMS023.SeamNo = newQMS021Stage.SeamNo;
                                        objQMS023.TPRevNo = newQMS021Stage.QMS020.RevNo;
                                        objQMS023.StageCode = x.StageCode;
                                        objQMS023.StageSequence = x.StageSequence;
                                        objQMS023.HeaderId = db.QMS022.Where(a => a.TPLineId == objQMS023.TPLineId && a.StageCode == x.StageCode && a.StageSequence == x.StageSequence).Select(a => a.HeaderId).FirstOrDefault();
                                        objQMS023.Element = x.Element;
                                        objQMS023.ReqMinValue = x.ReqMinValue;
                                        objQMS023.ReqMaxValue = x.ReqMaxValue;
                                        objQMS023.CreatedBy = objClsLoginInfo.UserName;
                                        objQMS023.CreatedOn = DateTime.Now;
                                        lstQMS023.Add(objQMS023);
                                    }
                                    db.QMS023.AddRange(lstQMS023);
                                    db.SaveChanges();

                                }
                                #endregion
                              
                                isUpdated = true;
                                #endregion
                            }

                        }
                    }

                    if (isUpdated)
                    {
                        // update Test Plan
                        if (item.Status == clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue())
                        {
                            item.Status = clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue();
                            item.RevNo = item.RevNo + 1;
                        }
                        item.QualityIdRev = objQMS015.RevNo;
                        item.EditedBy = objClsLoginInfo.UserName;
                        item.EditedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                }
                    */
                #endregion

                #region  Update On Test Plan (optimized)
                int? count = db.SP_IPI_APPROVE_QUALITYID_FOR_SEAM(string.Join(",", lstQproj.ToList()), objQMS015.Project, objQMS015.BU, objQMS015.Location, objQMS015.QualityId, objClsLoginInfo.UserName).FirstOrDefault();
                #endregion
            }
            else
            {
                #region Update On Part ICL

                lstQMS035 = db.QMS035.Where(c => c.QualityId == objQMS015.QualityId && c.BU == objQMS015.BU && c.Location == objQMS015.Location && lstQproj.Contains(c.QualityProject)).ToList();
                foreach (QMS035 item in lstQMS035)
                {
                    // get max offered stage sequence no.
                    int maxOfferedSequenceNo = mngr.GetMaxOfferedSequenceNo_PART(item.QualityProject, item.BU, item.Location, item.PartNo, item.ParentPartNo);
                    bool isUpdated = false;
                    foreach (var qIDStage in lstQMS16)
                    {
                        if (maxOfferedSequenceNo < qIDStage.StageSequance)
                        {
                            if (item.QMS036.Any(c => c.StageCode == qIDStage.StageCode && c.StageSequence == qIDStage.StageSequance))
                            {
                                // check for deleted stage
                                if (qIDStage.StageStatus == clsImplementationEnum.ICLPartStatus.Deleted.GetStringValue())
                                {
                                    #region delete stage from part ICL
                                    QMS036 deletedStageQMS036 = db.QMS036.Where(c => c.HeaderId == item.HeaderId && c.StageCode == qIDStage.StageCode && c.StageSequence == qIDStage.StageSequance).FirstOrDefault();
                                    if (deletedStageQMS036 != null)
                                    {
                                        deletedStageQMS036.StageStatus = qIDStage.StageStatus;
                                        deletedStageQMS036.EditedBy = qIDStage.EditedBy;
                                        deletedStageQMS036.EditedOn = qIDStage.EditedOn;
                                        db.SaveChanges();
                                        isUpdated = true;
                                    }
                                    #endregion
                                }
                                else
                                {
                                    #region update stage in part ICL
                                    QMS036 modifiedStageQMS036 = db.QMS036.Where(c => c.HeaderId == item.HeaderId && c.StageCode == qIDStage.StageCode && c.StageSequence == qIDStage.StageSequance).FirstOrDefault();
                                    if (modifiedStageQMS036 != null)
                                    {
                                        modifiedStageQMS036.StageSequence = qIDStage.StageSequance;
                                        modifiedStageQMS036.StageStatus = qIDStage.StageStatus;
                                        modifiedStageQMS036.AcceptanceStandard = qIDStage.AcceptanceStandard;
                                        modifiedStageQMS036.ApplicableSpecification = qIDStage.ApplicableSpecification;
                                        modifiedStageQMS036.InspectionExtent = qIDStage.InspectionExtent;
                                        modifiedStageQMS036.Remarks = qIDStage.Remarks;
                                        modifiedStageQMS036.EditedBy = qIDStage.EditedBy;
                                        modifiedStageQMS036.EditedOn = qIDStage.EditedOn;
                                        db.SaveChanges();
                                        isUpdated = true;
                                    }
                                    #endregion
                                }
                            }
                            else
                            {
                                #region add new stage in part ICL
                                QMS036 newQMS036Stage = new QMS036();
                                newQMS036Stage.HeaderId = item.HeaderId;
                                newQMS036Stage.QualityProject = item.QualityProject;
                                newQMS036Stage.Project = item.Project;
                                newQMS036Stage.BU = item.BU;
                                newQMS036Stage.Location = item.Location;
                                newQMS036Stage.PartNo = item.PartNo;
                                newQMS036Stage.ParentPartNo = item.ParentPartNo;
                                newQMS036Stage.ChildPartNo = item.ChildPartNo;
                                newQMS036Stage.StageCode = qIDStage.StageCode;
                                newQMS036Stage.StageSequence = qIDStage.StageSequance;
                                newQMS036Stage.StageStatus = clsImplementationEnum.ICLPartStatus.Added.GetStringValue();
                                newQMS036Stage.AcceptanceStandard = qIDStage.AcceptanceStandard;
                                newQMS036Stage.ApplicableSpecification = qIDStage.ApplicableSpecification;
                                newQMS036Stage.InspectionExtent = qIDStage.InspectionExtent;
                                newQMS036Stage.Remarks = qIDStage.Remarks;
                                newQMS036Stage.CreatedBy = qIDStage.CreatedBy;
                                newQMS036Stage.CreatedOn = qIDStage.CreatedOn;
                                db.QMS036.Add(newQMS036Stage);
                                db.SaveChanges();
                                isUpdated = true;
                                #endregion
                            }
                        }
                    }
                    if (isUpdated)
                    {
                        // update Part ICL Header
                        item.Status = clsImplementationEnum.ICLPartStatus.Draft.GetStringValue();
                        item.RevNo = item.RevNo + 1;
                        item.QualityIdRevNo = objQMS015.RevNo;
                        item.EditedBy = objClsLoginInfo.UserName;
                        item.EditedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                }

                #endregion
            }




            #endregion

        }

        public clsHelper.ResponseMsg InsUpdOnTestPlan(string qualityId, List<QMS016> lstQMS16)
        {
            string currentUser = objClsLoginInfo.UserName;

            var currentLoc = (from a in db.COM003
                              join b in db.COM002 on a.t_loca equals b.t_dimx
                              where b.t_dtyp == 1 && a.t_actv == 1
                              && a.t_psno.Equals(currentUser, StringComparison.OrdinalIgnoreCase)
                              select b.t_dimx).FirstOrDefault().ToString();

            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            QMS020 objQMS020 = new QMS020();
            QMS021 objQMS021 = new QMS021();

            if (!string.IsNullOrEmpty(qualityId))
            {
                objQMS020 = db.QMS020.Where(i => i.QualityId.Equals(qualityId, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                //var lstQMS021 = db.QMS021.Where(i => i.HeaderId == objQMS020.HeaderId).ToList();
                if (objQMS020 != null)
                {
                    foreach (var item in lstQMS16)
                    {
                        objQMS021 = db.QMS021.Where(i => i.HeaderId == objQMS020.HeaderId && i.StageCode.Equals(item.StageCode, StringComparison.OrdinalIgnoreCase) && i.StageSequance == item.StageSequance).FirstOrDefault();
                        if (objQMS021 != null)
                        {
                            objQMS021.StageStatus = item.StageStatus;
                            if (string.Equals(objQMS020.Status, clsImplementationEnum.TestPlanStatus.Approved.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                            {
                                objQMS020.Status = clsImplementationEnum.TestPlanStatus.Draft.GetStringValue();
                                objQMS020.RevNo += 1;
                            }
                            db.SaveChanges();
                        }
                        else
                        {
                            objQMS021 = new QMS021();
                            objQMS021.HeaderId = objQMS021.HeaderId;
                            objQMS021.QualityProject = objQMS021.QualityProject;
                            objQMS021.Project = objQMS021.Project;
                            objQMS021.BU = objQMS021.BU;
                            objQMS021.Location = currentLoc;
                            objQMS021.SeamNo = objQMS021.SeamNo;
                            objQMS021.StageCode = item.StageCode;
                            objQMS021.StageSequance = item.StageSequance;
                            objQMS021.StageStatus = item.StageStatus;
                            objQMS021.AcceptanceStandard = item.AcceptanceStandard;
                            objQMS021.ApplicableSpecification = item.ApplicableSpecification;
                            objQMS021.InspectionExtent = item.InspectionExtent;
                            objQMS021.Remarks = item.Remarks;
                            objQMS021.CreatedBy = objClsLoginInfo.UserName;
                            objQMS021.CreatedOn = DateTime.Now;

                            db.QMS021.Add(objQMS021);
                            db.SaveChanges();
                            if (string.Equals(objQMS020.Status, clsImplementationEnum.TestPlanStatus.Approved.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                            {
                                objQMS020.Status = clsImplementationEnum.TestPlanStatus.Draft.GetStringValue();
                                objQMS020.RevNo += 1;
                            }
                        }
                    }
                }
            }

            return objResponseMsg;
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    List<SP_IPI_GET_QUALITYPROJECT_RESULT_Result> lst = db.SP_IPI_GET_QUALITYPROJECT_RESULT(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    List<SP_IPI_GetQualityIDHeader_Result> lst = db.SP_IPI_GetQualityIDHeader(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      QualityIdApplicableFor = li.QualityIdApplicableFor,
                                      QualityId = li.QualityId,
                                      QualityIdDesc = li.QualityIdDesc,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      RevNo = li.RevNo,
                                      Status = li.Status
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);

                }
                else
                {
                    var lst = db.SP_IPI_GetQualityIdLines(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      QualityId = li.QualityId,
                                      RevNo = li.RevNo,
                                      Status = li.Status,
                                      StageCode = li.StageCode,
                                      StageSequance = li.StageSequance,
                                      StageStatus = li.StageStatus,
                                      AcceptanceStandard = li.AcceptanceStandard,
                                      ApplicableSpecification = li.ApplicableSpecification,
                                      InspectionExtent = li.InspectionExtent,
                                      Remarks = li.Remarks,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            if (LineId > 0)
            {
                QMS016 objQMS016 = db.QMS016.Where(x => x.LineId == LineId).FirstOrDefault();
                model.ApprovedBy = objQMS016.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objQMS016.ApprovedBy) : null;
                model.ApprovedOn = objQMS016.ApprovedOn;
                model.SubmittedBy = objQMS016.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objQMS016.SubmittedBy) : null;
                model.SubmittedOn = objQMS016.SubmittedOn;
                model.CreatedBy = objQMS016.CreatedBy != null ? Manager.GetUserNameFromPsNo(objQMS016.CreatedBy) : null;
                model.CreatedOn = objQMS016.CreatedOn;
                model.EditedBy = objQMS016.EditedBy != null ? Manager.GetUserNameFromPsNo(objQMS016.EditedBy) : null;
                model.EditedOn = objQMS016.EditedOn;
            }
            else
            {
                QMS015 objQMS015 = db.QMS015.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.ApprovedBy = objQMS015.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objQMS015.ApprovedBy) : null;
                model.ApprovedOn = objQMS015.ApprovedOn;
                model.SubmittedBy = objQMS015.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objQMS015.SubmittedBy) : null;
                model.SubmittedOn = objQMS015.SubmittedOn;
                model.CreatedBy = objQMS015.CreatedBy != null ? Manager.GetUserNameFromPsNo(objQMS015.CreatedBy) : null;
                model.CreatedOn = objQMS015.CreatedOn;
                model.EditedBy = objQMS015.EditedBy != null ? Manager.GetUserNameFromPsNo(objQMS015.EditedBy) : null;
                model.EditedOn = objQMS015.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress2.cshtml", model);
        }
        public ActionResult ShowLogTimeline(int id, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            if (LineId > 0)
            {

            }
            else
            {
                QMS015_Log objQMS015log = db.QMS015_Log.Where(x => x.Id == id).FirstOrDefault();
                model.ApprovedBy = objQMS015log.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objQMS015log.ApprovedBy) : null;
                model.ApprovedOn = objQMS015log.ApprovedOn;
                model.SubmittedBy = objQMS015log.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objQMS015log.SubmittedBy) : null;
                model.SubmittedOn = objQMS015log.SubmittedOn;
                model.CreatedBy = objQMS015log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objQMS015log.CreatedBy) : null;
                model.CreatedOn = objQMS015log.CreatedOn;
                model.EditedBy = objQMS015log.EditedBy != null ? Manager.GetUserNameFromPsNo(objQMS015log.EditedBy) : null;
                model.EditedOn = objQMS015log.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress2.cshtml", model);
        }
    }
}