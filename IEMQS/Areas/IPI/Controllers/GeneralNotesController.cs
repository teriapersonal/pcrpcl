﻿using IEMQS.Areas.Authorization.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.IPI.Controllers
{
    public class GeneralNotesController : clsBase
    {
        // GET: IPI/GeneralNotes
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }
        [SessionExpireFilter]
        public ActionResult GeneralDetail(int Id = 0)
        {
            SWP001 objSWP001 = new SWP001();
            var Loca = "";
            if (Id > 0)
            {
                objSWP001 = db.SWP001.Find(Id);
                Loca = objSWP001.Location;
            }
            else
            {
                Loca = objClsLoginInfo.Location;
            }

            var locaDescription = (from a in db.COM002
                                   where a.t_dimx == Loca
                                   select a.t_desc).FirstOrDefault();
            ViewBag.Location = Loca + " - " + locaDescription;

            return View(objSWP001);
        }
        public JsonResult getTaskManager(string HeaderId)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                int hd = Convert.ToInt32(HeaderId);
                List<clsTask> list = new List<clsTask>();
                list = (from a in db.SWP003
                        where a.HeaderId == hd
                        select new clsTask
                        {
                            id = a.IQualityProject,
                            text = a.IQualityProject
                        }).ToList();
                objResponseMsg.lsttask = list;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveHeader(SWP001 swp001, FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                SWP001 objswp001 = new SWP001();
                string QP = fc["txtQualityProject"].ToString().Split('-')[0];
                string loca = fc["txtLoca"].Split('-')[0].Trim().ToString();
                var Exists = db.SWP001.Any(x => x.QualityProject == QP && x.Location == loca);
                if (Exists == false)
                {
                    objswp001.QualityProject = fc["txtQualityProject"].ToString().Split('-')[0].Trim();
                    objswp001.Project = fc["txtProject"].ToString().Split('-')[0].Trim();
                    objswp001.Location = fc["txtLoca"].ToString().Split('-')[0].Trim();
                    objswp001.BU = fc["txtBU"].ToString().Split('-')[0].Trim();
                    objswp001.CreatedBy = objClsLoginInfo.UserName;
                    // objswp001.ApprovedBy = fc["ddlApprover"].ToString().Split('-')[0];
                    objswp001.CreatedOn = DateTime.Now;
                    db.SWP001.Add(objswp001);
                    db.SaveChanges();

                    if (!String.IsNullOrWhiteSpace(fc["IdenticalProject"]))
                    {
                        string[] IQP = (fc["IdenticalProject"] + "").Split(',');
                        if (IQP.Length > 0)
                        {
                            for (int i = 0; i < IQP.Length; i++)
                            {
                                SWP003 objswp003 = new SWP003();
                                objswp003.HeaderId = objswp001.HeaderId;
                                objswp003.IQualityProject = IQP[i];
                                objswp003.QualityProject = fc["txtQualityProject"].ToString().Split('-')[0].Trim();
                                objswp003.Project = fc["txtProject"].ToString().Split('-')[0].Trim();
                                objswp003.Location = fc["txtLoca"].ToString().Split('-')[0].Trim();
                                objswp003.BU = fc["txtBU"].ToString().Split('-')[0].Trim();
                                objswp003.CreatedBy = objClsLoginInfo.UserName;
                                objswp003.CreatedOn = DateTime.Now;
                                db.SWP003.Add(objswp003);
                                db.SaveChanges();
                            }
                        }
                    }
                    objResponseMsg.HeaderId = objswp001.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CTQMessages.Insert.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Record already Exists";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetQualityProjectDetails(string term)
        {
            string username = objClsLoginInfo.UserName;
            List<Projects> lstQualityProject = new List<Projects>();
            lstQualityProject =  db.QMS010.Where(i => i.Location == objClsLoginInfo.Location && i.QualityProject.ToLower().Contains(term.ToLower())).Select(i => new Projects { Value = i.QualityProject, projectCode = i.QualityProject }).Distinct().ToList();//&& i.CreatedBy.Equals(username,StringComparison.OrdinalIgnoreCase)
            
            return Json(lstQualityProject, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public ActionResult SaveLine(SWP002 swp002, FormCollection fc)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                int HeaderId = 0;
                string noteno = fc["hfNote"].ToString();
                string QualityProject = fc["hfQP"].ToString().Split('-')[0].Trim();
                if (!string.IsNullOrEmpty(QualityProject))
                {
                    var dt = db.SWP001.FirstOrDefault(a => a.QualityProject == QualityProject);
                    if(dt != null)
                    {
                        HeaderId = dt.HeaderId;
                    }
                    
                }
                if (fc["hfNote"].ToString() == "")
                {
                    noteno = fc["txtNote"].ToString();
                }
                if (noteno != "")
                {
                    if (!string.IsNullOrWhiteSpace(fc["hfLineid"].ToString()) && Convert.ToInt32(fc["hfLineid"].ToString()) > 0)
                    {
                        int hd = Convert.ToInt32(fc["hfLineid"].ToString());
                        SWP002 objswp002 = db.SWP002.Where(x => x.LineId == hd).FirstOrDefault();
                        objswp002.NoteDescription = fc["txtNoteDesc"].ToString();
                        objswp002.EditedBy = objClsLoginInfo.UserName;
                        objswp002.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CTQMessages.Update.ToString();
                    }
                    else
                    {
                        SWP002 objswp002 = new SWP002();
                        //objswp002.HeaderId = Convert.ToInt32(fc["hfHeaderID"].ToString());
                        objswp002.HeaderId = HeaderId;
                        objswp002.QualityProject = fc["hfQP"].ToString().Split('-')[0].Trim();
                        objswp002.Project = fc["hfProject"].ToString().Split('-')[0].Trim();
                        objswp002.Location = fc["hfLoca"].ToString().Split('-')[0].Trim();
                        objswp002.BU = fc["hfBU"].ToString().Split('-')[0].Trim();
                        objswp002.NoteNumber = Convert.ToInt32(fc["txtNote"].ToString());
                        objswp002.NoteDescription = fc["txtNoteDesc"].ToString();
                        objswp002.CreatedBy = objClsLoginInfo.UserName;
                        objswp002.CreatedOn = DateTime.Now;
                        db.SWP002.Add(objswp002);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CTQMessages.Insert.ToString();
                    }
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Please Enter Note Number";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetIdenticalProjects(string search, string param)
        {
            List<ddlValue> identicalProject = db.QMS010.Where(i => !i.QualityProject.Equals(param, StringComparison.OrdinalIgnoreCase) && i.QualityProject.ToLower().Contains(search.ToLower())).Select(i => new ddlValue { id = i.QualityProject, text = i.QualityProject }).ToList();
            return Json(identicalProject, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteLine(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                SWP002 objSWP002 = db.SWP002.Where(x => x.LineId == Id).FirstOrDefault();
                db.SWP002.Remove(objSWP002);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDetails(string qms, string loc)
        {
            if (!string.IsNullOrWhiteSpace(qms))
            {
                NDE010 objNDE010 = new NDE010();
                BUWiseDropdown objProjectDataModel = new BUWiseDropdown();

                var project = db.QMS010.Where(i => i.QualityProject.Equals(qms, StringComparison.OrdinalIgnoreCase)).Select(i => i.Project).FirstOrDefault();
                objProjectDataModel.Project = db.COM001.Where(i => i.t_cprj.Equals(project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + "-" + i.t_dsca).FirstOrDefault();// project;

                string BU = db.COM001.Where(i => i.t_cprj == project).FirstOrDefault().t_entu;
                var BUDescription = (from a in db.COM002
                                     where a.t_dtyp == 2 && a.t_dimx == BU
                                     select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
                objProjectDataModel.BUDescription = BUDescription.BUDesc;

                return Json(objProjectDataModel, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult getHeaderId(string QP, string project, string loca, string BU)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                project = project.Split('-')[0].Trim().ToString();
                loca = loca.Split('-')[0].Trim().ToString();
                BU = BU.Split('-')[0].Trim().ToString();
                int Headerid = (from a in db.SWP001
                                where a.Project == project || a.QualityProject == QP || a.Location == loca || a.BU == BU
                                select a.HeaderId).FirstOrDefault();
                objResponseMsg.Key = true;
                objResponseMsg.Value = Headerid.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetInsertView(string project, string QP, string loca, string BU, string headerid, string id)
        {
            ViewBag.project = project;
            ViewBag.QP = QP;
            ViewBag.BU = BU;
            ViewBag.loca = loca;
            int headerId = 0;
            if (!string.IsNullOrWhiteSpace(QP))
            {
                var dt = db.SWP001.FirstOrDefault(a => a.QualityProject == QP);
                if(dt != null)
                {
                    ViewBag.Headerid = dt.HeaderId;
                }
            }
            else
            {
                ViewBag.Headerid = headerId;
            }

            
            int hd = Convert.ToInt32(headerid);
            SWP002 objSWP002 = new SWP002();
            int lineid = 0;
            if (id != "")
            {
                lineid = Convert.ToInt32(id);
            }
            else
            {
                lineid = 0;
            }
            if (lineid > 0)
            {
                objSWP002 = db.SWP002.Where(x => x.LineId == lineid).FirstOrDefault();
            }
            return PartialView("_notesDetail", objSWP002);
        }
        [HttpPost]
        public JsonResult LoadHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;
                string strWhere = string.Empty;
                int headerid = Convert.ToInt32(param.Headerid);

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = "  (QualityProject like '%" + param.sSearch
                         + "%' or Project like '%" + param.sSearch
                         + "%' or BU like '%" + param.sSearch
                         + "%' or Location like '%" + param.sSearch
                         + "%' or CreatedBy +' - '+ b.t_name like '%" + param.sSearch
                         + "%' or CreatedOn like '%" + param.sSearch
                         + "%')";
                }
                else
                {
                    strWhere = " 1=1";
                }
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_SWP_GET_HEADERDETAILS
                                (
                                    StartIndex, EndIndex, strSortOrder, headerid, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.QualityProject),
                                Convert.ToString(uc.BU),
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Location),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn),
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #region Notes Line Grid Data

        [HttpPost]
        public ActionResult GetNotesLineIdGridDataPartial(string QualityProject, string Location, string BU)
        {
            ViewBag.qualityProject = QualityProject;
            ViewBag.location = Location;
            ViewBag.bu = BU;
            var dt = db.SWP001.FirstOrDefault(a => a.QualityProject == QualityProject);
            if(dt != null)
            {
                ViewBag.HeaderId = dt.HeaderId;
            }
            else
            {
                ViewBag.HeaderId = 0;
            }
            return PartialView("_GetNotesLineIdGridDataPartial");
        }

        public ActionResult CheckQualityProjectDataExistOrNot(string QualityProject)
        {
            try
            {
                string status = "";
                int HeaderId = 0;
                var dt = db.SWP001.FirstOrDefault(a => a.QualityProject == QualityProject);
                if (dt != null)
                {
                    HeaderId = dt.HeaderId;
                    status = "Exist";
                }
                return Json(new { Status = status, Data = HeaderId}, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        [HttpPost]
        public JsonResult LoadLineData(JQueryDataTableParamModel param,string QualityProject)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;
                int headerid = Convert.ToInt32(param.Headerid);
                string strWhere = string.Empty;

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (NoteNumber like '%" + param.sSearch
                         + "%' or NoteDescription like '%" + param.sSearch
                         + "%')";
                }
                else
                {
                    strWhere = " 1=1";
                }

                //if (!string.IsNullOrWhiteSpace(QualityProject))
                //{
                //    strWhere += " QualityProject='" + QualityProject + "'";
                //}
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
               //var lstResult = db.Database.SqlQuery<SWP002>("select * from SWP002 where QualityProject = '" + QualityProject + "'").ToList();
               var lstResult = db.SP_SWP_GET_LINEDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, headerid, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.NoteNumber),
                               Convert.ToString(uc.NoteDescription),
                               Convert.ToString(uc.LineId),
                               Convert.ToString(uc.LineId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult getCodeValue(string project, string approver, string BU)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                objResponseMsg.projdesc = db.COM001.Where(i => i.t_cprj == project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objResponseMsg.appdesc = db.COM003.Where(i => i.t_psno == approver && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
                objResponseMsg.budesc = db.COM002.Where(i => i.t_dimx == BU).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetCopyView(string QP)
        {
            QMS010 objQMS010 = new QMS010();
            ViewBag.QP = QP;
            return PartialView("_copyView", objQMS010);
        }

        [HttpPost]
        public ActionResult CopyGeneralNotes(string SourceQualityProject, string FromQualityProject, string ToQualityProject)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            var IsAllRecordExist = true;
            try
            {
                List<QMS010> allQualityProject = db.QMS010.Where(i=>i.Location==objClsLoginInfo.Location).ToList();

                List<string> newQualityProjectList = new List<string>();
                bool start = false;

                foreach (QMS010 QP in allQualityProject)
                {
                    if (QP.QualityProject == FromQualityProject)
                    {
                        start = true;
                    }

                    if (start)
                        newQualityProjectList.Add(QP.QualityProject);

                    if (QP.QualityProject == ToQualityProject)
                    {
                        break;
                    }
                }

                var sourceSWP002List = db.SWP002.Where(i => i.QualityProject == SourceQualityProject).ToList();
                foreach (var QP in newQualityProjectList)
                {
                    if (!db.SWP002.Any(i => i.QualityProject == QP) && !db.SWP003.Any(i => i.IQualityProject == QP))
                    {
                        IsAllRecordExist = false;
                        var copySWP002List = new List<SWP002>();
                        var objSWP001 = new SWP001();
                        if (!db.SWP001.Any(i => i.QualityProject == QP && i.Location == objClsLoginInfo.Location))
                        {
                            var objQMS010 = db.QMS010.Where(i => i.QualityProject == QP).First();

                            objSWP001.BU = objQMS010.BU;
                            objSWP001.CreatedBy = objClsLoginInfo.UserName;
                            objSWP001.CreatedOn = DateTime.Now;
                            objSWP001.Location = objClsLoginInfo.Location;
                            objSWP001.Project = objQMS010.Project;
                            objSWP001.QualityProject = QP;
                            db.SWP001.Add(objSWP001);
                        }
                        else
                        {
                            objSWP001 = db.SWP001.Where(i => i.QualityProject == QP && i.Location == objClsLoginInfo.Location).FirstOrDefault();
                        }

                        foreach (var notes in sourceSWP002List)
                        {
                            copySWP002List.Add(new SWP002
                            {
                                HeaderId = objSWP001.HeaderId,
                                QualityProject = QP,
                                Project = objSWP001.Project,
                                BU = objSWP001.BU,
                                Location = objSWP001.Location,
                                NoteNumber = notes.NoteNumber,
                                NoteDescription = notes.NoteDescription,
                                CreatedBy = objClsLoginInfo.UserName,
                                CreatedOn = DateTime.Now,
                            });
                        }
                        objSWP001.SWP002 = copySWP002List;
                        db.SaveChanges();
                    }
                }
                if (IsAllRecordExist)
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "No Quality Project Found For Copy!";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Copied Successfully!";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult CopyData(FormCollection fc)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                string Source = fc["txtQP"].ToString();
                string toQP = fc["txtTo"].ToString();
                string fromQP = fc["txtFrom"].ToString();

                int IDToQP = (from a in db.QMS010
                              where a.QualityProject == toQP
                              select a.Id).FirstOrDefault();

                int IDFromQP = (from a in db.QMS010
                                where a.QualityProject == fromQP
                                select a.Id).FirstOrDefault();

                List<Project> list = new List<Project>();
                list = (from a in db.QMS010
                        where a.Id >= IDFromQP && a.Id <= IDToQP
                        select new Project { projectCode = a.QualityProject }).ToList();

                bool alreadyExists = list.Any(s => s.projectCode == Source);
                if (alreadyExists == true)
                {
                    var firstMatch = list.First(s => s.projectCode == Source);
                    list.Remove(firstMatch);
                }

                foreach (var lst in list)
                {
                    int? noteNum = (from a in db.SWP002
                                    where a.QualityProject == Source
                                    select a.NoteNumber).FirstOrDefault();

                    string noteDec = (from a in db.SWP002
                                      where a.QualityProject == Source
                                      select a.NoteDescription).FirstOrDefault();

                    string qualityProject = lst.projectCode.ToString();
                    string Project = (from a in db.SWP001
                                      where a.QualityProject == Source
                                      select a.Project).FirstOrDefault();

                    string BU = (from a in db.SWP001
                                 where a.QualityProject == Source
                                 select a.BU).FirstOrDefault();

                    string Loca = (from a in db.SWP001
                                   where a.QualityProject == Source
                                   select a.Location).FirstOrDefault();
                    CopyDetails(qualityProject, Project, BU, Loca, noteNum, noteDec);

                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Copied Successfully...!";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public void CopyDetails(string qualityProject, string Project, string BU, string Loca, int? noteNum, string noteDec)
        {
            if (db.SWP001.Any(x => x.QualityProject == qualityProject))
            {
                if (db.SWP002.Any(x => x.QualityProject == qualityProject))
                {
                    if (!db.SWP002.Any(x => x.NoteNumber == noteNum))
                    {
                        int Headerid = (from a in db.SWP001
                                        where a.Project == Project && a.QualityProject == qualityProject
                                           && a.Location == Loca && a.BU == BU
                                        select a.HeaderId).FirstOrDefault();

                        SWP002 objswp002 = new SWP002();
                        objswp002.HeaderId = Headerid;
                        objswp002.QualityProject = qualityProject;
                        objswp002.Project = Project;
                        objswp002.Location = Loca;
                        objswp002.BU = BU;
                        objswp002.NoteNumber = noteNum;
                        objswp002.NoteDescription = noteDec;
                        objswp002.CreatedBy = objClsLoginInfo.UserName;
                        objswp002.CreatedOn = DateTime.Now;
                        db.SWP002.Add(objswp002);
                        db.SaveChanges();
                    }
                }
                else
                {
                    int Headerid = (from a in db.SWP001
                                    where a.Project == Project && a.QualityProject == qualityProject
                                       && a.Location == Loca && a.BU == BU
                                    select a.HeaderId).FirstOrDefault();

                    SWP002 objswp002 = new SWP002();
                    objswp002.HeaderId = Headerid;
                    objswp002.QualityProject = qualityProject;
                    objswp002.Project = Project;
                    objswp002.Location = Loca;
                    objswp002.BU = BU;
                    objswp002.NoteNumber = noteNum;
                    objswp002.NoteDescription = noteDec;
                    objswp002.CreatedBy = objClsLoginInfo.UserName;
                    objswp002.CreatedOn = DateTime.Now;
                    db.SWP002.Add(objswp002);
                    db.SaveChanges();
                }
            }
            else
            {
                if (db.SWP003.Any(x => x.IQualityProject == qualityProject))
                {
                    string QP = (from a in db.SWP003
                                 where a.IQualityProject == qualityProject
                                 select a.QualityProject).FirstOrDefault();

                    if (db.SWP002.Any(x => x.QualityProject == QP))
                    {
                        if (!db.SWP002.Any(x => x.NoteNumber == noteNum))
                        {
                            int Headerid = (from a in db.SWP001
                                            where a.Project == Project && a.QualityProject == QP
                                               && a.Location == Loca && a.BU == BU
                                            select a.HeaderId).FirstOrDefault();

                            SWP002 objswp002 = new SWP002();
                            objswp002.HeaderId = Headerid;
                            objswp002.QualityProject = QP;
                            objswp002.Project = Project;
                            objswp002.Location = Loca;
                            objswp002.BU = BU;
                            objswp002.NoteNumber = noteNum;
                            objswp002.NoteDescription = noteDec;
                            objswp002.CreatedBy = objClsLoginInfo.UserName;
                            objswp002.CreatedOn = DateTime.Now;
                            db.SWP002.Add(objswp002);
                            db.SaveChanges();
                        }
                    }
                }
                else
                {
                    SWP001 objswp001 = new SWP001();
                    objswp001.QualityProject = qualityProject;
                    objswp001.Project = Project;
                    objswp001.Location = Loca;
                    objswp001.BU = BU;
                    objswp001.CreatedBy = objClsLoginInfo.UserName;
                    objswp001.CreatedOn = DateTime.Now;
                    db.SWP001.Add(objswp001);
                    db.SaveChanges();

                    int Headerid = (from a in db.SWP001
                                    where a.Project == Project && a.QualityProject == qualityProject
                                       && a.Location == Loca && a.BU == BU
                                    select a.HeaderId).FirstOrDefault();

                    SWP002 objswp002 = new SWP002();
                    objswp002.HeaderId = Headerid;
                    objswp002.QualityProject = qualityProject;
                    objswp002.Project = Project;
                    objswp002.Location = Loca;
                    objswp002.BU = BU;
                    objswp002.NoteNumber = noteNum;
                    objswp002.NoteDescription = noteDec;
                    objswp002.CreatedBy = objClsLoginInfo.UserName;
                    objswp002.CreatedOn = DateTime.Now;
                    db.SWP002.Add(objswp002);
                    db.SaveChanges();
                }
            }
        }
        [HttpPost]
        public ActionResult CheckNoteNo(int HeaderId, int noteNo)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            if (db.SWP002.Where(x => x.HeaderId == HeaderId && x.NoteNumber == noteNo).Any())
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Note Number already exists";
            }
            else
                objResponseMsg.Key = true;
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetNextPrevQualityProject(string qualityProject, string option)
        {

            string username = objClsLoginInfo.UserName;
            var lstATHLocation = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.Location).Distinct().ToList();
            var lstCOMLocation = db.COM003.Where(i => i.t_psno.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_loca).Distinct().ToList();

            var lstCOMBU = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.BU).Distinct().ToList();

            lstCOMLocation = lstATHLocation.Union(lstCOMLocation).ToList();

            List<Projects> lstQualityProject = new List<Projects>();
            lstQualityProject = db.QMS010.Where(i => lstCOMLocation.Contains(i.Location)
                                                    && lstCOMBU.Contains(i.BU)
                                                    ).Select(i => new Projects { Value = i.QualityProject, projectCode = i.QualityProject }).Distinct().ToList();

            //List<Projects> lstQualityProject = new List<Projects>();
            //lstQualityProject = db.QMS020.Select(i => new Projects { Value = i.QualityProject, projectCode = i.QualityProject, Text = i.Location }).Distinct().OrderBy(x => x.Value).ThenBy(x => x.Text).ToList();

            ResponceMsgWithHeaderID objresponse = new ResponceMsgWithHeaderID();
            List<Projects> lstprojects = lstQualityProject;
            int projindexno = 0;
            int minproj = 0;
            int maxproj = 0;

            try
            {
                if (!string.IsNullOrEmpty(qualityProject))
                {
                    projindexno = lstprojects.Select((item, index) => new { index, item }).Where(x => x.item.projectCode.ToLower() == qualityProject.ToLower()).Select(x => x.index).FirstOrDefault();
                    minproj = lstprojects.Select((item, index) => new { index, item }).Min(x => x.index);
                    maxproj = lstprojects.Select((item, index) => new { index, item }).Max(x => x.index);

                    switch (option)
                    {
                        case "PrevProject":
                            ViewBag.Title = "PrevProject";
                            projindexno = (projindexno - 1);
                            qualityProject = lstprojects.Select((item, index) => new { index, item }).Where(x => x.index == projindexno).FirstOrDefault().item.projectCode;

                            break;
                        case "NextProject":
                            ViewBag.Title = "NextProject";
                            projindexno = (projindexno + 1);
                            qualityProject = lstprojects.Select((item, index) => new { index, item }).Where(x => x.index == projindexno).FirstOrDefault().item.projectCode;

                            break;
                        default:
                            ViewBag.Title = "";
                            break;
                    }

                    objresponse.Project = qualityProject;
                    objresponse.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objresponse.Key = false;
                objresponse.Value = "Error Occures Please try again";
            }

            return Json(objresponse, JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetIdenticalProjectEdit(string param)
        {
            List<ddlValue> identicalProject = db.QMS010.Where(i => !i.QualityProject.Equals(param, StringComparison.OrdinalIgnoreCase)).Select(i => new ddlValue { id = i.QualityProject, text = i.QualityProject }).Distinct().ToList();
            return Json(identicalProject, JsonRequestBehavior.AllowGet);
        }

        public bool isQualityIdExist(string qualityProject, string project, string bu, string location, string qid, int headerId = 0)
        {
            bool isQualityIdExist = false;

            List<QMS015> lstQMS015 = db.QMS015.Where(i => i.Location.Equals(location, StringComparison.OrdinalIgnoreCase)).ToList();

            if (!string.IsNullOrEmpty(qualityProject) && !string.IsNullOrEmpty(project) && !string.IsNullOrEmpty(bu) && !string.IsNullOrEmpty(qid))
            {
                lstQMS015 = lstQMS015.Where(i => i.QualityProject.Trim().Equals(qualityProject.Trim(), StringComparison.OrdinalIgnoreCase) && i.Project.Trim().Equals(project.Trim(), StringComparison.OrdinalIgnoreCase) && i.BU.Trim().Equals(bu.Trim(), StringComparison.OrdinalIgnoreCase) && i.QualityId.Trim().Equals(qid.Trim(), StringComparison.OrdinalIgnoreCase) && i.HeaderId != headerId).ToList();

                if (lstQMS015.Any())
                    isQualityIdExist = true;
            }


            return isQualityIdExist;
        }

        [HttpPost]
        public ActionResult GetQualityProjectWiseProjectBUDetail(string qualityProject, string location, string qid)
        {
            ModelQualityId objModelQualityId = new ModelQualityId();
            var project = db.QMS010.Where(i => i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase)).Select(i => i.Project).FirstOrDefault();
            objModelQualityId.Project = db.COM001.Where(i => i.t_cprj.Equals(project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + "-" + i.t_dsca).FirstOrDefault();// project;
            var ProjectWiseBULocation = Manager.getProjectWiseBULocation(project);
            objModelQualityId.QCPBU = ProjectWiseBULocation.QCPBU;
            if (isQualityIdExist(qualityProject, project, ProjectWiseBULocation.QCPBU.Split('-')[0], location, qid))
                objModelQualityId.headerAlreadyExist = true;

            var identicalProject = db.SWP003.Where(i => i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase)).Select(i => i.IQualityProject).Distinct().ToList();
            objModelQualityId.IdenticalProj = string.Join(",", identicalProject);
            return Json(objModelQualityId, JsonRequestBehavior.AllowGet);
        }
    }

   
    public class BUWiseDropdown
    {
        public string Project { get; set; }
        public string BUDescription { get; set; }
    }
    public class ResponceMsgWithStatus : clsHelper.ResponseMsg
    {
        public string budesc;
        public string projdesc;
        public string appdesc;
        public List<clsTask> lsttask { get; set; }
    }


    public class clsTask
    {
        public string id { get; set; }
        public string text { get; set; }
    }
    public class Project
    {
        public string projectCode { get; set; }
    }

    public class ResponceMsgWithHeaderID : clsHelper.ResponseMsg
    {
        public string Status;
        public int HeaderId;
        public int Sequence;
        public string Stage;
        public string Project;
        public string BU;
        public string Location;
        public string ProjectCode;
        public string BUCode;
        public string LocationCode;
        public int TPHeaderId;
        public int SWPHeaderId;
    }
}
