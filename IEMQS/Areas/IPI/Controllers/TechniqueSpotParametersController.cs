﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.IPI.Controllers
{
    public class TechniqueSpotParametersController : clsBase
    {
        [SessionExpireFilter]
        public ActionResult AscanUTDetails(int id)
        {
            QMS070 objQMS070 = db.QMS070.Where(x => x.SpotId == id).FirstOrDefault();
            QMS071 objQMS071 = db.QMS071.Where(x => x.SpotId == id).FirstOrDefault();
            string requestStatus = string.Empty;
            if (objQMS070 != null)
            {
                string Attended = clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue();
                requestStatus = db.QMS060.Where(x => x.RequestId == objQMS070.RequestId).Select(x => x.RequestStatus).FirstOrDefault();
                if (requestStatus.ToLower().Trim() == Attended.ToLower().Trim())
                {
                    ViewBag.Disabled = "disabled";
                }
            }

            if (objQMS071 != null)
            {
                string strBU = db.COM001.Where(i => i.t_cprj == objQMS071.Project).FirstOrDefault().t_entu;
                var BUDescription = (from a in db.COM002
                                     where a.t_dtyp == 2 && a.t_dimx == strBU
                                     select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
                objQMS071.BU = BUDescription.BUDesc;
                var locDescription = (from a in db.COM002
                                      where a.t_dtyp == 1 && a.t_dimx == objQMS071.Location
                                      select new { Location = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
                objQMS071.Location = locDescription.Location;

                var projectDetails = db.COM001.Where(x => x.t_cprj == objQMS071.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                ViewBag.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
            }
            else
            {
                objQMS071 = new QMS071();
                objQMS071.SpotId = objQMS070.SpotId;
                objQMS071.RequestId = objQMS070.RequestId;
                objQMS071.QualityProject = objQMS070.QualityProject;
                objQMS071.Project = db.COM001.Where(x => x.t_cprj == objQMS070.Project).Select(x => x.t_cprj + " - " + x.t_dsca).FirstOrDefault();
                objQMS071.BU = db.COM002.Where(a => a.t_dimx == objQMS070.BU).Select(a => a.t_dimx + "-" + a.t_desc).FirstOrDefault();
                objQMS071.Location = db.COM002.Where(a => a.t_dimx == objQMS070.Location).Select(a => a.t_dimx + "-" + a.t_desc).FirstOrDefault();
                objQMS071.SeamNo = objQMS070.SeamNo;
                objQMS071.StageCode = objQMS070.StageCode;
                objQMS071.StageSequence = objQMS070.StageSequence;
                objQMS071.IterationNo = objQMS070.IterationNo;
                objQMS071.RequestNo = objQMS070.RequestNo;
                objQMS071.RequestNoSequence = objQMS070.RequestNoSequence;
                objQMS071.SpotNumber = objQMS070.SpotNumber;
                //return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
            }
            return View(objQMS071);
        }

        [HttpPost]
        public ActionResult UpdateAscanUTDetails(QMS071 qms071)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS071 objQMS071 = db.QMS071.Where(x => x.SpotId == qms071.SpotId).FirstOrDefault();
                if (objQMS071 != null)
                {
                    var lstobjQMS071 = db.QMS071.Where(i => i.RequestId == objQMS071.RequestId).ToList();
                    objQMS071.ProbeAngle = qms071.ProbeAngle;
                    objQMS071.ScanningDirection = qms071.ScanningDirection;
                    objQMS071.EffReflectorSize = qms071.EffReflectorSize;
                    objQMS071.Amplitude = qms071.Amplitude;
                    objQMS071.BeamPath = qms071.BeamPath;
                    objQMS071.XAt = qms071.XAt;
                    objQMS071.YfromReferenceLine = qms071.YfromReferenceLine;
                    objQMS071.MinDepth = qms071.MinDepth;
                    objQMS071.MaxDepth = qms071.MaxDepth;
                    objQMS071.Length = qms071.Length;
                    objQMS071.LineRemark = qms071.LineRemark;
                    objQMS071.CommonRemark = qms071.CommonRemark;
                    objQMS071.EditedBy = objClsLoginInfo.UserName;
                    objQMS071.EditedOn = DateTime.Now;

                    lstobjQMS071.ForEach(i =>
                    {
                        i.CommonRemark = qms071.CommonRemark;
                    });

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Spot details successfully updated.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Spot parameter details not available.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteAscanUTDetails(int lineid)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {

                QMS071 objQMS071 = db.QMS071.Where(x => x.LineId == lineid).FirstOrDefault();
                if (objQMS071 != null)
                {
                    db.QMS071.Remove(objQMS071);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details successfully deleted.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details not available for delete.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadASCANUTParameterData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string WhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                string _urlform = string.Empty;
                string indextype = param.Department;
                if (string.IsNullOrWhiteSpace(indextype))
                {
                    indextype = clsImplementationEnum.ADDIndexType.maintain.GetStringValue();
                }
                WhereCondition += "1=1 and SpotId = " + param.CategoryId;
                bool flag = param.IsVisible;
                //search Condition 
                string[] columnName = { "DocumentName", "RevNo", "Description", "Status", "Title", "EffectiveDate" };
                WhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                var lstResult = db.SP_IPI_GET_ASCAN_UT_PARAMETER(StartIndex, EndIndex, strSortOrder, WhereCondition).ToList();

                var data = (from h in lstResult
                            select new[]
                           {
                            Convert.ToString(h.ROW_NO),
                            Convert.ToString(h.ProbeAngle),
                            Convert.ToString(h.ScanningDirection),
                            Convert.ToString(h.EffReflectorSize),
                            Convert.ToString(h.Amplitude),
                            Convert.ToString(h.BeamPath),
                            Convert.ToString(h.XAt),
                            Convert.ToString(h.YfromReferenceLine),
                            Convert.ToString(h.MinDepth),
                            Convert.ToString(h.MaxDepth),
                            Convert.ToString(h.Length),
                            Convert.ToString(h.LineRemark),
                            Convert.ToString(h.CommonRemark),
                            "<nobr><center>"
                                    + Helper.GenerateActionIcon(h.LineId, "Edit", "Edit Detail", "fa fa-edit","EditDetails("+h.LineId+", '"+ h.ProbeAngle+"', '"+h.ScanningDirection+"', '"+h.EffReflectorSize+"', '"+h.Amplitude+"', '"+h.BeamPath+"', '"+h.XAt+"', '"+h.YfromReferenceLine+"', '"+h.MinDepth+"', '"+h.MaxDepth+"', '"+h.Length+"', '"+h.LineRemark+"', '"+h.CommonRemark+"') ","",flag == true ? true:false)
                                    + Helper.GenerateActionIcon(h.LineId,"Delete","Delete Detail","fa fa-trash-o", "DeleteAscanUTDetails("+ h.LineId +");","", flag == true ? true:false)
                            + "</center></nobr>",
                          }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = WhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult DeletePAUTDetails(int lineid)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {

                QMS072 objQMS072 = db.QMS072.Where(x => x.LineId == lineid).FirstOrDefault();
                if (objQMS072 != null)
                {
                    int spotid = objQMS072.SpotId;
                    db.QMS072.Remove(objQMS072);
                    db.SaveChanges();
                    if (db.QMS072.Where(a => a.SpotId == spotid).Count() == 0)
                    {
                        objResponseMsg.Value = "redirect";

                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details successfully deleted.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details not available for delete.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveAscanUTDetails(QMS071 qms071)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS071 objQMS071 = new QMS071();
                var lstobjQMS071 = db.QMS071.Where(i => i.RequestId == qms071.RequestId).ToList();
                lstobjQMS071.ForEach(i =>
                {
                    i.CommonRemark = qms071.CommonRemark;
                });
                if (qms071.LineId > 0)
                {
                    objQMS071 = db.QMS071.Where(i => i.LineId == qms071.LineId).FirstOrDefault();
                    objQMS071.ProbeAngle = qms071.ProbeAngle;
                    objQMS071.ScanningDirection = qms071.ScanningDirection;
                    objQMS071.EffReflectorSize = qms071.EffReflectorSize;
                    objQMS071.Amplitude = qms071.Amplitude;
                    objQMS071.BeamPath = qms071.BeamPath;
                    objQMS071.XAt = qms071.XAt;
                    objQMS071.YfromReferenceLine = qms071.YfromReferenceLine;
                    objQMS071.MinDepth = qms071.MinDepth;
                    objQMS071.MaxDepth = qms071.MaxDepth;
                    objQMS071.Length = qms071.Length;
                    objQMS071.LineRemark = qms071.LineRemark;
                    objQMS071.CommonRemark = qms071.CommonRemark;
                    objQMS071.EditedBy = objClsLoginInfo.UserName;
                    objQMS071.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Value = "Spot details successfully updated.";
                }
                else
                {
                    objQMS071.SpotId = qms071.SpotId;
                    objQMS071.RequestId = qms071.RequestId;
                    objQMS071.QualityProject = qms071.QualityProject;
                    objQMS071.Project = qms071.Project.Split('-')[0].Trim();
                    objQMS071.BU = qms071.BU.Split('-')[0].Trim();
                    objQMS071.Location = qms071.Location.Split('-')[0].Trim();
                    objQMS071.SeamNo = qms071.SeamNo;
                    objQMS071.StageCode = qms071.StageCode;
                    objQMS071.StageSequence = qms071.StageSequence;
                    objQMS071.IterationNo = qms071.IterationNo;
                    objQMS071.RequestNo = qms071.RequestNo;
                    objQMS071.RequestNoSequence = qms071.RequestNoSequence;
                    objQMS071.SpotNumber = qms071.SpotNumber;
                    objQMS071.ProbeAngle = qms071.ProbeAngle;
                    objQMS071.ScanningDirection = qms071.ScanningDirection;
                    objQMS071.EffReflectorSize = qms071.EffReflectorSize;
                    objQMS071.Amplitude = qms071.Amplitude;
                    objQMS071.BeamPath = qms071.BeamPath;
                    objQMS071.XAt = qms071.XAt;
                    objQMS071.YfromReferenceLine = qms071.YfromReferenceLine;
                    objQMS071.MinDepth = qms071.MinDepth;
                    objQMS071.MaxDepth = qms071.MaxDepth;
                    objQMS071.Length = qms071.Length;
                    objQMS071.LineRemark = qms071.LineRemark;
                    objQMS071.CommonRemark = qms071.CommonRemark;
                    objQMS071.CreatedBy = objClsLoginInfo.UserName;
                    objQMS071.CreatedOn = DateTime.Now;
                    db.QMS071.Add(objQMS071);
                    db.SaveChanges();
                    objResponseMsg.Value = "Spot details successfully added.";
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public ActionResult PAUTDetails(int id)
        {
            QMS070 objQMS070 = db.QMS070.Where(x => x.SpotId == id).FirstOrDefault();
            QMS072 objQMS072 = db.QMS072.Where(x => x.SpotId == id).FirstOrDefault();
            string requestStatus = string.Empty;
            if (objQMS070 != null)
            {
                string Attended = clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue();
                requestStatus = db.QMS060.Where(x => x.RequestId == objQMS070.RequestId).Select(x => x.RequestStatus).FirstOrDefault();
                if (requestStatus.ToLower().Trim() == Attended.ToLower().Trim())
                {
                    ViewBag.Disabled = "disabled";
                }
            }
            if (objQMS072 != null)
            {
                string strBU = db.COM001.Where(i => i.t_cprj == objQMS072.Project).FirstOrDefault().t_entu;
                var BUDescription = (from a in db.COM002
                                     where a.t_dtyp == 2 && a.t_dimx == strBU
                                     select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
                objQMS072.BU = BUDescription.BUDesc;
                var locDescription = (from a in db.COM002
                                      where a.t_dtyp == 1 && a.t_dimx == objQMS072.Location
                                      select new { Location = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
                objQMS072.Location = locDescription.Location;

                var projectDetails = db.COM001.Where(x => x.t_cprj == objQMS072.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                ViewBag.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;


            }
            else
            {
                objQMS072 = new QMS072();
                objQMS072.SpotId = objQMS070.SpotId;
                objQMS072.RequestId = objQMS070.RequestId;
                objQMS072.QualityProject = objQMS070.QualityProject;
                objQMS072.Project = db.COM001.Where(x => x.t_cprj == objQMS070.Project).Select(x => x.t_cprj + " - " + x.t_dsca).FirstOrDefault();
                objQMS072.BU = db.COM002.Where(a => a.t_dimx == objQMS070.BU).Select(a => a.t_dimx + "-" + a.t_desc).FirstOrDefault();
                objQMS072.Location = db.COM002.Where(a => a.t_dimx == objQMS070.Location).Select(a => a.t_dimx + "-" + a.t_desc).FirstOrDefault();
                objQMS072.SeamNo = objQMS070.SeamNo;
                objQMS072.StageCode = objQMS070.StageCode;
                objQMS072.StageSequence = objQMS070.StageSequence;
                objQMS072.IterationNo = objQMS070.IterationNo;
                objQMS072.RequestNo = objQMS070.RequestNo;
                objQMS072.RequestNoSequence = objQMS070.RequestNoSequence;
                objQMS072.SpotNumber = objQMS070.SpotNumber;
                //return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
            }
            return View(objQMS072);
        }

        [HttpPost]
        public ActionResult SavePAUTDetails(QMS072 qms072)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS072 objQMS072 = new QMS072();
                var lstobjQMS072 = db.QMS072.Where(i => i.RequestId == qms072.RequestId).ToList();
                lstobjQMS072.ForEach(i =>
                {
                    i.CommonRemark = qms072.CommonRemark;
                });
                if (qms072.LineId > 0)
                {
                    objQMS072 = db.QMS072.Where(i => i.LineId == qms072.LineId).FirstOrDefault();
                    objQMS072.StartLocation = qms072.StartLocation;
                    objQMS072.EndLocation = qms072.EndLocation;
                    objQMS072.YfromReferenceLine = qms072.YfromReferenceLine;
                    objQMS072.MinDepth = qms072.MinDepth;
                    objQMS072.MaxDepth = qms072.MaxDepth;
                    objQMS072.a_l = qms072.a_l;
                    objQMS072.a_t = qms072.a_t;
                    objQMS072.TypeofIndication = qms072.TypeofIndication;
                    objQMS072.LineRemark = qms072.LineRemark;
                    objQMS072.CommonRemark = qms072.CommonRemark;
                    objQMS072.EditedBy = objClsLoginInfo.UserName;
                    objQMS072.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Value = "Spot details successfully updated.";
                }
                else
                {
                    objQMS072.SpotId = qms072.SpotId;
                    objQMS072.RequestId = qms072.RequestId;
                    objQMS072.QualityProject = qms072.QualityProject;
                    objQMS072.Project = qms072.Project.Split('-')[0].Trim();
                    objQMS072.BU = qms072.BU.Split('-')[0].Trim();
                    objQMS072.Location = qms072.Location.Split('-')[0].Trim();
                    objQMS072.SeamNo = qms072.SeamNo;
                    objQMS072.StageCode = qms072.StageCode;
                    objQMS072.StageSequence = qms072.StageSequence;
                    objQMS072.IterationNo = qms072.IterationNo;
                    objQMS072.RequestNo = qms072.RequestNo;
                    objQMS072.RequestNoSequence = qms072.RequestNoSequence;
                    objQMS072.SpotNumber = qms072.SpotNumber;
                    objQMS072.StartLocation = qms072.StartLocation;
                    objQMS072.EndLocation = qms072.EndLocation;
                    objQMS072.YfromReferenceLine = qms072.YfromReferenceLine;
                    objQMS072.MinDepth = qms072.MinDepth;
                    objQMS072.MaxDepth = qms072.MaxDepth;
                    objQMS072.a_l = qms072.a_l;
                    objQMS072.a_t = qms072.a_t;
                    objQMS072.TypeofIndication = qms072.TypeofIndication;
                    objQMS072.LineRemark = qms072.LineRemark;
                    objQMS072.CommonRemark = qms072.CommonRemark;
                    objQMS072.CreatedBy = objClsLoginInfo.UserName;
                    objQMS072.CreatedOn = DateTime.Now;
                    db.QMS072.Add(objQMS072);
                    db.SaveChanges();
                    objResponseMsg.Value = "Spot details successfully added.";
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        public ActionResult LoadPAUTUTParameterData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string WhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                string _urlform = string.Empty;
                string indextype = param.Department;
                if (string.IsNullOrWhiteSpace(indextype))
                {
                    indextype = clsImplementationEnum.ADDIndexType.maintain.GetStringValue();
                }
                WhereCondition += "1=1 and SpotId = " + param.CategoryId;
                //search Condition 
                bool flag = param.IsVisible;
                string[] columnName = { "DocumentName", "RevNo", "Description", "Status", "Title", "EffectiveDate" };
                WhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                var lstResult = db.SP_IPI_GET_TOFD_PAUT_UT_PARAMETER(StartIndex, EndIndex, strSortOrder, WhereCondition).ToList();

                var data = (from h in lstResult
                            select new[]
                           {
                               Convert.ToString(h.ROW_NO),
                               Convert.ToString(h.StartLocation),
                               Convert.ToString(h.EndLocation),
                               Convert.ToString(h.EndLocation -  h.StartLocation),
                               Convert.ToString(h.YfromReferenceLine),
                               Convert.ToString(h.MinDepth),
                               Convert.ToString(h.MaxDepth),
                               Convert.ToString(h.MaxDepth -  h.MinDepth),
                               Convert.ToString(h.TypeofIndication),
                               Convert.ToString(h.a_l),
                               Convert.ToString(h.a_t),
                               Convert.ToString(h.LineRemark),
                                Convert.ToString(h.CommonRemark),
                                "<nobr><center>"+
                                         Helper.GenerateActionIcon(h.LineId, "Edit", "Edit Detail", "fa fa-edit", "EditDetails("+h.LineId+",'"+h.StartLocation+"','"+h.EndLocation+"','"+h.YfromReferenceLine+"','"+h.MinDepth+"','"+h.MaxDepth+"','"+h.a_l+"','"+h.a_t+"','"+h.TypeofIndication+"','"+h.LineRemark+"','"+h.CommonRemark+"') ","" ,flag == true ? true:false)
                                       + Helper.GenerateActionIcon(h.LineId,"Delete","Delete Record","fa fa-trash-o", "DeletePAUTSpotDetails("+ h.LineId +");","",flag == true ? true:false)
                               + "</center></nobr>",

                          }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = WhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdatePAUTDetails(QMS072 qms072)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS072 objQMS072 = db.QMS072.Where(x => x.SpotId == qms072.SpotId).FirstOrDefault();
                if (objQMS072 != null)
                {
                    var lstobjQMS072 = db.QMS072.Where(i => i.RequestId == objQMS072.RequestId).ToList();
                    objQMS072.StartLocation = qms072.StartLocation;
                    objQMS072.EndLocation = qms072.EndLocation;
                    objQMS072.YfromReferenceLine = qms072.YfromReferenceLine;
                    objQMS072.MinDepth = qms072.MinDepth;
                    objQMS072.MaxDepth = qms072.MaxDepth;
                    objQMS072.a_l = qms072.a_l;
                    objQMS072.a_t = qms072.a_t;
                    objQMS072.TypeofIndication = qms072.TypeofIndication;
                    objQMS072.LineRemark = qms072.LineRemark;
                    objQMS072.CommonRemark = qms072.CommonRemark;
                    objQMS072.EditedBy = objClsLoginInfo.UserName;
                    objQMS072.EditedOn = DateTime.Now;

                    lstobjQMS072.ForEach(i =>
                    {
                        i.CommonRemark = qms072.CommonRemark;
                    });

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Spot details successfully updated.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Spot parameter details not available.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
    }
}