﻿using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.IPI.Controllers
{
    public class ApproveICLSeamController : clsBase
    {
        // GET: IPI/ApproveICLSeam
        #region Main page
        [SessionExpireFilter]
        [AllowAnonymous]
        //[UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadHeaderPartial(string status, string qualityProject,string location)
        {
            ViewBag.Status = status;
            ViewBag.qualityProject = qualityProject;
            ViewBag.location = location;
            return PartialView("_LoadHeaderGridPartial");
        }

        [HttpPost]
        public JsonResult LoadICLSeamHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = String.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    whereCondition += " 1=1 and qms30.Status in('" + clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue() + "')";
                }
                else
                {
                    whereCondition += " 1=1";
                }
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms30.BU", "qms30.Location");
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += " and (qms30.Location like '%" + param.sSearch + "%' or bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or qms30.QualityProject like '%" + param.sSearch + "%'or (qms30.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%')";
                }

                //  var lstHeader = db.SP_IPI_GetQualityIDHeader(startIndex, endIndex, "", whereCondition).ToList();
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstHeader = db.SP_IPI_GET_SEAM_ICL_GROUP(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstHeader
                          select new[] {
                        Convert.ToString(a.ROW_NO),
                        a.QualityProject,
                        a.Project,
                        a.BU,
                        a.Location,
                        Convert.ToString(a.HeaderId)
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadTPHeaderDataold(JQueryDataTableParamModel param, string qProj)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += " 1=1 and qms30.status in('" + clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue() + "')";
                }
                else
                {
                    strWhere += " 1=1";
                }
                if (!string.IsNullOrEmpty(qProj))
                {
                    strWhere += " AND UPPER(qms30.QualityProject) = '" + qProj.ToUpper() + "'";
                }
                if (!string.IsNullOrEmpty(param.Location))
                {
                    strWhere += " AND UPPER(qms30.Location) = '" + param.Location.ToUpper() + "'";
                }
                string[] columnName = { "qms30.QualityProject", "qms30.SeamNo", "qms30.Location", "qms30.PTCApplicable", "qms30.PTCNumber", "qms30.Status", "qms30.Status", "c20.QualityId", "c20.QualityIdRev" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.Location, "", "qms30.Location");
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_IPI_SEAM__ICL_HEADER_DETAILS
                               (
                               StartIndex, EndIndex, strSortOrder, strWhere
                               ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.HeaderId),
                            Helper.GenerateHidden(uc.HeaderId, "HeaderId", Convert.ToString(uc.HeaderId)),
                            Convert.ToString(uc.SeamNo),
                            Convert.ToString(uc.RevNo),
                            Convert.ToString(uc.QualityId),
                            Convert.ToString(!uc.QualityIdRev.HasValue ? "" : "R"+uc.QualityIdRev),
                            uc.PTCApplicable?"Yes":"No",
                            Convert.ToString(uc.PTCNumber),
                            Convert.ToString(uc.Status),
                            Convert.ToString(uc.TPRevNo),
                            "<a class='btn btn-xs' href='javascript:void(0)' onclick=ShowTimeline('/IPI/ApproveICLSeam/ShowTimeline?HeaderID="+Convert.ToInt32(uc.HeaderId)+"');><i style = 'margin-left:5px;' class='fa fa-clock-o'></i></a>",
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public class BUWiseDropdown
        {
            public string Project { get; set; }
            public string BUDescription { get; set; }
            public bool Key;
            public string Value;

            public List<CategoryData> TPIAgency { get; set; }
            public List<CategoryData> TPIIntervention { get; set; }
            public List<CategoryData> Stage { get; set; }
        }

        [HttpPost]
        public ActionResult GetProject(string qms)
        {   //get project and BU by Qms project
            if (!string.IsNullOrWhiteSpace(qms))
            {

                BUWiseDropdown objProjectDataModel = new BUWiseDropdown();

                string project = db.QMS010.Where(a => a.QualityProject == qms).FirstOrDefault().Project;
                string location = objClsLoginInfo.Location;
                var lstProject = (from a in db.COM001
                                  where a.t_cprj == project
                                  select new { ProjectDesc = a.t_cprj + " - " + a.t_dsca }).FirstOrDefault();
                string strBU = db.COM001.Where(i => i.t_cprj == project).FirstOrDefault().t_entu;

                var BUDescription = (from a in db.COM002
                                     where a.t_dtyp == 2 && a.t_dimx == strBU
                                     select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();

                var lstStage = db.QMS002.Where(i => i.BU.Equals(strBU, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(location, StringComparison.OrdinalIgnoreCase)).Select(i => new CategoryData { Value = i.StageCode, Code = i.StageCode, CategoryDescription = i.StageDesc }).ToList();
                var lstTPIAgency = Manager.GetSubCatagories("TPI Agency", strBU, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
                var lstTPIIntervention = Manager.GetSubCatagories("TPI Intervention", strBU, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();

                objProjectDataModel.Project = lstProject.ProjectDesc;
                objProjectDataModel.BUDescription = BUDescription.BUDesc;

                objProjectDataModel.TPIAgency = lstTPIAgency.Any() ? lstTPIAgency : null;
                objProjectDataModel.TPIIntervention = lstTPIIntervention.Any() ? lstTPIIntervention : null;
                objProjectDataModel.Stage = lstStage.Any() ? lstStage : null;

                if (lstTPIAgency.Any() && lstTPIIntervention.Any() && lstStage.Any())
                {
                    objProjectDataModel.Key = true;
                }
                else
                {
                    List<string> lstCategory = new List<string>();
                    if (!lstTPIAgency.Any())
                        lstCategory.Add("TPI Agency");
                    if (!lstTPIIntervention.Any())
                        lstCategory.Add("TPI Intervention");

                    objProjectDataModel.Value = string.Join(",", lstCategory.ToList()) + " Category not exist for selected project/Quality Project, Please contact Admin";
                }

                return Json(objProjectDataModel, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region View Details
        [SessionExpireFilter]
        [AllowAnonymous]
        //[UserPermissions]
        public ActionResult ViewDetails(string headerID)
        {
            int HeaderId = 0;
            if (!string.IsNullOrWhiteSpace(headerID))
            {
                HeaderId = Convert.ToInt32(headerID);
            }
            QMS030 objQMS030 = db.QMS030.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            if (objQMS030 != null)
            {
                ViewBag.BU = db.COM002.Where(a => a.t_dimx == objQMS030.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                ViewBag.Project = db.COM001.Where(i => i.t_cprj == objQMS030.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objQMS030.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            }
            return View(objQMS030);
        }

        public class ResponceMsgWithTPI : clsHelper.ResponseMsg
        {
            public string tpi;
            public string headerid;
        }
        [HttpPost]
        public JsonResult LoadICLSeamLineGridData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "";
                strWhere += Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();
                string[] columnName = { "StageCode", "StageSequance", "AcceptanceStandard", "ApplicableSpecification", "InspectionExtent", "RevNo", "StageStatus", "Remarks", "LineId", };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_IPI_SEAM_ICL_LINE_DETAILS
                              (
                             StartIndex,
                             EndIndex,
                             strSortOrder,
                             strWhere
                              ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                //Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.StageCode),
                                Convert.ToString(uc.StageSequance),
                                Convert.ToString(uc.AcceptanceStandard),
                                Convert.ToString(uc.ApplicableSpecification),
                                Convert.ToString(uc.InspectionExtent.Split('-')[1].Trim()),
                                Convert.ToString(uc.Parallel),
                                 Convert.ToString(uc.StageStatus),
                                Convert.ToString(uc.Remarks),
                                uc.ProtocolTypeDescription,
                                "<nobr>"+
                                "<a class='' href='javascript:void(0)' onclick=ShowTimeline('/IPI/ApproveICLSeam/ShowTimeline?HeaderID="+Convert.ToInt32(uc.HeaderId)+"&LineId="+Convert.ToInt32(uc.LineId)+"');><i style='' class='iconspace fa fa-clock-o'></i></a>"+
                                 (new MaintainSeamICLController()).MaintainIPIActionbutton(uc.HeaderId, uc.LineId,uc.QualityProject,uc.Project.Split('-')[0].Trim(),uc.SeamNo, uc.BU.Split('-')[0].Trim(), uc.Location.Split('-')[0].Trim(),uc.StageCode.Split('-')[0].Trim(),uc.StageCode,uc.StageSequance,uc.StageType)
                                + HTMLActionString(uc.LineId,"ViewProtocol","View Protocol","fa fa-file-text", uc.ProtocolType == clsImplementationEnum.ProtocolType.Other_Files.GetStringValue() ? "OpenAttachmentPopUp("+uc.LineId+", false, \"QMS031/" + uc.LineId+"\")" : "ShowProtocol(\"" + WebsiteURL + "/PROTOCOL/" + uc.ProtocolType + "/Linkage/" + Convert.ToInt32(uc.ProtocolId) + "\")", uc.ProtocolType == clsImplementationEnum.ProtocolType.Other_Files.GetStringValue() ? false : uc.ProtocolId == null)
                                +"</nobr>",
                                //+ HTMLActionString(uc.LineId,"ViewProtocol","View Protocol","fa fa-file-text","ShowProtocol(\"" + WebsiteURL + "/PROTOCOL/"+ uc.ProtocolType +"/Linkage/"+ Convert.ToInt32(uc.ProtocolId) + "\")", uc.ProtocolId == null),
                                Convert.ToString(uc.LineId),
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]
        [AllowAnonymous]
        [HttpPost]
        public ActionResult ApproveICLSeam_old(int strHeaderId)
        {
            QMS031 objQMS031 = new QMS031();
            objQMS031 = db.QMS031.Where(x => x.HeaderId == strHeaderId).FirstOrDefault();
            ResponceMsgWithTPI objResponseMsg = new ResponceMsgWithTPI();
            clsHelper.ResponseMsg objResponseMsg1 = new clsHelper.ResponseMsg();
            try
            {
                var lstQMS031 = (from dbo in db.QMS031
                                 where dbo.HeaderId == strHeaderId
                                 select dbo.StageCode.Trim()).Distinct().ToArray();
                bool isStageExists = true;
                UpdateTPIStagesInICL(strHeaderId);

                var lstQMS025 = (from dbo in db.QMS025
                                 where dbo.Location == objQMS031.Location && dbo.Project == objQMS031.Project && dbo.QualityProject == objQMS031.QualityProject
                                        && (dbo.FirstTPIAgency != null && dbo.FirstTPIAgency != "") && (dbo.FirstTPIIntervention != null && dbo.FirstTPIIntervention != "")
                                 select dbo.Stage.Trim()).ToList();
                string strStageName = string.Empty;
                foreach (var lst in lstQMS031)
                {
                    if (!lstQMS025.Contains(lst))
                    {
                        strStageName += lst + ",";
                        isStageExists = false;
                    }
                }
                if (isStageExists)
                {
                    var objQMS030 = db.QMS030.Where(x => x.HeaderId == strHeaderId).FirstOrDefault();
                    var stageStatus = clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue();
                    var lstTPStages = db.QMS031.Where(x => x.HeaderId == objQMS030.HeaderId && !x.StageStatus.Equals(stageStatus, StringComparison.OrdinalIgnoreCase)).ToList();
                    if (lstTPStages.Any())
                    {
                        objResponseMsg1 = InsUpdForReadyToOffer(objQMS030.HeaderId, lstTPStages);
                    }
                    if (objResponseMsg1.Key == true)
                    {
                        db.SP_IPI_ICL_SEAM_APPROVE(strHeaderId, "", objClsLoginInfo.UserName);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Approve.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.ICLSeamMessages.Error.ToString();
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Add Stages " + strStageName;
                    objResponseMsg.tpi = "AddTPI";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ICLSeamMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApproveByApproverSelected(string strHeaderIds)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = ApproveSeamICLCommon(strHeaderIds);
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApproveAllSeamICL(string QualityProject, string WhereCondition)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string strSendForApproval = clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue();
                //string whereCondition = "1=1 AND Status IN ('" + clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue() + "') AND UPPER(QualityProject) = '" + QualityProject.ToUpper() + "' ";
                //whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.Location, "", "Location");

                string strHeaderIds = string.Empty;
                if (!string.IsNullOrWhiteSpace(WhereCondition))
                {
                    var objList = db.SP_IPI_SEAM__ICL_HEADER_DETAILS(0, 0, "", WhereCondition).ToList();

                    //double checking here to fetch only pending records
                    var objPendingList = objList.Where(x => x.QualityProject.Equals(QualityProject, StringComparison.OrdinalIgnoreCase) && x.Status.Equals(strSendForApproval, StringComparison.OrdinalIgnoreCase)).ToList();
                    if (objPendingList != null && objPendingList.Count > 0)
                    {
                        strHeaderIds = string.Join(",", objPendingList.Select(x => x.HeaderId).ToList());
                        objResponseMsg = ApproveSeamICLCommon(strHeaderIds);
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No record found for approval";
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Something went Wrong..!";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ICLSeamMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponseMsgWithStatus ApproveSeamICLCommon(string strHeaderIds)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg1 = new clsHelper.ResponseMsgWithStatus();
            QMS031 objQMS031 = new QMS031();
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            int isattempted = 0;
            int isnotattempted = 0;
            string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
            int[] headerID = strHeaderIds.Split(',').Select(int.Parse).ToArray();
            try
            {
                string strStageName = string.Empty;
                bool key2 = true;
                bool key3 = true;
                List<string> lstTPIStagesSkipped = new List<string>();
                List<string> lstTPIStagesforSeamSkipped = new List<string>();
                string deleted = clsImplementationEnum.TestPlanStatus.Deleted.GetStringValue();
                for (int i = 0; i < arrayHeaderIds.Length; i++)
                {
                    int strHeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                    objQMS031 = db.QMS031.Where(x => x.HeaderId == strHeaderId).FirstOrDefault();
                    if (objQMS031.QMS030.Status == clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue())
                    {
                        //var lstQMS031 = (from dbo in db.QMS031
                        //                 where dbo.HeaderId == strHeaderId && dbo.StageStatus != deleted
                        //                 select dbo.StageCode.Trim()).Distinct().ToArray();
                        bool isStageExists = true;
                        //UpdateTPIStagesInICL(strHeaderId);

                        //var lstQMS025 = (from dbo in db.QMS025
                        //                 where dbo.Location == objQMS031.Location && dbo.Project == objQMS031.Project && dbo.QualityProject == objQMS031.QualityProject && dbo.BU == objQMS031.BU && dbo.Project == objQMS031.Project
                        //                       && (dbo.FirstTPIAgency != null && dbo.FirstTPIAgency != "") && (dbo.FirstTPIIntervention != null && dbo.FirstTPIIntervention != "")
                        //                 select dbo.Stage.Trim()).ToList();
                        //string strStageName = string.Empty;
                        //QMS010 objQMS010 = db.QMS010.Where(x => x.QualityProject == objQMS031.QualityProject && x.Location == objQMS031.Location && x.BU == objQMS031.BU).FirstOrDefault();
                        //if (objQMS010.TPIOnlineApproval)
                        //{
                        //    foreach (var lst in lstQMS031)
                        //    {
                        //        if (!lstQMS025.Contains(lst))
                        //        {
                        //            strStageName += lst + ",";
                        //            isStageExists = false;
                        //        }
                        //    }
                        //}
                        //else {
                        //List<string> stageType = new List<string>() { "PT", "MT", "RT", "UT" };

                        //foreach (var lst in lstQMS031)
                        //{
                        //    var sType = db.QMS002.Where(x => x.Location == objQMS031.Location && x.BU == objQMS031.BU && x.StageCode == lst).Select(x => x.StageType).FirstOrDefault();
                        //    if (stageType.Contains(sType))
                        //    {
                        //        if (!lstQMS025.Contains(lst))
                        //        {
                        //            lstTPIStagesSkipped.Add(lst);
                        //            lstTPIStagesforSeamSkipped.Add(objQMS031.SeamNo);
                        //            strStageName += lst + ",";
                        //            isStageExists = false;
                        //        }
                        //    }
                        //}
                        //}
                        if (isStageExists)
                        {
                            var objQMS030 = db.QMS030.Where(x => x.HeaderId == strHeaderId).FirstOrDefault();
                            //var stageStatus = clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue();
                            int maxOfferSeq = (new clsManager()).GetMaxOfferedSequenceNo(objQMS030.QualityProject, objQMS030.BU, objQMS030.Location, objQMS030.SeamNo);
                            var lstTPStages = db.QMS031.Where(x => x.HeaderId == objQMS030.HeaderId && x.StageSequance >= maxOfferSeq).ToList();
                            lstTPStages = lstTPStages.Where(x => !Manager.IsStageOffered(x.QualityProject, x.BU, x.Location, x.SeamNo, x.StageCode, x.StageSequance.Value)).ToList();
                            if (lstTPStages.Any())
                            {
                                if (Manager.IsLTFPSProject(objQMS030.Project, objQMS030.BU, objQMS030.Location))
                                {
                                    objResponseMsg1.Key = true;
                                }
                                else
                                {
                                    objResponseMsg1 = InsUpdForReadyToOffer(objQMS030.HeaderId, lstTPStages);
                                }
                            }
                            if (objResponseMsg1.Key == true)
                            {
                                db.SP_IPI_ICL_SEAM_APPROVE(strHeaderId, "", objClsLoginInfo.UserName);
                                //objResponseMsg.Key = true;
                                //objResponseMsg.Value = "Document approved successfully";
                                headerID = headerID.Where(val => val != strHeaderId).ToArray();
                                isattempted++;
                                #region Send Notification
                                (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), objQMS030.Project, objQMS030.BU, objQMS030.Location, "ICL for Seam: " + objQMS030.SeamNo + " of Project: " + objQMS030.QualityProject + "  has been Approved", clsImplementationEnum.NotificationType.Information.GetStringValue());
                                #endregion

                            }
                            else
                            {
                                //objResponseMsg.Key = false;
                                //objResponseMsg.Value = clsImplementationMessage.ICLSeamMessages.Error.ToString();
                                key2 = false;
                            }
                        }
                        else
                        {
                            //objResponseMsg.Key = false;
                            //objResponseMsg.Value = "Add Stages " + strStageName;
                            //objResponseMsg.tpi = "AddTPI";
                            key3 = false;
                        }
                    }
                    else
                    {
                        isnotattempted++;
                    }
                }
                if (!key2)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.ICLSeamMessages.Error.ToString();
                }
                else if (!key3)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Add Stages " + strStageName;
                    objResponseMsg.tpi = "AddTPI";
                    objResponseMsg.headerid = string.Join(",", headerID.Select(x => x.ToString()).ToArray());
                    if (lstTPIStagesSkipped.Any())
                    {
                        objResponseMsg.TPISkipped = string.Format("Seam {0} have been skipped as TPI Agency &  Intervention not maintained for stage(s) : {1}", string.Join(",", lstTPIStagesforSeamSkipped.Distinct()), string.Join(", ", lstTPIStagesSkipped.Distinct()));
                    }
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = string.Format("{0} Seam ICL(s) Approved successfully", isattempted);

                    if (isnotattempted > 0)
                    {
                        objResponseMsg.Remarks = string.Format("Seam ICL(s) {0} have been skipped as Seam ICL(s) {0} has been already attended", isnotattempted);
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ICLSeamMessages.Error.ToString();
            }
            return objResponseMsg;
        }

        [HttpPost]
        public ActionResult ReturnICLSelected(string strHeaderIds, string returnremark)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                List<string> lstICLNotSendForApprove = new List<string>();
                List<string> lstICLAttended = new List<string>();
                if (!string.IsNullOrEmpty(strHeaderIds))
                {
                    int[] headerIds = Array.ConvertAll(strHeaderIds.Split(','), i => Convert.ToInt32(i));
                    foreach (var headerId in headerIds)
                    {
                        var objQMS030 = db.QMS030.Where(i => i.HeaderId == headerId).FirstOrDefault();
                        if (objQMS030 != null)
                        {
                            if (objQMS030.Status == clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue())
                            {
                                objQMS030.Status = clsImplementationEnum.QualityIdHeaderStatus.Returned.GetStringValue();
                                objQMS030.ReturnedBy = objClsLoginInfo.UserName;
                                objQMS030.ReturnedOn = DateTime.Now;
                                objQMS030.ReturnRemark = returnremark;
                                db.SaveChanges();
                                #region Send Notification
                                (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.WE3.GetStringValue(), objQMS030.Project, objQMS030.BU, objQMS030.Location, "ICL for Seam: " + objQMS030.SeamNo + " of Project: " + objQMS030.QualityProject + "  has been Returned", clsImplementationEnum.NotificationType.Information.GetStringValue());
                                #endregion
                                lstICLAttended.Add(headerId.ToString());
                            }
                            else
                            {
                                lstICLNotSendForApprove.Add(headerId.ToString());
                            }
                        }
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = string.Format("{0} Seam(s) Returned successfully", lstICLAttended.Count());
                    if (lstICLNotSendForApprove.Any())
                    {
                        objResponseMsg.Remarks = string.Format("ICL(s) {0} have been skipped as Seam(s) {0} has been already attended", lstICLNotSendForApprove.Count());
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public class ResponceMsgWithStatus : clsHelper.ResponseMsg
        {
            public string Status;
        }


        public clsHelper.ResponseMsg UpdateTPIStagesInICL(int strHeaderId)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                string deleted = clsImplementationEnum.TestPlanStatus.Deleted.GetStringValue();

                QMS031 objQMS031 = new QMS031();
                var lstQMS031 = (from dbo in db.QMS031
                                 where dbo.HeaderId == strHeaderId && dbo.StageStatus != deleted
                                 select dbo.StageCode.Trim()).Distinct().ToArray();
                //string strStage = lstQMS031[i];
                for (int j = 0; j < lstQMS031.Length; j++)
                {
                    string strStage = lstQMS031[j];
                    //QMS031 newobjQMS031 = db.QMS031.Where(x => x.StageCode == strStage && x.HeaderId == strHeaderId).FirstOrDefault();
                    var obj031 = db.QMS031.Where(x => x.StageCode == strStage && x.HeaderId == strHeaderId).ToList();
                    ////QMS025 objQMS025 = db.QMS025.Where(a => a.Location == objQMS031.Location && a.BU == objQMS031.BU && a.Project == objQMS031.Project && a.QualityProject == objQMS031.QualityProject && strStage.Contains(a.Stage)).FirstOrDefault();
                    foreach (var newobjQMS031 in obj031)
                    {
                        QMS025 objQMS025 = db.QMS025.Where(a => a.Location == newobjQMS031.Location && a.BU == newobjQMS031.BU && a.Project == newobjQMS031.Project && a.QualityProject == newobjQMS031.QualityProject && strStage.Contains(a.Stage) && (a.FirstTPIAgency != null && a.FirstTPIAgency != "") && (a.FirstTPIIntervention != null && a.FirstTPIIntervention != "")).FirstOrDefault();

                        if (objQMS025 != null)
                        {
                            newobjQMS031.FirstTPIAgency = objQMS025.FirstTPIAgency;
                            newobjQMS031.FirstTPIIntervention = objQMS025.FirstTPIIntervention;
                            newobjQMS031.SecondTPIAgency = objQMS025.SecondTPIAgency;
                            newobjQMS031.SecondTPIIntervention = objQMS025.SecondTPIIntervention;
                            newobjQMS031.ThirdTPIAgency = objQMS025.ThirdTPIAgency;
                            newobjQMS031.ThirdTPIIntervention = objQMS025.ThirdTPIIntervention;
                            newobjQMS031.ForthTPIAgency = objQMS025.ForthTPIAgency;
                            newobjQMS031.ForthTPIIntervention = objQMS025.ForthTPIIntervention;
                            newobjQMS031.FifthTPIAgency = objQMS025.FifthTPIAgency;
                            newobjQMS031.FifthTPIIntervention = objQMS025.FifthTPIIntervention;
                            newobjQMS031.SixthTPIAgency = objQMS025.SixthTPIAgency;
                            newobjQMS031.SixthTPIIntervention = objQMS025.SixthTPIIntervention;
                            newobjQMS031.EditedBy = objClsLoginInfo.UserName;
                            newobjQMS031.EditedOn = DateTime.Now;
                            db.SaveChanges();

                            #region Update Inspection Agency In Protocol
                            /*
                          if (newobjQMS031.ProtocolId.HasValue)
                          {
                              List<string> lstInspectionAgency = new List<string>();
                              if (!string.IsNullOrWhiteSpace(Convert.ToString(newobjQMS031.FirstTPIAgency)))
                                  lstInspectionAgency.Add(Convert.ToString(newobjQMS031.FirstTPIAgency));
                              if (!string.IsNullOrWhiteSpace(Convert.ToString(newobjQMS031.SecondTPIAgency)))
                                  lstInspectionAgency.Add(Convert.ToString(newobjQMS031.SecondTPIAgency));
                              //if(!string.IsNullOrWhiteSpace(Convert.ToString(newobjQMS031.ThirdTPIAgency)))
                              //    lstInspectionAgency.Add(Convert.ToString(newobjQMS031.ThirdTPIAgency));
                              //if (!string.IsNullOrWhiteSpace(Convert.ToString(newobjQMS031.ForthTPIAgency)))
                              //    lstInspectionAgency.Add(Convert.ToString(newobjQMS031.ForthTPIAgency));
                              //if (!string.IsNullOrWhiteSpace(Convert.ToString(newobjQMS031.FifthTPIAgency)))
                              //    lstInspectionAgency.Add(Convert.ToString(newobjQMS031.FifthTPIAgency));
                              //if (!string.IsNullOrWhiteSpace(Convert.ToString(newobjQMS031.SixthTPIAgency)))
                              //    lstInspectionAgency.Add(Convert.ToString(newobjQMS031.SixthTPIAgency));

                              string strInspectionAgency = string.Join(", ", lstInspectionAgency);

                              string PRLTableName = Manager.GetLinkedProtocolTableName(newobjQMS031.ProtocolType);
                              db.Database.ExecuteSqlCommand("UPDATE " + PRLTableName + " SET InspectionAgency='" + strInspectionAgency + "' WHERE HeaderId = " + newobjQMS031.ProtocolId.Value);
                          }
                          */
                            #endregion

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return objResponseMsg;
        }


        public clsHelper.ResponseMsgWithStatus InsUpdForReadyToOffer(int headerid, List<QMS031> lstQMS31)
        {
            string currentUser = objClsLoginInfo.UserName;
            var currentLoc = objClsLoginInfo.Location;
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            QMS040 objQMS040 = new QMS040();
            var objQMS030 = db.QMS030.Where(x => x.HeaderId == headerid).FirstOrDefault();
            if (objQMS030 != null)
            {
                string RTOStatus = clsImplementationEnum.SeamListInspectionStatus.ReadyToOffer.GetStringValue();
                int maxOfferSeq = (new clsManager()).GetMaxOfferedSequenceNo(objQMS030.QualityProject, objQMS030.BU, objQMS030.Location, objQMS030.SeamNo);
                List<QMS040> lstOldQMS040 = db.QMS040.Where(i => i.QualityProject == objQMS030.QualityProject && i.BU == objQMS030.BU && i.SeamNo == objQMS030.SeamNo && i.Project == objQMS030.Project && i.Location == objQMS030.Location && i.StageSequence >= maxOfferSeq).ToList();
                lstOldQMS040 = lstOldQMS040.Where(i => !Manager.IsStageOffered(i.QualityProject, i.BU, i.Location, i.SeamNo, i.StageCode, i.StageSequence.Value)).ToList();
                if (lstOldQMS040.Count > 0)
                {
                    foreach (var itemQMS040 in lstOldQMS040)
                    {
                        var lst051 = itemQMS040.QMS051.ToList();
                        if (lst051.Count > 0)
                        {
                            db.QMS051.RemoveRange(lst051);
                        }

                        var lst061 = itemQMS040.QMS061.ToList();
                        if (lst061.Count > 0)
                        {
                            db.QMS061.RemoveRange(lst061);
                        }
                    }
                    db.QMS040.RemoveRange(lstOldQMS040);
                    db.SaveChanges();
                }

                foreach (var item in lstQMS31)
                {
                    objQMS040 = db.QMS040.Where(i => i.QualityProject.Equals(item.QualityProject, StringComparison.OrdinalIgnoreCase) && i.StageCode.Equals(item.StageCode, StringComparison.OrdinalIgnoreCase) && i.StageSequence == item.StageSequance && i.BU == item.BU && i.SeamNo == item.SeamNo && i.Project == item.Project && i.Location == item.Location).FirstOrDefault();
                    if (objQMS040 != null)
                    {
                        //objQMS040.StageStatus = item.StageStatus;
                        //if (string.Equals(objQMS020.Status, clsImplementationEnum.TestPlanStatus.Approved.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                        //{
                        //    objQMS020.Status = clsImplementationEnum.TestPlanStatus.Draft.GetStringValue();
                        //    objQMS020.RevNo += 1;
                        //}
                        //db.SaveChanges();
                        if (item.StageStatus.Equals(clsImplementationEnum.ICLSeamStatus.Deleted.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                        {
                            db.QMS040.Remove(objQMS040);
                        }
                        else
                        {
                            objQMS040.StageCode = item.StageCode;
                            objQMS040.StageSequence = item.StageSequance;
                            objQMS040.IsParallel = db.Database.SqlQuery<bool>("SELECT dbo.FN_IPI_ISSTAGEPARALLEL('" + item.QualityProject + "','" + item.Project + "','" + item.BU + "','" + item.Location + "','" + item.SeamNo + "'," + item.StageSequance + ")").FirstOrDefault();
                            objQMS040.Remarks = item.Remarks;
                            objQMS040.ProtocolId = item.ProtocolId;
                            objQMS040.ProtocolType = item.ProtocolType;
                            objQMS040.EditedBy = objClsLoginInfo.UserName; ;
                            objQMS040.EditedOn = DateTime.Now;
                        }
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                    }
                    else
                    {
                        if (!item.StageStatus.Equals(clsImplementationEnum.ICLSeamStatus.Deleted.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                        {
                            objQMS040 = new QMS040();
                            objQMS040.QualityProject = item.QualityProject;
                            objQMS040.Project = item.Project;
                            objQMS040.BU = item.BU;
                            //objQMS040.Location = currentLoc;
                            objQMS040.Location = item.Location;
                            objQMS040.SeamNo = item.SeamNo;
                            objQMS040.StageCode = item.StageCode;
                            objQMS040.IterationNo = 1;
                            objQMS040.StageSequence = item.StageSequance;
                            objQMS040.IsParallel = db.Database.SqlQuery<bool>("SELECT dbo.FN_IPI_ISSTAGEPARALLEL('" + item.QualityProject + "','" + item.Project + "','" + item.BU + "','" + item.Location + "','" + item.SeamNo + "'," + item.StageSequance + ")").FirstOrDefault();
                            objQMS040.Remarks = item.Remarks;
                            objQMS040.ProtocolId = item.ProtocolId;
                            objQMS040.ProtocolType = item.ProtocolType;
                            objQMS040.CreatedBy = objClsLoginInfo.UserName;
                            objQMS040.CreatedOn = DateTime.Now;
                            db.QMS040.Add(objQMS040);
                            db.SaveChanges();
                        }
                        objResponseMsg.Key = true;
                    }
                }

                // function for make part ready to offer 
                Manager.ReadyToOffer_SEAM(objQMS030);

            }

            //var readyToOfferStatus = clsImplementationEnum.PartlistOfferInspectionStatus.READY_TO_OFFER.GetStringValue();
            //var clearedStatus = clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue();
            //var ReadytoOfferStages = db.QMS040.Where(i => i.QualityProject.Equals(objQMS030.QualityProject, StringComparison.OrdinalIgnoreCase)
            //                                     && i.Project == objQMS030.Project && i.BU == objQMS030.BU && i.SeamNo == objQMS030.SeamNo && i.Location == objQMS030.Location && i.InspectionStatus.Equals(readyToOfferStatus)).ToList();
            //foreach (var readyToOfferStage in ReadytoOfferStages)
            //{
            //    readyToOfferStage.InspectionStatus = null;
            //    db.SaveChanges();
            //}
            //int maxOfferedSequenceNo = (new clsManager()).GetMaxOfferedSequenceNo(objQMS030.QualityProject, objQMS030.BU, objQMS030.Location, objQMS030.SeamNo);

            //if (!db.QMS040.Any(c => c.QualityProject.Equals(objQMS030.QualityProject, StringComparison.OrdinalIgnoreCase)
            //                                         && c.Project == objQMS030.Project && c.BU == objQMS030.BU && c.SeamNo == objQMS030.SeamNo && c.Location == objQMS030.Location 
            //                                         && c.InspectionStatus != clearedStatus && c.StageSequence <= maxOfferedSequenceNo))
            //{
            //    int LowerSequence = db.QMS040.Where(i => i.QualityProject.Equals(objQMS030.QualityProject, StringComparison.OrdinalIgnoreCase)
            //                                         && i.Project == objQMS030.Project && i.BU == objQMS030.BU && i.SeamNo == objQMS030.SeamNo && i.Location == objQMS030.Location && (i.StageSequence > maxOfferedSequenceNo || (i.StageSequence == maxOfferedSequenceNo && (i.InspectionStatus == null || i.InspectionStatus.Equals(readyToOfferStatus))))).OrderBy(o => o.StageSequence).FirstOrDefault().StageSequence.Value;

            //    var LowerSeqStages = db.QMS040.Where(i => i.QualityProject.Equals(objQMS030.QualityProject, StringComparison.OrdinalIgnoreCase)
            //                                         && i.Project == objQMS030.Project && i.BU == objQMS030.BU && i.SeamNo == objQMS030.SeamNo && i.Location == objQMS030.Location && i.StageSequence == LowerSequence && (i.InspectionStatus == null || i.InspectionStatus.Equals(readyToOfferStatus))).ToList();
            //    foreach (var lowerStage in LowerSeqStages)
            //    {
            //        lowerStage.InspectionStatus = clsImplementationEnum.PartlistOfferInspectionStatus.READY_TO_OFFER.GetStringValue();
            //        db.SaveChanges();
            //    }
            //}

            return objResponseMsg;
        }

        //public bool IsStageParallel(string qualityProject, string project, string bu, string location, string seamNo, int? stageSequence)
        //{
        //    bool isParallel = false;

        //    var result = (from qms31 in db.QMS031                             
        //                  join qms21 in db.QMS021 on new { A = qms31.QualityProject, B = qms31.Project, C = qms31.BU, D = qms31.Location, E = qms31.SeamNo, F = qms31.StageCode, G = qms31.StageSequance } equals new { A = qms21.QualityProject, B = qms21.Project, C = qms21.BU, D = qms21.Location, E = qms21.SeamNo, F = qms21.StageCode, G = qms21.StageSequance }
        //                  join qms2 in db.QMS002 on new { A = qms31.BU, B = qms31.Location, C = qms31.StageCode, D = qms31.StageSequance } equals new { A = qms2.BU, B = qms2.Location, C = qms2.StageCode, D = qms2.SequenceNo }
        //                  where qms31.QualityProject== qualityProject
        //                  && qms31.Project == project
        //                  && qms31.BU == bu
        //                  && qms31.Location == location
        //                  && qms31.SeamNo == seamNo
        //                  && qms31.StageSequance == stageSequence
        //                  && qms2.StageDepartment=="NDE"
        //                  && (qms2.StageType.Contains("PT") && qms2.StageType.Contains("MT") && qms2.StageType.Contains("MT") && )
        //                  select new { qms31.StageSequance }).ToList();











        //    return isParallel;
        //}
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.APPROVEINDEX.GetStringValue())
                {
                    var lst = db.SP_IPI_GET_SEAM_ICL_INDEXRESULT(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                }
                else
                {
                    var lst = db.SP_IPI_SEAM_ICL_LINE_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      SeamNo = li.SeamNo,
                                      StageCode = li.StageCode,
                                      StageSequance = li.StageSequance,
                                      StageStatus = li.StageStatus,
                                      Parallel = li.Parallel,
                                      FirstTPIAgency = li.FirstTPIAgency,
                                      FirstTPIIntervention = li.FirstTPIIntervention,
                                      SecondTPIAgency = li.SecondTPIAgency,
                                      SecondTPIIntervention = li.SecondTPIIntervention,
                                      ThirdTPIAgency = li.ThirdTPIAgency,
                                      ThirdTPIIntervention = li.ThirdTPIIntervention,
                                      ForthTPIAgency = li.ForthTPIAgency,
                                      ForthTPIIntervention = li.ForthTPIIntervention,
                                      FifthTPIAgency = li.FifthTPIAgency,
                                      FifthTPIIntervention = li.FifthTPIIntervention,
                                      SixthTPIAgency = li.SixthTPIAgency,
                                      SixthTPIIntervention = li.SixthTPIIntervention,
                                      AcceptanceStandard = li.AcceptanceStandard,
                                      ApplicableSpecification = li.ApplicableSpecification,
                                      InspectionExtent = li.InspectionExtent,
                                      Remarks = li.Remarks,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult LoadTPIData(int headerId)
        {
            QMS030 objQMS030 = new QMS030();
            QMS025 objQMS025 = new QMS025();
            if (headerId > 0)
            {
                objQMS030 = db.QMS030.Where(x => x.HeaderId == headerId).FirstOrDefault();

                if (objQMS030 != null)
                {
                    ViewBag.Stages = db.QMS002.Where(i => i.BU.Equals(objQMS030.BU, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(objQMS030.Location, StringComparison.OrdinalIgnoreCase)).OrderBy(x => x.StageCode).Select(i => new CategoryData { Value = i.StageCode, Code = i.StageCode, CategoryDescription = i.StageCode + "-" + i.StageDesc }).ToList();
                    var lstTPIAgency = Manager.GetSubCatagories("TPI Agency", objQMS030.BU, objQMS030.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
                    var lstTPIIntervention = Manager.GetSubCatagories("TPI Intervention", objQMS030.BU, objQMS030.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
                    ViewBag.TPIAgency = lstTPIAgency;
                    ViewBag.TPIIntervention = lstTPIIntervention;
                    objQMS025.BU = db.COM002.Where(a => a.t_dimx == objQMS030.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                    objQMS025.Project = db.COM001.Where(i => i.t_cprj == objQMS030.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                    objQMS025.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objQMS030.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                    objQMS025.QualityProject = objQMS030.QualityProject;
                    ViewBag.Action = "singleapprove";
                }
            }

            return PartialView("_LoadTPIDataHtml", objQMS025);
        }

        [HttpPost]
        public ActionResult SaveTPI(FormCollection fc, QMS025 QMS025)
        {


            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                string location = fc["Location"].Split('-')[0];
                string Status = clsImplementationEnum.ICLSeamStatus.Draft.GetStringValue();
                string Approved = clsImplementationEnum.ICLSeamStatus.Approved.GetStringValue();
                string loc = fc["Location"].Split('-')[0];
                string Fromstage = fc["hdFromStage"]; //fc["Stage"].Split('-')[0];
                string Tostage = fc["hdToStage"];//fc["ToStage"].Split('-')[0];
                string project = QMS025.Project.Split('-')[0];
                string BU = QMS025.BU.Split('-')[0];
                var lstResultForstage = db.SP_GETSTAGECODE_FORTPI(Fromstage, Tostage, QMS025.QualityProject).ToList();


                if (lstResultForstage.Count() > 0)
                {
                    foreach (var Stagecode in lstResultForstage.ToList())
                    {
                        if (!db.QMS025.Any(x => x.QualityProject == QMS025.QualityProject && x.Project == project && x.Location == location && x.BU == BU && x.Stage == Stagecode.StageCode.ToString()))
                        {


                            QMS025 objQMS025 = new QMS025();
                            objQMS025.QualityProject = QMS025.QualityProject;
                            objQMS025.Project = QMS025.Project.Split('-')[0];
                            objQMS025.BU = QMS025.BU.Split('-')[0];
                            objQMS025.Location = fc["Location"].Split('-')[0];
                            objQMS025.Stage = Stagecode.StageCode.ToString();

                            if (!string.IsNullOrWhiteSpace(fc["FirstTPIAgency"]))
                                objQMS025.FirstTPIAgency = fc["FirstTPIAgency"].Split('-')[0];
                            else
                                objQMS025.FirstTPIAgency = null;

                            if (!string.IsNullOrWhiteSpace(fc["FirstTPIIntervention"]))
                                objQMS025.FirstTPIIntervention = fc["FirstTPIIntervention"].Split('-')[0];
                            else
                                objQMS025.FirstTPIIntervention = null;


                            if (!string.IsNullOrWhiteSpace(fc["SecondTPIAgency"]))
                                objQMS025.SecondTPIAgency = fc["SecondTPIAgency"].Split('-')[0];
                            else
                                objQMS025.SecondTPIAgency = null;

                            if (!string.IsNullOrWhiteSpace(fc["SecondTPIIntervention"]))
                                objQMS025.SecondTPIIntervention = fc["SecondTPIIntervention"].Split('-')[0];
                            else
                                objQMS025.SecondTPIIntervention = null;


                            if (!string.IsNullOrWhiteSpace(fc["ThirdTPIAgency"]))
                                objQMS025.ThirdTPIAgency = fc["ThirdTPIAgency"].Split('-')[0];
                            else
                                objQMS025.ThirdTPIAgency = null;

                            if (!string.IsNullOrWhiteSpace(fc["ThirdTPIIntervention"]))
                                objQMS025.ThirdTPIIntervention = fc["ThirdTPIIntervention"].Split('-')[0];
                            else
                                objQMS025.ThirdTPIIntervention = null;


                            if (!string.IsNullOrWhiteSpace(fc["ForthTPIAgency"]))
                                objQMS025.ForthTPIAgency = fc["ForthTPIAgency"].Split('-')[0];
                            else
                                objQMS025.ForthTPIAgency = null;

                            if (!string.IsNullOrWhiteSpace(fc["ForthTPIIntervention"]))
                                objQMS025.ForthTPIIntervention = fc["ForthTPIIntervention"].Split('-')[0];
                            else
                                objQMS025.ForthTPIIntervention = null;


                            if (!string.IsNullOrWhiteSpace(fc["FifthTPIAgency"]))
                                objQMS025.FifthTPIAgency = fc["FifthTPIAgency"].Split('-')[0];
                            else
                                objQMS025.FifthTPIAgency = null;

                            if (!string.IsNullOrWhiteSpace(fc["FifthTPIIntervention"]))
                                objQMS025.FifthTPIIntervention = fc["FifthTPIIntervention"].Split('-')[0];
                            else
                                objQMS025.FifthTPIIntervention = null;


                            if (!string.IsNullOrWhiteSpace(fc["SixthTPIAgency"]))
                                objQMS025.SixthTPIAgency = fc["SixthTPIAgency"].Split('-')[0];
                            else
                                objQMS025.SixthTPIAgency = null;

                            if (!string.IsNullOrWhiteSpace(fc["SixthTPIIntervention"]))
                                objQMS025.SixthTPIIntervention = fc["SixthTPIIntervention"].Split('-')[0];
                            else
                                objQMS025.SixthTPIIntervention = null;
                            objQMS025.CreatedBy = objClsLoginInfo.UserName;
                            objQMS025.CreatedOn = DateTime.Now;
                            db.QMS025.Add(objQMS025);
                            db.SaveChanges();
                        }
                        else
                        {
                            var objQMS025 = db.QMS025.Where(x => x.QualityProject == QMS025.QualityProject && x.Project == project && x.Location == location && x.BU == BU && x.Stage == Stagecode.StageCode.ToString()).FirstOrDefault();
                            if (objQMS025 != null)
                            {
                                if (!string.IsNullOrWhiteSpace(fc["FirstTPIAgency"]))
                                    objQMS025.FirstTPIAgency = fc["FirstTPIAgency"].Split('-')[0];
                                else
                                    objQMS025.FirstTPIAgency = null;

                                if (!string.IsNullOrWhiteSpace(fc["FirstTPIIntervention"]))
                                    objQMS025.FirstTPIIntervention = fc["FirstTPIIntervention"].Split('-')[0];
                                else
                                    objQMS025.FirstTPIIntervention = null;


                                if (!string.IsNullOrWhiteSpace(fc["SecondTPIAgency"]))
                                    objQMS025.SecondTPIAgency = fc["SecondTPIAgency"].Split('-')[0];
                                else
                                    objQMS025.SecondTPIAgency = null;

                                if (!string.IsNullOrWhiteSpace(fc["SecondTPIIntervention"]))
                                    objQMS025.SecondTPIIntervention = fc["SecondTPIIntervention"].Split('-')[0];
                                else
                                    objQMS025.SecondTPIIntervention = null;


                                if (!string.IsNullOrWhiteSpace(fc["ThirdTPIAgency"]))
                                    objQMS025.ThirdTPIAgency = fc["ThirdTPIAgency"].Split('-')[0];
                                else
                                    objQMS025.ThirdTPIAgency = null;

                                if (!string.IsNullOrWhiteSpace(fc["ThirdTPIIntervention"]))
                                    objQMS025.ThirdTPIIntervention = fc["ThirdTPIIntervention"].Split('-')[0];
                                else
                                    objQMS025.ThirdTPIIntervention = null;


                                if (!string.IsNullOrWhiteSpace(fc["ForthTPIAgency"]))
                                    objQMS025.ForthTPIAgency = fc["ForthTPIAgency"].Split('-')[0];
                                else
                                    objQMS025.ForthTPIAgency = null;

                                if (!string.IsNullOrWhiteSpace(fc["ForthTPIIntervention"]))
                                    objQMS025.ForthTPIIntervention = fc["ForthTPIIntervention"].Split('-')[0];
                                else
                                    objQMS025.ForthTPIIntervention = null;


                                if (!string.IsNullOrWhiteSpace(fc["FifthTPIAgency"]))
                                    objQMS025.FifthTPIAgency = fc["FifthTPIAgency"].Split('-')[0];
                                else
                                    objQMS025.FifthTPIAgency = null;

                                if (!string.IsNullOrWhiteSpace(fc["FifthTPIIntervention"]))
                                    objQMS025.FifthTPIIntervention = fc["FifthTPIIntervention"].Split('-')[0];
                                else
                                    objQMS025.FifthTPIIntervention = null;


                                if (!string.IsNullOrWhiteSpace(fc["SixthTPIAgency"]))
                                    objQMS025.SixthTPIAgency = fc["SixthTPIAgency"].Split('-')[0];
                                else
                                    objQMS025.SixthTPIAgency = null;

                                if (!string.IsNullOrWhiteSpace(fc["SixthTPIIntervention"]))
                                    objQMS025.SixthTPIIntervention = fc["SixthTPIIntervention"].Split('-')[0];
                                else
                                    objQMS025.SixthTPIIntervention = null;
                                objQMS025.EditedBy = objClsLoginInfo.UserName;
                                objQMS025.EditedOn = DateTime.Now;
                                db.SaveChanges();
                            }
                        }
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Record Added Successfully";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Record Not Found";
                    //objResponseMsg.Status = "Record Not Found";
                    ViewBag.Action = "Add";
                    return Json(objResponseMsg);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg);
        }

        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            if (LineId > 0)
            {
                QMS031 objQMS031 = db.QMS031.Where(x => x.LineId == LineId).FirstOrDefault();
                model.ApprovedBy = objQMS031.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objQMS031.ApprovedBy) : null;
                model.ApprovedOn = objQMS031.ApprovedOn;
                model.SubmittedBy = objQMS031.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objQMS031.SubmittedBy) : null;
                model.SubmittedOn = objQMS031.SubmittedOn;
                model.CreatedBy = objQMS031.CreatedBy != null ? Manager.GetUserNameFromPsNo(objQMS031.CreatedBy) : null;
                model.CreatedOn = objQMS031.CreatedOn;
                model.EditedBy = objQMS031.EditedBy != null ? Manager.GetUserNameFromPsNo(objQMS031.EditedBy) : null;
                model.EditedOn = objQMS031.EditedOn;
            }
            else
            {
                QMS030 objQMS030 = db.QMS030.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.ApprovedBy = objQMS030.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objQMS030.ApprovedBy) : null;
                model.ApprovedOn = objQMS030.ApprovedOn;
                model.SubmittedBy = objQMS030.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objQMS030.SubmittedBy) : null;
                model.SubmittedOn = objQMS030.SubmittedOn;
                model.CreatedBy = objQMS030.CreatedBy != null ? Manager.GetUserNameFromPsNo(objQMS030.CreatedBy) : null;
                model.CreatedOn = objQMS030.CreatedOn;
                model.EditedBy = objQMS030.EditedBy != null ? Manager.GetUserNameFromPsNo(objQMS030.EditedBy) : null;
                model.EditedOn = objQMS030.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress2.cshtml", model);
        }


        public class ResponceMsgWithHeaderID : clsHelper.ResponseMsg
        {
            public int HeaderId;
        }

        public string HTMLActionString(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", bool isDisable = false)
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (isDisable)
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style=' Title='" + buttonTooltip + "' class='disabledicon " + className + "' ></i>";
            }
            else
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='iconspace " + className + "'" + onClickEvent + " ></i>";
            }

            return htmlControl;
        }


    }
}