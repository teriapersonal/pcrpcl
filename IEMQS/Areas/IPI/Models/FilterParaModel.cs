﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IEMQS.Areas.IPI.Models
{
    public class FilterParaModel
    {
        public string QualityProject { get; set; }
        public string Project { get; set; }
        public string ProjectDesc { get; set; }
        public string BU { get; set; }
        public string BUDesc { get; set; }
        public string Location { get; set; }
        public string LocationDesc { get; set; }
        public string Number { get; set; }
        public string ParentPartNo { get; set; }
        public string ChildPartNo { get; set; }
        public string LTFPSNo { get; set; }
        public string PartDescription { get; set; }
        public string ParentPartDescription { get; set; }


    }

    public class RequestFilterParaModel
    {
        public string QualityProject { get; set; }
        public string Project { get; set; }
        public string ProjectDesc { get; set; }
        public string BU { get; set; }
        public string BUDesc { get; set; }
        public string Location { get; set; }
        public string LocationDesc { get; set; }
        public string Number { get; set; }
        public string ParentPartNo { get; set; }
        public string ChildPartNo { get; set; }
        public string StageCode { get; set; }
        public string StageDesc { get; set; }
        public string StageSeq { get; set; }
        public string isNDEStage { get; set; }
        public string isFromSeam { get; set; }
    }
}