﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IEMQS.Areas.IPI.Models
{

    public class ModelProjectBU
    {
        public string Project { get; set; }
        public string BU { get; set; }
    }
    public class SeamListModel
    {


    }
    public class QualityProjectDetail
    {
        public string SeamListNo { get; set; }
        public string Condition { get; set; }
        public int? HeaderId { get; set; }
        public string Status { get; set; }
        public int? DocNo { get; set; }
        public string SeamListDescription { get; set; }
        public string ReviseRemark { get; set; }
    }
    public class SeamListSendOffer
    {
        public int HeaderId { get; set; }
        public string Shop { get; set; }
        public string Location { get; set; }
        public string ShopRemark { get; set; }
        public string SurfaceCondition { get; set; }
        public int? WeldThicknessReinforcement { get; set; }
        public string QualityProject { get; set; }
        public string StageCode { get; set; }
        public string folderPath { get; set; }
        public bool ISDocRequired { get; set; }

        public string OverlayThickness { get; set; }
        public string OverlayMaterials { get; set; }
    }

    public class SpotDetails
    {
        public int SpotNumber { get; set; }
        public string SpotDescription { get; set; }
        public int SpotLength { get; set; }
    }
    public class SpotResult
    {
        public int SpotId { get; set; }
        public int? SpotNumber { get; set; }
        public string SpotDescription { get; set; }
        public string Result { get; set; }
        public string IsRejected { get; set; }
        public int? DefectLength { get; set; }
        public int? DefectWidth { get; set; }
        public string IndicationType { get; set; }
        public string Reason { get; set; }
        public string Remarks { get; set; }
    }
    public class ICLSeam
    {
        public int SpotId { get; set; }
        public string SpotDescription { get; set; }
        public string Result { get; set; }
        public string IsRejected { get; set; }
        public string DefectLength { get; set; }
        public string DefectWidth { get; set; }
        public string IndicationType { get; set; }
        public string Reason { get; set; }
        public string Remarks { get; set; }
    }
    public partial class RevisionHistoryDtl
    {
        public string SeamNo { get; set; }
        public string SeamDescription { get; set; }
        public string SeamListNo { get; set; }
        public Nullable<int> SeamRev { get; set; }
        public string Status { get; set; }
        public string Position { get; set; }
        public string Thickness { get; set; }
        public string Material { get; set; }
        public string ViewDrawing { get; set; }
        public string WeldType { get; set; }
        public Nullable<int> SeamLengthArea { get; set; }
        public string Unit { get; set; }
        public string WEPDetail { get; set; }
        public string Remarks { get; set; }
        public string AssemblyNo { get; set; }
        public Nullable<int> DocNoSeam { get; set; }
        public string QualityProject { get; set; }
        public string Project { get; set; }
        public string SeamCategory { get; set; }
        public string ManualSeamNo { get; set; }
        public string ThicknessDefault { get; set; }
    }


    public class NDESeamExportExcel
    {
        public string QualityProjectNo { get; set; }
        public string SeamNo { get; set; }
        public string SeamNoDescription { get; set; }
        public Nullable<long> RequestNo { get; set; }
        public Nullable<int> Iteration { get; set; }
        public string StageCode { get; set; }
        public string StageDescription { get; set; }
        public Nullable<int> SequenceNo { get; set; }
        public string InspectionExtent { get; set; }
        public string BMThk1 { get; set; }
        public string BMThk2 { get; set; }
        public Nullable<int> WeldThkwithReinforcement { get; set; }
        public Nullable<int> SeamLength { get; set; }
        public string SeamArea { get; set; }
        public string JointType { get; set; }
        public string Shop { get; set; }
        public string FirstTPIAgency { get; set; }
        public string FirstTPIAgencyDescription { get; set; }
        public string FirstTPIIntervention { get; set; }
        public string SecondTPIAgency { get; set; }
        public string SecondTPIAgencyDescription { get; set; }
        public string SecondTPIIntervention { get; set; }
        public string ThirdTPIAgency { get; set; }
        public string ThirdTPIAgencyDescription { get; set; }
        public string ThirdTPIIntervention { get; set; }
        public string FourthTPIAgency { get; set; }
        public string FourthTPIAgencyDescription { get; set; }
        public string FourthTPIIntervention { get; set; }
        public string FifthTPIAgency { get; set; }
        public string FifthTPIAgencyDescription { get; set; }
        public string FifthTPIIntervention { get; set; }
        public string SixthTPIAgency { get; set; }
        public string SixthTPIAgencyDescription { get; set; }
        public string SixthTPIIntervention { get; set; }
        public string ApplicableSpecification { get; set; }
        public string AcceptanceStandard { get; set; }
        public string WeldingRemarks { get; set; }
        public string ShopRemarks { get; set; }
        public string CTQApplicable { get; set; }
        public string WelderStamp1 { get; set; }
        public string WelderStamp2 { get; set; }
        public string WelderStamp3 { get; set; }
        public string WelderStamp4 { get; set; }
        public string WelderStamp5 { get; set; }
        public string WelderStamp6 { get; set; }
        public string WelderStamp7 { get; set; }
        public string WelderStamp8 { get; set; }
        public string WelderStamp9 { get; set; }
        public string WelderStamp10 { get; set; }
        public string WelderStamp11 { get; set; }
        public string WelderStamp12 { get; set; }
        public string WelderStamp13 { get; set; }
        public string WelderStamp14 { get; set; }
        public string WelderStamp15 { get; set; }
        public string WelderStamp16 { get; set; }
        public string WelderStamp17 { get; set; }
        public string WelderStamp18 { get; set; }
        public string WelderStamp19 { get; set; }
        public string WelderStamp20 { get; set; }
        public string WelderStamp21 { get; set; }
        public string WelderStamp22 { get; set; }
        public string WelderStamp23 { get; set; }
        public string WelderStamp24 { get; set; }
        public string WeldingProcess { get; set; }
        public string ManufacturingCode { get; set; }
        public string SurfaceCondition { get; set; }
        public string OfferDate { get; set; }
        public string OfferTime { get; set; }
        public string InspectionDate { get; set; }
        public string InspectionTime { get; set; }
        public string InspectedBy { get; set; }
        public string InspectionStatus { get; set; }
        public string LNTTestResult { get; set; }
        public string NDTRemarks { get; set; }
    }

    public class CJCSeamExportExcel
    {
        public string QualityProjectNo { get; set; }
        public string SeamNo { get; set; }
        public string SeamCategory { get; set; }
        public string NozzleSize { get; set; }
        public string SeamSubCategory { get; set; }
        public string SeamJointType { get; set; }
        public string SeamLength { get; set; }
        public string Part1Position { get; set; }
        public string Part2Position { get; set; }
        public string Part3Position { get; set; }
        public string Part1Thickness { get; set; }
        public string Part2Thickness { get; set; }
        public string Part3Thickness { get; set; }
        public string MinThickness { get; set; }
        public string MeasuredValue { get; set; }
        public string WeldingProcess { get; set; }
        public string SetupRate { get; set; }
        public string WeldingRate { get; set; }
        public string CriticalSeam { get; set; }
        public string Factor { get; set; }
        public string Amount { get; set; }
    }
}