﻿using System.Web.Mvc;

namespace IEMQS.Areas.WQ
{
    public class WQAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "WQ";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "WQ_default",
                "WQ/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}