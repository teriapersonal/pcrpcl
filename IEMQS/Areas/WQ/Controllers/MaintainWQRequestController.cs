﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQS.Areas.WQ.Models;
using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQS.Areas.Utility.Models;

namespace IEMQS.Areas.WQ.Controllers
{
    public class MaintainWQRequestController : clsBase
    {
        // GET: WQ/MaintainWQRequest
        [SessionExpireFilter]
        #region WQR Index
        public ActionResult Index(int? roleId)
        {
            ModelRoleWiseIndex objModelRoleWiseIndex = new ModelRoleWiseIndex();

            if (roleId == Convert.ToInt32(RoleForWQ.ShoInCharge))
            {
                objModelRoleWiseIndex.TableTitle = "Welder Qualification Request List";
                objModelRoleWiseIndex.LinkRoleWise = "ShopInCharge";
                objModelRoleWiseIndex.PageTitle = "Approve Welder Qualification Request";
            }
            else if (roleId == Convert.ToInt32(RoleForWQ.WE))
            {
                objModelRoleWiseIndex.TableTitle = "Welder Qualification Request List";
                objModelRoleWiseIndex.LinkRoleWise = "WE";
                objModelRoleWiseIndex.PageTitle = "Approve Welder Qualification Request";
            }
            else
            {
                roleId = RoleForWQ.Initiator.GetHashCode();
                objModelRoleWiseIndex.AddNewHref = WebsiteURL + "/WQ/MaintainWQRequest/AddWQRequest?roleId=" + roleId;
                objModelRoleWiseIndex.AnchorText = "Add New";
                objModelRoleWiseIndex.TableTitle = "Welder Qualification Request List";
                objModelRoleWiseIndex.LinkRoleWise = "";
                objModelRoleWiseIndex.PageTitle = "Maintain Welder Qualification Request";
            }
            ViewBag.RoleId = roleId;
            return View(objModelRoleWiseIndex);
        }

        public ActionResult IndexShopInCharge()
        {
            TempData["Role"] = RoleForWQ.ShoInCharge;
            return RedirectToAction("Index", "MaintainWQRequest", new { roleId = Convert.ToInt32(RoleForWQ.ShoInCharge) });
        }

        public ActionResult IndexWE()
        {
            TempData["Role"] = RoleForWQ.WE;
            return RedirectToAction("Index", "MaintainWQRequest", new { roleId = Convert.ToInt32(RoleForWQ.WE) });
        }

        public ActionResult GetWQRequestHeaderDataPartial(string status, int roleId, string tableTitle)
        {
            ViewBag.TableTitle = tableTitle;
            ViewBag.status = status;
            ViewBag.roleId = roleId;
            return PartialView("_GetWQRequestHeaderDataPartial");
        }

        public ActionResult loadHeaderDataTable(JQueryDataTableParamModel param, int role, string status)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;

                string whereCondition = "1=1";
                //whereCondition += Manager.MakeDefaultLEMWhere(objClsLoginInfo.UserName, "wqr1.BU", "wqr1.Location");

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    //whereCondition += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or Location like '%" + param.sSearch + "%' or wps10.WPSNumber like '%" + param.sSearch + "%' or WPSRevNo like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%' or wps10.CreatedBy like '%" + param.sSearch + "%')";
                    whereCondition += Helper.MakeDatatableSearchCondition(new string[]
                                    { "wqr1.RequestNo", "wqr1.GeneralProject", "wqr1.QualificationProject", "com1.t_dsca","wqr1.RequestStatus", "wqr1.CreatedBy", "ecom3.t_name" }, param.sSearch);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (status.ToLower() == "pending")
                {
                    whereCondition += " AND wqr1.RequestStatus IN ('" + clsImplementationEnum.WQRequestStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.WQRequestStatus.Returned.GetStringValue() + "')";
                }
                whereCondition += Manager.MakeDefaultLEMWhere(objClsLoginInfo.UserName, "", "wqr1.Location");
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " and (RequestNo like '%" + param.sSearch
                         + "%' or GeneralProject like '%" + param.sSearch
                         + "%' or QualificationProject like '%" + param.sSearch
                         + "%' or Requeststatus like '%" + param.sSearch
                         + "%' or CreatedBy like '%" + param.sSearch
                         + "%' or CreatedOn like '%" + param.sSearch + "%')";
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                var lstHeader = db.SP_WQ_GET_REQUESTHEADER(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from h in lstHeader
                          select new[] {
                        Convert.ToString(h.HeaderId),
                        h.RequestNo,
                        h.GeneralProject,
                        h.QualificationProject,
                        h.Requeststatus,
                        h.CreatedBy,
                        Convert.ToDateTime(h.CreatedOn).ToString("dd/MM/yyyy"),
                        "<center><a class='' href='"+ WebsiteURL +"/WQ/MaintainWQRequest/AddWQRequest?roleId="+role+"&headerId="+h.HeaderId+"&Status="+status+"' ><i style='margin-left:5px;' class='fa fa-eye' title='Maintain WQ Request'></i></a>&nbsp;&nbsp;&nbsp;"+
                        (string.Equals(h.Requeststatus,clsImplementationEnum.WQRequestStatus.Draft.GetStringValue(),StringComparison.OrdinalIgnoreCase)|| string.Equals(h.Requeststatus,clsImplementationEnum.WQRequestStatus.Draft.GetStringValue(),StringComparison.OrdinalIgnoreCase) ? "<i style='cursor:pointer;' class='fa fa-trash-o' title='Delete Request' onClick='DeleteWQRequest("+h.HeaderId+","+h.RequestNo+")'></i></center>":  "<i style='cursor:pointer;opacity:0.5' class='fa fa-trash-o' title='Delete Request'></i>" ).ToString(),
                        //GetHTMLString(h.HeaderId)
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        //temp
        public ActionResult loadShopInChargeHeaderDataTable(JQueryDataTableParamModel param, int role, string status)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;

                string whereCondition = "1=1";
                //whereCondition += Manager.MakeDefaultLEMWhere(objClsLoginInfo.UserName, "wqr1.BU", "wqr1.Location");

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    //whereCondition += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or Location like '%" + param.sSearch + "%' or wps10.WPSNumber like '%" + param.sSearch + "%' or WPSRevNo like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%' or wps10.CreatedBy like '%" + param.sSearch + "%')";
                    whereCondition += Helper.MakeDatatableSearchCondition(new string[]
                                    { "RequestNo", "GeneralProject", "wqr1.QualificationProject", "com1.t_dsca","wqr1.Requeststatus" ,"wqr1.SubmittedBy","scom3.t_name" }, param.sSearch);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (status.ToLower() == "pending")
                {
                    whereCondition += " AND wqr1.Requeststatus IN ('" + clsImplementationEnum.WQRequestStatus.PendingWithShopInCharge.GetStringValue() + "')";
                    whereCondition += " AND wqr1.ShopInCharge=" + objClsLoginInfo.UserName;
                }
                if (status.ToLower() == "all")
                {
                    whereCondition += " AND wqr1.Requeststatus IN ('" + clsImplementationEnum.WQRequestStatus.Approved.GetStringValue() + "','" + clsImplementationEnum.WQRequestStatus.PendingWithWE.GetStringValue() + "')";
                    whereCondition += Manager.MakeDefaultLEMWhere(objClsLoginInfo.UserName, "wqr1.BU", "wqr1.Location");
                }

                var lstHeader = db.SP_WQ_GET_REQUESTHEADER(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from h in lstHeader
                          select new[] {
                        Convert.ToString(h.HeaderId),
                        Convert.ToString(h.HeaderId),
                        h.RequestNo,
                        h.GeneralProject,
                        h.QualificationProject,
                        h.Requeststatus,
                        h.SubmittedBy,
                        Convert.ToDateTime(h.SubmittedOn).ToString("dd/MM/yyyy"),
                        "<center><a class='' href='"+ WebsiteURL +"/WQ/MaintainWQRequest/AddWQRequest?roleId="+role+"&headerId="+h.HeaderId+"&Status="+status+"'><i style='margin-left:5px;' class='fa fa-eye' title='Maintain WQ Request' ></i></a></center>",
                        //GetHTMLString(h.HeaderId)
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult loadWEHeaderDataTable(JQueryDataTableParamModel param, int role, string status)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;

                string whereCondition = "1=1";

                //whereCondition += Manager.MakeDefaultLEMWhere(objClsLoginInfo.UserName, "wqr1.BU", "wqr1.Location");

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    //whereCondition += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or Location like '%" + param.sSearch + "%' or wps10.WPSNumber like '%" + param.sSearch + "%' or WPSRevNo like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%' or wps10.CreatedBy like '%" + param.sSearch + "%')";
                    whereCondition += Helper.MakeDatatableSearchCondition(new string[]
                                    { "wqr1.RequestNo", "wqr1.GeneralProject", "wqr1.QualificationProject", "com1.t_dsca","wqr1.Requeststatus", "wqr1.ShopInCharge","shcom3.t_name" }, param.sSearch);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (status.ToLower() == "pending")
                {
                    whereCondition += " AND wqr1.Requeststatus IN ('" + clsImplementationEnum.WQRequestStatus.PendingWithWE.GetStringValue() + "')";
                    whereCondition += " AND wqr1.WE=" + objClsLoginInfo.UserName;
                }
                if (status.ToLower() == "all")
                {
                    whereCondition += " AND wqr1.Requeststatus IN ('" + clsImplementationEnum.WQRequestStatus.Approved.GetStringValue() + "')";
                    whereCondition += Manager.MakeDefaultLEMWhere(objClsLoginInfo.UserName, "wqr1.BU", "wqr1.Location");
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " and (wqr1.RequestNo like '%" + param.sSearch
                         + "%' or wqr1.RequestNo like '%" + param.sSearch
                         + "%' or wqr1.QualificationProject+''-''+ com1.t_dsca like '%" + param.sSearch
                         + "%' or wqr1.Requeststatus like '%" + param.sSearch
                         + "%' or wqr1.ShopInCharge+'-'+shcom3.t_name like '%" + param.sSearch
                         + "%' or wqr1.ShopInChargeOn like '%" + param.sSearch + "%')";
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var lstHeader = db.SP_WQ_GET_REQUESTHEADER(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from h in lstHeader
                          select new[] {
                        Convert.ToString(h.HeaderId),
                        Convert.ToString(h.HeaderId),
                        h.RequestNo,
                        h.GeneralProject,
                        h.QualificationProject,
                        h.Requeststatus,
                        h.ShopInCharge,
                        Convert.ToDateTime(h.ShopInChargeOn).ToString("dd/MM/yyyy"),
                        "<center><a class='' href='"+ WebsiteURL +"/WQ/MaintainWQRequest/AddWQRequest?roleId="+role+"&headerId="+h.HeaderId+"&Status="+status+"'><i style='margin-left:5px;' class='fa fa-eye' title='Maintain WQ Request'></i></a></center>",
                        //GetHTMLString(h.HeaderId)
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        //timeline functionality for header grid
        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.TimelineTitle = "Welder Qualification Request History";
            model.Title = "ASME";
            WQR001 objWQR001 = db.WQR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            model.CreatedBy = Manager.GetUserNameFromPsNo(objWQR001.CreatedBy);
            model.CreatedOn = objWQR001.CreatedOn;
            model.ShopInCharge = Manager.GetUserNameFromPsNo(objWQR001.ShopInCharge);
            model.ShopInChargeOn = objWQR001.ShopInChargeOn;
            model.WE = Manager.GetUserNameFromPsNo(objWQR001.WE);
            model.WEOn = objWQR001.WEOn;
            model.WQTC = Manager.GetUserNameFromPsNo(objWQR001.WQTC);
            model.WQTCOn = objWQR001.WQTCOn;

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }

        [HttpPost]
        public JsonResult NextWQRequest(int roleId,int? headerId,string status)
        {
            if (headerId != 0)
            {
                ViewBag.roleId = roleId;
                ViewBag.status = status;
                string whereCondition = "1=1";
                
                whereCondition += Manager.MakeDefaultLEMWhere(objClsLoginInfo.UserName, "", "wqr1.Location");
                
                //var q = db.WQR001.Where(i => i.HeaderId > headerId && i.Requeststatus == "Draft" || i.Requeststatus == "Returned" ).OrderBy(x => x.HeaderId).FirstOrDefault();
                if (status == "Pending")
                {
                    //pending 
                    whereCondition += " AND wqr1.RequestStatus IN ('" + clsImplementationEnum.WQRequestStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.WQRequestStatus.Returned.GetStringValue() + "')";
                }                
                var lstHeader = db.SP_WQ_GET_REQUESTHEADER(0, 0, "", whereCondition).ToList();
                var q = lstHeader.Where(i => i.HeaderId > headerId).FirstOrDefault();
                return Json(q, JsonRequestBehavior.AllowGet);
            }
            return Json(null);
           
        }

        [HttpPost]
        public JsonResult PrevWQRequest(int roleId, int? headerId,string status)
        {
            db.Configuration.ProxyCreationEnabled = false;
            if (headerId != 0)
            {
                ViewBag.roleId = roleId;
                ViewBag.status = status;
                string whereCondition = "1=1";

               
                whereCondition += Manager.MakeDefaultLEMWhere(objClsLoginInfo.UserName, "", "wqr1.Location");
                
                //var q = db.WQR001.Where(i => i.HeaderId < headerId && i.Requeststatus == "Draft" || i.Requeststatus == "Returned").OrderByDescending(x => x.HeaderId).FirstOrDefault();

                if (status == "Pending")
                {
                    whereCondition += " AND wqr1.RequestStatus IN ('" + clsImplementationEnum.WQRequestStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.WQRequestStatus.Returned.GetStringValue() + "')";

                }
                var lstHeader = db.SP_WQ_GET_REQUESTHEADER(0, 0, "", whereCondition).ToList();
                var q = lstHeader.Where(i => i.HeaderId < headerId).OrderByDescending(i => i.HeaderId).FirstOrDefault();
                return Json(q, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);

        }
        #endregion

        [HttpPost]
        public JsonResult GetRequestNo(string term)
        {
            db.Configuration.ProxyCreationEnabled = false;

            var res = db.WQR001.Where(c => c.RequestNo.Contains(term)).Distinct();
            
            return Json(res, JsonRequestBehavior.AllowGet);
        }


        #region WQR Header
        [SessionExpireFilter]
        public ActionResult AddWQRequest(int roleId, int? headerId, string status)
        {
            ViewBag.roleId = roleId;
            ViewBag.status = status;
            WQR001 objWQR001 = new WQR001();
            var location = objClsLoginInfo.Location;
            //var userRole = objClsLoginInfo.GetUserRoleList();
            ViewBag.ReqForLocation = db.WQR001.Where(i => i.Location.Equals(location)).Count();
            ModelWQRequest objModelWQRequest = new ModelWQRequest();

            


            if (headerId > 0)
            {
                objWQR001 = db.WQR001.Where(i => i.HeaderId == headerId).FirstOrDefault();
                ViewBag.Location = db.WQR008.Where(x => x.Location == objWQR001.Location).Select(x => x.Location + "-" + x.Description).FirstOrDefault();
                ViewBag.WeldingProcess1 = objModelWQRequest.GetCategoryDescriptionLocationWise(objWQR001.WeldingProcess1, string.Empty, location).CategoryDescription;
                ViewBag.WeldingType1 = objModelWQRequest.GetCategoryDescriptionLocationWise(objWQR001.WeldingType1, string.Empty, location).CategoryDescription;
                ViewBag.WeldingPosition1 = objModelWQRequest.GetCategoryDescriptionLocationWise(objWQR001.WeldingPosition1, string.Empty, location).CategoryDescription;
                ViewBag.WeldingProcess2 = objModelWQRequest.GetCategoryDescriptionLocationWise(objWQR001.WeldingProcess2, string.Empty, location).CategoryDescription;
                ViewBag.WeldingType2 = objModelWQRequest.GetCategoryDescriptionLocationWise(objWQR001.WeldingType2, string.Empty, location).CategoryDescription;
                ViewBag.WeldingPosition2 = objModelWQRequest.GetCategoryDescriptionLocationWise(objWQR001.WeldingPosition2, string.Empty, location).CategoryDescription;
                ViewBag.JointType = objModelWQRequest.GetCategoryDescriptionLocationWise(objWQR001.Jointtype, string.Empty, location).CategoryDescription;
                ViewBag.CouponType = objModelWQRequest.GetCategoryDescriptionLocationWise(objWQR001.CouponType, string.Empty, location).CategoryDescription;
                ViewBag.ShopInCharge = db.COM003.Where(i => i.t_psno.Equals(objWQR001.ShopInCharge, StringComparison.OrdinalIgnoreCase) && i.t_actv == 1).Select(i => i.t_psno + "-" + i.t_name).FirstOrDefault();
                ViewBag.WE = db.COM003.Where(i => i.t_psno.Equals(objWQR001.WE, StringComparison.OrdinalIgnoreCase) && i.t_actv == 1).Select(i => i.t_psno + "-" + i.t_name).FirstOrDefault();
                ViewBag.WQTC = db.COM003.Where(i => i.t_psno.Equals(objWQR001.WQTC, StringComparison.OrdinalIgnoreCase) && i.t_actv == 1).Select(i => i.t_psno + "-" + i.t_name).FirstOrDefault();

                if (!string.IsNullOrWhiteSpace(objWQR001.QualificationProject))
                {
                    ViewBag.QualificationProject = db.COM001.Where(i => i.t_cprj.Equals(objWQR001.QualificationProject, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + "-" + i.t_dsca).FirstOrDefault();
                    objWQR001.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objWQR001.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                }
                else
                {
                    ViewBag.QualificationProject = "";
                    objWQR001.BU = "";
                }
            }
            else
            {
                //change by Ajay Chauhan On 16/1/2018 observation Id 14316
                //ViewBag.Location = db.COM002.Where(i => i.t_dtyp == 1 && i.t_dimx.Equals(location, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_dimx + "-" + i.t_desc).FirstOrDefault();

                //objWQR001.RequestNo = GetMaxRequestNo(location);

                objWQR001.Requeststatus = "Draft";
                //var generalProj = db.WQR008.Where(i => i.Location.Equals(location, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                //if (generalProj != null)
                //{
                //    objWQR001.GeneralProject = generalProj.Location + Convert.ToInt32(generalProj.WQRSeries).ToString("D4");
                //}
            }
            //change by Ajay Chauhan On 16/1/2018 observation Id 14316
            var locations = db.WQR008.ToList();
            ViewBag.lstLocation = locations.AsEnumerable().Select(x => new CategoryData { CategoryDescription = x.Location + "-" + x.Description, Value = x.Location }).ToList();


            List<CategoryData> lstJointType = objModelWQRequest.GetSubCatagory("Joint Type WQ", location, string.Empty);//.Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + " - " + i.Description }).Distinct().ToList();
            List<CategoryData> lstWeldingProcess = objModelWQRequest.GetSubCatagory("Welding Process", location, string.Empty);//.Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + " - " + i.Description }).Distinct().ToList();
            List<CategoryData> lstWeldingType = objModelWQRequest.GetSubCatagory("Welding Type", location, string.Empty);//.Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + " - " + i.Description }).Distinct().ToList();
            List<CategoryData> lstWeldingPosition = objModelWQRequest.GetSubCatagory("Welding Position", location, string.Empty);//.Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + " - " + i.Description }).Distinct().ToList();
            List<CategoryData> lstCoupanType = objModelWQRequest.GetSubCatagory("Coupan Type", location, string.Empty);//.Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + " - " + i.Description }).Distinct().ToList();
            objModelWQRequest.CoupanType = lstCoupanType.Any() ? lstCoupanType : null;
            objModelWQRequest.WeldingProcess = lstWeldingProcess.Any() ? lstWeldingProcess : null;
            objModelWQRequest.WeldingType = lstWeldingType.Any() ? lstWeldingType : null;
            objModelWQRequest.WeldingPosition = lstWeldingPosition.Any() ? lstWeldingPosition : null;
            objModelWQRequest.JointType = lstJointType.Any() ? lstJointType : null;

            ViewBag.UserRole = roleId;
            ViewBag.GlobalData = objModelWQRequest;

            return View(objWQR001);
        }
        public ActionResult getGeneralProject(string location)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var generalProj = db.WQR008.Where(i => i.Location.Equals(location, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                if (generalProj != null)
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.GeneralProject = generalProj.Project;// generalProj.Location + Convert.ToInt32(generalProj.WQRSeries).ToString("D4");
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SaveHeader(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            WQR001 objWQR001 = new WQR001();
            try
            {
                if (fc != null)
                {
                    int headerId = Convert.ToInt32(fc["HeaderId"]);
                    if (headerId > 0)
                    {
                        objWQR001 = db.WQR001.Where(i => i.HeaderId == headerId).FirstOrDefault();
                        objWQR001.QualificationProject = fc["QualificationProject"];
                        objWQR001.BU = fc["BU"].Split('-')[0];
                        objWQR001.Location = fc["Location"]; // change by Ajay Chauhan on 16-1-2018 observation id 14316
                        objWQR001.Jointtype = fc["JointType"];
                        objWQR001.WeldingProcess1 = fc["WeldingProcess1"];
                        objWQR001.WeldingType1 = fc["WeldingType1"];
                        objWQR001.WeldingPosition1 = fc["WeldingPosition1"];
                        objWQR001.PNo1 = fc["PNo1"];
                        objWQR001.FNo1 = fc["FNo1"];
                        objWQR001.WeldingProcess2 = fc["WeldingProcess2"];
                        objWQR001.WeldingType2 = fc["WeldingType2"];
                        objWQR001.WeldingPosition2 = fc["WeldingPosition2"];
                        objWQR001.PNo2 = fc["PNo2"];
                        objWQR001.FNo2 = fc["FNo2"];
                        objWQR001.CouponType = fc["CouponType"];
                        objWQR001.PipeOD = !string.IsNullOrEmpty(fc["PipeOD"]) ? Convert.ToInt32(fc["PipeOD"]) : 0;
                        objWQR001.BaseMetalThickness = !string.IsNullOrEmpty(fc["BaseMetalThickness"]) ? Convert.ToInt32(fc["BaseMetalThickness"]) : 0;
                        objWQR001.Requeststatus = clsImplementationEnum.WQRequestStatus.Draft.GetStringValue();

                        objWQR001.ShopInCharge = fc["ShopInCharge"];
                        objWQR001.ShopRemarks = fc["ShopRemarks"];
                        objWQR001.ShopInChargeRemarks = fc["ShopInChargeRemarks"];

                        objWQR001.WE = fc["WE"];
                        objWQR001.WERemarks = fc["WERemarks"];

                        objWQR001.WQTC = fc["WQTC"];

                        objWQR001.EditedBy = objClsLoginInfo.UserName;
                        objWQR001.EditedOn = DateTime.Now;

                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = string.Format("Request No.{0} Saved Sucessfully", objWQR001.RequestNo);
                        objResponseMsg.HeaderId = objWQR001.HeaderId;
                        objResponseMsg.HeaderStatus = objWQR001.Requeststatus;
                        objResponseMsg.RequestNo = objWQR001.RequestNo;
                    }
                    else
                    {
                        objWQR001.RequestNo = GetMaxRequestNo(fc["Location"]); // fc["RequestNo"];
                        objWQR001.GeneralProject = fc["GeneralProject"];
                        objWQR001.QualificationProject = fc["QualificationProject"];
                        objWQR001.BU = fc["BU"].Split('-')[0];
                        objWQR001.Location = fc["Location"]; // change by Ajay Chauhan on 16-1-2018 observation id 14316
                        objWQR001.Jointtype = fc["JointType"];
                        objWQR001.WeldingProcess1 = fc["WeldingProcess1"];
                        objWQR001.WeldingType1 = fc["WeldingType1"];
                        objWQR001.WeldingPosition1 = fc["WeldingPosition1"];
                        objWQR001.PNo1 = fc["PNo1"];
                        objWQR001.FNo1 = fc["FNo1"];
                        objWQR001.WeldingProcess2 = fc["WeldingProcess2"];
                        objWQR001.WeldingType2 = fc["WeldingType2"];
                        objWQR001.WeldingPosition2 = fc["WeldingPosition2"];
                        objWQR001.PNo2 = fc["PNo2"];
                        objWQR001.FNo2 = fc["FNo2"];
                        objWQR001.CouponType = fc["CouponType"];
                        objWQR001.PipeOD = !string.IsNullOrEmpty(fc["PipeOD"]) ? Convert.ToInt32(fc["PipeOD"]) : 0;
                        objWQR001.BaseMetalThickness = !string.IsNullOrEmpty(fc["BaseMetalThickness"]) ? Convert.ToInt32(fc["BaseMetalThickness"]) : 0;
                        objWQR001.Requeststatus = clsImplementationEnum.WQRequestStatus.Draft.GetStringValue();

                        objWQR001.ShopInCharge = fc["ShopInCharge"];
                        objWQR001.ShopRemarks = fc["ShopRemarks"];
                        objWQR001.ShopInChargeRemarks = fc["ShopInChargeRemarks"];

                        objWQR001.WE = fc["WE"];
                        objWQR001.WERemarks = fc["WERemarks"];

                        objWQR001.WQTC = fc["WQTC"];

                        objWQR001.MaintainedBy = objClsLoginInfo.UserName;
                        objWQR001.MaintainedOn = DateTime.Now;
                        objWQR001.CreatedBy = objClsLoginInfo.UserName;
                        objWQR001.CreatedOn = DateTime.Now;

                        db.WQR001.Add(objWQR001);
                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = string.Format("Request No.{0} Generated Sucessfully", objWQR001.RequestNo);
                        objResponseMsg.HeaderId = objWQR001.HeaderId;
                        objResponseMsg.HeaderStatus = objWQR001.Requeststatus;
                        objResponseMsg.RequestNo = objWQR001.RequestNo;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteWQRequest(int headerId)
        {
            WQR001 objWQR001 = new WQR001();
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (headerId > 0)
                {
                    objWQR001 = db.WQR001.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    if (objWQR001 != null)
                    {
                        if (db.WQR002.Where(i => i.HeaderId == headerId).Any())
                        {
                            db.WQR002.RemoveRange(db.WQR002.Where(i => i.HeaderId == headerId).ToList());
                        }
                        db.WQR001.Remove(objWQR001);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = string.Format("Request No. {0} Deleted Successfully", objWQR001.RequestNo);
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Request No Does not Exist";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region WQR Lines
        public ActionResult GetWQRLinesPartial(int headerId, int roleId)
        {
            WQR002 objWQR002 = new WQR002();
            objWQR002.HeaderId = headerId;
            ViewBag.HeaderStatus = db.WQR001.Where(i => i.HeaderId == headerId).FirstOrDefault().Requeststatus;
            ViewBag.RoleId = roleId;
            ViewBag.UserName = objClsLoginInfo.UserName;
            return PartialView("_GetWQRLinesPartial", objWQR002);
        }

        public ActionResult LoadWQRWelderData(JQueryDataTableParamModel param, int refheaderId, int roleId)
        {
            try
            {
                bool isInitiator = true;
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;

                string whereCondition = "1=1";
                //whereCondition += Manager.MakeDefaultLEMWhere(objClsLoginInfo.UserName, "wps10.BU", "wps10.Location");

                if (refheaderId > 0)
                    whereCondition += " and wqr2.HeaderId=" + refheaderId + " ";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " and (w.Stamp like '%" + param.sSearch + "%' or w.ShopCode like '%" + param.sSearch + "%' or  w.ShopCode+'-'+  com2.t_desc " + param.sSearch + " or wqr2.Welder like '%" + param.sSearch + "%' )";
                    //whereCondition += Helper.MakeDatatableSearchCondition(new string[] { "w.Stamp,w.ShopCode,wqr2.Welder" }, param.sSearch);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (roleId == RoleForWQ.ShoInCharge.GetHashCode())
                {
                    whereCondition += " AND (wqr2.Status IS Null OR wqr2.Status='" + string.Empty + "' OR wqr2.acceptedByShop=1)";
                }
                if (roleId == RoleForWQ.WE.GetHashCode())
                {
                    whereCondition += " AND (wqr2.acceptedByShop=1 And wqr2.Status Not In ('" + clsImplementationEnum.WQLinesStatus.RejectedByWeld.GetStringValue() + "') OR wqr2.acceptedByWeld=1 )";
                }
                if (roleId == RoleForWQ.ShoInCharge.GetHashCode() || roleId == RoleForWQ.WE.GetHashCode())
                {
                    isInitiator = false;
                }
                var lstHeader = db.SP_WQ_GET_WELDERDETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                int newRecordId = 0;
                var newRecord = new[] {
                                    Helper.GenerateHidden(newRecordId, "HeaderId", Convert.ToString(refheaderId)),
                                    Helper.GenerateHidden(newRecordId, "LineId", ""),
                                    "",
                                    HTMLAutoComplete(newRecordId,"txtWelder","","",false,"","Welder")+""+Helper.GenerateHidden(newRecordId,"Welder"),//Helper.GenerateTextbox(newRecordId, "StageCode"),
                                    GenerateLable(newRecordId,"Stamp"),
                                    GenerateLable(newRecordId,"ShopCode"),
                                    Helper.GenerateGridButtonNew(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveLines("+newRecordId+","+refheaderId+");" ),
                                };
                ModelWQRequest objModel = new ModelWQRequest();

                var res = (from h in lstHeader
                           select new[] {
                            Helper.GenerateHidden(h.LineId,"LineId",Convert.ToString(h.LineId)),
                            Helper.GenerateHidden(h.HeaderId,"HeaderId",Convert.ToString(h.HeaderId)),
                            Convert.ToString(h.ROW_NO),
                            objModel.GetWelderName(Convert.ToString(h.Welder),db.WQR001.Where(x=>x.HeaderId == h.HeaderId).Select(x=>x.Location).FirstOrDefault()).ToString(),
                            Convert.ToString(h.Stamp),
                            Convert.ToString(h.ShopCode),
                            isInitiator?(string.Equals(h.Requeststatus,clsImplementationEnum.WQRequestStatus.Draft.GetStringValue()) || string.Equals(h.Requeststatus,clsImplementationEnum.WQRequestStatus.Returned.GetStringValue() ))? "<center><i style='cursor:pointer;' class='fa fa-trash-o' title='Delete Welder' onClick='DeleteWQRWelder("+h.LineId+","+h.Welder+")'></i></center>":string.Empty:
                            //"<center><a onClick='ApproveWelder("+h.LineId+","+roleId+")'>Approve</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onClick='RejectWelder("+h.LineId+","+roleId+","+h.Welder+")'>Reject</a></center>" ,
                            "<center> <label class='mt-checkbox mt-checkbox-outline'> <input type='checkbox' class='clsCheckbox' name='chkAppRej' value='"+h.LineId+"'/> <span></span> </label></center>"
                    }).ToList();

                res.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ApproveWelder(int lineId, int roleId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            WQR002 objWQR002 = new WQR002();
            try
            {
                if (lineId > 0)
                {
                    objWQR002 = db.WQR002.Where(i => i.LineId == lineId).FirstOrDefault();
                    if (roleId == RoleForWQ.ShoInCharge.GetHashCode())
                    {
                        objWQR002.acceptedByShop = true;
                        objWQR002.EditedBy = objClsLoginInfo.UserName;
                        objWQR002.EditedOn = DateTime.Now;

                    }
                    if (roleId == RoleForWQ.WE.GetHashCode())
                    {
                        objWQR002.acceptedByWeld = true;
                        objWQR002.EditedBy = objClsLoginInfo.UserName;
                        objWQR002.EditedOn = DateTime.Now;

                    }
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = string.Format("Welder {0} Approved Successfully", objWQR002.Welder);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RejectWelder(int lineId, int roleId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            WQR002 objWQR002 = new WQR002();
            try
            {
                if (lineId > 0)
                {
                    objWQR002 = db.WQR002.Where(i => i.LineId == lineId).FirstOrDefault();
                    if (roleId == RoleForWQ.ShoInCharge.GetHashCode())
                    {
                        objWQR002.acceptedByShop = false;
                        objWQR002.Status = clsImplementationEnum.WQLinesStatus.RejectedByShop.GetStringValue();
                        objWQR002.EditedBy = objClsLoginInfo.UserName;
                        objWQR002.EditedOn = DateTime.Now;

                    }
                    if (roleId == RoleForWQ.WE.GetHashCode())
                    {
                        objWQR002.acceptedByWeld = false;
                        objWQR002.Status = clsImplementationEnum.WQLinesStatus.RejectedByWeld.GetStringValue();
                        objWQR002.EditedBy = objClsLoginInfo.UserName;
                        objWQR002.EditedOn = DateTime.Now;

                    }
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = string.Format("Welder {0} Rejected", objWQR002.Welder);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveLines(int lineId, int headerId, string welderPSNo)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            WQR002 objWQR002 = new WQR002();
            try
            {
                if (headerId > 0)
                {
                    //if (IsWelderExist(welderPSNo, headerId))
                    //{
                    //    objResponseMsg.Key = false;
                    //    objResponseMsg.Value = "Welder Already Exist";
                    //}
                    //else
                    {
                        if (lineId == 0)
                        {
                            objWQR002.HeaderId = headerId;
                            objWQR002.Welder = welderPSNo;
                            objWQR002.Status = string.Empty;
                            objWQR002.CreatedBy = objClsLoginInfo.UserName;
                            objWQR002.CreatedOn = DateTime.Now;

                            db.WQR002.Add(objWQR002);
                            db.SaveChanges();

                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "Welder Added Successfully";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg);
        }

        [HttpPost]
        public ActionResult DeleteWQRWelder(int lineId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            WQR002 objWQR002 = new WQR002();
            try
            {
                if (lineId > 0)
                {
                    objWQR002 = db.WQR002.Where(i => i.LineId == lineId).FirstOrDefault();
                    if (objWQR002 != null)
                    {
                        db.WQR002.Remove(objWQR002);
                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = string.Format("Welder {0} Deleted Successfully", objWQR002.Welder);
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Welder Doesn't Exist";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        [HttpPost]
        public ActionResult SendForApproval(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            WQR001 objWQR001 = new WQR001();
            try
            {
                if (headerId > 0)
                {
                    objWQR001 = db.WQR001.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    if (db.WQR002.Where(i => i.HeaderId == headerId).Any())
                    {
                        if (objWQR001 != null)
                        {
                            objWQR001.Requeststatus = clsImplementationEnum.WQRequestStatus.PendingWithShopInCharge.GetStringValue();
                            objWQR001.SubmittedBy = objClsLoginInfo.UserName;
                            objWQR001.SubmittedOn = DateTime.Now;
                            db.WQR002.Where(i => i.HeaderId == headerId).ToList().ForEach(i => i.Status = string.Empty);
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = string.Format("The request {0} is sent for approval successfully", objWQR001.RequestNo);
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = string.Format("No Welder Added For Request No. {0}.Please Add Welder", objWQR001.RequestNo);
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = true;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #region COPY WQR
        [HttpPost]
        public ActionResult CopyWQRequest(int headerId)
        {
            if (headerId > 0)
                ViewBag.SourceRequestNo = db.WQR001.Where(i => i.HeaderId == headerId).FirstOrDefault().RequestNo;
            return PartialView("_CopyWQRequest");
        }

        [HttpPost]
        public ActionResult CopyToDestination(string sourceReq, bool copyWelderDetail)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            WQR001 objWQR001Src = new WQR001();
            WQR001 objWQR001Dest = new WQR001();
            if (!string.IsNullOrEmpty(sourceReq))
            {
                objWQR001Src = db.WQR001.Where(i => i.RequestNo.Equals(sourceReq, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                if (objWQR001Src != null)
                {
                    objWQR001Dest = new WQR001();

                    objWQR001Dest.RequestNo = GetMaxRequestNo(objClsLoginInfo.Location);
                    objWQR001Dest.GeneralProject = objWQR001Src.GeneralProject;
                    objWQR001Dest.QualificationProject = objWQR001Src.QualificationProject;
                    objWQR001Dest.BU = objWQR001Src.BU;
                    objWQR001Dest.Location = objClsLoginInfo.Location;
                    objWQR001Dest.Jointtype = objWQR001Src.Jointtype;
                    objWQR001Dest.WeldingProcess1 = objWQR001Src.WeldingProcess1;
                    objWQR001Dest.WeldingType1 = objWQR001Src.WeldingType1;
                    objWQR001Dest.WeldingPosition1 = objWQR001Src.WeldingPosition1;
                    objWQR001Dest.PNo1 = objWQR001Src.PNo1;
                    objWQR001Dest.FNo1 = objWQR001Src.FNo1;
                    objWQR001Dest.WeldingProcess2 = objWQR001Src.WeldingProcess2;
                    objWQR001Dest.WeldingType2 = objWQR001Src.WeldingType2;
                    objWQR001Dest.WeldingPosition2 = objWQR001Src.WeldingPosition2;
                    objWQR001Dest.PNo2 = objWQR001Src.PNo2;
                    objWQR001Dest.FNo2 = objWQR001Src.FNo2;
                    objWQR001Dest.CouponType = objWQR001Src.CouponType;
                    objWQR001Dest.PipeOD = Convert.ToInt32(objWQR001Src.PipeOD);
                    objWQR001Dest.BaseMetalThickness = Convert.ToInt32(objWQR001Src.BaseMetalThickness);
                    objWQR001Dest.Requeststatus = clsImplementationEnum.WQRequestStatus.Draft.GetStringValue();

                    //objWQR001Dest.ShopRemarks = objWQR001Src.ShopRemarks;
                    objWQR001Dest.ShopInCharge = objWQR001Src.ShopInCharge;
                    //objWQR001Dest.ShopInChargeRemarks = objWQR001Src.ShopInChargeRemarks;

                    objWQR001Dest.WE = objWQR001Src.WE;
                    //objWQR001Dest.WERemarks = objWQR001Src.WERemarks;

                    objWQR001Dest.WQTC = objWQR001Src.WQTC;

                    objWQR001Dest.CreatedBy = objClsLoginInfo.UserName;
                    objWQR001Dest.CreatedOn = DateTime.Now;

                    db.WQR001.Add(objWQR001Dest);
                    db.SaveChanges();
                    bool withoutLines = false;
                    if (copyWelderDetail)
                    {
                        var lstWelders = db.WQR002.Where(i => i.HeaderId == objWQR001Src.HeaderId).ToList();
                        if (lstWelders.Any())
                        {
                            db.WQR002.AddRange(lstWelders.Select(i => new WQR002
                            {
                                HeaderId = objWQR001Dest.HeaderId,
                                Welder = i.Welder,
                                Status = string.Empty,
                                CreatedBy = objClsLoginInfo.UserName,
                                CreatedOn = DateTime.Now
                            }));
                            db.SaveChanges();
                        }
                        else
                        {
                            withoutLines = true;
                            //objResponseMsg.WithoutLines= string.Format("Request No.{0} Generated Sucessfully", objWQR001Dest.RequestNo);
                        }
                    }
                    string msg = string.Empty;
                    if (copyWelderDetail == true && withoutLines == true)
                    {
                        msg += "New Request No. {0} Is Generated Sucessfully Without Lines";
                    }
                    else
                    {
                        msg += "New Request No. {0} Is Generated Successfully";
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = string.Format(msg, objWQR001Dest.RequestNo);
                    objResponseMsg.HeaderId = objWQR001Dest.HeaderId;
                    objResponseMsg.HeaderStatus = objWQR001Dest.Requeststatus;

                }
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSourceRequestNoForCopy(string term, string location)
        {
            List<CategoryData> lstCategoryData = new List<CategoryData>();
            if (!string.IsNullOrEmpty(term))
            {
                lstCategoryData = db.WQR001.Where(i => i.Location.Equals(location, StringComparison.OrdinalIgnoreCase)
                                                  && i.RequestNo.ToLower().Contains(term.ToLower()))
                                                .Select(i => new CategoryData { Code = i.RequestNo }).ToList();
            }
            else
            {
                lstCategoryData = db.WQR001.Where(i => i.Location.Equals(location, StringComparison.OrdinalIgnoreCase))
                                                .Select(i => new CategoryData { Code = i.RequestNo }).Take(10).ToList();
            }
            return Json(lstCategoryData, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Approve Or Return WQR
        [HttpPost]
        public ActionResult ApproveWQRequest_Old(string strHeaderIds, int roleId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            List<string> lstSendToWE = new List<string>();
            List<string> lstApproved = new List<string>();
            List<string> lstSkipped = new List<string>();
            bool approveWQR = false;
            try
            {
                if (!string.IsNullOrEmpty(strHeaderIds))
                {
                    int[] headerIds = Array.ConvertAll(strHeaderIds.Split(','), i => Convert.ToInt32(i));
                    string statusShop = clsImplementationEnum.WQLinesStatus.RejectedByShop.GetStringValue();
                    string statusWE = clsImplementationEnum.WQLinesStatus.RejectedByShop.GetStringValue();
                    foreach (var headerId in headerIds)
                    {
                        WQR001 objWQR001 = db.WQR001.Where(i => i.HeaderId == headerId).FirstOrDefault();
                        if (db.WQR002.Where(i => i.HeaderId == headerId && (!i.Status.Equals(statusShop, StringComparison.OrdinalIgnoreCase) || !i.Status.Equals(statusWE, StringComparison.OrdinalIgnoreCase))).Any())
                        {
                            if (!string.IsNullOrEmpty(objWQR001.QualificationProject))
                            {
                                if (roleId == RoleForWQ.ShoInCharge.GetHashCode())
                                {
                                    objWQR001.Requeststatus = clsImplementationEnum.WQRequestStatus.PendingWithWE.GetStringValue();
                                    if (db.WQR002.Where(i => i.HeaderId == headerId && (i.Status == "" || i.acceptedByWeld == true)).Any())
                                    {
                                        db.WQR002.Where(i => i.HeaderId == headerId && (i.Status == "" || i.acceptedByShop == true)).ToList().ForEach(i => { i.acceptedByShop = true; i.EditedBy = objClsLoginInfo.UserName; i.EditedOn = DateTime.Now; });
                                    }
                                    objWQR001.ShopInChargeOn = DateTime.Now;
                                    db.SaveChanges();
                                    lstSendToWE.Add(objWQR001.RequestNo);
                                }
                                if (roleId == RoleForWQ.WE.GetHashCode())
                                {
                                    objWQR001.Requeststatus = clsImplementationEnum.WQRequestStatus.Approved.GetStringValue();
                                    if (db.WQR002.Where(i => i.HeaderId == headerId && (i.Status == "" || i.acceptedByWeld == true)).Any())
                                    {
                                        db.WQR002.Where(i => i.HeaderId == headerId && (i.Status == "" || i.acceptedByWeld == true)).ToList().ForEach(i => { i.acceptedByWeld = true; i.Status = clsImplementationEnum.WQLinesStatus.Accepted.GetStringValue(); ; i.EditedBy = objClsLoginInfo.UserName; i.EditedOn = DateTime.Now; });
                                    }
                                    objWQR001.WEOn = DateTime.Now;
                                    db.SaveChanges();
                                    approveWQR = SendTOWQTC(objWQR001, db.WQR002.Where(i => i.HeaderId == headerId && (i.Status == "" || i.acceptedByWeld == true)).ToList());
                                    lstApproved.Add(objWQR001.RequestNo);
                                }
                            }
                            else
                            {
                                objWQR001.Requeststatus = clsImplementationEnum.WQRequestStatus.Approved.GetStringValue();
                                if (db.WQR002.Where(i => i.HeaderId == headerId && (i.Status == "" || i.acceptedByWeld == true)).Any())
                                {
                                    db.WQR002.Where(i => i.HeaderId == headerId && (i.Status == "" || i.acceptedByWeld == true)).ToList().ForEach(i => { i.Status = clsImplementationEnum.WQLinesStatus.Accepted.GetStringValue(); });
                                }
                                db.SaveChanges();
                                approveWQR = SendTOWQTC(objWQR001, db.WQR002.Where(i => i.HeaderId == headerId && (i.Status == "" || i.acceptedByWeld == true)).ToList());
                                lstApproved.Add(objWQR001.RequestNo);
                                //lstSentToWQTC.Add(objWQR001.RequestNo);
                            }
                            //db.SaveChanges();
                            //lstApproved.Add(objWQR001.RequestNo);
                        }
                        else
                        {
                            lstSkipped.Add(objWQR001.RequestNo);
                        }
                    }


                    string strMsg = string.Empty;
                    string warning = string.Empty;

                    if (lstSendToWE.Any())
                    {
                        strMsg += "Request No.(s) " + string.Join(",", lstSendToWE) + " Sent For Approval Successfully";
                    }
                    if (lstApproved.Any())
                    {
                        strMsg += "And Request No.(s) " + string.Join(",", lstApproved) + " Approved Successfully";
                    }
                    if (lstSkipped.Any())
                    {
                        warning += "Request No.(s) " + string.Join(",", lstSkipped) + " can not be approved.Please Add Welders.";
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = strMsg;//strMsg;
                    objResponseMsg.WithoutLines = warning;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApproveWQRequest(string strHeaderIds, int roleId, string strlineIds = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            List<string> lstSendToWE = new List<string>();
            List<string> lstApproved = new List<string>();
            List<string> lstSkipped = new List<string>();
            bool approveWQR = false;
            try
            {
                if (!string.IsNullOrEmpty(strHeaderIds))
                {
                    int[] headerIds = Array.ConvertAll(strHeaderIds.Split(','), i => Convert.ToInt32(i));
                    int[] lineIds = !string.IsNullOrEmpty(strlineIds) ? Array.ConvertAll(strlineIds.Split(','), i => Convert.ToInt32(i)) : new int[0];
                    string statusShop = clsImplementationEnum.WQLinesStatus.RejectedByShop.GetStringValue();
                    string statusWE = clsImplementationEnum.WQLinesStatus.RejectedByShop.GetStringValue();
                    foreach (var headerId in headerIds)
                    {
                        WQR001 objWQR001 = db.WQR001.Where(i => i.HeaderId == headerId).FirstOrDefault();
                        if (db.WQR002.Where(i => i.HeaderId == headerId && (!i.Status.Equals(statusShop, StringComparison.OrdinalIgnoreCase) || !i.Status.Equals(statusWE, StringComparison.OrdinalIgnoreCase))).Any())
                        {
                            if (!string.IsNullOrEmpty(objWQR001.QualificationProject))
                            {
                                if (roleId == RoleForWQ.ShoInCharge.GetHashCode())
                                {
                                    objWQR001.Requeststatus = clsImplementationEnum.WQRequestStatus.PendingWithWE.GetStringValue();
                                    if (lineIds.ToList().Any())
                                    {
                                        db.WQR002.Where(i => i.HeaderId == headerId && lineIds.Contains(i.LineId)).ToList().ForEach(i => { i.acceptedByShop = true; ; i.EditedBy = objClsLoginInfo.UserName; i.EditedOn = DateTime.Now; });
                                        db.WQR002.Where(i => i.HeaderId == headerId && !lineIds.ToList().Contains(i.LineId)).ToList().ForEach(i => { i.acceptedByShop = false; i.Status = statusShop; ; i.EditedBy = objClsLoginInfo.UserName; i.EditedOn = DateTime.Now; });
                                    }
                                    else
                                    {
                                        db.WQR002.Where(i => i.HeaderId == headerId && (i.Status == "" || i.acceptedByShop == true)).ToList().ForEach(i => { i.acceptedByShop = true; ; i.EditedBy = objClsLoginInfo.UserName; i.EditedOn = DateTime.Now; });
                                    }
                                    //if (db.WQR002.Where(i => i.HeaderId == headerId && (i.Status == "" || i.acceptedByWeld == true)).Any())
                                    //{
                                    //    db.WQR002.Where(i => i.HeaderId == headerId && (i.Status == "" || i.acceptedByShop == true)).ToList().ForEach(i => { i.acceptedByShop = true; });
                                    //}
                                    objWQR001.ShopInChargeOn = DateTime.Now;
                                    db.SaveChanges();
                                    lstSendToWE.Add(objWQR001.RequestNo);
                                }
                                if (roleId == RoleForWQ.WE.GetHashCode())
                                {
                                    objWQR001.Requeststatus = clsImplementationEnum.WQRequestStatus.Approved.GetStringValue();

                                    if (lineIds.ToList().Any())
                                    {
                                        db.WQR002.Where(i => i.HeaderId == headerId && lineIds.Contains(i.LineId)).ToList().ForEach(i => { i.acceptedByWeld = true; ; i.EditedBy = objClsLoginInfo.UserName; i.EditedOn = DateTime.Now; });
                                        var lines = db.WQR002.Where(i => i.HeaderId == headerId && !lineIds.ToList().Contains(i.LineId)).ToList();
                                        if (lines.Any())
                                        {
                                            lines.ForEach(i => { i.acceptedByWeld = false; i.Status = statusWE; ; i.EditedBy = objClsLoginInfo.UserName; i.EditedOn = DateTime.Now; });
                                        }
                                        else
                                        {
                                            db.WQR002.Where(i => i.HeaderId == headerId && lineIds.Contains(i.LineId)).ToList().ForEach(i => { i.Status = clsImplementationEnum.WQLinesStatus.Accepted.GetStringValue(); ; i.EditedBy = objClsLoginInfo.UserName; i.EditedOn = DateTime.Now; });
                                        }
                                        //.ForEach(i => { i.acceptedByWeld = false; i.Status = statusWE; });
                                    }
                                    else
                                    {
                                        db.WQR002.Where(i => i.HeaderId == headerId && (i.Status == "" || i.acceptedByWeld == true)).ToList().ForEach(i => { i.acceptedByWeld = true; ; i.Status = clsImplementationEnum.WQLinesStatus.Accepted.GetStringValue(); ; i.EditedBy = objClsLoginInfo.UserName; i.EditedOn = DateTime.Now; });
                                    }

                                    //if (db.WQR002.Where(i => i.HeaderId == headerId && (i.Status == "" || i.acceptedByWeld == true)).Any())
                                    //{
                                    //    db.WQR002.Where(i => i.HeaderId == headerId && (i.Status == "" || i.acceptedByWeld == true)).ToList().ForEach(i => { i.acceptedByWeld = true; i.Status = clsImplementationEnum.WQLinesStatus.Accepted.GetStringValue(); });
                                    //}
                                    objWQR001.WEOn = DateTime.Now;
                                    db.SaveChanges();
                                    approveWQR = SendTOWQTC(objWQR001, db.WQR002.Where(i => i.HeaderId == headerId && (i.Status == "" || i.acceptedByWeld == true)).ToList());
                                    lstApproved.Add(objWQR001.RequestNo);
                                }
                            }
                            else
                            {
                                objWQR001.Requeststatus = clsImplementationEnum.WQRequestStatus.Approved.GetStringValue();
                                //if (lineIds.Any())
                                //{
                                //    db.WQR002.Where(i => i.HeaderId == headerId && lineIds.Contains(i.LineId) && (i.Status == "" || i.acceptedByShop == true)).ToList().ForEach(i => { i.acceptedByShop = true; });
                                //    db.WQR002.Where(i => i.HeaderId == headerId && !lineIds.Contains(i.LineId)).ToList().ForEach(i => { i.acceptedByShop = false; });
                                //}
                                //else
                                {
                                    db.WQR002.Where(i => i.HeaderId == headerId).ToList().ForEach(i => { i.acceptedByShop = true; ; i.EditedBy = objClsLoginInfo.UserName; i.EditedOn = DateTime.Now; });
                                }

                                //if (db.WQR002.Where(i => i.HeaderId == headerId && (i.Status == "" || i.acceptedByWeld == true)).Any())
                                //{
                                //    db.WQR002.Where(i => i.HeaderId == headerId && (i.Status == "" || i.acceptedByWeld == true)).ToList().ForEach(i => { i.Status = clsImplementationEnum.WQLinesStatus.Accepted.GetStringValue(); });
                                //}
                                db.SaveChanges();
                                approveWQR = SendTOWQTC(objWQR001, db.WQR002.Where(i => i.HeaderId == headerId && (i.Status == "" || i.acceptedByWeld == true)).ToList());
                                lstApproved.Add(objWQR001.RequestNo);
                                //lstSentToWQTC.Add(objWQR001.RequestNo);
                            }
                            //db.SaveChanges();
                            //lstApproved.Add(objWQR001.RequestNo);
                        }
                        else
                        {
                            lstSkipped.Add(objWQR001.RequestNo);
                        }
                    }

                    string strMsg = string.Empty;
                    string warning = string.Empty;

                    if (lstSendToWE.Any())
                    {
                        strMsg += "Request No.(s) " + string.Join(",", lstSendToWE) + " Sent For Approval Successfully";
                    }
                    if (lstApproved.Any())
                    {
                        strMsg += "And Request No.(s) " + string.Join(",", lstApproved) + " Approved Successfully";
                    }
                    if (lstSkipped.Any())
                    {
                        warning += "Request No.(s) " + string.Join(",", lstSkipped) + " cannot be approved.Please Add Welders.";
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = strMsg;//strMsg;
                    objResponseMsg.WithoutLines = warning;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public bool SendTOWQTC(WQR001 objWQR001, List<WQR002> lstWQR002)
        {
            bool saved = false;
            WQR003 objWQR003 = new WQR003();
            List<WQR003> lstWQR003 = new List<WQR003>();
            if (objWQR001 != null && lstWQR002.Any())
            {
                if (!string.IsNullOrEmpty(objWQR001.WeldingProcess1))
                {
                    foreach (var item in lstWQR002)
                    {
                        objWQR003 = new WQR003();
                        objWQR003.RequestNo = objWQR001.RequestNo;
                        objWQR003.LineId = item.LineId;
                        objWQR003.Welder = item.Welder;
                        objWQR003.WelderStamp = new ModelWQRequest().GetIntExtWelders(objWQR001.Location).Where(i => i.WelderPSNo.Equals(item.Welder, StringComparison.OrdinalIgnoreCase)).FirstOrDefault().Stamp;
                        objWQR003.QualificationProject = objWQR001.QualificationProject;
                        objWQR003.WeldingProcess = objWQR001.WeldingProcess1;
                        objWQR003.WeldType = objWQR001.WeldingType1;
                        objWQR003.PNo = objWQR001.PNo1;
                        objWQR003.FNo = objWQR001.FNo1;
                        objWQR003.JointType = objWQR001.Jointtype;
                        objWQR003.CreatedBy = objClsLoginInfo.UserName;
                        objWQR003.CreatedOn = DateTime.Now;
                        objWQR003.Location = objWQR001.Location;
                        lstWQR003.Add(objWQR003);
                    }
                }
                if (!string.IsNullOrEmpty(objWQR001.WeldingProcess2))
                {
                    foreach (var item in lstWQR002)
                    {
                        objWQR003 = new WQR003();
                        objWQR003.RequestNo = objWQR001.RequestNo;
                        objWQR003.LineId = item.LineId;
                        objWQR003.Welder = item.Welder;
                        objWQR003.WelderStamp = new ModelWQRequest().GetIntExtWelders(objWQR001.Location).Where(i => i.WelderPSNo.Equals(item.Welder, StringComparison.OrdinalIgnoreCase)).FirstOrDefault().Stamp;
                        objWQR003.QualificationProject = objWQR001.QualificationProject;
                        objWQR003.JointType = objWQR001.Jointtype;
                        objWQR003.WeldingProcess = objWQR001.WeldingProcess2;
                        objWQR003.WeldType = objWQR001.WeldingType2;
                        objWQR003.PNo = objWQR001.PNo2;
                        objWQR003.FNo = objWQR001.FNo2;
                        objWQR003.CreatedBy = objClsLoginInfo.UserName;
                        objWQR003.CreatedOn = DateTime.Now;
                        objWQR003.Location = objWQR001.Location;
                        lstWQR003.Add(objWQR003);
                    }
                }

                if (lstWQR003.Any())
                {
                    db.WQR003.AddRange(lstWQR003);
                    db.SaveChanges();
                    saved = true;
                }
            }
            return saved;
        }

        [HttpPost]
        public ActionResult ApproveRequest(int headerIds, int roleId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            WQR001 objWQR001 = new WQR001();
            string lineStatus = string.Empty;
            if (headerIds > 0)
            {
                objWQR001 = db.WQR001.Where(i => i.HeaderId == headerIds).FirstOrDefault();

                if (!string.IsNullOrEmpty(objWQR001.QualificationProject))
                {
                    if (roleId == RoleForWQ.ShoInCharge.GetHashCode())
                    {
                        objWQR001.Requeststatus = clsImplementationEnum.WQRequestStatus.PendingWithWE.GetStringValue();
                        lineStatus = clsImplementationEnum.WQLinesStatus.RejectedByShop.GetStringValue();
                    }
                    if (roleId == RoleForWQ.WE.GetHashCode())
                    {
                        objWQR001.Requeststatus = clsImplementationEnum.WQRequestStatus.Returned.GetStringValue();
                        lineStatus = clsImplementationEnum.WQLinesStatus.RejectedByWeld.GetStringValue();
                    }

                    //objWQR001.Requeststatus = clsImplementationEnum.WQRequestStatus.PendingWithWE.GetStringValue();
                    if (db.WQR002.Where(i => i.HeaderId == headerIds).Any())
                    {
                        db.WQR002.Where(i => i.HeaderId == headerIds).ToList().ForEach(i => { i.acceptedByShop = true; });
                    }
                    db.SaveChanges();
                }
                else
                {
                    objWQR001.Requeststatus = clsImplementationEnum.WQRequestStatus.Approved.GetStringValue();
                    if (db.WQR002.Where(i => i.HeaderId == headerIds).Any())
                    {
                        db.WQR002.Where(i => i.HeaderId == headerIds).ToList().ForEach(i => { i.Status = clsImplementationEnum.WQLinesStatus.Accepted.GetStringValue(); });
                    }
                    db.SaveChanges();
                }

            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReturnRequest(int headerId, string returnRemarks, int roleId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            WQR001 objWQR001 = new WQR001();
            string lineStatus = string.Empty;
            try
            {
                if (headerId > 0)
                {
                    objWQR001 = db.WQR001.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    if (objWQR001 != null)
                    {
                        if (roleId == RoleForWQ.ShoInCharge.GetHashCode())
                        {
                            objWQR001.Requeststatus = clsImplementationEnum.WQRequestStatus.Returned.GetStringValue();
                            objWQR001.ShopInChargeRemarks = returnRemarks;
                            objWQR001.ShopInChargeReturnOn = DateTime.Now;
                            lineStatus = clsImplementationEnum.WQLinesStatus.RejectedByShop.GetStringValue();
                        }
                        if (roleId == RoleForWQ.WE.GetHashCode())
                        {
                            objWQR001.Requeststatus = clsImplementationEnum.WQRequestStatus.Returned.GetStringValue();
                            objWQR001.WERemarks = returnRemarks;
                            objWQR001.WEReturnOn = DateTime.Now;
                            lineStatus = clsImplementationEnum.WQLinesStatus.RejectedByWeld.GetStringValue();
                        }

                        if (db.WQR002.Where(i => i.HeaderId == headerId).Any())
                        {
                            db.WQR002.Where(i => i.HeaderId == headerId).ToList().ForEach(i => { i.Status = lineStatus; });
                        }
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = string.Format("Request No. {0} is Return successfully", objWQR001.RequestNo);
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region WQR Commmon Functions
        [HttpPost]
        public ActionResult GetProjectWiseBuCategory(string qualificationProject, string location, int headerId)
        {
            ModelWQRequest objModelWQRequest = new ModelWQRequest();
            string strBu = string.Empty;

            //if (db.WQR001.Where(i => i.QualificationProject.Equals(qualificationProject, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && i.HeaderId != headerId).Any())
            //{
            //    objModelWQRequest.isheaderExist = true;
            //}

            PurchaseDetail objPurchaseDetail = new PurchaseDetail();
            if (!string.IsNullOrEmpty(qualificationProject))
            {
                objPurchaseDetail = Manager.getProjectWiseBULocation(qualificationProject);
                strBu = objPurchaseDetail.QCPBU.Split('-')[0];
            }

            //List<CategoryData> lstJointType = Manager.GetSubCatagories("Joint Type WQ", strBu, location).Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
            //List<CategoryData> lstWeldingProcess = Manager.GetSubCatagories("Welding Process", strBu, location).Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
            //List<CategoryData> lstWeldingType = Manager.GetSubCatagories("Welding Type", strBu, location).Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
            //List<CategoryData> lstWeldingPosition = Manager.GetSubCatagories("Welding Position", strBu, location).Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
            //List<CategoryData> lstCoupanType = Manager.GetSubCatagories("Coupan Type", strBu, location).Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();

            objModelWQRequest.BU = objPurchaseDetail.QCPBU;

            var generalProj = db.WQR008.Where(i => i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && i.BU.Equals(strBu, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
            if (generalProj != null)
            {
                objModelWQRequest.GeneralProject = generalProj.Project;// generalProj.Location + Convert.ToInt32(generalProj.WQRSeries).ToString("D4");
            }
            //objModelWQRequest.CoupanType = lstCoupanType.Any() ? lstCoupanType : null;
            //objModelWQRequest.WeldingProcess = lstWeldingProcess.Any() ? lstWeldingProcess : null;
            //objModelWQRequest.WeldingType = lstWeldingType.Any() ? lstWeldingType : null;
            //objModelWQRequest.WeldingPosition = lstWeldingPosition.Any() ? lstWeldingPosition : null;
            //objModelWQRequest.JointType = lstJointType.Any() ? lstJointType : null;

            return Json(objModelWQRequest, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetPNO(string term)
        {
            List<CategoryData> lstPNo = new List<CategoryData>();
            if (!string.IsNullOrEmpty(term))
            {
                lstPNo = db.WPS005.Where(i => i.PNO1.ToLower().Contains(term.ToLower())).Select(i => new CategoryData { Value = i.PNO1 }).ToList();
            }
            else
            {
                lstPNo = db.WPS005.Select(i => new CategoryData { Value = i.PNO1 }).Take(10).ToList();
            }
            return Json(lstPNo, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetFNO(string term, string location)
        {
            location = objClsLoginInfo.Location;
            List<CategoryData> lstFNo = new List<CategoryData>();
            if (!string.IsNullOrEmpty(term))
            {
                lstFNo = db.WPS002.Where(i => i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && i.FNumber.ToLower().Contains(term.ToLower())).Distinct().Select(i => new CategoryData { Value = i.FNumber }).ToList();
            }
            else
            {
                lstFNo = db.WPS002.Where(i => i.Location.Equals(location, StringComparison.OrdinalIgnoreCase)).Distinct().Select(i => new CategoryData { Value = i.FNumber }).Take(10).ToList();
            }

            return Json(lstFNo, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetRoleAndLocWiseEmployee(string term, string role, string location)
        {
            location = objClsLoginInfo.Location;
            List<ApproverModel> lstApprovers = new List<ApproverModel>();
            if (!string.IsNullOrEmpty(term))
            {
                lstApprovers = Manager.GetApproverList(role, location, objClsLoginInfo.UserName, term).Distinct().ToList();
            }
            else
            {
                lstApprovers = Manager.GetApproverList(role, location, objClsLoginInfo.UserName, term).Distinct().Take(10).ToList();
            }
            //return Json(lstApprovers, JsonRequestBehavior.AllowGet);
            return Json(lstApprovers, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetIntExtWelderDetail(string term, string location)
        {

            List<ApproverModel> lstApproverModel = new List<ApproverModel>();
            if (!string.IsNullOrEmpty(term))
            {
                //lstApproverModel = (from a in db.QMS003
                //                 where a.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && (a.WelderPSNo.ToLower().Contains(term.ToLower()))
                //                 select new ApproverModel { Code = a.WelderPSNo }
                //                 )
                //                 .Union(from b in db.QMS004
                //                        where b.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && (b.WelderPSNo.ToLower().Contains(term.ToLower()))
                //                        select new ApproverModel { Code = b.WelderPSNo }).ToList();

                lstApproverModel = new ModelWQRequest().GetIntExtWelders(location).Where(i => i.WelderPSNo.ToLower().Contains(term.ToLower())).Select(i => new ApproverModel { Code = i.WelderPSNo, Name = i.WelderPSNo + "-" + i.WelderName }).ToList();
            }
            else
            {
                lstApproverModel = new ModelWQRequest().GetIntExtWelders(location).Where(i => i.WelderPSNo.ToLower().Contains(term.ToLower())).Select(i => new ApproverModel { Code = i.WelderPSNo, Name = i.WelderPSNo + "-" + i.WelderName }).Take(10).ToList();
            }

            return Json(lstApproverModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetStampAndShopCodeForWelder(string welderPSNO, string location, int headerId)
        {

            ModelWelderDetail objModelWelderDetail = new ModelWelderDetail();
            if (!string.IsNullOrEmpty(welderPSNO))
            {
                if (IsWelderExist(welderPSNO, headerId))
                {
                    objModelWelderDetail.IsWelderExist = true;
                }
                var welderDetail = new ModelWQRequest().GetIntExtWelders(location).Where(i => i.WelderPSNo.Equals(welderPSNO, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                objModelWelderDetail.Stamp = welderDetail.Stamp;
                objModelWelderDetail.ShopCode = welderDetail.ShopCode;
            }

            return Json(objModelWelderDetail, JsonRequestBehavior.AllowGet);
        }

        public bool IsWelderExist(string welderPSNO, int headerId)
        {
            bool IsWelderExist = false;
            if (db.WQR002.Where(i => i.HeaderId == headerId && i.Welder.Equals(welderPSNO, StringComparison.OrdinalIgnoreCase)).Any())
                IsWelderExist = true;
            return IsWelderExist;
        }

        public string GetMaxRequestNo(string location)
        {
            string strRequestNo = string.Empty;
            var refc = db.WQR001.Where(i => i.Location.Equals(location, StringComparison.OrdinalIgnoreCase)).ToList();
            if (refc.Any())
            {
                var reqNo = refc.OrderByDescending(i => i.HeaderId).FirstOrDefault();
                int subloc = Convert.ToInt32(reqNo.RequestNo.Substring(location.Length + 1, (reqNo.RequestNo.Length - location.Length) - 1)) + 1;//reqNo.Substring("HZW".Length+1, reqNo.Length);
                strRequestNo = location + (subloc).ToString("D7");
                //string output = (reqNo.RequestNo.ToLower()).Contains(location.ToLower());//(0, reqNo.IndexOf("-")).Replace(" ", "");
            }
            else
            {
                int num = 1;
                strRequestNo = location + num.ToString("D7");
            }
            return strRequestNo;
        }

        public string HTMLAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string maxlength = "", string validate = "")
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string FieldValidate = !string.IsNullOrEmpty(validate) ? "validate='" + validate + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onchange='" + onBlurMethod + "'" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' hdElement='" + hdElementId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + (disabled ? "disabled" : "") + " " + MaxCharcters + " " + FieldValidate + " />";

            return strAutoComplete;
        }

        public string GenerateLable(int rowId, string columnName, string columnValue = "", string inputStyle = "")
        {
            string strlbl = string.Empty;
            string lblID = columnName + "" + rowId.ToString();
            //string inputValue = columnValue;
            strlbl = "<label id='" + lblID + "'></label>";
            return strlbl;
        }
        #endregion

        #region Export Excel
        //Code By : nikita vibhandik 18/12/2017 (observation  14096)
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                //initiator header
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_WQ_GET_REQUESTHEADER(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from h in lst
                                  select new
                                  {
                                      RequestNo = h.RequestNo,
                                      GeneralProject = h.GeneralProject,
                                      QualificationProject = h.QualificationProject,
                                      Requeststatus = h.Requeststatus,
                                      CreatedBy = h.CreatedBy,
                                      CreatedOn = Convert.ToDateTime(h.CreatedOn).ToString("dd/MM/yyyy"),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                //grid for Index of shop in charge person
                else if (gridType == clsImplementationEnum.GridType.ShopInChargeHeader.GetStringValue())
                {
                    var lst = db.SP_WQ_GET_REQUESTHEADER(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from h in lst
                                  select new
                                  {
                                      RequestNo = h.RequestNo,
                                      GeneralProject = h.GeneralProject,
                                      QualificationProject = h.QualificationProject,
                                      Requeststatus = h.Requeststatus,
                                      SubmittedBy = h.SubmittedBy,
                                      SubmittedOn = Convert.ToDateTime(h.SubmittedOn).ToString("dd/MM/yyyy"),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                //grid for Index of Welder person
                else if (gridType == clsImplementationEnum.GridType.WEHeader.GetStringValue())
                {
                    var lst = db.SP_WQ_GET_REQUESTHEADER(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from h in lst
                                  select new
                                  {
                                      RequestNo = h.RequestNo,
                                      GeneralProject = h.GeneralProject,
                                      QualificationProject = h.QualificationProject,
                                      Requeststatus = h.Requeststatus,
                                      ShopInCharge = h.ShopInCharge,
                                      ShopInChargeOn = Convert.ToDateTime(h.ShopInChargeOn).ToString("dd/MM/yyyy"),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {

                    ModelWQRequest objModel = new ModelWQRequest();

                    var lst = db.SP_WQ_GET_WELDERDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from h in lst
                                  select new
                                  {
                                      ROW_NO = Convert.ToString(h.ROW_NO),
                                      Welder = objModel.GetWelderName(Convert.ToString(h.Welder), db.WQR001.Where(x => x.HeaderId == h.HeaderId).Select(x => x.Location).FirstOrDefault()).ToString(),
                                      Stamp = Convert.ToString(h.Stamp),
                                      ShopCode = Convert.ToString(h.ShopCode),

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }

}