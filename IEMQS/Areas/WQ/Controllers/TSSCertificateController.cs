﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.WQ.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace IEMQS.Areas.WQ.Controllers
{

    public class TSSCertificateController : clsBase
    {
        [SessionExpireFilter]
        [AllowAnonymous]
        //[UserPermissions]
        #region WPQ Certificate

        public ActionResult TSSCertificateList()
        {
            ViewBag.IsDisplayOnly = false;
            return View();
        }

        public ActionResult DisplayTSSCertificateList()
        {
            ViewBag.IsDisplayOnly = true;
            return View("TSSCertificateList");
        }

        public ActionResult TSSCertificate_old(int? id)
        {
            ViewBag.Id = id;
            WQR006 objWQR006 = new WQR006();
            if (id > 0)
            {
                string WQTNo = db.WQR003.Where(i => i.Id == id).FirstOrDefault().WQTNo;
                //ViewBag.WQTNo = WQTNo;
                objWQR006 = db.WQR006.Where(i => i.WQTNo == WQTNo).FirstOrDefault();
                if (objWQR006 == null)
                {
                    objWQR006 = new WQR006();
                    ViewBag.CertId = objWQR006.Id;
                    objWQR006.WQTNo = WQTNo;
                }
                else
                {
                    ViewBag.CertId = objWQR006.Id;
                    objWQR006.WQTNo = WQTNo;
                }
            }
            else
            {
                ViewBag.CertId = 0;
                ViewBag.WQTNo = string.Empty;
            }
            return View(objWQR006);
        }

        [SessionExpireFilter]
        [AllowAnonymous]
        //[UserPermissions]
        #region WPQ Certificate
        public ActionResult IsWQTNoExists(string wqtNo, string location)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var wpqCertificate = false;
                var wopqCertificate = false;
                var ttsCertificate = false;

                wpqCertificate = db.WQR004.Any(x => x.WQTNo == wqtNo && x.Location == location);
                wopqCertificate = db.WQR005.Any(x => x.WQTNo == wqtNo && x.Location == location);
                ttsCertificate = db.WQR006.Any(x => x.WQTNo == wqtNo && x.Location == location);

                if (wpqCertificate || wopqCertificate || ttsCertificate)
                {
                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Key = false;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public ActionResult TSSCertificate(string wqtno, string l)
        {
            ViewBag.IsDisplayOnly = false;
            //ViewBag.Id = id;
            WQR006 objWQR006 = new WQR006();
            if (!string.IsNullOrWhiteSpace(wqtno))
            {
                //string WQTNo = db.WQR003.Where(i => i.Id == id).FirstOrDefault().WQTNo;
                ViewBag.WQTNo = wqtno;
                objWQR006 = db.WQR006.Where(i => i.WQTNo == wqtno && i.Location == l).FirstOrDefault();
                if (objWQR006 == null)
                {
                    objWQR006 = new WQR006();
                    ViewBag.CertId = objWQR006.Id;
                    objWQR006.WQTNo = wqtno;
                }
                else
                {
                    ViewBag.CertId = objWQR006.Id;
                    //   objWQR006.WQTNo = wqtno;
                }
            }
            else
            {
                ViewBag.CertId = 0;
                ViewBag.WQTNo = string.Empty;
            }
            return View(objWQR006);
        }

        public ActionResult DisplayTSSCertificate(string wqtno, string l)
        {
            ViewBag.IsDisplayOnly = true;
            //ViewBag.Id = id;
            WQR006 objWQR006 = new WQR006();
            if (!string.IsNullOrWhiteSpace(wqtno))
            {
                //string WQTNo = db.WQR003.Where(i => i.Id == id).FirstOrDefault().WQTNo;
                ViewBag.WQTNo = wqtno;
                objWQR006 = db.WQR006.Where(i => i.WQTNo == wqtno && i.Location == l).FirstOrDefault();
                if (objWQR006 == null)
                {
                    objWQR006 = new WQR006();
                    ViewBag.CertId = objWQR006.Id;
                    objWQR006.WQTNo = wqtno;
                }
                else
                {
                    ViewBag.CertId = objWQR006.Id;
                    //   objWQR006.WQTNo = wqtno;
                }
            }
            else
            {
                ViewBag.CertId = 0;
                ViewBag.WQTNo = string.Empty;
            }
            return View("TSSCertificate", objWQR006);
        }


        [SessionExpireFilter]
        [HttpPost]
        public ActionResult LoadTabWisePartial(int? id, string WQTNo, string IsDisplayOnly, string partialName)
        {
            string location = string.Empty;
            string filepath = string.Empty;
            string BU = string.Empty;
            NDEModels objNDEModels = new NDEModels();
            ModelWQRequest objModelWQRequest = new ModelWQRequest();
            WQR006 objWQR006 = new WQR006();
            var objWQR001 = (from w3 in db.WQR003
                             join w2 in db.WQR002 on w3.LineId equals w2.LineId
                             join w1 in db.WQR001 on w2.HeaderId equals w1.HeaderId
                             where w3.WQTNo == WQTNo
                             select new { w1.CouponType, w1.BU, w1.Location }
                               ).FirstOrDefault();
            if (objWQR001 != null)
            {
                BU = objWQR001.BU;
                location = objWQR001.Location;
                ViewBag.txtLocation = db.WQR008.Where(x => x.Location == objWQR001.Location).Select(x => x.Location + "-" + x.Description).FirstOrDefault();
            }
            else
            {
                BU = string.Empty;
                location = objClsLoginInfo.Location;
            }
            if (id > 0)
            {
                objWQR006 = db.WQR006.Where(i => i.Id == id).FirstOrDefault();
                location = objWQR006.Location;
                ViewBag.txtLocation = db.WQR008.Where(x => x.Location == objWQR006.Location).Select(x => x.Location + "-" + x.Description).FirstOrDefault();
                //objWQR006.Id = Convert.ToInt32(id);
                var objWQR003 = db.WQR003.Where(i => i.WQTNo == objWQR006.WQTNo).FirstOrDefault();
                if (objWQR003 != null)
                {
                    if (string.IsNullOrWhiteSpace(objWQR006.WeldingProcessEsActual))
                    {
                        objWQR006.WeldingProcessEsActual = objWQR003.WeldingProcess;
                        objWQR006.WeldingProcessEsQualified = objWQR003.WeldingProcess;
                    }
                    if (string.IsNullOrWhiteSpace(objWQR006.WeldingTypeActual))
                    {
                        objWQR006.WeldingTypeActual = objWQR003.WeldType;
                        objWQR006.WeldingTypeQualified = objWQR003.WeldType;
                    }
                    ViewBag.Data = "disable";
                }
            }
            else
            {
                //location = (from a in db.COM003
                //            join b in db.COM002 on a.t_loca equals b.t_dimx
                //            where b.t_dtyp == 1 && a.t_psno.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                //            select b.t_dimx).FirstOrDefault();
                var objWQR003 = db.WQR003.Where(i => i.WQTNo == WQTNo).FirstOrDefault();

                objWQR006 = new WQR006();
                objWQR006.Location = location;
                objWQR006.WQTNo = WQTNo;
                if (objWQR003 != null)
                {
                    objWQR006.WelderName = objWQR003.Welder;
                    objWQR006.WelderStampNo = objWQR003.WelderStamp;
                    objWQR006.JoinType = objWQR003.JointType;
                    objWQR006.WPSNo = objWQR003.WPSNo;
                    objWQR006.MaintainedOn = objWQR003.CreatedOn;
                    objWQR006.MaintainedBy = objWQR003.CreatedBy;
                    ViewBag.Data = "disable";
                }
                else
                {
                    objWQR006.MaintainedOn = DateTime.Now;
                    objWQR006.MaintainedBy = objClsLoginInfo.UserName;
                }
            }
            if (!string.IsNullOrEmpty(objWQR006.WelderName)) { ViewBag.WelderName = (new WPQCertificateController()).GetIntExtWelderName(objWQR006.WelderName); }
            if (!string.IsNullOrEmpty(objWQR006.MaintainedBy)) { ViewBag.MaintainedBy = objWQR006.MaintainedBy + " - " + Manager.GetUserNameFromPsNo(objWQR006.MaintainedBy); }

            ViewBag.Location = (from c2 in db.COM002
                                where c2.t_dimx == location
                                select c2.t_dimx + " - " + c2.t_desc
                              ).FirstOrDefault();
            //if (partialName == "Tab1")

            ViewBag.IsDisplayOnly = IsDisplayOnly;
            switch (partialName)
            {
                case "tab1":
                    filepath = "_TTSTab1";
                    var lstJointType = objModelWQRequest.LoadSubCatagoryDescription("Joint Type WQ", objClsLoginInfo.Location, "");//.ToList().Select(i => new CategoryData { Code = i.Code, Value = i.Code, CategoryDescription = i.Code + '-' + i.Description }).Distinct();
                    ViewBag.JointTypeWQ = lstJointType.Any() ? lstJointType : null;
                    if (objWQR006 != null)
                    {
                        ViewBag.JoinType = objModelWQRequest.GetCategoryByKey("Joint Type WQ", objWQR006.JoinType).CategoryDescription;
                    }
                    string approved = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
                    ViewBag.WPSNo = (from a in db.WPS010
                                     where a.Status.Equals(approved, StringComparison.OrdinalIgnoreCase)
                                     select new CategoryData { Value = a.WPSNumber, CategoryDescription = a.WPSNumber }).Distinct().ToList();

                    //change by Ajay Chauhan On 16/1/2018 observation Id 14316
                    var locations = db.WQR008.ToList();
                    ViewBag.lstLocation = locations.AsEnumerable().Select(x => new CategoryData { CategoryDescription = x.Location + "-" + x.Description, Value = x.Location }).ToList();

                    break;
                case "tab2":
                    filepath = "_TTSTab2";

                    var lstWeldingProcess = objModelWQRequest.LoadSubCatagoryDescription("Welding Process", objClsLoginInfo.Location, ""); // db.WQR003.Distinct().ToList().Select(i => new CategoryData { Code = i.WeldingProcess, CategoryDescription = objNDEModels.GetCategory(i.WeldingProcess).CategoryDescription }).Distinct();
                    var lstWeldingType = objModelWQRequest.LoadSubCatagoryDescription("Welding Type", objClsLoginInfo.Location, ""); //db.WQR003.Distinct().ToList().Select(i => new CategoryData { Code = i.WeldType, CategoryDescription = objNDEModels.GetCategory(i.WeldType).CategoryDescription }).Distinct();
                    var lstJointConfiguration = objModelWQRequest.LoadSubCatagoryDescription("Weld Joint Configuration", objClsLoginInfo.Location, "");//.ToList().Select(i => new CategoryData { Code = i.Code, Value = i.Code, CategoryDescription = i.Code + '-' + i.Description }).Distinct();
                    var lstPitch = objModelWQRequest.LoadSubCatagoryDescription("Pitch", objClsLoginInfo.Location, "");//.ToList().Distinct();
                    var lstSingleMultiPass = objModelWQRequest.LoadSubCatagoryDescription("Single Pass/Multipass", objClsLoginInfo.Location, "");//.ToList().Select(i => new CategoryData { Code = i.Code, Value = i.Code, CategoryDescription = i.Code + '-' + i.Description }).Distinct();

                    ViewBag.WeldingProcess = lstWeldingProcess.Any() ? lstWeldingProcess : null;
                    ViewBag.WeldingType = lstWeldingType.Any() ? lstWeldingType : null;
                    ViewBag.JointConfiguration = lstJointConfiguration.Any() ? lstJointConfiguration : null;
                    ViewBag.Pitch = lstPitch.Any() ? lstPitch : null;
                    ViewBag.SingleMultiPass = lstSingleMultiPass.Any() ? lstSingleMultiPass : null;

                    if (objWQR006 != null)
                    {
                        ViewBag.WeldingProcessASelected = objModelWQRequest.GetCategoryByKey("Welding Process", objWQR006.WeldingProcessEsActual).CategoryDescription;
                        ViewBag.WeldingProcessQSelected = objModelWQRequest.GetCategoryByKey("Welding Process", objWQR006.WeldingProcessEsQualified).CategoryDescription;
                        ViewBag.WeldingTypeASelected = objModelWQRequest.GetCategoryByKey("Welding Type", objWQR006.WeldingTypeActual).CategoryDescription;
                        ViewBag.WeldingTypeQSelected = objModelWQRequest.GetCategoryByKey("Welding Type", objWQR006.WeldingTypeQualified).CategoryDescription;
                        //ViewBag.JointConfigurationASelected = objModelWQRequest.GetCategoryByKey("Weld Joint Configuration", objWQR006.WeldJointConfigurationPreplacedFillerMetal).CategoryDescription;
                        //ViewBag.JointConfigurationQSelected = objModelWQRequest.GetCategoryByKey("Weld Joint Configuration", objWQR006.WeldJointConfigurationPreplacedFillerMetalQualified).CategoryDescription;
                        ViewBag.PitchASelected = objModelWQRequest.GetCategoryByKey("Pitch", objWQR006.PitchMmActual).CategoryDescription;
                        //ViewBag.PitchQSelected = objModelWQRequest.GetCategoryByKey(objWQR006.WeldingTypeQualified).CategoryDescription;
                        ViewBag.SingleMultiPassASelected = objModelWQRequest.GetCategoryByKey("Single Pass/Multipass", objWQR006.SinglePassMultipassActual).CategoryDescription;
                        ViewBag.SingleMultiPassQSelected = objModelWQRequest.GetCategoryByKey("Single Pass/Multipass", objWQR006.SinglePassMultpiassQualified).CategoryDescription;
                    }

                    break;
                case "tab3":
                    filepath = "_TTSTab3";
                    var lstPWHT = objModelWQRequest.LoadSubCatagoryDescription("PWHT", objClsLoginInfo.Location, "");//.ToList().Select(i => new CategoryData { Code = i.Code, Value = i.Code, CategoryDescription = i.Code + '-' + i.Description }).Distinct();
                    var lstCurrentType = objModelWQRequest.LoadSubCatagoryDescription("Current Type", objClsLoginInfo.Location, "");//.ToList().Select(i => new CategoryData { Code = i.Code, Value = i.Code, CategoryDescription = i.Code + '-' + i.Description }).Distinct();
                    var lstCurrentPolarity = objModelWQRequest.LoadSubCatagoryDescription("Current Polarity", objClsLoginInfo.Location, "");//.ToList().Select(i => new CategoryData { Code = i.Code, Value = i.Code, CategoryDescription = i.Code + '-' + i.Description }).Distinct();
                    var lstTubeExpansion = objModelWQRequest.LoadSubCatagoryDescription("Tube Expansion Prior to Welding", objClsLoginInfo.Location, "");//.ToList().Select(i => new CategoryData { Code = i.Code, Value = i.Code, CategoryDescription = i.Code + '-' + i.Description }).Distinct();
                    var lstShieldinggas = objModelWQRequest.LoadSubCatagoryDescription("Shielding gas", objClsLoginInfo.Location, "");//.ToList().Select(i => new CategoryData { Code = i.Code, Value = i.Code, CategoryDescription = i.Code + '-' + i.Description }).Distinct();
                    var lstFillerMetal = objModelWQRequest.LoadSubCatagoryDescription("Filler Metal(GTAW/PAW)", objClsLoginInfo.Location, "");//.ToList().Select(i => new CategoryData { Code = i.Code, Value = i.Code, CategoryDescription = i.Code + '-' + i.Description }).Distinct();
                    var lstAuxiliaryGasShield = objModelWQRequest.LoadSubCatagoryDescription("Auxiliary Gas Shield", objClsLoginInfo.Location, "");//.ToList().Select(i => new CategoryData { Code = i.Code, Value = i.Code, CategoryDescription = i.Code + '-' + i.Description }).Distinct();
                    var lstAutomaticArcvoltageControl = objModelWQRequest.LoadSubCatagoryDescription("Automatic arc voltage control", objClsLoginInfo.Location, "");//.ToList().Select(i => new CategoryData { Code = i.Code, Value = i.Code, CategoryDescription = i.Code + '-' + i.Description }).Distinct();

                    ViewBag.PWHT = lstPWHT.Any() ? lstPWHT : null;
                    ViewBag.CurrentType = lstCurrentType.Any() ? lstCurrentType : null;
                    ViewBag.CurrentPolarity = lstCurrentPolarity.Any() ? lstCurrentPolarity : null;
                    ViewBag.TubeExpansion = lstTubeExpansion.Any() ? lstTubeExpansion : null;
                    ViewBag.Shieldinggas = lstShieldinggas.Any() ? lstShieldinggas : null;
                    ViewBag.FillerMetal = lstFillerMetal.Any() ? lstFillerMetal : null;
                    ViewBag.AuxiliaryGasShield = lstAuxiliaryGasShield.Any() ? lstAuxiliaryGasShield : null;
                    ViewBag.AutomaticArcvoltageControl = lstAutomaticArcvoltageControl.Any() ? lstAutomaticArcvoltageControl : null;
                    if (objWQR006 != null)
                    {
                        ViewBag.PWHTASelected = objModelWQRequest.GetCategoryByKey("PWHT", objWQR006.PWHTActual).CategoryDescription;
                        ViewBag.CurrentTypeASelected = objModelWQRequest.GetCategoryByKey("Current Type", objWQR006.CurrentTypeActual).CategoryDescription;
                        ViewBag.CurrentPolarityASelected = objModelWQRequest.GetCategoryByKey("Current Polarity", objWQR006.CurrentPolarityActual).CategoryDescription;
                        ViewBag.TubeExpansionASelected = objModelWQRequest.GetCategoryByKey("Tube Expansion Prior to Welding", objWQR006.TubeExpansionPriorToWeldingActual).CategoryDescription;
                        ViewBag.ShieldinggasASelected = objModelWQRequest.GetCategoryByKey("Shielding gas", objWQR006.ShieldingGasEsActual).CategoryDescription;
                        ViewBag.FillerMetalASelected = objModelWQRequest.GetCategoryByKey("Filler Metal(GTAW/PAW)", objWQR006.FillerMetalGTAWPAWActual).CategoryDescription;
                        ViewBag.AuxiliaryGasShieldASelected = objModelWQRequest.GetCategoryByKey("Auxiliary Gas Shield", objWQR006.AuxiliaryGasShieldActual).CategoryDescription;
                        ViewBag.AutomaticArcvoltageControlASelected = objModelWQRequest.GetCategoryByKey("Automatic arc voltage control", objWQR006.AutomaticArcVoltageControlActual).CategoryDescription;

                        ViewBag.PWHTQSelected = objModelWQRequest.GetCategoryByKey("PWHT", objWQR006.PWHTQualified).CategoryDescription;
                        ViewBag.CurrentTypeQSelected = objModelWQRequest.GetCategoryByKey("Current Type", objWQR006.CurrentTypeQualified).CategoryDescription;
                        ViewBag.CurrentPolarityQSelected = objModelWQRequest.GetCategoryByKey("Current Polarity", objWQR006.CurrentPolarityQualified).CategoryDescription;
                        ViewBag.TubeExpansionQSelected = objModelWQRequest.GetCategoryByKey("Tube Expansion Prior to Welding", objWQR006.TubeExpansionPriorToWeldingQualified).CategoryDescription;
                        ViewBag.ShieldinggasQSelected = objModelWQRequest.GetCategoryByKey("Shielding gas", objWQR006.ShieldingGasEsQualified).CategoryDescription;
                        ViewBag.FillerMetalQSelected = objModelWQRequest.GetCategoryByKey("Filler Metal(GTAW/PAW)", objWQR006.FillerMetalGTAWPAWQualified).CategoryDescription;
                        ViewBag.AuxiliaryGasShieldQSelected = objModelWQRequest.GetCategoryByKey("Auxiliary Gas Shield", objWQR006.AuxiliaryGasShieldQualified).CategoryDescription;
                        ViewBag.AutomaticArcvoltageControlQSelected = objModelWQRequest.GetCategoryByKey("Automatic arc voltage control", objWQR006.AutomaticArcVoltageControlQualified).CategoryDescription;
                    }
                    break;
                case "tab4":
                    filepath = "_TTSTab4";
                    var lstResult = objModelWQRequest.LoadSubCatagoryDescription("Visual Result", objClsLoginInfo.Location, "");//.Distinct().ToList().Select(i => new CategoryData { Code = i.Code, Value = i.Code, CategoryDescription = i.Code + '-' + i.Description }).Distinct();
                    var lstLiquidPenetrationResultResult = objModelWQRequest.LoadSubCatagoryDescription("Liquid Penetration Result", objClsLoginInfo.Location, "");//.ToList().Select(i => new CategoryData { Code = i.Code, Value = i.Code, CategoryDescription = i.Code + '-' + i.Description }).Distinct();
                    var lstCracksObserved = objModelWQRequest.LoadSubCatagoryDescription("Crack/s Observed", objClsLoginInfo.Location, "");//.ToList().Select(i => new CategoryData { Code = i.Code, Value = i.Code, CategoryDescription = i.Code + '-' + i.Description }).Distinct();
                    var lstCompleteFusion = objModelWQRequest.LoadSubCatagoryDescription("Complete Fusion", objClsLoginInfo.Location, "");//.ToList().Select(i => new CategoryData { Code = i.Code, Value = i.Code, CategoryDescription = i.Code + '-' + i.Description }).Distinct();
                    var lstCompletePenetration = objModelWQRequest.LoadSubCatagoryDescription("Complete Penetration", objClsLoginInfo.Location, "");//.ToList().Select(i => new CategoryData { Code = i.Code, Value = i.Code, CategoryDescription = i.Code + '-' + i.Description }).Distinct();
                    var lstInspectionBy = objModelWQRequest.LoadSubCatagoryDescription("Inspection by", objClsLoginInfo.Location, "");//.ToList().Select(i => new CategoryData { Code = i.Code, Value = i.Code, CategoryDescription = i.Code + '-' + i.Description }).Distinct();
                    var lstMacroResult = objModelWQRequest.LoadSubCatagoryDescription("Macro Result", objClsLoginInfo.Location, "");
                    ViewBag.Result = lstResult.Any() ? lstResult : null;
                    ViewBag.LiquidPenetrationResultResult = lstLiquidPenetrationResultResult.Any() ? lstLiquidPenetrationResultResult : null;
                    ViewBag.CracksObserved = lstCracksObserved.Any() ? lstCracksObserved : null;
                    ViewBag.CompleteFusion = lstCompleteFusion.Any() ? lstCompleteFusion : null;
                    ViewBag.CompletePenetration = lstCompletePenetration.Any() ? lstCompletePenetration : null;
                    ViewBag.InspectionBy = lstInspectionBy.Any() ? lstInspectionBy : null;
                    ViewBag.ddlMacroResult = lstMacroResult.Any() ? lstMacroResult : null;
                    objWQR006.Company = "Larsen & Toubro Limited";

                    if (objWQR006 != null)
                    {
                        ViewBag.ResultSelected = objModelWQRequest.GetCategoryByKey("Visual Result", objWQR006.Result).CategoryDescription;
                        ViewBag.ResultLPSelected = objModelWQRequest.GetCategoryByKey("Liquid Penetration Result", objWQR006.LiquidPenetrationResult).CategoryDescription;
                        ViewBag.CracksObservedSelected = objModelWQRequest.GetCategoryByKey("Crack/s Observed", objWQR006.BCrackSObserved).CategoryDescription;
                        ViewBag.CompleteFusionSelected = objModelWQRequest.GetCategoryByKey("Complete Fusion", objWQR006.CCompleteFusion).CategoryDescription;
                        ViewBag.CompletePenetrationSelected = objModelWQRequest.GetCategoryByKey("Complete Penetration", objWQR006.DCompletePenetration).CategoryDescription;
                        ViewBag.InspectionBySelected = objModelWQRequest.GetCategoryByKey("Inspection by", objWQR006.InspectionByAndDescription).CategoryDescription;
                        ViewBag.MacroResult = objModelWQRequest.GetCategoryByKey("Macro Result", objWQR006.MacroResult).CategoryDescription;
                    }

                    //ViewBag.InspectionBy = LoadSubCatagoryDescription("Inspection by", location, BU);
                    //if (!string.IsNullOrEmpty(objWQR004.InspectionBy)) { ViewBag.lstInspectionBy = objNDEModels.GetCategory(objWQR004.InspectionBy).CategoryDescription; }

                    if (objWQR006.Company == string.Empty) { objWQR006.Company = "Larsen & Toubro Limited"; }
                    break;
                case "tab5":
                    filepath = "_TTSTab5";
                    break;
                default:
                    filepath = "_TTSTab1";
                    break;
            }


            return PartialView(filepath, objWQR006);
        }


        [HttpPost]
        [SessionExpireFilter]
        public JsonResult LoadTSSCertificateData(JQueryDataTableParamModel param)
        {
            try
            {
                var IsDisplayOnly = Convert.ToBoolean(param.IsVisible);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                string[] columnName = { "Location", "WQTNo", "welders.Welder", "wqr.MaintainedBy + '-' + c32.t_name", "wqr.Location", "WPSNo", "WelderName", "WelderStampNo", "JoinType", "MaintainedBy", "MaintainedOn" };

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                strWhere += "1=1 ";//Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "", "Location");
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var lstResult = db.SP_WQ_TSSCERTIFICATE_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.Location),
                                Convert.ToString(uc.WQTNo),
                                //Convert.ToString(uc.WPSNo),
                                Convert.ToString( uc.WelderName),
                                Convert.ToString(uc.WelderStampNo),
                                //Convert.ToString(uc.JoinType),
                                Convert.ToString(uc.WeldingProcessEsQualified),
                                Convert.ToString(uc.WeldingType),
                                Convert.ToString(uc.LastWeldDate),
                                uc.LastWeldDate == null || uc.LastWeldDate.Value==DateTime.MinValue? "" :uc.LastWeldDate.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture),
                                uc.ValueUpto == null || uc.ValueUpto.Value==DateTime.MinValue? "" :uc.ValueUpto.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture),
                                Convert.ToString( uc.MaintainedBy),
                                uc.MaintainedOn == null || uc.MaintainedOn.Value==DateTime.MinValue? "" :uc.MaintainedOn.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture),
                                IsDisplayOnly ?
                                " <center>"+
                                    "<a class='' title='View Details' href='"+ WebsiteURL + "/WQ/TSSCertificate/DisplayTSSCertificate?wqtno="+Convert.ToString(uc.WQTNo)+ Convert.ToString(!string.IsNullOrWhiteSpace(uc.Location)? "&&l="+  uc.Location.Split('-')[0]:"")+"'><i class='iconspace fa fa-eye'></i></a>"+
                                      "<i style=\"\" title=\"Copy Record\" class=\"disabledicon fa fa-files-o\"></i>" +
                                    "<i style='' title='Print Report' class='iconspace fa fa-print' onClick='PrintReport("+uc.Id+")'></i>" +
                                "</center>" :
                                "<center>"+
                                    "<a class='' title='View Details' href='"+ WebsiteURL + "/WQ/TSSCertificate/TSSCertificate?wqtno="+Convert.ToString(uc.WQTNo)+ Convert.ToString(!string.IsNullOrWhiteSpace(uc.Location)? "&&l="+  uc.Location.Split('-')[0]:"")+"'><i class='iconspace fa fa-eye'></i></a>"+
                                      "<i style=\"\" title=\"Copy Record\" class=\"iconspace fa fa-files-o\" onClick=\"CopyCertificate("+uc.Id+",'"+uc.WQTNo+"','tss','"+ Convert.ToString(!string.IsNullOrWhiteSpace(uc.Location) ?  uc.Location.Split('-')[0] : "")+"')\"></i>" +
                                    "<i style='' title='Print Report' class='iconspace fa fa-print' onClick='PrintReport("+uc.Id+")'></i>" +
                                "</center>"
                            //"<center>"+
                            //    "<a class='btn btn-xs' title='View Details' href='"+ WebsiteURL + "/WQ/TSSCertificate/TSSCertificate?wqtno="+Convert.ToString(uc.WQTNo)+"'><i class='fa fa-eye'></i></a>"+
                            //    "<i style='margin-left:5px;cursor:pointer;' title='Print Report' class='fa fa-print' onClick='PrintReport("+uc.Id+")'></i>" +
                            //    "</center>",
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveTTSCertificate(WQR006 objWQR006, FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            WQR006 objWQR6 = new WQR006();
            string partialName = fc["partialName"];
            int id = !string.IsNullOrEmpty(fc["Id"]) ? Convert.ToInt32(fc["Id"]) : 0;
            try
            {
                if (partialName == WQRCertificateTab.Tab1.GetStringValue())
                {
                    #region Tab1
                    var lstModelWelderDetail = (from a in db.QMS003
                                                where a.WelderPSNo.Equals(objWQR006.WelderName, StringComparison.OrdinalIgnoreCase)
                                                select new ModelWelderDetail { ShopCode = a.ShopCode }
                                          )
                                             .Union(from b in db.QMS004
                                                    where b.WelderPSNo.Equals(objWQR006.WelderName, StringComparison.OrdinalIgnoreCase) && b.ActiveWithCompany != null && b.ActiveWithCompany == true
                                                    select new ModelWelderDetail { ShopCode = b.ShopCode }).FirstOrDefault();

                    if (objWQR006.Id == 0)
                    {
                        objWQR6 = new WQR006();
                        WQR003 objWQR003 = db.WQR003.Where(i => i.WQTNo == objWQR006.WQTNo).FirstOrDefault();
                        if (objWQR003 != null)
                        {
                            objWQR003.CertificateGenerated = true;
                            objWQR003.CertificateGeneratedBy = objClsLoginInfo.UserName;
                            objWQR003.CertificateGeneratedOn = DateTime.Now;
                        }
                        // objWQR003.CertificateGenerated = true;
                        objWQR6.Shop = (lstModelWelderDetail != null) ? lstModelWelderDetail.ShopCode : "";
                        objWQR6.Location = objWQR006.Location;
                        objWQR6.WQTNo = objWQR006.WQTNo;
                        objWQR6.WelderName = objWQR006.WelderName;
                        objWQR6.WelderStampNo = objWQR006.WelderStampNo;
                        objWQR6.JoinType = objWQR006.JoinType;
                        objWQR6.WeldingType = objWQR006.WeldingType;
                        objWQR6.WPSNo = objWQR006.WPSNo;
                        objWQR6.FillerMetalAWS = objWQR006.FillerMetalAWS;
                        objWQR6.TestCoupon = objWQR006.TestCoupon;
                        objWQR6.ProductionWeld = objWQR006.ProductionWeld;
                        objWQR6.DateOfWelding = objWQR006.DateOfWelding;
                        string DateOfWelding = fc["DateOfWelding"] != null ? fc["DateOfWelding"].ToString() : "";
                        if (!string.IsNullOrWhiteSpace(DateOfWelding))
                        {
                            objWQR6.DateOfWelding = DateTime.ParseExact(DateOfWelding, @"d/M/yyyy", CultureInfo.InvariantCulture);
                        }
                        objWQR6.CreatedBy = objClsLoginInfo.UserName;
                        objWQR6.CreatedOn = DateTime.Now;
                        objWQR6.MaintainedBy = objWQR006.MaintainedBy;
                        objWQR6.MaintainedOn = objWQR006.MaintainedOn;
                        objWQR6.SpecificationTypeGrade = objWQR006.SpecificationTypeGrade;
                        objWQR6.FillerMetalAWS = objWQR006.FillerMetalAWS;
                        db.WQR006.Add(objWQR6);

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Record Saved Successfully!";
                        objResponseMsg.HeaderId = objWQR6.Id;
                        objResponseMsg.wqtno = objWQR6.WQTNo;
                    }
                    else
                    {

                        objWQR6 = db.WQR006.Where(i => i.Id == objWQR006.Id).FirstOrDefault();
                        objWQR6.Shop = (lstModelWelderDetail.ShopCode != null) ? lstModelWelderDetail.ShopCode : "";
                        objWQR6.Location = objWQR006.Location;
                        objWQR6.WQTNo = objWQR006.WQTNo;
                        objWQR6.WelderName = objWQR006.WelderName;
                        objWQR6.WelderStampNo = objWQR006.WelderStampNo;
                        objWQR6.JoinType = objWQR006.JoinType;
                        objWQR6.WeldingType = objWQR006.WeldingType;
                        objWQR6.WPSNo = objWQR006.WPSNo;
                        objWQR6.FillerMetalAWS = objWQR006.FillerMetalAWS;
                        objWQR6.TestCoupon = objWQR006.TestCoupon;
                        objWQR6.DateOfWelding = objWQR006.DateOfWelding;
                        string DateOfWelding = fc["DateOfWelding"] != null ? fc["DateOfWelding"].ToString() : "";
                        if (!string.IsNullOrWhiteSpace(DateOfWelding))
                        {
                            objWQR6.DateOfWelding = DateTime.ParseExact(DateOfWelding, @"d/M/yyyy", CultureInfo.InvariantCulture);
                        }
                        objWQR6.ProductionWeld = objWQR006.ProductionWeld;
                        objWQR6.MaintainedBy = objWQR006.MaintainedBy;
                        objWQR6.MaintainedOn = objWQR006.MaintainedOn;
                        objWQR6.SpecificationTypeGrade = objWQR006.SpecificationTypeGrade;
                        objWQR6.FillerMetalAWS = objWQR006.FillerMetalAWS;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Record Saved Successfully!";
                        objResponseMsg.HeaderId = objWQR6.Id;
                        objResponseMsg.wqtno = objWQR6.WQTNo;
                    }
                    #endregion

                }
                else if (partialName == WQRCertificateTab.Tab2.GetStringValue())
                {

                    objWQR6 = db.WQR006.Where(i => i.Id == objWQR006.Id).FirstOrDefault();
                    #region Tab2

                    objWQR6.WeldingProcessEsActual = objWQR006.WeldingProcessEsActual;
                    objWQR6.WeldingProcessEsQualified = objWQR006.WeldingProcessEsQualified;
                    //objWQR6.WeldJointConfigurationPreplacedFillerMetal = objWQR006.WeldJointConfigurationPreplacedFillerMetal;
                    //objWQR6.WeldJointConfigurationPreplacedFillerMetalQualified = objWQR006.WeldJointConfigurationPreplacedFillerMetalQualified;
                    objWQR6.WeldingTypeActual = objWQR006.WeldingTypeActual;
                    objWQR6.WeldingTypeQualified = objWQR006.WeldingTypeQualified;
                    objWQR6.JointConfigurationActual = objWQR006.JointConfigurationActual;
                    objWQR6.JointConfigurationQualified = objWQR006.JointConfigurationQualified;
                    objWQR6.DepthOfGrooveMmActual = objWQR006.DepthOfGrooveMmActual;
                    objWQR6.DepthOfGrooveMmQualified = objWQR006.DepthOfGrooveMmQualified;
                    objWQR6.GrooveAngleRadiusActualFrom = objWQR006.GrooveAngleRadiusActualFrom;
                    objWQR6.GrooveRadiusAngleQualified = objWQR006.GrooveRadiusAngleQualified;
                    objWQR6.TubeWallThicknessMmActual = objWQR006.TubeWallThicknessMmActual;
                    objWQR6.TubeWallThicknessMmQualified = objWQR006.TubeWallThicknessMmQualified;
                    objWQR6.TubeOutsideDiameterActual = objWQR006.TubeOutsideDiameterActual;
                    objWQR6.TubeOutsideDiameterQualified = objWQR006.TubeOutsideDiameterQualified;
                    objWQR6.PitchMmActual = objWQR006.PitchMmActual;
                    objWQR6.PitchMmQualified = objWQR006.PitchMmQualified;
                    objWQR6.LigamentMmActual = objWQR006.LigamentMmActual;
                    objWQR6.TubeSheetThicknessActual = objWQR006.TubeSheetThicknessActual;
                    objWQR6.TubeSheetThicknessQualified = objWQR006.TubeSheetThicknessQualified;
                    objWQR6.LigamentMmQualified = objWQR006.LigamentMmQualified;
                    objWQR6.TubeProjectionMmActual = objWQR006.TubeProjectionMmActual;
                    objWQR6.TubeProjectionMmQualified = objWQR006.TubeProjectionMmQualified;
                    objWQR6.SinglePassMultipassActual = objWQR006.SinglePassMultipassActual;
                    objWQR6.SinglePassMultpiassQualified = objWQR006.SinglePassMultpiassQualified;
                    objWQR6.WeldingPositionActual = objWQR006.WeldingPositionActual;
                    objWQR6.WeldingPositionQualified = objWQR006.WeldingPositionQualified;
                    objWQR6.VerticalProgressionActual = objWQR006.VerticalProgressionActual;
                    objWQR6.VerticalProgressionQualified = objWQR006.VerticalProgressionQualified;
                    objWQR6.TubeMaterialPNoActual = objWQR006.TubeMaterialPNoActual;
                    objWQR6.TubeMaterialPNoQualified = objWQR006.TubeMaterialPNoQualified;
                    objWQR6.TubesheetMaterialPNoIfTubesheetIsPartOfWeldActual = objWQR006.TubesheetMaterialPNoIfTubesheetIsPartOfWeldActual;
                    objWQR6.TubesheetMaterialPNoIfTubesheetIsPartOfWeldQualified = objWQR006.TubesheetMaterialPNoIfTubesheetIsPartOfWeldQualified;
                    //objWQR6.TubesheetCladdingMaterialPNoIfCladdingIsPartOfWeld = objWQR006.TubesheetCladdingMaterialPNoIfCladdingIsPartOfWeld;
                    objWQR6.TubesheetCladdingMatlANoIfCladdingIsPartOfWeldActual = objWQR006.TubesheetCladdingMatlANoIfCladdingIsPartOfWeldActual;
                    objWQR6.TubesheetCladdingMatlANoIfCladdingIsPartOfWeldQualified = objWQR006.TubesheetCladdingMatlANoIfCladdingIsPartOfWeldQualified;

                    objWQR6.TubeSheetOverlayThicknessActual = objWQR006.TubeSheetOverlayThicknessActual;
                    objWQR6.TubeSheetOverlayThicknessQualified = objWQR006.TubeSheetOverlayThicknessQualified;


                    objWQR6.TubeNoOfTubesActual = objWQR006.TubeNoOfTubesActual;
                    objWQR6.TubeNoOfTubesQualified = objWQR006.TubeNoOfTubesQualified;

                    objWQR6.FillerMetalSFANoClassificationActual = objWQR006.FillerMetalSFANoClassificationActual;
                    objWQR6.FillerMetalSFANoClassificationQualified = objWQR006.FillerMetalSFANoClassificationQualified;

                    objWQR6.FillerMetalANoActual = objWQR006.FillerMetalANoActual;
                    objWQR6.FillerMetalANoQualified = objWQR006.FillerMetalANoQualified;

                    objWQR6.FillerMetalTradeNameActual = objWQR006.FillerMetalTradeNameActual;
                    objWQR6.FillerMetalTradeNameQualified = objWQR006.FillerMetalTradeNameQualified;


                    //objWQR6.TubesheetCladdingMaterialPNoIfCladdingIsPartOfWeldQualified = objWQR006.TubesheetCladdingMaterialPNoIfCladdingIsPartOfWeldQualified;

                    objWQR6.Others = objWQR006.Others;

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Record Save Successfully!";
                    objResponseMsg.HeaderId = objWQR6.Id;
                    objResponseMsg.wqtno = objWQR6.WQTNo;
                    #endregion
                }
                else if (partialName == WQRCertificateTab.Tab3.GetStringValue())
                {
                    objWQR6 = db.WQR006.Where(i => i.Id == objWQR006.Id).FirstOrDefault();

                    #region Tab3
                    objWQR6.ANoOfWeldDepositIfFillerMetalIsAddedActual = objWQR006.ANoOfWeldDepositIfFillerMetalIsAddedActual;
                    objWQR6.ANoOfWeldDepositIfFillerMetalIsAddedQualified = objWQR006.ANoOfWeldDepositIfFillerMetalIsAddedQualified;
                    objWQR6.PreheatTempMinActual = objWQR006.PreheatTempMinActual;
                    objWQR6.PreheatTempMinQualified = objWQR006.PreheatTempMinQualified;
                    objWQR6.InterpassTempMaxActual = objWQR006.InterpassTempMaxActual;
                    objWQR6.InterpassTempMaxQualified = objWQR006.InterpassTempMaxQualified;
                    objWQR6.PWHTActual = objWQR006.PWHTActual;
                    objWQR6.PWHTQualified = objWQR006.PWHTQualified;
                    objWQR6.CurrentTypeActual = objWQR006.CurrentTypeActual;
                    objWQR6.CurrentTypeQualified = objWQR006.CurrentTypeQualified;
                    objWQR6.CurrentPolarityActual = objWQR006.CurrentPolarityActual;
                    objWQR6.CurrentPolarityQualified = objWQR006.CurrentPolarityQualified;
                    objWQR6.FillerMetalElectrodeDiaMmActual = objWQR006.FillerMetalElectrodeDiaMmActual;
                    objWQR6.FillerMetalElectrodeDiaMmQualified = objWQR006.FillerMetalElectrodeDiaMmQualified;
                    objWQR6.CurrentAActual = objWQR006.CurrentAActual;
                    objWQR6.CurrentAQualified = objWQR006.CurrentAQualified;
                    //objWQR6.FNoActual = objWQR006.FNoActual;
                    //objWQR6.FNoQualified = objWQR006.FNoQualified;
                    objWQR6.FillerMetalElectrodeDiaMmActual1 = objWQR006.FillerMetalElectrodeDiaMmActual1;
                    objWQR6.FillerMetalElectrodeDiaMmQualified1 = objWQR006.FillerMetalElectrodeDiaMmQualified1;
                    objWQR6.CurrentAActual1 = objWQR006.CurrentAActual1;
                    objWQR6.CurrentAQualified2 = objWQR006.CurrentAQualified2;
                    objWQR6.FNoActual1 = objWQR006.FNoActual1;
                    objWQR6.FNoQualified1 = objWQR006.FNoQualified1;
                    objWQR6.TubeExpansionPriorToWeldingActual = objWQR006.TubeExpansionPriorToWeldingActual;
                    objWQR6.TubeExpansionPriorToWeldingQualified = objWQR006.TubeExpansionPriorToWeldingQualified;
                    objWQR6.MethodOfCleaningActual = objWQR006.MethodOfCleaningActual;
                    objWQR6.MethodOfCleaningQualified = objWQR006.MethodOfCleaningQualified;
                    objWQR6.SizeShapeOfPreplacedMetalMmActual = objWQR006.SizeShapeOfPreplacedMetalMmActual;
                    objWQR6.SizeShapeOfPreplacedMetalMmQualified = objWQR006.SizeShapeOfPreplacedMetalMmQualified;
                    objWQR6.ShieldingGasEsActual = objWQR006.ShieldingGasEsActual;
                    objWQR6.ShieldingGasEsQualified = objWQR006.ShieldingGasEsQualified;
                    objWQR6.FlowRateOfMinorGasConstituentLMinActual = objWQR006.FlowRateOfMinorGasConstituentLMinActual;
                    objWQR6.FlowRateOfMinorGasConstituentLMinQualified = objWQR006.FlowRateOfMinorGasConstituentLMinQualified;
                    objWQR6.FillerMetalGTAWPAWActual = objWQR006.FillerMetalGTAWPAWActual;
                    objWQR6.FillerMetalGTAWPAWQualified = objWQR006.FillerMetalGTAWPAWQualified;
                    objWQR6.AuxiliaryGasShieldActual = objWQR006.AuxiliaryGasShieldActual;
                    objWQR6.AuxiliaryGasShieldQualified = objWQR006.AuxiliaryGasShieldQualified;
                    objWQR6.AutomaticArcVoltageControlActual = objWQR006.AutomaticArcVoltageControlActual;
                    objWQR6.AutomaticArcVoltageControlQualified = objWQR006.AutomaticArcVoltageControlQualified;
                    objWQR6.TungstenElectrodeDiaMmActual = objWQR006.TungstenElectrodeDiaMmActual;
                    objWQR6.TungstenElectrodeDiaMmQualified = objWQR006.TungstenElectrodeDiaMmQualified;

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Record Save Successfully!";
                    objResponseMsg.HeaderId = objWQR6.Id;
                    objResponseMsg.wqtno = objWQR6.WQTNo;
                    #endregion

                }
                else if (partialName == WQRCertificateTab.Tab4.GetStringValue())
                {
                    #region Tab4
                    objWQR6 = db.WQR006.Where(i => i.Id == objWQR006.Id).FirstOrDefault();
                    objWQR6.VisualExamDate = objWQR006.VisualExamDate;
                    objWQR6.CompleteFusionObserved = objWQR006.CompleteFusionObserved;
                    objWQR6.NoBurnThroughInTubeWall = objWQR006.NoBurnThroughInTubeWall;
                    objWQR6.NoCracks = objWQR006.NoCracks;
                    objWQR6.NoPorosity = objWQR006.NoPorosity;
                    objWQR6.NoPorosityResult = objWQR006.NoPorosityResult;
                    objWQR6.NoPorosityReportNo = objWQR006.NoPorosityReportNo;
                    objWQR6.LiquidPenetrationResult = objWQR006.LiquidPenetrationResult;
                    objWQR6.LiquidPenetrationReportNo = objWQR006.LiquidPenetrationReportNo;
                    objWQR6.Required = objWQR006.Required;
                    objWQR6.ObservedMin = objWQR006.ObservedMin;
                    objWQR6.ObservedMax = objWQR006.ObservedMax;
                    objWQR6.BCrackSObserved = objWQR006.BCrackSObserved;
                    objWQR6.CCompleteFusion = objWQR006.CCompleteFusion;
                    objWQR6.DCompletePenetration = objWQR006.DCompletePenetration;
                    objWQR6.Result = objWQR006.Result;
                    objWQR6.ReportNo = objWQR006.ReportNo;
                    objWQR6.TypeOfTest1 = objWQR006.TypeOfTest1;
                    objWQR6.TypeOfTestResult1 = objWQR006.TypeOfTestResult1;
                    objWQR6.TypeOfTestReportNo1 = objWQR006.TypeOfTestReportNo1;
                    objWQR6.TypeOfTest2 = objWQR006.TypeOfTest2;
                    objWQR6.TypeOfTestResult2 = objWQR006.TypeOfTestResult2;
                    objWQR6.TypeOfTestReportNo2 = objWQR006.TypeOfTestReportNo2;
                    objWQR6.TypeOfTest3 = objWQR006.TypeOfTest3;
                    objWQR6.TypeOfTestResult3 = objWQR006.TypeOfTestResult3;
                    objWQR6.TypeOfTestReportNo3 = objWQR006.TypeOfTestReportNo3;
                    objWQR6.FilmOrSpecimenEvaluatedBy = objWQR006.FilmOrSpecimenEvaluatedBy;
                    objWQR6.MechanicalTestsConductedBy = objWQR006.MechanicalTestsConductedBy;
                    objWQR6.WeldingSupervisedBy = objWQR006.WeldingSupervisedBy;
                    objWQR6.Company = objWQR006.Company;
                    objWQR6.LabTestNo = objWQR006.LabTestNo;
                    objWQR6.InspectionByAndDescription = objWQR006.InspectionByAndDescription;
                    objWQR6.Division1 = objWQR006.Division1;
                    objWQR6.Division2 = objWQR006.Division2;
                    objWQR6.Division3 = objWQR006.Division3;
                    objWQR6.MacroResult = objWQR006.MacroResult;
                    objWQR6.MacroReportNo = objWQR006.MacroReportNo;
                    objWQR6.LiquidExamDate = objWQR006.LiquidExamDate;
                    objWQR6.MacroExamDate = objWQR006.MacroExamDate;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Record Save Successfully!";
                    objResponseMsg.HeaderId = objWQR6.Id; objResponseMsg.wqtno = objWQR6.WQTNo;
                    #endregion
                }
                else if (partialName == WQRCertificateTab.Tab5.GetStringValue())
                {
                    if (objWQR006.Id == 0)
                    {
                        objWQR6.CreatedBy = objClsLoginInfo.UserName;
                        objWQR6.CreatedOn = DateTime.Now;
                        db.WQR006.Add(objWQR6);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Record Updated Successfully!";
                        objResponseMsg.HeaderId = objWQR6.Id; objResponseMsg.wqtno = objWQR6.WQTNo;
                    }
                    else
                    {
                        objWQR6 = db.WQR006.Where(i => i.Id == objWQR006.Id).FirstOrDefault();
                        objWQR6.LastWeldDate = objWQR006.LastWeldDate;
                        string LastWeldDate = fc["LastWeldDate"] != null ? fc["LastWeldDate"].ToString() : "";
                        if (!string.IsNullOrWhiteSpace(LastWeldDate))
                        {
                            objWQR6.LastWeldDate = DateTime.ParseExact(LastWeldDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                        }
                        objWQR6.Remarks = objWQR006.Remarks;
                        objWQR6.ValueUpto = objWQR006.ValueUpto;
                        string ValueUpto = fc["ValueUpto"] != null ? fc["ValueUpto"].ToString() : "";
                        if (!string.IsNullOrWhiteSpace(ValueUpto))
                        {
                            objWQR6.ValueUpto = DateTime.ParseExact(ValueUpto, @"d/M/yyyy", CultureInfo.InvariantCulture);
                        }
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Record Save Successfully!";
                        objResponseMsg.HeaderId = objWQR6.Id; objResponseMsg.wqtno = objWQR6.WQTNo;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion

        #region Export Excel
        //Code By : nikita vibhandik 18/12/2017 (observation  14096)
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                //t#TS certificate grid
                string strFileName = string.Empty;
                ModelWQRequest objModelWQRequest = new ModelWQRequest();

                //wOpq certificate
                if (gridType == clsImplementationEnum.GridType.TTSCertificate.GetStringValue())
                {


                    var lst = db.SP_WQ_TSSCERTIFICATE_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Location = Convert.ToString(uc.Location),
                                      WQTNo = Convert.ToString(uc.WQTNo),
                                      WPSNo = Convert.ToString(uc.WPSNo),
                                      WelderName = Convert.ToString(uc.WelderName),
                                      WelderStampNo = Convert.ToString(uc.WelderStampNo),
                                      JoinType = Convert.ToString(objModelWQRequest.GetCategoryByKey("Joint Type WQ", uc.JoinType).CategoryDescription),
                                      MaintainedBy = Convert.ToString(uc.MaintainedBy),
                                      MaintainedOn = Convert.ToDateTime(uc.MaintainedOn).ToString("dd/MM/yyyy"),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }


                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region WML partial

        [SessionExpireFilter]
        [HttpPost]
        public ActionResult LoadWMLPartial(int? id, string WQTNumber, string wmltype, string Location, string jointtype)
        {
            ViewBag.Id = id;
            ViewBag.WQTNumber = WQTNumber;
            ViewBag.WMLType = wmltype;
            ViewBag.Location = Location;
            ViewBag.JointType = jointtype;
            return PartialView("~/Areas/WQ/Views/Shared/_ReleaseWMLPartialHTML.cshtml");
        }

        [HttpPost]
        public JsonResult SaveWMLTSSCertificate(int Id, string wmltype, string project)
        {
            var objResponseMsg = new clsHelper.ResponseMsg();
            var objWML003 = new WML003();
            WQR006 objWQR006 = new WQR006();
            try
            {
                bool isSave = false;
                objWQR006 = db.WQR006.FirstOrDefault(w => w.Id == Id);

                var typeASME = clsImplementationEnum.WMLType.ASME.GetStringValue();

                if (objWQR006 != null)
                {
                    objWML003 = db.WML003.Where(a => a.Location == objWQR006.Location && a.WelderName == objWQR006.WelderName && a.WelderStampNo == objWQR006.WelderStampNo && a.WMLType == wmltype && a.WQTNo == objWQR006.WQTNo && a.Project == project).FirstOrDefault();
                    if (objWML003 == null)
                    {
                        objWML003 = new WML003();
                        if (db.WML001.Any(a => a.Location == objWQR006.Location && a.Welder == objWQR006.WelderName && a.WelderStampNo == objWQR006.WelderStampNo && a.WMLType == wmltype && a.WQTNo == objWQR006.WQTNo && (a.WMLType == typeASME || a.Project == project))
                            || db.WML002.Any(a => a.Location == objWQR006.Location && a.WelderName == objWQR006.WelderName && a.WelderStampNo == objWQR006.WelderStampNo && a.WMLType == wmltype && a.WQTNo == objWQR006.WQTNo && (a.WMLType == typeASME || a.Project == project))
                            || db.WML003.Any(a => a.Location == objWQR006.Location && a.WelderName == objWQR006.WelderName && a.WelderStampNo == objWQR006.WelderStampNo && a.WMLType == wmltype && a.WQTNo == objWQR006.WQTNo && a.Project == project))
                        {
                            objResponseMsg.Value = "WQT No. is already being used";
                            objResponseMsg.Key = false;
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }
                        else { isSave = true; }
                    }
                    objWML003.MaintainedBy = objClsLoginInfo.UserName;
                    objWML003.ModifiedBy = objClsLoginInfo.UserName;
                    objWML003.MaintainedOn = DateTime.Now;
                    objWML003.ModifiedOn = DateTime.Now;
                    objWML003.Location = objWQR006.Location;
                    objWML003.SpecificationNotes = objWQR006.SpecificationNotes;
                    objWML003.WQTNo = objWQR006.WQTNo;
                    objWML003.WelderName = objWQR006.WelderName;
                    objWML003.Shop = objWQR006.Shop;
                    objWML003.WelderStampNo = objWQR006.WelderStampNo;
                    objWML003.DateOfWelding = objWQR006.DateOfWelding;
                    objWML003.WPSNo = objWQR006.WPSNo;
                    objWML003.JoinType = objWQR006.JoinType;
                    objWML003.TestCoupon = objWQR006.TestCoupon;
                    objWML003.WeldingProcessEsQualified = objWQR006.WeldingProcessEsQualified;
                    objWML003.WeldingTypeQualified = objWQR006.WeldingTypeQualified;
                    objWML003.WeldingType = objWQR006.WeldingType;
                    objWML003.Remarks = objWQR006.Remarks;
                    // if (!objWQR006.LastWeldDate.HasValue)
                    // {
                    objWML003.LastWeldDate = objWQR006.LastWeldDate;
                    objWML003.ValueUpto = objWQR006.ValueUpto;
                    //}
                    objWML003.Project = project;
                    objWML003.WMLType = wmltype;
                    objWML003.Remarks = objWQR006.Remarks;

                    if (!db.WPS025.Any(a => a.WelderPSNO == objWML003.WelderName && a.JointType == "T#TS"))
                    {
                        objWML003.WelderName = objWQR006.WelderName;
                        objWML003.Remarks = objWQR006.Remarks;
                    }

                    if (isSave)
                    {
                        objWML003.CreatedBy = objClsLoginInfo.UserName;
                        objWML003.CreatedOn = DateTime.Now;
                        db.WML003.Add(objWML003);
                        db.SaveChanges();
                        objResponseMsg.Value = "Record added successfully";
                        objResponseMsg.Key = true;
                    }
                    else
                    {
                        objWML003.EditedBy = objClsLoginInfo.UserName;
                        objWML003.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Value = "Record updated successfully";
                        objResponseMsg.Key = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}