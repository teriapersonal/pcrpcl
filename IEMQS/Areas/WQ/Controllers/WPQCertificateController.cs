﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.WQ.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace IEMQS.Areas.WQ.Controllers
{
    public class WPQCertificateController : clsBase
    {
        // GET: WQ/WPQCertificate

        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        #region WPQ Certificate List
        //index page
        public ActionResult WPQCertificateList()
        {
            ViewBag.IsDisplayOnly = false;
            return View();
        }
        //not in use
        [HttpPost]
        public ActionResult LoadWPQCertificateListPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_LoadWPQCertificateListPartial");
        }
        //datatable for index / list page
        [HttpPost]
        public JsonResult LoadWPQCertificateData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                string[] columnName = { "WQTNo", "WPSNo", "welders.Welder", "WelderStampNo", "TypeOfTestCoupon", "MaintainedBy", "MaintainedOn", "wqr.MaintainedBy + '-' + c32.t_name", "wqr.Location" };

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                var IsDisplayOnly = Convert.ToBoolean(param.IsVisible);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                strWhere += "1=1 ";//Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "", "Location");
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var lstResult = db.SP_WQ_WPQCERTIFICATE_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.Location),
                                Convert.ToString(uc.WQTNo),
                                //Convert.ToString(uc.WPSNo),
                                Convert.ToString( uc.Welder),
                                Convert.ToString(uc.WelderStampNo),
                                Convert.ToString(uc.WeldingProcessQualified),
                                Convert.ToString(uc.TypeQualified),
                                uc.LastWeldDate == null || uc.LastWeldDate.Value==DateTime.MinValue? "" :uc.LastWeldDate.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture),
                                uc.ValueUpto == null || uc.ValueUpto.Value==DateTime.MinValue? "" :uc.ValueUpto.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture),
                                //Convert.ToString(uc.TypeOfTestCoupon),
                                Convert.ToString( uc.MaintainedBy),
                                uc.MaintainedOn == null || uc.MaintainedOn.Value==DateTime.MinValue? "" :uc.MaintainedOn.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture),
                                 IsDisplayOnly?
                                 "<center>"+
                                    "<a class='' title='View Details' href='"+ WebsiteURL +"/WQ/WPQCertificate/DisplayCertificate?wqtno="+Convert.ToString(uc.WQTNo)+ Convert.ToString(!string.IsNullOrWhiteSpace(uc.Location)? "&&l="+  uc.Location.Split('-')[0]:"")+"'><i class='iconspace fa fa-eye'></i></a>"+
                                    "<i style='' title='Copy Record' class='disabledicon fa fa-files-o'></i>" +
                                    "<i style='' title='Print Report' class='iconspace fa fa-print' onClick='PrintReport("+uc.Id+")'></i>" +
                                "</center>":
                                "<center>"+
                                    "<a class='' title='View Details' href='"+ WebsiteURL +"/WQ/WPQCertificate/Certificate?wqtno="+Convert.ToString(uc.WQTNo) + Convert.ToString(!string.IsNullOrWhiteSpace(uc.Location)? "&&l="+  uc.Location.Split('-')[0]:"") + "'><i class='iconspace fa fa-eye'></i></a>" +
                                    "<i style=\"\" title=\"Copy Record\" class=\"iconspace fa fa-files-o\" onClick=\"CopyCertificate(" + uc.Id + ",'" + uc.WQTNo + "','wpq','"+ Convert.ToString(!string.IsNullOrWhiteSpace(uc.Location) ?  uc.Location.Split('-')[0] : "")+"')\"></i>" +
                                    "<i style='' title='Print Report' class='iconspace fa fa-print' onClick='PrintReport(" + uc.Id + ")'></i>" +
                                "</center>",

                            //"<center>"+
                            //    "<a class='btn btn-xs' title='View Details' href='"+ WebsiteURL +"/WQ/WPQCertificate/Certificate?wqtno="+Convert.ToString(uc.WQTNo)+"'><i class='fa fa-eye'></i></a>"+
                            //  "<i style='margin-left:5px;cursor:pointer;' title='Print Report' class='fa fa-print' onClick='PrintReport("+uc.Id+")'></i>" +
                            //    "</center>",
            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region WPQ Certificate

        //WPQ certificate by wqt no(Maintain page)
        [SessionExpireFilter]
        public ActionResult Certificate(string wqtNo, string l)
        {
            WQR004 objWQR004 = new WQR004();
            ViewBag.WQTNo = wqtNo;
            if (!string.IsNullOrWhiteSpace(wqtNo))
            {
                objWQR004 = db.WQR004.Where(i => i.WQTNo == wqtNo && i.Location == l).FirstOrDefault();
                //var objWQR003 = db.WQR003.Where(i => i.WQTNo == wqtNo).FirstOrDefault();
                //if (objWQR003 != null)
                //{
                //    //objWQR005.Location = location.Split('-')[0].Trim();
                //    //objWQR004.MaintainedOn = objWQR003.CreatedOn;
                //    //objWQR004.WQTNo = objWQR003.WQTNo;
                //}
                //else
                //{
                //    objWQR004.MaintainedOn = DateTime.Now;
                //    objWQR004.MaintainedBy = objClsLoginInfo.UserName;
                //}
                if (objWQR004 == null)
                {
                    objWQR004 = new WQR004();
                    ViewBag.Id = 0;
                    ViewBag.WQTNo = wqtNo;
                    objWQR004.WQTNo = wqtNo;
                }
                else
                {
                    ViewBag.Id = objWQR004.Id;
                }
            }
            else
            {
                ViewBag.Id = 0;
                ViewBag.WQTNo = string.Empty;
            }
            ViewBag.IsDisplayOnly = false;
            return View(objWQR004);
        }

        //Tab wise data for WPQ certificate 
        [SessionExpireFilter]
        [HttpPost]
        public ActionResult LoadTabWisePartial(int? id, string WQTNo, string partialName, bool IsDisplayOnly)
        {
            string location = string.Empty;
            string filepath = string.Empty;
            string BU = string.Empty;
            NDEModels objNDEModels = new NDEModels();
            WQR004 objWQR004 = null;
            var objWQR001 = (from w3 in db.WQR003
                             join w2 in db.WQR002 on w3.LineId equals w2.LineId
                             join w1 in db.WQR001 on w2.HeaderId equals w1.HeaderId
                             where w3.WQTNo == WQTNo
                             select new { w1.CouponType, w1.BU, w1.Location }
                               ).FirstOrDefault();
            if (objWQR001 != null)
            {
                BU = objWQR001.BU;
                location = objWQR001.Location;
                ViewBag.txtLocation = db.WQR008.Where(x => x.Location == objWQR001.Location).Select(x => x.Location + "-" + x.Description).FirstOrDefault();
            }
            else
            {
                BU = string.Empty;
                location = objClsLoginInfo.Location;
            }
            if (id > 0)
            {
                objWQR004 = db.WQR004.Where(i => i.Id == id).FirstOrDefault();
                location = objWQR004.Location;
                ViewBag.txtLocation = db.WQR008.Where(x => x.Location == objWQR004.Location).Select(x => x.Location + "-" + x.Description).FirstOrDefault();
                if (!string.IsNullOrEmpty(objWQR004.Welder)) { ViewBag.WelderName = GetIntExtWelderName(objWQR004.Welder); }
                var objWQR003 = db.WQR003.Where(i => i.WQTNo == WQTNo).FirstOrDefault();
                if (objWQR003 != null)
                {
                    if (string.IsNullOrWhiteSpace(objWQR004.WeldingProcessActualValues))
                    {
                        objWQR004.WeldingProcessActualValues = objWQR003.WeldingProcess;
                        objWQR004.WeldingProcessQualified = objWQR003.WeldingProcess;
                    }
                    if (string.IsNullOrWhiteSpace(objWQR004.TypeActual))
                    {
                        objWQR004.TypeActual = objWQR003.WeldType;
                        objWQR004.TypeQualified = objWQR003.WeldType;
                    }
                    ViewBag.Data = "disable";//disable div 
                }
            }
            else
            {

                //location = (from a in db.COM003
                //            join b in db.COM002 on a.t_loca equals b.t_dimx
                //            where b.t_dtyp == 1 && a.t_psno.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                //            select b.t_dimx).FirstOrDefault();
                var objWQR003 = db.WQR003.Where(i => i.WQTNo == WQTNo && i.Location == location).FirstOrDefault();
                objWQR004 = new WQR004();
                if (objWQR003 != null)
                {
                    //objWQR004.Location = location.Split('-')[0].Trim();
                    objWQR004.WQTNo = WQTNo;
                    objWQR004.Welder = objWQR003.Welder;
                    objWQR004.WelderStampNo = objWQR003.WelderStamp;
                    objWQR004.WPSNo = objWQR003.WPSNo;
                    objWQR004.MaintainedOn = objWQR003.CreatedOn;
                    objWQR004.MaintainedBy = objWQR003.CreatedBy;
                    objWQR004.TypeOfTestCoupon = objWQR001.CouponType;

                    ViewBag.Data = "disable";
                    if (!string.IsNullOrEmpty(objWQR004.Welder))
                    {
                        ViewBag.WelderName = GetIntExtWelderName(objWQR004.Welder);
                    }
                }
                else
                {
                    objWQR004.MaintainedOn = DateTime.Now;
                    objWQR004.MaintainedBy = objClsLoginInfo.UserName;
                }
                //objWQR004.Location = location;
            }
            if (!string.IsNullOrEmpty(objWQR004.MaintainedBy)) { ViewBag.MaintainedBy = objWQR004.MaintainedBy + " - " + Manager.GetUserNameFromPsNo(objWQR004.MaintainedBy); }
            string approved = clsImplementationEnum.CommonStatus.Approved.GetStringValue();


            ViewBag.Location = (from c2 in db.WQR008
                                where c2.Location == location
                                select c2.Location + " - " + c2.Description
                              ).FirstOrDefault();
            //if (partialName == "Tab1")
            string strlocation = objClsLoginInfo.Location;
            ViewBag.IsDisplayOnly = IsDisplayOnly;
            switch (partialName)
            {

                case "tab1":
                    filepath = "_Tab1";
                    ViewBag.JointTypeWQ = GetSubCatagory("Joint Type WQ", strlocation, BU);
                    if (!string.IsNullOrEmpty(objWQR004.JointType)) { ViewBag.JoinType = GetCategory("Joint Type WQ", objWQR004.JointType).CategoryDescription; }

                    ViewBag.CoupanType = GetSubCatagory("Coupan Type", strlocation, BU);
                    if (!string.IsNullOrEmpty(objWQR004.TypeOfTestCoupon)) { ViewBag.TypeOfTestCoupon = GetCategory("Coupan Type", objWQR004.TypeOfTestCoupon).CategoryDescription; }
                    ViewBag.WPSNo = (from a in db.WPS010
                                     where a.Status.Equals(approved, StringComparison.OrdinalIgnoreCase)
                                     select new CategoryData { Value = a.WPSNumber, CategoryDescription = a.WPSNumber }).Distinct().ToList();

                    //change by Ajay Chauhan On 16/1/2018 observation Id 14316
                    var locations = db.WQR008.ToList();
                    ViewBag.lstLocation = locations.AsEnumerable().Select(x => new CategoryData { CategoryDescription = x.Location + "-" + x.Description, Value = x.Location }).ToList();
                    break;
                case "tab2":
                    filepath = "_Tab2";
                    ViewBag.Backing = GetSubCatagory("Backing", strlocation, BU);
                    ViewBag.PlatePipe = GetSubCatagory("Plate/Pipe", strlocation, BU);
                    ViewBag.BasemetalPno = GetParentMaterial().Select(i => new { i.PNO1, i.DESCRIPTION }).Distinct().Select(i => new CategoryData { Value = i.PNO1, CategoryDescription = i.DESCRIPTION });// GetSubCatagory("Base Metal PNo", strlocation, BU);
                    ViewBag.FillerMetalProductForm = GetSubCatagory("Filler metal Product Form", strlocation, BU);
                    ViewBag.WeldingPosition = GetSubCatagory("Welding Position", strlocation, BU);
                    ViewBag.VerticalProgression = GetSubCatagory("Vertical progression", strlocation, BU);
                    ViewBag.InertGasBacking = GetSubCatagory("Inert gas backing", strlocation, BU);
                    ViewBag.TransferMode = GetSubCatagory("Transfer Mode", strlocation, BU);
                    ViewBag.CurrentType = GetSubCatagory("Current type", strlocation, BU);
                    ViewBag.WeldingProcess = GetSubCatagory("Welding Process", strlocation, BU);
                    ViewBag.WeldingType = GetSubCatagory("Welding Type", strlocation, BU);
                    ViewBag.yesNo = clsImplementationEnum.getyesno().ToArray();

                    if (!string.IsNullOrEmpty(objWQR004.BackingActual)) { ViewBag.BackingActual = GetCategory("Backing", objWQR004.BackingActual).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR004.BackingQualified)) { ViewBag.BackingQualified = GetCategory("Backing", objWQR004.BackingQualified).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR004.PlatePipeActual1)) { ViewBag.PlatePipeActual1 = GetCategory("Plate/Pipe", objWQR004.PlatePipeActual1).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR004.BaseMetalPnoActual1)) { ViewBag.BaseMetalPnoActual1 = GetParentMaterialDesc(objWQR004.BaseMetalPnoActual1); }// GetCategory("Base Metal PNo", objWQR004.BaseMetalPnoActual1).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR004.BaseMetalPnoActual2)) { ViewBag.BaseMetalPnoActual2 = GetParentMaterialDesc(objWQR004.BaseMetalPnoActual2); } //GetCategory("Base Metal PNo", objWQR004.BaseMetalPnoActual2).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR004.BaseMetalPnoQualified)) { ViewBag.BaseMetalPnoQualified = objWQR004.BaseMetalPnoQualified; }
                        //if (!string.IsNullOrEmpty(objWQR004.BaseMetalPnoQualified)) { ViewBag.BaseMetalPnoQualified = GetCategory("Base Metal PNo", objWQR004.BaseMetalPnoQualified).CategoryDescription; }
                        if (!string.IsNullOrEmpty(objWQR004.FillerMetalProductFormActual)) { ViewBag.FillerMetalProductFormActual = GetCategory("Filler metal Product Form", objWQR004.FillerMetalProductFormActual).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR004.FillerMetalProductFormQualified)) { ViewBag.FillerMetalProductFormQualified = GetCategory("Filler metal Product Form", objWQR004.FillerMetalProductFormQualified).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR004.WeldingPositionActual)) { ViewBag.WeldingPositionActual = GetCategory("Welding Position", objWQR004.WeldingPositionActual).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR004.VerticalProgressionActual)) { ViewBag.VerticalProgressionActual = GetCategory("Vertical progression", objWQR004.VerticalProgressionActual).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR004.VerticalProgressionQualified)) { ViewBag.VerticalProgressionQualified = GetCategory("Vertical progression", objWQR004.VerticalProgressionQualified).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR004.InertGasBackingActual)) { ViewBag.InertGasBackingActual = GetCategory("Inert gas backing", objWQR004.InertGasBackingActual).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR004.InertGasBackingQualified)) { ViewBag.InertGasBackingQualified = GetCategory("Inert gas backing", objWQR004.InertGasBackingQualified).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR004.TransferModeActual)) { ViewBag.TransferModeActual = GetCategory("Transfer Mode", objWQR004.TransferModeActual).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR004.TransferModeQualified)) { ViewBag.TransferModeQualified = GetCategory("Transfer Mode", objWQR004.TransferModeQualified).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR004.CurrentTypeActual)) { ViewBag.CurrentTypeActual = GetCategory("Current type", objWQR004.CurrentTypeActual).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR004.CurrentTypeQualified)) { ViewBag.CurrentTypeQualified = GetCategory("Current type", objWQR004.CurrentTypeQualified).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR004.WeldingProcessActualValues)) { ViewBag.WeldingProcessActualValues = GetCategory("Welding Process", objWQR004.WeldingProcessActualValues).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR004.WeldingProcessQualified)) { ViewBag.WeldingProcessQualified = GetCategory("Welding Process", objWQR004.WeldingProcessQualified).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR004.TypeActual)) { ViewBag.TypeActual = GetCategory("Welding Type", objWQR004.TypeActual).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR004.TypeQualified)) { ViewBag.TypeQualified = GetCategory("Welding Type", objWQR004.TypeQualified).CategoryDescription; }

                    break;
                case "tab3":
                    filepath = "_Tab3";
                    ViewBag.VisualResult = GetSubCatagory("Visual Result", strlocation, BU);
                    ViewBag.TrResult = GetSubCatagory("Tr Result", strlocation, BU);
                    ViewBag.RTResult = GetSubCatagory("Rt Result", strlocation, BU);
                    ViewBag.FilletResult = GetSubCatagory("Fillet Result", strlocation, BU);
                    ViewBag.CorrosionResistantResult = GetSubCatagory("Corrosion resistant Result", strlocation, BU);

                    if (!string.IsNullOrEmpty(objWQR004.VisualResult)) { ViewBag.lstVisualResult = GetCategory("Visual Result", objWQR004.VisualResult).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR004.TrResult)) { ViewBag.lstTrResult = GetCategory("Tr Result", objWQR004.TrResult).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR004.RTResult)) { ViewBag.lstRTResult = GetCategory("Rt Result", objWQR004.RTResult).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR004.FilletResult)) { ViewBag.lstFilletResult = GetCategory("Fillet Result", objWQR004.FilletResult).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR004.CorrosionResistantResult)) { ViewBag.lstCorrosionResistantResult = GetCategory("Corrosion resistant Result", objWQR004.CorrosionResistantResult).CategoryDescription; }

                    break;
                case "tab4":
                    filepath = "_Tab4";
                    ViewBag.InspectionBy = GetSubCatagory("Inspection by", strlocation, BU);
                    if (!string.IsNullOrEmpty(objWQR004.InspectionBy)) { ViewBag.lstInspectionBy = GetCategory("Inspection by", objWQR004.InspectionBy).CategoryDescription; }

                    objWQR004.Company = "Larsen & Toubro Limited";
                    break;
                case "tab5":
                    filepath = "_Tab5";
                    break;
                default:
                    filepath = "_Tab1";
                    break;
            }


            return PartialView(filepath, objWQR004);
        }
        public List<WPS005> GetParentMaterial()
        {
            List<WPS005> lstWPS005 = (from wps in db.WPS005
                                      select wps).ToList();
            return lstWPS005;
        }
        //not in use
        public string GetParentMaterialDesc(string code)
        {
            string parentmaterial = (from wps in db.WPS005
                                     where wps.PNO1.Trim() == code.Trim()
                                     select wps.DESCRIPTION).FirstOrDefault();
            if (string.IsNullOrWhiteSpace(parentmaterial))
            {
                parentmaterial = code;
            }
            return parentmaterial;
        }
        //Insert Update WPQ certificate
        [SessionExpireFilter]
        public ActionResult SaveWQRCertificate(WQR004 objWQR004, string partialName, FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (partialName == WQRCertificateTab.Tab1.GetStringValue())
                {
                    var lstModelWelderDetail = (from a in db.QMS003
                                                where a.WelderPSNo.Equals(objWQR004.Welder, StringComparison.OrdinalIgnoreCase)
                                                select new ModelWelderDetail { ShopCode = a.ShopCode }
                                                  )
                                                     .Union(from b in db.QMS004
                                                            where b.WelderPSNo.Equals(objWQR004.Welder, StringComparison.OrdinalIgnoreCase) && b.ActiveWithCompany != null && b.ActiveWithCompany == true
                                                            select new ModelWelderDetail { ShopCode = b.ShopCode }).FirstOrDefault();

                    if (objWQR004.Id == 0)
                    {
                        WQR004 objaddWQR004 = new WQR004();
                        WQR003 objWQR003 = db.WQR003.Where(i => i.WQTNo == objWQR004.WQTNo).FirstOrDefault();
                        //set flag of WQT No. (In WQR003)
                        if (objWQR003 != null)
                        {
                            objWQR003.CertificateGenerated = true;
                            objWQR003.CertificateGeneratedBy = objClsLoginInfo.UserName;
                            objWQR003.CertificateGeneratedOn = DateTime.Now;
                            objaddWQR004.FNo = objWQR003.FNo;
                            objaddWQR004.PNo = objWQR003.PNo;
                        }
                        else
                        {
                            objaddWQR004.FNo = "NA";
                            objaddWQR004.PNo = "NA";
                        }

                        objaddWQR004.Shop = (lstModelWelderDetail != null) ? lstModelWelderDetail.ShopCode : "";
                        objaddWQR004.TypeOfTestCoupon = objWQR004.TypeOfTestCoupon;
                        objaddWQR004.WelderStampNo = objWQR004.WelderStampNo;
                        objaddWQR004.WPSNo = objWQR004.WPSNo;

                        objaddWQR004.Location = objWQR004.Location;
                        objaddWQR004.Welder = objWQR004.Welder;
                        objaddWQR004.WQTNo = objWQR004.WQTNo;
                        objaddWQR004.TestCoupon = objWQR004.TestCoupon;
                        objaddWQR004.ProductionWeld = objWQR004.ProductionWeld;
                        objaddWQR004.SpecificationTypeGrade = objWQR004.SpecificationTypeGrade;
                        objaddWQR004.DateOfWelding = objWQR004.DateOfWelding;
                        string DateOfWelding = fc["DateOfWelding"] != null ? fc["DateOfWelding"].ToString() : "";
                        if (!string.IsNullOrWhiteSpace(DateOfWelding))
                        {
                            objaddWQR004.DateOfWelding = DateTime.ParseExact(DateOfWelding, @"d/M/yyyy", CultureInfo.InvariantCulture);
                        }
                        objaddWQR004.JointType = objWQR004.JointType;
                        objaddWQR004.MaintainedBy = objWQR004.MaintainedBy;
                        objaddWQR004.MaintainedOn = objWQR004.MaintainedOn;
                        objaddWQR004.CreatedBy = objClsLoginInfo.UserName;
                        objaddWQR004.CreatedOn = DateTime.Now;
                        db.WQR004.Add(objaddWQR004);
                        db.SaveChanges();
                        objResponseMsg.HeaderId = objaddWQR004.Id;
                        objResponseMsg.wqtno = objaddWQR004.WQTNo;
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Record Save Successfully!";
                    }
                    else
                    {
                        var objWQR004Update = db.WQR004.Where(i => i.Id == objWQR004.Id).FirstOrDefault();
                        WQR003 objWQR003 = db.WQR003.Where(i => i.WQTNo == objWQR004.WQTNo).FirstOrDefault();
                        //set flag of WQT No. (In WQR003)
                        if (objWQR003 != null)
                        {
                            objWQR003.CertificateGenerated = true;
                            objWQR003.CertificateGeneratedBy = objClsLoginInfo.UserName;
                            objWQR003.CertificateGeneratedOn = DateTime.Now;
                            objWQR004Update.FNo = objWQR003.FNo;
                            objWQR004Update.PNo = objWQR003.PNo;
                        }
                        else
                        {
                            objWQR004Update.FNo = "NA";
                            objWQR004Update.PNo = "NA";
                        }
                        objWQR004Update.JointType = objWQR004.JointType;
                        objWQR004Update.Shop = (lstModelWelderDetail != null) ? lstModelWelderDetail.ShopCode : "";

                        objWQR004Update.TypeOfTestCoupon = objWQR004.TypeOfTestCoupon;
                        objWQR004Update.WelderStampNo = objWQR004.WelderStampNo;
                        objWQR004Update.Location = objWQR004.Location;
                        objWQR004Update.WPSNo = objWQR004.WPSNo;
                        objWQR004Update.Welder = objWQR004.Welder;
                        objWQR004Update.WQTNo = objWQR004.WQTNo;
                        objWQR004Update.TestCoupon = objWQR004.TestCoupon;
                        objWQR004Update.ProductionWeld = objWQR004.ProductionWeld;
                        objWQR004Update.SpecificationTypeGrade = objWQR004.SpecificationTypeGrade;
                        objWQR004Update.DateOfWelding = objWQR004.DateOfWelding;
                        string DateOfWelding = fc["DateOfWelding"] != null ? fc["DateOfWelding"].ToString() : "";
                        if (!string.IsNullOrWhiteSpace(DateOfWelding))
                        {
                            objWQR004Update.DateOfWelding = DateTime.ParseExact(DateOfWelding, @"d/M/yyyy", CultureInfo.InvariantCulture);
                        }
                        objWQR004Update.MaintainedBy = objWQR004.MaintainedBy;
                        objWQR004Update.MaintainedOn = objWQR004.MaintainedOn;

                        db.SaveChanges();
                        objResponseMsg.HeaderId = objWQR004Update.Id;
                        objResponseMsg.wqtno = objWQR004Update.WQTNo;
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Record Save Successfully!";
                    }

                }
                else if (partialName == WQRCertificateTab.Tab2.GetStringValue())
                {

                    var objWQR004Update = db.WQR004.Where(i => i.Id == objWQR004.Id).FirstOrDefault();
                    objWQR004Update.WeldingProcessActualValues = objWQR004.WeldingProcessActualValues;
                    objWQR004Update.WeldingProcessQualified = objWQR004.WeldingProcessQualified;
                    objWQR004Update.TypeActual = objWQR004.TypeActual;
                    objWQR004Update.TypeQualified = objWQR004.TypeQualified;
                    objWQR004Update.BackingActual = objWQR004.BackingActual;
                    objWQR004Update.BackingQualified = objWQR004.BackingQualified;
                    objWQR004Update.PlatePipeActual1 = objWQR004.PlatePipeActual1;
                    objWQR004Update.PlatePipeActual2 = objWQR004.PlatePipeActual2;
                    objWQR004Update.PlatePipeQualified = objWQR004.PlatePipeQualified;
                    objWQR004Update.BaseMetalPnoActual1 = objWQR004.BaseMetalPnoActual1;
                    objWQR004Update.BaseMetalPnoActual2 = objWQR004.BaseMetalPnoActual2;
                    objWQR004Update.BaseMetalPnoQualified = objWQR004.BaseMetalPnoQualified;
                    objWQR004Update.BaseMetalThicknessActual = objWQR004.BaseMetalThicknessActual;
                    objWQR004Update.BaseMetalThicknessQualified = objWQR004.BaseMetalThicknessQualified;
                    objWQR004Update.FillerMetalSpecificationActual = objWQR004.FillerMetalSpecificationActual;
                    objWQR004Update.FillerMetalSpecificationQualified = objWQR004.FillerMetalSpecificationQualified;
                    objWQR004Update.FillerMetalClassificationActual = objWQR004.FillerMetalClassificationActual;
                    objWQR004Update.FillerMetalClassificationQualified = objWQR004.FillerMetalClassificationQualified;
                    objWQR004Update.FillerMetalFNumbersActual = objWQR004.FillerMetalFNumbersActual;
                    objWQR004Update.FillerMetalFNumbersQualified = objWQR004.FillerMetalFNumbersQualified;
                    objWQR004Update.ConsumableInsertActual = objWQR004.ConsumableInsertActual;
                    objWQR004Update.ConsumableInsertQualified = objWQR004.ConsumableInsertQualified;
                    objWQR004Update.FillerMetalProductFormActual = objWQR004.FillerMetalProductFormActual;
                    objWQR004Update.FillerMetalProductFormQualified = objWQR004.FillerMetalProductFormQualified;
                    objWQR004Update.DepositedThicknessActual1 = objWQR004.DepositedThicknessActual1;
                    objWQR004Update.DepositedThicknessActual2 = objWQR004.DepositedThicknessActual2;
                    objWQR004Update.DepositedThicknessQualified = objWQR004.DepositedThicknessQualified;
                    objWQR004Update.WeldingPositionActual = objWQR004.WeldingPositionActual;
                    objWQR004Update.CircWeldOvly = objWQR004.CircWeldOvly;
                    objWQR004Update.PlatePipePositionActual = objWQR004.PlatePipePositionActual;
                    objWQR004Update.PlatePipePositionQualified = objWQR004.PlatePipePositionQualified;
                    objWQR004Update.PipeWeldingPositionActual = objWQR004.PipeWeldingPositionActual;
                    objWQR004Update.PipeWeldingPositionQualified = objWQR004.PipeWeldingPositionQualified;
                    objWQR004Update.VerticalProgressionActual = objWQR004.VerticalProgressionActual;
                    objWQR004Update.VerticalProgressionQualified = objWQR004.VerticalProgressionQualified;
                    objWQR004Update.InertGasBackingActual = objWQR004.InertGasBackingActual;
                    objWQR004Update.InertGasBackingQualified = objWQR004.InertGasBackingQualified;
                    objWQR004Update.TransferModeActual = objWQR004.TransferModeActual;
                    objWQR004Update.TransferModeQualified = objWQR004.TransferModeQualified;
                    objWQR004Update.CurrentTypeActual = objWQR004.CurrentTypeActual;
                    objWQR004Update.CurrentTypeQualified = objWQR004.CurrentTypeQualified;
                    objWQR004Update.Other = objWQR004.Other;
                    db.SaveChanges();
                    objResponseMsg.HeaderId = objWQR004Update.Id;
                    objResponseMsg.wqtno = objWQR004Update.WQTNo;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Record Save Successfully!";
                }
                else if (partialName == WQRCertificateTab.Tab3.GetStringValue())
                {
                    var objWQR004Update = db.WQR004.Where(i => i.Id == objWQR004.Id).FirstOrDefault();
                    objWQR004Update.Groove = objWQR004.Groove;
                    objWQR004Update.HardFacingResult = objWQR004.HardFacingResult;
                    objWQR004Update.TrGrooveReport = objWQR004.TrGrooveReport;
                    objWQR004Update.TrExamDate = objWQR004.TrExamDate;
                    objWQR004Update.VisualExam = objWQR004.VisualExam;
                    objWQR004Update.VisualResult = objWQR004.VisualResult;
                    objWQR004Update.LgRootFaceBend = objWQR004.LgRootFaceBend;
                    objWQR004Update.TrRootFace = objWQR004.TrRootFace;
                    objWQR004Update.TrSideBend = objWQR004.TrSideBend;
                    objWQR004Update.TrResult = objWQR004.TrResult;
                    objWQR004Update.AltVolExam = objWQR004.AltVolExam;
                    objWQR004Update.UT = objWQR004.UT;
                    objWQR004Update.RT = objWQR004.RT;
                    objWQR004Update.RTResult = objWQR004.RTResult;
                    objWQR004Update.Rtreport = objWQR004.Rtreport;
                    objWQR004Update.Fillet = objWQR004.Fillet;
                    objWQR004Update.FilletResult = objWQR004.FilletResult;
                    objWQR004Update.FilletWeldsInPlate = objWQR004.FilletWeldsInPlate;
                    objWQR004Update.FilletWeldsInPipe = objWQR004.FilletWeldsInPipe;
                    objWQR004Update.FilletWeldFracture = objWQR004.FilletWeldFracture;
                    objWQR004Update.LengthPercentOfDefect = objWQR004.LengthPercentOfDefect;
                    objWQR004Update.MacroExamination = objWQR004.MacroExamination;
                    objWQR004Update.FilletSize = objWQR004.FilletSize;
                    objWQR004Update.ConcavityConvexity = objWQR004.ConcavityConvexity;
                    objWQR004Update.FilletReport = objWQR004.FilletReport;
                    objWQR004Update.CorrosionResistantOverlay = objWQR004.CorrosionResistantOverlay;
                    objWQR004Update.PipeBend = objWQR004.PipeBend;
                    objWQR004Update.PlateBend = objWQR004.PlateBend;
                    objWQR004Update.CorrosionResistantResult = objWQR004.CorrosionResistantResult;
                    objWQR004Update.CorrosionResistantReport = objWQR004.CorrosionResistantReport;
                    objWQR004Update.HardFacingOverlay = objWQR004.HardFacingOverlay;
                    objWQR004Update.MacroTestForPipe = objWQR004.MacroTestForPipe;
                    objWQR004Update.MacroTestForPlate = objWQR004.MacroTestForPlate;
                    objWQR004Update.LPExamination = objWQR004.LPExamination;
                    objWQR004Update.Lpreport = objWQR004.Lpreport;
                    objWQR004Update.TypeOfTest1 = objWQR004.TypeOfTest1;
                    objWQR004Update.Result1 = objWQR004.Result1;
                    objWQR004Update.ReportNo1 = objWQR004.ReportNo1;

                    objWQR004Update.TypeOfTest2 = objWQR004.TypeOfTest2;
                    objWQR004Update.Result2 = objWQR004.Result2;
                    objWQR004Update.ReportNo2 = objWQR004.ReportNo2;

                    objWQR004Update.TypeOfTest3 = objWQR004.TypeOfTest3;
                    objWQR004Update.Result3 = objWQR004.Result3;
                    objWQR004Update.ReportNo3 = objWQR004.ReportNo3;

                    objWQR004Update.TypeOfTest4 = objWQR004.TypeOfTest4;
                    objWQR004Update.Result4 = objWQR004.Result4;
                    objWQR004Update.ReportNo4 = objWQR004.ReportNo4;

                    objWQR004Update.TypeOfTest5 = objWQR004.TypeOfTest5;
                    objWQR004Update.Result5 = objWQR004.Result5;
                    objWQR004Update.ReportNo5 = objWQR004.ReportNo5;

                    objWQR004Update.TypeOfTest6 = objWQR004.TypeOfTest6;
                    objWQR004Update.Result6 = objWQR004.Result6;
                    objWQR004Update.ReportNo6 = objWQR004.ReportNo6;
                    objWQR004Update.VisualExamDate = objWQR004.VisualExamDate;
                    objWQR004Update.ResultDate = objWQR004.ResultDate;
                    objWQR004Update.CorrosionResistantDate = objWQR004.CorrosionResistantDate;
                    objWQR004Update.HardFacingtDate = objWQR004.HardFacingtDate;
                    db.SaveChanges();
                    objResponseMsg.HeaderId = objWQR004Update.Id;
                    objResponseMsg.wqtno = objWQR004Update.WQTNo; objResponseMsg.Key = true;
                    objResponseMsg.Value = "Record Save Successfully!";

                }
                else if (partialName == WQRCertificateTab.Tab4.GetStringValue())
                {
                    var objWQR004Update = db.WQR004.Where(i => i.Id == objWQR004.Id).FirstOrDefault();
                    objWQR004Update.FilmOrSpecimenEvaluatedBy = objWQR004.FilmOrSpecimenEvaluatedBy;
                    objWQR004Update.MechanicalTestsConductedBy = objWQR004.MechanicalTestsConductedBy;
                    objWQR004Update.WeldingSupervisedBy = objWQR004.WeldingSupervisedBy;
                    objWQR004Update.Company = objWQR004.Company;
                    objWQR004Update.LabTestNo = objWQR004.LabTestNo;
                    objWQR004Update.InspectionBy = objWQR004.InspectionBy;
                    objWQR004Update.Division1 = objWQR004.Division1;
                    objWQR004Update.Division2 = objWQR004.Division2;
                    objWQR004Update.Division3 = objWQR004.Division3;
                    db.SaveChanges();
                    objResponseMsg.wqtno = objWQR004Update.WQTNo; objResponseMsg.HeaderId = objWQR004Update.Id;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Record Save Successfully!";
                }
                else if (partialName == WQRCertificateTab.Tab5.GetStringValue())
                {
                    if (objWQR004.Id == 0)
                    {
                        WQR004 wqr004 = new WQR004();
                        wqr004.LastWeldDate = objWQR004.LastWeldDate;
                        string LastWeldDate = fc["LastWeldDate"] != null ? fc["LastWeldDate"].ToString() : "";
                        if (!string.IsNullOrWhiteSpace(LastWeldDate))
                        {
                            wqr004.LastWeldDate = DateTime.ParseExact(LastWeldDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                        }
                        wqr004.ValueUpto = objWQR004.ValueUpto;
                        string ValueUpto = fc["ValueUpto"] != null ? fc["ValueUpto"].ToString() : "";
                        if (!string.IsNullOrWhiteSpace(ValueUpto))
                        {
                            wqr004.ValueUpto = DateTime.ParseExact(ValueUpto, @"d/M/yyyy", CultureInfo.InvariantCulture);
                        }
                        wqr004.Remarks = objWQR004.Remarks;
                        wqr004.CreatedBy = objClsLoginInfo.UserName;
                        wqr004.CreatedOn = DateTime.Now;
                        db.WQR004.Add(wqr004);
                        db.SaveChanges();
                        objResponseMsg.wqtno = wqr004.WQTNo; objResponseMsg.HeaderId = wqr004.Id;
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Record Updated Successfully!";
                    }
                    else
                    {
                        var objWQR004Update = db.WQR004.Where(i => i.WQTNo == objWQR004.WQTNo).FirstOrDefault();
                        objWQR004Update.LastWeldDate = objWQR004.LastWeldDate;
                        string LastWeldDate = fc["LastWeldDate"] != null ? fc["LastWeldDate"].ToString() : "";
                        if (!string.IsNullOrWhiteSpace(LastWeldDate))
                        {
                            objWQR004Update.LastWeldDate = DateTime.ParseExact(LastWeldDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                        }
                        objWQR004Update.Remarks = objWQR004.Remarks;
                        objWQR004Update.ValueUpto = objWQR004.ValueUpto;
                        string ValueUpto = fc["ValueUpto"] != null ? fc["ValueUpto"].ToString() : "";
                        if (!string.IsNullOrWhiteSpace(ValueUpto))
                        {
                            objWQR004Update.ValueUpto = DateTime.ParseExact(ValueUpto, @"d/M/yyyy", CultureInfo.InvariantCulture);
                        }
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.wqtno = objWQR004Update.WQTNo; objResponseMsg.HeaderId = objWQR004Update.Id;
                        objResponseMsg.Value = "Record Save Successfully!";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region WOPQ Certificate List
        //WOPQ Certificate List
        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult WOPQCertificateList()
        {
            ViewBag.IsDisplayOnly = false;
            return View();
        }

        //Load Datatable for WOPQ
        [HttpPost]
        public JsonResult LoadWOPQCertificateData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                string[] columnName = { "Location", "WQTNo", "welders.Welder", "WPSNo", "WelderName", "wqr.MaintainedBy + '-' + c32.t_name", "wqr.Location", "WelderStampNo", "TypeOfTestCoupon", "MaintainedBy", "MaintainedOn" };

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                strWhere += "1=1 ";//Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "", "Location");

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var IsDisplayOnly = Convert.ToBoolean(param.IsVisible);
                var lstResult = db.SP_WQ_WOPQCERTIFICATE_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.Location),
                            Convert.ToString(uc.WQTNo),
                            //Convert.ToString(uc.WPSNo),
                            Convert.ToString(uc.Welder),
                            Convert.ToString(uc.WelderStampNo),
                            Convert.ToString(uc.WeldingProcessMachineQualified),
                            Convert.ToString(uc.WeldingType),
                            uc.LastWeldDate == null || uc.LastWeldDate.Value==DateTime.MinValue? "" :uc.LastWeldDate.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture),
                            uc.ValueUpto == null || uc.ValueUpto.Value==DateTime.MinValue? "" :uc.ValueUpto.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture),
                                //Convert.ToString(uc.TypeOfTestCoupon),
                            Convert.ToString( uc.MaintainedBy),
                           uc.MaintainedOn == null || uc.MaintainedOn.Value==DateTime.MinValue? "" :uc.MaintainedOn.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture),
                           IsDisplayOnly?
                                "<center>"+
                                    "<a class='' title='View Details'   href='"+ WebsiteURL +"/WQ/WPQCertificate/DisplayWOPQCertificate?wqtno="+Convert.ToString(uc.WQTNo)+ Convert.ToString(!string.IsNullOrWhiteSpace(uc.Location)? "&&l="+  uc.Location.Split('-')[0]:"")+"'><i class='iconspace fa fa-eye'></i></a>"+
                                      "<i style=\"\" title=\"Copy Record\" class=\"disabledicon fa fa-files-o\" ></i>" +
                                    "<i style=';' title='Print Report' class='iconspace fa fa-print' onClick='PrintReport("+uc.Id+")'></i>" +
                                "</center>":
                                "<center>"+
                                    "<a class='' title='View Details'   href='"+ WebsiteURL +"/WQ/WPQCertificate/WOPQCertificate?wqtno="+Convert.ToString(uc.WQTNo)+ Convert.ToString(!string.IsNullOrWhiteSpace(uc.Location) ? "&&l=" + uc.Location.Split('-')[0] : "") + "'><i class='iconspace fa fa-eye'></i></a>" +
                                      "<i style=\"\" title=\"Copy Record\" class=\"iconspace fa fa-files-o\" onClick=\"CopyCertificate("+uc.Id+",'"+uc.WQTNo+"','wopq','"+ Convert.ToString(!string.IsNullOrWhiteSpace(uc.Location) ?  uc.Location.Split('-')[0] : "")+"')\"></i>" +
                                    "<i style=';' title='Print Report' class='iconspace fa fa-print' onClick='PrintReport("+uc.Id+")'></i>" +
                                "</center>",
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region WOPQ Certificate
        //WOPQ certificate  by wqt no(Maintain page)
        [SessionExpireFilter]
        public ActionResult WOPQCertificate(string wqtno, string l)
        {
            //ViewBag.Id = id;
            WQR005 objWQR005 = new WQR005();
            ViewBag.WQTNo = wqtno;
            if (!string.IsNullOrWhiteSpace(wqtno))
            {

                objWQR005 = db.WQR005.Where(i => i.WQTNo == wqtno && i.Location == l).FirstOrDefault();
                var objWQR003 = db.WQR003.Where(i => i.WQTNo == wqtno && i.Location == l).FirstOrDefault();
                if (objWQR005 == null)
                {
                    ViewBag.Id = 0;
                    objWQR005 = new WQR005();
                    ViewBag.WQTNo = wqtno;
                    objWQR005.WQTNo = wqtno;
                }
                else
                {
                    ViewBag.Id = objWQR005.Id;
                }
            }
            else
            {
                ViewBag.Id = 0;
                ViewBag.WQTNo = string.Empty;
            }
            ViewBag.IsDisplayOnly = false;
            return View(objWQR005);
        }

        //Tab wise data for WOPQ certificate 
        [SessionExpireFilter]
        [HttpPost]
        public ActionResult LoadWOPQTabWisePartial(int? id, string WQTNo, string partialName, bool IsDisplayOnly)
        {
            string location = string.Empty;
            string filepath = string.Empty;
            string BU = string.Empty;
            NDEModels objNDEModels = new NDEModels();
            WQR005 objWQR005 = null;
            var objWQR001 = (from w3 in db.WQR003
                             join w2 in db.WQR002 on w3.LineId equals w2.LineId
                             join w1 in db.WQR001 on w2.HeaderId equals w1.HeaderId
                             where w3.WQTNo == WQTNo
                             select new { w1.CouponType, w1.BU, w1.Location }
                               ).FirstOrDefault();
            if (objWQR001 != null)
            {
                BU = objWQR001.BU;
                location = objWQR001.Location;
            }
            else
            {
                BU = string.Empty;
                location = objClsLoginInfo.Location;
            }
            if (id > 0)
            {
                objWQR005 = db.WQR005.Where(i => i.WQTNo == WQTNo && i.Id == id).FirstOrDefault();
                location = objWQR005.Location;
                ViewBag.txtLocation = db.WQR008.Where(x => x.Location == objWQR005.Location).Select(x => x.Location + "-" + x.Description).FirstOrDefault();
                var objWQR003 = db.WQR003.Where(i => i.WQTNo == WQTNo).FirstOrDefault();
                if (objWQR003 != null)
                {
                    if (string.IsNullOrWhiteSpace(objWQR005.WeldingProcessEsActual))
                    {
                        objWQR005.WeldingProcessEsActual = objWQR003.WeldingProcess;
                        objWQR005.WeldingProcessEsQualified = objWQR003.WeldingProcess;
                    }
                    if (string.IsNullOrWhiteSpace(objWQR005.TypeOfWeldingAutomaticActual))
                    {
                        objWQR005.TypeOfWeldingAutomaticActual = objWQR003.WeldType;
                        objWQR005.TypeOfWeldingAutomaticQualified = objWQR003.WeldType;
                    }
                    ViewBag.Data = "disable";
                }
            }
            else
            {
                //location = (from a in db.COM003
                //            join b in db.COM002 on a.t_loca equals b.t_dimx
                //            where b.t_dtyp == 1 && a.t_psno.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                //            select b.t_dimx).FirstOrDefault();
                objWQR005 = new WQR005();
                var objWQR003 = db.WQR003.Where(i => i.WQTNo == WQTNo).FirstOrDefault();
                if (objWQR003 != null)
                {

                    //objWQR005.Location = location.Split('-')[0].Trim();
                    objWQR005.WQTNo = WQTNo;
                    objWQR005.WelderName = objWQR003.Welder;
                    objWQR005.WelderStampNo = objWQR003.WelderStamp;
                    objWQR005.WPSNo = objWQR003.WPSNo;
                    objWQR005.WeldingType = objWQR003.WeldType;
                    objWQR005.TypeOfTestCoupon = objWQR001.CouponType;
                    objWQR005.MaintainedBy = objWQR003.CreatedBy;
                    objWQR005.MaintainedOn = objWQR003.CreatedOn;

                    ViewBag.Data = "disable";
                    ViewBag.WelderName = GetIntExtWelderName(objWQR003.Welder);
                }
                else
                {
                    objWQR005.MaintainedOn = DateTime.Now;
                    objWQR005.MaintainedBy = objClsLoginInfo.UserName;
                }
                //objWQR005.Location = location;

            }
            ViewBag.IsDisplayOnly = IsDisplayOnly;
            if (!string.IsNullOrEmpty(objWQR005.WeldingType)) { ViewBag.WeldType = GetCategory("Welding Type", objWQR005.WeldingType).CategoryDescription; }
            if (!string.IsNullOrEmpty(objWQR005.MaintainedBy)) { ViewBag.MaintainedBy = objWQR005.MaintainedBy + " - " + Manager.GetUserNameFromPsNo(objWQR005.MaintainedBy); }
            if (!string.IsNullOrEmpty(objWQR005.WelderName)) { ViewBag.WelderName = GetIntExtWelderName(objWQR005.WelderName); }

            ViewBag.Location = (from c2 in db.COM002
                                where c2.t_dimx == location
                                select c2.t_dimx + " - " + c2.t_desc
                              ).FirstOrDefault();
            string folderpath = "~/Areas/WQ/Views/WOPQCertificate/";
            string strlocation = objClsLoginInfo.Location;
            switch (partialName)
            {
                case "tab1":
                    filepath = "_WOPQTab1";
                    ViewBag.JointTypeWQ = GetSubCatagory("Joint Type WQ", strlocation, BU);
                    if (!string.IsNullOrEmpty(objWQR005.JointType)) { ViewBag.JoinType = GetCategory("Joint Type WQ", objWQR005.JointType).CategoryDescription; }

                    ViewBag.Position = GetSubCatagory("Position", strlocation, BU);
                    if (!string.IsNullOrEmpty(objWQR005.Position)) { ViewBag.PositionActual = GetCategory("Position", objWQR005.Position).CategoryDescription; }
                    ViewBag.CoupanType = GetSubCatagory("Coupan Type", strlocation, BU);
                    if (!string.IsNullOrEmpty(objWQR005.TypeOfTestCoupon)) { ViewBag.TypeOfTestCoupon = GetCategory("Coupan Type", objWQR005.TypeOfTestCoupon).CategoryDescription; }
                    ViewBag.WeldType = GetSubCatagory("Welding Type", strlocation, BU);
                    if (!string.IsNullOrEmpty(objWQR005.WeldingType)) { ViewBag.WeldingType = GetCategory("Welding Type", objWQR005.WeldingType).CategoryDescription; }

                    string approved = clsImplementationEnum.CommonStatus.Approved.GetStringValue();

                    ViewBag.WPSNo = (from a in db.WPS010
                                     where a.Status.Equals(approved, StringComparison.OrdinalIgnoreCase)
                                     select new CategoryData { Value = a.WPSNumber, CategoryDescription = a.WPSNumber }).Distinct().ToList();

                    //change by Ajay Chauhan On 16/1/2018 observation Id 14316
                    var locations = db.WQR008.ToList();
                    ViewBag.lstLocation = locations.AsEnumerable().Select(x => new CategoryData { CategoryDescription = x.Location + "-" + x.Description, Value = x.Location }).ToList();

                    break;
                case "tab2":
                    filepath = "_WOPQTab2";
                    ModelWQRequest objModelWQRequest = new ModelWQRequest();
                    ViewBag.TypeofWelding = GetSubCatagory("Welding Type", strlocation, BU); //objModelWQRequest.GetSubCatagory("Welding Type", strlocation, string.Empty);//.Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + " - " + i.Description }).Distinct().ToList();
                    ViewBag.FillerMetal = GetSubCatagory("Filler Metal (EBW/LBW)", strlocation, BU);
                    ViewBag.TypeOfLaser = GetSubCatagory("Type Of Laser", strlocation, BU);
                    ViewBag.PlatePipe = GetSubCatagory("Plate/Pipe", strlocation, BU);
                    ViewBag.DirectOrRemoteVisualControl = GetSubCatagory("Direct Or Remote Visual Control", strlocation, BU);
                    ViewBag.AutomaticArcVoltage = GetSubCatagory("Automatic Arc Voltage", strlocation, BU);
                    ViewBag.AutomaticJointTracking = GetSubCatagory("Automatic Joint Tracking", strlocation, BU);
                    ViewBag.WeldingPosition = GetSubCatagory("Welding Position", strlocation, BU);
                    ViewBag.ConsumableInsert = GetSubCatagory("Consumable Insert", strlocation, BU);
                    ViewBag.Backing = GetSubCatagory("Backing", strlocation, BU);
                    ViewBag.SingleOrMultiplePassesPerSide = GetSubCatagory("Single/Multiple Passes Per Side", strlocation, BU);
                    ViewBag.WeldingProcess = GetSubCatagory("Welding Process", strlocation, BU);

                    //Bhadresh Change Start
                    ViewBag.BasemetalPno = GetParentMaterial().Select(i => new { i.PNO1, i.DESCRIPTION }).Distinct().Select(i => new CategoryData { Value = i.PNO1, CategoryDescription = i.DESCRIPTION });// GetSubCatagory("Base Metal PNo", strlocation, BU);
                    if (!string.IsNullOrEmpty(objWQR005.BaseMetalPnoActual1)) { ViewBag.BaseMetalPnoActual1 = GetParentMaterialDesc(objWQR005.BaseMetalPnoActual1); }// GetCategory("Base Metal PNo", objWQR004.BaseMetalPnoActual1).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR005.BaseMetalPnoActual2)) { ViewBag.BaseMetalPnoActual2 = GetParentMaterialDesc(objWQR005.BaseMetalPnoActual2); } //GetCategory("Base Metal PNo", objWQR004.BaseMetalPnoActual2).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR005.BaseMetalPnoQualified)) { ViewBag.BaseMetalPnoQualified = objWQR005.BaseMetalPnoQualified; }
                    //if (!string.IsNullOrEmpty(objWQR005.BaseMetalPnoQualified)) { ViewBag.BaseMetalPnoQualified = GetCategory("Base Metal PNo", objWQR005.BaseMetalPnoQualified).CategoryDescription; }
                    //Bhadresh Change End
                    if (!string.IsNullOrEmpty(objWQR005.TypeOfWeldingAutomaticActual)) { ViewBag.TypeOfWeldingAutomaticActual = GetCategory("Welding Type", objWQR005.TypeOfWeldingAutomaticActual).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR005.TypeOfWeldingAutomaticQualified)) { ViewBag.TypeOfWeldingAutomaticQualified = GetCategory("Welding Type", objWQR005.TypeOfWeldingAutomaticQualified).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR005.FillerMetalUsedEBWLBWActual)) { ViewBag.FillerMetalActual = GetCategory("Filler Metal (EBW/LBW)", objWQR005.FillerMetalUsedEBWLBWActual).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR005.FillerMetalUsedEBWLBWQualified)) { ViewBag.FillerMetalQualified = GetCategory("Filler Metal (EBW/LBW)", objWQR005.FillerMetalUsedEBWLBWQualified).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR005.TypeOfLaserForLBWCO2ToYAGEtcActual)) { ViewBag.TypeOfLaserActual = GetCategory("Type Of Laser", objWQR005.TypeOfLaserForLBWCO2ToYAGEtcActual).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR005.TypeOfLaserForLBWCO2ToYAGEtcQualified)) { ViewBag.TypeOfLaserQualified = GetCategory("Type Of Laser", objWQR005.TypeOfLaserForLBWCO2ToYAGEtcQualified).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR005.PlatePipeEnterDiaIfPipeOrTubeActual)) { ViewBag.PlatePipeActual = GetCategory("Plate/Pipe", objWQR005.PlatePipeEnterDiaIfPipeOrTubeActual).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR005.PlatePipeEnterDiaIfPipeOrTubeActualTo)) { ViewBag.PlatePipeActualto = GetCategory("Plate/Pipe", objWQR005.PlatePipeEnterDiaIfPipeOrTubeActualTo).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR005.PlatePipeEnterDiaIfPipeOrTubeQualified)) { ViewBag.PlatePipeQualified = GetCategory("Plate/Pipe", objWQR005.PlatePipeEnterDiaIfPipeOrTubeQualified).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR005.DirectOrRemoteVisualControlActual)) { ViewBag.DirectOrRemoteVisualControlActual = GetCategory("Direct Or Remote Visual Control", objWQR005.DirectOrRemoteVisualControlActual).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR005.DirectOrRemoteVisualControlQualified)) { ViewBag.DirectOrRemoteVisualControlQualified = GetCategory("Direct Or Remote Visual Control", objWQR005.DirectOrRemoteVisualControlQualified).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR005.AutomaticArcVoltageControlActual)) { ViewBag.AutomaticArcVoltageActual = GetCategory("Automatic Arc Voltage", objWQR005.AutomaticArcVoltageControlActual).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR005.AutomaticArcVoltageControlQualified)) { ViewBag.AutomaticArcVoltageQualified = GetCategory("Automatic Arc Voltage", objWQR005.AutomaticArcVoltageControlQualified).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR005.AutomaticJointTrackingActual)) { ViewBag.AutomaticJointTrackingActual = GetCategory("Automatic Joint Tracking", objWQR005.AutomaticJointTrackingActual).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR005.AutomaticJointTrackingQualified)) { ViewBag.AutomaticJointTrackingQualified = GetCategory("Automatic Joint Tracking", objWQR005.AutomaticJointTrackingQualified).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR005.WeldingPositionActual)) { ViewBag.WeldingPositionActual = GetCategory("Welding Position", objWQR005.WeldingPositionActual).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR005.ConsumableInsertActual)) { ViewBag.ConsumableInsertActual = GetCategory("Consumable Insert", objWQR005.ConsumableInsertActual).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR005.ConsumableInsertQualified)) { ViewBag.ConsumableInsertQualified = GetCategory("Consumable Insert", objWQR005.ConsumableInsertQualified).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR005.BackingActual)) { ViewBag.BackingActual = GetCategory("Backing", objWQR005.BackingActual).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR005.BackingQualified)) { ViewBag.BackingQualified = GetCategory("Backing", objWQR005.BackingQualified).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR005.SingleOrMultiplePassesPerSideActual)) { ViewBag.SingleOrMultiplePassesPerSideActual = GetCategory("Single/Multiple Passes Per Side", objWQR005.SingleOrMultiplePassesPerSideActual).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR005.SingleOrMultiplePassesPerSideQualified)) { ViewBag.SingleOrMultiplePassesPerSideQualified = GetCategory("Single/Multiple Passes Per Side", objWQR005.SingleOrMultiplePassesPerSideQualified).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR005.WeldingProcessEsQualified)) { ViewBag.WeldingProcessEsQualified = GetCategory("Welding Process", objWQR005.WeldingProcessEsQualified).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR005.WeldingProcessEsActual)) { ViewBag.WeldingProcessEsActual = GetCategory("Welding Process", objWQR005.WeldingProcessEsActual).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR005.WeldingProcessMachineQualified)) { ViewBag.WeldingProcessMachineQualified = GetCategory("Welding Process", objWQR005.WeldingProcessMachineQualified).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR005.WeldingProcessMachineActual)) { ViewBag.WeldingProcessMachineActual = GetCategory("Welding Process", objWQR005.WeldingProcessMachineActual).CategoryDescription; }

                    break;
                case "tab3":
                    filepath = "_WOPQTab3";
                    ViewBag.VisualResult = GetSubCatagory("Visual Result", strlocation, BU);
                    ViewBag.TrResult = GetSubCatagory("Tr Result", strlocation, BU);
                    ViewBag.RTResult = GetSubCatagory("Rt Result", strlocation, BU);
                    ViewBag.FilletResult = GetSubCatagory("Fillet Result", strlocation, BU);
                    ViewBag.CorrosionResistantResult = GetSubCatagory("Corrosion resistant Result", strlocation, BU);

                    if (!string.IsNullOrEmpty(objWQR005.VisualResult)) { ViewBag.lstVisualResult = GetCategory("Visual Result", objWQR005.VisualResult).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR005.LgRootResult)) { ViewBag.lstTrResult = GetCategory("Tr Result", objWQR005.LgRootResult).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR005.AltVolResult)) { ViewBag.lstRTResult = GetCategory("Rt Result", objWQR005.AltVolResult).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR005.FilletResult)) { ViewBag.lstFilletResult = GetCategory("Fillet Result", objWQR005.FilletResult).CategoryDescription; }
                    if (!string.IsNullOrEmpty(objWQR005.CorrosionResult)) { ViewBag.lstCorrosionResistantResult = GetCategory("Corrosion resistant Result", objWQR005.CorrosionResult).CategoryDescription; }

                    break;
                case "tab4":
                    filepath = "_WOPQTab4";
                    ViewBag.InspectionBy = GetSubCatagory("Inspection by", strlocation, BU);
                    if (!string.IsNullOrEmpty(objWQR005.InspectionByAndDescription)) { ViewBag.lstInspectionBy = GetCategory("Inspection by", objWQR005.InspectionByAndDescription).CategoryDescription; }
                    objWQR005.Company = "Larsen & Toubro Limited";
                    break;
                case "tab5":
                    filepath = "_WOPQTab5";
                    break;
                default:
                    filepath = "_WOPQTab1";
                    ViewBag.Position = GetSubCatagory("Position", strlocation, BU);
                    if (!string.IsNullOrEmpty(objWQR005.Position)) { ViewBag.PositionActual = GetCategory("Position", objWQR005.Position).CategoryDescription; }
                    break;
            }
            string strFilepath = folderpath + filepath + ".cshtml";

            return PartialView(strFilepath, objWQR005);
        }

        //Insert Update WOPQ certificate
        [SessionExpireFilter]
        public ActionResult SaveWOPQCertificate(WQR005 objWQR005, string partialName, FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (partialName == WQRCertificateTab.Tab1.GetStringValue())
                {
                    var lstModelWelderDetail = (from a in db.QMS003
                                                where a.WelderPSNo.Equals(objWQR005.WelderName, StringComparison.OrdinalIgnoreCase)
                                                select new ModelWelderDetail { ShopCode = a.ShopCode }
                                                 )
                                                    .Union(from b in db.QMS004
                                                           where b.WelderPSNo.Equals(objWQR005.WelderName, StringComparison.OrdinalIgnoreCase) && b.ActiveWithCompany != null && b.ActiveWithCompany == true
                                                           select new ModelWelderDetail { ShopCode = b.ShopCode }).FirstOrDefault();

                    if (objWQR005.Id == 0)
                    {
                        WQR005 objaddWQR005 = new WQR005();
                        objaddWQR005.Shop = lstModelWelderDetail != null ? lstModelWelderDetail.ShopCode : "";
                        var objWQR003 = db.WQR003.Where(i => i.WQTNo == objaddWQR005.WQTNo).FirstOrDefault();
                        if (objWQR003 != null)
                        {
                            objWQR003.CertificateGenerated = true;
                            objWQR003.CertificateGeneratedBy = objClsLoginInfo.UserName;
                            objWQR003.CertificateGeneratedOn = DateTime.Now;
                            objaddWQR005.FNo = objWQR003.FNo;
                            objaddWQR005.PNo = objWQR003.PNo;
                        }
                        else
                        {
                            objaddWQR005.FNo = "NA";
                            objaddWQR005.PNo = "NA";
                        }
                        objaddWQR005.Location = objWQR005.Location;
                        objaddWQR005.WQTNo = objWQR005.WQTNo;
                        objaddWQR005.WelderName = objWQR005.WelderName;
                        objaddWQR005.WelderStampNo = objWQR005.WelderStampNo;
                        objaddWQR005.WeldingType = objWQR005.WeldingType;
                        objaddWQR005.DateOfWelding = objWQR005.DateOfWelding;
                        string DateOfWelding = fc["DateOfWelding"] != null ? fc["DateOfWelding"].ToString() : "";
                        if (!string.IsNullOrWhiteSpace(DateOfWelding))
                        {
                            objaddWQR005.DateOfWelding = DateTime.ParseExact(DateOfWelding, @"d/M/yyyy", CultureInfo.InvariantCulture);
                        }

                        objaddWQR005.WPSNo = objWQR005.WPSNo;

                        objaddWQR005.TypeOfTestCoupon = objWQR005.TypeOfTestCoupon;
                        objaddWQR005.SpecificationTypeGrade = objWQR005.SpecificationTypeGrade;
                        objaddWQR005.FillerMetalSpecification = objWQR005.FillerMetalSpecification;
                        objaddWQR005.FillerElectrodeclassification = objWQR005.FillerElectrodeclassification;
                        objaddWQR005.TestCoupon = objWQR005.TestCoupon;
                        objaddWQR005.ProductionWeld = objWQR005.ProductionWeld;
                        objaddWQR005.Position = objWQR005.Position;
                        objaddWQR005.MaintainedOn = objWQR005.MaintainedOn;
                        objaddWQR005.MaintainedBy = objWQR005.MaintainedBy;
                        objaddWQR005.CreatedBy = objClsLoginInfo.UserName;
                        objaddWQR005.CreatedOn = DateTime.Now;
                        objaddWQR005.ThicknessMm = objWQR005.ThicknessMm;
                        objaddWQR005.JointType = objWQR005.JointType;
                        db.WQR005.Add(objaddWQR005);
                        db.SaveChanges();
                        objResponseMsg.HeaderId = objaddWQR005.Id;
                        objResponseMsg.wqtno = objaddWQR005.WQTNo;
                    }
                    else
                    {
                        var objWQR005Update = db.WQR005.Where(i => i.Id == objWQR005.Id).FirstOrDefault();
                        var objWQR003 = db.WQR003.Where(i => i.WQTNo == objWQR005.WQTNo).FirstOrDefault();
                        if (objWQR003 != null)
                        {
                            objWQR003.CertificateGenerated = true;
                            objWQR003.CertificateGeneratedBy = objClsLoginInfo.UserName;
                            objWQR003.CertificateGeneratedOn = DateTime.Now;
                            objWQR005Update.FNo = objWQR003.FNo;
                            objWQR005Update.PNo = objWQR003.PNo;
                        }
                        else
                        {
                            objWQR005Update.FNo = "NA";
                            objWQR005Update.PNo = "NA";
                        }
                        objWQR005Update.JointType = objWQR005.JointType;
                        objWQR005Update.Shop = lstModelWelderDetail != null ? lstModelWelderDetail.ShopCode : "";
                        objWQR005Update.Location = objWQR005.Location;
                        objWQR005Update.WQTNo = objWQR005.WQTNo;
                        objWQR005Update.WelderName = objWQR005.WelderName;
                        objWQR005Update.WelderStampNo = objWQR005.WelderStampNo;
                        objWQR005Update.WeldingType = objWQR005.WeldingType;
                        objWQR005Update.DateOfWelding = objWQR005.DateOfWelding;
                        string DateOfWelding = fc["DateOfWelding"] != null ? fc["DateOfWelding"].ToString() : "";
                        if (!string.IsNullOrWhiteSpace(DateOfWelding))
                        {
                            objWQR005Update.DateOfWelding = DateTime.ParseExact(DateOfWelding, @"d/M/yyyy", CultureInfo.InvariantCulture);
                        }
                        objWQR005Update.WPSNo = objWQR005.WPSNo;

                        objWQR005Update.TypeOfTestCoupon = objWQR005.TypeOfTestCoupon;
                        objWQR005Update.SpecificationTypeGrade = objWQR005.SpecificationTypeGrade;
                        objWQR005Update.FillerMetalSpecification = objWQR005.FillerMetalSpecification;
                        objWQR005Update.FillerElectrodeclassification = objWQR005.FillerElectrodeclassification;
                        objWQR005Update.TestCoupon = objWQR005.TestCoupon;
                        objWQR005Update.ProductionWeld = objWQR005.ProductionWeld;
                        objWQR005Update.Position = objWQR005.Position;
                        objWQR005Update.ThicknessMm = objWQR005.ThicknessMm;
                        objWQR005Update.MaintainedOn = objWQR005.MaintainedOn;
                        objWQR005Update.MaintainedBy = objWQR005.MaintainedBy;

                        db.SaveChanges();
                        objResponseMsg.wqtno = objWQR005Update.WQTNo;
                        objResponseMsg.HeaderId = objWQR005Update.Id;
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Record Save Successfully!";
                }
                else if (partialName == WQRCertificateTab.Tab2.GetStringValue())
                {

                    var objWQR005Update = db.WQR005.Where(i => i.Id == objWQR005.Id).FirstOrDefault();
                    objWQR005Update.TypeOfWeldingAutomaticActual = objWQR005.TypeOfWeldingAutomaticActual;
                    objWQR005Update.TypeOfWeldingAutomaticQualified = objWQR005.TypeOfWeldingAutomaticQualified;
                    //Bhadresh Change Start
                    objWQR005Update.BaseMetalPnoActual1 = objWQR005.BaseMetalPnoActual1;
                    objWQR005Update.BaseMetalPnoActual2 = objWQR005.BaseMetalPnoActual2;
                    objWQR005Update.BaseMetalPnoQualified = objWQR005.BaseMetalPnoQualified;
                    objWQR005Update.BaseMetalThicknessActual = objWQR005.BaseMetalThicknessActual;
                    objWQR005Update.BaseMetalThicknessQualified = objWQR005.BaseMetalThicknessQualified;
                    //Badresh Change End
                    objWQR005Update.WeldingProcessEsQualified = objWQR005.WeldingProcessEsQualified;
                    objWQR005Update.WeldingProcessEsActual = objWQR005.WeldingProcessEsActual;
                    objWQR005Update.WeldingProcessMachineActual = objWQR005.WeldingProcessMachineActual;
                    objWQR005Update.WeldingProcessMachineQualified = objWQR005.WeldingProcessMachineQualified;
                    objWQR005Update.FillerMetalUsedEBWLBWActual = objWQR005.FillerMetalUsedEBWLBWActual;
                    objWQR005Update.FillerMetalUsedEBWLBWQualified = objWQR005.FillerMetalUsedEBWLBWQualified;
                    objWQR005Update.TypeOfLaserForLBWCO2ToYAGEtcActual = objWQR005.TypeOfLaserForLBWCO2ToYAGEtcActual;
                    objWQR005Update.TypeOfLaserForLBWCO2ToYAGEtcQualified = objWQR005.TypeOfLaserForLBWCO2ToYAGEtcQualified;
                    objWQR005Update.ContinuousDriveOrInertiaWeldingFWActual = objWQR005.ContinuousDriveOrInertiaWeldingFWActual;
                    objWQR005Update.ContinuousDriveOrInertiaWeldingFWQualified = objWQR005.ContinuousDriveOrInertiaWeldingFWQualified;
                    objWQR005Update.VacuumOrOutOfVacuumEBWActual = objWQR005.VacuumOrOutOfVacuumEBWActual;
                    objWQR005Update.VacuumOrOutOfVacuumEBWQualified = objWQR005.VacuumOrOutOfVacuumEBWQualified;
                    objWQR005Update.TypeOfWeldingMachineActual = objWQR005.TypeOfWeldingMachineActual;
                    objWQR005Update.TypeOfWeldingMachineQualified = objWQR005.TypeOfWeldingMachineQualified;
                    objWQR005Update.PlatePipeEnterDiaIfPipeOrTubeActual = objWQR005.PlatePipeEnterDiaIfPipeOrTubeActual;
                    objWQR005Update.PlatePipeEnterDiaIfPipeOrTubeActualTo = objWQR005.PlatePipeEnterDiaIfPipeOrTubeActualTo;
                    objWQR005Update.PlatePipeEnterDiaIfPipeOrTubeQualified = objWQR005.PlatePipeEnterDiaIfPipeOrTubeQualified;
                    objWQR005Update.DirectOrRemoteVisualControlActual = objWQR005.DirectOrRemoteVisualControlActual;
                    objWQR005Update.DirectOrRemoteVisualControlQualified = objWQR005.DirectOrRemoteVisualControlQualified;
                    objWQR005Update.AutomaticArcVoltageControlActual = objWQR005.AutomaticArcVoltageControlActual;
                    objWQR005Update.AutomaticArcVoltageControlQualified = objWQR005.AutomaticArcVoltageControlQualified;
                    objWQR005Update.AutomaticJointTrackingActual = objWQR005.AutomaticJointTrackingActual;
                    objWQR005Update.AutomaticJointTrackingQualified = objWQR005.AutomaticJointTrackingQualified;
                    objWQR005Update.WeldingPositionActual = objWQR005.WeldingPositionActual;
                    objWQR005Update.CircWeldOvly = objWQR005.CircWeldOvly;
                    objWQR005Update.IPlatePipeOverlayWeldingPositionQualified = objWQR005.IPlatePipeOverlayWeldingPositionQualified;
                    objWQR005Update.IPlatePipeFilletWeldingPositionQualified = objWQR005.IPlatePipeFilletWeldingPositionQualified;
                    objWQR005Update.IiPipeOverlayWeldingPositionQualified = objWQR005.IiPipeOverlayWeldingPositionQualified;
                    objWQR005Update.IiPipeFilletWeldingPositionQualified = objWQR005.IiPipeFilletWeldingPositionQualified;
                    objWQR005Update.ConsumableInsertActual = objWQR005.ConsumableInsertActual;
                    objWQR005Update.ConsumableInsertQualified = objWQR005.ConsumableInsertQualified;
                    objWQR005Update.BackingActual = objWQR005.BackingActual;
                    objWQR005Update.BackingQualified = objWQR005.BackingQualified;
                    objWQR005Update.SingleOrMultiplePassesPerSideActual = objWQR005.SingleOrMultiplePassesPerSideActual;
                    objWQR005Update.SingleOrMultiplePassesPerSideQualified = objWQR005.SingleOrMultiplePassesPerSideQualified;
                    objWQR005Update.Other = objWQR005.Other;
                    db.SaveChanges();
                    objResponseMsg.HeaderId = objWQR005Update.Id; objResponseMsg.wqtno = objWQR005Update.WQTNo;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Record Save Successfully!";
                }
                else if (partialName == WQRCertificateTab.Tab3.GetStringValue())
                {
                    var objWQR005Update = db.WQR005.Where(i => i.Id == objWQR005.Id).FirstOrDefault();
                    objWQR005Update.VisualExamOfCompWeld = objWQR005.VisualExamOfCompWeld;
                    objWQR005Update.LgRootFaceBend = objWQR005.LgRootFaceBend;
                    objWQR005Update.AltVolExam = objWQR005.AltVolExam;
                    objWQR005Update.Groove = objWQR005.Groove;                //bhadresh added
                    objWQR005Update.VisualResult = objWQR005.VisualResult;
                    objWQR005Update.TrRootFace = objWQR005.TrRootFace;
                    objWQR005Update.HardFacingResult = objWQR005.HardFacingResult;
                    objWQR005Update.TrGrooveReport = objWQR005.TrGrooveReport;
                    objWQR005Update.TrExamDate = objWQR005.TrExamDate;
                    objWQR005Update.UT = objWQR005.UT;
                    objWQR005Update.TrSideBend = objWQR005.TrSideBend;
                    objWQR005Update.LgRootResult = objWQR005.LgRootResult;
                    objWQR005Update.RT = objWQR005.RT;
                    objWQR005Update.AltVolResult = objWQR005.AltVolResult;
                    objWQR005Update.GrooveReport = objWQR005.GrooveReport;
                    objWQR005Update.Fillet = objWQR005.Fillet;
                    objWQR005Update.FilletResult = objWQR005.FilletResult;
                    objWQR005Update.FilletWeldsInPlate = objWQR005.FilletWeldsInPlate;
                    objWQR005Update.FilletWeldFractureTest = objWQR005.FilletWeldFractureTest;
                    objWQR005Update.MacroExamination = objWQR005.MacroExamination;
                    objWQR005Update.FilletReportNo = objWQR005.FilletReportNo;
                    objWQR005Update.FilletWeldsInPipe = objWQR005.FilletWeldsInPipe;
                    objWQR005Update.LengthAndPercentOfDefect = objWQR005.LengthAndPercentOfDefect;
                    objWQR005Update.FilletSizeMm = objWQR005.FilletSizeMm;
                    objWQR005Update.ConcavityConvexityMm = objWQR005.ConcavityConvexityMm;
                    objWQR005Update.CorrosionResistantOverlay = objWQR005.CorrosionResistantOverlay;
                    objWQR005Update.PipeBend = objWQR005.PipeBend;
                    objWQR005Update.PlateBend = objWQR005.PlateBend;
                    objWQR005Update.CorrosionResult = objWQR005.CorrosionResult;
                    objWQR005Update.CorrosionReportNo = objWQR005.CorrosionReportNo;
                    objWQR005Update.HardFacingOverlay = objWQR005.HardFacingOverlay;
                    objWQR005Update.MacroTestForPipe = objWQR005.MacroTestForPipe;
                    objWQR005Update.MacroTestForPlate = objWQR005.MacroTestForPlate;
                    objWQR005Update.LPExamination = objWQR005.LPExamination;
                    objWQR005Update.HardFacingReport = objWQR005.HardFacingReport;
                    objWQR005Update.TypeOfTest1 = objWQR005.TypeOfTest1;
                    objWQR005Update.Result1 = objWQR005.Result1;
                    objWQR005Update.ReportNo1 = objWQR005.ReportNo1;
                    objWQR005Update.TypeOfTest2 = objWQR005.TypeOfTest2;
                    objWQR005Update.Result2 = objWQR005.Result2;
                    objWQR005Update.ReportNo2 = objWQR005.ReportNo2;
                    objWQR005Update.TypeOfTest3 = objWQR005.TypeOfTest3;
                    objWQR005Update.Result3 = objWQR005.Result3;
                    objWQR005Update.ReportNo3 = objWQR005.ReportNo3;
                    objWQR005Update.TypeOfTest4 = objWQR005.TypeOfTest4;
                    objWQR005Update.Result4 = objWQR005.Result4;
                    objWQR005Update.ReportNo4 = objWQR005.ReportNo4;
                    objWQR005Update.TypeOfTest5 = objWQR005.TypeOfTest5;
                    objWQR005Update.Result5 = objWQR005.Result5;
                    objWQR005Update.ReportNo5 = objWQR005.ReportNo5;
                    objWQR005Update.TypeOfTest6 = objWQR005.TypeOfTest6;
                    objWQR005Update.Result6 = objWQR005.Result6;
                    objWQR005Update.ReportNo6 = objWQR005.ReportNo6;
                    objWQR005Update.VisualExamDate = objWQR005.VisualExamDate;
                    objWQR005Update.ResultDate = objWQR005.ResultDate;
                    objWQR005Update.CorrosionResistantDate = objWQR005.CorrosionResistantDate;
                    objWQR005Update.HardFacingtDate = objWQR005.HardFacingtDate;
                    db.SaveChanges();
                    objResponseMsg.HeaderId = objWQR005Update.Id; objResponseMsg.wqtno = objWQR005Update.WQTNo;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Record Save Successfully!";
                }
                else if (partialName == WQRCertificateTab.Tab4.GetStringValue())
                {
                    var objWQR005Update = db.WQR005.Where(i => i.Id == objWQR005.Id).FirstOrDefault();
                    objWQR005Update.FilmOrSpecimenEvaluatedBy = objWQR005.FilmOrSpecimenEvaluatedBy;
                    objWQR005Update.MechanicalTestsConductedBy = objWQR005.MechanicalTestsConductedBy;
                    objWQR005Update.WeldingSupervisedBy = objWQR005.WeldingSupervisedBy;
                    objWQR005Update.Company = objWQR005.Company;
                    objWQR005Update.LabTestNo = objWQR005.LabTestNo;
                    objWQR005Update.InspectionByAndDescription = objWQR005.InspectionByAndDescription;
                    objWQR005Update.Division1 = objWQR005.Division1;
                    objWQR005Update.Division2 = objWQR005.Division2;
                    objWQR005Update.Division3 = objWQR005.Division3;
                    db.SaveChanges();
                    objResponseMsg.HeaderId = objWQR005Update.Id; objResponseMsg.wqtno = objWQR005Update.WQTNo;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Record Save Successfully!";
                }
                else if (partialName == WQRCertificateTab.Tab5.GetStringValue())
                {
                    if (objWQR005.Id == 0)
                    {
                        objWQR005.CreatedBy = objClsLoginInfo.UserName;
                        objWQR005.CreatedOn = DateTime.Now;
                        db.WQR005.Add(objWQR005);
                        db.SaveChanges();
                        objResponseMsg.HeaderId = objWQR005.Id; objResponseMsg.wqtno = objWQR005.WQTNo;
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Record Updated Successfully!";
                    }
                    else
                    {
                        var objWQR005Update = db.WQR005.Where(i => i.Id == objWQR005.Id).FirstOrDefault();
                        objWQR005Update.LastWeldDate = objWQR005.LastWeldDate;
                        string LastWeldDate = fc["LastWeldDate"] != null ? fc["LastWeldDate"].ToString() : "";
                        if (!string.IsNullOrWhiteSpace(LastWeldDate))
                        {
                            objWQR005Update.LastWeldDate = DateTime.ParseExact(LastWeldDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                        }
                        objWQR005Update.Remarks = objWQR005.Remarks;
                        objWQR005Update.ValueUpto = objWQR005.ValueUpto;
                        string ValueUpto = fc["ValueUpto"] != null ? fc["ValueUpto"].ToString() : "";
                        if (!string.IsNullOrWhiteSpace(ValueUpto))
                        {
                            objWQR005Update.ValueUpto = DateTime.ParseExact(ValueUpto, @"d/M/yyyy", CultureInfo.InvariantCulture);
                        }

                        db.SaveChanges();
                        objResponseMsg.HeaderId = objWQR005Update.Id; objResponseMsg.wqtno = objWQR005Update.WQTNo;
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Record Save Successfully!";
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Record Save Successfully!";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Display Wpq List
        // [SessionExpireFilter]
        // [UserPermissions]
        public ActionResult DisplayWpqCertificateList()
        {
            ViewBag.IsDisplayOnly = true;
            return View("WPQCertificateList");
        }
        public ActionResult DisplayCertificate(string wqtNo, string l)
        {
            WQR004 objWQR004 = new WQR004();
            ViewBag.WQTNo = wqtNo;
            if (!string.IsNullOrWhiteSpace(wqtNo))
            {
                objWQR004 = db.WQR004.Where(i => i.WQTNo == wqtNo && i.Location == l).FirstOrDefault();

                if (objWQR004 == null)
                {
                    objWQR004 = new WQR004();
                    ViewBag.Id = 0;
                    ViewBag.WQTNo = wqtNo;
                    objWQR004.WQTNo = wqtNo;
                }
                else
                {
                    ViewBag.Id = objWQR004.Id;
                }
            }
            else
            {
                ViewBag.Id = 0;
                ViewBag.WQTNo = string.Empty;
            }
            ViewBag.IsDisplayOnly = true;
            return View("Certificate", objWQR004);
        }


        #endregion

        #region Display Wopq List
        public ActionResult DisplayWOPQCertificateList()
        {
            ViewBag.IsDisplayOnly = true;
            return View("WOPQCertificateList");
        }
        public ActionResult DisplayWOPQCertificate(string wqtno, string l)
        {
            //ViewBag.Id = id;
            WQR005 objWQR005 = new WQR005();
            ViewBag.WQTNo = wqtno;
            if (!string.IsNullOrWhiteSpace(wqtno))
            {

                objWQR005 = db.WQR005.Where(i => i.WQTNo == wqtno && i.Location == l).FirstOrDefault();
                var objWQR003 = db.WQR003.Where(i => i.WQTNo == wqtno && i.Location == l).FirstOrDefault();
                if (objWQR005 == null)
                {
                    ViewBag.Id = 0;
                    objWQR005 = new WQR005();
                    ViewBag.WQTNo = wqtno;
                    objWQR005.WQTNo = wqtno;
                }
                else
                {
                    ViewBag.Id = objWQR005.Id;
                }
            }
            else
            {
                ViewBag.Id = 0;
                ViewBag.WQTNo = string.Empty;
            }
            ViewBag.IsDisplayOnly = true;
            return View("WOPQCertificate", objWQR005);
        }
        #endregion

        #region Common Functions
        //category by Location and Bu
        public IQueryable<CategoryData> GetSubCatagory(string Key, string strLoc, string BU = "")
        {
            IQueryable<CategoryData> lstGLB002 = null;
            if (BU != string.Empty) //Get BU and Location Base Sub Category
            {
                lstGLB002 = (from glb002 in db.GLB002
                             join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                             where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.BU.Equals(BU, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true && strLoc.Equals(glb002.Location)
                             select glb002).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Description }).Distinct();
            }
            else //Get Location Base Sub Category
            {
                lstGLB002 = (from glb002 in db.GLB002
                             join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                             where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true && strLoc.Equals(glb002.Location)
                             select glb002).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Description }).Distinct();
            }
            return lstGLB002;
        }
        public JsonResult GetSubCatagoryForAutoComplete(string Key, string strLoc, string BU = "")
        {
            List<AutoCompleteList> lstData = null;
            if (BU != string.Empty) //Get BU and Location Base Sub Category
            {
                lstData = (from glb002 in db.GLB002
                           join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                           where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.BU.Equals(BU, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true && strLoc.Equals(glb002.Location)
                           select new AutoCompleteList { id = glb002.Code, value = glb002.Description, label = glb002.Code + " - " + glb002.Description }).Distinct().ToList();
            }
            else //Get Location Base Sub Category
            {
                lstData = (from glb002 in db.GLB002
                           join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                           where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true && strLoc.Equals(glb002.Location)
                           select new AutoCompleteList { id = glb002.Code, value = glb002.Description, label = glb002.Code + " - " + glb002.Description }).Distinct().ToList();
            }
            return Json(lstData, JsonRequestBehavior.AllowGet);
        }

        public CategoryData GetCategory(string mainCategory, string categoryCode, bool isOnlyDescriptionRequired = false)
        {
            CategoryData objGLB002 = new CategoryData();
            ModelWQRequest obj = new ModelWQRequest();
            if (obj.IsCategorywithCodeExist(mainCategory, categoryCode))
            {
                if (!string.IsNullOrEmpty(categoryCode))
                {
                    if (isOnlyDescriptionRequired)
                    {
                        objGLB002 = db.GLB002.Where(i => i.Code.Equals(categoryCode, StringComparison.OrdinalIgnoreCase)).Select(i => new CategoryData { Code = i.Code, CategoryDescription = i.Description }).FirstOrDefault();
                        objGLB002 = (from glb002 in db.GLB002
                                     join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                                     where glb001.Category.Equals(mainCategory, StringComparison.OrdinalIgnoreCase) && glb002.Code.Equals(categoryCode) && glb002.IsActive == true
                                     select new CategoryData { Code = glb002.Code, CategoryDescription = glb002.Description }
                                     ).FirstOrDefault();
                    }
                    else
                    {
                        objGLB002 = (from glb002 in db.GLB002
                                     join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                                     where glb001.Category.Equals(mainCategory, StringComparison.OrdinalIgnoreCase) && glb002.Code.Equals(categoryCode) && glb002.IsActive == true
                                     select new CategoryData { Code = glb002.Code, CategoryDescription = glb002.Description }
                                    ).FirstOrDefault();
                    }
                }
            }
            else
            {
                objGLB002.CategoryDescription = categoryCode;
            }

            return objGLB002;
        }


        //WPQ certificate by wqr003 id
        public ActionResult Certificate_old(int? id)
        {
            WQR004 objWQR004 = new WQR004();
            ViewBag.Id = id;
            if (id > 0)
            {
                string WQTNo = db.WQR003.Where(i => i.Id == id).FirstOrDefault().WQTNo;
                ViewBag.WQTNo = WQTNo;
                objWQR004 = db.WQR004.Where(i => i.WQTNo == WQTNo).FirstOrDefault();
                if (objWQR004 == null)
                {
                    ViewBag.Id = 0;
                }
                else
                {
                    ViewBag.Id = objWQR004.Id;
                }
            }
            else
            {
                ViewBag.Id = 0;
                ViewBag.WQTNo = string.Empty;
            }
            return View(objWQR004);
        }

        //check duplication for wqt no
        [HttpPost]
        public ActionResult IsWQTNoExists(string wqtNo, string location)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var wpqCertificate = false;
                var wopqCertificate = false;
                var ttsCertificate = false;

                wpqCertificate = db.WQR004.Any(x => x.WQTNo == wqtNo && x.Location == location);
                wopqCertificate = db.WQR005.Any(x => x.WQTNo == wqtNo && x.Location == location);
                ttsCertificate = db.WQR006.Any(x => x.WQTNo == wqtNo && x.Location == location);

                if (wpqCertificate || wopqCertificate || ttsCertificate)
                {
                    objResponseMsg.Key = true;
                    if (wpqCertificate)
                    { objResponseMsg.dataValue = "WPQ Certificate"; }
                    if (wopqCertificate)
                    { objResponseMsg.dataValue = "WOPQ Certificate"; }
                    if (ttsCertificate)
                    { objResponseMsg.dataValue = "T#TS Certificate"; }
                }
                else
                {
                    objResponseMsg.Key = false;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //WOPQ certificate by wqr003 id
        [SessionExpireFilter]
        public ActionResult WOPQCertificate_old(int? id)
        {
            ViewBag.Id = id;
            WQR005 objWQR005 = new WQR005();
            if (id > 0)
            {
                string WQTNo = db.WQR003.Where(i => i.Id == id).FirstOrDefault().WQTNo;
                ViewBag.WQTNo = WQTNo;
                objWQR005 = db.WQR005.Where(i => i.WQTNo == WQTNo).FirstOrDefault();
                if (objWQR005 == null)
                {
                    ViewBag.Id = 0;
                }
                else
                {
                    ViewBag.Id = objWQR005.Id;
                }
            }
            else
            {
                ViewBag.Id = 0;
                ViewBag.WQTNo = string.Empty;
            }
            return View(objWQR005);
        }
        #endregion

        #region Export Excel
        //Code By : nikita vibhandik 18/12/2017 (observation  14096)
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                //t#TS certificate grid
                string strFileName = string.Empty;
                NDEModels objNDEModels = new NDEModels();

                //wpq certificate
                if (gridType == clsImplementationEnum.GridType.WPQCertificate.GetStringValue())
                {

                    var lst = db.SP_WQ_WPQCERTIFICATE_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Location = Convert.ToString(uc.Location),
                                      WQTNo = Convert.ToString(uc.WQTNo),
                                      WPSNo = Convert.ToString(uc.WPSNo),
                                      WelderName = Convert.ToString(uc.Welder),
                                      WelderStampNo = Convert.ToString(uc.WelderStampNo),
                                      TypeOfTestCoupon = Convert.ToString(objNDEModels.GetCategory(uc.TypeOfTestCoupon).CategoryDescription),
                                      MaintainedBy = Convert.ToString(uc.MaintainedBy),
                                      MaintainedOn = Convert.ToDateTime(uc.MaintainedOn).ToString("dd/MM/yyyy"),

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                //wOpq certificate
                if (gridType == clsImplementationEnum.GridType.WOPQCertificate.GetStringValue())
                {

                    var lst = db.SP_WQ_WOPQCERTIFICATE_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Location = Convert.ToString(uc.Location),
                                      WQTNo = Convert.ToString(uc.WQTNo),
                                      WPSNo = Convert.ToString(uc.WPSNo),
                                      WelderName = Convert.ToString(uc.Welder),
                                      WelderStampNo = Convert.ToString(uc.WelderStampNo),
                                      TypeOfTestCoupon = Convert.ToString(objNDEModels.GetCategory(uc.TypeOfTestCoupon).CategoryDescription),
                                      MaintainedBy = Convert.ToString(uc.MaintainedBy),
                                      MaintainedOn = Convert.ToDateTime(uc.MaintainedOn).ToString("dd/MM/yyyy"),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }


                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Task Id: 17996 Hardik create new report for lapse welder
        [HttpGet]
        public JsonResult Locations(string filter)
        {
            var locations = db.WQR008
                        .Where(x => x.Location.Contains(filter) || x.Description.Contains(filter))
                        .AsEnumerable()
                        .Select(x => new { label = x.Location + "-" + x.Description, value = x.Location })
                        .ToList();
            return Json(locations, JsonRequestBehavior.AllowGet);
        }

        public string GetIntExtWelderName(string psno)
        {
            List<ApproverModel> lstApproverModel = new List<ApproverModel>();
            List<ModelWelderDetail> lstModelWelderDetail = new List<ModelWelderDetail>();

            string Welders = (from a in db.QMS003
                              where a.WelderPSNo == psno
                              select new ModelWelderDetail { WelderPSNo = a.WelderPSNo, WelderName = string.IsNullOrEmpty(a.Name) ? "" : a.Name }
                                  )
                             .Union(from b in db.QMS004
                                    where b.WelderPSNo == psno
                                    select new ModelWelderDetail { WelderPSNo = b.WelderPSNo, WelderName = string.IsNullOrEmpty(b.Name) ? "" : b.Name }).Select(i => i.WelderPSNo + "-" + i.WelderName).Distinct().FirstOrDefault();

            return Welders;
        }
        #endregion

        #region Copy Certificate
        [HttpPost]
        public ActionResult CopyCertificate(string wqtno, string location)
        {
            ViewBag.SourceWQTNo = wqtno;
            ViewBag.Location = location;
            return PartialView("_CopyCertificate");
        }

        [HttpPost]
        public ActionResult CopyToDestination(int headerId, string srcwqtno, string destwqtno, string type)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            if (type == "wpq")
            {
                var srcWQR004 = db.WQR004.Where(x => x.WQTNo == srcwqtno && x.Id == headerId).FirstOrDefault();
                objResponseMsg = CopyWPQCertificate(srcWQR004, destwqtno);
            }
            else if (type == "wopq")
            {
                var srcWQR005 = db.WQR005.Where(x => x.WQTNo == srcwqtno && x.Id == headerId).FirstOrDefault();
                objResponseMsg = CopyWOPQCertificate(srcWQR005, destwqtno);
            }
            else if (type == "tss")
            {
                var srcWQR006 = db.WQR006.Where(x => x.WQTNo == srcwqtno && x.Id == headerId).FirstOrDefault();
                objResponseMsg = CopyTTSCertificate(srcWQR006, destwqtno);
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponseMsgWithStatus CopyWPQCertificate(WQR004 objSrcWQR004, string destWQTNo)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var objDestWQR004 = new WQR004();
                var lstModelWelderDetail = (from a in db.QMS003
                                            where a.WelderPSNo.Equals(objSrcWQR004.Welder, StringComparison.OrdinalIgnoreCase)
                                            select new ModelWelderDetail { ShopCode = a.ShopCode }
                                                  )
                                                     .Union(from b in db.QMS004
                                                            where b.WelderPSNo.Equals(objSrcWQR004.Welder, StringComparison.OrdinalIgnoreCase) && b.ActiveWithCompany != null && b.ActiveWithCompany == true
                                                            select new ModelWelderDetail { ShopCode = b.ShopCode }).FirstOrDefault();

                WQR004 objaddWQR004 = new WQR004();
                WQR003 objWQR003 = db.WQR003.Where(i => i.WQTNo == objSrcWQR004.WQTNo).FirstOrDefault();
                //set flag of WQT No. (In WQR003)
                if (objWQR003 != null)
                {
                    objWQR003.CertificateGenerated = true;
                    objWQR003.CertificateGeneratedBy = objClsLoginInfo.UserName;
                    objWQR003.CertificateGeneratedOn = DateTime.Now;
                    objDestWQR004.FNo = objWQR003.FNo;
                    objDestWQR004.PNo = objWQR003.PNo;
                }
                else
                {
                    objDestWQR004.FNo = "NA";
                    objDestWQR004.PNo = "NA";
                }
                objDestWQR004.Shop = (lstModelWelderDetail != null) ? lstModelWelderDetail.ShopCode : "";
                objDestWQR004.HardFacingResult = objSrcWQR004.HardFacingResult;
                objDestWQR004.TrGrooveReport = objSrcWQR004.TrGrooveReport;
                objDestWQR004.TrExamDate = objSrcWQR004.TrExamDate;
                objDestWQR004.TypeOfTestCoupon = objSrcWQR004.TypeOfTestCoupon;
                objDestWQR004.WelderStampNo = objSrcWQR004.WelderStampNo;
                objDestWQR004.WPSNo = objSrcWQR004.WPSNo;
                objDestWQR004.Location = objSrcWQR004.Location;
                objDestWQR004.Welder = objSrcWQR004.Welder;
                objDestWQR004.WQTNo = destWQTNo;
                objDestWQR004.TestCoupon = objSrcWQR004.TestCoupon;
                objDestWQR004.ProductionWeld = objSrcWQR004.ProductionWeld;
                objDestWQR004.SpecificationTypeGrade = objSrcWQR004.SpecificationTypeGrade;
                objDestWQR004.DateOfWelding = objSrcWQR004.DateOfWelding;
                objDestWQR004.JointType = objSrcWQR004.JointType;

                objDestWQR004.WeldingProcessActualValues = objSrcWQR004.WeldingProcessActualValues;
                objDestWQR004.WeldingProcessQualified = objSrcWQR004.WeldingProcessQualified;
                objDestWQR004.TypeActual = objSrcWQR004.TypeActual;
                objDestWQR004.TypeQualified = objSrcWQR004.TypeQualified;
                objDestWQR004.BackingActual = objSrcWQR004.BackingActual;
                objDestWQR004.BackingQualified = objSrcWQR004.BackingQualified;
                objDestWQR004.PlatePipeActual1 = objSrcWQR004.PlatePipeActual1;
                objDestWQR004.PlatePipeActual2 = objSrcWQR004.PlatePipeActual2;
                objDestWQR004.PlatePipeQualified = objSrcWQR004.PlatePipeQualified;
                objDestWQR004.BaseMetalPnoActual1 = objSrcWQR004.BaseMetalPnoActual1;
                objDestWQR004.BaseMetalPnoActual2 = objSrcWQR004.BaseMetalPnoActual2;
                objDestWQR004.BaseMetalPnoQualified = objSrcWQR004.BaseMetalPnoQualified;
                objDestWQR004.BaseMetalThicknessActual = objSrcWQR004.BaseMetalThicknessActual;
                objDestWQR004.BaseMetalThicknessQualified = objSrcWQR004.BaseMetalThicknessQualified;
                objDestWQR004.FillerMetalSpecificationActual = objSrcWQR004.FillerMetalSpecificationActual;
                objDestWQR004.FillerMetalSpecificationQualified = objSrcWQR004.FillerMetalSpecificationQualified;
                objDestWQR004.FillerMetalClassificationActual = objSrcWQR004.FillerMetalClassificationActual;
                objDestWQR004.FillerMetalClassificationQualified = objSrcWQR004.FillerMetalClassificationQualified;
                objDestWQR004.FillerMetalFNumbersActual = objSrcWQR004.FillerMetalFNumbersActual;
                objDestWQR004.FillerMetalFNumbersQualified = objSrcWQR004.FillerMetalFNumbersQualified;
                objDestWQR004.ConsumableInsertActual = objSrcWQR004.ConsumableInsertActual;
                objDestWQR004.ConsumableInsertQualified = objSrcWQR004.ConsumableInsertQualified;
                objDestWQR004.FillerMetalProductFormActual = objSrcWQR004.FillerMetalProductFormActual;
                objDestWQR004.FillerMetalProductFormQualified = objSrcWQR004.FillerMetalProductFormQualified;
                objDestWQR004.DepositedThicknessActual1 = objSrcWQR004.DepositedThicknessActual1;
                objDestWQR004.DepositedThicknessActual2 = objSrcWQR004.DepositedThicknessActual2;
                objDestWQR004.DepositedThicknessQualified = objSrcWQR004.DepositedThicknessQualified;
                objDestWQR004.WeldingPositionActual = objSrcWQR004.WeldingPositionActual;
                objDestWQR004.CircWeldOvly = objSrcWQR004.CircWeldOvly;
                objDestWQR004.PlatePipePositionActual = objSrcWQR004.PlatePipePositionActual;
                objDestWQR004.PlatePipePositionQualified = objSrcWQR004.PlatePipePositionQualified;
                objDestWQR004.PipeWeldingPositionActual = objSrcWQR004.PipeWeldingPositionActual;
                objDestWQR004.PipeWeldingPositionQualified = objSrcWQR004.PipeWeldingPositionQualified;
                objDestWQR004.VerticalProgressionActual = objSrcWQR004.VerticalProgressionActual;
                objDestWQR004.VerticalProgressionQualified = objSrcWQR004.VerticalProgressionQualified;
                objDestWQR004.InertGasBackingActual = objSrcWQR004.InertGasBackingActual;
                objDestWQR004.InertGasBackingQualified = objSrcWQR004.InertGasBackingQualified;
                objDestWQR004.TransferModeActual = objSrcWQR004.TransferModeActual;
                objDestWQR004.TransferModeQualified = objSrcWQR004.TransferModeQualified;
                objDestWQR004.CurrentTypeActual = objSrcWQR004.CurrentTypeActual;
                objDestWQR004.CurrentTypeQualified = objSrcWQR004.CurrentTypeQualified;
                objDestWQR004.Other = objSrcWQR004.Other;
                objDestWQR004.Groove = objSrcWQR004.Groove;
                objDestWQR004.VisualExam = objSrcWQR004.VisualExam;
                objDestWQR004.VisualResult = objSrcWQR004.VisualResult;
                objDestWQR004.LgRootFaceBend = objSrcWQR004.LgRootFaceBend;
                objDestWQR004.TrRootFace = objSrcWQR004.TrRootFace;
                objDestWQR004.TrSideBend = objSrcWQR004.TrSideBend;
                objDestWQR004.TrResult = objSrcWQR004.TrResult;
                objDestWQR004.AltVolExam = objSrcWQR004.AltVolExam;
                objDestWQR004.UT = objSrcWQR004.UT;
                objDestWQR004.RT = objSrcWQR004.RT;
                objDestWQR004.RTResult = objSrcWQR004.RTResult;
                objDestWQR004.Rtreport = objSrcWQR004.Rtreport;
                objDestWQR004.Fillet = objSrcWQR004.Fillet;
                objDestWQR004.FilletResult = objSrcWQR004.FilletResult;
                objDestWQR004.FilletWeldsInPlate = objSrcWQR004.FilletWeldsInPlate;
                objDestWQR004.FilletWeldsInPipe = objSrcWQR004.FilletWeldsInPipe;
                objDestWQR004.FilletWeldFracture = objSrcWQR004.FilletWeldFracture;
                objDestWQR004.LengthPercentOfDefect = objSrcWQR004.LengthPercentOfDefect;
                objDestWQR004.MacroExamination = objSrcWQR004.MacroExamination;
                objDestWQR004.FilletSize = objSrcWQR004.FilletSize;
                objDestWQR004.ConcavityConvexity = objSrcWQR004.ConcavityConvexity;
                objDestWQR004.FilletReport = objSrcWQR004.FilletReport;
                objDestWQR004.CorrosionResistantOverlay = objSrcWQR004.CorrosionResistantOverlay;
                objDestWQR004.PipeBend = objSrcWQR004.PipeBend;
                objDestWQR004.PlateBend = objSrcWQR004.PlateBend;
                objDestWQR004.CorrosionResistantResult = objSrcWQR004.CorrosionResistantResult;
                objDestWQR004.CorrosionResistantReport = objSrcWQR004.CorrosionResistantReport;
                objDestWQR004.HardFacingOverlay = objSrcWQR004.HardFacingOverlay;
                objDestWQR004.MacroTestForPipe = objSrcWQR004.MacroTestForPipe;
                objDestWQR004.MacroTestForPlate = objSrcWQR004.MacroTestForPlate;
                objDestWQR004.LPExamination = objSrcWQR004.LPExamination;
                objDestWQR004.Lpreport = objSrcWQR004.Lpreport;
                objDestWQR004.TypeOfTest1 = objSrcWQR004.TypeOfTest1;
                objDestWQR004.Result1 = objSrcWQR004.Result1;
                objDestWQR004.ReportNo1 = objSrcWQR004.ReportNo1;

                objDestWQR004.TypeOfTest2 = objSrcWQR004.TypeOfTest2;
                objDestWQR004.Result2 = objSrcWQR004.Result2;
                objDestWQR004.ReportNo2 = objSrcWQR004.ReportNo2;

                objDestWQR004.TypeOfTest3 = objSrcWQR004.TypeOfTest3;
                objDestWQR004.Result3 = objSrcWQR004.Result3;
                objDestWQR004.ReportNo3 = objSrcWQR004.ReportNo3;

                objDestWQR004.TypeOfTest4 = objSrcWQR004.TypeOfTest4;
                objDestWQR004.Result4 = objSrcWQR004.Result4;
                objDestWQR004.ReportNo4 = objSrcWQR004.ReportNo4;

                objDestWQR004.TypeOfTest5 = objSrcWQR004.TypeOfTest5;
                objDestWQR004.Result5 = objSrcWQR004.Result5;
                objDestWQR004.ReportNo5 = objSrcWQR004.ReportNo5;

                objDestWQR004.TypeOfTest6 = objSrcWQR004.TypeOfTest6;
                objDestWQR004.Result6 = objSrcWQR004.Result6;
                objDestWQR004.ReportNo6 = objSrcWQR004.ReportNo6;
                objDestWQR004.FilmOrSpecimenEvaluatedBy = objSrcWQR004.FilmOrSpecimenEvaluatedBy;
                objDestWQR004.MechanicalTestsConductedBy = objSrcWQR004.MechanicalTestsConductedBy;
                objDestWQR004.WeldingSupervisedBy = objSrcWQR004.WeldingSupervisedBy;
                objDestWQR004.Company = objSrcWQR004.Company;
                objDestWQR004.LabTestNo = objSrcWQR004.LabTestNo;
                objDestWQR004.InspectionBy = objSrcWQR004.InspectionBy;
                objDestWQR004.Division1 = objSrcWQR004.Division1;
                objDestWQR004.Division2 = objSrcWQR004.Division2;
                objDestWQR004.Division3 = objSrcWQR004.Division3;

                objDestWQR004.LastWeldDate = objSrcWQR004.LastWeldDate;
                objDestWQR004.ValueUpto = objSrcWQR004.ValueUpto;
                objDestWQR004.VisualExamDate = objSrcWQR004.VisualExamDate;
                objDestWQR004.Remarks = objSrcWQR004.Remarks;
                objDestWQR004.MaintainedBy = objClsLoginInfo.UserName;
                objDestWQR004.MaintainedOn = DateTime.Now;
                objDestWQR004.CreatedBy = objClsLoginInfo.UserName;
                objDestWQR004.CreatedOn = DateTime.Now;
                db.WQR004.Add(objDestWQR004);
                db.SaveChanges();
                objResponseMsg.wqtno = objDestWQR004.WQTNo; objResponseMsg.HeaderId = objDestWQR004.Id;
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Record copied successfully!";

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return objResponseMsg;
        }

        public clsHelper.ResponseMsgWithStatus CopyWOPQCertificate(WQR005 objSrcWQR005, string destWQTNo)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var objdestWQR005Update = new WQR005();
                var lstModelWelderDetail = (from a in db.QMS003
                                            where a.WelderPSNo.Equals(objSrcWQR005.WelderName, StringComparison.OrdinalIgnoreCase)
                                            select new ModelWelderDetail { ShopCode = a.ShopCode }
                                           )
                                              .Union(from b in db.QMS004
                                                     where b.WelderPSNo.Equals(objSrcWQR005.WelderName, StringComparison.OrdinalIgnoreCase) && b.ActiveWithCompany != null && b.ActiveWithCompany == true
                                                     select new ModelWelderDetail { ShopCode = b.ShopCode }).FirstOrDefault();

                WQR005 objDestWQR005 = new WQR005();
                objDestWQR005.Shop = lstModelWelderDetail != null ? lstModelWelderDetail.ShopCode : "";
                var objWQR003 = db.WQR003.Where(i => i.WQTNo == objDestWQR005.WQTNo).FirstOrDefault();
                if (objWQR003 != null)
                {
                    objWQR003.CertificateGenerated = true;
                    objWQR003.CertificateGeneratedBy = objClsLoginInfo.UserName;
                    objWQR003.CertificateGeneratedOn = DateTime.Now;
                    objDestWQR005.FNo = objWQR003.FNo;
                    objDestWQR005.PNo = objWQR003.PNo;
                }
                else
                {
                    objDestWQR005.FNo = "NA";
                    objDestWQR005.PNo = "NA";
                }
                objDestWQR005.VisualExamDate = objSrcWQR005.VisualExamDate;
                objDestWQR005.Location = objSrcWQR005.Location;
                objDestWQR005.WQTNo = destWQTNo;
                objDestWQR005.Groove = objSrcWQR005.Groove;              //Bhadresh Added
                objDestWQR005.WelderName = objSrcWQR005.WelderName;
                objDestWQR005.WelderStampNo = objSrcWQR005.WelderStampNo;
                objDestWQR005.WeldingType = objSrcWQR005.WeldingType;
                objDestWQR005.DateOfWelding = objSrcWQR005.DateOfWelding;
                objDestWQR005.WPSNo = objSrcWQR005.WPSNo;
                objDestWQR005.HardFacingResult = objSrcWQR005.HardFacingResult;
                objDestWQR005.TrGrooveReport = objSrcWQR005.TrGrooveReport;
                objDestWQR005.TrExamDate = objSrcWQR005.TrExamDate;
                objDestWQR005.TypeOfTestCoupon = objSrcWQR005.TypeOfTestCoupon;
                objDestWQR005.SpecificationTypeGrade = objSrcWQR005.SpecificationTypeGrade;
                objDestWQR005.TestCoupon = objSrcWQR005.TestCoupon;
                objDestWQR005.ProductionWeld = objSrcWQR005.ProductionWeld;
                objDestWQR005.Position = objSrcWQR005.Position;
                objDestWQR005.ThicknessMm = objSrcWQR005.ThicknessMm;
                objDestWQR005.JointType = objSrcWQR005.JointType;

                //Bhadresh Change Start
                objDestWQR005.BaseMetalPnoActual1 = objSrcWQR005.BaseMetalPnoActual1;
                objDestWQR005.BaseMetalPnoActual2 = objSrcWQR005.BaseMetalPnoActual2;
                objDestWQR005.BaseMetalPnoQualified = objSrcWQR005.BaseMetalPnoQualified;
                objDestWQR005.BaseMetalThicknessActual = objSrcWQR005.BaseMetalThicknessActual;
                objDestWQR005.BaseMetalThicknessQualified = objSrcWQR005.BaseMetalThicknessQualified;
                //Bhadresh Change End
                objDestWQR005.TypeOfWeldingAutomaticActual = objSrcWQR005.TypeOfWeldingAutomaticActual;
                objDestWQR005.TypeOfWeldingAutomaticQualified = objSrcWQR005.TypeOfWeldingAutomaticQualified;
                objDestWQR005.WeldingProcessEsQualified = objSrcWQR005.WeldingProcessEsQualified;
                objDestWQR005.WeldingProcessEsActual = objSrcWQR005.WeldingProcessEsActual;
                objDestWQR005.WeldingProcessMachineActual = objSrcWQR005.WeldingProcessMachineActual;
                objDestWQR005.WeldingProcessMachineQualified = objSrcWQR005.WeldingProcessMachineQualified;
                objDestWQR005.FillerMetalUsedEBWLBWActual = objSrcWQR005.FillerMetalUsedEBWLBWActual;
                objDestWQR005.FillerMetalUsedEBWLBWQualified = objSrcWQR005.FillerMetalUsedEBWLBWQualified;
                objDestWQR005.TypeOfLaserForLBWCO2ToYAGEtcActual = objSrcWQR005.TypeOfLaserForLBWCO2ToYAGEtcActual;
                objDestWQR005.TypeOfLaserForLBWCO2ToYAGEtcQualified = objSrcWQR005.TypeOfLaserForLBWCO2ToYAGEtcQualified;
                objDestWQR005.ContinuousDriveOrInertiaWeldingFWActual = objSrcWQR005.ContinuousDriveOrInertiaWeldingFWActual;
                objDestWQR005.ContinuousDriveOrInertiaWeldingFWQualified = objSrcWQR005.ContinuousDriveOrInertiaWeldingFWQualified;
                objDestWQR005.VacuumOrOutOfVacuumEBWActual = objSrcWQR005.VacuumOrOutOfVacuumEBWActual;
                objDestWQR005.VacuumOrOutOfVacuumEBWQualified = objSrcWQR005.VacuumOrOutOfVacuumEBWQualified;
                objDestWQR005.TypeOfWeldingMachineActual = objSrcWQR005.TypeOfWeldingMachineActual;
                objDestWQR005.TypeOfWeldingMachineQualified = objSrcWQR005.TypeOfWeldingMachineQualified;
                objDestWQR005.PlatePipeEnterDiaIfPipeOrTubeActual = objSrcWQR005.PlatePipeEnterDiaIfPipeOrTubeActual;
                objDestWQR005.PlatePipeEnterDiaIfPipeOrTubeActualTo = objSrcWQR005.PlatePipeEnterDiaIfPipeOrTubeActualTo;
                objDestWQR005.PlatePipeEnterDiaIfPipeOrTubeQualified = objSrcWQR005.PlatePipeEnterDiaIfPipeOrTubeQualified;
                objDestWQR005.DirectOrRemoteVisualControlActual = objSrcWQR005.DirectOrRemoteVisualControlActual;
                objDestWQR005.DirectOrRemoteVisualControlQualified = objSrcWQR005.DirectOrRemoteVisualControlQualified;
                objDestWQR005.AutomaticArcVoltageControlActual = objSrcWQR005.AutomaticArcVoltageControlActual;
                objDestWQR005.AutomaticArcVoltageControlQualified = objSrcWQR005.AutomaticArcVoltageControlQualified;
                objDestWQR005.AutomaticJointTrackingActual = objSrcWQR005.AutomaticJointTrackingActual;
                objDestWQR005.AutomaticJointTrackingQualified = objSrcWQR005.AutomaticJointTrackingQualified;
                objDestWQR005.WeldingPositionActual = objSrcWQR005.WeldingPositionActual;
                objDestWQR005.CircWeldOvly = objSrcWQR005.CircWeldOvly;
                objDestWQR005.IPlatePipeOverlayWeldingPositionQualified = objSrcWQR005.IPlatePipeOverlayWeldingPositionQualified;
                objDestWQR005.IPlatePipeFilletWeldingPositionQualified = objSrcWQR005.IPlatePipeFilletWeldingPositionQualified;
                objDestWQR005.IiPipeOverlayWeldingPositionQualified = objSrcWQR005.IiPipeOverlayWeldingPositionQualified;
                objDestWQR005.IiPipeFilletWeldingPositionQualified = objSrcWQR005.IiPipeFilletWeldingPositionQualified;
                objDestWQR005.ConsumableInsertActual = objSrcWQR005.ConsumableInsertActual;
                objDestWQR005.ConsumableInsertQualified = objSrcWQR005.ConsumableInsertQualified;
                objDestWQR005.BackingActual = objSrcWQR005.BackingActual;
                objDestWQR005.BackingQualified = objSrcWQR005.BackingQualified;
                objDestWQR005.SingleOrMultiplePassesPerSideActual = objSrcWQR005.SingleOrMultiplePassesPerSideActual;
                objDestWQR005.SingleOrMultiplePassesPerSideQualified = objSrcWQR005.SingleOrMultiplePassesPerSideQualified;
                objDestWQR005.Other = objSrcWQR005.Other;

                objDestWQR005.VisualExamOfCompWeld = objSrcWQR005.VisualExamOfCompWeld;
                objDestWQR005.LgRootFaceBend = objSrcWQR005.LgRootFaceBend;
                objDestWQR005.AltVolExam = objSrcWQR005.AltVolExam;
                objDestWQR005.VisualResult = objSrcWQR005.VisualResult;
                objDestWQR005.TrRootFace = objSrcWQR005.TrRootFace;
                objDestWQR005.UT = objSrcWQR005.UT;
                objDestWQR005.TrSideBend = objSrcWQR005.TrSideBend;
                objDestWQR005.LgRootResult = objSrcWQR005.LgRootResult;
                objDestWQR005.RT = objSrcWQR005.RT;
                objDestWQR005.AltVolResult = objSrcWQR005.AltVolResult;
                objDestWQR005.GrooveReport = objSrcWQR005.GrooveReport;
                objDestWQR005.Fillet = objSrcWQR005.Fillet;
                objDestWQR005.FilletResult = objSrcWQR005.FilletResult;
                objDestWQR005.FilletWeldsInPlate = objSrcWQR005.FilletWeldsInPlate;
                objDestWQR005.FilletWeldFractureTest = objSrcWQR005.FilletWeldFractureTest;
                objDestWQR005.MacroExamination = objSrcWQR005.MacroExamination;
                objDestWQR005.FilletReportNo = objSrcWQR005.FilletReportNo;
                objDestWQR005.FilletWeldsInPipe = objSrcWQR005.FilletWeldsInPipe;
                objDestWQR005.LengthAndPercentOfDefect = objSrcWQR005.LengthAndPercentOfDefect;
                objDestWQR005.FilletSizeMm = objSrcWQR005.FilletSizeMm;
                objDestWQR005.ConcavityConvexityMm = objSrcWQR005.ConcavityConvexityMm;
                objDestWQR005.CorrosionResistantOverlay = objSrcWQR005.CorrosionResistantOverlay;
                objDestWQR005.PipeBend = objSrcWQR005.PipeBend;
                objDestWQR005.PlateBend = objSrcWQR005.PlateBend;
                objDestWQR005.CorrosionResult = objSrcWQR005.CorrosionResult;
                objDestWQR005.CorrosionReportNo = objSrcWQR005.CorrosionReportNo;
                objDestWQR005.HardFacingOverlay = objSrcWQR005.HardFacingOverlay;
                objDestWQR005.MacroTestForPipe = objSrcWQR005.MacroTestForPipe;
                objDestWQR005.MacroTestForPlate = objSrcWQR005.MacroTestForPlate;
                objDestWQR005.LPExamination = objSrcWQR005.LPExamination;
                objDestWQR005.HardFacingReport = objSrcWQR005.HardFacingReport;
                objDestWQR005.TypeOfTest1 = objSrcWQR005.TypeOfTest1;
                objDestWQR005.Result1 = objSrcWQR005.Result1;
                objDestWQR005.ReportNo1 = objSrcWQR005.ReportNo1;
                objDestWQR005.TypeOfTest2 = objSrcWQR005.TypeOfTest2;
                objDestWQR005.Result2 = objSrcWQR005.Result2;
                objDestWQR005.ReportNo2 = objSrcWQR005.ReportNo2;
                objDestWQR005.TypeOfTest3 = objSrcWQR005.TypeOfTest3;
                objDestWQR005.Result3 = objSrcWQR005.Result3;
                objDestWQR005.ReportNo3 = objSrcWQR005.ReportNo3;
                objDestWQR005.TypeOfTest4 = objSrcWQR005.TypeOfTest4;
                objDestWQR005.Result4 = objSrcWQR005.Result4;
                objDestWQR005.ReportNo4 = objSrcWQR005.ReportNo4;
                objDestWQR005.TypeOfTest5 = objSrcWQR005.TypeOfTest5;
                objDestWQR005.Result5 = objSrcWQR005.Result5;
                objDestWQR005.ReportNo5 = objSrcWQR005.ReportNo5;
                objDestWQR005.TypeOfTest6 = objSrcWQR005.TypeOfTest6;
                objDestWQR005.Result6 = objSrcWQR005.Result6;
                objDestWQR005.ReportNo6 = objSrcWQR005.ReportNo6;
                objDestWQR005.FilmOrSpecimenEvaluatedBy = objSrcWQR005.FilmOrSpecimenEvaluatedBy;
                objDestWQR005.MechanicalTestsConductedBy = objSrcWQR005.MechanicalTestsConductedBy;
                objDestWQR005.WeldingSupervisedBy = objSrcWQR005.WeldingSupervisedBy;
                objDestWQR005.Company = objSrcWQR005.Company;
                objDestWQR005.LabTestNo = objSrcWQR005.LabTestNo;
                objDestWQR005.InspectionByAndDescription = objSrcWQR005.InspectionByAndDescription;
                objDestWQR005.Division1 = objSrcWQR005.Division1;
                objDestWQR005.Division2 = objSrcWQR005.Division2;
                objDestWQR005.Division3 = objSrcWQR005.Division3;
                objDestWQR005.LastWeldDate = objSrcWQR005.LastWeldDate;
                objDestWQR005.Remarks = objSrcWQR005.Remarks;
                objDestWQR005.ValueUpto = objSrcWQR005.ValueUpto;
                objDestWQR005.MaintainedBy = objClsLoginInfo.UserName;
                objDestWQR005.MaintainedOn = DateTime.Now;
                objDestWQR005.CreatedBy = objClsLoginInfo.UserName;
                objDestWQR005.CreatedOn = DateTime.Now;

                db.WQR005.Add(objDestWQR005);
                db.SaveChanges();
                objResponseMsg.HeaderId = objDestWQR005.Id;
                objResponseMsg.wqtno = objDestWQR005.WQTNo;
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Record copied successfully!";

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return objResponseMsg;
        }

        public clsHelper.ResponseMsgWithStatus CopyTTSCertificate(WQR006 objSrcWQR006, string destWQTNo)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            WQR006 objWQR6 = new WQR006();

            try
            {

                var lstModelWelderDetail = (from a in db.QMS003
                                            where a.WelderPSNo.Equals(objSrcWQR006.WelderName, StringComparison.OrdinalIgnoreCase)
                                            select new ModelWelderDetail { ShopCode = a.ShopCode }
                                      )
                                         .Union(from b in db.QMS004
                                                where b.WelderPSNo.Equals(objSrcWQR006.WelderName, StringComparison.OrdinalIgnoreCase) && b.ActiveWithCompany != null && b.ActiveWithCompany == true
                                                select new ModelWelderDetail { ShopCode = b.ShopCode }).FirstOrDefault();

                WQR003 objWQR003 = db.WQR003.Where(i => i.WQTNo == objSrcWQR006.WQTNo).FirstOrDefault();
                if (objWQR003 != null)
                {
                    objWQR003.CertificateGenerated = true;
                    objWQR003.CertificateGeneratedBy = objClsLoginInfo.UserName;
                    objWQR003.CertificateGeneratedOn = DateTime.Now;
                }
                objWQR6.VisualExamDate = objSrcWQR006.VisualExamDate;
                // objWQR003.CertificateGenerated = true;
                objWQR6.Shop = (lstModelWelderDetail != null) ? lstModelWelderDetail.ShopCode : "";
                objWQR6.Location = objSrcWQR006.Location;
                objWQR6.WQTNo = destWQTNo;
                objWQR6.WelderName = objSrcWQR006.WelderName;
                objWQR6.WelderStampNo = objSrcWQR006.WelderStampNo;
                objWQR6.JoinType = objSrcWQR006.JoinType;
                objWQR6.WeldingType = objSrcWQR006.WeldingType;
                objWQR6.WPSNo = objSrcWQR006.WPSNo;
                objWQR6.FillerMetalAWS = objSrcWQR006.FillerMetalAWS;
                objWQR6.TestCoupon = objSrcWQR006.TestCoupon;
                objWQR6.ProductionWeld = objSrcWQR006.ProductionWeld;
                objWQR6.DateOfWelding = objSrcWQR006.DateOfWelding;
                objWQR6.SpecificationTypeGrade = objSrcWQR006.SpecificationTypeGrade;
                objWQR6.FillerMetalAWS = objSrcWQR006.FillerMetalAWS;

                objWQR6.WeldingProcessEsActual = objSrcWQR006.WeldingProcessEsActual;
                objWQR6.WeldingProcessEsQualified = objSrcWQR006.WeldingProcessEsQualified;
                objWQR6.WeldJointConfigurationPreplacedFillerMetal = objSrcWQR006.WeldJointConfigurationPreplacedFillerMetal;
                objWQR6.WeldJointConfigurationPreplacedFillerMetalQualified = objSrcWQR006.WeldJointConfigurationPreplacedFillerMetalQualified;
                objWQR6.WeldingTypeActual = objSrcWQR006.WeldingTypeActual;
                objWQR6.WeldingTypeQualified = objSrcWQR006.WeldingTypeQualified;
                objWQR6.JointConfigurationActual = objSrcWQR006.JointConfigurationActual;
                objWQR6.JointConfigurationQualified = objSrcWQR006.JointConfigurationQualified;
                objWQR6.DepthOfGrooveMmActual = objSrcWQR006.DepthOfGrooveMmActual;
                objWQR6.DepthOfGrooveMmQualified = objSrcWQR006.DepthOfGrooveMmQualified;
                objWQR6.GrooveAngleRadiusActualFrom = objSrcWQR006.GrooveAngleRadiusActualFrom;
                objWQR6.GrooveRadiusAngleQualified = objSrcWQR006.GrooveRadiusAngleQualified;
                objWQR6.TubeWallThicknessMmActual = objSrcWQR006.TubeWallThicknessMmActual;
                objWQR6.TubeWallThicknessMmQualified = objSrcWQR006.TubeWallThicknessMmQualified;
                objWQR6.TubeOutsideDiameterActual = objSrcWQR006.TubeOutsideDiameterActual;
                objWQR6.TubeOutsideDiameterQualified = objSrcWQR006.TubeOutsideDiameterQualified;
                objWQR6.PitchMmActual = objSrcWQR006.PitchMmActual;
                objWQR6.PitchMmQualified = objSrcWQR006.PitchMmQualified;
                objWQR6.LigamentMmActual = objSrcWQR006.LigamentMmActual;
                objWQR6.LigamentMmQualified = objSrcWQR006.LigamentMmQualified;
                objWQR6.TubeProjectionMmActual = objSrcWQR006.TubeProjectionMmActual;
                objWQR6.TubeProjectionMmQualified = objSrcWQR006.TubeProjectionMmQualified;
                objWQR6.SinglePassMultipassActual = objSrcWQR006.SinglePassMultipassActual;
                objWQR6.SinglePassMultpiassQualified = objSrcWQR006.SinglePassMultpiassQualified;
                objWQR6.WeldingPositionActual = objSrcWQR006.WeldingPositionActual;
                objWQR6.WeldingPositionQualified = objSrcWQR006.WeldingPositionQualified;
                objWQR6.VerticalProgressionActual = objSrcWQR006.VerticalProgressionActual;
                objWQR6.VerticalProgressionQualified = objSrcWQR006.VerticalProgressionQualified;
                objWQR6.TubeMaterialPNoActual = objSrcWQR006.TubeMaterialPNoActual;
                objWQR6.TubeMaterialPNoQualified = objSrcWQR006.TubeMaterialPNoQualified;
                objWQR6.TubesheetMaterialPNoIfTubesheetIsPartOfWeldActual = objSrcWQR006.TubesheetMaterialPNoIfTubesheetIsPartOfWeldActual;
                objWQR6.TubesheetMaterialPNoIfTubesheetIsPartOfWeldQualified = objSrcWQR006.TubesheetMaterialPNoIfTubesheetIsPartOfWeldQualified;
                objWQR6.TubesheetCladdingMaterialPNoIfCladdingIsPartOfWeld = objSrcWQR006.TubesheetCladdingMaterialPNoIfCladdingIsPartOfWeld;
                objWQR6.TubesheetCladdingMatlANoIfCladdingIsPartOfWeldActual = objSrcWQR006.TubesheetCladdingMatlANoIfCladdingIsPartOfWeldActual;
                objWQR6.TubesheetCladdingMatlANoIfCladdingIsPartOfWeldQualified = objSrcWQR006.TubesheetCladdingMatlANoIfCladdingIsPartOfWeldQualified;
                objWQR6.TubesheetCladdingMaterialPNoIfCladdingIsPartOfWeldQualified = objSrcWQR006.TubesheetCladdingMaterialPNoIfCladdingIsPartOfWeldQualified;
                objWQR6.Others = objSrcWQR006.Others;

                objWQR6.ANoOfWeldDepositIfFillerMetalIsAddedActual = objSrcWQR006.ANoOfWeldDepositIfFillerMetalIsAddedActual;
                objWQR6.ANoOfWeldDepositIfFillerMetalIsAddedQualified = objSrcWQR006.ANoOfWeldDepositIfFillerMetalIsAddedQualified;
                objWQR6.PreheatTempMinActual = objSrcWQR006.PreheatTempMinActual;
                objWQR6.PreheatTempMinQualified = objSrcWQR006.PreheatTempMinQualified;
                objWQR6.InterpassTempMaxActual = objSrcWQR006.InterpassTempMaxActual;
                objWQR6.InterpassTempMaxQualified = objSrcWQR006.InterpassTempMaxQualified;
                objWQR6.PWHTActual = objSrcWQR006.PWHTActual;
                objWQR6.PWHTQualified = objSrcWQR006.PWHTQualified;
                objWQR6.CurrentTypeActual = objSrcWQR006.CurrentTypeActual;
                objWQR6.CurrentTypeQualified = objSrcWQR006.CurrentTypeQualified;
                objWQR6.CurrentPolarityActual = objSrcWQR006.CurrentPolarityActual;
                objWQR6.CurrentPolarityQualified = objSrcWQR006.CurrentPolarityQualified;
                objWQR6.FillerMetalElectrodeDiaMmActual = objSrcWQR006.FillerMetalElectrodeDiaMmActual;
                objWQR6.FillerMetalElectrodeDiaMmQualified = objSrcWQR006.FillerMetalElectrodeDiaMmQualified;
                objWQR6.CurrentAActual = objSrcWQR006.CurrentAActual;
                objWQR6.CurrentAQualified = objSrcWQR006.CurrentAQualified;
                objWQR6.FNoActual = objSrcWQR006.FNoActual;
                objWQR6.FNoQualified = objSrcWQR006.FNoQualified;
                objWQR6.FillerMetalElectrodeDiaMmActual1 = objSrcWQR006.FillerMetalElectrodeDiaMmActual1;
                objWQR6.FillerMetalElectrodeDiaMmQualified1 = objSrcWQR006.FillerMetalElectrodeDiaMmQualified1;
                objWQR6.CurrentAActual1 = objSrcWQR006.CurrentAActual1;
                objWQR6.CurrentAQualified2 = objSrcWQR006.CurrentAQualified2;
                objWQR6.FNoActual1 = objSrcWQR006.FNoActual1;
                objWQR6.FNoQualified1 = objSrcWQR006.FNoQualified1;
                objWQR6.TubeExpansionPriorToWeldingActual = objSrcWQR006.TubeExpansionPriorToWeldingActual;
                objWQR6.TubeExpansionPriorToWeldingQualified = objSrcWQR006.TubeExpansionPriorToWeldingQualified;
                objWQR6.MethodOfCleaningActual = objSrcWQR006.MethodOfCleaningActual;
                objWQR6.MethodOfCleaningQualified = objSrcWQR006.MethodOfCleaningQualified;
                objWQR6.SizeShapeOfPreplacedMetalMmActual = objSrcWQR006.SizeShapeOfPreplacedMetalMmActual;
                objWQR6.SizeShapeOfPreplacedMetalMmQualified = objSrcWQR006.SizeShapeOfPreplacedMetalMmQualified;
                objWQR6.ShieldingGasEsActual = objSrcWQR006.ShieldingGasEsActual;
                objWQR6.ShieldingGasEsQualified = objSrcWQR006.ShieldingGasEsQualified;
                objWQR6.FlowRateOfMinorGasConstituentLMinActual = objSrcWQR006.FlowRateOfMinorGasConstituentLMinActual;
                objWQR6.FlowRateOfMinorGasConstituentLMinQualified = objSrcWQR006.FlowRateOfMinorGasConstituentLMinQualified;
                objWQR6.FillerMetalGTAWPAWActual = objSrcWQR006.FillerMetalGTAWPAWActual;
                objWQR6.FillerMetalGTAWPAWQualified = objSrcWQR006.FillerMetalGTAWPAWQualified;
                objWQR6.AuxiliaryGasShieldActual = objSrcWQR006.AuxiliaryGasShieldActual;
                objWQR6.AuxiliaryGasShieldQualified = objSrcWQR006.AuxiliaryGasShieldQualified;
                objWQR6.AutomaticArcVoltageControlActual = objSrcWQR006.AutomaticArcVoltageControlActual;
                objWQR6.AutomaticArcVoltageControlQualified = objSrcWQR006.AutomaticArcVoltageControlQualified;

                objWQR6.CompleteFusionObserved = objSrcWQR006.CompleteFusionObserved;
                objWQR6.NoBurnThroughInTubeWall = objSrcWQR006.NoBurnThroughInTubeWall;
                objWQR6.NoCracks = objSrcWQR006.NoCracks;
                objWQR6.NoPorosity = objSrcWQR006.NoPorosity;
                objWQR6.NoPorosityResult = objSrcWQR006.NoPorosityResult;
                objWQR6.NoPorosityReportNo = objSrcWQR006.NoPorosityReportNo;
                objWQR6.LiquidPenetrationResult = objSrcWQR006.LiquidPenetrationResult;
                objWQR6.LiquidPenetrationReportNo = objSrcWQR006.LiquidPenetrationReportNo;
                objWQR6.Required = objSrcWQR006.Required;
                objWQR6.ObservedMin = objSrcWQR006.ObservedMin;
                objWQR6.ObservedMax = objSrcWQR006.ObservedMax;
                objWQR6.BCrackSObserved = objSrcWQR006.BCrackSObserved;
                objWQR6.CCompleteFusion = objSrcWQR006.CCompleteFusion;
                objWQR6.DCompletePenetration = objSrcWQR006.DCompletePenetration;
                objWQR6.Result = objSrcWQR006.Result;
                objWQR6.ReportNo = objSrcWQR006.ReportNo;
                objWQR6.TypeOfTest1 = objSrcWQR006.TypeOfTest1;
                objWQR6.TypeOfTestResult1 = objSrcWQR006.TypeOfTestResult1;
                objWQR6.TypeOfTestReportNo1 = objSrcWQR006.TypeOfTestReportNo1;
                objWQR6.TypeOfTest2 = objSrcWQR006.TypeOfTest2;
                objWQR6.TypeOfTestResult2 = objSrcWQR006.TypeOfTestResult2;
                objWQR6.TypeOfTestReportNo2 = objSrcWQR006.TypeOfTestReportNo2;
                objWQR6.TypeOfTest3 = objSrcWQR006.TypeOfTest3;
                objWQR6.TypeOfTestResult3 = objSrcWQR006.TypeOfTestResult3;
                objWQR6.TypeOfTestReportNo3 = objSrcWQR006.TypeOfTestReportNo3;
                objWQR6.FilmOrSpecimenEvaluatedBy = objSrcWQR006.FilmOrSpecimenEvaluatedBy;
                objWQR6.MechanicalTestsConductedBy = objSrcWQR006.MechanicalTestsConductedBy;
                objWQR6.WeldingSupervisedBy = objSrcWQR006.WeldingSupervisedBy;
                objWQR6.Company = objSrcWQR006.Company;
                objWQR6.LabTestNo = objSrcWQR006.LabTestNo;
                objWQR6.InspectionByAndDescription = objSrcWQR006.InspectionByAndDescription;
                objWQR6.Division1 = objSrcWQR006.Division1;
                objWQR6.Division2 = objSrcWQR006.Division2;
                objWQR6.Division3 = objSrcWQR006.Division3;
                objWQR6.MacroResult = objSrcWQR006.MacroResult;
                objWQR6.MacroReportNo = objSrcWQR006.MacroReportNo;
                objWQR6.LastWeldDate = objSrcWQR006.LastWeldDate;
                objWQR6.Remarks = objSrcWQR006.Remarks;
                objWQR6.ValueUpto = objSrcWQR006.ValueUpto;
                objWQR6.CreatedBy = objClsLoginInfo.UserName;
                objWQR6.CreatedOn = DateTime.Now;
                objWQR6.MaintainedBy = objClsLoginInfo.UserName;
                objWQR6.MaintainedOn = DateTime.Now;
                db.WQR006.Add(objWQR6);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Record copied successfully!";
                objResponseMsg.HeaderId = objWQR6.Id;
                objResponseMsg.wqtno = objWQR6.WQTNo;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return objResponseMsg;
        }

        #endregion

        #region Save data to WML
        [HttpPost]
        public JsonResult SaveWMLCertificate(int Id, string wmltype, string project, string jointtype, string certificatetype)
        {
            WQR004 objWQR004 = null;
            WQR005 objWQR005 = null;
            var objResponseMsg = new clsHelper.ResponseMsg();
            bool isSave = false;
            var typeASME = clsImplementationEnum.WMLType.ASME.GetStringValue();
            string location = string.Empty, WQTNo = string.Empty, Welder = string.Empty, WelderStampNo = string.Empty;
            if (certificatetype == "WPQ")
            {
                objWQR004 = db.WQR004.Where(a => a.Id == Id).FirstOrDefault();
                location = objWQR004.Location;
                Welder = objWQR004.Welder;
                WelderStampNo = objWQR004.WelderStampNo;
                WQTNo = objWQR004.WQTNo;
            }
            else
            {
                objWQR005 = db.WQR005.Where(a => a.Id == Id).FirstOrDefault();
                location = objWQR005.Location;
                Welder = objWQR005.WelderName;
                WelderStampNo = objWQR005.WelderStampNo;
                WQTNo = objWQR005.WQTNo;
            }
            try
            {
                ModelWQRequest objModelWQRequest = new ModelWQRequest();

                var jointtypeDesc = GetCategory("Joint Type WQ", jointtype).CategoryDescription;
                if (jointtype.ToUpper().Contains("OVERLAY") || jointtypeDesc.ToUpper().Contains("OVERLAY"))
                {
                    #region WOPQ (Overlay)
                    var objWML002 = new WML002();
                    objWML002 = db.WML002.Where(a => a.Location == location && a.WelderName == Welder && a.WelderStampNo == WelderStampNo && a.WMLType == wmltype && a.WQTNo == WQTNo && a.Project == project).FirstOrDefault();
                    if (objWML002 == null)
                    {
                        objWML002 = new WML002();
                        if (db.WML001.Any(a => a.Location == location && a.Welder == Welder && a.WelderStampNo == WelderStampNo && a.WMLType == wmltype && a.WQTNo == WQTNo && (a.WMLType == typeASME || a.Project == project))
                              || db.WML002.Any(a => a.Location == location && a.WelderName == Welder && a.WelderStampNo == WelderStampNo && a.WMLType == wmltype && a.WQTNo == WQTNo && (a.WMLType == typeASME || a.Project == project))
                              || db.WML003.Any(a => a.Location == location && a.WelderName == Welder && a.WelderStampNo == WelderStampNo && a.WMLType == wmltype && a.WQTNo == WQTNo && a.Project == project))
                        {
                            objResponseMsg.Value = "WQT No. is already being used";
                            objResponseMsg.Key = false;
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }
                        else { isSave = true; }
                    }

                    if (objWQR005 != null)
                    {
                        #region from WQR005
                        objWML002.MaintainedBy = objClsLoginInfo.UserName;
                        objWML002.ModifiedBy = objClsLoginInfo.UserName;
                        objWML002.MaintainedOn = DateTime.Now;
                        objWML002.ModifiedOn = DateTime.Now;
                        objWML002.Location = objWQR005.Location;
                        objWML002.SpecificationNotes = objWQR005.SpecificationNotes;
                        objWML002.WQTNo = objWQR005.WQTNo;
                        objWML002.WPSNo = objWQR005.WPSNo;
                        objWML002.WelderName = objWQR005.WelderName;
                        objWML002.WelderStampNo = objWQR005.WelderStampNo;
                        objWML002.Shop = objWQR005.Shop;
                        objWML002.TypeOfTestCoupon = objWQR005.TypeOfTestCoupon;
                        objWML002.WeldingProcessEsQualified = objWQR005.WeldingProcessEsQualified;
                        objWML002.TypeOfWeldingAutomaticQualified = objWQR005.TypeOfWeldingAutomaticQualified;

                        objWML002.PlatePipeEnterDiaIfPipeOrTubeQualified = objWQR005.PlatePipeEnterDiaIfPipeOrTubeQualified;
                        objWML002.IPlatePipeOverlayWeldingPositionQualified = objWQR005.IPlatePipeOverlayWeldingPositionQualified;
                        objWML002.IiPipeFilletWeldingPositionQualified = objWQR005.IiPipeFilletWeldingPositionQualified;
                        objWML002.IPlatePipeFilletWeldingPositionQualified = objWQR005.IPlatePipeFilletWeldingPositionQualified;
                        //objWML002.BaseMetalThicknessQualified = objWQR005.BaseMetalThicknessQualified;
                        objWML002.BackingQualified = objWQR005.BackingQualified;
                        //objWML002.InertGasBackingQualified = objWQR005.InertGasBackingQualified;
                        objWML002.CircWeldOvly = objWQR005.CircWeldOvly;
                        objWML002.Remarks = objWQR005.Remarks;
                        objWML002.PNo = objWQR005.PNo;
                        objWML002.FNo = objWQR005.FNo;
                        objWML002.WeldingType = objWQR005.WeldingType;
                        objWML002.WeldingProcessMachineQualified = objWQR005.WeldingProcessMachineQualified;
                        //if (!objWML002.LastWeldDate.HasValue)
                        //{
                        objWML002.LastWeldDate = objWQR005.LastWeldDate;
                        objWML002.ValueUpto = objWQR005.ValueUpto;
                        //}
                        objWML002.JointType = objWQR005.JointType;
                        objWML002.Project = project;
                        objWML002.WMLType = wmltype;

                        if (isSave)
                        {
                            objWML002.CreatedBy = objClsLoginInfo.UserName;
                            objWML002.CreatedOn = DateTime.Now;
                            db.WML002.Add(objWML002);
                            db.SaveChanges();
                            objResponseMsg.Value = "Record added successfully";
                            objResponseMsg.Key = true;
                        }
                        else
                        {
                            objWML002.EditedBy = objClsLoginInfo.UserName;
                            objWML002.EditedOn = DateTime.Now;
                            db.SaveChanges();
                            objResponseMsg.Value = "Record updated successfully";
                            objResponseMsg.Key = true;
                        }
                        #endregion

                    }
                    else
                    {
                        #region from WQR004
                        if (objWQR004 != null)
                        {
                            objWML002.MaintainedBy = objClsLoginInfo.UserName;
                            objWML002.ModifiedBy = objClsLoginInfo.UserName;
                            objWML002.MaintainedOn = DateTime.Now;
                            objWML002.ModifiedOn = DateTime.Now;
                            objWML002.Location = objWQR004.Location;
                            objWML002.SpecificationNotes = objWQR004.SpecificationNotes;
                            objWML002.WQTNo = objWQR004.WQTNo;
                            objWML002.WPSNo = objWQR004.WPSNo;
                            objWML002.WelderName = objWQR004.Welder;
                            objWML002.WelderStampNo = objWQR004.WelderStampNo;
                            objWML002.Shop = objWQR004.Shop;
                            objWML002.TypeOfTestCoupon = objWQR004.TypeOfTestCoupon;
                            objWML002.WeldingProcessEsQualified = objWQR004.WeldingProcessQualified;
                            objWML002.TypeOfWeldingAutomaticQualified = objWQR004.TypeQualified;


                            objWML002.PlatePipeEnterDiaIfPipeOrTubeQualified = objWQR004.PlatePipePositionActual;
                            objWML002.IPlatePipeOverlayWeldingPositionQualified = objWQR004.PlatePipePositionQualified;
                            objWML002.IiPipeFilletWeldingPositionQualified = objWQR004.PipeWeldingPositionQualified;
                            objWML002.IPlatePipeFilletWeldingPositionQualified = objWQR004.PlatePipeQualified;
                            objWML002.BaseMetalThicknessQualified = objWQR004.BaseMetalThicknessQualified;
                            objWML002.BackingQualified = objWQR004.BackingQualified;
                            objWML002.InertGasBackingQualified = objWQR004.InertGasBackingQualified;
                            objWML002.CircWeldOvly = objWQR004.CircWeldOvly;
                            objWML002.Remarks = objWQR004.Remarks;
                            objWML002.PNo = objWQR004.PNo;
                            objWML002.FNo = objWQR004.FNo;
                            objWML002.WeldingType = objWQR004.TypeQualified;
                            objWML002.WeldingProcessMachineQualified = objWQR004.WeldingProcessQualified;
                            objWML002.LastWeldDate = objWQR004.LastWeldDate;
                            //if (!objWML002.LastWeldDate.HasValue)
                            //{
                            objWML002.ValueUpto = objWQR004.ValueUpto;
                            objWML002.JointType = objWQR004.JointType;
                            //}
                            objWML002.Project = project;
                            objWML002.WMLType = wmltype;

                            if (isSave)
                            {
                                objWML002.CreatedBy = objClsLoginInfo.UserName;
                                objWML002.CreatedOn = DateTime.Now;
                                db.WML002.Add(objWML002);
                                db.SaveChanges();
                                objResponseMsg.Value = "Record added successfully";
                                objResponseMsg.Key = true;
                            }
                            else
                            {
                                objWML002.EditedBy = objClsLoginInfo.UserName;
                                objWML002.EditedOn = DateTime.Now;
                                db.SaveChanges();
                                objResponseMsg.Value = "Record updated successfully";
                                objResponseMsg.Key = true;
                            }

                        }
                        #endregion
                    }
                    #endregion
                }
                else
                {
                    WML001 objWML001 = new WML001();
                    if (!jointtypeDesc.Contains("T#TS") && !jointtype.Contains("T#TS"))
                    {
                        objWML001 = db.WML001.Where(a => a.Location == location && a.Welder == Welder && a.WelderStampNo == WelderStampNo && a.WMLType == wmltype && a.WQTNo == WQTNo && a.Project == project).FirstOrDefault();
                        if (objWML001 == null)
                        {
                            objWML001 = new WML001();
                            if (db.WML001.Any(a => a.Location == location && a.Welder == Welder && a.WelderStampNo == WelderStampNo && a.WMLType == wmltype && a.WQTNo == WQTNo && (a.WMLType == typeASME || a.Project == project))
                                || db.WML002.Any(a => a.Location == location && a.WelderName == Welder && a.WelderStampNo == WelderStampNo && a.WMLType == wmltype && a.WQTNo == WQTNo && (a.WMLType == typeASME || a.Project == project))
                                || db.WML003.Any(a => a.Location == location && a.WelderName == Welder && a.WelderStampNo == WelderStampNo && a.WMLType == wmltype && a.WQTNo == WQTNo && a.Project == project))
                            {
                                objResponseMsg.Value = "WQT No. is already being used";
                                objResponseMsg.Key = false;
                                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                            }
                            else { isSave = true; }
                        }

                        #region WPQ (Grove/Fillet)
                        if (objWQR004 != null)
                        {
                            #region WQR004 
                            objWML001.MaintainedBy = objWQR004.MaintainedBy;
                            objWML001.ModifiedBy = objWQR004.ModifiedBy;
                            objWML001.MaintainedOn = objWQR004.MaintainedOn;
                            objWML001.ModifiedOn = objWQR004.ModifiedOn;
                            objWML001.Location = objWQR004.Location;
                            objWML001.SpecificationNotes = objWQR004.SpecificationNotes;
                            objWML001.WQTNo = objWQR004.WQTNo;
                            objWML001.WPSNo = objWQR004.WPSNo;
                            objWML001.Welder = objWQR004.Welder;
                            objWML001.WelderStampNo = objWQR004.WelderStampNo;
                            objWML001.Shop = objWQR004.Shop;
                            objWML001.TypeOfTestCoupon = objWQR004.TypeOfTestCoupon;
                            objWML001.WeldingProcessQualified = objWQR004.WeldingProcessQualified;
                            objWML001.TypeQualified = objWQR004.TypeQualified;

                            objWML001.PNo = objWQR004.PNo;
                            objWML001.FNo = objWQR004.FNo;
                            objWML001.PlatePipeQualified = objWQR004.PlatePipeQualified;
                            objWML001.DepositedThicknessQualified = objWQR004.DepositedThicknessQualified;
                            objWML001.PlatePipePositionActual = objWQR004.PlatePipePositionActual;
                            objWML001.PipeWeldingPositionQualified = objWQR004.PipeWeldingPositionQualified;
                            objWML001.PlatePipePositionQualified = objWQR004.PlatePipePositionQualified;
                            objWML001.BaseMetalThicknessQualified = objWQR004.BaseMetalThicknessQualified;
                            objWML001.BackingQualified = objWQR004.BackingQualified;
                            objWML001.InertGasBackingQualified = objWQR004.InertGasBackingQualified;
                            objWML001.CircWeldOvly = objWQR004.CircWeldOvly;
                            objWML001.Remarks = objWQR004.Remarks;
                            //if (!objWML001.LastWeldDate.HasValue)
                            //{
                            objWML001.ValueUpto = objWQR004.ValueUpto;
                            objWML001.LastWeldDate = objWQR004.LastWeldDate;
                            // }
                            objWML001.JointType = objWQR004.JointType;
                            objWML001.Project = project;
                            objWML001.WMLType = wmltype;

                            if (isSave)
                            {
                                objWML001.CreatedBy = objClsLoginInfo.UserName;
                                objWML001.CreatedOn = DateTime.Now;
                                db.WML001.Add(objWML001);
                                db.SaveChanges();
                                objResponseMsg.Value = "Record added successfully";
                                objResponseMsg.Key = true;
                            }
                            else
                            {
                                objWML001.EditedBy = objClsLoginInfo.UserName;
                                objWML001.EditedOn = DateTime.Now;
                                db.SaveChanges();
                                objResponseMsg.Value = "Record updated successfully";
                                objResponseMsg.Key = true;
                            }
                            #endregion
                        }
                        else
                        {
                            #region WQR005
                            if (objWQR005 != null)
                            {
                                objWML001.MaintainedBy = objWQR005.MaintainedBy;
                                objWML001.ModifiedBy = objWQR005.ModifiedBy;
                                objWML001.MaintainedOn = objWQR005.MaintainedOn;
                                objWML001.ModifiedOn = objWQR005.ModifiedOn;
                                objWML001.Location = objWQR005.Location;
                                objWML001.SpecificationNotes = objWQR005.SpecificationNotes;
                                objWML001.WQTNo = objWQR005.WQTNo;
                                objWML001.WPSNo = objWQR005.WPSNo;
                                objWML001.Welder = objWQR005.WelderName;
                                objWML001.WelderStampNo = objWQR005.WelderStampNo;
                                objWML001.Shop = objWQR005.Shop;
                                objWML001.TypeOfTestCoupon = objWQR005.TypeOfTestCoupon;
                                objWML001.WeldingProcessQualified = !string.IsNullOrWhiteSpace(objWQR005.WeldingProcessEsQualified) ? objWQR005.WeldingProcessEsQualified : objWQR005.WeldingProcessMachineQualified;
                                objWML001.TypeQualified = objWQR005.WeldingType;

                                objWML001.PNo = objWQR005.PNo;
                                objWML001.FNo = objWQR005.FNo;
                                objWML001.PlatePipeQualified = objWQR005.PlatePipeEnterDiaIfPipeOrTubeQualified;
                                objWML001.PlatePipePositionActual = objWQR005.IPlatePipeOverlayWeldingPositionQualified;
                                objWML001.PipeWeldingPositionQualified = objWQR005.IiPipeFilletWeldingPositionQualified;
                                objWML001.PlatePipePositionQualified = objWQR005.IPlatePipeFilletWeldingPositionQualified;
                                objWML001.BackingQualified = objWQR005.BackingQualified;
                                objWML001.CircWeldOvly = objWQR005.CircWeldOvly;
                                objWML001.Remarks = objWQR005.Remarks;
                                //if (!objWML001.LastWeldDate.HasValue)
                                // {
                                objWML001.LastWeldDate = objWQR005.LastWeldDate;
                                objWML001.ValueUpto = objWQR005.ValueUpto;
                                //}
                                objWML001.JointType = objWQR005.JointType;
                                objWML001.Project = project;
                                objWML001.WMLType = wmltype;

                                if (isSave)
                                {
                                    objWML001.CreatedBy = objClsLoginInfo.UserName;
                                    objWML001.CreatedOn = DateTime.Now;
                                    db.WML001.Add(objWML001);
                                    db.SaveChanges();
                                    objResponseMsg.Value = "Record added successfully";
                                    objResponseMsg.Key = true;
                                }
                                else
                                {
                                    objWML001.EditedBy = objClsLoginInfo.UserName;
                                    objWML001.EditedOn = DateTime.Now;
                                    db.SaveChanges();
                                    objResponseMsg.Value = "Record updated successfully";
                                    objResponseMsg.Key = true;
                                }

                            }
                        }
                        #endregion
                    }
                    #endregion
                }
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}