﻿using IEMQS.Areas.Utility.Controllers;
using IEMQS.Areas.WQ.Models;
using IEMQS.DESCore;
using IEMQS.DESServices;
using IEMQS.Models;
using IEMQSImplementation;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Web.Mvc;


namespace IEMQS.Areas.ICS.Controllers
{
    public class MaintainController : clsBase
    {
        // GET: ICS/Maintain
        #region Header List
        [SessionExpireFilter]
        [AllowAnonymous]
        // [UserPermissions]
        public ActionResult Index()
        {
            ViewBag.Title = "Maintain ICS";
            //ViewBag.IndexType = clsImplementationEnum.CFARIndexType.maintain.GetStringValue();
            return View();
        }

        //load tab wise partial
        [HttpPost]
        public ActionResult LoadDataGridPartial(string status, string title, string indextype)
        {
            ViewBag.Status = status;
            ViewBag.GridTitle = title;
            ViewBag.IndexDataFor = indextype;
            return PartialView("_GetIndexGridDataPartial");
        }
        //bind datatable for Data Grid
        [HttpPost]
        public JsonResult LoadDataGridData(JQueryDataTableParamModel param)

        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                strWhereCondition += "1=1 ";
                if (param.Status.ToLower() == "pending")
                {
                    strWhereCondition += " and Status  in ('" + clsImplementationEnum.CFARStatus.Draft.GetStringValue() + "') and CreatedBy = '" + objClsLoginInfo.UserName.Trim() + "'";
                }
                //search Condition 

                string[] columnName = {
                           "QualityProject",
                           "ICSNo",
                           "InitiatingInspectionCentre",
                           "CreatedOn"};
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                if (strWhereCondition.Contains("ClearedNextOperation") == true)
                {

                    if (strWhereCondition.ToLower().Contains("yes"))
                    {
                        strWhereCondition = strWhereCondition.ToLower().Replace("like '%yes%'", "= 'True'");
                    }
                    else
                    {
                        strWhereCondition = strWhereCondition.ToLower().Replace("like '%no%'", "= 'False'");
                    }
                }
                var lstResult = db.SP_ICS_INDEX_DATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.HeaderId),
                            Convert.ToString(uc.QualityProject),
                            Convert.ToString(uc.ICSNo),
                            Convert.ToString(uc.PartNo),
                            Convert.ToString(uc.InitiatingInspectionCentre),
                            Convert.ToString(uc.ClearedToShop),
                            uc.ClearedNextOperation==true?"Yes":"No",
                            uc.CreatedOn==null || uc.CreatedOn.Value==DateTime.MinValue?"":uc.CreatedOn.Value.ToString("dd/MM/yyyy" ,CultureInfo.InvariantCulture),
                            Convert.ToString(string.IsNullOrWhiteSpace(uc.SubmittedBy) ?uc.CreatedBy :uc.SubmittedBy),
                            Convert.ToString(uc.Status),
                            Helper.GenerateActionIcon(uc.HeaderId, "View", "View Detail", "fa fa-eye", "",WebsiteURL +"/ICS/Maintain/Details/"+uc.HeaderId ,false)
                            +    Helper.GenerateActionIcon(uc.HeaderId, "Print", "Print Detail", "fa fa-print","printReport("+uc.HeaderId+")","" ,false)

                          }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Add header
        [SessionExpireFilter]
        public ActionResult Details(int id = 0, string urlForm = "")
        {
            ICS001 objICS001 = new ICS001();
            string Heading = string.Empty;
            var Role = clsImplementationEnum.UserRoleName.MFG1.GetStringValue() + "," + clsImplementationEnum.UserRoleName.MFG2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PROD3.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PMG1.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PMG2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PMG3.GetStringValue() + "," + clsImplementationEnum.UserRoleName.QC2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.QC3.GetStringValue();
            // ViewBag.Users = (new GeneralController()).GetRoleWiseEmployee(Role).ToList();
            ViewBag.Users = (new GeneralController()).GetICSEmployee().ToList();

            List<string> lstyesnona = clsImplementationEnum.getYesNoNA().ToList();
            ViewBag.YesNoNAEnum = lstyesnona.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ModelWQRequest objModelWQRequest = new ModelWQRequest();
            List<CategoryData> lstShop = objModelWQRequest.GetSubCatagorywithoutBULocationCode("Shop").ToList();
            ViewBag.lstShop = lstShop;
            ViewBag.Button = "false";
            if (id > 0)
            {
                objICS001 = db.ICS001.Where(x => x.HeaderId == id).FirstOrDefault();
                ViewBag.QualityprojectDesc = objICS001.QualityProject;
                ViewBag.InitiatingInspectionCentre = lstShop.Where(x => x.Value == objICS001.InitiatingInspectionCentre).Select(x => x.CategoryDescription).FirstOrDefault();
                ViewBag.ClearedToShop = lstShop.Where(x => x.Value == objICS001.ClearedToShop).Select(x => x.CategoryDescription).FirstOrDefault();
                string project = db.QMS010.Where(x => x.QualityProject == objICS001.QualityProject).Select(x => x.Project).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(objICS001.PartNo))
                {
                    var PartNo = Manager.GetHBOMData(project).Where(x => x.FindNo == objICS001.PartNo).FirstOrDefault();
                    //var PartNo = db.VW_IPI_GETHBOMLIST.Where(x => x.Project == project && x.FindNo == objICS001.PartNo).FirstOrDefault();
                    ViewBag.PartNo = PartNo != null ? (PartNo.Part + " - " + PartNo.Description) : "";
                }
                ViewBag.Action = "edit";
                if (objClsLoginInfo.UserName == objICS001.CreatedBy && objICS001.Status == clsImplementationEnum.CommonStatus.DRAFT.GetStringValue())
                {
                    ViewBag.Button = "true";
                }else
                {
                    ViewBag.Button = "false";
                }
                objICS001.Status = objICS001.Status;
            }
            else
            {
                try
                {
                    clsHelper.ResponseMsgWithStatus objResponseMsg1 = new clsHelper.ResponseMsgWithStatus();
                    objResponseMsg1 = GetICSNo("");
                    objICS001.ICSNo = objResponseMsg1.Value;
                    objICS001.CreatedBy = objClsLoginInfo.UserName;
                    objICS001.CreatedOn = DateTime.Now;
                    db.ICS001.Add(objICS001);
                    db.SaveChanges();
                    objICS001.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                    ViewBag.Button = "true";
                }
                catch (Exception ex)
                {
                    // Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    // objResponseMsg.Key = false;
                    // objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
                }

            }
           // ViewBag.Button = "true";
            return View(objICS001);
        }

        [HttpPost]
        public ActionResult SaveHeader(ICS001 ics001)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            clsHelper.ResponseMsgWithStatus objResponseMsg1 = new clsHelper.ResponseMsgWithStatus();
            try
            {
                ICS001 objICS001 = new ICS001();
                if (ics001.HeaderId > 0)
                {
                    objICS001 = db.ICS001.Where(o => o.HeaderId == ics001.HeaderId).FirstOrDefault();
                    objICS001.QualityProject = ics001.QualityProject;
                    //  objResponseMsg1 = GetICSNo(ics001.QualityProject);
                    // objICS001.ICSNo = objResponseMsg1.Value;
                    objICS001.DrawingNo = ics001.DrawingNo;
                    objICS001.PartNo = ics001.PartNo;
                    objICS001.InitiatingInspectionCentre = ics001.InitiatingInspectionCentre;
                    objICS001.ClearedToShop = ics001.ClearedToShop;
                    objICS001.Identification = ics001.Identification;
                    objICS001.Quantity = ics001.Quantity;
                    objICS001.InspectedOperations = ics001.InspectedOperations;
                    objICS001.Remarks = ics001.Remarks;
                    objICS001.ClearedNextOperation = ics001.ClearedNextOperation;
                    objICS001.InspectionEngineer = ics001.InspectionEngineer;
                    objICS001.SendEmailTo = ics001.SendEmailTo;
                    objICS001.QualityProjectTo = ics001.QualityProjectTo;
                    objICS001.EditedBy = objClsLoginInfo.UserName;
                    objICS001.EditedOn = DateTime.Now;
                    objICS001.TPICustomerCleared = ics001.TPICustomerCleared;
                    objICS001.Status = clsImplementationEnum.CFARStatus.Draft.GetStringValue();
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update;
                    objResponseMsg.Status = objICS001.Status;
                    objResponseMsg.HeaderId = objICS001.HeaderId;
                }
                else
                {
                    objICS001.Status = clsImplementationEnum.CFARStatus.Draft.GetStringValue();

                    objResponseMsg1 = GetICSNo(ics001.QualityProject);

                    objICS001.ICSNo = objResponseMsg1.Value;
                    objICS001.QualityProject = ics001.QualityProject;
                    objICS001.DrawingNo = ics001.DrawingNo;
                    objICS001.PartNo = ics001.PartNo;
                    objICS001.InitiatingInspectionCentre = ics001.InitiatingInspectionCentre;
                    objICS001.ClearedToShop = ics001.ClearedToShop;
                    objICS001.Identification = ics001.Identification;
                    objICS001.Quantity = ics001.Quantity;
                    objICS001.InspectedOperations = ics001.InspectedOperations;
                    objICS001.Remarks = ics001.Remarks;
                    objICS001.ClearedNextOperation = ics001.ClearedNextOperation;
                    objICS001.InspectionEngineer = ics001.InspectionEngineer;
                    objICS001.SendEmailTo = ics001.SendEmailTo;
                    objICS001.QualityProjectTo = ics001.QualityProjectTo;
                    objICS001.CreatedBy = objClsLoginInfo.UserName;
                    objICS001.CreatedOn = DateTime.Now;
                    objICS001.TPICustomerCleared = ics001.TPICustomerCleared;
                    db.ICS001.Add(objICS001);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;
                    objResponseMsg.Status = objICS001.Status;
                    objResponseMsg.HeaderId = objICS001.HeaderId;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Submit Header
        [HttpPost]
        public ActionResult SubmitHeader(int strHeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                DateTime? actionon = DateTime.Now;
                List<string> emailTo = new List<string>();
                List<string> emailCc = new List<string>();
                string emailfrom = "";

                ICS001 objICS001 = db.ICS001.Where(u => u.HeaderId == strHeaderId).SingleOrDefault();
                if (objICS001 != null)
                {
                    List<string> lstrole = objClsLoginInfo.GetUserRoleList();
                    //return to draft
                    if (objICS001.Status == clsImplementationEnum.CFARStatus.Draft.GetStringValue())
                    {
                        objICS001.Status = clsImplementationEnum.AutoCJCStatus.Submitted.GetStringValue();
                        objICS001.SubmittedBy = objClsLoginInfo.UserName;
                        objICS001.SubmittedOn = DateTime.Now;

                        emailfrom = objClsLoginInfo.UserName;
                        db.SaveChanges();
                        objResponseMsg.ActionKey = true;
                        objResponseMsg.ActionValue = "ICS : " + objICS001.ICSNo + " submitted successfully";
                        #region Send Mail
                        if (!string.IsNullOrWhiteSpace(objICS001.SendEmailTo))
                        {
                            foreach (var person in objICS001.SendEmailTo.Split(','))
                            {
                                emailTo.Add(Manager.GetMailIdFromPsNo(person));
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(objICS001.CreatedBy))
                        {
                            foreach (var person in objICS001.CreatedBy.Split(','))
                            {
                                emailCc.Add(Manager.GetMailIdFromPsNo(person));
                            }
                        }

                        string ICSNo = objICS001.ICSNo;
                        #region generate pdf

                        string filenameformat = objICS001.ICSNo;
                        string downloadpath = "~/Resources/ICSEmailAttachments/";
                        string Reporturl = "/ICS/ICS Detail";

                        List<SSRSParam> reportParams = new List<SSRSParam>();
                        reportParams.Add(new SSRSParam { Param = "HeaderId", Value = Convert.ToString(objICS001.HeaderId) });

                        string enumICSReportImageURL = clsImplementationEnum.ConfigParameter.ExternalImagePath.GetStringValue();

                        string ICSReportImageURL = Manager.GetConfigValue(enumICSReportImageURL).ToString();

                        string reportFilename = (new Utility.Controllers.GeneralController()).GeneratePDFfromSSRSreport(Server.MapPath(downloadpath + filenameformat + ".pdf"), Reporturl, reportParams);
                        string reportImageFilename = (new Utility.Controllers.GeneralController()).GenerateUploadImagefromSSRSreport(Server.MapPath(downloadpath + filenameformat + ".jpeg"), Reporturl, reportParams);

                        string folderpath = "ICS001/" + objICS001.HeaderId;
                        //var doc = (new clsFileUpload()).GetDocuments(folderpath, true).ToList();
                        var doc = db.FCS001.Where(f => f.TableId == objICS001.HeaderId && f.TableName == "ICS001").ToList();

                        #endregion

                        if (emailTo.Count > 0)
                        {
                            try
                            {
                                Hashtable _ht = new Hashtable();
                                EmailSend _objEmail = new EmailSend();
                                //_ht["[CC]"] = ccTo;
                                _ht["[ICSNo]"] = ICSNo;

                                _ht["[FromEmail]"] = Manager.GetUserNameFromPsNo(emailfrom);
                                _ht["[ProjectNo]"] = objICS001.QualityProject;

                                System.Net.Mail.AttachmentCollection lstAttch = _objEmail.MailAttachMentCollection;

                                System.Net.Mail.Attachment att = new System.Net.Mail.Attachment(reportImageFilename);
                                string contentID = ICSNo;
                                att.ContentId = contentID;

                                _ht["[ICSReportPath]"] = "cid:" + contentID;
                                lstAttch.Add(att);

                                lstAttch.Add(new System.Net.Mail.Attachment(reportFilename));

                               
                                //CURRENTLY WE FIX THE CONDITION FOR HZW | WE HAVE TO PUT CONDITION FOR LOCATION
                                var UserName = Manager.GetConfigValue("FCS_HZW_UserName").ToString();
                                var Password = Manager.GetConfigValue("FCS_HZW_Password").ToString();
                                var Domain = Manager.GetConfigValue("FCS_HZW_Domain").ToString();
                                IpLocation ipLocation = CommonService.GetUseIPConfig;
                                string[] ipArray = ipLocation.File_Path.Split('/');
                                var NetworkDomain = "\\\\" + ipArray[0];
                                var NetCre = new NetworkCredential(UserName, Password, Domain);

                                foreach (var item in doc)
                                {
                                    #region OLD CODE
                                    //var UserName = System.Configuration.ConfigurationManager.AppSettings["File_Upload_UserID"];
                                    //var Password = System.Configuration.ConfigurationManager.AppSettings["File_Upload_Password"];
                                    //var NetworkDomain = System.Configuration.ConfigurationManager.AppSettings["NetworkDomain"];
                                    //var client = new System.Net.WebClient();
                                    //client.Credentials = new NetworkCredential(UserName, Password, NetworkDomain);
                                    //client.DownloadFile(item.MainDocumentPath, Server.MapPath(downloadpath + item.Document_name));
                                    //Attachment attachment = new Attachment(Server.MapPath(downloadpath + item.Document_name));
                                    //lstAttch.Add(attachment);
                                    #endregion

                                    using (new ConnectToSharedFolder(NetworkDomain + "\\FCSFiles", NetCre))
                                    {
                                        System.IO.File.Copy("\\"+item.MainDocumentPath, Server.MapPath(downloadpath + item.Document_name), true);
                                        Attachment attachment = new Attachment(Server.MapPath(downloadpath + item.Document_name));
                                        lstAttch.Add(attachment);
                                    }
                                }
                                MAIL001 objTemplateMaster = new MAIL001();
                                objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.ICS.ICSReport).SingleOrDefault();
                                _objEmail.MailToAdd = string.Join(",", emailTo.Distinct());
                                _objEmail.MailAttachMentCollection = lstAttch;
                                _objEmail.MailCc = string.Join(",", emailCc.Distinct());
                                _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                            }
                            catch (Exception ex)
                            {
                                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.ActionKey = false;
                        objResponseMsg.ActionValue = "Please try again";
                    }
                }
                else
                {
                    objResponseMsg.ActionKey = true;
                    objResponseMsg.ActionValue = "Details not available.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Copy ICS
        [HttpPost]
        public ActionResult GetCopyDetails()
        {
            return PartialView("_GetICopyPartial");
        }

        [HttpPost]
        public ActionResult CopyHeader(string icsno, int headerid)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            clsHelper.ResponseMsgWithStatus objResponseMsg1 = new clsHelper.ResponseMsgWithStatus();
            try
            {
                bool isAdded = false;
                string status = clsImplementationEnum.CFARActionType.Submitted.GetStringValue();
                ICS001 objDestICS001 = new ICS001();
                ICS001 objSrcICS001 = db.ICS001.Where(o => o.ICSNo == icsno).FirstOrDefault();
                if (objSrcICS001 != null)
                {
                    if (headerid > 0)
                    {
                        objDestICS001 = db.ICS001.Where(o => o.HeaderId == headerid).FirstOrDefault();
                        if (objDestICS001 != null && objDestICS001?.Status == status)
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = objDestICS001.ICSNo + " is not in Draft status.";
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        objResponseMsg1 = GetICSNo(objSrcICS001.QualityProject);
                        objDestICS001.ICSNo = objResponseMsg1.Value;
                        isAdded = true;
                    }
                    objDestICS001.QualityProject = objSrcICS001.QualityProject;
                    objDestICS001.DrawingNo = objSrcICS001.DrawingNo;
                    objDestICS001.PartNo = objSrcICS001.PartNo;
                    objDestICS001.InitiatingInspectionCentre = objSrcICS001.InitiatingInspectionCentre;
                    objDestICS001.ClearedToShop = objSrcICS001.ClearedToShop;
                    objDestICS001.Identification = objSrcICS001.Identification;
                    objDestICS001.Quantity = objSrcICS001.Quantity;
                    objDestICS001.InspectedOperations = objSrcICS001.InspectedOperations;
                    objDestICS001.Remarks = objSrcICS001.Remarks;
                    objDestICS001.ClearedNextOperation = objSrcICS001.ClearedNextOperation;
                    objDestICS001.InspectionEngineer = objSrcICS001.InspectionEngineer;
                    objDestICS001.SendEmailTo = objSrcICS001.SendEmailTo;
                    objDestICS001.QualityProjectTo = objSrcICS001.QualityProjectTo;
                    objDestICS001.Status = clsImplementationEnum.CFARStatus.Draft.GetStringValue();
                    if (isAdded)
                    {
                        objDestICS001.CreatedBy = objClsLoginInfo.UserName;
                        objDestICS001.CreatedOn = DateTime.Now;
                        db.ICS001.Add(objDestICS001);
                    }
                    else
                    {
                        objDestICS001.EditedBy = objClsLoginInfo.UserName;
                        objDestICS001.EditedOn = DateTime.Now;
                    }
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.CopyRecord;
                    objResponseMsg.Status = objDestICS001.Status;
                    objResponseMsg.HeaderId = objDestICS001.HeaderId;

                    var folderPath = "ICS001/" + objDestICS001.HeaderId;
                    var oldFolderPath = "ICS001/" + objSrcICS001.HeaderId;
                    //(new clsFileUpload()).CopyFolderContentsAsync(oldFolderPath, folderPath);
                    FileUploadController _objFUC = new FileUploadController();
                    _objFUC.CopyDataOnFCSServerAsync(oldFolderPath, folderPath, "ICS001", objSrcICS001.HeaderId, "ICS001", objDestICS001.HeaderId, CommonService.GetUseIPConfig, CommonService.objClsLoginInfo.UserName,0,true);
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details not found";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Common
        [HttpPost]
        public ActionResult GetICSNotoCopy()
        {
            try
            {
                string status = clsImplementationEnum.CFARActionType.Submitted.GetStringValue();
                var obj = db.ICS001.Where(x => x.Status == status && (x.ICSNo != null && x.ICSNo != "")).OrderByDescending(x => x.HeaderId).Select(x => new { Text = x.ICSNo + " (" + x.QualityProject + ")", Value = x.ICSNo }).ToList();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult GetICSNoandEmailPerson(string qproject)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                objResponseMsg = GetICSNo(qproject);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                //objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponseMsgWithStatus GetICSNo(string qproject)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            string strRequestNo = string.Empty;
            string strEmailPerson = string.Empty;
            int num = 1;
            var objICS001 = db.ICS001.OrderByDescending(x => x.HeaderId).FirstOrDefault();
            if (objICS001 != null)
            {
                string ICSNumber = objICS001.ICSNo;
                num = Convert.ToInt32(ICSNumber.Replace("ICS", "")) + 1;
                strRequestNo = "ICS" + num.ToString("D6");
                strEmailPerson = objICS001.SendEmailTo;
            }
            else
            {
                strRequestNo = "ICS" + num.ToString("D6");
            }
            objResponseMsg.Value = strRequestNo;
            //  objResponseMsg.dataValue = strEmailPerson;
            return objResponseMsg;
        }
        [HttpPost]
        public ActionResult GetAllDrawingNoQualityProject(string param = "")
        {
            try
            {
                string project = db.QMS010.Where(x => x.QualityProject == param).Select(x => x.Project).FirstOrDefault();
                List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(project).ToList();
                var item = lstdrawing.AsEnumerable().Select(x => new { id = x, text = project + " - " + x, Text = x, Value = project + " - " + x, }).ToList();
                return Json(item, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetAllDrawingNoQualityProjectonEdit(string param = "", int headerid = 0, string QualityProjectto = "")
        {
            try
            {
                List<CategoryData> itemlist = new List<CategoryData>();
                if (!string.IsNullOrEmpty(QualityProjectto))
                {
                    var lstProjectByUser = Manager.getProjectsByUser(objClsLoginInfo.UserName.Trim());
                    var ProjectList = (from lst in lstProjectByUser
                                       join qms10 in db.QMS010 on lst.projectCode equals qms10.Project
                                       where (qms10.QualityProject.CompareTo(param) >= 0 && qms10.QualityProject.CompareTo(QualityProjectto) <= 0)
                                       orderby qms10.QualityProject
                                       select new { Value = qms10.Project, Text = qms10.Project }).Distinct().ToList();
                    foreach (var project in ProjectList)
                    {
                        List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(project.Value).ToList();
                        var item = lstdrawing.AsEnumerable().Select(x => new CategoryData { id = project.Value + " - " + x, text = project.Value + " - " + x, Value = project.Value + " - " + x, }).ToList();
                        itemlist.AddRange(item);
                    }
                }
                else
                {

                    string project = db.QMS010.Where(x => x.QualityProject == param).Select(x => x.Project).FirstOrDefault();
                    List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(project).ToList();

                    var item = lstdrawing.AsEnumerable().Select(x => new CategoryData { id = project + " - " + x, text = project + " - " + x, Value = project + " - " + x, }).ToList();
                    itemlist.AddRange(item);
                }
                string strResult = db.ICS001.Where(s => s.HeaderId == headerid).Select(s => s.DrawingNo).FirstOrDefault();
                var lstresult = strResult.Split(',').ToList();
                var lstProjectid = itemlist.Select(x => x.id).ToList();
                foreach (var lst in lstresult)
                {
                    if (!lstProjectid.Contains(lst))
                    {
                        itemlist.Add(new CategoryData { id = lst, text = lst, Description = "newData" });
                    }
                }

                return Json(itemlist, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetAllPartbyProject(string param = "")
        {
            try
            {
                string project = db.QMS010.Where(x => x.QualityProject == param).Select(x => x.Project).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(project))
                {
                    var lstView = Manager.GetHBOMData(project);
                    var result = (from vw in lstView
                                  select new { FindNo = vw.FindNo, Part = vw.Part, Description = vw.Description }).Distinct().ToList();
                    //var result = (from vw in db.VW_IPI_GETHBOMLIST
                    //              where vw.Project == project
                    //              select new { FindNo = vw.FindNo, Part = vw.Part, Description = vw.Description }).Distinct().ToList();

                    var items = (from li in result
                                 select new
                                 {
                                     id = li.FindNo.ToString(),
                                     text = li.Part.ToString() + " - " + li.Description.ToString()
                                 }).Distinct().ToList();
                    return Json(items, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }



        public ActionResult GetAllPartbyProjectonEdit(string param = "", int headerid = 0)
        {
            try
            {
                string project = db.QMS010.Where(x => x.QualityProject == param).Select(x => x.Project).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(project))
                {
                    var lstView = Manager.GetHBOMData(project);
                    var result = (from vw in lstView
                                  select new { FindNo = vw.FindNo, Part = vw.Part, Description = vw.Description }).Distinct().ToList();

                    //var result = (from vw in db.VW_IPI_GETHBOMLIST
                    //              where vw.Project == project
                    //              select new { FindNo = vw.FindNo, Part = vw.Part, Description = vw.Description }).Distinct().ToList();

                    List<CategoryData> items = (from li in result
                                                select new CategoryData
                                                {
                                                    id = li.FindNo.ToString(),
                                                    text = li.Part.ToString() + " - " + li.Description.ToString()
                                                }).Distinct().ToList();

                    string strResult = db.ICS001.Where(s => s.HeaderId == headerid).Select(s => s.PartNo).FirstOrDefault();
                    var lstresult = strResult.Split(',').ToList();
                    var lstProjectid = items.Select(x => x.id).ToList();
                    foreach (var lst in lstresult)
                    {
                        if (!lstProjectid.Contains(lst))
                        {
                            items.Add(new CategoryData { id = lst, text = lst, Description = "newData" });
                        }
                    }

                    return Json(items, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                #region Maintain header
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_ICS_INDEX_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }

                    var newlst = (from uc in lst
                                  select new
                                  {
                                      QualityProject = Convert.ToString(uc.QualityProject),
                                      ICSNo = Convert.ToString(uc.ICSNo),
                                      PartNo = Convert.ToString(uc.PartNo),
                                      ClearedToShop = Convert.ToString(uc.ClearedToShop),
                                      InitiatingInspectionCentre = uc.CreatedOn == null || uc.CreatedOn.Value == DateTime.MinValue ? "" : uc.CreatedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      CreatedOn = uc.CreatedOn == null || uc.CreatedOn.Value == DateTime.MinValue ? "" : uc.CreatedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      IssuedBy = Convert.ToString(string.IsNullOrWhiteSpace(uc.SubmittedBy) ? uc.CreatedBy : uc.SubmittedBy),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                #endregion

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetAllDrawingNoQualityProjectto(string QualityProjectfrom, string QualityProjectto)
        {
            try
            {
                List<CategoryData> itemlist = new List<CategoryData>();
                if (!string.IsNullOrEmpty(QualityProjectto))
                {
                    var lstProjectByUser = Manager.getProjectsByUser(objClsLoginInfo.UserName.Trim());
                    var ProjectList = (from lst in lstProjectByUser
                                       join qms10 in db.QMS010 on lst.projectCode equals qms10.Project
                                       where (qms10.QualityProject.CompareTo(QualityProjectfrom) >= 0 && qms10.QualityProject.CompareTo(QualityProjectto) <= 0)
                                       orderby qms10.QualityProject
                                       select new { Value = qms10.Project, Text = qms10.Project }).Distinct().ToList();
                    foreach (var project in ProjectList)
                    {
                        List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(project.Value).ToList();
                        var item = lstdrawing.AsEnumerable().Select(x => new CategoryData { id = project.Value + " - " + x, text = project.Value + " - " + x, Value = project.Value + " - " + x, }).ToList();
                        itemlist.AddRange(item);
                    }
                }
                else
                {
                    string project = db.QMS010.Where(x => x.QualityProject == QualityProjectfrom).Select(x => x.Project).FirstOrDefault();
                    List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(project).ToList();
                    var item = lstdrawing.AsEnumerable().Select(x => new CategoryData { id = project + " - " + x, text = project + " - " + x, Value = project + " - " + x, }).ToList();
                    itemlist.AddRange(item);

                }

                return Json(itemlist, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        #region line table

        [HttpPost]
        public ActionResult GetICSLinePartial(int HeaderId, string Status)
        {
            ViewBag.HeaderId = HeaderId;
            ViewBag.IsDisplayOnly = false;
            ViewBag.ReqStatus = Status;

            return PartialView("_GetInspectionOperationHtmlPartial");
        }
        [HttpPost]
        public JsonResult LoadOpData(JQueryDataTableParamModel param, string ReqStatus)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var IsDisplayOnly = param.IsDisplayOnly;// !string.IsNullOrEmpty(Request["IsDisplayOnly"]) ? Convert.ToBoolean(Request["IsDisplayOnly"].ToString()) : false;
                var isEditable = true;
                string strWhere = "1=1";

                if (param.Headerid != "")
                {
                    strWhere += " and HeaderId='" + param.Headerid + "'";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and ProjectNo like '%" + param.sSearch + "%' or ItemNo like '%" + param.sSearch + "%' or NozzleNo like '%" + param.sSearch + "%' or Qty like '%" + param.sSearch + "%'  or SeamNo like '%" + param.sSearch + "%'";
                }
                if (!string.IsNullOrWhiteSpace(param.SearchFilter))
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                //strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "BU", "Location");
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                //if (!string.IsNullOrWhiteSpace(sortColumnName))
                //{
                //    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                //}
                var lstResult = db.SP_ICS_GetInspectionOperation
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();
                if (ReqStatus != "Submitted")
                {
                    var data = (from uc in lstResult
                                select new[]
                               {
                                // onblur='UpdateRemarks(this," + LineId.ToString() + ")' readonly='readonly' ProjectNo
                           Convert.ToString(uc.ROW_NO),
                           Convert.ToString("<input type='text' name=ProjectNo_"+uc.LineId+" class='form-control ProjectNo'   value='" + uc.ProjectNo + "' Hid='"+uc.HeaderId+"' Lid='"+uc.LineId+"' onchange='UpdateData(this)' />"),
                           Convert.ToString("<input type='text' name=ItemNo_"+uc.LineId+" class='form-control ItemNo'    value='" + uc.ItemNo + "' Hid='"+uc.HeaderId+"'  Lid='"+uc.LineId+"' onchange='UpdateData(this)' />"),
                           Convert.ToString("<input type='text' name=NozzleNo_"+uc.LineId+" class='form-control NozzleNo'  value='" + uc.NozzleNo + "' Hid='"+uc.HeaderId+"' Lid='"+uc.LineId+"' onchange='UpdateData(this)'  />"),
                           Convert.ToString("<input type='text' name=Qty_"+uc.LineId+" class='form-control Qty'  type='number'  value='" + uc.Qty + "' Hid='"+uc.HeaderId+"' Lid='"+uc.LineId+"' onchange='UpdateData(this)' />"),
                           Convert.ToString("<input type='text' name=SeamNo_"+uc.LineId+" class='form-control SeamNo'    value='" + uc.SeamNo + "' Hid='"+uc.HeaderId+"' Lid='"+uc.LineId+"' onchange='UpdateData(this)' />"),
                                              //       +    Helper.GenerateActionIcon(uc.HeaderId, "Print", "Print Detail", "fa fa-print","printReport("+uc.HeaderId+")","" ,false)
                                Helper.GenerateActionIcon(uc.LineId, "Delete", "Delete Record", "fa fa-trash-o","DeleteLine('"+uc.HeaderId+"','"+uc.LineId+"')","",false)
                            }).ToList();
                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                        iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                        aaData = data,
                        strSortOrder = strSortOrder,
                        whereCondition = strWhere
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var data = (from uc in lstResult
                                select new[]
                               {
                                // onblur='UpdateRemarks(this," + LineId.ToString() + ")' readonly='readonly' ProjectNo
                           Convert.ToString(uc.ROW_NO),
                           Convert.ToString("<input type='text' name=ProjectNo_"+uc.LineId+" class='form-control ProjectNo'   value='" + uc.ProjectNo + "' Hid='"+uc.HeaderId+"' Lid='"+uc.LineId+"' onchange='UpdateData(this)' readonly=true/>"),
                           Convert.ToString("<input type='text' name=ItemNo_"+uc.LineId+" class='form-control ItemNo'    value='" + uc.ItemNo + "' Hid='"+uc.HeaderId+"'  Lid='"+uc.LineId+"' onchange='UpdateData(this)' readonly=true/>"),
                           Convert.ToString("<input type='text' name=NozzleNo_"+uc.LineId+" class='form-control NozzleNo'  value='" + uc.NozzleNo + "' Hid='"+uc.HeaderId+"' Lid='"+uc.LineId+"' onchange='UpdateData(this)'  readonly=true/>"),
                           Convert.ToString("<input type='text' name=Qty_"+uc.LineId+" class='form-control Qty'  type='number'  value='" + uc.Qty + "' Hid='"+uc.HeaderId+"' Lid='"+uc.LineId+"' onchange='UpdateData(this)'readonly=true />"),
                           Convert.ToString("<input type='text' name=SeamNo_"+uc.LineId+" class='form-control SeamNo'    value='" + uc.SeamNo + "' Hid='"+uc.HeaderId+"' Lid='"+uc.LineId+"' onchange='UpdateData(this)' readonly=true/>"),
                                              //       +    Helper.GenerateActionIcon(uc.HeaderId, "Print", "Print Detail", "fa fa-print","printReport("+uc.HeaderId+")","" ,false)
                                Helper.GenerateActionIcon(uc.LineId, "Delete", "Delete Record", "fa fa-trash-o","DeleteLine('"+uc.HeaderId+"','"+uc.LineId+"')","",false)
                            }).ToList();
                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                        iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                        aaData = data,
                        strSortOrder = strSortOrder,
                        whereCondition = strWhere
                    }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                ex.Message.ToString();
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult UpdateLineData(string HeaderID, string LineId, string ColName, string colValue)
        {
            ColName = ColName.Split('_')[0];
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            int HID = 0, LID = 0;
            try
            {
                if (HeaderID != "" && LineId != "")
                {
                    HID = Convert.ToInt32(HeaderID);
                    LID = Convert.ToInt32(LineId);
                }
                ICS002 iCS002 = new ICS002();
                if (db.ICS002.Any(x => x.LineId == LID && x.HeaderId == HID))
                {
                    iCS002 = db.ICS002.Where(x => x.LineId == LID && x.HeaderId == HID).FirstOrDefault();
                    if (ColName == "ProjectNo")
                    {
                        iCS002.ProjectNo = colValue;
                        iCS002.EditedBy = objClsLoginInfo.UserName;
                        iCS002.EditedOn = DateTime.Now;
                    }
                    if (ColName == "ItemNo")
                    {
                        iCS002.ItemNo = colValue;
                        iCS002.EditedBy = objClsLoginInfo.UserName;
                        iCS002.EditedOn = DateTime.Now;
                    }
                    if (ColName == "NozzleNo")
                    {
                        iCS002.NozzleNo = colValue;
                        iCS002.EditedBy = objClsLoginInfo.UserName;
                        iCS002.EditedOn = DateTime.Now;
                    }
                    if (ColName == "SeamNo")
                    {
                        iCS002.SeamNo = colValue;
                        iCS002.EditedBy = objClsLoginInfo.UserName;
                        iCS002.EditedOn = DateTime.Now;
                    }
                    if (ColName == "Qty")
                    {
                        int qty = Convert.ToInt32(colValue);
                        iCS002.Qty = qty;
                        iCS002.EditedBy = objClsLoginInfo.UserName;
                        iCS002.EditedOn = DateTime.Now;
                    }
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteLine(string HeaderID, string LineId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            int HID = 0, LID = 0;
            try
            {
                if (HeaderID != "" && LineId != "")
                {
                    HID = Convert.ToInt32(HeaderID);
                    LID = Convert.ToInt32(LineId);
                }
                ICS002 iCS002 = new ICS002();
                if (db.ICS002.Any(x => x.LineId == LID && x.HeaderId == HID))
                {
                    iCS002 = db.ICS002.Where(x => x.LineId == LID && x.HeaderId == HID).FirstOrDefault();
                    db.ICS002.Remove(iCS002);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddLine(string HeaderID)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            int HID = 0, LID = 0;
            try
            {
                if (HeaderID != "")
                {
                    HID = Convert.ToInt32(HeaderID);
                }
                ICS002 iCS002 = new ICS002();
                iCS002.HeaderId = HID;
                iCS002.CreatedBy = objClsLoginInfo.UserName;
                iCS002.CreatedOn = DateTime.Now;
                db.ICS002.Add(iCS002);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}