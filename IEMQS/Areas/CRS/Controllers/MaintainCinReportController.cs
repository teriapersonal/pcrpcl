﻿using IEMQS.Areas.IPI.Controllers;
using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.CRS.Controllers
{
    public class MaintainCinReportController : clsBase
    {

        #region Utility

        [HttpPost]
        public ActionResult GetContractNos(string term, string strBus)
        {
            List<ApproverModel> lstContractNos = new List<ApproverModel>();
            string[] arrayBUs = { };
            if (!string.IsNullOrWhiteSpace(strBus))
            {
                arrayBUs = strBus.Split(',').ToArray();
            }
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstContractNos = (from cm004 in db.COM004
                                  join cm008 in db.COM008 on cm004.t_cono equals cm008.t_cono
                                  //join cm005 in db.COM005 on cm004.t_cono equals cm005.t_cono
                                  where arrayBUs.Contains(cm008.t_csbu) && (cm004.t_cono + "-" + cm004.t_desc).Contains(term)
                                  orderby cm004.t_cono
                                  select new ApproverModel { Code = cm004.t_cono, Name = cm004.t_cono + "-" + cm004.t_desc }).Distinct().ToList();

            }
            else
            {
                lstContractNos = (from cm004 in db.COM004
                                  join cm008 in db.COM008 on cm004.t_cono equals cm008.t_cono
                                  //  join cm005 in db.COM005 on cm004.t_cono equals cm005.t_cono
                                  where arrayBUs.Contains(cm008.t_csbu)
                                  orderby cm004.t_cono
                                  select new ApproverModel { Code = cm004.t_cono, Name = cm004.t_cono + "-" + cm004.t_desc }).Distinct().Take(10).ToList();
            }
            //if (!string.IsNullOrWhiteSpace(term))
            //{
            //    lstContractNos = (from cm004 in db.COM004
            //                      join cm008 in db.COM008 on cm004.t_cono equals cm008.t_cono
            //                      join cm005 in db.COM005 on cm004.t_cono equals cm005.t_cono
            //                      where arrayBUs.Contains(cm008.t_csbu) && (cm004.t_cono + "-" + cm005.t_desc).Contains(term)
            //                      orderby cm004.t_cono
            //                      select new ApproverModel { Code = cm004.t_cono, Name = cm004.t_cono + "-" + cm005.t_desc }).Distinct().ToList();

            //}
            //else
            //{
            //    lstContractNos = (from cm004 in db.COM004
            //                      join cm008 in db.COM008 on cm004.t_cono equals cm008.t_cono
            //                      join cm005 in db.COM005 on cm004.t_cono equals cm005.t_cono
            //                      where arrayBUs.Contains(cm008.t_csbu)
            //                      orderby cm004.t_cono
            //                      select new ApproverModel { Code = cm004.t_cono, Name = cm004.t_cono + "-" + cm005.t_desc }).Distinct().Take(10).ToList();
            //}
            return Json(lstContractNos, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetCustomers(string contractNo)
        {
            ApproverModel objCustomers = new ApproverModel();
            if (!string.IsNullOrWhiteSpace(contractNo))
            {
                objCustomers = (from cm004 in db.COM004
                                join cm005 in db.COM005 on cm004.t_cono equals cm005.t_cono
                                join cm006 in db.COM006 on cm004.t_ofbp equals cm006.t_bpid
                                where cm005.t_cono.Trim() == contractNo.Trim()
                                select new ApproverModel { Code = cm004.t_ofbp, Name = cm004.t_ofbp + "-" + cm006.t_nama }).FirstOrDefault();
            }
            return Json(objCustomers, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetProjects(string contractNo)
        {
            // List<ApproverModel> lstContractNos = new List<ApproverModel>();

            var lstProjects = (from cm001 in db.COM001
                               join cm005 in db.COM005 on cm001.t_cprj equals cm005.t_sprj
                               where cm001.t_psts == 2 && cm005.t_cono.Trim() == contractNo.Trim()
                               select new BULocWiseCategoryModel { CatDesc = (cm001.t_cprj + "-" + cm001.t_dsca), CatID = cm001.t_cprj }).Distinct().ToList();

            return Json(lstProjects, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetInspectionAgency(string Project)
        {
            try
            {
                string buCode = db.COM001.Where(i => i.t_cprj == Project).FirstOrDefault().t_entu;
                var lstInspectionAgency = Manager.GetSubCatagories("Inspection Agency", buCode, objClsLoginInfo.Location).Select(i => new BULocWiseCategoryModel { CatID = i.Code, CatDesc = i.Code + " - " + i.Description }).ToList();

                return Json(lstInspectionAgency, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public string GenerateTable(string contract, string project)
        {
            var objCRS002 = db.CRS002.Where(x => x.ContractNo == contract && x.Project == project).FirstOrDefault();
            var objCRS002_Log = db.CRS002_Log.Where(x => x.CINId == objCRS002.CINId).ToList();
            //var ArmList = db.CRS001_Log.Where(x => x.ContractNo == "C01150001" && x.Project == "0017001").GroupBy(x => x.ARMSet).ToList();

            var ArmList = db.CRS001_Log.Where(x => x.ContractNo == contract && x.Project == project)
                .GroupBy(u => u.ARMSet)
                .Select(grp => grp.ToList())
                .ToList();
            StringBuilder tableHtml = new StringBuilder();
            tableHtml.Append("<table class=\"table table-striped table-bordered table-hover dt-responsive\" width=\"100 % \" id=\"tblCINResult\" cellspacing=\"0\">");
            if ((objCRS002.Status == "Draft" || objCRS002.Status == "Issued") && objCRS002.RevNo == 0)
            {

                tableHtml.Append("<thead>");


                tableHtml.Append("<tr>");
                tableHtml.Append("<th rowspan=\"2\" class=\"all clsHeaderColor\"></th>");
                tableHtml.Append("<th class=\"all scenter clsHeaderColor\">Project</th>");
                tableHtml.Append("<th  colspan=\"1\" id=\"txttblProject\" class=\"all scenter clsHeaderColor\"></th>");
                tableHtml.Append("</tr>");

                tableHtml.Append("<tr>");
                tableHtml.Append("<th class=\"all scenter clsHeaderColor\">Customer</th>");
                tableHtml.Append("<th colspan=\"1\" id=\"txttblCustomer\" class=\"all scenter clsHeaderColor\"></th>");
                tableHtml.Append("</tr>");

                tableHtml.Append("<tr>");
                tableHtml.Append("<th rowspan=\"3\" class=\"all clsHeaderColor\"><img style=\"height:90px;\" src=\"" + WebsiteURL + "/Images/Logo.jpg\" alt=\"L & T Logo\"></th>");
                tableHtml.Append("<th class=\"all scenter clsHeaderColor\">Issued By</th>");
                var objlog = objCRS002_Log.Where(x => x.RevNo == objCRS002.RevNo).FirstOrDefault();
                string issuedBy = string.Empty;
                string issuedOn = string.Empty;

                COM003 objCOM003 = new COM003();
                if (objlog != null && !string.IsNullOrWhiteSpace(objlog.IssuedBy))
                {
                    issuedBy = GetIssueByPSNOOrName(objlog.IssuedBy);
                }
                for (int i = 1; i <= (objCRS002.RevNo + 1); i++)
                {
                    //if (objlog != null)
                    //{
                    //    issuedBy = objlog.IssuedBy;
                    //}
                    tableHtml.Append("<th class=\"all scenter clsHeaderColor\">" + issuedBy + "</th>");
                }
                tableHtml.Append("</tr>");


                tableHtml.Append("<tr>");
                //tableHtml.Append("<th class=\"all clsHeaderColor\"></th>");
                tableHtml.Append("<th class=\"all scenter clsHeaderColor\">Issue Date</th>");
                for (int i = 1; i <= (objCRS002.RevNo + 1); i++)
                {
                    if (objlog != null)
                    {
                        issuedOn = (objlog.IssuedOn != null ? Convert.ToDateTime(objlog.IssuedOn).ToString("dd-MMM") : "");//string.Format(new MyCustomDateProvider(), "{0}", objlog.IssuedOn);//objlog.IssuedOn.ToString();
                    }
                    tableHtml.Append("<th class=\"all scenter clsHeaderColor\">" + issuedOn + "</th>");
                }
                tableHtml.Append("</tr>");
                tableHtml.Append("<tr>");
                //tableHtml.Append("<th class=\"all clsHeaderColor\"></th>");
                tableHtml.Append("<th class=\"all scenter clsHeaderColor\">Issue</th>");
                for (int i = 1; i <= (objCRS002.RevNo + 1); i++)
                {
                    tableHtml.Append("<th class=\"all scenter clsHeaderColor\">R" + (i - 1) + "</th>");
                }

                tableHtml.Append("</tr>");
                tableHtml.Append("<tr>");
                tableHtml.Append("<th class=\"all clsHeaderColor\">#</th>");
                tableHtml.Append("<th class=\"all clsHeaderColor\">Description</th>");

                for (int i = 1; i <= (objCRS002.RevNo + 1); i++)
                {
                    tableHtml.Append("<th class=\"all scenter clsHeaderColor\"></th>");
                }
                tableHtml.Append("</tr>");
                tableHtml.Append("</thead>");

                tableHtml.Append("<tbody>");

                int count = 1;

                foreach (var objArm in ArmList)
                {
                    CRS001_Log objCRS001_Log = objArm.OrderByDescending(x => x.RevNo).FirstOrDefault();

                    tableHtml.Append("<tr>");
                    tableHtml.Append("<td class=\"clsBodyColor\">" + count + "</td>");
                    tableHtml.Append("<td class=\"clsBodyColor\">" + objCRS001_Log.ARMSet + "</td>");
                    if (objCRS001_Log != null)
                        tableHtml.Append("<td class=\"scenter clsHeaderColor\"><a href=\"" + WebsiteURL + "/CRS/MaintainARM/ARMRevDetailsView?Id=" + objCRS001_Log.Id + "&CINId=" + objCRS002.CINId + "\" target=\"_blank\">" + objCRS001_Log.ARMRevision + "</a></td>");
                    else
                        tableHtml.Append("<td class=\"scenter\">< i style=\"font-weight: 100; font-size: 10px;\" class=\"fa fa-asterisk\" aria-hidden=\"true\"></i></td>");

                    tableHtml.Append("</tr>");
                    count++;

                }
                tableHtml.Append("</tbody>");
                tableHtml.Append("<tfoot>");


                tableHtml.Append("</tfoot>");
            }
            else if (objCRS002.RevNo > 0)
            {
                List<CRS003_Log> lstCRS003_Log = db.CRS003_Log.Where(x => x.CINId == objCRS002.CINId).ToList();

                tableHtml.Append("<thead>");

                var objlog = objCRS002_Log.Where(x => x.RevNo == objCRS002.RevNo).FirstOrDefault();
                string issuedBy = string.Empty;
                string issuedOn = string.Empty;
                COM003 objCOM003 = new COM003();
                //if (objlog != null && !string.IsNullOrWhiteSpace(objlog.IssuedBy))
                //{
                //    objCOM003 = db.COM003.Where(x => x.t_psno == objlog.IssuedBy && x.t_actv == 1).FirstOrDefault();
                //    if (objCOM003 != null)
                //    {
                //        if (!string.IsNullOrWhiteSpace(objCOM003.t_init))
                //        {
                //            issuedBy = objCOM003.t_init;
                //        }
                //        else if (!string.IsNullOrWhiteSpace(objCOM003.t_name))
                //        {
                //            var names = objCOM003.t_name.Split(new char[] { ' ' });
                //            for (int i = 0; i < names.Length; i++)
                //            {
                //                issuedBy += names[i].Substring(0, 1);
                //            }
                //        }
                //        else
                //        {
                //            issuedBy = objCOM003.t_psno;
                //        }
                //    }
                //    else
                //    {
                //        issuedBy = objlog.IssuedBy;
                //    }
                //}

                tableHtml.Append("<tr>");
                tableHtml.Append("<th rowspan=\"2\" class=\"all clsHeaderColor\"></th>");
                tableHtml.Append("<th class=\"all scenter clsHeaderColor\">Project</th>");
                tableHtml.Append("<th colspan=\"" + (objCRS002.RevNo + 1) + "\" id=\"txttblProject\" class=\"all scenter clsHeaderColor\"></th>");
                tableHtml.Append("</tr>");

                tableHtml.Append("<tr>");
                tableHtml.Append("<th class=\"all scenter clsHeaderColor\">Customer</th>");
                tableHtml.Append("<th colspan=\"" + (objCRS002.RevNo + 1) + "\" id=\"txttblCustomer\" class=\"all scenter clsHeaderColor\"></th>");
                tableHtml.Append("</tr>");

                tableHtml.Append("<tr>");
                tableHtml.Append("<th rowspan=\"3\" class=\"all clsHeaderColor\"><img style=\"height:90px;\" src=\"" + WebsiteURL + "/Images/Logo.jpg\" alt=\"L & T Logo\"></th>");
                tableHtml.Append("<th class=\"all scenter clsHeaderColor\">Issued By</th>");
                for (int i = 1; i <= (objCRS002.RevNo + 1); i++)
                {
                    issuedBy = string.Empty;
                    var objIssuedBy = db.CRS002_Log.Where(x => x.CINId == objCRS002.CINId && x.RevNo == (i - 1)).FirstOrDefault();
                    if (objIssuedBy != null)
                    {
                        issuedBy = GetIssueByPSNOOrName(objIssuedBy.IssuedBy);
                    }
                    //if (objlog != null)
                    //{
                    //    issuedBy = objlog.IssuedBy.ToString();
                    //}
                    tableHtml.Append("<th class=\"all scenter clsHeaderColor\">" + issuedBy + "</th>");
                }
                tableHtml.Append("</tr>");



                tableHtml.Append("<tr>");
                //tableHtml.Append("<th class=\"all clsHeaderColor\"></th>");
                tableHtml.Append("<th class=\"all scenter clsHeaderColor\">Issue Date</th>");
                for (int i = 1; i <= (objCRS002.RevNo + 1); i++)
                {
                    //if (objlog != null)
                    //{
                    //    issuedOn = (objlog.IssuedOn != null ? Convert.ToDateTime(objlog.IssuedOn).ToString("dd-MMM") : "");
                    //}
                    issuedOn = string.Empty;
                    var objIssuedOn = db.CRS002_Log.Where(x => x.CINId == objCRS002.CINId && x.RevNo == (i - 1)).FirstOrDefault();
                    if (objIssuedOn != null)
                    {
                        issuedOn = objIssuedOn.IssuedOn.HasValue? objIssuedOn.IssuedOn.Value.ToString("dd-MMM") :"";
                    }
                    tableHtml.Append("<th class=\"all scenter clsHeaderColor\">" + issuedOn + "</th>");
                }
                tableHtml.Append("</tr>");
                tableHtml.Append("<tr>");
                //tableHtml.Append("<th class=\"all clsHeaderColor\"></th>");
                tableHtml.Append("<th class=\"all scenter clsHeaderColor\">Issue</th>");
                for (int i = 1; i <= (objCRS002.RevNo + 1); i++)
                {
                    tableHtml.Append("<th class=\"all scenter clsHeaderColor\">R" + (i - 1) + "</th>");
                }
                tableHtml.Append("</tr>");
                tableHtml.Append("<tr>");
                tableHtml.Append("<th class=\"all clsHeaderColor\">#</th>");
                tableHtml.Append("<th class=\"all clsHeaderColor\">Description</th>");
                for (int i = 1; i <= (objCRS002.RevNo + 1); i++)
                {
                    tableHtml.Append("<th class=\"all scenter clsHeaderColor\"></th>");
                }
                tableHtml.Append("</tr>");
                tableHtml.Append("</thead>");
                tableHtml.Append("<tbody>");
                int count = 1;
                foreach (var objCRS001_Log in ArmList)
                {

                    int armId = objCRS001_Log.FirstOrDefault().HeaderId;
                    string strPreRev = string.Empty;
                    tableHtml.Append("<tr>");
                    tableHtml.Append("<td class=\"clsBodyColor\">" + count + "</td>");
                    tableHtml.Append("<td class=\"clsBodyColor\">" + Convert.ToString(objCRS001_Log.FirstOrDefault().ARMSet) + "</td>");
                    for (int i = 0; i <= objCRS002.RevNo; i++)
                    {
                        CRS003_Log objCRS003_Log = lstCRS003_Log.Where(x => x.ARMHeaderId == armId && x.CINRevNo == i).FirstOrDefault();
                        if (objCRS003_Log != null)
                        {
                            if (strPreRev == Convert.ToString(objCRS003_Log.ARMStrRev))
                            {
                                tableHtml.Append("<td class=\"scenter\"><i style=\"font-weight: 100; font-size: 10px;\" class=\"fa fa-arrow-right\" aria-hidden=\"true\"></i></td>");
                            }
                            else
                            {
                                tableHtml.Append("<td class=\"scenter clsHeaderColor\"><a href=\"" + WebsiteURL + "/CRS/MaintainARM/ARMRevDetailsView?Id=" + objCRS003_Log.RefId + "&CINId=" + objCRS002.CINId + "\" target=\"_blank\">" + objCRS003_Log.ARMStrRev + "</a></td>");
                                strPreRev = Convert.ToString(objCRS003_Log.ARMStrRev);
                            }
                        }
                        else
                        {
                            string MaxRevNo = string.Empty;
                            if (i == objCRS002.RevNo)
                            {
                                var DataCRS001_Log = (objCRS001_Log.OrderByDescending(x => x.RevNo).FirstOrDefault() != null ? objCRS001_Log.OrderByDescending(x => x.RevNo).FirstOrDefault() : null);
                                MaxRevNo = (objCRS001_Log.OrderByDescending(x => x.RevNo).FirstOrDefault() != null ? Convert.ToString(objCRS001_Log.OrderByDescending(x => x.RevNo).Select(x => x.ARMRevision).FirstOrDefault()) : "");
                                if (strPreRev == MaxRevNo)
                                {
                                    MaxRevNo = "<i style=\"font-weight: 100; font-size: 10px;\" class=\"fa fa-arrow-right\" aria-hidden=\"true\"></i>";
                                }
                                else
                                {
                                    MaxRevNo = "<a href=\"" + WebsiteURL + "/CRS/MaintainARM/ARMRevDetailsView?Id=" + DataCRS001_Log.Id + "&CINId=" + objCRS002.CINId + "\" target=\"_blank\">" + DataCRS001_Log.ARMRevision + "</a>";
                                }

                            }
                            else
                            {
                                MaxRevNo = "<i style=\"font-weight: 100; font-size: 10px;\" class=\"fa fa-asterisk\" aria-hidden=\"true\"></i>";
                            }
                            tableHtml.Append("<td class=\"scenter\">" + MaxRevNo + "</td>");
                        }
                    }
                    tableHtml.Append("</tr>");
                    count++;
                }
                tableHtml.Append("</tbody>");
                tableHtml.Append("<tfoot>");



                tableHtml.Append("</tfoot>");
            }
            tableHtml.Append("</table>");
            string str = tableHtml.ToString();
            return str;
        }

        public string GetIssueByPSNOOrName(string PSNO)
        {
            string issuedBy = string.Empty;
            try
            {
                var objCOM003 = db.COM003.Where(x => x.t_psno == PSNO && x.t_actv == 1).FirstOrDefault();

                if (objCOM003 != null)
                {
                    if (!string.IsNullOrWhiteSpace(objCOM003.t_init))
                    {
                        issuedBy = objCOM003.t_init;
                    }
                    else if (!string.IsNullOrWhiteSpace(objCOM003.t_name))
                    {
                        var names = objCOM003.t_name.Split(new char[] { ' ' });
                        for (int i = 0; i < names.Length; i++)
                        {
                            issuedBy += names[i].Substring(0, 1);
                        }
                    }
                    else
                    {
                        issuedBy = objCOM003.t_psno;
                    }
                }
                else
                {
                    issuedBy = PSNO;
                }
            }
            catch (Exception)
            {

                throw;
            }
            return issuedBy;
        }
        #endregion
        // GET: CRS/MaintainSinReport
        #region Maintain
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetCINGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetCINGridDataPartial");
        }

        public ActionResult loadCINDataTable(JQueryDataTableParamModel param, string status)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);


                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";

                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and status in ('" + clsImplementationEnum.PAMStatus.Draft.GetStringValue() + "')";
                }
                else
                {
                    whereCondition += " and status in ('" + clsImplementationEnum.PAMStatus.Issued.GetStringValue() + "','" + clsImplementationEnum.PAMStatus.Draft.GetStringValue() + "')";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = { "Project", "ContractNo", "Customer", "DocumentNo", "Status", "RevNo" };

                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstPam = db.SP_CIN_GETCINDETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from x in lstPam
                           select new[] {
                               Convert.ToString(x.ContractNo),
                               Convert.ToString(x.Project),
                               Convert.ToString(x.Customer),
                               Convert.ToString(x.DocumentNo),
                               Convert.ToString("R"+x.RevNo),
                               Convert.ToString(x.Status),
                               "<center style=\"display:inline-flex;\"><a href='"+WebsiteURL+"/CRS/MaintainCinReport/CINDetails?CINId="+Convert.ToInt32(x.CINId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a><a class='btn btn-xs' href='javascript:void(0)' onclick=ShowTimeline('/CRS/MaintainCinReport/ShowTimeline?HeaderID=" + x.CINId + "');><i class='fa fa-clock-o'></i></a></center>",
                               Convert.ToString(x.CINId),
                    }).ToList();
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult CINDetails(int CINId = 0)
        {
            CRS002 objCRS002 = null;
            if (CINId > 0)
            {
                objCRS002 = db.CRS002.Where(x => x.CINId == CINId).FirstOrDefault();
                string buCode = db.COM001.Where(i => i.t_cprj == objCRS002.Project).FirstOrDefault().t_entu;
                var lstInspectionAgency = Manager.GetSubCatagories("Inspection Agency", buCode, objClsLoginInfo.Location).Select(i => new BULocWiseCategoryModel { CatID = i.Code, CatDesc = i.Code + " - " + i.Description }).ToList();
                var AgencyList = lstInspectionAgency.Where(x => x.CatID == objCRS002.InspectionAgency).FirstOrDefault();
                ViewBag.lstAgency = lstInspectionAgency;
                if (AgencyList != null)
                {
                    objCRS002.InspectionAgency = AgencyList.CatID;
                    ViewBag.txtInspection = AgencyList.CatDesc;
                }
                ViewBag.Customer = Manager.GetCustomerCodeAndNameByContractNo(objCRS002.ContractNo);
                objCRS002 = db.CRS002.Where(x => x.CINId == CINId).FirstOrDefault();
                ViewBag.ContractNo = Manager.GetContractorAndDescription(objCRS002.ContractNo);
                ViewBag.Project = Manager.GetProjectAndDescription(objCRS002.Project);
                if (!string.IsNullOrWhiteSpace(objCRS002.IssuedBy))
                {
                    objCRS002.IssuedBy = Manager.GetPsidandDescription(objCRS002.IssuedBy);
                }
            }
            else
            {
                objCRS002 = new CRS002();
                objCRS002.Status = clsImplementationEnum.PAMStatus.Draft.GetStringValue();
                objCRS002.RevNo = 0;

            }
            string user = objClsLoginInfo.UserName;

            List<string> lstBUs = (from a in db.ATH001
                                   where a.Employee == user
                                   select a.BU).Distinct().ToList();
            if (lstBUs.Count > 0)
            {
                ViewBag.strBUs = string.Join(",", lstBUs);
            }
            else
            {
                ViewBag.strBUs = "";
            }

            return View(objCRS002);
        }

        #region Department
        public ActionResult GetDepartments(int CINId = 0, string Status = "")
        {
            ViewBag.CINId = CINId;
            ViewBag.Status = Status;
            CRS002 objCRS002 = new CRS002();
            if (CINId > 0)
            {
                objCRS002 = db.CRS002.Where(x => x.CINId == CINId).FirstOrDefault();
            }
            return PartialView("_GetCINDepartment", objCRS002);
        }

        public ActionResult GetDepartmentList(string param)
        {
            List<ddlValue> lstDepartment = new List<ddlValue>();
            lstDepartment = db.COM002.Where(x => x.t_dtyp == 3).Select(x => new ddlValue { text = (x.t_dimx + "-" + x.t_desc), id = x.t_dimx }).Distinct().ToList();
            return Json(lstDepartment, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetRepresantativeList(string param)
        {
            var arr = param.Split(',');
            var employeslist = db.COM003.Where(x => arr.Contains(x.t_depc) && x.t_actv == 1).ToList();

            var lstEmployee = employeslist.Select(x => new ddlValue { text = (x.t_psno + "-" + x.t_name), id = x.t_psno }).ToList();
            return Json(lstEmployee, JsonRequestBehavior.AllowGet);
        }

        public void loadARMList()
        {
            var objCRS002 = db.CRS002.Where(x => x.ContractNo == "C01150001" && x.Project == "0017001").FirstOrDefault();

            var ArmList = db.CRS001_Log.Where(x => x.ContractNo == "C01150001" && x.Project == "0017001").GroupBy(x => x.ARMSet).ToList();
            if (objCRS002.Status.ToLower() == "draft" && objCRS002.RevNo == 0)
            {

            }
            //if (ArmList.Count > 0)
            //{
            //    //calculate rows
            //    var rows = ArmList.GroupBy

            //    var column = objCRS002.RevNo;
            //    foreach (var item in rows)
            //    {
            //        for (int i = 0; i <= column; i++)
            //        {
            //            if (i == column)
            //            {
            //                var latestRev = db.CRS001_Log.Where(x => x.ContractNo == "C01150001" && x.Project == "0017001").OrderByDescending(x => x.RevNo).FirstOrDefault().RevNo;
            //            }
            //            else
            //            {
            //                var latestRev = db.CRS003_Log.Where(x => x.CINId == objCRS002.CINId && x.CINRevNo == i && x.AMRHeaderId == item.HeaderId).FirstOrDefault();
            //                if (latestRev != null)
            //                {

            //                }
            //            }
            //        }
            //    }


            //}

            // var maxRev = objCRS001_Log.Max(x => x.RevNo);

            //return null;
        }
        #endregion

        public ActionResult SaveHeader(FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int CINId = Convert.ToInt32(fc["CINId"]);
                CRS002 objCRS002 = null;

                if (CINId > 0)
                {
                    objCRS002 = db.CRS002.Where(x => x.CINId == CINId).FirstOrDefault();

                    objCRS002.Project = fc["Project"];
                    objCRS002.Status = clsImplementationEnum.PAMStatus.Draft.GetStringValue();
                    objCRS002.ContractNo = fc["ContractNo"];
                    objCRS002.Customer = fc["Customer"];
                    objCRS002.DocumentNo = fc["DocumentNo"];
                    objCRS002.RevNo = 0;
                    objCRS002.PCCDept = fc["ddlPCCDept"];
                    objCRS002.PCCRep = fc["ddlPCCPerson"];
                    objCRS002.MCCDept = fc["ddlMCCDept"];
                    objCRS002.MCCRep = fc["ddlMCCPerson"];
                    objCRS002.WEDept = fc["ddlWEDept"];
                    objCRS002.WERep = fc["ddlWEPerson"];
                    objCRS002.PMGDept = fc["ddlPMGDept"];
                    objCRS002.PMGRep = fc["ddlPMGPerson"];
                    objCRS002.QCDept = fc["ddlQCDept"];
                    objCRS002.QCRep = fc["ddlQCPerson"];
                    objCRS002.ASME = Convert.ToBoolean(fc["ASME"]);
                    objCRS002.InspectionAgency = fc["InspectionAgency"];
                    objCRS002.EditedBy = objClsLoginInfo.UserName;
                    objCRS002.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                }
                else
                {
                    objCRS002 = new CRS002();

                    objCRS002.Project = fc["Project"];
                    objCRS002.Status = clsImplementationEnum.PAMStatus.Draft.GetStringValue();
                    objCRS002.ContractNo = fc["ContractNo"];
                    objCRS002.Customer = fc["Customer"];
                    objCRS002.DocumentNo = fc["DocumentNo"];
                    objCRS002.RevNo = 0;
                    objCRS002.PCCDept = fc["ddlPCCDept"];
                    objCRS002.PCCRep = fc["ddlPCCPerson"];
                    objCRS002.MCCDept = fc["ddlMCCDept"];
                    objCRS002.MCCRep = fc["ddlMCCPerson"];
                    objCRS002.WEDept = fc["ddlWEDept"];
                    objCRS002.WERep = fc["ddlWEPerson"];
                    objCRS002.PMGDept = fc["ddlPMGDept"];
                    objCRS002.PMGRep = fc["ddlPMGPerson"];
                    objCRS002.QCDept = fc["ddlQCDept"];
                    objCRS002.QCRep = fc["ddlQCPerson"];
                    objCRS002.ASME = Convert.ToBoolean(fc["ASME"]);
                    objCRS002.InspectionAgency = fc["InspectionAgency"];
                    objCRS002.CreatedBy = objClsLoginInfo.UserName;
                    objCRS002.CreatedOn = DateTime.Now;

                    db.CRS002.Add(objCRS002);
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ContractIssued(int CINId = 0)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (CINId > 0)
                {
                    CRS002 objCRS002 = db.CRS002.Where(x => x.CINId == CINId).FirstOrDefault();

                    CRS003_Log objCRS003_Log = new CRS003_Log();
                    // CRS001_Log objCRS001_Log = db.CRS001_Log.Where(x => x.ContractNo == objCRS002.ContractNo && x.Project == objCRS002.Project).OrderByDescending(x => x.RevNo).FirstOrDefault();
                    var ArmList = db.CRS001_Log.Where(x => x.ContractNo == objCRS002.ContractNo && x.Project == objCRS002.Project)
                         .GroupBy(u => u.ARMSet)
                         .Select(grp => grp.ToList())
                          .ToList();
                    if (ArmList != null && ArmList.Count > 0)
                    {
                        List<CRS003_Log> lstCRS003_Log = new List<CRS003_Log>();
                        foreach (var objArm in ArmList)
                        {
                            objCRS003_Log = new CRS003_Log();
                            CRS001_Log objCRS001_Log = objArm.OrderByDescending(x => x.RevNo).FirstOrDefault();
                            objCRS003_Log.RefId = objCRS001_Log.Id;
                            objCRS003_Log.LineId = 1;
                            objCRS003_Log.CINId = CINId;
                            objCRS003_Log.CINRevNo = objCRS002.RevNo;
                            objCRS003_Log.ARMHeaderId = objCRS001_Log.HeaderId;
                            objCRS003_Log.ARMStrRev = objCRS001_Log.ARMRevision;
                            objCRS003_Log.ARMDesc = objCRS001_Log.ARMDesc;
                            objCRS003_Log.ARMRev = objCRS001_Log.RevNo;
                            objCRS003_Log.CreatedBy = objClsLoginInfo.UserName;
                            objCRS003_Log.CreatedOn = DateTime.Now;
                            lstCRS003_Log.Add(objCRS003_Log);
                        }
                        if (lstCRS003_Log != null && lstCRS003_Log.Count > 0)
                        {
                            db.CRS003_Log.AddRange(lstCRS003_Log);
                        }
                        objCRS002.Status = clsImplementationEnum.PAMStatus.Issued.GetStringValue();
                        objCRS002.IssuedBy = objClsLoginInfo.UserName;
                        objCRS002.IssuedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Status = clsImplementationEnum.PAMStatus.Issued.GetStringValue();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                    }
                    else
                    {
                        objCRS002.Status = clsImplementationEnum.PAMStatus.Issued.GetStringValue();
                        objCRS002.IssuedBy = objClsLoginInfo.UserName;
                        objCRS002.IssuedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Status = clsImplementationEnum.PAMStatus.Issued.GetStringValue();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                    }

                    CRS002_Log objCRS002_Log = new CRS002_Log
                    {
                        CINId = CINId,
                        Project = objCRS002.Project,
                        Status = objCRS002.Status,
                        ContractNo = objCRS002.ContractNo,
                        Customer = objCRS002.Customer,
                        DocumentNo = objCRS002.DocumentNo,
                        RevNo = objCRS002.RevNo,
                        PCCDept = objCRS002.PCCDept,
                        PCCRep = objCRS002.PCCRep,
                        MCCDept = objCRS002.MCCDept,
                        MCCRep = objCRS002.MCCRep,
                        WEDept = objCRS002.WEDept,
                        WERep = objCRS002.WERep,
                        PMGDept = objCRS002.PMGDept,
                        PMGRep = objCRS002.PMGRep,
                        QCDept = objCRS002.QCDept,
                        QCRep = objCRS002.QCRep,
                        ASME = objCRS002.ASME,
                        InspectionAgency = objCRS002.InspectionAgency,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now,
                        IssuedBy = objClsLoginInfo.UserName,
                        IssuedOn = DateTime.Now
                    };
                    db.CRS002_Log.Add(objCRS002_Log);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ContractRevised(int CINId = 0)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (CINId > 0)
                {
                    CRS002 objCRS002 = db.CRS002.Where(x => x.CINId == CINId).FirstOrDefault();
                    objCRS002.RevNo += 1;
                    objCRS002.Status = clsImplementationEnum.PAMStatus.Draft.GetStringValue();
                    objCRS002.EditedBy = null;
                    objCRS002.EditedOn = null;
                    objCRS002.IssuedBy = null;
                    objCRS002.IssuedOn = null;
                    db.SaveChanges();


                    objResponseMsg.Key = true;
                    objResponseMsg.Status = clsImplementationEnum.PAMStatus.Draft.GetStringValue();
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revision.ToString();
                    objResponseMsg.RevNo = "R" + objCRS002.RevNo;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Issued
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult IssuerIndex()
        {
            return View();
        }

        public ActionResult GetIssuedCINGridDataPartial(string status)
        {
            ViewBag.Status = status;
            string username = objClsLoginInfo.UserName;
            var contractStatus = clsImplementationEnum.PAMStatus.Issued.GetStringValue().ToLower();
            List<CRS002> lstObjCRS002 = db.CRS002.Where(x => x.Status.ToLower() == contractStatus).ToList();
            string CINIds = string.Empty;
            string listofReceiveBy = string.Empty;
            if (lstObjCRS002.Count > 0)
            {
                foreach (var item in lstObjCRS002)
                {
                    listofReceiveBy += lstObjCRS002.Where(x => x.CINId == item.CINId).Select(x => x.PCCRep).FirstOrDefault();
                    listofReceiveBy += "," + lstObjCRS002.Where(x => x.CINId == item.CINId).Select(x => x.MCCRep).FirstOrDefault();
                    listofReceiveBy += "," + lstObjCRS002.Where(x => x.CINId == item.CINId).Select(x => x.WERep).FirstOrDefault();
                    listofReceiveBy += "," + lstObjCRS002.Where(x => x.CINId == item.CINId).Select(x => x.PMGRep).FirstOrDefault();
                    listofReceiveBy += "," + lstObjCRS002.Where(x => x.CINId == item.CINId).Select(x => x.QCRep).FirstOrDefault();

                    var result = listofReceiveBy.Split(',').Contains(username);
                    if (result)
                    {
                        CINIds += item.CINId + ",";
                    }
                    listofReceiveBy = string.Empty;
                }
            }
            ViewBag.CINIds = CINIds;
            return PartialView("_GetIssuedCINGridDataPartial");
        }

        public ActionResult loadIssuerCINDataTable(JQueryDataTableParamModel param, string status, string CINIds)
        {
            try
            {

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);


                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";

                if (!string.IsNullOrEmpty(CINIds))
                {
                    CINIds = CINIds.Substring(0, CINIds.Length - 1);
                    whereCondition += " and CINId in(" + CINIds + ")";
                }
                else
                {
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = 0,
                        iTotalRecords = 0,
                        aaData = new string[0],
                        whereCondition = "",
                        strSortOrder = "",
                    }, JsonRequestBehavior.AllowGet);
                }

                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and status in ('" + clsImplementationEnum.PAMStatus.Issued.GetStringValue() + "')";
                }
                //else
                //{
                //    whereCondition += " and status in ('" + clsImplementationEnum.PAMStatus.Issued.GetStringValue() + "','" + clsImplementationEnum.PAMStatus.Draft.GetStringValue() + "')";
                //}

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = { "Project", "ContractNo", "Customer", "DocumentNo", "Status", "RevNo" };

                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }


                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstPam = db.SP_CIN_GETCINDETAILS(StartIndex, EndIndex, "", whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from x in lstPam
                           select new[] {
                               Convert.ToString(x.ContractNo),
                               Convert.ToString(x.Project),
                               Convert.ToString(x.Customer),
                               Convert.ToString(x.DocumentNo),
                               Convert.ToString("R"+x.RevNo),
                               Convert.ToString(x.Status),
                               "<center style=\"display:inline-flex;\"><a href='"+WebsiteURL+"/CRS/MaintainCinReport/IssuerCINDetails?CINId="+Convert.ToInt32(x.CINId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a><a class='btn btn-xs' href='javascript:void(0)' onclick=ShowTimeline('/CRS/MaintainCinReport/ShowTimeline?HeaderID=" + x.CINId + "');><i class='fa fa-clock-o'></i></a></center>",
                               Convert.ToString(x.CINId),
                    }).ToList();
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult IssuerCINDetails(int CINId = 0)
        {
            CRS002 objCRS002 = null;
            if (CINId > 0)
            {
                objCRS002 = db.CRS002.Where(x => x.CINId == CINId).FirstOrDefault();
                string buCode = db.COM001.Where(i => i.t_cprj == objCRS002.Project).FirstOrDefault().t_entu;
                var lstInspectionAgency = Manager.GetSubCatagories("Inspection Agency", buCode, objClsLoginInfo.Location).Select(i => new BULocWiseCategoryModel { CatID = i.Code, CatDesc = i.Code + " - " + i.Description }).ToList();
                var AgencyList = lstInspectionAgency.Where(x => x.CatID == objCRS002.InspectionAgency).FirstOrDefault();

                if (AgencyList != null)
                {
                    objCRS002.InspectionAgency = AgencyList.CatID;
                    ViewBag.txtInspection = AgencyList.CatID + "-" + AgencyList.CatDesc;
                    ViewBag.lstAgency = lstInspectionAgency;
                }
                ViewBag.Customer = Manager.GetCustomerCodeAndNameByContractNo(objCRS002.ContractNo);
                objCRS002 = db.CRS002.Where(x => x.CINId == CINId).FirstOrDefault();
                ViewBag.ContractNo = Manager.GetContractorAndDescription(objCRS002.ContractNo);
                ViewBag.Project = Manager.GetProjectAndDescription(objCRS002.Project);
                if (!string.IsNullOrWhiteSpace(objCRS002.IssuedBy))
                {
                    objCRS002.IssuedBy = Manager.GetPsidandDescription(objCRS002.IssuedBy);
                }

            }
            else
            {
                objCRS002 = new CRS002();
                objCRS002.Status = clsImplementationEnum.PAMStatus.Draft.GetStringValue();
                objCRS002.RevNo = 0;

            }
            string user = objClsLoginInfo.UserName;

            List<string> lstBUs = (from a in db.ATH001
                                   where a.Employee == user
                                   select a.BU).Distinct().ToList();
            if (lstBUs.Count > 0)
            {
                ViewBag.strBUs = string.Join(",", lstBUs);
            }
            else
            {
                ViewBag.strBUs = "";
            }

            return View(objCRS002);
        }

        #endregion

        public ActionResult ShowTimeline(int HeaderId)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.Title = "CIN";

            CRS002 objCRS002 = db.CRS002.Where(x => x.CINId == HeaderId).FirstOrDefault();
            model.ApprovedBy = Manager.GetUserNameFromPsNo(objCRS002.IssuedBy);
            model.ApprovedOn = objCRS002.IssuedOn;

            model.CreatedBy = Manager.GetUserNameFromPsNo(objCRS002.CreatedBy);
            model.CreatedOn = objCRS002.CreatedOn;

            return PartialView("~/Views/Shared/_DisplayCRSTimeLinePartial.cshtml", model);
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
            {
                List<clsExportCIN> lstclsExportCIN = new List<clsExportCIN>();
                var lst = db.SP_CIN_GETCINDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                if (lst != null && lst.Count > 0)
                {
                    lstclsExportCIN.AddRange(lst.Select(x => new clsExportCIN
                    {
                        ROW_NO = x.ROW_NO,
                        ContractNo = x.ContractNo,
                        Project = x.Project,
                        Customer = x.Customer,
                        DocumentNo = x.DocumentNo,
                        RevNo = x.RevNo,
                        Status = x.Status,
                        CreatedBy = x.CreatedBy,
                        EditedBy = x.EditedBy,
                        IssuedBy = x.IssuedBy
                    }));
                    string str = Helper.GenerateExcel(lstclsExportCIN, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = str;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Data not available.";
                }
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Data not available.";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public class MyCustomDateProvider : IFormatProvider, ICustomFormatter
        {
            public object GetFormat(Type formatType)
            {
                if (formatType == typeof(ICustomFormatter))
                    return this;

                return null;
            }

            public string Format(string format, object arg, IFormatProvider formatProvider)
            {
                if (!(arg is DateTime)) throw new NotSupportedException();

                var dt = (DateTime)arg;

                string suffix;

                if (new[] { 11, 12, 13 }.Contains(dt.Day))
                {
                    suffix = "th";
                }
                else if (dt.Day % 10 == 1)
                {
                    suffix = "st";
                }
                else if (dt.Day % 10 == 2)
                {
                    suffix = "nd";
                }
                else if (dt.Day % 10 == 3)
                {
                    suffix = "rd";
                }
                else
                {
                    suffix = "th";
                }

                return string.Format("{0:MMM} {1}{2}, {0:yyyy}", arg, dt.Day, suffix);
            }
        }

        public partial class clsExportCIN
        {
            public Nullable<long> ROW_NO { get; set; }
            public string ContractNo { get; set; }
            public string Project { get; set; }
            public string Customer { get; set; }
            public string DocumentNo { get; set; }
            public Nullable<int> RevNo { get; set; }
            public string Status { get; set; }
            public string CreatedBy { get; set; }
            public string EditedBy { get; set; }
            public string IssuedBy { get; set; }
        }
    }

}