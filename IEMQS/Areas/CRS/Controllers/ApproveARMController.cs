﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.CRS.Controllers
{
    public class ApproveARMController : clsBase
    {
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadARMDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_LoadARMDataPartial");
        }

        [HttpPost]
        public JsonResult LoadARMDataResult(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.ARMStatus.SEND_FOR_APPROVER.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "1=1";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "crs001.Project+' - '+com1.t_dsca",
                                                "Status",
                                                "crs001.Status",
                                                "(select cm4.t_cono+'-'+cm4.t_desc from COM004 cm4 where cm4.t_cono=crs001.ContractNo)",
                                               "(select cm6.t_bpid+'-'+cm6.t_nama from COM006 cm6 where cm6.t_bpid = crs001.Customer)",
                                                //"crs001.RevNo",
                                                "crs001.ARMSet",
                                                "crs001.ARMType",
                                                "crs001.ARMDesc",
                                                "crs001.ARMRevision",
                                                "crs001.ReasonOfRevsion",
                                                "crs001.SubmittedBy+' - '+com003s.t_name as SubmittedBy",
                                                "crs001.ApprovedBy+' - '+com003a.t_name ApprovedBy",
                                                "crs001.CreatedBy +' - '+com003c.t_name as CreatedBy",
                                                "crs001.ReviewedBy +' - '+com003r.t_name as ReviewedBy",
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_FETCH_ARM_HEADERS_RESULT
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.ContractNo),
                               Convert.ToString(uc.Customer),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.Project),
                               //Convert.ToString(uc.RevNo),
                               Convert.ToString(uc.ARMSet),
                               Convert.ToString(uc.ARMType),
                               Convert.ToString(uc.ARMDesc),
                               Convert.ToString(uc.ARMRevision),
                               Convert.ToString(uc.ReasonOfRevsion),
                               Convert.ToString(uc.CreatedBy),
                              Convert.ToString(uc.CreatedOn !=null?(Convert.ToDateTime(uc.CreatedOn)).ToString("dd/MM/yyyy"):""),
                               Convert.ToString(uc.SubmittedBy),
                               Convert.ToString(uc.SubmittedOn !=null?(Convert.ToDateTime(uc.SubmittedOn)).ToString("dd/MM/yyyy"):""),//Convert.ToString(uc.SubmittedOn),
                               Convert.ToString(uc.ReviewedBy),
                               Convert.ToString(uc.ReviewedOn !=null?(Convert.ToDateTime(uc.ReviewedOn)).ToString("dd/MM/yyyy"):""),//Convert.ToString(uc.ReviewedOn),
                               Convert.ToString(uc.ApprovedBy),
                               Convert.ToString(uc.ApprovedOn !=null?(Convert.ToDateTime(uc.ApprovedOn)).ToString("dd/MM/yyyy"):""),//Convert.ToString(uc.ApprovedOn),
                               "<center style=\"display:inline-flex;\"><a style=\"View Details\" class='' href='"+WebsiteURL+"/CRS/ApproveARM/ARMDetailsView?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a><a class='btn btn-xs' href='javascript:void(0)' onclick=ShowTimeline('/CRS/MaintainARM/ShowTimeline?HeaderID=" + uc.HeaderId + "');><i class='fa fa-clock-o'></i></a></center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhere,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = 0,
                    iTotalDisplayRecords = 0,
                    aaData = new string[0]
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]
        public ActionResult ARMDetailsView(int HeaderID = 0)
        {
            ViewBag.HeaderID = HeaderID;
            return View();
        }

        [HttpPost]
        public ActionResult LoadARMDetails(int HeaderID)
        {
            List<string> lstARMType = clsImplementationEnum.getARMType().ToList();
            ViewBag.ARMType = lstARMType.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

            string user = objClsLoginInfo.UserName;

            List<string> lstBUs = (from a in db.ATH001
                                   where a.Employee == user
                                   select a.BU).Distinct().ToList();

            if (lstBUs.Count > 0)
            {
                ViewBag.strBUs = string.Join(",", lstBUs);
            }
            else
            {
                ViewBag.strBUs = "";
            }
            CRS001 objCRS001 = db.CRS001.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
            if (objCRS001 != null && objCRS001.HeaderId > 0)
            {
                ViewBag.Project = objCRS001.Project.Split(',').ToArray();
                var objCustomers = (from cm004 in db.COM004
                                   // join cm005 in db.COM005 on cm004.t_cono equals cm005.t_cono
                                    join cm006 in db.COM006 on cm004.t_ofbp equals cm006.t_bpid
                                    where cm004.t_cono.Trim() == objCRS001.ContractNo.Trim()
                                    select new ApproverModel { Code = cm004.t_ofbp, Name = cm004.t_ofbp + "-" + cm006.t_nama }).FirstOrDefault();

                var objContractNos = (from cm004 in db.COM004
                                      join cm008 in db.COM008 on cm004.t_cono equals cm008.t_cono
                                    //  join cm005 in db.COM005 on cm004.t_cono equals cm005.t_cono
                                      where cm004.t_cono == objCRS001.ContractNo
                                      orderby cm004.t_cono
                                      select new ApproverModel { Code = cm004.t_cono, Name = cm004.t_cono + "-" + cm004.t_desc }).FirstOrDefault();


                ViewBag.ContractDesc = objContractNos.Name;
                ViewBag.CustomersDesc = objCustomers.Name;
            }
            else
            {
                objCRS001 = new CRS001();
                objCRS001.Status = clsImplementationEnum.ARMStatus.DRAFT.GetStringValue();
                objCRS001.RevNo = 0;
            }
            return PartialView("_LoadARMDetails", objCRS001);
        }

        [HttpPost]
        public ActionResult ApproveARMDetails(int headerId)
        {
            ResponceMsgWithData objResponseMsg = new ResponceMsgWithData();
            CRS001 objCRS001 = db.CRS001.Where(x => x.HeaderId == headerId).FirstOrDefault();
            if (objCRS001 != null)
            {
                objCRS001.Status = clsImplementationEnum.ARMStatus.APPROVED.GetStringValue();
                objCRS001.ApprovedBy = objClsLoginInfo.UserName;
                objCRS001.ApprovedOn = DateTime.Now;
                db.SaveChanges();
                List<CRS001_Log> lstCRS001_Log = new List<CRS001_Log>();
                string[] arrayProject = objCRS001.Project.Split(',').ToArray();
                for (int i = 0; i < arrayProject.Length; i++)
                {
                    CRS001_Log objCRS001_Log = new CRS001_Log();
                    objCRS001_Log.HeaderId = objCRS001.HeaderId;
                    objCRS001_Log.Project = Convert.ToString(arrayProject[i]);
                    objCRS001_Log.Status = objCRS001.Status;
                    objCRS001_Log.ContractNo = objCRS001.ContractNo;
                    objCRS001_Log.Customer = objCRS001.Customer;
                    objCRS001_Log.RevNo = objCRS001.RevNo;
                    objCRS001_Log.ARMSet = objCRS001.ARMSet;
                    objCRS001_Log.ARMType = objCRS001.ARMType;
                    objCRS001_Log.ARMDesc = objCRS001.ARMDesc;
                    objCRS001_Log.ARMRevision = objCRS001.ARMRevision;
                    objCRS001_Log.ReasonOfRevsion = objCRS001.ReasonOfRevsion;
                    objCRS001_Log.CreatedBy = objClsLoginInfo.UserName;
                    objCRS001_Log.CreatedOn = DateTime.Now;
                    objCRS001_Log.EditedBy = objCRS001.EditedBy;
                    objCRS001_Log.EditedOn = objCRS001.EditedOn;
                    objCRS001_Log.SubmittedBy = objCRS001.SubmittedBy;
                    objCRS001_Log.SubmittedOn = objCRS001.SubmittedOn;
                    objCRS001_Log.ReviewedBy = objCRS001.ReviewedBy;
                    objCRS001_Log.ReviewedOn = objCRS001.ReviewedOn;
                    objCRS001_Log.ApprovedBy = objCRS001.ApprovedBy;
                    objCRS001_Log.ApprovedOn = objCRS001.ApprovedOn;
                    objCRS001_Log.ReturnByApprover = objCRS001.ReturnByApprover;
                    lstCRS001_Log.Add(objCRS001_Log);
                }
                if(lstCRS001_Log.Count > 0)
                {
                    db.CRS001_Log.AddRange(lstCRS001_Log);
                    objCRS001.ReasonOfRevsion = null;
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "ARM Details successfully approved.";
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "ARM Details are not available.";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReturnARMDetails(int headerId,string returnRemarks)
        {
            ResponceMsgWithData objResponseMsg = new ResponceMsgWithData();
            CRS001 objCRS001 = db.CRS001.Where(x => x.HeaderId == headerId).FirstOrDefault();
            if (objCRS001 != null)
            {
                objCRS001.ReturnedOnApprover = DateTime.Now;
                objCRS001.ReturnApproverRemarks = returnRemarks;
                objCRS001.ReturnByApprover = objClsLoginInfo.UserName;
                objCRS001.Status = clsImplementationEnum.ARMStatus.RETURNED_BY_APPROVER.GetStringValue();
                objCRS001.EditedBy = objClsLoginInfo.UserName;
                objCRS001.EditedOn = DateTime.Now;
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = "ARM Details successfully returned.";
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "ARM Details are not available.";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
    }
}