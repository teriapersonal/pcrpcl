﻿using IEMQS.Areas.IPI.Controllers;
using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.CRS.Controllers
{
    public class MaintainARMController : clsBase
    {
        #region Utility
        [HttpPost]
        public bool CheckDuplicateARMSet(string armset)
        {
            bool result = db.CRS001.Where(x => x.ARMSet == armset).Any();
            return result;
        }
        #endregion
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadARMDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_LoadARMDataPartial");
        }

        [HttpPost]
        public JsonResult LoadARMDataResult(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.ARMStatus.DRAFT.GetStringValue() + "','" + clsImplementationEnum.ARMStatus.RETURNED_BY_REVIEWER.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "1=1";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "crs001.Project",
                                                "Status",
                                                "crs001.Status",
                                                "(select cm4.t_cono+'-'+cm4.t_desc from COM004 cm4 where cm4.t_cono=crs001.ContractNo)",
                                                "(select cm6.t_bpid+'-'+cm6.t_nama from COM006 cm6 where cm6.t_bpid = crs001.Customer)",
                                                //"crs001.RevNo",
                                                "crs001.ARMSet",
                                                "crs001.ARMType",
                                                "crs001.ARMDesc",
                                                "crs001.ARMRevision",
                                                "crs001.ReasonOfRevsion",
                                                "crs001.SubmittedBy+' - '+com003s.t_name",
                                                "crs001.ApprovedBy+' - '+com003a.t_name",
                                                "crs001.CreatedBy +' - '+com003c.t_name",
                                                "crs001.ReviewedBy +' - '+com003r.t_name",
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_FETCH_ARM_HEADERS_RESULT
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.ContractNo),
                               Convert.ToString(uc.Customer),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.Project),
                               //Convert.ToString(uc.RevNo),
                               Convert.ToString(uc.ARMSet),
                               Convert.ToString(uc.ARMType),
                               Convert.ToString(uc.ARMDesc),
                               Convert.ToString(uc.ARMRevision),
                               Convert.ToString(uc.ReasonOfRevsion),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn !=null?(Convert.ToDateTime(uc.CreatedOn)).ToString("dd/MM/yyyy"):""),
                               Convert.ToString(uc.SubmittedBy),
                               Convert.ToString(uc.SubmittedOn !=null?(Convert.ToDateTime(uc.SubmittedOn)).ToString("dd/MM/yyyy"):""),//Convert.ToString(uc.SubmittedOn),
                               Convert.ToString(uc.ReviewedBy),
                               Convert.ToString(uc.ReviewedOn !=null?(Convert.ToDateTime(uc.ReviewedOn)).ToString("dd/MM/yyyy"):""),//Convert.ToString(uc.ReviewedOn),
                               Convert.ToString(uc.ApprovedBy),
                               Convert.ToString(uc.ApprovedOn !=null?(Convert.ToDateTime(uc.ApprovedOn)).ToString("dd/MM/yyyy"):""),//Convert.ToString(uc.ApprovedOn),
                               "<center style=\"display:inline-flex;\"><a style=\"View Details\" class='' href='"+WebsiteURL+"/CRS/MaintainARM/ARMDetailsView?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a><a class='btn btn-xs' href='javascript:void(0)' onclick=ShowTimeline('/CRS/MaintainARM/ShowTimeline?HeaderID=" + uc.HeaderId + "');><i class='fa fa-clock-o'></i></a></center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhere,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = 0,
                    iTotalDisplayRecords = 0,
                    aaData = new string[0]
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]
        public ActionResult ARMDetailsView(int HeaderID = 0)
        {
            ViewBag.HeaderID = HeaderID;
            return View();
        }

        [HttpPost]
        public ActionResult LoadARMDetails(int HeaderID)
        {
            List<string> lstARMType = clsImplementationEnum.getARMType().ToList();
            ViewBag.ARMType = lstARMType.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

            string user = objClsLoginInfo.UserName;

            List<string> lstBUs = (from a in db.ATH001
                                   where a.Employee == user
                                   select a.BU).Distinct().ToList();

            if (lstBUs.Count > 0)
            {
                ViewBag.strBUs = string.Join(",", lstBUs);
            }
            else
            {
                ViewBag.strBUs = "";
            }
            CRS001 objCRS001 = db.CRS001.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
            if (objCRS001 != null && objCRS001.HeaderId > 0)
            {
                ViewBag.Project = objCRS001.Project.Split(',').ToArray();
                var objCustomers = (from cm004 in db.COM004
                                        // join cm005 in db.COM005 on cm004.t_cono equals cm005.t_cono
                                    join cm006 in db.COM006 on cm004.t_ofbp equals cm006.t_bpid
                                    where cm004.t_cono.Trim() == objCRS001.ContractNo.Trim()
                                    select new ApproverModel { Code = cm004.t_ofbp, Name = cm004.t_ofbp + "-" + cm006.t_nama }).FirstOrDefault();

                var objContractNos = (from cm004 in db.COM004
                                      join cm008 in db.COM008 on cm004.t_cono equals cm008.t_cono
                                      //  join cm005 in db.COM005 on cm004.t_cono equals cm005.t_cono
                                      where cm004.t_cono == objCRS001.ContractNo
                                      orderby cm004.t_cono
                                      select new ApproverModel { Code = cm004.t_cono, Name = cm004.t_cono + "-" + cm004.t_desc }).FirstOrDefault();


                ViewBag.ContractDesc = objContractNos.Name;
                ViewBag.CustomersDesc = objCustomers.Name;
            }
            else
            {
                objCRS001 = new CRS001();
                objCRS001.Status = clsImplementationEnum.ARMStatus.DRAFT.GetStringValue();
                objCRS001.RevNo = 0;
                objCRS001.ARMRevision = "R0";
            }
            return PartialView("_LoadARMDetails", objCRS001);
        }

        [HttpPost]
        public ActionResult GetContractNos(string term, string strBus)
        {
            List<ApproverModel> lstContractNos = new List<ApproverModel>();
            string[] arrayBUs = { };
            if (!string.IsNullOrWhiteSpace(strBus))
            {
                arrayBUs = strBus.Split(',').ToArray();
            }
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstContractNos = (from cm004 in db.COM004
                                  join cm008 in db.COM008 on cm004.t_cono equals cm008.t_cono
                                  //join cm005 in db.COM005 on cm004.t_cono equals cm005.t_cono
                                  where arrayBUs.Contains(cm008.t_csbu) && (cm004.t_cono + "-" + cm004.t_desc).Contains(term)
                                  orderby cm004.t_cono
                                  select new ApproverModel { Code = cm004.t_cono, Name = cm004.t_cono + "-" + cm004.t_desc }).Distinct().ToList();

            }
            else
            {
                lstContractNos = (from cm004 in db.COM004
                                  join cm008 in db.COM008 on cm004.t_cono equals cm008.t_cono
                                  //  join cm005 in db.COM005 on cm004.t_cono equals cm005.t_cono
                                  where arrayBUs.Contains(cm008.t_csbu)
                                  orderby cm004.t_cono
                                  select new ApproverModel { Code = cm004.t_cono, Name = cm004.t_cono + "-" + cm004.t_desc }).Distinct().Take(10).ToList();
            }
            return Json(lstContractNos, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetCustomers(string contractNo)
        {
            ApproverModel objCustomers = new ApproverModel();
            if (!string.IsNullOrWhiteSpace(contractNo))
            {
                objCustomers = (from cm004 in db.COM004
                                    // join cm005 in db.COM005 on cm004.t_cono equals cm005.t_cono
                                join cm006 in db.COM006 on cm004.t_ofbp equals cm006.t_bpid
                                where cm004.t_cono.Trim() == contractNo.Trim()
                                select new ApproverModel { Code = cm004.t_ofbp, Name = cm004.t_ofbp + "-" + cm006.t_nama }).FirstOrDefault();
            }
            return Json(objCustomers, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetProjectByContract(string search, string param)
        {
            List<ddlValue> lstProject = new List<ddlValue>();


            if (!string.IsNullOrWhiteSpace(search))
            {

                var lstProjectold = (from cm001 in db.COM001
                                     join cm005 in db.COM005 on cm001.t_cprj equals cm005.t_sprj
                                     where cm005.t_cono == param && (cm001.t_cprj + " - " + cm001.t_dsca).Contains(search) && cm001.t_psts == 2
                                     select new { cm001.t_cprj, cm001.t_dsca }).Distinct().ToList();
                lstProject = (from a in lstProjectold
                              select new ddlValue { id = a.t_cprj, text = a.t_cprj + " - " + a.t_dsca }).ToList();
            }
            else
            {
                var lstProjectold = (from cm001 in db.COM001
                                     join cm005 in db.COM005 on cm001.t_cprj equals cm005.t_sprj
                                     where cm005.t_cono == param && cm001.t_psts == 2
                                     select new { cm001.t_cprj, cm001.t_dsca }).Distinct().Take(10).ToList();
                lstProject = (from a in lstProjectold
                              select new ddlValue { id = a.t_cprj, text = a.t_cprj + " - " + a.t_dsca }).ToList();
            }

            return Json(lstProject, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        //public ActionResult SaveARMDetails(CRS001 crs001, bool hasAttachments, Dictionary<string, string> Attach)
       public ActionResult SaveARMDetails(CRS001 crs001)
        {
            ResponceMsgWithData objResponseMsg = new ResponceMsgWithData();
            try
            {
                bool flag = false;
                int oldRevision = 0;
                if (crs001.HeaderId > 0)
                {
                    CRS001 objCRS001 = db.CRS001.Where(x => x.HeaderId == crs001.HeaderId).FirstOrDefault();
                    if (objCRS001.Status == clsImplementationEnum.ARMStatus.APPROVED.GetStringValue())
                    {
                        oldRevision = Convert.ToInt32(objCRS001.RevNo);
                        objCRS001.RevNo = Convert.ToInt32(objCRS001.RevNo) + 1;
                        objCRS001.Status = clsImplementationEnum.ARMStatus.DRAFT.GetStringValue();
                        objCRS001.ReasonOfRevsion = crs001.ReasonOfRevsion;
                        objCRS001.ARMRevision = crs001.ARMRevision;
                        objCRS001.SubmittedBy = null;
                        objCRS001.SubmittedOn = null;
                        objCRS001.ReviewedBy = null;
                        objCRS001.ReviewedOn = null;
                        objCRS001.ApprovedBy = null;
                        objCRS001.ApprovedOn = null;
                        objCRS001.ReturnByApprover = null;
                        objCRS001.ReturnedOnApprover = null;
                        objCRS001.ReturnByReviewer = null;
                        objCRS001.ReturnedOnReviewer = null;
                        objCRS001.ReturnApproverRemarks = null;
                        objCRS001.ReturnReviewerRemark = null;
                        flag = true;

                    }
                    else
                    {
                        oldRevision = Convert.ToInt32(objCRS001.RevNo);
                        objCRS001.ARMRevision = crs001.ARMRevision;
                    }
                    objCRS001.Project = crs001.Project;
                    objCRS001.Customer = crs001.Customer;
                    objCRS001.ARMSet = crs001.ARMSet;
                    objCRS001.ARMType = crs001.ARMType;
                    objCRS001.ARMDesc = crs001.ARMDesc;
                    objCRS001.ContractNo = crs001.ContractNo;
                    objCRS001.EditedBy = objClsLoginInfo.UserName;
                    objCRS001.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    if (flag)
                    {
                        string referenceDoc = "CRS001/" + objCRS001.HeaderId + "/" + oldRevision;
                        string newfolderPath = "CRS001/" + objCRS001.HeaderId + "/R" + objCRS001.RevNo;
                        //(new clsFileUpload()).CopyFolderContentsAsync(referenceDoc, newfolderPath);
                        Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                        _objFUC.CopyDataOnFCSServerAsync(referenceDoc, newfolderPath, referenceDoc, objCRS001.HeaderId, newfolderPath, objCRS001.HeaderId, DESServices.CommonService.GetUseIPConfig, DESServices.CommonService.objClsLoginInfo.UserName, 0, false);

                    }
                   
                    objResponseMsg.HeaderID = objCRS001.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PTMTCTQMessages.Update.ToString();
                    objResponseMsg.Revision = Convert.ToInt32(objCRS001.RevNo);
                    objResponseMsg.Status = objCRS001.Status;
                }
                else
                {
                    CRS001 objCRS001 = new CRS001();
                    objCRS001.Project = crs001.Project;
                    objCRS001.Status = clsImplementationEnum.ARMStatus.DRAFT.GetStringValue();
                    objCRS001.ContractNo = crs001.ContractNo;
                    objCRS001.Customer = crs001.Customer;
                    objCRS001.RevNo = 0;
                    objCRS001.ARMSet = crs001.ARMSet;
                    objCRS001.ARMType = crs001.ARMType;
                    objCRS001.ARMDesc = crs001.ARMDesc;
                    objCRS001.ARMRevision = crs001.ARMRevision;
                    objCRS001.ReasonOfRevsion = crs001.ReasonOfRevsion;
                    objCRS001.CreatedBy = objClsLoginInfo.UserName;
                    objCRS001.CreatedOn = DateTime.Now;
                    db.CRS001.Add(objCRS001);
                    db.SaveChanges();
                    //var folderPath = "CRS001/" + objCRS001.HeaderId + "/R" + objCRS001.RevNo;
                    //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                    objResponseMsg.HeaderID = objCRS001.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PTMTCTQMessages.Insert.ToString();
                    objResponseMsg.Revision = Convert.ToInt32(objCRS001.RevNo);
                    objResponseMsg.Status = objCRS001.Status;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message; ;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetProjectByContractOnEdit(string param)
        {
            List<ddlValue> lstProject = new List<ddlValue>();
            var lstProjectold = (from cm001 in db.COM001
                                 join cm005 in db.COM005 on cm001.t_cprj equals cm005.t_sprj
                                 where cm005.t_cono == param && cm001.t_psts == 2
                                 select new { cm001.t_cprj, cm001.t_dsca }).Distinct().ToList();
            lstProject = (from a in lstProjectold
                          select new ddlValue { id = a.t_cprj, text = a.t_cprj + " - " + a.t_dsca }).ToList();


            return Json(lstProject, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSelectedProject(int headerId)
        {
            try
            {
                string Project = (db.CRS001.Where(x => x.HeaderId == headerId).Select(x => x.Project).FirstOrDefault());
                string[] Projects = null;

                if (!string.IsNullOrWhiteSpace(Project))
                {
                    Projects = Project.Split(',');
                }
                return Json(Projects, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SubmitARMDetails(int headerId, string ARMRevision)
        {
            ResponceMsgWithData objResponseMsg = new ResponceMsgWithData();
            CRS001 objCRS001 = db.CRS001.Where(x => x.HeaderId == headerId).FirstOrDefault();
            if (objCRS001 != null)
            {
                objCRS001.Status = clsImplementationEnum.ARMStatus.SEND_FOR_REVIEWER.GetStringValue();
                objCRS001.EditedBy = objClsLoginInfo.UserName;
                objCRS001.EditedOn = DateTime.Now;
                objCRS001.SubmittedBy = objClsLoginInfo.UserName;
                objCRS001.SubmittedOn = DateTime.Now;
                objCRS001.ARMRevision = ARMRevision;
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = "ARM submitted successfully to reviewer.";
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "ARM Details are not available.";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult checkRevision(int HeaderId, string armRev)
        {
            bool flag = true;
            CRS001_Log CRS001_Log = db.CRS001_Log.Where(x => x.HeaderId == HeaderId && x.ARMRevision.Trim() == armRev.Trim()).FirstOrDefault();
            if (CRS001_Log != null)
            {
                flag = false;
            }
            return Json(flag);
        }

        [HttpPost]
        public JsonResult checkReasonRemarks(string remarks, bool revisionClick = false)
        {
            bool flag = true;
            if (revisionClick)
            {
                if (string.IsNullOrWhiteSpace(remarks))
                    flag = false;
            }
            return Json(flag);
        }

        public ActionResult ARMRevDetailsView(int Id, int CINId)
        {
            List<string> lstARMType = clsImplementationEnum.getARMType().ToList();
            ViewBag.ARMType = lstARMType.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();
            ViewBag.CINId = CINId;
            string user = objClsLoginInfo.UserName;

            List<string> lstBUs = (from a in db.ATH001
                                   where a.Employee == user
                                   select a.BU).Distinct().ToList();

            if (lstBUs.Count > 0)
            {
                ViewBag.strBUs = string.Join(",", lstBUs);
            }
            else
            {
                ViewBag.strBUs = "";
            }
            CRS001_Log objCRS001_Log = db.CRS001_Log.Where(x => x.Id == Id).FirstOrDefault();
            if (objCRS001_Log != null && objCRS001_Log.Id > 0)
            {
                ViewBag.Project = objCRS001_Log.Project.Split(',').ToArray();
                var objCustomers = (from cm004 in db.COM004
                                    join cm006 in db.COM006 on cm004.t_ofbp equals cm006.t_bpid
                                    where cm004.t_cono.Trim() == objCRS001_Log.ContractNo.Trim()
                                    select new ApproverModel { Code = cm004.t_ofbp, Name = cm004.t_ofbp + "-" + cm006.t_nama }).FirstOrDefault();

                var objContractNos = (from cm004 in db.COM004
                                      join cm008 in db.COM008 on cm004.t_cono equals cm008.t_cono
                                      where cm004.t_cono == objCRS001_Log.ContractNo
                                      orderby cm004.t_cono
                                      select new ApproverModel { Code = cm004.t_cono, Name = cm004.t_cono + "-" + cm004.t_desc }).FirstOrDefault();


                ViewBag.ContractDesc = objContractNos.Name;
                ViewBag.CustomersDesc = objCustomers.Name;
                return View(objCRS001_Log);
            }
            else
            {
                return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
            }

        }

        public ActionResult ShowTimeline(int HeaderId)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.Title = "CRS";

            CRS001 objCRS001 = db.CRS001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            model.ApprovedBy = Manager.GetUserNameFromPsNo(objCRS001.ApprovedBy);
            model.ApprovedOn = objCRS001.ApprovedOn;

            model.CreatedBy = Manager.GetUserNameFromPsNo(objCRS001.CreatedBy);
            model.CreatedOn = objCRS001.CreatedOn;

            model.ReviewedBy = Manager.GetUserNameFromPsNo(objCRS001.ReviewedBy);
            model.ReviewedOn = objCRS001.ReviewedOn;

            model.ReturnedByReviewer = Manager.GetUserNameFromPsNo(objCRS001.ReturnByReviewer);
            model.ReturnedOnReviewer = objCRS001.ReturnedOnReviewer;
            model.ReturnedByApprover = Manager.GetUserNameFromPsNo(objCRS001.ReturnByApprover);
            model.ReturnedOnApprover = objCRS001.ReturnedOnApprover;
            return PartialView("~/Views/Shared/_DisplayCRSTimeLinePartial.cshtml", model);
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
            {
                List<clsARMExport> lstclsARMExport = new List<clsARMExport>();
                var lst = db.SP_FETCH_ARM_HEADERS_RESULT(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                if (lst != null && lst.Count > 0)
                {
                    lstclsARMExport.AddRange(lst.Select(x => new clsARMExport
                    {
                        ROW_NO = x.ROW_NO,
                        ContractNo = x.ContractNo,
                        Customer = x.Customer,
                        Status = x.Status,
                        Project = x.Project,
                        ARMSet = x.ARMSet,
                        ARMType = x.ARMType,
                        ARMDesc = x.ARMDesc,
                        ARMRevision = x.ARMRevision,
                        //RevNo = x.RevNo,
                        ReasonOfRevsion = x.ReasonOfRevsion,
                        SubmittedBy = x.SubmittedBy,
                        SubmittedOn = x.SubmittedOn,
                        ApprovedBy = x.ApprovedBy,
                        ApprovedOn = x.ApprovedOn,
                        CreatedBy = x.CreatedBy,
                        CreatedOn = x.CreatedOn,
                        EditedBy = x.EditedBy,
                        EditedOn = x.EditedOn,
                        ReviewedBy = x.ReviewedBy,
                        ReviewedOn = x.ReviewedOn
                    }));
                    string str = Helper.GenerateExcel(lstclsARMExport, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = str;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Data not available.";
                }
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Data not available.";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

    }
    public class ResponceMsgWithData : clsHelper.ResponseMsg
    {
        public int HeaderID;
        public int Revision;
        public string Status;
    }
    public class MyCustomDateProvider : IFormatProvider, ICustomFormatter
    {
        public object GetFormat(Type formatType)
        {
            if (formatType == typeof(ICustomFormatter))
                return this;

            return null;
        }

        public string Format(string format, object arg, IFormatProvider formatProvider)
        {
            if (!(arg is DateTime)) throw new NotSupportedException();

            var dt = (DateTime)arg;

            string suffix;

            if (new[] { 11, 12, 13 }.Contains(dt.Day))
            {
                suffix = "th";
            }
            else if (dt.Day % 10 == 1)
            {
                suffix = "st";
            }
            else if (dt.Day % 10 == 2)
            {
                suffix = "nd";
            }
            else if (dt.Day % 10 == 3)
            {
                suffix = "rd";
            }
            else
            {
                suffix = "th";
            }

            return string.Format("{0:MMMM} {1}{2}, {0:yyyy}", arg, dt.Day, suffix);
        }
    }

    public class clsARMExport
    {
        public Nullable<long> ROW_NO { get; set; }
        public string ContractNo { get; set; }
        public string Customer { get; set; }
        public string Project { get; set; }
        public string Status { get; set; }
        public string ARMSet { get; set; }
        public string ARMType { get; set; }
        public string ARMDesc { get; set; }
        public string ARMRevision { get; set; }
        public string ReasonOfRevsion { get; set; }
        public Nullable<int> DocNo { get; set; }
        public string SubmittedBy { get; set; }
        public Nullable<System.DateTime> SubmittedOn { get; set; }
        public string ApprovedBy { get; set; }
        public Nullable<System.DateTime> ApprovedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string ReviewedBy { get; set; }
        public Nullable<System.DateTime> ReviewedOn { get; set; }
        public Nullable<int> RevNo { get; set; }

    }
}