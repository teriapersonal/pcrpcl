﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;

using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.TPI.Controllers
{
    public class MaintainController : clsBase
    {
        /// <summary>
        /// Created By :Deepak tiwari
        /// Created Date :16/08/2017
        /// Description: TPI Agency and Intervention
        /// </summary>
        /// <returns></returns>
        // GET:TPI/MaintainTPIIntervention
        [SessionExpireFilter]
         [AllowAnonymous]
          [UserPermissions]

        #region first Page
        public ActionResult Index()
        {
            return View();
        }

        // load datatable
        public JsonResult LoadMTData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                //if (param.MTStatus.ToUpper() == "PENDING")
                //{
                //    strWhere += "1=1 and status in('Draft','Returned')";
                //}
                //else
                //{
                //    strWhere += "1=1";
                //}
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += "  (QualityProject like '%" + param.sSearch + "%' or Stage like '%" + param.sSearch + "%' )";
                }
                //strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "BU", "Location");

                var lstResult = db.SP_TPT_GetTPIAgencyHeader
                                (
                                StartIndex, EndIndex, "", strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                           Convert.ToString(uc.QualityProject),
                           Convert.ToString(uc.StageType),
                           Convert.ToString(uc.Stage),
                           Convert.ToString(uc.StageDesc),
                          Convert.ToString(uc.Id)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Tab
        [HttpPost]
        public ActionResult GetMTPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetTPiInterventionHtmlPartial");
        }
        #endregion

        #region Create Consumable
        public ActionResult AddTPIAgency(int? id)
        {

            QMS025 objQMS025 = new QMS025();
            QMS010 objQMS010 = new QMS010();

            var user = objClsLoginInfo.UserName;
            var location = objClsLoginInfo.Location;


            if (id != null)
            {
                var objId = Convert.ToInt32(id);
                objQMS025 = db.QMS025.Where(x => x.Id == objId).FirstOrDefault();
                ViewBag.QualityProject = objQMS025.QualityProject;
                ViewBag.FirstTPIAgency = objQMS025.FirstTPIAgency;
                ViewBag.FirstTPIIntervention = objQMS025.FirstTPIIntervention;
                ViewBag.SecondTPIAgency = objQMS025.SecondTPIAgency;
                ViewBag.SecondTPIIntervention = objQMS025.SecondTPIIntervention;
                ViewBag.ThirdTPIAgency = objQMS025.ThirdTPIAgency;
                ViewBag.ThirdTPIIntervention = objQMS025.ThirdTPIIntervention;
                ViewBag.ForthTPIAgency = objQMS025.ForthTPIAgency;
                ViewBag.ForthTPIIntervention = objQMS025.ForthTPIIntervention;
                ViewBag.FifthTPIAgency = objQMS025.FifthTPIAgency;
                ViewBag.FifthTPIIntervention = objQMS025.FifthTPIIntervention;
                ViewBag.SixthTPIAgency = objQMS025.SixthTPIAgency;
                ViewBag.SixthTPIIntervention = objQMS025.SixthTPIIntervention;

                objQMS025.Location = db.COM002.Where(i => i.t_dtyp == 1 && i.t_dimx == objQMS010.Location).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
                objQMS025.Project = db.COM001.Where(i => i.t_cprj == objQMS025.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objQMS025.BU = db.COM002.Where(i => i.t_dtyp == 2 && i.t_dimx == objQMS025.BU).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
                ViewBag.Action = "Edit";
            }
            else
            {

                objQMS025.Location = location;
                ViewBag.Action = "AddNew";
            }


            return View(objQMS025);
        }


        [HttpPost]
        public ActionResult SaveTPI(FormCollection fc, QMS025 QMS025)
        {

            QMS025 objQMS025 = new QMS025();
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {

                string location = fc["Location"].Split('-')[0];
                string Status = clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue();
                string Approved = clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue();
                if (objQMS025.Id > 0)
                {
                    objQMS025 = db.QMS025.Where(x => x.Id == QMS025.Id).FirstOrDefault();
                    objQMS025.QualityProject = QMS025.QualityProject;
                    objQMS025.Stage = QMS025.Stage;
                    objQMS025.FirstTPIAgency = QMS025.FirstTPIAgency.Split('-')[0];
                    objQMS025.FirstTPIIntervention = QMS025.FirstTPIIntervention.Split('-')[0];
                    if ((QMS025.SecondTPIAgency).ToString() != null || (QMS025.SecondTPIAgency).ToString() != "")
                    {
                        objQMS025.SecondTPIAgency = QMS025.SecondTPIAgency.Split('-')[0];
                    }
                    else
                    {
                        objQMS025.SecondTPIAgency = null;
                    }
                    if ((QMS025.SecondTPIIntervention).ToString() != null || (QMS025.SecondTPIIntervention).ToString() != "")
                    {
                        objQMS025.SecondTPIIntervention = QMS025.SecondTPIIntervention.Split('-')[0];
                    }
                    else
                    {
                        objQMS025.SecondTPIIntervention = null;
                    }
                    if ((QMS025.ThirdTPIAgency).ToString() != null || (QMS025.ThirdTPIAgency).ToString() != "")
                    {
                        objQMS025.ThirdTPIAgency = QMS025.ThirdTPIAgency.Split('-')[0];
                    }
                    else
                    {
                        objQMS025.ThirdTPIAgency = null;
                    }
                    if ((QMS025.ThirdTPIIntervention).ToString() != null || (QMS025.ThirdTPIIntervention).ToString() != "")
                    {
                        objQMS025.ThirdTPIIntervention = QMS025.ThirdTPIIntervention.Split('-')[0];
                    }
                    else
                    {
                        objQMS025.ThirdTPIIntervention = null;
                    }
                    if ((QMS025.ForthTPIAgency).ToString() != null || (QMS025.ForthTPIAgency).ToString() != "")
                    {
                        objQMS025.ForthTPIAgency = QMS025.ForthTPIAgency.Split('-')[0];
                    }
                    else
                    {
                        objQMS025.ForthTPIAgency = null;
                    }
                    if ((QMS025.ForthTPIIntervention).ToString() != null || (QMS025.ForthTPIIntervention).ToString() != "")
                    {
                        objQMS025.ForthTPIIntervention = QMS025.ForthTPIIntervention.Split('-')[0];
                    }
                    else
                    {
                        objQMS025.ForthTPIIntervention = null;
                    }


                    if ((QMS025.FifthTPIAgency).ToString() != null || (QMS025.FifthTPIAgency).ToString() != "")
                    {
                        objQMS025.FifthTPIAgency = QMS025.FifthTPIAgency.Split('-')[0];
                    }
                    else
                    {
                        objQMS025.FifthTPIAgency = null;
                    }
                    if ((QMS025.FifthTPIIntervention).ToString() != null || (QMS025.FifthTPIIntervention).ToString() != "")
                    {
                        objQMS025.FifthTPIIntervention = QMS025.FifthTPIIntervention.Split('-')[0];
                    }
                    else
                    {
                        objQMS025.FifthTPIIntervention = null;
                    }


                    if ((QMS025.SixthTPIAgency).ToString() != null || (QMS025.SixthTPIAgency).ToString() != "")
                    {
                        objQMS025.SixthTPIAgency = QMS025.SixthTPIAgency.Split('-')[0];
                    }
                    else
                    {
                        objQMS025.SixthTPIAgency = null;
                    }
                    if ((QMS025.SixthTPIIntervention).ToString() != null || (QMS025.SixthTPIIntervention).ToString() != "")
                    {
                        objQMS025.SixthTPIIntervention = QMS025.SixthTPIIntervention.Split('-')[0];
                    }
                    else
                    {
                        objQMS025.SixthTPIIntervention = null;
                    }
                    objQMS025.Remarks = QMS025.Remarks;
                   

                    objQMS025.CreatedBy = QMS025.CreatedBy;
                    objQMS025.CreatedOn = QMS025.CreatedOn;
                    objQMS025.EditedBy = objClsLoginInfo.UserName;
                    objQMS025.EditedOn = DateTime.Now;
                    int Id = objQMS025.Id;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = Id.ToString();
                    // objResponseMsg.Status = objNDE020.Status;
                    db.SaveChanges();
                }
                else
                {

                    string loc = fc["Location"].Split('-')[0];
                    string Fromstage = fc["Stage"].Split('-')[0];
                    string Tostage = fc["ToStage"].Split('-')[0];

                    var lstResultForstage = db.SP_GETSTAGECODE_FORTPI
                             (
                             Fromstage, Tostage, objQMS025.QualityProject).ToList();
                    
                    if(lstResultForstage.Count>0)
                    {
                        //int count = lstResultForstage.Count;
                        //for(int i=0;i<count;i++)
                        //{
                            foreach(var Stagecode in lstResultForstage.ToList())
                            {
                            objQMS025.QualityProject = QMS025.QualityProject;
                            objQMS025.Project = fc["Project"].Split('-')[0];
                            objQMS025.BU = fc["BU"].Split('-')[0];
                            objQMS025.Location = loc;
                            objQMS025.Stage = Stagecode.ToString();
                            //objQMS025.Stage = fc["Location"].Split('-')[0];
                            objQMS025.FirstTPIAgency = fc["FirstTPIAgency"].Split('-')[0];
                            objQMS025.FirstTPIIntervention = fc["FirstTPIIntervention"].Split('-')[0];

                            if (objQMS025.SecondTPIAgency != null)
                            {
                                objQMS025.SecondTPIAgency = fc["SecondTPIAgency"].Split('-')[0];
                            }
                            else
                            {
                                objQMS025.SecondTPIAgency = null;
                            }
                            if (objQMS025.SecondTPIIntervention != null)
                            {
                                objQMS025.SecondTPIIntervention = fc["SecondTPIIntervention"].Split('-')[0];
                            }
                            else
                            {
                                objQMS025.SecondTPIIntervention = null;
                            }

                            if (objQMS025.ThirdTPIAgency != null)
                            {
                                objQMS025.ThirdTPIAgency = fc["ThirdTPIAgency"].Split('-')[0];
                            }
                            else
                            {
                                objQMS025.ThirdTPIAgency = null;
                            }
                            if (objQMS025.ThirdTPIIntervention != null)
                            {
                                objQMS025.ThirdTPIIntervention = fc["ThirdTPIIntervention"].Split('-')[0];
                            }
                            else
                            {
                                objQMS025.ThirdTPIIntervention = null;
                            }

                            if (objQMS025.ForthTPIAgency != null)
                            {
                                objQMS025.ForthTPIAgency = fc["ForthTPIAgency"].Split('-')[0];
                            }
                            else
                            {
                                objQMS025.ForthTPIAgency = null;
                            }
                            if (objQMS025.ForthTPIIntervention != null)
                            {
                                objQMS025.ForthTPIIntervention = fc["ForthTPIIntervention"].Split('-')[0];
                            }
                            else
                            {
                                objQMS025.ForthTPIIntervention = null;
                            }

                            if (objQMS025.FifthTPIAgency != null)
                            {
                                objQMS025.FifthTPIAgency = fc["FifthTPIAgency"].Split('-')[0];
                            }
                            else
                            {
                                objQMS025.FifthTPIAgency = null;
                            }
                            if (objQMS025.FifthTPIIntervention != null)
                            {
                                objQMS025.FifthTPIIntervention = fc["FifthTPIIntervention"].Split('-')[0];
                            }
                            else
                            {
                                objQMS025.FifthTPIIntervention = null;
                            }

                            if (objQMS025.SixthTPIAgency != null)
                            {
                                objQMS025.SixthTPIAgency = fc["SixthTPIAgency"].Split('-')[0];
                            }
                            else
                            {
                                objQMS025.SixthTPIAgency = null;
                            }
                            if (objQMS025.SixthTPIIntervention != null)
                            {
                                objQMS025.SixthTPIIntervention = fc["SixthTPIIntervention"].Split('-')[0];
                            }
                            else
                            {
                                objQMS025.SixthTPIIntervention = null;
                            }
                            objQMS025.CreatedBy = objClsLoginInfo.UserName;
                            objQMS025.CreatedOn = DateTime.Now;
                            db.QMS025.Add(objQMS025);
                            db.SaveChanges();
                            int Id = objQMS025.Id;
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = Id.ToString();

                        }
                        //}

                    }
                    else
                    {
                        //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Record Not Found";
                        objResponseMsg.Status= "Record Not Found";
                        ViewBag.Action = "Add";
                        return Json(objResponseMsg);
                    }


                  
                    //objResponseMsg.Status = objNDE020.Status;
                }
            }
            catch (Exception ex)
            {
              
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg);



        }
        [HttpPost]
        public ActionResult DeleteTPI(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS025 objQMS025 = db.QMS025.Where(x => x.Id == Id).FirstOrDefault();
                db.QMS025.Remove(objQMS025);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.NDEConsumeMessage.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Common Functions

        [HttpPost]
        public ActionResult GetProject(string qms)
        {   //get project and BU by Qms project
            if (!string.IsNullOrWhiteSpace(qms))
            {

                BUWiseDropdown objProjectDataModel = new BUWiseDropdown();

                string project = db.QMS010.Where(a => a.QualityProject == qms).FirstOrDefault().Project;

                var lstProject = (from a in db.COM001
                                  where a.t_cprj == project
                                  select new { ProjectDesc = a.t_cprj + " - " + a.t_dsca }).FirstOrDefault();
                string BU = db.COM001.Where(i => i.t_cprj == project).FirstOrDefault().t_entu;

                var BUDescription = (from a in db.COM002
                                     where a.t_dtyp == 2 && a.t_dimx == BU
                                     select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();

                objProjectDataModel.Project = lstProject.ProjectDesc;
                objProjectDataModel.Location = objClsLoginInfo.Location;
                objProjectDataModel.BUDescription = BUDescription.BUDesc;

                List<BULocWiseCategoryModel> lstBULocWiseMethodModel;


                List<QMS002> lstMethodGLB002 = GetStageType(BU, objClsLoginInfo.Location).ToList();
                if (lstMethodGLB002.Count > 0)
                {
                    lstBULocWiseMethodModel = lstMethodGLB002.Select(x => new BULocWiseCategoryModel { CatDesc = (x.StageCode + "-" + x.StageDesc), CatID = x.StageCode }).ToList();
                    objProjectDataModel.lstBULocWiseMethodModel = lstBULocWiseMethodModel;
                }


                return Json(objProjectDataModel, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<QMS002> GetStageType(string strBU, string strLoc)
        {
            //List<QMS002> lstqms002 = (from QMS002 in db.QMS002                      
            //                          select QMS002).ToList();
            //return lstqms002;

            List<QMS002> lstGLB002 = (from qmS002 in db.QMS002
                                      where strBU.Equals(qmS002.BU) && strLoc.Equals(qmS002.Location)

                                      select qmS002).ToList();
            return lstGLB002;


        }




        #endregion



        [HttpPost]
        public JsonResult LoadMTHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;

                strWhere += "1=1 and Id=" + param.Headerid;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or (NDE.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%' or  QualityProject like '%" + param.sSearch + "%' or MTConsuNo like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%')";
                }
                var lstResult = db.SP_NDE_MT_CONSUMABLE_HISTORY_DETAILS
                                (
                                StartIndex, EndIndex, "", strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                           Convert.ToString(uc.QualityProject),
                           Convert.ToString(uc.Method),
                           Convert.ToString(uc.MTConsuNo),
                           Convert.ToString("R " +uc.RevNo),
                           Convert.ToString(uc.Status),
                           Convert.ToString(uc.CreatedBy),
                           Convert.ToString(uc.LogId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }


        #region Copy data
        [HttpPost]
        public ActionResult GetCopyDetails(string projectid) //partial for Copy Details
        {
            //ViewBag.QualityProject = new SelectList(db.TMP001.ToList(), "QualityProject", "QualityProject");
            return PartialView("~/Areas/NDE/Views/Shared/_CopyQualityProjectPartial.cshtml");
        }

        public class ResponceMsgWithHeaderID : clsHelper.ResponseMsg
        {
            public int HeaderID;
        }


        #endregion

        #region Classes
        public class ResponceMsgWithStatus : clsHelper.ResponseMsg
        {
            public string Status;
        }

        public class BUWiseDropdown : ProjectDataModel
        {
            public string Project { get; set; }
            public string Location { get; set; }
            public List<BULocWiseCategoryModel> lstBULocWiseMethodModel { get; set; }
            public List<BULocWiseCategoryModel> lstBULocWiseBrandModel { get; set; }
        }

        #endregion


    }
}