﻿using IEMQS.Areas.HTC.Models;
using IEMQS.Areas.MSW.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.HTC.Controllers
{
    public class ApproveHTCController : clsBase
    {
        // GET: HTC/ApproveHTC
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("GetHTCGridApproveHTML");
        }

        [HttpPost]
        public JsonResult LoadHTCHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue() + "') and ApprovedBy=" + user+"";
                }
                else
                {
                    strWhere += "1=1";
                }
               // strWhere += " and ApprovedBy=" + user;

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += "and (Project like '%" + param.sSearch
                       + "%' or Document like '%" + param.sSearch
                       + "%' or Customer like '%" + param.sSearch
                       + "%' or RevNo like '%" + param.sSearch
                       + "%' or Product like '%" + param.sSearch
                       + "%' or ProcessLicensor like '%" + param.sSearch
                       + "%' or Status like '%" + param.sSearch + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                var lstResult = db.SP_HTC_GET_HEADERDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]{
                                Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(GetCodeAndNameByProject(uc.Project)),
                               Convert.ToString(uc.Document),
                               Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Project)),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                                Convert.ToString(uc.SubmittedBy),
                                Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),
                                Convert.ToString(uc.ApprovedBy),
                                Convert.ToString(Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy")),

                               //Convert.ToString(uc.HeaderId),
                               //Convert.ToString(uc.HeaderId),
                               // "<a title='View' href='/HTC/ApproveHTC/HTCDetailApprove?Id="+Convert.ToInt32(uc.HeaderId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a>"+" "+(uc.RevNo>0 ?"<a title=\"History\" onclick=\"ViewHistoryForProjectPLN('"+uc.HeaderId+"','Initiator','/HTC/ApproveHTC/GetHistoryView','Heat Treatment ')\"><i style='margin-left:5px;' class='fa fa-history'></i></a>"+" "+"<a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/HTC/MaintainHTC/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')><i class='fa fa-clock-o'></i></a>":"")
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [SessionExpireFilter]
        public ActionResult HTCDetailApprove(int Id = 0)
        {
            HTC001 objHTC001 = new HTC001();
            var user = objClsLoginInfo.UserName;

            var lstProjectDesc = (from a in db.COM001
                                  select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).ToList();
            ViewBag.Project = new SelectList(lstProjectDesc, "projectCode", "projectDescription");

            List<string> lstProCat = clsImplementationEnum.getProductCategory().ToList();
            ViewBag.lstProCat = lstProCat.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();
            var project = (from a in db.HTC001
                           where a.HeaderId == Id
                           select a.Project).FirstOrDefault();
            ViewBag.Customer = Manager.GetCustomerCodeAndNameByProject(project);
            var approver = (from t1 in db.ATH001
                            join t2 in db.COM003 on t1.Employee equals t2.t_psno
                            where t2.t_actv == 1
                            select new { t1.Employee, Desc = t2.t_psno + " - " + t2.t_name }).Distinct().ToList();
            ViewBag.approver = new SelectList(approver, "Employee", "Desc");
            List<string> lstFuranceCharge = clsImplementationEnum.getFurnaceCharge().ToList();
            ViewBag.lstFuranceCharge = lstFuranceCharge.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            List<string> lstFurnaceTypes = clsImplementationEnum.getFurnacetypes().ToList();
            ViewBag.lstFurnaceTypes = lstFurnaceTypes.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            if (Id > 0)
            {
                objHTC001 = db.HTC001.Where(x => x.HeaderId == Id).FirstOrDefault();
                //if (objHTC001.ApprovedBy != objClsLoginInfo.UserName)
                //{
                //    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                //}
                var PlanningDinID = db.PDN002.Where(x => x.RefId == Id && x.DocumentNo == objHTC001.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;
                ViewBag.Revoke = Manager.IsRevokeApplicable(PlanningDinID, Id, clsImplementationEnum.PlanList.Heat_Treatment_Charge_Sheet.GetStringValue());
                if (objHTC001.ApprovedBy != null)
                {
                    if (objHTC001.ApprovedBy.Trim() != objClsLoginInfo.UserName.Trim())
                    {
                        ViewBag.IsDisplayMode = true;
                    }
                    else
                    {
                        ViewBag.IsDisplayMode = false;
                    }
                }
                else
                {
                    ViewBag.IsDisplayMode = true;
                }
            }

            return View(objHTC001);
        }
        public ActionResult ReturnSelectedHTC(string strHeaderIds, string remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
                for (int i = 0; i < arrayHeaderIds.Length; i++)
                {
                    int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                    HTC001 objHTC001 = db.HTC001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    objHTC001.Status = clsImplementationEnum.CTQStatus.Returned.GetStringValue();
                    objHTC001.ApprovedBy = objClsLoginInfo.UserName;
                    objHTC001.ApprovedOn = DateTime.Now;
                    objHTC001.ReturnRemark = remarks;
                    db.SaveChanges();
                    Manager.UpdatePDN002(objHTC001.HeaderId, objHTC001.Status, objHTC001.RevNo, objHTC001.Project, objHTC001.Document);

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(), objHTC001.Project, "", "", Manager.GetPDINDocumentNotificationMsg(objHTC001.Project, clsImplementationEnum.PlanList.Heat_Treatment_Charge_Sheet.GetStringValue(), objHTC001.RevNo.Value.ToString(), objHTC001.Status), clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(), Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Heat_Treatment_Charge_Sheet.GetStringValue(), objHTC001.HeaderId.ToString(), false), objHTC001.CreatedBy);
                    #endregion
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.QCPMessages.Return.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ApproveSelectedHTC(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
                for (int i = 0; i < arrayHeaderIds.Length; i++)
                {
                    int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                    GenerateHTR(HeaderId);
                    db.SP_HTC_APPROVE(HeaderId, objClsLoginInfo.UserName);
                    HTC001 objHTC001 = db.HTC001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    int newId = db.HTC001_Log.Where(q => q.HeaderId == HeaderId).OrderByDescending(q => q.ApprovedOn).FirstOrDefault().Id;
                    string strApproved = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
                    Manager.UpdatePDN002(objHTC001.HeaderId, strApproved, objHTC001.RevNo, objHTC001.Project, objHTC001.Document, newId);

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(), objHTC001.Project, "", "", Manager.GetPDINDocumentNotificationMsg(objHTC001.Project, clsImplementationEnum.PlanList.Heat_Treatment_Charge_Sheet.GetStringValue(), objHTC001.RevNo.Value.ToString(), objHTC001.Status), clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(), Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Heat_Treatment_Charge_Sheet.GetStringValue(), objHTC001.HeaderId.ToString(), false), objHTC001.CreatedBy);
                    #endregion
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.QCPMessages.Approve.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetInsertView(string project, string document, string RevNo)
        {
            project = project.Split('-')[0].ToString().Trim();
            int Headerid = (from a in db.HTC001
                            where a.Project == project
                            select a.HeaderId).FirstOrDefault();
            List<string> lstFuranceCharge = clsImplementationEnum.getFurnaceCharge().ToList();
            ViewBag.lstFuranceCharge = lstFuranceCharge.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            List<string> lstFurnaceTypes = clsImplementationEnum.getFurnacetypes().ToList();
            ViewBag.lstFurnaceTypes = lstFurnaceTypes.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            ViewBag.project = project;
            ViewBag.document = document;
            ViewBag.RevNo = RevNo;
            ViewBag.Headerid = Headerid;

            HTC002 objHTC002 = new HTC002();
            return PartialView("HTCLinesApprove", objHTC002);
        }

        [HttpPost]
        public ActionResult GetUpdateView(string project, string document, string RevNo, int lineid)
        {
            project = project.Split('-')[0].ToString().Trim();
            HTC002 objHTC002 = new HTC002();
            var isvalid = db.HTC002.Any(x => x.LineId == lineid);
            if (isvalid == true)
            {
                objHTC002 = db.HTC002.Where(x => x.LineId == lineid).FirstOrDefault();
                if (objHTC002 != null)
                {
                    int Headerid = (from a in db.HTC001
                                    where a.Project == project
                                    select a.HeaderId).FirstOrDefault();
                    List<string> lstFuranceCharge = clsImplementationEnum.getFurnaceCharge().ToList();
                    ViewBag.lstFuranceCharge = lstFuranceCharge.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

                    List<string> lstFurnaceTypes = clsImplementationEnum.getFurnacetypes().ToList();
                    ViewBag.lstFurnaceTypes = lstFurnaceTypes.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

                    ViewBag.project = project;
                    ViewBag.document = document;
                    ViewBag.RevNo = RevNo;
                    ViewBag.Headerid = Headerid;


                }
            }
            return PartialView("HTCLinesApprove", objHTC002);
        }

        [HttpPost]
        public ActionResult GenerateHTR(int strHeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string currentUser = objClsLoginInfo.UserName;
                var currentLoc = objClsLoginInfo.Location;
                var lstHTC002 = db.HTC002.Where(x => x.HeaderId == strHeaderId && (x.IsHTRCreated == false || x.IsHTRCreated == null)).ToList();
                List<HTR001> lstHTR001 = new List<HTR001>();
                string userdept = (from deptr in db.COM003
                                   where deptr.t_psno == objClsLoginInfo.UserName && deptr.t_actv == 1
                                   select deptr.t_depc).FirstOrDefault();
                HTC001 objhtc001 = db.HTC001.Where(m => m.HeaderId == strHeaderId).FirstOrDefault();

                string IdenticalProject = Manager.getPDNIdenticalproject(objhtc001.Project);
                string[] Idenproject = IdenticalProject.Split(',');
                foreach (var item in lstHTC002)
                {                    
                    var HTRHeaderId = 0;
                    List<int> Secondryhtrheaderids = new List<int>();
                    foreach (string project in Idenproject)
                    {
                        HTR001 objHTR001Add = new HTR001();                        
                        objHTR001Add.Project = project.Trim();
                        objHTR001Add.HTRNo = MakeHTR(project.Trim()); //item.HTRNo;
                        string doc = objHTR001Add.HTRNo.Split('/')[2];
                        objHTR001Add.DocNo = Convert.ToInt32(doc);
                        objHTR001Add.RevNo = 0;
                        objHTR001Add.Department = userdept;
                        objHTR001Add.BU = db.COM001.Where(i => i.t_cprj == objHTR001Add.Project).FirstOrDefault().t_entu;
                        objHTR001Add.HTRType = clsImplementationEnum.TypeOfHTR.JobComponent.GetStringValue();
                        objHTR001Add.Location = objClsLoginInfo.Location;
                        var HTRDescription = item.FurnaceCharge + " - " + item.SectionDescription;
                        if(HTRDescription.Length <= 350) // Validate Max Langth
                            objHTR001Add.HTRDescription = HTRDescription;
                        objHTR001Add.CreatedBy = item.HTC001?.SubmittedBy;
                        objHTR001Add.CreatedOn = DateTime.Now;
                        objHTR001Add.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                        db.HTR001.Add(objHTR001Add);
                        db.SaveChanges();
                        (new clsManager()).ScurveHTC(objHTR001Add.Project, objHTR001Add.Location);
                        if (project.Trim() == objhtc001.Project.Trim())
                        {
                            HTRHeaderId = objHTR001Add.HeaderId;
                        }
                        else
                        {
                            Secondryhtrheaderids.Add(objHTR001Add.HeaderId);
                        }
                    }
                    //lstHTR001.Add(objHTR001Add);
                    
                    if (item.IsHTRCreated == false || item.IsHTRCreated == null)
                    {
                        item.IsHTRCreated = true;
                        item.HTRHeaderId = HTRHeaderId;
                        if (Secondryhtrheaderids.Count() > 0)
                        {
                            item.IdenticalHTRHeaderIds = string.Join(",", Secondryhtrheaderids);
                        }
                        db.SaveChanges();
                    }
                }                
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public string MakeHTR(string code)
        {
            var lstobjHTR001 = db.HTR001.Where(x => x.Project == code).ToList();
            string HTRNumber = "";
            string AHTRNo = string.Empty;

            if (lstobjHTR001 != null && lstobjHTR001.Count() != 0)
            {
                var HTRNum = (from a in db.HTR001
                              where a.Project.Equals(code, StringComparison.OrdinalIgnoreCase)
                              select a).Max(a => a.DocNo);
                HTRNumber = (HTRNum + 1).ToString();
            }
            else
            {
                HTRNumber = "1";
            }
            AHTRNo = "HTR/" + code + "/" + HTRNumber.PadLeft(3, '0');
            return AHTRNo;
        }

        [HttpPost]
        public ActionResult SaveHTCLines(HTC002 htc002, FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                HTC002 objHTC002 = new HTC002();
                objHTC002.HeaderId = Convert.ToInt32(fc["hfHeaderId"].ToString());
                objHTC002.Project = fc["hfProject"].ToString();
                objHTC002.Document = fc["hfdoc"].ToString();
                objHTC002.RevNo = Convert.ToInt32(Regex.Replace(fc["hfRev"].ToString(), "[^0-9]+", string.Empty));
                objHTC002.SectionDescription = fc["txtSecDesc"].ToString();
                objHTC002.FurnaceCharge = fc["ddlFCharge"].ToString();
                objHTC002.Height = string.IsNullOrWhiteSpace(fc["txtHeight"].ToString()) ? 0 : Convert.ToInt32(fc["txtHeight"]);
                objHTC002.Width = string.IsNullOrWhiteSpace(fc["txtWidth"].ToString()) ? 0 : Convert.ToInt32(fc["txtWidth"]); //Convert.ToInt32(fc["txtWidth"]);
                objHTC002.Length = string.IsNullOrWhiteSpace(fc["txtLength"].ToString()) ? 0 : Convert.ToInt32(fc["txtLength"]); //Convert.ToInt32(fc["txtLength"]);
                objHTC002.Weight = string.IsNullOrWhiteSpace(fc["txtWeight"].ToString()) ? 0 : Convert.ToDecimal(fc["txtWeight"], CultureInfo.InvariantCulture);//Convert.ToInt32(fc["txtWeight"]);
                objHTC002.Furnace = fc["ddlFType"].ToString();
                objHTC002.CreatedBy = objClsLoginInfo.UserName;
                objHTC002.CreatedOn = DateTime.Now;
                db.HTC002.Add(objHTC002);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult LoadHTCLineData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                int headerid = Convert.ToInt32(param.Headerid);
                bool isEditable = true;
                HTC001 objhtc001 = db.HTC001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                if (objhtc001.Status.ToLower() == clsImplementationEnum.WKGHeaderStatus.SentForApproval.GetStringValue().ToLower() && objhtc001.ApprovedBy == objClsLoginInfo.UserName)
                {
                    isEditable = false;
                }
                string headerstatus = param.MTStatus;
                string strWhere = string.Empty;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (SectionDescription like '%" + param.sSearch
                        + "%' or FurnaceCharge like '%" + param.sSearch
                        + "%' or Height like '%" + param.sSearch
                        + "%' or Width like '%" + param.sSearch
                        + "%' or Length like '%" + param.sSearch
                        + "%' or Weight like '%" + param.sSearch
                        + "%' or Furnace like '%" + param.sSearch
                        + "%')";
                }
                else
                {
                    strWhere = "1=1";
                }
                var strSortOrder = string.Empty;
                var lstResult = db.SP_HTC_GET_LINEDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, headerid, strWhere
                                ).ToList();
                int newRecordId = 0;
                var newRecord = new[] {
                                        //Helper.GenerateHidden(newRecordId, "HeaderId", param.CTQHeaderId),
                                            GenerateTextboxFor(newRecordId, "SectionDescription", "", "","", false,"", "100"),
                                            GenerateAutoComplete(newRecordId,"FurnaceCharge","","",false,"","FurnaceCharge"),
                                            //GenerateTextboxFor(newRecordId, "FurnaceCharge", Convert.ToString(uc.FurnaceCharge),"UpdateData(this, "+ uc.LineId +");","checkStatus("+ uc.HeaderId +");"),
                                            GenerateNumericTextboxFor(newRecordId, "Length", "", "","", false,"", "10","numberonly"),
                                            GenerateNumericTextboxFor(newRecordId, "Width", "", "","", false,"", "10","numberonly"),
                                            GenerateNumericTextboxFor(newRecordId, "Height", "", "","", false,"", "10","numberonly"),
                                            GenerateNumericTextboxFor(newRecordId, "Weight", "", "","", false,"", "10"),
                                            GenerateAutoComplete(newRecordId,"Furnace","","",false,"","Furnace"),
                                            "",
                                            //"No",//GenerateTextboxFor(newRecordId, "Furnace", Convert.ToString(uc.Furnace),"UpdateData(this, "+ uc.LineId +");","checkStatus("+ uc.HeaderId +");"),

                                            Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveHTCLineDetails();" ),
                                            Helper.GenerateHidden(newRecordId, "HeaderId",Convert.ToString(param.Headerid)),
                                            "",

                                        };
                SourceHTC htc = new SourceHTC();
                var data = (from uc in lstResult
                            select new[]
                            {
                                GenerateTextboxFor(uc.HeaderId, "SectionDescription", Convert.ToString(uc.SectionDescription), "UpdateData(this, "+ uc.LineId +");","checkStatus("+ uc.HeaderId +");",((string.Equals(headerstatus,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)&& !isEditable)?false:true),"","100"),
                                GenerateAutoComplete(uc.HeaderId, "FurnaceCharge", Convert.ToString(uc.FurnaceCharge),"UpdateData(this, "+ uc.LineId +");",false,"","FurnaceCharge",((string.Equals(headerstatus,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)&& !isEditable)?false:true)),
                                //GenerateTextboxFor(uc.HeaderId, "FurnaceCharge", Convert.ToString(uc.FurnaceCharge),"UpdateData(this, "+ uc.LineId +");","checkStatus("+ uc.HeaderId +");"),
                                GenerateNumericTextboxFor(uc.HeaderId, "Length", Convert.ToString(uc.Length),"UpdateData(this, "+ uc.LineId +");","checkStatus("+ uc.HeaderId +");",((string.Equals(headerstatus,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)&& !isEditable)?false:true),"","10","numberonly"),
                                GenerateNumericTextboxFor(uc.HeaderId, "Width", Convert.ToString(uc.Width),"UpdateData(this, "+ uc.LineId +");","checkStatus("+ uc.HeaderId +");",((string.Equals(headerstatus,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)&& !isEditable)?false:true),"","10","numberonly"),
                                GenerateNumericTextboxFor(uc.HeaderId, "Height", Convert.ToString(uc.Height),"UpdateData(this, "+ uc.LineId +");","checkStatus("+ uc.HeaderId +");",((string.Equals(headerstatus,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)&& !isEditable)?false:true),"","10","numberonly"),
                                GenerateNumericTextboxFor(uc.HeaderId, "Weight", Convert.ToString(uc.Weight),"UpdateData(this, "+ uc.LineId +");","checkStatus("+ uc.HeaderId +");",((string.Equals(headerstatus,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)&& !isEditable)?false:true),"","10"),
                                GenerateAutoComplete(uc.HeaderId, "Furnace", Convert.ToString(uc.Furnace),"UpdateData(this, "+ uc.LineId +");",false,"","Furnace",((string.Equals(headerstatus,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)&& !isEditable)?false:true)),
                                Helper.GenerateActionIcon(uc.LineId,"LinkedHTR","Linked HTR","fa fa-fire-extinguisher", "LinkedHTR('"+ uc.HTRHeaderId+"','"+uc.IdenticalHTRHeaderIds+"')","",false),
                          //htc.htrLink(Convert.ToInt32(uc.HTRHeaderId)),
                               //Convert.ToString(uc.IsHTRCreated == true ? "Yes":"No"),
                                //GenerateTextboxFor(uc.HeaderId, "Furnace", Convert.ToString(uc.Furnace),"UpdateData(this, "+ uc.LineId +");","checkStatus("+ uc.HeaderId +");"),
                                (string.Equals(headerstatus,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) ? HTMLActionString(uc.HeaderId,headerstatus,"Delete","Delete Record","fa fa-trash-o","DeleteRecord("+ uc.LineId +");") : ""),
                                //HTMLActionString(uc.HeaderId,headerstatus,"Delete","Delete Record","fa fa-trash-o","DeleteRecord("+ uc.LineId +");"),
                                Convert.ToString(uc.HeaderId),
                                  Convert.ToString(uc.LineId) ,
                           }).ToList();

                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }



        public string GenerateAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false)
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' hdElement='" + hdElementId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + (disabled ? "disabled" : "") + " />";

            return strAutoComplete;
        }

        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (!string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
            {
                if (string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue()))
                {
                    htmlControl = "";// "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }
                else
                {
                    htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }
            }

            return htmlControl;
        }
        [HttpPost]
        public ActionResult UpdateDetails(int lineId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string tableName = "HTC002";
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    db.SP_COMMON_LINES_UPDATE(lineId, columnName, columnValue, tableName, objClsLoginInfo.UserName);
                    //ReviseData(headerId);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveHTCLineDetails(FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int newRowIndex = 0;
                HTC001 objHTC001 = new HTC001();
                HTC002 objHTC002 = new HTC002();
                int headerId = Convert.ToInt32(fc["HeaderId" + newRowIndex]);
                objHTC001 = db.HTC001.Where(x => x.HeaderId == headerId).FirstOrDefault();

                objHTC002.HeaderId = headerId;
                objHTC002.Project = objHTC001.Project;
                objHTC002.Document = objHTC001.Document;
                objHTC002.RevNo = objHTC001.RevNo;
                objHTC002.SectionDescription = fc["SectionDescription" + newRowIndex];
                objHTC002.FurnaceCharge = fc["FurnaceCharge" + newRowIndex];
                objHTC002.Height = string.IsNullOrWhiteSpace(fc["Height" + newRowIndex].ToString()) ? 0 : Convert.ToInt32(fc["Height" + newRowIndex]);
                objHTC002.Width = string.IsNullOrWhiteSpace(fc["Width" + newRowIndex].ToString()) ? 0 : Convert.ToInt32(fc["Width" + newRowIndex]); //Convert.ToInt32(fc["txtWidth" + newRowIndex]);
                objHTC002.Length = string.IsNullOrWhiteSpace(fc["Length" + newRowIndex].ToString()) ? 0 : Convert.ToInt32(fc["Length" + newRowIndex]); //Convert.ToInt32(fc["txtLength" + newRowIndex]);
                objHTC002.Weight = string.IsNullOrWhiteSpace(fc["Weight" + newRowIndex].ToString()) ? 0 : Convert.ToDecimal(fc["Weight" + newRowIndex]);//Convert.ToInt32(fc["txtWeight" + newRowIndex]);
                objHTC002.Furnace = fc["Furnace" + newRowIndex].ToString();
                objHTC002.IsHTRCreated = false;
                objHTC002.CreatedBy = objClsLoginInfo.UserName;
                objHTC002.CreatedOn = DateTime.Now;
                db.HTC002.Add(objHTC002);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReviseData(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                HTC001 objHTC001 = db.HTC001.Where(x => x.HeaderId == headerId).FirstOrDefault();
                if (objHTC001.Status == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue())
                {
                    objHTC001.RevNo = Convert.ToInt32(objHTC001.RevNo) + 1;
                    objHTC001.Status = clsImplementationEnum.UTCTQStatus.DRAFT.GetStringValue();
                    objHTC001.EditedBy = objClsLoginInfo.UserName;
                    objHTC001.EditedOn = DateTime.Now;
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult RevokeDocument(int HeaderId, int pdinId, string Remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (Manager.IsRevokeApplicable(pdinId, HeaderId, clsImplementationEnum.PlanList.Heat_Treatment_Charge_Sheet.GetStringValue()))
                {
                    objResponseMsg.Key = Manager.RevokePDINDocument(HeaderId, clsImplementationEnum.PlanList.Heat_Treatment_Charge_Sheet, Remarks);
                    if (objResponseMsg.Key)
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revoke;
                    }
                    else
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.UnSuccess;
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.pdnRevokeMessage;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public static string GenerateNumericTextboxFor(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string maxlength = "", string classname = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = !string.IsNullOrEmpty(classname) ? "form-control " + classname : "form-control numeric-only";
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            htmlControl = "<input type='text' " + (isReadOnly ? "disabled" : "") + " id='" + inputID + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' " + MaxCharcters + " style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + "  />";

            return htmlControl;
        }

        public static string GenerateTextboxFor(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string maxlength = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control";
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            htmlControl = "<input type='text' " + (isReadOnly ? "disabled" : "") + " id='" + inputID + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' " + MaxCharcters + " style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + "  />";

            return htmlControl;
        }


        [HttpPost]
        public ActionResult LoadHTCLineData_load(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                int headerid = Convert.ToInt32(param.Headerid);

                string strWhere = string.Empty;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (SectionDescription like '%" + param.sSearch
                        + "%' or FurnaceCharge like '%" + param.sSearch
                        + "%' or Height like '%" + param.sSearch
                        + "%' or Width like '%" + param.sSearch
                        + "%' or Length like '%" + param.sSearch
                        + "%' or Weight like '%" + param.sSearch
                        + "%' or Furnace like '%" + param.sSearch
                        + "%')";
                }
                else
                {
                    strWhere = "1=1";
                }
                var lstResult = db.SP_HTC_GET_LINEDETAILS
                                (
                                StartIndex, EndIndex, "", headerid, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                            {
                           Convert.ToString(uc.SectionDescription),
                           Convert.ToString(uc.FurnaceCharge),
                           Convert.ToString(uc.Length),
                           Convert.ToString(uc.Width),
                           Convert.ToString(uc.Height),
                           Convert.ToString(uc.Weight),
                           Convert.ToString(uc.Furnace),
                           Convert.ToString(uc.LineId),
                           Convert.ToString(uc.HeaderId),
                           Convert.ToString(uc.LineId)
                           }).ToList();


                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        public ActionResult DeleteHTCLine(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                HTC002 objHTC002 = db.HTC002.Where(x => x.LineId == Id).FirstOrDefault();
                db.HTC002.Remove(objHTC002);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateHTCLines(HTC002 htc002, FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int lineid = Convert.ToInt32(fc["hfLineid"].ToString());
                if (lineid > 0)
                {
                    HTC002 objHTC002 = db.HTC002.Where(x => x.LineId == lineid).FirstOrDefault();
                    objHTC002.HeaderId = Convert.ToInt32(fc["hfHeaderId"].ToString());
                    objHTC002.Project = fc["hfProject"].ToString();
                    objHTC002.Document = fc["hfdoc"].ToString();
                    objHTC002.RevNo = Convert.ToInt32(Regex.Replace(fc["hfRev"].ToString(), "[^0-9]+", string.Empty));
                    objHTC002.SectionDescription = fc["txtSecDesc"].ToString();
                    objHTC002.FurnaceCharge = fc["ddlFCharge"].ToString();
                    objHTC002.Height = string.IsNullOrWhiteSpace(fc["txtHeight"].ToString()) ? 0 : Convert.ToInt32(fc["txtHeight"]);
                    objHTC002.Width = string.IsNullOrWhiteSpace(fc["txtWidth"].ToString()) ? 0 : Convert.ToInt32(fc["txtWidth"]); //Convert.ToInt32(fc["txtWidth"]);
                    objHTC002.Length = string.IsNullOrWhiteSpace(fc["txtLength"].ToString()) ? 0 : Convert.ToInt32(fc["txtLength"]); //Convert.ToInt32(fc["txtLength"]);
                    objHTC002.Weight = string.IsNullOrWhiteSpace(fc["txtWeight"].ToString()) ? 0 : Convert.ToDecimal(fc["txtWeight"], CultureInfo.InvariantCulture);//Convert.ToInt32(fc["txtWeight"]);
                    objHTC002.Furnace = fc["ddlFType"].ToString();
                    objHTC002.EditedBy = objClsLoginInfo.UserName;
                    objHTC002.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult createRevison(int headerid)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int? revison = (from a in db.HTC001
                                where a.HeaderId == headerid
                                select a.RevNo).FirstOrDefault();
                revison = revison + 1;
                HTC001 objHTC001 = db.HTC001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                objHTC001.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                objHTC001.RevNo = revison;
                objHTC001.EditedBy = objClsLoginInfo.UserName;
                objHTC001.EditedOn = DateTime.Now;
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = revison.ToString();
                Manager.UpdatePDN002(objHTC001.HeaderId, objHTC001.Status, objHTC001.RevNo, objHTC001.Project, objHTC001.Document);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult getCodeValue(string project, string approver)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {

                objResponseMsg.projdesc = db.COM001.Where(i => i.t_cprj == project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objResponseMsg.appdesc = db.COM003.Where(i => i.t_psno == approver && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public string GetCodeAndNameByProject(string project)
        {
            var projdesc = db.COM001.Where(i => i.t_cprj == project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            return projdesc;
        }
        [HttpPost]
        public JsonResult LoadHTCHeaderHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;

                var HeaderId = param.CTQHeaderId;
                var role = param.CTQCompileStatus;

                string strWhere = string.Empty;
                strWhere += " 1=1 and HeaderId=" + HeaderId;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (Project like '%" + param.sSearch
                       + "%' or Document like '%" + param.sSearch
                       + "%' or Customer like '%" + param.sSearch
                       + "%' or RevNo like '%" + param.sSearch
                       + "%' or Product like '%" + param.sSearch
                       + "%' or ProcessLicensor like '%" + param.sSearch
                       + "%' or Status like '%" + param.sSearch + "%')";
                }
                var strSortOrder = string.Empty;
                var lstResult = db.SP_HTC_GET_HISTORY_HEADERDETAIL
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(GetCodeAndNameByProject(uc.Project)),
                               Convert.ToString(uc.Document),
                               Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Project)),
                             //  Convert.ToDateTime(uc.CDD).ToString("dd/MM/yyyy"),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                                Convert.ToString(uc.SubmittedBy),
                                Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),
                                Convert.ToString(uc.ApprovedBy),
                                Convert.ToString(Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy")),

                               Convert.ToString(uc.HeaderId),
                                Convert.ToString(db.HTC001_Log.FirstOrDefault(q => (q.HeaderId == uc.HeaderId && q.RevNo == uc.RevNo)).Id)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult GetHistoryView(int headerid = 0)
        {

            HTC001_Log objHTC001 = new HTC001_Log();
            // ViewBag.Role = strRole;
            ViewBag.HeaderId = headerid;
            Session["Headerid"] = Convert.ToInt32(headerid);
            //ViewBag.HeaderId = HeaderId;
            return PartialView("getHistoryPartial", objHTC001);
        }

        [SessionExpireFilter]
        public ActionResult getHistoryDetail(int Id = 0)
        {
            HTC001_Log objHTC001 = new HTC001_Log();
            objHTC001 = db.HTC001_Log.Where(x => x.Id == Id).FirstOrDefault();
            var user = objClsLoginInfo.UserName;
            if (objHTC001 != null)
            {
                var lstProjectDesc = (from a in db.COM001
                                      select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).ToList();
                ViewBag.Project = new SelectList(lstProjectDesc, "projectCode", "projectDescription");

                List<string> lstProCat = clsImplementationEnum.getProductCategory().ToList();
                ViewBag.lstProCat = lstProCat.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

                var approver = (from t1 in db.ATH001
                                join t2 in db.COM003 on t1.Employee equals t2.t_psno
                                where t2.t_actv == 1
                                select new { t1.Employee, Desc = t2.t_psno + " - " + t2.t_name }).Distinct().ToList();
                ViewBag.approver = new SelectList(approver, "Employee", "Desc");
                var project = (from a in db.HTC001
                               where a.HeaderId == Id
                               select a.Project).FirstOrDefault();

                ViewBag.Customer = Manager.GetCustomerCodeAndNameByProject(project);
            }
            var urlPrefix = Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.LastIndexOf('=') + 1);
            if (!objClsLoginInfo.GetUserRoleList().Contains("SHOP"))
            {
                ViewBag.RevPrev = (db.HTC001_Log.Any(q => (q.HeaderId == objHTC001.HeaderId && q.RevNo == (objHTC001.RevNo - 1))) ? urlPrefix + db.HTC001_Log.Where(q => (q.HeaderId == objHTC001.HeaderId && q.RevNo == (objHTC001.RevNo - 1))).FirstOrDefault().Id : null);
                ViewBag.RevNext = (db.HTC001_Log.Any(q => (q.HeaderId == objHTC001.HeaderId && q.RevNo == (objHTC001.RevNo + 1))) ? urlPrefix + db.HTC001_Log.Where(q => (q.HeaderId == objHTC001.HeaderId && q.RevNo == (objHTC001.RevNo + 1))).FirstOrDefault().Id : null);
            }
            return View(objHTC001);
        }

        [HttpPost]
        public ActionResult LoadHTCLineHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                //int headerid = Convert.ToInt32(Session["Headerid"]);
                int headerid = Convert.ToInt32(Request["HeaderId"]);

                string strWhere = string.Empty;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (SectionDescription like '%" + param.sSearch
                        + "%' or FurnaceCharge like '%" + param.sSearch
                        + "%' or Height like '%" + param.sSearch
                        + "%' or Width like '%" + param.sSearch
                        + "%' or Length like '%" + param.sSearch
                        + "%' or Weight like '%" + param.sSearch
                        + "%' or Furnace like '%" + param.sSearch
                        + "%')";
                }
                else
                {
                    strWhere = "1=1";
                }
                var strSortOrder = string.Empty;
                var lstResult = db.SP_HTC_GET_HISTORY_LINEDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, headerid, strWhere
                                ).ToList();
                SourceHTC htc = new SourceHTC();
                var data = (from uc in lstResult
                            select new[]
                            {
                           Convert.ToString(uc.SectionDescription),
                           Convert.ToString(uc.FurnaceCharge),
                           Convert.ToString(uc.Length),
                           Convert.ToString(uc.Width),
                           Convert.ToString(uc.Height),
                           Convert.ToString(uc.Weight),
                           Convert.ToString(uc.Furnace),
                           Helper.GenerateActionIcon(uc.LineId,"LinkedHTR","Linked HTR","fa fa-fire-extinguisher", "LinkedHTR('"+ uc.HTRHeaderId+"','"+uc.IdenticalHTRHeaderIds+"')","",false),

                             //htc.htrLink(Convert.ToInt32(uc.HTRHeaderId)),
                           }).ToList();


                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }
    }

}