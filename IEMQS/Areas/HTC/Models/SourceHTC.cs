﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IEMQSImplementation;

namespace IEMQS.Areas.HTC.Models
{
    public class SourceHTC
    {
        public HTC001 SourceHeader { get; set; }
        public List<HTC002> SourceCategories { get; set; }

        public string htrLink(int id)
        {
            string htmlControl = "";
            if (id > 0)
            {
                IEMQSEntitiesContext db = new IEMQSEntitiesContext();
                var objhtr = db.HTR001.Where(x => x.HeaderId == id).FirstOrDefault();
                if (objhtr != null)
                {
                    htmlControl = "<a href=\"" + clsBase.WebsiteURL + "/HTR/Maintain/AddHeader?HeaderID=" + id + "\" name=\"documentLink\">" + objhtr.HTRNo + " R" + objhtr.RevNo + "</a>";
                }
                else
                {
                    htmlControl = "";
                }
            }
            else
            {
                htmlControl = "";
            }

            return htmlControl;
        }
    }



}