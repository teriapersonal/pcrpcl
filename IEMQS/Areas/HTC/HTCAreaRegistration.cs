﻿using System.Web.Mvc;

namespace IEMQS.Areas.HTC
{
    public class HTCAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "HTC";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "HTC_default",
                "HTC/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}