﻿using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.HTR.Controllers
{
    public class ReleaseController : clsBase
    {
        /// <summary>
        /// </summary>
        /// <returns></returns>

        #region Index
        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadHTRHeaderDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetHeaderGridDataPartial");
        }

        [HttpPost]
        public JsonResult LoadHTRHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                strWhere += " 1=1 and status in('" + clsImplementationEnum.HTRStatus.Release.GetStringValue() + "')";
                string[] columnName = { "Project", "HTRNo", "HTRDescription", "Department", "Treatment", "Location", "ConcertoMSP", "Furnace", "RevNo", "Status", "CreatedBy", "EditedBy", " (Select top 1 STUFF((select distinct ', '+ PartNo from htr002 where HeaderId = htr.HeaderId FOR XML PATH(''), TYPE ).value('.', 'NVARCHAR(MAX)') ,1,1,''))" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);

                var lstResult = db.SP_HTR_HEADER_GETDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                           Convert.ToString(uc.Project),
                            Convert.ToString(uc.HTRNo),
                            Convert.ToString(uc.PartDescription),
                            Convert.ToString("R" + uc.RevNo),
                            uc.HTRDescription,
                            Convert.ToString(uc.Location),
                            Convert.ToString(uc.Status),
                            Convert.ToString(uc.CreatedOn.Value!=null ?uc.CreatedOn.Value.ToString("dd/MM/yyyy"):string.Empty),

                            Convert.ToString( uc.Department),
                            Convert.ToString( uc.ConcertoMSP),
                            Convert.ToString(uc.Treatment),
                            Convert.ToString(uc.Furnace),
                            Convert.ToString(uc.CreatedBy),
                            Convert.ToString(uc.EditedBy),
                            Convert.ToString(uc.ProjectDesc),
                            "<nobr><center>"+
                            "<a class='iconspace'  title='View Details' href='"+WebsiteURL+"/HTR/Release/HTRDetails?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i class='fa fa-eye'></i></a>"+
                            "<i class='iconspace fa fa-clock-o'  title='Show Timeline' href='javascript:void(0)' onclick=ShowTimeline('/HTR/Release/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')></i>"+
                             ((string.Equals(uc.Status, clsImplementationEnum.HTRStatus.Release.GetStringValue())) ? "<i title=\"Print Report\" class=\"iconspace fa fa-print\" onClick=\"PrintReport(" + uc.HeaderId+ ")\"></i> " : "<i title=\"Print Report\" class=\"disabledicon fa fa-print\" ></i> ")+
                            "<i id='History' name='History' style='cursor: pointer;' title='History Record' class='iconspace fa fa-history' onclick='ViewHistoryData("+ Convert.ToInt32(uc.HeaderId) +");'></i>"+
                              "</center></nobr>",
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.TimelineTitle = "HTR History";
            model.Title = "HTR";
            HTR001 objHTR001 = db.HTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            model.CreatedBy = Manager.GetUserNameFromPsNo(objHTR001.CreatedBy);
            model.CreatedOn = objHTR001.CreatedOn;
            model.ReviewedBy = Manager.GetUserNameFromPsNo(objHTR001.MET);
            model.ReviewedOn = objHTR001.ReviewdOn;
            model.FurnaceBy = Manager.GetUserNameFromPsNo(objHTR001.FurnaceSupervisor);
            model.FurnaceOn = objHTR001.FurnaceOn;
            model.InspectBy = Manager.GetUserNameFromPsNo(objHTR001.Inspector);
            model.InspectOn = objHTR001.InspectOn;
            model.SubmittedBy = Manager.GetUserNameFromPsNo(objHTR001.SubmittedBy);
            model.SubmittedOn = objHTR001.SubmittedOn;
            if (objHTR001.HTRType == clsImplementationEnum.TypeOfHTR.JobComponent.GetStringValue())
            {
                model.HTRType = objHTR001.HTRType;
            }
            else if (objHTR001.HTRType == clsImplementationEnum.TypeOfHTR.PTC.GetStringValue())
            {
                model.HTRType = objHTR001.HTRType;
            }
            else
            {
                model.HTRType = objHTR001.HTRType;
                model.HTRpqrStatus = objHTR001.Status;
            }

            return PartialView("_HTRTimelineProgress", model);
        }
        #endregion

        #region View Details
        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult HTRDetails(string headerID)
        {
            int HeaderId = 0;
            if (!string.IsNullOrWhiteSpace(headerID))
            {
                HeaderId = Convert.ToInt32(headerID);
            }
            HTR001 objHTR001 = db.HTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            if (objHTR001 != null)
            {
                ViewBag.Project = db.COM001.Where(i => i.t_cprj == objHTR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objHTR001.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objHTR001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objHTR001.Department = db.COM002.Where(a => a.t_dimx == objHTR001.Department).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                string strInspector = db.COM003.Where(x => x.t_psno == objHTR001.Inspector).Select(x => x.t_name).FirstOrDefault();
                string strMET = db.COM003.Where(x => x.t_psno == objHTR001.MET).Select(x => x.t_name).FirstOrDefault();
                string strSupervisor = db.COM003.Where(x => x.t_psno == objHTR001.FurnaceSupervisor).Select(x => x.t_name).FirstOrDefault();
                ViewBag.Inspector = objHTR001.Inspector + " - " + strInspector;
                ViewBag.MET = objHTR001.MET + " - " + strMET;
                ViewBag.FurnaceSupervisor = objHTR001.FurnaceSupervisor + " - " + strSupervisor;

                ViewBag.Action = "edit";
            }
            return View(objHTR001);
        }

        [HttpPost]
        public ActionResult LoadHTRCheckList(int HeaderId)
        {
            HTR001 objHTR001 = new HTR001();
            if (HeaderId > 0)
            {
                objHTR001 = db.HTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            }
            return PartialView("~/Areas/HTR/Views/Shared/_LoadHTRCheckListPartial.cshtml", objHTR001);
        }

        [HttpPost]
        public ActionResult GetHTRLinesViewForm(int HeaderId, int LineId, string project)
        {
            HTR001 objHTR001 = new HTR001();
            HTR002 objHTR002 = new HTR002();
            objHTR001 = db.HTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            if (objHTR001 != null)
            {
                string buCode = db.COM001.Where(i => i.t_cprj == objHTR001.Project).FirstOrDefault().t_entu;
                var InspectionAgency = Manager.GetSubCatagories("Inspection Agency", buCode, objClsLoginInfo.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
                //List<GLB002> lstCTGLB002 = Manager.GetSubCatagories("Inspection Agency", buCode, objClsLoginInfo.Location).ToList();
                //ViewBag.Agency = lstCTGLB002.Count();
                // lstCTGLB002.AsEnumerable().Select(x => new SelectListItem() { Text = (x.Code + "-" + x.Description).ToString(), Value = x.Code.ToString() }).ToList();
                ViewBag.Agency = InspectionAgency.Any() ? InspectionAgency : null;


                if (InspectionAgency.Count > 0)
                    ViewBag.InspectionAgency = InspectionAgency;
                else
                {
                    List<string> lstCategory = new List<string>();
                    if (!InspectionAgency.Any())
                        lstCategory.Add("Inspection Agency");
                    ViewBag.Agency = null;
                    ViewBag.Value = string.Join(",", lstCategory.ToList()) + " Category not exist for selected project/Quality Project, Please contact Admin";
                }
            }

            if (LineId > 0)
            {
                objHTR002 = db.HTR002.Where(x => x.LineId == LineId).FirstOrDefault();

                ViewBag.Action = "viewline";
            }
            else
            {
                objHTR002.RevNo = 0;
            }

            ViewBag.LineProject = project;//db.COM001.Where(i => i.t_cprj == objHTR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            ViewBag.HTRNo = objHTR001.HTRNo;
            ViewBag.RevNo = objHTR001.RevNo;
            objHTR002.HeaderId = objHTR001.HeaderId;
            return PartialView("~/Areas/HTR/Views/Shared/_LoadHTRLinesPartial.cshtml", objHTR002);
        }


        [HttpPost]
        public JsonResult LoadHTRLineGridData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "";
                strWhere += "1=1 and a.HeaderId =" + param.CTQHeaderId;
                //string[] arrayCTQStatus = { clsImplementationEnum.CTQStatus.DRAFT.GetStringValue(), clsImplementationEnum.CTQStatus.Returned.GetStringValue() };
                string[] columnName = { "SeamNo", "PartNo", "HeatNo", "DrawingNo", "DrawingRev", "HTPReference", "PTCMTCPQR", "Description", "MaterialSpec", "SizeThickness", "Quantity", "TotalWeight", "InspectionAgency", "MaterialFrom", "MaterialTo", "ICSBy", "ProjectNumber" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_HTR_LINE_GETDETAILS
                                (
                               StartIndex,
                               EndIndex,
                               strSortOrder,
                               strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.ROW_NO),
                            Convert.ToString(uc.LineId),
                            Convert.ToString(uc.HeaderId),
                            Convert.ToString(uc.SeamNo),
                            Convert.ToString(uc.PartNo),
                            Convert.ToString(uc.HeatNo),
                            Convert.ToString(uc.DrawingNo),
                            Convert.ToString(uc.DrawingRev),
                            Convert.ToString(uc.HTPReference),
                            Convert.ToString(uc.PTCMTCPQR),
                            Convert.ToString(uc.Description),
                            Convert.ToString(uc.MaterialSpec),
                            Convert.ToString(uc.SizeThickness),
                            Convert.ToString(uc.Quantity),
                            Convert.ToString(uc.TotalWeight),
                            Convert.ToString(uc.InspectionAgency),
                            Convert.ToString(uc.MaterialFrom),
                            Convert.ToString(uc.MaterialTo),
                            Convert.ToString(uc.ICSBy),
                            Convert.ToString(uc.ProjectNumber),
                            Convert.ToString("R"+uc.RevNo),
                            "<center>"+ "<i class='fa fa-plus'  id='btnComponentDetail' name='ComponentDetail' onclick='AddComponentDetail(" + uc.LineId + ")'> </i> <i class='fa fa-upload' id='UploadFile' onclick='ShowPopUp("+uc.LineId+")'></i>",
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Export Excell
        //Code By : Ajay Chauhan on 09/11/2017, Export Funxtionality
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_HTR_HEADER_GETDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      HTRNo = li.HTRNo,
                                      RevNo = li.RevNo,
                                      Description = li.HTRDescription,
                                      Department = li.Department,
                                      Location = li.Location,
                                      ConcertoMSP = li.ConcertoMSP,
                                      Treatment = li.Treatment,
                                      Furnace = li.Furnace,
                                      Status = li.Status,
                                      CreatedBy = li.CreatedBy,
                                      EditedBy = li.EditedBy,
                                      CreatedOn = li.CreatedOn
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_HTR_LINE_GETDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from li in lst
                                  select new
                                  {
                                      SeamNo = li.SeamNo,
                                      PartNo = li.PartNo,
                                      HeatNo = li.HeatNo,
                                      DrawingNo = li.DrawingNo,
                                      DrawingRev = li.DrawingRev,
                                      HTPReference = li.HTPReference,
                                      PTCMTCPQR = li.PTCMTCPQR,
                                      Description = li.Description,
                                      MaterialSpec = li.MaterialSpec,
                                      SizeThickness = li.SizeThickness,
                                      Quantity = li.Quantity,
                                      TotalWeight = li.TotalWeight,
                                      InspectionAgency = li.InspectionAgency,
                                      MaterialFrom = li.MaterialFrom,
                                      MaterialTo = li.MaterialTo,
                                      ICSBy = li.ICSBy,
                                      ProjectNumber = li.ProjectNumber,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        //pop up
        [HttpPost]
        public ActionResult getChangeSet(int Id) //
        {
            ViewBag.header = Id;
            return PartialView("~/Areas/HTR/Views/Shared/_HTRChangeSet.cshtml");
        }
        public class ResponceMsgWithHeaderID : clsHelper.ResponseMsg
        {
            public int HeaderId;
        }

    }
}
