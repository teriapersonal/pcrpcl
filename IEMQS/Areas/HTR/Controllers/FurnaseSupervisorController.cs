﻿using IEMQS.Areas.Utility.Controllers;
using IEMQS.Areas.Utility.Models;
using IEMQS.DESServices;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.HTR.Controllers
{
    public class FurnaseSupervisorController : clsBase
    {
        // GET: HTR/FurnaseSupervisor
        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]

        #region Index Page
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadHTRHeaderDataPartial(string status, string title)
        {
            ViewBag.Status = status;
            ViewBag.Title = title;
            return PartialView("_GetHeaderGridDataPartial");
        }

        [HttpPost]
        public JsonResult LoadHTRHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += " 1=1 and status in('" + clsImplementationEnum.HTRStatus.WithFurnaceSupervisor.GetStringValue() + "')";
                    strWhere += "and FurnaceSupervisor=" + objClsLoginInfo.UserName;
                }
                else
                {
                    strWhere += " 1=1";
                    strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "BU", "Location");
                }
                string[] columnName = { "Project", "HTRNo", "HTRDescription", "Department", "Treatment", "Location", "ConcertoMSP", "Furnace", "RevNo", "Status", "CreatedBy", "EditedBy", " (Select top 1 STUFF((select distinct ', '+ PartNo from htr002 where HeaderId = htr.HeaderId FOR XML PATH(''), TYPE ).value('.', 'NVARCHAR(MAX)') ,1,1,''))" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_HTR_HEADER_GETDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.Project),
                            Convert.ToString(uc.HTRNo),
                            Convert.ToString(uc.PartDescription),
                            Convert.ToString("R" + uc.RevNo),
                            uc.HTRDescription,
                            Convert.ToString(uc.Location),
                            Convert.ToString(uc.Status),
                            Convert.ToString(uc.CreatedOn.Value!=null ?uc.CreatedOn.Value.ToString("dd/MM/yyyy"):string.Empty),

                            Convert.ToString( uc.Department),
                            Convert.ToString( uc.ConcertoMSP),
                            Convert.ToString(uc.Treatment),
                            Convert.ToString(uc.Furnace),
                            Convert.ToString(uc.CreatedBy),
                            Convert.ToString(uc.EditedBy),
                            Convert.ToString(uc.ProjectDesc),

                            "<nobr><center>"+
                            "<a class='iconspace' title='View Details' href='"+WebsiteURL +"/HTR/FurnaseSupervisor/HTRDetails?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i class='fa fa-eye'></i></a>"+
                            "<i class='iconspace fa fa-clock-o' title='Show Timeline' href='javascript:void(0)' onclick=ShowTimeline('/HTR/FurnaseSupervisor/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')></i>"+
                             ((string.Equals(uc.Status, clsImplementationEnum.HTRStatus.Release.GetStringValue())) ? "<i title=\"Print Report\" class=\"iconspace fa fa-print\" onClick=\"PrintReport(" + uc.HeaderId+ ")\"></i> " : "<i title=\"Print Report\" class=\"disabledicon fa fa-print\" ></i> ")+
                            "<i id='History' name='History' style='cursor: pointer;' title='History Record' class='iconspace fa fa-history' onclick='ViewHistoryData("+ Convert.ToInt32(uc.HeaderId) +");'></i>"+
                            "</center></nobr>",
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                     strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.TimelineTitle = "HTR History";
            model.Title = "HTR";
            HTR001 objHTR001 = db.HTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            model.CreatedBy = Manager.GetUserNameFromPsNo(objHTR001.CreatedBy);
            model.CreatedOn = objHTR001.CreatedOn;
            model.ReviewedBy = Manager.GetUserNameFromPsNo(objHTR001.MET);
            model.ReviewedOn = objHTR001.ReviewdOn;
            model.FurnaceBy = Manager.GetUserNameFromPsNo(objHTR001.FurnaceSupervisor);
            model.FurnaceOn = objHTR001.FurnaceOn;
            model.InspectBy = Manager.GetUserNameFromPsNo(objHTR001.Inspector);
            model.InspectOn = objHTR001.InspectOn;
            model.SubmittedBy = Manager.GetUserNameFromPsNo(objHTR001.SubmittedBy);
            model.SubmittedOn = objHTR001.SubmittedOn;
            if (objHTR001.HTRType == clsImplementationEnum.TypeOfHTR.JobComponent.GetStringValue())
            {
                model.HTRType = objHTR001.HTRType;
            }
            else if (objHTR001.HTRType == clsImplementationEnum.TypeOfHTR.JobComponentNuclear.GetStringValue())
            {
                model.HTRType = objHTR001.HTRType;
            }
            else if (objHTR001.HTRType == clsImplementationEnum.TypeOfHTR.PTC.GetStringValue())
            {
                model.HTRType = objHTR001.HTRType;
            }
            else
            {
                model.HTRType = objHTR001.HTRType;
                model.HTRpqrStatus = objHTR001.Status;
            }

            return PartialView("_HTRTimelineProgress", model);
        }

        #endregion

        #region View Header
        [SessionExpireFilter]
        [AllowAnonymous]
        //[UserPermissions]
        public ActionResult HTRDetails(string headerID)
        {
            int HeaderId = 0;
            if (!string.IsNullOrWhiteSpace(headerID))
            {
                HeaderId = Convert.ToInt32(headerID);
            }
            HTR001 objHTR001 = db.HTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            if (objHTR001 != null)
            {
                ViewBag.Project = db.COM001.Where(i => i.t_cprj == objHTR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objHTR001.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objHTR001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objHTR001.Department = db.COM002.Where(a => a.t_dimx == objHTR001.Department).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                string strInspector = db.COM003.Where(x => x.t_psno == objHTR001.Inspector && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                string strMET = db.COM003.Where(x => x.t_psno == objHTR001.MET && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                string strSupervisor = db.COM003.Where(x => x.t_psno == objHTR001.FurnaceSupervisor && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                ViewBag.Inspector = objHTR001.Inspector + " - " + strInspector;
                ViewBag.MET = objHTR001.MET + " - " + strMET;
                ViewBag.FurnaceSupervisor = objHTR001.FurnaceSupervisor + " - " + strSupervisor;
                ViewBag.MinLoadingTemp = clsImplementationEnum.GetMinLoadingTemp().ToArray();
                ViewBag.MaxLoadingTemp = clsImplementationEnum.GetMaxLoadingTemp().ToArray();
                ViewBag.MinRateOfHeating = clsImplementationEnum.GetMinRateOfHeating().ToArray();
                ViewBag.MaxRateOfHeating = clsImplementationEnum.GetMaxRateOfHeating().ToArray();
                ViewBag.MinRateOfCooling = clsImplementationEnum.GetMinRateOfCooling().ToArray();
                ViewBag.MaxRateOfCooling = clsImplementationEnum.GetMaxRateOfCooling().ToArray();
                ViewBag.MinUnloadingTemp = clsImplementationEnum.GetMinUnloadingTemp().ToArray();
                ViewBag.MaxUnloadingTemp = clsImplementationEnum.GetMaxUnloadingTemp().ToArray();
                ViewBag.CoolPart = clsImplementationEnum.GetCoolPart().ToArray();
                ViewBag.CoolBy = clsImplementationEnum.GetCoolBy().ToArray();
                ViewBag.VolTempOf = clsImplementationEnum.GetVolTempOf().ToArray();
                ViewBag.Furnace = clsImplementationEnum.GetFurnace().ToArray();
                ViewBag.StandardProcedure = clsImplementationEnum.GetStandardProcedure().ToArray();
                ViewBag.HeatingThermocouplesVariation = clsImplementationEnum.GetHeatingThermocouplesVariation().ToArray();
                ViewBag.CoolingThermocouplesVariation = clsImplementationEnum.GetCoolingThermocouplesVariation().ToArray();
                ViewBag.CoolingThermocouplesVariation = clsImplementationEnum.GetCoolingThermocouplesVariation().ToArray();
                ViewBag.WPSNo = clsImplementationEnum.GetWPSNo().ToArray();
                if (objHTR001.HTRType == clsImplementationEnum.TypeOfHTR.JobComponent.GetStringValue() || objHTR001.HTRType == clsImplementationEnum.TypeOfHTR.JobComponentNuclear.GetStringValue())
                {
                    ViewBag.Demote = clsImplementationEnum.GetJCPTCfurnase().ToArray();
                }
                if (objHTR001.FurnaceSupervisor != null ? (objHTR001.FurnaceSupervisor.Trim() != objClsLoginInfo.UserName.Trim()) : true)
                {
                    ViewBag.Access = "noaccess";
                }
                ViewBag.Action = "edit";
            }
            return View(objHTR001);
        }

        [HttpPost]
        public ActionResult SaveHeader(HTR001 htr001, FormCollection fc, HttpPostedFileBase fileScanPlan)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                HTR001 objHTR001 = new HTR001();
                if (htr001.HeaderId > 0)
                {
                    objHTR001 = db.HTR001.Where(x => x.HeaderId == htr001.HeaderId).FirstOrDefault();
                }
                objHTR001.MinLoadingTemp = htr001.MinLoadingTemp;
                objHTR001.MaxLoadingTemp = htr001.MaxLoadingTemp;
                objHTR001.MinRateOfHeating = htr001.MinRateOfHeating;
                objHTR001.MaxRateOfHeating = htr001.MaxRateOfHeating;
                objHTR001.MinSoakingTemperature = htr001.MinSoakingTemperature;
                objHTR001.MaxSoakingTemperature = htr001.MaxSoakingTemperature;
                objHTR001.SoakingTimeHH = htr001.SoakingTimeHH;
                objHTR001.SoakingTimeMM = htr001.SoakingTimeMM;
                objHTR001.MinRateOfCooling = htr001.MinRateOfCooling;
                objHTR001.MaxRateOfCooling = htr001.MaxRateOfCooling;
                objHTR001.MinUnloadingTemp = htr001.MinUnloadingTemp;
                objHTR001.MaxUnloadingTemp = htr001.MaxUnloadingTemp;
                objHTR001.CoolPart = htr001.CoolPart;
                objHTR001.CoolBy = htr001.CoolBy;
                objHTR001.MaxDelay = htr001.MaxDelay;
                objHTR001.VolTempOf = htr001.VolTempOf;
                objHTR001.TempOfLiquidAfterQuenching = htr001.TempOfLiquidAfterQuenching;
                objHTR001.MinVol = htr001.MinVol;
                objHTR001.HeatingThermocouplesVariation = htr001.HeatingThermocouplesVariation;
                objHTR001.CoolingThermocouplesVariation = htr001.CoolingThermocouplesVariation;
                objHTR001.StandardProcedure = htr001.StandardProcedure;
                objHTR001.WPSNo = htr001.WPSNo;
                objHTR001.Comments = htr001.Comments;

                if (htr001.HeaderId > 0)
                {

                    if (objHTR001.Status == clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue())
                    {
                        objHTR001.RevNo = Convert.ToInt32(objHTR001.RevNo) + 1;
                        objHTR001.Status = clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue();
                    }
                    objHTR001.EditedBy = objClsLoginInfo.UserName;
                    objHTR001.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    //HTR003 objHTR003 = new HTR003();
                    //var LinkedProject = fc["LinkedProject"].Split('-')[0];
                    //List<HTR003> lstHTR003 = db.HTR003.Where(x => x.HeaderId == objHTR001.HeaderId).ToList();
                    //if (lstHTR003 != null && lstHTR003.Count > 0)
                    //{
                    //    db.HTR003.RemoveRange(lstHTR003);
                    //}
                    //db.SaveChanges();
                    //foreach (var item in LinkedProject)
                    //{
                    //    try
                    //    {
                    //        List<HTR003> lstSrcHTR003 = db.HTR003.Where(x => x.HeaderId == objHTR001.HeaderId).ToList();
                    //        db.HTR003.AddRange(
                    //                            lstSrcHTR003.Select(x =>
                    //                            new HTR003
                    //                            {
                    //                                HeaderId = objHTR001.HeaderId,
                    //                                Project = objHTR001.Project,
                    //                                HTRNo = objHTR001.HTRNo,
                    //                                RevNo = objHTR001.RevNo,
                    //                                LinkedProject = item.ToString(),
                    //                                CreatedBy = objClsLoginInfo.UserName,
                    //                                CreatedOn = DateTime.Now,
                    //                            })
                    //                          );
                    //        db.SaveChanges();
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    //        objResponseMsg.Key = false;
                    //        objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
                    //    }
                    //}
                    objResponseMsg.HeaderId = objHTR001.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PTMTCTQMessages.Update.ToString();
                }
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult SaveAttachment(int headerId, int revNo, bool hasLSAttachments, Dictionary<string, string> lsAttach, bool ISPromoted)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                var folderLoadingSketchPath = "HTR001/LoadingSketch/" + headerId + "/R" + revNo;

                Manager.ManageDocuments(folderLoadingSketchPath, hasLSAttachments, lsAttach, objClsLoginInfo.UserName);

                objResponseMsg.HeaderId = headerId;
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.PTMTCTQMessages.Update.ToString();
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult LoadHTRCheckList(int HeaderId)
        {
            HTR001 objHTR001 = new HTR001();
            if (HeaderId > 0)
            {
                objHTR001 = db.HTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            }
            return PartialView("~/Areas/HTR/Views/Shared/_LoadHTRCheckListPartial.cshtml", objHTR001);
        }
        [HttpPost]
        public ActionResult GetHTRLinesViewForm(int HeaderId, int LineId, string project)
        {
            HTR001 objHTR001 = new HTR001();
            HTR002 objHTR002 = new HTR002();
            objHTR001 = db.HTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            if (objHTR001 != null)
            {
                string buCode = db.COM001.Where(i => i.t_cprj == objHTR001.Project).FirstOrDefault().t_entu;
                var InspectionAgency = Manager.GetSubCatagories("Inspection Agency", buCode, objClsLoginInfo.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
                //List<GLB002> lstCTGLB002 = Manager.GetSubCatagories("Inspection Agency", buCode, objClsLoginInfo.Location).ToList();
                //ViewBag.Agency = lstCTGLB002.Count();
                // lstCTGLB002.AsEnumerable().Select(x => new SelectListItem() { Text = (x.Code + "-" + x.Description).ToString(), Value = x.Code.ToString() }).ToList();
                ViewBag.Agency = InspectionAgency.Any() ? InspectionAgency : null;


                if (InspectionAgency.Count > 0)
                    ViewBag.InspectionAgency = InspectionAgency;
                else
                {
                    List<string> lstCategory = new List<string>();
                    if (!InspectionAgency.Any())
                        lstCategory.Add("Inspection Agency");
                    ViewBag.Agency = null;
                    ViewBag.Value = string.Join(",", lstCategory.ToList()) + " Category not exist for selected project/Quality Project, Please contact Admin";
                }
            }

            if (LineId > 0)
            {
                objHTR002 = db.HTR002.Where(x => x.LineId == LineId).FirstOrDefault();

                ViewBag.Action = "viewline";
            }
            else
            {
                objHTR002.RevNo = 0;
            }

            ViewBag.LineProject = project;//db.COM001.Where(i => i.t_cprj == objHTR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            ViewBag.HTRNo = objHTR001.HTRNo;
            ViewBag.RevNo = objHTR001.RevNo;
            objHTR002.HeaderId = objHTR001.HeaderId;
            return PartialView("~/Areas/HTR/Views/Shared/_LoadHTRLinesPartial.cshtml", objHTR002);
        }


        [HttpPost]
        public JsonResult LoadHTRLineGridData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                string headerid = param.CTQHeaderId;
                strWhere = " (1=1 and a.HeaderId IN(" + headerid + "))";
                //string[] arrayCTQStatus = { clsImplementationEnum.CTQStatus.DRAFT.GetStringValue(), clsImplementationEnum.CTQStatus.Returned.GetStringValue() };
                string[] columnName = { "SeamNo", "PartNo", "HeatNo", "DrawingNo", "DrawingRev", "HTPReference", "PTCMTCPQR", "Description", "MaterialSpec", "SizeThickness", "Quantity", "TotalWeight", "InspectionAgency", "MaterialFrom", "MaterialTo", "ICSBy", "ProjectNumber" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch).Replace("RevNo", "a.RevNo");
                else
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter).Replace("RevNo", "a.RevNo");

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_HTR_LINE_GETDETAILS
                                (
                               StartIndex,
                               EndIndex,
                               strSortOrder,
                               strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.ROW_NO),
                            Convert.ToString(uc.LineId),
                            Convert.ToString(uc.HeaderId),
                            Convert.ToString(uc.SeamNo),
                            Convert.ToString(uc.PartNo),
                            Convert.ToString(uc.HeatNo),
                            Convert.ToString(uc.DrawingNo),
                            Convert.ToString(uc.DrawingRev),
                            Convert.ToString(uc.HTPReference),
                            Convert.ToString(uc.PTCMTCPQR),
                            Convert.ToString(uc.Description),
                            Convert.ToString(uc.MaterialSpec),
                            Convert.ToString(uc.SizeThickness),
                            Convert.ToString(uc.Quantity),
                            Convert.ToString(uc.TotalWeight),
                            Convert.ToString(uc.InspectionAgency),
                            Convert.ToString(uc.MaterialFrom),
                            Convert.ToString(uc.MaterialTo),
                            Convert.ToString(uc.ICSBy),
                            Convert.ToString(uc.ProjectNumber),
                            Convert.ToString("R"+uc.RevNo),
                            "<center>"+ "<i class='fa fa-plus'  id='btnComponentDetail' name='ComponentDetail' onclick='AddComponentDetail(" + uc.LineId + ")'> </i><i class='fa fa-upload' id='UploadFile' onclick='ShowPopUp("+uc.LineId+")'></i>",


                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Promote Demote
        [HttpPost]
        public ActionResult SendForPromote(int strHeaderId, string remark, string ProjectCode)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                string status = "",HeaderIdDetail=string.Empty;
                HTR001 objHTR001 = db.HTR001.Where(u => u.HeaderId == strHeaderId).SingleOrDefault();
                if (objHTR001 != null)
                {
                    if (objHTR001.HTRType == clsImplementationEnum.TypeOfHTR.JobComponent.GetStringValue())
                        status = clsImplementationEnum.HTRStatus.Release.GetStringValue();
                    if (objHTR001.HTRType == clsImplementationEnum.TypeOfHTR.JobComponentNuclear.GetStringValue())
                        status = clsImplementationEnum.HTRStatus.WithInspector.GetStringValue();

                    db.SP_HTR_PROMOTE_DEMOTE(strHeaderId, remark, status, objClsLoginInfo.UserName, "SUPERVISOR");
                    (new clsManager()).ScurveHTC(objHTR001.Project, objHTR001.Location);
                    objResponseMsg.ActionKey = true;
                    objResponseMsg.ActionValue = string.Format(clsImplementationMessage.CommonMessages.Promote, objHTR001.HTRNo);


                    //bhadresh start
                    string IdenticalProjects = (from i in db.HTR001 where i.HeaderId == strHeaderId select i.IdenticalProject).FirstOrDefault();
                    if (IdenticalProjects != null)
                    {
                        if (objHTR001.RevNo == 0)
                        {
                            string[] IdentProjs = IdenticalProjects.Split(',');
                            if (IdentProjs.Length > 0)
                            {
                                for (int ii = 0; ii < IdentProjs.Length; ii++)
                                {

                                    HTR001 obj_HTR001 = new HTR001();
                                    obj_HTR001.DONo = objHTR001.DONo;
                                    obj_HTR001.PoNo = objHTR001.PoNo;
                                    obj_HTR001.IdenticalProject = objHTR001.IdenticalProject;
                                    obj_HTR001.ConcertoMSP = objHTR001.ConcertoMSP;
                                    obj_HTR001.Treatment = objHTR001.Treatment;
                                    obj_HTR001.HTRReferenceNo = objHTR001.HTRReferenceNo;
                                    obj_HTR001.Furnace = objHTR001.Furnace;
                                    obj_HTR001.HTRDescription = objHTR001.HTRDescription;
                                    obj_HTR001.FurnaceCommentsRequired = objHTR001.FurnaceCommentsRequired;
                                    obj_HTR001.FurnaceSupervisor = objHTR001.FurnaceSupervisor;
                                    obj_HTR001.InspectionCommentsRequired = objHTR001.InspectionCommentsRequired;
                                    obj_HTR001.Inspector = objHTR001.Inspector;
                                    obj_HTR001.MET = objHTR001.MET;
                                    obj_HTR001.MinLoadingTemp = objHTR001.MinLoadingTemp;
                                    obj_HTR001.MaxLoadingTemp = objHTR001.MaxLoadingTemp;
                                    obj_HTR001.MinRateOfHeating = objHTR001.MinRateOfHeating;
                                    obj_HTR001.MaxRateOfHeating = objHTR001.MaxRateOfHeating;
                                    obj_HTR001.MinSoakingTemperature = objHTR001.MinSoakingTemperature;
                                    obj_HTR001.MaxSoakingTemperature = objHTR001.MaxSoakingTemperature;
                                    obj_HTR001.SoakingTimeHH = objHTR001.SoakingTimeHH;
                                    obj_HTR001.SoakingTimeMM = objHTR001.SoakingTimeMM;
                                    obj_HTR001.SoakingTimeTolerance = objHTR001.SoakingTimeTolerance;
                                    obj_HTR001.MinRateOfCooling = objHTR001.MinRateOfCooling;
                                    obj_HTR001.MaxRateOfCooling = objHTR001.MaxRateOfCooling;
                                    obj_HTR001.MinUnloadingTemp = objHTR001.MinUnloadingTemp;
                                    obj_HTR001.MaxUnloadingTemp = objHTR001.MaxUnloadingTemp;
                                    obj_HTR001.CoolPart = objHTR001.CoolPart;
                                    obj_HTR001.CoolBy = objHTR001.CoolBy;
                                    obj_HTR001.MaxDelay = objHTR001.MaxDelay;
                                    obj_HTR001.VolTempOf = objHTR001.VolTempOf;
                                    obj_HTR001.TempOfLiquidAfterQuenching = objHTR001.TempOfLiquidAfterQuenching;
                                    obj_HTR001.MinVol = objHTR001.MinVol;
                                    obj_HTR001.HeatingThermocouplesVariation = objHTR001.HeatingThermocouplesVariation;
                                    obj_HTR001.CoolingThermocouplesVariation = objHTR001.CoolingThermocouplesVariation;
                                    obj_HTR001.StandardProcedure = objHTR001.StandardProcedure;
                                    obj_HTR001.FurnaceBedDistance = objHTR001.FurnaceBedDistance;
                                    obj_HTR001.WPSNo = objHTR001.WPSNo;
                                    obj_HTR001.Comments = objHTR001.Comments;
                                    obj_HTR001.Status = objHTR001.Status;


                                    obj_HTR001.HTRType = objHTR001.HTRType;
                                    obj_HTR001.Project = IdentProjs[ii].Split('/')[0].Trim();
                                    obj_HTR001.Location = objHTR001.Location.Trim(); //.Split('-')[0];
                                    obj_HTR001.Department = objHTR001.Department.Split('-')[0].Trim();
                                    obj_HTR001.BU = db.COM001.Where(i => i.t_cprj == objHTR001.Project.Trim()).FirstOrDefault().t_entu.Trim();
                                    obj_HTR001.HTRNo = GetNumber(IdentProjs[ii].Split('/')[0].Trim());
                                    obj_HTR001.RevNo = objHTR001.RevNo;
                                    obj_HTR001.DocNo = Convert.ToInt32(obj_HTR001.HTRNo.Split('/')[2]);
                                    //obj_HTR001.Status = clsImplementationEnum.HTRStatus.Draft.GetStringValue();
                                    obj_HTR001.CreatedBy = objHTR001.CreatedBy;
                                    obj_HTR001.CreatedOn = DateTime.Now;
                                    obj_HTR001.SubmittedBy = objHTR001.SubmittedBy;
                                    obj_HTR001.SubmittedOn = objHTR001.SubmittedOn;
                                    obj_HTR001.MET = objHTR001.MET;
                                    obj_HTR001.ReviewdOn = objHTR001.ReviewdOn;
                                    obj_HTR001.FurnaceSupervisor = objHTR001.FurnaceSupervisor;
                                    obj_HTR001.FurnaceOn = objHTR001.FurnaceOn;
                                    obj_HTR001.Inspector = objHTR001.Inspector;
                                    obj_HTR001.InspectOn = objHTR001.InspectOn;
                                    obj_HTR001.FurnaceComments = objHTR001.FurnaceComments;
                                    obj_HTR001.ReviewerComments = objHTR001.ReviewerComments;
                                    obj_HTR001.InspectorComments = objHTR001.InspectorComments;
                                    obj_HTR001.chkl1 = objHTR001.chkl1;
                                    obj_HTR001.chkl2 = objHTR001.chkl2;
                                    obj_HTR001.chkl3 = objHTR001.chkl3;
                                    obj_HTR001.chkl4 = objHTR001.chkl4;
                                    obj_HTR001.chkl5 = objHTR001.chkl5;
                                    obj_HTR001.chkl6 = objHTR001.chkl6;
                                    obj_HTR001.chkl7 = objHTR001.chkl7;
                                    obj_HTR001.chkl8 = objHTR001.chkl8;
                                    obj_HTR001.chkl9 = objHTR001.chkl9;
                                    obj_HTR001.chkl10 = objHTR001.chkl10;
                                    obj_HTR001.chkl11 = objHTR001.chkl11;
                                    obj_HTR001.chkl12 = objHTR001.chkl12;
                                    obj_HTR001.chkl13 = objHTR001.chkl13;
                                    obj_HTR001.chkl14 = objHTR001.chkl14;
                                    obj_HTR001.chkl15 = objHTR001.chkl15;
                                    obj_HTR001.chkl16 = objHTR001.chkl16;
                                    obj_HTR001.chkl17 = objHTR001.chkl17;
                                    obj_HTR001.chkl18 = objHTR001.chkl18;
                                    obj_HTR001.chkl19 = objHTR001.chkl19;
                                    obj_HTR001.chkl20 = objHTR001.chkl20;
                                    obj_HTR001.chkl21 = objHTR001.chkl21;
                                    obj_HTR001.chkl22 = objHTR001.chkl22;
                                    obj_HTR001.chkl23 = objHTR001.chkl23;
                                    obj_HTR001.chkl24 = objHTR001.chkl24;
                                    db.HTR001.Add(obj_HTR001);
                                    db.SaveChanges();
                                    db.SP_HTR_PROMOTE_DEMOTE(obj_HTR001.HeaderId, remark, status, objClsLoginInfo.UserName, "SUPERVISOR");
                                    (new clsManager()).ScurveHTC(objHTR001.Project, objHTR001.Location);
                                    var folderPath = "HTR001/" + obj_HTR001.HeaderId + "/1";
                                    var oldFolderPath = "HTR001/" + objHTR001.HeaderId + "/1";
                                    FileUploadController _objFUC = new FileUploadController();
                                    _objFUC.CopyDataOnFCSServerAsync(oldFolderPath, folderPath, "HTR001", objHTR001.HeaderId, "HTR001", obj_HTR001.HeaderId, CommonService.GetUseIPConfig, CommonService.objClsLoginInfo.UserName, 0, true);
                                    // HeaderIdDetail += obj_HTR001.HeaderId;// obj_HTR001.HTRNo.Substring(4, obj_HTR001.HTRNo.Length).Replace("/","") + "$";
                                    List<HTR002> lstobjHtr002 = db.HTR002.Where(x => x.HeaderId == strHeaderId).ToList();
                                    int iindex = 0;
                                    foreach (var objHtr002 in lstobjHtr002)
                                    {
                                        iindex = iindex + 1;
                                        HTR002 obj_HTR002 = new HTR002();
                                        obj_HTR002.HeaderId = obj_HTR001.HeaderId;
                                        obj_HTR002.Project = obj_HTR001.Project.Trim();
                                        obj_HTR002.HTRNo = obj_HTR001.HTRNo.Trim();
                                        obj_HTR002.RevNo = objHtr002.RevNo;
                                        obj_HTR002.HTRLineNo = objHtr002.HTRLineNo;
                                        obj_HTR002.SeamNo = objHtr002.SeamNo;
                                        obj_HTR002.PartNo = objHtr002.PartNo;
                                        obj_HTR002.HeatNo = objHtr002.HeatNo;
                                        obj_HTR002.DrawingNo = objHtr002.DrawingNo;
                                        obj_HTR002.DrawingRev = objHtr002.DrawingRev;
                                        obj_HTR002.HTPReference = objHtr002.HTPReference;
                                        obj_HTR002.PTCMTCPQR = objHtr002.PTCMTCPQR;
                                        obj_HTR002.Description = objHtr002.Description;
                                        obj_HTR002.MaterialSpec = objHtr002.MaterialSpec;
                                        obj_HTR002.SizeThickness = objHtr002.SizeThickness;
                                        obj_HTR002.Quantity = objHtr002.Quantity;
                                        obj_HTR002.TotalWeight = objHtr002.TotalWeight;
                                        obj_HTR002.InspectionAgency = objHtr002.InspectionAgency;
                                        obj_HTR002.MaterialFrom = objHtr002.MaterialFrom;
                                        obj_HTR002.MaterialTo = objHtr002.MaterialTo;
                                        obj_HTR002.ICSBy = objHtr002.ICSBy;
                                        obj_HTR002.SubHTRNo = obj_HTR001.HTRNo + "/" + iindex;
                                        obj_HTR002.ProjectNumber = objHtr002.ProjectNumber.Trim();
                                        obj_HTR002.CreatedBy = objHtr002.CreatedBy;
                                        obj_HTR002.CreatedOn = DateTime.Now;
                                        db.HTR002.Add(obj_HTR002);
                                        db.SaveChanges();
                                        // HeaderIdDetail +=  obj_HTR002.LineId + "-" + objHtr002.LineId + "#";
                                        var folderPath1 = "HTR002/" + obj_HTR002.LineId + "/1";
                                        var oldFolderPath1 = "HTR002/" + objHtr002.LineId + "/1";
                                        FileUploadController _objFUC1 = new FileUploadController();
                                        _objFUC1.CopyDataOnFCSServerAsync(oldFolderPath1, folderPath1, "HTR002", objHtr002.LineId, "HTR002", obj_HTR002.LineId, CommonService.GetUseIPConfig, CommonService.objClsLoginInfo.UserName, 0, true);
                                        //var folderPath2 = "HTR002/" + obj_HTR002.LineId + "/1";
                                        //var oldFolderPath2 = "HTR002/" + obj_HTR002.LineId + "/1";
                                        //(new clsFileUpload()).CopyFolderContentsAsync(oldFolderPath2, folderPath2);
                                        List<HTR005> lstobjHtr005 = db.HTR005.Where(x => x.HTR2LineId == objHtr002.LineId).ToList();
                                        foreach (var objHtr005 in lstobjHtr005)
                                        {
                                            HTR005 obj_HTR005 = new HTR005();
                                            obj_HTR005.HeaderId = obj_HTR001.HeaderId;
                                            obj_HTR005.Project = obj_HTR001.Project.Trim();
                                            obj_HTR005.HTRNo = obj_HTR001.HTRNo.Trim();
                                            obj_HTR005.RevNo = obj_HTR001.RevNo;
                                            obj_HTR005.HTRLineNo = objHtr005.HTRLineNo;
                                            obj_HTR005.SeamNo = objHtr005.SeamNo;
                                            obj_HTR005.HTR2LineId = obj_HTR002.LineId;// (from m in db.HTR002 where m.LineId == objHtr005.LineId select m.LineId).FirstOrDefault();
                                            obj_HTR005.HTRLineNo = objHtr005.HTRLineNo;
                                            obj_HTR005.PartNo = objHtr005.PartNo;
                                            obj_HTR005.HeatNo = objHtr005.HeatNo;
                                            obj_HTR005.DrawingNo = objHtr005.DrawingNo;
                                            obj_HTR005.DrawingRev = objHtr005.DrawingRev;
                                            obj_HTR005.HTPReference = objHtr005.HTPReference;
                                            obj_HTR005.PTCMTCPQR = objHtr005.PTCMTCPQR;
                                            obj_HTR005.Description = objHtr005.Description;
                                            obj_HTR005.MaterialSpec = objHtr005.MaterialSpec;
                                            obj_HTR005.SizeThickness = objHtr005.SizeThickness;
                                            obj_HTR005.Quantity = objHtr005.Quantity;
                                            obj_HTR005.TotalWeight = objHtr005.TotalWeight;
                                            obj_HTR005.InspectionAgency = objHtr005.InspectionAgency;
                                            obj_HTR005.MaterialFrom = objHtr005.MaterialFrom;
                                            obj_HTR005.MaterialTo = objHtr005.MaterialTo;
                                            obj_HTR005.ICSBy = objHtr005.ICSBy;
                                            obj_HTR005.SubHTRNo = obj_HTR002.SubHTRNo.Trim();
                                            obj_HTR005.ProjectNumber = objHtr005.ProjectNumber;
                                            obj_HTR005.CreatedBy = objHtr005.CreatedBy;
                                            obj_HTR005.CreatedOn = DateTime.Now;
                                            db.HTR005.Add(obj_HTR005);
                                            db.SaveChanges();
                                            // HeaderIdDetail +=  obj_HTR002.LineId + "-" + objHtr002.LineId + "#";
                                            var folderPath11 = "HTR005/" + obj_HTR005.LineId + "/1";
                                            var oldFolderPath11 = "HTR005/" + objHtr005.LineId + "/1";
                                            FileUploadController _objFUC11 = new FileUploadController();
                                            _objFUC11.CopyDataOnFCSServerAsync(oldFolderPath11, folderPath11, "HTR005", objHtr005.LineId, "HTR005", obj_HTR005.LineId, CommonService.GetUseIPConfig, CommonService.objClsLoginInfo.UserName, 0, true);
                                            //var folderPath2 = "HTR002/" + obj_HTR002.LineId + "/1";
                                            //var oldFolderPath2 = "HTR002/" + obj_HTR002.LineId + "/1";
                                            //(new clsFileUpload()).CopyFolderContentsAsync(oldFolderPath2, folderPath2);
                                        }
                                    }

                                    //var folderPath = "HTR001/LoadingSketch/" + strHeaderId + "/R" + obj_HTR001.RevNo;
                                    //var oldFolderPath = "HTR001/LoadingSketch/" + obj_HTR001.HeaderId + "/R" + obj_HTR001.RevNo;
                                    //(new clsFileUpload()).CopyFolderContentsAsync(oldFolderPath, folderPath);
                                    //var HTRDocuments = (new clsFileUpload()).GetDocuments(oldFolderPath).Select(s => s.Name);
                                    //try
                                    //{
                                    //    // ----- To check After file copied all files are availabled in destination -----//
                                    //    var newHTRDocuments = (new clsFileUpload()).GetDocuments(folderPath).Select(s => s.Name);
                                    //    List<string> fileNotFound = new List<string>(), fileFound = new List<string>();
                                    //    foreach (var item in HTRDocuments)
                                    //    {
                                    //        if (newHTRDocuments.Contains(item))
                                    //            fileFound.Add(item);
                                    //        else
                                    //            fileNotFound.Add(item);
                                    //    }
                                    //    objResponseMsg.ActionValue = "Copied Files [" + String.Join(" | ", fileFound) + "] ; Not Found [" + String.Join(" | ", fileNotFound) + "]";
                                    //    // ---------------------------------------- //
                                    //    Utility.Controllers.FileUploadController.FileUpload_WriteContentOnPDF(String.Join(",", HTRDocuments), folderPath, obj_HTR001.HTRNo + " (R" + obj_HTR001.RevNo + ")", 0, 0, 90, 0, "bottomleft");
                                    //    Utility.Controllers.FileUploadController.FileUpload_WriteContentOnPDF(String.Join(",", HTRDocuments), folderPath, obj_HTR001.HTRNo + " (R" + obj_HTR001.RevNo + ")", 0, 0, 0, 0, "topright");
                                    //}
                                    //catch (Exception ex)
                                    //{
                                    //    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                                    //    objResponseMsg.Key = false;
                                    //    objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
                                    //}
                                }

                            }
                        }
                    }
                   // objResponseMsg.ActionValue = HeaderIdDetail;

                    //bhadresh end

                }
                else
                {
                    objResponseMsg.ActionKey = true;
                    objResponseMsg.ActionValue = "Details not available.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.ActionKey = false;
                objResponseMsg.ActionValue = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        //bhadresh start
        public string GetNumber(string code)
        {   //generate HTR number
            if (!string.IsNullOrWhiteSpace(code))
            {

                HTR001 objHTR001 = new HTR001();
                NDE.Controllers.MaintainScanUTTechController.BUWiseDropdown objProjectDataModel = new NDE.Controllers.MaintainScanUTTechController.BUWiseDropdown();

                var lstProject = (from a in db.COM001
                                  where a.t_cprj == code
                                  select new { ProjectDesc = a.t_cprj + " - " + a.t_dsca }).FirstOrDefault();

                var HTRNum = MakeAHTR(code);
                objProjectDataModel.TechNo = HTRNum;

                return HTRNum;
            }
            else
            {
                return "";
            }
        }
        public string MakeAHTR(string code)
        {
            var lstobjHTR001 = db.HTR001.Where(x => x.Project == code).ToList();
            string HTRNumber = "";
            string AHTRNo = string.Empty;

            if (lstobjHTR001 != null && lstobjHTR001.Count() != 0)
            {
                var HTRNum = (from a in db.HTR001
                              where a.Project.Equals(code, StringComparison.OrdinalIgnoreCase)
                              select a).Max(a => a.DocNo);
                HTRNumber = (HTRNum + 1).ToString();
            }
            else
            {
                HTRNumber = "1";
            }
            AHTRNo = "HTR/" + code + "/" + HTRNumber.PadLeft(3, '0');
            return AHTRNo;
        }
        //bhadresh end
        [HttpPost]
        public ActionResult SendForDemote(int strHeaderId, string status, string remark)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                HTR001 objHTR001 = db.HTR001.Where(u => u.HeaderId == strHeaderId).SingleOrDefault();
                if (objHTR001 != null)
                {
                    db.SP_HTR_PROMOTE_DEMOTE(strHeaderId, remark, status, objClsLoginInfo.UserName, "SUPERVISOR");
                    objResponseMsg.ActionKey = true;
                    objResponseMsg.ActionValue = "Details has been successfully Demoted.";
                }
                else
                {
                    objResponseMsg.ActionKey = true;
                    objResponseMsg.ActionValue = "Details not available.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.ActionKey = false;
                objResponseMsg.ActionValue = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        //pop up
        [HttpPost]
        public ActionResult getChangeSet(int Id) //
        {
            ViewBag.header = Id;
            return PartialView("~/Areas/HTR/Views/Shared/_HTRChangeSet.cshtml");
        }


        public class ResponceMsgWithHeaderID : clsHelper.ResponseMsg
        {
            public int HeaderId;
            public bool ActionKey;
            public string ActionValue;
        }


        public JsonResult GetProjectsEdit(int? headerId)
        {
            try
            {
                var lst = Manager.getProjectsByUserAutocomplete(objClsLoginInfo.UserName, "").ToList();
                var item = from li in lst
                           select new
                           {
                               id = li.projectCode,
                               text = li.projectDescription
                           };
                return Json(item, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetLinkedProjectValue(int headerId)
        {
            var result = db.HTR003.Where(s => s.HeaderId == headerId).Select(s => s.LinkedProject).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}