﻿using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.HTR.Controllers
{
    public class ReviewController : clsBase
    {
        /// <summary>
        /// </summary>
        /// <returns></returns>

        #region Index
        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult LoadHTRHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += " 1=1 and status in('" + clsImplementationEnum.HTRStatus.InReview.GetStringValue() + "')";
                    strWhere += "and MET=" + objClsLoginInfo.UserName;
                }
                else
                {
                    strWhere += " 1=1";
                    strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "BU", "Location");
                }
                string[] columnName = { "Project", "HTRNo", "HTRDescription", "Department", "Treatment", "Location", "ConcertoMSP", "Furnace", "RevNo", "Status", "CreatedBy", "EditedBy", " (Select top 1 STUFF((select distinct ', '+ PartNo from htr002 where HeaderId = htr.HeaderId FOR XML PATH(''), TYPE ).value('.', 'NVARCHAR(MAX)') ,1,1,''))" };
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);

                var lstResult = db.SP_HTR_HEADER_GETDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                           Convert.ToString(uc.Project),
                            Convert.ToString(uc.HTRNo),
                            Convert.ToString(uc.PartDescription),
                            Convert.ToString("R" + uc.RevNo),
                            uc.HTRDescription,
                            Convert.ToString(uc.Location),
                            Convert.ToString(uc.Status),
                            Convert.ToString(uc.CreatedOn.Value!=null ?uc.CreatedOn.Value.ToString("dd/MM/yyyy"):string.Empty),

                            Convert.ToString( uc.Department),
                            Convert.ToString( uc.ConcertoMSP),
                            Convert.ToString(uc.Treatment),
                            Convert.ToString(uc.Furnace),
                            Convert.ToString(uc.CreatedBy),
                            Convert.ToString(uc.EditedBy),
                            Convert.ToString(uc.ProjectDesc),

                            "<nobr><center>"+
                            "<a class='iconspace' title='View Details' href='"+WebsiteURL+"/HTR/Review/HTRDetails?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i class='fa fa-eye'></i></a>"+
                            "<i class='iconspace fa fa-clock-o'  title='Show Timeline' href='javascript:void(0)' onclick=ShowTimeline('/HTR/Review/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')></i>"+
                            ((string.Equals(uc.Status, clsImplementationEnum.HTRStatus.Release.GetStringValue())) ? "<i title=\"Print Report\" class=\"iconspace fa fa-print\" onClick=\"PrintReport(" + uc.HeaderId+ ")\"></i> " : "<i title=\"Print Report\" class=\"disabledicon fa fa-print\" ></i> ")+
                            "<i id='History' name='History' title='History Record' class='iconspace fa fa-history' onclick='ViewHistoryData("+ Convert.ToInt32(uc.HeaderId) +");'></i>"+
                             "</center></nobr>",
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.TimelineTitle = "HTR History";
            model.Title = "HTR";
            HTR001 objHTR001 = db.HTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            model.CreatedBy = Manager.GetUserNameFromPsNo(objHTR001.CreatedBy);
            model.CreatedOn = objHTR001.CreatedOn;
            model.ReviewedBy = Manager.GetUserNameFromPsNo(objHTR001.MET);
            model.ReviewedOn = objHTR001.ReviewdOn;
            model.FurnaceBy = Manager.GetUserNameFromPsNo(objHTR001.FurnaceSupervisor);
            model.FurnaceOn = objHTR001.FurnaceOn;
            model.InspectBy = Manager.GetUserNameFromPsNo(objHTR001.Inspector);
            model.InspectOn = objHTR001.InspectOn;
            model.SubmittedBy = Manager.GetUserNameFromPsNo(objHTR001.SubmittedBy);
            model.SubmittedOn = objHTR001.SubmittedOn;
            if (objHTR001.HTRType == clsImplementationEnum.TypeOfHTR.JobComponent.GetStringValue())
            {
                model.HTRType = objHTR001.HTRType;

            }
            else if (objHTR001.HTRType == clsImplementationEnum.TypeOfHTR.JobComponentNuclear.GetStringValue())
            {
                model.HTRType = objHTR001.HTRType;

            }
            else if (objHTR001.HTRType == clsImplementationEnum.TypeOfHTR.PTC.GetStringValue())
            {
                model.HTRType = objHTR001.HTRType;

            }
            else
            {
                model.HTRType = objHTR001.HTRType;
                model.HTRpqrStatus = objHTR001.Status;
            }

            return PartialView("_HTRTimelineProgress", model);
        }

        [HttpPost]
        public ActionResult LoadHTRHeaderDataPartial(string status, string title)
        {
            ViewBag.Status = status;
            ViewBag.Title = title;
            return PartialView("_GetHeaderGridDataPartial");
        }
        #endregion

        [HttpPost]
        public ActionResult GetHTRLinesViewForm(int HeaderId, int LineId, string project)
        {
            HTR001 objHTR001 = new HTR001();
            HTR002 objHTR002 = new HTR002();
            objHTR001 = db.HTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            if (objHTR001 != null)
            {
                string buCode = db.COM001.Where(i => i.t_cprj == objHTR001.Project).FirstOrDefault().t_entu;
                var InspectionAgency = Manager.GetSubCatagories("Inspection Agency", buCode, objClsLoginInfo.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
                //List<GLB002> lstCTGLB002 = Manager.GetSubCatagories("Inspection Agency", buCode, objClsLoginInfo.Location).ToList();
                //ViewBag.Agency = lstCTGLB002.Count();
                // lstCTGLB002.AsEnumerable().Select(x => new SelectListItem() { Text = (x.Code + "-" + x.Description).ToString(), Value = x.Code.ToString() }).ToList();
                ViewBag.Agency = InspectionAgency.Any() ? InspectionAgency : null;


                if (InspectionAgency.Count > 0)
                    ViewBag.InspectionAgency = InspectionAgency;
                else
                {
                    List<string> lstCategory = new List<string>();
                    if (!InspectionAgency.Any())
                        lstCategory.Add("Inspection Agency");
                    ViewBag.Agency = null;
                    ViewBag.Value = string.Join(",", lstCategory.ToList()) + " Category not exist for selected project/Quality Project, Please contact Admin";
                }
            }

            if (LineId > 0)
            {
                objHTR002 = db.HTR002.Where(x => x.LineId == LineId).FirstOrDefault();

                ViewBag.Action = "viewline";
            }
            else
            {
                objHTR002.RevNo = 0;
            }

            ViewBag.LineProject = project;//db.COM001.Where(i => i.t_cprj == objHTR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            ViewBag.HTRNo = objHTR001.HTRNo;
            ViewBag.RevNo = objHTR001.RevNo;
            objHTR002.HeaderId = objHTR001.HeaderId;
            return PartialView("~/Areas/HTR/Views/Shared/_LoadHTRLinesPartial.cshtml", objHTR002);
        }

        #region Header
        [SessionExpireFilter]
        [AllowAnonymous]
        //[UserPermissions]
        public ActionResult HTRDetails(string headerID)
        {
            int HeaderId = 0;
            if (!string.IsNullOrWhiteSpace(headerID))
            {
                HeaderId = Convert.ToInt32(headerID);
            }
            HTR001 objHTR001 = db.HTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            if (objHTR001 != null)
            {
                ViewBag.Project = db.COM001.Where(i => i.t_cprj == objHTR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objHTR001.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objHTR001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objHTR001.Department = db.COM002.Where(a => a.t_dimx == objHTR001.Department).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                string strInspector = db.COM003.Where(x => x.t_psno == objHTR001.Inspector).Select(x => x.t_name).FirstOrDefault();
                string strMET = db.COM003.Where(x => x.t_psno == objHTR001.MET).Select(x => x.t_name).FirstOrDefault();
                string strSupervisor = db.COM003.Where(x => x.t_psno == objHTR001.FurnaceSupervisor).Select(x => x.t_name).FirstOrDefault();
                ViewBag.Inspector = objHTR001.Inspector + " - " + strInspector;
                ViewBag.MET = objHTR001.MET + " - " + strMET;
                ViewBag.FurnaceSupervisor = objHTR001.FurnaceSupervisor + " - " + strSupervisor;
                ViewBag.TypeOfHTR = clsImplementationEnum.GetTypeOfHTR().ToArray();
                ViewBag.Treatment = clsImplementationEnum.GetTreatment().ToArray();
                ViewBag.Furnace = clsImplementationEnum.GetFurnace().ToArray();
                ViewBag.MinLoadingTemp = clsImplementationEnum.GetMinLoadingTemp().ToArray();
                ViewBag.MaxLoadingTemp = clsImplementationEnum.GetMaxLoadingTemp().ToArray();
                ViewBag.MinRateOfHeating = clsImplementationEnum.GetMinRateOfHeating().ToArray();
                ViewBag.MaxRateOfHeating = clsImplementationEnum.GetMaxRateOfHeating().ToArray();
                ViewBag.SoakingTimeTolerance = clsImplementationEnum.GetSoakingTimeTolerance().ToArray();
                ViewBag.MinRateOfCooling = clsImplementationEnum.GetMinRateOfCooling().ToArray();
                ViewBag.MaxRateOfCooling = clsImplementationEnum.GetMaxRateOfCooling().ToArray();
                ViewBag.MinUnloadingTemp = clsImplementationEnum.GetMinUnloadingTemp().ToArray();
                ViewBag.MaxUnloadingTemp = clsImplementationEnum.GetMaxUnloadingTemp().ToArray();
                ViewBag.CoolPart = clsImplementationEnum.GetCoolPart().ToArray();
                ViewBag.CoolBy = clsImplementationEnum.GetCoolBy().ToArray();
                ViewBag.VolTempOf = clsImplementationEnum.GetVolTempOf().ToArray();
                ViewBag.HeatingThermocouplesVariation = clsImplementationEnum.GetHeatingThermocouplesVariation().ToArray();
                ViewBag.CoolingThermocouplesVariation = clsImplementationEnum.GetCoolingThermocouplesVariation().ToArray();
                ViewBag.CoolingThermocouplesVariation = clsImplementationEnum.GetCoolingThermocouplesVariation().ToArray();
                ViewBag.FurnaceBedDistance = clsImplementationEnum.GetFurnaceBedDistance().ToArray();
                ViewBag.StandardProcedure = clsImplementationEnum.GetStandardProcedure().ToArray();
                ViewBag.WPSNo = clsImplementationEnum.GetWPSNo().ToArray();
                ViewBag.Demote = clsImplementationEnum.GetInitiator().ToArray();
                ViewBag.demotecomments = objHTR001.Comments;
                if (objHTR001.MET != null ? objHTR001.MET.Trim() != objClsLoginInfo.UserName.Trim() : true)
                {
                    ViewBag.Access = "noaccess";
                }
                ViewBag.Action = "edit";
            }
            return View(objHTR001);
        }

        //editable grid for HTR Components/Line
        [HttpPost]
        public ActionResult LoadLinesFormPartial(int HeaderId)
        {
            HTR001 objHTR001 = new HTR001();

            objHTR001 = db.HTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            if (objHTR001 != null)
            {
                if (objHTR001.MET == objClsLoginInfo.UserName)
                {
                    ViewBag.Department = clsImplementationEnum.HTRDepartment.METReviewer.GetStringValue();
                }
                else
                {
                    ViewBag.Department = string.Empty;
                }
                //objHTR002.HeaderId = objHTR001.HeaderId;
            }
            else
            {
                ViewBag.Department = string.Empty;
                objHTR001.HeaderId = HeaderId;
            }
            return PartialView("~/Areas/HTR/Views/Shared/_LoadHTRLinesPartial.cshtml", objHTR001);
        }
        //[HttpPost]
        //public ActionResult SaveHeader(HTR001 htr001, FormCollection fc, HttpPostedFileBase fileScanPlan)
        //{
        //    ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
        //    try
        //    {
        //        HTR001 objHTR001 = new HTR001();
        //        if (htr001.HeaderId > 0)
        //        {
        //            objHTR001 = db.HTR001.Where(x => x.HeaderId == htr001.HeaderId).FirstOrDefault();
        //        }
        //        objHTR001.MinLoadingTemp = htr001.MinLoadingTemp;
        //        objHTR001.MaxLoadingTemp = htr001.MaxLoadingTemp;
        //        objHTR001.MinRateOfHeating = htr001.MinRateOfHeating;
        //        objHTR001.MaxRateOfHeating = htr001.MaxRateOfHeating;
        //        objHTR001.MinSoakingTemperature = htr001.MinSoakingTemperature;
        //        objHTR001.MaxSoakingTemperature = htr001.MaxSoakingTemperature;
        //        objHTR001.SoakingTimeHH = htr001.SoakingTimeHH;
        //        objHTR001.SoakingTimeMM = htr001.SoakingTimeMM;
        //        objHTR001.MinRateOfCooling = htr001.MinRateOfCooling;
        //        objHTR001.MaxRateOfCooling = htr001.MaxRateOfCooling;
        //        objHTR001.MinUnloadingTemp = htr001.MinUnloadingTemp;
        //        objHTR001.MaxUnloadingTemp = htr001.MaxUnloadingTemp;
        //        objHTR001.CoolPart = htr001.CoolPart;
        //        objHTR001.CoolBy = htr001.CoolBy;
        //        objHTR001.MaxDelay = htr001.MaxDelay;
        //        objHTR001.VolTempOf = htr001.VolTempOf;
        //        objHTR001.TempOfLiquidAfterQuenching = htr001.TempOfLiquidAfterQuenching;
        //        objHTR001.MinVol = htr001.MinVol;
        //        objHTR001.HeatingThermocouplesVariation = htr001.HeatingThermocouplesVariation;
        //        objHTR001.CoolingThermocouplesVariation = htr001.CoolingThermocouplesVariation;
        //        objHTR001.StandardProcedure = htr001.StandardProcedure;
        //        objHTR001.WPSNo = htr001.WPSNo;
        //        objHTR001.Comments = htr001.Comments;

        //        if (htr001.HeaderId > 0)
        //        {

        //            if (objHTR001.Status == clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue())
        //            {
        //                objHTR001.RevNo = Convert.ToInt32(objHTR001.RevNo) + 1;
        //                objHTR001.Status = clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue();
        //            }
        //            objHTR001.EditedBy = objClsLoginInfo.UserName;
        //            objHTR001.EditedOn = DateTime.Now;
        //            db.SaveChanges();
        //            //HTR003 objHTR003 = new HTR003();
        //            //var LinkedProject = fc["LinkedProject"].Split('-')[0];
        //            //List<HTR003> lstHTR003 = db.HTR003.Where(x => x.HeaderId == objHTR001.HeaderId).ToList();
        //            //if (lstHTR003 != null && lstHTR003.Count > 0)
        //            //{
        //            //    db.HTR003.RemoveRange(lstHTR003);
        //            //}
        //            //db.SaveChanges();
        //            //foreach (var item in LinkedProject)
        //            //{
        //            //    try
        //            //    {
        //            //        List<HTR003> lstSrcHTR003 = db.HTR003.Where(x => x.HeaderId == objHTR001.HeaderId).ToList();
        //            //        db.HTR003.AddRange(
        //            //                            lstSrcHTR003.Select(x =>
        //            //                            new HTR003
        //            //                            {
        //            //                                HeaderId = objHTR001.HeaderId,
        //            //                                Project = objHTR001.Project,
        //            //                                HTRNo = objHTR001.HTRNo,
        //            //                                RevNo = objHTR001.RevNo,
        //            //                                LinkedProject = item.ToString(),
        //            //                                CreatedBy = objClsLoginInfo.UserName,
        //            //                                CreatedOn = DateTime.Now,
        //            //                            })
        //            //                          );
        //            //        db.SaveChanges();
        //            //    }
        //            //}
        //            //catch (Exception ex)
        //            //{
        //            //    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //            //    objResponseMsg.Key = false;
        //            //    objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
        //            //}
        //        }
        //        objResponseMsg.HeaderId = objHTR001.HeaderId;
        //        objResponseMsg.Key = true;
        //        objResponseMsg.Value = clsImplementationMessage.PTMTCTQMessages.Update.ToString();
        //    }

        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        objResponseMsg.Key = false;
        //        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
        //    }
        //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        public ActionResult SaveHeader(HTR001 htr001, FormCollection fc, bool ISPromoted)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                HTR001 objHTR001 = new HTR001();
                if (htr001.HeaderId > 0)
                {
                    objHTR001 = db.HTR001.Where(x => x.HeaderId == htr001.HeaderId).FirstOrDefault();
                }

                objHTR001.DONo = htr001.DONo;
                objHTR001.PoNo = htr001.PoNo;
                objHTR001.ConcertoMSP = htr001.ConcertoMSP;
                objHTR001.Treatment = htr001.Treatment;
                objHTR001.HTRReferenceNo = htr001.HTRReferenceNo;
                objHTR001.Furnace = htr001.Furnace;
                objHTR001.IdenticalProject = htr001.IdenticalProject;

                //objHTR001.HTRDescription = htr001.HTRDescription;
                objHTR001.FurnaceCommentsRequired = htr001.FurnaceCommentsRequired;
                objHTR001.FurnaceSupervisor = htr001.FurnaceSupervisor;
                objHTR001.InspectionCommentsRequired = htr001.InspectionCommentsRequired;
                objHTR001.Inspector = htr001.Inspector;
                objHTR001.MET = htr001.MET;
                objHTR001.MinLoadingTemp = htr001.MinLoadingTemp;
                objHTR001.MaxLoadingTemp = htr001.MaxLoadingTemp;
                objHTR001.MinRateOfHeating = htr001.MinRateOfHeating;
                objHTR001.MaxRateOfHeating = htr001.MaxRateOfHeating;
                objHTR001.MinSoakingTemperature = htr001.MinSoakingTemperature;
                objHTR001.MaxSoakingTemperature = htr001.MaxSoakingTemperature;
                objHTR001.SoakingTimeHH = htr001.SoakingTimeHH;
                objHTR001.SoakingTimeMM = htr001.SoakingTimeMM;
                objHTR001.SoakingTimeTolerance = htr001.SoakingTimeTolerance;
                objHTR001.MinRateOfCooling = htr001.MinRateOfCooling;
                objHTR001.MaxRateOfCooling = htr001.MaxRateOfCooling;
                objHTR001.MinUnloadingTemp = htr001.MinUnloadingTemp;
                objHTR001.MaxUnloadingTemp = htr001.MaxUnloadingTemp;
                objHTR001.CoolPart = htr001.CoolPart;
                objHTR001.CoolBy = htr001.CoolBy;
                objHTR001.MaxDelay = htr001.MaxDelay;
                objHTR001.VolTempOf = htr001.VolTempOf;
                objHTR001.TempOfLiquidAfterQuenching = htr001.TempOfLiquidAfterQuenching;
                objHTR001.MinVol = htr001.MinVol;
                objHTR001.HeatingThermocouplesVariation = htr001.HeatingThermocouplesVariation;
                objHTR001.CoolingThermocouplesVariation = htr001.CoolingThermocouplesVariation;
                objHTR001.StandardProcedure = htr001.StandardProcedure;
                objHTR001.FurnaceBedDistance = htr001.FurnaceBedDistance;
                objHTR001.WPSNo = htr001.WPSNo;
                objHTR001.Comments = htr001.Comments;

                if (htr001.HeaderId > 0)
                {
                    objHTR001.EditedBy = objClsLoginInfo.UserName;
                    objHTR001.EditedOn = DateTime.Now;
                    db.SaveChanges();

                    #region code for Linked Project (removed on 24/10/2017 - as per requirements [observation no. 13507])
                    //HTR003 objHTR003 = new HTR003();
                    //List<HTR003> lstDesHTR003 = db.HTR003.Where(x => x.HeaderId == objHTR001.HeaderId).ToList();
                    //if (lstDesHTR003 != null && lstDesHTR003.Count > 0)
                    //{
                    //    db.HTR003.RemoveRange(lstDesHTR003);
                    //}
                    //var strLinkedProject = LinkedProject.Split('-')[0];
                    //string[] arrayLinkedProject = strLinkedProject.Split(',').ToArray();
                    //HTR003 newobjHTR003 = db.HTR003.Where(x => x.HeaderId == objHTR001.HeaderId).FirstOrDefault();
                    //List<HTR003> lstHTR003 = new List<HTR003>();
                    //for (int i = 0; i < arrayLinkedProject.Length; i++)
                    //{
                    //    HTR003 objHTR003Add = new HTR003();
                    //    objHTR003Add.HeaderId = objHTR001.HeaderId;
                    //    objHTR003Add.Project = objHTR001.Project;
                    //    objHTR003Add.HTRNo = objHTR001.HTRNo;
                    //    objHTR003Add.RevNo = objHTR001.RevNo;
                    //    objHTR003Add.LinkedProject = arrayLinkedProject[i];
                    //    objHTR003Add.CreatedBy = objClsLoginInfo.UserName;
                    //    objHTR003Add.CreatedOn = DateTime.Now;
                    //    lstHTR003.Add(objHTR003Add);

                    //}
                    //if (lstHTR003 != null && lstHTR003.Count > 0)
                    //{
                    //    db.HTR003.AddRange(lstHTR003);
                    //}
                    ////attachments
                    //var folderGeneralNotesPath = "HTR001/GeneralNotes/" + objHTR001.HeaderId + "/R" + objHTR001.RevNo;
                    //var folderThermocouplePath = "HTR001/ThermocoupleAttachments/" + objHTR001.HeaderId + "/R" + objHTR001.RevNo;
                    //Manager.ManageDocuments(folderGeneralNotesPath, hasGNAttachments, gnAttach, objClsLoginInfo.UserName);
                    //Manager.ManageDocuments(folderThermocouplePath, hasTAAttachments, taAttach, objClsLoginInfo.UserName);
                    #endregion

                    db.SaveChanges();

                    objResponseMsg.HeaderId = objHTR001.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PTMTCTQMessages.Update.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.PTMTCTQMessages.Error.ToString();

                }
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //editable datatable for Line grid
        [HttpPost]
        public JsonResult LoadHTRLineGridData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "";
                strWhere += Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();
                int headerid = Convert.ToInt32(param.CTQHeaderId);
                strWhere = "1=1 and a.HeaderId =" + headerid;
                string[] columnName = { "SeamNo", "PartNo", "HeatNo", "DrawingNo", "DrawingRev", "HTPReference", "PTCMTCPQR", "Description", "MaterialSpec", "SizeThickness", "Quantity", "TotalWeight", "InspectionAgency", "MaterialFrom", "MaterialTo", "ICSBy", "ProjectNumber" };

                HTR001 objHTR001 = db.HTR001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                string buCode = db.COM001.Where(i => i.t_cprj == objHTR001.Project).FirstOrDefault().t_entu;
                var InspectionAgency = Manager.GetSubCatagories("Inspection Agency", buCode, objClsLoginInfo.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
                var items = (from li in InspectionAgency
                             select new SelectItemList { id = li.Code, text = li.CategoryDescription }).ToList();

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);

                var lstResult = db.SP_HTR_LINE_GETDETAILS
                                (
                               StartIndex,
                               EndIndex,
                               strSortOrder,
                               strWhere
                                ).ToList();
                int newRecordId = 0;
                var newRecord = new[] {
                                        "",
                                        Helper.GenerateHidden(newRecordId, "HeaderId", param.CTQHeaderId),
                                        Helper.GenerateHidden(newRecordId, "LineId", param.CTQHeaderId),
                                        Helper.GenerateTextbox(newRecordId, "SeamNo",   "", "", false, "", "50"),
                                        Helper.GenerateTextbox(newRecordId, "PartNo",   "", "", false, "", "50"),
                                        Helper.GenerateTextbox(newRecordId, "HeatNo",  "", "", false, "", "50"),
                                        Helper.GenerateTextbox(newRecordId, "DrawingNo",   "", "", false, "", "50"),
                                        Helper.GenerateTextbox(newRecordId, "DrawingRev",  "", "", false, "", "50"),
                                        Helper.GenerateTextbox(newRecordId, "HTPReference",  "", "", false, "", "50"),
                                        Helper.GenerateTextbox(newRecordId, "PTCMTCPQR",  "", "", false, "", "50"),
                                        Helper.GenerateTextbox(newRecordId, "Description",   "", "", false, "", "50"),
                                        Helper.GenerateTextbox(newRecordId, "MaterialSpec",  "", "", false, "", "50"),
                                        Helper.GenerateTextbox(newRecordId, "SizeThickness", "", "", false, "", "50"),
                                        Helper.GenerateNumericTextbox(newRecordId, "Quantity",  "", "", false, "", "10"),
                                        Helper.GenerateNumericTextbox(newRecordId, "TotalWeight",  "", "", false, "", "10"),
                                        MultiSelectDropdown(items,newRecordId,"InspectionAgency",false,"","","InspectionAgency"),
                                        Helper.GenerateTextbox(newRecordId, "MaterialFrom",  "", "", false, "", "50"),
                                        Helper.GenerateTextbox(newRecordId, "MaterialTo",  "", "", false, "", "50"),
                                        Helper.GenerateTextbox(newRecordId, "ICSBy",  "", "", false, "", "50"),
                                        Helper.GenerateTextbox(newRecordId,"ProjectNumber" ,"", "", false, "", "50"),
                                        Helper.GenerateTextbox(newRecordId, "Revision",   "", "", true, "", "50"),
                                        Helper.GenerateGridButtonNew(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveNewLine();" ),
                                    };
                string department = clsImplementationEnum.HTRDepartment.METReviewer.GetStringValue();
                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.ROW_NO),
                            Convert.ToString(uc.LineId),
                            Convert.ToString(uc.HeaderId),
                            GenerateTextboxOnChange(uc.LineId, "SeamNo",   Convert.ToString(uc.SeamNo), "UpdateData(this,"+ uc.LineId +",true )", (param.Department == department && string.Equals(param.MTStatus, clsImplementationEnum.HTRStatus.InReview.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true), "", "50"),
                            GenerateTextboxOnChange(uc.LineId, "PartNo",   Convert.ToString(uc.PartNo), "UpdateData(this,"+ uc.LineId +",true)",  (param.Department == department && string.Equals(param.MTStatus, clsImplementationEnum.HTRStatus.InReview.GetStringValue(),StringComparison.OrdinalIgnoreCase)   ? false : true), "", "50"),
                            GenerateTextboxOnChange(uc.LineId, "HeatNo",   Convert.ToString(uc.HeatNo), "UpdateData(this,"+ uc.LineId  +",true)",  (param.Department == department && string.Equals(param.MTStatus, clsImplementationEnum.HTRStatus.InReview.GetStringValue(),StringComparison.OrdinalIgnoreCase)   ? false : true), "", "50"),
                            GenerateTextboxOnChange(uc.LineId, "DrawingNo",   Convert.ToString(uc.DrawingNo), "UpdateData(this,"+ uc.LineId +",true)",  (param.Department == department && string.Equals(param.MTStatus, clsImplementationEnum.HTRStatus.InReview.GetStringValue(),StringComparison.OrdinalIgnoreCase)   ? false : true), "", "50"),
                            GenerateTextboxOnChange(uc.LineId, "DrawingRev",   Convert.ToString(uc.DrawingRev), "UpdateData(this,"+ uc.LineId +",true)",  (param.Department == department && string.Equals(param.MTStatus, clsImplementationEnum.HTRStatus.InReview.GetStringValue(),StringComparison.OrdinalIgnoreCase)   ? false : true), "", "50"),
                            GenerateTextboxOnChange(uc.LineId, "HTPReference",   Convert.ToString(uc.HTPReference), "UpdateData(this,"+ uc.LineId +",true)",  (param.Department == department && string.Equals(param.MTStatus, clsImplementationEnum.HTRStatus.InReview.GetStringValue(),StringComparison.OrdinalIgnoreCase)   ? false : true), "", "50"),
                            GenerateTextboxOnChange(uc.LineId, "PTCMTCPQR",   Convert.ToString(uc.PTCMTCPQR), "UpdateData(this,"+ uc.LineId +",true)",  (param.Department == department && string.Equals(param.MTStatus, clsImplementationEnum.HTRStatus.InReview.GetStringValue(),StringComparison.OrdinalIgnoreCase)   ? false : true), "", "50"),
                            GenerateTextboxOnChange(uc.LineId, "Description",   Convert.ToString(uc.Description), "UpdateData(this,"+ uc.LineId +",true)",  (param.Department == department && string.Equals(param.MTStatus, clsImplementationEnum.HTRStatus.InReview.GetStringValue(),StringComparison.OrdinalIgnoreCase)   ? false : true), "", "50"),
                            GenerateTextboxOnChange(uc.LineId, "MaterialSpec",   Convert.ToString(uc.MaterialSpec), "UpdateData(this,"+ uc.LineId +",true)",  (param.Department == department && string.Equals(param.MTStatus, clsImplementationEnum.HTRStatus.InReview.GetStringValue(),StringComparison.OrdinalIgnoreCase)   ? false : true), "", "50"),
                            GenerateTextboxOnChange(uc.LineId, "SizeThickness",   Convert.ToString(uc.SizeThickness), "UpdateData(this,"+ uc.LineId +",true)",  (param.Department == department && string.Equals(param.MTStatus, clsImplementationEnum.HTRStatus.InReview.GetStringValue(),StringComparison.OrdinalIgnoreCase)   ? false : true), "", "50"),
                            Helper.GenerateNumericTextbox(uc.LineId, "Quantity",   Convert.ToString(uc.Quantity), "UpdateData(this,"+ uc.LineId +",true)",  (param.Department == department && string.Equals(param.MTStatus, clsImplementationEnum.HTRStatus.InReview.GetStringValue(),StringComparison.OrdinalIgnoreCase)   ? false : true), "", "10"),
                            Helper.GenerateNumericTextbox(uc.LineId, "TotalWeight",   Convert.ToString(uc.TotalWeight), "UpdateData(this,"+ uc.LineId +",true)",  (param.Department == department && string.Equals(param.MTStatus, clsImplementationEnum.HTRStatus.InReview.GetStringValue(),StringComparison.OrdinalIgnoreCase)   ? false : true), "", "10"),
                            //Helper.MultiSelectDropdown(items,uc.InspectionAgency,false,"UpdateData(this,'"+Convert.ToString(uc.LineId)+"')","InspectionAgency"),
                            MultiSelectDropdown(items,uc.LineId,uc.InspectionAgency, (param.Department == department && string.Equals(param.MTStatus, clsImplementationEnum.HTRStatus.InReview.GetStringValue(),StringComparison.OrdinalIgnoreCase)   ? false : true),"UpdateData(this,'"+ uc.LineId  +"',true);","","InspectionAgency"),
                            GenerateTextboxOnChange(uc.LineId, "MaterialFrom",   Convert.ToString(uc.MaterialFrom), "UpdateData(this,"+ uc.LineId+",true)",  (param.Department == department && string.Equals(param.MTStatus, clsImplementationEnum.HTRStatus.InReview.GetStringValue(),StringComparison.OrdinalIgnoreCase)   ? false : true), "", "50"),
                            GenerateTextboxOnChange(uc.LineId, "MaterialTo",   Convert.ToString(uc.MaterialTo), "UpdateData(this,"+ uc.LineId+",true)",  (param.Department == department && string.Equals(param.MTStatus, clsImplementationEnum.HTRStatus.InReview.GetStringValue(),StringComparison.OrdinalIgnoreCase)   ? false : true), "", "50"),
                            GenerateTextboxOnChange(uc.LineId, "ICSBy",   Convert.ToString(uc.ICSBy), "UpdateData(this,"+ uc.LineId+",false)",  (param.Department == department && string.Equals(param.MTStatus, clsImplementationEnum.HTRStatus.InReview.GetStringValue(),StringComparison.OrdinalIgnoreCase)   ? false : true), "", "50"),
                            GenerateTextboxOnChange(uc.LineId,"ProjectNumber"  ,Convert.ToString(uc.ProjectNumber), "UpdateData(this,"+ uc.LineId+",false)",  (param.Department == department && string.Equals(param.MTStatus, clsImplementationEnum.HTRStatus.InReview.GetStringValue(),StringComparison.OrdinalIgnoreCase)   ? false : true), "", "50"),
                            GenerateTextboxOnChange(uc.LineId,"Revision"  ,Convert.ToString("R"+uc.RevNo), "UpdateData(this,"+ uc.LineId+",false)",  true, "", "50"),
                            
                            //"<center><i  data-modal='' id='btnDelete' name='btnAction' style='cursor:pointer;' Title='Delete Record' class='fa fa-trash-o' onClick='DeleteRecord(" + uc.LineId+ ")'></i></center>"
                            "<center>"+ "<i class='fa fa-plus'  id='btnComponentDetail' name='ComponentDetail' onclick='AddComponentDetail(" + uc.LineId + ")'> </i>"+ HTMLActionString(uc.LineId,param.MTStatus, "Delete", "Delete Record", "fa fa-trash-o", "DeleteRecord(" + uc.LineId + ");")+""+
                            
                            //HTMLActionString(uc.LineId,param.MTStatus, "Revise", "Revise Record", "fa fa-repeat", "SetRevisionNo(" + uc.LineId + ");",true)+" "+
                            "<i class='fa fa-upload' id='UploadFile' onclick='ShowPopUp("+uc.LineId+")'></i>"+"</center>",                                                                                                                                                                                                                                                                                           
                            //Convert.ToString(uc.LineId) //, "Delete", "Delete Rocord", "fa fa-trash-o", "DeleteQualityID("+ uc.LineId +");")
                            //Convert.ToString(uc.LineId),
            }).ToList();
                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult AddComponentDetail(string HeaderId, string Status, string LineId)
        {
            ViewBag.HdHeaderId = HeaderId;
            ViewBag.HdStatus = Status;
            ViewBag.HdLineId = LineId;
            return PartialView("_ComponentDetailsPartial");
        }
        [HttpPost]
        public JsonResult LoadHTRLineGridData_R0(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "";
                string headerid = param.CTQHeaderId;
                strWhere = " (1=1 and a.HeaderId IN(" + headerid + "))";
                string[] columnName = { "SeamNo", "PartNo", "HeatNo", "DrawingNo", "DrawingRev", "HTPReference", "PTCMTCPQR", "Description", "MaterialSpec", "SizeThickness", "Quantity", "TotalWeight", "InspectionAgency", "MaterialFrom", "MaterialTo", "ICSBy", "ProjectNumber" };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_HTR_LINE_GETDETAILS
                                (
                               StartIndex,
                               EndIndex,
                               strSortOrder,
                               strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.ROW_NO),
                            Convert.ToString(uc.LineId),
                            Convert.ToString(uc.HeaderId),
                            Convert.ToString(uc.SeamNo),
                            Convert.ToString(uc.PartNo),
                            Convert.ToString(uc.HeatNo),
                            Convert.ToString(uc.DrawingNo),
                            Convert.ToString(uc.DrawingRev),
                            Convert.ToString(uc.HTPReference),
                            Convert.ToString(uc.PTCMTCPQR),
                            Convert.ToString(uc.Description),
                            Convert.ToString(uc.MaterialSpec),
                            Convert.ToString(uc.SizeThickness),
                            Convert.ToString(uc.Quantity),
                            Convert.ToString(uc.TotalWeight),
                            Convert.ToString(uc.InspectionAgency),
                            Convert.ToString(uc.MaterialFrom),
                            Convert.ToString(uc.MaterialTo),
                            Convert.ToString(uc.ICSBy),
                            Convert.ToString(uc.ProjectNumber)
                 }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult LoadHTRCheckList(int HeaderId)
        {
            HTR001 objHTR001 = new HTR001();
            if (HeaderId > 0)
            {
                objHTR001 = db.HTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            }
            return PartialView("~/Areas/HTR/Views/Shared/_LoadHTRCheckListPartial.cshtml", objHTR001);
        }

        [NonAction]
        public static string GenerateTextboxOnChange(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string maxlength = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control";
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onchange='" + onBlurMethod + "'" : "";

            htmlControl = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' " + MaxCharcters + " value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " spara='" + rowId + "' />";

            return htmlControl;
        }

        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", bool isVisible = true, bool atagrequired = false)
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick=" + onClickMethod : "";

            if (buttonName.ToLower() == "History".ToLower())
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            }
            else
            {
                if (string.Equals(status, clsImplementationEnum.CommonStatus.DRAFT.GetStringValue()))
                {
                    if (isVisible)
                    {
                        htmlControl = "<i id='" + inputID + "' name='" + inputID + "' Title='" + buttonTooltip + "' class='iconspace " + className + "'" + onClickEvent + " ></i>";
                    }
                    else
                    {
                        htmlControl = "<i id='" + inputID + "' name='" + inputID + "' Title='" + buttonTooltip + "' class='disabledicon " + className + "'" + onClickEvent + " ></i>";
                    }
                    if (atagrequired)
                    {
                        htmlControl = "<a>" + htmlControl + "</a>";
                    }
                }
                else
                {
                    htmlControl = "<i id='" + inputID + "' name='" + inputID + "' Title='" + buttonTooltip + "' class='disabledicon " + className + "'" + onClickEvent + " ></i>";
                }
            }
            return htmlControl;
        }

        public static string MultiSelectDropdown(List<SelectItemList> list, int rowID, string selectedValue, bool Disabled = false, string OnChangeEvent = "", string OnBlurEvent = "", string ColumnName = "")
        {
            string multipleSelect = string.Empty;
            string[] arrayVal = { };

            try
            {
                if (!string.IsNullOrWhiteSpace(selectedValue))
                {
                    arrayVal = selectedValue.Split(',');
                }


                multipleSelect += "<select data-name='ddlmultiple' name='ddlmultiple" + rowID + "' id='ddlmultiple" + rowID + "' data-lineid='" + rowID + "' multiple='multiple'  style='width: 100 % ' colname='" + ColumnName + "' class='form-control multiselect-drodown' " + (Disabled ? "disabled " : "") + (OnChangeEvent != string.Empty ? "onchange=" + OnChangeEvent + "" : "") + (OnBlurEvent != string.Empty ? " onblur='" + OnBlurEvent + "'" : "") + " >";
                foreach (var item in list)
                {
                    multipleSelect += "<option value='" + item.id + "' " + ((arrayVal.Contains(item.id)) ? " selected" : "") + " >" + item.text + "</option>";
                }
                multipleSelect += "</select>";
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return multipleSelect;
        }

        #endregion

        #region Promote Demote
        [HttpPost]
        public ActionResult SendForPromote(int strHeaderId, string remark)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                string status = "";
                HTR001 objHTR001 = db.HTR001.Where(u => u.HeaderId == strHeaderId).FirstOrDefault();
                if (objHTR001 != null)
                {
                    if (objHTR001.HTRType == clsImplementationEnum.TypeOfHTR.JobComponent.GetStringValue())
                    {
                        status = clsImplementationEnum.HTRStatus.WithFurnaceSupervisor.GetStringValue();
                    }
                    if (objHTR001.HTRType == clsImplementationEnum.TypeOfHTR.JobComponentNuclear.GetStringValue())
                    {
                        status = clsImplementationEnum.HTRStatus.WithFurnaceSupervisor.GetStringValue();
                    }
                    if (objHTR001.HTRType == clsImplementationEnum.TypeOfHTR.PTC.GetStringValue())
                    {
                        status = clsImplementationEnum.HTRStatus.WithInspector.GetStringValue();
                    }
                    if (objHTR001.HTRType == clsImplementationEnum.TypeOfHTR.PQR.GetStringValue())
                    {
                        status = clsImplementationEnum.HTRStatus.Release.GetStringValue();
                    }
                    db.SP_HTR_PROMOTE_DEMOTE(strHeaderId, remark, status, objClsLoginInfo.UserName, "REVIEWER");
                    (new clsManager()).ScurveHTC(objHTR001.Project, objHTR001.Location);
                    objResponseMsg.ActionKey = true;
                    //objResponseMsg.ActionValue =
                    objResponseMsg.ActionValue = string.Format(clsImplementationMessage.CommonMessages.Promote, objHTR001.HTRNo);
                }
                else
                {
                    objResponseMsg.ActionKey = true;
                    objResponseMsg.ActionValue = "Details not available.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.ActionKey = false;
                objResponseMsg.ActionValue = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SendForDemote(int headerId, string status, string remark)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                HTR001 objHTR001 = db.HTR001.Where(u => u.HeaderId == headerId).SingleOrDefault();
                if (objHTR001 != null)
                {
                    db.SP_HTR_PROMOTE_DEMOTE(headerId, remark, status, objClsLoginInfo.UserName, "REVIEWER");
                    objResponseMsg.ActionKey = true;
                    objResponseMsg.ActionValue = "Details has been successfully Demoted.";
                }
                else
                {
                    objResponseMsg.ActionKey = true;
                    objResponseMsg.ActionValue = "Details not available.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.ActionKey = false;
                objResponseMsg.ActionValue = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        //pop up
        [HttpPost]
        public ActionResult getChangeSet(int Id) //
        {
            ViewBag.header = Id;
            return PartialView("~/Areas/HTR/Views/Shared/_HTRChangeSet.cshtml");
        }
        public class ResponceMsgWithHeaderID : clsHelper.ResponseMsg
        {
            public int HeaderId;
            public bool ActionKey;
            public string ActionValue;
        }


        public JsonResult GetProjectsEdit(int? headerId)
        {
            try
            {
                var lst = Manager.getProjectsByUserAutocomplete(objClsLoginInfo.UserName, "").ToList();
                var item = from li in lst
                           select new
                           {
                               id = li.projectCode,
                               text = li.projectDescription
                           };
                return Json(item, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetLinkedProjectValue(int headerId)
        {
            var result = db.HTR003.Where(s => s.HeaderId == headerId).Select(s => s.LinkedProject).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #region Export Excell
        //Code By : Ajay Chauhan on 09/11/2017, Export Funxtionality
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_HTR_HEADER_GETDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      HTRNo = li.HTRNo,
                                      RevNo = li.RevNo,
                                      Description = li.HTRDescription,
                                      Department = li.Department,
                                      Location = li.Location,
                                      ConcertoMSP = li.ConcertoMSP,
                                      Treatment = li.Treatment,
                                      Furnace = li.Furnace,
                                      Status = li.Status,
                                      CreatedBy = li.CreatedBy,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_HTR_LINE_GETDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from li in lst
                                  select new
                                  {
                                      SeamNo = li.SeamNo,
                                      PartNo = li.PartNo,
                                      HeatNo = li.HeatNo,
                                      DrawingNo = li.DrawingNo,
                                      DrawingRev = li.DrawingRev,
                                      HTPReference = li.HTPReference,
                                      PTCMTCPQR = li.PTCMTCPQR,
                                      Description = li.Description,
                                      MaterialSpec = li.MaterialSpec,
                                      SizeThickness = li.SizeThickness,
                                      Quantity = li.Quantity,
                                      TotalWeight = li.TotalWeight,
                                      InspectionAgency = li.InspectionAgency,
                                      MaterialFrom = li.MaterialFrom,
                                      MaterialTo = li.MaterialTo,
                                      ICSBy = li.ICSBy,
                                      ProjectNumber = li.ProjectNumber,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {
                    var lst = db.SP_HTR_HEADER_HISTORY_GETDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      HTRNo = li.HTRNo,
                                      RevNo = li.RevNo,
                                      Description = li.HTRDescription,
                                      Department = li.Department,
                                      Location = li.Location,
                                      ConcertoMSP = li.ConcertoMSP,
                                      Treatment = li.Treatment,
                                      Furnace = li.Furnace,
                                      Status = li.Status,
                                      //PromotedDemotedBy = getPromotedDemotedBy(Convert.ToString(li.FurnaceOn), Convert.ToString(li.ReviewdOn), Convert.ToString(li.InspectOn), Convert.ToString(li.SubmittedOn), Convert.ToString(li.Status), Convert.ToString(li.HTRType)),
                                      CreatedBy = li.CreatedBy,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYLINES.GetStringValue())
                {
                    var lst = db.SP_HTR_LINE_HISTORY_GETDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      SeamNo = li.SeamNo,
                                      PartNo = li.PartNo,
                                      HeatNo = li.HeatNo,
                                      DrawingNo = li.DrawingNo,
                                      DrawingRev = li.DrawingRev,
                                      HTPReference = li.HTPReference,
                                      PTCMTCPQR = li.PTCMTCPQR,
                                      Description = li.Description,
                                      MaterialSpec = li.MaterialSpec,
                                      SizeThickness = li.SizeThickness,
                                      Quantity = li.Quantity,
                                      TotalWeight = li.TotalWeight,
                                      InspectionAgency = li.InspectionAgency,
                                      MaterialFrom = li.MaterialFrom,
                                      MaterialTo = li.MaterialTo,
                                      ICSBy = li.ICSBy,
                                      ProjectNumber = li.ProjectNumber
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }

}