﻿using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.HTR.Controllers
{
    public class InspectorController : clsBase
    {
        // GET: HTR/Inspector
        #region Index Page
        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadHTRHeaderDataPartial(string status, string title)
        {
            ViewBag.Status = status;
            ViewBag.Title = title;
            return PartialView("_GetHeaderGridDataPartial");
        }

        [HttpPost]
        public JsonResult LoadHTRHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += " 1=1 and status in('" + clsImplementationEnum.HTRStatus.WithInspector.GetStringValue() + "')";
                    strWhere += "and Inspector=" + objClsLoginInfo.UserName;
                }
                else
                {
                    strWhere += " 1=1";
                    strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "BU", "Location");
                }
                string[] columnName = { "Project", "HTRNo", "HTRDescription", "Department", "Treatment", "Location", "ConcertoMSP", "Furnace", "RevNo", "Status", "CreatedBy", "EditedBy", " (Select top 1 STUFF((select distinct ', '+ PartNo from htr002 where HeaderId = htr.HeaderId FOR XML PATH(''), TYPE ).value('.', 'NVARCHAR(MAX)') ,1,1,''))" };
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);

                var lstResult = db.SP_HTR_HEADER_GETDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.Project),
                            Convert.ToString(uc.HTRNo),
                            Convert.ToString(uc.PartDescription),
                            Convert.ToString("R" + uc.RevNo),
                            uc.HTRDescription,
                            Convert.ToString(uc.Location),
                            Convert.ToString(uc.Status),
                            Convert.ToString(uc.CreatedOn.Value!=null ?uc.CreatedOn.Value.ToString("dd/MM/yyyy"):string.Empty),

                            Convert.ToString( uc.Department),
                            Convert.ToString( uc.ConcertoMSP),
                            Convert.ToString(uc.Treatment),
                            Convert.ToString(uc.Furnace),
                            Convert.ToString(uc.CreatedBy),
                            Convert.ToString(uc.EditedBy),
                            Convert.ToString(uc.ProjectDesc),

                            "<nobr><center>"+
                            "<a class='iconspace' title='View Details' href='"+WebsiteURL+"/HTR/Inspector/HTRDetails?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i class='fa fa-eye'></i></a>"+
                            "<i class='iconspace fa fa-clock-o' title='Show Timeline' href='javascript:void(0)' onclick=ShowTimeline('/HTR/Inspector/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')></i>"+
                            ((string.Equals(uc.Status, clsImplementationEnum.HTRStatus.Release.GetStringValue())) ? "<i style=\"margin-right:5px;cursor:pointer;\" title=\"Print Report\" class=\"iconspace fa fa-print\" onClick=\"PrintReport(" + uc.HeaderId+ ")\"></i> " :  "<i id='Print' name='Print' style='cursor: pointer;' title='Print Document' class='disabledicon fa fa-print'></i>") +
                            "<i id='History' name='History' style='cursor: pointer;' title='History Record' class='iconspace fa fa-history' onclick='ViewHistoryData("+ Convert.ToInt32(uc.HeaderId) +");'></i>"+
                            "</center></nobr>",
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.TimelineTitle = "HTR History";
            model.Title = "HTR";
            HTR001 objHTR001 = db.HTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            model.CreatedBy = Manager.GetUserNameFromPsNo(objHTR001.CreatedBy);
            model.CreatedOn = objHTR001.CreatedOn;
            model.ReviewedBy = Manager.GetUserNameFromPsNo(objHTR001.MET);
            model.ReviewedOn = objHTR001.ReviewdOn;
            //model.FurnaceBy = Manager.GetUserNameFromPsNo(objHTR001.Furnace);
            model.FurnaceBy = Manager.GetUserNameFromPsNo(objHTR001.FurnaceSupervisor);
            model.FurnaceOn = objHTR001.FurnaceOn;
            model.InspectBy = Manager.GetUserNameFromPsNo(objHTR001.Inspector);
            model.InspectOn = objHTR001.InspectOn;
            model.SubmittedBy = Manager.GetUserNameFromPsNo(objHTR001.SubmittedBy);
            model.SubmittedOn = objHTR001.SubmittedOn;
            if (objHTR001.HTRType == clsImplementationEnum.TypeOfHTR.JobComponent.GetStringValue())
            {
                model.HTRType = objHTR001.HTRType;
            }
            else if (objHTR001.HTRType == clsImplementationEnum.TypeOfHTR.PTC.GetStringValue())
            {
                model.HTRType = objHTR001.HTRType;
            }
            else
            {
                model.HTRType = objHTR001.HTRType;
                model.HTRpqrStatus = objHTR001.Status;
            }

            return PartialView("_HTRTimelineProgress", model);
        }
        #endregion

        #region View Header
        [SessionExpireFilter]
        [AllowAnonymous]
        //[UserPermissions]
        public ActionResult HTRDetails(string headerID)
        {
            int HeaderId = 0;
            if (!string.IsNullOrWhiteSpace(headerID))
            {
                HeaderId = Convert.ToInt32(headerID);
            }
            HTR001 objHTR001 = db.HTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            if (objHTR001 != null)
            {
                ViewBag.Project = db.COM001.Where(i => i.t_cprj == objHTR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objHTR001.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objHTR001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objHTR001.Department = db.COM002.Where(a => a.t_dimx == objHTR001.Department).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                string strInspector = db.COM003.Where(x => x.t_psno == objHTR001.Inspector && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                string strMET = db.COM003.Where(x => x.t_psno == objHTR001.MET && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                string strSupervisor = db.COM003.Where(x => x.t_psno == objHTR001.FurnaceSupervisor && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                ViewBag.Inspector = objHTR001.Inspector + " - " + strInspector;
                ViewBag.MET = objHTR001.MET + " - " + strMET;
                ViewBag.FurnaceSupervisor = objHTR001.FurnaceSupervisor + " - " + strSupervisor;

                ViewBag.MinLoadingTemp = clsImplementationEnum.GetMinLoadingTemp().ToArray();
                ViewBag.MaxLoadingTemp = clsImplementationEnum.GetMaxLoadingTemp().ToArray();
                ViewBag.MinRateOfHeating = clsImplementationEnum.GetMinRateOfHeating().ToArray();
                ViewBag.MaxRateOfHeating = clsImplementationEnum.GetMaxRateOfHeating().ToArray();
                ViewBag.MinRateOfCooling = clsImplementationEnum.GetMinRateOfCooling().ToArray();
                ViewBag.MaxRateOfCooling = clsImplementationEnum.GetMaxRateOfCooling().ToArray();
                ViewBag.MinUnloadingTemp = clsImplementationEnum.GetMinUnloadingTemp().ToArray();
                ViewBag.MaxUnloadingTemp = clsImplementationEnum.GetMaxUnloadingTemp().ToArray();
                ViewBag.CoolPart = clsImplementationEnum.GetCoolPart().ToArray();
                ViewBag.CoolBy = clsImplementationEnum.GetCoolBy().ToArray();
                ViewBag.VolTempOf = clsImplementationEnum.GetVolTempOf().ToArray();
                ViewBag.Furnace = clsImplementationEnum.GetFurnace().ToArray();
                ViewBag.StandardProcedure = clsImplementationEnum.GetStandardProcedure().ToArray();
                ViewBag.HeatingThermocouplesVariation = clsImplementationEnum.GetHeatingThermocouplesVariation().ToArray();
                ViewBag.CoolingThermocouplesVariation = clsImplementationEnum.GetCoolingThermocouplesVariation().ToArray();
                ViewBag.CoolingThermocouplesVariation = clsImplementationEnum.GetCoolingThermocouplesVariation().ToArray();
                ViewBag.WPSNo = clsImplementationEnum.GetWPSNo().ToArray();
                if (objHTR001.HTRType == clsImplementationEnum.TypeOfHTR.JobComponentNuclear.GetStringValue())
                {
                    ViewBag.Demote = clsImplementationEnum.GetInspector().ToArray();
                }
                if (objHTR001.HTRType == clsImplementationEnum.TypeOfHTR.PTC.GetStringValue())
                {
                    ViewBag.Demote = clsImplementationEnum.GetJCPTCfurnase().ToArray();
                }
                if (objHTR001.Inspector != null ? objHTR001.Inspector.Trim() != objClsLoginInfo.UserName.Trim() : true)
                {
                    ViewBag.Access = "noaccess";
                }
                ViewBag.Action = "edit";
            }
            return View(objHTR001);
        }

        [HttpPost]
        public ActionResult LoadHTRCheckList(int HeaderId)
        {
            HTR001 objHTR001 = new HTR001();
            if (HeaderId > 0)
            {
                objHTR001 = db.HTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            }
            return PartialView("~/Areas/HTR/Views/Shared/_LoadHTRCheckListPartial.cshtml", objHTR001);
        }
        [HttpPost]
        public ActionResult SaveHeader(HTR001 htr001, FormCollection fc, HttpPostedFileBase fileScanPlan)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                HTR001 objHTR001 = new HTR001();
                if (htr001.HeaderId > 0)
                {
                    objHTR001 = db.HTR001.Where(x => x.HeaderId == htr001.HeaderId).FirstOrDefault();
                }
                objHTR001.MinLoadingTemp = htr001.MinLoadingTemp;
                objHTR001.MaxLoadingTemp = htr001.MaxLoadingTemp;
                objHTR001.MinRateOfHeating = htr001.MinRateOfHeating;
                objHTR001.MaxRateOfHeating = htr001.MaxRateOfHeating;
                objHTR001.MinSoakingTemperature = htr001.MinSoakingTemperature;
                objHTR001.MaxSoakingTemperature = htr001.MaxSoakingTemperature;
                objHTR001.SoakingTimeHH = htr001.SoakingTimeHH;
                objHTR001.SoakingTimeMM = htr001.SoakingTimeMM;
                objHTR001.MinRateOfCooling = htr001.MinRateOfCooling;
                objHTR001.MaxRateOfCooling = htr001.MaxRateOfCooling;
                objHTR001.MinUnloadingTemp = htr001.MinUnloadingTemp;
                objHTR001.MaxUnloadingTemp = htr001.MaxUnloadingTemp;
                objHTR001.CoolPart = htr001.CoolPart;
                objHTR001.CoolBy = htr001.CoolBy;
                objHTR001.MaxDelay = htr001.MaxDelay;
                objHTR001.VolTempOf = htr001.VolTempOf;
                objHTR001.TempOfLiquidAfterQuenching = htr001.TempOfLiquidAfterQuenching;
                objHTR001.MinVol = htr001.MinVol;
                objHTR001.HeatingThermocouplesVariation = htr001.HeatingThermocouplesVariation;
                objHTR001.CoolingThermocouplesVariation = htr001.CoolingThermocouplesVariation;
                objHTR001.StandardProcedure = htr001.StandardProcedure;
                objHTR001.WPSNo = htr001.WPSNo;
                objHTR001.Comments = htr001.Comments;

                if (htr001.HeaderId > 0)
                {

                    //if (objHTR001.Status == clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue())
                    //{
                    //    objHTR001.RevNo = Convert.ToInt32(objHTR001.RevNo) + 1;
                    //    objHTR001.Status = clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue();
                    //}
                    objHTR001.EditedBy = objClsLoginInfo.UserName;
                    objHTR001.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    //HTR003 objHTR003 = new HTR003();
                    //var LinkedProject = fc["LinkedProject"].Split('-')[0];
                    //List<HTR003> lstHTR003 = db.HTR003.Where(x => x.HeaderId == objHTR001.HeaderId).ToList();
                    //if (lstHTR003 != null && lstHTR003.Count > 0)
                    //{
                    //    db.HTR003.RemoveRange(lstHTR003);
                    //}
                    //db.SaveChanges();
                    //foreach (var item in LinkedProject)
                    //{
                    //    try
                    //    {
                    //        List<HTR003> lstSrcHTR003 = db.HTR003.Where(x => x.HeaderId == objHTR001.HeaderId).ToList();
                    //        db.HTR003.AddRange(
                    //                            lstSrcHTR003.Select(x =>
                    //                            new HTR003
                    //                            {
                    //                                HeaderId = objHTR001.HeaderId,
                    //                                Project = objHTR001.Project,
                    //                                HTRNo = objHTR001.HTRNo,
                    //                                RevNo = objHTR001.RevNo,
                    //                                LinkedProject = item.ToString(),
                    //                                CreatedBy = objClsLoginInfo.UserName,
                    //                                CreatedOn = DateTime.Now,
                    //                            })
                    //                          );
                    //        db.SaveChanges();
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    //        objResponseMsg.Key = false;
                    //        objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
                    //    }
                    //}
                    objResponseMsg.HeaderId = objHTR001.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PTMTCTQMessages.Update.ToString();
                }
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetHTRLinesViewForm(int HeaderId, int LineId, string project)
        {
            HTR001 objHTR001 = new HTR001();
            HTR002 objHTR002 = new HTR002();
            objHTR001 = db.HTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            if (objHTR001 != null)
            {
                string buCode = db.COM001.Where(i => i.t_cprj == objHTR001.Project).FirstOrDefault().t_entu;
                var InspectionAgency = Manager.GetSubCatagories("Inspection Agency", buCode, objClsLoginInfo.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
                //List<GLB002> lstCTGLB002 = Manager.GetSubCatagories("Inspection Agency", buCode, objClsLoginInfo.Location).ToList();
                //ViewBag.Agency = lstCTGLB002.Count();
                // lstCTGLB002.AsEnumerable().Select(x => new SelectListItem() { Text = (x.Code + "-" + x.Description).ToString(), Value = x.Code.ToString() }).ToList();
                ViewBag.Agency = InspectionAgency.Any() ? InspectionAgency : null;


                if (InspectionAgency.Count > 0)
                    ViewBag.InspectionAgency = InspectionAgency;
                else
                {
                    List<string> lstCategory = new List<string>();
                    if (!InspectionAgency.Any())
                        lstCategory.Add("Inspection Agency");
                    ViewBag.Agency = null;
                    ViewBag.Value = string.Join(",", lstCategory.ToList()) + " Category not exist for selected project/Quality Project, Please contact Admin";
                }
            }

            if (LineId > 0)
            {
                objHTR002 = db.HTR002.Where(x => x.LineId == LineId).FirstOrDefault();

                ViewBag.Action = "viewline";
            }
            else
            {
                objHTR002.RevNo = 0;
            }

            ViewBag.LineProject = project;//db.COM001.Where(i => i.t_cprj == objHTR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            ViewBag.HTRNo = objHTR001.HTRNo;
            ViewBag.RevNo = objHTR001.RevNo;
            objHTR002.HeaderId = objHTR001.HeaderId;
            return PartialView("~/Areas/HTR/Views/Shared/_LoadHTRLinesPartial.cshtml", objHTR002);
        }


        [HttpPost]
        public JsonResult LoadHTRLineGridData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                string headerid = param.CTQHeaderId;
                strWhere = " (1=1 and a.HeaderId IN(" + headerid + "))";
                //strWhere += Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();
                //string[] arrayCTQStatus = { clsImplementationEnum.CTQStatus.DRAFT.GetStringValue(), clsImplementationEnum.CTQStatus.Returned.GetStringValue() };
                string[] columnName = { "SeamNo", "PartNo", "HeatNo", "DrawingNo", "DrawingRev", "HTPReference", "PTCMTCPQR", "Description", "MaterialSpec", "SizeThickness", "Quantity", "TotalWeight", "InspectionAgency", "MaterialFrom", "MaterialTo", "ICSBy", "ProjectNumber" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch).Replace("RevNo", "a.RevNo");
                else
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter).Replace("RevNo", "a.RevNo");

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_HTR_LINE_GETDETAILS
                                (
                               StartIndex,
                               EndIndex,
                               strSortOrder,
                               strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                             Convert.ToString(uc.ROW_NO),
                            Convert.ToString(uc.LineId),
                            Convert.ToString(uc.HeaderId),
                            Convert.ToString(uc.SeamNo),
                            Convert.ToString(uc.PartNo),
                            Convert.ToString(uc.HeatNo),
                            Convert.ToString(uc.DrawingNo),
                            Convert.ToString(uc.DrawingRev),
                            Convert.ToString(uc.HTPReference),
                            Convert.ToString(uc.PTCMTCPQR),
                            Convert.ToString(uc.Description),
                            Convert.ToString(uc.MaterialSpec),
                            Convert.ToString(uc.SizeThickness),
                            Convert.ToString(uc.Quantity),
                            Convert.ToString(uc.TotalWeight),
                            Convert.ToString(uc.InspectionAgency),
                            Convert.ToString(uc.MaterialFrom),
                            Convert.ToString(uc.MaterialTo),
                            Convert.ToString(uc.ICSBy),
                            Convert.ToString(uc.ProjectNumber),
                            Convert.ToString("R"+uc.RevNo),
                            "<center>"+ "<i class='fa fa-plus'  id='btnComponentDetail' name='ComponentDetail' onclick='AddComponentDetail(" + uc.LineId + ")'> </i> ",

                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Promote Demote
        [SessionExpireFilter]
        [HttpPost]
        public ActionResult SendForPromote(int strHeaderId, string remark)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                string status = "";
                HTR001 objHTR001 = db.HTR001.Where(u => u.HeaderId == strHeaderId).SingleOrDefault();
                if (objHTR001 != null)
                {
                    status = clsImplementationEnum.HTRStatus.Release.GetStringValue();
                    db.SP_HTR_PROMOTE_DEMOTE(strHeaderId, remark, status, objClsLoginInfo.UserName, "INSPECTOR");
                    (new clsManager()).ScurveHTC(objHTR001.Project, objHTR001.Location);
                    objResponseMsg.ActionKey = true;
                    objResponseMsg.ActionValue = string.Format(clsImplementationMessage.CommonMessages.Promote, objHTR001.HTRNo);
                }
                else
                {
                    objResponseMsg.ActionKey = true;
                    objResponseMsg.ActionValue = "Details not available.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.ActionKey = false;
                objResponseMsg.ActionValue = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SendForDemote(int strHeaderId, string status, string remark)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                HTR001 objHTR001 = db.HTR001.Where(u => u.HeaderId == strHeaderId).SingleOrDefault();
                if (objHTR001 != null)
                {
                    db.SP_HTR_PROMOTE_DEMOTE(strHeaderId, remark, status, objClsLoginInfo.UserName, "INSPECTOR");
                    objResponseMsg.ActionKey = true;
                    objResponseMsg.ActionValue = "Details has been successfully Demoted.";
                }
                else
                {
                    objResponseMsg.ActionKey = true;
                    objResponseMsg.ActionValue = "Details not available.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.ActionKey = false;
                objResponseMsg.ActionValue = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        //pop up
        [HttpPost]
        public ActionResult getChangeSet(int Id) //
        {
            ViewBag.header = Id;
            return PartialView("~/Areas/HTR/Views/Shared/_HTRChangeSet.cshtml");
        }
        public class ResponceMsgWithHeaderID : clsHelper.ResponseMsg
        {
            public int HeaderId;
            public bool ActionKey;
            public string ActionValue;
        }

        public JsonResult GetProjects(string search)
        {
            try
            {
                var items = Manager.getProjectsByUserAutocomplete(objClsLoginInfo.UserName, search).ToList();

                var item = from li in items
                           select new
                           {
                               id = li.projectCode,
                               text = li.projectDescription
                           };
                return Json(item, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetProjectsEdit(int? headerId)
        {
            try
            {
                var lst = Manager.getProjectsByUserAutocomplete(objClsLoginInfo.UserName, "").ToList();
                var item = from li in lst
                           select new
                           {
                               id = li.projectCode,
                               text = li.projectDescription
                           };
                return Json(item, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetLinkedProjectValue(int headerId)
        {
            var result = db.HTR003.Where(s => s.HeaderId == headerId).Select(s => s.LinkedProject).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #region Export Excell
        //Code By : Ajay Chauhan on 09/11/2017, Export Funxtionality
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_HTR_HEADER_GETDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      HTRNo = li.HTRNo,
                                      RevNo = li.RevNo,
                                      Description = li.HTRDescription,
                                      Department = li.Department,
                                      Location = li.Location,
                                      ConcertoMSP = li.ConcertoMSP,
                                      Treatment = li.Treatment,
                                      Furnace = li.Furnace,
                                      Status = li.Status,
                                      CreatedBy = li.CreatedBy,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_HTR_LINE_GETDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from li in lst
                                  select new
                                  {
                                      SeamNo = li.SeamNo,
                                      PartNo = li.PartNo,
                                      HeatNo = li.HeatNo,
                                      DrawingNo = li.DrawingNo,
                                      DrawingRev = li.DrawingRev,
                                      HTPReference = li.HTPReference,
                                      PTCMTCPQR = li.PTCMTCPQR,
                                      Description = li.Description,
                                      MaterialSpec = li.MaterialSpec,
                                      SizeThickness = li.SizeThickness,
                                      Quantity = li.Quantity,
                                      TotalWeight = li.TotalWeight,
                                      InspectionAgency = li.InspectionAgency,
                                      MaterialFrom = li.MaterialFrom,
                                      MaterialTo = li.MaterialTo,
                                      ICSBy = li.ICSBy,
                                      ProjectNumber = li.ProjectNumber,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

    }
}