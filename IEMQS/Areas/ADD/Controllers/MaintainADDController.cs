﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace IEMQS.Areas.ADD.Controllers
{
    public class MaintainADDController : clsBase
    {
        // GET: ADD/MaintainADD
        #region Header List
        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult Index()
        {
            ViewBag.Title = clsImplementationEnum.ADDIndexTitle.maintainADD.GetStringValue();
            ViewBag.IndexType = clsImplementationEnum.ADDIndexType.maintain.GetStringValue();
            return View();
        }

        public ActionResult Review()
        {
            ViewBag.Title = clsImplementationEnum.ADDIndexTitle.ReviewADD.GetStringValue();
            ViewBag.IndexType = clsImplementationEnum.ADDIndexType.review.GetStringValue();
            return View("Index");
        }

        public ActionResult GetADDIndexDataPartial(string status, string title, string indextype)
        {
            ViewBag.Status = status;
            ViewBag.GridTitle = title;
            ViewBag.IndexDataFor = indextype;
            return PartialView("_GetIndexGridDataPartial");
        }

        public ActionResult LoadADDIndexData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string WhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                string _urlform = string.Empty;
                string indextype = param.Department;
                if (string.IsNullOrWhiteSpace(indextype))
                {
                    indextype = clsImplementationEnum.ADDIndexType.maintain.GetStringValue();
                }
                WhereCondition += "1=1 ";

                if (param.Status.ToLower() == "pending")
                {
                    if (indextype == clsImplementationEnum.ADDIndexType.maintain.GetStringValue())
                    {
                        WhereCondition += " and Status  in ( '" + clsImplementationEnum.ADDStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.ADDStatus.ReturnByReviewer.GetStringValue() + "') ";
                    }
                    if (indextype == clsImplementationEnum.ADDIndexType.review.GetStringValue())
                    {
                        WhereCondition += " and Status in ( '" + clsImplementationEnum.ADDStatus.SubmittedForReview.GetStringValue() + "','" + clsImplementationEnum.ADDStatus.ReturnByAssignee.GetStringValue() + "')  ";
                    }
                }

                //search Condition 
                string[] columnName = { "DocumentName", "RevNo", "Description", "Status", "Title", "EffectiveDate" };
                WhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {

                    WhereCondition += " and (DocumentName like '%" + param.sSearch
                         + "%' or RevNo like '%" + param.sSearch
                         + "%' or Description like '%" + param.sSearch
                         + "%' or Status like '%" + param.sSearch
                         + "%' or Title like '%" + param.sSearch
                         + "%' or EffectiveDate like '%" + param.sSearch + "%')";

                }
                else
                {
                    WhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                var lstResult = db.SP_ADD_GET_INDEX_DATA(StartIndex, EndIndex, strSortOrder, WhereCondition).ToList();

                var data = (from h in lstResult
                            select new[]
                           {
                               Convert.ToString(h.HeaderId),
                               Convert.ToString(h.DocumentName),
                               Convert.ToString("R"+h.RevNo),
                               Convert.ToString(h.Title),
                               Convert.ToString(h.Status),
                               (h.EffectiveDate.HasValue ? h.EffectiveDate.Value.ToString("dd/MM/yyyy") :  ""),
                                "<nobr><center>"+
                                         Helper.GenerateActionIcon(h.HeaderId, "View", "View Detail", "fa fa-eye", "",WebsiteURL +"/ADD/MaintainADD/Details/"+h.HeaderId+"?urlForm="+ indextype ,false)
                                       + (indextype == clsImplementationEnum.ADDIndexType.maintain.GetStringValue() ? Helper.GenerateActionIcon(h.HeaderId,"Delete","Delete Record","fa fa-trash-o", "DeleteHeader("+ h.HeaderId +");","", ( h.Status==clsImplementationEnum.ADDStatus.Draft.GetStringValue() || h.Status==clsImplementationEnum.ADDStatus.ReturnByReviewer.GetStringValue()? false : true)) : "")
                                       +  Helper.GenerateActionIcon(h.HeaderId, "History", "History Detail", "fa fa-history", "GetHistoryDetailsPartial("+h.HeaderId+",'"+indextype+"')","", (h.RevNo > 0 || h.Status.Equals(clsImplementationEnum.ADDStatus.Approved.GetStringValue()) ? false:true))
                               + "</center></nobr>",
                          }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = WhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DeleteHeader(int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                ADD001 objADD001 = db.ADD001.Where(x => x.HeaderId == headerId).FirstOrDefault();
                string strStatus = clsImplementationEnum.ADDStatus.Approved.GetStringValue();
                ADD001_Log objSourceADD001 = db.ADD001_Log.Where(u => u.HeaderId == headerId && u.Status == strStatus).OrderByDescending(a => a.Id).FirstOrDefault();

                if (objADD001 != null)
                {
                    if (string.Equals(objADD001.Status, clsImplementationEnum.ADDStatus.Draft.GetStringValue(), StringComparison.OrdinalIgnoreCase) || string.Equals(objADD001.Status, clsImplementationEnum.ADDStatus.ReturnByReviewer.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                    {
                        if (objADD001.RevNo == 0)
                        {
                            var lstADD002 = db.ADD002.Where(x => x.HeaderId == objADD001.HeaderId).ToList();
                            if (lstADD002 != null && lstADD002.Count > 0)
                            {
                                db.ADD002.RemoveRange(lstADD002);
                                db.SaveChanges();
                            }
                            db.ADD001.Remove(objADD001);
                            db.SaveChanges();
                        }
                        else
                        {
                            UpdateRevDetails(objSourceADD001, objADD001, true);
                        }
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Details not available.";
                    }
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponseMsgWithStatus UpdateRevDetails(ADD001_Log objSrc, ADD001 objDest, bool IsEdited)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();

            try
            {
                int? oldrev = objSrc.RevNo;
                int? newrev = objDest.RevNo;

                int oldId = objSrc.Id;
                objDest.RevNo = objSrc.RevNo;
                objDest.Description = objSrc.Description;
                objDest.Title = objSrc.Title;
                objDest.EffectiveDate = objSrc.EffectiveDate;
                objDest.ModelType = objSrc.ModelType;
                objDest.AIApprovalTaken = objSrc.AIApprovalTaken;
                objDest.AIApprovalRequired = objSrc.AIApprovalRequired;
                objDest.PEApprovalTaken = objSrc.PEApprovalTaken;
                objDest.PEApprovalRequired = objSrc.PEApprovalRequired;
                objDest.ApproveDate = objSrc.ApproveDate;
                objDest.DrawingSize = objSrc.DrawingSize;
                objDest.CustomerComment = objSrc.CustomerComment;
                objDest.PageSize = objSrc.PageSize;
                objDest.DOBApprovalTaken = objSrc.DOBApprovalTaken;
                objDest.DOBApprovalRequired = objSrc.DOBApprovalRequired;
                objDest.PageNo = objSrc.PageNo;
                objDest.Remarks = objSrc.Remarks;
                objDest.Status = objSrc.Status;
                objDest.CreatedBy = objSrc.CreatedBy;
                objDest.CreatedOn = objSrc.CreatedOn;
                objDest.EditedBy = objSrc.EditedBy;
                objDest.EditedOn = objSrc.EditedOn;
                objDest.SubmittedBy = objSrc.SubmittedBy;
                objDest.SubmittedOn = objSrc.SubmittedOn;
                objDest.ReviewedBy = objSrc.ReviewedBy;
                objDest.ReviewedOn = objSrc.ReviewedOn;
                objDest.Comments = objSrc.Comments;
                db.SaveChanges();

                if (objDest.ADD002.Count > 0)
                {
                    foreach (var item in objDest.ADD002.ToList())
                    {
                        var objDestTask = db.ADD002_Log.Where(m => m.RefId == objSrc.Id && m.LineId == item.LineId).FirstOrDefault();
                        if (objDestTask != null)
                        {
                            ADD002 objADD002 = db.ADD002.Where(m => m.HeaderId == objSrc.HeaderId && m.LineId == item.LineId).FirstOrDefault();

                            objADD002.HeaderId = objDestTask.HeaderId;
                            objADD002.RevNo = objDestTask.RevNo;
                            objADD002.Assignee = objDestTask.Assignee;
                            objADD002.Comments = objDestTask.Comments;
                            objADD002.AssignedBy = objDestTask.AssignedBy;
                            objADD002.AssignedOn = objDestTask.AssignedOn;
                            objADD002.TaskName = objDestTask.TaskName;
                            objADD002.TaskDescription = objDestTask.TaskDescription;
                            objADD002.TaskStatus = objDestTask.TaskStatus;
                            objADD002.AssigneeComments = objDestTask.AssigneeComments;
                            objADD002.ApprovedBy = objDestTask.ApprovedBy;
                            objADD002.ApprovedOn = objDestTask.ApprovedOn;
                            objADD002.RejectedBy = objDestTask.RejectedBy;
                            objADD002.RejectedOn = objDestTask.RejectedOn;
                            objADD002.CreatedBy = objDestTask.CreatedBy;
                            objADD002.CreatedOn = objDestTask.CreatedOn;
                            objADD002.EditedBy = objDestTask.EditedBy;
                            objADD002.EditedOn = objDestTask.EditedOn;
                            objADD002.RouteNo = objDestTask.RouteNo;
                            db.SaveChanges();
                        }
                        else
                        {
                            db.ADD002.Remove(item);
                            db.SaveChanges();
                        }
                    }
                }

                if (newrev != oldrev)
                {
                    //string deletefolderPath = "ADD001/" + objDest.HeaderId + "/R" + newrev;
                    //var lstTVL011RefDoc = (new clsFileUpload()).GetDocuments(deletefolderPath, true).Select(x => x.Name).ToList();
                    //foreach (var filename in lstTVL011RefDoc)
                    //{
                    //    //(new clsFileUpload()).DeleteFileAsync(deletefolderPath, filename.ToString());
                    //}

                    Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                    string Table_Name = "ADD001//" + objDest.HeaderId + "//R" + newrev;
                    _objFUC.DeleteAllFileOnFCSServerAsync(objDest.HeaderId, Table_Name, Table_Name, DESServices.CommonService.GetUseIPConfig);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                objResponseMsg.HeaderId = objDest.HeaderId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return objResponseMsg;
        }

        //revise header
        [HttpPost]
        public ActionResult ReviseHeader(int strHeaderId, string Remarks)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string status = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
                ADD001 objADD001 = db.ADD001.Where(u => u.HeaderId == strHeaderId && u.Status == status).FirstOrDefault();
                if (objADD001 != null)
                {
                    int? oldrev = objADD001.RevNo;
                    int? newrev = Convert.ToInt32(objADD001.RevNo) + 1;
                    objADD001.RevNo = Convert.ToInt32(objADD001.RevNo) + 1;
                    objADD001.Remarks = Remarks;
                    objADD001.Status = clsImplementationEnum.ADDStatus.Draft.GetStringValue();
                    objADD001.ReviewedBy = null;
                    objADD001.ReviewedOn = null;
                    objADD001.EditedBy = objClsLoginInfo.UserName;
                    objADD001.EditedOn = DateTime.Now;
                    objADD001.SubmittedBy = null;
                    objADD001.SubmittedOn = null;
                    db.SaveChanges();
                    #region copy documents
                    string oldfolderPath = "ADD001//" + objADD001.HeaderId + "//R" + (objADD001.RevNo-1) ;
                    string newfolderPath = "ADD001//" + objADD001.HeaderId + "//R" + newrev;
                    //(new clsFileUpload()).CopyFolderContentsAsync(oldfolderPath, newfolderPath);
                    Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                    _objFUC.CopyDataOnFCSServerAsync(oldfolderPath, newfolderPath, oldfolderPath, objADD001.HeaderId, newfolderPath, objADD001.HeaderId, IEMQS.DESServices.CommonService.GetUseIPConfig, IEMQS.DESServices.CommonService.objClsLoginInfo.UserName, 0);

                    #endregion
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objADD001.HeaderId;
                    objResponseMsg.Status = objADD001.Status;
                    objResponseMsg.Revision = Convert.ToInt32(objADD001.RevNo);
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revision;
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Add header
        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult Details(int id = 0, string urlForm = "")
        {
            ADD001 objADD001 = new ADD001();

            if (id > 0)
            {
                objADD001 = db.ADD001.Where(x => x.HeaderId == id).FirstOrDefault();
                if (objADD001 != null)
                {
                    string routename = db.ADD003.Where(x => x.HeaderId == objADD001.HeaderId).OrderByDescending(x => x.Id).Select(x => x.RouteNo).FirstOrDefault();
                    ViewBag.RouteNo = string.IsNullOrWhiteSpace(routename) ? "" : routename;
                    ViewBag.Action = "edit";
                }
            }
            else
            {
                urlForm = clsImplementationEnum.ADDIndexType.maintain.GetStringValue();
                objADD001.RevNo = 0;
                objADD001.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
            }
            ViewBag.yesNo = clsImplementationEnum.getyesno().ToArray();

            string status = objADD001.Status;
            if ((status == clsImplementationEnum.ADDStatus.Draft.GetStringValue() || status == clsImplementationEnum.ADDStatus.ReturnByReviewer.GetStringValue()) && urlForm.ToLower() == clsImplementationEnum.ADDIndexType.maintain.GetStringValue().ToLower())
            {
                ViewBag.buttonname = clsImplementationEnum.ADDIndexType.maintain.GetStringValue();
            }
            if ((status == clsImplementationEnum.ADDStatus.SubmittedForReview.GetStringValue() || status == clsImplementationEnum.ADDStatus.ReturnByAssignee.GetStringValue()) && urlForm.ToLower() == clsImplementationEnum.ADDIndexType.review.GetStringValue().ToLower())
            {
                ViewBag.buttonname = clsImplementationEnum.ADDIndexType.review.GetStringValue();
            }
            if ((status == clsImplementationEnum.ADDStatus.Approved.GetStringValue()) && urlForm.ToLower() == clsImplementationEnum.ADDIndexType.maintain.GetStringValue().ToLower())
            {
                ViewBag.buttonname = clsImplementationEnum.ADDIndexType.approve.GetStringValue();
            }

            if (string.IsNullOrWhiteSpace(ViewBag.buttonname))
            {
                ViewBag.buttonname = "hideall";
            }
            ViewBag.formRedirect = urlForm;
            ViewBag.Title = "Advance Drawing Document";
            return View(objADD001);
        }

        public string getNextRouteID()
        {
            string id = string.Empty;
            int i = (from n in db.ADD003
                     orderby n.Id descending
                     select n.Id).FirstOrDefault();
            if (i != 0)
            {
                i = Convert.ToInt32(i) + 1;
                id = "Route-" + i.ToString("D5");
            }
            else
            {
                i = 1;
                id = "Route-" + i.ToString("D5");
            }
            return id;
        }


        [HttpPost]
        public ActionResult SaveHeader(ADD001 add001, FormCollection fc)
        {

            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                ADD001 objADD001 = new ADD001();
                if (add001.HeaderId > 0)
                {
                    objADD001 = db.ADD001.Where(x => x.HeaderId == add001.HeaderId).FirstOrDefault();
                    //objADD001.DocumentName = add001.DocumentName;
                    objADD001.RevNo = add001.RevNo;
                    objADD001.Description = add001.Description;
                    if (string.IsNullOrWhiteSpace(add001.Title))
                    {
                        objADD001.Title = add001.DocumentName;
                    }
                    else { objADD001.Title = add001.Title; }
                    objADD001.EffectiveDate = add001.EffectiveDate;
                    objADD001.ModelType = add001.ModelType;
                    objADD001.AIApprovalTaken = add001.AIApprovalTaken;
                    objADD001.AIApprovalRequired = add001.AIApprovalRequired;
                    objADD001.PEApprovalTaken = add001.PEApprovalTaken;
                    objADD001.PEApprovalRequired = add001.PEApprovalRequired;
                    objADD001.ApproveDate = add001.ApproveDate;
                    objADD001.DrawingSize = add001.DrawingSize;
                    objADD001.CustomerComment = add001.CustomerComment;
                    objADD001.PageSize = add001.PageSize;
                    objADD001.DOBApprovalTaken = add001.DOBApprovalTaken;
                    objADD001.DOBApprovalRequired = add001.DOBApprovalRequired;
                    objADD001.PageNo = add001.PageNo;
                    objADD001.Remarks = add001.Remarks;
                    objADD001.Status = objADD001.Status;
                    objADD001.EditedBy = objClsLoginInfo.UserName;
                    objADD001.EditedOn = DateTime.Now;
                    objADD001.Comments = add001.Comments;
                    db.SaveChanges();

                    #region Reviewer
                    if (fc["hdRole"].ToLower() == clsImplementationEnum.ADDIndexType.review.GetStringValue().ToLower())
                    {
                        ADD003 objADD003 = new ADD003();
                        objADD003.HeaderId = objADD001.HeaderId;
                        objADD003.ADDRevNo = objADD001.RevNo;
                        objADD003.RouteNo = getNextRouteID();
                        objADD003.CreatedBy = objClsLoginInfo.UserName;
                        objADD003.CreatedOn = DateTime.Now;
                        db.ADD003.Add(objADD003);
                        db.SaveChanges();
                        if (objADD001.Status == clsImplementationEnum.ADDStatus.SubmittedForReview.GetStringValue()
                            || objADD001.Status == clsImplementationEnum.ADDStatus.ReturnByAssignee.GetStringValue())
                        {
                            ADD002 objADD002 = new ADD002();
                            //var lstDesADD002 = db.ADD002.Where(x => x.HeaderId == objADD001.HeaderId).ToList();
                            //if (lstDesADD002 != null && lstDesADD002.Count > 0)
                            //{
                            //    db.ADD002.RemoveRange(lstDesADD002);
                            //    db.SaveChanges();
                            //}

                            string Assignee = fc["TeamMember"];
                            string[] arrayAssignee = Assignee.Split(',').ToArray();
                            ADD002 newobjADD002 = db.ADD002.Where(x => x.HeaderId == objADD001.HeaderId).FirstOrDefault();
                            List<ADD002> lstADD002 = new List<ADD002>();
                            for (int i = 0; i < arrayAssignee.Length; i++)
                            {
                                ADD002 objADD002Add = new ADD002();

                                objADD002Add.HeaderId = objADD001.HeaderId;
                                objADD002Add.RevNo = objADD001.RevNo;
                                objADD002Add.Comments = objADD001.Comments;
                                objADD002Add.TaskDescription = objADD001.Description;
                                objADD002Add.TaskName = objADD001.DocumentName;
                                objADD002Add.TaskStatus = clsImplementationEnum.ADDTaskStatus.SubmittedForApproval.GetStringValue();
                                objADD002Add.Assignee = arrayAssignee[i];
                                objADD002Add.CreatedBy = objClsLoginInfo.UserName;
                                objADD002Add.CreatedOn = DateTime.Now;
                                objADD002Add.AssignedBy = objClsLoginInfo.UserName;
                                objADD002Add.AssignedOn = DateTime.Now;
                                objADD002Add.RouteNo = objADD003.RouteNo;
                                lstADD002.Add(objADD002Add);
                            }
                            if (lstADD002 != null && lstADD002.Count > 0)
                            {
                                db.ADD002.AddRange(lstADD002);
                            }
                            db.SaveChanges();
                        }
                    }
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update;
                    objResponseMsg.Status = objADD001.Status;
                    objResponseMsg.HeaderId = objADD001.HeaderId;
                    objResponseMsg.dataValue = objADD001.Title;
                    objResponseMsg.Revision = Convert.ToInt32(objADD001.RevNo);
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(fc["hdAuto"]))
                    { objADD001.DocumentName = generateNum(); }
                    else
                    {
                        objADD001.DocumentName = add001.DocumentName;
                    }
                    objADD001.RevNo = add001.RevNo;
                    objADD001.Description = add001.Description;

                    if (string.IsNullOrWhiteSpace(add001.Title))
                    {
                        objADD001.Title = add001.DocumentName;
                    }
                    else { objADD001.Title = add001.Title; }

                    objADD001.EffectiveDate = add001.EffectiveDate;
                    objADD001.ModelType = add001.ModelType;
                    objADD001.AIApprovalTaken = add001.AIApprovalTaken;
                    objADD001.AIApprovalRequired = add001.AIApprovalRequired;
                    objADD001.PEApprovalTaken = add001.PEApprovalTaken;
                    objADD001.PEApprovalRequired = add001.PEApprovalRequired;
                    objADD001.ApproveDate = add001.ApproveDate;
                    objADD001.DrawingSize = add001.DrawingSize;
                    objADD001.CustomerComment = add001.CustomerComment;
                    objADD001.PageSize = add001.PageSize;
                    objADD001.DOBApprovalTaken = add001.DOBApprovalTaken;
                    objADD001.DOBApprovalRequired = add001.DOBApprovalRequired;
                    objADD001.PageNo = add001.PageNo;
                    objADD001.Remarks = add001.Remarks;
                    objADD001.Comments = add001.Comments;
                    objADD001.Status = clsImplementationEnum.ADDStatus.Draft.GetStringValue();
                    objADD001.CreatedBy = objClsLoginInfo.UserName;
                    objADD001.CreatedOn = DateTime.Now;
                    db.ADD001.Add(objADD001);
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;
                    objResponseMsg.Status = objADD001.Status;
                    objResponseMsg.HeaderId = objADD001.HeaderId;
                    objResponseMsg.dataValue = objADD001.Title;
                    objResponseMsg.Revision = Convert.ToInt32(objADD001.RevNo);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Route Details
        public ActionResult GetRouteDataPartial(int headerid, string documentname, string routeno)
        {
            ViewBag.headerid = headerid;
            ViewBag.documentname = documentname;
            ViewBag.RouteNo = routeno;
            ADD001 objADD001 = new ADD001();
            if (headerid > 0)
            {
                objADD001 = db.ADD001.Where(x => x.HeaderId == headerid).FirstOrDefault();
            }
            return PartialView("_GetRouteHistory", objADD001);
        }

        public ActionResult LoadRouteData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string WhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                WhereCondition += " 1=1 and HeaderId=" + Convert.ToInt32(param.Headerid);
                if (param.IsVisible == true)
                {
                    WhereCondition += " and RouteNo <> '" + param.RouteNo + "' and ADDRevNo = " + Convert.ToInt32(param.RevNo);
                }

                //search Condition 
                string[] columnName = { "ADDRevNo", "RouteNo", "IsStopped", "CreatedBy", "CreatedOn" };
                WhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                var lstResult = db.SP_ADD_ROUTE_DATA(StartIndex, EndIndex, strSortOrder, WhereCondition).ToList();
                string submittedforapproval = clsImplementationEnum.ADDTaskStatus.SubmittedForApproval.GetStringValue();
                var data = (from uc in lstResult
                            select new[]
                           {
                              Convert.ToString(uc.ROW_NO),
                              Convert.ToString("R"+uc.ADDRevNo),
                              Convert.ToString(uc.RouteNo),
                              Convert.ToString(uc.IsStopped == true ? "Stopped" : ""),
                              Convert.ToString(uc.CreatedBy),
                              (uc.CreatedOn.HasValue ? uc.CreatedOn.Value.ToString("dd/MM/yyyy hh:mm tt") :  ""),
                              Convert.ToString(uc.HeaderId),
                              Convert.ToString(uc.Id),
                          }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = WhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region AssigneDeatils
        public ActionResult LoadTaskData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string WhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                WhereCondition += "1=1 and add2.HeaderId=" + Convert.ToInt32(param.Headerid) + " and add2.RouteNo = '" + param.RouteNo + "'";

                //search Condition 
                string[] columnName = { "add2.Assignee", "add2.Comments", "add2.TaskName", "add2.TaskDescription", "add2.TaskStatus", "add2.AssigneeComments", "add2.AssignedBy", "add2.AssignedOn" };
                WhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                var lstResult = db.SP_ADD_GET_TASK_DATA(StartIndex, EndIndex, strSortOrder, WhereCondition).ToList();
                string submittedforapproval = clsImplementationEnum.ADDTaskStatus.SubmittedForApproval.GetStringValue();
                var data = (from uc in lstResult
                            select new[]
                           {
                              Convert.ToString(uc.ROW_NO),
                              Convert.ToString(uc.Assignee),
                              Convert.ToString(uc.Comments),
                              Convert.ToString(uc.TaskName),
                              Convert.ToString(uc.TaskDescription),
                              Convert.ToString(uc.TaskStatus == submittedforapproval ? "" : uc.TaskStatus),
                              Convert.ToString(uc.AssigneeComments),
                              Convert.ToString(uc.AssignedBy),
                              (uc.AssignedOn.HasValue ? uc.AssignedOn.Value.ToString("dd/MM/yyyy hh:mm tt") :  "")
                          }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    //comment = lstResult.FirstOrDefault().Comments,
                    //taskname = lstResult.FirstOrDefault().TaskName,
                    //taskdesc = lstResult.FirstOrDefault().TaskDescription,
                    whereCondition = WhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Approve / Demote by reviewer Header
        [HttpPost]
        public ActionResult ApproveandAddAssignee(int headerId, string TeamMember, string comments)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            ADD001 objADD001 = new ADD001();
            try
            {
                string Status = clsImplementationEnum.ADDStatus.SubmittedForApproval.GetStringValue();
                if (headerId > 0)
                {
                    objADD001 = db.ADD001.Where(i => i.HeaderId == headerId).FirstOrDefault();

                    if (objADD001.Status == clsImplementationEnum.ADDStatus.SubmittedForReview.GetStringValue() || objADD001.Status == clsImplementationEnum.ADDStatus.ReturnByAssignee.GetStringValue())
                    {
                        //#region add assignee
                        //ADD002 objADD002 = new ADD002();
                        //var lstDesADD002 = db.ADD002.Where(x => x.HeaderId == objADD001.HeaderId).ToList();
                        //if (lstDesADD002 != null && lstDesADD002.Count > 0)
                        //{
                        //    db.ADD002.RemoveRange(lstDesADD002);
                        //    db.SaveChanges();
                        //}

                        //string Assignee = TeamMember;
                        //string[] arrayAssignee = Assignee.Split(',').ToArray();
                        //ADD002 newobjADD002 = db.ADD002.Where(x => x.HeaderId == objADD001.HeaderId).FirstOrDefault();
                        //List<ADD002> lstADD002 = new List<ADD002>();
                        //for (int i = 0; i < arrayAssignee.Length; i++)
                        //{
                        //    ADD002 objADD002Add = new ADD002();

                        //    objADD002Add.HeaderId = objADD001.HeaderId;
                        //    objADD002Add.RevNo = objADD001.RevNo;
                        //    objADD002Add.Comments = comments;
                        //    objADD002Add.TaskDescription = objADD001.Description;
                        //    objADD002Add.TaskName = objADD001.DocumentName;
                        //    objADD002Add.TaskStatus = clsImplementationEnum.ADDTaskStatus.SubmittedForApproval.GetStringValue();
                        //    objADD002Add.Assignee = arrayAssignee[i];
                        //    objADD002Add.CreatedBy = objClsLoginInfo.UserName;
                        //    objADD002Add.CreatedOn = DateTime.Now;
                        //    objADD002Add.AssignedBy = objClsLoginInfo.UserName;
                        //    objADD002Add.AssignedOn = DateTime.Now;
                        //    lstADD002.Add(objADD002Add);
                        //}
                        //if (lstADD002 != null && lstADD002.Count > 0)
                        //{
                        //    db.ADD002.AddRange(lstADD002);
                        //}
                        //db.SaveChanges();
                        //#endregion

                        objADD001.Status = Status;
                        objADD001.ReviewedBy = objClsLoginInfo.UserName;
                        objADD001.ReviewedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = string.Format(clsImplementationMessage.CommonMessages.SubmitDetailsto, objADD001.DocumentName, objADD001.Status);
                    }
                    else
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = string.Format(clsImplementationMessage.CommonMessages.doesnotSubmitDetails, objADD001.DocumentName, Status);
                    }
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }

            return Json(objResponseMsg);

        }
        [HttpPost]
        public ActionResult ReturnHeader(string Remarks, int? headerid)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string returnStatus = clsImplementationEnum.ADDStatus.ReturnByReviewer.GetStringValue();
            try
            {
                ADD001 objADD001 = db.ADD001.Where(u => u.HeaderId == headerid).SingleOrDefault();
                if (objADD001 != null)
                {
                    objADD001.Status = returnStatus;
                    objADD001.Remarks = Remarks;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = string.Format(clsImplementationMessage.CommonMessages.SubmitDetailsto, objADD001.DocumentName, objADD001.Status);
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Send for Approval
        [HttpPost]
        public ActionResult SendForApproval(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            ADD001 objADD001 = new ADD001();
            string[] arrStatus = { clsImplementationEnum.ADDStatus.Draft.GetStringValue() };
            try
            {
                string SubmittedForReview = clsImplementationEnum.ADDStatus.SubmittedForReview.GetStringValue();
                if (headerId > 0)
                {
                    objADD001 = db.ADD001.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    if (objADD001.Status == clsImplementationEnum.ADDStatus.Draft.GetStringValue() || objADD001.Status == clsImplementationEnum.ADDStatus.ReturnByReviewer.GetStringValue())
                    {
                        objADD001.Status = SubmittedForReview;
                        objADD001.SubmittedBy = objClsLoginInfo.UserName;
                        objADD001.SubmittedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = string.Format(clsImplementationMessage.CommonMessages.SubmitDetailsto, objADD001.DocumentName, objADD001.Status);
                    }
                    else
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = string.Format(clsImplementationMessage.CommonMessages.doesnotSubmitDetails, objADD001.DocumentName, SubmittedForReview);
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }

            return Json(objResponseMsg);

        }
        #endregion

        #region History
        [HttpPost]
        public ActionResult GetHistoryDetails(int headerId, string type)
        {
            ViewBag.IndexDataFor = type;
            ADD001 objADD001 = db.ADD001.Where(x => x.HeaderId == headerId).FirstOrDefault();
            return PartialView("_GetADDHistoryDetailsPartial", objADD001);
        }

        [HttpPost]
        public JsonResult LoadHistoryDetailData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                whereCondition += " AND addlg.HeaderId=" + Convert.ToInt32(param.Headerid);
                string[] columnName = { "DocumentName", "RevNo", "Description", "Title", "EffectiveDate" };
                #region Sorting
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion
                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                var lstResult = db.SP_ADD_GET_HISTORY_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstResult.Select(i => i.TotalCount).FirstOrDefault();


                var data = (from h in lstResult
                            select new[]
                           {
                                Convert.ToString(h.DocumentName),
                                Convert.ToString("R" + h.RevNo),
                                Convert.ToString(h.Title),
                                Convert.ToString(h.Status),
                                (h.EffectiveDate.HasValue ? h.EffectiveDate.Value.ToString("dd/MM/yyyy") : ""),
                                "<nobr><center>" +
                                         Helper.GenerateActionIcon(h.HeaderId, "View", "View Detail", "fa fa-eye", "", WebsiteURL + "/ADD/MaintainADD/ViewLogDetails/" + h.Id + "?uf=" + param.Department, false,true)
                               + "</center></nobr>",
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]
        public ActionResult ViewLogDetails(int? Id, string uf)
        {
            ADD001_Log objADD001 = new ADD001_Log();
            if (Id > 0)
            {
                objADD001 = db.ADD001_Log.Where(i => i.Id == Id).FirstOrDefault();
                ViewBag.type = uf;
                ViewBag.Title = "Advance Drawing Document";
            }
            return View(objADD001);
        }
        public ActionResult LoadLogTaskData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string WhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                WhereCondition += "1=1 and refId=" + Convert.ToInt32(param.Headerid) + " and RevNo =" + param.RevNo;
                //WhereCondition += "1=1 and refId=" + Convert.ToInt32(param.Headerid) + " and RouteNo = '" + param.RouteNo + "'";



                //search Condition 
                string[] columnName = { "Assignee", "Comments", "TaskName", "TaskDescription", "TaskStatus", "AssigneeComments", "AssignedBy", "AssignedOn" };
                WhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                var lstResult = db.SP_ADD_HISTORY_TASK_DATA(StartIndex, EndIndex, strSortOrder, WhereCondition).ToList();
                string submittedforapproval = clsImplementationEnum.ADDTaskStatus.SubmittedForApproval.GetStringValue();
                var data = (from uc in lstResult
                            select new[]
                           {
                              Convert.ToString(uc.ROW_NO),
                              Convert.ToString(uc.Assignee),
                              Convert.ToString(uc.Comments),
                              Convert.ToString(uc.TaskName),
                              Convert.ToString(uc.TaskDescription),
                              Convert.ToString(uc.TaskStatus == submittedforapproval ? "" : uc.TaskStatus),
                              Convert.ToString(uc.AssigneeComments),
                              Convert.ToString(uc.AssignedBy),
                              (uc.AssignedOn.HasValue ? uc.AssignedOn.Value.ToString("dd/MM/yyyy") :  "")
                          }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = WhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region common function
        public JsonResult GetSelectedTeamMemberValue(int headerId)
        {
            var result = db.ADD002.Where(s => s.HeaderId == headerId).Select(s => s.Assignee).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetSelectedLogTeamMemberValue(int Id)
        {
            var result = db.ADD002_Log.Where(s => s.RefId == Id).Select(s => s.Assignee).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAssignee(string search, string param)
        {
            try
            {
                var items = (from ath1 in db.ATH001
                             join li in db.COM003 on ath1.Employee equals li.t_psno
                             join ath4 in db.ATH004 on ath1.Role equals ath4.Id
                             where// li.t_psno != param && (li.t_psno.ToLower().Contains(search) || li.t_name.ToLower().Contains(search)) &&
                             li.t_actv == 1
                             select new
                             {
                                 id = li.t_psno,
                                 text = li.t_psno + "-" + li.t_name,
                             }).Distinct().ToList();

                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetNextDocumentNo()
        {
            clsHelper.ResponseMsgWithStatus obj = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string strRequestNo = generateNum();

                obj.Key = true;
                obj.Value = strRequestNo;
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                obj.Key = false;
                obj.Value = "Error in generate Document Number";
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
        }
        public string generateNum()
        {
            string requestdateformat = DateTime.Now.ToString("yyMMdd");
            int initialrequest = 1;
            string term = requestdateformat;
            var DocumentNo = db.ADD001.Where(x => x.DocumentName.Contains(term)).OrderByDescending(x => x.HeaderId).Select(x => x.DocumentName).FirstOrDefault();
            if (DocumentNo != null)
            {
                string doc = DocumentNo.Split('-')[1].Replace(requestdateformat, "");
                requestdateformat = "Doc-" + requestdateformat + (Convert.ToInt32(doc) + 1).ToString();
            }
            else
            {
                requestdateformat = "Doc-" + requestdateformat + initialrequest;
            }
            return requestdateformat;
        }
        #endregion

        #region Export Excel

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;


                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_ADD_GET_INDEX_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      DocumentName = Convert.ToString(uc.DocumentName),
                                      RevNo = Convert.ToString("R" + uc.RevNo),
                                      Title = Convert.ToString(uc.Title),
                                      Status = Convert.ToString(uc.Status),
                                      EffectiveDate = Convert.ToString(uc.EffectiveDate),


                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_ADD_GET_TASK_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();


                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from c in lst
                                  select new
                                  {
                                      SrNo = Convert.ToString(c.ROW_NO),
                                      Assignee = Convert.ToString(c.Assignee),
                                      TaskStatus = Convert.ToString(c.TaskStatus),
                                      AssigneeComments = Convert.ToString(c.AssigneeComments),
                                      AssignedBy = Convert.ToString(c.AssignedBy),
                                      AssignedOn = Convert.ToString(c.AssignedOn)

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.ROUTE.GetStringValue())
                {
                    var lst = db.SP_ADD_ROUTE_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();


                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from c in lst
                                  select new
                                  {
                                      SrNo = Convert.ToString(c.ROW_NO),
                                      ADDRevNo = Convert.ToString("R" + c.ADDRevNo),
                                      RouteNo = Convert.ToString(c.RouteNo),
                                      CreatedBy = Convert.ToString(c.CreatedBy),
                                      CreatedOn = Convert.ToString(c.CreatedOn)

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {
                    var lst = db.SP_ADD_GET_HISTORY_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();


                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from c in lst
                                  select new
                                  {
                                      SrNo = Convert.ToString(c.DocumentName),
                                      RevNo = Convert.ToString("R" + c.RevNo),
                                      Title = Convert.ToString(c.Title),
                                      Status = Convert.ToString(c.Status),
                                      EffectiveDate = Convert.ToString(c.EffectiveDate)

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYLINES.GetStringValue())
                {
                    var lst = db.SP_ADD_HISTORY_TASK_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();


                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from c in lst
                                  select new
                                  {
                                      SrNo = Convert.ToString(c.Assignee),
                                      TaskStatus = Convert.ToString(c.TaskStatus),
                                      AssigneeComments = Convert.ToString(c.AssigneeComments),
                                      AssignedBy = Convert.ToString(c.AssignedBy),
                                      AssignedOn = Convert.ToString(c.AssignedOn)

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}