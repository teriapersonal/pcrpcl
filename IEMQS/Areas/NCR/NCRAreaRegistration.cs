﻿using System.Web.Mvc;

namespace IEMQS.Areas.NCR
{
    public class NCRAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "NCR";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "NCR_default",
                "NCR/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}