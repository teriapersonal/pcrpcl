﻿using System.Web.Mvc;

namespace IEMQS.Areas.RCC
{
    public class RCCAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "RCC";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "RCC_default",
                "RCC/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}