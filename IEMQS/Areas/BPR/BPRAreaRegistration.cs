﻿using System.Web.Mvc;

namespace IEMQS.Areas.BPR
{
    public class BPRAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "BPR";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "BPR_default",
                "BPR/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}