﻿using System.Web.Mvc;

namespace IEMQS.Areas.TP
{
    public class TPAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "TP";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "TP_default",
                "TP/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}