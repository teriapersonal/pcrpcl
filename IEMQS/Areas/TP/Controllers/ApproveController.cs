﻿using IEMQS.Areas.MSW.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.TP.Controllers
{
    public class ApproveController : clsBase
    {
        [SessionExpireFilter]
        // GET: TP/Approve
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult GetHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("TPApproveGridPartial");
        }

        //public ActionResult ApproveSelectedTP(string strHeaderIds, bool hasAttachments, Dictionary<string, string> Attach)
       public ActionResult ApproveSelectedTP(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
                for (int i = 0; i < arrayHeaderIds.Length; i++)
                {
                    int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                    if (HeaderId > 0)
                    {
                        db.SP_TP_APPROVE(HeaderId, objClsLoginInfo.UserName);
                        TLP001 objTLP001 = db.TLP001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                        //var folderPath = "TLP001/" + objTLP001.HeaderId + "/R" + objTLP001.RevNo.ToString();
                        //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);



                        int newId = db.TLP001_Log.Where(q => q.HeaderId == HeaderId).OrderByDescending(q => q.ApprovedOn).FirstOrDefault().Id;
                        Manager.UpdatePDN002(objTLP001.HeaderId, objTLP001.Status, objTLP001.RevNo, objTLP001.Project, objTLP001.Document, newId);
                        int? revNoNull = (from a in db.TLP001
                                          where a.HeaderId == HeaderId
                                          select a.RevNo).FirstOrDefault();
                        int revNo = (revNoNull.HasValue) ? revNoNull.Value : 0;
                        //clsUpload.CopyFolderContents("TLP001/" + HeaderId + "/R" + revNo, "TLP001/" + HeaderId + "/R" + (revNo + 1).ToString());
                        #region Copy Reference document
                        string referenceDoc = "TLP001//" + HeaderId + "//R" + revNo;
                        string newfolderPath = "TLP001//" + HeaderId + "//R" + (revNo+1).ToString();
                        //(new clsFileUpload()).CopyFolderContentsAsync(referenceDoc, newfolderPath);

                        Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                        _objFUC.CopyDataOnFCSServerAsync(referenceDoc, newfolderPath, referenceDoc, HeaderId, newfolderPath, HeaderId, IEMQS.DESServices.CommonService.GetUseIPConfig, IEMQS.DESServices.CommonService.objClsLoginInfo.UserName, 0);

                        #endregion


                        string DocumentName = string.Empty;
                        var ObjPDN002 = db.PDN002.Where(n => n.RefId == objTLP001.HeaderId && n.DocumentNo == objTLP001.Document).FirstOrDefault();
                        if (ObjPDN002 != null)
                        {
                            DocumentName = ObjPDN002.Description;
                        }
                        else
                        {
                            DocumentName = clsImplementationEnum.PlanList.Technical_Process.GetStringValue();
                        }

                        #region Send Notification
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(),
                                                        objTLP001.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objTLP001.Project, DocumentName, objTLP001.RevNo.Value.ToString(), objTLP001.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Technical_Process.GetStringValue(), objTLP001.HeaderId.ToString(), false),
                                                        objTLP001.CreatedBy);
                        #endregion
                    }
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.QCPMessages.Approve.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult LoadTPApproveHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue() + "') and ApprovedBy=" + user+"";
                }
                else
                {
                    strWhere += "1=1";
                }
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
              //  strWhere += " and ApprovedBy=" + user;

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += "and (Project like '%" + param.sSearch
                         + "%' or Document like '%" + param.sSearch
                         + "%' or Customer like '%" + param.sSearch
                         + "%' or RevNo like '%" + param.sSearch
                         + "%' or CDD like '%" + param.sSearch
                         + "%' or Product like '%" + param.sSearch
                         + "%' or ProcessLicensor like '%" + param.sSearch
                         + "%' or Status like '%" + param.sSearch + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_TP_GET_HEADERDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                           Convert.ToString(uc.HeaderId),
                           Convert.ToString(GetCodeAndNameByProject(uc.Project)),
                           Convert.ToString(uc.Document),
                           Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Project)),
                           uc.CDD == null || uc.CDD.Value == DateTime.MinValue ? "NA" : uc.CDD.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                           Convert.ToString(uc.Product),
                           Convert.ToString(uc.ProcessLicensor),
                           Convert.ToString("R"+uc.RevNo),
                           Convert.ToString(uc.Status),
                           Convert.ToString(uc.SubmittedBy),
                           uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                           Convert.ToString(uc.ApprovedBy),
                           uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                           Convert.ToString(uc.HeaderId),
                           Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public string GetCodeAndNameByProject(string project)
        {
            var projdesc = db.COM001.Where(i => i.t_cprj == project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            return projdesc;
        }
        [SessionExpireFilter]
        public ActionResult TPApproveDetail(int Id = 0)
        {
            TLP001 objTLP001 = null;
            if (Id > 0)
            {
                objTLP001 = db.TLP001.Where(x => x.HeaderId == Id).FirstOrDefault();
            }
            else
            {
                objTLP001 = new TLP001();
            }

            var user = objClsLoginInfo.UserName;

            var lstProjectDesc = (from a in db.COM001
                                  select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).ToList();
            ViewBag.Project = new SelectList(lstProjectDesc, "projectCode", "projectDescription");
            string name = (from a in db.COM003
                           where a.t_psno == objClsLoginInfo.UserName && a.t_actv == 1
                           select a.t_name).FirstOrDefault();


            ViewBag.CreatedBy = objClsLoginInfo.UserName + " - " + name;
            //List<string> lstProCat = clsImplementationEnum.getProductCategory().ToList();
            //ViewBag.lstProCat = lstProCat.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            //ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments("TLP001/" + objTLP001.HeaderId + "/R" + objTLP001.RevNo));

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.lstyesno = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            var project = (from a in db.TLP001
                           where a.HeaderId == Id
                           select a.Project).FirstOrDefault();

            ViewBag.Customer = Manager.GetCustomerCodeAndNameByProject(project);

            var approver = (from t1 in db.ATH001
                            join t2 in db.COM003 on t1.Employee equals t2.t_psno
                            where t2.t_actv == 1
                            select new { t1.Employee, Desc = t2.t_psno + " - " + t2.t_name }).Distinct().ToList();
            ViewBag.approver = new SelectList(approver, "Employee", "Desc");

            if (Id > 0)
            {
                ViewBag.Customer = Manager.GetCustomerCodeAndNameByProject(project);
                objTLP001 = db.TLP001.Where(x => x.HeaderId == Id).FirstOrDefault();               
                var PlanningDinID = db.PDN002.Where(x => x.RefId == Id && x.DocumentNo == objTLP001.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;
                ViewBag.Revoke = Manager.IsRevokeApplicable(PlanningDinID, Id, clsImplementationEnum.PlanList.Technical_Process.GetStringValue());
                if (objTLP001.ApprovedBy != null)
                {
                    if (objTLP001.ApprovedBy.Trim() != objClsLoginInfo.UserName.Trim())
                    {
                        ViewBag.IsDisplayMode = true;
                    }
                    else
                    {
                        ViewBag.IsDisplayMode = false;
                    }
                }
                else
                {
                    ViewBag.IsDisplayMode = true;
                }
            }
            return View(objTLP001);
        }

        [HttpPost]
        public ActionResult getHeaderId(string project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int Headerid = (from a in db.TLP001
                                where a.Project == project
                                select a.HeaderId).FirstOrDefault();
                string status = (from a in db.TLP001
                                 where a.Project == project
                                 select a.Status).FirstOrDefault();
                objResponseMsg.Key = true;
                objResponseMsg.Value = Headerid.ToString() + "|" + status;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SaveTPHeader(TLP001 tlp001, FormCollection fc)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                string s1, s2;
                s1 = fc["txtdoc"].ToString();
                s2 = s1.Substring(0, 3);
                s2 = s1.Substring(s1.Length - 2);
                // bool d1;
                //if (fc["ddlJob"].ToString() == "Yes")
                //{
                //    d1 = true;
                //}
                //else
                //{
                //    d1 = false;
                //}

                if (!string.IsNullOrWhiteSpace(fc["hfHeaderId"].ToString()) && Convert.ToInt32(fc["hfHeaderId"].ToString()) > 0)
                {
                    int hid = Convert.ToInt32(fc["hfHeaderId"].ToString());
                    TLP001 objTLP001 = db.TLP001.Where(x => x.HeaderId == hid).FirstOrDefault();
                    string rev = Regex.Replace(fc["txtRev"].ToString(), "[^0-9]+", string.Empty);
                    objTLP001.Project = fc["hfProject"].ToString();

                    objTLP001.DocumentNo = Convert.ToInt32(s2);
                    objTLP001.Document = fc["txtdoc"].ToString();
                    objTLP001.Customer = fc["txtCust"].ToString().Split('-')[0];
                    if (objTLP001.Status == clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue())
                    {
                        objTLP001.RevNo = Convert.ToInt32(objTLP001.RevNo) + 1;
                        objTLP001.Status = clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue();
                    }
                    if (string.IsNullOrWhiteSpace(fc["txtCdd"].ToString()) || fc["txtCdd"].ToString() == "01-01-0001" || fc["txtCdd"].ToString() == "01/01/0001")
                    {
                        objTLP001.CDD = null;
                    }
                    else
                    {
                        objTLP001.CDD = Convert.ToDateTime(fc["txtCdd"].ToString());
                    }
                    objTLP001.Product = fc["ddlCat"].ToString();
                    objTLP001.ProcessLicensor = fc["txtlicence"].ToString();
                    objTLP001.Attachment = "";
                    objTLP001.JobDescription = fc["ddlJob"].ToString();
                    objTLP001.EditedBy = objClsLoginInfo.UserName;
                    objTLP001.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CTQMessages.Update.ToString();
                    objResponseMsg.Revision = "R" + objTLP001.RevNo.ToString();
                    objResponseMsg.status = objTLP001.Status;
                    Manager.UpdatePDN002(objTLP001.HeaderId, objTLP001.Status, objTLP001.RevNo, objTLP001.Project, objTLP001.Document);
                }
                else
                {
                    string project = fc["txtProject"].ToString();
                    string rev = Regex.Replace(fc["txtRev"].ToString(), "[^0-9]+", string.Empty);
                    TLP001 objTLP001 = new TLP001();
                    objTLP001.Project = fc["txtProject"].ToString();
                    objTLP001.DocumentNo = Convert.ToInt32(s2);
                    objTLP001.Document = fc["txtdoc"].ToString();
                    objTLP001.Customer = fc["txtCust"].ToString().Split('-')[0];
                    objTLP001.RevNo = Convert.ToInt32(rev);
                    objTLP001.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                    if (string.IsNullOrWhiteSpace(fc["txtCdd"].ToString()) || fc["txtCdd"].ToString() == "01-01-0001" || fc["txtCdd"].ToString() == "01/01/0001")
                    {
                        objTLP001.CDD = null;
                    }
                    else
                    {
                        objTLP001.CDD = Convert.ToDateTime(fc["txtCdd"].ToString());
                    }
                    objTLP001.Product = fc["ddlCat"].ToString();
                    objTLP001.ProcessLicensor = fc["txtlicence"].ToString();
                    objTLP001.Attachment = "";
                    objTLP001.JobDescription = fc["ddlJob"].ToString();
                    objTLP001.CreatedBy = objClsLoginInfo.UserName;
                    objTLP001.CreatedOn = DateTime.Now;
                    db.TLP001.Add(objTLP001);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CTQMessages.Insert.ToString();
                    objResponseMsg.Revision = "R" + objTLP001.RevNo.ToString();
                    objResponseMsg.status = objTLP001.Status;
                    Manager.UpdatePDN002(objTLP001.HeaderId, objTLP001.Status, objTLP001.RevNo, objTLP001.Project, objTLP001.Document);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult ReturnSelectedTP(string strHeaderIds, string remarks, bool hasAttachments, Dictionary<string, string> Attach)
       public ActionResult ReturnSelectedTP(string strHeaderIds, string remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
                for (int i = 0; i < arrayHeaderIds.Length; i++)
                {
                    int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                    TLP001 objTLP001 = db.TLP001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    objTLP001.Status = clsImplementationEnum.CTQStatus.Returned.GetStringValue();
                    objTLP001.ApprovedBy = objClsLoginInfo.UserName;
                    objTLP001.ApprovedOn = DateTime.Now;
                    objTLP001.ReturnRemark = remarks;
                    db.SaveChanges();

                 
                    Manager.UpdatePDN002(objTLP001.HeaderId, objTLP001.Status, objTLP001.RevNo, objTLP001.Project, objTLP001.Document);

                    string DocumentName = string.Empty;
                    var ObjPDN002 = db.PDN002.Where(n => n.RefId == objTLP001.HeaderId && n.DocumentNo == objTLP001.Document).FirstOrDefault();
                    if (ObjPDN002 != null)
                    {
                        DocumentName = ObjPDN002.Description;
                    }
                    else
                    {
                        DocumentName = clsImplementationEnum.PlanList.Technical_Process.GetStringValue();
                    }

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(),
                                                        objTLP001.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objTLP001.Project, DocumentName, objTLP001.RevNo.Value.ToString(), objTLP001.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Technical_Process.GetStringValue(), objTLP001.HeaderId.ToString(), false),
                                                        objTLP001.CreatedBy);
                    #endregion
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.QCPMessages.Return.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult getCodeValue(string project, string approver, string CreatedBy)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                string name = (from a in db.COM003
                               where a.t_psno == CreatedBy && a.t_actv == 1
                               select a.t_name).FirstOrDefault();

                objResponseMsg.CreatedBy = CreatedBy + " - " + name;

                objResponseMsg.projdesc = db.COM001.Where(i => i.t_cprj == project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objResponseMsg.appdesc = db.COM003.Where(i => i.t_psno == approver).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        //public ActionResult SaveHeader(TLP001 tlp001, bool hasAttachments, Dictionary<string, string> Attach)
        public ActionResult SaveHeader(TLP001 tlp001)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                string s1, s2;
                s1 = tlp001.Document.ToString();
                s2 = s1.Substring(0, 3);
                s2 = s1.Substring(s1.Length - 2);

                //int NewHeaderId = 0;
                if (tlp001.HeaderId > 0)
                {
                    TLP001 objTLP001 = db.TLP001.Where(x => x.HeaderId == tlp001.HeaderId).FirstOrDefault();
                    if (objTLP001 != null)
                    {
                        objTLP001.Product = tlp001.Product;
                        objTLP001.JobDescription = tlp001.JobDescription;
                        objTLP001.ProcessLicensor = tlp001.ProcessLicensor;
                        objTLP001.ApprovedBy = tlp001.ApprovedBy.Split('-')[0].ToString().Trim();
                        objTLP001.EditedBy = objClsLoginInfo.UserName;
                        objTLP001.EditedOn = DateTime.Now;
                        if (objTLP001.Status == clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue())
                        {
                            objTLP001.RevNo = Convert.ToInt32(objTLP001.RevNo) + 1;
                            objTLP001.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                        }
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.PlanMessages.Update.ToString();
                        objResponseMsg.Revision = objTLP001.RevNo.ToString();
                        Manager.UpdatePDN002(objTLP001.HeaderId, objTLP001.Status, objTLP001.RevNo, objTLP001.Project, objTLP001.Document);
                        objResponseMsg.Headerid = Convert.ToInt32(objTLP001.HeaderId);
                        objResponseMsg.Revision = "R" + objTLP001.RevNo;

                        // var folderPath = "TLP001/" + tlp001.HeaderId + "/R" + objTLP001.RevNo;
                        //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Process Details not available.";
                    }
                }
                else
                {
                    TLP001 objTLP001 = db.TLP001.Add(new TLP001
                    {
                        Project = tlp001.Project.Split('-')[0].ToString().Trim(),
                        DocumentNo = Convert.ToInt32(s2),
                        Document = tlp001.Document,
                        Customer = tlp001.Customer.Split('-')[0].ToString().Trim(),
                        RevNo = 0,
                        Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                        CDD = Helper.ToNullIfTooEarlyForDb(tlp001.CDD),
                        Product = tlp001.Product,
                        ProcessLicensor = tlp001.ProcessLicensor,
                        JobDescription = tlp001.JobDescription,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now,
                        ApprovedBy = tlp001.ApprovedBy.Split('-')[0].ToString().Trim()
                    });
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PlanMessages.Insert.ToString();
                    objResponseMsg.Revision = objTLP001.RevNo.ToString();
                    Manager.UpdatePDN002(objTLP001.HeaderId, objTLP001.Status, objTLP001.RevNo, objTLP001.Project, objTLP001.Document);
                  
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetHistoryView(int headerid = 0)
        {
            TLP001_Log objTLP001 = new TLP001_Log();
            Session["HeaderId"] = Convert.ToInt32(headerid);
            return PartialView("getHistoryPartial", objTLP001);
        }

        [HttpPost]
        public JsonResult LoadTPHeaderHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;

                string strWhere = string.Empty;

                //strWhere += "CreatedBy = " + user + " or ApprovedBy = " + user;
                strWhere += "HeaderId = " + Convert.ToInt32(Session["HeaderId"]) + "";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += "and (Project like '%" + param.sSearch
                         + "%' or Document like '%" + param.sSearch
                         + "%' or Customer like '%" + param.sSearch
                         + "%' or RevNo like '%" + param.sSearch
                         + "%' or CDD like '%" + param.sSearch
                         + "%' or Product like '%" + param.sSearch
                         + "%' or ProcessLicensor like '%" + param.sSearch
                         + "%' or Status like '%" + param.sSearch + "%')";
                }
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_TP_GET_HISTORY_HEADERDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(GetCodeAndNameByProject(uc.Project)),
                            Convert.ToString(uc.Document),
                            Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Project)),
                            // Convert.ToDateTime(uc.CDD).ToString("dd/MM/yyyy"),
                            //Convert.ToString(uc.CDD),
                            Convert.ToString(uc.Product),
                            Convert.ToString(uc.ProcessLicensor),
                            Convert.ToString("R"+uc.RevNo),
                            Convert.ToString(uc.Status),
                            Convert.ToString(uc.SubmittedBy),
                            uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                            Convert.ToString(uc.ApprovedBy),
                            uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),    
                            Convert.ToString(uc.HeaderId),
                            Convert.ToString(uc.Id)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [SessionExpireFilter]
        public ActionResult getHistoryDetail(int Id = 0)
        {
            TLP001_Log objTLP001 = new TLP001_Log();
            var user = objClsLoginInfo.UserName;

            var lstProjectDesc = (from a in db.COM001
                                  select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).ToList();
            ViewBag.Project = new SelectList(lstProjectDesc, "projectCode", "projectDescription");
            //ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments("TLP001/" + objTLP001.HeaderId + "/" + objTLP001.RevNo));
            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.lstyesno = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            List<string> lstProCat = clsImplementationEnum.getProductCategory().ToList();
            ViewBag.lstProCat = lstProCat.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            var approver = (from t1 in db.ATH001
                            join t2 in db.COM003 on t1.Employee equals t2.t_psno
                            where t2.t_actv == 1
                            select new { t1.Employee, Desc = t2.t_psno + " - " + t2.t_name }).Distinct().ToList();
            ViewBag.approver = new SelectList(approver, "Employee", "Desc");
            if (Id > 0)
            {
                var project = (from a in db.TLP001_Log
                               where a.Id == Id
                               select a.Project).FirstOrDefault();
                ViewBag.Customer = Manager.GetCustomerCodeAndNameByProject(project);
                objTLP001 = db.TLP001_Log.Where(x => x.Id == Id).FirstOrDefault();
                if (objTLP001.CreatedBy != objClsLoginInfo.UserName && objTLP001.ApprovedBy != objClsLoginInfo.UserName)
                {
                    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                }
            }
            var urlPrefix = Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.LastIndexOf('=') + 1);
            ViewBag.RevPrev = (db.TLP001_Log.Any(q => (q.HeaderId == objTLP001.HeaderId && q.RevNo == (objTLP001.RevNo - 1))) ? urlPrefix + db.TLP001_Log.Where(q => (q.HeaderId == objTLP001.HeaderId && q.RevNo == (objTLP001.RevNo - 1))).FirstOrDefault().Id : null);
            ViewBag.RevNext = (db.TLP001_Log.Any(q => (q.HeaderId == objTLP001.HeaderId && q.RevNo == (objTLP001.RevNo + 1))) ? urlPrefix + db.TLP001_Log.Where(q => (q.HeaderId == objTLP001.HeaderId && q.RevNo == (objTLP001.RevNo + 1))).FirstOrDefault().Id : null);
            return View(objTLP001);
        }
        [HttpPost]
        public ActionResult RevokeDocument(int HeaderId, int pdinId, string Remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (Manager.IsRevokeApplicable(pdinId, HeaderId, clsImplementationEnum.PlanList.Technical_Process.GetStringValue()))
                {
                    objResponseMsg.Key = Manager.RevokePDINDocument(HeaderId, clsImplementationEnum.PlanList.Technical_Process, Remarks);
                    if (objResponseMsg.Key)
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revoke;
                    }
                    else
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.UnSuccess;
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.pdnRevokeMessage;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult createRevison(int headerid)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int? revison = (from a in db.EMT001
                                where a.HeaderId == headerid
                                select a.RevNo).FirstOrDefault();
                revison = revison + 1;
                EMT001 objEMT001 = db.EMT001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                objEMT001.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                objEMT001.RevNo = revison;
                objEMT001.EditedBy = objClsLoginInfo.UserName;
                objEMT001.EditedOn = DateTime.Now;
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = revison.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #region Export Excel
        //Excel funtionality
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                { //header grid data
                    var lst = db.SP_TP_GET_HEADERDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Project = Convert.ToString(GetCodeAndNameByProject(uc.Project)),
                                      Document = uc.Document,
                                      Customer = Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Project)),
                                      CDD = uc.CDD == null || uc.CDD.Value == DateTime.MinValue ? "NA" : uc.CDD.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      Product = uc.Product,
                                      ProcessLicensor = Convert.ToString(uc.ProcessLicensor),
                                      RevNo = Convert.ToString("R" + uc.RevNo),
                                      Status = Convert.ToString(uc.Status),
                                      SubmittedBy = Convert.ToString(uc.SubmittedBy),
                                      SubmittedOn = uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ApprovedBy = Convert.ToString(uc.ApprovedBy),
                                      ApprovedOn = uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {  //line grid data

                    var lst = db.SP_TP_GET_HISTORY_HEADERDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();


                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Project = Convert.ToString(GetCodeAndNameByProject(uc.Project)),
                                      Document = uc.Document,
                                      Customer = Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Project)),
                                      CDD = uc.CDD == null || uc.CDD.Value == DateTime.MinValue ? "NA" : uc.CDD.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      Product = uc.Product,
                                      ProcessLicensor = Convert.ToString(uc.ProcessLicensor),
                                      RevNo = Convert.ToString("R" + uc.RevNo),
                                      Status = Convert.ToString(uc.Status),
                                      SubmittedBy = Convert.ToString(uc.SubmittedBy),
                                      SubmittedOn = uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ApprovedBy = Convert.ToString(uc.ApprovedBy),
                                      ApprovedOn = uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
    public class ResponceMsgWithStatus : clsHelper.ResponseMsg
    {
        public string status;
        public string Revision;
        public string projdesc;
        public string appdesc;
        public string CreatedBy;
        public int Headerid;
    }
}