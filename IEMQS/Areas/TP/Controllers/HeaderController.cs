﻿using IEMQS.Areas.MSW.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using IEMQS.Areas.Utility.Models;
using System.Globalization;

namespace IEMQS.Areas.TP.Controllers
{
    public class HeaderController : clsBase
    {
        // GET: TP/Header
        [SessionExpireFilter]

        public ActionResult Index()
        {

            return View();
        }
        [SessionExpireFilter]
        public ActionResult TPDetail(int Id = 0)
        {
            TLP001 objTLP001 = null;
            if (Id > 0)
            {
                objTLP001 = db.TLP001.Where(x => x.HeaderId == Id).FirstOrDefault();
                ViewBag.Status = objTLP001.Status;
            }
            else
            {
                objTLP001 = new TLP001();
                ViewBag.Status = "Draft";
            }
            var user = objClsLoginInfo.UserName;

            var lstProjectDesc = (from a in db.COM001
                                  select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).ToList();
            ViewBag.Project = new SelectList(lstProjectDesc, "projectCode", "projectDescription");

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.lstyesno = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            string name = (from a in db.COM003
                           where a.t_psno == objClsLoginInfo.UserName && a.t_actv == 1
                           select a.t_name).FirstOrDefault();


            ViewBag.CreatedBy = objClsLoginInfo.UserName + " - " + name;
            //List<string> lstProCat = clsImplementationEnum.getProductCategory().ToList();
            //ViewBag.lstProCat = lstProCat.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            //ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments("TLP001/" + objTLP001.HeaderId + "/R" + objTLP001.RevNo));

            var approver = (from t1 in db.ATH001
                            join t2 in db.COM003 on t1.Employee equals t2.t_psno
                            where t2.t_actv == 1
                            select new { t1.Employee, Desc = t2.t_psno + " - " + t2.t_name }).Distinct().ToList();
            ViewBag.approver = new SelectList(approver, "Employee", "Desc");

            if (Id > 0)
            {
                var project = (from a in db.TLP001
                               where a.HeaderId == Id
                               select a.Project).FirstOrDefault();
                ViewBag.Customer = Manager.GetCustomerCodeAndNameByProject(project);
                objTLP001 = db.TLP001.Where(x => x.HeaderId == Id).FirstOrDefault();
                //if (objTLP001.CreatedBy != objClsLoginInfo.UserName )
                //{
                //    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                //}
                var PlanningDinID = db.PDN002.Where(x => x.RefId == Id && x.DocumentNo == objTLP001.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;

                ViewBag.IsReviseBtnEnabled = Manager.IsReviseEnabled(PlanningDinID, objTLP001.HeaderId, objTLP001.Document);
                ViewBag.DocMessage = clsImplementationMessage.CommonMessages.DocMessage.ToString();
            }
            return View(objTLP001);
        }
        public ActionResult ShowHistoryTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();

            model.TimelineTitle = " Technical Process TimeLine";
            model.Title = "PDinDoc";
            if (HeaderId > 0)
            {

                TLP001_Log objTLP001_Log = db.TLP001_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objTLP001_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objTLP001_Log.CreatedBy) : null;
                model.CreatedOn = objTLP001_Log.CreatedOn;
                model.EditedBy = objTLP001_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(objTLP001_Log.EditedBy) : null;
                model.EditedOn = objTLP001_Log.EditedOn;
                model.SubmittedBy = objTLP001_Log.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objTLP001_Log.SubmittedBy) : null;
                model.SubmittedOn = objTLP001_Log.SubmittedOn;

                model.ApprovedBy = objTLP001_Log.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objTLP001_Log.ApprovedBy) : null;
                model.ApprovedOn = objTLP001_Log.ApprovedOn;

            }
            else
            {

                TLP001_Log objTLP001_Log = db.TLP001_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objTLP001_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objTLP001_Log.CreatedBy) : null;
                model.CreatedOn = objTLP001_Log.CreatedOn;
                model.EditedBy = objTLP001_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(objTLP001_Log.EditedBy) : null;
                model.EditedOn = objTLP001_Log.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.Title = "PDinDoc";
            model.TimelineTitle = "Technical Process Timeline";

            if (HeaderId > 0)
            {
                TLP001 objTLP001 = db.TLP001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objTLP001.CreatedBy != null ? Manager.GetUserNameFromPsNo(objTLP001.CreatedBy) : null;
                model.CreatedOn = objTLP001.CreatedOn;
                model.EditedBy = objTLP001.EditedBy != null ? Manager.GetUserNameFromPsNo(objTLP001.EditedBy) : null;
                model.EditedOn = objTLP001.EditedOn;
                model.SubmittedBy = objTLP001.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objTLP001.SubmittedBy) : null;
                model.SubmittedOn = objTLP001.SubmittedOn;
                model.ApprovedBy = objTLP001.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objTLP001.ApprovedBy) : null;
                model.ApprovedOn = objTLP001.ApprovedOn;

            }
            else
            {
                TLP001 objTLP001 = db.TLP001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objTLP001.CreatedBy != null ? Manager.GetUserNameFromPsNo(objTLP001.CreatedBy) : null;
                model.CreatedOn = objTLP001.CreatedOn;
                model.EditedBy = objTLP001.EditedBy != null ? Manager.GetUserNameFromPsNo(objTLP001.EditedBy) : null;
                model.EditedOn = objTLP001.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        //[HttpPost]
        //public ActionResult UploadFDR(string project, bool hasAttachments, Dictionary<string, string> Attach)
        //{
        //    clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
        //    try
        //    {
        //        int Headerid = (from a in db.TLP001
        //                        where a.Project == project
        //                        select a.HeaderId).FirstOrDefault();
        //        int? revNoNull = (from a in db.TLP001
        //                          where a.Project == project
        //                          select a.RevNo).FirstOrDefault();
        //        int revNo = (revNoNull.HasValue) ? revNoNull.Value : 0;
        //        var folderPath = "TLP001/" + Headerid + "/R" + revNo;
        //        Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        objResponseMsg.Key = false;
        //        objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
        //    }
        //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //}
        [HttpPost]
        public ActionResult GetHeaderData(string project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var ExistsPrject = db.COM001.Any(x => x.t_cprj == project);
                if (ExistsPrject == false)
                {
                    objResponseMsg.Key = false;
                }
                else
                {
                    int RevNum, docNum;
                    string customer = Manager.GetCustomerCodeAndNameByProject(project);
                    var Exists = db.TLP001.Any(x => x.RevNo == null && x.Project == project);
                    if (Exists == false)
                    {
                        RevNum = 0;
                    }
                    else
                    {
                        int? revNo = (from rev in db.TLP001
                                      where rev.Project == project
                                      select rev.RevNo).FirstOrDefault();
                        RevNum = Convert.ToInt32(revNo) + 1;
                    }
                    var Exists2 = db.TLP001.Any(x => x.Project == project);
                    if (Exists2 == false)
                    {
                        docNum = 21;
                    }
                    else
                    {
                        int? docno = (from doc in db.TLP001
                                      where doc.Project == project
                                      select doc.DocumentNo).FirstOrDefault();
                        docNum = Convert.ToInt32(docno) + 1;
                    }
                    //var contract = (from a in db.COM005
                    //                where a.t_sprj == project
                    //                select a.t_cono).FirstOrDefault();

                    //var ccdd = (from a in db.COM008
                    //            where a.t_cono == contract
                    //            select a.t_ccdd).FirstOrDefault();

                    project = project.Split('-')[0].ToString().Trim();
                    var ccdd = Manager.GetContractWiseCdd(project);

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = customer + "|" + RevNum + "|" + ccdd + "|" + docNum;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult getHeaderId(string project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int Headerid = (from a in db.TLP001
                                where a.Project == project
                                select a.HeaderId).FirstOrDefault();
                string status = (from a in db.TLP001
                                 where a.Project == project
                                 select a.Status).FirstOrDefault();
                objResponseMsg.Key = true;
                objResponseMsg.Value = Headerid.ToString() + "|" + status;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        //public JsonResult sentForApproval(string Approver, int headerid, bool hasAttachments, Dictionary<string, string> Attach)
        public JsonResult sentForApproval(string Approver, int headerid)
        {
            //clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                if (headerid > 0)
                {
                    Approver = Approver.Split('-')[0].Trim();
                    if (Approver.Trim() == objClsLoginInfo.UserName)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.SMessage.ToString();
                    }
                    else
                    {
                        TLP001 objTLP001 = db.TLP001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                        objTLP001.Status = clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue();
                        objTLP001.ApprovedBy = Approver.Trim();
                        objTLP001.SubmittedBy = objClsLoginInfo.UserName;
                        objTLP001.SubmittedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.AMessage.ToString();
                        var folderPath = "TLP001/" + objTLP001.HeaderId + "/R" + objTLP001.RevNo.ToString();
                        // Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                        objResponseMsg.Headerid = objTLP001.HeaderId;
                        objResponseMsg.Revision = "R" + objTLP001.RevNo;
                        Manager.UpdatePDN002(objTLP001.HeaderId, objTLP001.Status, objTLP001.RevNo, objTLP001.Project, objTLP001.Document);

                        string DocumentName = string.Empty;
                        var ObjPDN002 = db.PDN002.Where(i => i.RefId == objTLP001.HeaderId && i.DocumentNo == objTLP001.Document).FirstOrDefault();
                        if (ObjPDN002 != null)
                        {
                            DocumentName = ObjPDN002.Description;
                        }
                        else
                        {
                            DocumentName = clsImplementationEnum.PlanList.Technical_Process.GetStringValue();
                        }
                        #region Send Notification
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PLNG1.GetStringValue(),
                                                            objTLP001.Project,
                                                            "",
                                                            "",
                                                            Manager.GetPDINDocumentNotificationMsg(objTLP001.Project, DocumentName, objTLP001.RevNo.Value.ToString(), objTLP001.Status),
                                                            clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                            Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Technical_Process.GetStringValue(), objTLP001.HeaderId.ToString(), true),
                                                            objTLP001.ApprovedBy);
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("GetTTPGridPartial");
        }
        [HttpPost]
        public JsonResult LoadTPHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.CTQStatus.DRAFT.GetStringValue() + "','" + clsImplementationEnum.CTQStatus.Returned.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "1=1";
                }
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                //strWhere += " and CreatedBy=" + user;

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += "and (Project like '%" + param.sSearch
                         + "%' or Document like '%" + param.sSearch
                         + "%' or Customer like '%" + param.sSearch
                         + "%' or RevNo like '%" + param.sSearch
                         + "%' or CDD like '%" + param.sSearch
                         + "%' or Product like '%" + param.sSearch
                         + "%' or ProcessLicensor like '%" + param.sSearch
                         + "%' or Status like '%" + param.sSearch + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_TP_GET_HEADERDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(GetCodeAndNameByProject(uc.Project)),
                            Convert.ToString(uc.Document),
                            Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Project)),
                            uc.CDD == null || uc.CDD.Value == DateTime.MinValue ? "NA" : uc.CDD.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                            //Convert.ToString(uc.CDD),
                            Convert.ToString(uc.Product),
                            Convert.ToString(uc.ProcessLicensor),
                            Convert.ToString("R"+uc.RevNo),
                            Convert.ToString(uc.Status),
                            Convert.ToString(uc.SubmittedBy),
                            uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                            Convert.ToString(uc.ApprovedBy),
                            uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                            "<nobr><center>"+"<a title='View' href='"+WebsiteURL+"/TP/Header/TPDetail?Id="+Convert.ToInt32(uc.HeaderId)+"'><i style='' class='iconspace fa fa-eye'></i></a>"+
                            Helper.GenerateActionIcon(uc.HeaderId,"Delete","Delete Record","fa fa-trash-o", "DeleteDocument("+ uc.HeaderId +",'/TP/Header/DeleteHeader', {headerid:"+uc.HeaderId+"}, 'tblTPHeader')","",  (( uc.RevNo >0 && uc.Status.ToLower() != clsImplementationEnum.CommonStatus.SendForApprovel.GetStringValue().ToLower()) || ( uc.RevNo == 0 && uc.Status.ToLower() == clsImplementationEnum.CommonStatus.Approved.GetStringValue().ToLower()) ) ? false:true) +
                            (uc.RevNo>0 ?"<i title=\"History\" onclick=\"histroy('"+uc.HeaderId+"')\" class=\"iconspace fa fa-history\"></i>":"<i title=\"History\" style='' class='disabledicon fa fa-history'></i>")+"<i title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/TP/Header/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "') style='' class='iconspace fa fa-clock-o'></i>"
                            + "</center></nobr>",
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DeleteHeader(int headerid)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                objResponseMsg = Manager.DeletePDINDocument(headerid, clsImplementationEnum.PlanList.Technical_Process);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public string GetCodeAndNameByProject(string project)
        {
            var projdesc = db.COM001.Where(i => i.t_cprj == project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            return projdesc;
        }
        [HttpPost]
        public ActionResult GetHistoryView(int headerid)
        {
            TLP001_Log objTLP001 = new TLP001_Log();
            Session["HeaderId"] = Convert.ToInt32(headerid);
            return PartialView("getHistoryPartial", objTLP001);
        }

        [HttpPost]
        public JsonResult LoadTPHeaderHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;

                string strWhere = string.Empty;

                // strWhere += "CreatedBy = " + user + " or ApprovedBy = " + user;
                strWhere += "HeaderId = " + Convert.ToInt32(Session["HeaderId"]) + "";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += "and (Project like '%" + param.sSearch
                         + "%' or Document like '%" + param.sSearch
                         + "%' or Customer like '%" + param.sSearch
                         + "%' or RevNo like '%" + param.sSearch
                         + "%' or CDD like '%" + param.sSearch
                         + "%' or Product like '%" + param.sSearch
                         + "%' or ProcessLicensor like '%" + param.sSearch
                         + "%' or Status like '%" + param.sSearch + "%')";
                }
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_TP_GET_HISTORY_HEADERDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(GetCodeAndNameByProject(uc.Project)),
                               Convert.ToString(uc.Document),
                               Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Project)),
                               // Convert.ToDateTime(uc.CDD).ToString("dd/MM/yyyy"),
                               //Convert.ToString(uc.CDD),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                                Convert.ToString(uc.SubmittedBy),
                                uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                Convert.ToString(uc.ApprovedBy),
                                uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.Id)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [SessionExpireFilter]
        public ActionResult getHistoryDetail(int Id = 0)
        {
            TLP001_Log objTLP001 = new TLP001_Log();
            var user = objClsLoginInfo.UserName;

            var lstProjectDesc = (from a in db.COM001
                                  select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).ToList();
            ViewBag.Project = new SelectList(lstProjectDesc, "projectCode", "projectDescription");
            //ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments("TLP001/" + objTLP001.HeaderId + "/" + objTLP001.RevNo));
            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.lstyesno = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            List<string> lstProCat = clsImplementationEnum.getProductCategory().ToList();
            ViewBag.lstProCat = lstProCat.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            var approver = (from t1 in db.ATH001
                            join t2 in db.COM003 on t1.Employee equals t2.t_psno
                            where t2.t_actv == 1
                            select new { t1.Employee, Desc = t2.t_psno + " - " + t2.t_name }).Distinct().ToList();
            ViewBag.approver = new SelectList(approver, "Employee", "Desc");
            if (Id > 0)
            {
                var project = (from a in db.TLP001_Log
                               where a.Id == Id
                               select a.Project).FirstOrDefault();
                ViewBag.Customer = Manager.GetCustomerCodeAndNameByProject(project);
                objTLP001 = db.TLP001_Log.Where(x => x.Id == Id).FirstOrDefault();
                //if (objTLP001.CreatedBy != objClsLoginInfo.UserName &&  objTLP001.ApprovedBy != objClsLoginInfo.UserName)
                //{
                //    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                //}
            }
            var urlPrefix = Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.LastIndexOf('=') + 1);
            ViewBag.RevPrev = (db.TLP001_Log.Any(q => (q.HeaderId == objTLP001.HeaderId && q.RevNo == (objTLP001.RevNo - 1))) ? urlPrefix + db.TLP001_Log.Where(q => (q.HeaderId == objTLP001.HeaderId && q.RevNo == (objTLP001.RevNo - 1))).FirstOrDefault().Id : null);
            ViewBag.RevNext = (db.TLP001_Log.Any(q => (q.HeaderId == objTLP001.HeaderId && q.RevNo == (objTLP001.RevNo + 1))) ? urlPrefix + db.TLP001_Log.Where(q => (q.HeaderId == objTLP001.HeaderId && q.RevNo == (objTLP001.RevNo + 1))).FirstOrDefault().Id : null);
            return View(objTLP001);
        }
        [HttpPost]
        public ActionResult getCodeValue(string project, string approver)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {

                objResponseMsg.projdesc = db.COM001.Where(i => i.t_cprj == project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objResponseMsg.appdesc = db.COM003.Where(i => i.t_psno == approver && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult verifyApprover(string approver)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                approver = approver.Split('-')[0].Trim();
                var Exists = db.COM003.Any(x => x.t_psno == approver && x.t_actv == 1);
                if (Exists == false)
                {
                    objResponseMsg.Key = false;
                }
                else
                {
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult retrack(string headerid)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                int hd = Convert.ToInt32(headerid);
                if (hd > 0)
                {
                    TLP001 objTLP001 = db.TLP001.Where(x => x.HeaderId == hd).FirstOrDefault();
                    objTLP001.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                    objTLP001.SubmittedOn = null;
                    objTLP001.SubmittedBy = null;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    Manager.UpdatePDN002(objTLP001.HeaderId, objTLP001.Status, objTLP001.RevNo, objTLP001.Project, objTLP001.Document);
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ReviseHeader(int strHeaderId, string strRemarks)
        {
            CustomResponceMsg objResponseMsg = new CustomResponceMsg();
            try
            {
                TLP001 objTLP001 = db.TLP001.Where(u => u.HeaderId == strHeaderId).SingleOrDefault();
                if (objTLP001 != null)
                {
                    objTLP001.RevNo = Convert.ToInt32(objTLP001.RevNo) + 1;
                    objTLP001.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                    objTLP001.ReviseRemark = strRemarks;
                    objTLP001.EditedBy = objClsLoginInfo.UserName;
                    objTLP001.EditedOn = DateTime.Now;
                    objTLP001.ReturnRemark = null;
                    objTLP001.ApprovedOn = null;
                    objTLP001.SubmittedBy = null;
                    objTLP001.SubmittedOn = null;
                    db.SaveChanges();
                    Manager.UpdatePDN002(objTLP001.HeaderId, objTLP001.Status, objTLP001.RevNo, objTLP001.Project, objTLP001.Document);
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderID = objTLP001.HeaderId;
                    objResponseMsg.Status = objTLP001.Status;
                    objResponseMsg.rev = objTLP001.RevNo;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revision;
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        //public ActionResult SaveHeader(TLP001 tlp001, bool hasAttachments, Dictionary<string, string> Attach)
        public ActionResult SaveHeader(TLP001 tlp001)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();

            TLP001 objTLP001 = new TLP001();
            try
            {
                string s1, s2;
                s1 = tlp001.Document.ToString();
                s2 = s1.Substring(0, 3);
                s2 = s1.Substring(s1.Length - 2);

                string revnum = "";
                if (tlp001.HeaderId > 0)
                {
                     objTLP001 = db.TLP001.Where(x => x.HeaderId == tlp001.HeaderId).FirstOrDefault();
                    if (objTLP001 != null)
                    {
                        objTLP001.Product = tlp001.Product;
                        objTLP001.JobDescription = tlp001.JobDescription;
                        objTLP001.ProcessLicensor = tlp001.ProcessLicensor;
                        objTLP001.ApprovedBy = tlp001.ApprovedBy.Split('-')[0].ToString().Trim();
                        objTLP001.EditedBy = objClsLoginInfo.UserName;
                        objTLP001.EditedOn = DateTime.Now;
                        objTLP001.ReviseRemark = tlp001.ReviseRemark;

                        if (objTLP001.Status == clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue())
                        {

                            objTLP001.RevNo = Convert.ToInt32(objTLP001.RevNo) + 1;
                            revnum = objTLP001.RevNo.ToString();
                            objTLP001.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                            objTLP001.ReturnRemark = null;
                            objTLP001.ApprovedOn = null;
                        }
                        else
                        {
                            revnum = objTLP001.RevNo.ToString();
                        }
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.PlanMessages.Update.ToString();
                        objResponseMsg.status = objTLP001.Status.ToString();
                        objResponseMsg.Value = clsImplementationMessage.PlanMessages.Update.ToString();
                        objResponseMsg.Headerid = objTLP001.HeaderId;

                        objResponseMsg.Revision = "R" + revnum.ToString();
                        var folderPath = "TLP001/" + tlp001.HeaderId + "/R" + revnum;

                        Manager.UpdatePDN002(objTLP001.HeaderId, objTLP001.Status, objTLP001.RevNo, objTLP001.Project, objTLP001.Document);
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Process Details not available.";
                    }
                }
                else
                {
                     objTLP001 = db.TLP001.Add(new TLP001
                    {
                        Project = tlp001.Project.Split('-')[0].ToString().Trim(),
                        DocumentNo = Convert.ToInt32(s2),
                        Document = tlp001.Document,
                        Customer = tlp001.Customer.Split('-')[0].ToString().Trim(),
                        RevNo = 0,
                        Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                        CDD = Helper.ToNullIfTooEarlyForDb(tlp001.CDD),
                        Product = tlp001.Product,
                        ProcessLicensor = tlp001.ProcessLicensor,
                        JobDescription = tlp001.JobDescription,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now,
                        ReviseRemark = tlp001.ReviseRemark,
                        ApprovedBy = tlp001.ApprovedBy.Split('-')[0].ToString().Trim()
                    });
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    string project = tlp001.Project.Split('-')[0].ToString().Trim();
                    objResponseMsg.Value = clsImplementationMessage.PlanMessages.Insert.ToString();
                    objResponseMsg.status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                    objResponseMsg.Revision = "R0";
                    int Headerid = (from a in db.TLP001
                                    where a.Project == project
                                    select a.HeaderId).FirstOrDefault();
                    var folderPath = "TLP001/" + Headerid + "/R0";
                    objResponseMsg.Headerid = objTLP001.HeaderId;


                    Manager.UpdatePDN002(objTLP001.HeaderId, objTLP001.Status, objTLP001.RevNo, objTLP001.Project, objTLP001.Document);
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #region Export Excel
        //Excel funtionality
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                { //header grid data
                    var lst = db.SP_TP_GET_HEADERDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Project = Convert.ToString(GetCodeAndNameByProject(uc.Project)),
                                      Document = uc.Document,
                                      Customer = Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Project)),
                                      CDD = uc.CDD == null || uc.CDD.Value == DateTime.MinValue ? "NA" : uc.CDD.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      Product = uc.Product,
                                      ProcessLicensor = Convert.ToString(uc.ProcessLicensor),
                                      RevNo = Convert.ToString("R" + uc.RevNo),
                                      Status = Convert.ToString(uc.Status),
                                      SubmittedBy = Convert.ToString(uc.SubmittedBy),
                                      SubmittedOn = uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ApprovedBy = Convert.ToString(uc.ApprovedBy),
                                      ApprovedOn = uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {  //line grid data

                    var lst = db.SP_TP_GET_HISTORY_HEADERDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();


                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Project = Convert.ToString(GetCodeAndNameByProject(uc.Project)),
                                      Document = uc.Document,
                                      Customer = Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Project)),
                                      CDD = uc.CDD == null || uc.CDD.Value == DateTime.MinValue ? "NA" : uc.CDD.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      Product = uc.Product,
                                      ProcessLicensor = Convert.ToString(uc.ProcessLicensor),
                                      RevNo = Convert.ToString("R" + uc.RevNo),
                                      Status = Convert.ToString(uc.Status),
                                      SubmittedBy = Convert.ToString(uc.SubmittedBy),
                                      SubmittedOn = uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ApprovedBy = Convert.ToString(uc.ApprovedBy),
                                      ApprovedOn = uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}