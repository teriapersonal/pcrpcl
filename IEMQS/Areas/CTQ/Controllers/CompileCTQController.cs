﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace IEMQS.Areas.CTQ.Controllers
{
    public class CompileCTQController : clsBase
    {
        /// <summary>
        /// Added by Dharmesh For Make Compile CTQ
        /// </summary>
        /// <returns></returns>

        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult CTQ()
        {
            return View();
        }

        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult CTQLineDetails(string ID)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("CTQ001");
            int HeaderId = 0;
            if (!string.IsNullOrWhiteSpace(ID))
            {
                HeaderId = Convert.ToInt32(ID);
            }
            CTQ001 objCTQ001 = db.CTQ001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            if (objCTQ001 != null)
            {
                ViewBag.HeaderID = objCTQ001.HeaderId;
                ViewBag.Status = objCTQ001.Status;
                ViewBag.CTQNo = objCTQ001.CTQNo;

                var BUDescription = (from a in db.COM002
                                     where a.t_dtyp == 2 && a.t_dimx == objCTQ001.BU
                                     select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
                objCTQ001.BU = BUDescription.BUDesc;

                var Project = (from a in db.COM001
                               where a.t_cprj == objCTQ001.Project
                               select new { pDesc = a.t_cprj + " - " + a.t_dsca }).FirstOrDefault();
                objCTQ001.Project = Project.pDesc;

                var locDescription = (from a in db.COM002
                                      where a.t_dtyp == 1 && a.t_dimx == objCTQ001.Location
                                      select new { Location = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
                objCTQ001.Location = locDescription.Location;

                //objCTQ001.Project = Convert.ToString(db.COM001.Where(x => x.t_cprj == objCTQ001.Project).Select(x =>  x.t_dsca).FirstOrDefault());
                //objCTQ001.BU = BUDescription.BUDesc;

                //objCTQ001.Location = Convert.ToString(db.COM002.Where(x => x.t_dimx == objCTQ001.Location).Select(x => x.t_desc).FirstOrDefault());
            }
            return View(objCTQ001);
        }

        [HttpPost]
        public JsonResult LoadCTQHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;


                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.CTQStatus.SendForCompile.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "1=1";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or (ctq1.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%' or CTQNo like '%" + param.sSearch + "%' or CTQRev like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "ctq1.BU", "ctq1.Location");
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_CTQ_GETHEADER
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                           Convert.ToString(uc.Project),
                           Convert.ToString(uc.BU),
                           Convert.ToString(uc.CTQNo),
                           Convert.ToString("R"+uc.CTQRev),
                           Convert.ToString(uc.Status),
                           Convert.ToString(uc.Location),
                            "<center>"+
                           Helper.GenerateActionIcon(uc.HeaderId, "View", "View Detail", "fa fa-eye", "", WebsiteURL + "/CTQ/CompileCTQ/CTQLineDetails?ID="+Convert.ToString(uc.HeaderId),false)
                           +HTMLActionString(uc.HeaderId,uc.Status,"Timeline","Timeline","fa fa-clock-o", "ShowTimeline(\"/CTQ/Maintain/ShowTimeline?HeaderID="+ uc.HeaderId +"\");")
                           + Helper.GenerateActionIcon(uc.HeaderId, "Print", "Print Detail", "fa fa-print", "PrintReport("+uc.HeaderId+")","",false)
                           +HTMLActionString(uc.HeaderId,"","HistoryCTQ","History","fa fa-history","HistoryCTQ(\""+ uc.HeaderId +"\",\"CompileCTQ\");",!(uc.CTQRev > 0 || uc.Status.Equals(clsImplementationEnum.CTQStatus.Approved.GetStringValue()))) +"</center>",//Convert.ToString(uc.HeaderId),
                           Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadCTQHeaderLinesData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();
                string[] arrayCTQStatus = { clsImplementationEnum.CTQStatus.SendForCompile.GetStringValue() };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (Status like '%" + param.sSearch + "%' or Specification like '%" + param.sSearch + "%' or CSRMOMDoc like '%" + param.sSearch + "%' or ClauseNo like '%" + param.sSearch + "%' or CTQDesc like '%" + param.sSearch + "%' or Department like '%" + param.sSearch + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName);
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_CTQ_GET_HEADER_LINES
                                (
                               StartIndex,
                               EndIndex,
                               strSortOrder,
                               strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.ROW_NO),
                            (arrayCTQStatus.Contains(uc.Status)?"<input maxlength='100' type='text' name='txtSpecification' class='form-control input-sm input-small input-inline' value='"+Convert.ToString(uc.Specification)+"' />":Convert.ToString(uc.Specification)),
                            (arrayCTQStatus.Contains(uc.Status)?"<input maxlength='100' type='text' name='txtCSRMOMDoc' class='form-control input-sm input-small input-inline' value='"+Convert.ToString(uc.CSRMOMDoc)+"' />":Convert.ToString(uc.CSRMOMDoc)),
                            (arrayCTQStatus.Contains(uc.Status)?"<input maxlength='100' type='text' name='txtClauseNo' class='form-control input-sm input-small input-inline' value='"+Convert.ToString(uc.ClauseNo)+"' />":Convert.ToString(uc.ClauseNo)),
                            (arrayCTQStatus.Contains(uc.Status)?"<input maxlength='1000' type='text' name='txtSCTQDesc' style='width: 350px;' class='form-control input-sm '  value='" + Convert.ToString(uc.CTQDesc)+"' />":Convert.ToString(uc.CTQDesc)),
                            (arrayCTQStatus.Contains(uc.Status)?"<input maxlength='1000' type='text' name='txtActionDesc' class='form-control input-sm input-small input-inline' value='"+Convert.ToString(uc.ActionDesc)+"' />":Convert.ToString(uc.ActionDesc)),
                            Convert.ToString((new MaintainController()).getActiondeptName(uc.ActionBy)),
                            Convert.ToString(uc.ReturnRemark),
                            Convert.ToString(uc.Status),
                            Convert.ToString("R"+uc.LineRev),
                            Convert.ToString(uc.CreatedBy),
                                     Convert.ToDateTime(uc.CreatedOn).ToString("dd/MM/yyyy"),
                            (arrayCTQStatus.Contains(uc.Status)?"<center><i  data-modal='' id='btnDelete' name='btnAction' style='cursor: pointer;' Title='Delete Record' class='fa fa-trash-o' onClick='DeleteLineRecord("+uc.LineId+")'></i>"+"   "+ HTMLActionString(uc.LineId,uc.Status,"Timeline","Timeline","fa fa-clock-o", "ShowTimeline(\"/CTQ/Maintain/ShowTimeline?HeaderID="+ uc.LineId +"&LineId="+ uc.LineId +"\");")+"</center>":"<center><i  data-modal='' id='btnDelete' name='btnAction' style='cursor: pointer;opacity: 0.5;' Title='Delete Record' class='fa fa-trash - o' ></i>" +"   "+ HTMLActionString(uc.LineId,uc.Status,"Timeline","Timeline","fa fa-clock-o", "ShowTimeline(\"/CTQ/Maintain/ShowTimeline?HeaderID="+ uc.LineId +"&LineId="+ uc.LineId +"\");")+ "</center>"),
                            Convert.ToString(uc.LineId),
                            Convert.ToString(uc.HeaderId)
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetCTQHeaderLinesHtml(int HeaderId, string strCTQNo)
        {
            ViewBag.HeaderId = HeaderId;
            ViewBag.strCTQNo = strCTQNo;
            return PartialView("_GetCTQHeaderLinesHtml");
        }

        [HttpPost]
        public ActionResult GetHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetHeaderGridDataPartial");
        }

        [HttpPost]
        public ActionResult SendForApproval(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string compilerStatus = clsImplementationEnum.CTQStatus.SendForCompile.GetStringValue().ToUpper();
                List<CTQ002> lstCTQ002 = db.CTQ002.Where(x => x.HeaderId == HeaderId && x.Status.ToUpper().Trim() == compilerStatus.ToUpper().Trim()).ToList();
                if (lstCTQ002 != null && lstCTQ002.Count > 0)
                {
                    foreach (var objData in lstCTQ002)
                    {
                        CTQ002 objCTQ002 = lstCTQ002.Where(x => x.LineId == objData.LineId).FirstOrDefault();
                        objCTQ002.Status = clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue();
                        objCTQ002.CompiledBy = objClsLoginInfo.UserName;
                        objCTQ002.CompiledOn = DateTime.Now;
                        objCTQ002.EditedBy = objClsLoginInfo.UserName;
                        objCTQ002.EditedOn = DateTime.Now;
                    }
                }
                CTQ001 objCTQ001 = db.CTQ001.Where(x => x.HeaderId == HeaderId && x.Status.ToUpper() == compilerStatus).FirstOrDefault();
                objCTQ001.Status = clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue();
                db.SaveChanges();

                #region Send Notification
                string role = clsImplementationEnum.UserRoleName.QI1.GetStringValue() + "," + clsImplementationEnum.UserRoleName.QC2.GetStringValue();
                (new clsManager()).SendNotification(role, objCTQ001.Project, objCTQ001.BU, objCTQ001.Location, "CTQ " + objCTQ001.CTQNo + " has been has been submitted for your Approval", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/CTQ/ApproveCTQ/CTQLineDetails?ID=" + objCTQ001.HeaderId);
                #endregion

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Lines successfully sent for approval.";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateCTQLine(int LineID, string changeText, string strFrom)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                CTQ002 objCTQ002 = db.CTQ002.Where(x => x.LineId == LineID).FirstOrDefault();
                if (objCTQ002 != null)
                {
                    if (strFrom.ToUpper() == "A")
                    {
                        objCTQ002.Specification = changeText;
                    }
                    if (strFrom.ToUpper() == "B")
                    {
                        objCTQ002.CSRMOMDoc = changeText;
                    }
                    if (strFrom.ToUpper() == "C")
                    {
                        objCTQ002.ClauseNo = changeText;
                    }
                    if (strFrom.ToUpper() == "D")
                    {
                        objCTQ002.CTQDesc = changeText;
                    }
                    if (strFrom.ToUpper() == "E")
                    {
                        objCTQ002.ActionDesc = changeText;
                    }
                    objCTQ002.CompiledBy = objClsLoginInfo.UserName;
                    objCTQ002.CompiledOn = DateTime.Now;
                    objCTQ002.EditedBy = objClsLoginInfo.UserName;
                    objCTQ002.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Line updated successfully.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Line not available";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RemoveLine(int LineId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrayCTQStatus = { clsImplementationEnum.CTQStatus.DRAFT.GetStringValue(), clsImplementationEnum.CTQStatus.Returned.GetStringValue(), clsImplementationEnum.CTQStatus.SendForCompile.GetStringValue() };
                CTQ002 objCTQ002 = db.CTQ002.Where(x => x.LineId == LineId && arrayCTQStatus.Contains(x.Status)).FirstOrDefault();
                if (objCTQ002 != null)
                {
                    db.CTQ002.Remove(objCTQ002);
                    db.SaveChanges();
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CTQMessages.Delete.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Line not available";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", bool disabled = false)
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (disabled)
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='disabledicon " + className + "' ></i>";
            }
            else
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;margin-left:5px;' Title='" + buttonTooltip + "' class='iconspace " + className + "'" + onClickEvent + " ></i>";

            return htmlControl;
        }

        [HttpPost]
        public ActionResult pendingdept(int headerID)
        {
            CTQ001 objCTQ001 = db.CTQ001.Where(u => u.HeaderId == headerID).SingleOrDefault();
            var lstCtq003 = (from dbo in db.CTQ003
                             where dbo.Location == objCTQ001.Location && dbo.BU == objCTQ001.BU && dbo.IsMandatory == true
                             select dbo.Department.Trim()).Distinct().ToList();

            var lstCtq002 = (from dbo in db.CTQ002
                             where dbo.HeaderId == headerID
                             select dbo.Department.Trim()).Distinct().ToList();
            string strDept = string.Empty;
            string deptDescription;
            foreach (var lst in lstCtq003)
            {
                if (!lstCtq002.Contains(lst))
                {
                    deptDescription = db.COM002.Where(a => a.t_dimx == lst).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                    if (strDept.Length == 0)
                    {
                        strDept += deptDescription;
                    }
                    else
                    {
                        strDept += "," + deptDescription;
                    }

                }
            }
            string[] dept = null;
            if (!string.IsNullOrWhiteSpace(strDept))
            {
                dept = strDept.Split(',');
                ViewBag.Deprtment = dept.ToList();
            }
            else
            {
                dept = null;
                ViewBag.Deprtment = null;
            }

            return PartialView("~/Areas/CTQ/Views/Shared/_PendingDeptPartial.cshtml");
        }
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_CTQ_GETHEADER(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      Location = li.Location,
                                      BU = li.BU,
                                      CTQNo = li.CTQNo,
                                      CTQRev = li.CTQRev,
                                      CreatedBy = li.CreatedBy,
                                      EditedBy = li.EditedBy,
                                      Status = li.Status
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                else
                {
                    var lst = db.SP_CTQ_GET_HEADER_LINES(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      CTQRev = li.CTQRev,
                                      LineRev = li.LineRev,
                                      Status = li.Status,
                                      SrNo = li.SrNo,
                                      Specification = li.Specification,
                                      CSRMOMDoc = li.CSRMOMDoc,
                                      ClauseNo = li.ClauseNo,
                                      CTQDesc = li.CTQDesc,
                                      Department = li.Department,
                                      ActionDesc = li.ActionDesc,
                                      ActionBy = li.ActionBy,
                                      EditedBy = li.EditedBy,
                                      SendToCompiledBy = li.SendToCompiledBy,
                                      CompiledBy = li.CompiledBy,
                                      ApprovedBy = li.ApprovedBy,
                                      ReturnRemark = li.ReturnRemark,
                                      SendToCompiler = li.SendToCompiler,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
    }

    public class clsCompileApprove
    {
        public int LineID { get; set; }
        public int HeaderID { get; set; }
        public string SpecNo { get; set; }
        public string CSRMOMNo { get; set; }
        public string ClauseNo { get; set; }
        public string CTQDesc { get; set; }
        public string ActionTobeTaken { get; set; }

    }
}