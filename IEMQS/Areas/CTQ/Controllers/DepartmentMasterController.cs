﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.CTQ.Controllers
{
    public class DepartmentMasterController : clsBase
    {
        // GET: CTQ/DepartmentMaster
        [AllowAnonymous]
        [SessionExpireFilter]
        [UserPermissions]
        public ActionResult Index()
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("CTQ003");
            return View();
        }

        [HttpPost]
        public ActionResult getData(JQueryDataTableParamModel param)
        {
            try
            {

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                int CategoryId = Convert.ToInt32(param.CategoryId);
                string strWhere = "1=1";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (Location like '%" + param.sSearch
                         + "%' or BU like '%" + param.sSearch
                        + "%' or Department like '%" + param.sSearch
                        + "%' or IsMandatory like '%" + param.sSearch
                        + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                #region Sorting
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion
                var lstResult = db.SP_CTQ_GET_DEPARTMENT_LIST
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {

                           Convert.ToString(uc.BU),
                           Convert.ToString(uc.Department),
                           Convert.ToString(uc.Location),
                           Convert.ToString(uc.IsMandatory),
                           Convert.ToString(uc.Id),
                           Convert.ToString(uc.Id)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0,
                    iTotalDisplayRecords = lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult GetDataHtml(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            var ctqdata = db.CTQ003.Where(x => x.Id == Id).FirstOrDefault();
            var edit = db.COM002.Where(x => x.t_dimx == ctqdata.Location).Select(x => new { data = ctqdata.BU + "|" + ctqdata.Location + "|" + ctqdata.Department + "|" + ctqdata.IsMandatory + "|" + x.t_desc,  }).FirstOrDefault();

            objResponseMsg.Value = edit.data;
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveDepartment(CTQ003 ctq003)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (ctq003.Id > 0)
                {
                    var chklist = db.CTQ003.Where(x => x.Id == ctq003.Id).FirstOrDefault();

                    if (chklist != null)
                    {
                        chklist.IsMandatory = ctq003.IsMandatory;
                        chklist.EditedBy = objClsLoginInfo.UserName;
                        chklist.EditedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                }
                else
                {
                    CTQ003 objCTQ003 = new CTQ003();
                    List<CTQ003> lstCTQ003 = new List<CTQ003>();

                    string[] arraystrbu = ctq003.BU.Split(',').ToArray();
                    string[] arraystrdep = ctq003.Department.Split(',').ToArray();

                    for (int i = 0; i < arraystrbu.Length; i++)
                    {
                        for (int j = 0; j < arraystrdep.Length; j++)
                        {
                            CTQ003 objCTQ003Add = new CTQ003();
                            var dep = arraystrdep[j].ToString();
                            var bu = arraystrbu[i].ToString();

                            var chklist = db.CTQ003.Where(x => x.BU == bu && x.Department == dep && x.Location == ctq003.Location).FirstOrDefault();

                            if (chklist != null)
                            {
                                chklist.BU = bu;
                                chklist.Department = dep;
                                chklist.Location = ctq003.Location;
                                chklist.IsMandatory = ctq003.IsMandatory;
                                chklist.EditedBy = objClsLoginInfo.UserName;
                                chklist.EditedOn = DateTime.Now;
                                db.SaveChanges();
                            }
                            else {
                                objCTQ003Add.BU = bu;
                                objCTQ003Add.Department = dep;
                                objCTQ003Add.Location = ctq003.Location;
                                objCTQ003Add.IsMandatory = ctq003.IsMandatory;
                                objCTQ003Add.CreatedBy = objClsLoginInfo.UserName;
                                objCTQ003Add.CreatedOn = DateTime.Now;
                                lstCTQ003.Add(objCTQ003Add);
                            }
                        }
                    }
                    if (lstCTQ003 != null && lstCTQ003.Count > 0)
                    {
                        db.CTQ003.AddRange(lstCTQ003);
                    }
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetBu(string search, string param)
        {
            try
            {
                int BUTypeId = Convert.ToInt32(clsImplementationEnum.BLDNumber.BU.GetStringValue());

                var objBU = (from a1 in db.ATH001
                             join c2 in db.COM002 on a1.BU equals c2.t_dimx
                             where a1.Employee == objClsLoginInfo.UserName && c2.t_dtyp == BUTypeId
                             select new { BU = a1.BU, Name = c2.t_desc }
                       ).ToList().Select(i => new { id = i.BU, text = i.Name }).Distinct();
                
                return Json(objBU, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetLocation(string search, string param)
        {
            try
            {
                int LocationTypeId = Convert.ToInt32(clsImplementationEnum.BLDNumber.LOC.GetStringValue());

                var objLocation = (from a1 in db.ATH001
                                   join c2 in db.COM002 on a1.Location equals c2.t_dimx
                                   where a1.Employee == objClsLoginInfo.UserName && c2.t_dtyp == LocationTypeId
                                   select new { Location = a1.Location, Name = c2.t_desc }
                ).ToList().Select(i => new { Value = i.Location, Text = i.Name }).Distinct();
                //Text = s.Role, Value = s.Id
                return Json(objLocation, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetDepartments(string search, string location, int? isAdmin)
        {
            try
            {
                var items = db.FN_GET_LOCATIONWISE_DEPARTMENT(location, "");
                if(isAdmin == 0)
                {
                    items = items.Where(c=>c.t_dimx == objClsLoginInfo.Department);
                }
                return Json(items.Select(x => new SelectItemList { id = x.t_dimx, text = x.t_dimx + "-" + x.t_desc, }).ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        #region Export Excell
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_CTQ_GET_DEPARTMENT_LIST(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Bu = li.BU,
                                      Location = li.Location,
                                      Department = li.Department,
                                      IsMandatory = li.IsMandatory 
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}