﻿using IEMQS.Areas.MOM.Models;
using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.MOM.Controllers
{
    /// <summary>
    /// Area Cloned from "IMB" and Modified by M. Ozair Khatri (90385894)
    /// 
    /// First Commit - 28-07-2017
    /// Commit - 90385894 - 01-08-2017 - Added autocomplete, changed primary key to HeaderId and LineId.
    /// Commit - 90385894 - 08-08-2017 - Added code to delete uploaded files if no attachments exist, Documents moved to clsUploadKM.
    /// </summary>
    public class MaintainController : clsBase
    {
        #region ViewMethods

        //[SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult Index(string p)
        {
            ViewBag.IsDisplayOnly = false;
            ViewBag.Project = p;
            return View();
        }

        public ActionResult DisplayMOM(string p)
        {
            ViewBag.IsDisplayOnly = true;
            ViewBag.Project = p;
            return View("Index");
        }

        [SessionExpireFilter]
        [UserPermissions]
        public ActionResult DisplayMaintainProjectwiseMoM()
        {
            ViewBag.IsDisplayOnly = true;
            return View("MaintainProjectwiseMoM");
        }

        [SessionExpireFilter]
        public async Task<ActionResult> EditHeader(string HeaderId, string MOMNo)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (string.IsNullOrWhiteSpace(HeaderId))
                    throw new Exception("Invalid Request");
                int headerId = Int32.Parse(HeaderId);
                MOM001 objMOM001 = await db.MOM001.FirstOrDefaultAsync(x => (x.HeaderId == headerId && x.MOMNo.ToString() == MOMNo));


                if (objMOM001 != null)
                {
                    if (objMOM001.CreatedBy != objClsLoginInfo.UserName)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "MOM is created by someone else";
                    }
                    else
                    {
                        ViewBag.HeaderId = HeaderId;
                        ViewBag.MOMNo = MOMNo;
                        ViewBag.Action = "Edit";
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Header already Exists.";
                        return View("EditHeader");
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Document Not Found";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTeamLeaderEdit()
        {
            try
            {

                var items = (from li in db.COM003
                             where li.t_actv == 1
                             select new
                             {
                                 id = li.t_psno,
                                 text = li.t_psno + "-" + li.t_name,
                             }).ToList();
                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetTeamMember(string search, string param)
        {
            try
            {


                var items = (from li in db.COM003
                             where li.t_psno != param && (li.t_psno.ToLower().Contains(search) || li.t_name.ToLower().Contains(search)) && li.t_actv == 1
                             select new
                             {
                                 id = li.t_psno,
                                 text = li.t_psno + "-" + li.t_name,
                             }).ToList();

                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetTeamMemberEdit(string param)
        {
            try
            {


                var items = (from li in db.COM003
                             where li.t_psno != param && li.t_actv == 1
                             select new
                             {
                                 id = li.t_psno,
                                 text = li.t_psno + "-" + li.t_name,
                             }).ToList();
                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetTeamMemberValue(int headerId)
        {
            try
            {
                //headerId = 15;
                var teamMember = (db.MOM002.Where(x => x.HeaderId == headerId).Select(s => s.Attendee).ToList());
                var teamMembers = String.Join(",", teamMember);

                return Json(teamMember, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult getMOMAttendee(int HeaderId, int MOMNo)
        {
            try
            {

                var MOMHeader = db.Sp_Get_MOM_Attendie(HeaderId, MOMNo, objClsLoginInfo.UserName).ToList();



                foreach (var item in MOMHeader)
                {
                    item.MOMNo = null;
                    //item.MOMNo = null;
                    var department = db.COM002.Where(m => m.t_dimx == item.Department).FirstOrDefault();
                    if (item.Department != null && item.Attendee != null)
                    {

                        item.Department = department.t_desc;
                        item.Attendee = item.Attendee.Replace(",", "<br>");
                    }

                }
                var list = JsonConvert.SerializeObject(new { success = true, obj = MOMHeader },
                    Formatting.None,
                    new JsonSerializerSettings()
                    {
                        ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                    });
                return Content(list, "application/json");//Json(new { success = true, obj = MOMHeader.ToArray() }, JsonRequestBehavior.AllowGet);// Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Serialize});
            }
            catch (Exception ex)
            {
                return Json(new { success = false, obj = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }


        public ActionResult getMOMProject(int HeaderId, int MOMNo)
        {
            try
            {
                List<MOM005> MOMProject = db.MOM005.Where(li => li.HeaderId == HeaderId && li.MOMNo == MOMNo && li.CreatedBy == objClsLoginInfo.UserName).ToList();

                foreach (var itm in MOMProject)
                {
                    itm.MOM001 = null;

                }
                var list = JsonConvert.SerializeObject(new
                {
                    success = true,
                    obj = MOMProject.Select(q => new
                    {
                        LineId = q.LineId,
                        Project = getproject(q.Project),
                        CDD = q.CDD.HasValue ? q.CDD.Value.ToString("yyyy-MM-dd") : "",
                        PDD = q.PDD.HasValue ? q.PDD.Value.ToString("yyyy-MM-dd") : "",
                        PDDCur = q.PDDCur.HasValue ? q.PDDCur.Value.ToString("yyyy-MM-dd") : "",
                        BC = q.BC == "" ? "NA" : q.BC,
                        CC = q.CC == "" ? "NA" : q.CC,

                    }).ToList()
                },
                    Formatting.None,
                    new JsonSerializerSettings()
                    {
                        ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                    });
                return Content(list, "application/json");
                //return Json(new { success = true, obj = MOMProject }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, obj = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }


        public string getproject(string project)
        {
            String projectdescrption = "";
            var projdesc = db.COM001.Where(m => m.t_cprj == project).FirstOrDefault();
            projectdescrption = projdesc.t_cprj + '-' + projdesc.t_dsca;

            return projectdescrption;
        }

        public ActionResult getMOMAction(int HeaderId, int MOMNo)
        {
            try
            {
                List<MOM004> MOMAction = db.MOM004.Where(li => li.HeaderId == HeaderId && li.MOMNo == MOMNo && li.CreatedBy == objClsLoginInfo.UserName).ToList();

                foreach (var item in MOMAction)
                {

                    item.MOM001 = null;
                    item.ByWhom = item.ByWhom + '-' + Manager.GetUserNameFromPsNo(item.ByWhom);
                }
                var list = JsonConvert.SerializeObject(new { success = true, obj = MOMAction },
                 Formatting.None,
                 new JsonSerializerSettings()
                 {
                     ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                 });
                return Content(list, "application/json");
                // return Json(new { success = true, obj = MOMAction }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, obj = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult getMOMActionList(int HeaderId, int MOMNo)
        {
            try
            {
                // List<MOM004> Momactionlist = null;
                // List<int> lstMomactionlist = Momactionlist.Select(x => x.HeaderId).ToList();
                List<MOM005> MOMProject = db.MOM005.Where(li => li.HeaderId == HeaderId && li.MOMNo == MOMNo).ToList();
                List<string> lstProject = MOMProject.Select(x => x.Project).ToList();

                var MOMgetheaderid = db.MOM005.Where(li => lstProject.Contains(li.Project)).Select(x => x.HeaderId).ToList();
                var MOMactionheaderid = db.MOM004.Where(li => MOMgetheaderid.Contains(li.HeaderId) && li.Completed == false && li.CreatedBy.Trim() == objClsLoginInfo.UserName.Trim()).ToList();

                foreach (var item in MOMactionheaderid)
                {

                    item.MOM001 = null;
                    item.Project = item.Description;
                    var mom = db.MOM001.Where(m => m.MOMNo == item.MOMNo).FirstOrDefault();
                    item.Description = mom.Venue;

                    item.ByWhom = item.ByWhom + '-' + Manager.GetUserNameFromPsNo(item.ByWhom);
                }


                var list = JsonConvert.SerializeObject(new { success = true, obj = MOMactionheaderid },
                 Formatting.None,
                 new JsonSerializerSettings()
                 {
                     ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                 });
                return Content(list, "application/json");
                // return Json(new { success = true, obj = MOMAction }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, obj = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult getMOMAddActionList(string project)
        {
            try
            {
                var MOMgetheaderid = db.MOM005.Where(li => li.Project == project).Select(x => x.HeaderId).ToList();
                var MOMactionheaderid = db.MOM004.Where(li => MOMgetheaderid.Contains(li.HeaderId) && li.Completed == false && li.CreatedBy == objClsLoginInfo.UserName).ToList();

                foreach (var item in MOMactionheaderid)
                {

                    item.MOM001 = null;
                    var mom = db.MOM001.Where(m => m.MOMNo == item.MOMNo).FirstOrDefault();
                    item.Project = item.Description;
                    item.Description = mom.Venue;

                    item.ByWhom = item.ByWhom + '-' + Manager.GetUserNameFromPsNo(item.ByWhom);
                }


                var list = JsonConvert.SerializeObject(new { success = true, obj = MOMactionheaderid },
                 Formatting.None,
                 new JsonSerializerSettings()
                 {
                     ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                 });
                return Content(list, "application/json");
                // return Json(new { success = true, obj = MOMAction }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, obj = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }
        //[SessionExpireFilter]
        public ActionResult PrintFormat(int? HeaderId, int? MOMNo)
        {
            //var data = (from m in db.MOM001 where m.MOMNo == MOMNo select m).ToList();
            MOM001 objMOM001 = db.MOM001.Where(q => q.MOMNo == MOMNo).FirstOrDefault();
            foreach (var md in objMOM001.MOM004)
            {
                md.ByWhom = Manager.GetUserNameFromPsNo(md.ByWhom);
            }
            foreach (var md in objMOM001.MOM002)
            {
                md.Department = getDeptFromPS(md.Attendee);
                md.Attendee = Manager.GetUserNameFromPsNo(md.Attendee);

            }
            foreach (var prj in objMOM001.MOM005)
            {
                prj.Project = GetProjectDescr(prj.Project);

            }
            objMOM001.CreatedBy = Manager.GetUserNameFromPsNo(objMOM001.CreatedBy);

            return View(objMOM001);
        }

        public ActionResult getMOMAgenda(int HeaderId, int MOMNo)
        {
            try
            {
                List<MOM003> MOMAgenda = db.MOM003.Where(li => li.HeaderId == HeaderId && li.MOMNo == MOMNo && li.CreatedBy == objClsLoginInfo.UserName).ToList();



                foreach (var item in MOMAgenda)
                {
                    item.MOM001 = null;

                }
                var list = JsonConvert.SerializeObject(new { success = true, obj = MOMAgenda },
                 Formatting.None,
                 new JsonSerializerSettings()
                 {
                     ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                 });
                return Content(list, "application/json");
                //return Json(new { success = true, obj = MOMAgenda }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, obj = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult getMOMHeader(int HeaderId, int MOMNo)
        {
            try
            {
                MOM001 MOMHeader = db.MOM001.Where(li => li.HeaderId == HeaderId && li.MOMNo == MOMNo && li.CreatedBy == objClsLoginInfo.UserName).FirstOrDefault();

                MOMHeader.MOM002 = null;
                MOMHeader.MOM003 = null;
                MOMHeader.MOM004 = null;
                MOMHeader.MOM005 = null;



                var list = JsonConvert.SerializeObject(new { success = true, obj = MOMHeader },
                 Formatting.None,
                 new JsonSerializerSettings()
                 {
                     ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                 });
                return Content(list, "application/json");
                //return Json(new { success = true, obj = MOMHeader }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, obj = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        // public ActionResult updateAttendee(int HeaderId, int MOMNo, string PS)
        public ActionResult updateAttendee(int HeaderId, int MOMNo, string PS)

        {

            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {


                MOM002 objMOM002 = db.MOM002.Where(m => m.HeaderId == HeaderId).FirstOrDefault();

                db.sP_Mom002_Delete(HeaderId);

                int[] Attendie = Array.ConvertAll(PS.Split(','), int.Parse);
                for (int i = 0; i <= Attendie[0]; i++)
                {
                    // var rem = db.MOM002.Where(q => q.HeaderId == HeaderId).FirstOrDefault();
                    MOM002 m1 = new MOM002();
                    m1.Attendee = Attendie[i].ToString();
                    string dept = getDeptFromPS(Attendie[i].ToString());
                    m1.Department = dept.Split('-')[0].Trim();
                    m1.MOMNo = MOMNo;
                    m1.HeaderId = HeaderId;
                    m1.CreatedBy = objClsLoginInfo.UserName;
                    m1.CreatedOn = DateTime.Now;
                    //db.Entry(rem).State = EntityState.Modified;
                    //db.SaveChanges();
                    db.MOM002.Add(m1);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = true;

            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult deleteAttendee(int HeaderId, int MOMNo, int LineID)
        {

            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var rem = db.MOM002.Where(q => q.HeaderId == HeaderId && q.MOMNo == MOMNo && q.LineId == LineID).FirstOrDefault();
                db.MOM002.Remove(rem);
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;

            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        // public ActionResult addAttendee(MOM002 m1)
        public ActionResult addAttendee(int HeaderId, int MOMNo, string PS)
        {

            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {


                int[] Attendie = Array.ConvertAll(PS.Split(','), int.Parse);

                for (int i = 0; i < Attendie.Count(); i++)
                {
                    // var rem = db.MOM002.Where(q => q.HeaderId == HeaderId).FirstOrDefault();
                    MOM002 m1 = new MOM002();
                    m1.Attendee = Attendie[i].ToString();
                    string dept = getDeptFromPS(Attendie[i].ToString());
                    m1.Department = dept.Split('-')[0].Trim();
                    m1.MOMNo = MOMNo;
                    m1.HeaderId = HeaderId;
                    m1.CreatedBy = objClsLoginInfo.UserName;
                    m1.CreatedOn = DateTime.Now;
                    //db.Entry(rem).State = EntityState.Modified;
                    //db.SaveChanges();
                    db.MOM002.Add(m1);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;

            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult updateAction(MOM004 m4)
        {

            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var obj = db.MOM004.Where(q => q.HeaderId == m4.HeaderId && q.MOMNo == m4.MOMNo && q.LineId == m4.LineId).FirstOrDefault();
                obj.ByWhom = m4.ByWhom;
                if (m4.ByWhen.ToString() != null)
                {
                    obj.ByWhen = DateTime.ParseExact(m4.ByWhen.ToString(), @"d/M/yyyy", CultureInfo.InvariantCulture); //DateTime.ParseExact(PenetrantBatchDate1, "MM/dd/yyyy", CultureInfo.InvariantCulture);// DateTime.Parse(m4.ByWhen.ToString());
                }

                obj.Description = m4.Description;
                obj.Completed = m4.Completed;
                obj.EditedBy = objClsLoginInfo.UserName;
                obj.EditedOn = DateTime.Now;
                db.Entry(obj).State = EntityState.Modified;
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;

            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult deleteAction(int HeaderId, int MOMNo, int LineID)
        {

            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var obj = db.MOM004.Where(q => q.HeaderId == HeaderId && q.MOMNo == MOMNo && q.LineId == LineID).FirstOrDefault();
                db.MOM004.Remove(obj);
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;

            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult addAction(MOM004 m4)
        {

            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            int? mom = m4.MOMNo;
            try
            {
                var ser = (from m in db.MOM004
                           where m.MOMNo == mom
                           select m).Max(m => m.Serial);

                m4.Serial = ser + 1;
                m4.EditedBy = objClsLoginInfo.UserName;
                m4.EditedOn = DateTime.Now;
                m4.CreatedBy = objClsLoginInfo.UserName;
                m4.CreatedOn = DateTime.Now;
                db.MOM004.Add(m4);
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;

            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult setAgenda(MOM003 m3)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var obj = db.MOM003.Where(q => q.HeaderId == m3.HeaderId && q.MOMNo == m3.MOMNo && q.SerialNo == m3.SerialNo).FirstOrDefault();
                if (obj != null)
                {
                    obj.Discussed = m3.Discussed;
                    obj.Remarks = m3.Remarks;
                    obj.EditedBy = objClsLoginInfo.UserName;
                    obj.EditedOn = DateTime.Now;
                    db.Entry(obj).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    obj = new MOM003();
                    var objmom1 = db.MOM001.Where(a => a.HeaderId == m3.HeaderId).FirstOrDefault();
                    obj.SerialNo = m3.SerialNo;
                    obj.Discussed = m3.Discussed;
                    //obj.Project = objmom1.Project;
                    obj.MOMNo = objmom1.MOMNo;
                    obj.HeaderId = objmom1.HeaderId;
                    obj.CreatedBy = objClsLoginInfo.UserName;
                    obj.CreatedOn = DateTime.Now;
                    db.Entry(obj).State = EntityState.Added;
                    db.MOM003.Add(obj);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;

            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetHeaderGridDataPartial(bool IsDisplayOnly = false, string project = "")
        {
            ViewBag.IsDisplayOnly = IsDisplayOnly;
            ViewBag.Project = project;
            return PartialView("_GetHeaderGridDataPartial");
        }

        public ActionResult GetAllLinesGridDataPartial(bool IsDisplayOnly = false, string project = "", string status = "")
        {
            ViewBag.IsDisplayOnly = IsDisplayOnly;
            ViewBag.Project = project;
            ViewBag.status = status;
            return PartialView("_GetAllLinesGridDataPartial");
        }

        #endregion

        #region DataHandlers

        [NonAction]
        public string getDeptFromPS(string psno)
        {
            var dept = "";
            try
            {
                dept = (from c3 in db.COM003
                        join
                            c2 in db.COM002 on c3.t_depc equals c2.t_dimx
                        where c3.t_psno == psno && c2.t_dtyp == 3 && c3.t_actv == 1
                        select c3.t_depc + "-" + c2.t_desc).FirstOrDefault();

                return dept;

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return dept;
            }

        }

        [HttpPost]
        public async Task<JsonResult> getDept(string psno)
        {
            var dept = "";
            var emp = "";
            var empid = "";
            try
            {
                dept = await (from c3 in db.COM003
                              join c2 in db.COM002 on c3.t_depc equals c2.t_dimx
                              where c3.t_psno == psno && c2.t_dtyp == 3 && c3.t_actv == 1
                              select c3.t_depc + "-" + c2.t_desc).FirstOrDefaultAsync();
                emp = await (from c3 in db.COM003

                             where c3.t_psno == psno && c3.t_actv == 1
                             select c3.t_name).FirstOrDefaultAsync();


                empid = await (from c3 in db.COM003

                               where c3.t_psno == psno && c3.t_actv == 1
                               select c3.t_psno).FirstOrDefaultAsync();




                return Json(new { success = true, message = dept, empname = emp, id = empid }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public JsonResult getProjStatus(string projectCode)
        {
            var ppdd = "";
            var sql = "";
            var pdd = "";
            var cc = "";
            var bc = "";
            var chDuration = "";
            var chainRemainingDuration = "";
            try
            {
                string conn = ConfigurationManager.ConnectionStrings["LNConcerto"].ToString();
                //"Data Source=PHZPDSQLDB2K12;Initial Catalog=EDW;User ID=edw_read;Password=edw@#@!;"
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(conn))
                {
                    //sql = "select distinct * from ProjectDashboard where PROJECTNAME='"+projectCode+"'";
                    sql = "select PROJECTED_COMPLETION from Project where PROJECTNAME = '" + projectCode + "'";
                    using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(sql, con))
                    {
                        con.Open();
                        System.Data.SqlClient.SqlDataReader sdr = cmd.ExecuteReader();
                        if (sdr.HasRows)
                        {
                            while (sdr.Read())
                            {
                                pdd = sdr["PROJECTED_COMPLETION"].ToString();
                            }
                        }
                    }
                    sql = "select  PERCENTPENETRATION as BC,CHAINDURATION , CHAINREMAININGDURATION from S2M_BUFFERHISTORY where PROJECTNAME = '" + projectCode + "'";
                    using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(sql, con))
                    {
                        System.Data.SqlClient.SqlDataReader sdr = cmd.ExecuteReader();
                        if (sdr.HasRows)
                        {
                            while (sdr.Read())
                            {
                                bc = sdr["BC"].ToString();
                                chDuration = sdr["CHAINDURATION"].ToString();
                                chainRemainingDuration = sdr["CHAINREMAININGDURATION"].ToString();
                            }
                        }
                    }
                    con.Close();
                }
                if (chDuration != "" && chainRemainingDuration != "")
                {
                    if ((float.Parse(chDuration) - float.Parse(chainRemainingDuration)) == 0.0)
                    {
                        cc = "0";
                    }
                    else
                    {

                        cc = (((float.Parse(chDuration) - float.Parse(chainRemainingDuration)) / float.Parse(chDuration)) * 100).ToString();


                    }
                }
                else
                {
                    cc = "";
                }

                /*var cdd = (from proj in db.COM004
                            join con in db.COM008 on proj.t_cono equals con.t_cono
                            where proj.t_cprj == projectCode
                            select con.t_ccdd).FirstOrDefault();*/
                DateTime? cdd = GetCDDFromProject(projectCode);

                var pdd1 = (from cd in db.MOM005
                            where cd.Project == projectCode
                            orderby cd.MOMNo descending
                            select cd.PDDCur
                            ).FirstOrDefault();

                ppdd = pdd1.HasValue ? pdd1.Value.ToString("dd-MM-yyyy") : "";
                if (pdd != "")
                {
                    pdd = DateTime.Parse(pdd).ToString("dd-MM-yyyy");
                }
                var cddStr =  cdd.Value == DateTime.MinValue || !cdd.HasValue ? "" : cdd.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                return Json(new { success = true, PDD = pdd, CC = cc.ToString(), BC = bc.ToString(), CDD = cddStr, PPDD = ppdd }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public JsonResult saveHeader(MOM001 m1)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (m1 != null)
                {
                    m1.CreatedBy = objClsLoginInfo.UserName;
                    m1.EditedBy = objClsLoginInfo.UserName;
                    m1.CreatedOn = DateTime.Now;
                    m1.EditedOn = DateTime.Now;

                    var MOMs = db.MOM001.Where(w => w.Project == m1.Project).Select(s => s.MOMNo).OrderByDescending(o => o);
                    m1.MOMNo = MOMs.Count() == 0 ? 0 : MOMs.FirstOrDefault() + 1;

                    //if (!m1.CDD.HasValue || (m1.CDD.HasValue && m1.CDD.Value.Year <= 1754))
                    //    m1.CDD = null;

                    db.MOM001.Add(m1);
                    db.SaveChanges();

                    var getMOM = (from m in db.MOM001
                                  where m.CreatedBy == objClsLoginInfo.UserName
                                  orderby m.MOMNo descending
                                  select m.MOMNo + "-" + m.HeaderId).FirstOrDefault();

                    var objMOM001 = (from m in db.MOM001
                                     where m.CreatedBy == objClsLoginInfo.UserName
                                     orderby m.MOMNo descending
                                     select m).FirstOrDefault();


                    //#region Send Notification
                    //(new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(), objMOM001.Project, "", "", Manager.GetPDINDocumentNotificationMsg(objMOM001.Project, clsImplementationEnum.Mom.Project_Review_Meeting.GetStringValue(), "", ""), clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(), Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.Mom.Project_Review_Meeting.GetStringValue(), objMOM001.HeaderId.ToString(), true), objHTC001.ApprovedBy);
                    //#endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = getMOM;
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult saveProject(List<MOM005> m5)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (m5.Count > 0)
                {
                    for (int i = 0; i < m5.Count; i++)
                    {
                        if (!m5[i].CDD.HasValue || (m5[i].CDD.HasValue && m5[i].CDD.Value.Year <= 1754))
                            m5[i].CDD = null;
                        //m5[i].PDD = DateTime.Now;
                        m5[i].CreatedBy = objClsLoginInfo.UserName;
                        m5[i].EditedBy = objClsLoginInfo.UserName;
                        m5[i].CreatedOn = DateTime.Now;
                        m5[i].EditedOn = DateTime.Now;

                        db.MOM005.Add(m5[i]);

                    }

                    db.SaveChanges();

                    var getMOM = (from m in db.MOM001
                                  where m.CreatedBy == objClsLoginInfo.UserName
                                  orderby m.MOMNo descending
                                  select m.MOMNo + "-" + m.HeaderId).FirstOrDefault();

                    string project = "";
                    for (int i = 0; i < m5.Count; i++)
                    {
                        project = project + m5[i].Project + ",";
                    }
                    project = project.Substring(0, (project.Length - 1));
                    int momNo = m5[0].MOMNo;
                    var obj = db.MOM001.Where(q => q.MOMNo == momNo).FirstOrDefault();
                    obj.Project = project;
                    db.Entry(obj).State = EntityState.Modified;
                    db.SaveChanges();

                    objResponseMsg.Value = getMOM;
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult saveAttendee(List<MOM002> m2)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (m2 != null)
                {
                    for (int i = 0; i < m2.Count; i++)
                    {
                        var EMPNAME = m2[i].Attendee;
                        var emp = db.COM003.Where(m => m.t_name == EMPNAME && m.t_actv == 1).FirstOrDefault();
                        m2[i].Attendee = emp.t_psno;
                        if (m2[i].Department == "-Empty dimension for reconcilia")
                        {
                            m2[i].Department = null;
                        }
                        else
                        {
                            m2[i].Department = m2[i].Department.Split('-')[0];
                        }

                        m2[i].CreatedBy = objClsLoginInfo.UserName;
                        m2[i].EditedBy = objClsLoginInfo.UserName;
                        m2[i].CreatedOn = DateTime.Now;
                        m2[i].EditedOn = DateTime.Now;
                        db.MOM002.Add(m2[i]);

                    }

                    db.SaveChanges();

                    var getMOM = (from m in db.MOM001
                                  where m.CreatedBy == objClsLoginInfo.UserName
                                  orderby m.MOMNo descending
                                  select m.MOMNo + "-" + m.HeaderId).FirstOrDefault();

                    var objMom001 = (from m in db.MOM001
                                     where m.CreatedBy == objClsLoginInfo.UserName
                                     orderby m.MOMNo descending
                                     select m).FirstOrDefault();
                    var Objmom002 = db.MOM002.Where(m => m.HeaderId == objMom001.HeaderId).ToList();

                    foreach (var user in Objmom002)
                    {

                        var Objmom005 = db.MOM005.Where(m => m.HeaderId == objMom001.HeaderId).ToList();
                        if (Objmom005 != null)
                        {
                            StringBuilder projectbuilder = new StringBuilder();
                            foreach (var item in Objmom005.ToList()) // Loop through all strings
                            {
                                projectbuilder.Append(item.Project).Append(","); // Append string to StringBuilder
                            }
                            string project = projectbuilder.ToString();
                            string projectofattendie = project.Substring(0, project.Length - 1);

                            #region Send Notification
                            if (!string.IsNullOrWhiteSpace(objMom001.Project) && !string.IsNullOrWhiteSpace(user.Attendee))
                            {
                                (new clsManager()).SendNotification("", objMom001.Project, "", "", Manager.GetMOMNotificationMsg(projectofattendie), clsImplementationEnum.NotificationType.Information.GetStringValue(), Manager.GetMOMNotificationurl(Convert.ToString(user.HeaderId), Convert.ToString(user.MOMNo)), user.Attendee);
                            }
                            #endregion
                        }
                    }

                    //#region Send Notification
                    //(new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG2.GetStringValue(), objHTC001.Project, "", "", Manager.GetPDINDocumentNotificationMsg(objHTC001.Project, clsImplementationEnum.PlanList.Heat_Treatment_Charge_Sheet.GetStringValue(), objHTC001.RevNo.Value.ToString(), objHTC001.Status), clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(), Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Heat_Treatment_Charge_Sheet.GetStringValue(), objHTC001.HeaderId.ToString(), true), objHTC001.ApprovedBy);
                    //#endregion

                    objResponseMsg.Value = getMOM;
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult saveAgenda(List<MOM003> m3)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                //if (m3.Count > 0)
                //{
                for (int i = 0; i < m3.Count; i++)
                {

                    m3[i].CreatedBy = objClsLoginInfo.UserName;
                    m3[i].EditedBy = objClsLoginInfo.UserName;
                    m3[i].CreatedOn = DateTime.Now;
                    m3[i].EditedOn = DateTime.Now;
                    db.MOM003.Add(m3[i]);

                }

                db.SaveChanges();

                var getMOM = (from m in db.MOM001
                              where m.CreatedBy == objClsLoginInfo.UserName
                              orderby m.MOMNo descending
                              select m.MOMNo + "-" + m.HeaderId).FirstOrDefault();


                objResponseMsg.Value = getMOM;
                //}
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult saveActionPoints(List<MOM004> m4)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (m4.Count > 0)
                {
                    for (int i = 0; i < m4.Count; i++)
                    {

                        m4[i].CreatedBy = objClsLoginInfo.UserName;
                        m4[i].EditedBy = objClsLoginInfo.UserName;
                        m4[i].CreatedOn = DateTime.Now;
                        m4[i].EditedOn = DateTime.Now;
                        db.MOM004.Add(m4[i]);

                    }
                    db.SaveChanges();
                    var objMom001 = (from m in db.MOM001
                                     where m.CreatedBy == objClsLoginInfo.UserName
                                     orderby m.MOMNo descending
                                     select m).FirstOrDefault();
                    var Objmom004 = db.MOM004.Where(m => m.HeaderId == objMom001.HeaderId).ToList();

                    foreach (var user in Objmom004)
                    {

                        var Objmom005 = db.MOM005.Where(m => m.HeaderId == objMom001.HeaderId).ToList();
                        if (Objmom005 != null)
                        {
                            StringBuilder projectbuilder = new StringBuilder();
                            foreach (var item in Objmom005.ToList()) // Loop through all strings
                            {
                                projectbuilder.Append(item.Project).Append(","); // Append string to StringBuilder
                            }
                            string project = projectbuilder.ToString();
                            string projectofeuser = project.Substring(0, project.Length - 1);

                            #region Send Notification

                            if (!string.IsNullOrWhiteSpace(objMom001.Project))
                            {
                                (new clsManager()).SendNotification("", objMom001.Project, "", "", Manager.GetMOMNotificationMsg(projectofeuser), clsImplementationEnum.NotificationType.Information.GetStringValue(), Manager.GetMOMNotificationurl(Convert.ToString(user.HeaderId), Convert.ToString(user.MOMNo)), user.ByWhom);
                            }
                            #endregion
                        }

                    }
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult loadLinesDataTable(JQueryDataTableParamModel param, string HeaderId, string MOMNO)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;


                var MOMAtten = (from m in db.MOM002
                                where m.MOMNo == Int32.Parse(MOMNO) && m.HeaderId == Int32.Parse(HeaderId)
                                select m).ToList();
                int? totalRecords = MOMAtten.Count;

                var res = from c in MOMAtten
                          select new[] {
                                        Convert.ToString(c.LineId),
                                        Convert.ToString(c.Attendee),
                                        Convert.ToString(Manager.GetUserNameFromPsNo(c.Attendee)),
                                        Convert.ToString(c.Department),
                                        ""
                          };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,

                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""

                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult saveVenue(int HeaderId, int MOMNO, string Venue, string Project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var obj = db.MOM001.Where(q => q.HeaderId == HeaderId && q.MOMNo == MOMNO).FirstOrDefault();
                obj.Venue = Venue;
                obj.Project = Project;
                obj.EditedBy = objClsLoginInfo.UserName;
                obj.EditedOn = DateTime.Now;
                db.Entry(obj).State = EntityState.Modified;
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;

            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult saveMdate(int HeaderId, int MOMNO, string Mdate)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var obj = db.MOM001.Where(q => q.HeaderId == HeaderId && q.MOMNo == MOMNO).FirstOrDefault();
                if (Mdate != null)
                {
                    obj.MeetingDate = DateTime.ParseExact(Mdate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                }
                obj.EditedBy = objClsLoginInfo.UserName;
                obj.EditedOn = DateTime.Now;
                db.Entry(obj).State = EntityState.Modified;
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;

            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        // [SessionExpireFilter]
        public ViewResult AddHeader(int? HeaderId, int? MOMNo)
        {
            if (HeaderId > 0 && MOMNo > 0)
            {
                List<MOM004> data = null;
                // List<MOM005> proj = null;
                List<MOM005> proj = new List<MOM005>();
                try
                {
                    data = (from m in db.MOM004
                            where m.HeaderId == HeaderId && m.MOMNo == MOMNo && m.Completed == false
                            select m).ToList();

                    foreach (var dt in data)
                    {
                        dt.ByWhom = dt.ByWhom + "-" + Manager.GetUserNameFromPsNo(dt.ByWhom);
                    }

                    proj = (from m in db.MOM005
                            where m.HeaderId == HeaderId && m.MOMNo == MOMNo
                            select m).ToList();
                    ViewBag.RequestFrom = "EditHeader";
                    ViewBag.mom004Data = data;
                    ViewBag.mom005Data = proj;
                    return View();
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    return View("EditHeader");
                }
            }
            else
            {
                ViewBag.RequestFrom = "AddHeader";
                return View();
            }

        }

        [HttpPost]
        public JsonResult getAttendee(string HeaderId, string MOMNO)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            MOM002 MOMAtten = null;
            try
            {
                MOMAtten = (from m in db.MOM002
                            where m.MOMNo == Int32.Parse(MOMNO) && m.HeaderId == Int32.Parse(HeaderId)
                            select m).FirstOrDefault();
                MOMAtten.Attendee = MOMAtten.Attendee + "/" + Manager.GetUserNameFromPsNo(MOMAtten.Attendee);
                return Json(MOMAtten, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(MOMAtten, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult getPrintVersion(int headerId)
        {
            var model = db.MOM001.Where(q => q.HeaderId == headerId).FirstOrDefault();
            foreach (var md in model.MOM004)
            {
                md.ByWhom = Manager.GetUserNameFromPsNo(md.ByWhom);
            }
            foreach (var md in model.MOM002)
            {
                md.Attendee = Manager.GetUserNameFromPsNo(md.Attendee);
                md.Department = getDeptFromPS(md.Attendee);
            }
            foreach (var prj in model.MOM005)
            {
                prj.Project = GetProjectDescr(prj.Project);
            }
            model.CreatedBy = Manager.GetUserNameFromPsNo(model.CreatedBy);

            return View("PrintFormat", model);
        }

        public JsonResult GetProjects(string term)
        {
            try
            {
                var items = from li in db.COM001
                            where li.t_cprj.Contains(term) || li.t_dsca.Contains(term)
                            select new
                            {
                                id = li.t_cprj,
                                text = li.t_cprj + "-" + li.t_dsca
                            };
                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        [NonAction]
        public string GetProjectDescr(string proj)
        {
            var items = "";
            try
            {
                items = (from li in db.COM001
                         where li.t_cprj == proj
                         select li.t_cprj + "-" + li.t_dsca).FirstOrDefault();

                return items;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return items;
            }
        }

        [HttpPost]
        public JsonResult GetProductTypesAutoComplete(string term)
        {
            var vals = clsImplementationEnum.getProductCategory();
            var filtered = vals.Where(q => q.ToLower().Contains(term.ToLower())).ToList();
            return Json(filtered, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetProjectsAutoComplete(string term)
        {
            var vals = clsImplementationEnum.getProductCategory();
            var filtered = vals.Where(q => q.ToLower().Contains(term.ToLower())).ToList();
            return Json(filtered, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetProductTypes(string search)
        {
            try
            {
                var vals = clsImplementationEnum.getProductCategory().AsEnumerable();//LNCDataHelper.ProductType.AsEnumerable();
                if (!string.IsNullOrWhiteSpace(search))
                    vals = vals.Where(q => q.ToLower().Contains(search.ToLower()));
                var items = vals.Select(q => new
                {
                    id = q,
                    text = q
                }).AsQueryable();
                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult loadHeaderDataTable(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                var IsDisplayOnly = !string.IsNullOrEmpty(Request["IsDisplayOnly"]) ? Convert.ToBoolean(Request["IsDisplayOnly"].ToString()) : false;
                //var list = db.LNC001.ToList();
                string whereCondition = " MOMNo != 0 and (Venue is not null and Venue <> ' ') ";

                if (!string.IsNullOrWhiteSpace(param.Project))
                {
                    whereCondition += " and Project='" + param.Project + "'";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += "and(" +
                        "Project like '%" + param.sSearch + "%' or " +
                        "MOMNo like '%" + param.sSearch + "%' or " +
                        "Venue like '%" + param.sSearch + "%'" +
                        ")";
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var data = db.SP_MOM_GET_DETAILS(StartIndex, EndIndex, "", whereCondition).ToList();

                int? totalRecords = data.Select(i => i.TotalCount).FirstOrDefault();

                var res = from h in data
                          select new[] {
                           Convert.ToString(h.HeaderId),
                           Convert.ToString(h.MOMNo),
                           Convert.ToString(h.Venue),
                           Convert.ToString(h.MeetingDate.HasValue? h.MeetingDate.Value.ToString("dd/MM/yyyy",CultureInfo.InvariantCulture):""),
                           Convert.ToString(h.CreatedOn.HasValue?h.CreatedOn.Value.ToString("dd/MM/yyyy",CultureInfo.InvariantCulture):""),
                           Convert.ToString(getProjectFromMOM(h.MOMNo,h.HeaderId)),
                           ""
                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }



        public ActionResult loadAllLinesDataTable(JQueryDataTableParamModel param)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                var IsDisplayOnly = !string.IsNullOrEmpty(Request["IsDisplayOnly"]) ? Convert.ToBoolean(Request["IsDisplayOnly"].ToString()) : false;
                string whereCondition = "1 = 1";


                if (!string.IsNullOrWhiteSpace(param.Project))
                {
                    whereCondition += " and mom1.Project='" + param.Project + "'";
                }
                if (!string.IsNullOrWhiteSpace(param.Status))
                {
                    if (param.Status == "ClosedLines")
                    {
                        whereCondition += " and Completed='1'";
                    }
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " and"
                         + " ( maxid.HeaderId like '%" + param.sSearch + "%'"
                         + " or mom4.MOMNo like '%" + param.sSearch + "%'"
                         + " or mom4.Serial like '%" + param.sSearch + "%'"
                         + " or maxid.Description like '%" + param.sSearch + "%'"
                         + " or mom4.ByWhen like '%" + param.sSearch + "%'"
                         + " or mom4.ByWhom like '%" + param.sSearch + "%'"
                         + " or mom4.CreatedOn like '%" + param.sSearch + "%'"
                         + " or mom1.Project like '%" + param.sSearch + "%'"
                         + " or mom4.Completed like '%" + param.sSearch + "%'"
                         + " or mom4.EditedBy like '%" + param.sSearch + "%'"
                         + " or mom4.Remarks like '%" + param.sSearch + "%'"
                         + " or mom4.EditedOn like '%" + param.sSearch + "%')";
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion
                var data = db.SP_MOM_GET_OPEN_ACTION_POINTS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                List<SelectListItem> BoolenList = new List<SelectListItem>() { new SelectListItem { Text = "Yes", Value = "Yes" }, new SelectListItem { Text = "No", Value = "No" } };
                var empty = string.IsNullOrWhiteSpace(param.sSearch);
                int? totalRecords = data.Select(i => i.TotalCount).FirstOrDefault();

                var res = from h in data
                          select new[] {
                            Convert.ToString(h.LineId),
                           Convert.ToString(h.HeaderId),
                           Convert.ToString(h.MOMNo),
                           Convert.ToString(h.Description),
                           IsDisplayOnly ? Convert.ToString(h.ByWhen.HasValue ? ("<lable title='" + Convert.ToString(h.ByWhenHistory) + "' >"+ h.ByWhen.Value.ToString("dd/MM/yyyy",CultureInfo.InvariantCulture) +" </label>") : "") : (h.Completed == true) ? Convert.ToString(h.ByWhen.HasValue ? ("<lable title='" + Convert.ToString(h.ByWhenHistory) + "' >"+ h.ByWhen.Value.ToString("dd/MM/yyyy",CultureInfo.InvariantCulture) +" </label>") : "") : GenerateTextbox(h.LineId, "ByWhen",h.ByWhen.HasValue?h.ByWhen.Value.ToString("yyyy-MM-dd",CultureInfo.InvariantCulture):"","UpdateData(this, "+ h.LineId +")", false, "", "","form-control editable datePicker ByWhen", false,"","",Convert.ToString(h.ByWhenHistory)),
                           Convert.ToString(Getbywhom(h.ByWhom)),
                           Convert.ToString(h.CreatedOn.HasValue ? h.CreatedOn.Value.ToString("dd/MM/yyyy") : ""),
                           Convert.ToString(h.Project),
                           IsDisplayOnly ? Helper.GenerateDropdown(h.LineId, "Completed", new SelectList(BoolenList, "Value", "Text",h.Completed == true ? "Yes":"No"),"","UpdateAllActionData(this, "+h.LineId +");",true) : Helper.GenerateDropdown(h.LineId, "Completed", new SelectList(BoolenList, "Value", "Text",h.Completed == true ? "Yes":"No"),"","UpdateAllActionData(this, "+h.LineId +");",h.Completed == true? true:false),
                           Convert.ToString(h.Completed==true ? Getbywhom(h.EditedBy): ""),
                           Convert.ToString(h.Completed==true ?h.EditedOn.HasValue ? h.EditedOn.Value.ToString("dd/MM/yyyy") : "":""),
                           IsDisplayOnly ? Convert.ToString(h.Remarks) : (h.Completed == true) ? Convert.ToString(h.Remarks) : Helper.GenerateTextbox(h.LineId, "Remarks", Convert.ToString(h.Remarks), "UpdateData(this, "+ h.LineId +");",false,"","50","Remarks"),
                    };
                //res = res.Skip(param.iDisplayStart).Take(param.iDisplayLength);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                }, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [NonAction]
        public string getProjectFromMOM(int MOMNo, int HeaderID)
        {
            List<MOM005> data = (from m5 in db.MOM005
                                 where (m5.MOMNo == MOMNo) && (m5.HeaderId == HeaderID)
                                 select m5).ToList();

            string project = "";
            foreach (var dt in data)
            {
                project = project + dt.Project + ",";
            }
            if (!String.IsNullOrEmpty(project))
            {
                project = project.Substring(0, (project.Length - 1));
            }
            return project;
        }

        [NonAction]
        public static string GenerateTextbox(int rowId, string columnName, string columnValue = "", string onBlurMethod = "",
            bool isReadOnly = false, string inputStyle = "", string maxlength = "", string classname = "", bool disabled = false, string onchange = "", string placeholder = "", string buttonTooltip = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + (rowId < 0 ? "" : rowId.ToString());
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control " + classname;
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";

            htmlControl = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' "
                + MaxCharcters + " value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' title='" + buttonTooltip + "' class='"
                + className + "' style='" + inputStyle + "'  " + onBlurEvent + " spara='" + (rowId < 0 ? "0" : rowId.ToString()) + "' " + (!string.IsNullOrEmpty(onchange) ? "onchange='" + onchange + "'" : "")
                + " placeholder='" + placeholder + "' " + (disabled ? "disabled" : "") + " />";

            return htmlControl;
        }
        #endregion

        #region HelperMethods



        [NonAction]
        public DateTime GetCDDFromProjectOld(string Project)
        {
            try
            {
                string date = Manager.GetContractWiseCdd(Project);
                if (!String.IsNullOrEmpty(date))
                {
                    DateTime dt = DateTime.Parse(date);
                    return dt;
                }
                else
                {
                    return new DateTime();
                }
                //var contract = db.COM005.Where(q => q.t_sprj.Equals(Project)).FirstOrDefault().t_cono;
                //return db.COM008.Where(q => q.t_cono.Equals(contract)).FirstOrDefault().t_ccdd;
            }
            catch (Exception) { return new DateTime(); }
        }


        [NonAction]
        public DateTime GetCDDFromProject(string Project)
        {
            try
            {
                var sql = "";
                var cdd = "";
                DateTime cdddt= new DateTime();
                string conn = ConfigurationManager.ConnectionStrings["LNConcerto"].ToString();
                //"Data Source=PHZPDSQLDB2K12;Initial Catalog=EDW;User ID=edw_read;Password=edw@#@!;"
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(conn))
                {
                    sql = "select FinishDate from Project where PROJECTNAME = '" + Project + "'";
                    using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(sql, con))
                    {
                        con.Open();
                        System.Data.SqlClient.SqlDataReader sdr = cmd.ExecuteReader();
                        if (sdr.HasRows)
                        {
                            while (sdr.Read())
                            {
                                if (!string.IsNullOrWhiteSpace(Convert.ToString(sdr["FinishDate"])))
                                {
                                    cdd = Convert.ToDateTime(sdr["FinishDate"]).ToShortDateString();
                                    // It throws Argument null exception  
                                    cdddt = DateTime.ParseExact(cdd, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                }
                                else {
                                    cdddt = DateTime.MinValue;
                                }
                            }
                        }
                    }                   
                }
                return cdddt;               
            }
            catch (Exception) { return new DateTime(); }
        }

        [NonAction]
        public static string ToBase64(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        [NonAction]
        public static byte[] FromBase64(string base64EncodedData)
        {
            base64EncodedData = base64EncodedData.Substring(base64EncodedData.IndexOf("base64,") + 7);
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return base64EncodedBytes;
        }

        [NonAction]
        public string GetCustomerCodeAndNameByProject(string project)
        {
            var result = (from cm004 in db.COM004
                          join cm005 in db.COM005 on cm004.t_cono equals cm005.t_cono
                          join cm006 in db.COM006 on cm004.t_ofbp equals cm006.t_bpid
                          where cm005.t_sprj == project
                          select cm004.t_ofbp + "-" + cm006.t_nama).FirstOrDefault();
            return (result == null) ? " " : result;
        }

        [NonAction]
        public List<T> FilterList<T>(List<T> inputList, string Filter)
        {
            var outputList = inputList;
            var props = typeof(T).GetProperties();
            foreach (var property in props)
            {
                outputList = outputList.Where(q => property.GetValue(q).ToString().ToLower().Contains(Filter.ToLower())).ToList();
            }
            return outputList;
        }

        [NonAction]
        public string getProjectDescription(string Project)
        {
            return db.COM001.Where(q => q.t_cprj.Equals(Project)).Select(q => q.t_cprj + " - " + q.t_dsca).FirstOrDefault();
        }

        //[NonAction]
        //public string getDocuments(int LineId)
        //{
        //    string Folder = "LNC002/" + LineId.ToString();
        //    string result = "";
        //    var Files = clsUploadKM.getDocs(Folder);
        //    foreach (var file in Files)
        //    {
        //        result += "<input type=\"hidden\" id=\"" + file.Key + "\" value=\"URL:" + file.Value + "\" class=\"attach\" />";
        //    }
        //    return result;
        //}
        #endregion

        #region inline grid
        public ActionResult GetInlineGridActionData(JQueryDataTableParamModel param, int HeaderId, int MOMNo)
        {
            try
            {
                // List<MOM004> lstResult;
                //var total = db.MOM004.Where(li => li.HeaderId == HeaderId && li.MOMNo == MOMNo && li.CreatedBy == objClsLoginInfo.UserName).Count();
                //if (total > param.iDisplayStart)
                //    lstResult = db.MOM004.Where(li => li.HeaderId == HeaderId && li.MOMNo == MOMNo && li.CreatedBy == objClsLoginInfo.UserName).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                //else
                //    lstResult = new List<MOM004>();


                List<MOM004> lstResult = db.MOM004.Where(li => li.HeaderId == HeaderId && li.MOMNo == MOMNo && li.CreatedBy == objClsLoginInfo.UserName).ToList();
                foreach (var item in lstResult)
                {
                    item.MOM001 = null;
                    item.ByWhom = item.ByWhom + '-' + Manager.GetUserNameFromPsNo(item.ByWhom);
                }
                List<SelectListItem> BoolenList = new List<SelectListItem>() { new SelectListItem { Text = "Yes", Value = "Yes" }, new SelectListItem { Text = "No", Value = "No" } };
                int newRecordId = 0;
                var newRecord = new[] {
                                    Helper.GenerateHidden(newRecordId, "LineId", ""),
                                    Helper.GenerateTextbox(newRecordId, "ActionPoints"),
                                    Helper.GenerateTextbox(newRecordId, "ByWhom"),
                                    Helper.GenerateTextbox(newRecordId, "ByWhen"),
                                    Helper.GenerateDropdown(newRecordId, "Completed", new SelectList(BoolenList, "Value", "Text"), " "),
                                    Helper.GenerateGridButton(newRecordId, "Save", "Add Rocord", "fa fa-plus", "SaveNewRecord();" ),
                                    // GenerateAutoComplete(newRecordId,"txtDept","","",false,"","Department")+""+Helper.GenerateHidden(newRecordId,"Department"),
                                };


                var data = (from uc in lstResult
                            select new[]
                            {
                               Helper.GenerateHidden(uc.LineId, "LineId", Convert.ToString(uc.LineId)),
                               Helper.GenerateTextbox(uc.LineId, "ActionPoints", uc.Description, "UpdateData(this, "+ uc.LineId +");"),
                               Helper.GenerateTextbox(uc.LineId, "ByWhom", uc.ByWhom),
                               Helper.GenerateTextbox(uc.LineId, "ByWhen", Convert.ToDateTime(uc.ByWhen).ToString("yyyy-MM-dd",CultureInfo.InvariantCulture), "UpdateData(this, "+ uc.LineId +");"),
                               Helper.GenerateDropdown(uc.LineId, "Completed", new SelectList(BoolenList, "Value", "Text",uc.Completed == true ? "Yes":"No"),"","UpdateData(this, "+ uc.LineId +");"),
                               Helper.GenerateGridButton(uc.LineId, "", "Delete Rocord", "fa fa-trash-o", "RemoveRecord("+ uc.LineId +");"),
                               //GenerateAutoComplete(uc.LineId, "ByWhen",Convert.ToString(uc.ByWhen),"UpdateData(this, "+ uc.LineId +");",false,"","" , false) +""+ Helper.GenerateHidden(uc.LineId,"Department",uc.Department),
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    //sEcho = Convert.ToInt32(param.sEcho),
                    //iTotalRecords = (data.Count > 0 ? data.Count():0),//lstResult.FirstOrDefault().TotalCount : 0),
                    //iTotalDisplayRecords = (data.Count > 0 ? data.Count() : 0),//(lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                },
                JsonRequestBehavior.AllowGet);
                //var list = JsonConvert.SerializeObject(new { success = true, obj = lstResult },
                // Formatting.None,
                // new JsonSerializerSettings()
                // {
                //     ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                // });
                //return Content(list, "application/json");

            }
            catch (Exception ex)
            {
                return Json(new { success = false, obj = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult SaveInlineAction(MOM004 m4)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();

            var ser = (from m in db.MOM004
                       where m.MOMNo == m4.MOMNo
                       select m).Max(m => m.Serial);
            try
            {
                MOM004 objmom004 = new MOM004();
                objmom004.MOMNo = m4.MOMNo;
                objmom004.HeaderId = m4.HeaderId;
                objmom004.Serial = ser + 1;
                objmom004.Description = m4.Description;
                objmom004.ByWhom = m4.ByWhom;
                if (m4.ByWhen.ToString() != null)
                {
                    objmom004.ByWhen = DateTime.ParseExact(m4.ByWhen.ToString(), @"d/M/yyyy", CultureInfo.InvariantCulture); //DateTime.ParseExact(PenetrantBatchDate1, "MM/dd/yyyy", CultureInfo.InvariantCulture);// DateTime.Parse(m4.ByWhen.ToString());
                }
                objmom004.Completed = m4.Completed;
                objmom004.CreatedBy = objClsLoginInfo.UserName;
                objmom004.CreatedOn = DateTime.Now;
                objmom004.EditedBy = objClsLoginInfo.UserName;
                objmom004.EditedOn = DateTime.Now;
                var objheader = db.MOM001.Where(x => x.HeaderId == m4.HeaderId).FirstOrDefault();
                if (objheader != null)
                {
                    objheader.EditedBy = objClsLoginInfo.UserName;
                    objheader.EditedOn = DateTime.Now;
                    objmom004.RefHeaderId = m4.HeaderId;
                }
                db.MOM004.Add(objmom004);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public string GenerateAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false)
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' hdElement='" + hdElementId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + (disabled ? "disabled" : "") + " />";

            return strAutoComplete;
        }

        public ActionResult RemoveInlineAction(int Id)
        {

            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var obj = db.MOM004.Where(q => q.LineId == Id).FirstOrDefault();
                db.MOM004.Remove(obj);
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;

            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateInlineActionData(string rowId, string columnName, string columnValue, int refHeaderid)
        {

            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            int lineID = Int32.Parse(rowId);
            try
            {
                var obj = db.MOM004.Where(q => q.LineId == lineID).FirstOrDefault();

                if (columnName.Equals("Description"))
                {
                    obj.Description = columnValue;
                }
                else if (columnName.Equals("ByWhom"))
                {
                    obj.ByWhom = columnValue;
                }
                else if (columnName.Equals("ByWhen"))
                {
                    if (!string.IsNullOrEmpty(columnValue))
                    {
                        string curbywhen = obj.ByWhen.Value.ToString("dd/MM/yyyy");
                        columnValue = DateTime.Parse(columnValue).ToString("dd/MM/yyyy");

                        if (curbywhen.ToLower() != columnValue.ToLower())
                        {
                            if (!string.IsNullOrEmpty(obj.ByWhenHistory))
                                obj.ByWhenHistory = obj.ByWhenHistory + "," + curbywhen;
                            else
                                obj.ByWhenHistory = curbywhen;

                            var splitDate = columnValue.Split('/');
                            columnValue = splitDate[2] + "/" + splitDate[1] + "/" + splitDate[0];
                            obj.ByWhen = DateTime.Parse(columnValue);
                        }
                    }
                }
                else if (columnName.Equals("Completed"))
                {
                    obj.Completed = columnValue == "Yes" ? true : false;
                    if (obj.Completed)
                    {
                        obj.RefHeaderId = refHeaderid;
                    }
                }
                else if (columnName.Equals("Remarks"))
                {
                    obj.Remarks = columnValue;
                }
                obj.MOM001.EditedBy = objClsLoginInfo.UserName;
                obj.MOM001.EditedOn = DateTime.Now;
                obj.EditedBy = objClsLoginInfo.UserName;
                obj.EditedOn = DateTime.Now;
                db.Entry(obj).State = EntityState.Modified;
                db.SaveChanges();
                objResponseMsg.Key = true;

                //db.SP_QMS_STAGEMASTER_ROW_UPDATE(Convert.ToInt32(rowId), columnName, columnValue);
                objResponseMsg.Key = true;
            }
            catch
            {
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateProjectDetails(string rowId, string projectremarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            objResponseMsg.Key = false;
            int lineID = Int32.Parse(rowId);
            try
            {
                var obj = db.MOM005.Where(q => q.LineId == lineID).FirstOrDefault();

                if (obj != null)
                {
                    obj.Remarks = projectremarks;
                    obj.EditedBy = objClsLoginInfo.UserName;
                    obj.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Remark Successfully Updated";
                }
                else
                {
                    objResponseMsg.Value = "Project not available for Update.";
                }
            }
            catch
            {
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateAttendeesDetails(string rowId, string attendeesremarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            objResponseMsg.Key = false;
            int lineID = Int32.Parse(rowId);
            try
            {
                var obj = db.MOM002.Where(q => q.LineId == lineID).FirstOrDefault();

                if (obj != null)
                {
                    obj.Remarks = attendeesremarks;
                    obj.EditedBy = objClsLoginInfo.UserName;
                    obj.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Remark Successfully Updated";
                }
                else
                {
                    objResponseMsg.Value = "Project not available for Update.";
                }
            }
            catch
            {
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateInlineAllActionData(string rowId, string columnName, string columnValue)
        {

            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            int lineID = Int32.Parse(rowId);
            try
            {
                var obj = db.MOM004.Where(q => q.LineId == lineID).FirstOrDefault();


                if (columnName.Equals("Completed"))
                {
                    obj.Completed = columnValue == "Yes" ? true : false;
                }

                obj.EditedBy = objClsLoginInfo.UserName;
                obj.EditedOn = DateTime.Now;
                db.Entry(obj).State = EntityState.Modified;
                db.SaveChanges();
                objResponseMsg.Key = true;

                //db.SP_QMS_STAGEMASTER_ROW_UPDATE(Convert.ToInt32(rowId), columnName, columnValue);
                objResponseMsg.Key = true;
            }
            catch
            {
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Code Added by Nikita

        #region Project wise mom page
        [SessionExpireFilter]
        public ActionResult MaintainProjectwiseMoM()
        {
            ViewBag.IsDisplayOnly = false;
            return View();
        }

        [HttpPost]
        public ActionResult GetProjectWiseIndexData(JQueryDataTableParamModel param)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(param.Headerid);
                int MomNo = Convert.ToInt32(param.Title);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1 ";

                string strSortOrder = string.Empty;
                bool IsDisplayOnly = param.Flag;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                string[] columnName = { "M1.PROJECT", "[dbo].[FUN_GET_CDD_BY_PROJECT](M1.PROJECT) ", "MOMCOUNT", "[dbo].[GET_PROJECTDESC_BY_CODE](M1.PROJECT)", "[dbo].[FUN_GET_CUSTOMER_CODEANDNAME_BY_PROJECT](M1.PROJECT)" };
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }

                var lstResult = db.SP_MOM_GET_PROJECTWISE_MOM_INDEX(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                Helper.HTMLAutoComplete(newRecordId, "txtPROJECT","","",false,"","PROJECT" , false) +""+ Helper.GenerateHidden(newRecordId,"PROJECT"),
                                Helper.GenerateLabelFor(newRecordId,"CUSTOMER"),
                                "",
                                Helper.GenerateLabelFor(newRecordId,"CDD"),
                                Helper.GenerateLabelFor(newRecordId,"MOMCOUNT"),
                                Helper.GenerateLabelFor(newRecordId,"LASTMOMDATE"),
                                Helper.GenerateGridButton(newRecordId, "Save", "Add Rocord", "fa fa-plus", "SaveNewProject();" ),
                               };
                var data = (from uc in lstResult
                            select new[]
                                {
                                Convert.ToString(uc.PROJECTDESCRIPTION),
                                Convert.ToString(uc.CUSTOMER),
                                uc.ZERODATE.HasValue ? uc.ZERODATE.Value.ToString("dd/MM/yyy",CultureInfo.InvariantCulture):"",
                                uc.CDD.HasValue ? uc.CDD.Value.ToString("dd/MM/yyy",CultureInfo.InvariantCulture):"",
                                Convert.ToString(uc.MOMCOUNT.HasValue? uc.MOMCOUNT: 0) ,
                                Convert.ToString((!uc.MOMCOUNT.HasValue || uc.MOMCOUNT ==0 ) ? "" : uc.LASTMOMDATE),
                                Helper.GenerateActionIcon(Convert.ToInt32(uc.ROW_NO), "AddMoM", "Add MoM", "fa fa-plus", "",WebsiteURL+"/MOM/Maintain/"+ (!string.IsNullOrWhiteSpace(uc.PROJECT) ? (IsDisplayOnly ? "DisplayMom?p=" +uc.PROJECT.Trim() : "Index?p="+uc.PROJECT.Trim()) : "Index"),false)
                               }).ToList();
                if (!IsDisplayOnly)
                {
                    data.Insert(0, newRecord);
                }
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetProjctCustomerCDD(string project)
        {
            projectModel obj = new projectModel();
            ApproverModel objApproverModel = Manager.GetCustomerProjectWise(project);
            try
            {
                if (db.MOM001.Where(x => x.Project == project).Any())
                {
                    obj.key = false;
                    obj.value = clsImplementationMessage.CommonMessages.Duplicate;
                }
                else
                {
                    obj.key = true;
                    obj.Name = objApproverModel != null ? objApproverModel.Code + "-" + objApproverModel.Name : "";
                    string strCdd = Manager.GetContractWiseCdd(project);
                    if (!string.IsNullOrWhiteSpace(strCdd))
                    {
                        DateTime dtCdd = Convert.ToDateTime(strCdd);
                        obj.Cdd = dtCdd.Date.ToShortDateString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveProjectwiseData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string currentUser = objClsLoginInfo.UserName;
                var currentLoc = objClsLoginInfo.Location;
                int newid = 0;
                MOM001 objMOM001 = new MOM001();
                objMOM001.Project = Convert.ToString(fc["PROJECT0"]);

                var MOMs = db.MOM001.Where(w => w.Project == objMOM001.Project).Select(s => s.MOMNo).OrderByDescending(o => o);
                objMOM001.MOMNo = MOMs.Count() == 0 ? 0 : MOMs.FirstOrDefault() + 1;

                objMOM001.Venue = " ";
                objMOM001.MeetingDate = null;
                objMOM001.CreatedBy = objClsLoginInfo.UserName;
                objMOM001.CreatedOn = DateTime.Now;
                db.MOM001.Add(objMOM001);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;
                objResponseMsg.HeaderId = newid;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region header MOM001
        /// <summary>
        /// Main Header Page (Added on 7- aug-2018)
        /// </summary>
        /// <param name="HeaderId"></param>
        /// <param name="MOMNo"></param>
        /// <returns></returns>
        [SessionExpireFilter]
        public ActionResult AddMomHeader(string p = "", int? HeaderId = 0, int? n = 0)
        {
            ViewBag.IsDisplayOnly = false;
            MOM001 objMOM001 = new MOM001();
            objMOM001.Project = p;
            if (HeaderId > 0)
            {
                List<MOM005> proj = new List<MOM005>();

                objMOM001 = db.MOM001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                if (n == 1)
                {
                    ViewBag.Action = "new";
                }
                else
                {
                    ViewBag.Action = "edit";
                }
            }
            return View(objMOM001);
        }

        [SessionExpireFilter]
        public ActionResult DisplayMomHeader(int? HeaderId, int? n)
        {
            ViewBag.IsDisplayOnly = true;
            MOM001 objMOM001 = new MOM001();
            if (HeaderId > 0)
            {
                List<MOM005> proj = new List<MOM005>();

                objMOM001 = db.MOM001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                if (n == 1)
                {
                    ViewBag.Action = "new";
                }
                else
                {
                    ViewBag.Action = "edit";
                }
            }

            return View("AddMomHeader", objMOM001);
        }

        /// <summary>
        /// Save header and generate MOM Number (MOM001)
        /// </summary>
        /// <param name="m1"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SaveMOMHeader(MOM001 m1)
        {
            MOM001 objMOM001 = new MOM001();
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (m1.HeaderId > 0)
                {
                    objMOM001 = db.MOM001.Where(x => x.HeaderId == m1.HeaderId).FirstOrDefault();
                    objMOM001.Venue = m1.Venue;
                    objMOM001.MeetingDate = m1.MeetingDate;
                    objMOM001.EditedBy = objClsLoginInfo.UserName;
                    objMOM001.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Value = "MOM No '" + objMOM001.MOMNo + "' is successfully updated.";
                }
                else
                {
                    var MOMs = db.MOM001.Where(w => w.Project == m1.Project).Select(s => s.MOMNo).OrderByDescending(o => o);
                    objMOM001.MOMNo = MOMs.Count() == 0 ? 0 : MOMs.FirstOrDefault() + 1;
                    objMOM001.Venue = m1.Venue;
                    objMOM001.Project = m1.Project;
                    objMOM001.MeetingDate = m1.MeetingDate;
                    objMOM001.CreatedBy = objClsLoginInfo.UserName;
                    objMOM001.CreatedOn = DateTime.Now;
                    db.MOM001.Add(objMOM001);
                    db.SaveChanges();

                    var mom = (from mom5 in db.MOM005
                               join mom1 in db.MOM001 on mom5.HeaderId equals mom1.HeaderId
                               where mom1.Project == objMOM001.Project
                               select new
                               {
                                   Project = mom5.Project,
                               }).Distinct().ToList().Select(x => x.Project).ToList();

                    string projectcode = string.Join(",", mom);
                    objResponseMsg = insertmom005(objMOM001.HeaderId, objMOM001.MOMNo, projectcode, "");

                    objResponseMsg.Value = "MOM No '" + objMOM001.MOMNo + "' is successfully created.";

                }
                objResponseMsg.Key = true;
                objResponseMsg.HeaderId = objMOM001.HeaderId;
                objResponseMsg.RequestId = objMOM001.MOMNo;
                objResponseMsg.item = Manager.GetPsidandDescription(objMOM001.CreatedBy);
                objResponseMsg.itemdesc = objMOM001.CreatedOn.Value.ToString("dd/MM/yyyy");
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Project Partial

        [HttpPost]
        public ActionResult GetProjectsPartial() //partial for Copy Details
        {
            return PartialView("_AddProjectPartial");
        }

        [HttpPost]
        public ActionResult SaveProjects(int headerid, int MOMNo, string strprojectCode, string remark)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();

            try
            {
                objResponseMsg = insertmom005(headerid, MOMNo, strprojectCode, remark);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetProjectData(JQueryDataTableParamModel param)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(param.Headerid);
                int MomNo = Convert.ToInt32(param.Title);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = " HeaderId = " + intHeaderId + " and MOMNo=" + MomNo;

                string strSortOrder = string.Empty;

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (Project like '%" + param.sSearch
                        + "%' or CDD like '%" + param.sSearch + "%' or PDD like '%" + param.sSearch + "%' or PDDCur like '%" + param.sSearch
                        + "%')";
                }
                bool isComplete = param.Flag;
                bool isCompletebtn = false;
                bool IsPrjExists = db.MOM005.Any(x => x.HeaderId == intHeaderId);
                bool IstattendeeExists = db.MOM002.Any(x => x.HeaderId == intHeaderId);
                if (IstattendeeExists == true && IsPrjExists == true)
                {
                    isCompletebtn = true;
                }
                var lstResult = db.SP_MOM_GET_PROJECT_DETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                                {
                                "1",
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(Manager.GetProjectAndDescription(uc.Project)),
                                uc.CDD.HasValue ? uc.CDD.Value.ToString("dd/MM/yyy",CultureInfo.InvariantCulture):"",
                                uc.PDD.HasValue ? uc.PDD.Value.ToString("dd/MM/yyy",CultureInfo.InvariantCulture):"",
                                uc.PDDCur.HasValue ? uc.PDDCur.Value.ToString("dd/MM/yyy",CultureInfo.InvariantCulture):"",
                                Convert.ToString(uc.CC),
                                Convert.ToString(uc.BC),
                                //Convert.ToString(uc.Remarks),
                                isComplete ? uc.Remarks : Helper.GenerateTextbox(uc.LineId, "Remarks", uc.Remarks, "UpdateProjectData(this, "+ uc.LineId +");"),
                                Helper.GenerateActionIcon(uc.LineId,"Delete","Delete Record","fa fa-trash-o", "DeleteRecord("+ uc.LineId +",'/MOM/Maintain/DeleteProject','Project','tblProject');","", isComplete ? true:false)
                               }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    isCompletebtn = isCompletebtn,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetProjectResult()
        {
            List<CategoryData> lstProjects = new List<CategoryData>();
            var objAccessProjects = db.fn_GetProjectAccessForEmployee(objClsLoginInfo.UserName, "").ToList();
            var lstPDN001 = db.PDN001.Select(s=>new { s.Project, s.IProject }).ToList();
            lstProjects = (from c1 in db.COM001
                           where objAccessProjects.Contains(c1.t_cprj)
                           select new CategoryData
                           {
                               id = c1.t_cprj,
                               text = c1.t_cprj + " - " + c1.t_dsca
                           }
                          ).Distinct().ToList()
                          .Where(w => lstPDN001.Any(a=>a.Project == w.id || (a.IProject != null && a.IProject.Split(',').Contains(w.id)))).ToList();
            return Json(lstProjects, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult getProjectDetails(string strprojectCode)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            var lstprojects = strprojectCode.Split(',');
            List<Projects> lstdetails = new List<Projects>();

            foreach (var projectCode in lstprojects)
            {
                var ppdd = "";
                var sql = "";
                var pdd = "";
                var cc = "";
                var bc = "";
                var chDuration = "";
                var chainRemainingDuration = "";

                string conn = ConfigurationManager.ConnectionStrings["LNConcerto"].ToString();
                //"Data Source=PHZPDSQLDB2K12;Initial Catalog=EDW;User ID=edw_read;Password=edw@#@!;"
                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(conn))
                {
                    //sql = "select distinct * from ProjectDashboard where PROJECTNAME='"+projectCode+"'";
                    sql = "select PROJECTED_COMPLETION from Project where PROJECTNAME = '" + projectCode + "'";
                    using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(sql, con))
                    {
                        con.Open();
                        System.Data.SqlClient.SqlDataReader sdr = cmd.ExecuteReader();
                        if (sdr.HasRows)
                        {
                            while (sdr.Read())
                            {
                                pdd = sdr["PROJECTED_COMPLETION"].ToString();
                            }
                        }
                    }
                    sql = "select  PERCENTPENETRATION as BC,CHAINDURATION , CHAINREMAININGDURATION from S2M_BUFFERHISTORY where PROJECTNAME = '" + projectCode + "'";
                    using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(sql, con))
                    {
                        System.Data.SqlClient.SqlDataReader sdr = cmd.ExecuteReader();
                        if (sdr.HasRows)
                        {
                            while (sdr.Read())
                            {
                                bc = sdr["BC"].ToString();
                                chDuration = sdr["CHAINDURATION"].ToString();
                                chainRemainingDuration = sdr["CHAINREMAININGDURATION"].ToString();
                            }
                        }
                    }
                    con.Close();
                }
                if (chDuration != "" && chainRemainingDuration != "")
                {
                    if ((float.Parse(chDuration) - float.Parse(chainRemainingDuration)) == 0.0)
                    {
                        cc = "0";
                    }
                    else
                    {

                        cc = (((float.Parse(chDuration) - float.Parse(chainRemainingDuration)) / float.Parse(chDuration)) * 100).ToString();
                    }
                }
                else
                {
                    cc = "";
                }

                DateTime? cdd = GetCDDFromProject(projectCode);

                var pdd1 = (from cd in db.MOM005
                            where cd.Project == projectCode
                            orderby cd.MOMNo descending
                            select cd.PDDCur
                            ).FirstOrDefault();

                ppdd = pdd1.HasValue ? pdd1.Value.ToString("dd-MM-yyyy") : "";
                if (pdd != "")
                {
                    pdd = DateTime.Parse(pdd).ToString("dd-MM-yyyy");
                }
                var cddStr = cdd.Value == DateTime.MinValue || !cdd.HasValue ? "" : cdd.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                lstdetails.Add(new Projects { projectCode = projectCode, projectDescription = Manager.GetProjectAndDescription(projectCode), PDD = pdd, CC = cc.ToString(), BC = bc.ToString(), CDD = cddStr, PPDD = ppdd });
            }
            return Json(lstdetails, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProject(int lineid)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                MOM005 objMOM005 = db.MOM005.Where(x => x.LineId == lineid).FirstOrDefault();
                if (objMOM005 != null)
                {
                    db.MOM005.Remove(objMOM005);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Project successfully deleted.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Project not available for delete.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Attendee

        [HttpPost]
        public ActionResult GetAttendeePartial() //partial for Copy Details
        {
            return PartialView("_AddLinesPartial");
        }

        [HttpPost]
        public ActionResult GetAttendeeData(JQueryDataTableParamModel param)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(param.Headerid);
                int MomNo = Convert.ToInt32(param.Title);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "HeaderId = " + intHeaderId + " and MOMNo=" + MomNo;

                string strSortOrder = string.Empty;

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (Department like '%" + param.sSearch
                        + "%' or Attendee like '%" + param.sSearch
                        + "%')";
                }
                bool isComplete = param.Flag;
                var lstResult = db.SP_MOM_GET_ATTENDEE_DETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                bool IsPrjExists = db.MOM005.Any(x => x.HeaderId == intHeaderId);
                bool IstattendeeExists = db.MOM002.Any(x => x.HeaderId == intHeaderId);
                bool isCompletebtn = false;
                if (IstattendeeExists == true && IsPrjExists == true)
                {
                    isCompletebtn = true;
                }
                var data = (from uc in lstResult
                            select new[]
                                {
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(Manager.GetDepartmentandDescription(uc.Department)),
                                Convert.ToString("<nobr>"+"<span> <ul style='margin-left: 34px !important;'><li>" + Manager.GetPsidName(uc.Attendee)+"</li></ul></span></nobr>"),
                                isComplete ? uc.Remarks : Helper.GenerateTextbox(uc.LineId, "Remarks", uc.Remarks, "UpdateAttendeesData(this, "+ uc.LineId +");"),
                                Helper.GenerateActionIcon(uc.LineId,"Delete","Delete Record","fa fa-trash-o", "DeleteRecord("+ uc.LineId +",'/MOM/Maintain/DeleteAttendee','Attendee','tblAttendee');","",isComplete?true: false)
                               }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    isCompletebtn = isCompletebtn,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveAttendeeDetails(int HeaderId, int MOMNo, string PS, string Remark)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var lstExistingAttendeeLines = db.MOM002.Where(o => o.HeaderId == HeaderId && o.MOMNo == MOMNo).Select(u => u.Attendee).ToList();
                string msg = string.Empty;
                var lstAttendee = PS.Split(',');
                List<MOM002> lstAddAttendee = new List<MOM002>();
                List<string> lstAlreadyExistAttendee = new List<string>();
                if (lstAttendee != null)
                {
                    foreach (var item in lstAttendee)
                    {
                        if (!lstExistingAttendeeLines.Contains(item.ToString()) && !lstAddAttendee.Any(i => i.Attendee == item.ToString()))
                        {
                            MOM002 objMOM002 = new MOM002();
                            var objMOM001 = db.MOM001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                            objMOM002.Attendee = item;
                            string dept = getDeptFromPS(item);
                            objMOM002.Department = dept.Split('-')[0].Trim();
                            objMOM002.MOMNo = MOMNo;
                            objMOM002.Project = objMOM001.Project;
                            objMOM002.Remarks = Remark;
                            objMOM002.HeaderId = HeaderId;
                            objMOM002.CreatedOn = DateTime.Now;
                            objMOM002.CreatedBy = objClsLoginInfo.UserName;
                            db.MOM002.Add(objMOM002);
                            lstAddAttendee.Add(objMOM002);
                        }
                        else
                        {
                            lstAlreadyExistAttendee.Add(item);
                        }
                    }

                    if (lstAddAttendee != null && lstAddAttendee.Count > 0)
                    {
                        db.MOM002.AddRange(lstAddAttendee);
                        msg += string.Format("{0} Attendee(s) is/are saved successfully,", lstAddAttendee.Count());
                    }
                    db.SaveChanges();
                }

                if (lstAlreadyExistAttendee.Count > 0)
                {
                    msg += string.Format("{0} Attendee(s) already exists", lstAlreadyExistAttendee.Count());
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = msg;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteAttendee(int lineid)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {

                MOM002 objMOM002 = db.MOM002.Where(x => x.LineId == lineid).FirstOrDefault();
                if (objMOM002 != null)
                {
                    db.MOM002.Remove(objMOM002);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Attendee successfully deleted.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Attendee not available for delete.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Action points

        [HttpPost]
        public ActionResult GetActionPoint() //partial for Copy Details
        {
            return PartialView("_AddLinesPartial");
        }

        [HttpPost]
        public ActionResult GetActionPointData(JQueryDataTableParamModel param, int Completed = 0)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(param.Headerid);
                int MomNo = Convert.ToInt32(param.Title);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                var IsDisplayOnly = !string.IsNullOrEmpty(Request["IsDisplayOnly"]) ? Convert.ToBoolean(Request["IsDisplayOnly"].ToString()) : false;
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "HeaderId <= " + param.Headerid;

                string strSortOrder = string.Empty;
                bool isActionPoint = param.isActionPoint;
                bool isProjectAPoint = false;
                if (!isActionPoint)
                {
                    if (!string.IsNullOrWhiteSpace(param.Project))
                    {
                        whereCondition += "  and Project='" + param.Project + "'";
                    }
                    string MOMactionheaderid = string.Empty;
                    List<MOM005> MOMProject = db.MOM005.Where(li => li.HeaderId == intHeaderId && li.MOMNo == MomNo).ToList();
                    List<string> lstProject = MOMProject.Select(x => x.Project).ToList();
                    if (lstProject.Count > 0)
                    {
                        var MOMgetheaderid = db.MOM005.Where(li => lstProject.Contains(li.Project)).Select(x => x.HeaderId).ToList();
                        var lstheaderids = db.MOM004.Where(li => MOMgetheaderid.Contains(li.HeaderId)).Select(x => x.HeaderId).Distinct().ToList();
                        if (lstheaderids.Count > 0)
                        {
                            //MOMactionheaderid = string.Join(",", lstheaderids);
                            //whereCondition += " and HeaderId in (" + MOMactionheaderid + ")";
                        }
                        else
                        {
                            whereCondition += "  and MOMNo = " + MomNo;
                        }
                    }
                    // else { if (MomNo > 0) { whereCondition += "  and MOMNo = " + MomNo; } }
                    whereCondition += " and Completed=" + Completed + " ";
                }
                else
                {
                    isProjectAPoint = true;
                    whereCondition += "  and Project='" + param.Project + "'  and Completed != 1 ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (Description like '%" + param.sSearch
                                    + "%' or ByWhom like '%" + param.sSearch
                                    + "%' or ByWhen like '%" + param.sSearch
                                    + "%' or Completed like '%" + param.sSearch
                        + "%')";
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstEmployee = (from a in db.COM003
                                   where a.t_actv == 1
                                   select
                                   new SelectItemList { id = a.t_psno, text = a.t_name }).ToList();

                var lstResult = db.SP_MOM_GET_ACTION_POINTS_DETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                bool isComplete = param.Flag;

                if (isComplete)
                {
                    isActionPoint = false;
                }
                List<SelectListItem> BoolenList = new List<SelectListItem>() { new SelectListItem { Text = "Select", Value = "" }, new SelectListItem { Text = "Yes", Value = "Yes" }, new SelectListItem { Text = "No", Value = "No" } };
                int newRecordId = 0;
                var newRecord = new[] {
                                    Helper.GenerateHidden(newRecordId, "HeaderId", param.Headerid),
                                    Helper.GenerateHidden(newRecordId, "LineId", ""),
                                    "",
                                    Helper.GenerateTextArea(newRecordId, "Description"),
                                    MultiSelectDropdown(new List<SelectItemList>(),newRecordId,"ByWhom",false,"","","ByWhom"),
                                    Helper.GenerateTextbox(newRecordId, "ByWhen"),
                                    Helper.GenerateDropdown(newRecordId, "Completed", new SelectList(BoolenList, "Value", "Text","No"), ""),
                                    "",
                                    Helper.GenerateTextbox(newRecordId, "Remarks"),
                                    Helper.GenerateGridButton(newRecordId, "Save", "Add Rocord", "fa fa-plus", "SaveNewRecord();" ),
                                    // GenerateAutoComplete(newRecordId,"txtDept","","",false,"","Department")+""+Helper.GenerateHidden(newRecordId,"Department"),
                                };


                var data = (from uc in lstResult
                            select new[] {
                                isActionPoint? Helper.GenerateHidden(uc.LineId, "HeaderId", uc.HeaderId.ToString()) : Convert.ToString(uc.HeaderId),
                                isActionPoint? Helper.GenerateHidden(uc.LineId, "LineId", Convert.ToString(uc.LineId)) : Convert.ToString(uc.LineId),
                                Convert.ToString(uc.MOMNo),
                                isActionPoint? IsDisplayOnly? Helper.GenerateTextArea(uc.LineId, "Description", uc.Description, "",true) :  Helper.GenerateTextArea(uc.LineId, "Description", uc.Description, "UpdateData(this, "+ uc.LineId +");") : Convert.ToString(uc.Description),
                                isActionPoint? IsDisplayOnly? MultiSelectDropdown(new List<SelectItemList>(),uc.LineId,uc.ByWhom,  true ,"","","ByWhom")+""+Helper.GenerateHidden(uc.LineId,"hdByWhom",uc.ByWhom) : MultiSelectDropdown(new List<SelectItemList>(),uc.LineId,uc.ByWhom,  false ,"UpdateData(this,"+ uc.LineId  +");","","ByWhom")+""+Helper.GenerateHidden(uc.LineId,"hdByWhom",uc.ByWhom): Getbywhom(uc.ByWhom),
                                isActionPoint? IsDisplayOnly? GenerateTextbox(uc.LineId, "ByWhen", Convert.ToDateTime(uc.ByWhen).ToString("yyyy-MM-dd",CultureInfo.InvariantCulture), "",true,"","","",false,"","",Convert.ToString(uc.ByWhenHistory)) : GenerateTextbox(uc.LineId, "ByWhen", Convert.ToDateTime(uc.ByWhen).ToString("yyyy-MM-dd",CultureInfo.InvariantCulture), "UpdateData(this, "+ uc.LineId +");",false,"","","",false,"","",Convert.ToString(uc.ByWhenHistory)): uc.ByWhen.HasValue ? ("<lable title='" + Convert.ToString(uc.ByWhenHistory) + "' >"+ uc.ByWhen.Value.ToString("dd/MM/yyy",CultureInfo.InvariantCulture) +" </label>"):"",
                                isActionPoint? IsDisplayOnly? Helper.GenerateDropdown(uc.LineId, "Completed", new SelectList(BoolenList, "Value", "Text",uc.Completed ?"Yes":"No"),"","",true,"width:100px !important") : Helper.GenerateDropdown(uc.LineId, "Completed", new SelectList(BoolenList, "Value", "Text",uc.Completed ?"Yes":"No"),"","UpdateData(this, "+ uc.LineId +");",false,"width:100px !important"): (uc.Completed == true? "Yes":"No"),
                                Convert.ToString(uc.Completed==true ?uc.EditedOn.HasValue ? uc.EditedOn.Value.ToString("dd/MM/yyyy") : "":""),
                                isActionPoint? IsDisplayOnly? Helper.GenerateTextbox(uc.LineId, "Remarks", Convert.ToString(uc.Remarks), "",true) : Helper.GenerateTextbox(uc.LineId, "Remarks", Convert.ToString(uc.Remarks), "UpdateData(this, "+ uc.LineId +");"): uc.Remarks,
                                IsDisplayOnly? "" : isProjectAPoint?  Helper.GenerateActionIcon(uc.LineId,"Delete","Delete Record","fa fa-trash-o", "DeleteRecord("+ uc.LineId +",'/MOM/Maintain/DeleteActionPoint','Action Point','tblInlineAction');","", isComplete?true:false): "",
                                //GenerateAutoComplete(uc.LineId, "ByWhen",Convert.ToString(uc.ByWhen),"UpdateData(this, "+ uc.LineId +");",false,"","" , false) +""+ Helper.GenerateHidden(uc.LineId,"Department",uc.Department),
                            }).ToList();

                if (isProjectAPoint) { data.Insert(0, newRecord); };

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public string Getbywhom(string psnos)
        {
            string[] arrayVal = { };
            List<string> lsnames = new List<string>();
            if (!string.IsNullOrWhiteSpace(psnos))
            {
                arrayVal = psnos.Split(',');
            }
            foreach (var item in arrayVal)
            {
                lsnames.Add(Manager.GetPsidName(item));
            }

            string names = String.Join(",", lsnames);
            return names;
        }
        public string MultiSelectDropdown(List<SelectItemList> list, int rowId, string selectedValue, bool Disabled = false, string OnChangeEvent = "", string OnBlurEvent = "", string columnName = "")
        {
            string multipleSelect = string.Empty;
            string[] arrayVal = { };
            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = "hd" + columnName + "" + rowId.ToString();

            try
            {
                if (!string.IsNullOrWhiteSpace(selectedValue))
                {
                    arrayVal = selectedValue.Split(',');
                }


                multipleSelect += "<select data-name='ddlmultiple' name='" + inputID + "' id='" + inputID + "' data-lineid='" + rowId + "' multiple='multiple' hdElement='" + hdElementId + "' style='width: 100 % ' colname='" + columnName + "' data-value='" + selectedValue + "' class='form-control multiselect-drodown' " + (Disabled ? "disabled " : "") + (OnChangeEvent != string.Empty ? "onchange=" + OnChangeEvent + "" : "") + (OnBlurEvent != string.Empty ? " onblur='" + OnBlurEvent + "'" : "") + " >";
                foreach (var item in list)
                {
                    multipleSelect += "<option value='" + item.id + "' " + ((arrayVal.Contains(item.id)) ? " selected" : "") + " >" + item.text + "</option>";
                }
                multipleSelect += "</select>";
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return multipleSelect;
        }


        [HttpPost]
        public ActionResult SaveNewRecord(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            int newRowIndex = 0;
            int userId = Convert.ToInt32(fc["UserId" + newRowIndex]);
            string tableName = string.Empty;
            int? momno = Convert.ToInt32(fc["MOMNo"]);
            string Project = fc["Project"];
            try
            {
                MOM004 objmom004 = new MOM004();
                var ser = (from m in db.MOM004
                           where m.MOMNo == momno
                           select m).Max(m => m.Serial);

                objmom004.MOMNo = momno;
                objmom004.Project = Project;
                objmom004.HeaderId = Convert.ToInt32(fc["HeaderId"]);
                objmom004.Serial = ser + 1;
                objmom004.Description = fc["Description" + newRowIndex];
                objmom004.ByWhom = fc["ByWhom" + newRowIndex];
                objmom004.Remarks = fc["Remarks" + newRowIndex];
                objmom004.ByWhen = Convert.ToDateTime(fc["ByWhen" + newRowIndex]);
                objmom004.Completed = fc["Completed" + newRowIndex].ToLower() == "yes" ? true : false;
                if (fc["Completed" + newRowIndex].ToLower() == "yes")
                {
                    objmom004.EditedOn = DateTime.Now;
                    objmom004.EditedBy = objClsLoginInfo.UserName;
                }
                else { objmom004.EditedOn = null; }

                objmom004.CreatedBy = objClsLoginInfo.UserName;
                objmom004.CreatedOn = DateTime.Now;
                db.MOM004.Add(objmom004);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteActionPoint(int lineid)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {

                MOM004 objMOM004 = db.MOM004.Where(x => x.LineId == lineid).FirstOrDefault();
                if (objMOM004 != null)
                {
                    db.MOM004.Remove(objMOM004);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Action Point deleted successfully.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Action Point not available for delete.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Create New MOM 

        #endregion
        [HttpPost]
        public ActionResult AddNewMOM(int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string currentUser = objClsLoginInfo.UserName;
                var currentLoc = objClsLoginInfo.Location;
                int newid = 0;
                MOM001 objExistingMOM001 = db.MOM001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                MOM001 objMOM001 = new MOM001();
                if (objExistingMOM001 != null)
                {
                    var MOMs = db.MOM001.Where(w => w.Project == objExistingMOM001.Project).Select(s => s.MOMNo).OrderByDescending(o => o);
                    objMOM001.MOMNo = MOMs.Count() == 0 ? 0 : MOMs.FirstOrDefault() + 1;

                    objMOM001.Venue = objExistingMOM001.Venue;
                    objMOM001.MeetingDate = null;
                    objMOM001.Project = objExistingMOM001.Project;
                    objMOM001.CreatedBy = objClsLoginInfo.UserName;
                    objMOM001.CreatedOn = DateTime.Now;
                    db.MOM001.Add(objMOM001);
                    db.SaveChanges();
                    newid = objMOM001.HeaderId;

                    List<MOM005> lstMOM005 = new List<MOM005>();
                    List<MOM005> lstExistingMOM005 = db.MOM005.Where(x => x.HeaderId == HeaderId).ToList();

                    string projCode = string.Join(",", lstExistingMOM005.Select(x => x.Project).ToList());
                    string remark = lstExistingMOM005.Select(x => x.Remarks).FirstOrDefault();
                    SaveProjects(objMOM001.HeaderId, objMOM001.MOMNo, projCode, remark);

                    List<MOM004> lstMOM004 = new List<MOM004>();
                    List<MOM004> lstOpenPoints = db.MOM004.Where(x => x.HeaderId == HeaderId && x.Completed == true).ToList();
                    foreach (var item in lstOpenPoints)
                    {
                        MOM004 objmom004 = new MOM004();
                        var ser = (from m in db.MOM004
                                   where m.MOMNo == objMOM001.MOMNo
                                   select m).Max(m => m.Serial);

                        objmom004.MOMNo = objMOM001.MOMNo;
                        objmom004.HeaderId = objMOM001.HeaderId;
                        objmom004.Serial = ser != null ? ser + 1 : 1;
                        objmom004.Description = item.Description;
                        objmom004.ByWhom = item.ByWhom;
                        objmom004.ByWhen = item.ByWhen;
                        objmom004.Completed = item.Completed;
                        objmom004.CreatedBy = objClsLoginInfo.UserName;
                        objmom004.CreatedOn = DateTime.Now;
                        lstMOM004.Add(objmom004);
                    }
                    if (lstMOM004.Count() > 0)
                    {
                        db.MOM004.AddRange(lstMOM004);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "MOM :" + objMOM001.MOMNo + "generated successfully";
                objResponseMsg.HeaderId = newid;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult CompleteMOM(int headerid)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                MOM001 objMOM001 = db.MOM001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                if (objMOM001 != null)
                {
                    string Attendees = string.Join(",", objMOM001.MOM002.Select(x => x.Attendee).ToList());
                    objMOM001.IsComplete = true;
                    objMOM001.CompletedBy = objClsLoginInfo.UserName;
                    objMOM001.CompletedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "MOM :'" + objMOM001.MOMNo + "' completed successfully.";
                    objResponseMsg.dataKey = true;
                    #region Send Notification
                    (new clsManager()).SendNotificationByUserPSNumber("", Attendees, "MOM :" + objMOM001.MOMNo + " has been Generated for Meeting Date :" + objMOM001.MeetingDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) + " .", clsImplementationEnum.NotificationType.Information.GetStringValue());
                    #endregion
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "MOM not available.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetActiveEmployeeValue(int lineId)
        {
            try
            {
                string strSuperior = (db.MOM004.Where(x => x.LineId == lineId).Select(x => x.ByWhom).FirstOrDefault());
                string[] arraySuperior = null;

                if (!string.IsNullOrWhiteSpace(strSuperior))
                {
                    arraySuperior = strSuperior.Split(',');
                    var items = db.COM003.Where(i => arraySuperior.Contains(i.t_psno)).Select(x => new { id = x.t_psno, text = x.t_name, }).ToList();
                    return Json(items, JsonRequestBehavior.AllowGet);
                }
                return Json(arraySuperior, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public clsHelper.ResponseMsgWithStatus insertmom005(int headerid, int MOMNo, string strprojectCode, string remark)
        {
            MOM005 objMOM005 = new MOM005();
            MOM001 objMOM001 = db.MOM001.Where(x => x.HeaderId == headerid).FirstOrDefault();
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            var lstExistingProjects = db.MOM005.Where(o => o.HeaderId == headerid && o.MOMNo == MOMNo).Select(u => u.Project.Trim()).ToList();
            string msg = string.Empty;


            List<string> lstAlreadyExistProjects = new List<string>();

            try
            {
                var lstprojects = strprojectCode.Split(',');
                List<MOM005> lstMOM005 = new List<MOM005>();

                foreach (var projectCode in lstprojects)
                {

                    var ppdd = "";
                    var sql = "";
                    var pdd = "";
                    var cc = "";
                    var bc = "";
                    var chDuration = "";
                    var chainRemainingDuration = "";

                    string conn = ConfigurationManager.ConnectionStrings["LNConcerto"].ToString();
                    //"Data Source=PHZPDSQLDB2K12;Initial Catalog=EDW;User ID=edw_read;Password=edw@#@!;"
                    using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(conn))
                    {
                        //sql = "select distinct * from ProjectDashboard where PROJECTNAME='"+projectCode+"'";
                        sql = "select PROJECTED_COMPLETION from Project where PROJECTNAME = '" + projectCode + "'";
                        using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(sql, con))
                        {
                            con.Open();
                            System.Data.SqlClient.SqlDataReader sdr = cmd.ExecuteReader();
                            if (sdr.HasRows)
                            {
                                while (sdr.Read())
                                {
                                    pdd = sdr["PROJECTED_COMPLETION"].ToString();
                                }
                            }
                        }
                        //sql = "select  PERCENTPENETRATION as BC,CHAINDURATION , CHAINREMAININGDURATION from S2M_BUFFERHISTORY where PROJECTNAME = '" + projectCode + "'";
                        sql = "select  a.projectname, a.chainduration/4800 CHAINDURATION, a.chainremainingduration/4800 CHAINREMAININGDURATION"
                                + " , (a.chainduration - a.chainremainingduration)/4800 compDur, ((a.chainduration - a.chainremainingduration)/a.chainduration)*100 as CC"
                                + " , a.percentpenetration as BC, a.duration/4800 duration"
                                + " , (a.percentpenetration/100)*(a.duration/4800) usedbuffer"
                                + " from   S2M_BUFFERHISTORY a with(nolock)"
                                + " where  a.task_type = 1"
                                + " and a.projectname = '" + projectCode + "'"
                                + " and a.history_timestamp = (select max(a0.history_timestamp) from s2m_bufferhistory a0 where a0.projectname = a.projectname) ";


                        using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(sql, con))
                        {
                            System.Data.SqlClient.SqlDataReader sdr = cmd.ExecuteReader();
                            if (sdr.HasRows)
                            {
                                while (sdr.Read())
                                {
                                    bc = sdr["BC"].ToString();
                                    cc = sdr["CC"].ToString();
                                    chDuration = sdr["CHAINDURATION"].ToString();
                                    chainRemainingDuration = sdr["CHAINREMAININGDURATION"].ToString();
                                }
                            }
                        }
                        con.Close();
                    }
                    //if (chDuration != "" && chainRemainingDuration != "")
                    //{
                    //    if ((float.Parse(chDuration) - float.Parse(chainRemainingDuration)) == 0.0)
                    //    {
                    //        cc = "0";
                    //    }
                    //    else
                    //    {

                    //        cc = (((float.Parse(chDuration) - float.Parse(chainRemainingDuration)) / float.Parse(chDuration)) * 100).ToString();
                    //    }
                    //}
                    //else
                    //{
                    //    cc = "";
                    //}

                    DateTime? cdd = GetCDDFromProject(projectCode);

                    var pdd1 = (from cd in db.MOM005
                                where cd.Project == projectCode
                                orderby cd.MOMNo descending
                                select cd.PDDCur
                                ).FirstOrDefault();

                    ppdd = pdd1.HasValue ? pdd1.Value.ToString("dd-MM-yyyy") : "";
                    if (pdd != "")
                    {
                        pdd = DateTime.Parse(pdd).ToString("dd-MM-yyyy");
                    }
                    var cddStr = cdd.Value == DateTime.MinValue || !cdd.HasValue ? "" : cdd.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);



                    if (!lstExistingProjects.Contains(projectCode.Trim()) && !lstMOM005.Any(i => i.Project.Trim() == projectCode.Trim()))
                    {
                        objMOM005 = new MOM005();
                        objMOM005.HeaderId = headerid;
                        objMOM005.Project = projectCode;
                        objMOM005.Remarks = remark;
                        objMOM005.MOMNo = MOMNo;
                        objMOM005.CDD = cdd;
                        if (!string.IsNullOrWhiteSpace(ppdd))
                        {
                            objMOM005.PDD = Convert.ToDateTime(ppdd);
                        }
                        objMOM005.BC = bc;
                        objMOM005.CC = cc;
                        objMOM005.CreatedBy = objClsLoginInfo.UserName;
                        objMOM005.CreatedOn = DateTime.Now;
                        if (!string.IsNullOrWhiteSpace(pdd))
                        {
                            objMOM005.PDDCur = Convert.ToDateTime(pdd);
                        }
                        lstMOM005.Add(objMOM005);
                    }
                    else
                    {
                        lstAlreadyExistProjects.Add(projectCode);
                    }
                    //lstdetails.Add(new Projects { projectCode = projectCode, projectDescription = Manager.GetProjectAndDescription(projectCode), PDD = pdd, CC = cc.ToString(), BC = bc.ToString(), CDD = cddStr, PPDD = ppdd });
                }
                if (lstMOM005.Count > 0)
                {
                    db.MOM005.AddRange(lstMOM005);
                    msg += string.Format("{0} Project(s) is/are saved successfully,", lstMOM005.Count());
                }
                if (lstAlreadyExistProjects.Count > 0)
                {
                    msg += string.Format("{0} Project(s) already exists", lstAlreadyExistProjects.Count());
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.dataValue = msg;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return objResponseMsg;
        }

        #endregion

        public class projectModel : ApproverModel
        {
            public bool key { get; set; }
            public string value { get; set; }
        }
    }
}