﻿using IEMQS.Areas.GTM.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;
using static IEMQSImplementation.clsImplementationEnum;

namespace IEMQS.Areas.GTM.Controllers
{
    /// <summary>
    /// Observation Id : 15353
    /// </summary>     
    public class MaintainGTMController : clsBase
    {
        [AllowAnonymous]
        [SessionExpireFilter]
        //[UserPermissions]
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetTables()
        {
            try
            {
                clsManager clsmanager = new clsManager();
                List<GTMTable> tableList = clsmanager.GetGTMTableList();

                var objTables = (from u in tableList
                                 select new { id = u.Name.Trim(), text = u.Description.Trim().Replace("\t", "") + " - " + u.Name.Trim() }).ToList();

                return Json(objTables, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetUsers()
        {
            try
            {
                var objTables = (from u in db.GTM001
                                 join com3 in db.COM003 on u.CreatedBy equals com3.t_psno
                                 orderby u.CreatedBy.Trim()
                                 select new
                                 { id = com3.t_psno.Trim(), text = com3.t_psno.Trim().Replace("\t", "") + " - " + com3.t_name.Trim() }).ToList().Distinct().ToList();

                return Json(objTables, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult CheckGTMLoginDetail(string password)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();

            objResponseMsg.Key = false;
            objResponseMsg.Value = "Invalid Password";

            try
            {
                if (System.Configuration.ConfigurationManager.AppSettings["GTMPassword"] != null)
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["GTMPassword"].ToString().Trim() == password.Trim())
                    {
                        objResponseMsg.Key = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult LoadGTMPartialView()
        {
            clsManager clsmanager = new clsManager();
            List<GTMTable> tableList = clsmanager.GetGTMTableList();

            ViewBag.TableList = (from u in tableList
                                 select new { Name = u.Name.Trim(), Description = u.Description.Trim().Replace("\t", "") + " - " + u.Name.Trim() }).ToList();

            ViewBag.CurrentDate = DateTime.Now.ToString("MM/dd/yyyy");

            return PartialView("_GTMPartial");
        }

        [HttpPost]
        public ActionResult LoadTableColumnDataPartial(string tableName, string pageNo, List<TableData> searchValueList, string oprt = "")
        {
            int totalCount = 0;
            clsTableHelper objclsTableHelper = new clsTableHelper();
            List<TableData> list = new List<TableData>();

            if (oprt == "add")
            {
                ViewBag.Oprt = "add";
                list = objclsTableHelper.GetTableColumnListForAddView(tableName);
            }
            else
            {
                list = objclsTableHelper.GetTableDataList(tableName, searchValueList, pageNo, ref totalCount);
            }

            ViewBag.TableName = tableName;
            ViewBag.PageNo = Convert.ToInt32(pageNo);
            ViewBag.TotalCount = Convert.ToInt32(totalCount);
            ViewBag.TableData = list;
            return PartialView("_TableColumnPartial");
        }

        [HttpPost]
        public ActionResult LoadSearchTableColumnDataPartial(string tableName)
        {
            List<TableData> list = new List<TableData>();

            clsTableHelper objclsTableHelper = new clsTableHelper();

            List<TableData> primaryKeyColumnList = objclsTableHelper.GetKeyColumnList(tableName, "PRIMARY KEY");
            List<TableData> uniqueKeyColumnList = objclsTableHelper.GetKeyColumnList(tableName, "UNIQUE");

            if (primaryKeyColumnList != null)
            {
                foreach (TableData item in primaryKeyColumnList)
                {
                    if (!list.Any(x => x.ColumnName == item.ColumnName))
                    {
                        list.Add(new TableData() { ColumnName = item.ColumnName, ColumnDescription = item.ColumnDescription });
                    }
                }
            }
            if (uniqueKeyColumnList != null)
            {
                foreach (TableData item in uniqueKeyColumnList)
                {
                    if (!list.Any(x => x.ColumnName == item.ColumnName))
                    {
                        list.Add(new TableData() { ColumnName = item.ColumnName, ColumnDescription = item.ColumnDescription });
                    }
                }
            }

            if (list.Count <= 0)
            {
                List<TableData> colsDescList = objclsTableHelper.GetColumnListWithDescription(tableName);
                if (colsDescList.Count > 0)
                    list.Add(new TableData() { ColumnName = colsDescList[0].ColumnName, ColumnDescription = colsDescList[0].ColumnDescription });
            }

            ViewBag.SearchTableData = list;

            return PartialView("_SearchTableColumnPartial");
        }

        #region CRUD

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CopyRecord(string tableName, string GTMNo, List<TableData> keyColumnList)
        {
            List<TableData> logList = new List<TableData>();
            List<TableData> updatedColumnList = new List<TableData>();

            clsTableHelper objclsTableHelper = new clsTableHelper();
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string excutedQuery = string.Empty;
            List<TableData> identityColumns = objclsTableHelper.GetIdentityColumnList(tableName);

            objResponseMsg.Key = false;
            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();

            try
            {
                if (keyColumnList != null && keyColumnList.Count > 0)
                {
                    #region Dynamic Query Param

                    int totalCount = 0;
                    List<TableData> list = objclsTableHelper.GetTableDataList(tableName, keyColumnList, "1", ref totalCount);
                    if (list != null)
                    {
                        foreach (var item in list)
                        {
                            bool IsAdd = true;
                            if (identityColumns != null)
                            {
                                if (identityColumns.Any(x => x.ColumnName.Trim() == item.ColumnName.Trim()))
                                {
                                    IsAdd = false;
                                }
                            }

                            if (IsAdd)
                            {
                                updatedColumnList.Add(item);
                                TableData objLog = new TableData();
                                objLog.ColumnName = item.ColumnName != null ? item.ColumnName : "";
                                objLog.ColumnDescription = item.ColumnDescription != null ? item.ColumnDescription : "";
                                objLog.ColumnValue = item.ColumnValue != null ? item.ColumnValue : "";
                                if (item.ColumnType.Trim().ToLower() == "datetime" || item.ColumnType.Trim().ToLower() == "date")
                                {
                                    if (!string.IsNullOrWhiteSpace(objLog.ColumnValue))
                                    {
                                        objLog.ColumnValue = Convert.ToDateTime(objLog.ColumnValue).ToString("dd/MM/yyyy HH:mm:ss");
                                    }
                                }
                                objLog.OldValue = string.Empty;
                                logList.Add(objLog);
                            }
                        }
                    }

                    #endregion

                    #region Copy in DB

                    string errMsg = string.Empty;
                    objclsTableHelper.CopyRecord(tableName, updatedColumnList, keyColumnList, ref excutedQuery, ref errMsg);
                    if (errMsg != "")
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = errMsg;
                    }
                    else
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.CopyRecord.ToString();

                        #region Maintain Logs

                        if (logList.Count > 0)
                        {
                            GTM001 objGTM001 = new GTM001();
                            objGTM001.GTMNo = GTMNo;
                            objGTM001.TableName = tableName;
                            objGTM001.ActionType = ActionType.Copy.ToString();
                            objGTM001.ExecutedQuery = excutedQuery;
                            objGTM001.ColumnsValue = GetXMLFromList(logList);
                            objGTM001.Status = GTMStatus.Committed.ToString();
                            objGTM001.CreatedBy = objClsLoginInfo.UserName;
                            objGTM001.CreatedOn = DateTime.Now;
                            db.GTM001.Add(objGTM001);
                            db.SaveChanges();
                        }

                        #endregion
                    }

                    #endregion
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Primary key does not exist";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddRecord(string tableName, string GTMNo, List<TableData> columnList)
        {
            List<TableData> logList = new List<TableData>();
            List<TableData> updatedColumnList = new List<TableData>();

            clsTableHelper objclsTableHelper = new clsTableHelper();
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string excutedQuery = string.Empty;
            List<TableData> identityColumns = objclsTableHelper.GetIdentityColumnList(tableName);

            objResponseMsg.Key = false;
            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();

            try
            {
                if (columnList != null)
                {
                    #region Dynamic Query Param

                    if (columnList.Count > 0)
                    {
                        foreach (var item in columnList)
                        {
                            bool IsAdd = true;
                            if (identityColumns != null)
                            {
                                if (identityColumns.Any(x => x.ColumnName.Trim() == item.ColumnName.Trim()))
                                {
                                    IsAdd = false;
                                }
                            }

                            if (IsAdd)
                            {
                                updatedColumnList.Add(item);

                                TableData objLog = new TableData();
                                objLog.ColumnName = item.ColumnName != null ? item.ColumnName : "";
                                objLog.ColumnDescription = item.ColumnDescription != null ? item.ColumnDescription : "";
                                objLog.OldValue = string.Empty;
                                objLog.ColumnValue = item.ColumnValue != null ? item.ColumnValue : "";
                                if (item.ColumnType.Trim().ToLower() == "datetime" || item.ColumnType.Trim().ToLower() == "date")
                                {
                                    if (!string.IsNullOrWhiteSpace(objLog.ColumnValue))
                                    {
                                        objLog.ColumnValue = Convert.ToDateTime(objLog.ColumnValue).ToString("dd/MM/yyyy HH:mm:ss");
                                    }
                                }

                                logList.Add(objLog);
                            }
                        }
                    }

                    #endregion

                    #region Add in DB

                    if (updatedColumnList.Count > 0)
                    {
                        string errMsg = string.Empty;
                        objclsTableHelper.AddRecord(tableName, updatedColumnList, ref excutedQuery, ref errMsg);
                        if (errMsg != "")
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = errMsg;
                        }
                        else
                        {
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();

                            #region Maintain Logs

                            if (logList.Count > 0)
                            {
                                GTM001 objGTM001 = new GTM001();
                                objGTM001.GTMNo = GTMNo;
                                objGTM001.TableName = tableName;
                                objGTM001.ActionType = ActionType.Insert.ToString();
                                objGTM001.ExecutedQuery = excutedQuery;
                                objGTM001.ColumnsValue = GetXMLFromList(logList);
                                objGTM001.Status = GTMStatus.Committed.ToString();
                                objGTM001.CreatedBy = objClsLoginInfo.UserName;
                                objGTM001.CreatedOn = DateTime.Now;
                                db.GTM001.Add(objGTM001);
                                db.SaveChanges();
                            }

                            #endregion
                        }
                    }

                    #endregion
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Column does not exist";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UpdateRecord(string tableName, string GTMNo, List<TableData> columnList)
        {
            List<TableData> logList = new List<TableData>();
            List<TableData> updatedColumnList = new List<TableData>();

            clsTableHelper objclsTableHelper = new clsTableHelper();
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string excutedQuery = string.Empty;

            objResponseMsg.Key = false;
            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();

            try
            {
                if (columnList != null)
                {
                    var keyColumnList = (from u in columnList where u.IsPrimaryKey == true select u).ToList();
                    if (keyColumnList != null && keyColumnList.Count > 0)
                    {
                        #region Dynamic Query Param

                        DataTable dtOldData = objclsTableHelper.GetRecord(tableName, keyColumnList);
                        if (dtOldData != null)
                        {
                            var collist = (from u in columnList where u.IsPrimaryKey == false select u).ToList();
                            if (collist != null && collist.Count > 0)
                            {
                                foreach (var column in collist)
                                {
                                    string columnName = column.ColumnName != null ? column.ColumnName.Trim() : "";
                                    string columnDesc = column.ColumnDescription != null ? column.ColumnDescription.Trim() : "";
                                    string columnValue = column.ColumnValue != null ? column.ColumnValue.Trim() : "";
                                    string columnType = column.ColumnType != null ? column.ColumnType.Trim().ToLower() : "";

                                    var oldValue = (from DataRow row in dtOldData.Rows select row[columnName]).FirstOrDefault();
                                    if (oldValue != null)
                                    {
                                        if (oldValue.ToString().Trim() != columnValue.Trim())
                                        {
                                            updatedColumnList.Add(column);

                                            TableData objLog = new TableData();
                                            objLog.ColumnName = columnName;
                                            objLog.ColumnDescription = columnDesc;
                                            objLog.ColumnValue = columnValue;
                                            objLog.OldValue = oldValue.ToString();
                                            if (columnType == "datetime" || columnType == "date")
                                            {
                                                if (!string.IsNullOrWhiteSpace(objLog.ColumnValue))
                                                {
                                                    objLog.ColumnValue = Convert.ToDateTime(objLog.ColumnValue).ToString("dd/MM/yyyy HH:mm:ss");
                                                }
                                                if (!string.IsNullOrWhiteSpace(objLog.OldValue))
                                                {
                                                    objLog.OldValue = Convert.ToDateTime(objLog.OldValue).ToString("dd/MM/yyyy HH:mm:ss");
                                                }
                                            }
                                            logList.Add(objLog);
                                        }
                                    }
                                }
                            }
                        }

                        #endregion

                        #region Update in DB

                        if (updatedColumnList.Count > 0 && keyColumnList.Count > 0)
                        {
                            string errMsg = string.Empty;
                            objclsTableHelper.UpdateRecord(tableName, updatedColumnList, keyColumnList, ref excutedQuery, ref errMsg);
                            if (errMsg != "")
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = errMsg;
                            }
                            else
                            {
                                objResponseMsg.Key = true;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();

                                #region Maintain Logs

                                if (logList.Count > 0)
                                {
                                    GTM001 objGTM001 = new GTM001();
                                    objGTM001.GTMNo = GTMNo;
                                    objGTM001.TableName = tableName;
                                    objGTM001.ActionType = ActionType.Update.ToString();
                                    objGTM001.ExecutedQuery = excutedQuery;
                                    objGTM001.ColumnsValue = GetXMLFromList(logList);
                                    objGTM001.Status = GTMStatus.Committed.ToString();
                                    objGTM001.CreatedBy = objClsLoginInfo.UserName;
                                    objGTM001.CreatedOn = DateTime.Now;
                                    db.GTM001.Add(objGTM001);
                                    db.SaveChanges();
                                }

                                #endregion
                            }
                        }
                        else
                        {
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                        }

                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Primary key does not exist";
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Column does not exist";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult DeleteRecord(string tableName, string GTMNo, List<TableData> keyColumnList)
        {
            List<TableData> logList = new List<TableData>();

            clsTableHelper objclsTableHelper = new clsTableHelper();
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();

            string excutedQuery = string.Empty;

            objResponseMsg.Key = false;
            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();

            try
            {
                if (keyColumnList != null && keyColumnList.Count > 0)
                {
                    #region Dynamic Query Param

                    int totalCount = 0;
                    List<TableData> list = objclsTableHelper.GetTableDataList(tableName, keyColumnList, "1", ref totalCount);
                    if (list != null)
                    {
                        foreach (var item in list)
                        {
                            TableData objLog = new TableData();
                            objLog.ColumnName = item.ColumnName != null ? item.ColumnName : "";
                            objLog.ColumnDescription = item.ColumnDescription != null ? item.ColumnDescription : "";
                            objLog.OldValue = item.ColumnValue != null ? item.ColumnValue : "";
                            if (item.ColumnType.Trim().ToLower() == "datetime" || item.ColumnType.Trim().ToLower() == "date")
                            {
                                if (!string.IsNullOrWhiteSpace(objLog.OldValue))
                                {
                                    objLog.OldValue = Convert.ToDateTime(objLog.OldValue).ToString("dd/MM/yyyy HH:mm:ss");
                                }
                            }
                            objLog.ColumnValue = string.Empty;
                            logList.Add(objLog);
                        }
                    }

                    #endregion

                    #region Delete From DB

                    string errMsg = string.Empty;
                    objclsTableHelper.DeleteRecord(tableName, keyColumnList, ref excutedQuery, ref errMsg);
                    if (errMsg != "")
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = errMsg;
                    }
                    else
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();

                        #region Maintain Logs

                        if (logList.Count > 0)
                        {
                            GTM001 objGTM001 = new GTM001();
                            objGTM001.GTMNo = GTMNo;
                            objGTM001.TableName = tableName;
                            objGTM001.ActionType = ActionType.Delete.ToString();
                            objGTM001.ExecutedQuery = excutedQuery;
                            objGTM001.ColumnsValue = GetXMLFromList(logList);
                            objGTM001.Status = GTMStatus.Committed.ToString();
                            objGTM001.CreatedBy = objClsLoginInfo.UserName;
                            objGTM001.CreatedOn = DateTime.Now;
                            db.GTM001.Add(objGTM001);
                            db.SaveChanges();
                        }

                        #endregion
                    }

                    #endregion
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Primary key does not exist";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        public string GetXMLFromList(List<TableData> logList)
        {
            var xmlfromLINQ = new XElement("columnlist",
            from c in logList
            select new XElement("column",
                   new XElement("columnname", c.ColumnName),
                   new XElement("columndesc", c.ColumnDescription),
                   new XElement("oldvalue", c.OldValue),
                   new XElement("newvalue", c.ColumnValue)));
            return xmlfromLINQ.ToString();
        }
    }
}