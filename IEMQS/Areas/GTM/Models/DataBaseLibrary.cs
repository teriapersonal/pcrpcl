using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace IEMQS.Areas.GTM.DataBaseLibrary
{
    public class DatabaseHelper : IDisposable
    {
        private SqlConnection objConnection;
        private SqlCommand objCommand;

        public DatabaseHelper()
        {
            objConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["IEMQS"].ConnectionString);
            objCommand = new SqlCommand();
            objCommand.CommandTimeout = 120;
            objCommand.Connection = objConnection;
        }

        public SqlParameter AddParameter(string name, object value)
        {
            SqlParameter p = objCommand.CreateParameter();
            p.ParameterName = name;
            p.Value = value;
            return objCommand.Parameters.Add(p);
        }

        public void ClearParameters()
        {
            objCommand.Parameters.Clear();
        }

        public SqlCommand Command
        {
            get { return objCommand; }
        }

        public int ExecuteNonQuery(string query, ref string errormsg)
        {
            return ExecuteNonQuery(query, CommandType.Text, ref errormsg);
        }

        public int ExecuteNonQuery(string query, CommandType commandtype, ref string errormsg)
        {
            errormsg = "";
            objCommand.CommandText = query;
            objCommand.CommandType = commandtype;
            int i = -1;
            try
            {
                if (objConnection.State == System.Data.ConnectionState.Closed)
                {
                    objConnection.Open();
                }
                i = objCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                errormsg = ex.Message.ToString();
                SqlException sx = (SqlException)ex;
                if (sx.Number == 547)       // Foreign Key Error
                    i = sx.Number;
                else
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex); //  HandleExceptions(ex, query);
            }
            finally
            {
                //objCommand.Parameters.Clear();
                if (objCommand.Transaction == null)
                {
                    objConnection.Close();
                }
            }
            return i;
        }

        public DataSet ExecuteDataSet(string query)
        {
            return ExecuteDataSet(query, CommandType.Text);
        }
        public DataSet ExecuteDataSet(string query, CommandType commandtype)
        {
            SqlDataAdapter adapter = new SqlDataAdapter();
            objCommand.CommandText = query;
            objCommand.CommandType = commandtype;
            adapter.SelectCommand = objCommand;
            DataSet ds = new DataSet();
            try
            {
                adapter.Fill(ds);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                // HandleExceptions(ex, query);
            }
            finally
            {
                //objCommand.Parameters.Clear();
                if (objCommand.Transaction == null)
                {
                    objConnection.Close();
                }
            }
            return ds;
        }

        public void Dispose()
        {
            objCommand.Parameters.Clear();
            objConnection.Close();
            objConnection.Dispose();
            objCommand.Dispose();
        }
        public enum ParamType
        {
            Input, Output, InputOutput
        }
    }
}

