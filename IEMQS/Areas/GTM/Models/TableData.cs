﻿using IEMQS.Areas.GTM.DataBaseLibrary;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IEMQS.Areas.GTM.Models
{
    public class TableData
    {
        public string ColumnName { get; set; }

        public string ColumnDescription { get; set; }

        public string ColumnValue { get; set; }

        public string OldValue { get; set; }

        public string ColumnType { get; set; }

        public bool IsPrimaryKey { get; set; }

        public bool IsUniqueKey { get; set; }
    }

    public class clsTableHelper
    {
        public List<TableData> GetTableDataList(string tableName, List<TableData> searchColumnList, string pageNo, ref int totalCount)
        {
            totalCount = 0;
            string query = string.Empty;
            List<TableData> objTableDataList = new List<TableData>();
            if (!string.IsNullOrEmpty(tableName))
            {
                DatabaseHelper dbHelper = new DatabaseHelper();
                try
                {
                    List<TableData> primaryKeyColumnList = GetKeyColumnList(tableName, "PRIMARY KEY");
                    List<TableData> uniqueKeyColumnList = GetKeyColumnList(tableName, "UNIQUE");
                    List<TableData> colsDescList = GetColumnListWithDescription(tableName);

                    string primaryKeys = "";
                    if (primaryKeyColumnList != null && primaryKeyColumnList.Count > 0)
                    {
                        primaryKeys = String.Join(",", primaryKeyColumnList.Select(i => i.ColumnName).ToList());
                    }
                    else
                    {
                        primaryKeys = colsDescList[0].ColumnName;
                    }

                    query = "select * from (select count(1) over () AS totalcount,ROW_NUMBER() OVER(ORDER BY " + primaryKeys + " ASC) as rowno, * from " + tableName.Trim() + "";

                    if (searchColumnList != null && searchColumnList.Count > 0)
                    {
                        query += " where 1=1";
                        foreach (TableData item in searchColumnList)
                        {
                            //query += " and " + item.ColumnName + " like '%' + @" + item.ColumnName + " + '%'";
                            query += " and " + item.ColumnName + " =@" + item.ColumnName;

                            if (!dbHelper.Command.Parameters.Contains("@" + item.ColumnName))
                                dbHelper.AddParameter("@" + item.ColumnName, item.ColumnValue);
                        }
                    }

                    query += ") as t where t.rowno = @pageNo";

                    dbHelper.AddParameter("@pageNo", pageNo.Trim());

                    DataSet ds = dbHelper.ExecuteDataSet(query);
                    if (ds != null && ds.Tables[0].Rows.Count > 0)
                    {
                        totalCount = Convert.ToInt32(ds.Tables[0].Rows[0]["totalcount"]);
                        foreach (DataColumn cols in ds.Tables[0].Columns)
                        {
                            if (cols.ColumnName != "rowno" && cols.ColumnName != "totalcount")
                            {
                                TableData temp = new TableData();
                                temp.ColumnName = cols.ColumnName;
                                temp.ColumnValue = ds.Tables[0].Rows[0][cols].ToString();
                                if (colsDescList != null)
                                {
                                    var descItem = (from x in colsDescList
                                                    where x.ColumnName.Trim() == cols.ColumnName.Trim()
                                                    select new { x.ColumnDescription, x.ColumnType }).FirstOrDefault();
                                    if (descItem != null)
                                    {
                                        temp.ColumnDescription = descItem.ColumnDescription;
                                        temp.ColumnType = descItem.ColumnType;
                                    }
                                }
                                temp.IsPrimaryKey = false;
                                if (primaryKeyColumnList != null)
                                {
                                    if (primaryKeyColumnList.Any(x => x.ColumnName.Trim() == cols.ColumnName.Trim()))
                                    {
                                        temp.IsPrimaryKey = true;
                                    }
                                }

                                temp.IsUniqueKey = false;
                                if (uniqueKeyColumnList != null)
                                {
                                    if (uniqueKeyColumnList.Any(x => x.ColumnName.Trim() == cols.ColumnName.Trim()))
                                    {
                                        temp.IsUniqueKey = true;
                                    }
                                }

                                objTableDataList.Add(temp);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                }
                finally
                {
                    dbHelper.Dispose();
                }
            }
            return objTableDataList;
        }

        public List<TableData> GetTableColumnListForAddView(string tableName)
        {
            List<TableData> objTableDataList = new List<TableData>();
            if (!string.IsNullOrEmpty(tableName))
            {
                DatabaseHelper dbHelper = new DatabaseHelper();
                try
                {
                    List<TableData> primaryKeyColumnList = GetKeyColumnList(tableName, "PRIMARY KEY");
                    List<TableData> colsDescList = GetColumnListWithDescription(tableName);

                    string query = "select top 1 * from " + tableName.Trim() + "";

                    DataSet ds = dbHelper.ExecuteDataSet(query);
                    if (ds != null && ds.Tables[0] != null)
                    {
                        foreach (DataColumn cols in ds.Tables[0].Columns)
                        {
                            TableData temp = new TableData();
                            temp.ColumnName = cols.ColumnName;
                            temp.ColumnValue = "";
                            if (colsDescList != null)
                            {
                                var descItem = (from x in colsDescList
                                                where x.ColumnName.Trim() == cols.ColumnName.Trim()
                                                select new { x.ColumnDescription, x.ColumnType }).FirstOrDefault();
                                if (descItem != null)
                                {
                                    temp.ColumnDescription = descItem.ColumnDescription;
                                    temp.ColumnType = descItem.ColumnType;
                                }
                            }
                            temp.IsPrimaryKey = false;
                            if (primaryKeyColumnList != null)
                            {
                                if (primaryKeyColumnList.Any(x => x.ColumnName.Trim() == cols.ColumnName.Trim()))
                                {
                                    temp.IsPrimaryKey = true;
                                }
                            }
                            objTableDataList.Add(temp);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                }
                finally
                {
                    dbHelper.Dispose();
                }
            }
            return objTableDataList;
        }

        public List<TableData> GetKeyColumnList(string tableName, string constrainType)
        {
            List<TableData> objTableDataList = new List<TableData>();
            if (!string.IsNullOrEmpty(tableName))
            {
                List<TableData> colsDescList = GetColumnListWithDescription(tableName);

                DatabaseHelper dbHelper = new DatabaseHelper();
                try
                {
                    string query = "SELECT KU.table_name as TABLE_NAME,column_name as KEY_COLUMN FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS AS TC" +
                            " INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS KU ON TC.CONSTRAINT_TYPE = '" + constrainType +
                            "' AND TC.CONSTRAINT_NAME = KU.CONSTRAINT_NAME AND KU.table_name = '" + tableName + "'";

                    DataSet ds = dbHelper.ExecuteDataSet(query);
                    if (ds != null && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            TableData temp = new TableData();
                            temp.ColumnName = Convert.ToString(row["KEY_COLUMN"]);

                            if (colsDescList != null)
                            {
                                var descItem = (from x in colsDescList
                                                where x.ColumnName.Trim() == temp.ColumnName.Trim()
                                                select new { x.ColumnDescription }).FirstOrDefault();
                                if (descItem != null)
                                {
                                    temp.ColumnDescription = descItem.ColumnDescription;                                    
                                }
                            }

                            objTableDataList.Add(temp);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                }
                finally
                {
                    dbHelper.Dispose();
                }
            }
            return objTableDataList;
        }

        public List<TableData> GetIdentityColumnList(string tableName)
        {
            List<TableData> objTableDataList = new List<TableData>();
            if (!string.IsNullOrEmpty(tableName))
            {
                DatabaseHelper dbHelper = new DatabaseHelper();
                try
                {
                    string query = "select TABLE_NAME,COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA = 'dbo'";
                    query += " and COLUMNPROPERTY(object_id(TABLE_NAME), COLUMN_NAME, 'IsIdentity') = 1 and TABLE_NAME = '" + tableName + "'";

                    DataSet ds = dbHelper.ExecuteDataSet(query);
                    if (ds != null && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            TableData temp = new TableData();
                            temp.ColumnName = Convert.ToString(row["COLUMN_NAME"]);
                            objTableDataList.Add(temp);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                }
                finally
                {
                    dbHelper.Dispose();
                }
            }
            return objTableDataList;
        }

        public List<TableData> GetColumnListWithDescription(string tableName)
        {
            List<TableData> objTableDataList = new List<TableData>();
            if (!string.IsNullOrEmpty(tableName))
            {
                DatabaseHelper dbHelper = new DatabaseHelper();
                try
                {
                    string query = "";
                    query = "select st.name [TableName],sc.name [ColumnName],sep.value [ColumnDescription],(select a.DATA_TYPE from information_schema.COLUMNS a where a.TABLE_NAME = '" + tableName + "' and a.COLUMN_NAME = sc.name) as ColumnType";
                    query += " from sys.tables st inner join sys.columns sc on st.object_id = sc.object_id";
                    query += " left join sys.extended_properties sep on st.object_id = sep.major_id and sc.column_id = sep.minor_id and sep.name = 'MS_Description'";
                    query += " where st.name = '" + tableName + "'";

                    DataSet ds = dbHelper.ExecuteDataSet(query);
                    if (ds != null && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            TableData temp = new TableData();
                            temp.ColumnName = Convert.ToString(row["ColumnName"]);
                            temp.ColumnType = Convert.ToString(row["ColumnType"]);
                            temp.ColumnDescription = Convert.ToString(row["ColumnDescription"]);                           
                            objTableDataList.Add(temp);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                }
                finally
                {
                    dbHelper.Dispose();
                }
            }
            return objTableDataList;
        }

        public DataTable GetRecord(string tableName, List<TableData> whereColumns)
        {
            DataTable dt = new DataTable();
            DatabaseHelper dbHelper = new DatabaseHelper();
            try
            {
                string query = "select * from " + tableName;
                int i = 1;
                foreach (var item in whereColumns)
                {
                    if (i == 1)
                        query += " where " + item.ColumnName + "=@" + item.ColumnName;
                    else
                        query += " and " + item.ColumnName + "=@" + item.ColumnName;

                    if (!dbHelper.Command.Parameters.Contains("@" + item.ColumnName))
                        dbHelper.AddParameter("@" + item.ColumnName, item.ColumnValue);

                    i++;
                }

                DataSet ds = dbHelper.ExecuteDataSet(query);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    dt = ds.Tables[0];
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            finally
            {
                dbHelper.Dispose();
            }
            return dt;
        }

        #region CRUD

        public string CopyRecord(string tableName, List<TableData> columns, List<TableData> whereColumns, ref string query, ref string errorMsg)
        {
            string res = string.Empty;
            DatabaseHelper dbHelper = new DatabaseHelper();
            try
            {
                errorMsg = "";
                query = "";
                if (columns != null)
                {
                    string columnNames = string.Empty;
                    string columnValues = string.Empty;

                    int i = 1;
                    foreach (var item in columns)
                    {
                        if (i == 1)
                        {
                            columnNames += item.ColumnName;
                        }
                        else
                        {
                            columnNames += "," + item.ColumnName;
                        }

                        i++;
                    }

                    query = "insert into " + tableName + " (" + columnNames + ") select " + columnNames + " from " + tableName + " where ";

                    if (whereColumns != null)
                    {
                        i = 1;
                        foreach (var item in whereColumns)
                        {
                            if (i == 1)
                                query += item.ColumnName + "=@" + item.ColumnName;
                            else
                                query += " and " + item.ColumnName + "=@" + item.ColumnName;

                            if (!dbHelper.Command.Parameters.Contains("@" + item.ColumnName))
                                dbHelper.AddParameter("@" + item.ColumnName, item.ColumnValue != null ? item.ColumnValue : "");

                            i++;
                        }
                    }
                }

                res = dbHelper.ExecuteNonQuery(query, ref errorMsg).ToString();
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message.ToString();
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            finally
            {
                dbHelper.Dispose();
            }
            return res;
        }

        public string AddRecord(string tableName, List<TableData> columns, ref string query, ref string errorMsg)
        {
            string res = string.Empty;
            DatabaseHelper dbHelper = new DatabaseHelper();
            try
            {
                errorMsg = "";
                query = "";
                if (columns != null)
                {
                    string columnNames = string.Empty;
                    string columnValues = string.Empty;

                    int i = 1;
                    foreach (var item in columns)
                    {
                        string columnValue = item.ColumnValue != null ? item.ColumnValue : "";
                        if (i == 1)
                        {
                            columnNames += item.ColumnName;
                            columnValues += "@" + item.ColumnName;
                        }
                        else
                        {
                            columnNames += "," + item.ColumnName;
                            columnValues += "," + "@" + item.ColumnName;
                        }

                        if (!dbHelper.Command.Parameters.Contains("@" + item.ColumnName))
                        {
                            if (item.ColumnType.Trim().ToLower() == "datetime" || item.ColumnType.Trim().ToLower() == "date")
                            {
                                if (columnValue != "")
                                    dbHelper.AddParameter("@" + item.ColumnName, columnValue);
                                else
                                    dbHelper.AddParameter("@" + item.ColumnName, DBNull.Value);
                            }
                            else
                                dbHelper.AddParameter("@" + item.ColumnName, columnValue);
                        }

                        i++;
                    }
                    query = "insert into " + tableName + " (" + columnNames + ") values (" + columnValues + ")";
                }

                res = dbHelper.ExecuteNonQuery(query, ref errorMsg).ToString();
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message.ToString();
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            finally
            {
                dbHelper.Dispose();
            }
            return res;
        }

        public string UpdateRecord(string tableName, List<TableData> updatedColumns, List<TableData> whereColumns, ref string query, ref string errorMsg)
        {
            string res = string.Empty;
            DatabaseHelper dbHelper = new DatabaseHelper();
            try
            {
                errorMsg = "";
                query = "";
                if (updatedColumns != null)
                {
                    query = "update " + tableName + " set ";

                    int i = 1;
                    foreach (var item in updatedColumns)
                    {
                        string columnValue = item.ColumnValue != null ? item.ColumnValue : "";

                        if (i == 1)
                            query += item.ColumnName + "=@" + item.ColumnName;
                        else
                            query += "," + item.ColumnName + "=@" + item.ColumnName;

                        if (!dbHelper.Command.Parameters.Contains("@" + item.ColumnName))
                        {
                            if (item.ColumnType.Trim().ToLower() == "datetime" || item.ColumnType.Trim().ToLower() == "date")
                            {
                                if (columnValue != "")
                                    dbHelper.AddParameter("@" + item.ColumnName, columnValue);
                                else
                                    dbHelper.AddParameter("@" + item.ColumnName, DBNull.Value);
                            }
                            else
                                dbHelper.AddParameter("@" + item.ColumnName, columnValue);
                        }

                        i++;
                    }

                }

                query += " where ";

                if (whereColumns != null)
                {
                    int i = 1;
                    foreach (var item in whereColumns)
                    {
                        if (i == 1)
                            query += item.ColumnName + "=@" + item.ColumnName;
                        else
                            query += " and " + item.ColumnName + "=@" + item.ColumnName;

                        if (!dbHelper.Command.Parameters.Contains("@" + item.ColumnName))
                            dbHelper.AddParameter("@" + item.ColumnName, item.ColumnValue != null ? item.ColumnValue : "");

                        i++;
                    }
                }

                res = dbHelper.ExecuteNonQuery(query, ref errorMsg).ToString();
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message.ToString();
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            finally
            {
                dbHelper.Dispose();
            }
            return res;
        }

        public string DeleteRecord(string tableName, List<TableData> whereColumns, ref string query, ref string errorMsg)
        {
            string res = string.Empty;
            DatabaseHelper dbHelper = new DatabaseHelper();
            try
            {
                errorMsg = "";
                query = "";
                query = "delete from " + tableName + " where ";

                if (whereColumns != null)
                {
                    int i = 1;
                    foreach (var item in whereColumns)
                    {
                        if (i == 1)
                            query += item.ColumnName + "=@" + item.ColumnName;
                        else
                            query += " and " + item.ColumnName + "=@" + item.ColumnName;

                        if (!dbHelper.Command.Parameters.Contains("@" + item.ColumnName))
                            dbHelper.AddParameter("@" + item.ColumnName, item.ColumnValue != null ? item.ColumnValue : "");

                        i++;
                    }
                }
                res = dbHelper.ExecuteNonQuery(query, ref errorMsg).ToString();
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message.ToString();
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            finally
            {
                dbHelper.Dispose();
            }
            return res;
        }

        #endregion
    }
}