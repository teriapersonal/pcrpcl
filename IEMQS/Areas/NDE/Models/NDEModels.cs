﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IEMQSImplementation;
using IEMQS.Models;
using System.Collections;

namespace IEMQS.Areas.NDE.Models
{
    public class NDEModels
    {
        public IEMQSEntitiesContext db = new IEMQSEntitiesContext();
        List<string> lstGlobalCategoryUsed  = new List<string>();

        public NDEModels() {
            HttpContext.Current.Session[clsImplementationMessage.Session.GlobalCategoryUsed] = null;
        }
        
        public List<CategoryData> GetCategoryData(int categoryId)
        {
            List<CategoryData> lstGLB002 = new List<CategoryData>();


            if (categoryId > 0)
            {
                lstGLB002 = (from a in db.GLB002
                             where a.Category == categoryId
                             select new CategoryData { Id = a.Id, CategoryDescription = a.Code + " - " + a.Description }).ToList();//db.GLB002.Where(i => i.Category == categoryId).ToList();
            }

            return lstGLB002;
        }

        public List<CategoryData> GetQIDForSeam(string qualityProject)
        {
            List<CategoryData> lstGLB002 = new List<CategoryData>();


            if (!string.IsNullOrWhiteSpace(qualityProject))
            {
                var lstQualityIds1 = (from a in db.QMS015_Log
                                      where a.QualityProject == qualityProject && (a.QualityIdApplicableFor == "Seam" || a.QualityIdApplicableFor == "Seam")
                                      select new CategoryData { catId = a.QualityId, CategoryDescription = a.QualityId + " - " + a.QualityIdDesc }).Distinct().ToList();

                //    lstGLB002 = (from a in db.GLB002
                //                 where a.Category == categoryId
                //                 select new CategoryData { Id = a.Id, CategoryDescription = a.Code + " - " + a.Description }).ToList();//db.GLB002.Where(i => i.Category == categoryId).ToList();
            }

            return lstGLB002;
        }

        public CategoryData GetCategory(string categoryCode, bool isOnlyDescriptionRequired = false)
        {
            CategoryData objGLB002 = new CategoryData();

            if (IsCategoryExist(categoryCode))
            {

                if (!string.IsNullOrEmpty(categoryCode))
                {
                    if (isOnlyDescriptionRequired)
                    {
                        objGLB002 = db.GLB002.Where(i => i.Code.Equals(categoryCode, StringComparison.OrdinalIgnoreCase)).Select(i => new CategoryData { Code = i.Code, CategoryDescription = i.Description }).FirstOrDefault();
                    }
                    else
                    {
                        objGLB002 = db.GLB002.Where(i => i.Code.Equals(categoryCode, StringComparison.OrdinalIgnoreCase)).Select(i => new CategoryData { Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).FirstOrDefault();
                    }
                }
            }
            else
            {
                objGLB002.CategoryDescription = categoryCode;
            }

            return objGLB002;
        }

        public CategoryData GetCategory(string categoryCode, string BU, string Location)
        {
            CategoryData objGLB002 = new CategoryData();

            if (IsCategoryExist(categoryCode))
            {

                if (!string.IsNullOrEmpty(categoryCode))
                {
                    objGLB002 = db.GLB002.Where(i => i.Code.Equals(categoryCode, StringComparison.OrdinalIgnoreCase) && i.BU == BU && i.Location == Location).Select(i => new CategoryData { Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).FirstOrDefault();
                }
            }
            else
            {
                objGLB002.CategoryDescription = categoryCode;
            }

            return objGLB002;
        }
        public CategoryData GetCategory(string category, string categoryCode, string BU, string Location, bool isOnlyDescriptionRequired = false)
        {
            CategoryData objGLB002 = new CategoryData();
            //var myList = new List<string>();
            if (isOnlyDescriptionRequired)
            {
                objGLB002 = (from glb002 in db.GLB002
                             join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                             where glb001.Category.Equals(category, StringComparison.OrdinalIgnoreCase) && glb002.Code.Equals(categoryCode) && glb002.IsActive == true && BU.Equals(glb002.BU) && Location.Equals(glb002.Location)
                             select new CategoryData { Code = glb002.Code, CategoryDescription = glb002.Description }).FirstOrDefault();
            }
            else
            {
                objGLB002 = (from glb002 in db.GLB002
                             join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                             where glb001.Category.Equals(category, StringComparison.OrdinalIgnoreCase) && glb002.Code.Equals(categoryCode) && glb002.IsActive == true && BU.Equals(glb002.BU) && Location.Equals(glb002.Location)
                             select new CategoryData { Code = glb002.Code, CategoryDescription = glb002.Code + " - " + glb002.Description }).FirstOrDefault();
            }
            if (objGLB002 == null)
            {
                objGLB002 = new CategoryData();
                objGLB002.CategoryDescription = categoryCode;
            }

            // Add items to the list
            lstGlobalCategoryUsed.Add(category);
            HttpContext.Current.Session[clsImplementationMessage.Session.GlobalCategoryUsed] = lstGlobalCategoryUsed;

            return objGLB002;
        }
        public CategoryData GetInspectionExtent(string categoryCode, string BU, string Location)
        {
            CategoryData objGLB002 = new CategoryData();

            if (IsCategoryExist(categoryCode))
            {

                if (!string.IsNullOrEmpty(categoryCode))
                {
                    objGLB002 = db.GLB002.Where(i => i.Code.Equals(categoryCode, StringComparison.OrdinalIgnoreCase) && i.BU == BU && i.Location == Location).Select(i => new CategoryData { Code = i.Code, CategoryDescription = i.Description }).FirstOrDefault();
                }
            }
            else
            {
                objGLB002.CategoryDescription = categoryCode;
            }

            return objGLB002;
        }
        public bool IsCategoryExist(string categoryCode)
        {
            bool categoryExist = false;

            if (db.GLB002.Where(i => i.Code.Equals(categoryCode, StringComparison.OrdinalIgnoreCase)).Any())
                categoryExist = true;

            return categoryExist;
        }

        public ModelNDETechnique GetQMSProjectDetail(string QMSProject)
        {
            ModelNDETechnique objModelTechniqueNo = new ModelNDETechnique();
            if (!string.IsNullOrEmpty(QMSProject))
            {
                var projectCode = db.QMS010.Where(i => i.QualityProject.Equals(QMSProject, StringComparison.OrdinalIgnoreCase)).Select(i => i.Project).FirstOrDefault();
                var manufacturingCode = db.QMS010.Where(i => i.QualityProject.Equals(QMSProject, StringComparison.OrdinalIgnoreCase)).Select(i => i.ManufacturingCode).FirstOrDefault();
                if (!string.IsNullOrEmpty(projectCode))
                {
                    objModelTechniqueNo.project = (from a in db.COM001
                                                   where a.t_cprj.Equals(projectCode, StringComparison.OrdinalIgnoreCase)
                                                   select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).FirstOrDefault();

                    var projectDetail = new clsManager().getProjectWiseBULocation(projectCode);
                    objModelTechniqueNo.BU = projectDetail.QCPBU;
                    objModelTechniqueNo.ManufacturingCode = manufacturingCode;
                    //objModelTechniqueNo.Location = projectDetail.lo;

                }
            }

            return objModelTechniqueNo;
        }

        public ModelCategory GetProjectDetailWithCategory(string QMSProject)
        {
            ModelCategory objModelTechniqueNo = new ModelCategory();
            if (!string.IsNullOrEmpty(QMSProject))
            {
                var projectCode = db.QMS010.Where(i => i.QualityProject.Equals(QMSProject, StringComparison.OrdinalIgnoreCase)).Select(i => i.Project).FirstOrDefault();
                if (!string.IsNullOrEmpty(projectCode))
                {
                    objModelTechniqueNo.project = (from a in db.COM001
                                                   where a.t_cprj.Equals(projectCode, StringComparison.OrdinalIgnoreCase)
                                                   select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).FirstOrDefault();

                    var projectDetail = new clsManager().getProjectWiseBULocation(projectCode);
                    objModelTechniqueNo.BU = projectDetail.QCPBU;

                }
            }

            return objModelTechniqueNo;
        }

        public QMS010 GetQMSProject(string qmsProject)
        {
            QMS010 objQMS010 = new QMS010();
            if (!string.IsNullOrEmpty(qmsProject))
                objQMS010 = db.QMS010.Where(i => i.QualityProject.Equals(qmsProject, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
            return objQMS010;
        }

        public int GetCategoryId(string categoryCode)
        {
            int Id = 0;
            CategoryData objGLB002 = new CategoryData();
            if (!string.IsNullOrEmpty(categoryCode))
            {
                objGLB002 = db.GLB001.Where(i => i.Category.Equals(categoryCode, StringComparison.OrdinalIgnoreCase)).Select(i => new CategoryData { Id = i.Id }).FirstOrDefault();
            }
            if (objGLB002 != null)
            {
                Id = objGLB002.Id;
            }
            return Id;
        }

        public string GenerateTextAreaFor(int rowId, string status, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string maxLength = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control";

            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onChange='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";
            if (string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
            {
                htmlControl = "<textarea disabled id='" + inputID + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " maxlength='" + maxLength + "' />";
            }
            else
            {
                htmlControl = "<textarea " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + " maxlength='" + maxLength + "' />";
            }

            return htmlControl;
        }

        public IQueryable<CategoryData> GetSubCatagory(string Key, string strLoc, string BU = "")
        {
            IQueryable<CategoryData> lstGLB002 = null;
            if (BU != string.Empty) //Get BU and Location Base Sub Category
            {
                lstGLB002 = (from glb002 in db.GLB002
                             join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                             where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.BU.Equals(BU, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true && strLoc.Equals(glb002.Location)
                             select glb002).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Code = i.Code, Description = i.Code + " - " + i.Description }).Distinct();
            }
            else //Get Location Base Sub Category
            {
                lstGLB002 = (from glb002 in db.GLB002
                             join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                             where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true && strLoc.Equals(glb002.Location)
                             select glb002).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Code = i.Code, Description = i.Code + " - " + i.Description }).Distinct();
            }
            return lstGLB002;
        }

    }

    public class ModelNDEPAUTCategory
    {
        public int HeaderId { get; set; }
        public int LineId { get; set; }
        public int ZoneNo { get; set; }
        public string Probe { get; set; }
        public int ProbeId { get; set; }
        public string Wedge { get; set; }
        public string WedgeCode { get; set; }
        public string NearRefractedArea { get; set; }
        public string NearRefractedAreaCode { get; set; }
        public string ScanType { get; set; }
        public string Skip { get; set; }
    }

    public class ProjectDataModel
    {
        public string ManufacturingCode { get; set; }
        public string BUDescription { get; set; }
        public List<BULocWiseCategoryModel> lstBULocWiseCategoryModel { get; set; }
    }

    public class BULocWiseCategoryModel
    {
        public string CatID { get; set; }
        public string CatDesc { get; set; }
    }

    public class ModelCategory : clsHelper.ResponseMsg
    {
        public Projects project { get; set; }
        public string BU { get; set; }
        public string ManufacturingCode { get; set; }

        #region PT Technique
        public List<CategoryData> PTMethod { get; set; }
        public List<CategoryData> CleaningSolvent { get; set; }
        public List<CategoryData> ApplicationMethod { get; set; }
        public List<CategoryData> RemovalMethod { get; set; }
        #endregion
        public List<CategoryData> LightIntensity { get; set; }
        public List<CategoryData> LightSource { get; set; }

        #region MT Technique
        public List<CategoryData> FieldAdequacy { get; set; }
        public List<CategoryData> Calibration { get; set; }
        public List<CategoryData> PostCleaning { get; set; }

        public List<CategoryData> MTMethod { get; set; }
        public List<CategoryData> MagnetizationTechnique { get; set; }
        public List<CategoryData> MethodOfExamination { get; set; }
        public List<CategoryData> VisibleUVLightIntensity { get; set; }
        public List<CategoryData> PenetrantProcessType { get; set; }
        public List<CategoryData> DeveloperApplicationMethod { get; set; }
        public List<CategoryData> Current { get; set; }
        public List<CategoryData> MagneticParticleType { get; set; }
        public List<CategoryData> ParticleColour { get; set; }
        public List<CategoryData> CarrierMedium { get; set; }
        public List<CategoryData> DemagnetizationUpto { get; set; }
        #endregion

        #region RT Technique
        public List<CategoryData> Technique { get; set; }
        public List<CategoryData> SourceOfRadiation { get; set; }
        public List<CategoryData> Film { get; set; }
        public List<CategoryData> IQI { get; set; }
        public List<CategoryData> ExposureDetail { get; set; }
        public List<CategoryData> Weld { get; set; }
        public List<CategoryData> GammaRaySource { get; set; }
        public List<CategoryData> IntensifyingScreen { get; set; }
        public List<CategoryData> IQIHoleType { get; set; }
        public List<CategoryData> IQIWireType { get; set; }
        public List<CategoryData> FilmBrand { get; set; }
        public List<CategoryData> FilmViewing { get; set; }
        #endregion

        #region PAUT Technique
        public List<CategoryData> Couplant { get; set; }
        public List<CategoryData> ReflectorSize { get; set; }
        public List<CategoryData> CableType { get; set; }
        public List<CategoryData> CableLength { get; set; }
        public List<CategoryData> EncoderType { get; set; }
        public List<CategoryData> ScanningTechnique { get; set; }
        public List<CategoryData> ScannerType { get; set; }

        public List<CategoryData> Probe { get; set; }
        public List<CategoryData> Wedge { get; set; }
        public List<CategoryData> NearRefractedArea { get; set; }
        public List<CategoryData> ScanType { get; set; }

        public string celluloscatCode { get; set; }
        public string celluloscatDesc { get; set; }
        public string Co_AxialcatCode { get; set; }
        public string Co_AxialcatDesc { get; set; }
        public string cabelLengthcatCode { get; set; }
        public string cabelLengthcatDesc { get; set; }
        public string Semi_AutomatedcatCode { get; set; }
        public string Semi_AutomatedcatDesc { get; set; }
        public bool IsCodeExists { get; set; }
        public string CodeValue { get; set; }

        #endregion

        public string Method { get; set; }
        public int DocumentNo { get; set; }
    }

    public class ModelNDETechnique
    {
        public bool isExists { get; set; }
        public string DocumentNo { get; set; }
        public string Method { get; set; }
        public Projects project { get; set; }
        public string BU { get; set; }
        public string Location { get; set; }
        public string ManufacturingCode { get; set; }
    }

    public class MethodResponse
    {
        public bool Key { get; set; }
        public string Method { get; set; }
        public int DocumentNo { get; set; }
    }

    public class clsPAUTEnum
    {
        public enum PAUTEnum
        {
            [StringValue("Manual")]
            Manual,
            [StringValue("Automatic")]
            Automatic,
            [StringValue("Semi-Automatic")]
            SemiAutomatic,

            [StringValue("Non-Automated")]
            NonAutomatedScanner,
            [StringValue("Semi -Automated")]
            SemiAutomatedScanner,
            [StringValue("Automated")]
            AutomatedScanner
        }

        public enum ScanType
        {
            [StringValue("Parallel")]
            Parallel,
            [StringValue("Non-Parallel")]
            NonParallel
        }

        public static List<string> getPAUTEnum()
        {
            List<string> items = clsImplementationEnum.GetListOfDescription<PAUTEnum>();
            return items;
        }

        public static List<string> getScanTypeEnum()
        {
            List<string> items = clsImplementationEnum.GetListOfDescription<ScanType>();
            return items;
        }
    }

    //created by Nikita - (Scan UT Technique)
    public class QualityProjectResponse
    {
        public bool Key { get; set; }
        public string Project { get; set; }
        public string BU { get; set; }
        public int DocumentNo { get; set; }
    }

    public class SourcePAUTTechnique
    {
        public NDE014 SourceHeader { get; set; }
        public List<NDE015> SourceCategory { get; set; }
        public List<NDE016> SourceSubCategory { get; set; }
    }

    public class ModelProbeDetail
    {
        public int ProbeId { get; set; }
        public string ProbeMake { get; set; }
        public int Frequency { get; set; }
        public decimal ElementSize { get; set; }
        public decimal ElementPitch { get; set; }
        public decimal ElementGapDistance { get; set; }
        public int NoOfElement { get; set; }
    }
}