﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.NDE.Controllers
{
    public class ApproveMTConsumeController : clsBase
    {
        /// <summary>
        /// created by nikita vibhandik : 11/7/2017
        /// description :approval of MT consuamable
        /// </summary>
        /// <returns></returns>
        // GET: NDE/ApproveMTConsume
        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        #region Index//main page
        public ActionResult Index()
        {
            return View();
        }

        #region Tab
        [HttpPost]
        public ActionResult GetMTPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_approveMTHtmlPartial");
        }

        // load datatable
        [HttpPost]
        public JsonResult LoadMTData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.MTStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.NDETechniqueStatus.SentForApproval.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "1=1";
                }
                string[] columnName = { "bcom2.t_desc", "lcom2.t_desc", "NDE.Project", "com1.t_dsca", "QualityProject", "MTConsuNo", "Status" };
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "NDE.BU", "NDE.Location");

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_MT_CONSUMABLE_GETDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                           Convert.ToString(uc.Id),
                           Convert.ToString(uc.QualityProject),
                           Convert.ToString(uc.Project),
                           Convert.ToString(uc.BU),
                           Convert.ToString(uc.Location),
                           Convert.ToString(uc.Method),
                           Convert.ToString(uc.MTConsuNo),
                           Convert.ToString(uc.CreatedBy),
                           Convert.ToString(uc.CreatedOn !=null?(Convert.ToDateTime(uc.CreatedOn)).ToString("dd/MM/yyyy"):""),
                            Convert.ToString(uc.Status),
                           Convert.ToString(uc.Manufacturer),
                           Convert.ToString(uc.BrandType),
                           Convert.ToString(uc.ReturnRemarks),
                           Convert.ToString("R " +uc.RevNo),                          
                           generateActionButtons(uc.Id,uc.Status,"/NDE/ApproveMTConsume/ApproveConsumable?Id="+uc.Id,"/NDE/ConsumeMTMaster/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.Id),true,false,true,false,Convert.ToInt32(uc.RevNo))
                           //Convert.ToString(uc.Id)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhere,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #endregion

        #region Approval Page
        [SessionExpireFilter]
        public ActionResult ApproveConsumable(int? id)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("NDE020");
            NDE020 objNDE020 = new NDE020();
            var user = objClsLoginInfo.UserName;
            NDEModels objNDEModels = new NDEModels();
            if (id != null)
            {
                objNDE020 = db.NDE020.Where(x => x.Id == id).FirstOrDefault();
                //ViewBag.BrandType = db.GLB002.Where(i => i.Code == objNDE020.BrandType).Select(i => i.Code + " - " + i.Description).FirstOrDefault();
                ViewBag.BrandType = objNDEModels.GetCategory(objNDE020.BrandType).CategoryDescription;
                ViewBag.BrandType2 = objNDEModels.GetCategory(objNDE020.BrandType2).CategoryDescription;
                ViewBag.BrandType3 = objNDEModels.GetCategory(objNDE020.BrandType3).CategoryDescription;
                ViewBag.Method = db.GLB002.Where(i => i.Code == objNDE020.Method).Select(i => i.Code + " - " + i.Description).FirstOrDefault();
                ViewBag.QualityProject = db.QMS010.Where(i => i.QualityProject == objNDE020.QualityProject).Select(i => i.QualityProject).FirstOrDefault();
                objNDE020.Project = db.COM001.Where(i => i.t_cprj == objNDE020.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objNDE020.BU = db.COM002.Where(i => i.t_dtyp == 2 && i.t_dimx == objNDE020.BU).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
                objNDE020.Location = db.COM002.Where(i => i.t_dtyp == 1 && i.t_dimx == objNDE020.Location).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
                ViewBag.ManufacturingCode = db.QMS010.Where(x => x.QualityProject == objNDE020.QualityProject).Select(x => x.ManufacturingCode).FirstOrDefault();

            }

            return View(objNDE020);
        }
        //approve header
        [HttpPost]
        public ActionResult ApproveMT(int Id, string returnRemarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (Id > 0)
                {
                    if (!IsapplicableForAttend(Id))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This record has been already attended, Please refresh page.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                }
                db.SP_MT_CONSUMABLE_APPROVE(Id, objClsLoginInfo.UserName);

                #region Send Notification
                NDE020 objNDE020 = db.NDE020.Where(c => c.Id == Id).FirstOrDefault();
                if (objNDE020 != null)
                {
                    objNDE020.ReturnRemarks = returnRemarks;
                    db.SaveChanges();
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE3.GetStringValue(), objNDE020.Project, objNDE020.BU, objNDE020.Location, "Consumable: " + objNDE020.MTConsuNo + " has been Approved", clsImplementationEnum.NotificationType.Information.GetStringValue());
                }
                #endregion

                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.QCPMessages.Approve.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        private bool IsapplicableForAttend(int Id)
        {
            NDE020 objNDE020 = db.NDE020.Where(x => x.Id == Id).FirstOrDefault();
            if (objNDE020 != null)
            {
                if (objNDE020.Status == clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return true;
           
        }
        //approve all header on selection(from index)
        [HttpPost]
        public ActionResult ApproveByApproverSelected(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
            try
            {
                for (int i = 0; i < arrayHeaderIds.Length; i++)
                {
                    int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                    db.SP_MT_CONSUMABLE_APPROVE(HeaderId, objClsLoginInfo.UserName);

                    #region Send Notification
                    NDE020 objNDE020 = db.NDE020.Where(c => c.Id == HeaderId).FirstOrDefault();
                    if (objNDE020 != null)
                    {
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE3.GetStringValue(), objNDE020.Project, objNDE020.BU, objNDE020.Location, "Consumable: " + objNDE020.MTConsuNo + " has been Approved", clsImplementationEnum.NotificationType.Information.GetStringValue());
                    }
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.QCPMessages.Approve.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        //return header
        [HttpPost]
        public ActionResult ReturnMT(FormCollection fc, NDE020 nde020)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string returnStatus = clsImplementationEnum.NDETechniqueStatus.Returned.GetStringValue();
            try
            {
                NDE020 objNDE020 = db.NDE020.Where(u => u.Id == nde020.Id).SingleOrDefault();
                if (objNDE020 != null)
                {
                    if (!IsapplicableForAttend(objNDE020.Id))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This record has been already attended, Please refresh page.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    objNDE020.Status = returnStatus;
                    objNDE020.ReturnRemarks = nde020.ReturnRemarks;
                    objNDE020.ApprovedBy = objClsLoginInfo.UserName;
                    objNDE020.ApprovedOn = DateTime.Now;
                    db.SaveChanges();

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE3.GetStringValue(), objNDE020.Project, objNDE020.BU, objNDE020.Location, "Consumable: " + objNDE020.MTConsuNo + " has been Returned", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/NDE/ConsumeMTMaster/AddConsumable?Id=" + objNDE020.Id);
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Returned.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Common function
        public string generateActionButtons(int HeaderId, string status, string viewUrl = "", string timelineUrl = "", bool isViewButton = false, bool isDeleteButton = false, bool isTimelinebutton = false, bool isHistoryButton = false, int Revision = 0)
        {
            string strButtons = "<center>";
            if (isViewButton)
            {
                strButtons += "<a title=\"View\" href=\"" + WebsiteURL + viewUrl + "\"><i style=\"margin-left:5px;\" class=\"fa fa-eye\"></i></a>";
            }
            if (isDeleteButton)
            {
                if (status == clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue() && Revision == 0)
                {
                    strButtons += "<i id=\"btnDelete\" name=\"btnAction\" style=\"cursor: pointer;margin-left:5px;\" Title=\"Delete Record\" class=\"fa fa-trash-o\" onclick=\"DeleteHeader(" + HeaderId + ")\"></i>";
                }
                else
                {
                    strButtons += "<i id=\"btnDelete\" name=\"btnAction\" style=\"cursor: pointer; opacity: 0.5;margin-left:5px;\" Title=\"Delete Record\" class=\"fa fa-trash-o\"></i>";
                }
            }
            if (isTimelinebutton)
            {
                strButtons += "<a title=\"View Timeline\" href=\"javascript:void(0)\" onclick=\"ShowTimeline('" + timelineUrl + "');\"><i style=\"margin-left:5px;\" class=\"fa fa-clock-o\"></i></a>";
            }
            if (isHistoryButton)
            {
                if (Revision > 0 || status == clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue())
                {
                    strButtons += "<i title=\"History\" onclick=History('" + Convert.ToInt32(HeaderId) + "'); style = \"cursor: pointer;margin-left:5px;\" class=\"fa fa-history\"></i>";
                }
                else
                {
                    strButtons += "<i title=\"History\" style = \"cursor: pointer;margin-left:5px;opacity:0.5\" class=\"fa fa-history\"></i>";
                }
            }
            strButtons += "</center>";
            return strButtons;
        }
        #endregion
    }
}