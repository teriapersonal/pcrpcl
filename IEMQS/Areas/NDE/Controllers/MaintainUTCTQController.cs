﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.NDE.Controllers
{
    public class MaintainUTCTQController : clsBase
    {

        /// <summary>
        /// Made By Dharmesh Vasani
        /// Modified by Nikita
        /// </summary>
        /// <returns></returns>

        #region Header
        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadUTCTQListDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_UTCTQListDataPartial");
        }
        [HttpPost]
        public JsonResult LoadUTCTQHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.UTCTQStatus.DRAFT.GetStringValue() + "','" + clsImplementationEnum.UTCTQStatus.Returned.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "1=1";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (ltrim(rtrim(nde5.BU))+'-'+bcom2.t_desc like '%" + param.sSearch +
                                "%' or ltrim(rtrim(nde5.Location))+'-'+lcom2.t_desc like '%" + param.sSearch +
                                "%' or (nde5.Project+'-'+com1.t_dsca) like '%" + param.sSearch +
                                "%' or CTQNo like '%" + param.sSearch +
                                "%' or RevNo like '%" + param.sSearch +
                                "%' or ApplicableSpecification like '%" + param.sSearch +
                                "%' or AcceptanceStandard like '%" + param.sSearch +
                                "%' or ASMECodeStamp like '%" + param.sSearch +
                                "%' or TPIName like '%" + param.sSearch +
                                "%' or NotificationRequired like '%" + param.sSearch +
                                "%' or ThicknessOfJob like '%" + param.sSearch +
                                "%' or SizeDimension like '%" + param.sSearch +
                                "%' or NDERemarks like '%" + param.sSearch +
                                "%' or nde5.CreatedBy +' - '+com003c.t_name like '%" + param.sSearch +
                                "%' or ManufacturingCode like '%" + param.sSearch +
                                "%' or Status like '%" + param.sSearch + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "nde5.BU", "nde5.Location");

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_UTCTQ_GETHEADER
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.BU),
                               Convert.ToString(uc.Location),
                               Convert.ToString(uc.CTQNo),
                               Convert.ToString(uc.ManufacturingCode),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn !=null?(Convert.ToDateTime(uc.CreatedOn)).ToString("dd/MM/yyyy"):""),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.ApplicableSpecification),
                               Convert.ToString(uc.AcceptanceStandard),
                               (uc.ASMECodeStamp == true?"<label class=\"mt-checkbox mt-checkbox-outline\"><input checked=\"checked\" disabled=\"disabled\" type = \"checkbox\" value=\"\" /><span></span></label>":"<label class=\"mt-checkbox mt-checkbox-outline\"><input disabled=\"disabled\" type = \"checkbox\" value=\"\" /><span></span></label>"),//Convert.ToString(uc.ASMECodeStamp),
                               Convert.ToString(uc.TPIName),
                               Convert.ToString(uc.NotificationRequired == true ? "Yes" : uc.NotificationRequired == false ? "No" : ""),
                               Convert.ToString(uc.ThicknessOfJob),
                               Convert.ToString(uc.SizeDimension),
                               Convert.ToString(uc.NDERemarks),
                               "<nobr><center>"+
                               "<a title='View' href='"+WebsiteURL+"/NDE/MaintainUTCTQ/AddUpdateUTCTQ?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i class='iconspace fa fa-eye'></i></a>"+
                                 (uc.Status.ToLower() == clsImplementationEnum.CommonStatus.DRAFT.GetStringValue().ToLower() && uc.RevNo == 0 ? "<i id=\"btnDelete\" name=\"btnAction\" style=\"cursor:pointer;margin-left:5px;\" Title=\"Delete Record\" class=\"fa fa-trash-o\" onclick=\"DeleteHeader('" + uc.HeaderId + "')\"></i>" : "<i id=\"btnDelete\" name=\"btnAction\" style=\"cursor:pointer;\" Title=\"Delete Record\" class=\"disabledicon fa fa-trash-o\" ></i>")+
                               "<a title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/NDE/MaintainUTCTQ/ShowTimeline?HeaderID="+Convert.ToInt32(uc.HeaderId)+"');><i  class='iconspace fa fa-clock-o'></i></a>"+""+((uc.RevNo > 0 || uc.Status.ToLower() == clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue().ToLower()) ?"<i title='History' onclick=History('"+Convert.ToInt32(uc.HeaderId)+"'); style ='cursor:pointer;' class='iconspace fa fa-history'></i>" : "<i title='History' style = 'cursor:pointer;' class='disabledicon fa fa-history'></i>" )+""+"</center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        //delete header (in draft and R0)
        [HttpPost]
        public ActionResult DeleteHeader(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                NDE005 objNDE005 = db.NDE005.Where(x => x.HeaderId == headerId).FirstOrDefault();
                List<NDE006> objNDE006 = db.NDE006.Where(x => x.HeaderId == headerId).ToList();
                if (objNDE005 != null)
                {
                    if (objNDE006.Count > 0)
                    {
                        db.NDE006.RemoveRange(objNDE006);
                        db.SaveChanges();
                    }
                    db.NDE005.Remove(objNDE005);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Add header form
        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult AddUpdateUTCTQ(int HeaderID = 0)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("NDE005");
            ViewBag.HeaderID = HeaderID;
            return View();
        }

        [HttpPost]
        public ActionResult LoadUTCTQAddUpdateFormPartial(int HeaderID)
        {
            NDE005 objNDE005 = new NDE005();

            string user = objClsLoginInfo.UserName;
            string BU = string.Join(",", db.ATH001.Where(i => i.Employee == user).Select(i => i.BU).ToList());
            List<Projects> project = Manager.getProjectsByUser(user);
            string[] location = db.ATH001.Where(i => i.Employee == user).Select(i => i.Location).ToArray();

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.lstyesno = lstyesno.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

            if (HeaderID > 0)
            {
                objNDE005 = db.NDE005.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
                NDEModels objNDEModels = new NDEModels();
                string strBU = db.COM001.Where(i => i.t_cprj == objNDE005.Project).FirstOrDefault().t_entu;
                var BUDescription = (from a in db.COM002
                                     where a.t_dtyp == 2 && a.t_dimx == strBU
                                     select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
                objNDE005.BU = BUDescription.BUDesc;
                var locDescription = (from a in db.COM002
                                      where a.t_dtyp == 1 && a.t_dimx == objNDE005.Location
                                      select new { Location = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
                objNDE005.Location = locDescription.Location;

                var projectDetails = db.COM001.Where(x => x.t_cprj == objNDE005.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                ViewBag.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
            }
            else
            {
                objNDE005.Status = clsImplementationEnum.RTCTQStatus.DRAFT.GetStringValue();
                objNDE005.RevNo = 0;
                var locDescription = (from a in db.COM003
                                      join b in db.COM002 on a.t_loca equals b.t_dimx
                                      where b.t_dtyp == 1 && a.t_actv == 1
                                      && a.t_psno.Equals(user, StringComparison.OrdinalIgnoreCase)
                                      select b.t_dimx + "-" + b.t_desc).FirstOrDefault();

                objNDE005.Location = locDescription;
            }
            // ViewBag.Project = new SelectList(project, "projectCode", "projectDescription");
            return PartialView("_UTCTQAddUpdateFormPartial", objNDE005);
        }

        [HttpPost]
        public ActionResult SaveHeader(NDE005 nde005, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                if (fc["txtNotificationRequired"].ToString().ToLower() == "yes")
                {
                    nde005.NotificationRequired = true;
                }
                else
                {
                    nde005.NotificationRequired = false;
                }
                if (nde005.HeaderId > 0)
                {
                    if (!IsapplicableForSave(nde005.HeaderId))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This record has been already submitted, Please refresh page.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    NDE005 objNDE005 = db.NDE005.Where(x => x.HeaderId == nde005.HeaderId).FirstOrDefault();
                    objNDE005.ApplicableSpecification = nde005.ApplicableSpecification;
                    objNDE005.AcceptanceStandard = nde005.AcceptanceStandard;
                    objNDE005.ASMECodeStamp = nde005.ASMECodeStamp;
                    objNDE005.TPIName = nde005.TPIName;
                    objNDE005.NotificationRequired = nde005.NotificationRequired;
                    objNDE005.ThicknessOfJob = nde005.ThicknessOfJob;
                    objNDE005.SizeDimension = nde005.SizeDimension;
                    objNDE005.ManufacturingCode = nde005.ManufacturingCode;
                    objNDE005.NDERemarks = nde005.NDERemarks;
                    if (objNDE005.Status == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue())
                    {
                        objNDE005.RevNo = Convert.ToInt32(objNDE005.RevNo) + 1;
                        objNDE005.ReturnRemarks = null;
                        objNDE005.SubmittedBy = null;
                        objNDE005.SubmittedOn = null;
                        objNDE005.ApprovedOn = null;
                        foreach (var item in objNDE005.NDE006)
                        {
                            item.ReturnRemark = null;
                            item.ReturnedBy = null;
                            item.ReturnedOn = null;
                        }
                        objNDE005.Status = clsImplementationEnum.UTCTQStatus.DRAFT.GetStringValue();
                    }
                    objNDE005.EditedBy = objClsLoginInfo.UserName;
                    objNDE005.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objNDE005.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.UTCTQMessages.Update.ToString();
                    objResponseMsg.Remarks = objNDE005.ReturnRemarks;
                }
                else
                {
                    NDE005 objNDE005 = new NDE005();
                    objNDE005.Project = nde005.Project;
                    objNDE005.BU = nde005.BU.Split('-')[0].ToString();
                    objNDE005.Location = nde005.Location.Split('-')[0].ToString();
                    objNDE005.RevNo = 0;
                    objNDE005.CTQNo = nde005.CTQNo;
                    objNDE005.Status = nde005.Status;
                    objNDE005.ApplicableSpecification = nde005.ApplicableSpecification;
                    objNDE005.AcceptanceStandard = nde005.AcceptanceStandard;
                    objNDE005.ASMECodeStamp = nde005.ASMECodeStamp;
                    objNDE005.TPIName = nde005.TPIName;
                    objNDE005.NotificationRequired = nde005.NotificationRequired;
                    objNDE005.ThicknessOfJob = nde005.ThicknessOfJob;
                    objNDE005.SizeDimension = nde005.SizeDimension;
                    objNDE005.ManufacturingCode = nde005.ManufacturingCode;
                    objNDE005.NDERemarks = nde005.NDERemarks;
                    objNDE005.CreatedBy = objClsLoginInfo.UserName;
                    objNDE005.CreatedOn = DateTime.Now;
                    db.NDE005.Add(objNDE005);
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objNDE005.HeaderId;
                    var lstDescription = Manager.GetSubCatagories("NDE-UTCTQ", objNDE005.BU, objNDE005.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();

                    List<NDE006> lstNDE006 = new List<NDE006>();
                    for (int i = 0; i < lstDescription.Count(); i++)
                    {
                        NDE006 objNDE006 = new NDE006();
                        objNDE006.Description = lstDescription[i].CategoryDescription;
                        objNDE006.HeaderId = objNDE005.HeaderId;
                        objNDE006.Project = objNDE005.Project;
                        objNDE006.BU = objNDE005.BU;
                        objNDE006.Location = objNDE005.Location;
                        objNDE006.RevNo = 0;
                        objNDE006.LineRevNo = 0;
                        objNDE006.CreatedBy = objClsLoginInfo.UserName;
                        objNDE006.CreatedOn = DateTime.Now;
                        objNDE006.Status = clsImplementationEnum.CommonStatus.Added.GetStringValue();
                        lstNDE006.Add(objNDE006);
                    }
                    if (lstNDE006 != null && lstNDE006.Count > 0)
                    {
                        db.NDE006.AddRange(lstNDE006);
                        db.SaveChanges();
                    }


                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.UTCTQMessages.Insert.ToString();
                    objResponseMsg.Remarks = objNDE005.ReturnRemarks;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Lines

        [HttpPost]
        public ActionResult GetUTCTQLinesForm(string projCode, string BU, string location, string ctqNo, int LineId)
        {
            NDE006 objNDE006 = new NDE006();
            NDE005 objNDE005 = db.NDE005.Where(x => x.Project == projCode).FirstOrDefault();

            List<string> lstScanningRequirement = clsImplementationEnum.GetScanningRequirement().ToList();
            ViewBag.lstScanningRequirement = lstScanningRequirement.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            if (LineId > 0)
            {
                objNDE006 = db.NDE006.Where(x => x.LineId == LineId).FirstOrDefault();
                projCode = objNDE006.Project;
                objNDE006.NDE005.RevNo = objNDE006.RevNo;
            }
            else
            {
                ViewBag.CTQNo = projCode + "_CTQ_UT";
                objNDE006.RevNo = 0;

            }
            objNDE006.HeaderId = objNDE005.HeaderId;
            string user = objClsLoginInfo.UserName;
            //string projectDesc = Manager.getProjectsByUser(user).Where(x => x.projectCode == projCode).Select(x => x.projectDescription).FirstOrDefault();

            var projectDetails = db.COM001.Where(x => x.t_cprj == objNDE005.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
            string projectDesc = projectDetails.t_cprj + " - " + projectDetails.t_dsca;

            BU = db.COM001.Where(i => i.t_cprj == projCode).FirstOrDefault().t_entu;
            var BUDescription = (from a in db.COM002
                                 where a.t_dtyp == 2 && a.t_dimx == BU
                                 select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();

            ViewBag.Project = projectDesc;
            ViewBag.BU = BUDescription.BUDesc;
            ViewBag.Location = location;
            ViewBag.CTQNo = projCode + "_CTQ_UT";
            return PartialView("_GetRTCTQLinesForm", objNDE006);
        }
        [HttpPost]
        public ActionResult LoadUTCTQHeaderLineData(JQueryDataTableParamModel param)
        {
            try
            {
                NDEModels ndeHelper = new NDEModels();
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();
                string[] arrayCTQStatus = { clsImplementationEnum.RTCTQStatus.DRAFT.GetStringValue(), clsImplementationEnum.RTCTQStatus.Returned.GetStringValue() };
                string[] columnName = { "Description", "SpecificationNo", "SpecificationWithClauseNo", "SpecificationRequirement", "ToBeDiscussed", "CriticalToQuality", "CTQRequirement", "ReturnRemark" };
                if (param.sSearch == "yes" || param.sSearch == "Yes" || param.sSearch == "YES")
                {
                    param.sSearch = "1";
                }
                if (param.sSearch == "no" || param.sSearch == "NO" || param.sSearch == "No")
                {
                    param.sSearch = "0";
                }
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);

                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_GET_UT_HEADER_LINES
                                (
                               StartIndex,
                               EndIndex,
                               strSortOrder,
                               strWhere
                                ).ToList();
                List<SelectListItem> getyesno = new List<SelectListItem>() { new SelectListItem { Text = "Select", Value = "" }, new SelectListItem { Text = "Yes", Value = "Yes" }, new SelectListItem { Text = "No", Value = "No" } };

                List<SelectListItem> BoolenList = new List<SelectListItem>() { new SelectListItem { Text = "Select", Value = "" }, new SelectListItem { Text = "Yes", Value = "True" }, new SelectListItem { Text = "No", Value = "False" } };
                int newRecordId = 0;
                var newRecord = new[] {
                                    "",
                                    Helper.GenerateTextArea(newRecordId, "Description","","",false,"width:100%;","100"),
                                    Helper.GenerateDropdown(newRecordId, "ApplicableForAScan", new SelectList(getyesno, "Value", "Text","Select"), ""),
                                    Helper.GenerateDropdown(newRecordId, "ApplicableForToFD", new SelectList(getyesno, "Value", "Text","Select"),""),
                                    Helper.GenerateDropdown(newRecordId, "ApplicableForPAUT", new SelectList(getyesno, "Value", "Text","Select"), ""),
                                    Helper.GenerateTextArea(newRecordId, "SpecificationNo","","",false,"width:200px;","50"),
                                    Helper.GenerateTextArea(newRecordId, "SpecificationWithClauseNo","","",false,"width:200px;","50"),
                                    Helper.GenerateDropdown(newRecordId, "ToBeDiscussed", new SelectList(BoolenList, "Value", "Text","Select"), ""),
                                    Helper.GenerateTextArea(newRecordId, "SpecificationRequirement","","",false,"width:200px;","100"),
                                    Helper.GenerateDropdown(newRecordId, "CriticalToQuality", new SelectList(BoolenList, "Value", "Text","Select"),"","",false,"width:100px;"),
                                    Helper.GenerateTextArea(newRecordId, "CTQRequirement", "","",false,"width:200px;","100"),
                                    "",
                                    clsImplementationEnum.CommonStatus.Added.GetStringValue(),
                                    Helper.GenerateGridButton(newRecordId, "Save", "Add Rocord", "fa fa-save", "SaveNewRecord();" ),
                                    "",
                                    Helper.GenerateHidden(newRecordId, "HeaderId", param.CTQHeaderId),
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.Description),
                                Helper.GenerateDropdown(uc.LineId, "ApplicableForAScan", new SelectList(getyesno, "Value", "Text", Convert.ToString(uc.ApplicableForAScan)),"","UpdateData(this, "+ uc.HeaderId +","+uc.LineId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Deleted.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.CTQCompileStatus,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"width:100px;"),
                                Helper.GenerateDropdown(uc.LineId, "ApplicableForToFD", new SelectList(getyesno, "Value", "Text", Convert.ToString(uc.ApplicableForToFD)),"","UpdateData(this, "+ uc.HeaderId +","+uc.LineId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Deleted.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.CTQCompileStatus,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),""),
                                Helper.GenerateDropdown(uc.LineId, "ApplicableForPAUT", new SelectList(getyesno, "Value", "Text", Convert.ToString(uc.ApplicableForPAUT)),"","UpdateData(this, "+ uc.HeaderId +","+uc.LineId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Deleted.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.CTQCompileStatus,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"width:100px;"),
                                Helper.GenerateTextArea(uc.LineId, "SpecificationNo",    Convert.ToString(uc.SpecificationNo),"UpdateData(this, "+ uc.HeaderId +","+uc.LineId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Deleted.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.CTQCompileStatus,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"width:200px;","50"),
                                Helper.GenerateTextArea(uc.LineId, "SpecificationWithClauseNo",    Convert.ToString(uc.SpecificationWithClauseNo),"UpdateData(this, "+ uc.HeaderId +","+uc.LineId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Deleted.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.CTQCompileStatus,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"width:200px;","50"),
                                Helper.GenerateDropdown(uc.LineId, "ToBeDiscussed", new SelectList(BoolenList, "Value", "Text", Convert.ToString(uc.ToBeDiscussed)),"","UpdateData(this, "+ uc.HeaderId +","+uc.LineId+",false);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Deleted.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.CTQCompileStatus,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),""),
                                Helper.GenerateTextArea(uc.LineId, "SpecificationRequirement",    Convert.ToString(uc.SpecificationRequirement),"UpdateData(this, "+ uc.HeaderId +","+uc.LineId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Deleted.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.CTQCompileStatus,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"width:200px;","100"),
                                Helper.GenerateDropdown(uc.LineId,"CriticalToQuality", new SelectList(BoolenList, "Value", "Text", Convert.ToString(uc.CriticalToQuality)),"","UpdateData(this, "+ uc.HeaderId +","+uc.LineId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Deleted.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.CTQCompileStatus,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"width:100px;"),
                                Helper.GenerateTextArea(uc.LineId, "CTQRequirement",    Convert.ToString(uc.CTQRequirement),"UpdateData(this, "+ uc.HeaderId +","+uc.LineId+",false);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Deleted.GetStringValue(),StringComparison.OrdinalIgnoreCase) ||string.Equals(param.CTQCompileStatus,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"width:200px;","100"),
                                Convert.ToString(uc.ReturnRemark),
                                Convert.ToString(uc.Status),
                                "<center style=\"display:inline;\">"+ Helper.HTMLActionString(uc.LineId,"Delete","Delete Record","fa fa-trash-o","DeleteRecord("+ uc.LineId +");","",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Deleted.GetStringValue(),StringComparison.OrdinalIgnoreCase) ||string.Equals(param.CTQCompileStatus,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false))
                               +"<a class='' href='javascript:void(0)' onclick=ShowTimeline('/NDE/MaintainUTCTQ/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId)+"&LineId="+Convert.ToInt32(uc.LineId)+"');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a>"
                               +"</center>",
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId),
                             }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdateData(int headerId, string columnName, string columnValue, int LineId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (headerId > 0)
                {
                    if (!IsapplicableForSubmit(headerId))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This record has been already submitted, Please refresh page.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                }
                if (!string.IsNullOrEmpty(columnName))
                {
                    db.SP_NDE_UT_CTQ_UPDATE_COLUMN(LineId, columnName, columnValue);
                    ReviseData(headerId);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                    objResponseMsg.LineStatus = clsImplementationEnum.CommonStatus.Modified.GetStringValue();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReviseData(int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                NDE005 objNDE005 = db.NDE005.Where(x => x.HeaderId == headerId).FirstOrDefault();
                //NDE006 objNDE006 = db.NDE006.Where(x => x.HeaderId == headerId).FirstOrDefault();
                if (objNDE005.Status == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue())
                {
                    objNDE005.RevNo = Convert.ToInt32(objNDE005.RevNo) + 1;
                    objNDE005.Status = clsImplementationEnum.UTCTQStatus.DRAFT.GetStringValue();
                    objNDE005.EditedBy = objClsLoginInfo.UserName;
                    objNDE005.EditedOn = DateTime.Now;
                    objNDE005.ReturnRemarks = null;
                    objNDE005.SubmittedBy = null;
                    objNDE005.SubmittedOn = null;
                    objNDE005.ApprovedOn = null;
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                objResponseMsg.Remarks = objNDE005.ReturnRemarks;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveNewLines(FormCollection fc)
        {
            int newRowIndex = 0;
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int headerId = Convert.ToInt32(fc["HeaderId" + newRowIndex]);
                if (headerId > 0)
                {
                    if (!IsapplicableForSubmit(headerId))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This record has been already submitted, Please refresh page.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                }
                NDE005 objNDE005 = db.NDE005.Where(x => x.HeaderId == headerId).FirstOrDefault();
                NDE006 objNDE006 = new NDE006();
                objNDE006.HeaderId = objNDE005.HeaderId;
                objNDE006.Project = objNDE005.Project;
                objNDE006.BU = objNDE005.BU;
                objNDE006.Location = objNDE005.Location;
                objNDE006.Description = fc["Description" + newRowIndex];
                objNDE006.SpecificationNo = fc["SpecificationNo" + newRowIndex];
                objNDE006.SpecificationWithClauseNo = fc["SpecificationWithClauseNo" + newRowIndex];
                objNDE006.SpecificationRequirement = fc["SpecificationRequirement" + newRowIndex];
                if (fc["ToBeDiscussed" + newRowIndex] != null && fc["ToBeDiscussed" + newRowIndex] != "")
                {
                    objNDE006.ToBeDiscussed = Convert.ToBoolean(fc["ToBeDiscussed" + newRowIndex]);
                }
                objNDE006.CTQRequirement = fc["CTQRequirement" + newRowIndex];
                if (fc["CriticalToQuality" + newRowIndex] != null && fc["CriticalToQuality" + newRowIndex] != "")
                {
                    objNDE006.CriticalToQuality = Convert.ToBoolean(fc["CriticalToQuality" + newRowIndex]);
                }
                objNDE006.ApplicableForAScan = fc["ApplicableForAScan" + newRowIndex];
                objNDE006.ApplicableForPAUT = fc["ApplicableForPAUT" + newRowIndex];
                objNDE006.ApplicableForToFD = fc["ApplicableForToFD" + newRowIndex];
                objNDE006.RevNo = 0;
                objNDE006.LineRevNo = 0;
                objNDE006.Status = clsImplementationEnum.CommonStatus.Added.GetStringValue();
                objNDE006.CreatedBy = objClsLoginInfo.UserName;
                objNDE006.CreatedOn = DateTime.Now;
                if (objNDE005.Status == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue())
                {
                    objNDE005.RevNo = Convert.ToInt32(objNDE005.RevNo) + 1;
                    objNDE005.Status = clsImplementationEnum.UTCTQStatus.DRAFT.GetStringValue();
                    objNDE005.EditedBy = objClsLoginInfo.UserName;
                    objNDE005.EditedOn = DateTime.Now;
                    objNDE005.ReturnRemarks = null;
                    objNDE005.SubmittedBy = null;
                    objNDE005.SubmittedOn = null;
                    objNDE005.ApprovedOn = null;
                }
                db.NDE006.Add(objNDE006);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
                objResponseMsg.Remarks = objNDE005.ReturnRemarks;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveUTCTQLines(NDE006 nde006, FormCollection fc)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {

                NDE005 objNDE005 = db.NDE005.Where(x => x.HeaderId == nde006.HeaderId).FirstOrDefault();
                if (nde006.LineId > 0)
                {
                    NDE006 objNDE006 = db.NDE006.Where(x => x.LineId == nde006.LineId).FirstOrDefault();
                    if (objNDE006.Status == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue())
                    {
                        objNDE006.RevNo = Convert.ToInt32(objNDE006.RevNo) + 1;
                        objNDE006.LineRevNo = Convert.ToInt32(objNDE006.LineRevNo) + 1;
                        objNDE005.RevNo = Convert.ToInt32(objNDE005.RevNo) + 1;
                        objNDE005.ReturnRemarks = null;
                        objNDE005.SubmittedBy = null;
                        objNDE005.SubmittedOn = null;
                        objNDE005.ApprovedOn = null;
                    }
                    objNDE005.Status = clsImplementationEnum.UTCTQStatus.DRAFT.GetStringValue();
                    objNDE006.Status = clsImplementationEnum.CommonStatus.Added.GetStringValue();
                    objNDE006.HeaderId = nde006.HeaderId;
                    objNDE006.Project = objNDE005.Project;
                    objNDE006.BU = objNDE005.BU;
                    objNDE006.Location = objNDE005.Location;
                    objNDE006.SpecificationNo = nde006.SpecificationNo;
                    objNDE006.SpecificationWithClauseNo = nde006.SpecificationWithClauseNo;
                    objNDE006.SpecificationRequirement = nde006.SpecificationRequirement;
                    objNDE006.CriticalToQuality = nde006.CriticalToQuality;
                    objNDE006.CTQRequirement = nde006.CTQRequirement;
                    objNDE006.ToBeDiscussed = nde006.ToBeDiscussed;
                    objNDE006.ScanningRequirement = nde006.ScanningRequirement;
                    objNDE006.ApprovalRequired = nde006.ApprovalRequired;
                    objNDE006.ProcedureDemonstration = nde006.ProcedureDemonstration;
                    objNDE006.SpecialQualification = nde006.SpecialQualification;
                    objNDE006.SpecialAccessories = nde006.SpecialAccessories;
                    objNDE006.Others = nde006.Others;
                    objNDE006.FeasibilityDone = nde006.FeasibilityDone;
                    objNDE006.Blocktobefabricated = nde006.Blocktobefabricated;
                    objNDE006.BlindTest = nde006.BlindTest;
                    objNDE006.BlockSubmitToCustomer = nde006.BlockSubmitToCustomer;
                    objNDE006.PAUTinAddition = nde006.PAUTinAddition;
                    objNDE006.EditedBy = objClsLoginInfo.UserName;
                    objNDE006.EditedOn = DateTime.Now;
                }
                else
                {
                    db.NDE006.Add(new NDE006
                    {
                        HeaderId = nde006.HeaderId,
                        Project = objNDE005.Project,
                        BU = objNDE005.BU,
                        Location = objNDE005.Location,
                        RevNo = 0,
                        LineRevNo = 0,
                        Status = clsImplementationEnum.UTCTQStatus.DRAFT.GetStringValue(),
                        SpecificationNo = nde006.SpecificationNo,
                        SpecificationWithClauseNo = nde006.SpecificationWithClauseNo,
                        SpecificationRequirement = nde006.SpecificationRequirement,
                        CriticalToQuality = nde006.CriticalToQuality,
                        CTQRequirement = nde006.CTQRequirement,
                        ToBeDiscussed = nde006.ToBeDiscussed,
                        ScanningRequirement = nde006.ScanningRequirement,
                        ApprovalRequired = nde006.ApprovalRequired,
                        ProcedureDemonstration = nde006.ProcedureDemonstration,
                        Others = nde006.Others,
                        FeasibilityDone = nde006.FeasibilityDone,
                        Blocktobefabricated = nde006.Blocktobefabricated,
                        BlindTest = nde006.BlindTest,
                        BlockSubmitToCustomer = nde006.BlockSubmitToCustomer,
                        PAUTinAddition = nde006.PAUTinAddition,
                        SpecialQualification = nde006.SpecialQualification,
                        SpecialAccessories = nde006.SpecialAccessories,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    if (objNDE005.Status == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue())
                    {
                        objNDE005.RevNo = Convert.ToInt32(objNDE005.RevNo) + 1;
                        objNDE005.Status = clsImplementationEnum.UTCTQStatus.DRAFT.GetStringValue();
                        objNDE005.EditedBy = objClsLoginInfo.UserName;
                        objNDE005.EditedOn = DateTime.Now;
                        objNDE005.ReturnRemarks = null;
                        objNDE005.SubmittedBy = null;
                        objNDE005.SubmittedOn = null;
                        objNDE005.ApprovedOn = null;
                    }
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
                if (nde006.LineId > 0)
                {
                    objResponseMsg.Value = clsImplementationMessage.UTCTQMessages.Update;
                }
                else
                {
                    objResponseMsg.Value = clsImplementationMessage.UTCTQMessages.Insert;
                }
                objResponseMsg.Status = clsImplementationEnum.UTCTQStatus.DRAFT.GetStringValue();
                objResponseMsg.Remarks = objNDE005.ReturnRemarks;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteLine(int LineID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string deleted = clsImplementationEnum.CommonStatus.Deleted.GetStringValue(); //, clsImplementationEnum.UTCTQStatus.Returned.GetStringValue() };
                NDE006 objNDE006 = db.NDE006.Where(x => x.LineId == LineID).FirstOrDefault();
                if (objNDE006 != null)
                {
                    if (objNDE006.HeaderId > 0)
                    {
                        if (!IsapplicableForSubmit(objNDE006.HeaderId))
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "This record has been already submitted, Please refresh page.";
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }
                    }
                    if (objNDE006.NDE005.RevNo > 0)
                    {
                        objNDE006.Status = deleted;
                    }
                    else
                    {
                        db.NDE006.Remove(objNDE006);
                    }
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details successfully deleted.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Send for approval
        [HttpPost]
        public ActionResult SendForApproval(int HeaderId)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                if (HeaderId > 0)
                {
                    if (!IsapplicableForSubmit(HeaderId))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This record has been already submitted, Please refresh page.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                }
                string[] arrLineStatus = { clsImplementationEnum.UTCTQStatus.DRAFT.GetStringValue(), clsImplementationEnum.UTCTQStatus.Returned.GetStringValue() };
                List<NDE006> lstNDE006 = db.NDE006.Where(x => x.HeaderId == HeaderId && arrLineStatus.Contains(x.Status)).ToList();
                List<NDE006> IsNullEntry = db.NDE006.Where(x => x.HeaderId == HeaderId && (x.ApplicableForAScan == null || x.ApplicableForToFD == null ||
                                                              x.ApplicableForPAUT == null || x.SpecificationNo == null || x.SpecificationWithClauseNo == null ||
                                                              x.SpecificationRequirement == null || x.CriticalToQuality == null)).ToList();

                if (!IsNullEntry.Any())
                {
                    if (lstNDE006 != null && lstNDE006.Count > 0)
                    {
                        foreach (var objData in lstNDE006)
                        {
                            NDE006 objNDE006 = lstNDE006.Where(x => x.LineId == objData.LineId).FirstOrDefault();
                            //  objNDE006.Status = clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue();
                            objNDE006.EditedBy = objClsLoginInfo.UserName;
                            objNDE006.EditedOn = DateTime.Now;
                            objNDE006.SubmittedBy = objClsLoginInfo.UserName;
                            objNDE006.SubmittedOn = DateTime.Now;
                        }
                    }
                    NDE005 objNDE003 = db.NDE005.Where(x => x.HeaderId == HeaderId && arrLineStatus.Contains(x.Status)).FirstOrDefault();
                    if (objNDE003 != null)
                    {
                        objNDE003.Status = clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue();
                        objNDE003.SubmittedBy = objClsLoginInfo.UserName;
                        objNDE003.SubmittedOn = DateTime.Now;
                        db.SaveChanges();

                        #region Send Notification
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE2.GetStringValue(), objNDE003.Project, objNDE003.BU, objNDE003.Location, "CTQ: " + objNDE003.CTQNo + " has come for your Approval", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/NDE/ApproveUTCTQ/UTCTQLineDetails?HeaderID=" + objNDE003.HeaderId);
                        #endregion

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Details successfully sent for approval.";
                    }
                    else
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Details not available for approval.";
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "info";
                    objResponseMsg.Remarks = string.Format(clsImplementationMessage.UTCTQMessages.NullRecord, "UT CTQ Details");
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        private bool IsapplicableForSubmit(int headerId)
        {
            NDE005 objNDE005 = db.NDE005.Where(x => x.HeaderId == headerId).FirstOrDefault();
            if (objNDE005 != null)
            {
                if (objNDE005.Status == clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue() || objNDE005.Status == clsImplementationEnum.PTMTCTQStatus.Returned.GetStringValue())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return true;
            
        }

        private bool IsapplicableForSave(int headerId)
        {
            NDE005 objNDE005 = db.NDE005.Where(x => x.HeaderId == headerId).FirstOrDefault();
            if (objNDE005 != null)
            {
                if (objNDE005.Status == clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue())
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return true;
           
        }

        #endregion

        #region Copy
        [HttpPost]
        public ActionResult GetCopyDataFormPartial()
        {
            //List<Projects> project = Manager.getProjectsByUser(objClsLoginInfo.UserName);
            //ViewBag.Project = new SelectList(project, "projectCode", "projectDescription");
            return PartialView("_GetCopyDataFormPartial");
        }

        [HttpPost]
        public ActionResult copyUTCTQ(string projCode, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string currentUser = objClsLoginInfo.UserName;

                var currentLoc = (from a in db.COM003
                                  join b in db.COM002 on a.t_loca equals b.t_dimx
                                  where b.t_dtyp == 1 && a.t_actv == 1
                                  && a.t_psno.Equals(currentUser, StringComparison.OrdinalIgnoreCase)
                                  select b.t_dimx).FirstOrDefault().ToString();


                NDE005 objNDE005 = db.NDE005.Where(x => x.Project == projCode && x.Location == currentLoc).FirstOrDefault();
                NDE005 objExistingNDE005 = db.NDE005.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                if (objNDE005 != null)
                {
                    #region OldCode
                    //if (objNDE005.Location == currentLoc)
                    //{
                    //    if (objNDE005.Status == clsImplementationEnum.UTCTQStatus.DRAFT.GetStringValue())
                    //    {
                    //        objResponseMsg = InsertNewUTCTQ(objExistingNDE005, objNDE005, true);
                    //    }
                    //    else
                    //    {
                    //        objResponseMsg.Key = false;
                    //        objResponseMsg.Value = "UT CTQ already exists";
                    //    }
                    //}
                    //else
                    //{
                    //    NDE005 objDesNDE005 = new NDE005();
                    //    string BU = db.COM001.Where(i => i.t_cprj == projCode).FirstOrDefault().t_entu;
                    //    objDesNDE005.Project = projCode;
                    //    objDesNDE005.BU = BU;
                    //    objDesNDE005.Location = currentLoc;
                    //    objDesNDE005.CTQNo = projCode + "_CTQ_UT";
                    //    objResponseMsg = InsertNewUTCTQ(objExistingNDE005, objDesNDE005, false);
                    //}
                    #endregion

                    #region NewCode
                    if (objNDE005.Status == clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue() ||
                        objNDE005.Status == clsImplementationEnum.PTMTCTQStatus.Returned.GetStringValue())
                    {
                        objResponseMsg = InsertNewUTCTQ(objExistingNDE005, objNDE005, true);
                    }
                    else if (objNDE005.Status == clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue() ||
                        objNDE005.Status == clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "UT CTQ already exists";
                    }
                    #endregion
                }
                else
                {
                    NDE005 objDesNDE005 = new NDE005();
                    string BU = db.COM001.Where(i => i.t_cprj == projCode).FirstOrDefault().t_entu;
                    objDesNDE005.Project = projCode;
                    objDesNDE005.BU = BU;
                    objDesNDE005.Location = currentLoc;
                    objDesNDE005.CTQNo = projCode + "_CTQ_UT";
                    objResponseMsg = InsertNewUTCTQ(objExistingNDE005, objDesNDE005, false);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponseMsgWithStatus InsertNewUTCTQ(NDE005 objSrcNDE005, NDE005 objDesNDE005, bool IsEdited)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();

            try
            {
                if (IsEdited)
                {
                    //If Record exist for destination project then no need to change header table confirm by satish (On 26-03-2017 11:40 AM). 
                    //So Comment OldCode For Update Header
                    #region OldCode For Update Header
                    //objDesNDE005.ApplicableSpecification = objSrcNDE005.ApplicableSpecification;
                    //objDesNDE005.AcceptanceStandard = objSrcNDE005.AcceptanceStandard;
                    //objDesNDE005.ASMECodeStamp = objSrcNDE005.ASMECodeStamp;
                    //objDesNDE005.TPIName = objSrcNDE005.TPIName;
                    //objDesNDE005.NotificationRequired = objSrcNDE005.NotificationRequired;
                    //objDesNDE005.ThicknessOfJob = objSrcNDE005.ThicknessOfJob;
                    //objDesNDE005.SizeDimension = objSrcNDE005.SizeDimension;
                    //objDesNDE005.ManufacturingCode = objSrcNDE005.ManufacturingCode;
                    //objDesNDE005.NDERemarks = objSrcNDE005.NDERemarks;
                    objDesNDE005.EditedBy = objClsLoginInfo.UserName;
                    objDesNDE005.EditedOn = DateTime.Now;
                    #endregion

                    //Add Source Line item in destination project. So comment Remove Existing Data code (On 26-03-2017 11:00 AM)
                    #region Remove Existing Data
                    //List<NDE006> lstDesNDE006 = db.NDE006.Where(x => x.HeaderId == objDesNDE005.HeaderId).ToList();
                    //if (lstDesNDE006 != null && lstDesNDE006.Count > 0)
                    //{
                    //    db.NDE006.RemoveRange(lstDesNDE006);
                    //}
                    #endregion

                    List<NDE006> lstSrcNDE006 = db.NDE006.Where(x => x.HeaderId == objSrcNDE005.HeaderId).ToList();

                    if (lstSrcNDE006 != null && lstSrcNDE006.Count > 0)
                    {
                        db.NDE006.AddRange(
                                            lstSrcNDE006.Select(x =>
                                            new NDE006
                                            {
                                                HeaderId = objDesNDE005.HeaderId,
                                                Project = objDesNDE005.Project,
                                                BU = objDesNDE005.BU,
                                                Location = objDesNDE005.Location,
                                                RevNo = Convert.ToInt32(objDesNDE005.RevNo),
                                                LineRevNo = 0,
                                                Status = clsImplementationEnum.UTCTQStatus.DRAFT.GetStringValue(),
                                                ApplicableForAScan = x.ApplicableForAScan,
                                                ApplicableForPAUT = x.ApplicableForPAUT,
                                                ApplicableForToFD = x.ApplicableForToFD,
                                                Description = x.Description,
                                                SpecificationNo = x.SpecificationNo,
                                                SpecificationWithClauseNo = x.SpecificationWithClauseNo,
                                                ToBeDiscussed = x.ToBeDiscussed,
                                                SpecificationRequirement = x.SpecificationRequirement,
                                                CriticalToQuality = x.CriticalToQuality,
                                                CTQRequirement = x.CTQRequirement,
                                                ReturnRemark = x.ReturnRemark,
                                                CreatedBy = objClsLoginInfo.UserName,
                                                CreatedOn = DateTime.Now
                                            })
                                          );
                    }

                    db.SaveChanges();
                    objResponseMsg.HeaderId = objDesNDE005.HeaderId;
                    objResponseMsg.HeaderStatus = objDesNDE005.CTQNo;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.UTCTQMessages.Update.ToString() + " To Document No : " + objResponseMsg.HeaderStatus + " of " + objDesNDE005.Project + " Project.";
                }
                else
                {
                    objDesNDE005.ApplicableSpecification = objSrcNDE005.ApplicableSpecification;
                    objDesNDE005.AcceptanceStandard = objSrcNDE005.AcceptanceStandard;
                    objDesNDE005.ASMECodeStamp = objSrcNDE005.ASMECodeStamp;
                    objDesNDE005.TPIName = objSrcNDE005.TPIName;
                    objDesNDE005.NotificationRequired = objSrcNDE005.NotificationRequired;
                    objDesNDE005.ManufacturingCode = objSrcNDE005.ManufacturingCode;
                    objDesNDE005.ThicknessOfJob = objSrcNDE005.ThicknessOfJob;
                    objDesNDE005.SizeDimension = objSrcNDE005.SizeDimension;
                    objDesNDE005.NDERemarks = objSrcNDE005.NDERemarks;
                    objDesNDE005.Status = clsImplementationEnum.UTCTQStatus.DRAFT.GetStringValue();
                    objDesNDE005.RevNo = 0;
                    objDesNDE005.CreatedBy = objClsLoginInfo.UserName;
                    objDesNDE005.CreatedOn = DateTime.Now;
                    db.NDE005.Add(objDesNDE005);
                    db.SaveChanges();
                    //List<NDE006> lstDesNDE005 = db.NDE006.Where(x => x.HeaderId == objDesNDE005.HeaderId).ToList();
                    //if (lstDesNDE005 != null && lstDesNDE005.Count > 0)
                    //{
                    //    db.NDE006.RemoveRange(lstDesNDE005);
                    //}
                    List<NDE006> lstSrcNDE006 = db.NDE006.Where(x => x.HeaderId == objSrcNDE005.HeaderId).ToList();
                    if (lstSrcNDE006 != null && lstSrcNDE006.Count > 0)
                    {
                        db.NDE006.AddRange(
                                            lstSrcNDE006.Select(x =>
                                            new NDE006
                                            {
                                                HeaderId = objDesNDE005.HeaderId,
                                                Project = objDesNDE005.Project,
                                                BU = objDesNDE005.BU,
                                                Location = objDesNDE005.Location,
                                                RevNo = Convert.ToInt32(objDesNDE005.RevNo),
                                                LineRevNo = 0,
                                                Status = clsImplementationEnum.UTCTQStatus.DRAFT.GetStringValue(),
                                                ApplicableForAScan = x.ApplicableForAScan,
                                                ApplicableForPAUT = x.ApplicableForPAUT,
                                                ApplicableForToFD = x.ApplicableForToFD,
                                                Description = x.Description,
                                                SpecificationNo = x.SpecificationNo,
                                                SpecificationWithClauseNo = x.SpecificationWithClauseNo,
                                                ToBeDiscussed = x.ToBeDiscussed,
                                                SpecificationRequirement = x.SpecificationRequirement,
                                                CriticalToQuality = x.CriticalToQuality,
                                                CTQRequirement = x.CTQRequirement,
                                                ReturnRemark = x.ReturnRemark,
                                                CreatedBy = objClsLoginInfo.UserName,
                                                CreatedOn = DateTime.Now
                                            })
                                          );
                    }
                    db.SaveChanges();
                    objResponseMsg.HeaderId = objDesNDE005.HeaderId;
                    objResponseMsg.HeaderStatus = objDesNDE005.CTQNo;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.UTCTQMessages.Insert.ToString() + " To Document No : " + objResponseMsg.HeaderStatus + " of " + objDesNDE005.Project + " Project.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return objResponseMsg;
        }
        #endregion 

        #region History
        [HttpPost]
        public ActionResult GetHistoryDataPartial(int HeaderID)
        {
            ViewBag.HeaderID = HeaderID;
            return PartialView("_GetHistoryDataPartial");
        }

        [HttpPost]
        public JsonResult LoadUTCTQHeaderHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (ltrim(rtrim(nde5.BU))+'-'+bcom2.t_desc like '%" + param.sSearch +
                                "%' or ltrim(rtrim(nde5.Location))+'-'+lcom2.t_desc like '%" + param.sSearch +
                                "%' or (nde5.Project+'-'+com1.t_dsca) like '%" + param.sSearch +
                                "%' or CTQNo like '%" + param.sSearch +
                                "%' or RevNo like '%" + param.sSearch +
                                "%' or ApplicableSpecification like '%" + param.sSearch +
                                "%' or AcceptanceStandard like '%" + param.sSearch +
                                "%' or ASMECodeStamp like '%" + param.sSearch +
                                "%' or TPIName like '%" + param.sSearch +
                                "%' or NotificationRequired like '%" + param.sSearch +
                                "%' or ThicknessOfJob like '%" + param.sSearch +
                                "%' or SizeDimension like '%" + param.sSearch +
                                "%' or NDERemarks like '%" + param.sSearch +
                                "%' or nde5.CreatedBy +' - '+com003c.t_name like '%" + param.sSearch +
                                "%' or ManufacturingCode like '%" + param.sSearch +
                                "%' or Status like '%" + param.sSearch + "%')";
                }
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "nde5.BU", "nde5.Location");

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_UTCTQ_GETHEADER_HISTORY
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.BU),
                               Convert.ToString(uc.Location),
                               Convert.ToString(uc.CTQNo),
                               Convert.ToString(uc.ManufacturingCode),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn !=null?(Convert.ToDateTime(uc.CreatedOn)).ToString("dd/MM/yyyy"):""),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.ApplicableSpecification),
                               Convert.ToString(uc.AcceptanceStandard),
                               (uc.ASMECodeStamp == true?"<label class=\"mt-checkbox mt-checkbox-outline\"><input checked=\"checked\" disabled=\"disabled\" type = \"checkbox\" value=\"\" /><span></span></label>":"<label class=\"mt-checkbox mt-checkbox-outline\"><input disabled=\"disabled\" type = \"checkbox\" value=\"\" /><span></span></label>"),//Convert.ToString(uc.ASMECodeStamp),
                               Convert.ToString(uc.TPIName),
                               Convert.ToString(uc.NotificationRequired == true ? "Yes" : uc.NotificationRequired == false ? "No" : ""),
                               Convert.ToString(uc.ThicknessOfJob),
                               Convert.ToString(uc.SizeDimension),
                               Convert.ToString(uc.NDERemarks),
                               "<center style=\"display:inline-flex;\"><a title='View' href='"+WebsiteURL+"/NDE/MaintainUTCTQ/GetUTCTQLineHistory?ID="+Convert.ToInt32(uc.Id)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a><a title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/NDE/MaintainUTCTQ/ShowTimeline?HeaderID="+Convert.ToInt32(uc.Id)+"&LineId=0&isHistory=true');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a></center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]
        public ActionResult GetUTCTQLineHistory(int ID = 0)
        {
            NDEModels objNDEModels = new NDEModels();
            if (ID > 0)
            {
                ID = Convert.ToInt32(ID);
            }
            NDE005_Log objNDE005_Log = db.NDE005_Log.Where(x => x.Id == ID).FirstOrDefault();
            if (objNDE005_Log != null)
            {
                ViewBag.HeaderID = objNDE005_Log.HeaderId;
                ViewBag.Status = objNDE005_Log.Status;
                ViewBag.CTQNo = objNDE005_Log.CTQNo;
                var ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objNDE005_Log.ManufacturingCode, objNDE005_Log.BU, objNDE005_Log.Location, true);
                var projectDetails = db.COM001.Where(x => x.t_cprj == objNDE005_Log.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                objNDE005_Log.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                var BUDescription = db.COM002.Where(x => x.t_dimx == objNDE005_Log.BU).Select(x => x.t_desc).FirstOrDefault();
                objNDE005_Log.BU = Convert.ToString(objNDE005_Log.BU + " - " + BUDescription);
                var Location = db.COM002.Where(x => x.t_dimx == objNDE005_Log.Location).Select(x => x.t_desc).FirstOrDefault();
                objNDE005_Log.Location = Convert.ToString(objNDE005_Log.Location + " - " + Location);

                if (ManufacturingCode != null && !string.IsNullOrWhiteSpace(ManufacturingCode.Code) && !string.IsNullOrWhiteSpace(ManufacturingCode.CategoryDescription))
                {
                    ViewBag.ManufactCode = ManufacturingCode.CategoryDescription;
                }
            }
            return View(objNDE005_Log);
        }

        [HttpPost]
        public ActionResult GetCTQLinesHistoryHtml(int ID, string strCTQNo)
        {
            ViewBag.ID = ID;
            ViewBag.strCTQNo = strCTQNo;
            ViewBag.CTQStatus = db.NDE005_Log.Where(x => x.Id == ID).Select(x => x.Status).FirstOrDefault();
            return PartialView("_GetCTQLinesHistoryHtml");
        }

        [HttpPost]
        public JsonResult LoadUTCTQLineHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "1=1 " + Manager.MakeIntInCondition("RefId", param.CTQHeaderId).ToString();
                int ndeHisId = Convert.ToInt32(param.CTQHeaderId);
                NDE005_Log objNDE005_Log = db.NDE005_Log.FirstOrDefault(x => x.Id == ndeHisId);
                string[] columnName = { "Description", "SpecificationNo", "SpecificationWithClauseNo", "SpecificationRequirement", "ToBeDiscussed", "CriticalToQuality", "CTQRequirement" };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);

                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName);
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_GET_UT_HEADER_LINES_HISTORY
                                (
                               StartIndex,
                               EndIndex,
                               strSortOrder,
                               strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.ROW_NO),
                            Convert.ToString(uc.Description),
                            Convert.ToString(uc.ApplicableForAScan),
                            Convert.ToString(uc.ApplicableForToFD),
                            Convert.ToString(uc.ApplicableForPAUT),
                            Convert.ToString(uc.SpecificationNo),
                            Convert.ToString(uc.SpecificationWithClauseNo),
                            Convert.ToString(uc.ToBeDiscussed==true ? "Yes":uc.ToBeDiscussed==false ?"No":""),
                            Convert.ToString(uc.SpecificationRequirement),
                            Convert.ToString(uc.CriticalToQuality==true ? "Yes":uc.CriticalToQuality==false ?"No":""),
                            Convert.ToString(uc.CTQRequirement),
                            Convert.ToString(uc.ReturnRemark),
                            Convert.ToString(uc.Status),
                            Convert.ToString(uc.LineId),
                            Convert.ToString(uc.HeaderId),
                            "<center style=\"display:inline;\"><a class='' href='javascript:void(0)' onclick=ShowTimeline('/NDE/MaintainUTCTQ/ShowTimeline?HeaderID="+Convert.ToInt32(uc.HeaderId)+"&LineId="+Convert.ToInt32(uc.Id)+"&isHistory=true');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a></center>",
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere

                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Common

        [HttpPost]
        public bool checkProjectExist(string projectcode)
        {
            bool Flag = false;
            if (!string.IsNullOrWhiteSpace(projectcode))
            {
                NDE005 objNDE005 = db.NDE005.Where(x => x.Project == projectcode).FirstOrDefault();
                if (objNDE005 != null)
                {
                    Flag = true;
                }
            }
            return Flag;
        }

        [HttpPost]
        public ActionResult GetProjectWiseBU(string projectcode)
        {
            ProjectDataModel objProjectDataModel = new ProjectDataModel();
            if (!string.IsNullOrWhiteSpace(projectcode))
            {
                string BU = db.COM001.Where(i => i.t_cprj == projectcode).FirstOrDefault().t_entu;
                var BUDescription = (from a in db.COM002
                                     where a.t_dtyp == 2 && a.t_dimx == BU
                                     select new ProjectDataModel { BUDescription = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
                objProjectDataModel.ManufacturingCode = Manager.getManufacturingCode(null, projectcode);
                objProjectDataModel.BUDescription = BUDescription.BUDescription;
                return Json(objProjectDataModel, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_UTCTQ_GETHEADER(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      RevNo = li.RevNo,
                                      CTQNo = li.CTQNo,
                                      Status = li.Status,
                                      ApplicableSpecification = li.ApplicableSpecification,
                                      AcceptanceStandard = li.AcceptanceStandard,
                                      ASMECodeStamp = li.ASMECodeStamp,
                                      TPIName = li.TPIName,
                                      NotificationRequired = li.NotificationRequired,
                                      ThicknessOfJob = li.ThicknessOfJob,
                                      SizeDimension = li.SizeDimension,
                                      ManufacturingCode = li.ManufacturingCode,
                                      NDERemarks = li.NDERemarks,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn
                                  }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_GET_UT_HEADER_LINES(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      SrNo = Convert.ToString(uc.ROW_NO),
                                      Description = Convert.ToString(uc.Description),
                                      ApplicableForAScan = Convert.ToString(uc.ApplicableForAScan),
                                      ApplicableForToFD = Convert.ToString(uc.ApplicableForToFD),
                                      ApplicableForPAUT = Convert.ToString(uc.ApplicableForPAUT),
                                      SpecificationNo = Convert.ToString(uc.SpecificationNo),
                                      SpecificationWithClauseNo = Convert.ToString(uc.SpecificationWithClauseNo),
                                      ToBeDiscussed = Convert.ToString(uc.ToBeDiscussed == true ? "Yes" : uc.ToBeDiscussed == false ? "No" : ""),
                                      SpecificationRequirement = Convert.ToString(uc.SpecificationRequirement),
                                      CriticalToQuality = Convert.ToString(uc.CriticalToQuality == true ? "Yes" : uc.CriticalToQuality == false ? "No" : ""),
                                      CTQRequirement = Convert.ToString(uc.CTQRequirement),
                                      ReturnRemark = Convert.ToString(uc.ReturnRemark),
                                      Status = Convert.ToString(uc.Status),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {
                    var lst = db.SP_UTCTQ_GETHEADER_HISTORY(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      RevNo = li.RevNo,
                                      CTQNo = li.CTQNo,
                                      Status = li.Status,
                                      ManufacturingCode = li.ManufacturingCode,
                                      ApplicableSpecification = li.ApplicableSpecification,
                                      AcceptanceStandard = li.AcceptanceStandard,
                                      ASMECodeStamp = li.ASMECodeStamp,
                                      TPIName = li.TPIName,
                                      NotificationRequired = li.NotificationRequired,
                                      ThicknessOfJob = li.ThicknessOfJob,
                                      SizeDimension = li.SizeDimension,
                                      NDERemarks = li.NDERemarks,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else
                {
                    var lst = db.SP_GET_UT_HEADER_LINES_HISTORY(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      SrNo = Convert.ToString(uc.ROW_NO),
                                      Description = Convert.ToString(uc.Description),
                                      ApplicableForAScan = Convert.ToString(uc.ApplicableForAScan),
                                      ApplicableForToFD = Convert.ToString(uc.ApplicableForToFD),
                                      ApplicableForPAUT = Convert.ToString(uc.ApplicableForPAUT),
                                      SpecificationNo = Convert.ToString(uc.SpecificationNo),
                                      SpecificationWithClauseNo = Convert.ToString(uc.SpecificationWithClauseNo),
                                      ToBeDiscussed = Convert.ToString(uc.ToBeDiscussed == true ? "Yes" : uc.ToBeDiscussed == false ? "No" : ""),
                                      SpecificationRequirement = Convert.ToString(uc.SpecificationRequirement),
                                      CriticalToQuality = Convert.ToString(uc.CriticalToQuality == true ? "Yes" : uc.CriticalToQuality == false ? "No" : ""),
                                      CTQRequirement = Convert.ToString(uc.CTQRequirement),
                                      ReturnRemark = Convert.ToString(uc.ReturnRemark),
                                      Status = Convert.ToString(uc.Status),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }


                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ShowTimeline(int HeaderId, int LineId = 0, bool isHistory = false)
        {
            TimelineViewModel model = new TimelineViewModel();
            if (isHistory)
            {
                if (LineId > 0)
                {
                    NDE006_Log objNDE006 = db.NDE006_Log.Where(x => x.Id == LineId).FirstOrDefault();
                    model.Title = "NDECTQ Lines";
                    model.ApprovedBy = Manager.GetUserNameFromPsNo(objNDE006.ApprovedBy);
                    model.ApprovedOn = objNDE006.ApprovedOn;
                    model.SubmittedBy = Manager.GetUserNameFromPsNo(objNDE006.SubmittedBy);
                    model.SubmittedOn = objNDE006.SubmittedOn;
                    model.CreatedBy = Manager.GetUserNameFromPsNo(objNDE006.CreatedBy);
                    model.CreatedOn = objNDE006.CreatedOn;
                    model.ReturnedBy = Manager.GetUserNameFromPsNo(objNDE006.ReturnedBy);
                    model.ReturnedOn = objNDE006.ReturnedOn;
                }
                else
                {
                    NDE005_Log objNDE005 = db.NDE005_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                    model.Title = "NDECTQ Header";
                    model.ApprovedBy = Manager.GetUserNameFromPsNo(objNDE005.ApprovedBy);
                    model.ApprovedOn = objNDE005.ApprovedOn;
                    model.SubmittedBy = Manager.GetUserNameFromPsNo(objNDE005.SubmittedBy);
                    model.SubmittedOn = objNDE005.SubmittedOn;
                    model.CreatedBy = Manager.GetUserNameFromPsNo(objNDE005.CreatedBy);
                    model.CreatedOn = objNDE005.CreatedOn;
                }
            }
            else
            {
                if (LineId > 0)
                {
                    NDE006 objNDE006 = db.NDE006.Where(x => x.LineId == LineId).FirstOrDefault();
                    model.Title = "NDECTQ Lines";
                    model.ApprovedBy = Manager.GetUserNameFromPsNo(objNDE006.ApprovedBy);
                    model.ApprovedOn = objNDE006.ApprovedOn;
                    model.SubmittedBy = Manager.GetUserNameFromPsNo(objNDE006.SubmittedBy);
                    model.SubmittedOn = objNDE006.SubmittedOn;
                    model.CreatedBy = Manager.GetUserNameFromPsNo(objNDE006.CreatedBy);
                    model.CreatedOn = objNDE006.CreatedOn;
                    model.ReturnedBy = Manager.GetUserNameFromPsNo(objNDE006.ReturnedBy);
                    model.ReturnedOn = objNDE006.ReturnedOn;
                }
                else
                {
                    NDE005 objNDE005 = db.NDE005.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    model.Title = "NDECTQ Header";
                    model.ApprovedBy = Manager.GetUserNameFromPsNo(objNDE005.ApprovedBy);
                    model.ApprovedOn = objNDE005.ApprovedOn;
                    model.SubmittedBy = Manager.GetUserNameFromPsNo(objNDE005.SubmittedBy);
                    model.SubmittedOn = objNDE005.SubmittedOn;
                    model.CreatedBy = Manager.GetUserNameFromPsNo(objNDE005.CreatedBy);
                    model.CreatedOn = objNDE005.CreatedOn;
                }
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }

        #endregion
    }
}