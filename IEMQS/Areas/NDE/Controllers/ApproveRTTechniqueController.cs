﻿using IEMQS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQS.Areas.NDE.Models;

namespace IEMQS.Areas.NDE.Controllers
{
    public class ApproveRTTechniqueController : clsBase
    {
        // GET: NDE/ApproveRTTechnique
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult ViewRTTechnique(int headerID)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("NDE009");
            NDE009 objNDE009 = new NDE009();
            if (headerID > 0)
            {
                objNDE009 = db.NDE009.Where(i => i.HeaderId == headerID).FirstOrDefault();
                var QMSProject = new NDEModels().GetQMSProject(objNDE009.QualityProject);
                NDEModels objNDEModels = new NDEModels();

                ViewBag.QMSProject = QMSProject.QualityProject + " - " + QMSProject.ManufacturingCode;//db.TMP001.Where(i => i.QualityProject == objNDE009.QualityProject).Select(i => i.QualityProject + " - " + i.ManufacturingCode).FirstOrDefault();
                ViewBag.Project = db.COM001.Where(i => i.t_cprj == objNDE009.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                ViewBag.BU = db.COM002.Where(i => i.t_dtyp == 2 && i.t_dimx == objNDE009.BU).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
                ViewBag.Location = db.COM002.Where(i => i.t_dtyp == 1 && i.t_dimx == objNDE009.Location).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault(); ;
                ViewBag.MfgCode = objNDEModels.GetQMSProject(objNDE009.QualityProject).ManufacturingCode;
                ViewBag.Technique = objNDEModels.GetCategory("RT Technique", objNDE009.Technique, objNDE009.BU, objNDE009.Location, true).CategoryDescription;
                ViewBag.SourceOfRadiation = objNDEModels.GetCategory("Source of Radiation", objNDE009.SourceofRadiation, objNDE009.BU, objNDE009.Location, true).CategoryDescription;
                ViewBag.Film = objNDEModels.GetCategory("Film", objNDE009.Film, objNDE009.BU, objNDE009.Location, true).CategoryDescription;
                ViewBag.IQI = objNDEModels.GetCategory("IQI", objNDE009.IQI, objNDE009.BU, objNDE009.Location, true).CategoryDescription;
                ViewBag.ExposureDetail = objNDEModels.GetCategory("Exposure Detail", objNDE009.ExposureDetail, objNDE009.BU, objNDE009.Location, true).CategoryDescription;
                ViewBag.Weld = objNDEModels.GetCategory("Weld", objNDE009.Weld, objNDE009.BU, objNDE009.Location, true).CategoryDescription;
                ViewBag.GammaRaySource = objNDEModels.GetCategory("Gamma Ray Source", objNDE009.GammaRaySource, objNDE009.BU, objNDE009.Location, true).CategoryDescription;
                ViewBag.IntensifyingScreens = objNDEModels.GetCategory("Intensifying Screens", objNDE009.IntensifyingScreen, objNDE009.BU, objNDE009.Location, true).CategoryDescription;

                //ViewBag.IQIHoleType = objNDEModels.GetCategory("IQI Hole Type", objNDE009.IQIHoleType, objNDE009.BU, objNDE009.Location, true).CategoryDescription;
                //ViewBag.IQIWireType = objNDEModels.GetCategory("IQI Wire Type", objNDE009.IQIWireType, objNDE009.BU, objNDE009.Location, true).CategoryDescription;
                ViewBag.FilmBrand = objNDEModels.GetCategory("Film Branding", objNDE009.FilmBrand, objNDE009.BU, objNDE009.Location, true).CategoryDescription;
                ViewBag.FilmViewing = objNDEModels.GetCategory("Film Viewing", objNDE009.FilmViewing, objNDE009.BU, objNDE009.Location, true).CategoryDescription;
            }
            return View(objNDE009);
        }

        public ActionResult GetApproveRTHeaderGridDataPartial(string status)
        {
            ViewBag.status = status;
            return PartialView("_GetApproveRTHeaderGridDataPartial");
        }

        public ActionResult LoadHeaderDataTable(JQueryDataTableParamModel param, string status)
        {
            try
            {

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;


                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "nde9.BU", "nde9.Location");
                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and nde9.Status in('" + clsImplementationEnum.QCPStatus.SentForApproval.GetStringValue() + "')";
                }
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += " and (ltrim(rtrim(nde9.BU))+'-'+bcom2.t_desc like '%" + param.sSearch + "%' or ltrim(rtrim(nde9.Location))+'-'+lcom2.t_desc like '%" + param.sSearch + "%' " +
                                            " or nde9.QualityProject like '%" + param.sSearch + "%' or (nde9.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%' " +
                                            " or RTTechNo like '%" + param.sSearch + "%' or RevNo like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%' " +
                                            " or TechniqueSheetNo like '%" + param.sSearch + "%' or (ecom3.t_psno+'-'+ecom3.t_name) like '%" + param.sSearch + "%')";
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstHeader = db.SP_NDE_GetRTTechniqueHeader(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstHeader
                          select new[] {
                              Convert.ToString(a.HeaderId),
                              Convert.ToString(a.QualityProject),
                              Convert.ToString(a.Project),
                              Convert.ToString(a.BU),
                              Convert.ToString(a.Location),
                              Convert.ToString(a.TechniqueSheetNo),
                              Convert.ToString(a.RTTechNo),
                              Convert.ToString("R" + a.RevNo),
                              a.CreatedBy,
                              Convert.ToString(a.CreatedOn !=null?(Convert.ToDateTime(a.CreatedOn)).ToString("dd/MM/yyyy"):""),
                              a.Status,
                              "<center><a href=\""+WebsiteURL +"/NDE/ApproveRTTechnique/ViewRTTechnique?headerID=" + a.HeaderId + "\" title=\"View\"><i style=\"margin-left:5px;\" class=\"fa fa-eye\"></i></a><a title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/NDE/MaintainRTTechnique/ShowTimeline?HeaderID="+Convert.ToInt32(a.HeaderId)+"');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a><a title='Print' href='javascript:void(0)' onclick='PrintReport("+Convert.ToInt32(a.HeaderId)+",\""+a.Status+"\");'><i style='margin-left:10px;' " +
                                "class='fa fa-print'></i></a></center>"
                              //Convert.ToString(a.HeaderId)
                              //a.Status,
            };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult ReturnHeader(int headerId, string returnRemarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            NDE009 objNDE009 = new NDE009();
            try
            {
                if (!IsapplicableForAttend(headerId))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "This record has been already attended, Please refresh page.";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                if (headerId > 0)
                {
                    objNDE009 = db.NDE009.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    if (objNDE009 != null)
                    {
                        objNDE009.Status = clsImplementationEnum.NDETechniqueStatus.Returned.GetStringValue();
                        objNDE009.ReturnRemarks = returnRemarks;
                        objNDE009.ReturnedBy = objClsLoginInfo.UserName;
                        objNDE009.ReturnedOn = DateTime.Now;
                        db.SaveChanges();

                        #region Send Notification
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE3.GetStringValue(), objNDE009.Project, objNDE009.BU, objNDE009.Location, "Technique No: " + objNDE009.RTTechNo + " has been Returned", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/NDE/MaintainRTTechnique/AddRTTechnique?headerId=" + objNDE009.HeaderId);
                        #endregion

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.PTTechniqueMeassage.Return.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Header Found";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg);
        }

        [HttpPost]
        public ActionResult ApproveHeader(int headerID, string returnRemarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            NDE009 objNDE009 = new NDE009();

            try
            {
                if (!IsapplicableForAttend(headerID))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "This record has been already attended, Please refresh page.";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                if (headerID > 0)
                {
                    objNDE009 = db.NDE009.Where(i => i.HeaderId == headerID).FirstOrDefault();
                    if (objNDE009 != null)
                    {
                        objNDE009.ReturnRemarks = returnRemarks;
                        db.SP_NDE_ApproveRTHeader(headerID, objClsLoginInfo.UserName);
                        db.SaveChanges();
                        #region Send Notification
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE3.GetStringValue(), objNDE009.Project, objNDE009.BU, objNDE009.Location, "Technique No: " + objNDE009.RTTechNo + " has been Approved", clsImplementationEnum.NotificationType.Information.GetStringValue());
                        #endregion

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.PTTechniqueMeassage.Approve.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Header Not Found";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg);
        }

        private bool IsapplicableForAttend(int headerId)
        {
            NDE009 objNDE009 = db.NDE009.Where(x => x.HeaderId == headerId).FirstOrDefault();
            if (objNDE009 != null)
            {
                if (objNDE009.Status == clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        [HttpPost]
        public ActionResult ApproveSelectedTechnique(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (!string.IsNullOrEmpty(strHeaderIds))
                {
                    string[] arrstrHeaderIds = strHeaderIds.Split(',');
                    for (int i = 0; i < arrstrHeaderIds.Length; i++)
                    {
                        int headerId = Convert.ToInt32(arrstrHeaderIds[i]);
                        db.SP_NDE_ApproveRTHeader(headerId, objClsLoginInfo.UserName);

                        #region Send Notification
                        NDE009 objNDE009 = db.NDE009.Where(c => c.HeaderId == headerId).FirstOrDefault();
                        if (objNDE009 != null)
                        {
                            (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE3.GetStringValue(), objNDE009.Project, objNDE009.BU, objNDE009.Location, "Technique No: " + objNDE009.RTTechNo + " has been Approved", clsImplementationEnum.NotificationType.Information.GetStringValue());
                        }
                        #endregion
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PTTechniqueMeassage.Approve.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
    }
}