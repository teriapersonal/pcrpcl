﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.NDE.Controllers
{
    public class MaintainPTMTCTQController : clsBase
    {
        /// <summary>
        /// PT/MT CTQ 
        /// Added by Dharmesh Vasani
        /// Modified by Nikita 
        /// </summary>
        /// <returns></returns>

        #region Header index
        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadPTMTCTQListDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_PTMTCTQListDataPartial");
        }

        [HttpPost]
        public JsonResult LoadPTMTCTQHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.CTQStatus.DRAFT.GetStringValue() + "','" + clsImplementationEnum.CTQStatus.Returned.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "1=1";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (ltrim(rtrim(nde1.BU))+'-'+bcom2.t_desc like '%" + param.sSearch +
                                "%' or ltrim(rtrim(nde1.Location))+'-'+lcom2.t_desc like '%" + param.sSearch +
                                "%' or (nde1.Project+'-'+com1.t_dsca) like '%" + param.sSearch +
                                "%' or CTQNo like '%" + param.sSearch +
                                "%' or RevNo like '%" + param.sSearch +
                                "%' or ApplicableSpecification like '%" + param.sSearch +
                                "%' or AcceptanceStandard like '%" + param.sSearch +
                                "%' or ASMECodeStamp like '%" + param.sSearch +
                                "%' or TPIName like '%" + param.sSearch +
                                "%' or NotificationRequired like '%" + param.sSearch +
                                "%' or ThicknessOfJob like '%" + param.sSearch +
                                "%' or SizeDimension like '%" + param.sSearch +
                                "%' or NDERemarks like '%" + param.sSearch +
                                "%' or nde1.CreatedBy + ' - ' + com003c.t_name like '%" + param.sSearch +
                                "%' or Status like '%" + param.sSearch + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "nde1.BU", "nde1.Location");

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_PTMTCTQ_GETHEADER
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.BU),
                               Convert.ToString(uc.Location),
                               Convert.ToString(uc.CTQNo),
                               Convert.ToString(uc.ManufacturingCode),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn !=null?(Convert.ToDateTime(uc.CreatedOn)).ToString("dd/MM/yyyy"):""),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.ApplicableSpecification),
                               Convert.ToString(uc.AcceptanceStandard),
                               (uc.ASMECodeStamp == true?"<label class=\"mt-checkbox mt-checkbox-outline\"><input checked=\"checked\" disabled=\"disabled\" type = \"checkbox\" value=\"\" /><span></span></label>":"<label class=\"mt-checkbox mt-checkbox-outline\"><input disabled=\"disabled\" type = \"checkbox\" value=\"\" /><span></span></label>"),//Convert.ToString(uc.ASMECodeStamp),
                               Convert.ToString(uc.TPIName),
                               Convert.ToString(uc.NotificationRequired==true ? "Yes":uc.NotificationRequired == false ? "No" : ""),
                               Convert.ToString(uc.ThicknessOfJob),
                               Convert.ToString(uc.SizeDimension),
                               Convert.ToString(uc.NDERemarks),
                               "<nobr><center>"
                                   + Helper.GenerateActionIcon(uc.HeaderId, "View", "View Detail", "fa fa-eye", "",WebsiteURL + "/NDE/MaintainPTMTCTQ/AddUpdatePTMTCTQ?HeaderID="+uc.HeaderId,false)
                                    +(uc.Status.ToLower() == clsImplementationEnum.CommonStatus.DRAFT.GetStringValue().ToLower() && uc.RevNo == 0 ? "<i id=\"btnDelete\" name=\"btnAction\" style=\"cursor:pointer;\" Title=\"Delete Record\" class=\"iconspace fa fa-trash-o\" onclick=\"DeleteHeader('" + uc.HeaderId + "')\"></i>" : "<i id=\"btnDelete\" name=\"btnAction\" style=\"cursor:pointer;\" Title=\"Delete Record\" class=\"disabledicon fa fa-trash-o\" ></i>")+
                                    "<a title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/NDE/MaintainPTMTCTQ/ShowTimeline?HeaderID="+Convert.ToInt32(uc.HeaderId)+"');><i  class='iconspace fa fa-clock-o'></i></a>"+""+((uc.RevNo > 0 || uc.Status.ToLower() == clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue().ToLower()) ?"<i title='History' onclick=History('"+Convert.ToInt32(uc.HeaderId)+"'); class='iconspace fa fa-history'></i>" : "<i title='History' class='disabledicon fa fa-history'></i>" )+""+"</center></nobr>",
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        //delete header (in draft and R0)
        [HttpPost]
        public ActionResult DeleteHeader(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                NDE001 objNDE001 = db.NDE001.Where(x => x.HeaderId == headerId).FirstOrDefault();
                List<NDE002> objNDE002 = db.NDE002.Where(x => x.HeaderId == headerId).ToList();
                if (objNDE001 != null)
                {
                    if (objNDE002.Count > 0)
                    {
                        db.NDE002.RemoveRange(objNDE002);
                        db.SaveChanges();
                    }
                    db.NDE001.Remove(objNDE001);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        //check for submit for duplicate tab
        private bool IsapplicableForSubmit(int headerId)
        {
            NDE001 objNDE001 = db.NDE001.Where(x => x.HeaderId == headerId).FirstOrDefault();
            if (objNDE001 != null)
            {
                if (objNDE001.Status == clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue() || objNDE001.Status == clsImplementationEnum.PTMTCTQStatus.Returned.GetStringValue())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return true;
                
        }

       
        private bool IsapplicableForSave(int headerId)
        {
            NDE001 objNDE001 = db.NDE001.Where(x => x.HeaderId == headerId).FirstOrDefault();
            if (objNDE001 != null)
            {
                if (objNDE001.Status == clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue() || objNDE001.Status == clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue())
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return true;
        }


        #region Add/Update form
        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult AddUpdatePTMTCTQ(int HeaderID = 0)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("NDE001");
            ViewBag.HeaderID = HeaderID;
            return View();
        }

        [HttpPost]
        public ActionResult LoadPTMTCTQAddUpdateFormPartial(int HeaderID)
        {
            NDE001 objNDE001 = new NDE001();

            string user = objClsLoginInfo.UserName;

            string BU = string.Join(",", db.ATH001.Where(i => i.Employee == user).Select(i => i.BU).ToList());
            // List<Projects> project = Manager.getProjectsByUser(user);
            string[] location = db.ATH001.Where(i => i.Employee == user).Select(i => i.Location).ToArray();

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.lstyesno = lstyesno.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

            if (HeaderID > 0)
            {
                objNDE001 = db.NDE001.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
                NDEModels objNDEModels = new NDEModels();
                string strBU = db.COM001.Where(i => i.t_cprj == objNDE001.Project).FirstOrDefault().t_entu;
                var BUDescription = (from a in db.COM002
                                     where a.t_dtyp == 2 && a.t_dimx == strBU
                                     select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
                objNDE001.BU = BUDescription.BUDesc;
                var locDescription = (from a in db.COM002
                                      where a.t_dtyp == 1 && a.t_dimx == objNDE001.Location
                                      select new { Location = a.t_dimx + "-" + a.t_desc }).FirstOrDefault();
                objNDE001.Location = locDescription.Location;
                var projectDetails = db.COM001.Where(x => x.t_cprj == objNDE001.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                ViewBag.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
            }
            else
            {
                objNDE001.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                objNDE001.RevNo = 0;
                var locDescription = (from a in db.COM003
                                      join b in db.COM002 on a.t_loca equals b.t_dimx
                                      where b.t_dtyp == 1 && a.t_actv == 1
                                      && a.t_psno.Equals(user, StringComparison.OrdinalIgnoreCase)
                                      select b.t_dimx + "-" + b.t_desc).FirstOrDefault();

                objNDE001.Location = locDescription;
            }
            //ViewBag.Project = new SelectList(project, "projectCode", "projectDescription");
            return PartialView("_PTMTCTQAddUpdateFormPartial", objNDE001);
        }

        [HttpPost]
        public ActionResult SaveHeader(NDE001 nde001, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                if (fc["txtNotificationRequired"].ToString().ToLower() == "yes")
                {
                    nde001.NotificationRequired = true;
                }
                else
                {
                    nde001.NotificationRequired = false;
                }

                if (nde001.HeaderId > 0)
                {
                    if (!IsapplicableForSave(nde001.HeaderId))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This record has been already submitted, Please refresh page.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }

                    NDE001 objNDE001 = db.NDE001.Where(x => x.HeaderId == nde001.HeaderId).FirstOrDefault();
                    objNDE001.ApplicableSpecification = nde001.ApplicableSpecification;
                    objNDE001.AcceptanceStandard = nde001.AcceptanceStandard;
                    objNDE001.ASMECodeStamp = nde001.ASMECodeStamp;
                    objNDE001.TPIName = nde001.TPIName;
                    objNDE001.NotificationRequired = nde001.NotificationRequired;
                    objNDE001.ThicknessOfJob = nde001.ThicknessOfJob;
                    objNDE001.SizeDimension = nde001.SizeDimension;
                    objNDE001.ManufacturingCode = nde001.ManufacturingCode;
                    objNDE001.NDERemarks = nde001.NDERemarks;
                    if (objNDE001.Status == clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue())
                    {
                        objNDE001.RevNo = Convert.ToInt32(objNDE001.RevNo) + 1;
                        objNDE001.ReturnRemarks = null;
                        objNDE001.SubmittedBy = null;
                        objNDE001.SubmittedOn = null;
                        foreach (var item in objNDE001.NDE002)
                        {
                            item.ReturnRemark = null;
                            item.ReturnedBy = null;
                            item.ReturnedOn = null;
                        }
                        objNDE001.Status = clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue();
                    }
                    objNDE001.EditedBy = objClsLoginInfo.UserName;
                    objNDE001.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objNDE001.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PTMTCTQMessages.Update.ToString();
                    objResponseMsg.Remarks = objNDE001.ReturnRemarks;
                }
                else
                {
                    NDE001 objNDE001 = new NDE001();
                    objNDE001.Project = nde001.Project;
                    objNDE001.BU = nde001.BU.Split('-')[0].ToString();
                    objNDE001.Location = nde001.Location.Split('-')[0].ToString();
                    objNDE001.RevNo = 0;
                    objNDE001.CTQNo = nde001.CTQNo;
                    objNDE001.Status = nde001.Status;
                    objNDE001.ManufacturingCode = nde001.ManufacturingCode;
                    objNDE001.ApplicableSpecification = nde001.ApplicableSpecification;
                    objNDE001.AcceptanceStandard = nde001.AcceptanceStandard;
                    objNDE001.ASMECodeStamp = nde001.ASMECodeStamp;
                    objNDE001.TPIName = nde001.TPIName;
                    objNDE001.NotificationRequired = nde001.NotificationRequired;
                    objNDE001.ThicknessOfJob = nde001.ThicknessOfJob;
                    objNDE001.SizeDimension = nde001.SizeDimension;
                    objNDE001.NDERemarks = nde001.NDERemarks;
                    objNDE001.CreatedBy = objClsLoginInfo.UserName;
                    objNDE001.CreatedOn = DateTime.Now;
                    db.NDE001.Add(objNDE001);
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objNDE001.HeaderId;
                    var lstDescription = Manager.GetSubCatagories("NDE-PTMTCTQ", objNDE001.BU, objNDE001.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
                    List<NDE002> lstNDE002 = new List<NDE002>();
                    for (int i = 0; i < lstDescription.Count(); i++)
                    {
                        NDE002 objNDE002 = new NDE002();
                        objNDE002.Description = lstDescription[i].CategoryDescription;
                        objNDE002.HeaderId = objNDE001.HeaderId;
                        objNDE002.Project = objNDE001.Project;
                        objNDE002.BU = objNDE001.BU;
                        objNDE002.Location = objNDE001.Location;
                        objNDE002.RevNo = 0;
                        objNDE002.LineRevNo = 0;
                        objNDE002.CreatedBy = objClsLoginInfo.UserName;
                        objNDE002.CreatedOn = DateTime.Now;
                        objNDE002.Status = clsImplementationEnum.CommonStatus.Added.GetStringValue();
                        lstNDE002.Add(objNDE002);
                    }
                    if (lstNDE002 != null && lstNDE002.Count > 0)
                    {
                        db.NDE002.AddRange(lstNDE002);
                        db.SaveChanges();
                    }


                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PTMTCTQMessages.Insert.ToString();
                    objResponseMsg.Remarks = objNDE001.ReturnRemarks;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReviseData(int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                NDE001 objNDE001 = db.NDE001.Where(x => x.HeaderId == headerId).FirstOrDefault();
                if (objNDE001.Status == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue())
                {
                    objNDE001.RevNo = Convert.ToInt32(objNDE001.RevNo) + 1;
                    objNDE001.Status = clsImplementationEnum.UTCTQStatus.DRAFT.GetStringValue();
                    objNDE001.EditedBy = objClsLoginInfo.UserName;
                    objNDE001.EditedOn = DateTime.Now;
                    objNDE001.ReturnRemarks = null;
                    objNDE001.SubmittedBy = null;
                    objNDE001.SubmittedOn = null;
                    objNDE001.ApprovedOn = null;
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                objResponseMsg.Remarks = objNDE001.ReturnRemarks;

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Lines
        [HttpPost]
        public ActionResult GetPTMTCTQLinesForm(string projCode, string BU, string location, string ctqNo, int LineId)
        {
            NDE002 objNDE002 = new NDE002();
            NDE001 objNDE001 = db.NDE001.Where(x => x.Project == projCode).FirstOrDefault();

            List<string> lstPTMethods = clsImplementationEnum.GetPTMethods().ToList();
            ViewBag.lstPTMethods = lstPTMethods.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();


            List<string> lstMTMethods = clsImplementationEnum.GetMTMethods().ToList();
            ViewBag.lstMTMethods = lstMTMethods.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            List<string> lstMTParticleTypes = clsImplementationEnum.GetMTParticleType().ToList();
            ViewBag.lstMTParticleTypes = lstMTParticleTypes.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            List<string> lstMTTechniques = clsImplementationEnum.GetMTTechniques().ToList();
            ViewBag.lstMTTechniques = lstMTTechniques.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            List<GLB002> lstGLB002 = Manager.GetSubCatagories("Qualification Exam", objNDE001.BU, objNDE001.Location).ToList();
            ViewBag.lstQualificationExam = lstGLB002.AsEnumerable().Select(x => new BULocWiseCategoryModel { CatDesc = (x.Code + "-" + x.Description).ToString(), CatID = x.Id.ToString() }).ToList();

            if (LineId > 0)
            {
                objNDE002 = db.NDE002.Where(x => x.LineId == LineId).FirstOrDefault();
                projCode = objNDE002.Project;
                objNDE002.NDE001.RevNo = objNDE002.RevNo;
            }
            else
            {
                ViewBag.CTQNo = projCode + "_CTQ_PTMT";
                objNDE002.RevNo = 0;

            }
            objNDE002.HeaderId = objNDE001.HeaderId;
            string user = objClsLoginInfo.UserName;

            var projectDetails = db.COM001.Where(x => x.t_cprj == objNDE001.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
            string projectDesc = projectDetails.t_cprj + " - " + projectDetails.t_dsca;

            //string projectDesc = Manager.getProjectsByUser(user).Where(x => x.projectCode == projCode).Select(x => x.projectDescription).FirstOrDefault();

            BU = db.COM001.Where(i => i.t_cprj == projCode).FirstOrDefault().t_entu;
            var BUDescription = (from a in db.COM002
                                 where a.t_dtyp == 2 && a.t_dimx == BU
                                 select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();

            ViewBag.Project = projectDesc;
            ViewBag.BU = BUDescription.BUDesc;
            ViewBag.Location = location;
            ViewBag.CTQNo = projCode + "_CTQ_PTMT";
            return PartialView("_GetPTMTCTQLinesForm", objNDE002);
        }

        [HttpPost]
        public JsonResult LoadPTMTCTQHeaderLineData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();
                string[] arrayCTQStatus = { clsImplementationEnum.CTQStatus.DRAFT.GetStringValue(), clsImplementationEnum.CTQStatus.Returned.GetStringValue() };
                string[] columnName = { "Description", "SpecificationNo", "SpecificationWithClauseNo", "SpecificationRequirement", "ToBeDiscussed", "CriticalToQuality", "CTQRequirement" };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);

                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName);
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_GET_PT_MT_HEADER_LINES
                                (
                               StartIndex,
                               EndIndex,
                               strSortOrder,
                               strWhere
                                ).ToList();
                List<SelectListItem> lstYesNo = new List<SelectListItem>() { new SelectListItem { Text = "Select", Value = "" }, new SelectListItem { Text = "Yes", Value = "Yes" }, new SelectListItem { Text = "No", Value = "No" } };

                List<SelectListItem> BoolenList = new List<SelectListItem>() { new SelectListItem { Text = "Select", Value = "" }, new SelectListItem { Text = "Yes", Value = "True" }, new SelectListItem { Text = "No", Value = "False" } };
                int newRecordId = 0;
                var newRecord = new[] {
                                    "",
                                    Helper.GenerateTextArea(newRecordId, "Description","","",false,"width:200px;","100"),
                                    Helper.GenerateDropdown(newRecordId, "ApplicableForPT", new SelectList(lstYesNo, "Value", "Text"), "","",false,"width:100px;"),
                                    Helper.GenerateDropdown(newRecordId, "ApplicableForMT", new SelectList(lstYesNo, "Value", "Text"),"","",false,"width:100px;"),
                                    Helper.GenerateTextArea(newRecordId, "SpecificationNo","","",false,"width:200px;","50"),
                                    Helper.GenerateTextArea(newRecordId, "SpecificationWithClauseNo","","",false,"width:200px;","50"),
                                    Helper.GenerateDropdown(newRecordId, "ToBeDiscussed", new SelectList(BoolenList, "Value", "Text"), ""),
                                    Helper.GenerateTextArea(newRecordId, "SpecificationRequirement","","",false,"width:200px;","100"),
                                    Helper.GenerateDropdown(newRecordId, "CriticalToQuality", new SelectList(BoolenList, "Value", "Text"),"","",false,"width:100px;"),
                                    Helper.GenerateTextArea(newRecordId, "CTQRequirement", "","",false,"width:200px;","100"),
                                    "",
                                    "Added",
                                    Helper.HTMLActionString(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveNewRecord();" ),
                                    "",
                                     Helper.GenerateHidden(newRecordId, "HeaderId", param.CTQHeaderId),
                                };
                var data = (from uc in lstResult
                            select new[]
                           {
                                    Convert.ToString(uc.ROW_NO),
                                    Convert.ToString(uc.Description),
                                    Helper.GenerateDropdown(uc.LineId, "ApplicableForPT", new SelectList(lstYesNo, "Value", "Text", Convert.ToString(uc.ApplicableForPT)),"","UpdateData(this, "+ uc.HeaderId +","+uc.LineId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Deleted.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.CTQCompileStatus,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"width:100px;"),
                                    Helper.GenerateDropdown(uc.LineId, "ApplicableForMT", new SelectList(lstYesNo, "Value", "Text", Convert.ToString(uc.ApplicableForMT)),"","UpdateData(this, "+ uc.HeaderId +","+uc.LineId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Deleted.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.CTQCompileStatus,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"width:100px;"),
                                    Helper.GenerateTextArea(uc.LineId, "SpecificationNo",    Convert.ToString(uc.SpecificationNo),"UpdateData(this, "+ uc.HeaderId +","+uc.LineId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Deleted.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.CTQCompileStatus,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"width:200px;","50"),
                                    Helper.GenerateTextArea(uc.LineId, "SpecificationWithClauseNo", Convert.ToString(uc.SpecificationWithClauseNo),"UpdateData(this, "+ uc.HeaderId +","+uc.LineId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Deleted.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.CTQCompileStatus,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"width:200px;","50"),
                                    Helper.GenerateDropdown(uc.LineId, "ToBeDiscussed", new SelectList(BoolenList, "Value", "Text", Convert.ToString(uc.ToBeDiscussed)),"","UpdateData(this, "+ uc.HeaderId +","+uc.LineId+",false);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Deleted.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.CTQCompileStatus,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"width:100px;"),
                                    Helper.GenerateTextArea(uc.LineId, "SpecificationRequirement",  Convert.ToString(uc.SpecificationRequirement),"UpdateData(this, "+ uc.HeaderId +","+uc.LineId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Deleted.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.CTQCompileStatus,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"width:200px;","100"),
                                    Helper.GenerateDropdown(uc.LineId, "CriticalToQuality", new SelectList(BoolenList, "Value", "Text", Convert.ToString(uc.CriticalToQuality)),"","UpdateData(this, "+ uc.HeaderId +","+uc.LineId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Deleted.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.CTQCompileStatus,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"width:100px;"),
                                    Helper.GenerateTextArea(uc.LineId, "CTQRequirement",    Convert.ToString(uc.CTQRequirement),"UpdateData(this, "+ uc.HeaderId +","+uc.LineId+",false);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Deleted.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.CTQCompileStatus,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"width:200px;","100"),
                                    Convert.ToString(uc.ReturnRemark),
                                    Convert.ToString(uc.Status),
                                    "<center style=\"display:inline;\">"
                                    + Helper.HTMLActionString(uc.LineId,"Delete","Delete Record","fa fa-trash-o","DeleteRecord(" + uc.LineId  +")","",string.Equals(uc.Status, clsImplementationEnum.QCPStatus.Deleted.GetStringValue(), StringComparison.OrdinalIgnoreCase) || string.Equals(param.CTQCompileStatus, clsImplementationEnum.QCPStatus.SentForApproval.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                                    + "<a class='' href='javascript:void(0)' onclick=ShowTimeline('/NDE/MaintainPTMTCTQ/ShowTimeline?HeaderID="+Convert.ToInt32(uc.HeaderId)+"&LineId="+Convert.ToInt32(uc.LineId)+"');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a></center>",

                                    Convert.ToString(uc.LineId),
                                    Convert.ToString(uc.HeaderId),

                                #region old code(Dharmesh)

                                // Convert.ToString(uc.ROW_NO),
                                // Convert.ToString(uc.SpecificationWithClauseNo),
                                // Convert.ToString(uc.SpecificationRequirement),
                                // (uc.CriticalToQuality == true?"<label class=\"mt-checkbox mt-checkbox-outline\"><input checked=\"checked\" disabled=\"disabled\" type = \"checkbox\" value=\"\" /><span></span></label>":"<label class=\"mt-checkbox mt-checkbox-outline\"><input disabled=\"disabled\" type = \"checkbox\" value=\"\" /><span></span></label>"),//Convert.ToString(uc.CriticalToQuality),
                                // Convert.ToString(uc.CTQRequirement),
                                //(uc.ToBeDiscussed == true?"<label class=\"mt-checkbox mt-checkbox-outline\"><input checked=\"checked\" disabled=\"disabled\" type = \"checkbox\" value=\"\" /><span></span></label>":"<label class=\"mt-checkbox mt-checkbox-outline\"><input disabled=\"disabled\" type = \"checkbox\" value=\"\" /><span></span></label>"),// Convert.ToString(uc.ToBeDiscussed),
                                // Convert.ToString(uc.QualificationForExam),
                                // (uc.ApprovalRequired == true?"<label class=\"mt-checkbox mt-checkbox-outline\"><input checked=\"checked\" disabled=\"disabled\" type = \"checkbox\" value=\"\" /><span></span></label>":"<label class=\"mt-checkbox mt-checkbox-outline\"><input disabled=\"disabled\" type = \"checkbox\" value=\"\" /><span></span></label>"),//Convert.ToString(uc.ApprovalRequired),
                                // Convert.ToString(uc.ConsumableSelection),
                                // Convert.ToString(uc.MethodofPT),
                                // Convert.ToString(uc.MethodofMT),
                                // Convert.ToString(uc.MTParticleTypes),
                                // Convert.ToString(uc.ExtentofCustomer),
                                // Convert.ToString(uc.PTMTAcceptanceStandard),
                                // Convert.ToString(uc.DwellTimeforPT),
                                // Convert.ToString(uc.MTTechniques),
                                // (uc.ColourContrast == true?"<label class=\"mt-checkbox mt-checkbox-outline\"><input checked=\"checked\" disabled=\"disabled\" type = \"checkbox\" value=\"\" /><span></span></label>":"<label class=\"mt-checkbox mt-checkbox-outline\"><input disabled=\"disabled\" type = \"checkbox\" value=\"\" /><span></span></label>"),//Convert.ToString(uc.ColourContrast),
                                // Convert.ToString(uc.MTInaccessible),
                                // Convert.ToString("R"+uc.RevNo),
                                // Convert.ToString("R"+uc.LineRevNo),
                                //  Convert.ToString(uc.Status),
                                //  Convert.ToString(uc.ReturnRemark),
                                // Convert.ToString(uc.LineId),
                                // Convert.ToString(uc.LineId),
                                // Convert.ToString(uc.HeaderId)
#endregion
                            }).ToList();
                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveNewLines(FormCollection fc)
        {
            int newRowIndex = 0;
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int headerId = Convert.ToInt32(fc["HeaderId" + newRowIndex]);
                if (headerId > 0)
                {
                    if (!IsapplicableForSubmit(headerId))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This record has been already submitted, Please refresh page.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                }
                NDE001 objNDE001 = db.NDE001.Where(x => x.HeaderId == headerId).FirstOrDefault();
                NDE002 objNDE002 = new NDE002();
                objNDE002.HeaderId = objNDE001.HeaderId;
                objNDE002.Project = objNDE001.Project;
                objNDE002.BU = objNDE001.BU;
                objNDE002.Location = objNDE001.Location;
                objNDE002.Description = fc["Description" + newRowIndex];
                objNDE002.SpecificationNo = fc["SpecificationNo" + newRowIndex];
                objNDE002.SpecificationRequirement = fc["SpecificationRequirement" + newRowIndex];
                objNDE002.ToBeDiscussed = Convert.ToBoolean(fc["ToBeDiscussed" + newRowIndex]);
                objNDE002.CTQRequirement = fc["CTQRequirement" + newRowIndex];
                objNDE002.SpecificationWithClauseNo = fc["SpecificationWithClauseNo" + newRowIndex];
                objNDE002.CriticalToQuality = Convert.ToBoolean(fc["CriticalToQuality" + newRowIndex]);
                objNDE002.ApplicableForPT = fc["ApplicableForPT" + newRowIndex];
                objNDE002.ApplicableForMT = fc["ApplicableForMT" + newRowIndex];
                objNDE002.RevNo = 0;
                objNDE002.LineRevNo = 0;
                objNDE002.Status = clsImplementationEnum.CommonStatus.Added.GetStringValue();
                objNDE002.CreatedBy = objClsLoginInfo.UserName;
                objNDE002.CreatedOn = DateTime.Now;
                if (objNDE001.Status == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue())
                {
                    objNDE001.RevNo = Convert.ToInt32(objNDE001.RevNo) + 1;
                    objNDE001.Status = clsImplementationEnum.UTCTQStatus.DRAFT.GetStringValue();
                    objNDE001.EditedBy = objClsLoginInfo.UserName;
                    objNDE001.EditedOn = DateTime.Now;
                    objNDE001.ReturnRemarks = null;
                    objNDE001.SubmittedBy = null;
                    objNDE001.SubmittedOn = null;
                    objNDE001.ApprovedOn = null;
                }
                db.NDE002.Add(objNDE002);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
                objResponseMsg.Remarks = objNDE001.ReturnRemarks;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateData(int headerId, string columnName, string columnValue, int LineId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {

                if (headerId > 0)
                {


                    if (!IsapplicableForSubmit(headerId))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This record has been already submitted,Please refresh page.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                }
                if (!string.IsNullOrEmpty(columnName))
                {
                    db.SP_NDE_PTMT_CTQ_UPDATE_COLUMN(LineId, columnName, columnValue);
                    ReviseData(headerId);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                    objResponseMsg.LineStatus = clsImplementationEnum.CommonStatus.Modified.GetStringValue();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SavePTMTCTQLines(NDE002 nde002, FormCollection fc)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                NDE001 objNDE001 = db.NDE001.Where(x => x.HeaderId == nde002.HeaderId).FirstOrDefault();
                if (nde002.LineId > 0)
                {
                    NDE002 objNDE002 = db.NDE002.Where(x => x.LineId == nde002.LineId).FirstOrDefault();
                    if (objNDE002.Status == clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue())
                    {
                        objNDE002.RevNo = Convert.ToInt32(objNDE002.RevNo) + 1;
                        objNDE002.LineRevNo = Convert.ToInt32(objNDE002.LineRevNo) + 1;
                        objNDE001.RevNo = Convert.ToInt32(objNDE001.RevNo) + 1;
                        objNDE001.Status = clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue();
                        objNDE001.ReturnRemarks = null;
                        objNDE001.SubmittedBy = null;
                        objNDE001.SubmittedOn = null;
                        objNDE001.ApprovedOn = null;
                    }
                    objNDE002.Status = clsImplementationEnum.CommonStatus.Added.GetStringValue();
                    objNDE002.HeaderId = nde002.HeaderId;
                    objNDE002.Project = objNDE001.Project;
                    objNDE002.BU = objNDE001.BU;
                    objNDE002.Location = objNDE001.Location;
                    objNDE002.SpecificationWithClauseNo = nde002.SpecificationWithClauseNo;
                    objNDE002.SpecificationRequirement = nde002.SpecificationRequirement;
                    objNDE002.CriticalToQuality = nde002.CriticalToQuality;
                    objNDE002.CTQRequirement = nde002.CTQRequirement;
                    objNDE002.ToBeDiscussed = nde002.ToBeDiscussed;
                    objNDE002.QualificationForExam = nde002.QualificationForExam;
                    objNDE002.ApprovalRequired = nde002.ApprovalRequired;
                    objNDE002.ConsumableSelection = nde002.ConsumableSelection;
                    objNDE002.MethodofPT = nde002.MethodofPT;
                    objNDE002.MethodofMT = nde002.MethodofMT;
                    objNDE002.MTParticleTypes = nde002.MTParticleTypes;
                    objNDE002.ExtentofCustomer = nde002.ExtentofCustomer;
                    objNDE002.PTMTAcceptanceStandard = nde002.PTMTAcceptanceStandard;
                    objNDE002.DwellTimeforPT = nde002.DwellTimeforPT;
                    objNDE002.MTTechniques = nde002.MTTechniques;
                    objNDE002.ColourContrast = nde002.ColourContrast;
                    objNDE002.PT_MTInaccessible = nde002.PT_MTInaccessible;
                    objNDE002.Others = nde002.Others;
                    objNDE002.EditedBy = objClsLoginInfo.UserName;
                    objNDE002.EditedOn = DateTime.Now;
                }
                else
                {
                    db.NDE002.Add(new NDE002
                    {
                        HeaderId = nde002.HeaderId,
                        Project = objNDE001.Project,
                        BU = objNDE001.BU,
                        Location = objNDE001.Location,
                        RevNo = 0,
                        LineRevNo = 0,
                        Status = clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue(),
                        SpecificationWithClauseNo = nde002.SpecificationWithClauseNo,
                        SpecificationRequirement = nde002.SpecificationRequirement,
                        CriticalToQuality = nde002.CriticalToQuality,
                        CTQRequirement = nde002.CTQRequirement,
                        ToBeDiscussed = nde002.ToBeDiscussed,
                        QualificationForExam = nde002.QualificationForExam,
                        ApprovalRequired = nde002.ApprovalRequired,
                        ConsumableSelection = nde002.ConsumableSelection,
                        MethodofPT = nde002.MethodofPT,
                        MethodofMT = nde002.MethodofMT,
                        MTParticleTypes = nde002.MTParticleTypes,
                        ExtentofCustomer = nde002.ExtentofCustomer,
                        PTMTAcceptanceStandard = nde002.PTMTAcceptanceStandard,
                        DwellTimeforPT = nde002.DwellTimeforPT,
                        MTTechniques = nde002.MTTechniques,
                        ColourContrast = nde002.ColourContrast,
                        PT_MTInaccessible = nde002.PT_MTInaccessible,
                        Others = nde002.Others,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    if (objNDE001.Status == clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue())
                    {
                        objNDE001.RevNo = Convert.ToInt32(objNDE001.RevNo) + 1;
                        objNDE001.Status = clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue();
                        objNDE001.EditedBy = objClsLoginInfo.UserName;
                        objNDE001.EditedOn = DateTime.Now;
                        objNDE001.ReturnRemarks = null;
                        objNDE001.SubmittedBy = null;
                        objNDE001.SubmittedOn = null;
                        objNDE001.ApprovedOn = null;
                    }
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
                if (nde002.LineId > 0)
                {
                    objResponseMsg.Value = clsImplementationMessage.PTMTCTQMessages.Update;
                }
                else
                {
                    objResponseMsg.Value = clsImplementationMessage.PTMTCTQMessages.Insert;
                }
                objResponseMsg.Status = clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue();
                objResponseMsg.Remarks = objNDE001.ReturnRemarks;
                objResponseMsg.LineStatus = clsImplementationEnum.CommonStatus.Added.GetStringValue();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteLine(int LineID)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string deleted = clsImplementationEnum.CommonStatus.Deleted.GetStringValue();
                NDE002 objNDE002 = db.NDE002.Where(x => x.LineId == LineID).FirstOrDefault();

                if (objNDE002.HeaderId > 0)
                {
                    if (!IsapplicableForSubmit(objNDE002.HeaderId))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This record has been already submitted, Please refresh page.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                }
                if (objNDE002 != null)
                {
                    if (objNDE002.NDE001.RevNo > 0)
                    {
                        objNDE002.Status = deleted;
                    }
                    else
                    {
                        db.NDE002.Remove(objNDE002);
                    }
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details successfully deleted.";
                    objResponseMsg.LineStatus = deleted;
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region send for approval
        [HttpPost]
        public ActionResult SendForApproval(int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (HeaderId > 0)
                {
                    if (!IsapplicableForSubmit(HeaderId))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This record has been already submitted, Please refresh page.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                }
                string[] arrLineStatus = { clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue(), clsImplementationEnum.PTMTCTQStatus.Returned.GetStringValue() };
                List<NDE002> lstNDE002 = db.NDE002.Where(x => x.HeaderId == HeaderId && arrLineStatus.Contains(x.Status)).ToList();
                List<NDE002> IsNullEntry = db.NDE002.Where(x => x.HeaderId == HeaderId && (x.ApplicableForMT == null || x.ApplicableForPT == null ||
                                                              x.SpecificationNo == null || x.SpecificationWithClauseNo == null ||
                                                             x.SpecificationRequirement == null || x.CriticalToQuality == null)).ToList();

                if (!IsNullEntry.Any())
                {
                    if (lstNDE002 != null && lstNDE002.Count > 0)
                    {
                        foreach (var objData in lstNDE002)
                        {
                            NDE002 objNDE002 = lstNDE002.Where(x => x.LineId == objData.LineId).FirstOrDefault();
                            //  objNDE002.Status = clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue();
                            objNDE002.EditedBy = objClsLoginInfo.UserName;
                            objNDE002.EditedOn = DateTime.Now;
                            objNDE002.SubmittedBy = objClsLoginInfo.UserName;
                            objNDE002.SubmittedOn = DateTime.Now;
                        }
                    }
                    NDE001 objNDE001 = db.NDE001.Where(x => x.HeaderId == HeaderId && arrLineStatus.Contains(x.Status)).FirstOrDefault();
                    if (objNDE001 != null)
                    {
                        objNDE001.Status = clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue();
                        objNDE001.SubmittedBy = objClsLoginInfo.UserName;
                        objNDE001.SubmittedOn = DateTime.Now;
                        db.SaveChanges();

                        #region Send Notification
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE2.GetStringValue(), objNDE001.Project, objNDE001.BU, objNDE001.Location, "CTQ: " + objNDE001.CTQNo + " has come for your Approval", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/NDE/ApprovePTMTCTQ/PTMTCTQLineDetails?HeaderID=" + objNDE001.HeaderId);
                        #endregion

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Details successfully sent for approval.";
                    }
                    else
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Details not available for approval.";
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "info";
                    objResponseMsg.Remarks = string.Format(clsImplementationMessage.UTCTQMessages.NullRecord, "PT/MT  CTQ Details");
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Copy
        [HttpPost]
        public ActionResult GetCopyDataFormPartial()
        {
            //List<Projects> project = Manager.getProjectsByUser(objClsLoginInfo.UserName);
            //ViewBag.Project = new SelectList(project, "projectCode", "projectDescription");
            return PartialView("_GetCopyDataFormPartial");
        }

        [HttpPost]
        public ActionResult copyPTMTCTQ(string projCode, int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string currentUser = objClsLoginInfo.UserName;

                var currentLoc = (from a in db.COM003
                                  join b in db.COM002 on a.t_loca equals b.t_dimx
                                  where b.t_dtyp == 1 && a.t_actv == 1
                                  && a.t_psno.Equals(currentUser, StringComparison.OrdinalIgnoreCase)
                                  select b.t_dimx).FirstOrDefault().ToString();


                NDE001 objNDE001 = db.NDE001.Where(x => x.Project == projCode && x.Location == currentLoc).FirstOrDefault();
                NDE001 objExistingNDE001 = db.NDE001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                if (objNDE001 != null)
                {
                    #region OldCode
                    //if (objNDE001.Location == currentLoc)
                    //{
                    //    if (objNDE001.Status == clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue())
                    //    {
                    //        objResponseMsg = InsertNewPTMTCTQ(objExistingNDE001, objNDE001, true);
                    //    }
                    //    else
                    //    {
                    //        objResponseMsg.Key = false;
                    //        objResponseMsg.Value = "PT/MT CTQ already exists";
                    //    }
                    //}
                    //else
                    //{
                    //    NDE001 objDesNDE001 = new NDE001();
                    //    string BU = db.COM001.Where(i => i.t_cprj == projCode).FirstOrDefault().t_entu;
                    //    objDesNDE001.Project = projCode;
                    //    objDesNDE001.BU = BU;
                    //    objDesNDE001.Location = currentLoc;
                    //    objDesNDE001.CTQNo = projCode + "_CTQ_PTMT";
                    //    objResponseMsg = InsertNewPTMTCTQ(objExistingNDE001, objDesNDE001, false);
                    //}
                    #endregion

                    #region NewCode
                    if (objNDE001.Status == clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue() ||
                        objNDE001.Status == clsImplementationEnum.PTMTCTQStatus.Returned.GetStringValue())
                    {
                        objResponseMsg = InsertNewPTMTCTQ(objExistingNDE001, objNDE001, true);
                    }
                    else if (objNDE001.Status == clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue() ||
                        objNDE001.Status == clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "PT/MT CTQ already exists";
                    }
                    #endregion
                }
                else
                {
                    NDE001 objDesNDE001 = new NDE001();
                    string BU = db.COM001.Where(i => i.t_cprj == projCode).FirstOrDefault().t_entu;
                    objDesNDE001.Project = projCode;
                    objDesNDE001.BU = BU;
                    objDesNDE001.Location = currentLoc;
                    objDesNDE001.CTQNo = projCode + "_CTQ_PTMT";
                    objResponseMsg = InsertNewPTMTCTQ(objExistingNDE001, objDesNDE001, false);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponseMsgWithStatus InsertNewPTMTCTQ(NDE001 objSrcNDE001, NDE001 objDesNDE001, bool IsEdited)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();

            try
            {
                if (IsEdited)
                {
                    //If Record exist for destination project then no need to change header table confirm by satish (On 26-03-2017 11:00 AM). 
                    //So Comment OldCode For Update Header
                    #region OldCode For Update Header
                    //objDesNDE001.ApplicableSpecification = objSrcNDE001.ApplicableSpecification;
                    //objDesNDE001.AcceptanceStandard = objSrcNDE001.AcceptanceStandard;
                    //objDesNDE001.ASMECodeStamp = objSrcNDE001.ASMECodeStamp;
                    //objDesNDE001.TPIName = objSrcNDE001.TPIName;
                    //objDesNDE001.NotificationRequired = objSrcNDE001.NotificationRequired;
                    //objDesNDE001.ThicknessOfJob = objSrcNDE001.ThicknessOfJob;
                    //objDesNDE001.SizeDimension = objSrcNDE001.SizeDimension;
                    //objDesNDE001.ManufacturingCode = objSrcNDE001.ManufacturingCode;
                    //objDesNDE001.NDERemarks = objSrcNDE001.NDERemarks;
                    objDesNDE001.EditedBy = objClsLoginInfo.UserName;
                    objDesNDE001.EditedOn = DateTime.Now;
                    #endregion

                    //Add Source Line item in destination project. So comment Remove Existing Data code (On 26-03-2017 11:00 AM)
                    #region Remove Existing Data
                    //List<NDE002> lstDesNDE001 = db.NDE002.Where(x => x.HeaderId == objDesNDE001.HeaderId).ToList();
                    //if (lstDesNDE001 != null && lstDesNDE001.Count > 0)
                    //{
                    //    db.NDE002.RemoveRange(lstDesNDE001);
                    //}
                    #endregion

                    List<NDE002> lstSrcNDE002 = db.NDE002.Where(x => x.HeaderId == objSrcNDE001.HeaderId).ToList();

                    if (lstSrcNDE002 != null && lstSrcNDE002.Count > 0)
                    {
                        db.NDE002.AddRange(
                                            lstSrcNDE002.Select(x =>
                                            new NDE002
                                            {
                                                HeaderId = objDesNDE001.HeaderId,
                                                Project = objDesNDE001.Project,
                                                BU = objDesNDE001.BU,
                                                Location = objDesNDE001.Location,
                                                RevNo = Convert.ToInt32(objDesNDE001.RevNo),
                                                LineRevNo = 0,
                                                Status = clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue(),
                                                ApplicableForMT = x.ApplicableForMT,
                                                ApplicableForPT = x.ApplicableForPT,
                                                Description = x.Description,
                                                SpecificationNo = x.SpecificationNo,
                                                SpecificationWithClauseNo = x.SpecificationWithClauseNo,
                                                ToBeDiscussed = x.ToBeDiscussed,
                                                SpecificationRequirement = x.SpecificationRequirement,
                                                CriticalToQuality = x.CriticalToQuality,
                                                CTQRequirement = x.CTQRequirement,

                                                ReturnRemark = x.ReturnRemark,
                                                CreatedBy = objClsLoginInfo.UserName,
                                                CreatedOn = DateTime.Now,
                                            })
                                          );
                    }

                    db.SaveChanges();
                    objResponseMsg.HeaderId = objDesNDE001.HeaderId;
                    objResponseMsg.HeaderStatus = objDesNDE001.CTQNo;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PTMTCTQMessages.Update.ToString() + " To Document No : " + objResponseMsg.HeaderStatus + " of " + objDesNDE001.Project + " Project.";
                }
                else
                {
                    objDesNDE001.ApplicableSpecification = objSrcNDE001.ApplicableSpecification;
                    objDesNDE001.AcceptanceStandard = objSrcNDE001.AcceptanceStandard;
                    objDesNDE001.ASMECodeStamp = objSrcNDE001.ASMECodeStamp;
                    objDesNDE001.TPIName = objSrcNDE001.TPIName;
                    objDesNDE001.NotificationRequired = objSrcNDE001.NotificationRequired;
                    objDesNDE001.ThicknessOfJob = objSrcNDE001.ThicknessOfJob;
                    objDesNDE001.SizeDimension = objSrcNDE001.SizeDimension;
                    objDesNDE001.ManufacturingCode = objSrcNDE001.ManufacturingCode;
                    objDesNDE001.NDERemarks = objSrcNDE001.NDERemarks;
                    objDesNDE001.Status = clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue();
                    objDesNDE001.RevNo = 0;
                    objDesNDE001.CreatedBy = objClsLoginInfo.UserName;
                    objDesNDE001.CreatedOn = DateTime.Now;
                    db.NDE001.Add(objDesNDE001);

                    db.SaveChanges();
                    objResponseMsg.HeaderId = objDesNDE001.HeaderId;

                    //List<NDE002> lstDesNDE001 = db.NDE002.Where(x => x.HeaderId == objDesNDE001.HeaderId).ToList();
                    //if (lstDesNDE001 != null && lstDesNDE001.Count > 0)
                    //{
                    //    db.NDE002.RemoveRange(lstDesNDE001);
                    //}
                    List<NDE002> lstSrcNDE002 = db.NDE002.Where(x => x.HeaderId == objSrcNDE001.HeaderId).ToList();
                    if (lstSrcNDE002 != null && lstSrcNDE002.Count > 0)
                    {
                        db.NDE002.AddRange(
                                            lstSrcNDE002.Select(x =>
                                            new NDE002
                                            {
                                                HeaderId = objDesNDE001.HeaderId,
                                                Project = objDesNDE001.Project,
                                                BU = objDesNDE001.BU,
                                                Location = objDesNDE001.Location,
                                                RevNo = Convert.ToInt32(objDesNDE001.RevNo),
                                                ApplicableForPT = x.ApplicableForPT,
                                                ApplicableForMT = x.ApplicableForMT,
                                                LineRevNo = 0,
                                                Status = clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue(),
                                                //ApplicableForMT = x.ApplicableForMT,
                                                //ApplicableForPT = x.ApplicableForPT,
                                                Description = x.Description,
                                                SpecificationNo = x.SpecificationNo,
                                                SpecificationWithClauseNo = x.SpecificationWithClauseNo,
                                                ToBeDiscussed = x.ToBeDiscussed,
                                                SpecificationRequirement = x.SpecificationRequirement,
                                                CriticalToQuality = x.CriticalToQuality,
                                                CTQRequirement = x.CTQRequirement,

                                                CreatedBy = objClsLoginInfo.UserName,
                                                CreatedOn = DateTime.Now,
                                            })
                                          );
                    }
                    db.SaveChanges();
                    objResponseMsg.HeaderStatus = objDesNDE001.CTQNo;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PTMTCTQMessages.Insert.ToString() + " To Document No : " + objResponseMsg.HeaderStatus + " of " + objDesNDE001.Project + " Project.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
                objResponseMsg.HeaderId = 0;
            }
            return objResponseMsg;
        }
        #endregion

        #region history
        [HttpPost]
        public ActionResult GetHistoryDataPartial(int HeaderID, string isApprover = "false")
        {
            ViewBag.HeaderID = HeaderID;
            ViewBag.approver = isApprover;
            return PartialView("_GetHistoryDataPartial");
        }

        [HttpPost]
        public JsonResult LoadPTMTCTQHeaderHistoryData(JQueryDataTableParamModel param, string isApprover = "false")
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (ltrim(rtrim(nde1.BU))+'-'+bcom2.t_desc like '%" + param.sSearch +
                                "%' or ltrim(rtrim(nde1.Location))+'-'+lcom2.t_desc like '%" + param.sSearch +
                                "%' or (nde1.Project+'-'+com1.t_dsca) like '%" + param.sSearch +
                                "%' or CTQNo like '%" + param.sSearch +
                                "%' or RevNo like '%" + param.sSearch +
                                "%' or ApplicableSpecification like '%" + param.sSearch +
                                "%' or AcceptanceStandard like '%" + param.sSearch +
                                "%' or ASMECodeStamp like '%" + param.sSearch +
                                "%' or TPIName like '%" + param.sSearch +
                                "%' or NotificationRequired like '%" + param.sSearch +
                                "%' or ThicknessOfJob like '%" + param.sSearch +
                                "%' or SizeDimension like '%" + param.sSearch +
                                "%' or NDERemarks like '%" + param.sSearch +
                                "%' or nde1.CreatedBy +' - '+com003c.t_name like '%" + param.sSearch +
                                "%' or Status like '%" + param.sSearch + "%')";
                }
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "nde1.BU", "nde1.Location");
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_PTMTCTQ_GETHEADER_HISTORY
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.BU),
                               Convert.ToString(uc.Location),
                               Convert.ToString(uc.CTQNo),
                               Convert.ToString(uc.ManufacturingCode),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn !=null?(Convert.ToDateTime(uc.CreatedOn)).ToString("dd/MM/yyyy"):""),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.ApplicableSpecification),
                               Convert.ToString(uc.AcceptanceStandard),
                               (uc.ASMECodeStamp == true?"<label class=\"mt-checkbox mt-checkbox-outline\"><input checked=\"checked\" disabled=\"disabled\" type = \"checkbox\" value=\"\" /><span></span></label>":"<label class=\"mt-checkbox mt-checkbox-outline\"><input disabled=\"disabled\" type = \"checkbox\" value=\"\" /><span></span></label>"),//Convert.ToString(uc.ASMECodeStamp),
                               Convert.ToString(uc.TPIName),
                            Convert.ToString(uc.NotificationRequired==true ? "Yes":uc.NotificationRequired == false ? "No" : ""),
                               Convert.ToString(uc.ThicknessOfJob),
                               Convert.ToString(uc.SizeDimension),
                               Convert.ToString(uc.NDERemarks),
                                   "<center>"
                                 + Helper.GenerateActionIcon(uc.HeaderId, "View", "View Detail", "fa fa-eye", "", WebsiteURL + "/NDE/MaintainPTMTCTQ/GetPTMTCTQLineHistory?ID="+Convert.ToInt32(uc.Id)+"&approver="+isApprover+"'",false)
                                 +"<a title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/NDE/MaintainPTMTCTQ/ShowTimeline?HeaderID="+Convert.ToInt32(uc.Id)+"&LineId=0&isHistory=true');><i class='iconspace fa fa-clock-o'></i></a>"
                                 +"</center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]
        public ActionResult GetPTMTCTQLineHistory(int ID = 0, string approver = "false")
        {
            NDEModels objNDEModels = new NDEModels();
            ViewBag.approver = approver;
            int Id = 0;
            if (ID > 0)
            {
                Id = Convert.ToInt32(ID);
            }
            NDE001_Log objNDE001_Log = db.NDE001_Log.Where(x => x.Id == Id).FirstOrDefault();
            if (objNDE001_Log != null)
            {
                ViewBag.HeaderID = objNDE001_Log.HeaderId;
                ViewBag.Status = objNDE001_Log.Status;
                ViewBag.CTQNo = objNDE001_Log.CTQNo;

                var projectDetails = db.COM001.Where(x => x.t_cprj == objNDE001_Log.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                objNDE001_Log.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                var BUDescription = db.COM002.Where(x => x.t_dimx == objNDE001_Log.BU).Select(x => x.t_desc).FirstOrDefault();
                objNDE001_Log.BU = Convert.ToString(objNDE001_Log.BU + " - " + BUDescription);
                var Location = db.COM002.Where(x => x.t_dimx == objNDE001_Log.Location).Select(x => x.t_desc).FirstOrDefault();
                objNDE001_Log.Location = Convert.ToString(objNDE001_Log.Location + " - " + Location);
            }
            return View(objNDE001_Log);
        }

        [HttpPost]
        public ActionResult GetCTQLinesHistoryHtml(int ID, string strCTQNo, string isApprover = "false")
        {
            ViewBag.ID = ID;
            ViewBag.approver = isApprover;
            ViewBag.strCTQNo = strCTQNo;
            ViewBag.CTQStatus = db.NDE001_Log.Where(x => x.Id == ID).Select(x => x.Status).FirstOrDefault();
            return PartialView("_GetCTQLinesHistoryHtml");
        }

        [HttpPost]
        public JsonResult LoadPTMTCTQLineHistoryData(JQueryDataTableParamModel param)
        {

            try
            {
                string[] arrayCTQStatus = { clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue() };
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "1=1 " + Manager.MakeIntInCondition("RefId", param.CTQHeaderId).ToString();
                int ndeHisId = Convert.ToInt32(param.CTQHeaderId);
                NDE001_Log objNDE001_Log = db.NDE001_Log.FirstOrDefault(x => x.Id == ndeHisId);
                string[] columnName = { "Description", "SpecificationNo", "SpecificationWithClauseNo", "SpecificationRequirement", "ToBeDiscussed", "CriticalToQuality", "CTQRequirement" };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);

                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_GET_PT_MT_HEADER_LINES_HISTORY
                                (
                               StartIndex,
                               EndIndex,
                               strSortOrder,
                               strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.Description),
                                Convert.ToString(uc.ApplicableForPT),
                                Convert.ToString(uc.ApplicableForMT),
                                Convert.ToString(uc.SpecificationNo),
                                Convert.ToString(uc.SpecificationWithClauseNo),
                                Convert.ToString(uc.ToBeDiscussed==true ? "Yes":uc.ToBeDiscussed==false?"No":""),
                                Convert.ToString(uc.SpecificationRequirement),
                                Convert.ToString(uc.CriticalToQuality==true ? "Yes":uc.CriticalToQuality==false?"No":""),
                                Convert.ToString(uc.CTQRequirement),
                                Convert.ToString(uc.ReturnRemark),
                                Convert.ToString(uc.Status),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId),
                                "<center style=\"display:inline;\"><a class='' href='javascript:void(0)' onclick=ShowTimeline('/NDE/MaintainPTMTCTQ/ShowTimeline?HeaderID="+Convert.ToInt32(uc.HeaderId)+"&LineId="+Convert.ToInt32(uc.Id)+"&isHistory=true');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a></center>",
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Common functions
        [HttpPost]
        public bool checkProjectExist(string projectcode)
        {
            bool Flag = false;
            if (!string.IsNullOrWhiteSpace(projectcode))
            {
                NDE001 objNDE001 = db.NDE001.Where(x => x.Project == projectcode).FirstOrDefault();
                if (objNDE001 != null)
                {
                    Flag = true;
                }
            }
            return Flag;
        }

        [HttpPost]
        public ActionResult GetProjectWiseBU(string projectcode)
        {
            if (!string.IsNullOrWhiteSpace(projectcode))
            {
                ProjectDataModel objProjectDataModel = new ProjectDataModel();
                string BU = db.COM001.Where(i => i.t_cprj == projectcode).FirstOrDefault().t_entu;
                var BUDescription = (from a in db.COM002
                                     where a.t_dtyp == 2 && a.t_dimx == BU
                                     select new ProjectDataModel { BUDescription = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
                objProjectDataModel.ManufacturingCode = Manager.getManufacturingCode(null, projectcode);
                objProjectDataModel.BUDescription = BUDescription.BUDescription;
                return Json(objProjectDataModel, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ShowTimeline(int HeaderId, int LineId = 0, bool isHistory = false)
        {
            TimelineViewModel model = new TimelineViewModel();
            if (isHistory)
            {
                if (LineId > 0)
                {
                    NDE002_Log objNDE002 = db.NDE002_Log.Where(x => x.Id == LineId).FirstOrDefault();
                    model.Title = "NDECTQ Lines";
                    model.ApprovedBy = Manager.GetUserNameFromPsNo(objNDE002.ApprovedBy);
                    model.ApprovedOn = objNDE002.ApprovedOn;
                    model.SubmittedBy = Manager.GetUserNameFromPsNo(objNDE002.SubmittedBy);
                    model.SubmittedOn = objNDE002.SubmittedOn;
                    model.CreatedBy = Manager.GetUserNameFromPsNo(objNDE002.CreatedBy);
                    model.CreatedOn = objNDE002.CreatedOn;
                    model.ReturnedBy = Manager.GetUserNameFromPsNo(objNDE002.ReturnedBy);
                    model.ReturnedOn = objNDE002.ReturnedOn;
                }
                else
                {
                    NDE001_Log objNDE001 = db.NDE001_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                    model.Title = "NDECTQ Header";
                    model.ApprovedBy = Manager.GetUserNameFromPsNo(objNDE001.ApprovedBy);
                    model.ApprovedOn = objNDE001.ApprovedOn;
                    model.SubmittedBy = Manager.GetUserNameFromPsNo(objNDE001.SubmittedBy);
                    model.SubmittedOn = objNDE001.SubmittedOn;
                    model.CreatedBy = Manager.GetUserNameFromPsNo(objNDE001.CreatedBy);
                    model.CreatedOn = objNDE001.CreatedOn;
                }
            }
            else
            {
                if (LineId > 0)
                {
                    NDE002 objNDE002 = db.NDE002.Where(x => x.LineId == LineId).FirstOrDefault();
                    model.Title = "NDECTQ Lines";
                    model.ApprovedBy = Manager.GetUserNameFromPsNo(objNDE002.ApprovedBy);
                    model.ApprovedOn = objNDE002.ApprovedOn;
                    model.SubmittedBy = Manager.GetUserNameFromPsNo(objNDE002.SubmittedBy);
                    model.SubmittedOn = objNDE002.SubmittedOn;
                    model.CreatedBy = Manager.GetUserNameFromPsNo(objNDE002.CreatedBy);
                    model.CreatedOn = objNDE002.CreatedOn;
                    model.ReturnedBy = Manager.GetUserNameFromPsNo(objNDE002.ReturnedBy);
                    model.ReturnedOn = objNDE002.ReturnedOn;
                }
                else
                {
                    NDE001 objNDE001 = db.NDE001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    model.Title = "NDECTQ Header";
                    model.ApprovedBy = Manager.GetUserNameFromPsNo(objNDE001.ApprovedBy);
                    model.ApprovedOn = objNDE001.ApprovedOn;
                    model.SubmittedBy = Manager.GetUserNameFromPsNo(objNDE001.SubmittedBy);
                    model.SubmittedOn = objNDE001.SubmittedOn;
                    model.CreatedBy = Manager.GetUserNameFromPsNo(objNDE001.CreatedBy);
                    model.CreatedOn = objNDE001.CreatedOn;
                }
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }

        [HttpPost]
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_PTMTCTQ_GETHEADER(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      RevNo = li.RevNo,
                                      CTQNo = li.CTQNo,
                                      Status = li.Status,
                                      ApplicableSpecification = li.ApplicableSpecification,
                                      AcceptanceStandard = li.AcceptanceStandard,
                                      ASMECodeStamp = li.ASMECodeStamp,
                                      TPIName = li.TPIName,
                                      NotificationRequired = Convert.ToString(li.NotificationRequired == true ? "Yes" : li.NotificationRequired == false ? "No" : ""),
                                      ThicknessOfJob = li.ThicknessOfJob,
                                      SizeDimension = li.SizeDimension,
                                      ManufacturingCode = li.ManufacturingCode,
                                      NDERemarks = li.NDERemarks,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn
                                  }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_GET_PT_MT_HEADER_LINES(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      SrNo = Convert.ToString(uc.ROW_NO),
                                      Description = Convert.ToString(uc.Description),
                                      ApplicableForPT = Convert.ToString(uc.ApplicableForPT),
                                      ApplicableForMT = Convert.ToString(uc.ApplicableForMT),
                                      SpecificationNo = Convert.ToString(uc.SpecificationNo),
                                      SpecificationWithClauseNo = Convert.ToString(uc.SpecificationWithClauseNo),
                                      ToBeDiscussed = Convert.ToString(uc.ToBeDiscussed == true ? "Yes" : uc.ToBeDiscussed == false ? "No" : ""),
                                      SpecificationRequirement = Convert.ToString(uc.SpecificationRequirement),
                                      CriticalToQuality = Convert.ToString(uc.CriticalToQuality == true ? "Yes" : uc.CriticalToQuality == false ? "No" : ""),
                                      CTQRequirement = Convert.ToString(uc.CTQRequirement),
                                      ReturnRemark = Convert.ToString(uc.ReturnRemark),
                                      Status = Convert.ToString(uc.Status),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {
                    var lst = db.SP_PTMTCTQ_GETHEADER_HISTORY(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      RevNo = li.RevNo,
                                      CTQNo = li.CTQNo,
                                      Status = li.Status,
                                      ManufacturingCode = li.ManufacturingCode,
                                      ApplicableSpecification = li.ApplicableSpecification,
                                      AcceptanceStandard = li.AcceptanceStandard,
                                      ASMECodeStamp = li.ASMECodeStamp,
                                      TPIName = li.TPIName,
                                      NotificationRequired = Convert.ToString(li.NotificationRequired == true ? "Yes" : li.NotificationRequired == false ? "No" : ""),
                                      ThicknessOfJob = li.ThicknessOfJob,
                                      SizeDimension = li.SizeDimension,
                                      NDERemarks = li.NDERemarks,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else
                {
                    var lst = db.SP_GET_PT_MT_HEADER_LINES_HISTORY(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      SrNo = Convert.ToString(uc.ROW_NO),
                                      Description = Convert.ToString(uc.Description),
                                      ApplicableForPT = Convert.ToString(uc.ApplicableForPT),
                                      ApplicableForMT = Convert.ToString(uc.ApplicableForMT),
                                      SpecificationNo = Convert.ToString(uc.SpecificationNo),
                                      SpecificationWithClauseNo = Convert.ToString(uc.SpecificationWithClauseNo),
                                      ToBeDiscussed = Convert.ToString(uc.ToBeDiscussed == true ? "Yes" : uc.ToBeDiscussed == false ? "No" : ""),
                                      SpecificationRequirement = Convert.ToString(uc.SpecificationRequirement),
                                      CriticalToQuality = Convert.ToString(uc.CriticalToQuality == true ? "Yes" : uc.CriticalToQuality == false ? "No" : ""),
                                      CTQRequirement = Convert.ToString(uc.CTQRequirement),
                                      ReturnRemark = Convert.ToString(uc.ReturnRemark),
                                      Status = Convert.ToString(uc.Status),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }


                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion
    }
    public class ResponceMsgWithHeaderID : clsHelper.ResponseMsg
    {
        public int HeaderID;
        public int? RevNo;
        public string Remarks;
        public string Status;
    }
    public class ResponceMsgWithStatus : clsHelper.ResponseMsg
    {
        public string Status;
        public string Remarks;
        public string LineStatus;
    }
}