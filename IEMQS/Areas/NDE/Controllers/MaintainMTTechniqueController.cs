﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQS.Areas.NDE.Models;
using IEMQSImplementation;
using IEMQS.Models;
using IEMQS.Areas.Utility.Models;

namespace IEMQS.Areas.NDE.Controllers
{
    public class MaintainMTTechniqueController : clsBase
    {
        // GET: NDE/MaintainMTTechnique
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LoadHeaderDataTable(JQueryDataTableParamModel param, string status)
        {
            try
            {

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;


                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "nde8.BU", "nde8.Location");
                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and nde8.Status in('" + clsImplementationEnum.QCPStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.QCPStatus.Returned.GetStringValue() + "')";
                }
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += " and (ltrim(rtrim(NDE8.BU))+'-'+bcom2.t_desc like '%" + param.sSearch + "%' or ltrim(rtrim(nde8.Location))+'-'+lcom2.t_desc like '%" + param.sSearch + "%' " +
                                            " or NDE8.QualityProject like '%" + param.sSearch + "%' or (NDE8.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%' " +
                                            " or MTTechNo like '%" + param.sSearch + "%' or RevNo like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%' " +
                                            " or TechniqueSheetNo like '%" + param.sSearch + "%'" +
                                            " or (ecom3.t_psno+'-'+ecom3.t_name) like '%" + param.sSearch + "%')";
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstHeader = db.SP_NDE_GetMTTechniqueHeader(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstHeader
                          select new[] {
                        //Convert.ToString(a.HeaderId),
                        Convert.ToString(a.QualityProject),
                        Convert.ToString(a.Project),
                        Convert.ToString(a.BU),
                        Convert.ToString(a.Location),
                        Convert.ToString(a.TechniqueSheetNo),
                        Convert.ToString(a.MTTechNo),
                        Convert.ToString("R" + a.RevNo),
                        a.CreatedBy,
                        Convert.ToString(a.CreatedOn !=null?(Convert.ToDateTime(a.CreatedOn)).ToString("dd/MM/yyyy"):""),
                        a.Status,
                        "<center style='display:inline;'>"
                         + Helper.GenerateActionIcon(a.HeaderId, "View", "View Detail", "fa fa-eye", "", WebsiteURL + "/NDE/MaintainMTTechnique/AddMTTechnique?headerId="+a.HeaderId,false)
                         + HTMLActionString(a.HeaderId,"","btnDelete","Delete Record","fa fa-trash-o","DeleteHeader(" + a.HeaderId + ");",!(a.RevNo ==0 && a.Status.Equals(clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue())))
                         +"<a title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/NDE/MaintainMTTechnique/ShowTimeline?HeaderID="+Convert.ToInt32(a.HeaderId)+"');><i lass='iconspace fa fa-clock-o'></i></a>"
                         + HTMLActionString(a.HeaderId,"","btnHistory","History","fa fa-history","History(" + a.HeaderId + ");",!(a.RevNo > 0 || a.Status.Equals(clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue())))+"</center>",
                        //Convert.ToString(a.HeaderId)
                        //a.EditedBy
                        //a.Status,
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetMTTechniqueGridDataPartial(string status)
        {
            ViewBag.status = status;
            return PartialView("_GetMTTechniqueGridDataPartial");
        }

        #region MT Technique Header
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult AddMTTechnique(int? headerId)
        {
            NDE008 objNDE008 = new NDE008();
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("NDE008");
            string user = objClsLoginInfo.UserName;

            var lstQMSProject = (from a in db.QMS010
                                 select new { a.QualityProject, QMSProjectDesc = a.QualityProject + " - " + a.ManufacturingCode }).ToList();

            var location = db.COM003.Where(i => i.t_psno.Equals(objClsLoginInfo.UserName) && i.t_actv == 1).Select(i => i.t_loca).FirstOrDefault();
            var Location = db.COM002.Where(i => i.t_dtyp == 1 && i.t_dimx.Equals(location, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();

            NDEModels objNDEModels = new NDEModels();
            ViewBag.MTConsume = "";
            if (headerId > 0)
            {
                objNDE008 = db.NDE008.Where(i => i.HeaderId == headerId).FirstOrDefault();
                ViewBag.MTConsume = objNDE008.MTConsuNo;
                var QualityProj = objNDEModels.GetQMSProject(objNDE008.QualityProject);
                ViewBag.QMSProject = QualityProj.QualityProject; //new SelectList(lstQMSProject, "QMSProject", "QMSProjectDesc", objNDE008.QualityProject);
                ViewBag.Location = Location;
                ViewBag.MTMethod = objNDEModels.GetCategory(objNDE008.Method).CategoryDescription;
                ViewBag.MagnetizationTechnique = objNDEModels.GetCategory("Magnetization Technique", objNDE008.MagnetizationTechnique, objNDE008.BU, objNDE008.Location, true).CategoryDescription;
                ViewBag.MethodOfExamination = objNDEModels.GetCategory("Method Of Examination", objNDE008.MethodofExamination, objNDE008.BU, objNDE008.Location, true).CategoryDescription;
                ViewBag.Current = objNDEModels.GetCategory("Current", objNDE008.Current, objNDE008.BU, objNDE008.Location, true).CategoryDescription;
                ViewBag.MagneticParticleType = objNDEModels.GetCategory("Magnetic Particle Type", objNDE008.MagneticParticleType, objNDE008.BU, objNDE008.Location, true).CategoryDescription;
                ViewBag.ParticleColour = objNDEModels.GetCategory("Particle Colour", objNDE008.ParticleColour, objNDE008.BU, objNDE008.Location, true).CategoryDescription;
                ViewBag.CarrierMedium = objNDEModels.GetCategory("Carrier Medium", objNDE008.CarrierMedium, objNDE008.BU, objNDE008.Location, true).CategoryDescription;
                ViewBag.LightIntensity = objNDEModels.GetCategory("Light Intensity", objNDE008.LightIntensity, objNDE008.BU, objNDE008.Location, true).CategoryDescription;
                ViewBag.LightSource = objNDEModels.GetCategory("Light Source", objNDE008.LightSource, objNDE008.BU, objNDE008.Location, true).CategoryDescription;
                ViewBag.DemagnetizationUpto = objNDEModels.GetCategory("Demagnetization upto", objNDE008.Demagnetization, objNDE008.BU, objNDE008.Location, true).CategoryDescription;
                ViewBag.FieldAdequacy = objNDEModels.GetCategory("Field Adequacy", objNDE008.FieldAdequacy, objNDE008.BU, objNDE008.Location, true).CategoryDescription;
                ViewBag.Calibration = objNDEModels.GetCategory("Calibration", objNDE008.Calibration, objNDE008.BU, objNDE008.Location, true).CategoryDescription;
                ViewBag.PostCleaning = objNDEModels.GetCategory("Post Cleaning", objNDE008.PostCleaning, objNDE008.BU, objNDE008.Location, true).CategoryDescription;
            }
            else
            {
                objNDE008.Status = clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue();
                objNDE008.RevNo = 0;
                ViewBag.Location = Location;
            }
            return View(objNDE008);
        }

        [HttpPost]
        public ActionResult SaveHeader(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsgWithStatus = new clsHelper.ResponseMsgWithStatus();
            NDE008 objNDE008 = new NDE008();
            try
            {
                if (fc != null)
                {
                    int headerId = Convert.ToInt32(fc["HeaderID"]);
                   
                    if (headerId > 0)
                    {
                        if (!IsapplicableForSave(headerId))
                        {
                            objResponseMsgWithStatus.Key = false;
                            objResponseMsgWithStatus.Value = "This record has been already submitted, Please refresh page.";
                            return Json(objResponseMsgWithStatus, JsonRequestBehavior.AllowGet);
                        }
                        #region MT Technique Header Update

                        objNDE008 = db.NDE008.Where(i => i.HeaderId == headerId).FirstOrDefault();

                        if (string.Equals(objNDE008.Status, clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                        {
                            objNDE008.RevNo = objNDE008.RevNo + 1;
                            objNDE008.ReturnRemarks = null;
                            objNDE008.ReturnedBy = null;
                            objNDE008.ReturnedOn = null;
                            objNDE008.SubmittedBy = null;
                            objNDE008.SubmittedOn = null;
                            objNDE008.ApprovedOn = null;
                            objNDE008.Status = clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue();
                        }

                        objNDE008.MTTechNo = fc["MTTechNo"];
                        objNDE008.DocNo = Convert.ToInt32(fc["DocNo"]);
                        //objNDE008.Method = Convert.ToInt32(fc["Method"]);

                        objNDE008.MagnetizationTechnique = fc["MagnetizationTechnique"];
                        objNDE008.MethodofExamination = fc["MethodofExamination"];
                        objNDE008.Current = fc["Current"];
                        objNDE008.MagneticParticleType = fc["MagneticParticleType"];
                        objNDE008.ParticleColour = fc["ParticleColour"];
                        objNDE008.CarrierMedium = fc["CarrierMedium"];
                        objNDE008.LightIntensity = fc["LightIntensity"];
                        objNDE008.LightSource = fc["LightSource"];
                        objNDE008.OtherLightSource = fc["OtherLightSource"];
                        objNDE008.PoleSpacing = fc["PoleSpacing"];
                        objNDE008.Demagnetization = fc["Demagnetization"];
                        objNDE008.SurfaceTemperature = fc["SurfaceTemperature"];
                        objNDE008.MTProcedureNo = fc["MTProcedureNo"];
                        objNDE008.PlannerRemarks = fc["PlannerRemarks"];
                        objNDE008.FieldAdequacy = fc["FieldAdequacy"];
                        objNDE008.Calibration = fc["Calibration"];
                        objNDE008.PostCleaning = fc["PostCleaning"];
                        objNDE008.TechniqueSheetNo = fc["TechniqueSheetNo"];
                       // objNDE008.MTConsuNo = fc["MTConsuNo"];
                        objNDE008.EditedBy = objClsLoginInfo.UserName;
                        objNDE008.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsgWithStatus.Key = true;
                        objResponseMsgWithStatus.Value = Convert.ToString(objNDE008.HeaderId);
                        objResponseMsgWithStatus.RevNo = Convert.ToString(objNDE008.RevNo);
                        objResponseMsgWithStatus.HeaderStatus = objNDE008.Status;
                        objResponseMsgWithStatus.Remarks = objNDE008.ReturnRemarks;
                        #endregion
                    }
                    else
                    {
                        #region MT Technique New Header

                        objNDE008.QualityProject = fc["QualityProject"];
                        objNDE008.Project = fc["Project"].Split('-')[0];
                        objNDE008.BU = fc["BU"].Split('-')[0];
                        objNDE008.Location = fc["Location"].Split('-')[0];
                        objNDE008.RevNo = 0;
                        objNDE008.MTTechNo = fc["MTTechNo"];
                        objNDE008.DocNo = Convert.ToInt32(fc["DocNo"]);
                        objNDE008.Status = clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue();
                        objNDE008.Method = fc["Method"];

                        objNDE008.MagnetizationTechnique = fc["MagnetizationTechnique"];
                        objNDE008.MethodofExamination = fc["MethodofExamination"];
                        objNDE008.Current = fc["Current"];
                        objNDE008.MagneticParticleType = fc["MagneticParticleType"];
                        objNDE008.ParticleColour = fc["ParticleColour"];
                        objNDE008.CarrierMedium = fc["CarrierMedium"];
                        objNDE008.LightIntensity = fc["LightIntensity"];
                        objNDE008.LightSource = fc["LightSource"];
                        objNDE008.OtherLightSource = fc["OtherLightSource"];
                        objNDE008.PoleSpacing = fc["PoleSpacing"];
                        objNDE008.Demagnetization = fc["Demagnetization"];
                        objNDE008.SurfaceTemperature = fc["SurfaceTemperature"];
                        objNDE008.MTProcedureNo = fc["MTProcedureNo"];
                        objNDE008.PlannerRemarks = fc["PlannerRemarks"];
                        objNDE008.CreatedBy = objClsLoginInfo.UserName;
                        objNDE008.CreatedOn = DateTime.Now;
                        objNDE008.FieldAdequacy = fc["FieldAdequacy"];
                        objNDE008.Calibration = fc["Calibration"];
                        objNDE008.PostCleaning = fc["PostCleaning"];
                        objNDE008.TechniqueSheetNo = fc["TechniqueSheetNo"];
                        objNDE008.MTConsuNo = fc["txtMTConsuNo"];
                        db.NDE008.Add(objNDE008);
                        db.SaveChanges();

                        objResponseMsgWithStatus.Key = true;
                        objResponseMsgWithStatus.Value = Convert.ToString(objNDE008.HeaderId);
                        objResponseMsgWithStatus.RevNo = Convert.ToString(objNDE008.RevNo);
                        objResponseMsgWithStatus.HeaderStatus = objNDE008.Status;
                        objResponseMsgWithStatus.Remarks = objNDE008.ReturnRemarks;
                        #endregion
                    }

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsgWithStatus.Key = false;
                objResponseMsgWithStatus.Value = ex.Message;
            }

            return Json(objResponseMsgWithStatus);
        }

        [HttpPost]
        public ActionResult DeleteHeader(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            NDE008 objNDE008 = new NDE008();
            try
            {
                if (headerId > 0)
                {
                    objNDE008 = db.NDE008.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    if (objNDE008 != null)
                    {
                        if (string.Equals(objNDE008.Status, clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue(), StringComparison.OrdinalIgnoreCase) && objNDE008.RevNo == 0)
                        {
                            db.NDE008.Remove(objNDE008);
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "Technique Deleted Successfully";
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Technique Cannot Deleted";
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Technique Do Not Exist";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion



        #region MT Technique Send For Approval

        [HttpPost]
        public ActionResult SendForApproval(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            NDE008 objNDE008 = new NDE008();
            try
            {
                if (headerId > 0)
                {
                    if (!IsapplicableForSubmit(headerId))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This record has been already submitted, Please refresh page.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    objNDE008 = db.NDE008.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    if (objNDE008 != null)
                    {
                        objNDE008.Status = clsImplementationEnum.NDETechniqueStatus.SentForApproval.GetStringValue();
                        objNDE008.SubmittedBy = objClsLoginInfo.UserName;
                        objNDE008.SubmittedOn = DateTime.Now;
                        db.SaveChanges();

                        #region Send Notification
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE2.GetStringValue(), objNDE008.Project, objNDE008.BU, objNDE008.Location, "Technique No: " + objNDE008.MTTechNo + " has come for your Approval", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/NDE/ApproveMTTechnique/ViewMTTechnique?headerID=" + objNDE008.HeaderId);
                        #endregion

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.PTTechniqueMeassage.SentForApproval.ToString();
                    }

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg);
        }

        private bool IsapplicableForSubmit(int headerId)
        {
            NDE008 objNDE008 = db.NDE008.Where(x => x.HeaderId == headerId).FirstOrDefault();
            if (objNDE008.Status == clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue() || objNDE008.Status == clsImplementationEnum.PTMTCTQStatus.Returned.GetStringValue())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IsapplicableForSave(int headerId)
        {
            NDE008 objNDE008 = db.NDE008.Where(x => x.HeaderId == headerId).FirstOrDefault();
            if (objNDE008.Status == clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue())
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        #endregion

        #region MT Technique Copy
        [HttpPost]
        public ActionResult CopyMTTechniquePartial()
        {
            var lstQMSProject = (from a in db.QMS010
                                 select new { a.QualityProject, QMSProjectDesc = a.QualityProject + " - " + a.ManufacturingCode }).ToList();
            ViewBag.QMSProject = new SelectList(lstQMSProject, "QMSProject", "QMSProjectDesc");
            return PartialView("~/Areas/NDE/Views/Shared/_CopyNDETechniquePartial.cshtml");
        }

        [HttpPost]
        public ActionResult CopyToDestination(int headerId, string destQMSProject)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            NDE008 objNDE008Src = new NDE008();
            NDE008 objNDE008Dest = new NDE008();

            try
            {
                string currentUser = objClsLoginInfo.UserName;

                var currentLoc = (from a in db.COM003
                                  join b in db.COM002 on a.t_loca equals b.t_dimx
                                  where b.t_dtyp == 1 && a.t_actv == 1
                                  && a.t_psno.Equals(currentUser, StringComparison.OrdinalIgnoreCase)
                                  select b.t_dimx).FirstOrDefault().ToString();

                if (headerId > 0)
                    objNDE008Src = db.NDE008.Where(i => i.HeaderId == headerId).FirstOrDefault();

                #region OldCode
                //if (!string.IsNullOrEmpty(destQMSProject))
                //{
                //    objNDE008Dest = db.NDE008.Where(i => i.QualityProject.Equals(destQMSProject, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(currentLoc, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                //    if (objNDE008Dest != null)
                //    {
                //        if (string.Equals(objNDE008Dest.Status, clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue()))
                //        {
                //            objResponseMsg = InsertOrUpdateMTTechnique(objNDE008Src, objNDE008Dest, true);
                //        }
                //        else
                //        {
                //            objResponseMsg.Key = false;
                //            objResponseMsg.Value = "You cannot copy to " + destQMSProject + " as it is in " + objNDE008Dest.Status + " Staus";
                //        }
                //    }
                //    else
                //    {
                //        string project = new NDEModels().GetQMSProject(destQMSProject).Project;
                //        objNDE008Dest = new NDE008();
                //        objNDE008Dest.QualityProject = destQMSProject;
                //        objNDE008Dest.Project = project;
                //        objNDE008Dest.BU = db.COM001.Where(i => i.t_cprj.Equals(project, StringComparison.OrdinalIgnoreCase)).FirstOrDefault().t_entu;
                //        objNDE008Dest.Location = currentLoc;

                //        var maxMTTechno = MaxMTTechNo(objNDE008Src.Method, currentLoc, destQMSProject);

                //        objNDE008Dest.MTTechNo = "MT/" + maxMTTechno.Method + "/" + maxMTTechno.DocumentNo.ToString("D3");
                //        objNDE008Dest.DocNo = maxMTTechno.DocumentNo;
                //        objResponseMsg = InsertOrUpdateMTTechnique(objNDE008Src, objNDE008Dest, false);
                //    }
                //}
                #endregion

                #region NewCode
                string project = new NDEModels().GetQMSProject(destQMSProject).Project;
                var BU = db.COM001.Where(i => i.t_cprj.Equals(project, StringComparison.OrdinalIgnoreCase)).FirstOrDefault().t_entu;

                var Category = new NDEModels().GetCategoryId("MT Method");
                if (Manager.isMethodExistForBULocation(BU, currentLoc, objNDE008Src.Method, Category))
                {

                    objNDE008Dest = new NDE008();
                    objNDE008Dest.QualityProject = destQMSProject;
                    objNDE008Dest.Project = project;
                    objNDE008Dest.BU = db.COM001.Where(i => i.t_cprj.Equals(project, StringComparison.OrdinalIgnoreCase)).FirstOrDefault().t_entu;
                    objNDE008Dest.Location = currentLoc;

                    var maxMTTechno = MaxMTTechNo(objNDE008Src.Method, currentLoc, destQMSProject);

                    objNDE008Dest.MTTechNo = "MT/" + maxMTTechno.Method + "/" + maxMTTechno.DocumentNo.ToString("D3");
                    objNDE008Dest.DocNo = maxMTTechno.DocumentNo;
                    objResponseMsg = InsertOrUpdateMTTechnique(objNDE008Src, objNDE008Dest, false);
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Technique Is Not Available For Destination Project!";
                }
                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message.ToString();
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region MT Technique History
        [HttpPost]
        public ActionResult GetHistoryMTTechniquePartial(int headerId)
        {
            NDE008 objNDE008 = new NDE008();
            if (headerId > 0)
                objNDE008 = db.NDE008.Where(i => i.HeaderId == headerId).FirstOrDefault();
            return PartialView("_GetHistoryMTTechniquePartial", objNDE008);
        }

        public ActionResult LoadMTTechniqueHistory(JQueryDataTableParamModel param, int headerId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;


                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "nde8.BU", "nde8.Location");
                whereCondition += " AND NDE8.HeaderId=" + headerId;
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += " and (ltrim(rtrim(nde8.BU))+'-'+bcom2.t_desc like '%" + param.sSearch + "%' or ltrim(rtrim(nde8.Location))+'-'+lcom2.t_desc like '%" + param.sSearch + "%' " +
                                            " or NDE8.QualityProject like '%" + param.sSearch + "%' or (NDE8.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%' " +
                                            " or MTTechNo like '%" + param.sSearch + "%' or RevNo like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%' " +
                                             " or TechniqueSheetNo like '%" + param.sSearch + "%'" + " or (ecom3.t_psno+'-'+ecom3.t_name) like '%" + param.sSearch + "%')";
                }
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstHeader = db.SP_NDE_MTTechnique_GetHistory(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstHeader
                          select new[] {
                        //Convert.ToString(a.Id),
                        //Convert.ToString(a.HeaderId),
                        Convert.ToString(a.QualityProject),
                        Convert.ToString(a.Project),
                        Convert.ToString(a.BU),
                        Convert.ToString(a.Location),
                        Convert.ToString(a.TechniqueSheetNo),
                        Convert.ToString(a.MTTechNo),
                        Convert.ToString("R" + a.RevNo),
                        a.CreatedBy,
                        Convert.ToString(a.CreatedOn !=null?(Convert.ToDateTime(a.CreatedOn)).ToString("dd/MM/yyyy"):""),
                        a.Status,
                        "<center>"
                      
                        +"<a href=\"" +WebsiteURL + "/NDE/MaintainMTTechnique/ViewHistory?id="+a.Id+"\" target=\"_blank\" title=\"View History\" style=\"cursor:pointer;\" name=\"btnAction\" id=\"btnViewHistory\"><i class=\"iconspace fa fa-eye\"></i></a>"
                        +"<a title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/NDE/MaintainMTTechnique/ShowTimeline?HeaderID="+Convert.ToInt32(a.Id)+"&isHistory=true');><i class='iconspace fa fa-clock-o'></i></a></center>"
                        //Convert.ToString(a.Id)
                        //a.EditedBy
                        //a.Status,
            };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter, AllowAnonymous]
        public ActionResult ViewHistory(int id)
        {
            NDE008_Log objNDE008 = new NDE008_Log();

            var location = db.COM003.Where(i => i.t_psno.Equals(objClsLoginInfo.UserName) && i.t_actv == 1).Select(i => i.t_loca).FirstOrDefault();
            var Location = db.COM002.Where(i => i.t_dtyp == 1 && i.t_dimx.Equals(location, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
            
            if (id > 0)
            {
                objNDE008 = db.NDE008_Log.Where(i => i.Id == id).FirstOrDefault();
                var BU = objNDE008.BU;
                NDEModels objNDEModels = new NDEModels();
                var QualityProj = objNDEModels.GetQMSProject(objNDE008.QualityProject);
                objNDE008.BU = db.COM002.Where(i => i.t_dtyp == 2 && i.t_dimx == objNDE008.BU).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
                ViewBag.QMSProject = QualityProj.QualityProject; //new SelectList(lstQMSProject, "QMSProject", "QMSProjectDesc", objNDE008.QualityProject);
                ViewBag.MfgCode = QualityProj.ManufacturingCode;
                ViewBag.Project = db.COM001.Where(i => i.t_cprj == objNDE008.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                ViewBag.Location = Location;
                ViewBag.MTMethod = objNDEModels.GetCategory(objNDE008.Method).CategoryDescription;
                ViewBag.MagnetizationTechnique = objNDEModels.GetCategory("Magnetization Technique", objNDE008.MagnetizationTechnique, BU, objNDE008.Location, true).CategoryDescription;
                ViewBag.MethodOfExamination = objNDEModels.GetCategory("Method Of Examination", objNDE008.MethodofExamination, BU, objNDE008.Location, true).CategoryDescription;
                ViewBag.Current = objNDEModels.GetCategory("Current", objNDE008.Current, BU, objNDE008.Location, true).CategoryDescription;
                ViewBag.MagneticParticleType = objNDEModels.GetCategory("Magnetic Particle Type", objNDE008.MagneticParticleType, BU, objNDE008.Location, true).CategoryDescription;
                ViewBag.ParticleColour = objNDEModels.GetCategory("Particle Colour", objNDE008.ParticleColour, BU, objNDE008.Location, true).CategoryDescription;
                ViewBag.CarrierMedium = objNDEModels.GetCategory("Carrier Medium", objNDE008.CarrierMedium, BU, objNDE008.Location, true).CategoryDescription;
                ViewBag.LightIntensity = objNDEModels.GetCategory("Light Intensity", objNDE008.LightIntensity, BU, objNDE008.Location, true).CategoryDescription;
                ViewBag.LightSource = objNDEModels.GetCategory("Light Source", objNDE008.LightSource, BU, objNDE008.Location, true).CategoryDescription;
                ViewBag.DemagnetizationUpto = objNDEModels.GetCategory("Demagnetization upto", objNDE008.Demagnetization, BU, objNDE008.Location, true).CategoryDescription;
                objNDE008.FieldAdequacy = objNDEModels.GetCategory("Field Adequacy", objNDE008.FieldAdequacy, BU, objNDE008.Location, true).CategoryDescription;
                objNDE008.Calibration = objNDEModels.GetCategory("Calibration", objNDE008.Calibration, BU, objNDE008.Location, true).CategoryDescription;
                objNDE008.PostCleaning = objNDEModels.GetCategory("Post Cleaning", objNDE008.PostCleaning, BU, objNDE008.Location, true).CategoryDescription;
            }
            return View(objNDE008);
        }
        #endregion

        #region MT Technique Common Functions

        [HttpPost]
        public ActionResult GetQMSProjectDetail(string QMSProject, string location)
        {
            ModelCategory objModelCategory = new ModelCategory();
            ModelNDETechnique objModelTechniqueNo = new ModelNDETechnique();
            if (!string.IsNullOrEmpty(QMSProject))
            {
                objModelTechniqueNo = new NDEModels().GetQMSProjectDetail(QMSProject);
            }

            string strBu = string.Empty;
            objModelCategory.project = objModelTechniqueNo.project;
            objModelCategory.BU = objModelTechniqueNo.BU;
            objModelCategory.ManufacturingCode = objModelTechniqueNo.ManufacturingCode;
            strBu = objModelTechniqueNo.BU.Split('-')[0];

            var lstMTMethod = Manager.GetSubCatagories("MT Method", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
            var lstMagnetizationTechnique = Manager.GetSubCatagories("Magnetization Technique", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            var lstMethodOfExamination = Manager.GetSubCatagories("Method Of Examination", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            var lstCurrent = Manager.GetSubCatagories("Current", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            var lstMagneticParticleType = Manager.GetSubCatagories("Magnetic Particle Type", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            var lstParticleColour = Manager.GetSubCatagories("Particle Colour", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            var lstCarrierMedium = Manager.GetSubCatagories("Carrier Medium", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            var lstDemagnetizationUpto = Manager.GetSubCatagories("Demagnetization upto", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            var lstLightIntensity = Manager.GetSubCatagories("Light Intensity", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            var lstLightSource = Manager.GetSubCatagories("Light Source", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            var lstFieldAdequacy = Manager.GetSubCatagories("Field Adequacy", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            var lstCalibration = Manager.GetSubCatagories("Calibration", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            var lstPostCleaning = Manager.GetSubCatagories("Post Cleaning", strBu, location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();

            objModelCategory.MTMethod = lstMTMethod.Any() ? lstMTMethod : null;
            objModelCategory.MagnetizationTechnique = lstMagnetizationTechnique.Any() ? lstMagnetizationTechnique : null;
            objModelCategory.MethodOfExamination = lstMethodOfExamination.Any() ? lstMethodOfExamination : null;
            objModelCategory.Current = lstCurrent.Any() ? lstCurrent.ToList() : null;
            objModelCategory.MagneticParticleType = lstMagneticParticleType.Any() ? lstMagneticParticleType : null;
            objModelCategory.ParticleColour = lstParticleColour.Any() ? lstParticleColour : null;
            objModelCategory.CarrierMedium = lstCarrierMedium.Any() ? lstCarrierMedium : null;
            objModelCategory.DemagnetizationUpto = lstDemagnetizationUpto.Any() ? lstDemagnetizationUpto : null;
            objModelCategory.LightIntensity = lstLightIntensity.Any() ? lstLightIntensity : null;
            objModelCategory.LightSource = lstLightSource.Any() ? lstLightSource : null;
            objModelCategory.FieldAdequacy = lstFieldAdequacy.Any() ? lstFieldAdequacy : null;
            objModelCategory.Calibration = lstCalibration.Any() ? lstCalibration : null;
            objModelCategory.PostCleaning = lstPostCleaning.Any() ? lstPostCleaning : null;

            if (lstMTMethod.Any() && lstMagnetizationTechnique.Any() && lstMethodOfExamination.Any() && lstCurrent.Any() && lstMagneticParticleType.Any() && lstParticleColour.Any() && lstCarrierMedium.Any() && lstDemagnetizationUpto.Any() && lstLightIntensity.Any() && lstLightSource.Any())
                objModelCategory.Key = true;
            else
            {
                List<string> lstCategory = new List<string>();
                if (!lstMTMethod.Any())
                    lstCategory.Add("MT Method");
                if (!lstMagnetizationTechnique.Any())
                    lstCategory.Add("Magnetization Technique");
                if (!lstMethodOfExamination.Any())
                    lstCategory.Add("Method Of Examination");
                if (!lstCurrent.Any())
                    lstCategory.Add("Current");
                if (!lstMagneticParticleType.Any())
                    lstCategory.Add("Magnetic Particle Type");
                if (!lstParticleColour.Any())
                    lstCategory.Add("Particle Colour");
                if (!lstCarrierMedium.Any())
                    lstCategory.Add("Carrier Medium");
                if (!lstDemagnetizationUpto.Any())
                    lstCategory.Add("Demagnetization upto");
                if (!lstLightIntensity.Any())
                    lstCategory.Add("Light Intensity");
                if (!lstLightSource.Any())
                    lstCategory.Add("Light Source");
                if (!lstFieldAdequacy.Any())
                    lstCategory.Add("Field Adequacy");
                if (!lstCalibration.Any())
                    lstCategory.Add("Calibration");
                if (!lstPostCleaning.Any())
                    lstCategory.Add("Post Cleaning");

                objModelCategory.Key = false;
                objModelCategory.Value = string.Join(",", lstCategory.ToList()) + " Category not exist for selected project/Quality Project, Please contact Admin";
            }

            return Json(objModelCategory);
        }

        [HttpPost]
        public ActionResult GetMaxMTTechNo(string method, string location, string QMSproject)
        {
            MethodResponse objMethodResponse = new MethodResponse();
            try
            {
                objMethodResponse = MaxMTTechNo(method, location, QMSproject);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objMethodResponse.Key = false;
                objMethodResponse.Method = ex.Message.ToString();
            }
            return Json(objMethodResponse);
        }

        public clsHelper.ResponseMsgWithStatus InsertOrUpdateMTTechnique(NDE008 objNDE008Src, NDE008 objNDE008Dest, bool isUpdate)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (isUpdate)
                {
                    objNDE008Dest.TechniqueSheetNo = objNDE008Dest.TechniqueSheetNo;
                    objNDE008Dest.MagnetizationTechnique = objNDE008Src.MagnetizationTechnique;
                    objNDE008Dest.MethodofExamination = objNDE008Src.MethodofExamination;
                    objNDE008Dest.Current = objNDE008Src.Current;
                    objNDE008Dest.MagneticParticleType = objNDE008Src.MagneticParticleType;
                    objNDE008Dest.ParticleColour = objNDE008Src.ParticleColour;
                    objNDE008Dest.CarrierMedium = objNDE008Src.CarrierMedium;
                    objNDE008Dest.PoleSpacing = objNDE008Src.PoleSpacing;
                    objNDE008Dest.Demagnetization = objNDE008Src.Demagnetization;
                    objNDE008Dest.LightIntensity = objNDE008Src.LightIntensity;
                    objNDE008Dest.LightSource = objNDE008Src.LightSource;
                    objNDE008Dest.OtherLightSource = objNDE008Src.OtherLightSource;
                    objNDE008Dest.SurfaceTemperature = objNDE008Src.SurfaceTemperature;
                    objNDE008Dest.MTProcedureNo = objNDE008Src.MTProcedureNo;
                    objNDE008Dest.PlannerRemarks = objNDE008Dest.PlannerRemarks;
                    objNDE008Dest.EditedBy = objClsLoginInfo.UserName;
                    objNDE008Dest.EditedOn = DateTime.Now;
                    objNDE008Dest.FieldAdequacy = objNDE008Dest.FieldAdequacy;
                    objNDE008Dest.Calibration = objNDE008Dest.Calibration;
                    objNDE008Dest.PostCleaning = objNDE008Dest.PostCleaning;
                    db.SaveChanges();
                    objResponseMsg.HeaderId = objNDE008Dest.HeaderId;
                    objResponseMsg.HeaderStatus = objNDE008Dest.MTTechNo;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Data Copied Successfully To Document No : " + objNDE008Dest.MTTechNo + ".";
                }
                else
                {
                    objNDE008Dest.TechniqueSheetNo = objNDE008Dest.TechniqueSheetNo;
                    objNDE008Dest.MagnetizationTechnique = objNDE008Src.MagnetizationTechnique;
                    objNDE008Dest.MethodofExamination = objNDE008Src.MethodofExamination;
                    objNDE008Dest.Current = objNDE008Src.Current;
                    objNDE008Dest.MagneticParticleType = objNDE008Src.MagneticParticleType;
                    objNDE008Dest.ParticleColour = objNDE008Src.ParticleColour;
                    objNDE008Dest.CarrierMedium = objNDE008Src.CarrierMedium;
                    objNDE008Dest.PoleSpacing = objNDE008Src.PoleSpacing;
                    objNDE008Dest.Demagnetization = objNDE008Src.Demagnetization;
                    objNDE008Dest.LightIntensity = objNDE008Src.LightIntensity;
                    objNDE008Dest.LightSource = objNDE008Src.LightSource;
                    objNDE008Dest.OtherLightSource = objNDE008Src.OtherLightSource;
                    objNDE008Dest.SurfaceTemperature = objNDE008Src.SurfaceTemperature;
                    objNDE008Dest.MTProcedureNo = objNDE008Src.MTProcedureNo;
                    objNDE008Dest.PlannerRemarks = objNDE008Src.PlannerRemarks;
                    objNDE008Dest.Method = objNDE008Src.Method;
                    objNDE008Dest.RevNo = 0;
                    objNDE008Dest.Status = clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue();
                    objNDE008Dest.CreatedBy = objClsLoginInfo.UserName;
                    objNDE008Dest.CreatedOn = DateTime.Now;
                    objNDE008Dest.FieldAdequacy = objNDE008Dest.FieldAdequacy;
                    objNDE008Dest.Calibration = objNDE008Dest.Calibration;
                    objNDE008Dest.PostCleaning = objNDE008Dest.PostCleaning;

                    db.NDE008.Add(objNDE008Dest);
                    db.SaveChanges();
                    objResponseMsg.HeaderId = objNDE008Dest.HeaderId;
                    objResponseMsg.HeaderStatus = objNDE008Dest.MTTechNo;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Data Copied Successfully To Document No : " + objResponseMsg.HeaderStatus + " of " + objNDE008Dest.Project + " Project.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.HeaderId = 0;
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return objResponseMsg;
        }

        public MethodResponse MaxMTTechNo(string method, string location, string QMSproject)
        {
            MethodResponse objMethodResponse = new MethodResponse();
            try
            {
                string methodNm = new NDEModels().GetCategory(method).Code;//db.GLB002.Where(i => i.cor == method).Select(i => i.Code).FirstOrDefault();
                if (isMTTechniqueNoExist(method, location, QMSproject))
                {
                    var PTTechniqueDocNo = (from a in db.NDE008
                                            where a.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && a.Method.Equals(method, StringComparison.OrdinalIgnoreCase) && a.QualityProject.Equals(QMSproject, StringComparison.OrdinalIgnoreCase)
                                            select a).Max(a => a.DocNo);
                    objMethodResponse.Key = true;
                    objMethodResponse.Method = methodNm;
                    objMethodResponse.DocumentNo = Convert.ToInt32(PTTechniqueDocNo + 1);
                }
                else
                {
                    objMethodResponse.Key = true;
                    objMethodResponse.Method = methodNm;
                    objMethodResponse.DocumentNo = 1;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objMethodResponse.Key = false;
                objMethodResponse.Method = ex.Message.ToString();
            }
            return objMethodResponse;
        }

        public bool isMTTechniqueNoExist(string method, string location, string QMSproject)
        {
            bool flag = false;
            try
            {
                var lstPTTechnique = (from a in db.NDE008
                                      where a.Location.Equals(location, StringComparison.OrdinalIgnoreCase)
                                      select a).ToList();

                if (lstPTTechnique.Any())
                {
                    if (!string.IsNullOrEmpty(QMSproject) && !string.IsNullOrEmpty(method))
                    {
                        var methodslist = lstPTTechnique.Where(i => i.QualityProject.Equals(QMSproject, StringComparison.OrdinalIgnoreCase) && i.Method == method);
                        if (methodslist != null && methodslist.Count() > 0)
                        {
                            flag = true;
                        }
                        //if (lstPTTechnique.Where(i => i.QualityProject.Equals(QMSproject, StringComparison.OrdinalIgnoreCase) && i.Method.Equals(method, StringComparison.OrdinalIgnoreCase)).ToList().Any())
                        //    flag = true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return flag;
        }

        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", bool disabled = false)
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (disabled)
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='disabledicon " + className + "'" + " ></i>";
            }
            else
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='iconspace " + className + "'" + onClickEvent + " ></i>";
            }

            return htmlControl;
        }
        #endregion

        public ActionResult ShowTimeline(int HeaderId, int LineId = 0, bool isHistory = false)
        {
            TimelineViewModel model = new TimelineViewModel();
            if (isHistory)
            {
                NDE008_Log objNDE008 = db.NDE008_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.Title = "NDETECHNIQUE";
                model.ApprovedBy = Manager.GetUserNameFromPsNo(objNDE008.ApprovedBy);
                model.ApprovedOn = objNDE008.ApprovedOn;
                model.SubmittedBy = Manager.GetUserNameFromPsNo(objNDE008.SubmittedBy);
                model.SubmittedOn = objNDE008.SubmittedOn;
                model.CreatedBy = Manager.GetUserNameFromPsNo(objNDE008.CreatedBy);
                model.CreatedOn = objNDE008.CreatedOn;
                model.ReturnedBy = Manager.GetUserNameFromPsNo(objNDE008.ReturnedBy);
                model.ReturnedOn = objNDE008.ReturnedOn;

            }
            else
            {
                NDE008 objNDE008 = db.NDE008.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.Title = "NDETECHNIQUE";
                model.ApprovedBy = Manager.GetUserNameFromPsNo(objNDE008.ApprovedBy);
                model.ApprovedOn = objNDE008.ApprovedOn;
                model.SubmittedBy = Manager.GetUserNameFromPsNo(objNDE008.SubmittedBy);
                model.SubmittedOn = objNDE008.SubmittedOn;
                model.CreatedBy = Manager.GetUserNameFromPsNo(objNDE008.CreatedBy);
                model.CreatedOn = objNDE008.CreatedOn;
                model.ReturnedBy = Manager.GetUserNameFromPsNo(objNDE008.ReturnedBy);
                model.ReturnedOn = objNDE008.ReturnedOn;
            }
            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_NDE_GetMTTechniqueHeader(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      MTTechNo = li.TechniqueSheetNo,
                                      TechniqueSheetNo = Convert.ToString(li.MTTechNo),
                                      RevNo = "R" + li.RevNo,
                                      CreatedBy = li.CreatedBy,
                                      EditedBy = li.EditedBy,
                                      Status = li.Status,
                                      CreatedOn = li.CreatedOn,
                                      EditedOn = li.EditedOn
                                  }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else
                {
                    var lst = db.SP_NDE_MTTechnique_GetHistory(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      MTTechNo = Convert.ToString(li.TechniqueSheetNo),
                                      DocumnetNo = li.MTTechNo,
                                      RevNo = "R" + li.RevNo,
                                      CreatedBy = li.CreatedBy,
                                      EditedBy = li.EditedBy,
                                      Status = li.Status,
                                      CreatedOn = li.CreatedOn,
                                      EditedOn = li.EditedOn
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult GetMTConsumNumber(string methodtext)
        {
            string method = methodtext.Substring(0, methodtext.IndexOf("-"));
            method = method.Trim();
            NDE020 objNDE020 = new NDE020();
            var data = (from o in db.NDE020
                        where o.Method == method
                        select new
                        {
                            MTConsuNo = o.MTConsuNo
                        }).Distinct().ToList();

            //DataTable dt = ToDataTable(data);
            List<SelectListItem> ptcon = new List<SelectListItem>();
            if (data.Count > 0)
            {
                foreach (var item in data)
                {

                    ptcon.Add(new SelectListItem { Text = item.MTConsuNo.ToString(), Value = item.MTConsuNo.ToString() });
                }
            }


            return Json(new SelectList(ptcon, "Value", "Text"));

        }
    }
}