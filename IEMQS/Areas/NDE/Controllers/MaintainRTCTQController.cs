﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.NDE.Controllers
{
    public class MaintainRTCTQController : clsBase
    {
        /// <summary>
        /// Made By Dharmesh Vasani
        /// </summary>
        /// <returns></returns>

        #region header index
        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        private bool IsapplicableForSubmit(int headerId)
        {
            NDE003 objNDE003 = db.NDE003.Where(x => x.HeaderId == headerId).FirstOrDefault();
            if (objNDE003.Status == clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue() || objNDE003.Status == clsImplementationEnum.PTMTCTQStatus.Returned.GetStringValue())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IsapplicableForSave(int headerId)
        {
            NDE003 objNDE003 = db.NDE003.Where(x => x.HeaderId == headerId).FirstOrDefault();
            if (objNDE003.Status == clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue())
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        [HttpPost]
        public ActionResult LoadRTCTQListDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_RTCTQListDataPartial");
        }
        [HttpPost]
        public JsonResult LoadRTCTQHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.RTCTQStatus.DRAFT.GetStringValue() + "','" + clsImplementationEnum.RTCTQStatus.Returned.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "1=1";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (ltrim(rtrim(nde3.BU))+'-'+bcom2.t_desc like '%" + param.sSearch +
                                "%' or ltrim(rtrim(nde3.Location))+'-'+lcom2.t_desc like '%" + param.sSearch +
                                "%' or (nde3.Project+'-'+com1.t_dsca) like '%" + param.sSearch +
                                "%' or CTQNo like '%" + param.sSearch +
                                "%' or RevNo like '%" + param.sSearch +
                                "%' or ApplicableSpecification like '%" + param.sSearch +
                                "%' or AcceptanceStandard like '%" + param.sSearch +
                                "%' or ASMECodeStamp like '%" + param.sSearch +
                                "%' or TPIName like '%" + param.sSearch +
                                "%' or NotificationRequired like '%" + param.sSearch +
                                "%' or ThicknessOfJob like '%" + param.sSearch +
                                "%' or SizeDimension like '%" + param.sSearch +
                                "%' or NDERemarks like '%" + param.sSearch +
                                "%' or nde3.CreatedBy +' - '+com003c.t_name like '%" + param.sSearch +
                                "%' or ManufacturingCode like '%" + param.sSearch +
                                "%' or Status like '%" + param.sSearch + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "nde3.BU", "nde3.Location");

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_RTCTQ_GETHEADER(StartIndex, EndIndex, strSortOrder, strWhere).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.BU),
                               Convert.ToString(uc.Location),
                               Convert.ToString(uc.CTQNo),
                               Convert.ToString(uc.ManufacturingCode),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn !=null?(Convert.ToDateTime(uc.CreatedOn)).ToString("dd/MM/yyyy"):""),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.ApplicableSpecification),
                               Convert.ToString(uc.AcceptanceStandard),
                               (uc.ASMECodeStamp == true?"<label class=\"mt-checkbox mt-checkbox-outline\"><input checked=\"checked\" disabled=\"disabled\" type = \"checkbox\" value=\"\" /><span></span></label>":"<label class=\"mt-checkbox mt-checkbox-outline\"><input disabled=\"disabled\" type = \"checkbox\" value=\"\" /><span></span></label>"),//Convert.ToString(uc.ASMECodeStamp),
                               Convert.ToString(uc.TPIName),
                              Convert.ToString(uc.NotificationRequired == true ? "Yes" : uc.NotificationRequired == false ? "No" : ""),
                               Convert.ToString(uc.ThicknessOfJob),
                               Convert.ToString(uc.SizeDimension),
                               Convert.ToString(uc.NDERemarks),
                               "<nobr><center>"
                                  + Helper.GenerateActionIcon(uc.HeaderId, "View", "View Detail", "fa fa-eye", "", WebsiteURL + "/NDE/MaintainRTCTQ/AddUpdateRTCTQ?HeaderID="+uc.HeaderId,false)
                                  + (uc.Status.ToLower() == clsImplementationEnum.CommonStatus.DRAFT.GetStringValue().ToLower() && uc.RevNo == 0 ? "<i id=\"btnDelete\" name=\"btnAction\" style=\"cursor:pointer;\" Title=\"Delete Record\" class=\"iconspace fa fa-trash-o\" onclick=\"DeleteHeader('" + uc.HeaderId + "')\"></i>" : "<i id=\"btnDelete\" name=\"btnAction\" style=\"cursor:pointer;\" Title=\"Delete Record\" class=\"disabledicon fa fa-trash-o\" ></i>")+
                               "<a title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/NDE/MaintainRTCTQ/ShowTimeline?HeaderID=" +Convert.ToInt32(uc.HeaderId)+"');><i class='iconspace fa fa-clock-o'></i></a>"+""+((uc.RevNo > 0 || uc.Status.ToLower() == clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue().ToLower()) ?"<i title='History' onclick=History('"+Convert.ToInt32(uc.HeaderId)+"'); style = 'cursor:pointer;' class='iconspace fa fa-history'></i>" : "<i title='History' style = 'cursor:pointer;' class='disabledicon fa fa-history'></i>" )+""+"</center></nobr>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        //delete header (in draft and R0)
        [HttpPost]
        public ActionResult DeleteHeader(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                NDE003 objNDE003 = db.NDE003.Where(x => x.HeaderId == headerId).FirstOrDefault();
                List<NDE004> objNDE004 = db.NDE004.Where(x => x.HeaderId == headerId).ToList();
                if (objNDE003 != null)
                {
                    if (objNDE004.Count > 0)
                    {
                        db.NDE004.RemoveRange(objNDE004);
                        db.SaveChanges();
                    }
                    db.NDE003.Remove(objNDE003);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Add / update form 
        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult AddUpdateRTCTQ(int HeaderID = 0)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("NDE003");
            ViewBag.HeaderID = HeaderID;
            return View();
        }

        [HttpPost]
        public ActionResult LoadRTCTQAddUpdateFormPartial(int HeaderID)
        {
            NDE003 objNDE003 = new NDE003();

            string user = objClsLoginInfo.UserName;
            string BU = string.Join(",", db.ATH001.Where(i => i.Employee == user).Select(i => i.BU).ToList());
            List<Projects> project = Manager.getProjectsByUser(user);
            string[] location = db.ATH001.Where(i => i.Employee == user).Select(i => i.Location).ToArray();

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.lstyesno = lstyesno.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

            //var lstLocation = (from a in db.COM002
            //                   where a.t_dtyp == 1 && location.Contains(a.t_dimx)
            //                   select new { a.t_dimx, Location = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();

            //List<GLB002> lstGLB002 = Manager.GetSubCatagories("Manufacturing Code").ToList();
            //ViewBag.lstManufacturingCode = lstGLB002.AsEnumerable().Select(x => new SelectListItem() { Text = (x.Code + "-" + x.Description).ToString(), Value = x.Id.ToString() }).ToList();

            if (HeaderID > 0)
            {
                objNDE003 = db.NDE003.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
                NDEModels objNDEModels = new NDEModels();
                ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objNDE003.ManufacturingCode, objNDE003.BU, objNDE003.Location, true).CategoryDescription;
                string strBU = db.COM001.Where(i => i.t_cprj == objNDE003.Project).FirstOrDefault().t_entu;
                var BUDescription = (from a in db.COM002
                                     where a.t_dtyp == 2 && a.t_dimx == strBU
                                     select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
                objNDE003.BU = BUDescription.BUDesc;
                var locDescription = (from a in db.COM002
                                      where a.t_dtyp == 1 && a.t_dimx == objNDE003.Location
                                      select new { Location = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
                objNDE003.Location = locDescription.Location;

                var projectDetails = db.COM001.Where(x => x.t_cprj == objNDE003.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                ViewBag.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
            }
            else
            {
                objNDE003.Status = clsImplementationEnum.RTCTQStatus.DRAFT.GetStringValue();
                objNDE003.RevNo = 0;
                var locDescription = (from a in db.COM003
                                      join b in db.COM002 on a.t_loca equals b.t_dimx
                                      where b.t_dtyp == 1 && a.t_actv == 1
                                      && a.t_psno.Equals(user, StringComparison.OrdinalIgnoreCase)
                                      select b.t_dimx + "-" + b.t_desc).FirstOrDefault();

                objNDE003.Location = locDescription;
            }
            //ViewBag.Project = new SelectList(project, "projectCode", "projectDescription");
            //  objNDE003.Location = lstLocation.Location;
            return PartialView("_RTCTQAddUpdateFormPartial", objNDE003);
        }

        [HttpPost]
        public ActionResult SaveHeader(NDE003 nde003, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                if (fc["txtNotificationRequired"].ToString().ToLower() == "yes")
                {
                    nde003.NotificationRequired = true;
                }
                else
                {
                    nde003.NotificationRequired = false;
                }

                if (nde003.HeaderId > 0)
                {
                    if (!IsapplicableForSave(nde003.HeaderId))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This record has been already submitted, Please refresh page.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }

                    NDE003 objNDE003 = db.NDE003.Where(x => x.HeaderId == nde003.HeaderId).FirstOrDefault();
                    objNDE003.ApplicableSpecification = nde003.ApplicableSpecification;
                    objNDE003.AcceptanceStandard = nde003.AcceptanceStandard;
                    objNDE003.ASMECodeStamp = nde003.ASMECodeStamp;
                    objNDE003.TPIName = nde003.TPIName;
                    objNDE003.NotificationRequired = nde003.NotificationRequired;
                    objNDE003.ThicknessOfJob = nde003.ThicknessOfJob;
                    objNDE003.SizeDimension = nde003.SizeDimension;
                    objNDE003.ManufacturingCode = nde003.ManufacturingCode;
                    objNDE003.NDERemarks = nde003.NDERemarks;
                    if (objNDE003.Status == clsImplementationEnum.RTCTQStatus.Approved.GetStringValue())
                    {
                        objNDE003.RevNo = Convert.ToInt32(objNDE003.RevNo) + 1;
                        objNDE003.ReturnRemarks = null;
                        objNDE003.SubmittedBy = null;
                        objNDE003.SubmittedOn = null;
                        objNDE003.ApprovedOn = null;
                        foreach (var item in objNDE003.NDE004)
                        {
                            item.ReturnRemark = null;
                            item.ReturnedBy = null;
                            item.ReturnedOn = null;
                        }
                        objNDE003.Status = clsImplementationEnum.RTCTQStatus.DRAFT.GetStringValue();
                    }
                    objNDE003.EditedBy = objClsLoginInfo.UserName;
                    objNDE003.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objNDE003.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.RTCTQMessages.Update.ToString();
                    objResponseMsg.Remarks = objNDE003.ReturnRemarks;
                }
                else
                {
                    NDE003 objNDE003 = new NDE003();
                    objNDE003.Project = nde003.Project;
                    objNDE003.BU = nde003.BU.Split('-')[0].ToString();
                    objNDE003.Location = nde003.Location.Split('-')[0].ToString();
                    objNDE003.RevNo = 0;
                    objNDE003.CTQNo = nde003.CTQNo;
                    objNDE003.Status = nde003.Status;
                    objNDE003.ApplicableSpecification = nde003.ApplicableSpecification;
                    objNDE003.AcceptanceStandard = nde003.AcceptanceStandard;
                    objNDE003.ASMECodeStamp = nde003.ASMECodeStamp;
                    objNDE003.TPIName = nde003.TPIName;
                    objNDE003.ManufacturingCode = nde003.ManufacturingCode;
                    objNDE003.NotificationRequired = nde003.NotificationRequired;
                    objNDE003.ThicknessOfJob = nde003.ThicknessOfJob;
                    objNDE003.SizeDimension = nde003.SizeDimension;
                    objNDE003.NDERemarks = nde003.NDERemarks;
                    objNDE003.CreatedBy = objClsLoginInfo.UserName;
                    objNDE003.CreatedOn = DateTime.Now;
                    db.NDE003.Add(objNDE003);
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objNDE003.HeaderId;
                    var lstDescription = Manager.GetSubCatagories("NDE-RTCTQ", objNDE003.BU, objNDE003.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
                    foreach (var item in lstDescription)
                    {
                        NDE004 objNDE004 = new NDE004();
                        objNDE004.Description = item.CategoryDescription;
                        objNDE004.HeaderId = objNDE003.HeaderId;
                        objNDE004.Project = objNDE003.Project;
                        objNDE004.BU = objNDE003.BU;
                        objNDE004.Location = objNDE003.Location;
                        objNDE004.RevNo = 0;
                        objNDE004.LineRevNo = 0;
                        objNDE004.CreatedBy = objClsLoginInfo.UserName;
                        objNDE004.CreatedOn = DateTime.Now;
                        objNDE004.Status = clsImplementationEnum.CommonStatus.Added.GetStringValue();
                        db.NDE004.Add(objNDE004);
                        db.SaveChanges();
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.RTCTQMessages.Insert.ToString();
                    objResponseMsg.Remarks = objNDE003.ReturnRemarks;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Lines
        [HttpPost]
        public ActionResult GetRTCTQLinesForm(string projCode, string BU, string location, string ctqNo, int LineId)
        {
            NDE004 objNDE004 = new NDE004();
            NDE003 objNDE003 = db.NDE003.Where(x => x.Project == projCode).FirstOrDefault();

            List<GLB002> lstGLB002 = Manager.GetSubCatagories("Qualification Exam", objNDE003.BU, objNDE003.Location).ToList();
            ViewBag.lstQualificationExam = lstGLB002.AsEnumerable().Select(x => new BULocWiseCategoryModel { CatDesc = (x.Code + "-" + x.Description).ToString(), CatID = x.Id.ToString() }).ToList();

            if (LineId > 0)
            {
                objNDE004 = db.NDE004.Where(x => x.LineId == LineId).FirstOrDefault();
                projCode = objNDE004.Project;
                objNDE004.NDE003.RevNo = objNDE004.RevNo;
            }
            else
            {
                ViewBag.CTQNo = projCode + "_CTQ_RT";
                objNDE004.RevNo = 0;

            }
            objNDE004.HeaderId = objNDE003.HeaderId;
            string user = objClsLoginInfo.UserName;

            var projectDetails = db.COM001.Where(x => x.t_cprj == objNDE003.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
            string projectDesc = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
            //string projectDesc = Manager.getProjectsByUser(user).Where(x => x.projectCode == projCode).Select(x => x.projectDescription).FirstOrDefault();

            BU = db.COM001.Where(i => i.t_cprj == projCode).FirstOrDefault().t_entu;
            var BUDescription = (from a in db.COM002
                                 where a.t_dtyp == 2 && a.t_dimx == BU
                                 select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();

            ViewBag.Project = projectDesc;
            ViewBag.BU = BUDescription.BUDesc;
            ViewBag.Location = location;
            ViewBag.CTQNo = projCode + "_CTQ_RT";
            return PartialView("_GetRTCTQLinesForm", objNDE004);
        }

        [HttpPost]
        public ActionResult UpdateData(int headerId, string columnName, string columnValue, int LineId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (!string.IsNullOrEmpty(columnName))
                {
                    if (headerId > 0)
                    {
                        if (!IsapplicableForSubmit(headerId))
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "This record has been already submitted,Please refresh page.";
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }
                    }
                    db.SP_NDE_RT_CTQ_UPDATE_COLUMN(LineId, columnName, columnValue);
                    ReviseData(headerId);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                    objResponseMsg.LineStatus = clsImplementationEnum.CommonStatus.Modified.GetStringValue();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReviseData(int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                NDE003 objNDE003 = db.NDE003.Where(x => x.HeaderId == headerId).FirstOrDefault();
                //NDE004 objNDE004 = db.NDE004.Where(x => x.HeaderId == headerId).FirstOrDefault();
                if (objNDE003.Status == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue())
                {
                    objNDE003.RevNo = Convert.ToInt32(objNDE003.RevNo) + 1;
                    objNDE003.ReturnRemarks = null;
                    objNDE003.SubmittedBy = null;
                    objNDE003.SubmittedOn = null;
                    objNDE003.ApprovedOn = null;
                    objNDE003.Status = clsImplementationEnum.UTCTQStatus.DRAFT.GetStringValue();
                    objNDE003.EditedBy = objClsLoginInfo.UserName;
                    objNDE003.EditedOn = DateTime.Now;
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                objResponseMsg.Remarks = objNDE003.ReturnRemarks;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult LoadRTCTQHeaderLineData(JQueryDataTableParamModel param)
        {
            try
            {
                NDEModels nde = new NDEModels();
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();
                string[] arrayCTQStatus = { clsImplementationEnum.RTCTQStatus.DRAFT.GetStringValue(), clsImplementationEnum.RTCTQStatus.Returned.GetStringValue() };

                string[] columnName = { "Description", "SpecificationNo", "SpecificationWithClauseNo", "SpecificationRequirement", "ToBeDiscussed", "CriticalToQuality", "CTQRequirement", "ReturnRemark" };

                if (param.sSearch == "yes" || param.sSearch == "Yes" || param.sSearch == "YES")
                {
                    param.sSearch = "1";
                }
                if (param.sSearch == "no" || param.sSearch == "NO" || param.sSearch == "No")
                {
                    param.sSearch = "0";
                }
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);

                List<SelectListItem> BoolenList = new List<SelectListItem>() { new SelectListItem { Text = "Yes", Value = "True" }, new SelectListItem { Text = "No", Value = "False" } };

                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName);
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_GET_RT_HEADER_LINES
                                (
                               StartIndex,
                               EndIndex,
                               strSortOrder,
                               strWhere
                                ).ToList();
                int newRecordId = 0;
                var newRecord = new[] {
                                    "",
                                    Helper.GenerateTextArea(newRecordId, "Description","","",false,"width:200px;","100"),
                                    Helper.GenerateTextArea(newRecordId, "SpecificationNo","","",false,"width:200px;","50"),
                                    Helper.GenerateTextArea(newRecordId, "SpecificationWithClauseNo","","",false,"width:200px;","50"),
                                    Helper.GenerateDropdown(newRecordId, "ToBeDiscussed", new SelectList(BoolenList, "Value", "Text"), "Select"),
                                    Helper.GenerateTextArea(newRecordId, "SpecificationRequirement","","",false,"width:200px;","100"),
                                    Helper.GenerateDropdown(newRecordId, "CriticalToQuality", new SelectList(BoolenList, "Value", "Text"),"Select","",false,"width:100px;"),
                                    Helper.GenerateTextbox(newRecordId, "CTQRequirement", "","",false,"width:200px;","100"),
                                    "",
                                    clsImplementationEnum.CommonStatus.Added.GetStringValue(),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Add Rocord", "fa fa-save", "SaveNewRecord();" ),
                                    "",
                                    Helper.GenerateHidden(newRecordId, "HeaderId", param.CTQHeaderId),
                                };

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.Description),
                                Helper.GenerateTextArea(uc.LineId, "SpecificationNo",    Convert.ToString(uc.SpecificationNo),"UpdateData(this, "+ uc.HeaderId +","+uc.LineId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Deleted.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.CTQCompileStatus,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"width:200px;","50"),
                                Helper.GenerateTextArea(uc.LineId, "SpecificationWithClauseNo",    Convert.ToString(uc.SpecificationWithClauseNo),"UpdateData(this, "+ uc.HeaderId +","+uc.LineId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Deleted.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.CTQCompileStatus,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"width:200px;","50"),
                                Helper.GenerateDropdown(uc.LineId, "ToBeDiscussed", new SelectList(BoolenList, "Value", "Text", Convert.ToString(uc.ToBeDiscussed)),"Select","UpdateData(this, "+ uc.HeaderId +","+uc.LineId+",false);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Deleted.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.CTQCompileStatus,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),""),
                                Helper.GenerateTextArea(uc.LineId, "SpecificationRequirement",    Convert.ToString(uc.SpecificationRequirement),"UpdateData(this, "+ uc.HeaderId +","+uc.LineId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Deleted.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.CTQCompileStatus,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"width:200px;","100"),
                                Helper.GenerateDropdown(uc.LineId, "CriticalToQuality", new SelectList(BoolenList, "Value", "Text", Convert.ToString(uc.CriticalToQuality)),"Select","UpdateData(this, "+ uc.HeaderId +","+uc.LineId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Deleted.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.CTQCompileStatus,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),""),
                                Helper.GenerateTextArea(uc.LineId, "CTQRequirement",    Convert.ToString(uc.CTQRequirement),"UpdateData(this, "+ uc.HeaderId +","+uc.LineId+",false);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Deleted.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.CTQCompileStatus,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"width:200px;","100"),
                                Convert.ToString(uc.ReturnRemark),
                                Convert.ToString(uc.Status),
                                "<center style=\"display:inline;\">"
                                +Helper.HTMLActionString(uc.LineId,"Delete","Delete Record","fa fa-trash-o","DeleteRecord(" + uc.LineId  +")","",string.Equals(uc.Status, clsImplementationEnum.QCPStatus.Deleted.GetStringValue(), StringComparison.OrdinalIgnoreCase) || string.Equals(param.CTQCompileStatus, clsImplementationEnum.QCPStatus.SentForApproval.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                                +"<a class='' href='javascript:void(0)' onclick=ShowTimeline('/NDE/MaintainRTCTQ/ShowTimeline?HeaderID="+Convert.ToInt32(uc.HeaderId)+"&LineId="+Convert.ToInt32(uc.LineId)+"');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a></center>",
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId),

                           }).ToList();
                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        
        [HttpPost]
        public ActionResult SaveNewLines(FormCollection fc)
        {
            int newRowIndex = 0;
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int headerId = Convert.ToInt32(fc["HeaderId" + newRowIndex]);
                if (headerId > 0)
                {
                    if (!IsapplicableForSubmit(headerId))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This record has been already submitted, Please refresh page.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                }
                NDE003 objNDE003 = db.NDE003.Where(x => x.HeaderId == headerId).FirstOrDefault();
                NDE004 objNDE004 = new NDE004();
                objNDE004.HeaderId = objNDE003.HeaderId;
                objNDE004.Project = objNDE003.Project;
                objNDE004.BU = objNDE003.BU;
                objNDE004.Location = objNDE003.Location;
                objNDE004.Description = fc["Description" + newRowIndex];
                objNDE004.SpecificationWithClauseNo = fc["SpecificationWithClauseNo" + newRowIndex];
                objNDE004.SpecificationNo = fc["SpecificationNo" + newRowIndex];
                objNDE004.SpecificationRequirement = fc["SpecificationRequirement" + newRowIndex];
                if (fc["ToBeDiscussed" + newRowIndex] != null && fc["ToBeDiscussed" + newRowIndex].ToString() != string.Empty)
                {
                    objNDE004.ToBeDiscussed = Convert.ToBoolean(fc["ToBeDiscussed" + newRowIndex]);
                }
                objNDE004.CTQRequirement = fc["CTQRequirement" + newRowIndex];
                objNDE004.CriticalToQuality = Convert.ToBoolean(fc["CriticalToQuality" + newRowIndex]);
                objNDE004.RevNo = 0;
                objNDE004.LineRevNo = 0;
                objNDE004.Status = clsImplementationEnum.CommonStatus.Added.GetStringValue();
                objNDE004.CreatedBy = objClsLoginInfo.UserName;
                objNDE004.CreatedOn = DateTime.Now;
                if (objNDE003.Status == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue())
                {
                    objNDE003.RevNo = Convert.ToInt32(objNDE003.RevNo) + 1;
                    objNDE003.ReturnRemarks = null;
                    objNDE003.SubmittedBy = null;
                    objNDE003.SubmittedOn = null;
                    objNDE003.ApprovedOn = null;
                    objNDE003.Status = clsImplementationEnum.UTCTQStatus.DRAFT.GetStringValue();
                    objNDE003.EditedBy = objClsLoginInfo.UserName;
                    objNDE003.EditedOn = DateTime.Now;
                }
                db.NDE004.Add(objNDE004);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
                objResponseMsg.Remarks = objNDE003.ReturnRemarks;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
      
        [HttpPost]
        public ActionResult SaveRTCTQLines(NDE004 nde004, FormCollection fc)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {

                NDE003 objNDE003 = db.NDE003.Where(x => x.HeaderId == nde004.HeaderId).FirstOrDefault();
                if (nde004.LineId > 0)
                {
                    NDE004 objNDE004 = db.NDE004.Where(x => x.LineId == nde004.LineId).FirstOrDefault();
                    if (objNDE004.Status == clsImplementationEnum.RTCTQStatus.Approved.GetStringValue())
                    {
                        objNDE004.RevNo = Convert.ToInt32(objNDE004.RevNo) + 1;
                        objNDE004.LineRevNo = Convert.ToInt32(objNDE004.LineRevNo) + 1;
                        objNDE003.RevNo = Convert.ToInt32(objNDE003.RevNo) + 1;
                        objNDE003.ReturnRemarks = null;
                        objNDE003.SubmittedBy = null;
                        objNDE003.SubmittedOn = null;
                        objNDE003.ApprovedOn = null;
                    }
                    objNDE003.Status = clsImplementationEnum.RTCTQStatus.DRAFT.GetStringValue();
                    objNDE004.Status = clsImplementationEnum.CommonStatus.Modified.GetStringValue();
                    objNDE004.HeaderId = nde004.HeaderId;
                    objNDE004.Project = objNDE003.Project;
                    objNDE004.BU = objNDE003.BU;
                    objNDE004.Location = objNDE003.Location;
                    objNDE004.SpecificationWithClauseNo = nde004.SpecificationWithClauseNo;
                    objNDE004.SpecificationRequirement = nde004.SpecificationRequirement;
                    objNDE004.CriticalToQuality = nde004.CriticalToQuality;
                    objNDE004.CTQRequirement = nde004.CTQRequirement;
                    objNDE004.ToBeDiscussed = nde004.ToBeDiscussed;
                    objNDE004.QualificationForExam = nde004.QualificationForExam;
                    objNDE004.ApprovalRequired = nde004.ApprovalRequired;
                    objNDE004.ImageQualityIndicator = nde004.ImageQualityIndicator;
                    objNDE004.FilmSelection = nde004.FilmSelection;
                    objNDE004.IntensifyingScreen = nde004.IntensifyingScreen;
                    objNDE004.RadiationSource = nde004.RadiationSource;
                    objNDE004.Density = nde004.Density;
                    objNDE004.RadiographyAcceptanceCriteria = nde004.RadiographyAcceptanceCriteria;
                    objNDE004.SpecialTechniqueforRadiography = nde004.SpecialTechniqueforRadiography;
                    objNDE004.FilmIdentification = nde004.FilmIdentification;
                    objNDE004.WeldCoverage = nde004.WeldCoverage;
                    objNDE004.Overlap = nde004.Overlap;
                    objNDE004.SFD = nde004.SFD;
                    objNDE004.SpotLength = nde004.SpotLength;
                    objNDE004.FilmRetentionPeriod = nde004.FilmRetentionPeriod;
                    objNDE004.FilmDigitization = nde004.FilmDigitization;
                    objNDE004.ProcedureDemonstration = nde004.ProcedureDemonstration;
                    objNDE004.Others = nde004.Others;
                    objNDE004.EditedBy = objClsLoginInfo.UserName;
                    objNDE004.EditedOn = DateTime.Now;
                }
                else
                {
                    db.NDE004.Add(new NDE004
                    {
                        HeaderId = nde004.HeaderId,
                        Project = objNDE003.Project,
                        BU = objNDE003.BU,
                        Location = objNDE003.Location,
                        RevNo = 0,
                        LineRevNo = 0,
                        Status = clsImplementationEnum.CommonStatus.Approved.GetStringValue(),
                        SpecificationWithClauseNo = nde004.SpecificationWithClauseNo,
                        SpecificationRequirement = nde004.SpecificationRequirement,
                        CriticalToQuality = nde004.CriticalToQuality,
                        CTQRequirement = nde004.CTQRequirement,
                        ToBeDiscussed = nde004.ToBeDiscussed,
                        QualificationForExam = nde004.QualificationForExam,
                        ApprovalRequired = nde004.ApprovalRequired,
                        ImageQualityIndicator = nde004.ImageQualityIndicator,
                        FilmSelection = nde004.FilmSelection,
                        IntensifyingScreen = nde004.IntensifyingScreen,
                        RadiationSource = nde004.RadiationSource,
                        Density = nde004.Density,
                        RadiographyAcceptanceCriteria = nde004.RadiographyAcceptanceCriteria,
                        SpecialTechniqueforRadiography = nde004.SpecialTechniqueforRadiography,
                        FilmIdentification = nde004.FilmIdentification,
                        WeldCoverage = nde004.WeldCoverage,
                        Overlap = nde004.Overlap,
                        SFD = nde004.SFD,
                        SpotLength = nde004.SpotLength,
                        FilmRetentionPeriod = nde004.FilmRetentionPeriod,
                        FilmDigitization = nde004.FilmDigitization,
                        ProcedureDemonstration = nde004.ProcedureDemonstration,
                        Others = nde004.Others,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    if (objNDE003.Status == clsImplementationEnum.RTCTQStatus.Approved.GetStringValue())
                    {
                        objNDE003.RevNo = Convert.ToInt32(objNDE003.RevNo) + 1;
                        objNDE003.Status = clsImplementationEnum.RTCTQStatus.DRAFT.GetStringValue();
                        objNDE003.EditedBy = objClsLoginInfo.UserName;
                        objNDE003.EditedOn = DateTime.Now;
                        objNDE003.ReturnRemarks = null;
                        objNDE003.SubmittedBy = null;
                        objNDE003.SubmittedOn = null;
                        objNDE003.ApprovedOn = null;
                    }
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
                if (nde004.LineId > 0)
                {
                    objResponseMsg.Value = clsImplementationMessage.RTCTQMessages.Update;
                }
                else
                {
                    objResponseMsg.Value = clsImplementationMessage.RTCTQMessages.Insert;
                }
                objResponseMsg.Status = clsImplementationEnum.RTCTQStatus.DRAFT.GetStringValue();
                objResponseMsg.Remarks = objNDE003.ReturnRemarks;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SendForApproval(int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (HeaderId > 0)
                {
                    if (!IsapplicableForSubmit(HeaderId))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This record has been already submitted, Please refresh page.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                }
                string[] arrLineStatus = { clsImplementationEnum.RTCTQStatus.DRAFT.GetStringValue(), clsImplementationEnum.RTCTQStatus.Returned.GetStringValue() };
                List<NDE004> lstNDE004 = db.NDE004.Where(x => x.HeaderId == HeaderId && arrLineStatus.Contains(x.Status)).ToList();
                List<NDE004> IsNullEntry = db.NDE004.Where(x => x.HeaderId == HeaderId && (x.SpecificationNo == null || x.SpecificationWithClauseNo == null ||
                                                         x.SpecificationRequirement == null || x.CriticalToQuality == null
                                               )).ToList();

                if (!IsNullEntry.Any())
                {
                    if (lstNDE004 != null && lstNDE004.Count > 0)
                    {
                        foreach (var objData in lstNDE004)
                        {
                            NDE004 objNDE004 = lstNDE004.Where(x => x.LineId == objData.LineId).FirstOrDefault();
                            //objNDE004.Status = clsImplementationEnum.RTCTQStatus.SendForApproval.GetStringValue();
                            objNDE004.EditedBy = objClsLoginInfo.UserName;
                            objNDE004.EditedOn = DateTime.Now;
                            objNDE004.SubmittedBy = objClsLoginInfo.UserName;
                            objNDE004.SubmittedOn = DateTime.Now;
                        }
                    }
                    NDE003 objNDE003 = db.NDE003.Where(x => x.HeaderId == HeaderId && arrLineStatus.Contains(x.Status)).FirstOrDefault();
                    if (objNDE003 != null)
                    {
                        objNDE003.Status = clsImplementationEnum.RTCTQStatus.SendForApproval.GetStringValue();
                        objNDE003.SubmittedBy = objClsLoginInfo.UserName;
                        objNDE003.SubmittedOn = DateTime.Now;
                        db.SaveChanges();

                        #region Send Notification
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE2.GetStringValue(), objNDE003.Project, objNDE003.BU, objNDE003.Location, "CTQ: " + objNDE003.CTQNo + " has come for your Approval", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/NDE/ApproveRTCTQ/RTCTQLineDetails?HeaderID=" + objNDE003.HeaderId);
                        #endregion

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Details successfully sent for approval.";
                    }
                    else
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Details not available for approval.";
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "info";
                    objResponseMsg.Remarks = string.Format(clsImplementationMessage.UTCTQMessages.NullRecord, "RT CTQ Details");
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteLine(int LineID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {               
                NDE004 objNDE004 = db.NDE004.Where(x => x.LineId == LineID).FirstOrDefault();
                if (objNDE004.HeaderId > 0)
                {
                    if (!IsapplicableForSubmit(objNDE004.HeaderId))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This record has been already submitted, Please refresh page.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                }
                if (objNDE004 != null)
                {
                    if (objNDE004.NDE003.RevNo > 0)
                    {
                        objNDE004.Status = clsImplementationEnum.CommonStatus.Deleted.GetStringValue();
                    }
                    else
                    {
                        db.NDE004.Remove(objNDE004);
                    }
                    
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details successfully deleted.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Copy
        [HttpPost]
        public ActionResult GetCopyDataFormPartial()
        {
            //List<Projects> project = Manager.getProjectsByUser(objClsLoginInfo.UserName);
            //ViewBag.Project = new SelectList(project, "projectCode", "projectDescription");
            return PartialView("_GetCopyDataFormPartial");
        }

        [HttpPost]
        public ActionResult copyRTCTQ(string projCode, int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string currentUser = objClsLoginInfo.UserName;

                var currentLoc = (from a in db.COM003
                                  join b in db.COM002 on a.t_loca equals b.t_dimx
                                  where b.t_dtyp == 1 && a.t_actv == 1
                                  && a.t_psno.Equals(currentUser, StringComparison.OrdinalIgnoreCase)
                                  select b.t_dimx).FirstOrDefault().ToString();


                NDE003 objNDE003 = db.NDE003.Where(x => x.Project == projCode && x.Location == currentLoc).FirstOrDefault();
                NDE003 objExistingNDE003 = db.NDE003.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                if (objNDE003 != null)
                {
                    #region OldCode
                    //if (objNDE003.Location == currentLoc)
                    //{
                    //    if (objNDE003.Status == clsImplementationEnum.RTCTQStatus.DRAFT.GetStringValue())
                    //    {
                    //        objResponseMsg = InsertNewRTCTQ(objExistingNDE003, objNDE003, true);
                    //    }
                    //    else
                    //    {
                    //        objResponseMsg.Key = false;
                    //        objResponseMsg.Value = "RT CTQ already exists";
                    //    }
                    //}
                    //else
                    //{
                    //    NDE003 objDesNDE003 = new NDE003();
                    //    string BU = db.COM001.Where(i => i.t_cprj == projCode).FirstOrDefault().t_entu;
                    //    objDesNDE003.Project = projCode;
                    //    objDesNDE003.BU = BU;
                    //    objDesNDE003.Location = currentLoc;
                    //    objDesNDE003.CTQNo = projCode + "_CTQ_RT";
                    //    objResponseMsg = InsertNewRTCTQ(objExistingNDE003, objDesNDE003, false);
                    //}
                    #endregion

                    #region NewCode
                    if (objNDE003.Status == clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue() ||
                        objNDE003.Status == clsImplementationEnum.PTMTCTQStatus.Returned.GetStringValue())
                    {
                        objResponseMsg = InsertNewRTCTQ(objExistingNDE003, objNDE003, true);
                    }
                    else if (objNDE003.Status == clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue() ||
                        objNDE003.Status == clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "RT CTQ already exists";
                    }
                    #endregion
                }
                else
                {
                    NDE003 objDesNDE003 = new NDE003();
                    string BU = db.COM001.Where(i => i.t_cprj == projCode).FirstOrDefault().t_entu;
                    objDesNDE003.Project = projCode;
                    objDesNDE003.BU = BU;
                    objDesNDE003.Location = currentLoc;
                    objDesNDE003.CTQNo = projCode + "_CTQ_RT";
                    objResponseMsg = InsertNewRTCTQ(objExistingNDE003, objDesNDE003, false);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponseMsgWithStatus InsertNewRTCTQ(NDE003 objSrcNDE003, NDE003 objDesNDE003, bool IsEdited)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();

            try
            {
                if (IsEdited)
                {
                    //If Record exist for destination project then no need to change header table confirm by satish (On 26-03-2017 11:40 AM). 
                    //So Comment OldCode For Update Header
                    #region OldCode For Update Header
                    //objDesNDE003.ApplicableSpecification = objSrcNDE003.ApplicableSpecification;
                    //objDesNDE003.AcceptanceStandard = objSrcNDE003.AcceptanceStandard;
                    //objDesNDE003.ASMECodeStamp = objSrcNDE003.ASMECodeStamp;
                    //objDesNDE003.TPIName = objSrcNDE003.TPIName;
                    //objDesNDE003.NotificationRequired = objSrcNDE003.NotificationRequired;
                    //objDesNDE003.ThicknessOfJob = objSrcNDE003.ThicknessOfJob;
                    //objDesNDE003.SizeDimension = objSrcNDE003.SizeDimension;
                    //objDesNDE003.ManufacturingCode = objSrcNDE003.ManufacturingCode;
                    //objDesNDE003.NDERemarks = objSrcNDE003.NDERemarks;
                    objDesNDE003.EditedBy = objClsLoginInfo.UserName;
                    objDesNDE003.EditedOn = DateTime.Now;
                    #endregion

                    //Add Source Line item in destination project. So comment Remove Existing Data code (On 26-03-2017 11:00 AM)
                    #region Remove Existing Data
                    //List<NDE004> lstDesNDE004 = db.NDE004.Where(x => x.HeaderId == objDesNDE003.HeaderId).ToList();
                    //if (lstDesNDE004 != null && lstDesNDE004.Count > 0)
                    //{
                    //    db.NDE004.RemoveRange(lstDesNDE004);
                    //}
                    #endregion

                    List<NDE004> lstSrcNDE004 = db.NDE004.Where(x => x.HeaderId == objSrcNDE003.HeaderId).ToList();

                    if (lstSrcNDE004 != null && lstSrcNDE004.Count > 0)
                    {
                        db.NDE004.AddRange(
                                            lstSrcNDE004.Select(x =>
                                            new NDE004
                                            {
                                                HeaderId = objDesNDE003.HeaderId,
                                                Project = objDesNDE003.Project,
                                                BU = objDesNDE003.BU,
                                                Location = objDesNDE003.Location,
                                                RevNo = Convert.ToInt32(objDesNDE003.RevNo),
                                                LineRevNo = 0,
                                                Status = clsImplementationEnum.RTCTQStatus.DRAFT.GetStringValue(),
                                                Description = x.Description,
                                                SpecificationNo = x.SpecificationNo,
                                                SpecificationWithClauseNo = x.SpecificationWithClauseNo,
                                                ToBeDiscussed = x.ToBeDiscussed,
                                                SpecificationRequirement = x.SpecificationRequirement,
                                                CriticalToQuality = x.CriticalToQuality,
                                                CTQRequirement = x.CTQRequirement,
                                                ReturnRemark = x.ReturnRemark,
                                                CreatedBy = objClsLoginInfo.UserName,
                                                CreatedOn = DateTime.Now,
                                            })
                                          );
                    }

                    db.SaveChanges();
                    objResponseMsg.HeaderId = objDesNDE003.HeaderId;
                    objResponseMsg.HeaderStatus = objDesNDE003.CTQNo;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.RTCTQMessages.Update.ToString() + " To Document No : " + objResponseMsg.HeaderStatus + ".";
                }
                else
                {
                    objDesNDE003.ApplicableSpecification = objSrcNDE003.ApplicableSpecification;
                    objDesNDE003.AcceptanceStandard = objSrcNDE003.AcceptanceStandard;
                    objDesNDE003.ASMECodeStamp = objSrcNDE003.ASMECodeStamp;
                    objDesNDE003.TPIName = objSrcNDE003.TPIName;
                    objDesNDE003.NotificationRequired = objSrcNDE003.NotificationRequired;
                    objDesNDE003.ThicknessOfJob = objSrcNDE003.ThicknessOfJob;
                    objDesNDE003.ManufacturingCode = objSrcNDE003.ManufacturingCode;
                    objDesNDE003.SizeDimension = objSrcNDE003.SizeDimension;
                    objDesNDE003.NDERemarks = objSrcNDE003.NDERemarks;
                    objDesNDE003.Status = clsImplementationEnum.RTCTQStatus.DRAFT.GetStringValue();
                    objDesNDE003.RevNo = 0;
                    objDesNDE003.CreatedBy = objClsLoginInfo.UserName;
                    objDesNDE003.CreatedOn = DateTime.Now;
                    db.NDE003.Add(objDesNDE003);
                    db.SaveChanges();
                    //List<NDE004> lstDesNDE004 = db.NDE004.Where(x => x.HeaderId == objDesNDE003.HeaderId).ToList();
                    //if (lstDesNDE004 != null && lstDesNDE004.Count > 0)
                    //{
                    //    db.NDE004.RemoveRange(lstDesNDE004);
                    //}
                    List<NDE004> lstSrcNDE004 = db.NDE004.Where(x => x.HeaderId == objSrcNDE003.HeaderId).ToList();
                    if (lstSrcNDE004 != null && lstSrcNDE004.Count > 0)
                    {
                        db.NDE004.AddRange(
                                            lstSrcNDE004.Select(x =>
                                            new NDE004
                                            {
                                                HeaderId = objDesNDE003.HeaderId,
                                                Project = objDesNDE003.Project,
                                                BU = objDesNDE003.BU,
                                                Location = objDesNDE003.Location,
                                                RevNo = Convert.ToInt32(objDesNDE003.RevNo),
                                                LineRevNo = 0,
                                                Status = clsImplementationEnum.RTCTQStatus.DRAFT.GetStringValue(),
                                                Description = x.Description,
                                                SpecificationNo = x.SpecificationNo,
                                                SpecificationWithClauseNo = x.SpecificationWithClauseNo,
                                                ToBeDiscussed = x.ToBeDiscussed,
                                                SpecificationRequirement = x.SpecificationRequirement,
                                                CriticalToQuality = x.CriticalToQuality,
                                                CTQRequirement = x.CTQRequirement,
                                                ReturnRemark = x.ReturnRemark,
                                                CreatedBy = objClsLoginInfo.UserName,
                                                CreatedOn = DateTime.Now,
                                            })
                                          );
                    }
                    db.SaveChanges();
                    objResponseMsg.HeaderId = objDesNDE003.HeaderId;
                    objResponseMsg.HeaderStatus = objDesNDE003.CTQNo;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.RTCTQMessages.Insert.ToString() + " To Document No : " + objResponseMsg.HeaderStatus + " of " + objDesNDE003.Project + " Project.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return objResponseMsg;
        }
        #endregion

        #region history
        [HttpPost]
        public ActionResult GetHistoryDataPartial(int HeaderID)
        {
            ViewBag.HeaderID = HeaderID;
            return PartialView("_GetHistoryDataPartial");
        }

        [HttpPost]
        public JsonResult LoadRTCTQHeaderHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (ltrim(rtrim(nde3.BU))+'-'+bcom2.t_desc like '%" + param.sSearch +
                                "%' or ltrim(rtrim(nde3.Location))+'-'+lcom2.t_desc like '%" + param.sSearch +
                                "%' or (nde3.Project+'-'+com1.t_dsca) like '%" + param.sSearch +
                                "%' or CTQNo like '%" + param.sSearch +
                                "%' or RevNo like '%" + param.sSearch +
                                "%' or ApplicableSpecification like '%" + param.sSearch +
                                "%' or AcceptanceStandard like '%" + param.sSearch +
                                "%' or ASMECodeStamp like '%" + param.sSearch +
                                "%' or TPIName like '%" + param.sSearch +
                                "%' or NotificationRequired like '%" + param.sSearch +
                                "%' or ThicknessOfJob like '%" + param.sSearch +
                                "%' or SizeDimension like '%" + param.sSearch +
                                "%' or NDERemarks like '%" + param.sSearch +
                                "%' or nde3.CreatedBy + ' - ' + com003c.t_name like '%" + param.sSearch +
                                "%' or ManufacturingCode like '%" + param.sSearch +
                                "%' or Status like '%" + param.sSearch + "%')";
                }
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "nde3.BU", "nde3.Location");
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_RTCTQ_GETHEADER_HISTORY
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.BU),
                               Convert.ToString(uc.Location),
                               Convert.ToString(uc.CTQNo),
                               Convert.ToString(uc.ManufacturingCode),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn !=null?(Convert.ToDateTime(uc.CreatedOn)).ToString("dd/MM/yyyy"):""),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.ApplicableSpecification),
                               Convert.ToString(uc.AcceptanceStandard),
                               (uc.ASMECodeStamp == true?"<label class=\"mt-checkbox mt-checkbox-outline\"><input checked=\"checked\" disabled=\"disabled\" type = \"checkbox\" value=\"\" /><span></span></label>":"<label class=\"mt-checkbox mt-checkbox-outline\"><input disabled=\"disabled\" type = \"checkbox\" value=\"\" /><span></span></label>"),//Convert.ToString(uc.ASMECodeStamp),
                               Convert.ToString(uc.TPIName),
                               Convert.ToString(uc.NotificationRequired == true ? "Yes" : uc.NotificationRequired == false ? "No" : ""),
                               Convert.ToString(uc.ThicknessOfJob),
                               Convert.ToString(uc.SizeDimension),
                               Convert.ToString(uc.NDERemarks),
                               "<center style=\"display:inline-flex;\"><a title='View' href='"+WebsiteURL+"/NDE/MaintainRTCTQ/GetRTCTQLineHistory?ID="+Convert.ToInt32(uc.Id)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a><a title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/NDE/MaintainRTCTQ/ShowTimeline?HeaderID="+Convert.ToInt32(uc.Id)+"&LineId=0&isHistory=true');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a></center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]
        public ActionResult GetRTCTQLineHistory(int ID = 0)
        {
            NDEModels objNDEModels = new NDEModels();
            if (ID > 0)
            {
                ID = Convert.ToInt32(ID);
            }
            NDE003_Log objNDE003_Log = db.NDE003_Log.Where(x => x.Id == ID).FirstOrDefault();
            if (objNDE003_Log != null)
            {
                ViewBag.HeaderID = objNDE003_Log.HeaderId;
                ViewBag.Status = objNDE003_Log.Status;
                ViewBag.CTQNo = objNDE003_Log.CTQNo;

                var ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objNDE003_Log.ManufacturingCode, objNDE003_Log.BU, objNDE003_Log.Location, true);
                var projectDetails = db.COM001.Where(x => x.t_cprj == objNDE003_Log.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                objNDE003_Log.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                var BUDescription = db.COM002.Where(x => x.t_dimx == objNDE003_Log.BU).Select(x => x.t_desc).FirstOrDefault();
                objNDE003_Log.BU = Convert.ToString(objNDE003_Log.BU + " - " + BUDescription);
                var Location = db.COM002.Where(x => x.t_dimx == objNDE003_Log.Location).Select(x => x.t_desc).FirstOrDefault();
                objNDE003_Log.Location = Convert.ToString(objNDE003_Log.Location + " - " + Location);

                if (ManufacturingCode != null && !string.IsNullOrWhiteSpace(ManufacturingCode.Code) && !string.IsNullOrWhiteSpace(ManufacturingCode.CategoryDescription))
                {
                    ViewBag.ManufactCode = ManufacturingCode.CategoryDescription;
                }
            }
            return View(objNDE003_Log);
        }

        [HttpPost]
        public ActionResult GetCTQLinesHistoryHtml(int ID, string strCTQNo)
        {
            ViewBag.ID = ID;
            ViewBag.strCTQNo = strCTQNo;
            ViewBag.CTQStatus = db.NDE003_Log.Where(x => x.Id == ID).Select(x => x.Status).FirstOrDefault();
            return PartialView("_GetCTQLinesHistoryHtml");
        }

        [HttpPost]
        public JsonResult LoadRTCTQLineHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "1=1 " + Manager.MakeIntInCondition("RefId", param.CTQHeaderId).ToString();
                int ndeHisId = Convert.ToInt32(param.CTQHeaderId);
                NDE003_Log objNDE003_Log = db.NDE003_Log.FirstOrDefault(x => x.Id == ndeHisId);

                string[] columnName = { "Description", "SpecificationNo", "SpecificationWithClauseNo", "SpecificationRequirement", "ToBeDiscussed", "CriticalToQuality", "CTQRequirement" };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);

                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName);
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_GET_RT_HEADER_LINES_HISTORY(StartIndex, EndIndex, strSortOrder, strWhere).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.Description),
                                Convert.ToString(uc.SpecificationNo),
                                Convert.ToString(uc.SpecificationWithClauseNo),
                                Convert.ToString(uc.ToBeDiscussed==true ? "Yes":uc.ToBeDiscussed==false?"No":""),
                                Convert.ToString(uc.SpecificationRequirement),
                                Convert.ToString(uc.CriticalToQuality==true ? "Yes":uc.CriticalToQuality==false?"No":""),
                                Convert.ToString(uc.CTQRequirement),
                                Convert.ToString(uc.ReturnRemark),
                                Convert.ToString(uc.Status),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId),
                                "<center style=\"display:inline;\"><a class='' href='javascript:void(0)' onclick=ShowTimeline('/NDE/MaintainRTCTQ/ShowTimeline?HeaderID="+Convert.ToInt32(uc.HeaderId)+"&LineId="+Convert.ToInt32(uc.Id)+"&isHistory=true');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a></center>",
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region common functions
        [NonAction]
        public static string GenerateGridButton(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";


            htmlControl = "<a id='" + inputID + "' name='" + inputID + "' class='btn btn-outline btn-circle btn-sm blue' " + onClickEvent + " ><i class='" + className + "'></i> " + buttonName + "</a>";

            //htmlControl = "<i  data-modal='' id='" + inputID + "' name='" + inputID + "' style='cursor:Pointer;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";

            return htmlControl;
        }

        [HttpPost]
        public bool checkProjectExist(string projectcode)
        {
            bool Flag = false;
            if (!string.IsNullOrWhiteSpace(projectcode))
            {
                NDE003 objNDE003 = db.NDE003.Where(x => x.Project == projectcode).FirstOrDefault();
                if (objNDE003 != null)
                {
                    Flag = true;
                }
            }
            return Flag;
        }

        [HttpPost]
        public ActionResult GetProjectWiseBU(string projectcode)
        {
            if (!string.IsNullOrWhiteSpace(projectcode))
            {
                ProjectDataModel objProjectDataModel = new ProjectDataModel();
                string BU = db.COM001.Where(i => i.t_cprj == projectcode).FirstOrDefault().t_entu;
                var BUDescription = (from a in db.COM002
                                     where a.t_dtyp == 2 && a.t_dimx == BU
                                     select new ProjectDataModel { BUDescription = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
                objProjectDataModel.ManufacturingCode = Manager.getManufacturingCode(null, projectcode);
                objProjectDataModel.BUDescription = BUDescription.BUDescription;
                return Json(objProjectDataModel, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_RTCTQ_GETHEADER(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      RevNo = li.RevNo,
                                      CTQNo = li.CTQNo,
                                      Status = li.Status,
                                      ApplicableSpecification = li.ApplicableSpecification,
                                      AcceptanceStandard = li.AcceptanceStandard,
                                      ASMECodeStamp = li.ASMECodeStamp,
                                      TPIName = li.TPIName,
                                      NotificationRequired = Convert.ToString(li.NotificationRequired == true ? "Yes" : li.NotificationRequired == false ? "No" : ""),
                                      ThicknessOfJob = li.ThicknessOfJob,
                                      SizeDimension = li.SizeDimension,
                                      ManufacturingCode = li.ManufacturingCode,
                                      NDERemarks = li.NDERemarks,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn
                                  }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_GET_RT_HEADER_LINES(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      SrNo = Convert.ToString(uc.ROW_NO),
                                      Description = Convert.ToString(uc.Description),
                                      SpecificationNo = Convert.ToString(uc.SpecificationNo),
                                      SpecificationWithClauseNo = Convert.ToString(uc.SpecificationWithClauseNo),
                                      ToBeDiscussed = Convert.ToString(uc.ToBeDiscussed == true ? "Yes" : uc.ToBeDiscussed == false ? "No" : ""),
                                      SpecificationRequirement = Convert.ToString(uc.SpecificationRequirement),
                                      CriticalToQuality = Convert.ToString(uc.CriticalToQuality == true ? "Yes" : uc.CriticalToQuality == false ? "No" : ""),
                                      CTQRequirement = Convert.ToString(uc.CTQRequirement)
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {
                    var lst = db.SP_RTCTQ_GETHEADER_HISTORY(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      RevNo = li.RevNo,
                                      CTQNo = li.CTQNo,
                                      Status = li.Status,
                                      ManufacturingCode = li.ManufacturingCode,
                                      ApplicableSpecification = li.ApplicableSpecification,
                                      AcceptanceStandard = li.AcceptanceStandard,
                                      ASMECodeStamp = li.ASMECodeStamp,
                                      TPIName = li.TPIName,
                                      NotificationRequired = Convert.ToString(li.NotificationRequired == true ? "Yes" : li.NotificationRequired == false ? "No" : ""),
                                      ThicknessOfJob = li.ThicknessOfJob,
                                      SizeDimension = li.SizeDimension,
                                      NDERemarks = li.NDERemarks,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else
                {
                    var lst = db.SP_GET_RT_HEADER_LINES_HISTORY(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      SrNo = Convert.ToString(uc.ROW_NO),
                                      Description = Convert.ToString(uc.Description),
                                      SpecificationNo = Convert.ToString(uc.SpecificationNo),
                                      SpecificationWithClauseNo = Convert.ToString(uc.SpecificationWithClauseNo),
                                      ToBeDiscussed = Convert.ToString(uc.ToBeDiscussed == true ? "Yes" : uc.ToBeDiscussed == false ? "No" : ""),
                                      SpecificationRequirement = Convert.ToString(uc.SpecificationRequirement),
                                      CriticalToQuality = Convert.ToString(uc.CriticalToQuality == true ? "Yes" : uc.CriticalToQuality == false ? "No" : ""),
                                      CTQRequirement = Convert.ToString(uc.CTQRequirement)
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }


                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ShowTimeline(int HeaderId, int LineId = 0, bool isHistory = false)
        {
            TimelineViewModel model = new TimelineViewModel();
            if (isHistory)
            {
                if (LineId > 0)
                {
                    NDE004_Log objNDE004 = db.NDE004_Log.Where(x => x.Id == LineId).FirstOrDefault();
                    model.Title = "NDECTQ Lines";
                    model.ApprovedBy = Manager.GetUserNameFromPsNo(objNDE004.ApprovedBy);
                    model.ApprovedOn = objNDE004.ApprovedOn;
                    model.SubmittedBy = Manager.GetUserNameFromPsNo(objNDE004.SubmittedBy);
                    model.SubmittedOn = objNDE004.SubmittedOn;
                    model.CreatedBy = Manager.GetUserNameFromPsNo(objNDE004.CreatedBy);
                    model.CreatedOn = objNDE004.CreatedOn;
                    model.ReturnedBy = Manager.GetUserNameFromPsNo(objNDE004.ReturnedBy);
                    model.ReturnedOn = objNDE004.ReturnedOn;
                }
                else
                {
                    NDE003_Log objNDE003 = db.NDE003_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                    model.Title = "NDECTQ Header";
                    model.ApprovedBy = Manager.GetUserNameFromPsNo(objNDE003.ApprovedBy);
                    model.ApprovedOn = objNDE003.ApprovedOn;
                    model.SubmittedBy = Manager.GetUserNameFromPsNo(objNDE003.SubmittedBy);
                    model.SubmittedOn = objNDE003.SubmittedOn;
                    model.CreatedBy = Manager.GetUserNameFromPsNo(objNDE003.CreatedBy);
                    model.CreatedOn = objNDE003.CreatedOn;
                }
            }
            else
            {
                if (LineId > 0)
                {
                    NDE004 objNDE004 = db.NDE004.Where(x => x.LineId == LineId).FirstOrDefault();
                    model.Title = "NDECTQ Lines";
                    model.ApprovedBy = Manager.GetUserNameFromPsNo(objNDE004.ApprovedBy);
                    model.ApprovedOn = objNDE004.ApprovedOn;
                    model.SubmittedBy = Manager.GetUserNameFromPsNo(objNDE004.SubmittedBy);
                    model.SubmittedOn = objNDE004.SubmittedOn;
                    model.CreatedBy = Manager.GetUserNameFromPsNo(objNDE004.CreatedBy);
                    model.CreatedOn = objNDE004.CreatedOn;
                    model.ReturnedBy = Manager.GetUserNameFromPsNo(objNDE004.ReturnedBy);
                    model.ReturnedOn = objNDE004.ReturnedOn;
                }
                else
                {
                    NDE003 objNDE003 = db.NDE003.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    model.Title = "NDECTQ Header";
                    model.ApprovedBy = Manager.GetUserNameFromPsNo(objNDE003.ApprovedBy);
                    model.ApprovedOn = objNDE003.ApprovedOn;
                    model.SubmittedBy = Manager.GetUserNameFromPsNo(objNDE003.SubmittedBy);
                    model.SubmittedOn = objNDE003.SubmittedOn;
                    model.CreatedBy = Manager.GetUserNameFromPsNo(objNDE003.CreatedBy);
                    model.CreatedOn = objNDE003.CreatedOn;
                }
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        #endregion
    }
}