﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQS.Models;
using IEMQS.Areas.NDE.Models;
using IEMQSImplementation;

namespace IEMQS.Areas.NDE.Controllers
{
    public class ApproveMTTechniqueController : clsBase
    {
        // GET: NDE/ApproveMTTechnique
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetApproveMTTechniqueGridDataPartial(string status)
        {
            ViewBag.status = status;
            return PartialView("_GetApproveMTTechniqueGridDataPartial");
        }

        public ActionResult LoadHeaderDataTable(JQueryDataTableParamModel param, string status)
        {
            try
            {

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;


                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "nde8.BU", "nde8.Location");
                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and nde8.Status in('" + clsImplementationEnum.QCPStatus.SentForApproval.GetStringValue() + "')";
                }
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += " and (ltrim(rtrim(NDE8.BU))+'-'+bcom2.t_desc like '%" + param.sSearch + "%' or ltrim(rtrim(nde8.Location))+'-'+lcom2.t_desc like '%" + param.sSearch + "%' " +
                                            " or NDE8.QualityProject like '%" + param.sSearch + "%' or (NDE8.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%' " +
                                            " or MTTechNo like '%" + param.sSearch + "%' or RevNo like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%' " +
                                            " or TechniqueSheetNo like '%" + param.sSearch + "%'" + " or (ecom3.t_psno+'-'+ecom3.t_name) like '%" + param.sSearch + "%')";
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstHeader = db.SP_NDE_GetMTTechniqueHeader(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstHeader
                          select new[] {
                        Convert.ToString(a.HeaderId),
                        Convert.ToString(a.QualityProject),
                        Convert.ToString(a.Project),
                        Convert.ToString(a.BU),
                        Convert.ToString(a.Location),
                        Convert.ToString(a.TechniqueSheetNo),
                        Convert.ToString(a.MTTechNo),
                        Convert.ToString("R" + a.RevNo),
                        a.CreatedBy,
                        Convert.ToString(a.CreatedOn !=null?(Convert.ToDateTime(a.CreatedOn)).ToString("dd/MM/yyyy"):""),
                        a.Status,
                        "<center>"
                         + Helper.GenerateActionIcon(a.HeaderId, "View", "View Detail", "fa fa-eye", "", WebsiteURL + "/NDE/ApproveMTTechnique/ViewMTTechnique?headerID="+a.HeaderId,false)
                         +"<a title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/NDE/MaintainMTTechnique/ShowTimeline?HeaderID="+Convert.ToInt32(a.HeaderId)+"');><i class='iconspace fa fa-clock-o'></i></a><a title='Print' href='javascript:void(0)' onclick='PrintReport("+Convert.ToInt32(a.HeaderId)+",\""+a.Status+"\");'><i style = 'margin-left:10px;' " +
                                "class='fa fa-print'></i></a></center>"
                        //Convert.ToString(a.HeaderId)
                        //a.EditedBy
                        //a.Status,
            };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult ViewMTTechnique(int headerID)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("NDE008");
            NDE008 objNDE008 = new NDE008();
            if (headerID > 0)
            {
                NDEModels objNDEModels = new NDEModels();
                objNDE008 = db.NDE008.Where(i => i.HeaderId == headerID).FirstOrDefault();
                var QMSProject = new NDEModels().GetQMSProject(objNDE008.QualityProject);

                //ViewBag.QMSProject = QMSProject.QualityProject + " - " + QMSProject.ManufacturingCode; //db.TMP001.Where(i => i.QMSProject == objNDE008.QMSProject).Select(i => i.QMSProject + " - " + i.ManufacturingCode).FirstOrDefault();
                ViewBag.QMSProject = QMSProject.QualityProject;
                ViewBag.MfgCode = QMSProject.ManufacturingCode;
                ViewBag.Project = db.COM001.Where(i => i.t_cprj == objNDE008.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                ViewBag.BU = db.COM002.Where(i => i.t_dtyp == 2 && i.t_dimx == objNDE008.BU).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
                ViewBag.Location = db.COM002.Where(i => i.t_dtyp == 1 && i.t_dimx == objNDE008.Location).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
                ViewBag.MTMethod = objNDEModels.GetCategory(objNDE008.Method).CategoryDescription;
                ViewBag.MagnetizationTechnique = objNDEModels.GetCategory("Magnetization Technique", objNDE008.MagnetizationTechnique, objNDE008.BU, objNDE008.Location, true).CategoryDescription;
                ViewBag.MethodOfExamination = objNDEModels.GetCategory("Method Of Examination", objNDE008.MethodofExamination, objNDE008.BU, objNDE008.Location, true).CategoryDescription;
                ViewBag.Current = objNDEModels.GetCategory("Current", objNDE008.Current, objNDE008.BU, objNDE008.Location, true).CategoryDescription;
                ViewBag.MagneticParticleType = objNDEModels.GetCategory("Magnetic Particle Type", objNDE008.MagneticParticleType, objNDE008.BU, objNDE008.Location, true).CategoryDescription;
                ViewBag.ParticleColour = objNDEModels.GetCategory("Particle Colour", objNDE008.ParticleColour, objNDE008.BU, objNDE008.Location, true).CategoryDescription;
                ViewBag.CarrierMedium = objNDEModels.GetCategory("Carrier Medium", objNDE008.CarrierMedium, objNDE008.BU, objNDE008.Location, true).CategoryDescription;
                ViewBag.LightIntensity = objNDEModels.GetCategory("Light Intensity", objNDE008.LightIntensity, objNDE008.BU, objNDE008.Location, true).CategoryDescription;
                ViewBag.LightSource = objNDEModels.GetCategory("Light Source", objNDE008.LightSource, objNDE008.BU, objNDE008.Location, true).CategoryDescription;
                ViewBag.DemagnetizationUpto = objNDEModels.GetCategory("Demagnetization upto", objNDE008.Demagnetization, objNDE008.BU, objNDE008.Location, true).CategoryDescription;
                objNDE008.FieldAdequacy = objNDEModels.GetCategory("Field Adequacy", objNDE008.FieldAdequacy, objNDE008.BU, objNDE008.Location, true).CategoryDescription;
                objNDE008.Calibration = objNDEModels.GetCategory("Calibration", objNDE008.Calibration, objNDE008.BU, objNDE008.Location, true).CategoryDescription;
                objNDE008.PostCleaning = objNDEModels.GetCategory("Post Cleaning", objNDE008.PostCleaning, objNDE008.BU, objNDE008.Location, true).CategoryDescription;

                ViewBag.ManufacturingCode = QMSProject.ManufacturingCode;
            }

            return View(objNDE008);
        }

        [HttpPost]
        public ActionResult ReturnHeader(int headerId, string returnRemark)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            NDE008 objNDE008 = new NDE008();
            try
            {
                if (headerId > 0)
                {
                    if (!IsapplicableForAttend(headerId))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This record has been already attended, Please refresh page.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    objNDE008 = db.NDE008.Where(i => i.HeaderId == headerId).FirstOrDefault();

                    objNDE008.Status = clsImplementationEnum.NDETechniqueStatus.Returned.GetStringValue();
                    objNDE008.ReturnRemarks = returnRemark;
                    objNDE008.ReturnedBy = objClsLoginInfo.UserName;
                    objNDE008.ReturnedOn = DateTime.Now;
                    db.SaveChanges();

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE3.GetStringValue(), objNDE008.Project, objNDE008.BU, objNDE008.Location, "Technique No: " + objNDE008.MTTechNo + " has been Returned", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/NDE/MaintainMTTechnique/AddMTTechnique?headerId=" + objNDE008.HeaderId);
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PTTechniqueMeassage.Return.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg);
        }

        [HttpPost]
        public ActionResult ApproveHeader(int headerId, string returnRemarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (headerId > 0)
                {
                    if (!IsapplicableForAttend(headerId))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This record has been already attended, Please refresh page.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                }
                db.SP_NDE_ApproveMTHeader(headerId, objClsLoginInfo.UserName);

                #region Send Notification
                NDE008 objNDE008 = db.NDE008.Where(i => i.HeaderId == headerId).FirstOrDefault();
                if (objNDE008 != null)
                {
                    objNDE008.ReturnRemarks = returnRemarks;
                    db.SaveChanges();
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE3.GetStringValue(), objNDE008.Project, objNDE008.BU, objNDE008.Location, "Technique No: " + objNDE008.MTTechNo + " has been Approved", clsImplementationEnum.NotificationType.Information.GetStringValue());
                }
                #endregion

                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.PTTechniqueMeassage.Approve.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg);
        }

        private bool IsapplicableForAttend(int headerId)
        {
            NDE008 objNDE008 = db.NDE008.Where(x => x.HeaderId == headerId).FirstOrDefault();
            if (objNDE008 != null)
            {
                if (objNDE008.Status == clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return true;
            
        }

        [HttpPost]
        public ActionResult ApproveSelectedTechnique(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (!string.IsNullOrEmpty(strHeaderIds))
                {
                    string[] arrstrHeaderIds = strHeaderIds.Split(',');
                    for (int i = 0; i < arrstrHeaderIds.Length; i++)
                    {
                        int headerId = Convert.ToInt32(arrstrHeaderIds[i]);
                        db.SP_NDE_ApproveMTHeader(headerId, objClsLoginInfo.UserName);

                        #region Send Notification
                        NDE008 objNDE008 = db.NDE008.Where(c => c.HeaderId == headerId).FirstOrDefault();
                        if (objNDE008 != null)
                        {
                            (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE3.GetStringValue(), objNDE008.Project, objNDE008.BU, objNDE008.Location, "Technique No: " + objNDE008.MTTechNo + " has been Approved", clsImplementationEnum.NotificationType.Information.GetStringValue());
                        }
                        #endregion

                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PTTechniqueMeassage.Approve.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
    }
};