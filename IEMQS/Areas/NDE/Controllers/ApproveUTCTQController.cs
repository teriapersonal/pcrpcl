﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.NDE.Controllers
{
    public class ApproveUTCTQController : clsBase
    {
        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult UTCTQLineDetails(string HeaderID)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("NDE005");
            NDEModels objNDEModels = new NDEModels();
            int HeaderId = 0;
            if (!string.IsNullOrWhiteSpace(HeaderID))
            {
                HeaderId = Convert.ToInt32(HeaderID);
            }
            NDE005 objNDE005 = db.NDE005.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            if (objNDE005 != null)
            {
                ViewBag.HeaderID = objNDE005.HeaderId;
                ViewBag.Status = objNDE005.Status;
                ViewBag.CTQNo = objNDE005.CTQNo;

                var projectDetails = db.COM001.Where(x => x.t_cprj == objNDE005.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                objNDE005.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                var BUDescription = db.COM002.Where(x => x.t_dimx == objNDE005.BU).Select(x => x.t_desc).FirstOrDefault();
                objNDE005.BU = Convert.ToString(objNDE005.BU + " - " + BUDescription);
                var Location = db.COM002.Where(x => x.t_dimx == objNDE005.Location).Select(x => x.t_desc).FirstOrDefault();
                objNDE005.Location = Convert.ToString(objNDE005.Location + " - " + Location);
            }
            return View(objNDE005);
        }

        [HttpPost]
        public ActionResult GetHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetHeaderGridDataPartial");
        }

        [HttpPost]
        public ActionResult GetCTQHeaderLinesHtml(int HeaderId, string strCTQNo)
        {
            ViewBag.HeaderId = HeaderId;
            ViewBag.strCTQNo = strCTQNo;
            ViewBag.CTQStatus = db.NDE005.Where(x => x.HeaderId == HeaderId).Select(x => x.Status).FirstOrDefault();
            return PartialView("_GetHeaderLinesHtml");
        }

        [HttpPost]
        public JsonResult LoadUTCTQHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.UTCTQStatus.SendForApproval.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "1=1";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (ltrim(rtrim(nde5.BU))+'-'+bcom2.t_desc like '%" + param.sSearch +
                                "%' or ltrim(rtrim(nde5.Location))+'-'+lcom2.t_desc like '%" + param.sSearch +
                                "%' or (nde5.Project+'-'+com1.t_dsca) like '%" + param.sSearch +
                                "%' or CTQNo like '%" + param.sSearch +
                                "%' or RevNo like '%" + param.sSearch +
                                "%' or ApplicableSpecification like '%" + param.sSearch +
                                "%' or AcceptanceStandard like '%" + param.sSearch +
                                "%' or ASMECodeStamp like '%" + param.sSearch +
                                "%' or TPIName like '%" + param.sSearch +
                                "%' or NotificationRequired like '%" + param.sSearch +
                                "%' or ThicknessOfJob like '%" + param.sSearch +
                                "%' or SizeDimension like '%" + param.sSearch +
                                "%' or NDERemarks like '%" + param.sSearch +
                                "%' or nde5.CreatedBy +' - '+com003c.t_name like '%" + param.sSearch +
                                "%' or ManufacturingCode like '%" + param.sSearch +
                                "%' or Status like '%" + param.sSearch + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "nde5.BU", "nde5.Location");

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_UTCTQ_GETHEADER
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.HeaderId),
                              Convert.ToString(uc.Project),
                               Convert.ToString(uc.BU),
                               Convert.ToString(uc.Location),
                               Convert.ToString(uc.CTQNo),
                               Convert.ToString(uc.ManufacturingCode),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn !=null?(Convert.ToDateTime(uc.CreatedOn)).ToString("dd/MM/yyyy"):""),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.ApplicableSpecification),
                               Convert.ToString(uc.AcceptanceStandard),
                               (uc.ASMECodeStamp == true?"<label class=\"mt-checkbox mt-checkbox-outline\"><input checked=\"checked\" disabled=\"disabled\" type = \"checkbox\" value=\"\" /><span></span></label>":"<label class=\"mt-checkbox mt-checkbox-outline\"><input disabled=\"disabled\" type = \"checkbox\" value=\"\" /><span></span></label>"),//Convert.ToString(uc.ASMECodeStamp),
                               Convert.ToString(uc.TPIName),
                               Convert.ToString(uc.NotificationRequired == true ? "Yes" : uc.NotificationRequired == false ? "No" : ""),
                               Convert.ToString(uc.ThicknessOfJob),
                               Convert.ToString(uc.SizeDimension),
                               Convert.ToString(uc.NDERemarks),
                               "<center><a title='View' href='"+WebsiteURL +"/NDE/ApproveUTCTQ/UTCTQLineDetails?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a><a title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/NDE/MaintainUTCTQ/ShowTimeline?HeaderID="+Convert.ToInt32(uc.HeaderId)+"');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a></center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadUTCTQHeaderLineData(JQueryDataTableParamModel param)
        {
            try
            {

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();
                int ndeHeaderId = Convert.ToInt32(param.CTQHeaderId);
                NDE005 objNDE005 = db.NDE005.FirstOrDefault(x => x.HeaderId == ndeHeaderId);

                string[] columnName = { "Description", "SpecificationNo", "SpecificationWithClauseNo", "SpecificationRequirement", "ToBeDiscussed", "CriticalToQuality", "CTQRequirement", "ReturnRemark" };
                if (param.sSearch == "yes" || param.sSearch == "Yes" || param.sSearch == "YES")
                {
                    param.sSearch = "1";
                }
                if (param.sSearch == "no" || param.sSearch == "NO" || param.sSearch == "No")
                {
                    param.sSearch = "0";
                }
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);

                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName);
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_GET_UT_HEADER_LINES
                                (
                               StartIndex,
                               EndIndex,
                               strSortOrder,
                               strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.Description),
                                Convert.ToString(uc.ApplicableForAScan),
                                Convert.ToString(uc.ApplicableForToFD),
                                Convert.ToString(uc.ApplicableForPAUT),
                                Convert.ToString(uc.SpecificationNo),
                                Convert.ToString(uc.SpecificationWithClauseNo),
                                Convert.ToString(uc.ToBeDiscussed==true ? "Yes":uc.ToBeDiscussed==false?"No":""),
                                Convert.ToString(uc.SpecificationRequirement),
                                Convert.ToString(uc.CriticalToQuality==true ? "Yes":uc.CriticalToQuality==false?"No":""),
                                Convert.ToString(uc.CTQRequirement),
                                GetRemarkStyle(objNDE005.Status,uc.Status,uc.ReturnRemark),
                                Convert.ToString(uc.Status),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId),
                                "<center style=\"display:inline;\"><a class='' href='javascript:void(0)' onclick=ShowTimeline('/NDE/MaintainUTCTQ/ShowTimeline?HeaderID="+Convert.ToInt32(uc.HeaderId)+"&LineId="+Convert.ToInt32(uc.LineId)+"');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a></center>",
                               
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string GetRemarkStyle(string status,string linestatus, string returnRemarks)
        {

            if (linestatus != clsImplementationEnum.CommonStatus.Deleted.GetStringValue() && status == clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue())
            {
                return "<input type='text' name='txtReturnRemark' class='form-control input-sm input-small input-inline' value='" + returnRemarks + "' maxlength=\"100\" />";

            }
            else
            {
                return "<input type='text' readonly= 'readonly' name='txtReturnRemark' class='form-control input-sm input-small input-inline' value='" + returnRemarks + "' maxlength=\"100\" />";
            }
        }

        [HttpPost]
        public ActionResult ApproveByApproverSelected(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
            for (int i = 0; i < arrayHeaderIds.Length; i++)
            {
                int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                objResponseMsg = ApproveHeaderByApprover(HeaderId, null, false);
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApproveByApprover(int HeaderId, string returnRemarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (!IsapplicableForAttend(HeaderId))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "This record has been already attended, Please refresh page.";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                objResponseMsg = ApproveHeaderByApprover(HeaderId, returnRemarks, true);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        private bool IsapplicableForAttend(int headerId)
        {
            NDE005 objNDE005 = db.NDE005.Where(x => x.HeaderId == headerId).FirstOrDefault();
            if (objNDE005 != null)
            {
                if (objNDE005.Status == clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        [HttpPost]
        public clsHelper.ResponseMsg ApproveHeaderByApprover(int HeaderId, string returnRemarks, bool IsReturnRemark)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string Status = clsImplementationEnum.RTCTQStatus.SendForApproval.GetStringValue().ToUpper();

                string deleted = clsImplementationEnum.CommonStatus.Deleted.GetStringValue().ToUpper();
                List<NDE006> lstdeletedLines = db.NDE006.Where(x => x.HeaderId == HeaderId && x.Status.Trim() == deleted.Trim()).ToList();
                if (lstdeletedLines != null && lstdeletedLines.Count > 0)
                {
                    db.NDE006.RemoveRange(lstdeletedLines);
                    db.SaveChanges();
                }

                List<NDE006> lstNDE006 = db.NDE006.Where(x => x.HeaderId == HeaderId).ToList();
                List<NDE006_Log> lstNDE006_Log = new List<NDE006_Log>();
                if (lstNDE006 != null && lstNDE006.Count > 0)
                {
                    foreach (var objData in lstNDE006)
                    {
                        NDE006 objNDE006 = lstNDE006.Where(x => x.LineId == objData.LineId).FirstOrDefault();

                        objNDE006.Status = clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue();
                        objNDE006.ApprovedBy = objClsLoginInfo.UserName;
                        objNDE006.ApprovedOn = DateTime.Now;
                        objNDE006.EditedBy = objClsLoginInfo.UserName;
                        objNDE006.EditedOn = DateTime.Now;

                        #region Line Log

                        NDE006_Log objNDE006_Log = new NDE006_Log();

                        objNDE006_Log.LineId = objNDE006.LineId;
                        objNDE006_Log.HeaderId = objNDE006.HeaderId;
                        objNDE006_Log.Project = objNDE006.Project;
                        objNDE006_Log.BU = objNDE006.BU;
                        objNDE006_Log.Location = objNDE006.Location;
                        objNDE006_Log.RevNo = objNDE006.RevNo;
                        objNDE006_Log.LineRevNo = objNDE006.LineRevNo;
                        objNDE006_Log.Status = objNDE006.Status;
                        objNDE006_Log.ApplicableForAScan = objNDE006.ApplicableForAScan;
                        objNDE006_Log.ApplicableForPAUT = objNDE006.ApplicableForPAUT;
                        objNDE006_Log.ApplicableForToFD = objNDE006.ApplicableForToFD;
                        objNDE006_Log.Description = objNDE006.Description;
                        objNDE006_Log.SpecificationNo = objNDE006.SpecificationNo;
                        objNDE006_Log.SpecificationWithClauseNo = objNDE006.SpecificationWithClauseNo;
                        objNDE006_Log.ToBeDiscussed = objNDE006.ToBeDiscussed;
                        objNDE006_Log.SpecificationRequirement = objNDE006.SpecificationRequirement;
                        objNDE006_Log.CriticalToQuality = objNDE006.CriticalToQuality;
                        objNDE006_Log.CTQRequirement = objNDE006.CTQRequirement;
                        objNDE006_Log.ReturnRemark = objNDE006.ReturnRemark;
                        objNDE006_Log.CreatedBy = objNDE006.CreatedBy;
                        objNDE006_Log.CreatedBy = objNDE006.CreatedBy;
                        objNDE006_Log.CreatedOn = objNDE006.CreatedOn;
                        objNDE006_Log.EditedBy = objNDE006.EditedBy;
                        objNDE006_Log.EditedOn = objNDE006.EditedOn;
                        objNDE006_Log.ApprovedBy = objNDE006.ApprovedBy;
                        objNDE006_Log.ApprovedOn = objNDE006.ApprovedOn;
                        objNDE006_Log.ReturnedBy = objNDE006.ReturnedBy;
                        objNDE006_Log.ReturnedOn = objNDE006.ReturnedOn;
                        objNDE006_Log.ReturnRemark = objNDE006.ReturnRemark;
                        objNDE006_Log.ApproverRemark = objNDE006.ApproverRemark;
                        objNDE006_Log.SubmittedBy = objNDE006.SubmittedBy;
                        objNDE006_Log.SubmittedOn = objNDE006.SubmittedOn;
                        lstNDE006_Log.Add(objNDE006_Log);
                        #endregion

                    }


                }
                var lstNDE005_Log = db.NDE005_Log.Where(x => x.HeaderId == HeaderId).ToList();
                NDE005_Log objLog = new NDE005_Log();
                if (objLog != null)
                {
                    objLog = lstNDE005_Log.FirstOrDefault();
                    lstNDE005_Log.ForEach(i => { i.Status = clsImplementationEnum.PlanStatus.Superseded.GetStringValue(); });
                }

                NDE005 objNDE005 = db.NDE005.Where(x => x.HeaderId == HeaderId && x.Status.ToUpper() == Status).FirstOrDefault();
                if (objNDE005 != null)
                {
                    objNDE005.Status = clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue();
                    if (IsReturnRemark)
                    {
                        objNDE005.ReturnRemarks = returnRemarks;
                    }
                    objNDE005.ApprovedBy = objClsLoginInfo.UserName;
                    objNDE005.ApprovedOn = DateTime.Now;
                    objNDE005.EditedBy = objClsLoginInfo.UserName;
                    objNDE005.EditedOn = DateTime.Now;

                    #region Header Log
                    NDE005_Log objNDE005_Log = db.NDE005_Log.Where(x => x.HeaderId == objNDE005.HeaderId).FirstOrDefault();

                    if (objNDE005_Log == null)
                    {
                        objNDE005_Log = db.NDE005_Log.Add(new NDE005_Log
                        {
                            HeaderId = objNDE005.HeaderId,
                            Project = objNDE005.Project,
                            BU = objNDE005.BU,
                            Location = objNDE005.Location,
                            RevNo = objNDE005.RevNo,
                            CTQNo = objNDE005.CTQNo,
                            Status = objNDE005.Status,
                            ManufacturingCode = objNDE005.ManufacturingCode,
                            ApplicableSpecification = objNDE005.ApplicableSpecification,
                            AcceptanceStandard = objNDE005.AcceptanceStandard,
                            ASMECodeStamp = objNDE005.ASMECodeStamp,
                            TPIName = objNDE005.TPIName,
                            NotificationRequired = objNDE005.NotificationRequired,
                            ThicknessOfJob = objNDE005.ThicknessOfJob,
                            SizeDimension = objNDE005.SizeDimension,
                            NDERemarks = objNDE005.NDERemarks,
                            CreatedBy = objNDE005.CreatedBy,
                            CreatedOn = objNDE005.CreatedOn,
                            EditedBy = objNDE005.EditedBy,
                            EditedOn = objNDE005.EditedOn,
                            ApprovedBy = objNDE005.ApprovedBy,
                            ApprovedOn = objNDE005.ApprovedOn,
                            SubmittedBy = objNDE005.SubmittedBy,
                            SubmittedOn = objNDE005.SubmittedOn
                        });
                    }
                    else
                    {
                        objNDE005_Log.Status = clsImplementationEnum.PTMTCTQStatus.Superseded.GetStringValue();
                        objNDE005_Log = db.NDE005_Log.Add(new NDE005_Log
                        {
                            HeaderId = objNDE005.HeaderId,
                            Project = objNDE005.Project,
                            BU = objNDE005.BU,
                            Location = objNDE005.Location,
                            RevNo = objNDE005.RevNo,
                            CTQNo = objNDE005.CTQNo,
                            Status = objNDE005.Status,
                            ManufacturingCode = objNDE005.ManufacturingCode,
                            ApplicableSpecification = objNDE005.ApplicableSpecification,
                            AcceptanceStandard = objNDE005.AcceptanceStandard,
                            ASMECodeStamp = objNDE005.ASMECodeStamp,
                            TPIName = objNDE005.TPIName,
                            NotificationRequired = objNDE005.NotificationRequired,
                            ThicknessOfJob = objNDE005.ThicknessOfJob,
                            SizeDimension = objNDE005.SizeDimension,
                            NDERemarks = objNDE005.NDERemarks,
                            CreatedBy = objNDE005.CreatedBy,
                            CreatedOn = objNDE005.CreatedOn,
                            EditedBy = objNDE005.EditedBy,
                            EditedOn = objNDE005.EditedOn,
                            ApprovedBy = objNDE005.ApprovedBy,
                            ApprovedOn = objNDE005.ApprovedOn,
                            SubmittedBy = objNDE005.SubmittedBy,
                            SubmittedOn = objNDE005.SubmittedOn
                        });
                    }
                    #endregion
                    db.SaveChanges();
                    if (lstNDE006_Log != null && lstNDE006_Log.Count > 0)
                    {
                        lstNDE006_Log.ForEach(x => x.RefId = objNDE005_Log.Id);
                        db.NDE006_Log.AddRange(lstNDE006_Log);
                    }
                    db.SaveChanges();

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE3.GetStringValue(), objNDE005.Project, objNDE005.BU, objNDE005.Location, "CTQ: " + objNDE005.CTQNo + " has been Approved", clsImplementationEnum.NotificationType.Information.GetStringValue());
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details successfully approved.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Details not available approved.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return objResponseMsg;
        }

        [HttpPost]
        public ActionResult UpdateReturnRemarks(int LineID, string changeText)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                NDE006 objNDE006 = db.NDE006.Where(x => x.LineId == LineID).FirstOrDefault();
                if (objNDE006 != null)
                {
                    if (!string.IsNullOrWhiteSpace(changeText))
                    {
                        objNDE006.ReturnRemark = changeText;
                    }
                    else
                    {
                        objNDE006.ReturnRemark = null;
                    }
                    objNDE006.EditedBy = objClsLoginInfo.UserName;
                    objNDE006.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details remarks successfully updated.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Details not available";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReturnLines(int HeaderId, string returnRemarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (!IsapplicableForAttend(HeaderId))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "This record has been already attended, Please refresh page.";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                string Status = clsImplementationEnum.RTCTQStatus.SendForApproval.GetStringValue().ToUpper();
                //List<NDE006> lstNDE006 = db.NDE006.Where(x => x.HeaderId == HeaderId && x.Status.ToUpper().Trim() == Status.ToUpper().Trim() && x.ReturnRemark != null).ToList();
                List<NDE006> lstNDE006 = db.NDE006.Where(x => x.HeaderId == HeaderId && x.ReturnRemark != null).ToList();

                if (lstNDE006.Count > 0 || !string.IsNullOrWhiteSpace(returnRemarks))
                {
                    foreach (var objData in lstNDE006)
                    {
                        NDE006 objNDE006 = lstNDE006.Where(x => x.LineId == objData.LineId).FirstOrDefault();
                       // objNDE006.Status = clsImplementationEnum.CTQStatus.Returned.GetStringValue();
                        objNDE006.ReturnedBy = objClsLoginInfo.UserName;
                        objNDE006.ReturnedOn = DateTime.Now;
                        objNDE006.EditedBy = objClsLoginInfo.UserName;
                        objNDE006.EditedOn = DateTime.Now;
                    }
                    NDE005 objNDE005 = db.NDE005.Where(x => x.HeaderId == HeaderId && x.Status.ToUpper() == Status).FirstOrDefault();
                    objNDE005.Status = clsImplementationEnum.CTQStatus.Returned.GetStringValue();
                    objNDE005.EditedBy = objClsLoginInfo.UserName;
                    objNDE005.EditedOn = DateTime.Now;
                    objNDE005.ReturnRemarks = returnRemarks;
                    db.SaveChanges();

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE3.GetStringValue(), objNDE005.Project, objNDE005.BU, objNDE005.Location, "CTQ: " + objNDE005.CTQNo + " has been Returned", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/NDE/MaintainUTCTQ/AddUpdateUTCTQ?HeaderID=" + objNDE005.HeaderId);
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details successfully returned.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please add return remarks for header/lines.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_UTCTQ_GETHEADER(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      RevNo = li.RevNo,
                                      CTQNo = li.CTQNo,
                                      Status = li.Status,
                                      ApplicableSpecification = li.ApplicableSpecification,
                                      AcceptanceStandard = li.AcceptanceStandard,
                                      ASMECodeStamp = li.ASMECodeStamp,
                                      TPIName = li.TPIName,
                                      NotificationRequired = li.NotificationRequired,
                                      ThicknessOfJob = li.ThicknessOfJob,
                                      SizeDimension = li.SizeDimension,
                                      ManufacturingCode = li.ManufacturingCode,
                                      NDERemarks = li.NDERemarks,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn
                                  }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else 
                {
                    var lst = db.SP_GET_UT_HEADER_LINES(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      SrNo = Convert.ToString(uc.ROW_NO),
                                      Description = Convert.ToString(uc.Description),
                                      ApplicableForAScan = Convert.ToString(uc.ApplicableForAScan),
                                      ApplicableForToFD = Convert.ToString(uc.ApplicableForToFD),
                                      ApplicableForPAUT = Convert.ToString(uc.ApplicableForPAUT),
                                      SpecificationNo = Convert.ToString(uc.SpecificationNo),
                                      SpecificationWithClauseNo = Convert.ToString(uc.SpecificationWithClauseNo),
                                      ToBeDiscussed = Convert.ToString(uc.ToBeDiscussed == true ? "Yes" : uc.ToBeDiscussed == false ? "No" : ""),
                                      SpecificationRequirement = Convert.ToString(uc.SpecificationRequirement),
                                      CriticalToQuality = Convert.ToString(uc.CriticalToQuality == true ? "Yes" : uc.CriticalToQuality == false ? "No" : ""),
                                      CTQRequirement = Convert.ToString(uc.CTQRequirement),
                                      ReturnRemark = Convert.ToString(uc.ReturnRemark),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }    

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

    }
}