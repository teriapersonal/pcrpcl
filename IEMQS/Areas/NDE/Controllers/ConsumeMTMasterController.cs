﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.NDE.Controllers
{
    public class ConsumeMTMasterController : clsBase
    {
        // GET: NDE/ConsumeMTMaster
        /// <summary>
        /// created by nikita vibhandik : 10/7/2017
        /// description :CRUD of MT consuamable
        /// </summary>
        /// <returns></returns>

        #region first Page
        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        // load datatable
        [HttpPost]
        public JsonResult LoadMTData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.MTStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('Draft','Returned')";
                }
                else
                {
                    strWhere += "1=1";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or (NDE.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%' or  QualityProject like '%" + param.sSearch + "%' or MTConsuNo like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "NDE.BU", "NDE.Location");

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_MT_CONSUMABLE_GETDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                           Convert.ToString(uc.QualityProject),
                           Convert.ToString(uc.Project),
                           Convert.ToString(uc.BU),
                           Convert.ToString(uc.Location),
                           Convert.ToString(uc.Method),
                           Convert.ToString(uc.MTConsuNo),
                           Convert.ToString(uc.CreatedBy),
                           Convert.ToString(uc.CreatedOn !=null?(Convert.ToDateTime(uc.CreatedOn)).ToString("dd/MM/yyyy"):""),
                           Convert.ToString(uc.Manufacturer),
                           Convert.ToString(uc.BrandType),
                            Convert.ToString(uc.ReturnRemarks),
                           Convert.ToString("R" +uc.RevNo),
                           Convert.ToString(uc.Status),
                           //Convert.ToString(uc.Id),
                           generateActionButtons(uc.Id,uc.Status,"/NDE/ConsumeMTMaster/AddConsumable?Id="+uc.Id,"/NDE/ConsumeMTMaster/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.Id),true,true,true,true,Convert.ToInt32(uc.RevNo))
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhere,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Tab
        [HttpPost]
        public ActionResult GetMTPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetMTHtmlPartial");
        }
        #endregion

        #region Create Consumable
        [SessionExpireFilter]
        [AllowAnonymous]
        // [UserPermissions]
        public ActionResult AddConsumable(int? id)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("NDE020");
            NDE020 objNDE020 = new NDE020();
            NDEModels objNDEModels = new NDEModels();
            var user = objClsLoginInfo.UserName;
            var BU = db.ATH001.Where(x => x.Employee == user && x.Location == objClsLoginInfo.Location).Select(x => x.BU).FirstOrDefault();
            var ManufacturerType = Manager.GetSubCatagories("Manufacturer Type", BU, objClsLoginInfo.Location).Select(i => new BULocWiseCategoryModel { CatDesc = i.Description, CatID = i.Code }).ToList();



            if (id != null)
            {
                objNDE020 = db.NDE020.Where(x => x.Id == id).FirstOrDefault();
                ViewBag.QualityProject = objNDE020.QualityProject;
                objNDE020.Location = db.COM002.Where(i => i.t_dtyp == 1 && i.t_dimx == objNDE020.Location).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
                objNDE020.Project = db.COM001.Where(i => i.t_cprj == objNDE020.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objNDE020.BU = db.COM002.Where(i => i.t_dtyp == 2 && i.t_dimx == objNDE020.BU).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
                ViewBag.MTMethod = objNDEModels.GetCategory(objNDE020.Method).CategoryDescription;
                ViewBag.BrandType = objNDEModels.GetCategory(objNDE020.BrandType).CategoryDescription;
                ViewBag.BrandType2 = !string.IsNullOrEmpty(objNDE020.BrandType2) ? objNDEModels.GetCategory(objNDE020.BrandType2).CategoryDescription : "";
                ViewBag.BrandType3 = !string.IsNullOrEmpty(objNDE020.BrandType3) ? objNDEModels.GetCategory(objNDE020.BrandType3).CategoryDescription : "";
                ViewBag.ManufacturingCode = db.QMS010.Where(x => x.QualityProject == objNDE020.QualityProject).Select(x => x.ManufacturingCode).FirstOrDefault();
                ViewBag.ManufacturerType = ManufacturerType;
                ViewBag.ManufacturerDesc = ManufacturerType.Where(x => x.CatID == objNDE020.Manufacturer).Select(x => x.CatDesc).FirstOrDefault();
                ViewBag.Action = "Edit";
            }
            else
            {
                objNDE020.Status = "Draft";
                objNDE020.RevNo = 0;
                objNDE020.Location = db.COM002.Where(i => i.t_dtyp == 1 && i.t_dimx == objClsLoginInfo.Location).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
                ViewBag.Action = "AddNew";
                ViewBag.ManufacturerType = ManufacturerType;
            }

            return View(objNDE020);
        }

        [HttpPost]
        public ActionResult DeleteHeader(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                NDE020 objNDE020 = db.NDE020.Where(x => x.Id == headerId).FirstOrDefault();
                if (objNDE020 != null)
                {
                    db.NDE020.Remove(objNDE020);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SendtoApprover(string project, string MTNumber, string Location)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string dStatus = clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue();
            try
            {
                string location = Location.Split('-')[0];
                NDE020 objNDE020 = db.NDE020.Where(u => u.QualityProject == project && u.MTConsuNo == MTNumber && u.Location == location).SingleOrDefault();
                if (objNDE020 != null)
                {
                    if (objNDE020.Id > 0)
                    {
                        if (!IsapplicableForSubmit(objNDE020.Id))
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "This record has been already submitted, Please refresh page.";
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }
                    }
                    objNDE020.Status = dStatus;
                    objNDE020.SubmittedBy = objClsLoginInfo.UserName;
                    objNDE020.SubmittedOn = DateTime.Now;
                    db.SaveChanges();

                    #region Send Notification
                    string NDE2 = clsImplementationEnum.UserRoleName.NDE2.GetStringValue();
                    (new clsManager()).SendNotification(NDE2, objNDE020.Project, objNDE020.BU, objNDE020.Location, "Consumable: " + objNDE020.MTConsuNo + " has come for your Approval", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/NDE/ApproveMTConsume/ApproveConsumable?Id=" + objNDE020.Id);
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.NDEConsumeMessage.Approve.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please save the details";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        private bool IsapplicableForSubmit(int Id)
        {
            NDE020 objNDE020 = db.NDE020.Where(x => x.Id == Id).FirstOrDefault();
            if (objNDE020 != null)
            {
                if (objNDE020.Status == clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue() || objNDE020.Status == clsImplementationEnum.PTMTCTQStatus.Returned.GetStringValue())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return true;
            
        }


        private bool IsapplicableForSave(int Id)
        {
            NDE020 objNDE020 = db.NDE020.Where(x => x.Id == Id).FirstOrDefault();
            if (objNDE020 != null)
            {
                if (objNDE020.Status == clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue())
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return true;
        }

        [HttpPost]
        public ActionResult SaveMT(FormCollection fc, NDE020 nde020)
        {
            NDE020 objNDE020 = new NDE020();
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
               
                string location = fc["Location"].Split('-')[0];
                string Status = clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue();
                string Approved = clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue();
                if (nde020.Id > 0)
                {
                    if (!IsapplicableForSave(nde020.Id))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This record has been already submitted, Please refresh page.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    objNDE020 = db.NDE020.Where(x => x.Id == nde020.Id).FirstOrDefault();
                    objNDE020.RevNo = nde020.RevNo;
                    if (objNDE020.Status == Approved)
                    {
                        objNDE020.RevNo = objNDE020.RevNo + 1;
                        objNDE020.ReturnRemarks = null;
                        objNDE020.ReturnedBy = null;
                        objNDE020.ReturnedOn = null;
                        objNDE020.SubmittedBy = null;
                        objNDE020.SubmittedOn = null;
                        objNDE020.ApprovedOn = null;
                    }
                    objNDE020.Status = Status;
                    objNDE020.Manufacturer = nde020.Manufacturer;
                    objNDE020.BrandType = nde020.BrandType;
                    objNDE020.BrandType2 = nde020.BrandType2;
                    objNDE020.BrandType3 = nde020.BrandType3;
                    objNDE020.BatchNo = nde020.BatchNo;
                    objNDE020.BatchNo2 = nde020.BatchNo2;
                    objNDE020.BatchNo3 = nde020.BatchNo3;
                    objNDE020.BatchDate1 = nde020.BatchDate1;
                    objNDE020.BatchDate2 = nde020.BatchDate2;
                    objNDE020.BatchDate3 = nde020.BatchDate3;
                    objNDE020.BatchDate4 = nde020.BatchDate4;
                    objNDE020.PlannerRemarks = nde020.PlannerRemarks;
                    //objNDE020.CreatedBy = nde020.CreatedBy;
                    //objNDE020.CreatedOn = nde020.CreatedOn;
                    objNDE020.EditedBy = objClsLoginInfo.UserName;
                    objNDE020.EditedOn = DateTime.Now;
                    int Id = objNDE020.Id;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = Id.ToString();
                    objResponseMsg.Status = objNDE020.Status;
                    objResponseMsg.RevNo = objNDE020.RevNo;
                    objResponseMsg.Remarks = objNDE020.ReturnRemarks;
                    db.SaveChanges();
                }
                else
                {
                    string doc = nde020.MTConsuNo.Split('/')[2];
                    string loc = fc["Location"].Split('-')[0];
                    int docNo = Convert.ToInt32(doc);
                    objNDE020.QualityProject = nde020.QualityProject;
                    objNDE020.Project = fc["Project"].Split('-')[0];
                    objNDE020.BU = fc["BU"].Split('-')[0];
                    objNDE020.Location = location;
                    objNDE020.RevNo = 0;
                    objNDE020.MTConsuNo = nde020.MTConsuNo;
                    objNDE020.DocNo = docNo;
                    objNDE020.Status = Status;
                    objNDE020.Method = nde020.Method;
                    objNDE020.Manufacturer = nde020.Manufacturer;
                    objNDE020.BrandType = nde020.BrandType;
                    objNDE020.BrandType2 = nde020.BrandType2;
                    objNDE020.BrandType3 = nde020.BrandType3;
                    objNDE020.BatchNo = nde020.BatchNo;
                    objNDE020.BatchNo2 = nde020.BatchNo2;
                    objNDE020.BatchNo3 = nde020.BatchNo3;
                    objNDE020.PlannerRemarks = nde020.PlannerRemarks;
                    objNDE020.BatchDate1 = nde020.BatchDate1;
                    objNDE020.BatchDate2 = nde020.BatchDate2;
                    objNDE020.BatchDate3 = nde020.BatchDate3;
                    objNDE020.BatchDate4 = nde020.BatchDate4;
                    objNDE020.CreatedBy = objClsLoginInfo.UserName;
                    objNDE020.CreatedOn = DateTime.Now;
                    db.NDE020.Add(objNDE020);
                    db.SaveChanges();
                    int Id = objNDE020.Id;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = Id.ToString();
                    objResponseMsg.Status = objNDE020.Status;
                    objResponseMsg.RevNo = objNDE020.RevNo;
                    objResponseMsg.Remarks = objNDE020.ReturnRemarks;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg);
        }
        [HttpPost]
        public ActionResult DeleteMT(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                NDE020 objNDE020 = db.NDE020.Where(x => x.Id == Id).FirstOrDefault();
                db.NDE020.Remove(objNDE020);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.NDEConsumeMessage.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Common Functions
        //project by quality project
        [HttpPost]
        public ActionResult GetProject(string qms)
        {   //get project and BU by Qms project
            if (!string.IsNullOrWhiteSpace(qms))
            {
                NDE020 objNDE020 = new NDE020();
                BUWiseDropdown objProjectDataModel = new BUWiseDropdown();

                string project = db.QMS010.Where(a => a.QualityProject == qms).FirstOrDefault().Project;

                var lstProject = (from a in db.COM001
                                  where a.t_cprj == project
                                  select new { ProjectDesc = a.t_cprj + " - " + a.t_dsca }).FirstOrDefault();
                string BU = db.COM001.Where(i => i.t_cprj == project).FirstOrDefault().t_entu;

                var BUDescription = (from a in db.COM002
                                     where a.t_dtyp == 2 && a.t_dimx == BU
                                     select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();

                objProjectDataModel.Project = lstProject.ProjectDesc;
                objProjectDataModel.BUDescription = BUDescription.BUDesc;

                List<BULocWiseCategoryModel> lstBULocWiseMethodModel;
                List<BULocWiseCategoryModel> lstBULocWiseBrandModel;
                objProjectDataModel.ManufacturingCode = db.QMS010.Where(x => x.QualityProject == qms).Select(x => x.ManufacturingCode).FirstOrDefault();

                List<GLB002> lstMethodGLB002 = Manager.GetSubCatagories("MT Method", BU, objClsLoginInfo.Location).ToList();
                if (lstMethodGLB002.Count > 0)
                {
                    lstBULocWiseMethodModel = lstMethodGLB002.Select(x => new BULocWiseCategoryModel { CatDesc = (x.Code + "-" + x.Description), CatID = x.Code }).ToList();
                    objProjectDataModel.lstBULocWiseMethodModel = lstBULocWiseMethodModel;
                }

                List<GLB002> lstBrandGLB002 = Manager.GetSubCatagories("MT Brand Type", BU, objClsLoginInfo.Location).ToList();
                if (lstBrandGLB002.Count > 0)
                {
                    lstBULocWiseBrandModel = lstBrandGLB002.Select(x => new BULocWiseCategoryModel { CatDesc = (x.Code + "-" + x.Description), CatID = x.Code }).ToList();
                    objProjectDataModel.lstBULocWiseBrandModel = lstBULocWiseBrandModel;
                }

                if (lstBrandGLB002.Any() && lstMethodGLB002.Any())
                    objProjectDataModel.Key = true;
                else
                {
                    List<string> lstCategory = new List<string>();
                    if (!lstMethodGLB002.Any())
                        lstCategory.Add("MT Method");
                    if (!lstBrandGLB002.Any())
                        lstCategory.Add("MT Brand Type");

                    objProjectDataModel.Key = false;
                    objProjectDataModel.Value = string.Join(",", lstCategory.ToList()) + " Category not exist for selected project/Quality Project, Please contact Admin";
                }
                return Json(objProjectDataModel, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        //generate MT Number
        public string MakeDocNumber(string qms, string method, string loc)
        {
            string location = loc.Split('-')[0];
            var lstobjNDE020 = db.NDE020.Where(x => x.QualityProject == qms && x.Method == method && x.Location == location).ToList();
            string strMethod = db.GLB002.Where(i => i.Code == method).Select(i => i.Code).FirstOrDefault();
            string mtNumber = "";
            string mtConsuNo = string.Empty;

            if (lstobjNDE020 != null && lstobjNDE020.Count() != 0)
            {
                var mtDocNum = (from a in db.NDE020
                                where a.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && a.Method == method && a.QualityProject.Equals(qms, StringComparison.OrdinalIgnoreCase)
                                select a).Max(a => a.DocNo);

                mtNumber = (mtDocNum + 1).ToString();
            }
            else
            {
                mtNumber = "1";
            }
            mtConsuNo = "MT/" + strMethod + "/" + mtNumber.PadLeft(3, '0');
            return mtConsuNo;
        }

        //fetch MT Number
        [HttpPost]
        public ActionResult GetMTNumber(string qms, string method, string loc)
        {
            NDE020 objNDE020 = new NDE020();
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            MethodResponse objMethodResponse = new MethodResponse();
            try
            {
                string mtConsuNo = MakeDocNumber(qms, method, loc);
                objMethodResponse.Method = mtConsuNo;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objMethodResponse.Key = false;
                objMethodResponse.Method = ex.Message.ToString();
            }
            return Json(objMethodResponse);
        }

        #endregion

        #region History
        [SessionExpireFilter]
        [HttpPost]
        public ActionResult GetMTHistoryDetails(int Id) //partial for CTQ History Lines Details
        {
            NDE020_Log objNDE020_Log = new NDE020_Log();
            objNDE020_Log = db.NDE020_Log.Where(x => x.Id == Id).FirstOrDefault();
            return PartialView("_MTConsumableHistoryPartial", objNDE020_Log);
        }

        [SessionExpireFilter]
        public ActionResult ViewLogDetails(int? Id)
        {
            NDE020_Log objLogNDE020 = new NDE020_Log();
            NDEModels objNDEModels = new NDEModels();
            var user = objClsLoginInfo.UserName;

            if (Id != null)
            {
                objLogNDE020 = db.NDE020_Log.Where(x => x.LogId == Id).FirstOrDefault();
                //ViewBag.BrandType = db.GLB002.Where(i => i.Code == objLogNDE020.BrandType).Select(i => i.Code + " - " + i.Description).FirstOrDefault();
                ViewBag.BrandType = !string.IsNullOrEmpty(objLogNDE020.BrandType) ? objNDEModels.GetCategory(objLogNDE020.BrandType).CategoryDescription : "";
                ViewBag.BrandType2 = !string.IsNullOrEmpty(objLogNDE020.BrandType2) ? objNDEModels.GetCategory(objLogNDE020.BrandType2).CategoryDescription : "";
                ViewBag.BrandType3 = !string.IsNullOrEmpty(objLogNDE020.BrandType3) ? objNDEModels.GetCategory(objLogNDE020.BrandType3).CategoryDescription : "";
                ViewBag.Method = db.GLB002.Where(i => i.Code == objLogNDE020.Method).Select(i => i.Code + " - " + i.Description).FirstOrDefault();
                objLogNDE020.QualityProject = db.QMS010.Where(i => i.QualityProject == objLogNDE020.QualityProject).Select(i => i.QualityProject).FirstOrDefault();
                objLogNDE020.Project = db.COM001.Where(i => i.t_cprj == objLogNDE020.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objLogNDE020.BU = db.COM002.Where(i => i.t_dtyp == 2 && i.t_dimx == objLogNDE020.BU).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
                objLogNDE020.Location = db.COM002.Where(i => i.t_dtyp == 1 && i.t_dimx == objLogNDE020.Location).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
                ViewBag.ManufacturingCode = db.QMS010.Where(x => x.QualityProject == objLogNDE020.QualityProject).Select(x => x.ManufacturingCode).FirstOrDefault();

            }

            return View(objLogNDE020);
        }

        [HttpPost]
        public JsonResult LoadMTHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;

                strWhere += "1=1 and Id=" + param.Headerid;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or (NDE.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%' or  QualityProject like '%" + param.sSearch + "%' or MTConsuNo like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%')";
                }

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_NDE_MT_CONSUMABLE_HISTORY_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                           Convert.ToString(uc.QualityProject),
                           Convert.ToString(uc.Method),
                           Convert.ToString(uc.MTConsuNo),
                           Convert.ToString(uc.CreatedBy),
                           Convert.ToString(uc.CreatedOn !=null?(Convert.ToDateTime(uc.CreatedOn)).ToString("dd/MM/yyyy"):""),
                           Convert.ToString("R " +uc.RevNo),
                           Convert.ToString(uc.Status),
                           generateActionButtons(uc.LogId,uc.Status,"/NDE/ConsumeMTMaster/ViewLogDetails?Id="+uc.LogId,"/NDE/ConsumeMTMaster/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.LogId) + "&isHistory=true",true,false,true,false,Convert.ToInt32(uc.RevNo))
                           //Convert.ToString(uc.LogId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhere,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Copy data
        [SessionExpireFilter]
        [HttpPost]
        public ActionResult GetCopyDetails(string projectid) //partial for Copy Details
        {
            //ViewBag.QualityProject = new SelectList(db.TMP001.ToList(), "QualityProject", "QualityProject");
            return PartialView("~/Areas/NDE/Views/Shared/_CopyQualityProjectPartial.cshtml");
        }

        [SessionExpireFilter]
        [HttpPost]
        public ActionResult copyDetails(string QualityProject, string method, int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string currentUser = objClsLoginInfo.UserName;
                var currentLoc = objClsLoginInfo.Location;

                string strProject = new NDEModels().GetQMSProject(QualityProject).Project;

                //NDE020 objNDE020 = db.NDE020.Where(x => x.QualityProject == QualityProject && x.Method == method && x.Location == currentLoc).FirstOrDefault();
                NDE020 objExistingNDE020 = db.NDE020.Where(x => x.Id == headerId).FirstOrDefault();
                string ptConsuNo = MakeDocNumber(QualityProject, method, currentLoc);
                string BU = db.COM001.Where(i => i.t_cprj == strProject).FirstOrDefault().t_entu;
                var Category = new NDEModels().GetCategoryId("MT Method");
                if (Manager.isMethodExistForBULocation(BU, currentLoc, objExistingNDE020.Method, Category))
                {
                    NDE020 objDesNDE020 = new NDE020();

                    objDesNDE020.QualityProject = QualityProject;
                    objDesNDE020.Project = strProject;
                    objDesNDE020.BU = BU;
                    objDesNDE020.Location = currentLoc;
                    objDesNDE020.MTConsuNo = ptConsuNo;
                    objDesNDE020.Method = method;
                    objResponseMsg = InsertNewDoc(objExistingNDE020, objDesNDE020, false);
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Technique Is Not Available For Destination Project!";
                }




                #region old code
                //if (objNDE020 != null)
                //{
                //    if (objNDE020.Location.Trim() == currentLoc)
                //    {
                //        if (objNDE020.Status == clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue() || objNDE020.Status == clsImplementationEnum.NDETechniqueStatus.Returned.GetStringValue())
                //        {
                //            objResponseMsg = InsertNewDoc(objExistingNDE020, objNDE020, true);
                //        }
                //        else
                //        {
                //            objResponseMsg.Key = false;
                //            objResponseMsg.Value = "Document MT Consumable already exists.";
                //        }
                //    }
                //    else
                //    {
                //        NDE020 objDesNDE020 = new NDE020();
                //        string BU = db.COM001.Where(i => i.t_cprj == strProject).FirstOrDefault().t_entu;
                //        objDesNDE020.QualityProject = QualityProject;
                //        objDesNDE020.Project = strProject;
                //        objDesNDE020.BU = BU;
                //        objDesNDE020.Location = currentLoc;
                //        objDesNDE020.Method = method;
                //        objDesNDE020.MTConsuNo = ptConsuNo;
                //        objResponseMsg = InsertNewDoc(objExistingNDE020, objDesNDE020, false);
                //    }
                //}
                //else
                //{
                //    NDE020 objDesNDE020 = new NDE020();

                //    string BU = db.COM001.Where(i => i.t_cprj == strProject).FirstOrDefault().t_entu;
                //    objDesNDE020.QualityProject = QualityProject;
                //    objDesNDE020.Project = strProject;
                //    objDesNDE020.BU = BU;
                //    objDesNDE020.Location = currentLoc;
                //    objDesNDE020.MTConsuNo = ptConsuNo;
                //    objDesNDE020.Method = method;
                //    objResponseMsg = InsertNewDoc(objExistingNDE020, objDesNDE020, false);
                //}
                #endregion

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponseMsg InsertNewDoc(NDE020 objSrcNDE020, NDE020 objDesNDE020, bool IsEdited)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                string doc = objDesNDE020.MTConsuNo.Split('/')[2];
                int docNo = Convert.ToInt32(doc);
                objDesNDE020.DocNo = docNo;
                objDesNDE020.RevNo = 0;
                objDesNDE020.Status = clsImplementationEnum.NDETechniqueStatus.Draft.ToString();
                objDesNDE020.Manufacturer = objSrcNDE020.Manufacturer;
                objDesNDE020.BrandType = objSrcNDE020.BrandType;
                objDesNDE020.BrandType2 = objSrcNDE020.BrandType2;
                objDesNDE020.BrandType3 = objSrcNDE020.BrandType3;
                objDesNDE020.BatchNo = objSrcNDE020.BatchNo;
                objDesNDE020.BatchNo2 = objSrcNDE020.BatchNo2;
                objDesNDE020.BatchNo3 = objSrcNDE020.BatchNo3;
                objDesNDE020.PlannerRemarks = objSrcNDE020.PlannerRemarks;
                objDesNDE020.BatchDate1 = objSrcNDE020.BatchDate1;
                objDesNDE020.BatchDate2 = objSrcNDE020.BatchDate2;
                objDesNDE020.BatchDate3 = objSrcNDE020.BatchDate3;
                objDesNDE020.BatchDate4 = objSrcNDE020.BatchDate4;
                #region old code
                //if (IsEdited)
                //{
                //    objDesNDE020.EditedBy = objClsLoginInfo.UserName;
                //    objDesNDE020.EditedOn = DateTime.Now;
                //    db.SaveChanges();
                //    objResponseMsg.Key = true;
                //    objResponseMsg.Value = clsImplementationMessage.NDETechniquesMessage.HeaderUpdate.ToString();
                //    objResponseMsg.HeaderID = objDesNDE020.Id;
                //}
                //else
                //{
                #endregion
                objDesNDE020.CreatedBy = objClsLoginInfo.UserName;
                objDesNDE020.CreatedOn = DateTime.Now;
                db.NDE020.Add(objDesNDE020);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.NDETechniquesMessage.Copy.ToString();
                objResponseMsg.HeaderID = objDesNDE020.Id;
                objResponseMsg.Value = "Data Copied Successfully To Document No : " + objDesNDE020.MTConsuNo + " of " + objDesNDE020.Project + " Project.";
                //}

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return objResponseMsg;
        }
        #endregion

        #region Classes

        public class ResponceMsgWithHeaderID : clsHelper.ResponseMsg
        {
            public int HeaderID;
            public string doc;
            public string Status;
            public int? RevNo;
            public string Remarks;
        }

        public class BUWiseDropdown : ProjectDataModel
        {
            public string Project { get; set; }
            public new string ManufacturingCode { get; set; }
            public string Value { get; set; }
            public bool Key { get; set; }
            public List<BULocWiseCategoryModel> lstBULocWiseMethodModel { get; set; }
            public List<BULocWiseCategoryModel> lstBULocWiseBrandModel { get; set; }
            public List<BULocWiseCategoryModel> lstBULocWisePBrandModel { get; set; }
            public List<BULocWiseCategoryModel> lstBULocWiseCBrandModel { get; set; }
            public List<BULocWiseCategoryModel> lstBULocWiseDBrandModel { get; set; }
            public List<BULocWiseCategoryModel> lstBULocWiseP_manufacturer { get; set; }
            public List<BULocWiseCategoryModel> lstBULocWiseC_manufacturer { get; set; }
            public List<BULocWiseCategoryModel> lstBULocWiseD_manufacturer { get; set; }
        }
        #endregion

        #region Export to Excel
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
            {
                var lstResult = db.SP_MT_CONSUMABLE_GETDETAILS
                                (
                                1, int.MaxValue, strSortOrder, whereCondition
                                ).Select(x => new { x.QualityProject, x.Project, x.BU, x.Location, x.Method, x.MTConsuNo, x.Manufacturer, x.BrandType, x.ReturnRemarks, x.RevNo, x.Status, x.CreatedBy }).ToList();

                if (lstResult != null && lstResult.Count > 0)
                {
                    string str = Helper.GenerateExcel(lstResult, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = str;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Data not available.";
                }
            }
            else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
            {
                var lstResult = db.SP_NDE_MT_CONSUMABLE_HISTORY_DETAILS
                           (
                           1, int.MaxValue, strSortOrder, whereCondition
                           ).Select(x => new { x.QualityProject, x.Project, x.BU, x.Location, x.Method, x.MTConsuNo, x.Manufacturer, x.BrandType, x.ReturnRemarks, x.RevNo, x.Status, x.CreatedBy }).ToList();

                if (lstResult != null && lstResult.Count > 0)
                {
                    string str = Helper.GenerateExcel(lstResult, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = str;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Data not available.";
                }
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Data not available.";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Show time line
        public ActionResult ShowTimeline(int HeaderId, bool isHistory = false)
        {
            TimelineViewModel model = new TimelineViewModel();
            if (isHistory)
            {
                NDE020_Log objNDE020 = db.NDE020_Log.Where(x => x.LogId == HeaderId).FirstOrDefault();
                model.Title = "NDECONSUMABLE HEADER";
                model.ApprovedBy = Manager.GetUserNameFromPsNo(objNDE020.ApprovedBy);
                model.ApprovedOn = objNDE020.ApprovedOn;
                model.SubmittedBy = Manager.GetUserNameFromPsNo(objNDE020.SubmittedBy);
                model.SubmittedOn = objNDE020.SubmittedOn;
                model.CreatedBy = Manager.GetUserNameFromPsNo(objNDE020.CreatedBy);
                model.CreatedOn = objNDE020.CreatedOn;
                model.ReturnedBy = Manager.GetUserNameFromPsNo(objNDE020.ReturnedBy);
                model.ReturnedBy = objNDE020.ReturnedBy;
            }
            else
            {
                NDE020 objNDE020 = db.NDE020.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.Title = "NDECONSUMABLE HEADER";
                model.ApprovedBy = Manager.GetUserNameFromPsNo(objNDE020.ApprovedBy);
                model.ApprovedOn = objNDE020.ApprovedOn;
                model.SubmittedBy = Manager.GetUserNameFromPsNo(objNDE020.SubmittedBy);
                model.SubmittedOn = objNDE020.SubmittedOn;
                model.CreatedBy = Manager.GetUserNameFromPsNo(objNDE020.CreatedBy);
                model.CreatedOn = objNDE020.CreatedOn;
                model.ReturnedBy = Manager.GetUserNameFromPsNo(objNDE020.ReturnedBy);
                model.ReturnedBy = objNDE020.ReturnedBy;
            }
            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        #endregion

        #region common function
        public string generateActionButtons(int HeaderId, string status, string viewUrl = "", string timelineUrl = "", bool isViewButton = false, bool isDeleteButton = false, bool isTimelinebutton = false, bool isHistoryButton = false, int Revision = 0)
        {
            string strButtons = "<center>";
            if (isViewButton)
            {
                strButtons += "<a title=\"View\" href=\"" + WebsiteURL + viewUrl + "\"><i style=\"margin-left:5px;\" class=\"fa fa-eye\"></i></a>";
            }
            if (isDeleteButton)
            {
                if (status == clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue() && Revision == 0)
                {
                    strButtons += "<i id=\"btnDelete\" name=\"btnAction\" style=\"cursor: pointer;margin-left:5px;\" Title=\"Delete Record\" class=\"fa fa-trash-o\" onclick=\"DeleteHeader(" + HeaderId + ")\"></i>";
                }
                else
                {
                    strButtons += "<i id=\"btnDelete\" name=\"btnAction\" style=\"cursor: pointer; opacity: 0.5;margin-left:5px;\" Title=\"Delete Record\" class=\"fa fa-trash-o\"></i>";
                }
            }
            if (isTimelinebutton)
            {
                strButtons += "<a title=\"View Timeline\" href=\"javascript:void(0)\" onclick=\"ShowTimeline('" + timelineUrl + "');\"><i style=\"margin-left:5px;\" class=\"fa fa-clock-o\"></i></a>";
            }
            if (isHistoryButton)
            {
                if (Revision > 0 || status == clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue())
                {
                    strButtons += "<i title=\"History\" onclick=History('" + Convert.ToInt32(HeaderId) + "'); style = \"cursor: pointer;margin-left:5px;\" class=\"fa fa-history\"></i>";
                }
                else
                {
                    strButtons += "<i title=\"History\" style = \"cursor: pointer;margin-left:5px;opacity:0.5\" class=\"fa fa-history\"></i>";
                }
            }
            strButtons += "</center>";
            return strButtons;
        }
        #endregion
    }
}