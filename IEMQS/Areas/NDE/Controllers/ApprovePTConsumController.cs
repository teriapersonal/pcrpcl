﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.NDE.Controllers
{
    public class ApprovePTConsumController : clsBase
    {
        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]

        // GET: NDE/ApprovePTConsum
        public ActionResult Index()
        {
            return View();
        }

        #region Tab
        [HttpPost]
        public ActionResult GetPTPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_approvePTHtmlPartial");
        }

        // load datatable
        [HttpPost]
        public JsonResult LoadPTData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.MTStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.NDETechniqueStatus.SentForApproval.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "1=1";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (bcom2.t_desc like '%" + param.sSearch +
                                     "%' or lcom2.t_desc like '%" + param.sSearch +
                                     "%' or (NDE.Project+'-'+com1.t_dsca) like '%" + param.sSearch +
                                     "%' or  QualityProject like '%" + param.sSearch +
                                     "%' or PTConsuNo like '%" + param.sSearch +
                                     "%' or BU like '%" + param.sSearch +
                                     "%' or Location like '%" + param.sSearch +
                                     "%' or Method like '%" + param.sSearch +
                                     "%' or PenetrantManufacturer like '%" + param.sSearch +
                                     "%' or PenetrantBrandType like '%" + param.sSearch +
                                     "%' or ReturnRemarks like '%" + param.sSearch +
                                     "%' or RevNo like '%" + param.sSearch +
                                     "%' or CreatedBy like '%" + param.sSearch +
                                     "%'  or Status like '%" + param.sSearch + "%')";
                }
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "NDE.BU", "NDE.Location");

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                NDEModels objNDEModels = new NDEModels();
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_PT_CONSUMABLE_GETDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.HeaderId),
                            Convert.ToString(uc.QualityProject),
                            Convert.ToString(uc.Project),
                            Convert.ToString(uc.BU),
                            Convert.ToString(uc.Location),
                            Convert.ToString(uc.Method),
                            Convert.ToString(uc.PTConsuNo),
                            Convert.ToString(uc.CreatedBy),
                            Convert.ToString(uc.CreatedOn !=null?(Convert.ToDateTime(uc.CreatedOn)).ToString("dd/MM/yyyy"):""),
                            Convert.ToString(objNDEModels.GetCategory(uc.PenetrantManufacturer).CategoryDescription),
                            Convert.ToString(uc.PenetrantBrandType),
                            Convert.ToString(uc.ReturnRemarks),
                            Convert.ToString("R " +uc.RevNo),
                            Convert.ToString(uc.Status),
                           generateActionButtons(uc.HeaderId,uc.Status,"/NDE/ApprovePTConsum/ApproveConsumable?Id="+uc.HeaderId,"/NDE/ConsumePTMaster/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId),true,false,true,false,Convert.ToInt32(uc.RevNo))
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhere,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        [SessionExpireFilter]
        #region Approval Page
        public ActionResult ApproveConsumable(int? id)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("NDE021");
            NDE021 objNDE021 = new NDE021();
            var user = objClsLoginInfo.UserName;
            NDEModels objNDEModels = new NDEModels();

            if (id != null)
            {
                var objId = Convert.ToInt32(id);
                objNDE021 = db.NDE021.Where(x => x.HeaderId == objId).FirstOrDefault();
                ViewBag.BrandType = db.GLB002.Where(i => i.Code == objNDE021.PenetrantBrandType).Select(i => i.Code + " - " + i.Description).FirstOrDefault();
                ViewBag.Method = db.GLB002.Where(i => i.Code == objNDE021.Method).Select(i => i.Code + " - " + i.Description).FirstOrDefault();
                ViewBag.QualityProject = db.QMS010.Where(i => i.QualityProject == objNDE021.QualityProject).Select(i => i.QualityProject).FirstOrDefault();
                objNDE021.Project = db.COM001.Where(i => i.t_cprj == objNDE021.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objNDE021.BU = db.COM002.Where(i => i.t_dtyp == 2 && i.t_dimx == objNDE021.BU).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
                objNDE021.Location = db.COM002.Where(i => i.t_dtyp == 1 && i.t_dimx == objNDE021.Location).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
                ViewBag.ManufacturingCode = db.QMS010.Where(x => x.QualityProject == objNDE021.QualityProject).Select(x => x.ManufacturingCode).FirstOrDefault();

                ViewBag.PenetrantManufacturer = objNDEModels.GetCategory(objNDE021.PenetrantManufacturer).CategoryDescription;
                ViewBag.CleanerBrandType = objNDEModels.GetCategory(objNDE021.CleanerBrandType).CategoryDescription;
                ViewBag.CleanerManufacturer = objNDEModels.GetCategory(objNDE021.CleanerManufacturer).CategoryDescription;
                ViewBag.DeveloperBrandType = objNDEModels.GetCategory(objNDE021.DeveloperBrandType).CategoryDescription;
                ViewBag.DeveloperManufacturer = objNDEModels.GetCategory(objNDE021.DeveloperManufacturer).CategoryDescription;

            }

            return View(objNDE021);
        }

        [HttpPost]
        public ActionResult ApproveByApproverSelected(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
            try
            {
                for (int i = 0; i < arrayHeaderIds.Length; i++)
                {
                    int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                    db.SP_PT_CONSUMABLE_APPROVE(HeaderId, objClsLoginInfo.UserName);

                    #region Send Notification
                    NDE021 objNDE021 = db.NDE021.Where(c => c.HeaderId == HeaderId).FirstOrDefault();
                    if (objNDE021 != null)
                    {
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE3.GetStringValue(), objNDE021.Project, objNDE021.BU, objNDE021.Location, "Consumable: " + objNDE021.PTConsuNo + " has been Approved", clsImplementationEnum.NotificationType.Information.GetStringValue());
                    }
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.QCPMessages.Approve.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ApprovePT(int Id,string returnRemarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (!IsapplicableForAttend(Id))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "This record has been already attended, Please refresh page.";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                db.SP_PT_CONSUMABLE_APPROVE(Id, objClsLoginInfo.UserName);

                #region Send Notification
                NDE021 objNDE021 = db.NDE021.Where(c => c.HeaderId == Id).FirstOrDefault();
                if (objNDE021 != null)
                {
                    objNDE021.ReturnRemarks = returnRemarks;
                    db.SaveChanges();
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE3.GetStringValue(), objNDE021.Project, objNDE021.BU, objNDE021.Location, "Consumable: " + objNDE021.PTConsuNo + " has been Approved", clsImplementationEnum.NotificationType.Information.GetStringValue());
                }
                #endregion

                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.QCPMessages.Approve.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        private bool IsapplicableForAttend(int headerId)
        {
            NDE021 objNDE021 = db.NDE021.Where(x => x.HeaderId == headerId).FirstOrDefault();
            if(objNDE021!=null)
            {
                if (objNDE021.Status == clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        [HttpPost]
        public ActionResult ReturnPT(FormCollection fc, NDE021 nde021)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string returnStatus = clsImplementationEnum.NDETechniqueStatus.Returned.GetStringValue();
            try
            {
                if (!IsapplicableForAttend(nde021.HeaderId))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "This record has been already attended, Please refresh page.";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                NDE021 objNDE021 = db.NDE021.Where(u => u.HeaderId == nde021.HeaderId).SingleOrDefault();
                if (objNDE021 != null)
                {
                    objNDE021.Status = returnStatus;
                    objNDE021.ReturnRemarks = nde021.ReturnRemarks;
                    objNDE021.ApprovedBy = objClsLoginInfo.UserName;
                    objNDE021.ApprovedOn = DateTime.Now;
                    db.SaveChanges();

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE3.GetStringValue(), objNDE021.Project, objNDE021.BU, objNDE021.Location, "Consumable: " + objNDE021.PTConsuNo + " has been Returned", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/NDE/ConsumePTMaster/AddConsumable?Id=" + objNDE021.HeaderId);
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.QCPMessages.Approve.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public string generateActionButtons(int HeaderId, string status, string viewUrl = "", string timelineUrl = "", bool isViewButton = false, bool isDeleteButton = false, bool isTimelinebutton = false, bool isHistoryButton = false, int Revision = 0)
        {
            string strButtons = "<center>";
            if (isViewButton)
            {
                strButtons += "<a title=\"View\" href=\"" + WebsiteURL + viewUrl + "\"><i style=\"margin-left:5px;\" class=\"fa fa-eye\"></i></a>";
            }
            if (isDeleteButton)
            {
                if (status == clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue() && Revision == 0)
                {
                    strButtons += "<i id=\"btnDelete\" name=\"btnAction\" style=\"cursor: pointer;margin-left:5px;\" Title=\"Delete Record\" class=\"fa fa-trash-o\" onclick=\"DeleteHeader(" + HeaderId + ")\"></i>";
                }
                else
                {
                    strButtons += "<i id=\"btnDelete\" name=\"btnAction\" style=\"cursor: pointer; opacity: 0.5;margin-left:5px;\" Title=\"Delete Record\" class=\"fa fa-trash-o\"></i>";
                }
            }
            if (isTimelinebutton)
            {
                strButtons += "<a title=\"View Timeline\" href=\"javascript:void(0)\" onclick=\"ShowTimeline('" + timelineUrl + "');\"><i style=\"margin-left:5px;\" class=\"fa fa-clock-o\"></i></a>";
            }
            if (isHistoryButton)
            {
                if (Revision > 0 || status == clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue())
                {
                    strButtons += "<i title=\"History\" onclick=History('" + Convert.ToInt32(HeaderId) + "'); style = \"cursor: pointer;margin-left:5px;\" class=\"fa fa-history\"></i>";
                }
                else
                {
                    strButtons += "<i title=\"History\" style = \"cursor: pointer;margin-left:5px;opacity:0.5\" class=\"fa fa-history\"></i>";
                }
            }
            strButtons += "</center>";
            return strButtons;
        }
    }
}