﻿using IEMQS.Areas.NDE.Models;
using IEMQSImplementation;
using OfficeOpenXml;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace IEMQS.Areas.NDE.Controllers
{
    public class WelderPerformanceReportController : clsBase
    {
        // GET: NDE/WelderPerformanceReport


        public ActionResult Index()
        {
            NDEModels objNDEModels = new NDEModels();
            //ViewBag.Shops = objNDEModels.GetSubCatagory("Shop", objClsLoginInfo.Location, "");
            ViewBag.JointType = new List<CategoryData> { new CategoryData { Code = "true", Description = "Overlay" }, new CategoryData { Code = "false", Description = "Non-Overlay" } };
            return View();
        }

        [HttpPost]
        public ActionResult GetAllStagesType()
        {
            //var lstlocation = Manager.GetUserwiseLocation(objClsLoginInfo.UserName);
            //var lstBu = Manager.GetUserwiseBU(objClsLoginInfo.UserName);
            //List<CategoryData> lstCategoryData = new List<CategoryData>();
            //lstCategoryData = db.QMS002.Where(x => x.StageType != "" && lstlocation.Contains(x.Location) && lstBu.Contains(x.BU)).
            //                    Select(i =>
            //                    new CategoryData
            //                    { id = i.StageType, text = i.StageType }
            //                    ).Distinct().ToList();

            List<CategoryData> lstCategoryData = new List<CategoryData>();
            lstCategoryData.Add(new CategoryData() { id = "PT", text = "PT" });
            lstCategoryData.Add(new CategoryData() { id = "MT", text = "MT" });            
            lstCategoryData.Add(new CategoryData() { id = "RT", text = "RT" });
            lstCategoryData.Add(new CategoryData() { id = "UT", text = "UT" });

            return Json(lstCategoryData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetAllLocation()
        {
            try
            {
                var lstLoc = (from ath1 in db.ATH001
                              join com2 in db.COM002 on ath1.Location equals com2.t_dimx
                              where ath1.Employee.Trim() == objClsLoginInfo.UserName.Trim()
                              select new { id = ath1.Location, text = com2.t_desc }).Distinct().ToList();
                return Json(lstLoc, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetFromWelderDetail(string term)
        {
            try
            {
                var lstWelderDetail = (from a in db.QMS061
                                       where a.welderstamp.Contains(term)
                                       select new { id = a.welderstamp, text = a.welderstamp }
                                       ).Distinct().ToList();
                lstWelderDetail.Insert(0, new { id = "ALL", text = "ALL" });
                return Json(lstWelderDetail, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetWelderDetail(string term)
        {
            try
            {
                var lstWelderDetail = (from a in db.QMS061
                                       where a.welderstamp.Contains(term)
                                       select new { id = a.welderstamp, text = a.welderstamp }
                                       ).Distinct().ToList();
                return Json(lstWelderDetail, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetProcessCluster(string term)
        {
            try
            {
                var lstlocation = Manager.GetUserwiseLocation(objClsLoginInfo.UserName);
                var lstBu = Manager.GetUserwiseBU(objClsLoginInfo.UserName);
                var lstProcessCluster = (from a in db.QMS010
                                         where a.InspectionCentre.Contains(term) && lstlocation.Contains(a.Location) && lstBu.Contains(a.BU)
                                         select new { id = a.InspectionCentre, text = a.InspectionCentre }
                                       ).Distinct().ToList();

                return Json(lstProcessCluster, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult GetAllQualityProject(string term)
        {
            try
            {
                var lstProjectByUser = Manager.getProjectsByUser(objClsLoginInfo.UserName.Trim());
                string userLocation = objClsLoginInfo.Location.Trim();
                var lstQualityProject = (from lst in lstProjectByUser
                                         join qms10 in db.QMS010 on lst.projectCode equals qms10.Project
                                         where qms10.QualityProject.Contains(term)
                                         select new { id = qms10.QualityProject, text = qms10.QualityProject }).Distinct().ToList();
                return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        //new method based on Shop

        [HttpPost]
        public ActionResult GetShopDetailByLocation(string term, string Location)
        {
            try
            {
                List<string> locList = Location.Split(',').ToList();

                var objQMS60List = (from a in db.QMS060
                                    where locList.Contains(a.Location) && a.Shop.Contains(term)
                                    select new { Value = a.Shop, Text = a.Shop }).Distinct().OrderBy(o => o.Value).ToList();

                return Json(objQMS60List, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetQualityProjectByShop(string term, string FromShop, string ToShop)
        {
            try
            {
                List<string> list = new List<string>();

                string query = "";
                if (!string.IsNullOrWhiteSpace(FromShop) && !string.IsNullOrWhiteSpace(ToShop))
                {
                    query += "SELECT DISTINCT QualityProject FROM QMS060 where QualityProject is not null and QualityProject != '' and Shop between '" + FromShop + "' and '" + ToShop + "' ";

                    if (!string.IsNullOrWhiteSpace(term))
                    {
                        query += " and QualityProject like '%" + term + "%'";
                    }

                    query += " order by QualityProject";

                    list = db.Database.SqlQuery<string>(query).ToList();
                }

                var finalList = (from a in list
                                 select new { Value = a, Text = a }).Distinct().ToList();
                return Json(finalList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetFromWelderDetailByProject(string term, string FromProject, string ToProject)
        {
            try
            {
                List<string> list = new List<string>();

                string query = "";
                if (!string.IsNullOrWhiteSpace(FromProject) && !string.IsNullOrWhiteSpace(ToProject))
                {
                    query += "SELECT DISTINCT welderstamp FROM QMS061 where welderstamp is not null and welderstamp != '' and QualityProject between '" + FromProject + "' and '" + ToProject + "' ";

                    if (!string.IsNullOrWhiteSpace(term))
                    {
                        query += " and welderstamp like '%" + term + "%'";
                    }

                    query += " order by welderstamp";

                    list = db.Database.SqlQuery<string>(query).ToList();

                    list.Insert(0, "ALL");
                }
                                
                var finalList = (from a in list
                                 select new { Value = a, Text = a }).Distinct().ToList();
                return Json(finalList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetToWelderDetailByProject(string term, string FromProject, string ToProject)
        {
            try
            {
                List<string> list = new List<string>();

                string query = "";
                if (!string.IsNullOrWhiteSpace(FromProject) && !string.IsNullOrWhiteSpace(ToProject))
                {
                    query += "SELECT DISTINCT welderstamp FROM QMS061 where welderstamp is not null and welderstamp != '' and QualityProject between '" + FromProject + "' and '" + ToProject + "' ";

                    if (!string.IsNullOrWhiteSpace(term))
                    {
                        query += " and welderstamp like '%" + term + "%'";
                    }
                    query += " order by welderstamp";
                    list = db.Database.SqlQuery<string>(query).ToList();
                }

                var finalList = (from a in list
                                 select new { Value = a, Text = a }).Distinct().ToList();
                return Json(finalList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
    }
}