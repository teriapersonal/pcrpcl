﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace IEMQS.Areas.NDE.Controllers
{
    public class ApproveRTCTQController : clsBase
    {
        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        private bool IsapplicableForAttend(int headerId)
        {
            NDE003 objNDE003 = db.NDE003.Where(x => x.HeaderId == headerId).FirstOrDefault();
            if (objNDE003 != null)
            {
                if (objNDE003.Status == clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return true;
            
        }

        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult RTCTQLineDetails(string HeaderID)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("NDE003");
            NDEModels objNDEModels = new NDEModels();
            int HeaderId = 0;
            if (!string.IsNullOrWhiteSpace(HeaderID))
            {
                HeaderId = Convert.ToInt32(HeaderID);
            }
            NDE003 objNDE003 = db.NDE003.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            if (objNDE003 != null)
            {
                ViewBag.HeaderID = objNDE003.HeaderId;
                ViewBag.Status = objNDE003.Status;
                ViewBag.CTQNo = objNDE003.CTQNo;

                var projectDetails = db.COM001.Where(x => x.t_cprj == objNDE003.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                objNDE003.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                var BUDescription = db.COM002.Where(x => x.t_dimx == objNDE003.BU).Select(x => x.t_desc).FirstOrDefault();
                objNDE003.BU = Convert.ToString(objNDE003.BU + " - " + BUDescription);
                var Location = db.COM002.Where(x => x.t_dimx == objNDE003.Location).Select(x => x.t_desc).FirstOrDefault();
                objNDE003.Location = Convert.ToString(objNDE003.Location + " - " + Location);

            }
            return View(objNDE003);
        }

        [HttpPost]
        public ActionResult GetHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetHeaderGridDataPartial");
        }

        [HttpPost]
        public ActionResult GetCTQHeaderLinesHtml(int HeaderId, string strCTQNo)
        {
            ViewBag.HeaderId = HeaderId;
            ViewBag.strCTQNo = strCTQNo;
            ViewBag.CTQStatus = db.NDE003.Where(x => x.HeaderId == HeaderId).Select(x => x.Status).FirstOrDefault();
            return PartialView("_GetHeaderLinesHtml");
        }

        [HttpPost]
        public JsonResult LoadRTCTQHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.RTCTQStatus.SendForApproval.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "1=1";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (ltrim(rtrim(nde3.BU))+'-'+bcom2.t_desc like '%" + param.sSearch +
                                "%' or ltrim(rtrim(nde3.Location))+'-'+lcom2.t_desc like '%" + param.sSearch +
                                "%' or (nde3.Project+'-'+com1.t_dsca) like '%" + param.sSearch +
                                "%' or CTQNo like '%" + param.sSearch +
                                "%' or RevNo like '%" + param.sSearch +
                                "%' or ApplicableSpecification like '%" + param.sSearch +
                                "%' or AcceptanceStandard like '%" + param.sSearch +
                                "%' or ASMECodeStamp like '%" + param.sSearch +
                                "%' or TPIName like '%" + param.sSearch +
                                "%' or NotificationRequired like '%" + param.sSearch +
                                "%' or ThicknessOfJob like '%" + param.sSearch +
                                "%' or SizeDimension like '%" + param.sSearch +
                                "%' or NDERemarks like '%" + param.sSearch +
                                "%' or nde3.CreatedBy +' - '+com003c.t_name like '%" + param.sSearch +
                                "%' or ManufacturingCode like '%" + param.sSearch +
                                "%' or Status like '%" + param.sSearch + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "nde3.BU", "nde3.Location");

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_RTCTQ_GETHEADER
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.BU),
                               Convert.ToString(uc.Location),
                               Convert.ToString(uc.CTQNo),
                               Convert.ToString(uc.ManufacturingCode),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn !=null?(Convert.ToDateTime(uc.CreatedOn)).ToString("dd/MM/yyyy"):""),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.ApplicableSpecification),
                               Convert.ToString(uc.AcceptanceStandard),
                               (uc.ASMECodeStamp == true?"<label class=\"mt-checkbox mt-checkbox-outline\"><input checked=\"checked\" disabled=\"disabled\" type = \"checkbox\" value=\"\" /><span></span></label>":"<label class=\"mt-checkbox mt-checkbox-outline\"><input disabled=\"disabled\" type = \"checkbox\" value=\"\" /><span></span></label>"),//Convert.ToString(uc.ASMECodeStamp),
                               Convert.ToString(uc.TPIName),
                              Convert.ToString(uc.NotificationRequired == true ? "Yes" : uc.NotificationRequired == false ? "No" : ""),
                               Convert.ToString(uc.ThicknessOfJob),
                               Convert.ToString(uc.SizeDimension),
                               Convert.ToString(uc.NDERemarks),
                               "<center style=\"display:inline-flex;\"><a title='View' href='"+WebsiteURL +"/NDE/ApproveRTCTQ/RTCTQLineDetails?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a><a title='View Timeline' href='javascript:void(0)' onclick=ShowTimeline('/NDE/MaintainRTCTQ/ShowTimeline?HeaderID="+Convert.ToInt32(uc.HeaderId)+"');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a></center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadRTCTQHeaderLineData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();
                string[] arrayCTQStatus = { clsImplementationEnum.RTCTQStatus.DRAFT.GetStringValue(), clsImplementationEnum.RTCTQStatus.Returned.GetStringValue() };

                int ndeHeaderId = Convert.ToInt32(param.CTQHeaderId);
                NDE003 objNDE003 = db.NDE003.FirstOrDefault(x => x.HeaderId == ndeHeaderId);

                string[] columnName = { "Description", "SpecificationNo", "SpecificationWithClauseNo", "SpecificationRequirement", "ToBeDiscussed", "CriticalToQuality", "CTQRequirement", "ReturnRemark" };
                if (param.sSearch == "yes" || param.sSearch == "Yes" || param.sSearch == "YES")
                {
                    param.sSearch = "1";
                }
                if (param.sSearch == "no" || param.sSearch == "NO" || param.sSearch == "No")
                {
                    param.sSearch = "0";
                }
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);

                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_GET_RT_HEADER_LINES(StartIndex, EndIndex, strSortOrder, strWhere).ToList();
                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.Description),
                                Convert.ToString(uc.SpecificationNo),
                                Convert.ToString(uc.SpecificationWithClauseNo),
                                Convert.ToString(uc.ToBeDiscussed==true ? "Yes":uc.ToBeDiscussed==false?"No":""),
                                Convert.ToString(uc.SpecificationRequirement),
                                Convert.ToString(uc.CriticalToQuality==true ? "Yes":uc.CriticalToQuality==false?"No":""),
                                Convert.ToString(uc.CTQRequirement),
                                GetRemarkStyle(objNDE003.Status,uc.Status,uc.ReturnRemark),
                                Convert.ToString(uc.Status),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId),
                                "<center style=\"display:inline;\"><a class='' href='javascript:void(0)' onclick=ShowTimeline('/NDE/MaintainRTCTQ/ShowTimeline?HeaderID="+Convert.ToInt32(uc.HeaderId)+"&LineId="+Convert.ToInt32(uc.LineId)+"');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a></center>",
                               // Convert.ToString(uc.ROW_NO),
                               
                               // Convert.ToString(uc.SpecificationWithClauseNo),
                               // Convert.ToString(uc.SpecificationRequirement),
                               //(uc.CriticalToQuality == true?"<label class=\"mt-checkbox mt-checkbox-outline\"><input checked=\"checked\" disabled=\"disabled\" type = \"checkbox\" value=\"\" /><span></span></label>":"<label class=\"mt-checkbox mt-checkbox-outline\"><input disabled=\"disabled\" type = \"checkbox\" value=\"\" /><span></span></label>"),
                               // Convert.ToString(uc.CTQRequirement),
                               //(uc.ToBeDiscussed == true?"<label class=\"mt-checkbox mt-checkbox-outline\"><input checked=\"checked\" disabled=\"disabled\" type = \"checkbox\" value=\"\" /><span></span></label>":"<label class=\"mt-checkbox mt-checkbox-outline\"><input disabled=\"disabled\" type = \"checkbox\" value=\"\" /><span></span></label>"),
                               // Convert.ToString(uc.QualificationForExam),
                               //(uc.ApprovalRequired == true?"<label class=\"mt-checkbox mt-checkbox-outline\"><input checked=\"checked\" disabled=\"disabled\" type = \"checkbox\" value=\"\" /><span></span></label>":"<label class=\"mt-checkbox mt-checkbox-outline\"><input disabled=\"disabled\" type = \"checkbox\" value=\"\" /><span></span></label>"),
                               // Convert.ToString(uc.ImageQualityIndicator),
                               // Convert.ToString(uc.FilmSelection),
                               // Convert.ToString(uc.IntensifyingScreen),
                               // Convert.ToString(uc.RadiationSource),
                               // Convert.ToString(uc.Density),
                               // Convert.ToString(uc.RadiographyAcceptanceCriteria),
                               // Convert.ToString(uc.SpecialTechniqueforRadiography),
                               // Convert.ToString(uc.FilmIdentification),
                               // Convert.ToString(uc.WeldCoverage),
                               // Convert.ToString(uc.Overlap),
                               // Convert.ToString(uc.SFD),
                               // Convert.ToString(uc.SpotLength),
                               // Convert.ToString(uc.FilmRetentionPeriod),
                               // Convert.ToString(uc.FilmDigitization),
                               // Convert.ToString(uc.ProcedureDemonstration),
                               // Convert.ToString(uc.Others),
                               // Convert.ToString("R"+uc.RevNo),
                               // Convert.ToString("R"+uc.LineRevNo),
                               // Convert.ToString(uc.Status),
                               // GetRemarkStyle(objNDE003.Status,uc.ReturnRemark),
                               // Convert.ToString(uc.ReturnRemark),
                               // Convert.ToString(uc.LineId),
                               // Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string GetRemarkStyle(string status,string linestatus, string returnRemarks)
        {
            if (linestatus != clsImplementationEnum.CommonStatus.Deleted.GetStringValue() && status == clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue())
            {
                return "<input type='text' name='txtReturnRemark' class='form-control input-sm input-small input-inline' value='" + returnRemarks + "' maxlength=\"100\" />";
            }
            else
            {
                return "<input type='text' readonly= 'readonly' name='txtReturnRemark' class='form-control input-sm input-small input-inline' value='" + returnRemarks + "' maxlength=\"100\" />";
            }
        }

        [HttpPost]
        public ActionResult ApproveByApproverSelected(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
            for (int i = 0; i < arrayHeaderIds.Length; i++)
            {
                int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                objResponseMsg = ApproveHeaderByApprover(HeaderId, null, false);
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApproveByApprover(int HeaderId, string returnRemarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (!IsapplicableForAttend(HeaderId))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "This record has been already attended, Please refresh page.";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                objResponseMsg = ApproveHeaderByApprover(HeaderId, returnRemarks, true);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public clsHelper.ResponseMsg ApproveHeaderByApprover(int HeaderId, string returnRemarks, bool IsReturnRemark)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string Status = clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue().ToUpper();

                string deleted = clsImplementationEnum.CommonStatus.Deleted.GetStringValue().ToUpper();
                List<NDE004> lstdeletedLines = db.NDE004.Where(x => x.HeaderId == HeaderId && x.Status.Trim() == deleted.Trim()).ToList();
                if (lstdeletedLines != null && lstdeletedLines.Count > 0)
                {
                    db.NDE004.RemoveRange(lstdeletedLines);
                    db.SaveChanges();
                }

                List<NDE004> lstNDE004 = db.NDE004.Where(x => x.HeaderId == HeaderId).ToList();
                List<NDE004_Log> lstNDE004_Log = new List<NDE004_Log>();
                if (lstNDE004 != null && lstNDE004.Count > 0)
                {
                    foreach (var objData in lstNDE004)
                    {
                        NDE004 objNDE004 = lstNDE004.Where(x => x.LineId == objData.LineId).FirstOrDefault();

                        objNDE004.Status = clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue();
                        objNDE004.ApprovedBy = objClsLoginInfo.UserName;
                        objNDE004.ApprovedOn = DateTime.Now;
                        objNDE004.EditedBy = objClsLoginInfo.UserName;
                        objNDE004.EditedOn = DateTime.Now;

                        #region Line Log

                        NDE004_Log objNDE004_Log = new NDE004_Log();

                        objNDE004_Log.LineId = objNDE004.LineId;
                        objNDE004_Log.HeaderId = objNDE004.HeaderId;
                        objNDE004_Log.Project = objNDE004.Project;
                        objNDE004_Log.BU = objNDE004.BU;
                        objNDE004_Log.Location = objNDE004.Location;
                        objNDE004_Log.RevNo = objNDE004.RevNo;
                        objNDE004_Log.LineRevNo = objNDE004.LineRevNo;
                        objNDE004_Log.Status = objNDE004.Status;
                        objNDE004_Log.Description = objNDE004.Description;
                        objNDE004_Log.SpecificationNo = objNDE004.SpecificationNo;
                        objNDE004_Log.SpecificationWithClauseNo = objNDE004.SpecificationWithClauseNo;
                        objNDE004_Log.ToBeDiscussed = objNDE004.ToBeDiscussed;
                        objNDE004_Log.SpecificationRequirement = objNDE004.SpecificationRequirement;
                        objNDE004_Log.CriticalToQuality = objNDE004.CriticalToQuality;
                        objNDE004_Log.CTQRequirement = objNDE004.CTQRequirement;
                        objNDE004_Log.ReturnRemark = objNDE004.ReturnRemark;
                        objNDE004_Log.CreatedBy = objNDE004.CreatedBy;
                        objNDE004_Log.CreatedOn = objNDE004.CreatedOn;
                        objNDE004_Log.EditedBy = objNDE004.EditedBy;
                        objNDE004_Log.EditedOn = objNDE004.EditedOn;
                        objNDE004_Log.ApprovedBy = objNDE004.ApprovedBy;
                        objNDE004_Log.ApprovedOn = objNDE004.ApprovedOn;
                        objNDE004_Log.ReturnedBy = objNDE004.ReturnedBy;
                        objNDE004_Log.ReturnedOn = objNDE004.ReturnedOn;
                        objNDE004_Log.ReturnRemark = objNDE004.ReturnRemark;
                        objNDE004_Log.ApproverRemark = objNDE004.ApproverRemark;
                        objNDE004_Log.SubmittedBy = objNDE004.SubmittedBy;
                        objNDE004_Log.SubmittedOn = objNDE004.SubmittedOn;
                        lstNDE004_Log.Add(objNDE004_Log);
                        #endregion

                    }


                }

                var lstNDE003_Log = db.NDE003_Log.Where(x => x.HeaderId == HeaderId).ToList();
                NDE003_Log objLog = new NDE003_Log();
                if (objLog != null)
                {
                    objLog = lstNDE003_Log.FirstOrDefault();
                    lstNDE003_Log.ForEach(i => { i.Status = clsImplementationEnum.PlanStatus.Superseded.GetStringValue(); });
                }

                NDE003 objNDE003 = db.NDE003.Where(x => x.HeaderId == HeaderId && x.Status.ToUpper() == Status).FirstOrDefault();
                if (objNDE003 != null)
                {
                    objNDE003.Status = clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue();
                    if (IsReturnRemark)
                    {
                        objNDE003.ReturnRemarks = returnRemarks;
                    }
                    objNDE003.ApprovedBy = objClsLoginInfo.UserName;
                    objNDE003.ApprovedOn = DateTime.Now;
                    objNDE003.EditedBy = objClsLoginInfo.UserName;
                    objNDE003.EditedOn = DateTime.Now;

                    #region Header Log
                    NDE003_Log objNDE003_Log = db.NDE003_Log.Where(x => x.HeaderId == objNDE003.HeaderId).FirstOrDefault();

                    if (objNDE003_Log == null)
                    {
                        objNDE003_Log = db.NDE003_Log.Add(new NDE003_Log
                        {
                            HeaderId = objNDE003.HeaderId,
                            Project = objNDE003.Project,
                            BU = objNDE003.BU,
                            Location = objNDE003.Location,
                            RevNo = objNDE003.RevNo,
                            CTQNo = objNDE003.CTQNo,
                            Status = objNDE003.Status,
                            ManufacturingCode = objNDE003.ManufacturingCode,
                            ApplicableSpecification = objNDE003.ApplicableSpecification,
                            AcceptanceStandard = objNDE003.AcceptanceStandard,
                            ASMECodeStamp = objNDE003.ASMECodeStamp,
                            TPIName = objNDE003.TPIName,
                            NotificationRequired = objNDE003.NotificationRequired,
                            ThicknessOfJob = objNDE003.ThicknessOfJob,
                            SizeDimension = objNDE003.SizeDimension,
                            NDERemarks = objNDE003.NDERemarks,
                            CreatedBy = objNDE003.CreatedBy,
                            CreatedOn = objNDE003.CreatedOn,
                            EditedBy = objNDE003.EditedBy,
                            EditedOn = objNDE003.EditedOn,
                            ApprovedBy = objNDE003.ApprovedBy,
                            ApprovedOn = objNDE003.ApprovedOn,
                            SubmittedBy = objNDE003.SubmittedBy,
                            SubmittedOn = objNDE003.SubmittedOn
                        });
                    }
                    else
                    {
                        objNDE003_Log.Status = clsImplementationEnum.PTMTCTQStatus.Superseded.GetStringValue();
                        objNDE003_Log = db.NDE003_Log.Add(new NDE003_Log
                        {
                            HeaderId = objNDE003.HeaderId,
                            Project = objNDE003.Project,
                            BU = objNDE003.BU,
                            Location = objNDE003.Location,
                            RevNo = objNDE003.RevNo,
                            CTQNo = objNDE003.CTQNo,
                            Status = objNDE003.Status,
                            ManufacturingCode = objNDE003.ManufacturingCode,
                            ApplicableSpecification = objNDE003.ApplicableSpecification,
                            AcceptanceStandard = objNDE003.AcceptanceStandard,
                            ASMECodeStamp = objNDE003.ASMECodeStamp,
                            TPIName = objNDE003.TPIName,
                            NotificationRequired = objNDE003.NotificationRequired,
                            ThicknessOfJob = objNDE003.ThicknessOfJob,
                            SizeDimension = objNDE003.SizeDimension,
                            NDERemarks = objNDE003.NDERemarks,
                            CreatedBy = objNDE003.CreatedBy,
                            CreatedOn = objNDE003.CreatedOn,
                            EditedBy = objNDE003.EditedBy,
                            EditedOn = objNDE003.EditedOn,
                            ApprovedBy = objNDE003.ApprovedBy,
                            ApprovedOn = objNDE003.ApprovedOn,
                            SubmittedBy = objNDE003.SubmittedBy,
                            SubmittedOn = objNDE003.SubmittedOn
                        });
                    }
                    #endregion
                    db.SaveChanges();
                    if (lstNDE004_Log != null && lstNDE004_Log.Count > 0)
                    {
                        lstNDE004_Log.ForEach(x => x.RefId = objNDE003_Log.Id);
                        db.NDE004_Log.AddRange(lstNDE004_Log);
                    }
                    db.SaveChanges();

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE3.GetStringValue(), objNDE003.Project, objNDE003.BU, objNDE003.Location, "CTQ: " + objNDE003.CTQNo + " has been Approved", clsImplementationEnum.NotificationType.Information.GetStringValue());
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details successfully approved.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Details not available approved.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return objResponseMsg;
        }

        [HttpPost]
        public ActionResult UpdateReturnRemarks(int LineID, string changeText)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                NDE004 objNDE004 = db.NDE004.Where(x => x.LineId == LineID).FirstOrDefault();
                if (objNDE004 != null)
                {
                    if (!string.IsNullOrWhiteSpace(changeText))
                    {
                        objNDE004.ReturnRemark = changeText;
                    }
                    else
                    {
                        objNDE004.ReturnRemark = null;
                    }
                    objNDE004.EditedBy = objClsLoginInfo.UserName;
                    objNDE004.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details remarks successfully updated.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Details not available";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReturnLines(int HeaderId, string returnRemarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (!IsapplicableForAttend(HeaderId))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "This record has been already attended, Please refresh page.";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                string Status = clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue().ToUpper();
                //List<NDE004> lstNDE004 = db.NDE004.Where(x => x.HeaderId == HeaderId && x.Status.ToUpper().Trim() == Status.ToUpper().Trim() && x.ReturnRemark != null).ToList();
                List<NDE004> lstNDE004 = db.NDE004.Where(x => x.HeaderId == HeaderId && x.ReturnRemark != null).ToList();

                if (lstNDE004.Count > 0 || !string.IsNullOrWhiteSpace(returnRemarks))
                {
                    foreach (var objData in lstNDE004)
                    {
                        NDE004 objNDE004 = lstNDE004.Where(x => x.LineId == objData.LineId).FirstOrDefault();
                        objNDE004.Status = clsImplementationEnum.CTQStatus.Returned.GetStringValue();
                        objNDE004.ReturnedBy = objClsLoginInfo.UserName;
                        objNDE004.ReturnedOn = DateTime.Now;
                        objNDE004.EditedBy = objClsLoginInfo.UserName;
                        objNDE004.EditedOn = DateTime.Now;
                    }
                    NDE003 objNDE003 = db.NDE003.Where(x => x.HeaderId == HeaderId && x.Status.ToUpper() == Status).FirstOrDefault();
                    objNDE003.Status = clsImplementationEnum.CTQStatus.Returned.GetStringValue();
                    objNDE003.EditedBy = objClsLoginInfo.UserName;
                    objNDE003.EditedOn = DateTime.Now;
                    objNDE003.ReturnRemarks = returnRemarks;
                    db.SaveChanges();

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE3.GetStringValue(), objNDE003.Project, objNDE003.BU, objNDE003.Location, "CTQ: " + objNDE003.CTQNo + " has been Returned", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/NDE/MaintainRTCTQ/AddUpdateRTCTQ?HeaderID=" + objNDE003.HeaderId);
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details successfully returned.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please add return remarks for header/lines.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_RTCTQ_GETHEADER(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      RevNo = li.RevNo,
                                      CTQNo = li.CTQNo,
                                      Status = li.Status,
                                      ApplicableSpecification = li.ApplicableSpecification,
                                      AcceptanceStandard = li.AcceptanceStandard,
                                      ASMECodeStamp = li.ASMECodeStamp,
                                      TPIName = li.TPIName,
                                      NotificationRequired = li.NotificationRequired,
                                      ThicknessOfJob = li.ThicknessOfJob,
                                      SizeDimension = li.SizeDimension,
                                      ManufacturingCode = li.ManufacturingCode,
                                      NDERemarks = li.NDERemarks,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn
                                  }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else
                {
                    var lst = db.SP_GET_RT_HEADER_LINES(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      RevNo = li.RevNo,
                                      LineRevNo = li.LineRevNo,
                                      Status = li.Status,
                                      SpecificationWithClauseNo = li.SpecificationWithClauseNo,
                                      SpecificationRequirement = li.SpecificationRequirement,
                                      CriticalToQuality = li.CriticalToQuality,
                                      CTQRequirement = li.CTQRequirement,
                                      ToBeDiscussed = li.ToBeDiscussed,
                                      QualificationForExam = li.QualificationForExam,
                                      ApprovalRequired = li.ApprovalRequired,
                                      ImageQualityIndicator = li.ImageQualityIndicator,
                                      FilmSelection = li.FilmSelection,
                                      IntensifyingScreen = li.IntensifyingScreen,
                                      RadiationSource = li.RadiationSource,
                                      Density = li.Density,
                                      RadiographyAcceptanceCriteria = li.RadiographyAcceptanceCriteria,
                                      SpecialTechniqueforRadiography = li.SpecialTechniqueforRadiography,
                                      FilmIdentification = li.FilmIdentification,
                                      WeldCoverage = li.WeldCoverage,
                                      Overlap = li.Overlap,
                                      SFD = li.SFD,
                                      SpotLength = li.SpotLength,
                                      FilmRetentionPeriod = li.FilmRetentionPeriod,
                                      FilmDigitization = li.FilmDigitization,
                                      ProcedureDemonstration = li.ProcedureDemonstration,
                                      Others = li.Others,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn,
                                      ReturnedBy = li.ReturnedBy,
                                      ReturnedOn = li.ReturnedOn,
                                      ReturnRemark = li.ReturnRemark,
                                      ApproverRemark = li.ApproverRemark,
                                      Description = li.Description,
                                      SpecificationNo = li.SpecificationNo,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }


                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

    }
}