﻿using IEMQS.Areas.PTS.Models;
using IEMQS.Models;
using IEMQSImplementation;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;


namespace IEMQS.Areas.PTS.Controllers
{
    public class MaintainPTSController : clsBase
    {

        List<PTSModel> listNodes = new List<PTSModel>();
        SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["LNConcerto"].ToString());

        List<ParentChildArray> listParentChildArray = new List<ParentChildArray>();

        List<string> listProceedItem = new List<string>();
        List<string> listUID = new List<string>();
        List<SelectItemList> items;


        // GET: Authentication/MaintainPTS
        public ActionResult Index()
        {

            return View("Filters");
        }

        public JsonResult LoadALlBu(IList<MaintainPTSController> model)
        {
            var data = "";
            //var list = new[] { model };
            List<string> lst = new List<string>();
            try
            {

                string query = "select	distinct case a.CATEGORY when 'RO' then 'RPV' else a.CATEGORY end BU " +
                                "from PROJECT a with(nolock) where a.PROJECT_TYPE = 2 and a.CATEGORY not in ('','SUP1','GMA','SUP','FS','SEC','WES','OB','Forge Shop','IT')";

                connection.Open();
                //var command = new SqlCommand(string.Format(query), connection);
                //SqlDataReader reader = command.ExecuteReader();

                using (var command = new SqlCommand(string.Format(query), connection))
                //using (SqlConnection connection = new SqlConnection(
                //connectionString.ToString()))
                {
                    //SqlCommand command = new SqlCommand(
                    //    query, connection);
                    //connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    try
                    {

                        while (reader.Read())
                        {
                            lst.Add(reader["BU"].ToString());
                        }

                    }
                    finally
                    {
                        // Always call Close when done reading.
                        reader.Close();
                        connection.Close();
                    }
                    //var lstResult = db.SP_CODINGSTANDARD_GETUSERDETAIL(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();


                    return Json(new
                    {
                        lst,
                    }, JsonRequestBehavior.AllowGet);
                }
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    //sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult LoadCustomerList(string BuName)
        {


            List<string> lst = new List<string>();
            try
            {

                string query = "select	distinct a.SUBJECT Customer  from	PROJECT a with(nolock)" +
                                "where a.SUBJECT is not null and	PROJECT_TYPE = 2 and case a.CATEGORY when 'RO' then 'RPV' else a.CATEGORY end in ('" + BuName + "')";

                connection.Open();

                using (var command = new SqlCommand(string.Format(query), connection))

                {
                    SqlDataReader reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            lst.Add(reader["Customer"].ToString());
                        }
                    }
                    finally
                    {
                        reader.Close();
                        connection.Close();
                    }

                    return Json(new
                    {
                        lst,
                    }, JsonRequestBehavior.AllowGet);
                }
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    //sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        //public JsonResult LoadCustomerProjectList(JQueryDataTableParamModel param, string BuName, string CustomerName)
        //{
        //    int StartIndex = 0;
        //    int EndIndex = 0;
        //    int datacount = 0;
        //    StartIndex = param.iDisplayStart;
        //    EndIndex = param.iDisplayStart + param.iDisplayLength;
        //    string strWhere = string.Empty;

        //    if (!string.IsNullOrWhiteSpace(param.sSearch))
        //    {
        //        strWhere += "and (a.PROJECTNAME like '%" + param.sSearch
        //             + "%' or TITLE like '%" + param.sSearch
        //             + "%' or CATEGORY like '%" + param.sSearch
        //             + "%' or SUBJECT like '%" + param.sSearch
        //             + "%' or ATTRIBUTE1 like '%" + param.sSearch
        //             + "%' or FINISHDATE like '%" + param.sSearch
        //             + "%' or PROJECTED_COMPLETION like '%" + param.sSearch + "%')";
        //    }

        //    List<string[]> lst = new List<string[]>();
        //    try
        //    {
        //        CustomerName = CustomerName.Trim() != "" ? CustomerName.Trim() : null;
        //        string query = "";
        //        if (strWhere == "")
        //        {
        //            query = "select	a.PROJECTID, a.PROJECTNAME ProjectCode, TITLE ProjectDescription, CATEGORY BU, SUBJECT Customer, ATTRIBUTE1 MainExecutionCentre, FINISHDATE CDD, PROJECTED_COMPLETION PDD" +
        //                ",COUNT(*) OVER () as TotalCount from PROJECT a with(nolock) where a.PROJECT_TYPE = 2 and a.CATEGORY in ('" + BuName.Trim() + "')  and a.SUBJECT in ('" + CustomerName + "') order by a.PROJECTNAME OFFSET " + StartIndex + " ROWS FETCH NEXT " + EndIndex + " ROWS ONLY";

        //        }
        //        else
        //        {
        //            query = "select	a.PROJECTID, a.PROJECTNAME ProjectCode, TITLE ProjectDescription, CATEGORY BU, SUBJECT Customer, ATTRIBUTE1 MainExecutionCentre, FINISHDATE CDD, PROJECTED_COMPLETION PDD" +
        //                ",COUNT(*) OVER () as TotalCount from PROJECT a with(nolock) where a.PROJECT_TYPE = 2 and a.CATEGORY in ('" + BuName.Trim() + "')  and a.SUBJECT in ('" + CustomerName + "') " + strWhere +
        //                "  order by a.PROJECTNAME OFFSET " + StartIndex + " ROWS FETCH NEXT " + EndIndex + " ROWS ONLY";
        //            //"from	PROJECT a with(nolock) where	a.PROJECT_TYPE = 2 and a.CATEGORY = 'RPV' order by a.PROJECTNAME a.CATEGORY end in ('" + BuName + "')";
        //        }
        //        connection.Open();

        //        using (var command = new SqlCommand(string.Format(query), connection))

        //        {


        //            SqlDataReader reader = command.ExecuteReader();
        //            try
        //            {
        //                while (reader.Read())
        //                {
        //                    var newRecord = new string[]
        //                    {
        //                       reader["PROJECTID"].ToString(),
        //                        reader["ProjectCode"].ToString(),
        //                       reader["ProjectDescription"].ToString(),
        //                         reader["BU"].ToString(),
        //                         reader["Customer"].ToString(),
        //                        reader["MainExecutionCentre"].ToString(),
        //                       Convert.ToDateTime(reader["CDD"]).ToString("dd/MM/yyyy"),
        //                         Convert.ToDateTime(reader["PDD"]).ToString("dd/MM/yyyy")


        //                };
        //                    datacount = Convert.ToInt32(reader["TotalCount"]);
        //                    lst.Add(newRecord);
        //                }
        //            }
        //            finally
        //            {
        //                reader.Close();
        //                connection.Close();
        //            }

        //            return Json(new
        //            {
        //                //sEcho = Convert.ToInt32(param.sEcho),
        //                iTotalRecords = datacount,
        //                iTotalDisplayRecords = datacount,
        //                aaData = lst,
        //            }, JsonRequestBehavior.AllowGet);
        //        }
        //    }

        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        return Json(new
        //        {
        //            //sEcho = param.sEcho,
        //            iTotalRecords = "0",
        //            iTotalDisplayRecords = "0",
        //            aaData = ""
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //}

        public JsonResult LoadCustomerProjectList(JQueryDataTableParamModel param, string BuName, string CustomerName)
        {
            int StartIndex = 0;
            int EndIndex = 0;
            int datacount = 0;
            StartIndex = param.iDisplayStart + 1;
            EndIndex = param.iDisplayStart + param.iDisplayLength;
            string strWhere = string.Empty;

            if (!string.IsNullOrWhiteSpace(param.sSearch))
            {
                strWhere += "and (a.PROJECTNAME like '%" + param.sSearch
                         + "%' or TITLE like '%" + param.sSearch
                         + "%' or CATEGORY like '%" + param.sSearch
                         + "%' or SUBJECT like '%" + param.sSearch
                         + "%' or ATTRIBUTE1 like '%" + param.sSearch
                         + "%' or FINISHDATE like '%" + param.sSearch
                         + "%' or PROJECTED_COMPLETION like '%" + param.sSearch + "%')";
            }

            var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
            var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
            string strSortOrder = string.Empty;
            string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
            string sortDirection = Convert.ToString(Request["sSortDir_0"]);

            if (!string.IsNullOrWhiteSpace(sortColumnName))
            {
                strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
            }
            else
            {
                strSortOrder = " Order By ProjectCode ";
            }


            List<string[]> lst = new List<string[]>();
            try
            {
                CustomerName = CustomerName.Trim() != "" ? CustomerName.Trim() : null;
                string query = "";
                if (strWhere == "")
                {
                    query = "select * from ( SELECT ROW_NUMBER() OVER ( "+ strSortOrder + " ) AS ROW_NO, COUNT(1) OVER() AS TotalCount, PROJECTID, ProjectCode, ProjectDescription, BU, Customer, MainExecutionCentre, CDD, PDD, CC, BC FROM (" +
                            "select	a.PROJECTID, a.PROJECTNAME ProjectCode, TITLE ProjectDescription, CATEGORY BU, SUBJECT Customer, ATTRIBUTE1 MainExecutionCentre, FINISHDATE CDD, PROJECTED_COMPLETION PDD" +
                            ", (case c.chainduration when 0 then 'NA' else convert(varchar(max),(c.chainduration - c.chainremainingduration)/(c.chainduration)*100) end) CC, c.percentpenetration BC , COUNT(*) OVER () as TotalCount " +
                            " from	PROJECT a with(nolock) join RES_USERS b witH(nolock) on a.MANAGER_ID = b.USER_NAME left join (select * from S2M_BUFFERHISTORY c0 with(nolock) where c0.task_type = 1 and c0.HISTORY_TIMESTAMP = (select max(c1.HISTORY_TIMESTAMP) from S2M_BUFFERHISTORY c1 where c1.PROJECTNAME = c0.PROJECTNAME and c1.TASK_TYPE = 1)) c on a.PROJECTNAME = c.PROJECTNAME" +
                            " where a.PROJECT_TYPE = 2 and a.CATEGORY in ('" + BuName.Trim() + "')  and a.SUBJECT in (" + CustomerName + "))tm " +
                            ") f WHERE f.ROW_NO BETWEEN " + StartIndex + " AND " + EndIndex + "";
                }
                else
                {
                    query = "select * from ( SELECT ROW_NUMBER() OVER ( " + strSortOrder + " ) AS ROW_NO, COUNT(1) OVER() AS TotalCount, PROJECTID, ProjectCode, ProjectDescription, BU, Customer, MainExecutionCentre, CDD, PDD, CC, BC FROM (" +
                            "select a.PROJECTID, a.PROJECTNAME ProjectCode, TITLE ProjectDescription, CATEGORY BU, SUBJECT Customer, ATTRIBUTE1 MainExecutionCentre, FINISHDATE CDD, PROJECTED_COMPLETION PDD" +
                            ", (case c.chainduration when 0 then 'NA' else convert(varchar(max),(c.chainduration - c.chainremainingduration)/(c.chainduration)*100) end) CC, c.percentpenetration BC " +
                            " from	PROJECT a with(nolock) join RES_USERS b witH(nolock) on a.MANAGER_ID = b.USER_NAME left join (select * from S2M_BUFFERHISTORY c0 with(nolock) where c0.task_type = 1 and c0.HISTORY_TIMESTAMP = (select max(c1.HISTORY_TIMESTAMP) from S2M_BUFFERHISTORY c1 where c1.PROJECTNAME = c0.PROJECTNAME and c1.TASK_TYPE = 1)) c on a.PROJECTNAME = c.PROJECTNAME " +
                            " where a.PROJECT_TYPE = 2 and a.CATEGORY in ('" + BuName.Trim() + "')  and a.SUBJECT in (" + CustomerName + ") " + strWhere + ") tm" +
                            ") f WHERE f.ROW_NO BETWEEN " + StartIndex + " AND " + EndIndex + "";
                }
                connection.Open();

                using (var command = new SqlCommand(string.Format(query), connection))
                {
                    SqlDataReader reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            var newRecord = new string[]
                            {
                               reader["PROJECTID"].ToString(),
                               reader["ProjectCode"].ToString(),
                               //reader["ProjectDescription"].ToString(),
                               (!string.IsNullOrEmpty(Convert.ToString(reader["ProjectDescription"]))) ? (reader["ProjectDescription"].ToString().Length > 15 ? GenerateSpan(reader["ProjectDescription"].ToString().Substring(0,15) + "...",reader["ProjectDescription"].ToString(),"") : reader["ProjectDescription"].ToString()):"",
                               reader["BU"].ToString(),
                               //reader["Customer"].ToString(),
                               (!string.IsNullOrEmpty(Convert.ToString(reader["Customer"]))) ? (reader["Customer"].ToString().Length > 15 ? GenerateSpan(reader["Customer"].ToString().Substring(0,15) + "...",reader["Customer"].ToString(),"") : reader["Customer"].ToString()):"",
                               //reader["MainExecutionCentre"].ToString(),
                               (!string.IsNullOrEmpty(Convert.ToString(reader["MainExecutionCentre"]))) ? (reader["MainExecutionCentre"].ToString().Length > 8 ? GenerateSpan(reader["MainExecutionCentre"].ToString().Substring(0,8) + "...",reader["MainExecutionCentre"].ToString(),"") : reader["MainExecutionCentre"].ToString()):"",
                               Convert.ToDateTime(reader["CDD"]).ToString("dd/MM/yyyy"),
                               Convert.ToDateTime(reader["PDD"]).ToString("dd/MM/yyyy"),
                               Convert.ToDecimal(reader["CC"].ToString()).ToString("0.00"),
                               Convert.ToDecimal(reader["BC"].ToString()).ToString("0.00"),
                            };
                            datacount = Convert.ToInt32(reader["TotalCount"]);
                            lst.Add(newRecord);
                        }
                    }
                    finally
                    {
                        reader.Close();
                        connection.Close();
                    }
                    return Json(new
                    {
                        //sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = datacount,
                        iTotalDisplayRecords = datacount,
                        aaData = lst,
                    }, JsonRequestBehavior.AllowGet);
                }
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    //sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult LoadUIDList(JQueryDataTableParamModel param, string ProjectId, string BuName, string CheckedCustomerList)
        {
            var objPTSModelList = new PTSModelList();
            int StartIndex = 0;
            int EndIndex = 0;
            //int datacount = 0;
            StartIndex = (param.iDisplayStart == 0 ? -1 : param.iDisplayStart) + 1;
            EndIndex = param.iDisplayStart + param.iDisplayLength;
            string strWhere = string.Empty;

            if (!string.IsNullOrWhiteSpace(param.sSearch))
            {
                strWhere += "and (GroupName like '%" + param.sSearch
                     + "%' or Uids like '%" + param.sSearch + "%')";
            }



            string query1 = "Select TASKUNIQUEID as UID, CONTACT, NAME as TaskDescription,COUNT(*) OVER () as TotalCount from PROJ_TASK  where PROJECTID = '" + ProjectId.Trim() + "'  and SUMMARY <> 1 and SPECIALTASKTYPE <> 2 and TASK_TYPE <> 3 ";

            connection.Open();
            List<SelectItemList> list = new List<SelectItemList>();

            using (var command = new SqlCommand(query1, connection))
            {
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        list.Add(new SelectItemList { id = reader["UID"].ToString(), text = reader["UID"].ToString() + " - " + reader["TaskDescription"].ToString() });
                    }
                }
                finally
                {
                    reader.Close();
                    connection.Close();
                }

            }

            List<ProcessGroup> lst = new List<ProcessGroup>();
            try
            {
                //string query = "Select TASKUNIQUEID as UID, CONTACT ,NAME as TaskDescription,COUNT(*) OVER () as TotalCount from PROJ_TASK  where PROJECTID = '" + ProjectId.Trim() + "'  order by TASKUNIQUEID OFFSET " + StartIndex + " ROWS FETCH NEXT " + EndIndex + " ROWS ONLY";
                string query = "Select *,COUNT(*) OVER () as TotalCount from PTS001 where Project = '" + ProjectId.Trim() + "'" + strWhere + " order by GroupId OFFSET " + StartIndex + " ROWS FETCH NEXT " + EndIndex + " ROWS ONLY";
                lst = db.Database.SqlQuery<ProcessGroup>(query).ToList();


                int newRecordId = 0;
                var newRecord = new[] {
                                           Helper.GenerateHTMLTextbox(newRecordId,"GroupId","","",true,"display:none",true),
                                        Helper.GenerateTextbox(newRecordId, "GroupName",  "", "", false, "", "100",""),
                                        Helper.GenerateActionCheckbox(newRecordId,"IsGroup",false,"",false,""),
                                         Helper.MultiSelectDropdown(list,newRecordId,"Uids",false,"","","Uids"),

                                        //Helper.GenerateNumericTextbox(newRecordId, "Uids",  "", "", false, "", "20"),                                                                         
                                        Helper.GenerateGridButtonNew(newRecordId, "Save", "Save Rocord", "fa fa-save",
                                        " SaveNewRecord('"+Convert.ToString(ProjectId)+"','"+BuName+"','"+CheckedCustomerList+"');"),


                                    };

                var data = (from uc in lst
                            select new[]
                                       {
                                           uc.GroupId.ToString(),

                                            "<span class=' ' data-col='description' data-maxlength='100' >" +Convert.ToString(uc.GroupName)+ "</span>",

                                            "<span class=' ' data-col='description' data-maxlength='100' >" +(uc.IsGroup==true?"Yes":"No")+ "</span>",

                                            "<span class=' ' data-col='description' data-maxlength='100' >" +Convert.ToString(uc.Uids)+ "</span>",
                                                 "<span class=' ' data-col='description' data-maxlength='100' ></span>",
                             //                 Helper.GenerateActionIcon(uc.GroupId, "Edit", "Edit Record", "editTableRow fa fa-edit", "","",false) + "|" +
                             //Helper.GenerateActionIcon(uc.GroupId, "Delete", "Delete Record", "fa fa-trash-o", "DeleteData("+uc.GroupId+")","",false)
                                        }).ToList();

                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lst.Count > 0 && lst.FirstOrDefault().TotalCount > 0 ? lst.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lst.Count > 0 && lst.FirstOrDefault().TotalCount > 0 ? lst.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    //sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult LoadUIDListSingleLine(string ProjectId, int Groupid)
        {
            var objPTSModelList = new PTSModelList();

            string query1 = "Select TASKUNIQUEID as UID, CONTACT, NAME as TaskDescription,COUNT(*) OVER () as TotalCount from PROJ_TASK  where PROJECTID = '" + ProjectId.Trim() + "'  and SUMMARY <> 1 and SPECIALTASKTYPE <> 2 and TASK_TYPE <> 3 ";
            //string query1 = "Select TASKUNIQUEID as UID, CONTACT, NAME as TaskDescription,COUNT(*) OVER () as TotalCount from PROJ_TASK  where PROJECTID = '" + ProjectId.Trim() + "'";

            connection.Open();
            List<SelectItemList> list = new List<SelectItemList>();

            using (var command = new SqlCommand(query1, connection))
            {
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        list.Add(new SelectItemList { id = reader["UID"].ToString(), text = reader["UID"].ToString() + " - " + reader["TaskDescription"].ToString() });
                    }
                }
                finally
                {
                    reader.Close();
                    connection.Close();
                }

            }

            List<ProcessGroup> lst = new List<ProcessGroup>();
            try
            {
                string query = "Select *,COUNT(*) OVER () as TotalCount from PTS001 where Project = '" + ProjectId.Trim() + "' and GroupId=" + Groupid;
                //string query = "Select *,COUNT(*) OVER () as TotalCount from PTS001 where Project = '" + ProjectId.Trim() + "' and GroupId=" + Groupid + "  order by GroupId OFFSET " + StartIndex + " ROWS FETCH NEXT " + EndIndex + " ROWS ONLY";
                lst = db.Database.SqlQuery<ProcessGroup>(query).ToList();

                var data = (from uc in lst
                            select new[]
                                       {
                                  uc.GroupId.ToString(),
                                  Helper.GenerateTextbox(uc.GroupId, "GroupName",    Convert.ToString(uc.GroupName),"UpdateData(this, "+ uc.GroupId+",true);", false ,"","50","editable"),
                                  Helper.GenerateActionCheckbox(uc.GroupId,"IsGroup",uc.IsGroup,"UpdateData(this, "+ uc.GroupId+",true);",false ,"editable"),
                                  Helper.MultiSelectDropdown(list,uc.GroupId,uc.Uids, false ,"UpdateData(this,'"+ uc.GroupId  +"',true);","","Uids","editable"),
                                     "<span class=' ' data-col='description' data-maxlength='100' ></span>",
                            //              Helper.GenerateActionIcon(uc.GroupId, "Edit", "Edit Record", "editTableRow fa fa-edit", "", "", false) + "|" +
                            //Helper.GenerateActionIcon(uc.GroupId, "Delete", "Delete Record", "fa fa-trash-o", "DeleteData(" + uc.GroupId + ")", "", false),

                              
                                         
                                       
                                         //uc.GroupId.ToString(),

                             //               "<span class=' ' data-col='description' data-maxlength='100' >" +Convert.ToString(uc.GroupName)+ "</span>",

                             //               "<span class=' ' data-col='description' data-maxlength='100' >" +Convert.ToString(uc.IsGroup)+ "</span>",

                             //               "<span class=' ' data-col='description' data-maxlength='100' >" +Convert.ToString(uc.Uids)+ "</span>",
                             //                 Helper.GenerateActionIcon(uc.GroupId, "Edit", "Edit Record", "editTableRow fa fa-edit", "","",false) + "|" +
                             //Helper.GenerateActionIcon(uc.GroupId, "Delete", "Delete Record", "fa fa-trash-o", "DeleteData("+uc.GroupId+")","",false)
                                        }).ToList();


                return Json(new
                {
                    //sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = 0,
                    iTotalDisplayRecords = 0,
                    aaData = data,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    //sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }



        public JsonResult LoadUIDList2(JQueryDataTableParamModel param, string ProjectId)
        {
            var objPTSModelList = new PTSModelList();
            int StartIndex = 0;
            int EndIndex = 0;
            int datacount = 0;
            StartIndex = param.iDisplayStart + 1;
            EndIndex = param.iDisplayStart + param.iDisplayLength;

            try
            {
                string query = "Select TASKUNIQUEID as UID, CONTACT, NAME as TaskDescription,COUNT(*) OVER () as TotalCount from PROJ_TASK  where PROJECTID = '" + ProjectId.Trim() + "'";

                connection.Open();
                List<PTSModel> list = new List<PTSModel>();

                using (var command = new SqlCommand(query, connection))
                {
                    SqlDataReader reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            var newRecord = new PTSModel()
                            {
                                UID = reader["UID"].ToString(),
                                TaskDescription = reader["TaskDescription"].ToString(),
                                CONTACT = reader["CONTACT"].ToString()
                            };
                            datacount = Convert.ToInt32(reader["TotalCount"]);
                            list.Add(newRecord);
                        }
                    }
                    finally
                    {
                        reader.Close();
                        connection.Close();
                    }
                    return Json(list, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public PartialViewResult LoadProjectTaskList(JQueryDataTableParamModel param, string ProjectName, string height, string width)
        {
            var objPTSModelList = new PTSModelList();
            int StartIndex = 0;
            int EndIndex = 0;
            //int datacount = 0;
            StartIndex = param.iDisplayStart;
            EndIndex = param.iDisplayStart + param.iDisplayLength;
            var str = "";
            string strWhere = string.Empty;




            ViewBag.WidowsHeight = height;
            ViewBag.WidowsWidth = width;
            ViewBag.SvgWidth = 0;
            try
            {
                string query = "with RCCPHours as ( SELECT PROJECTID, TASKUNIQUEID, SUM(CONVERT(decimal(20, 2), RCCP)) AS Hours FROM(       SELECT PROJECTID, TASKUNIQUEID, /*dbo.ufnGetRCCPValue(TEXT1)*/ 0 AS RCCP, TEXT1       FROM   dbo.S2M_TASKCHECKLIST AS S2M_TASKCHECKLIST_2 with(nolock)       WHERE(LTRIM(RTRIM(UPPER(CLITEMTEXT))) NOT IN('TOTAL', 'TOT', 'TOT.', 'TTL', 'TTL.'))       and    charindex('TOT', upper(ltrim(rtrim(CLITEMTEXT)))) != 1       ) AS a GROUP BY PROJECTID, TASKUNIQUEID ) " +

"select a.PROJECTID, a.PROJECTNAME ProjectCode, a.TASKUNIQUEID UID, a.NAME TaskDescription, a.ACTUALFINISH ActualFinish, BASELINEFINISH PlannedCompletion, a.EXPECTED_END_DATE ExpectedFinish, case a.TASK_STATUS when 0 then 'NS' when 1 then 'IP' when 2 then 'CO' end TaskStatus " +
 " , case when a.TASK_STATUS <> 2 then a.REMAININGDURATION / 4800 else 0 end CurrRemDur, b.USER_FULL_NAME Shop, d.IPDate, isnull(e.PlannedDuration, a.REMAININGDURATION / 4800) PlannedDuration     , f.Predecessors,a.TASKUNIQUEID UID, g.Successors, isnull(h.Hours, 0) RCCP, i.RTSDate " +
     " , CASE a.TASK_STATUS when 0 then 0.00 when 1      then datediff(d, isnull(convert(date, i.RTSDate), convert(date, d.IPDate)), GETDATE())-datediff(WK, isnull(convert(date, i.RTSDate), convert(date, d.IPDate)), GETDATE()) + a.REMAININGDURATION / 4800  when 2      then datediff(d, isnull(convert(date, i.RTSDate), convert(date, d.IPDate)), a.ACTUALFINISH + 1)-datediff(WK, isnull(convert(date, i.RTSDate), convert(date, d.IPDate)), a.ACTUALFINISH + 1)       end[ActualOrProjectedDuration] , a.CONTACT " +
     ", case isnull(j.TASKUNIQUEID,'') when '' then 'No' else 'Yes' end IsCritical,a.Text_27,a.TASK_TYPE " +

" from PROJ_TASK a with(nolock) left join " +

        " RES_USERS b with(Nolock) on a.CONTACT = b.USER_NAME outer apply " +

        "(select MAX(d0.MODIFIED_TIMESTAMP) IPDate from VR_ALL_TU_HISTORY d0 with(nolock) where a.PROJECTID = d0.PROJECTID and a.TASKUNIQUEID = d0.TASKUNIQUEID and d0.FIELDID = 7) d outer apply " +
        "(select top 1 LONGVAL / 4800 PlannedDuration from VR_ALL_TU_HISTORY e0 with(nolock) where a.PROJECTID = e0.PROJECTID and a.TASKUNIQUEID = e0.TASKUNIQUEID and e0.FIELDID = 9 and e0.MODIFIED_TIMESTAMP < d.IPDate order by e0.MODIFIED_TIMESTAMP desc) e outer apply " +
            "(select(select convert(varchar(max), f0.PredecessorTaskUniqueID) + ',' from Task_Dependencies f0 with(nolock) where f0.ProjectID = a.PROJECTID and f0.SuccessorTaskUniqueID = a.TASKUNIQUEID for xml path('')) Predecessors) f outer apply " +
            "(select(select convert(varchar(max), g0.SuccessorTaskUniqueID) + ',' from Task_Dependencies g0 with(nolock) where g0.ProjectID = a.PROJECTID and g0.PredecessorTaskUniqueID = a.TASKUNIQUEID for xml path('')) Successors) g left join " +
            "RCCPHours h with(nolock)on a.PROJECTID = h.PROJECTID and a.TASKUNIQUEID = h.TASKUNIQUEID outer apply(select MAX(i0.MODIFIED_TIMESTAMP) RTSDate from VR_ALL_TU_HISTORY i0 with(nolock) where a.PROJECTID = i0.PROJECTID and a.TASKUNIQUEID = i0.TASKUNIQUEID and i0.FIELDID = 25 and i0.LONGVAL <> 0 and convert(date, i0.MODIFIED_TIMESTAMP) <= (case a.TASK_STATUS when 2 then convert(date, a.ACTUALFINISH) else GETDATE() end)) i left join " +
            "VI_PEN_CHAIN_TASK j on a.PROJECTID = j.PROJECTID and a.TASKUNIQUEID = j.TASKUNIQUEID " +

"where   SUMMARY <> 1 " +

                  "/*and a.SPECIALTASKTYPE <> 2  and a.TASK_TYPE <> 3 */   and a.PROJECTNAME = '" + ProjectName.Trim() + "'  order by a.TASKID DESC";
                //"/*and a.SPECIALTASKTYPE <> 2  and a.TASK_TYPE <> 3  */  and a.PROJECTID = 19953  order by a.TASKID DESC";


                connection.Open();
                List<PTSModel> list = new List<PTSModel>();
                using (var command = new SqlCommand(string.Format(query), connection))

                {


                    SqlDataReader reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            var newRecord = new PTSModel
                            {
                                PROJECTID = reader["PROJECTID"].ToString(),
                                ProjectCode = reader["ProjectCode"].ToString(),
                                TaskDescription = reader["TaskDescription"].ToString(),
                                ActualFinish = reader["ActualFinish"].ToString() != string.Empty ? Convert.ToDateTime(reader["ActualFinish"]).ToString("dd-MM-yyyy") : string.Empty,
                                PlannedCompletion = reader["PlannedCompletion"].ToString(),
                                ExpectedFinish = reader["ExpectedFinish"].ToString(),
                                TaskStatus = reader["TaskStatus"].ToString(),
                                TaskType = Convert.ToInt16(reader["TASK_TYPE"]),
                                CurrRemDur = reader["CurrRemDur"].ToString(),
                                Shop = reader["Shop"].ToString(),
                                IPDate = reader["IPDate"].ToString(),
                                PlannedDuration = reader["PlannedDuration"].ToString(),
                                Predecessors = reader["Predecessors"].ToString(),
                                UID = reader["UID"].ToString(),
                                Successors = reader["Successors"].ToString(),
                                RCCP = reader["RCCP"].ToString(),
                                RTSDate = reader["RTSDate"].ToString(),
                                ActualOrProjectedDuration = reader["ActualOrProjectedDuration"].ToString(),
                                CONTACT = reader["CONTACT"].ToString(),
                                isCritical = reader["IsCritical"].ToString(),
                                Camera = reader["Text_27"] != null ? reader["Text_27"].ToString() : "",

                            };
                            list.Add(newRecord);
                        }

                        for (int i = 0; i < list.Count; i++)
                        {

                            str = list[i].PROJECTID + list[i].ProjectCode;
                        }

                    }
                    finally
                    {
                        reader.Close();
                        connection.Close();
                    }

                    List<PTS001> objListPTS001 = new List<PTS001>();/*.Where(x => x.Project == list[0].PROJECTID).ToList();   */
                    var pid = list[0].PROJECTID;
                    objListPTS001 = db.PTS001.Where(x => x.Project == pid && x.IsGroup == true).ToList();

                    List<string> UidList = new List<string>();
                    //if(objPTS001!=null && objPTS001.Uids != null)
                    //{
                    //    UidList = objPTS001.Uids.Split(',').ToList();

                    //}
                    var SecondList = list.ToList();

                    var newList = list.OrderBy(x => x.TaskStatus).ToList();

                    var objInitNode = newList.Where(x => x.Predecessors == "").ToList();
                    foreach (var item in objInitNode)
                    {
                        var Predecessors = item.Predecessors.Split(',').Where(i => i != "").ToList();
                        var Successors = item.Successors.Split(',').Where(i => i != "").ToList();

                        foreach (var parent in Predecessors)
                        {
                            var parentdtl = newList.Where(i => i.UID == parent).FirstOrDefault();
                            NodeDetails parentnodedtl = new NodeDetails();
                            parentnodedtl.id = parentnodedtl.UID;
                            parentnodedtl.UID = parentnodedtl.UID;
                            parentnodedtl.TaskDescription = parentnodedtl.TaskDescription;
                            parentnodedtl.ActualFinish = parentnodedtl.ActualFinish;
                            parentnodedtl.PlannedCompletion = parentnodedtl.PlannedCompletion;
                            parentnodedtl.TaskStatus = parentnodedtl.TaskStatus;
                            parentnodedtl.TaskType = parentnodedtl.TaskType;
                            parentnodedtl.Shop = parentnodedtl.Shop;
                            parentnodedtl.RCCP = parentnodedtl.RCCP;
                            parentnodedtl.isCritical = parentnodedtl.isCritical;

                            NodeDetails childnodedtl = new NodeDetails();
                            childnodedtl.id = item.UID;
                            childnodedtl.UID = item.UID;
                            childnodedtl.TaskDescription = item.TaskDescription;
                            childnodedtl.ActualFinish = item.ActualFinish;
                            childnodedtl.PlannedCompletion = item.PlannedCompletion;
                            childnodedtl.TaskStatus = item.TaskStatus;
                            childnodedtl.TaskType = item.TaskType;
                            childnodedtl.Shop = item.Shop;
                            childnodedtl.RCCP = item.RCCP;
                            childnodedtl.isCritical = item.isCritical;

                            listParentChildArray.Add(new ParentChildArray { child = childnodedtl, parent = parentnodedtl });
                            //listParentChildArray.Add(new ParentChildArray { ParentUID = parent, ChildUID = item.UID, IsProceed = false });
                        }

                        foreach (var parent in Successors)
                        {
                            var childdtl = newList.Where(i => i.UID == parent).FirstOrDefault();
                            NodeDetails parentnodedtl = new NodeDetails();
                            parentnodedtl.id = item.UID;
                            parentnodedtl.UID = item.UID;
                            parentnodedtl.TaskDescription = item.TaskDescription;
                            parentnodedtl.ActualFinish = item.ActualFinish;
                            parentnodedtl.PlannedCompletion = item.PlannedCompletion;
                            parentnodedtl.TaskStatus = item.TaskStatus;
                            parentnodedtl.TaskType = item.TaskType;
                            parentnodedtl.Shop = item.Shop;
                            parentnodedtl.RCCP = item.RCCP;
                            parentnodedtl.isCritical = item.isCritical;

                            NodeDetails childnodedtl = new NodeDetails();
                            childnodedtl.id = childdtl.UID;
                            childnodedtl.UID = childdtl.UID;
                            childnodedtl.TaskDescription = childdtl.TaskDescription;
                            childnodedtl.ActualFinish = childdtl.ActualFinish;
                            childnodedtl.PlannedCompletion = childdtl.PlannedCompletion;
                            childnodedtl.TaskStatus = childdtl.TaskStatus;
                            childnodedtl.TaskType = childdtl.TaskType;
                            childnodedtl.Shop = childdtl.Shop;
                            childnodedtl.RCCP = childdtl.RCCP;
                            childnodedtl.isCritical = childdtl.isCritical;

                            listParentChildArray.Add(new ParentChildArray { child = childnodedtl, parent = parentnodedtl });

                            //listParentChildArray.Add(new ParentChildArray { ParentUID = item.UID, ChildUID = parent, IsProceed = false });
                        }

                        GetChildArray(newList, item);
                    }


                    ////List<dynamic>[,] listfinal = new List<dynamic>[listParentChildArray.Count(), 2];

                    //var listfinal = new List<Tuple<string, string>>();

                    //string[,] listfinal = new string[listParentChildArray.Count(), 2];
                    int cnt = 0;
                    clsDataFlow objclsDataFlow = new clsDataFlow();
                    objclsDataFlow.linkDataArray = new List<linkDataArray>();
                    objclsDataFlow.nodeDataArray = new List<nodeDataArray>();
                    foreach (var item in listParentChildArray)
                    {
                        linkDataArray objlink = new linkDataArray();
                        if (item.parent.isCritical == "Yes")
                        {
                            objlink.isCritical = "Yes";
                        }
                        objlink.from = item.parent.UID;
                        objlink.frompid = "OUT";
                        objlink.to = item.child.UID;
                        objlink.topid = "IN";
                        objclsDataFlow.linkDataArray.Add(objlink);
                    }

                    foreach (var itm in objListPTS001.Where(x => x.IsGroup = true))
                    {
                        objclsDataFlow.nodeDataArray.Add(new nodeDataArray
                        {
                            key = (-itm.GroupId).ToString(),
                            name = (-itm.GroupId).ToString(),
                            type = "Contractor",
                            isGroup = itm.IsGroup,
                        });
                    }

                    // For set root node 
                    foreach (var InitNode in objInitNode)
                    {
                        foreach (var AllNode in newList)
                        {
                            if (AllNode.UID == InitNode.UID)
                            {
                                AllNode.isRoot = true;
                            }
                        }
                    }

                    foreach (var item in newList)
                    {
                        nodeDataArray objData = new nodeDataArray();
                        foreach (var ud in objListPTS001)
                        {
                            item.group = ((item.group == null || item.group == "") && ud.Uids.Contains(item.UID)) ? (-ud.GroupId).ToString() : "";
                            if (item.group != null && item.group != "")
                            {
                                objData.isGroup = item.isGroup;
                                objData.group = item.group;
                                break;
                            }
                        }

                        objData.key = item.UID;
                        objData.name = item.UID;
                        objData.type = "Contractor";
                        objData.PlannedCompletion = item.PlannedCompletion;
                        objData.RCCP = item.RCCP;
                        objData.Shop = item.Shop;
                        objData.TaskDescription = item.TaskDescription;
                        objData.TaskStatus = item.TaskStatus;
                        objData.TaskType = item.TaskType;
                        objData.CONTACT = item.CONTACT;
                        objData.isCritical = item.isCritical;
                        objData.isRoot = item.isRoot;
                        objData.Camera = item.Camera;
                        objclsDataFlow.nodeDataArray.Add(objData);

                    }



                    ViewBag.ParentChildDtl = listParentChildArray;
                    ViewBag.clsDataFlow = objclsDataFlow;
                    ViewBag.ProjectName = ProjectName;


                    return PartialView("_Diagram", newList);
                }



            }



            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_Diagram", objPTSModelList);   // its just formality for trying new opproch 
        }


        #region Load By Excel
        [HttpPost]
        public PartialViewResult LoadProjectTaskListByExcel(JQueryDataTableParamModel param, string ProjectName, string height, string width)
        {
            var objPTSModelList = new PTSModelList();
            var list = new List<PTSModel>();
            clsDataFlow objclsDataFlow = new clsDataFlow();
            objclsDataFlow.linkDataArray = new List<linkDataArray>();
            objclsDataFlow.nodeDataArray = new List<nodeDataArray>();

            ViewBag.WidowsHeight = height;
            ViewBag.WidowsWidth = width;
            ViewBag.SvgWidth = 0;

            try
            {
                #region Read File
                if (Request.Files.Count > 0)
                {
                    try
                    {
                        DataSet ds = new DataSet();
                        System.Web.HttpPostedFileBase file = Request.Files[0];
                        using (ExcelPackage package = new ExcelPackage(file.InputStream))
                        {
                            ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();
                            DataTable dtHeaderExcel = new DataTable();
                            int notesIndex = 0;

                            #region Read Lines data from excel 
                            foreach (var firstRowCell in excelWorksheet.Cells[1, 1, 1, excelWorksheet.Dimension.End.Column])
                            {
                                dtHeaderExcel.Columns.Add(firstRowCell.Text);
                            }

                            for (var rowNumber = 2; rowNumber <= excelWorksheet.Dimension.End.Row; rowNumber++)
                            {
                                var row = excelWorksheet.Cells[rowNumber, 1, rowNumber, excelWorksheet.Dimension.End.Column];
                                var newRow = dtHeaderExcel.NewRow();
                                if (excelWorksheet.Cells[rowNumber, 1].Value != null)
                                {
                                    foreach (var cell in row)
                                    {
                                        newRow[cell.Start.Column - 1] = cell.Text;
                                    }
                                }
                                else
                                {
                                    notesIndex = rowNumber + 1;
                                    break;
                                }

                                dtHeaderExcel.Rows.Add(newRow);
                            }
                            ds.Tables.Add(dtHeaderExcel);
                            #endregion
                        }

                        foreach (DataRow item in ds.Tables[0].Rows)
                        {
                            #region Add New MIP Master Operations
                            var newRecord = new PTSModel
                            {
                                PROJECTID = item.Field<string>("Project"),
                                ProjectCode = item.Field<string>("Project"),
                                TaskDescription = item.Field<string>("Name"),
                                ActualFinish = item.Field<string>("Finish_Date") != string.Empty ? Convert.ToDateTime(item.Field<string>("Finish_Date")).ToString("dd-MM-yyyy") : string.Empty,
                                PlannedCompletion = item.Field<string>("Start_Date"),
                                ExpectedFinish = item.Field<string>("Finish_Date"),
                                TaskStatus = "",
                                TaskType = 0,
                                CurrRemDur = "",
                                Shop = "",
                                IPDate = "",
                                PlannedDuration = item.Field<string>("Duration"),
                                Predecessors = item.Field<string>("Unique_ID_Predecessors")+"",
                                UID = item.Field<string>("Unique_ID"),
                                Successors = item.Field<string>("Unique_ID_Successors")+"",
                                RCCP = "",
                                RTSDate = "",
                                ActualOrProjectedDuration = item.Field<string>("Actual_Start"),
                                CONTACT = "",
                                isCritical = "",
                                Camera = "",

                            };
                            list.Add(newRecord);

                            //db.SaveChanges();
                            #endregion
                        }
                    }
                    catch (Exception ex)
                    {
                        return PartialView("_Diagram", new List<PTSModel>());
                        //return Content("Error occurred. Error details: " + ex.Message);
                    }
                }
                else
                {
                    return PartialView("_Diagram", new List<PTSModel>());
                    //return Json("No files selected.");
                }
                #endregion

                var newList = list.OrderBy(x => x.TaskStatus).ToList();
                foreach (var item in newList)
                {
                    nodeDataArray objData = new nodeDataArray();
                    objData.key = item.UID;
                    objData.name = item.UID;
                    objData.type = "Contractor";
                    objData.PlannedCompletion = item.PlannedCompletion;
                    objData.RCCP = item.RCCP;
                    objData.Shop = item.Shop;
                    objData.TaskDescription = item.TaskDescription;
                    objData.TaskStatus = item.TaskStatus;
                    objData.TaskType = item.TaskType;
                    objData.CONTACT = item.CONTACT;
                    objData.isCritical = item.isCritical;
                    objData.isRoot = (item.Predecessors == "");
                    objData.Camera = item.Camera;
                    objclsDataFlow.nodeDataArray.Add(objData);

                    var Predecessors = item.Predecessors.Split(',').Where(i => i != "").ToList();
                    var Successors = item.Successors.Split(',').Where(i => i != "").ToList();

                    foreach (var parent in Predecessors)
                    {
                        var parentNum = System.Text.RegularExpressions.Regex.Matches(parent, @"\d+")[0].Value;
                        var parentdtl = newList.FirstOrDefault(i => i.UID == parentNum);
                        if (parentdtl == null)
                            continue;

                        if (!listParentChildArray.Any(i => (i.ParentUID == parentdtl.UID && i.ChildUID == item.UID) || (i.ParentUID == item.UID && i.ChildUID == parentdtl.UID)))
                        {
                            listParentChildArray.Add(new ParentChildArray { ChildUID = item.UID, ParentUID = parentdtl.UID });
                        }
                    }

                    foreach (var parent in Successors)
                    {
                        var parentNum = System.Text.RegularExpressions.Regex.Matches(parent, @"\d+")[0].Value;
                        var childdtl = newList.FirstOrDefault(i => i.UID == parentNum);
                        if (childdtl == null)
                            continue;

                        if (!listParentChildArray.Any(i => (i.ParentUID == item.UID && i.ChildUID == childdtl.UID) || (i.ParentUID == childdtl.UID && i.ChildUID == item.UID)))
                        {
                            listParentChildArray.Add(new ParentChildArray { ChildUID = childdtl.UID, ParentUID = item.UID });
                        }
                    }

                    GetChildArrayByExcel(newList, item);
                }

                foreach (var item in listParentChildArray)
                {
                    linkDataArray objlink = new linkDataArray();
                    /*if (item.parent.isCritical == "Yes")
                    {
                        objlink.isCritical = "Yes";
                    }*/
                    objlink.from = item.ParentUID;
                    objlink.frompid = "OUT";
                    objlink.to = item.ChildUID;
                    objlink.topid = "IN";
                    objclsDataFlow.linkDataArray.Add(objlink);
                }
                  
                ViewBag.ParentChildDtl = listParentChildArray;
                ViewBag.clsDataFlow = objclsDataFlow;
                ViewBag.ProjectName = ProjectName;

                return PartialView("_Diagram", newList);
            }


            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return PartialView("_Diagram", objPTSModelList);   // its just formality for trying new opproch 
        }


        public void GetChildArrayByExcel(List<PTSModel> newList, PTSModel child)
        {
            if (!listProceedItem.Any(i => i == child.UID))
            {
                var newchild = newList.Where(i => i.Predecessors.Contains(child.UID + ",")).ToList();
                foreach (var ichild in newchild)
                {
                    var Predecessors = ichild.Predecessors.Split(',').Where(i => i != "").ToList();
                    var Successors = ichild.Successors.Split(',').Where(i => i != "").ToList();

                    foreach (var parent in Predecessors)
                    {
                        var parentNum = System.Text.RegularExpressions.Regex.Matches(parent, @"\d+")[0].Value;
                        if (!newList.Any(a => a.UID == parentNum))
                                continue;

                        if (!listParentChildArray.Any(i => (i.ParentUID == parentNum && i.ChildUID == ichild.UID) || (i.ParentUID == ichild.UID && i.ChildUID == parentNum)))
                        {
                            listParentChildArray.Add(new ParentChildArray { ChildUID = ichild.UID, ParentUID = parentNum });
                        }
                    }

                    foreach (var parent in Successors)
                    {
                        var parentNum = System.Text.RegularExpressions.Regex.Matches(parent, @"\d+")[0].Value;
                        if (!newList.Any(a => a.UID == parentNum))
                            continue;
                       
                        if (!listParentChildArray.Any(i => (i.ParentUID == ichild.UID && i.ChildUID == parentNum) || (i.ParentUID == parentNum && i.ChildUID == ichild.UID)))
                        {
                            listParentChildArray.Add(new ParentChildArray { ChildUID = parentNum, ParentUID = ichild.UID });
                        }
                    }
                    listProceedItem.Add(child.UID);
                    GetChildArrayByExcel(newList, ichild);
                }
            }

        }


        #endregion
        public void GetChildArray(List<PTSModel> newList, PTSModel child)
        {
            if (!listProceedItem.Any(i => i == child.UID))
            {
                var newchild = newList.Where(i => i.Predecessors.Contains(child.UID + ",")).ToList();
                foreach (var item in newchild)
                {
                    var Predecessors = item.Predecessors.Split(',').Where(i => i != "").ToList();
                    var Successors = item.Successors.Split(',').Where(i => i != "").ToList();

                    foreach (var parent in Predecessors)
                    {
                        var parentdtl = newList.Where(i => i.UID == parent).FirstOrDefault();
                        NodeDetails parentnodedtl = new NodeDetails();
                        parentnodedtl.id = parentdtl.UID;
                        parentnodedtl.UID = parentdtl.UID;
                        parentnodedtl.TaskDescription = parentdtl.TaskDescription;
                        parentnodedtl.ActualFinish = parentdtl.ActualFinish;
                        parentnodedtl.PlannedCompletion = parentdtl.PlannedCompletion;
                        parentnodedtl.TaskStatus = parentdtl.TaskStatus;
                        parentnodedtl.TaskType = parentdtl.TaskType;
                        parentnodedtl.Shop = parentdtl.Shop;
                        parentnodedtl.RCCP = parentdtl.RCCP;
                        parentnodedtl.isCritical = parentdtl.isCritical;

                        NodeDetails childnodedtl = new NodeDetails();
                        childnodedtl.id = item.UID;
                        childnodedtl.UID = item.UID;
                        childnodedtl.TaskDescription = item.TaskDescription;
                        childnodedtl.ActualFinish = item.ActualFinish;
                        childnodedtl.PlannedCompletion = item.PlannedCompletion;
                        childnodedtl.TaskStatus = item.TaskStatus;
                        childnodedtl.TaskType = item.TaskType;
                        childnodedtl.Shop = item.Shop;
                        childnodedtl.RCCP = item.RCCP;
                        childnodedtl.isCritical = item.isCritical;
                        if (!listParentChildArray.Any(i => (i.parent.UID == parentnodedtl.UID && i.child.UID == childnodedtl.UID) || (i.parent.UID == childnodedtl.UID && i.child.UID == parentnodedtl.UID)))
                        {
                            listParentChildArray.Add(new ParentChildArray { child = childnodedtl, parent = parentnodedtl });
                        }
                    }

                    foreach (var parent in Successors)
                    {
                        var childdtl = newList.Where(i => i.UID == parent).FirstOrDefault();
                        NodeDetails parentnodedtl = new NodeDetails();
                        parentnodedtl.id = item.UID;
                        parentnodedtl.UID = item.UID;
                        parentnodedtl.TaskDescription = item.TaskDescription;
                        parentnodedtl.ActualFinish = item.ActualFinish;
                        parentnodedtl.PlannedCompletion = item.PlannedCompletion;
                        parentnodedtl.TaskStatus = item.TaskStatus;
                        parentnodedtl.TaskType = item.TaskType;
                        parentnodedtl.Shop = item.Shop;
                        parentnodedtl.RCCP = item.RCCP;
                        parentnodedtl.isCritical = item.isCritical;

                        NodeDetails childnodedtl = new NodeDetails();
                        childnodedtl.id = childdtl.UID;
                        childnodedtl.UID = childdtl.UID;
                        childnodedtl.TaskDescription = childdtl.TaskDescription;
                        childnodedtl.ActualFinish = childdtl.ActualFinish;
                        childnodedtl.PlannedCompletion = childdtl.PlannedCompletion;
                        childnodedtl.TaskStatus = childdtl.TaskStatus;
                        childnodedtl.TaskType = childdtl.TaskType;
                        childnodedtl.Shop = childdtl.Shop;
                        childnodedtl.RCCP = childdtl.RCCP;
                        childnodedtl.isCritical = childdtl.isCritical;
                        if (!listParentChildArray.Any(i => (i.parent.UID == parentnodedtl.UID && i.child.UID == childnodedtl.UID) || (i.parent.UID == childnodedtl.UID && i.child.UID == parentnodedtl.UID)))
                        {
                            listParentChildArray.Add(new ParentChildArray { child = childnodedtl, parent = parentnodedtl });
                        }
                    }
                    listProceedItem.Add(child.UID);
                    GetChildArray(newList, item);
                }
            }

        }

        public class PTSModelList
        {
            public List<PTSModel> SecondLists { get; set; }
            public List<PTSModel> newLists { get; set; }
        }

        public void GetChildNodes(List<PTSModel> newList, PTSModel child, int Level)
        {

            var newchild = newList.Where(i => i.Predecessors.Contains(child.UID + ",")).ToList();
            foreach (var item in newchild)
            {
                item.Level = Level + 1;
                if (!listNodes.Any(i => i.UID == item.UID))
                {
                    listNodes.Add(item);
                }
                GetChildNodes(newList, item, item.Level);
            }
        }

        public static List<PTSModel> GetChildren(List<PTSModel> MaintainPTSs, string parentId, int Level)
        {
            var a = parentId.Split(',').Where(m => m != "");
            var lst = MaintainPTSs
                    .Where(c => a.Contains(c.UID))
                    .Select(c => new PTSModel
                    {

                        PROJECTID = c.PROJECTID,
                        ProjectCode = c.ProjectCode,
                        TaskDescription = c.TaskDescription,
                        ActualFinish = c.ActualFinish,
                        PlannedCompletion = c.PlannedCompletion,
                        ExpectedFinish = c.ExpectedFinish,
                        TaskStatus = c.TaskStatus,
                        CurrRemDur = c.CurrRemDur,
                        Shop = c.Shop,
                        IPDate = c.IPDate,
                        PlannedDuration = c.PlannedDuration,
                        Predecessors = c.Predecessors,
                        UID = c.UID,
                        Successors = c.Successors,
                        RCCP = c.RCCP,
                        RTSDate = c.RTSDate,
                        ActualOrProjectedDuration = c.ActualOrProjectedDuration,
                        Level = Level + 1,
                        Children = GetChildren(MaintainPTSs, c.Successors, Level + 1)
                    })
                    .ToList();
            return lst;
        }

        public JsonResult GetNodeData(string PROJECTID)
        {



            try
            {

                string query = "with RCCPHours as ( SELECT PROJECTID, TASKUNIQUEID, SUM(CONVERT(decimal(20, 2), RCCP)) AS Hours FROM(       SELECT PROJECTID, TASKUNIQUEID, /*dbo.ufnGetRCCPValue(TEXT1)*/ 0 AS RCCP, TEXT1       FROM   dbo.S2M_TASKCHECKLIST AS S2M_TASKCHECKLIST_2 with(nolock)       WHERE(LTRIM(RTRIM(UPPER(CLITEMTEXT))) NOT IN('TOTAL', 'TOT', 'TOT.', 'TTL', 'TTL.'))       and    charindex('TOT', upper(ltrim(rtrim(CLITEMTEXT)))) != 1       ) AS a GROUP BY PROJECTID, TASKUNIQUEID ) " +

"select a.PROJECTID, a.PROJECTNAME ProjectCode, a.TASKUNIQUEID UID, a.NAME TaskDescription, ACTUALFINISH ActualFinish, BASELINEFINISH PlannedCompletion, a.EXPECTED_END_DATE ExpectedFinish, case a.TASK_STATUS when 0 then 'NS' when 1 then 'IP' when 2 then 'CO' end TaskStatus " +
 " , case when a.TASK_STATUS <> 2 then a.REMAININGDURATION / 4800 else 0 end CurrRemDur, b.USER_FULL_NAME Shop, d.IPDate, isnull(e.PlannedDuration, a.REMAININGDURATION / 4800) PlannedDuration     , f.Predecessors,a.TASKUNIQUEID UID, g.Successors, isnull(h.Hours, 0) RCCP, i.RTSDate " +
     " , CASE a.TASK_STATUS when 0 then 0.00 when 1      then datediff(d, isnull(convert(date, i.RTSDate), convert(date, d.IPDate)), GETDATE())-datediff(WK, isnull(convert(date, i.RTSDate), convert(date, d.IPDate)), GETDATE()) + a.REMAININGDURATION / 4800  when 2      then datediff(d, isnull(convert(date, i.RTSDate), convert(date, d.IPDate)), a.ACTUALFINISH + 1)-datediff(WK, isnull(convert(date, i.RTSDate), convert(date, d.IPDate)), a.ACTUALFINISH + 1)       end[ActualOrProjectedDuration] " +

" from PROJ_TASK a with(nolock) left join " +

        " RES_USERS b with(Nolock) on a.CONTACT = b.USER_NAME outer apply " +

        "(select MAX(d0.MODIFIED_TIMESTAMP) IPDate from VR_ALL_TU_HISTORY d0 with(nolock) where a.PROJECTID = d0.PROJECTID and a.TASKUNIQUEID = d0.TASKUNIQUEID and d0.FIELDID = 7) d outer apply " +
        "(select top 1 LONGVAL / 4800 PlannedDuration from VR_ALL_TU_HISTORY e0 with(nolock) where a.PROJECTID = e0.PROJECTID and a.TASKUNIQUEID = e0.TASKUNIQUEID and e0.FIELDID = 9 and e0.MODIFIED_TIMESTAMP < d.IPDate order by e0.MODIFIED_TIMESTAMP desc) e outer apply " +
            "(select(select convert(varchar(max), f0.PredecessorTaskUniqueID) + ',' from Task_Dependencies f0 with(nolock) where f0.ProjectID = a.PROJECTID and f0.SuccessorTaskUniqueID = a.TASKUNIQUEID for xml path('')) Predecessors) f outer apply " +
            "(select(select convert(varchar(max), g0.SuccessorTaskUniqueID) + ',' from Task_Dependencies g0 with(nolock) where g0.ProjectID = a.PROJECTID and g0.PredecessorTaskUniqueID = a.TASKUNIQUEID for xml path('')) Successors) g left join " +
            "RCCPHours h with(nolock)on a.PROJECTID = h.PROJECTID and a.TASKUNIQUEID = h.TASKUNIQUEID outer apply(select MAX(i0.MODIFIED_TIMESTAMP) RTSDate from VR_ALL_TU_HISTORY i0 with(nolock) where a.PROJECTID = i0.PROJECTID and a.TASKUNIQUEID = i0.TASKUNIQUEID and i0.FIELDID = 25 and i0.LONGVAL <> 0 and convert(date, i0.MODIFIED_TIMESTAMP) <= (case a.TASK_STATUS when 2 then convert(date, a.ACTUALFINISH) else GETDATE() end)) i " +

"where   SUMMARY <> 1 " +

                //"and a.SPECIALTASKTYPE <> 2  and a.TASK_TYPE <> 3    and a.PROJECTNAME = '" + PROJECTID.Trim() + "'  order by a.TASKID DESC";
                "and a.SPECIALTASKTYPE <> 2  and a.TASK_TYPE <> 3    and a.PROJECTNAME = '22137 D1'  order by a.TASKID DESC";


                connection.Open();
                List<PTSModel> list = new List<PTSModel>();
                using (var command = new SqlCommand(string.Format(query), connection))

                {


                    SqlDataReader reader = command.ExecuteReader();

                    try
                    {
                        while (reader.Read())
                        {


                            var newRecord = new PTSModel
                            {
                                Id = Convert.ToInt32(reader["UID"].ToString()),
                                //ParentId = Convert.ToInt32((reader["Predecessors"].ToString()).Split(',')[0]),
                                //ParentId = reader["Predecessors"].ToString().Split(',')[0],
                                ParentId = Convert.ToInt32(reader["UID"].ToString()),
                                name = reader["TaskDescription"].ToString(),
                            };
                            list.Add(newRecord);
                        }

                    }
                    finally
                    {
                        reader.Close();
                        connection.Close();
                    }

                    var SecondList = list.ToList();

                    var newList = list.OrderBy(x => x.TaskStatus).ToList();



                    return Json(new
                    {
                        ////sEcho = Convert.ToInt32(param.sEcho),
                        //iTotalRecords = 0,
                        //iTotalDisplayRecords = 0,
                        newList

                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    //sEcho = param.sEcho,
                    //iTotalRecords = "0",
                    //iTotalDisplayRecords = "0",
                    data = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetCameraDetail(string camera)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (camera.Length > 0)
                {
                    var pts002 = new PTS002();
                    string query = "Select * from PTS002 where Camera = '" + camera + "'";
                    pts002 = db.Database.SqlQuery<PTS002>(query).FirstOrDefault();
                    if (pts002 != null)
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = pts002.Address;
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Camera not Found";
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Invalid camera detail";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadProjectUIDList(string PROJECTID, string BuName, string CheckedCustomerList)
        {
            ViewBag.ProjectID = PROJECTID;
            ViewBag.BuName = BuName;
            ViewBag.CheckedCustomerList = CheckedCustomerList;
            return PartialView("_GroupList");
        }

        //Save Group Record
        [HttpPost]
        public ActionResult SaveData(FormCollection fc, string PROJECTID, string BuName, string CheckedCustomerList)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            //#region Planning Din Header
            int newRowIndex = 0;
            int userId = Convert.ToInt32(fc["UserId" + newRowIndex]);
            string tableName = string.Empty;


            try
            {
                PTS001 objPTS001 = new PTS001();
                //PTS001 objPTS001 = db.PTS001.Add(new PTS001                
                objPTS001.GroupName = fc["GroupName" + newRowIndex];
                objPTS001.BU = BuName;
                objPTS001.IsGroup = Convert.ToBoolean(fc["IsGroup" + newRowIndex]);
                objPTS001.Project = PROJECTID;
                objPTS001.Customer = CheckedCustomerList;

                objPTS001.GroupName = fc["GroupName" + newRowIndex];
                objPTS001.Uids = fc["Uids" + newRowIndex];
                var PCNAME = System.Net.Dns.GetHostName();
                objPTS001.CreatedBy = Manager.GetSystemUserPSNo(); // its becuse compulsory 
                objPTS001.CreatedOn = DateTime.Now;
                db.PTS001.Add(objPTS001);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.NDEConsumeMessage.Insert;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);


        }

        [HttpPost]
        public ActionResult UpdateDetails(int id, string columnName, string columnValue)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    db.SP_COMMON_TABLE_UPDATE("PTS001", id, "GroupId", columnName, columnValue, Manager.GetSystemUserPSNo());
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult LoadCameraMasterPartial()
        {
            return PartialView("_CameraMasterPartial");
        }

        [HttpPost]
        public ActionResult LoadCameraMasterList(JQueryDataTableParamModel param)
        {
            var objPTSModelList = new PTSModelList();
            int StartIndex = 0;
            int EndIndex = 0;
            StartIndex = param.iDisplayStart + 0;
            EndIndex = param.iDisplayStart + param.iDisplayLength;

            var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
            var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
            string strSortOrder = string.Empty;
            string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
            string sortDirection = Convert.ToString(Request["sSortDir_0"]);

            if (!string.IsNullOrWhiteSpace(sortColumnName))
            {
                strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
            }
            else
            {
                strSortOrder = " Order By Id ";
            }



            var lst = new List<PTS002>();
            try
            {

                string query = "Select *,COUNT(*) OVER () as TotalCount from PTS002  " + strSortOrder + " OFFSET " + StartIndex + " ROWS FETCH NEXT " + EndIndex + " ROWS ONLY";
                lst = db.Database.SqlQuery<PTS002>(query).ToList();


                int newRecordId = 0;
                var newRecord = new[] {
                                        Convert.ToString(newRecordId),
                                        Helper.GenerateTextbox(newRecordId, "Camera",  "", "", false, "", "100",""),
                                        Helper.GenerateTextbox(newRecordId,"Address", "", "", false, "", "100",""),
                                        Helper.GenerateGridButtonNew(newRecordId, "Save", "Save Rocord", "fa fa-save"," SaveNewRecord();"),
                                    };

                var data = (from uc in lst
                            select new[]
                                       {
                                           uc.Id.ToString(),
                                            "<span class=' ' data-col='description' data-maxlength='100' >" +Convert.ToString(uc.Camera)+ "</span>",
                                            "<span class=' ' data-col='description' data-maxlength='100' >" +Convert.ToString(uc.Address)+ "</span>",
                                            Helper.GenerateGridButtonNew(newRecordId, "", "Delete Record", "icon-trash"," DeleteCameraRecord("+uc.Id+");"),
                                        }).ToList();

                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = lst.Count,
                    iTotalDisplayRecords = lst.Count,
                    aaData = data,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveCameraData(FormCollection fc)
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var camera = fc["Camera0"];
                if (!db.PTS002.Where(x => x.Camera == camera).Any())
                {
                    var pts002 = new PTS002()
                    {
                        Address = fc["Address0"],
                        Camera = camera,
                        CreatedBy = Manager.GetSystemUserPSNo(),
                        CreatedOn = DateTime.Now
                    };
                    db.PTS002.Add(pts002);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult LoadCameraSingleLine(int id)
        {
            var pts002 = new PTS002();
            try
            {
                string query = "Select *,count(*) from PTS002 where Id = " + id;
                pts002 = db.Database.SqlQuery<PTS002>(query).FirstOrDefault();

                var data = new string[] { };
                if (pts002 != null)
                {
                    data = new[]{
                                  pts002.Id.ToString(),
                                  Helper.GenerateTextbox(pts002.Id, "Camera", Convert.ToString(pts002.Camera),"UpdateData(this, "+ pts002.Id+",true);", false ,"","50","editable"),
                                  Helper.GenerateTextbox(pts002.Id, "Address", Convert.ToString(pts002.Address),"UpdateData(this, "+ pts002.Id+",true);", false ,"","50","editable"),
                                   Helper.GenerateGridButtonNew(pts002.Id, "", "Delete Record", "icon-trash"," DeleteCameraRecord("+pts002.Id+");")};
                }
                return Json(new
                {
                    iTotalRecords = data.Count(),
                    iTotalDisplayRecords = data.Count(),
                    aaData = data,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdateCameraDetails(int id, string columnName, string columnValue)
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    db.SP_COMMON_TABLE_UPDATE("PTS002", id, "Id", columnName, columnValue, Manager.GetSystemUserPSNo());
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteCameraDetails(int id)
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (id > 0)
                {
                    var pts002 = db.PTS002.Find(id);
                    db.PTS002.Remove(pts002);
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        #region -- For task partial

        [HttpPost]
        public ActionResult GetTaskDetails(int TaskId, string Project)
        {
            ViewBag.Project = Project;
            ViewBag.TaskId = TaskId;
            return PartialView("_TaskSummary");
        }

        public JsonResult LoadTaskList(JQueryDataTableParamModel param)
        {
            int StartIndex = 0;
            int EndIndex = 0;
            int datacount = 0;

            var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
            string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
            string sortDirection = Convert.ToString(Request["sSortDir_0"]);

            StartIndex = param.iDisplayStart + 1;
            EndIndex = param.iDisplayStart + param.iDisplayLength;
            string Nodeid = param.Nodeid;
            string project = param.Project.Replace("%20", " ").Trim();

            string strWhere = string.Empty;

            string strSortOrder = string.Empty;
            if (!string.IsNullOrWhiteSpace(sortColumnName))
            {
                strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
            }
            else
            {
                strSortOrder = " Order By SrNo ASC ";
            }

            if (!string.IsNullOrWhiteSpace(param.sSearch))
            {
                strWhere += "and (CLITEMID like '%" + param.sSearch
                     + "%' or CLITEMSTATUS like '%" + param.sSearch
                     + "%' or CLITEMTEXT like '%" + param.sSearch
                     + "%' or TEXT1 like '%" + param.sSearch
                     + "%' or TEXT2 like '%" + param.sSearch
                     + "%' or TEXT3 like '%" + param.sSearch
                     + "%' or TEXT4 like '%" + param.sSearch
                     + "%' or a.DATE1 like '%" + param.sSearch
                     + "%' or a.DATE2 like '%" + param.sSearch + "%')";
            }

            List<string[]> lst = new List<string[]>();
            try
            {
                string query = "";
                if (strWhere == "")
                {
                    query = "SELECT * FROM ( SELECT ROW_NUMBER() OVER ( " + strSortOrder + ") AS ROW_NO, COUNT(1) OVER() AS TotalCount, SrNo, TaskStatus, TaskDescription, Duration_WorkContent, DocumentList, ResourceList, MaterialList, ActualStartDate, ActualCompletionDate FROM (" +
                        "Select CLITEMID SrNo, case CLITEMSTATUS when 1 then 'Completed' else 'Pending' end TaskStatus, CLITEMTEXT TaskDescription, TEXT1 Duration_WorkContent, TEXT2 DocumentList, TEXT3 ResourceList, TEXT4 MaterialList, a.DATE1 ActualStartDate, a.DATE2 ActualCompletionDate" +
                        " From S2M_TASKCHECKLIST a with(nolock) join PROJECT b witH(nolock) on a.PROJECTID = b.PROJECTID Where b.PROJECTNAME = '" + project + "' and TASKUNIQUEID = '" + Nodeid + "' ) tm" +
                        ") f WHERE f.ROW_NO BETWEEN " + StartIndex + " AND " + EndIndex + "";
                }
                else
                {
                    query = "SELECT * FROM ( SELECT ROW_NUMBER() OVER ( " + strSortOrder + ") AS ROW_NO, COUNT(1) OVER() AS TotalCount, SrNo, TaskStatus, TaskDescription, Duration_WorkContent, DocumentList, ResourceList, MaterialList, ActualStartDate, ActualCompletionDate FROM (" +
                           "Select CLITEMID SrNo, case CLITEMSTATUS when 1 then 'Completed' else 'Pending' end TaskStatus, CLITEMTEXT TaskDescription, TEXT1 Duration_WorkContent, TEXT2 DocumentList, TEXT3 ResourceList, TEXT4 MaterialList, a.DATE1 ActualStartDate, a.DATE2 ActualCompletionDate" +
                           " From S2M_TASKCHECKLIST a with(nolock) join PROJECT b witH(nolock) on a.PROJECTID = b.PROJECTID Where b.PROJECTNAME = '" + project + "' and TASKUNIQUEID = '" + Nodeid + "' " + strWhere + " ) tm" +
                           ") f WHERE f.ROW_NO BETWEEN " + StartIndex + " AND " + EndIndex + "";

                }

                connection.Open();
                using (var command = new SqlCommand(string.Format(query), connection))
                {
                    SqlDataReader reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            var newRecord = new string[]
                            {
                               reader["SrNo"].ToString(),
                               reader["TaskStatus"].ToString() == "Completed" ? "<i title = 'Completed' class='icon-check' style='border-radius: 12px !important;background: green;color: white;font-size: 25px;' aria-hidden='true'></i>":"<i title = 'Pending' class='icon-info' style='border-radius: 12px !important;background: yellow;color: black;font-size: 25px;' aria-hidden='true'></i>",
                               reader["TaskDescription"].ToString(),
                               reader["Duration_WorkContent"].ToString(),
                               reader["DocumentList"].ToString(),
                               reader["ResourceList"].ToString(),
                               reader["MaterialList"].ToString(),
                               (!string.IsNullOrEmpty(Convert.ToString(reader["ActualStartDate"]))) ? Convert.ToDateTime(reader["ActualStartDate"]).ToString("dd/MM/yyyy"):"",
                               (!string.IsNullOrEmpty(Convert.ToString(reader["ActualCompletionDate"]))) ? Convert.ToDateTime(reader["ActualCompletionDate"]).ToString("dd/MM/yyyy"):""
                        };
                            datacount = Convert.ToInt32(reader["TotalCount"]);
                            lst.Add(newRecord);
                        }
                    }
                    finally
                    {
                        reader.Close();
                        connection.Close();
                    }
                    return Json(new
                    {
                        //sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = datacount,
                        iTotalDisplayRecords = datacount,
                        aaData = lst,
                    }, JsonRequestBehavior.AllowGet);
                }
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    //sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }


        #endregion

        #region -- for span tag --
        public static string GenerateSpan(string spanName, string buttonTooltip = "", string className = "")
        {
            string htmlControl = "";
            htmlControl = "<span Title='" + buttonTooltip + "' class='" + className + "'>"+ spanName + "</i>";
            return htmlControl;
        }
        #endregion
    }
}