﻿using IEMQS.Areas.Authenticate.Models;
using IEMQSImplementation;
using IEMQSImplementation.Models;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using IEMQS.HEDirectoryService;

namespace IEMQS.Areas.Authenticate.Controllers
{
    public class AuthenticateController : clsBase
    {
        #region User Authenticaion
        /// <summary>
        /// Created By Dharmesh
        /// 26-06-2017
        /// For User Authentication
        /// </summary>
        /// <returns></returns>
        [CookiesFilter]
        public ActionResult Index()
        {
            if (!String.IsNullOrWhiteSpace(Request["upsno"]))
            {
                try
                {
                    DirectoryService directoryService = new DirectoryService();
                    var upsno = directoryService.Decrypt(Request["upsno"]);
                    string PSNo = Convert.ToInt64(upsno, 16) + "";
                    var Msg = "";
                    Manager.SetLoginSessionDetails(PSNo, ref Msg, clsImplementationEnum.UserActionType.LoginAs);
                    if (Msg.Length > 0)
                    {
                        clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = Msg;
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        if (!String.IsNullOrWhiteSpace(Request["redirect"]))
                            return Redirect(Request["redirect"]);
                    }

                }
                catch { }
            }
            if (!string.IsNullOrWhiteSpace(Convert.ToString(ConfigurationManager.AppSettings["IsAppOffline"])) && Convert.ToString(ConfigurationManager.AppSettings["IsAppOffline"]) == "1")
            {
                return RedirectToAction("Maintenance", "Authenticate");
            }
            var IsDESLive = Convert.ToBoolean(Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.IsDESLive.GetStringValue()));
            if (IsDESLive)
            {
                if (objClsLoginInfo != null)
                {
                    //return RedirectToAction("Index", "Dashboard", new { area = "MyDashboard" });
                    var lstResult = objClsLoginInfo.ListRoles.Select(a => new { RoleId = a, RoleDesc = a });
                    if (lstResult.Any(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.ENGG3.GetStringValue(), StringComparison.OrdinalIgnoreCase) || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.ENGG2.GetStringValue(), StringComparison.OrdinalIgnoreCase) || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.ENGG1.GetStringValue(), StringComparison.OrdinalIgnoreCase)))
                    {
                        return RedirectToAction("Index", "Dashboard", new { area = "DES" });
                    }
                    else
                    {
                        return RedirectToAction("Index", "Dashboard", new { area = "MyDashboard" });
                    }
                }
                return View();
            }
            else
            {
                if (objClsLoginInfo != null)
                {
                    return RedirectToAction("Index", "Dashboard", new { area = "MyDashboard" });
                }
                return View("Login");
            }

        }

        [CookiesFilter]
        public ActionResult Login()
        {
            if (!String.IsNullOrWhiteSpace(Request["upsno"]))
            {
                try
                {
                    DirectoryService directoryService = new DirectoryService();
                    var upsno = directoryService.Decrypt(Request["upsno"]);
                    string PSNo = Convert.ToInt64(upsno, 16) + "";
                    var Msg = "";
                    Manager.SetLoginSessionDetails(PSNo, ref Msg, clsImplementationEnum.UserActionType.LoginAs);
                    if (Msg.Length > 0)
                    {
                        clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = Msg;
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        if (!String.IsNullOrWhiteSpace(Request["redirect"]))
                            return Redirect(Request["redirect"]);
                    }

                }
                catch { }
            }
            if (!string.IsNullOrWhiteSpace(Convert.ToString(ConfigurationManager.AppSettings["IsAppOffline"])) && Convert.ToString(ConfigurationManager.AppSettings["IsAppOffline"]) == "1")
            {
                return RedirectToAction("Maintenance", "Authenticate");
            }
            var IsDESLive = Convert.ToBoolean(Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.IsDESLive.GetStringValue()));
            if (IsDESLive)
            {
                if (objClsLoginInfo != null)
                {
                    // return RedirectToAction("Index", "Dashboard", new { area = "MyDashboard" });
                    var lstResult = objClsLoginInfo.ListRoles.Select(a => new { RoleId = a, RoleDesc = a });
                    if (lstResult.Any(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.ENGG3.GetStringValue(), StringComparison.OrdinalIgnoreCase) || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.ENGG2.GetStringValue(), StringComparison.OrdinalIgnoreCase) || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.ENGG1.GetStringValue(), StringComparison.OrdinalIgnoreCase)))
                    {
                        return RedirectToAction("Index", "Dashboard", new { area = "DES" });
                    }
                    else
                    {
                        return RedirectToAction("Index", "Dashboard", new { area = "MyDashboard" });
                    }
                }
                return View();
            }
            else
            {
                if (objClsLoginInfo != null)
                {
                    return RedirectToAction("Index", "Dashboard", new { area = "MyDashboard" });
                }
                return View("Login");
            }
        }
        public bool IsDESLive()
        {
            bool flg = false;
            var IsDESLive = Convert.ToBoolean(Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.IsDESLive.GetStringValue()));
            if (IsDESLive)
                flg = true;

            return flg;
        }
        public ActionResult Maintenance()
        {
            return View();
        }

        public ActionResult AutoLogin()
        {
            string returnUrl = string.Empty;

            var auto = db.CONFIG.Where(x => x.Key == "Autologin").FirstOrDefault();

            if (auto != null)
            {
                if (!String.IsNullOrWhiteSpace(auto.Value))
                {
                    var Msg = "";
                    clsManager Manager = new clsManager();
                    Manager.SetLoginSessionDetails(auto.Value, ref Msg, clsImplementationEnum.UserActionType.LoginAs);

                    if (System.Web.HttpContext.Current.Session[clsImplementationMessage.Session.LoginInfo] != null)
                    {
                        var url = db.CONFIG.Where(x => x.Key == "AutoURL").FirstOrDefault();
                        if (url != null)
                        {
                            if (!String.IsNullOrWhiteSpace(url.Value))
                                returnUrl = url.Value;
                        }
                    }
                }
            }
            return Redirect(WebsiteURL + returnUrl);
        }

        [HttpPost]
        public ActionResult Login(clsLogin objclsLogin)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                bool res = true;

                if (!string.IsNullOrWhiteSpace(Convert.ToString(ConfigurationManager.AppSettings["IsPasswordProtected"])) && Convert.ToString(ConfigurationManager.AppSettings["IsPasswordProtected"]).ToLower().Trim() == "true")
                {
                    //IEMQSImplementation.AuthenticationService.DirectoryService ds = new IEMQSImplementation.AuthenticationService.DirectoryService();
                    //res = ds.getAuthentication(objclsLogin.Username, objclsLogin.Password);
                    PrincipalContext ctx = new PrincipalContext(ContextType.Domain);
                    res = ctx.ValidateCredentials(objclsLogin.Username, objclsLogin.Password);
                }

                if (res)
                {
                    var Msg = "";
                    Manager.SetLoginSessionDetails(objclsLogin.Username, ref Msg, clsImplementationEnum.UserActionType.Login);
                    if (Msg.Length > 0)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = Msg;
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objResponseMsg.Key = true;
                        if (objclsLogin.RememberMe.HasValue && objclsLogin.RememberMe.Value)
                        {
                            HttpCookie uid = new HttpCookie("uid", clsHelper.EncryptString(objclsLogin.Username));
                            // set the cookie's expiration date
                            uid.Expires = DateTime.Now.AddDays(30);
                            // set the cookie on client's browser
                            HttpContext.Response.Cookies.Add(uid);

                            HttpCookie pid = new HttpCookie("pid", clsHelper.EncryptString(objclsLogin.Password));
                            // set the cookie's expiration date
                            pid.Expires = DateTime.Now.AddDays(30);
                            // set the cookie on client's browser
                            HttpContext.Response.Cookies.Add(pid);
                        }
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.AuthenticationMessages.UsetNotExist.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.ExceptionMessage.ToString();
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult LoginMobile(clsLogin objclsLogin)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                bool res = true;

                if (!string.IsNullOrWhiteSpace(Convert.ToString(ConfigurationManager.AppSettings["IsPasswordProtected"])) && Convert.ToString(ConfigurationManager.AppSettings["IsPasswordProtected"]).ToLower().Trim() == "true")
                {
                    //IEMQSImplementation.AuthenticationService.DirectoryService ds = new IEMQSImplementation.AuthenticationService.DirectoryService();
                    //res = ds.getAuthentication(objclsLogin.Username, objclsLogin.Password);
                    PrincipalContext ctx = new PrincipalContext(ContextType.Domain);
                    res = ctx.ValidateCredentials(objclsLogin.Username, objclsLogin.Password);
                }

                if (res)
                {
                    var Msg = "";
                    try
                    {
                        string strReleased = clsImplementationEnum.AppVersionStatus.Released.GetStringValue();
                        //COM003 objCOM003 = db.COM003.Where(x => x.t_psno == objclsLogin.Username && x.t_actv == 1).FirstOrDefault();
                        var objCOM003 = db.SP_GET_LOGIN_DETAILS(objclsLogin.Username).FirstOrDefault();
                        if (!string.IsNullOrEmpty(objCOM003.Msg))
                        {
                            Msg = objCOM003.Msg;
                        }

                        if (Msg.Length == 0)
                        {
                            clsLoginInfo objclsLoginInfo = new clsLoginInfo();
                            objclsLoginInfo.UserName = objCOM003.t_psno;
                            objclsLoginInfo.Name = objCOM003.t_name;
                            objclsLoginInfo.Location = objCOM003.t_loca;
                            objclsLoginInfo.Department = objCOM003.t_depc;
                            objclsLoginInfo.DepartmentName = objCOM003.Department;
                            objclsLoginInfo.LocationName = objCOM003.Location;
                            objclsLoginInfo.Empname = objCOM003.t_name + " " + "(" + objCOM003.t_init + ")";
                            objclsLoginInfo.Initial = objCOM003.t_init;
                            objclsLoginInfo.Emailid = objCOM003.EmailId;
                            objclsLoginInfo.Designation = objCOM003.t_desi;
                            objclsLoginInfo.ListRoles = db.SP_GET_USER_ROLE_NAME_LIST(objCOM003.t_psno).ToList();// objclsLoginInfo.GetUserRoleList();
                            objclsLoginInfo.UserRoles = string.Join(",", objclsLoginInfo.ListRoles);
                            objclsLoginInfo.IsRoleFound = objclsLoginInfo.ListRoles.Count > 0;
                            objclsLoginInfo.Version = objCOM003.AppVersion;
                            var objConfig = db.CONFIG.Where(x => x.Key == "DefaultMenuOrder").Select(i => i.Value).FirstOrDefault();
                            objclsLoginInfo.IsUserMenu = objConfig;
                            objclsLoginInfo.AppVersionId = objclsLoginInfo.AppVersionId;

                            if (objclsLoginInfo.IsUserMenu == "0")
                            {
                                objclsLoginInfo.listMenuResult = db.SP_ATH_INS_DEL_GET_USER_WISE_ACCESS_MENU(objclsLoginInfo.UserName)
                                        .Where(x => x.IsDisplayInMenu == true)
                                        .Where(d =>
                                              d.Process.Equals(MobileAppPermissionsMenuList.PART_OFFER.GetStringValue(), StringComparison.OrdinalIgnoreCase)
                                           || d.Process.Equals(MobileAppPermissionsMenuList.SEAM_OFFER.GetStringValue(), StringComparison.OrdinalIgnoreCase)
                                           || d.Process.Equals(MobileAppPermissionsMenuList.ATTEND_PART.GetStringValue(), StringComparison.OrdinalIgnoreCase)
                                           || d.Process.Equals(MobileAppPermissionsMenuList.ATTEND_SEAM.GetStringValue(), StringComparison.OrdinalIgnoreCase)
                                           || d.Process.Equals(MobileAppPermissionsMenuList.TOKEN_GENERATION.GetStringValue(), StringComparison.OrdinalIgnoreCase)
                                ).ToList();
                            }
                            //Manager.Add(clsImplementationEnum.UserActionType.Login.GetStringValue(), "Login Successful");
                            return Json(new
                            {
                                Key = true,
                                Value = "Logged in Successfully",
                                data = objclsLoginInfo,

                            }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = Msg;
                        }
                    }
                    catch (Exception ex)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
                    }

                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.AuthenticationMessages.UsetNotExist.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.ExceptionMessage.ToString();
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AccessSummary()
        {
            UserProfileController userprofile = new UserProfileController();
            var objResponseMsg = userprofile.GetUserProfileDtlEnt();
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        public ActionResult Logout()
        {
            ExpireAllCookies();
            AuthenticationManager.SignOut(Microsoft.AspNet.Identity.DefaultAuthenticationTypes.ApplicationCookie);
            Session.Abandon();
            return RedirectToAction("Index", "Authenticate");
        }

        private void ExpireAllCookies()
        {
            int cookieCount = HttpContext.Request.Cookies.Count;
            for (var i = 0; i < cookieCount; i++)
            {
                var cookie = HttpContext.Request.Cookies[i];
                if (cookie != null)
                {
                    var expiredCookie = new HttpCookie(cookie.Name)
                    {
                        Expires = DateTime.Now.AddDays(-1),
                        Domain = cookie.Domain
                    };
                    HttpContext.Response.Cookies.Add(expiredCookie); // overwrite it
                }
            }

            // clear cookies server side
            HttpContext.Request.Cookies.Clear();
        }
        // Used in Layout.cshtml
        [HttpPost]
        public JsonResult KeepSessionAlive()
        {
            Session["tempKeepSessionAlive"] = true; // this is use for stay connected
            if (System.Web.HttpContext.Current.Session[clsImplementationMessage.Session.LoginInfo] == null) // session was already expired before renewing
            {
                var ResponseMsg = "Fail to re-connect. Please <u><a href=\"/\">re-login</a></u>";
                return Json(new { Key = false, success = false, Value = ResponseMsg, message = ResponseMsg }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { Key = true, success = true, Value = "", message = "" }, JsonRequestBehavior.AllowGet);
        }

        // Used in clsBase.cs
        public ActionResult SessionTimeOut()
        {
            var ResponseMsg = "System time out and need <u><a href=\"/\">re-login</a></u>";
            return Json(new { Key = false, success = false, Value = ResponseMsg, message = ResponseMsg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AccessDenied()
        {
            return View();
        }
        public JsonResult GuestLogin()
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                COM003 objCOM003 = db.COM003.Where(x => x.t_psno.Trim() == "122963" && x.t_actv == 1).FirstOrDefault();
                Session["userid"] = objCOM003.t_psno;
                clsLoginInfo objclsLoginInfo = new clsLoginInfo();
                objclsLoginInfo.UserName = "122963";
                objclsLoginInfo.Name = objCOM003.t_name;
                objclsLoginInfo.Location = objCOM003.t_loca;
                objclsLoginInfo.Department = objCOM003.t_depc;
                objclsLoginInfo.GetUserRole();
                Session[clsImplementationMessage.Session.LoginInfo] = objclsLoginInfo;
                objResponseMsg.Key = true;
            }
            catch
            {
                objResponseMsg.Value = "Sorry! Your session can't be established";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public bool CheckUserHaveAnyRoles(string PSNO)
        {
            return true;
            if (db.ATH001.Any(i => i.Employee == PSNO && i.Contract.ToLower() != "none"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        [HttpPost]
        public ActionResult GetUsers(string term)
        {
            var lstUsers = new List<CategoryData>();
            if (!string.IsNullOrWhiteSpace(term))
            {

                lstUsers = (from ath1 in db.ATH001
                            join com3 in db.COM003 on ath1.Employee equals com3.t_psno
                            where com3.t_actv == 1
                                  && ((ath1.Employee + " - " + com3.t_name).Contains(term)
                                  || ath1.Employee.Contains(term)
                                  || com3.t_name.Contains(term))
                            select new CategoryData
                            {
                                id = com3.t_psno,
                                text = com3.t_psno + " - " + com3.t_name
                            }).Distinct().Take(10).ToList();
            }
            else
            {
                lstUsers = (from ath1 in db.ATH001
                            join com3 in db.COM003 on ath1.Employee equals com3.t_psno
                            where com3.t_actv == 1
                            select new CategoryData
                            {
                                id = com3.t_psno,
                                text = com3.t_psno + " - " + com3.t_name
                            }).Distinct().Take(10).ToList();
            }
            return Json(lstUsers, JsonRequestBehavior.AllowGet);
        }
    }
}