﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQS.Models;
using System.Data.Entity.Core.Objects;
using System.Threading.Tasks;
using IEMQS.Areas.NDE.Models;

namespace IEMQS.Areas.QCP.Controllers
{
    public class ApproveController : clsBase
    {
        // GET: QCP/Approve

        [SessionExpireFilter, AllowAnonymous, UserPermissions]

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Viewsublines(int id)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("QCP001");
            ViewBag.id = id;
            QCP001 objQCP001 = db.QCP001.FirstOrDefault(x => x.HeaderId == id);
            ViewBag.docNo = objQCP001.DocumentNo;
            ViewBag.btnApprAccess = false;
            NDEModels objNDEModels = new NDEModels();
            objQCP001.TP1 = !string.IsNullOrEmpty(objQCP001.TP1) ? objNDEModels.GetCategory("TPI Agency", objQCP001.TP1, objQCP001.BU, objQCP001.Location, true).CategoryDescription : string.Empty;
            objQCP001.TP2 = !string.IsNullOrEmpty(objQCP001.TP2) ? objNDEModels.GetCategory("TPI Agency", objQCP001.TP2, objQCP001.BU, objQCP001.Location, true).CategoryDescription : string.Empty;
            objQCP001.TP3 = !string.IsNullOrEmpty(objQCP001.TP3) ? objNDEModels.GetCategory("TPI Agency", objQCP001.TP3, objQCP001.BU, objQCP001.Location, true).CategoryDescription : string.Empty;
            objQCP001.TP4 = !string.IsNullOrEmpty(objQCP001.TP4) ? objNDEModels.GetCategory("TPI Agency", objQCP001.TP4, objQCP001.BU, objQCP001.Location, true).CategoryDescription : string.Empty;
            objQCP001.TP5 = !string.IsNullOrEmpty(objQCP001.TP5) ? objNDEModels.GetCategory("TPI Agency", objQCP001.TP5, objQCP001.BU, objQCP001.Location, true).CategoryDescription : string.Empty;
            objQCP001.TP6 = !string.IsNullOrEmpty(objQCP001.TP6) ? objNDEModels.GetCategory("TPI Agency", objQCP001.TP6, objQCP001.BU, objQCP001.Location, true).CategoryDescription : string.Empty;


            if (objQCP001.Status == clsImplementationEnum.QCPStatus.SentForApproval.GetStringValue())
            {
                ViewBag.btnApprAccess = true;
            }

            PurchaseDetail objQCPCommon = new PurchaseDetail();

            string BU = db.COM001.Where(i => i.t_cprj == objQCP001.Project).FirstOrDefault().t_entu;

            var BUDescription = (from a in db.COM002
                                 where a.t_dtyp == 2 && a.t_dimx == BU
                                 select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();

            var contract = db.COM005.Where(i => i.t_sprj == objQCP001.Project).Select(i => i.t_cono).FirstOrDefault();

            var purchaseDet = (from cm004 in db.COM004
                               join cm005 in db.COM005 on cm004.t_cono equals cm005.t_cono
                               where cm005.t_sprj == objQCP001.Project
                               select new { cm004.t_refb, cm004.t_codt, cm004.t_ofbp }).FirstOrDefault();
            objQCP001.Project = db.COM001.Where(i => i.t_cprj == objQCP001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            var location = (from a in db.COM002
                            where a.t_dtyp == 1 && a.t_dimx == objQCP001.Location
                            select a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            objQCP001.Location = location;
            ViewBag.QCPBU = BUDescription.BUDesc;
            ViewBag.purchaseOrderNo = purchaseDet.t_refb;
            ViewBag.purchaseOrderDate = purchaseDet.t_codt.ToString("dd/MM/yyyy hh:mm");
            ViewBag.Customer = db.COM006.Where(i => i.t_bpid.Equals(purchaseDet.t_ofbp, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_nama).FirstOrDefault();//db.COM004.Where(i => i.t_cprj == projectCd).Select(i => i.t_ofbp).FirstOrDefault();

            return View(objQCP001);
        }
        [HttpPost]
        public ActionResult GetHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetQCPHeaderGridDataPartial");
        }
        public ActionResult LoadHeaderData(JQueryDataTableParamModel param, string status)
        {
            string tab = "ALL";
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                var totalRecords = new ObjectParameter("totalRecords", typeof(int));
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                //string SortingFields = param.iSortingCols;



                Func<QCP001, string> orderingFunction = (tsk =>
                                        sortColumnIndex == 2 && isLocationSortable ? Convert.ToString(tsk.Location) : "");
                var sortDirection = Request["sSortDir_0"]; // asc or desc

                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qcp1.BU", "qcp1.Location");
                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and status in('" + clsImplementationEnum.QCPStatus.SentForApproval.GetStringValue() + "')";
                    tab = "PENDING";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or (qcp1.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%' or DocumentNo like '%" + param.sSearch + "%' or QCPRev like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%')";
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstHEADER = db.SP_QCP_GETHEADER
                                (
                                StartIndex, EndIndex, strSortOrder, whereCondition
                                ).ToList();

                int totalRecord = Convert.ToInt32(lstHEADER.Select(x => x.TotalCount).FirstOrDefault());

                var data = (from uc in lstHEADER
                            select new[]

                           {
                           Convert.ToString(uc.HeaderId),
                           //Convert.ToString(uc.Location),
                           Convert.ToString(uc.Project),
                           Convert.ToString(uc.BU),
                           Convert.ToString(uc.DocumentNo),
                           Convert.ToString("R"+uc.QCPRev),
                           Convert.ToString(uc.Status),
                           Convert.ToString(uc.CreatedBy),
                           Convert.ToDateTime( uc.CreatedOn).ToString("dd/MM/yyyy HH:MM"),
                           Convert.ToString(uc.Location),
                           Convert.ToString(uc.HeaderId),
                           Convert.ToString(uc.HeaderId),
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = totalRecord,
                    iTotalDisplayRecords = totalRecord,
                    aaData = data,
                    tab = tab,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = "",
                    tab = tab
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult LoadSublinesData(JQueryDataTableParamModel param, int id)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                var totalRecords = new ObjectParameter("totalRecords", typeof(int));
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                //string SortingFields = param.iSortingCols;
                //readonly= "readonly"
                QCP001 objQCP001 = db.QCP001.FirstOrDefault(x => x.HeaderId == id);

                Func<QCP001, string> orderingFunction = (tsk =>
                                        sortColumnIndex == 2 && isLocationSortable ? Convert.ToString(tsk.Location) : "");
                var sortDirection = Request["sSortDir_0"]; // asc or desc
                string whereCondition = "sl.HeaderId in (" + id + ")";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " and ( l.MainCategory like '%" + param.sSearch + "%' or Activity like '%" + param.sSearch + "%' or ReferenceDoc like '%" + param.sSearch + "%' or Characteristics like '%" + param.sSearch + "%' or AcceptanceCriteria like '%" + param.sSearch + "%'  or VerifyingDoc like '%" + param.sSearch + "%'   or HeaderNotes like '%" + param.sSearch + "%' or FooterNotes like '%" + param.sSearch + "%' or RevisionDesc like '%" + param.sSearch + "%' or Status like '%" + param.sSearch + "%' or sl.CreatedBy like '%" + param.sSearch + "%')";
                }
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstSublines = db.SP_QCP_GETSUBLINES(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                int totalRecord = Convert.ToInt32(lstSublines.Select(x => x.TotalCount).FirstOrDefault());

                var data = (from uc in lstSublines
                            select new[]
                            {
                           Convert.ToString(uc.ROW_NO),
                           GetRemarkStyle(string.Equals(objQCP001.Status, clsImplementationEnum.QCPStatus.SentForApproval.GetStringValue(), StringComparison.OrdinalIgnoreCase) && !string.Equals(uc.Status, clsImplementationEnum.QCPStatus.Deleted.GetStringValue(), StringComparison.OrdinalIgnoreCase),uc.ReturnRemark),
                             Convert.ToString(uc.MainCategory),
                           //Convert.ToString(uc.LineSrNo),
                           Convert.ToString(uc.Activity),
                            Convert.ToString(uc.ReferenceDoc),
                            Convert.ToString(uc.Characteristics),
                            Convert.ToString(uc.AcceptanceCriteria),
                            Convert.ToString(uc.VerifyingDoc),
                            Convert.ToString(uc.InitiatorRemark),
                           Convert.ToString(uc.HeaderNotes),
                           Convert.ToString(uc.FooterNotes),
                           Convert.ToString(uc.TPIInterventionName),
                           Convert.ToString(uc.TPIInterventionName2),
                           Convert.ToString(uc.TPIInterventionName3),
                           Convert.ToString(uc.TPIInterventionName4),
                           Convert.ToString(uc.TPIInterventionName5),
                           Convert.ToString(uc.TPIInterventionName6),
                           Convert.ToString("R"+  uc.SubLineRev),
                           Convert.ToString(uc.Status),
                           // Convert.ToString(uc.RevisionDesc),
                           //Convert.ToString(uc.CreatedBy),
                           //Convert.ToDateTime( uc.CreatedOn).ToString("dd/MM/yyyy HH:MM"),
                           //Convert.ToString("R"+  uc.SubLineRev),
                           //Convert.ToString(uc.Status),
                           // Convert.ToString(uc.TPIInterventionName),
                           Convert.ToString(uc.SubLineId),
                           }).ToList();
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = totalRecord,
                    iTotalDisplayRecords = totalRecord,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]

        public string GetRemarkStyle(bool IsVisible, string returnRemarks)
        {

            if (IsVisible)
            {
                return "<input type='text' name='txtRemark'  class='form-control input-sm input-small input-inline' value='" + returnRemarks + "' />";

            }
            else
            {
                return "<input type='text' name='txtRemark' readonly= 'readonly'  class='form-control input-sm input-small input-inline' value='" + returnRemarks + "' />";
            }
        }
        public async Task<ActionResult> UpdateQCPSubline(int subLineId, string changeText)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objQCP003 = await db.QCP003.FirstOrDefaultAsync(x => x.SubLineId == subLineId);
                if (objQCP003 != null)
                {
                    objQCP003.ReturnRemark = !string.IsNullOrWhiteSpace(changeText) ? changeText : null;
                    objQCP003.ReturnedBy = objClsLoginInfo.UserName;
                    objQCP003.ReturnedOn = DateTime.Now;
                    await db.SaveChangesAsync();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Remarks Updated Successfully";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Line not available";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ApproveQCP(int id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                db.SP_QCP_APPROVE(id, objClsLoginInfo.UserName);

                QCP001 objQCP001 = db.QCP001.FirstOrDefault(c => c.HeaderId == id);
                if (objQCP001 != null)
                {
                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.QC3.GetStringValue() + "," + clsImplementationEnum.UserRoleName.QA3.GetStringValue(), objQCP001.Project, objQCP001.BU, objQCP001.Location, "QCP: " + objQCP001.DocumentNo + " has been Approved", clsImplementationEnum.NotificationType.Information.GetStringValue());
                    #endregion
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.QCPMessages.Approve.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ApproveSelectedQCP(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
                for (int i = 0; i < arrayHeaderIds.Length; i++)
                {
                    int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);

                    db.SP_QCP_APPROVE(HeaderId, objClsLoginInfo.UserName);

                    QCP001 objQCP001 = db.QCP001.FirstOrDefault(c => c.HeaderId == HeaderId);
                    if (objQCP001 != null)
                    {
                        #region Send Notification
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.QC3.GetStringValue() + "," + clsImplementationEnum.UserRoleName.QA3.GetStringValue(), objQCP001.Project, objQCP001.BU, objQCP001.Location, "QCP: " + objQCP001.DocumentNo + " has been Approved", clsImplementationEnum.NotificationType.Information.GetStringValue());
                        #endregion
                    }
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.QCPMessages.Approve.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> ReturnQCP(int id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<QCP003> lstSubline = await (from li in db.QCP003
                                                 where li.HeaderId == id && (li.ReturnRemark != null)
                                                 select li).ToListAsync();
                //foreach (var lst in lstSubline)
                //{
                //    lst.Status = clsImplementationEnum.QCPStatus.Returned.ToString();
                //}
                //await db.SaveChangesAsync();

                if (lstSubline.Any())
                {
                    QCP001 objQCP001 = await db.QCP001.FirstOrDefaultAsync(x => x.HeaderId == id);
                    objQCP001.Status = clsImplementationEnum.QCPStatus.Returned.GetStringValue();
                    await db.SaveChangesAsync();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.QCPMessages.Return.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.QCPMessages.ReturnValidation.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_QCP_GETHEADER(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      DocumentNo = li.DocumentNo,
                                      Status = li.Status,
                                      QCPRev = li.QCPRev,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else
                {
                    var lst = db.SP_QCP_GETSUBLINES(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      LineSrNo = li.LineSrNo,
                                      QCPRev = li.QCPRev,
                                      SubLineSrNo = li.SubLineSrNo,
                                      Activity = li.Activity,
                                      ReferenceDoc = li.ReferenceDoc,
                                      Characteristics = li.Characteristics,
                                      AcceptanceCriteria = li.AcceptanceCriteria,
                                      VerifyingDoc = li.VerifyingDoc,
                                      HeaderNotes = li.HeaderNotes,
                                      FooterNotes = li.FooterNotes,
                                      RevisionDesc = li.RevisionDesc,
                                      InitiatorRemark = li.InitiatorRemark,
                                      ReturnRemark = li.ReturnRemark,
                                      Status = li.Status,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      ReturnedBy = li.ReturnedBy,
                                      ReturnedOn = li.ReturnedOn,
                                      MainCategory = li.MainCategory,
                                      SubLineRev = li.SubLineRev,
                                      TPIIntervention = li.TPIIntervention,
                                      TPIInterventionName = li.TPIInterventionName,
                                      TPIIntervention2 = li.TPIIntervention2,
                                      TPIIntervention3 = li.TPIIntervention3,
                                      TPIIntervention4 = li.TPIIntervention4,
                                      TPIIntervention5 = li.TPIIntervention5,
                                      TPIIntervention6 = li.TPIIntervention6,
                                      TPIInterventionName2 = li.TPIInterventionName2,
                                      TPIInterventionName3 = li.TPIInterventionName3,
                                      TPIInterventionName4 = li.TPIInterventionName4,
                                      TPIInterventionName5 = li.TPIInterventionName5,
                                      TPIInterventionName6 = li.TPIInterventionName6,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
    }
}