﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.PDIN.Controllers
{
    public class DeptConfigController : clsBase
    {
        // GET: PDIN/DeptConfig
        #region Parent Child Department Mapping
        [UserPermissions, SessionExpireFilter,AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }
        [SessionExpireFilter]
        public ActionResult AddParentChildDepartment(int id = 0)
        {
            var objPDN004 = new PDN004();
            if (id > 0)
            {
                objPDN004 = db.PDN004.Where(i => i.Id == id).FirstOrDefault();
                var ParentDept = db.COM002.Where(i => i.t_dtyp == 3 && i.t_dimx == objPDN004.ParentDeptCode).FirstOrDefault();
                if (ParentDept != null)
                {
                    ViewBag.ParentDept = ParentDept.t_dimx + " | " + ParentDept.t_desc;
                }
            }
            else
            {
                var lstParentDepartment = db.COM002.Where(i => i.t_dtyp == 3 && i.t_dimx != string.Empty).Select(i => new BULocWiseCategoryModel { CatDesc = i.t_dimx + " | " + i.t_desc, CatID = i.t_dimx }).ToList();
                ViewBag.lstParentDepartment = lstParentDepartment;
            }

            return PartialView("_AddParentChildDepartment", objPDN004);
        }
        [HttpPost, SessionExpireFilter]
        public JsonResult LoadParentChildDepartment(JQueryDataTableParamModel param)
        {
            try
            {
                IEnumerable<ATH001> _List = new List<ATH001>();
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (c2.t_dimx like '%" + param.sSearch
                        + "%' or c2.t_desc like '%" + param.sSearch
                        + "%' or p4.ChildDeptCode like '%" + param.sSearch
                        + "%' or p4.ChildDesc like '%" + param.sSearch
                        + "%' or p4.ChildInit like '%" + param.sSearch
                        + "%')";

                }
                else
                {
                    strWhere = "1=1";
                }

                #region Sorting
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstSubDepartment = db.SP_PDIN_GET_PARENT_CHILD_DEPARTMENT_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstSubDepartment
                            select new[]
                           {
                           Convert.ToString(uc.Id),
                           Convert.ToString(uc.ParentDeptCode)+" | "+Convert.ToString(uc.ParentDeptDesc),
                           Convert.ToString(uc.ChildDeptCode),
                           Convert.ToString(uc.ChildDesc),
                           Convert.ToString(uc.ChildInit),
                           uc.Active.Value == true ? "Yes" : "No",
                           uc.IsSFC == true ? "Yes" : "No"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = lstSubDepartment != null && lstSubDepartment.Count > 0 ? lstSubDepartment.FirstOrDefault().TotalCount : 0,
                    iTotalDisplayRecords = lstSubDepartment != null && lstSubDepartment.Count > 0 ? lstSubDepartment.FirstOrDefault().TotalCount : 0,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [SessionExpireFilter]
        public ActionResult SaveParentChildDepartment(PDN004 objPDN004)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (objPDN004.Id == 0)
                {
                    if (db.PDN004.Any(i => i.ChildDeptCode == objPDN004.ParentDeptCode + objPDN004.ChildDeptCode && i.Active == true))
                    {
                        objResponseMsg.HeaderId = db.PDN004.Where(i => i.ChildDeptCode == objPDN004.ParentDeptCode + objPDN004.ChildDeptCode && i.Active == true).FirstOrDefault().Id;
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.Message.ToString();
                    }
                    else
                    {
                        PDN004 newPDN004 = new PDN004();
                        newPDN004.Active = objPDN004.Active;
                        newPDN004.IsSFC = objPDN004.IsSFC;
                        newPDN004.ChildDeptCode = objPDN004.ParentDeptCode + objPDN004.ChildDeptCode;
                        newPDN004.ChildDesc = objPDN004.ChildDesc;
                        newPDN004.ChildInit = objPDN004.ChildInit;
                        newPDN004.CreatedBy = objClsLoginInfo.UserName;
                        newPDN004.CreatedOn = DateTime.Now;
                        newPDN004.EditedBy = objClsLoginInfo.UserName;
                        newPDN004.EditedOn = DateTime.Now;
                        newPDN004.ParentDeptCode = objPDN004.ParentDeptCode;
                        db.PDN004.Add(newPDN004);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
                    }
                }
                else
                {
                    if (db.PDN004.Any(i => i.ChildDeptCode == objPDN004.ChildDeptCode && i.Id != objPDN004.Id && i.Active == true))
                    {
                        objResponseMsg.HeaderId = db.PDN004.Where(i => i.ChildDeptCode == objPDN004.ChildDeptCode && i.Id != objPDN004.Id && i.Active == true).FirstOrDefault().Id;
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.Message.ToString();
                    }
                    else
                    {
                        PDN004 updatePDN004 = new PDN004();
                        updatePDN004 = db.PDN004.Where(i => i.Id == objPDN004.Id).FirstOrDefault();
                        updatePDN004.Active = objPDN004.Active;
                        updatePDN004.IsSFC = objPDN004.IsSFC;
                        updatePDN004.ChildDesc = objPDN004.ChildDesc;
                        updatePDN004.ChildInit = objPDN004.ChildInit;
                        updatePDN004.EditedBy = objClsLoginInfo.UserName;
                        updatePDN004.EditedOn = DateTime.Now;
                        updatePDN004.ParentDeptCode = objPDN004.ParentDeptCode;

                        if (!updatePDN004.Active.Value)
                        {
                            foreach (var item in updatePDN004.PDN005)
                            {
                                item.Active = false;
                            }
                        }

                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.HeaderId = 0;
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Sub Department User Mapping
        [UserPermissions, SessionExpireFilter, AllowAnonymous]
        public ActionResult UserSubDepartment()
        {
            return View();
        }
        [SessionExpireFilter]
        public ActionResult AddUserSubDepartment(int id = 0)
        {
            var objPDN005 = new PDN005();
            if (id > 0)
            {
                objPDN005 = db.PDN005.Where(i => i.Id == id).FirstOrDefault();
                var objPDN004 = db.PDN004.Where(i => i.Id == objPDN005.RefId).FirstOrDefault();
                ViewBag.ChildDepartment = objPDN004.ChildDeptCode + " | " + objPDN004.ChildDesc;
                var objCOM002 = db.COM002.Where(i => i.t_dimx == objPDN004.ParentDeptCode).FirstOrDefault();
                ViewBag.ParentDepartment = objCOM002.t_dimx + " | " + objCOM002.t_desc;
                var objCOM003 = db.COM003.Where(i => i.t_psno == objPDN005.Employee).FirstOrDefault();
                ViewBag.EmployeeName = objCOM003.t_psno + " | " + objCOM003.t_name;
            }
            else
            {
                var lstDepartment = (from c2 in db.COM002
                                     join p4 in db.PDN004 on c2.t_dimx equals p4.ParentDeptCode
                                     where p4.Active == true
                                     select new { CatID = c2.t_dimx, CatDesc = c2.t_dimx + " | " + c2.t_desc }
                                   ).Distinct().ToList().Select(i => new BULocWiseCategoryModel { CatID = i.CatID, CatDesc = i.CatDesc }).Distinct().ToList();

                ViewBag.lstDepartment = lstDepartment;
            }
            return PartialView("_AddUserSubDepartment", objPDN005);
        }
        [HttpPost, SessionExpireFilter]
        public JsonResult LoadUserChildDepartment(JQueryDataTableParamModel param)
        {
            try
            {
                IEnumerable<ATH001> _List = new List<ATH001>();
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (c3.t_psno like '%" + param.sSearch
                        + "%' or c3.t_name like '%" + param.sSearch
                        + "%' or p4.ChildDeptCode like '%" + param.sSearch
                        + "%' or p4.ChildDesc like '%" + param.sSearch
                        + "%' or p4.ChildInit like '%" + param.sSearch
                        + "%')";

                }
                else
                {
                    strWhere = "1=1";
                }

                #region Sorting
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstUserSubDepartment = db.SP_PDIN_GET_USER_CHILD_DEPARTMENT_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstUserSubDepartment
                            select new[]
                           {
                           Convert.ToString(uc.Id),
                           Convert.ToString(uc.PSNO) + " - "+Convert.ToString(uc.EmployeeName),
                           Convert.ToString(uc.ChildDeptCode),
                           Convert.ToString(uc.ChildDesc),
                           Convert.ToString(uc.ChildInit),
                           uc.Active.Value == true ? "Yes" : "No",
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = lstUserSubDepartment != null && lstUserSubDepartment.Count > 0 ? lstUserSubDepartment.FirstOrDefault().TotalCount : 0,
                    iTotalDisplayRecords = lstUserSubDepartment != null && lstUserSubDepartment.Count > 0 ? lstUserSubDepartment.FirstOrDefault().TotalCount : 0,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [SessionExpireFilter]
        public ActionResult SaveUserChildDepartment(PDN005 objPDN005)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (objPDN005.Id == 0)
                {
                    if (db.PDN005.Any(i => i.Employee == objPDN005.Employee && i.ChildDeptCode == objPDN005.ChildDeptCode && i.Active == true))
                    {
                        objResponseMsg.HeaderId = db.PDN005.Where(i => i.Employee == objPDN005.Employee && i.Active == true).FirstOrDefault().Id;
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.Message.ToString();
                    }
                    else
                    {
                        PDN005 newPDN005 = new PDN005();
                        newPDN005.Active = objPDN005.Active;
                        newPDN005.ChildDeptCode = objPDN005.ChildDeptCode;
                        newPDN005.Employee = objPDN005.Employee;
                        newPDN005.CreatedBy = objClsLoginInfo.UserName;
                        newPDN005.CreatedOn = DateTime.Now;
                        newPDN005.EditedBy = objClsLoginInfo.UserName;
                        newPDN005.EditedOn = DateTime.Now;
                        newPDN005.RefId = objPDN005.RefId;
                        db.PDN005.Add(newPDN005);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
                    }
                }
                else
                {
                    if (db.PDN005.Any(i => i.Employee == objPDN005.Employee && i.ChildDeptCode == objPDN005.ChildDeptCode && i.Id != objPDN005.Id && i.Active == true))
                    {
                        objResponseMsg.HeaderId = db.PDN005.Where(i => i.Employee == objPDN005.Employee && i.Id != objPDN005.Id && i.Active == true).FirstOrDefault().Id;
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.Message.ToString();
                    }
                    else if (db.PDN004.Any(i => i.Id == objPDN005.RefId && i.Active == false))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Child department is inactive.";
                    }
                    else
                    {
                        PDN005 updatePDN005 = new PDN005();
                        updatePDN005 = db.PDN005.Where(i => i.Id == objPDN005.Id).FirstOrDefault();
                        updatePDN005.Active = objPDN005.Active;
                        updatePDN005.ChildDeptCode = objPDN005.ChildDeptCode;
                        updatePDN005.Employee = objPDN005.Employee;
                        updatePDN005.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [SessionExpireFilter]
        public ActionResult GetSubDepartmentAndEmployeeByParentDepartment(string ParentDeptCode)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            List<BULocWiseCategoryModel> lstChildDepartment = new List<BULocWiseCategoryModel>();
            List<BULocWiseCategoryModel> lstEmployee = new List<BULocWiseCategoryModel>();
            try
            {
                lstChildDepartment = db.PDN004.Where(i => i.ParentDeptCode == ParentDeptCode && i.Active == true).Select(i => new BULocWiseCategoryModel { CatID = i.Id.ToString() + "#" + i.ChildDeptCode, CatDesc = i.ChildDeptCode + " | " + i.ChildDesc }).Distinct().ToList();
                var ExistEmployee = db.PDN005.Where(i => i.Active == true).Select(i => i.Employee).ToArray();
                //lstEmployee = db.COM003.Where(i => i.t_depc == ParentDeptCode && i.t_depc != string.Empty && !ExistEmployee.Contains(i.t_psno)).Select(i => new BULocWiseCategoryModel { CatID = i.t_psno, CatDesc = i.t_psno + " | " + i.t_name }).Distinct().ToList();
                lstEmployee = db.COM003.Where(i => i.t_depc == ParentDeptCode && i.t_depc != string.Empty).Select(i => new BULocWiseCategoryModel { CatID = i.t_psno, CatDesc = i.t_psno + " | " + i.t_name }).Distinct().ToList();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    lstChildDepartment = lstChildDepartment,
                    lstEmployee = lstEmployee,
                }, JsonRequestBehavior.AllowGet);
            }
            return Json(new
            {
                lstChildDepartment = lstChildDepartment,
                lstEmployee = lstEmployee,
            }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Export Excell
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.ParentChildDepartment.GetStringValue())
                {
                    var lst = db.SP_PDIN_GET_PARENT_CHILD_DEPARTMENT_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      ParentDepartment = Convert.ToString(uc.ParentDeptCode) + " | " + Convert.ToString(uc.ParentDeptDesc),
                                      ChildDeptCode = Convert.ToString(uc.ChildDeptCode),
                                      ChildDesc = Convert.ToString(uc.ChildDesc),
                                      ChildInit = Convert.ToString(uc.ChildInit),
                                      Active = uc.Active.Value == true ? "Yes" : "No",
                                      IsSFC = uc.IsSFC == true ? "Yes" : "No"
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.UserChildDepartment.GetStringValue())
                {
                    var lst = db.SP_PDIN_GET_USER_CHILD_DEPARTMENT_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Employee = Convert.ToString(uc.PSNO) + " - " + Convert.ToString(uc.EmployeeName),
                                      ChildDeptCode = Convert.ToString(uc.ChildDeptCode),
                                      ChildDesc = Convert.ToString(uc.ChildDesc),
                                      ChildInit = Convert.ToString(uc.ChildInit),
                                      Active = uc.Active.Value == true ? "Yes" : "No",
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}