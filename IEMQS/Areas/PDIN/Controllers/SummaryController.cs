﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
//using System.Data.Linq.Mapping;

namespace IEMQS.Areas.PDIN.Controllers
{
    public class SummaryController : clsBase
    {
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetHeaderDataPartial(string status)
        {
            ViewBag.Status = status;
            ViewBag.UserDept = objClsLoginInfo.Department;
            return PartialView("_GetHeaderDataPartial");
        }

        [HttpPost]
        public JsonResult LoadDINHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                //if (param.CTQCompileStatus.ToUpper() == "ALL")
                //{
                //    strWhere += "1=1";
                //}
                //else
                //{
                //    strWhere += "1=1";
                //}
                strWhere += "LOWER([Status])='unreleased' ";//and pdn001.CreatedBy=('" + objClsLoginInfo.UserName + "')";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "pdn001.Project+' - '+com1.t_dsca",
                                                "pdn001.Customer+' - '+com6.t_nama",
                                                "[Status]",
                                                "Product",
                                                "ProcessLicensor",
                                                "JobDescription",
                                                "pdn001.CreatedBy +' - '+com003c.t_name",
                                                "pdn001.IssueBy +' - '+com003e.t_name"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                var lstResult = db.SP_FETCH_PLANNINGDIN_HEADERS
                                (
                                StartIndex, EndIndex, "", strWhere
                                ). ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Customer),
                               Convert.ToString(uc.Department),
                               Convert.ToString(uc.CDD),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.IssueNo),
                               Convert.ToString(uc.ProcessLicensor),
                                Convert.ToString(uc.JobDescription),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn),
                               Convert.ToString(uc.IssueBy),
                               Convert.ToString(uc.IssueDate),
                                "<center><button class='btn btn-xs green' type='button' onclick=\"GetReport('"+uc.Project.Split('-')[0].Trim()+"')\">Summary<i style='margin-left:5px;' class='fa fa-eye'></i></a></center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #region Commented
        /*
        [SessionExpireFilter]
        public ActionResult Create(int HeaderID = 0)
        {
            string currentUser = objClsLoginInfo.UserName;
            PDN001 objPDN001 = db.PDN001.Where(x => x.HeaderId == HeaderID && x.CreatedBy == currentUser).FirstOrDefault();
            if (HeaderID > 0 && objPDN001 == null)
            {
                return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
            }
            ViewBag.HeaderID = HeaderID;
            return View(objPDN001);
        }

        [HttpPost]
        public ActionResult LoadHeaderFormPartial(int HeaderID)
        {
            PDN001 objPDN001 = db.PDN001.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
            if (objPDN001 != null && objPDN001.HeaderId > 0)
            {
                var projectDetails = db.COM001.Where(x => x.t_cprj == objPDN001.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                ViewBag.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string customerName = db.COM006.Where(x => x.t_bpid == objPDN001.Customer).Select(x => x.t_nama).FirstOrDefault();
                ViewBag.customerName = objPDN001.Customer + " - " + customerName;
            }
            else
            {
                objPDN001 = new PDN001();
                // int? maxIssueNo = db.PDN001.Select(x => x.IssueNo).FirstOrDefault();
                objPDN001.IssueNo = 1;//(maxIssueNo != null ? Convert.ToInt32(maxIssueNo + 1) : 1);
                objPDN001.Status = clsImplementationEnum.PlanningDinStatus.UNRELEASED.GetStringValue();
            }
            ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            return PartialView("_LoadHeaderFormPartial", objPDN001);
        }

        [HttpPost]
        public JsonResult LoadDINHeaderLineData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "Description",
                                                "[Status]",
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                // strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName);
                var lstResult = db.SP_FETCH_PLANNINGDIN_HEADERS_LINES
                                (
                               StartIndex,
                               EndIndex,
                               "",
                               strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.Description),
                                Convert.ToString(uc.Status),
                                Manager.GenerateLink(uc.PlanName,uc.DocumentNo,Convert.ToInt32(uc.RefId),false),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    isDisplayButton = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? IsDisplaySpecificationButton(lstResult.FirstOrDefault().Project) : false)
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = 0,
                    iTotalDisplayRecords = 0,
                    aaData = "",
                    isDisplayButton = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public bool IsDisplaySpecificationButton(string projectCode)
        {
            bool flage = false;
            TLP001 objTLP001 = db.TLP001.Where(x => x.Project == projectCode).OrderByDescending(x => x.Document).FirstOrDefault();
            if (objTLP001 != null)
            {
                string Document = objTLP001.Document.Trim().ToString();
                int LatestDocument = (Convert.ToInt32(Document.Substring(Document.Length - 2)) + 1);
                if (LatestDocument >= 21 && LatestDocument <= 50)
                {
                    flage = true;
                }
                else { flage = false; }
            }
            else
            {
                flage = true;
            }
            return flage;
        }

        [HttpPost]
        public ActionResult GenerateHeader(PDN001 pdn001)
        {
            ResponceMsgWithValues objResponseMsg = new ResponceMsgWithValues();
            try
            {

                #region Planning Din Header

                PDN001 objPDN001 = db.PDN001.Add(new PDN001
                {
                    Project = pdn001.Project,
                    IssueNo = pdn001.IssueNo,
                    Customer = pdn001.Customer,
                    CDD = pdn001.CDD,
                    Status = clsImplementationEnum.PlanningDinStatus.UNRELEASED.GetStringValue(),
                    Product = pdn001.Product,
                    ProcessLicensor = pdn001.ProcessLicensor,
                    JobDescription = pdn001.JobDescription,
                    Department = pdn001.Department,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                    IssueBy = objClsLoginInfo.UserName, // It's Same as created by and date after discussion with satish
                    IssueDate = DateTime.Now,
                });

                #endregion

                #region JPP

                JPP001 objJPP001 = db.JPP001.Add(new JPP001
                {
                    Project = pdn001.Project,
                    Document = "H02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL01",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                    CDDDate = Helper.ToNullIfTooEarlyForDb(pdn001.CDD),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                });

                #endregion

                #region PMB
                PMB001 objPMB001 = db.PMB001.Add(new PMB001
                {
                    Project = pdn001.Project,
                    Document = "H02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL02",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.FixtureStatus.Draft.GetStringValue(),
                    Product = pdn001.Product,
                    ProcessLicensor = pdn001.ProcessLicensor,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region Improvment Budget
                IMB001 objIMB001 = db.IMB001.Add(new IMB001
                {
                    Project = pdn001.Project,
                    Document = "H02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL03",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.FixtureStatus.Draft.GetStringValue(),
                    Product = pdn001.Product,
                    ProcessLicensor = pdn001.ProcessLicensor,
                    Location = objClsLoginInfo.Location,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region Heat Tratement Charge sheet
                HTC001 objHTC001 = db.HTC001.Add(new HTC001
                {
                    Project = pdn001.Project,
                    Document = "H02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL04",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                    ProcessLicensor = pdn001.ProcessLicensor,
                    Product = pdn001.Product,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                });
                #endregion

                #region SubContracting Plan Header

                SCP001 objSCP001 = db.SCP001.Add(new SCP001
                {
                    Project = pdn001.Project,
                    Document = "H02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL05",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                    ProcessLicensor = pdn001.ProcessLicensor,
                    CDD = Helper.ToNullIfTooEarlyForDb(pdn001.CDD),
                    Product = pdn001.Product,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                });
                #endregion

                #region Planning checklist

                PCL001 objPCL001 = db.PCL001.Add(new PCL001
                {
                    Project = pdn001.Project,
                    Document = "H02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL06",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                    ProcessLicensor = pdn001.ProcessLicensor,
                    ProductType = pdn001.Product,
                    //EquipmentNo=pdn001.EquipmentNo,
                    //ApprovedBy=pdn001.ApprovedBy,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                });
                #endregion

                #region Weld KG
                WKG001 objWKG001 = db.WKG001.Add(new WKG001
                {
                    Project = pdn001.Project,
                    Document = "H02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL07",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    CDD = Helper.ToNullIfTooEarlyForDb(pdn001.CDD),
                    Status = clsImplementationEnum.FixtureStatus.Draft.GetStringValue(),
                    Product = pdn001.Product,
                    ProcessLicensor = pdn001.ProcessLicensor,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region Machining Scope of Work

                MSW001 objMSW001 = db.MSW001.Add(new MSW001
                {
                    Project = pdn001.Project,
                    Document = "H02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL08",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    CDD = Helper.ToNullIfTooEarlyForDb(pdn001.CDD),
                    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                    ProcessLicensor = pdn001.ProcessLicensor,
                    Product = pdn001.Product,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                });
                #endregion

                #region Fixture
                FXR001 objFXR001 = db.FXR001.Add(new FXR001
                {
                    Project = pdn001.Project,
                    Document = "H02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL09",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.FixtureStatus.Draft.GetStringValue(),
                    Product = pdn001.Product,
                    CDD = pdn001.CDD,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region Reference Sketch

                PLN003 objPLN003 = db.PLN003.Add(new PLN003
                {
                    Project = pdn001.Project,
                    Document = "H02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL10",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                    CDD = Helper.ToNullIfTooEarlyForDb(pdn001.CDD),
                    Product = pdn001.Product,
                    ProcessLicensor = pdn001.ProcessLicensor,
                    ProcessPlan = clsImplementationEnum.PlanList.Reference_Sketch.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                });

                #endregion

                #region Locking Cleat

                PLN019 objPLN019 = db.PLN019.Add(new PLN019
                {
                    Project = pdn001.Project,
                    Document = "H02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL11",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                    CDD = Helper.ToNullIfTooEarlyForDb(pdn001.CDD),
                    // JointType = pdn001.JointType,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                });

                #endregion

                #region RCCP

                //PLN013 objPLN013 = db.PLN013.Add(new PLN013
                //{
                //    Project = pdn001.Project,
                //    Document = "H02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL14",
                //    Customer = pdn001.Customer,
                //    RevNo = 0,
                //    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                //    CDD = Helper.ToNullIfTooEarlyForDb(pdn001.CDD),
                //    Product = pdn001.Product,
                //    ProcessLicensor = pdn001.ProcessLicensor,
                //    ProcessPlan = clsImplementationEnum.PlanList.Reference_Sketch.GetStringValue(),
                //    CreatedBy = objClsLoginInfo.UserName,
                //    CreatedOn = DateTime.Now,
                //});

                #endregion

                #region Tank Rotator

                PLN005 objPLN005 = db.PLN005.Add(new PLN005
                {
                    Project = pdn001.Project,
                    Document = "H02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL13",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                    CDD = Helper.ToNullIfTooEarlyForDb(pdn001.CDD),
                    Product = pdn001.Product,
                    ProcessLicensor = pdn001.ProcessLicensor,
                    ProcessPlan = clsImplementationEnum.PlanList.Tank_Rotator_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                });


                #endregion

                #region Positioner Plan

                PLN013 objPLN013 = db.PLN013.Add(new PLN013
                {
                    Project = pdn001.Project,
                    Document = "H02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL14",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                    CDD = Helper.ToNullIfTooEarlyForDb(pdn001.CDD),
                    Product = pdn001.Product,
                    ProcessLicensor = pdn001.ProcessLicensor,
                    ProcessPlan = clsImplementationEnum.PlanList.Positioner_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                });

                #endregion

                #region Stool Plan

                PLN017 objPLN017 = db.PLN017.Add(new PLN017
                {
                    Project = pdn001.Project,
                    Document = "H02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL15",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                    CDD = Helper.ToNullIfTooEarlyForDb(pdn001.CDD),
                    Product = pdn001.Product,
                    ProcessLicensor = pdn001.ProcessLicensor,
                    ProcessPlan = clsImplementationEnum.PlanList.Stool_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                });

                #endregion

                #region Section Handling

                PLN009 objPLN009 = db.PLN009.Add(new PLN009
                {
                    Project = pdn001.Project,
                    Document = "H02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL16",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                    CDD = Helper.ToNullIfTooEarlyForDb(pdn001.CDD),
                    Product = pdn001.Product,
                    ProcessLicensor = pdn001.ProcessLicensor,
                    ProcessPlan = clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                });

                #endregion

                #region Internal Axel Movement

                PLN018 objPLN018 = db.PLN018.Add(new PLN018
                {
                    Project = pdn001.Project,
                    Document = "H02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL17",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                    CDD = Helper.ToNullIfTooEarlyForDb(pdn001.CDD),
                    Product = pdn001.Product,
                    ProcessLicensor = pdn001.ProcessLicensor,
                    ProcessPlan = clsImplementationEnum.PlanList.Internal_Axel_Movement_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                });

                #endregion

                #region Technical Process

                int docNo;
                var projectExists = db.TLP001.Where(x => x.Project.Trim() == pdn001.Project.Trim()).FirstOrDefault();
                if (projectExists == null)
                {
                    docNo = 21;
                }
                else
                {
                    int? existingDocNo = (from doc in db.TLP001
                                          where doc.Project == pdn001.Project
                                          select doc.DocumentNo).FirstOrDefault();
                    docNo = Convert.ToInt32(existingDocNo + 1);
                }

                TLP001 objTLP001 = db.TLP001.Add(new TLP001
                {
                    Project = pdn001.Project,
                    Document = "H02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL" + docNo,
                    DocumentNo = docNo,
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    CDD = Helper.ToNullIfTooEarlyForDb(pdn001.CDD),
                    JobDescription = pdn001.JobDescription,
                    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                    ProcessLicensor = pdn001.ProcessLicensor,
                    Product = pdn001.Product,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                });
                #endregion

                #region Equipment Plan

                PLN011 objPLN011 = db.PLN011.Add(new PLN011
                {
                    Project = pdn001.Project,
                    Document = "H02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL51",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                    CDD = Helper.ToNullIfTooEarlyForDb(pdn001.CDD),
                    Product = pdn001.Product,
                    ProcessLicensor = pdn001.ProcessLicensor,
                    ProcessPlan = clsImplementationEnum.PlanList.Equipment_Handling_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                });

                #endregion

                #region Equipment Jacking

                PLN016 objPLN016 = db.PLN016.Add(new PLN016
                {
                    Project = pdn001.Project,
                    Document = "H02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL52",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                    CDD = Helper.ToNullIfTooEarlyForDb(pdn001.CDD),
                    Product = pdn001.Product,
                    ProcessLicensor = pdn001.ProcessLicensor,
                    ProcessPlan = clsImplementationEnum.PlanList.Equipment_Jacking_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                });

                #endregion

                #region Airtest Plan

                PLN015 objPLN015 = db.PLN015.Add(new PLN015
                {
                    Project = pdn001.Project,
                    Document = "H02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL53",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                    CDD = Helper.ToNullIfTooEarlyForDb(pdn001.CDD),
                    Product = pdn001.Product,
                    ProcessLicensor = pdn001.ProcessLicensor,
                    ProcessPlan = clsImplementationEnum.PlanList.Air_Test_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                });

                #endregion

                #region Hydro Test

                PLN007 objPLN007 = db.PLN007.Add(new PLN007
                {
                    Project = pdn001.Project,
                    Document = "H02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL54",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                    CDD = Helper.ToNullIfTooEarlyForDb(pdn001.CDD),
                    Product = pdn001.Product,
                    ProcessLicensor = pdn001.ProcessLicensor,
                    ProcessPlan = clsImplementationEnum.PlanList.Hydro_Test_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                });

                #endregion

                #region Initiate Exit Meeting

                EMT001 objEMT001 = db.EMT001.Add(new EMT001
                {
                    Project = pdn001.Project,
                    Document = "H02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL55",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    CDD = Helper.ToNullIfTooEarlyForDb(pdn001.CDD),
                    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                    ProcessLicensor = pdn001.ProcessLicensor,
                    Product = pdn001.Product,
                    //ApprovedBy=pdn001.ApprovedBy,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                });
                #endregion

                #region Learning Capture
                LNC001 objLNC001 = db.LNC001.Add(new LNC001
                {
                    Project = pdn001.Project,
                    DocumentNo = "H02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL56",
                    Customer = pdn001.Customer,
                    CDD = Convert.ToDateTime(Helper.ToNullIfTooEarlyForDb(pdn001.CDD)),
                    Product = pdn001.Product,
                    ProcessLicensor = pdn001.ProcessLicensor,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedDate = DateTime.Now
                });
                #endregion

                db.SaveChanges();

                PDN002 objPDN002 = new PDN002();

                #region JPP

                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Job Planning & Progress Sheet & Line Sketch",
                    DocumentNo = objJPP001.Document,
                    TargetDate = null,
                    RevNo = objJPP001.RevNo,
                    RefId = objJPP001.HeaderId,
                    Status = objJPP001.Status,
                    Plan = clsImplementationEnum.PlanList.JPP.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region PMB

                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Manufacturing Sequence & Schedule for Equipment",
                    DocumentNo = objPMB001.Document,
                    TargetDate = null,
                    RevNo = objPMB001.RevNo,
                    RefId = objPMB001.HeaderId,
                    Status = objPMB001.Status,
                    Plan = clsImplementationEnum.PlanList.Pre_Manufacturing.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region IMB

                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Improvement Budget Plan",
                    DocumentNo = objIMB001.Document,
                    TargetDate = null,
                    RevNo = objIMB001.RevNo,
                    RefId = objIMB001.HeaderId,
                    Status = objIMB001.Status,
                    Plan = clsImplementationEnum.PlanList.Improvement_Budget_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region Heat Tratement Charge sheet

                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Heat Treatment Charge Sheet",
                    DocumentNo = objHTC001.Document,
                    TargetDate = null,
                    RevNo = objHTC001.RevNo,
                    RefId = objHTC001.HeaderId,
                    Status = objHTC001.Status,
                    Plan = clsImplementationEnum.PlanList.Heat_Treatment_Charge_Sheet.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region SubContracting Plan Header
                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "SubContracting Plan",
                    DocumentNo = objSCP001.Document,
                    TargetDate = null,
                    RevNo = objSCP001.RevNo,
                    RefId = objSCP001.HeaderId,
                    Status = objSCP001.Status,
                    Plan = clsImplementationEnum.PlanList.Sub_Contracting_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region Planning checklist

                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Planning Checklist",
                    DocumentNo = objPCL001.Document,
                    TargetDate = null,
                    RevNo = objPCL001.RevNo,
                    RefId = objPCL001.HeaderId,
                    Status = objPCL001.Status,
                    Plan = clsImplementationEnum.PlanList.Planning_Checklist.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region WKG

                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Weld Kg Calculation",
                    DocumentNo = objWKG001.Document,
                    TargetDate = null,
                    RevNo = objWKG001.RevNo,
                    RefId = objWKG001.HeaderId,
                    Status = objWKG001.Status,
                    Plan = clsImplementationEnum.PlanList.Weld_KG_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region Machining Scope of Work

                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Machining Scope of Work",
                    DocumentNo = objMSW001.Document,
                    TargetDate = null,
                    RevNo = objMSW001.RevNo,
                    RefId = objMSW001.HeaderId,
                    Status = objMSW001.Status,
                    Plan = clsImplementationEnum.PlanList.Machining_Scope_of_Work.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region Fixture

                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Fixture Requirement Sheet",
                    DocumentNo = objFXR001.Document,
                    TargetDate = null,
                    RevNo = objFXR001.RevNo,
                    RefId = objFXR001.HeaderId,
                    Status = objFXR001.Status,
                    Plan = clsImplementationEnum.PlanList.Fixture.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region Reference Sketch

                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Reference Line Sketch",
                    DocumentNo = objPLN003.Document,
                    TargetDate = null,
                    RevNo = objPLN003.RevNo,
                    RefId = objPLN003.HeaderId,
                    Status = objPLN003.Status,
                    Plan = clsImplementationEnum.PlanList.Reference_Sketch.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region Locking Cleat

                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Locking Cleat /Tacking Plan",
                    DocumentNo = objPLN019.Document,
                    TargetDate = null,
                    RevNo = objPLN019.RevNo,
                    RefId = objPLN019.HeaderId,
                    Status = objPLN019.Status,
                    Plan = clsImplementationEnum.PlanList.Locking_Cleat.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region RCCP

                //objPDN002 = db.PDN002.Add(new PDN002
                //{
                //    HeaderId = objPDN001.HeaderId,
                //    Project = objPDN001.Project,
                //    IssueNo = objPDN001.IssueNo,
                //    Description = "Locking Cleat Plan",
                //    DocumentNo = objPLN019.Document,
                //    TargetDate = null,
                //    RevNo = objPLN019.RevNo,
                //    RefId = objPLN019.HeaderId,
                //    Status = objPLN019.Status,
                //    Plan = clsImplementationEnum.PlanList.Locking_Cleat.GetStringValue(),
                //    CreatedBy = objClsLoginInfo.UserName,
                //    CreatedOn = DateTime.Now
                //});

                #endregion

                #region Tank Rotator

                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Tank Rotator Plan",
                    DocumentNo = objPLN005.Document,
                    TargetDate = null,
                    RevNo = objPLN005.RevNo,
                    RefId = objPLN005.HeaderId,
                    Status = objPLN005.Status,
                    Plan = clsImplementationEnum.PlanList.Tank_Rotator_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region Positioner Plan

                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Positioner Plan",
                    DocumentNo = objPLN013.Document,
                    TargetDate = null,
                    RevNo = objPLN013.RevNo,
                    RefId = objPLN013.HeaderId,
                    Status = objPLN013.Status,
                    Plan = clsImplementationEnum.PlanList.Positioner_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region Stool Plan

                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Stool Plan",
                    DocumentNo = objPLN017.Document,
                    TargetDate = null,
                    RevNo = objPLN017.RevNo,
                    RefId = objPLN017.HeaderId,
                    Status = objPLN017.Status,
                    Plan = clsImplementationEnum.PlanList.Stool_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region Section Handling

                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Section Handling Plan",
                    DocumentNo = objPLN009.Document,
                    TargetDate = null,
                    RevNo = objPLN009.RevNo,
                    RefId = objPLN009.HeaderId,
                    Status = objPLN009.Status,
                    Plan = clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region Internal Axel Movement

                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Internal Axel Movement Plan",
                    DocumentNo = objPLN018.Document,
                    TargetDate = null,
                    RevNo = objPLN018.RevNo,
                    RefId = objPLN018.HeaderId,
                    Status = objPLN018.Status,
                    Plan = clsImplementationEnum.PlanList.Internal_Axel_Movement_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region Technical Procedure 

                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Technical Procedure",
                    DocumentNo = objTLP001.Document,
                    TargetDate = null,
                    RevNo = objTLP001.RevNo,
                    RefId = objTLP001.HeaderId,
                    Status = objTLP001.Status,
                    Plan = clsImplementationEnum.PlanList.Technical_Process.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region Equipment Plan

                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Equipment Handling Plan",
                    DocumentNo = objPLN011.Document,
                    TargetDate = null,
                    RevNo = objPLN011.RevNo,
                    RefId = objPLN011.HeaderId,
                    Status = objPLN011.Status,
                    Plan = clsImplementationEnum.PlanList.Equipment_Handling_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region Equipment Jacking

                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Equipment Jacking Plan",
                    DocumentNo = objPLN016.Document,
                    TargetDate = null,
                    RevNo = objPLN016.RevNo,
                    RefId = objPLN016.HeaderId,
                    Status = objPLN016.Status,
                    Plan = clsImplementationEnum.PlanList.Equipment_Jacking_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region Airtest Plan

                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Air Test Plan (Bolting, Air Filling, Support)",
                    DocumentNo = objPLN015.Document,
                    TargetDate = null,
                    RevNo = objPLN015.RevNo,
                    RefId = objPLN015.HeaderId,
                    Status = objPLN015.Status,
                    Plan = clsImplementationEnum.PlanList.Air_Test_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region Hydro Test

                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Hydro Test Plan (Bolting, Water Filling, Support, Chemicals)",
                    DocumentNo = objPLN007.Document,
                    TargetDate = null,
                    RevNo = objPLN007.RevNo,
                    RefId = objPLN007.HeaderId,
                    Status = objPLN007.Status,
                    Plan = clsImplementationEnum.PlanList.Hydro_Test_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region Initiate Exit Meeting
                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Job Closing Summary",
                    DocumentNo = objEMT001.Document,
                    TargetDate = null,
                    RevNo = objEMT001.RevNo,
                    RefId = objEMT001.HeaderId,
                    Status = objEMT001.Status,
                    Plan = clsImplementationEnum.PlanList.Initiate_Exit_Meeting.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region LNC
                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Learning from Project",
                    DocumentNo = objLNC001.DocumentNo,
                    TargetDate = null,
                    RefId = objLNC001.HeaderId,
                    Plan = clsImplementationEnum.PlanList.Learning_Capture.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Planning DIN created successfully.";
                objResponseMsg.HeaderID = objPDN001.HeaderId;
                objResponseMsg.Status = objPDN001.Status;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetJobSpecificationFormPartial(string projectCode)
        {
            List<TLP001> lstTLP001 = db.TLP001.Where(x => x.Project == projectCode).OrderByDescending(x => x.Document).ToList();
            if (lstTLP001 != null && lstTLP001.Count > 0)
            {
                string LatestDocument = lstTLP001.Select(x => x.Document).FirstOrDefault();
                ViewBag.Document = "H02_" + projectCode.Trim().Substring(projectCode.Trim().Length - 4) + "_PL" + (Convert.ToInt32(LatestDocument.Trim().Substring(LatestDocument.Trim().Length - 2)) + 1);
            }
            else
            {
                ViewBag.Document = "H02_" + projectCode.Trim().Substring(projectCode.Trim().Length - 4) + "_PL21";
            }
            return PartialView("_GetJobSpecificationFormPartial");
        }

        [HttpPost]
        public ActionResult SaveTPDetails(TLP001 tlp001)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PDN001 objPDN001 = db.PDN001.Where(x => x.HeaderId == tlp001.HeaderId).FirstOrDefault();
                if (objPDN001 != null)
                {
                    TLP001 objTLP001 = db.TLP001.Add(new TLP001
                    {
                        Project = objPDN001.Project,
                        DocumentNo = Convert.ToInt32(tlp001.Document.Trim().Substring(tlp001.Document.Trim().Length - 2)),
                        Document = tlp001.Document.Trim(),
                        Customer = objPDN001.Customer,
                        RevNo = 0,
                        Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                        CDD = objPDN001.CDD,
                        Product = objPDN001.Product,
                        ProcessLicensor = objPDN001.ProcessLicensor,
                        JobDescription = objPDN001.JobDescription,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });

                    db.SaveChanges();

                    PDN002 objPDN002 = db.PDN002.Add(new PDN002
                    {
                        HeaderId = objPDN001.HeaderId,
                        Project = objPDN001.Project,
                        IssueNo = objPDN001.IssueNo,
                        Description = tlp001.JobDescription,
                        DocumentNo = objTLP001.Document,
                        RevNo = 0,
                        RefId = objTLP001.HeaderId,
                        Plan = "Technical Procedure",
                        Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Job specification added successfully.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Planning Din header details not available.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetCustomerProjectWise(string projectCode)
        {
            ApproverModel objApproverModel = Manager.GetCustomerProjectWise(projectCode);
            string strCdd = Manager.GetContractWiseCdd(projectCode);
            if (!string.IsNullOrWhiteSpace(strCdd))
            {
                DateTime dtCdd = Convert.ToDateTime(strCdd);
                objApproverModel.Cdd = dtCdd.Date.ToShortDateString();
            }
            return Json(objApproverModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public bool checkProjectExist(string projectcode)
        {
            bool Flag = false;
            if (!string.IsNullOrWhiteSpace(projectcode))
            {
                PDN001 objPDN001 = db.PDN001.Where(x => x.Project == projectcode).FirstOrDefault();
                if (objPDN001 != null)
                {
                    Flag = true;
                }
            }
            return Flag;
        }

        public JsonResult GetDepartments(string search)
        {
            try
            {
                if (search == null)
                {
                    search = "";
                }
                var items = db.FN_GET_LOCATIONWISE_DEPARTMENT(objClsLoginInfo.Location, search).Select(x => new { id = x.t_dimx, text = x.t_dimx + "-" + x.t_desc, }).ToList();
                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetDepartmentsValue(int headerId)
        {
            try
            {
                string strDepartments = (db.PDN001.Where(x => x.HeaderId == headerId).Select(x => x.Department).FirstOrDefault());
                string[] arrayDepartments = null;

                if (!string.IsNullOrWhiteSpace(strDepartments))
                {
                    arrayDepartments = strDepartments.Split(',');
                }
                return Json(arrayDepartments, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        */
        #endregion
    }



}