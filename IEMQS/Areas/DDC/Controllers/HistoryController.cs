﻿using IEMQS.Areas.MSW.Controllers;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsHelper;

namespace IEMQS.Areas.DDC.Controllers
{
    public class HistoryController : clsBase
    {
        // GET: DDC/History   
        public ActionResult Index()
        {
            return View();
        }
        #region history
        [HttpPost]
        public ActionResult GetHistoryDetails(string strRole, int HeaderId)
        {
            DDC001_Log objLog = new DDC001_Log();
            ViewBag.action = strRole;
            ViewBag.HeaderId = HeaderId;
            objLog = db.DDC001_Log.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            return PartialView("_LoadHistoryDetail", objLog);
        }
        //datatable function for header
        [HttpPost]
        public JsonResult LoadDDCHeaderHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                strWhere += "1=1 and HeaderId=" + param.Headerid;
                #region sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion
                string action = param.CTQCompileStatus.ToUpper();
                #region searching
                string[] columnName = { "com1.t_dsca", "Project", "Document", "Customer", "RevNo", "Product", "ProcessLicensor", "Status" };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                #endregion

                var lstResult = db.SP_DDC_GET_HEADER_HISTORY_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.Project),
                                Convert.ToString(uc.Document),
                                Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Project)),
                                Convert.ToString(uc.Product),
                                Convert.ToString(uc.ProcessLicensor),
                                Convert.ToString("R"+uc.RevNo),
                                Convert.ToString(uc.Status),
                                Convert.ToString(uc.SubmittedBy),
                                uc.SubmittedOn == null || uc.SubmittedOn.Value==DateTime.MinValue? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture),
                                Convert.ToString(uc.ApprovedBy),
                                uc.ApprovedOn == null || uc.ApprovedOn.Value==DateTime.MinValue? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture),
                                "<center><a href=\""+WebsiteURL+"/DDC/History/ViewLogDetail?Id=" + uc.Id +"&&action="+action+"\"><i class=\"iconspace fa fa-eye\"></i></a> <i class=\"iconspace fa fa-clock-o\" title=\"Show Timeline\"  onclick=ShowTimeline('/DDC/History/ShowHistoryTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')></i></center>",
                          }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [SessionExpireFilter]
        public ActionResult ViewLogDetail(int id, string action)
        {
            ViewBag.Page = action;
            DDC001_Log objDDC001 = new DDC001_Log();

            if (id > 0)
            {
                objDDC001 = db.DDC001_Log.Where(x => x.Id == id).FirstOrDefault();
                ViewBag.Project = db.COM001.Where(i => i.t_cprj == objDDC001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault(); ;
                if (objDDC001.ApprovedBy != null)
                {
                    ViewBag.ApproverName = db.COM003.Where(x => x.t_psno == objDDC001.ApprovedBy && x.t_actv == 1).Select(x => objDDC001.ApprovedBy + "-" + x.t_name).FirstOrDefault();
                }
                else { ViewBag.ApproverName = ""; }
                ViewBag.Customer = Manager.GetCustomerCodeAndNameByProject(objDDC001.Project);
            }

            return View(objDDC001);
        }

        //show history timeline
        public ActionResult ShowHistoryTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();

            model.TimelineTitle = "Dimension Data Capturing Plan Timeline";
            model.Title = "PDinDoc";
            if (HeaderId > 0)
            {
                DDC001_Log objDDC001_Log = db.DDC001_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objDDC001_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objDDC001_Log.CreatedBy) : null;
                model.CreatedOn = objDDC001_Log.CreatedOn;
                model.EditedBy = objDDC001_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(objDDC001_Log.EditedBy) : null;
                model.EditedOn = objDDC001_Log.EditedOn;
                model.SubmittedBy = objDDC001_Log.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objDDC001_Log.SubmittedBy) : null;
                model.SubmittedOn = objDDC001_Log.SubmittedOn;

                model.ApprovedBy = objDDC001_Log.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objDDC001_Log.ApprovedBy) : null;
                model.ApprovedOn = objDDC001_Log.ApprovedOn;

            }
            else
            {

                DDC001_Log objDDC001_Log = db.DDC001_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objDDC001_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objDDC001_Log.CreatedBy) : null;
                model.CreatedOn = objDDC001_Log.CreatedOn;
                model.EditedBy = objDDC001_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(objDDC001_Log.EditedBy) : null;
                model.EditedOn = objDDC001_Log.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        #endregion


        #region Export Excel
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_DDC_GET_HEADER_HISTORY_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      Document = li.Document,
                                      Customer = li.Customer,
                                      Product = li.Product,
                                      ProcessLicensor = li.ProcessLicensor,
                                      Status = li.Status,
                                      RevNo = "R" + li.RevNo,
                                      SubmittedBy = li.SubmittedBy,
                                      SubmittedOn = li.SubmittedOn,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion     
    }
}