﻿using IEMQS.Areas.HTR.Models;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Microsoft.Reporting.WebForms;
using ReportViewerForMvc;
using System.Web.UI.WebControls;
using System.Web.Services.Protocols;
using System.IO;
using System.Collections;
using IEMQSImplementation.Models;
using IEMQS.Areas.Utility.Controllers;

namespace IEMQS.Areas.TEMP.Controllers
{
    public class ReportController : clsBase
    {
        // GET: TEMP/Report
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult TestPowerBI()
        {

            return View();
        }
        public ActionResult AttachReportInEmail()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ExportToPDF(string ReportPath, List<SSRSParam> reportParams)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                Warning[] warnings = null;
                string[] streamIds;
                string mimeType = string.Empty;
                string encoding = string.Empty;
                string extension = string.Empty;

                var ReportParams = new List<ReportParameter>();
                var param = new Dictionary<string, string[]>();
                foreach (var rp in reportParams)
                {
                    if (param.Any(q => q.Key.Equals(rp.Param)))
                    {
                        var oldValue = param.FirstOrDefault(q => q.Key.Equals(rp.Param)).Value.ToList();
                        param.Remove(rp.Param);
                        oldValue.Add(rp.Value);
                        param.Add(rp.Param, oldValue.ToArray());
                    }
                    else
                        param.Add(rp.Param, new string[] { rp.Value });
                }
                foreach (var p in param)
                    ReportParams.Add(new ReportParameter(p.Key, p.Value));

                // Setup the report viewer object and get the array of bytes

                var viewer = new ReportViewer();
                viewer = (new GeneralController()).GetReportServerCreadential(viewer, ReportPath, ReportParams);

                byte[] bytes = viewer.ServerReport.Render("PDF", null, out mimeType, out encoding, out extension,
                              out streamIds, out warnings);
                string fileName1 = "HTR_Report_" + objClsLoginInfo.UserName;
                //create file
                string fullPath = Path.Combine(Server.MapPath("~/Resources/Download/"), fileName1 + ".pdf");
                using (FileStream fs = new FileStream(fullPath, FileMode.Create))
                { fs.Write(bytes, 0, bytes.Length); }

                objResponseMsg.Key = true;
                string reportFilename = fullPath;

                string emailTo = objClsLoginInfo.UserName;
                string cc1 = objClsLoginInfo.UserName;
                string cc2 = objClsLoginInfo.UserName;
                #region Send Mail
                if (!string.IsNullOrWhiteSpace(emailTo))
                {
                    Hashtable _ht = new Hashtable();
                    EmailSend _objEmail = new EmailSend();
                    _objEmail.MailCc = Manager.GetMailIdFromPsNo(cc1) + ";" + Manager.GetMailIdFromPsNo(cc2);
                    _ht["[Approver]"] = Manager.GetUserNameFromPsNo(emailTo);
                    _ht["[FDMSNo]"] = "Test";
                    _ht["[Initiator]"] = Manager.GetUserNameFromPsNo(emailTo);
                    _ht["[FromState]"] = clsImplementationEnum.FDMSStatus.DRAFT.GetStringValue();
                    _ht["[ToState]"] = clsImplementationEnum.FDMSStatus.DocVeri.GetStringValue();
                    _ht["[FDMSLink]"] = WebsiteURL + "/FDMS/FDMS/CreateFDMs?Id=" + 1;
                    MAIL001 objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.FDMS.PromoteFDMS).SingleOrDefault();
                    _objEmail.MailToAdd = Manager.GetMailIdFromPsNo(emailTo); //initiator
                    _ht["[Subject]"] = "Test Attachment";
                    _objEmail.MailAttachment = reportFilename;
                    _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                }
                #endregion
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Mail Sent";
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult SendMailWithAttachment(MailConfiguration objemail, string ReportPath, List<SSRSParam> reportParams)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                Warning[] warnings = null;
                string[] streamIds;
                string mimeType = string.Empty;
                string encoding = string.Empty;
                string extension = string.Empty;

                var ReportParams = new List<ReportParameter>();
                var param = new Dictionary<string, string[]>();
                foreach (var rp in reportParams)
                {
                    if (param.Any(q => q.Key.Equals(rp.Param)))
                    {
                        var oldValue = param.FirstOrDefault(q => q.Key.Equals(rp.Param)).Value.ToList();
                        param.Remove(rp.Param);
                        oldValue.Add(rp.Value);
                        param.Add(rp.Param, oldValue.ToArray());
                    }
                    else
                        param.Add(rp.Param, new string[] { rp.Value });
                }
                foreach (var p in param)
                    ReportParams.Add(new ReportParameter(p.Key, p.Value));

                // Setup the report viewer object and get the array of bytes
                var viewer = new ReportViewer();
                viewer = (new GeneralController()).GetReportServerCreadential(viewer, ReportPath, ReportParams);

                byte[] bytes = viewer.ServerReport.Render("PDF", null, out mimeType, out encoding, out extension,
                              out streamIds, out warnings);
                string fileName1 = "NCR_Report_" + objClsLoginInfo.UserName;
                //create file
                string fullPath = Path.Combine(Server.MapPath("~/Resources/Download/"), fileName1 + ".pdf");
                if (Directory.Exists(Path.GetDirectoryName(fullPath)))
                {
                    System.IO.File.Delete(fullPath);
                }
                using (FileStream fs = new FileStream(fullPath, FileMode.Create))
                { fs.Write(bytes, 0, bytes.Length); }

                objResponseMsg.Key = true;
                string reportFilename = fullPath;

                string emailTo = objClsLoginInfo.UserName;
                string cc1 = objClsLoginInfo.UserName;
                string cc2 = objClsLoginInfo.UserName;
                #region Send Mail
                if (!string.IsNullOrWhiteSpace(emailTo))
                {
                    Hashtable _ht = new Hashtable();
                    EmailSend _objEmail = new EmailSend();
                    _objEmail.MailCc = objemail.MAIL001.CCEmail;
                    _objEmail.MailSub = objemail.MAIL001.Subject;
                    _objEmail.MailBcc = objemail.MAIL001.BCCEmail;
                    _objEmail.MailBody = objemail.ContentBody;
                    //  MAIL001 objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.FDMS.PromoteFDMS).SingleOrDefault();
                    _objEmail.MailToAdd = objemail.TemplateName; //initiator
                    _ht["[Subject]"] = objemail.MAIL001.Subject;
                    _objEmail.MailAttachment = reportFilename;
                    _objEmail.SendMail(_objEmail, _ht, objemail.MAIL001);
                }
                #endregion
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Mail Sent";
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
    }
}