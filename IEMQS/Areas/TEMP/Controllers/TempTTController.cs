﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace IEMQS.Areas.TEMP.Controllers
{
    public class TempTTController : clsBase
    {
        // GET: TEMP/TempTT
        // static List<SearchFilter> selectedFilter = new List<SearchFilter>();
        public ActionResult Index(int? id)
        {
            CTQ001 objCTQ001 = new CTQ001();
            var user = objClsLoginInfo.UserName;
            var project = Manager.getProjectsByUser(user);
            ViewBag.Project = new SelectList(project, "projectCode", "projectDescription");
            //header id for view mode
            if (id != null)
            {
                var objHeader = Convert.ToInt32(id);
                string draftStatus = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                string returnedStatus = clsImplementationEnum.CTQStatus.Returned.GetStringValue();

                objCTQ001 = db.CTQ001.Where(x => x.HeaderId == objHeader).FirstOrDefault();
                objCTQ001.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objCTQ001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objCTQ001.Project = db.COM001.Where(i => i.t_cprj == objCTQ001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objCTQ001.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objCTQ001.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                ViewBag.Project = objCTQ001.Project;
                ViewBag.Action = "edit";
            }
            else
            {
                objCTQ001.Status = "Draft";
                objCTQ001.CTQRev = 0;
                var location = (from a in db.COM003
                                join b in db.COM002 on a.t_loca equals b.t_dimx
                                where b.t_dtyp == 1 && a.t_actv == 1
                                && a.t_psno.Equals(user, StringComparison.OrdinalIgnoreCase)
                                select b.t_dimx + "-" + b.t_desc).FirstOrDefault();
                objCTQ001.Location = location;
            }
            return View(objCTQ001);
        }
        [HttpPost]
        public ActionResult GetSMAWReport(int? id, string weldingProcess, string weldLayer, string type, string aWSClass, string fMSize, string WPSNumber, int WPSRevNo)
        {
            var data = db.SP_RPT_WELDERPARAMETERSLIP(weldingProcess, weldLayer, type, id, aWSClass, fMSize, WPSNumber, WPSRevNo).ToList().FirstOrDefault();
            if (data == null)
                data = new SP_RPT_WELDERPARAMETERSLIP_Result();
            return View(data);
        }
        #region Load Static Grid

        public ActionResult LoadGridData()
        {
            List<DataModel> list = GetDefaultData();
            TempData["Data"] = list;
            return View();
        }

        [HttpPost]
        public ActionResult GetHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetHeaderdetailDataPartial");
        }

        [HttpPost]
        public JsonResult LoadCTQHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;

                var location = (from a in db.COM003
                                join b in db.COM002 on a.t_loca equals b.t_dimx
                                where b.t_dtyp == 1 && a.t_actv == 1
                                && a.t_psno.Equals(user, StringComparison.OrdinalIgnoreCase)
                                select b.t_dimx).FirstOrDefault();

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.CTQStatus.DRAFT.GetStringValue() + "','" + clsImplementationEnum.CTQStatus.Returned.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "1=1";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or (ctq1.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%' or CTQNo like '%" + param.sSearch + "%' or CTQRev like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%')";
                }
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                strWhere += Manager.MakeDefaultWhere(user, "ctq1.BU", "ctq1.Location");
                var lstResult = db.SP_CTQ_GETHEADER
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                           Convert.ToString(uc.Project),
                           Convert.ToString(uc.BU),
                           Convert.ToString(uc.CTQNo),
                           Convert.ToString("R"+uc.CTQRev),
                            Convert.ToString(uc.Status),
                            Convert.ToString(uc.Location),
                           //Convert.ToString(uc.HeaderId),
                           "<center><a class=''   href='/CTQ/Maintain/CTQDetails?Id=" + uc.HeaderId + "'><i style = 'margin-left:5px;' class='fa fa-eye'></i></a>" +"   "+HTMLActionString(uc.HeaderId,uc.Status,"Timeline","Timeline","fa fa-clock-o", "ShowTimeline(\"/CTQ/Maintain/ShowTimeline?HeaderID="+ uc.HeaderId +"\");") +"   "+ "<i title='Print'  class='iconspace fa fa-print' onClick='PrintReport("+uc.HeaderId+")'></i>"+" "+ HTMLActionString(uc.HeaderId,"","HistoryCTQ","History","fa fa-history","HistoryCTQ(\""+ uc.HeaderId +"\", \"Maintain\");",!(uc.CTQRev > 0 || uc.Status.Equals(clsImplementationEnum.CTQStatus.Approved.GetStringValue()))) +"</center>",


                           Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [NonAction]
        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", bool disabled = false)
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (disabled)
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;opacity: 0.5;margin-left:5px;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";
            }
            else
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;margin-left:5px;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";

            return htmlControl;
        }

        public ActionResult GetDefaultGridData()
        {
            List<DataModel> list = GetDefaultData();
            TempData["Data"] = list;
            ViewBag.GridData = list;
            return PartialView("_GetHeaderGridDataPartial");
        }
        [HttpPost]
        public ActionResult SaveItem(FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();

            objResponseMsg.dModel = new DataModel { Name = fc["Name"], Address = fc["Address"], City = fc["City"], Mobile = fc["Mobile"], Pincode = fc["Pincode"] };
            objResponseMsg.Key = true;
            objResponseMsg.Value = clsImplementationMessage.ICLSeamMessages.LineInsert;

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }



        public List<DataModel> GetDefaultData()
        {
            return new List<DataModel>
                                        {
                                            new DataModel {  Name="Name 1", Mobile="9898989898", Address="Address 1", City="Surat", Pincode="216565" },
                                            new DataModel { Name="Name 2", Mobile="6565688989", Address="Address 2", City="MUmbai", Pincode="216565" },
                                            new DataModel {  Name="Name 3", Mobile="6869898988", Address="Address 3", City="Delhi", Pincode="216565" },
                                             new DataModel {  Name="Name 36", Mobile="9898989898", Address="Address 1", City="Surat", Pincode="216565" },
                                            new DataModel { Name="Name 4", Mobile="6565688989", Address="Address 2", City="MUmbai", Pincode="216565" },
                                            new DataModel {  Name="Name 5", Mobile="6869898988", Address="Address 3", City="Delhi", Pincode="216565" },
                                             new DataModel {  Name="Name 6", Mobile="9898989898", Address="Address 1", City="Surat", Pincode="216565" },
                                            new DataModel { Name="Name 7", Mobile="6565688989", Address="Address 2", City="MUmbai", Pincode="216565" },
                                            new DataModel {  Name="Name 8", Mobile="6869898988", Address="Address 3", City="Delhi", Pincode="216565" },
                                             new DataModel {  Name="Name 9", Mobile="9898989898", Address="Address 1", City="Surat", Pincode="216565" },
                                            new DataModel { Name="Name 10", Mobile="6565688989", Address="Address 2", City="MUmbai", Pincode="216565" },
                                            new DataModel {  Name="Name 11", Mobile="6869898988", Address="Address 3", City="Delhi", Pincode="216565" },
                                             new DataModel {  Name="Name 12", Mobile="9898989898", Address="Address 1", City="Surat", Pincode="216565" },
                                            new DataModel { Name="Name 13", Mobile="6565688989", Address="Address 2", City="MUmbai", Pincode="216565" },
                                            new DataModel {  Name="Name 14", Mobile="6869898988", Address="Address 3", City="Delhi", Pincode="216565" },
                                             new DataModel {  Name="Name 15", Mobile="9898989898", Address="Address 1", City="Surat", Pincode="216565" },
                                            new DataModel { Name="Name 16", Mobile="6565688989", Address="Address 2", City="MUmbai", Pincode="216565" },
                                            new DataModel {  Name="Name 17", Mobile="6869898988", Address="Address 3", City="Delhi", Pincode="216565" },
                                             new DataModel {  Name="Name 18", Mobile="9898989898", Address="Address 1", City="Surat", Pincode="216565" },
                                            new DataModel { Name="Name 19", Mobile="6565688989", Address="Address 2", City="MUmbai", Pincode="216565" },
                                            new DataModel {  Name="Name 20", Mobile="6869898988", Address="Address 3", City="Delhi", Pincode="216565" },
                                             new DataModel {  Name="Name 21", Mobile="9898989898", Address="Address 1", City="Surat", Pincode="216565" },
                                            new DataModel { Name="Name 22", Mobile="6565688989", Address="Address 2", City="MUmbai", Pincode="216565" },
                                            new DataModel {  Name="Name 23", Mobile="6869898988", Address="Address 3", City="Delhi", Pincode="216565" },
                                             new DataModel {  Name="Name 24", Mobile="9898989898", Address="Address 1", City="Surat", Pincode="216565" },
                                            new DataModel { Name="Name 25", Mobile="6565688989", Address="Address 2", City="MUmbai", Pincode="216565" },
                                            new DataModel {  Name="Name 26", Mobile="6869898988", Address="Address 3", City="Delhi", Pincode="216565" },
                                             new DataModel {  Name="Name 27", Mobile="9898989898", Address="Address 1", City="Surat", Pincode="216565" },
                                            new DataModel { Name="Name 28", Mobile="6565688989", Address="Address 2", City="MUmbai", Pincode="216565" },
                                            new DataModel {  Name="Name 29", Mobile="6869898988", Address="Address 3", City="Delhi", Pincode="216565" },
                                             new DataModel {  Name="Name 30", Mobile="9898989898", Address="Address 1", City="Surat", Pincode="216565" },
                                            new DataModel { Name="Name 31", Mobile="6565688989", Address="Address 2", City="MUmbai", Pincode="216565" },
                                             new DataModel {  Name="Name 32", Mobile="9898989898", Address="Address 1", City="Surat", Pincode="216565" },
                                            new DataModel { Name="Name 33", Mobile="6565688989", Address="Address 2", City="MUmbai", Pincode="216565" },
                                            new DataModel {  Name="Name 34", Mobile="6869898988", Address="Address 3", City="Delhi", Pincode="216565" },
                                            new DataModel {  Name="Name 35", Mobile="6869898988", Address="Address 3", City="Delhi", Pincode="216565" },

                                        };
        }

        #endregion


        #region Inline Edit Grid With Dynamic

        public ActionResult BindStages(int? id)
        {
            return View();
        }

        public ActionResult GetGridPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetGridDataPartial");
        }

        [HttpPost]
        public ActionResult UpdateData(string rowId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                db.SP_QMS_STAGEMASTER_ROW_UPDATE(Convert.ToInt32(rowId), columnName, columnValue);
                objResponseMsg.Key = true;
            }
            catch
            {
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RemoveData(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS002 objQMS002 = db.QMS002.Where(x => x.Id == Id).FirstOrDefault();
                db.QMS002.Remove(objQMS002);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveStageMaster(FormCollection fc)
        {
            int newRowIndex = 0;
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var BU = fc["BU" + newRowIndex].ToString();
                var Location = fc["Location" + newRowIndex].ToString();
                var StageCode = fc["StageCode" + newRowIndex].ToString().ToUpper();
                var isvalid = db.QMS002.Any(x => x.BU == BU && x.Location == Location && x.StageCode == StageCode);
                if (isvalid == false)
                {
                    bool d1, d2, d3, d4, d5;
                    if (fc["GPASCalculation" + newRowIndex].ToString() == "Yes")
                    {
                        d1 = true;
                    }
                    else
                    {
                        d1 = false;
                    }
                    if (fc["OverlayStage" + newRowIndex].ToString() == "Yes")
                    {
                        d2 = true;
                    }
                    else
                    {
                        d2 = false;
                    }
                    if (fc["PTCRelatedStage" + newRowIndex].ToString() == "Yes")
                    {
                        d3 = true;
                    }
                    else
                    {
                        d3 = false;
                    }
                    if (fc["IsHydroStage" + newRowIndex].ToString() == "Yes")
                    {
                        d4 = true;
                    }
                    else
                    {
                        d4 = false;
                    }
                    if (fc["IsWelderApplicable" + newRowIndex].ToString() == "Yes")
                    {
                        d5 = true;
                    }
                    else
                    {
                        d5 = false;
                    }
                    QMS002 objqms002 = new QMS002();
                    objqms002.BU = fc["BU" + newRowIndex].ToString();
                    objqms002.Location = fc["Location" + newRowIndex].ToString();
                    objqms002.StageCode = fc["StageCode" + newRowIndex].ToString().ToUpper();
                    objqms002.StageDesc = fc["StageDesc" + newRowIndex].ToString();
                    objqms002.StageType = fc["StageType" + newRowIndex].ToString();
                    objqms002.SequenceNo = fc["SequenceNo" + newRowIndex] != null && fc["SequenceNo" + newRowIndex].ToString() != string.Empty ? Convert.ToInt32(fc["SequenceNo" + newRowIndex].ToString()) : 0;
                    objqms002.NotesDrawing = fc["NotesDrawing" + newRowIndex].ToString();
                    objqms002.GPASCalculation = d1;
                    objqms002.OverlayStage = d2;
                    objqms002.PTCRelatedStage = d3;
                    objqms002.PrePostWeldHeadTreatment = fc["PrePostWeldHeadTreatment" + newRowIndex].ToString();
                    objqms002.IsHydroStage = d4;
                    objqms002.IsWelderApplicable = d5;
                    objqms002.StageDepartment = fc["StageDepartment" + newRowIndex].ToString();
                    objqms002.CreatedBy = objClsLoginInfo.UserName;
                    objqms002.CreatedOn = DateTime.Now;
                    db.QMS002.Add(objqms002);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.Message.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetStageMasterData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                string strSortOrder = string.Empty;

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (e.t_desc like '%" + param.sSearch
                        + "%' or d.t_desc like '%" + param.sSearch
                        + "%' or a.StageCode like '%" + param.sSearch
                        + "%' or a.StageDesc like '%" + param.sSearch
                        + "%' or a.StageType like '%" + param.sSearch
                        + "%' or a.SequenceNo like '%" + param.sSearch
                        + "%' or a.NotesDrawing like '%" + param.sSearch
                        + "%' or a.GPASCalculation like '%" + param.sSearch
                        + "%' or a.OverlayStage like '%" + param.sSearch
                        + "%' or a.PTCRelatedStage like '%" + param.sSearch
                        + "%' or a.PrePostWeldHeadTreatment like '%" + param.sSearch
                        + "%' or a.IsHydroStage like '%" + param.sSearch
                        + "%' or a.IsWelderApplicable like '%" + param.sSearch
                        + "%' or a.StageDepartment like '%" + param.sSearch
                        + "%' or a.CreatedBy like '%" + param.sSearch
                        + "%' or a.CreatedOn like '%" + param.sSearch
                        + "%')";

                }
                else
                {
                    if ((!string.IsNullOrWhiteSpace(param.StageCode)) || (!string.IsNullOrWhiteSpace(param.BU)) || (!string.IsNullOrWhiteSpace(param.Location)) || (!string.IsNullOrWhiteSpace(param.StageDesc)) || (!string.IsNullOrWhiteSpace(param.Stagetype)) || (!string.IsNullOrWhiteSpace(param.Sequenceno)) || (!string.IsNullOrWhiteSpace(param.RevNo)) || (!string.IsNullOrWhiteSpace(param.Gpascal)) || (!string.IsNullOrWhiteSpace(param.OverlayStage)) || (!string.IsNullOrWhiteSpace(param.PTCRelated)) || (!string.IsNullOrWhiteSpace(param.PrePostWeldHead)) || (!string.IsNullOrWhiteSpace(param.IsHydro)) || (!string.IsNullOrWhiteSpace(param.IsWelder)) || (!string.IsNullOrWhiteSpace(param.StageDepartment)) || (!string.IsNullOrWhiteSpace(param.CreatedOn)) || (!string.IsNullOrWhiteSpace(param.Createdby)))
                    {
                        strWhere = " (a.StageCode like '%" + param.StageCode
                             + "%' and d.t_desc like '%" + param.BU
                             + "%' and e.t_desc like '%" + param.Location
                            + "%' and a.StageDesc like '%" + param.StageDesc
                            + "%' and a.StageType like '%" + param.Stagetype
                            + "%' and a.SequenceNo like '%" + param.Sequenceno
                            + "%' and a.NotesDrawing like '%" + param.RevNo
                            + "%' and a.GPASCalculation like '%" + param.Gpascal
                            + "%' and a.OverlayStage like '%" + param.OverlayStage
                            + "%' and a.PTCRelatedStage like '%" + param.PTCRelated
                            + "%' and a.PrePostWeldHeadTreatment like '%" + param.PrePostWeldHead
                            + "%' and a.IsHydroStage like '%" + param.IsHydro
                            + "%' and a.IsWelderApplicable like '%" + param.IsWelder
                            + "%' and a.StageDepartment like '%" + param.StageDepartment
                            + "%' and f.t_namb like '%" + param.Createdby
                            + "%' and CONVERT(varchar, a.CreatedOn, 103) like '%" + param.CreatedOn


                            + "%')";
                        strSortOrder = string.Empty;
                    }
                    else
                    {

                        strWhere = "1=1";

                    }
                }


                var lstResult = db.SP_ATH_GET_StageMaster_TT
                            (
                            StartIndex, EndIndex, strSortOrder, strWhere
                            ).ToList();

                List<SelectListItem> BoolenList = new List<SelectListItem>() { new SelectListItem { Text = "Yes", Value = "Yes" }, new SelectListItem { Text = "No", Value = "No" } };

                var BU = (from a in db.COM002
                          where a.t_dtyp == 2 && a.t_dimx != ""
                          select new { a.t_dimx, Desc = a.t_desc }).ToList();

                var Location = (from a in db.COM002
                                where a.t_dtyp == 1 && a.t_dimx != ""
                                select new { a.t_dimx, Desc = a.t_desc }).ToList();

                var StageType = (from a in db.GLB002
                                 where a.Category == 24 && a.IsActive == true
                                 select new { a.Code, Desc = a.Code + " - " + a.Description }).ToList();

                var StageDept = (from a in db.GLB002
                                 where a.Category == 11 && a.IsActive == true
                                 select new { a.Code, Desc = a.Code + " - " + a.Description }).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                    Helper.GenerateHidden(newRecordId, "Id", ""),
                                    Helper.GenerateDropdown(newRecordId, "BU", new SelectList(BU, "t_dimx", "Desc"), " "),
                                    Helper.GenerateDropdown(newRecordId, "Location", new SelectList(Location, "t_dimx", "Desc"), " "),
                                    Helper.GenerateTextbox(newRecordId, "StageCode"),
                                    Helper.GenerateTextbox(newRecordId, "StageDesc"),
                                    Helper.GenerateDropdown(newRecordId, "StageType", new SelectList(StageType, "Code", "Desc"), " "),
                                    Helper.GenerateTextbox(newRecordId, "SequenceNo"),
                                    Helper.GenerateTextbox(newRecordId, "NotesDrawing"),
                                    Helper.GenerateDropdown(newRecordId, "GPASCalculation", new SelectList(BoolenList, "Value", "Text"), " "),
                                    Helper.GenerateDropdown(newRecordId, "OverlayStage", new SelectList(BoolenList, "Value", "Text"), " "),
                                    Helper.GenerateDropdown(newRecordId, "PTCRelatedStage", new SelectList(BoolenList, "Value", "Text"), " "),
                                    Helper.GenerateTextbox(newRecordId, "PrePostWeldHeadTreatment"),
                                    Helper.GenerateDropdown(newRecordId, "IsHydroStage", new SelectList(BoolenList, "Value", "Text"), " "),
                                    Helper.GenerateDropdown(newRecordId, "IsWelderApplicable", new SelectList(BoolenList, "Value", "Text"), " "),
                                    Helper.GenerateDropdown(newRecordId, "StageDepartment", new SelectList(StageDept, "Code", "Desc"), " "),
                                    "",
                                    "",
                                    Helper.GenerateGridButton(newRecordId, "Add", "Add Rocord", "fa fa-plus", "SaveNewRecord();" ),
                                };


                var data = (from uc in lstResult
                            select new[]
                            {
                               Helper.GenerateHidden(uc.Id, "Id", Convert.ToString(uc.Id)),
                               Helper.GenerateDropdown(uc.Id, "BU", new SelectList(BU, "t_dimx", "Desc", Convert.ToString(uc.BU)),"", "UpdateData(this, "+ uc.Id +");"),
                               Helper.GenerateDropdown(uc.Id, "Location", new SelectList(Location, "t_dimx", "Desc", Convert.ToString(uc.Location)),"", "UpdateData(this, "+ uc.Id +");"),
                               Helper.GenerateTextbox(uc.Id, "StageCode", uc.StageCode, "UpdateData(this, "+ uc.Id +");"),
                               Helper.GenerateTextbox(uc.Id, "StageDesc", uc.StageDesc),
                               Helper.GenerateDropdown(uc.Id, "StageType", new SelectList(StageType, "Code", "Desc", Convert.ToString(uc.StageType)),"","UpdateData(this, "+ uc.Id +");"),
                               Helper.GenerateTextbox(uc.Id, "SequenceNo", Convert.ToString(uc.SequenceNo), "UpdateData(this, "+ uc.Id +");"),
                               Helper.GenerateTextbox(uc.Id, "NotesDrawing", Convert.ToString(uc.NotesDrawing),"UpdateData(this, "+ uc.Id +");"),
                               Helper.GenerateDropdown(uc.Id, "GPASCalculation", new SelectList(BoolenList, "Value", "Text", Convert.ToString(uc.GPASCalculation)),"","UpdateData(this, "+ uc.Id +");"),
                               Helper.GenerateDropdown(uc.Id, "OverlayStage", new SelectList(BoolenList, "Value", "Text", Convert.ToString(uc.OverlayStage)),"","UpdateData(this, "+ uc.Id +");"),
                               Helper.GenerateDropdown(uc.Id, "PTCRelatedStage", new SelectList(BoolenList, "Value", "Text", Convert.ToString(uc.PTCRelatedStage)),"","UpdateData(this, "+ uc.Id +");"),
                               Helper.GenerateTextbox(uc.Id, "PrePostWeldHeadTreatment", Convert.ToString(uc.PrePostWeldHeadTreatment),"UpdateData(this, "+ uc.Id +");"),
                               Helper.GenerateDropdown(uc.Id, "IsHydroStage", new SelectList(BoolenList, "Value", "Text", Convert.ToString(uc.IsHydroStage)),"","UpdateData(this, "+ uc.Id +");"),
                               Helper.GenerateDropdown(uc.Id, "IsWelderApplicable", new SelectList(BoolenList, "Value", "Text", Convert.ToString(uc.IsWelderApplicable)),"","UpdateData(this, "+ uc.Id +");"),
                               Helper.GenerateDropdown(uc.Id, "StageDepartment", new SelectList(StageDept, "Code", "Desc", Convert.ToString(uc.StageDepartment)),"","UpdateData(this, "+ uc.Id +");"),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn),
                               Helper.GenerateGridButton(uc.Id, "Delete", "Delete Rocord", "fa fa-trash-o", "RemoveRecord("+ uc.Id +");"),
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }


        #endregion

        [SessionExpireFilter]
        public ActionResult InlineEditableDataGrid()
        {
            ViewBag.NameTitle = new List<CategoryData> { new CategoryData { Value = "Mr.", Code = "Mr.", CategoryDescription = "Mr." }, new CategoryData { Value = "Ms.", Code = "Ms.", CategoryDescription = "Ms." }, new CategoryData { Value = "Mrs.", Code = "Mrs.", CategoryDescription = "Mrs." } };
            ViewBag.Gender = new List<CategoryData> { new CategoryData { Value = "Male", Code = "Male", CategoryDescription = "Male" }, new CategoryData { Value = "Female", Code = "Female", CategoryDescription = "Female" } };

            return View();
        }

        //bind datatable for Editable Data Grid
        [HttpPost]
        public JsonResult LoadInlineEditableDataGridData(JQueryDataTableParamModel param)
      {
            try
            {
                //JavaScriptSerializer serializer1 = new JavaScriptSerializer();
                //List<SearchFilter> searchflt = serializer1.Deserialize<List<SearchFilter>>(SearchFilter);
                clsHelper ch = new clsHelper();

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion
                strWhereCondition = "1=1";
                //search Condition 
                string[] columnName = { "Title", "FirstName", "LastName", "Roles", "Address", "Birthdate", "Gender", "Salary", "Married", "Status" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }

                else
                {
                    string Rolesid = null;
                    if (param.Roles != null)
                    {
                        string Role = param.Roles.Trim();
                        List<string> numlist = new List<string>();

                        foreach (string Rolename in Role.Trim().Split(','))
                        {
                            var Roledesc = db.ATH004.Where(m => m.Role == Rolename.Trim()).FirstOrDefault();
                            numlist.Add(Convert.ToString(Roledesc.Id));
                        }
                        Rolesid = String.Join(",", numlist.ToArray());
                    }
                    string Filtertitle = param.FilterTitle;
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);

                }

                var items = (from li in db.ATH004
                             select new SelectItemList
                             {
                                 id = li.Id.ToString(),
                                 text = li.Role,
                             }).ToList();


                var lstResult = db.SP_CODINGSTANDARD_GETUSERDETAIL(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();
                int newRecordId = 0;
                var newRecord = new[] {
                                        Helper.HTMLAutoComplete(newRecordId,"UserId","","",false,"","UserId"),
                                        Helper.HTMLAutoComplete(newRecordId,"Title","","",false,"","Title"),
                                        Helper.GenerateTextbox(newRecordId, "FirstName",   "", "", false, "", "50"),
                                        Helper.GenerateTextbox(newRecordId, "LastName",  "", "", false, "", "50"),
                                        Helper.MultiSelectDropdown(items,newRecordId,"Roles",false,"","","Roles"),
                                        Helper.GenerateTextArea(newRecordId, "Address",  "", "", false, "", "500"),
                                        Helper.GenerateTextbox(newRecordId, "Birthdate"),
                                        Helper.HTMLAutoComplete(newRecordId,"Gender","","",false,"","Gender"),
                                        Helper.GenerateNumericTextbox(newRecordId, "Salary",  "", "", false, "", "20"),
                                      // Helper.GenerateActionCheckbox(newRecordId,"Married",false,"",false),
                                        clsImplementationEnum.CommonStatus.Active.GetStringValue(),
                                        Helper.GenerateGridButtonNew(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveNewRecord();" ),
                                    };
                var data = (from uc in lstResult
                            select new[]
                           {
                                uc.UserId+"",
                            Helper.HTMLAutoComplete(uc.UserId, "Title",    Convert.ToString(uc.Title),"UpdateData(this, "+ uc.UserId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true),"50"),
                            Helper.GenerateTextbox(uc.UserId, "FirstName",    Convert.ToString(uc.FirstName),"UpdateData(this, "+ uc.UserId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true),"50"),
                            Helper.GenerateTextbox(uc.UserId, "LastName",    Convert.ToString(uc.LastName),"UpdateData(this, "+ uc.UserId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true),"50"),
                            Helper.MultiSelectDropdown(items,uc.UserId,uc.Roles, (string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true) ,"UpdateData(this,'"+ uc.UserId  +"',true);","","Roles"),
                            Helper.GenerateTextArea(uc.UserId, "Address",    Convert.ToString(uc.Address),"UpdateData(this, "+ uc.UserId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true),"width:200px;","500"),
                            Helper.GenerateTextbox(uc.UserId, "Birthdate",uc.Birthdate==null || uc.Birthdate.Value==DateTime.MinValue ? "":Convert.ToDateTime(uc.Birthdate).ToString("yyyy-MM-dd") , "UpdateData(this, "+ uc.UserId +");",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true)),
                           Helper.HTMLAutoComplete(uc.UserId, "Gender",    Convert.ToString(uc.Gender),"UpdateData(this, "+ uc.UserId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true),"50"),
                            Helper.GenerateNumericTextbox(uc.UserId, "Salary",    Convert.ToString(uc.Salary),"UpdateData(this, "+ uc.UserId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true),"","50"),
                         //   Helper.GenerateActionCheckbox(uc.UserId,"Married",uc.Married,"UpdateData(this, "+ uc.UserId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true)),
                            Convert.ToString(uc.Status),
                            Helper.GenerateActionIcon(uc.UserId, "Delete", "Delete Record", "fa fa-trash-o", "DeleteData("+uc.UserId+")","",false)
                          }).ToList();
                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = new List<string>(),
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string MakeDatatableSearchCondition1(string filterstatus, string column_name, string column_value)
        {
            string strLikeFormat = string.Empty;
            StringBuilder sb = new StringBuilder();
            sb.Append(" and ( ");
            if (filterstatus == clsImplementationEnum.NumaricValidation.Equal.GetStringValue())
            {

                sb.Append(" " + column_name + " = '" + column_value + "' ");
            }

            else if (filterstatus == clsImplementationEnum.NumaricValidation.DoesNotEqual.GetStringValue())
            {
                sb.Append(" " + column_name + " != '" + column_value + "' ");

            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.StartsWith.GetStringValue())
            {
                sb.Append(" " + column_name + " like '" + column_value + "%'");

            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.GreaterThenorEquals.GetStringValue())
            {
                sb.Append(" " + column_name + " >= '" + column_value + "' ");

            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.LessThenorEquals.GetStringValue())
            {
                sb.Append(" " + column_name + " <= '" + column_value + "' ");

            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.GreaterThen.GetStringValue())
            {
                sb.Append(" " + column_name + " > '" + column_value + "' ");

            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.LessThen.GetStringValue())
            {
                sb.Append(" " + column_name + " < '" + column_value + "' ");

            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.Contains.GetStringValue())
            {
                sb.Append(" " + column_name + " like '%" + column_value + "%'");

            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.DoesNotContains.GetStringValue())
            {
                sb.Append(" " + column_name + " not like '%" + column_value + "%'");

            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.EndsWith.GetStringValue())
            {
                sb.Append(" " + column_name + " like '%" + column_value + "'");

            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.DoesNotEndsWith.GetStringValue())
            {
                sb.Append(" " + column_name + " not like '%" + column_value + "'");
            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.IsEmpty.GetStringValue())
            {
                sb.Append(" " + column_name + " is null   ");

            }
            else if (filterstatus == clsImplementationEnum.NumaricValidation.IsNotEmpty.GetStringValue())
            {
                sb.Append(" " + column_name + " is not null ");

            }
            else
            {
                sb.Append(" " + column_name + " like  '%" + column_value + "%'");
            }
            sb.Append(") ");
            strLikeFormat = sb.ToString();
            return strLikeFormat;
        }



        public ActionResult FileUpload()
        {
            return View();
        }

        [HttpPost]
        public ActionResult MultiFileUpload(HttpPostedFileBase file)
        {
            return Json("");
        }

        [SessionExpireFilter]
        public ActionResult ProtocolDetails(int? id)
        {
            PRO030 objPRO030 = new PRO030();

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = "WELD VISUAL & DIMENSION REPORT FOR NOZZLE";

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            return View(objPRO030);
        }
        [SessionExpireFilter]
        public ActionResult DatatableDesign()
        {
            return View();
        }

        public JsonResult GetAllRoles()
        {
            try
            {
                var items = (from li in db.ATH004
                             select new
                             {
                                 id = li.Role,
                                 text = li.Role,
                             }).ToList();

                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        #region QR Code Generater& Reader  and Push Notification
        [SessionExpireFilter]
        public ActionResult QRCode()
        {
            return View();
        }
        [SessionExpireFilter]
        public ActionResult OpenQRScanner()
        {
            return View();
        }
        #endregion

        #region PDFEditor
        [SessionExpireFilter]
        public ActionResult PDFEditor(string text = "")
        {
            if (String.IsNullOrWhiteSpace(text))
            {
                return View();
            }
            string srcfilePath = HttpContext.Server.MapPath("/Resources/Templates/") + "Full Job PWHT -1 Model.pdf";
            using (var ms = new System.IO.MemoryStream())
            {
                var doc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A3, 0, 0, 0, 0);
                text = (String.IsNullOrWhiteSpace(text)? DateTime.Now.Ticks+"": text) ;

                var writer = iTextSharp.text.pdf.PdfWriter.GetInstance(doc, ms);
                doc.Open();

                var templateReader = new iTextSharp.text.pdf.PdfReader(srcfilePath);
                using (var rdr = new iTextSharp.text.pdf.PdfReader(srcfilePath))
                {
                    iTextSharp.text.pdf.PdfImportedPage page;
                    for (int i = 1; i <= rdr.NumberOfPages; i++)
                    {
                        page = writer.GetImportedPage(templateReader, i);
                        writer.DirectContent.AddTemplate(page, 0, 0);
                        if (i == 1)
                        {
                            var bf = iTextSharp.text.pdf.BaseFont.CreateFont();
                            writer.DirectContent.BeginText();
                            writer.DirectContent.SetFontAndSize(bf, 18f);
                            writer.DirectContent.ShowTextAligned(iTextSharp.text.pdf.PdfContentByte.ALIGN_RIGHT, text, 50, 1150, 90);
                            writer.DirectContent.EndText();
                        }
                        doc.NewPage();
                    }
                }
                doc.Close();
                byte[] bytes = ms.ToArray();
                ms.Close();
                Response.Clear();
                Response.ContentType = "application/pdf";

                string pdfName = "Full Job PWHT -1 Model";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + pdfName + ".pdf");
                Response.ContentType = "application/pdf";
                Response.Buffer = true;
                Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
                Response.BinaryWrite(bytes);
                Response.End();
                Response.Close();
            }

            return View();
        }
        #endregion
    }

    public class DataModel
    {
        public string Name { get; set; }
        public string Mobile { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Pincode { get; set; }

    }
    public class ResponceMsgWithHeaderID : clsHelper.ResponseMsg
    {
        public string Status;
        public int HeaderId;
        public int Sequence;
        public string Stage;
        public DataModel dModel;
    }




}