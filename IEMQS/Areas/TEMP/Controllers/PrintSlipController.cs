﻿using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.TEMP.Controllers
{
    public class PrintSlipController : Controller
    {
        // GET: TEMP/PrintSlip
        private Font printFont;
        private StreamReader streamToPrint;
        static string filePath;
        private DataTable tmp_Table;
        private int rowcount;
        private Image photo;

        public ActionResult Print()
        {
            return View("Index");
        }

        public ActionResult Index()
        {
            try
            {
                //SaveImage("tt.jpg", ImageFormat.Jpeg);

                //tmp_Table = new DataTable();
                //tmp_Table.Columns.Add("Name");
                //tmp_Table.Columns.Add("Value");

                //for (int i = 0; i < 5; i++)
                //{
                //    DataRow row = tmp_Table.NewRow();
                //    row["Name"] = "Name" + i.ToString();
                //    row["Value"] = "ValueHow to print and print preview my table only with a selected row and value instead of all the row and valuesHow to print and print preview my table only with a selected row and value instead of all the row and valuesHow to print and print preview my table only with a selected row and value instead of all the row and valuesHow to print and print preview my table only with a selected row and value instead of all the row and values" + i.ToString();
                //    tmp_Table.Rows.Add(row);
                //}

                ////Print(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "PrintHtml.exe");
                ////Printing();

                //PrintDocument doc = new PrintDocument();
                //doc.PrinterSettings.PrinterName = @"RP 3220 star";
                //doc.PrintPage += new PrintPageEventHandler(document_PrintPage);
                //doc.Print();

                photo = Image.FromFile(@"D:\Sample.png");                
                PrintDocument printDoc = new PrintDocument();
                printDoc.PrinterSettings.PrinterName = @"RP 3220 star";
                printDoc.PrintPage += new PrintPageEventHandler(printImg_PrintPage);
                printDoc.Print();

               

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View();
        }

        void printImg_PrintPage(object sender, PrintPageEventArgs e)
        {
            Point ulCorner = new Point(10, 10);
            e.Graphics.DrawImage(photo, ulCorner);
            photo.Dispose();
        }

        public void SaveImage(string filename, ImageFormat format)
        {

            //using (WebClient client = new WebClient())
            //{
            //    client.DownloadFile(new Uri(url), @"c:\temp\image35.png");
            //    // OR 
            //    client.DownloadFileAsync(new Uri(url), @"c:\temp\image35.png");
            //}

            WebClient client = new WebClient();
            Stream stream = client.OpenRead(@"D:\");
            Bitmap bitmap; bitmap = new Bitmap(stream);

            if (bitmap != null)
            {
                bitmap.Save(filename, format);
            }

            stream.Flush();
            stream.Close();
            client.Dispose();
        }
        private void document_PrintPage(object sender, PrintPageEventArgs e)
        {
            
        }
        private void Print(string path, string printer)
        {
            

            if (System.IO.File.Exists(path))
            {
                var printJob = new Process
                {
                    StartInfo = new ProcessStartInfo
                    {
                        FileName = path,
                        Arguments = $"-p \" "+ printer + " http://localhost:27961/TEMP/PrintSlip/Print",
                        UseShellExecute = true,
                        Verb = "print",
                        CreateNoWindow = true,
                        WindowStyle = ProcessWindowStyle.Hidden,
                        WorkingDirectory = Path.GetDirectoryName(path)
                    }
                };

                printJob.Start();
            }
        }

        public void Printing()
        {
            WebClient client = new WebClient();
            Stream stream = client.OpenRead("http://localhost:27961/TEMP/PrintSlip/Print");
            StreamReader reader = new StreamReader(stream);

            streamToPrint = reader; // new StreamReader(@"http://localhost:27961/TEMP/PrintSlip");
            try
            {
                printFont = new Font("Arial", 10);
                PrintDocument pd = new PrintDocument();
                pd.PrinterSettings.PrinterName = @"RP 3220 star";
                pd.PrintPage += new PrintPageEventHandler(pd_PrintPage);
                // Print the document.
                pd.Print();
            }
            finally
            {
                streamToPrint.Close();
            }

        }

        private void pd_PrintPage(object sender, PrintPageEventArgs ev)
        {
            float linesPerPage = 0;
            float yPos = 0;
            int count = 0;
            float leftMargin = ev.MarginBounds.Left;
            float topMargin = ev.MarginBounds.Top;
            String line = null;

            // Calculate the number of lines per page.
            linesPerPage = ev.MarginBounds.Height / printFont.GetHeight(ev.Graphics);

            // Iterate over the file, printing each line.
            while (((line = streamToPrint.ReadLine()) != null))
            {
                yPos = topMargin + (count * printFont.GetHeight(ev.Graphics));
                ev.Graphics.DrawString(line, printFont, Brushes.Black,
                   leftMargin, yPos, new StringFormat());
                count++;
            }

            // If more lines exist, print another page.
            if (line != null)
                ev.HasMorePages = true;
            else
                ev.HasMorePages = false;
        }
        private void PrintHandler(object sender, PrintPageEventArgs ppeArgs)
        {
            Font FontNormal = new Font("", 10);
            Graphics g = ppeArgs.Graphics;
            g.DrawString(@" ", FontNormal, Brushes.Black, 10, 10, new StringFormat());
        }

        

    }
}