﻿using GleamTech.DocumentUltimate;
using GleamTech.DocumentUltimate.AspNet;
using GleamTech.DocumentUltimate.AspNet.UI;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsBase;

namespace IEMQS.Areas.TEMP.Controllers
{
    public class Xml3DController : clsBase
    {
        // GET: TEMP/Xml3D
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }

        [SessionExpireFilter]
        public ActionResult Advance()
        {
            ViewBag.PlayerSetting = "Advance";
            return View("Index");
        }

        public ActionResult ViewFile(string FileUrl)
        {
            if (String.IsNullOrWhiteSpace(FileUrl))
                FileUrl = TempData["FileUrl"] + "";
            ViewBag.FileUrl = FileUrl;
            ViewBag.FileExtension = Path.GetExtension(FileUrl).ToLower();

            if (ViewBag.FileExtension != ".3dxml" )//|| ViewBag.FileExtension != ".gif")
            {
                WebClient client = new WebClient();
                client.UseDefaultCredentials = true;
                var stream = client.OpenRead(ViewBag.FileUrl);

                DocumentUltimateWebConfiguration.Current.CacheMaxDays = -1;
                DocumentSource documentSource = new DocumentSource(new DocumentInfo( new Random().Next(1000)+"0" , Path.GetFileName(FileUrl) ),new StreamResult(stream));
                var documentViewer = new DocumentViewer
                {
                    Resizable = false,
                    DocumentSource = documentSource,
                    DownloadEnabled = false,
                    DownloadAsPdfEnabled = false,
                    PrintEnabled = false,
                    TextSelectionEnabled = false
                };
                return PartialView("_ViewFile", documentViewer);
            }
            return PartialView("_ViewFile");
        }

        public ActionResult SecureFileViewer(string FileUrl)
        {
            if (FileUrl.StartsWith("http") || FileUrl.StartsWith("~"))
                TempData["FileUrl"] = FileUrl;
            else
                TempData["FileUrl"] = @System.Configuration.ConfigurationManager.AppSettings["File_Prefix_Url"] + FileUrl;
            return RedirectToAction("ViewFile");
        }

        [SessionExpireFilter]
        public ActionResult LoadFileDataTable(JQueryDataTableParamModel param, string status)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                var lstFiles = (new clsFileUpload(clsImplementationEnum.UploadType.Normal.GetStringValue())).GetDocuments("3dXml/").ToList();
                
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    if (sortColumnName == "Comments")
                    {
                        if (sortDirection == "desc")
                            lstFiles = lstFiles.OrderByDescending(o => o.Comments).ToList();
                        else
                            lstFiles = lstFiles.OrderBy(o => o.Comments).ToList();
                    }
                    else if (sortColumnName == "Size")
                    {
                        if (sortDirection == "desc")
                            lstFiles = lstFiles.OrderByDescending(o => o.Size).ToList();
                        else
                            lstFiles = lstFiles.OrderBy(o => o.Size).ToList();
                    }
                    else
                    {
                        if (sortDirection == "desc")
                            lstFiles = lstFiles.OrderByDescending(o => o.Name).ToList();
                        else
                            lstFiles = lstFiles.OrderBy(o => o.Name).ToList();
                    }
                }
                if (!string.IsNullOrEmpty(param.sSearch))
                    lstFiles = lstFiles.Where(w => w.Name.Contains(param.sSearch) || w.Comments.Contains(param.sSearch)).ToList();

                int totalRecords = lstFiles.Count();
                lstFiles = lstFiles.Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();

                var res = from a in lstFiles
                        select new[] {
                            a.FileId,
                            Path.GetFileName(a.Name),
                            Path.GetExtension(a.Name),
                            a.Comments,
                            a.Size,
                            Helper.GenerateActionIcon(0, "View", "View File", "fa fa-eye", "ViewFile('"+a.URL+"','"+a.FileId+"')") 
                            + Helper.GenerateActionIcon(0, "Delete", "Delete File", "fa fa-trash", "DeleteFile('"+a.Name+"')")
                        };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords,
                    iTotalRecords = totalRecords,
                    aaData = res,
                    strSortOrder = "",
                    whereCondition = ""
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }
    }
}