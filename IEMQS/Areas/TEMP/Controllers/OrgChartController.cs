﻿using IEMQS.Areas.TEMP.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.TEMP.Controllers
{
    public class OrgChartController : Controller
    {
        SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["IEMQS"].ToString());

        // GET: TEMP/OrgChart
        public ActionResult Index()
        {

            clsDataFlow objclsDataFlow = new clsDataFlow();
            objclsDataFlow.nodeDataArray = FetchNodeDataArray();
            objclsDataFlow.linkDataArray = FetchLinkDataArray();

            return View(objclsDataFlow);
        }


        public List<nodeDataArray> FetchNodeDataArray()
        {
            List<nodeDataArray> list = new List<nodeDataArray>();
            try
            {
                string query = "select * from [lnentrep\\lndb3in].edw_ln.dbo.uvwOrgEmployeeData WHERE t_depc IN ('3483','3412') OR t_psno IN ('47377','46992','122046') order by t_iasp ";
                connection.Open();

                using (var command = new SqlCommand(query, connection))
                {
                    SqlDataReader reader = command.ExecuteReader();
                    try
                    {
                        string currentDept = "";
                        while (reader.Read())
                        {
                            //if (reader["t_iasp"].ToString() != currentDept && reader["GrandChild"].ToString() == "1" &&  !reader["cadre"].ToString().StartsWith("M"))
                            //{
                            //    currentDept = "G" + reader["t_iasp"].ToString();
                            //    var newDeptRecord = new nodeDataArray()
                            //    {
                            //        key = currentDept,
                            //        type = "TmpInOut",
                            //        name = reader["Department"].ToString(),
                            //        isGroup = true

                            //    };
                            //    list.Add(newDeptRecord);
                            //}

                            var newRecord = new nodeDataArray()
                            {
                                key = reader["t_psno"].ToString(),
                                type = "TmpInOut",
                                name = reader["t_name"].ToString(),
                                iasp = reader["t_iasp"].ToString(),
                                ifsp = reader["t_ifsp"].ToString(),
                                nfsp = reader["t_nfsp"].ToString(),
                                csup = reader["t_csup"].ToString(),
                                ccdr = "",//reader["t_ccdr"].ToString(),
                                cadre = reader["cadre"].ToString(),
                                depc = reader["t_depc"].ToString(),
                                department = reader["Department"].ToString(),
                                function = reader["Fnction"].ToString(),
                                grandchild = reader["GrandChild"].ToString(),
                                photo = ConfigurationManager.AppSettings["WebsiteURL"].ToString() + "/ImageHandler.ashx?psno=" + reader["t_psno"].ToString(),
                                lpdt = "Last Promotion: " + (reader["LastPromotion"] == null ? "" : Convert.ToDateTime(reader["LastPromotion"]).ToString("dd/MM/yyyy"))
                                //group = reader["GrandChild"].ToString() == "1" ? currentDept : ""
                            };
                            list.Add(newRecord);
                        }
                    }
                    finally
                    {
                        reader.Close();
                        connection.Close();
                    }
                    return list;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return list;
            }
        }


        public List<linkDataArray> FetchLinkDataArray()
        {
            List<linkDataArray> list = new List<linkDataArray>();
            try
            {
                string query = @"SELECT t_psno as PSNo, EmployeeType, ParentPSNo
                                FROM
                                   (
                                    SELECT t_psno, t_iasp, t_ifsp, t_nfsp, t_csup, LastPromotion
                                    from [lnentrep\lndb3in].edw_ln.dbo.uvwOrgEmployeeData WHERE t_depc IN ('3483','3412') OR t_psno IN ('47377','46992','122046')
                                   ) p
                                UNPIVOT
                                   (ParentPSNo FOR EmployeeType IN
                                            (t_iasp, t_ifsp, t_nfsp, t_csup)
                                    )AS unpvt
                                WHERE ParentPSNo <> ''";

                connection.Open();

                using (var command = new SqlCommand(query, connection))
                {
                    SqlDataReader reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            var newRecord = new linkDataArray()
                            {
                                from = reader["ParentPSNo"].ToString(),
                                frompid = reader["EmployeeType"].ToString() + "O",
                                to = reader["PSNo"].ToString(),
                                topid = reader["EmployeeType"].ToString()
                            };
                            list.Add(newRecord);
                        }
                    }
                    finally
                    {
                        reader.Close();
                        connection.Close();
                    }
                    return list;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return list;
            }
        }


    }
}