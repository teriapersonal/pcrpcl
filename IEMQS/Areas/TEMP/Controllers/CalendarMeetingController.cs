﻿using System;
using System.Net.Mail;
using System.Text;
using System.Web.Mvc;

namespace IEMQS.Areas.TEMP.Controllers
{
    public class CalendarMeetingController : Controller
    {
        public ActionResult Index()
        {
            MailMessage msg = new MailMessage("IEMQS@larsentoubro.com", "hardik.savani@rigelnetworks.com");
            msg.Headers.Add("Content-class", "urn:content-classes:calendarmessage");
            msg.To.Add("trishul.tandel@rigelnetworks.com");
            msg.Subject = "This is sample test event";
            msg.Body = "This is sample test string message";

            StringBuilder str = new StringBuilder();
            str.AppendLine("BEGIN:VCALENDAR");
            str.AppendLine("PRODID:-//Schedule a Meeting");
            str.AppendLine("VERSION:2.0");
            str.AppendLine("METHOD:REQUEST");
            str.AppendLine("BEGIN:VEVENT");
            str.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", new DateTime(2019, 1, 1, 13, 00, 00)));
            str.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", new DateTime(2019, 1, 1, 13, 00, 00)));
            str.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", new DateTime(2019, 1, 1, 16, 00, 00)));
            str.AppendLine("LOCATION:L&T, Hazira, Surat 394270.");
            str.AppendLine(string.Format("UID:{0}", "TestMeeting"));
            str.AppendLine(string.Format("DESCRIPTION:{0}", msg.Body));
            str.AppendLine(string.Format("X-ALT-DESC;FMTTYPE=text/html:{0}", msg.Body));
            str.AppendLine(string.Format("SUMMARY:{0}", msg.Subject));
            str.AppendLine(string.Format("ORGANIZER:MAILTO:{0}", msg.From.Address));
            str.AppendLine(string.Format("ATTENDEE;CN=\"{0}\";RSVP=TRUE:mailto:{1}", msg.To[0].DisplayName, msg.To[0].Address));
            str.AppendLine("BEGIN:VALARM");
            str.AppendLine("TRIGGER:-PT15M");
            str.AppendLine("ACTION:DISPLAY");
            str.AppendLine("DESCRIPTION:Reminder");
            str.AppendLine("END:VALARM");
            str.AppendLine("END:VEVENT");
            str.AppendLine("END:VCALENDAR");

            // byte[] byteArray = Encoding.ASCII.GetBytes(str.ToString());
            // MemoryStream stream = new MemoryStream(byteArray);
            // Attachment attach = new Attachment(stream, "meeting.ics");
            // msg.Attachments.Add(attach);

            System.Net.Mime.ContentType contype = new System.Net.Mime.ContentType("text/calendar");
            contype.Parameters.Add("method", "REQUEST");

            AlternateView avCal = AlternateView.CreateAlternateViewFromString(str.ToString(), contype);
            msg.AlternateViews.Add(avCal);

            new SmtpClient("172.27.14.246").Send(msg);
            return View();
        }

        public ActionResult Index2(string t)
        {
            try
            {
                MailMessage msg = new MailMessage("IEMQS@larsentoubro.com", "hardik.savani@rigelnetworks.com");
                // msg.Headers.Add("Content-class", "urn:content-classes:calendarmessage");
                msg.To.Add("hardik.savani@rigelnetworks.com,trishul.tandel@rigelnetworks.com");

                msg.Body = "Hello you there? This is sample test string message from index2 action" + t;

                //Now sending a mail with attachment ICS file.                     
                SmtpClient smtpclient = new SmtpClient();
                smtpclient.Host = "172.27.14.246"; // ip address of iemqs dev server. 

                // smtpclient.EnableSsl = true;
                // smtpclient.Credentials = new System.Net.NetworkCredential("anirudhagupta0011@gmail.com", "testing");

                smtpclient.Send(msg);
                return View();
            }
            catch (Exception ex)
            {
                return View();
            }
        }

        public string sendOutlook()
        {
            try
            {
                MailMessage msg = new MailMessage();

                msg.From = new MailAddress("IEMQS@larsentoubro.com");
                msg.To.Add("hardik_savani@hotmail.com,Hardik.savani@rigelnetworks.com");
                msg.Bcc.Add("Hrddk79@gmail.com");

                msg.Subject = "This is an email with test event";
                msg.Body = $"Hello, \n\nPlease find event details with this email.\n\nThanks,\n-Hardik";

                StringBuilder str = new StringBuilder();
                str.AppendLine("BEGIN:VCALENDAR");
                str.AppendLine("PRODID:-//Hardik.savani@rigelnetworks.com");
                str.AppendLine("VERSION:2.0");
                str.AppendLine("METHOD:REQUEST");
                str.AppendLine("BEGIN:VEVENT");
                str.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", DateTime.Now.ToString("yyyyMMdd\\THHmmss\\Z")));
                str.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow.ToString("yyyyMMdd\\THHmmss\\Z")));
                str.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", DateTime.Now.AddMinutes(+760).ToString("yyyyMMdd\\THHmmss\\Z")));
                str.AppendLine("LOCATION:L&T, HAZIRA, SURAT, 394270");
                str.AppendLine(string.Format("DESCRIPTION:{0}", msg.Body));
                str.AppendLine(string.Format("X-ALT-DESC;FMTTYPE=text/html:{0}", msg.Body));
                str.AppendLine(string.Format("SUMMARY:{0}", msg.Subject));
                str.AppendLine(string.Format("ORGANIZER:MAILTO:{0}", "Hardik.savani@rigelnetworks.com"));

                str.AppendLine(string.Format("ATTENDEE;CN=\"{0}\";RSVP=TRUE:mailto:{1}", msg.To[0].DisplayName, msg.To[0].Address));
                str.AppendLine("BEGIN:VALARM");
                str.AppendLine("TRIGGER:-PT15M");
                str.AppendLine("ACTION:DISPLAY");
                str.AppendLine("DESCRIPTION:Reminder");
                str.AppendLine("END:VALARM");
                str.AppendLine("END:VEVENT");
                str.AppendLine("END:VCALENDAR");
                System.Net.Mime.ContentType ct = new System.Net.Mime.ContentType("text/calendar");
                ct.Parameters.Add("METHOD", "REQUEST");
                AlternateView avCal = AlternateView.CreateAlternateViewFromString(str.ToString(), ct);
                msg.AlternateViews.Add(avCal);
                try
                {
                    new SmtpClient("172.27.14.246")
                        .Send(msg);
                    return "Success";
                }
                catch
                {
                    return "Fail";
                }
            }
            catch (Exception e)
            {
                return "Catch: " + e.Message;
            }
        }

        public string Update()
        {
            try
            {
                MailMessage msg = new MailMessage("IEMQS@larsentoubro.com", "hardik.savani@rigelnetworks.com");
                msg.Headers.Add("Content-class", "urn:content-classes:calendarmessage");
                msg.To.Add("trishul.tandel@rigelnetworks.com");
                msg.Bcc.Add("Hrddk79@gmail.com");

                msg.Subject = "Previously meeting been rescheduled. please pay attention.";
                msg.Body = $"Hello,\n\nPreviously Scheduled event has been updated.\nPlease be available on new time\n\nThanks,\n-Hardik";

                StringBuilder str = new StringBuilder();
                str.AppendLine("BEGIN:VCALENDAR");
                str.AppendLine("VERSION:2.0");
                str.AppendLine("PRODID:-//Schedule a Meeting");
                str.AppendLine("METHOD:REQUEST");
                str.AppendLine("BEGIN:VEVENT");
                str.AppendLine("SEQUENCE:5");
                str.AppendLine("STATUS:CONFIRMED");
                str.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", new DateTime(2019, 1, 1, 15, 00, 00)));
                str.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", new DateTime(2019, 1, 1, 15, 00, 00)));
                str.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", new DateTime(2019, 1, 1, 17, 00, 00)));

                str.AppendLine("LOCATION:L&T, Hazira, Surat 394270.");
                str.AppendLine(string.Format("UID:{0}", "TestMeeting"));
                str.AppendLine(string.Format("DESCRIPTION:{0}", msg.Body));
                str.AppendLine(string.Format("X-ALT-DESC;FMTTYPE=text/html:{0}", msg.Body));
                str.AppendLine(string.Format("SUMMARY:{0}", msg.Subject));
                str.AppendLine(string.Format("ORGANIZER:MAILTO:{0}", msg.From.Address));
                str.AppendLine("END:VEVENT");
                str.AppendLine("END:VCALENDAR");

                System.Net.Mime.ContentType contype = new System.Net.Mime.ContentType("text/calendar");
                contype.Parameters.Add("method", "CANCELLED");

                AlternateView avCal = AlternateView.CreateAlternateViewFromString(str.ToString(), contype);
                msg.AlternateViews.Add(avCal);

                new SmtpClient("172.27.14.246").Send(msg);
                return "success";
            }
            catch (Exception ex)
            {
                return "Catch: " + ex.Message;
            }
        }

        public string Cancel()
        {
            try
            {

                MailMessage msg = new MailMessage("IEMQS@larsentoubro.com", "hardik.savani@rigelnetworks.com");
                msg.Headers.Add("Content-class", "urn:content-classes:calendarmessage");
                msg.To.Add("trishul.tandel@rigelnetworks.com");
                msg.Bcc.Add("Hrddk79@gmail.com");

                msg.Subject = "This is an email with test event";
                msg.Body = $"Hello,\n\nPreviously Scheduled event has been cancelled.\nSorry for the that.\n\nThanks,\n-Hardik";

                StringBuilder str = new StringBuilder();
                str.AppendLine("BEGIN:VCALENDAR");
                str.AppendLine("VERSION:2.0");
                str.AppendLine("PRODID:-//Schedule a Meeting");
                str.AppendLine("METHOD:CANCELLED");
                str.AppendLine("BEGIN:VEVENT");
                str.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", new DateTime(2019, 1, 1, 13, 00, 00)));
                str.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", new DateTime(2019, 1, 1, 13, 00, 00)));
                str.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", new DateTime(2019, 1, 1, 16, 00, 00)));
                str.AppendLine("SEQUENCE:7");
                str.AppendLine("STATUS:CANCELLED");
                str.AppendLine("LOCATION:L&T, Hazira, Surat 394270.");
                str.AppendLine(string.Format("UID:{0}", "TestMeeting"));
                str.AppendLine(string.Format("DESCRIPTION:{0}", msg.Body));
                str.AppendLine(string.Format("X-ALT-DESC;FMTTYPE=text/html:{0}", msg.Body));
                str.AppendLine(string.Format("SUMMARY:{0}", msg.Subject));
                str.AppendLine(string.Format("ORGANIZER:MAILTO:{0}", msg.From.Address));
                str.AppendLine("END:VEVENT");
                str.AppendLine("END:VCALENDAR");

                System.Net.Mime.ContentType contype = new System.Net.Mime.ContentType("text/calendar");
                contype.Parameters.Add("method", "CANCELLED");

                AlternateView avCal = AlternateView.CreateAlternateViewFromString(str.ToString(), contype);
                msg.AlternateViews.Add(avCal);

                new SmtpClient("172.27.14.246").Send(msg);
                return "success";
            }
            catch (Exception ex)
            {
                return "Catch: " + ex.Message;
            }
        }
    }
}