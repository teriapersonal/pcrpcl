﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IEMQS.Areas.TEMP.Models
{
    public class OrgModel
    {
        public string psno { get; set; }
        public string name { get; set; }
        public string iasp { get; set; }
        public string ifsp { get; set; }
        public string nfsp { get; set; }
        public string csup { get; set; }
        public string ccdr { get; set; }
        public string cadre { get; set; }
        public string depc { get; set; }
        public string department { get; set; }
        public string function { get; set; }
        public string grandchild { get; set; }
    
        public string Predecessors { get; set; }
        public string Successors { get; set; }
     
        public int Level { get; set; }
        public string TopParentUID { get; set; }
     
        public bool isRoot { get; set; }
        public bool isGroup { get; set; }
        public string group { get; set; }

        public int ParentId { get; set; }
        public int Id { get; set; }        

        public List<OrgModel> Children { get; set; }

        public int x { get; set; }
        public int y { get; set; }

    }

    public class ParentChildArray
    {
        public NodeDetails parent { get; set; }
        public NodeDetails child { get; set; }
        public string ParentUID { get; set; }
        public string ChildUID { get; set; }
        public bool IsProceed { get; set; }
    }

    public class NodeDetails
    {
        public string id { get; set; }
        public string UID { get; set; }
        public string name { get; set; }
        public string iasp { get; set; }
        public string ifsp { get; set; }
        public string nfsp { get; set; }
        public string csup { get; set; }
        public string ccdr { get; set; }
        public string cadre { get; set; }
        public string depc { get; set; }
        public int department { get; set; }
        public string function { get; set; }
        public string grandchild { get; set; }
        public bool isRoot { get; set; }
        public bool isGroup { get; set; }
        public string group { get; set; }
    }

    public class clsDataFlow
    {
        [JsonProperty("class")]
        public string Class { get; set; }
        public string nodeCategoryProperty { get; set; }
        public string linkFromPortIdProperty { get; set; }
        public string linkToPortIdProperty { get; set; }
        public List<nodeDataArray> nodeDataArray { get; set; }
        public List<linkDataArray> linkDataArray { get; set; }
        public clsDataFlow()
        {
            Class = "go.GraphLinksModel";
            nodeCategoryProperty = "type";
            linkFromPortIdProperty = "frompid";
            linkToPortIdProperty = "topid";
        }
    }

    public class nodeDataArray
    {
        public string key { get; set; }
        public string type { get; set; }
        public string name { get; set; }
        public string iasp { get; set; }
        public string ifsp { get; set; }
        public string nfsp { get; set; }
        public string csup { get; set; }
        public string ccdr { get; set; }
        public string cadre { get; set; }
        public string depc { get; set; }
        public string department { get; set; }
        public string function { get; set; }
        public string grandchild { get; set; }
        public bool isRoot { get; set; }
        public bool isGroup { get; set; }
        public string group { get; set; }
        public string photo { get; set; }
        public string lpdt { get; set; }


        public nodeDataArray()
        {          
            isRoot = false;
            isGroup = false;
            group = "";
        }

    }

    public class linkDataArray
    {
        public string from { get; set; }
        public string frompid { get; set; }
        public string to { get; set; }
        public string topid { get; set; }      
      
    }
}