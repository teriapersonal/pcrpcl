﻿using System.Web.Mvc;

namespace IEMQS.Areas.ELB
{
    public class ELBAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "ELB";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "ELB_default",
                "ELB/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}