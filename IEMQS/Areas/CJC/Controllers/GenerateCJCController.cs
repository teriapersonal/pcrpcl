﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace IEMQS.Areas.CJC.Controllers
{
    public class GenerateCJCController : clsBase
    {
        #region Index
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult LoadDataGridPartial(string status)
        {
            ViewBag.status = status;
            return PartialView("_LoadIndexGridDataPartial");
        }

        public ActionResult LoadDataGridData(JQueryDataTableParamModel param)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                var objQMS011 = db.QMS011.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;

                string whereCondition = " 1=1";
                string[] columnName = { "Project", "ShopDesc", "LocationDesc", "BusinessPartner", "Revision", "Status", "CreatedBy", "CONVERT(nvarchar(20),CreatedOn,103)" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstLines = db.SP_CJC_GET_HEADER_DETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from c in lstLines
                           select new[] {
                                        Convert.ToString(c.HeaderId),
                                        c.Project,
                                        c.ShopDesc,
                                        c.LocationDesc,
                                        c.BusinessPartner,
                                        Convert.ToString("R"+c.Revision),
                                        c.Status,
                                        c.CreatedBy,
                                        Convert.ToDateTime(c.CreatedOn).ToString("dd/MM/yyyy"),
                                       Helper.GenerateActionIcon(c.HeaderId, "View", "View Detail", "fa fa-eye", "",WebsiteURL +"/CJC/GenerateCJC/CJCDetails/"+c.HeaderId ,false)
                          }).ToList();

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""

                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region CJC Detail Page
        [SessionExpireFilter]
        public ActionResult CJCDetails(int Id = 0)
        {
            CJC020 objCJC020 = new CJC020();
            var JobDiaList = (from u in db.CJC003
                              where u.IsActive == true
                              select new { CatID = u.Id, CatDesc = u.Description }).ToList();
            var Shops = Manager.GetSubCatagorywithoutBULocation("Shop").Select(x => new { CatID = x.Value, CatDesc = x.CategoryDescription }).ToList();

            var lstLocation = Manager.GetUserwiseLocation(objClsLoginInfo.UserName);
            var LocationList = (from ath1 in lstLocation
                                join com2 in db.COM002 on ath1 equals com2.t_dimx
                                select new { CatID = ath1, CatDesc = com2.t_desc }).Distinct().ToList();

            ViewBag.PONos = db.CJC013.Select(x => new { CatID = x.PurchaseOdrerNo, CatDesc = x.PurchaseOdrerNo }).Distinct().ToList();
            if (Id > 0)
            {
                objCJC020 = db.CJC020.Where(x => x.HeaderId == Id).FirstOrDefault();
                if (objCJC020 != null)
                {
                    ViewBag.Location = LocationList.Where(x => x.CatID == objCJC020.Location).Select(x => x.CatDesc).FirstOrDefault();
                    ViewBag.Project = Manager.GetProjectAndDescription(objCJC020.Project);
                    ViewBag.Shop = Shops.Where(x => x.CatID == objCJC020.Shop).Select(x => x.CatDesc).FirstOrDefault();
                    ViewBag.JobDia = JobDiaList.Where(x => x.CatID == objCJC020.Dia).Select(x => x.CatDesc).FirstOrDefault();
                    ViewBag.Initiator = Manager.GetPsidandDescription(objCJC020.Initiator);
                    ViewBag.BusinessPartner = db.COM006.Where(i => i.t_bpid == objCJC020.BusinessPartner).Select(i => i.t_bpid + " - " + i.t_nama).FirstOrDefault();
                    ViewBag.Action = "edit";
                }
            }
            else
            {
                objCJC020.Revision = 0;
                objCJC020.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                ViewBag.Action = "new";
            }

            ViewBag.Locations = LocationList;
            ViewBag.ContractorShops = Shops;
            ViewBag.DiaList = JobDiaList;
            return View(objCJC020);
        }

        [HttpPost]
        public ActionResult SaveHeader(CJC020 cjc020)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                CJC020 objCJC020 = new CJC020();
                if (cjc020.HeaderId > 0)
                {
                    objCJC020 = db.CJC020.Where(o => o.HeaderId == cjc020.HeaderId).FirstOrDefault();
                    objCJC020.Project = cjc020.Project;
                    objCJC020.Shop = cjc020.Shop;
                    objCJC020.Dia = cjc020.Dia;
                    objCJC020.Location = cjc020.Location;
                    objCJC020.BusinessPartner = cjc020.BusinessPartner;
                    objCJC020.PurchaseOdrerNo = cjc020.PurchaseOdrerNo;
                    objCJC020.PurchaseOdrerLineNo = cjc020.PurchaseOdrerLineNo;
                    objCJC020.Initiator = cjc020.Initiator;
                    objCJC020.BillNo = cjc020.BillNo;
                    objCJC020.BillDate = cjc020.BillDate;
                    objCJC020.Revision = cjc020.Revision;
                    objCJC020.EditedBy = objClsLoginInfo.UserName;
                    objCJC020.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update;
                    objResponseMsg.Status = objCJC020.Status;
                    objResponseMsg.HeaderId = objCJC020.HeaderId;
                }
                else
                {
                    objCJC020.Project = cjc020.Project;
                    objCJC020.Shop = cjc020.Shop;
                    objCJC020.Dia = cjc020.Dia;
                    objCJC020.Location = cjc020.Location;
                    objCJC020.BusinessPartner = cjc020.BusinessPartner;
                    objCJC020.PurchaseOdrerNo = cjc020.PurchaseOdrerNo;
                    objCJC020.PurchaseOdrerLineNo = cjc020.PurchaseOdrerLineNo;
                    objCJC020.Initiator = cjc020.Initiator;
                    objCJC020.BillNo = cjc020.BillNo;
                    objCJC020.BillDate = cjc020.BillDate;
                    objCJC020.Revision = 0;
                    objCJC020.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                    objCJC020.CreatedBy = objClsLoginInfo.UserName;
                    objCJC020.CreatedOn = DateTime.Now;
                    db.CJC020.Add(objCJC020);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;
                    objResponseMsg.Status = objCJC020.Status;
                    objResponseMsg.HeaderId = objCJC020.HeaderId;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Seam Line Details
        public ActionResult LoadSeamLineGridDataPartial()
        {

            //ViewBag.SeamCategory = (from cjc012 in db.CJC011
            //                        join glb002 in db.GLB002 on cjc012.SeamCategory equals glb002.Code
            //                        join glb001 in db.GLB001 on glb002.Category equals glb001.Id
            //                        where glb001.Category.Equals("Seam Category", StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true
            //                        select glb002).Select(i => new { CatID = i.Code, CatDesc = i.Description }).Distinct().ToList();
            ViewBag.SeamCategory = Manager.GetSubCatagorywithoutBULocation("Seam Category").Select(x => new { CatID = x.Value, CatDesc = x.CategoryDescription }).ToList();
            ViewBag.NozzleCategory = Manager.GetSubCatagorywithoutBULocation("Nozzle Category").Select(x => new { CatID = x.Value, CatDesc = x.CategoryDescription }).ToList();

            List<CategoryData> lstWeldingProcess = Manager.GetSubCatagorywithoutBULocation("Welding Process").ToList();
            string[] weldingList = { "SMAW", "SAW", "GMAW", "FCAW", "ESSC" };

            ViewBag.WeldingProcessList = lstWeldingProcess.AsEnumerable().Where(x => weldingList.Contains(x.Value)).Select(x => new { CatID = x.Value, CatDesc = x.CategoryDescription }).ToList();

            List<CategoryData> lstCriticalSeam = Manager.GetSubCatagorywithoutBULocation("Critical Seam").ToList();
            ViewBag.CriticalSeam = lstCriticalSeam.AsEnumerable().Select(x => new { CatID = x.Value, CatDesc = x.CategoryDescription }).ToList();

            return PartialView("_LoadSeamLineGridDataPartial");
        }

        public ActionResult LoadSeamLineGridDataTable(JQueryDataTableParamModel param, int HeaderId)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;

                string whereCondition = " 1=1 and HeaderId=" + HeaderId;

                string[] columnName = { "ActivityDesc", "Quantity", "UOM", "Rate", "Amount", "CJCNo", "CreatedBy", "CONVERT(nvarchar(20),CreatedOn,103)" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstLines = db.SP_CJC_GET_SEAM_LINE_DETAILS(StartIndex, EndIndex, strSortOrder, whereCondition, param.Project, param.Location).ToList();
                int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from c in lstLines
                           select new[] {
                                        Convert.ToString(c.ROW_NO),
                                        c.LineId.ToString(),
                                        c.HeaderId.ToString(),
                                        c.QualityProject.ToString(),
                                        c.SeamNo.ToString(),
                                        c.Status.ToString(),
                                        c.SeamCategoryDesc.ToString(),
                                        Convert.ToString(c.NozzleSize),
                                        c.NozzleCategoryDesc.ToString(),
                                        c.SeamJointType.ToString(),
                                         Convert.ToString(c.SeamLength),
                                        c.Part1Position.ToString(),
                                        c.Part2Position.ToString(),
                                        c.Part3Position.ToString(),
                                        c.Part1Thickness.ToString(),
                                        c.Part2Thickness.ToString(),
                                        c.Part3Thickness.ToString(),
                                        Convert.ToString(c.MinThickness),
                                        Convert.ToString(c.Unit),
                                        c.WeldingProcessDesc.ToString(),
                                        c.SetupRate.HasValue ? Convert.ToString(c.SetupRate):"",
                                        c.WeldingRate.HasValue? Convert.ToString(c.WeldingRate):"",
                                        c.CriticalSeam.ToString(),
                                        Convert.ToString(c.Factor),
                                        Convert.ToString(c.Amount),
                                        c.SetupCJCNo != null?  c.SetupCJCNo.ToString() : "",
                                        c.WeldCJCNo != null? c.WeldCJCNo.ToString():"",
                                        Convert.ToString( "R"+ c.Revision),
                                        c.CreatedBy,
                                        Convert.ToDateTime(c.CreatedOn).ToString("dd/MM/yyyy"),
                                        Helper.HTMLActionString(c.LineId,"Edit","Edit Record","fa fa-pencil-square-o","EnabledEditLine(" + c.LineId + ");") + Helper.HTMLActionString(c.LineId,"Update","Update Record","fa fa-floppy-o","EditLine(" + c.LineId + ");","",false,"display:none") + " " + Helper.HTMLActionString(c.LineId,"Cancel","Cancel Edit","fa fa-ban","CancelEditLine(" + c.LineId + "); ","",false,"display:none") + " " + Helper.HTMLActionString(c.LineId,"Delete","Delete Record","fa fa-trash-o","DeleteLine("+ c.LineId +");"),
                          }).ToList();

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""

                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetSeamDetailInlineEditableRow(int id, int HeaderId = 0, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();
                var result = new List<string>();

                if (id == 0)
                {
                    int newRecordId = 0;
                    var newRecord = new[] {
                          clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                          Helper.GenerateHidden(newRecordId, "LineId", Convert.ToString(id)),
                          Helper.GenerateHidden(newRecordId, "HeaderId", Convert.ToString(HeaderId)),
                          Helper.HTMLAutoComplete(newRecordId, "txtQualityProject","","", false,"","QualityProject",false,"","","Select","clsMandatory")+""+Helper.GenerateHidden(newRecordId, "QualityProject",Convert.ToString("")),
                          Helper.HTMLAutoComplete(newRecordId, "txtSeamNo","","", false,"","SeamNo",false,"","","Select","clsMandatory")+""+Helper.GenerateHidden(newRecordId, "SeamNo",Convert.ToString("")),
                          "",
                          Helper.HTMLAutoComplete(newRecordId, "txtSeamCategory","","", false,"","SeamCategory",false,"","","Select","clsMandatory")+""+Helper.GenerateHidden(newRecordId, "SeamCategory",Convert.ToString("")),
                          Helper.GenerateHTMLTextbox(newRecordId, "NozzleSize",  "", "",true, "", false,"","",""),
                          Helper.HTMLAutoComplete(newRecordId, "txtNozzleCategory","","", false,"","NozzleCategory",false)+""+Helper.GenerateHidden(newRecordId, "NozzleCategory",Convert.ToString("")),
                          Helper.GenerateHTMLTextbox(newRecordId, "SeamJointType",  "", "",true, "", false,"","",""),
                          Helper.GenerateHTMLTextbox(newRecordId, "SeamLength",  "", "",true, "", false,"","",""),
                          Helper.GenerateHTMLTextbox(newRecordId, "Part1Position",  "", "",true, "", false,"","",""),
                          Helper.GenerateHTMLTextbox(newRecordId, "Part2Position",  "", "",true, "", false,"","",""),
                          Helper.GenerateHTMLTextbox(newRecordId, "Part3Position",  "", "",true, "", false,"","",""),
                          Helper.GenerateHTMLTextbox(newRecordId, "Part1Thickness",  "", "",true, "", false,"","",""),
                          Helper.GenerateHTMLTextbox(newRecordId, "Part2Thickness",  "", "",true, "", false,"","",""),
                          Helper.GenerateHTMLTextbox(newRecordId, "Part3Thickness",  "", "",true, "", false,"","",""),
                          Helper.GenerateHTMLTextbox(newRecordId, "MinThickness",  "", "",true, "", false,"","",""),
                          Helper.GenerateHTMLTextbox(newRecordId, "Unit",  "", "",true, "", false,"","",""),
                          Helper.HTMLAutoComplete(newRecordId, "txtWeldingProcess","","", false,"","WeldingProcess",false)+""+Helper.GenerateHidden(newRecordId, "WeldingProcess",Convert.ToString("")),
                          Helper.GenerateHTMLTextbox(newRecordId, "SetupRate",  "", "",true, "", false,"","",""),
                          Helper.GenerateHTMLTextbox(newRecordId, "WeldingRate",  "", "",true, "", false,"","",""),
                          Helper.HTMLAutoComplete(newRecordId, "txtCriticalSeam","","", false,"","CriticalSeam",false)+""+Helper.GenerateHidden(newRecordId, "CriticalSeam",Convert.ToString("")),
                         Helper.GenerateHTMLTextbox(newRecordId, "Factor",  "1", "",true, "", false,"","",""),
                          Helper.GenerateTextbox(newRecordId, "Amount",  "", "",false, "","","clsMandatory", false,"",""),
                          "",
                          "",
                          "",
                          "",
                          "",
                          Helper.HTMLActionString(newRecordId,"Add","Add New Record","fa fa-plus","SaveRecord(0);"),
                    };
                    data.Add(newRecord);
                }
                else
                {
                    var lstLines = db.SP_CJC_GET_SEAM_LINE_DETAILS(1, 0, "", "LineId = " + id,"","").Take(1).ToList();

                    data = (from c in lstLines
                            select new[] {
                                    clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                    Helper.GenerateHidden(c.LineId, "LineId", Convert.ToString(c.LineId)),
                                    Helper.GenerateHidden(c.LineId, "HeaderId", Convert.ToString(c.HeaderId)),
                                    isReadOnly ? c.QualityProject.ToString():Helper.HTMLAutoComplete(c.LineId, "txtQualityProject",c.QualityProject,"", true,"","QualityProject",false,"","","Select","clsMandatory")+""+Helper.GenerateHidden(c.LineId, "QualityProject",Convert.ToString(c.QualityProject)),
                                    isReadOnly ? c.SeamNo.ToString():Helper.HTMLAutoComplete(c.LineId, "txtSeamNo",c.SeamNo,"", true,"","SeamNo",false,"","","Select","clsMandatory")+""+Helper.GenerateHidden(c.LineId, "SeamNo",Convert.ToString(c.SeamNo)),
                                    isReadOnly ? c.Status.ToString():"",
                                    isReadOnly ? c.SeamCategory.ToString():Helper.HTMLAutoComplete(c.LineId, "txtSeamCategory",WebUtility.HtmlEncode(c.SeamCategoryDesc),"", false,"","SeamCategory",false,"","","Select","clsMandatory")+""+Helper.GenerateHidden(c.LineId, "SeamCategory",Convert.ToString(c.SeamCategory)),
                                    isReadOnly ? c.NozzleSize.ToString():Helper.GenerateTextbox(c.LineId, "NozzleSize", c.NozzleSize.ToString(), "",true, "","","clsMandatory", false,""),
                                    isReadOnly ? c.NozzleCategory.ToString():Helper.HTMLAutoComplete(c.LineId, "txtNozzleCategory",c.NozzleCategoryDesc,"", false,"","NozzleCategory",false)+""+Helper.GenerateHidden(c.LineId, "NozzleCategory",Convert.ToString(c.NozzleCategory)),
                                    isReadOnly ? c.SeamJointType.ToString():Helper.GenerateTextbox(c.LineId, "SeamJointType",  c.SeamJointType, "",true, "","","", false,"",""),
                                    isReadOnly ? c.SeamLength.ToString():Helper.GenerateTextbox(c.LineId, "SeamLength",  c.SeamLength.ToString(), "",true, "","","", false,"",""),
                                    isReadOnly ? c.Part1Position.ToString():Helper.GenerateTextbox(c.LineId, "Part1Position",  c.Part1Position, "",true, "","","", false,"",""),
                                    isReadOnly ? c.Part2Position.ToString():Helper.GenerateTextbox(c.LineId, "Part2Position",  c.Part2Position , "",true, "","","", false,"",""),
                                    isReadOnly ? c.Part3Position.ToString():Helper.GenerateTextbox(c.LineId, "Part3Position",  c.Part3Position , "",true, "","","", false,"",""),
                                    isReadOnly ? c.Part1Thickness.ToString():Helper.GenerateTextbox(c.LineId, "Part1Thickness",  c.Part1Thickness, "",true, "","","", false,"",""),
                                    isReadOnly ? c.Part2Thickness.ToString():Helper.GenerateTextbox(c.LineId, "Part2Thickness",  c.Part2Thickness, "",true, "","","", false,"",""),
                                    isReadOnly ? c.Part3Thickness.ToString():Helper.GenerateTextbox(c.LineId, "Part3Thickness",  c.Part3Thickness, "",true, "","","", false,"",""),
                                    isReadOnly ? c.MinThickness.ToString():Helper.GenerateTextbox(c.LineId, "MinThickness", c.MinThickness.ToString(), "",true,"","", "", false,"",""),
                                    isReadOnly ? c.Unit.ToString():Helper.GenerateTextbox(c.LineId, "Unit",  c.Unit.ToString(), "",true, "","","", false,"",""),
                                    isReadOnly ? c.WeldingProcess.ToString():Helper.HTMLAutoComplete(c.LineId, "txtWeldingProcess",c.WeldingProcessDesc.ToString(),"", false,"","WeldingProcess",false)+""+Helper.GenerateHidden(c.LineId, "WeldingProcess",Convert.ToString(c.WeldingProcess)),
                                    isReadOnly ? (c.SetupRate.HasValue ? c.SetupRate.ToString() :""):Helper.GenerateTextbox(c.LineId, "SetupRate", c.SetupRate.ToString(), "",true,"", "","", false,"",""),
                                    isReadOnly ? (c.WeldingRate.HasValue ?  c.WeldingRate.ToString():""):Helper.GenerateTextbox(c.LineId, "WeldingRate", c.WeldingRate.ToString(), "",true,"","", "", false,"",""),
                                    isReadOnly ? c.CriticalSeam.ToString():Helper.HTMLAutoComplete(c.LineId, "txtCriticalSeam",c.CriticalSeam,"", false,"","CriticalSeam",false)+""+Helper.GenerateHidden(c.LineId, "CriticalSeam",Convert.ToString(c.CriticalSeam)),
                                    isReadOnly ? (c.Factor.HasValue ?c.Factor.ToString():""):Helper.GenerateTextbox(c.LineId, "Factor",  c.Factor.ToString(), "",true, "","","", false,"",""),
                                    isReadOnly ? (c.Amount.HasValue? c.Amount.ToString():""):Helper.GenerateTextbox(c.LineId, "Amount",  c.Amount.ToString(), "",false, "","","clsMandatory", false,"",""),
                                    !string.IsNullOrWhiteSpace(c.SetupCJCNo)? c.SetupCJCNo.ToString() :"",
                                    !string.IsNullOrWhiteSpace(c.WeldCJCNo)?   c.WeldCJCNo.ToString():"",
                                    "R"+ c.Revision.ToString(),
                          c.CreatedBy,
                          Convert.ToDateTime(c.CreatedOn).ToString("dd/MM/yyyy"),
                          Helper.HTMLActionString(c.LineId,"Edit","Edit Record","fa fa-pencil-square-o","EnabledEditLine(" + c.LineId + ");") + Helper.HTMLActionString(c.LineId,"Update","Update Record","fa fa-floppy-o","SaveRecord(" + c.LineId + ");","",false,"display:none") + " " + Helper.HTMLActionString(c.LineId,"Cancel","Cancel Edit","fa fa-ban","CancelEditLine(" + c.LineId + "); ","",false,"display:none") + " " + Helper.HTMLActionString(c.LineId,"Delete","Delete Record","fa fa-trash-o","DeleteLine("+ c.LineId +");"),
                        }).ToList();
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveSeamLineData(FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (fc != null)
                {
                    int HeaderId = Convert.ToInt32(fc["MainHeaderId"]);
                    int LineId = Convert.ToInt32(fc["MainLineId"]);

                    CJC021 objCJC021 = null;
                    CJC020 objCJC020 = db.CJC020.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    string QualityProject = fc["QualityProject" + LineId]; ;
                    string SeamNo = fc["SeamNo" + LineId];
                    if (LineId > 0)
                    {

                        objCJC021 = db.CJC021.Where(x => x.LineId == LineId).FirstOrDefault();
                        if (objCJC021 != null)
                        {
                            if (!db.CJC021.Any(x => x.HeaderId == HeaderId && x.QualityProject == QualityProject && x.SeamNo == SeamNo && x.LineId != LineId))
                            {
                                objCJC021.HeaderId = HeaderId;
                                objCJC021.QualityProject = fc["QualityProject" + LineId];
                                objCJC021.SeamNo = fc["SeamNo" + LineId];
                                objCJC021.SetupCJCNo = fc["SetupCJCNo" + LineId];
                                objCJC021.WeldCJCNo = fc["WeldCJCNo" + LineId];
                                objCJC021.SeamCategory = fc["SeamCategory" + LineId];
                                objCJC021.NozzleSize = Convert.ToDouble(fc["NozzleSize" + LineId]);
                                objCJC021.NozzleCategory = fc["NozzleCategory" + LineId];
                                objCJC021.SeamJointType = fc["SeamJointType" + LineId];
                                objCJC021.SeamLength = Convert.ToInt32(fc["SeamLength" + LineId]);
                                objCJC021.Part1Position = fc["Part1Position" + LineId];
                                objCJC021.Part2Position = fc["Part2Position" + LineId];
                                objCJC021.Part3Position = fc["Part3Position" + LineId];
                                objCJC021.Part1Thickness = fc["Part1Thickness" + LineId];
                                objCJC021.Part2Thickness = fc["Part2Thickness" + LineId];
                                objCJC021.Part3Thickness = fc["Part3Thickness" + LineId];
                                objCJC021.MinThickness = Convert.ToDouble(fc["MinThickness" + LineId]);
                                objCJC021.Unit = Convert.ToDouble(fc["Unit" + LineId]);
                                objCJC021.WeldingProcess = fc["WeldingProcess" + LineId];
                                objCJC021.SetupRate = Convert.ToDouble(fc["SetupRate" + LineId]);
                                objCJC021.WeldingRate = Convert.ToDouble(fc["WeldingRate" + LineId]);
                                objCJC021.CriticalSeam = fc["CriticalSeam" + LineId];
                                objCJC021.Factor = Convert.ToDouble(fc["Factor" + LineId]);
                                objCJC021.Amount = Convert.ToDouble(fc["Amount" + LineId]);
                                objCJC021.Revision = objCJC020.Revision;
                                objCJC021.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                                objCJC021.EditedBy = objClsLoginInfo.UserName;
                                objCJC021.EditedOn = DateTime.Now;
                                db.SaveChanges();
                                objResponseMsg.Key = true;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                            }
                            else
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                            }
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
                        }
                    }
                    else
                    {
                        if (!db.CJC021.Any(x => x.HeaderId == HeaderId && x.QualityProject == QualityProject && x.SeamNo == SeamNo))
                        {
                            objCJC021 = new CJC021();
                            objCJC021.HeaderId = HeaderId;
                            objCJC021.QualityProject = fc["QualityProject" + LineId];
                            objCJC021.SeamNo = fc["SeamNo" + LineId];
                            objCJC021.SetupCJCNo = fc["SetupCJCNo" + LineId];
                            objCJC021.WeldCJCNo = fc["WeldCJCNo" + LineId];
                            objCJC021.SeamCategory = fc["SeamCategory" + LineId];
                            objCJC021.NozzleSize = Convert.ToDouble(fc["NozzleSize" + LineId]);
                            objCJC021.NozzleCategory = fc["NozzleCategory" + LineId];
                            objCJC021.SeamJointType = fc["SeamJointType" + LineId];
                            objCJC021.SeamLength = Convert.ToInt32(fc["SeamLength" + LineId]);
                            objCJC021.Part1Position = fc["Part1Position" + LineId];
                            objCJC021.Part2Position = fc["Part2Position" + LineId];
                            objCJC021.Part3Position = fc["Part3Position" + LineId];
                            objCJC021.Part1Thickness = fc["Part1Thickness" + LineId];
                            objCJC021.Part2Thickness = fc["Part2Thickness" + LineId];
                            objCJC021.Part3Thickness = fc["Part3Thickness" + LineId];
                            objCJC021.MinThickness = Convert.ToDouble(fc["MinThickness" + LineId]);
                            if (!(string.IsNullOrWhiteSpace(fc["Unit" + LineId])))
                            {
                                objCJC021.Unit = Convert.ToDouble(fc["Unit" + LineId]);
                            }
                            objCJC021.WeldingProcess = fc["WeldingProcess" + LineId];
                            objCJC021.SetupRate = Convert.ToDouble(fc["SetupRate" + LineId]);
                            objCJC021.WeldingRate = Convert.ToDouble(fc["WeldingRate" + LineId]);
                            objCJC021.CriticalSeam = fc["CriticalSeam" + LineId];
                            objCJC021.Factor = Convert.ToDouble(!string.IsNullOrWhiteSpace(fc["Factor" + LineId]) ? fc["Factor" + LineId] : "0");
                            objCJC021.Amount = Convert.ToDouble(!string.IsNullOrWhiteSpace(fc["Amount" + LineId]) ? fc["Amount" + LineId] : "0");
                            objCJC021.Revision = objCJC020.Revision;
                            objCJC021.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                            objCJC021.CreatedBy = objClsLoginInfo.UserName;
                            objCJC021.CreatedOn = DateTime.Now;
                            db.CJC021.Add(objCJC021);
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult DeleteSeamLineData(int LineId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                CJC021 objCJC021 = db.CJC021.Where(x => x.LineId == LineId).FirstOrDefault();
                if (objCJC021 != null)
                {
                    db.CJC021.Remove(objCJC021);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Details not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Project Details

        public ActionResult LoadProjectLineGridDataPartial()
        {
            ViewBag.ProjectActivityList = (from cjc012 in db.CJC012
                                           join glb002 in db.GLB002 on cjc012.ProjectActivity equals glb002.Code
                                           join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                                           where glb001.Category.Equals("Project Activity", StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true
                                           select glb002).Select(i => new { CatID = i.Code, CatDesc = i.Description }).Distinct().ToList();

            return PartialView("_LoadProjectLineGridDataPartial");
        }

        public ActionResult LoadProjectLineGridDataTable(JQueryDataTableParamModel param)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;

                string whereCondition = " 1=1";
                string[] columnName = { "ActivityDesc", "Quantity", "UOM", "Rate", "Amount", "CJCNo", "CreatedBy", "CONVERT(nvarchar(20),CreatedOn,103)" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                else
                {
                    strSortOrder = " Order By EditedOn desc ";
                }

                var lstLines = db.SP_CJC_GET_PROJECT_LINE_DETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from c in lstLines
                           select new[] {
                                        Convert.ToString(c.ROW_NO),
                                        Convert.ToString(c.LineId),
                                        Convert.ToString(c.HeaderId),
                                        c.ActivityDesc,
                                        Convert.ToString(c.Quantity),
                                        c.UOM,
                                        Convert.ToString(c.Rate),
                                        Convert.ToString(c.Amount),
                                        c.CJCNo,
                                        c.CreatedBy,
                                        Convert.ToDateTime(c.CreatedOn).ToString("dd/MM/yyyy"),
                                        Helper.HTMLActionString(c.LineId,"Edit","Edit Record","fa fa-pencil-square-o","EnabledEditLine(" + c.LineId + ");") + Helper.HTMLActionString(c.LineId,"Update","Update Record","fa fa-floppy-o","EditLine(" + c.LineId + ");","",false,"display:none") + " " + Helper.HTMLActionString(c.LineId,"Cancel","Cancel Edit","fa fa-ban","CancelEditLine(" + c.LineId + "); ","",false,"display:none") + " " + Helper.HTMLActionString(c.LineId,"Delete","Delete Record","fa fa-trash-o","DeleteLine("+ c.LineId +");"),
                          }).ToList();

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""

                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetProjectDetailInlineEditableRow(int id, int HeaderId = 0, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();
                var result = new List<string>();

                if (id == 0)
                {
                    int newRecordId = 0;
                    var newRecord = new[] {
                                    clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                    Helper.GenerateHidden(newRecordId, "Id", Convert.ToString(id)),
                                    Helper.GenerateHidden(newRecordId, "HeaderId", Convert.ToString(HeaderId)),
                                    Helper.HTMLAutoComplete(newRecordId, "txtActivity","","", false,"","Activity",false)+""+Helper.GenerateHidden(newRecordId, "Activity",Convert.ToString("")),
                                    Helper.GenerateNumericTextbox(newRecordId, "txtQuantity",  "", "", false, "", ""),
                                    Helper.GenerateHTMLTextbox(newRecordId, "txtUOM",  "", "",true, "", false,"","",""),
                                    Helper.GenerateNumericTextbox(newRecordId, "txtRate",  "", "",true, "", ""),
                                    Helper.GenerateNumericTextbox(newRecordId, "txtAmount",  "", "",true, "", ""),
                                    Helper.GenerateHTMLTextbox(newRecordId, "txtCJCNo",  "", "", false, "", true,"","",""),
                                    "",
                                    "",
                                     Helper.HTMLActionString(newRecordId,"Add","Add New Record","fa fa-plus","SaveNewRecord();"),
                    };
                    data.Add(newRecord);
                }
                else
                {
                    var lstLines = db.SP_CJC_GET_PROJECT_LINE_DETAILS(1, 0, "", "LineId = " + id).Take(1).ToList();

                    data = (from c in lstLines
                            select new[] {
                                    clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                    Helper.GenerateHidden(c.LineId, "Id", Convert.ToString(id)),
                                    Helper.GenerateHidden(c.LineId, "HeaderId", Convert.ToString(HeaderId)),
                                    isReadOnly ? c.ActivityDesc : Helper.HTMLAutoComplete(c.LineId, "txtActivity",c.ActivityDesc,"", false,"","Activity",false)+""+Helper.GenerateHidden(c.LineId, "Activity",Convert.ToString(c.Activity)),
                                    isReadOnly ? Convert.ToString(c.Quantity) : Helper.GenerateNumericTextbox(c.LineId, "txtQuantity",(c.Quantity.HasValue?c.Quantity.Value.ToString():""), "",false, "", ""),
                                    isReadOnly ? c.UOM : Helper.GenerateHTMLTextbox(c.LineId, "txtUOM",c.UOM, "", true, "", false,"","",""),
                                    isReadOnly ? Convert.ToString(c.Rate) : Helper.GenerateNumericTextbox(c.LineId, "txtRate", c.Rate.HasValue?c.Rate.Value.ToString():"", "",true, "", ""),
                                    isReadOnly ? Convert.ToString(c.Amount) : Helper.GenerateNumericTextbox(c.LineId, "txtAmount",  c.Amount.HasValue?c.Amount.Value.ToString():"", "", true, "", ""),
                                    isReadOnly ? c.CJCNo : Helper.GenerateHTMLTextbox(c.LineId, "txtCJCNo",c.CJCNo, "", false, "", true,"","",""),
                                    c.CreatedBy,
                                    Convert.ToDateTime(c.CreatedOn).ToString("dd/MM/yyyy"),
                                    Helper.HTMLActionString(c.LineId,"Edit","Edit Record","fa fa-pencil-square-o","EnabledEditLine(" + c.LineId + ");") + Helper.HTMLActionString(c.LineId,"Update","Update Record","fa fa-floppy-o","EditLine(" + c.LineId + ");","",false,"display:none") + " " + Helper.HTMLActionString(c.LineId,"Cancel","Cancel Edit","fa fa-ban","CancelEditLine(" + c.LineId + "); ","",false,"display:none") + " " + Helper.HTMLActionString(c.LineId,"Delete","Delete Record","fa fa-trash-o","DeleteLine("+ c.LineId +");"),
                        }).ToList();
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProjectLineData(FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (fc != null)
                {
                    int HeaderId = Convert.ToInt32(fc["HeaderId"]);
                    int LineId = Convert.ToInt32(fc["LineId"]);

                    string Activity = fc["Activity" + LineId];
                    string UOM = fc["txtUOM" + LineId];
                    string CJCNo = fc["txtCJCNo" + LineId];

                    double? Quantity = null;
                    if (fc["txtQuantity" + LineId] != null && fc["txtQuantity" + LineId] != "")
                        Quantity = Convert.ToDouble(fc["txtQuantity" + LineId]);

                    double? Rate = null;
                    if (fc["txtRate" + LineId] != null && fc["txtRate" + LineId] != "")
                        Rate = Convert.ToDouble(fc["txtRate" + LineId]);

                    double? Amount = null;
                    if (fc["txtAmount" + LineId] != null && fc["txtAmount" + LineId] != "")
                        Amount = Convert.ToDouble(fc["txtAmount" + LineId]);

                    CJC022 objCJC022 = null;
                    if (LineId > 0)
                    {
                        objCJC022 = db.CJC022.Where(x => x.LineId == LineId).FirstOrDefault();
                        if (objCJC022 != null)
                        {
                            if (!db.CJC022.Any(x => x.HeaderId == HeaderId && x.Activity == Activity && x.LineId != LineId))
                            {
                                objCJC022.Activity = Activity;
                                objCJC022.Quantity = Quantity;
                                objCJC022.UOM = UOM;
                                objCJC022.Rate = Rate;
                                objCJC022.Amount = Amount;

                                objCJC022.EditedBy = objClsLoginInfo.UserName;
                                objCJC022.EditedOn = DateTime.Now;

                                db.SaveChanges();
                                objResponseMsg.Key = true;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                            }
                            else
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                            }
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
                        }
                    }
                    else
                    {
                        if (!db.CJC022.Any(x => x.HeaderId == HeaderId && x.Activity == Activity))
                        {
                            objCJC022 = new CJC022();

                            objCJC022.HeaderId = HeaderId;
                            objCJC022.Activity = Activity;
                            objCJC022.Quantity = Quantity;
                            objCJC022.UOM = UOM;
                            objCJC022.Rate = Rate;
                            objCJC022.Amount = Amount;

                            objCJC022.CreatedBy = objClsLoginInfo.UserName;
                            objCJC022.CreatedOn = DateTime.Now;

                            db.CJC022.Add(objCJC022);

                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult DeleteProjectLineData(int LineId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                CJC022 objCJC022 = db.CJC022.Where(x => x.LineId == LineId).FirstOrDefault();
                if (objCJC022 != null)
                {
                    db.CJC022.Remove(objCJC022);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Details not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Utility
        [HttpPost]
        public JsonResult GetInitiatorData(string term, string location)
        {
            string whereCondition = " Location='" + location + "' ";

            var lstLNInitiator = db.SP_CJC_GET_LN_INITIATOR(1, 10, "", whereCondition).ToList();
            var lstlstInitiator = (from a in lstLNInitiator
                                   join b in db.COM003 on a.PsNo equals b.t_psno
                                   where (!string.IsNullOrWhiteSpace(term) ? (a.PsNo.Contains(term) || b.t_name.Contains(term)) : true)
                                   select new { Value = a.PsNo, Text = a.PsNo + "-" + b.t_name }).Distinct().ToList();
            return Json(lstlstInitiator, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetBusinessPartner(string term)
        {
            var lstBusinessPartner = (from a in db.COM006
                                      where (a.t_bpid.Contains(term))
                                      select new { Value = a.t_bpid, Text = a.t_bpid + " - " + a.t_nama }).ToList();
            return Json(lstBusinessPartner, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetQualityProject(string term, string Project)
        {
            List<ddlValue> lstQProject = null;
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstQProject = db.QMS012_Log
                                     .Where(x => x.Project == Project && x.QualityProject.Contains(term))
                                     .Select(x => new ddlValue { Value = x.QualityProject, Text = x.QualityProject }).Distinct().Take(10).ToList();
            }
            else
            {
                lstQProject = db.QMS012_Log
                             .Where(x => x.Project == Project)
                             .Select(x => new ddlValue { Value = x.QualityProject, Text = x.QualityProject }).Distinct().Take(10).ToList();
            }
            return Json(lstQProject, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSeamFromQualityProject(string term, string qProject)
        {
            string approved = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
            List<ddlValue> lstSeam = null;
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstSeam = db.QMS012_Log
                                .Where(x => x.QualityProject == qProject && x.SeamNo.Contains(term) && x.Status == approved)
                                .OrderByDescending(x => x.RefId)
                                .Select(x => new ddlValue { Value = x.SeamNo, Text = x.SeamNo + "-" + x.SeamDescription }).Distinct().Take(10).ToList();
            }
            else
            {
                lstSeam = db.QMS012_Log
                         .Where(x => x.QualityProject == qProject && x.Status == approved)
                         .OrderByDescending(x => x.RefId)
                         .Select(x => new ddlValue { Value = x.SeamNo, Text = x.SeamNo + "-" + x.SeamDescription }).Distinct().Take(10).ToList();
            }
            return Json(lstSeam, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult FetchSeamListData(string seamno, string qProject)
        {
            CJC021 objModelCJC021 = SeamData(qProject, seamno);

            return Json(objModelCJC021, JsonRequestBehavior.AllowGet);
        }
        public CJC021 SeamData(string qproject, string seamno)
        {
            CJC021 objFetchCJC021 = new CJC021();
            string approved = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
            var objSeamData = (from q12 in db.QMS012_Log
                               join q11 in db.QMS011_Log on q12.HeaderId equals q11.HeaderId
                               where q11.QualityProject == qproject.Trim() && q12.SeamNo == seamno && q12.Status == approved
                               orderby q11.Id descending
                               select q12).FirstOrDefault();
            if (objSeamData != null)
            {
                objFetchCJC021.MinThickness = 0;
                //objFetchCJC021.NozzleSize = (objSeamData.SeamLengthArea / Math.PI) / 25.4;
                objFetchCJC021.SeamJointType = objSeamData.WeldType;
                objFetchCJC021.SeamLength = objSeamData.SeamLengthArea;

                if (!string.IsNullOrWhiteSpace(objSeamData.Position))
                {
                    var lstPosition = objSeamData.Position.Split(',');
                    objFetchCJC021.Part1Position = (lstPosition.Count() > 0) ? objSeamData.Position.Split(',')[0] : "";
                    objFetchCJC021.Part2Position = (lstPosition.Count() > 1) ? objSeamData.Position.Split(',')[1] : "";
                    objFetchCJC021.Part3Position = (lstPosition.Count() > 2) ? objSeamData.Position.Split(',')[2] : "";
                }
                if (!string.IsNullOrWhiteSpace(objSeamData.Thickness))
                {
                    var lstThickness = objSeamData.Thickness.Split(',');
                    List<int> minthickness = new List<int>();
                    objFetchCJC021.Part1Thickness = (lstThickness.Count() > 0) ? objSeamData.Thickness.Split(',')[0] : "";
                    objFetchCJC021.Part2Thickness = (lstThickness.Count() > 1) ? objSeamData.Thickness.Split(',')[1] : "";
                    objFetchCJC021.Part3Thickness = (lstThickness.Count() > 2) ? objSeamData.Thickness.Split(',')[2] : "";
                    if (lstThickness.Count() > 0)
                    {
                        if (lstThickness.Count() > 0)
                            minthickness.Add(Convert.ToInt32(objFetchCJC021.Part1Thickness));
                        if (lstThickness.Count() > 1)
                            minthickness.Add(Convert.ToInt32(objFetchCJC021.Part2Thickness));
                        if (lstThickness.Count() > 2)
                            minthickness.Add(Convert.ToInt32(objFetchCJC021.Part3Thickness));
                    }

                    objFetchCJC021.MinThickness = minthickness.Min(x => x);
                }
            }

            return objFetchCJC021;
        }

        [HttpPost]
        public ActionResult FetchFactorData(string CriticalSeam)
        {
            double? Factor = 1;
            var obj = db.CJC014.Where(i => i.CriticalSeam == CriticalSeam).FirstOrDefault();
            if (obj != null)
            {
                Factor = obj.Factor;
            }
            return Json(Factor, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult CalculateSetUpNonOverlayRate(string SeamCategory, double Minthk, int Jobdia, string typethickness)
        {
            double? SetUpRate = 0;
            CJC011 obj = new CJC011();
            int id = db.Database.SqlQuery<int>("SELECT dbo.FN_CJC_FETCH_ID_FROM_MASTER(" + Minthk + ",'" + typethickness + "')").FirstOrDefault();
            if (id > 0)
            {
                if (typethickness == "NonOverlay")
                {
                    obj = db.CJC011.Where(x => x.SeamCategory == SeamCategory && x.Dia == Jobdia && x.NonOverlayThickness == id).FirstOrDefault();
                    if (obj != null)
                    {
                        SetUpRate = obj.Rate;
                    }
                }
            }
            return Json(SetUpRate, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CalculateSetUpNozzleSizeRate(string SeamCategory, string nozzlecategory, int nozzlesize, string typethickness)
        {
            double? SetUpRate = 0;
            CJC011 obj = new CJC011();
            int id = db.Database.SqlQuery<int>("SELECT dbo.FN_CJC_FETCH_ID_FROM_MASTER(" + nozzlesize + ",'" + typethickness + "')").FirstOrDefault();
            if (id > 0)
            {
                if (typethickness == "NonOverlay")
                {
                    obj = db.CJC011.Where(x => x.SeamCategory == SeamCategory && x.NozzleSize == nozzlesize && x.NonOverlayThickness == id).FirstOrDefault();
                    if (obj != null)
                    {
                        SetUpRate = obj.Rate;
                    }
                }
                if (typethickness == "NozzleSize")
                {
                    obj = db.CJC011.Where(x => x.SeamCategory == SeamCategory && x.NozzleCategory == nozzlecategory && x.NozzleSize == id).FirstOrDefault();
                    if (obj != null)
                    {
                    }
                }
            }
            return Json(SetUpRate, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CalculateWeldNonOverlayRate(string SeamCategory, double Minthk, int Jobdia, string weldprocess, string jointtype)
        {
            //NonOverlay
            double? SetUpRate = 0;
            CJC011 obj = new CJC011();
            string[] arr1 = { "SC002", "SC003", "SC008" };
            if (arr1.Contains(weldprocess))
            {
                int id = db.Database.SqlQuery<int>("SELECT dbo.FN_CJC_FETCH_ID_FROM_MASTER(" + Minthk + ",'NonOverlay')").FirstOrDefault();
                if (id > 0)
                {
                    if (weldprocess == "SC008")
                    {
                        obj = db.CJC011.Where(x => x.SeamCategory == SeamCategory && x.WeldingProcess == weldprocess && x.NonOverlayThickness == id).FirstOrDefault();
                        if (obj != null)
                        {
                            SetUpRate = obj.Rate;
                        }
                    }
                    else
                    {
                        obj = db.CJC011.Where(x => x.SeamCategory == SeamCategory && x.WeldingProcess == weldprocess && x.Dia == Jobdia && x.NonOverlayThickness == id).FirstOrDefault();
                        if (obj != null)
                        {
                            SetUpRate = obj.Rate;
                        }
                    }
                }
            }
            if (!string.IsNullOrWhiteSpace(jointtype))
            {

            }

            return Json(SetUpRate, JsonRequestBehavior.AllowGet);
        }

        #endregion

    }
}