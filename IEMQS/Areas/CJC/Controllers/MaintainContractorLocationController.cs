﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
namespace IEMQS.Areas.CJC.Controllers
{
    public class MaintainContractorLocationController : clsBase
    {
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetVendorList(string term)
        {
            List<COM006> objCOM006List = new List<COM006>();
            if (!string.IsNullOrWhiteSpace(term))
            {
                objCOM006List = (from a in db.COM006
                                 where (a.t_bpid.ToLower().Contains(term.ToLower()) || a.t_nama.ToLower().Contains(term.ToLower()))
                                 select a).Take(10).ToList();
            }
            else
            {
                objCOM006List = (from a in db.COM006
                                 select a).Take(10).ToList();
            }
            var lstlist = (from a in objCOM006List
                           select new { Value = a.t_bpid, Text = a.t_bpid + " - " + a.t_nama }).ToList();

            return Json(lstlist, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public PartialViewResult LoadIndexGridDataPartial()
        {
            List<CategoryData> lstShop = Manager.GetSubCatagorywithoutBULocation("Shop").ToList();
            ViewBag.ShopList = lstShop.AsEnumerable().Select(x => new { CatID = x.Value, CatDesc = x.CategoryDescription }).ToList();

            return PartialView("_LoadIndexGridDataPartial");
        }

        public ActionResult GetIndexGridData(JQueryDataTableParamModel param)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;

                string whereCondition = " 1=1";
                string[] columnName = { "Contractor", "ShopDesc", "CreatedBy", "CONVERT(nvarchar(20),CreatedOn,103)" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                else
                {
                    strSortOrder = " Order By EditedOn desc ";
                }

                var lstLines = db.SP_CJC_GET_VENDOR_SHOP_MAPPING(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from c in lstLines
                           select new[] {
                                        Convert.ToString(c.ROW_NO),
                                        Convert.ToString(c.Id),
                                        c.Contractor,
                                        c.ShopDesc,
                                        c.CreatedBy,
                                        Convert.ToDateTime(c.CreatedOn).ToString("dd/MM/yyyy"),
                                        Helper.HTMLActionString(c.Id,"Edit","Edit Record","fa fa-pencil-square-o","EnabledEditLine(" + c.Id + ");") + Helper.HTMLActionString(c.Id,"Update","Update Record","fa fa-floppy-o","EditLine(" + c.Id + ");","",false,"display:none") + " " + Helper.HTMLActionString(c.Id,"Cancel","Cancel Edit","fa fa-ban","CancelEditLine(" + c.Id + "); ","",false,"display:none") + " " + Helper.HTMLActionString(c.Id,"Delete","Delete Record","fa fa-trash-o","DeleteLine("+ c.Id +");"),
                          }).ToList();

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""

                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetInlineEditableRow(int id, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();
                var result = new List<string>();

                if (id == 0)
                {
                    int newRecordId = 0;
                    var newRecord = new[] {
                                    clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                    Helper.GenerateHidden(newRecordId, "Id", Convert.ToString(id)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtVendorCode","","",  false,"","VendorCode",false) +""+Helper.GenerateHidden(newRecordId, "VendorCode", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtShopCode","","",  false,"","ShopCode",false) +""+Helper.GenerateHidden(newRecordId, "ShopCode", Convert.ToString(newRecordId)),
                                    "",
                                    "",
                                     Helper.HTMLActionString(newRecordId,"Add","Add New Record","fa fa-plus","SaveNewRecord();"),
                    };
                    data.Add(newRecord);
                }
                else
                {
                    var lstLines = db.SP_CJC_GET_VENDOR_SHOP_MAPPING(1, 0, "", "Id = " + id).Take(1).ToList();

                    data = (from c in lstLines
                            select new[] {
                                    clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//Convert.ToString(c.ROW_NO),
                                    Helper.GenerateHidden(c.Id, "Id", Convert.ToString(id)),
                                    isReadOnly ? c.Contractor : Helper.HTMLAutoComplete(c.Id, "txtVendorCode",c.Contractor,"", false,"","VendorCode",false)+""+Helper.GenerateHidden(c.Id, "VendorCode",Convert.ToString(c.VendorCode)),
                                    isReadOnly ? c.ShopDesc : Helper.HTMLAutoComplete(c.Id, "txtShopCode",c.ShopDesc,"", false,"","ShopCode",false)+""+Helper.GenerateHidden(c.Id, "ShopCode",Convert.ToString(c.ShopCode)),
                                    c.CreatedBy,
                                    Convert.ToDateTime(c.CreatedOn).ToString("dd/MM/yyyy"),
                                    Helper.HTMLActionString(c.Id,"Edit","Edit Record","fa fa-pencil-square-o","EnabledEditLine(" + c.Id + ");") + Helper.HTMLActionString(c.Id,"Update","Update Record","fa fa-floppy-o","EditLine(" + c.Id + ");","",false,"display:none") + " " + Helper.HTMLActionString(c.Id,"Cancel","Cancel Edit","fa fa-ban","CancelEditLine(" + c.Id + "); ","",false,"display:none") + " " + Helper.HTMLActionString(c.Id,"Delete","Delete Record","fa fa-trash-o","DeleteLine("+ c.Id +");"),
                        }).ToList();
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveData(FormCollection fc, int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (fc != null)
                {
                    string VendorCode = fc["VendorCode" + Id];
                    string ShopCode = fc["ShopCode" + Id];

                    CJC015 objCJC015 = null;
                    if (Id > 0)
                    {
                        if (!db.CJC015.Any(x => x.VendorCode == VendorCode && x.ShopCode == ShopCode && x.Id != Id))
                        {
                            objCJC015 = db.CJC015.Where(x => x.Id == Id).FirstOrDefault();
                            if (objCJC015 != null)
                            {
                                objCJC015.VendorCode = VendorCode;
                                objCJC015.ShopCode = ShopCode;
                                objCJC015.EditedBy = objClsLoginInfo.UserName;
                                objCJC015.EditedOn = DateTime.Now;

                                db.SaveChanges();
                                objResponseMsg.Key = true;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                            }
                            else
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
                            }
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                        }
                    }
                    else
                    {
                        if (!db.CJC015.Any(x => x.VendorCode == VendorCode && x.ShopCode == ShopCode))
                        {
                            objCJC015 = new CJC015();

                            objCJC015.VendorCode = VendorCode;
                            objCJC015.ShopCode = ShopCode;
                            objCJC015.CreatedBy = objClsLoginInfo.UserName;
                            objCJC015.CreatedOn = DateTime.Now;

                            db.CJC015.Add(objCJC015);

                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult DeleteData(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                CJC015 objCJC015 = db.CJC015.Where(x => x.Id == Id).FirstOrDefault();
                if (objCJC015 != null)
                {
                    db.CJC015.Remove(objCJC015);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Details not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                var lst = db.SP_CJC_GET_VENDOR_SHOP_MAPPING(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Data Found";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                var newlst = (from li in lst
                              select new
                              {
                                  Contractor = li.Contractor,
                                  Location = li.ShopDesc,
                                  CreatedBy = li.CreatedBy,
                                  CreatedOn = Convert.ToDateTime(li.CreatedOn).ToString("dd/MM/yyyy"),
                              }).ToList();

                strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
    }
}