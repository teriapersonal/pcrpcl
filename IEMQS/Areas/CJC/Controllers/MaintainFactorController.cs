﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.CJC.Controllers
{
    public class MaintainFactorController : clsBase
    {
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        [SessionExpireFilter]
        public PartialViewResult LoadIndexGridDataPartial()
        {
            List<CategoryData> lstCriticalSeam = Manager.GetSubCatagorywithoutBULocation("Critical Seam").ToList();
            ViewBag.CriticalSeamList = lstCriticalSeam.AsEnumerable().Select(x => new { CatID = x.Value, CatDesc = x.CategoryDescription }).ToList();
           
            return PartialView("_LoadIndexGridDataPartial");
        }

        public ActionResult GetIndexGridData(JQueryDataTableParamModel param)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
              
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;

                string whereCondition = " 1=1";
                string[] columnName = { "CriticalSeamDesc", "Factor", "CreatedBy", "CONVERT(nvarchar(20),CreatedOn,103)" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                else
                {
                    strSortOrder = " Order By EditedOn desc ";
                }

                var lstLines = db.SP_CJC_GET_FACTOR(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from c in lstLines
                           select new[] {
                                        Convert.ToString(c.ROW_NO),
                                        Convert.ToString(c.Id),
                                        c.CriticalSeamDesc,
                                        Convert.ToString(c.Factor),
                                        c.CreatedBy,
                                        Convert.ToDateTime(c.CreatedOn).ToString("dd/MM/yyyy"),
                                        Helper.HTMLActionString(c.Id,"Edit","Edit Record","fa fa-pencil-square-o","EnabledEditLine(" + c.Id + ");") + Helper.HTMLActionString(c.Id,"Update","Update Record","fa fa-floppy-o","EditLine(" + c.Id + ");","",false,"display:none") + " " + Helper.HTMLActionString(c.Id,"Cancel","Cancel Edit","fa fa-ban","CancelEditLine(" + c.Id + "); ","",false,"display:none") + " " + Helper.HTMLActionString(c.Id,"Delete","Delete Record","fa fa-trash-o","DeleteLine("+ c.Id +");"),
                          }).ToList();

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""

                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetInlineEditableRow(int id, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();
                var result = new List<string>();

                if (id == 0)
                {
                    int newRecordId = 0;
                    var newRecord = new[] {
                                    clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                    Helper.GenerateHidden(newRecordId, "Id", Convert.ToString(id)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtCriticalSeam","","",  false,"","CriticalSeam",false) +""+Helper.GenerateHidden(newRecordId, "CriticalSeam", Convert.ToString(newRecordId)),
                                    Helper.GenerateNumericTextbox(newRecordId, "txtFactor",  "", "", false, "", ""),                                  
                                    "",
                                    "",
                                     Helper.HTMLActionString(newRecordId,"Add","Add New Record","fa fa-plus","SaveNewRecord();"),
                    };
                    data.Add(newRecord);
                }
                else
                {
                    var lstLines = db.SP_CJC_GET_FACTOR(1, 0, "", "Id = " + id).Take(1).ToList();

                    data = (from c in lstLines
                            select new[] {
                                    clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//Convert.ToString(c.ROW_NO),
                                    Helper.GenerateHidden(c.Id, "Id", Convert.ToString(id)),
                                    isReadOnly ? c.CriticalSeamDesc : Helper.HTMLAutoComplete(c.Id, "txtCriticalSeam",WebUtility.HtmlEncode(c.CriticalSeamDesc),"", false,"","CriticalSeam",false)+""+Helper.GenerateHidden(c.Id, "CriticalSeam",Convert.ToString(c.CriticalSeam)),
                                    isReadOnly ? Convert.ToString(c.Factor) : Helper.GenerateNumericTextbox(c.Id, "txtFactor",  c.Factor.HasValue?c.Factor.Value.ToString():"", "", false, "", ""),                                    
                                    c.CreatedBy,
                                    Convert.ToDateTime(c.CreatedOn).ToString("dd/MM/yyyy"),
                                    Helper.HTMLActionString(c.Id,"Edit","Edit Record","fa fa-pencil-square-o","EnabledEditLine(" + c.Id + ");") + Helper.HTMLActionString(c.Id,"Update","Update Record","fa fa-floppy-o","EditLine(" + c.Id + ");","",false,"display:none") + " " + Helper.HTMLActionString(c.Id,"Cancel","Cancel Edit","fa fa-ban","CancelEditLine(" + c.Id + "); ","",false,"display:none") + " " + Helper.HTMLActionString(c.Id,"Delete","Delete Record","fa fa-trash-o","DeleteLine("+ c.Id +");"),
                        }).ToList();
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveData(FormCollection fc, int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (fc != null)
                {
                    string CriticalSeam = fc["CriticalSeam" + Id];
                   
                    double? Factor = null;
                    if (fc["txtFactor" + Id] != null && fc["txtFactor" + Id] != "")
                        Factor = Convert.ToDouble(fc["txtFactor" + Id]);
                  
                    CJC014 objCJC014 = null;
                    if (Id > 0)
                    {
                        if (!db.CJC014.Any(x => x.CriticalSeam == CriticalSeam && x.Id != Id))
                        {
                            objCJC014 = db.CJC014.Where(x => x.Id == Id).FirstOrDefault();
                            if (objCJC014 != null)
                            {
                                objCJC014.CriticalSeam = CriticalSeam;
                                objCJC014.Factor = Factor;

                                objCJC014.EditedBy = objClsLoginInfo.UserName;
                                objCJC014.EditedOn = DateTime.Now;

                                db.SaveChanges();
                                objResponseMsg.Key = true;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                            }
                            else
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
                            }
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                        }
                    }
                    else
                    {
                        if (!db.CJC014.Any(x => x.CriticalSeam == CriticalSeam))
                        {
                            objCJC014 = new CJC014();

                            objCJC014.CriticalSeam = CriticalSeam;
                            objCJC014.Factor = Factor;

                            objCJC014.CreatedBy = objClsLoginInfo.UserName;
                            objCJC014.CreatedOn = DateTime.Now;

                            db.CJC014.Add(objCJC014);

                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult DeleteData(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                CJC014 objCJC014 = db.CJC014.Where(x => x.Id == Id).FirstOrDefault();
                if (objCJC014 != null)
                {
                    db.CJC014.Remove(objCJC014);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Details not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                var lst = db.SP_CJC_GET_FACTOR(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Data Found";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                var newlst = (from li in lst
                              select new
                              {
                                  CriticalSeam = li.CriticalSeamDesc,
                                  Factor = li.Factor,                                 
                                  CreatedBy = li.CreatedBy,
                                  CreatedOn = Convert.ToDateTime(li.CreatedOn).ToString("dd/MM/yyyy"),
                              }).ToList();

                strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
    }
}