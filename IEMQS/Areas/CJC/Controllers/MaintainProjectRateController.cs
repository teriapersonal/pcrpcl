﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace IEMQS.Areas.CJC.Controllers
{
    public class MaintainProjectRateController : clsBase
    {
        // GET: CJC/MaintainProjectRate
        #region Index Page
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Index()
        {
            ViewBag.Title = "Maintain Project Rate Matrix";
            return View();
        }

        //load tab wise partial
        [HttpPost]
        public ActionResult LoadDataGridPartial()
        {
            var lstActivity = Manager.GetSubCatagorywithoutBULocation("Project Activity");
            var lstUOM = Manager.GetSubCatagorywithoutBULocation("UOM");
            ViewBag.Dia = db.CJC003.Select(x => new { CatID = x.Id.ToString(), CatDesc = x.Description });
            ViewBag.Activity = lstActivity.Select(x => new { CatID = x.Value, CatDesc = x.CategoryDescription }).ToList();
            ViewBag.UOM = lstUOM.Select(x => new { CatID = x.Value, CatDesc = x.CategoryDescription }).ToList();

            return PartialView("_LoadIndexGridDataPartial");
        }

        //bind datatable for Data Grid
        [HttpPost]
        public JsonResult LoadDataGridData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                strWhereCondition += "1=1 ";

                //search Condition 

                string[] columnName = { "ProjectActivityDesc", "UOMDesc", "Rate", "DiaDesc", "ProjectActivity", "UOM", "Dia", "CreatedBy" };
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                var lstResult = db.SP_CJC_GET_PROJECT_RATE_MATRIX(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            uc.ROW_NO.ToString(),
                            Convert.ToString(uc.Id),
                            uc.ProjectActivityDesc,
                            uc.UOMDesc,
                            uc.Rate.HasValue ? uc.Rate.ToString():"",
                            uc.DiaDesc,
                            uc.CreatedBy,
                            Convert.ToString(uc.CreatedOn.HasValue ? uc.CreatedOn.Value.ToString("dd/MM/yyyy"):string.Empty),
                            Helper.HTMLActionString(uc.Id, "Edit", "Edit Record", "fa fa-pencil-square-o", "EnabledEditLine(" + uc.Id + ");") + Helper.HTMLActionString(uc.Id, "Update", "Update Record", "fa fa-floppy-o", "EditLine(" + uc.Id + ");", "", false, "display:none") + " " + Helper.HTMLActionString(uc.Id, "Cancel", "Cancel Edit", "fa fa-ban", "CancelEditLine(" + uc.Id + "); ", "", false, "display:none") + " " + Helper.HTMLActionString(uc.Id, "Delete", "Delete Record", "fa fa-trash-o", "DeleteLine(" + uc.Id + ");")
                          }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public bool CheckDuplicate(int id, string activity, string uom, int Dia)
        {
            bool flg = false;
            if (db.CJC012.Where(x => x.ProjectActivity == activity && x.Id != id && x.UOM == uom && x.Dia == Dia).Any())
            { flg = true; }
            return flg;
        }
        [HttpPost]
        public JsonResult GetInlineEditableRow(int id, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();
                var result = new List<string>();
                if (id == 0)
                {
                    int newRecordId = 0;
                    var newRecord = new[] {
                         clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                          Helper.GenerateHidden(newRecordId, "Id", Convert.ToString(id)),
                          Helper.HTMLAutoComplete(newRecordId,"txtProjectActivity","","",  false,"","ProjectActivity",false,"","","Select")+""+Helper.GenerateHidden(newRecordId, "ProjectActivity", Convert.ToString(newRecordId)),
                          Helper.HTMLAutoComplete(newRecordId,"txtUOM","","",  false,"","UOM",false,"","","Select")+""+Helper.GenerateHidden(newRecordId, "UOM", Convert.ToString(newRecordId)),
                          Helper.GenerateNumericTextbox(newRecordId,"Rate","","",false,"",false,"10"),
                          Helper.HTMLAutoComplete(newRecordId,"txtDia","","",  false,"","Dia",false,"","","Select")+""+Helper.GenerateHidden(newRecordId, "Dia", Convert.ToString(newRecordId)),
                          "","",
                          Helper.HTMLActionString(newRecordId,"Add","Add New Record","fa fa-plus","SaveNewRecord();"),
                    };
                    data.Add(newRecord);
                }
                else
                {
                    var lstResult = db.SP_CJC_GET_PROJECT_RATE_MATRIX(1, 0, "", "Id = " + id).ToList();

                    data = (from uc in lstResult
                            select new[]
                           {
                        clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                        Helper.GenerateHidden(uc.Id, "Id", Convert.ToString(uc.Id)),
                        isReadOnly? uc.ProjectActivityDesc :Helper.HTMLAutoComplete(uc.Id, "txtProjectActivity", uc.ProjectActivityDesc, "",false,"","ProjectActivityDesc",false,"","","Select")+""+Helper.GenerateHidden(uc.Id, "ProjectActivity", Convert.ToString(uc.ProjectActivity)),
                        isReadOnly?uc.UOMDesc:Helper.HTMLAutoComplete(uc.Id, "txtUOM", uc.UOMDesc, "",false,"","UOM",false,"","","Select")+""+Helper.GenerateHidden(uc.Id, "UOM", Convert.ToString(uc.UOM)),
                        isReadOnly?uc.Rate.ToString():Helper.GenerateNumericTextbox(uc.Id,"Rate",uc.Rate.ToString(),"",false,"",true,"10"),
                        isReadOnly?uc.DiaDesc:Helper.HTMLAutoComplete(uc.Id, "txtDia", uc.DiaDesc, "",false,"","Dia",false,"","","Select")+""+Helper.GenerateHidden(uc.Id, "Dia", Convert.ToString(uc.Dia)),
                        uc.CreatedBy,
                        Convert.ToString(uc.CreatedOn.Value!=null ?uc.CreatedOn.Value.ToString("dd/MM/yyyy"):string.Empty),
                        Helper.HTMLActionString(uc.Id, "Edit", "Edit Record", "fa fa-pencil-square-o", "EnabledEditLine(" + uc.Id + ");") + Helper.HTMLActionString(uc.Id, "Update", "Update Record", "fa fa-floppy-o", "EditLine(" + uc.Id + ");", "", false, "display:none") + " " + Helper.HTMLActionString(uc.Id, "Cancel", "Cancel Edit", "fa fa-ban", "CancelEditLine(" + uc.Id + "); ", "", false, "display:none") + " " + Helper.HTMLActionString(uc.Id, "Delete", "Delete Record", "fa fa-trash-o", "DeleteLine(" + uc.Id + ");")
                        //Helper.HTMLActionString(uc.Id,"Edit","Edit Record","fa fa-pencil-square-o","EnabledEditLine(" + uc.Id + ");","",false,"display:none") + Helper.HTMLActionString(uc.Id,"Update","Update Record","fa fa-floppy-o","EditLine(" + uc.Id + "); ") + " " + Helper.HTMLActionString(uc.Id,"Cancel","Cancel Edit","fa fa-ban","CancelEditLine(" + uc.Id + "); ") + " " + Helper.HTMLActionString(uc.Id,"Delete","Delete Record","fa fa-trash-o","DeleteLine("+ uc.Id +");")
                          }).ToList();
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveLines(FormCollection fc)
        {
            CJC012 objCJC012 = objCJC012 = new CJC012();
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (fc != null)
                {
                    int Id = Convert.ToInt32(fc["MainId"]);
                    string ProjectActivity = fc["ProjectActivity" + Id];
                    int Dia = Convert.ToInt32(fc["Dia" + Id]);
                    string UOM = fc["UOM" + Id];
                    if (CheckDuplicate(Id, ProjectActivity, UOM, Dia))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    if (Id > 0)
                    {
                        objCJC012 = db.CJC012.Where(x => x.Id == Id).FirstOrDefault();
                        if (objCJC012 != null)
                        {
                            objCJC012.ProjectActivity = fc["ProjectActivity" + Id];
                            objCJC012.UOM = fc["UOM" + Id];
                            objCJC012.Rate = Convert.ToDouble(fc["Rate" + Id]);
                            objCJC012.Dia = Dia;
                            objCJC012.EditedBy = objClsLoginInfo.UserName;
                            objCJC012.EditedOn = DateTime.Now;
                        }
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();

                    }
                    else
                    {
                        objCJC012 = new CJC012();
                        objCJC012.ProjectActivity = fc["ProjectActivity" + Id];
                        objCJC012.UOM = fc["UOM" + Id];
                        objCJC012.Rate = Convert.ToDouble(fc["Rate" + Id]);
                        objCJC012.Dia = Dia;
                        objCJC012.CreatedBy = objClsLoginInfo.UserName;
                        objCJC012.CreatedOn = DateTime.Now;
                        db.CJC012.Add(objCJC012);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult DeleteLine(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                //string[] arrayStatus = { clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue(), clsImplementationEnum.PTMTCTQStatus.Returned.GetStringValue() };
                CJC012 objCJC012 = db.CJC012.Where(x => x.Id == Id).FirstOrDefault();

                if (objCJC012 != null)
                {

                    db.CJC012.Remove(objCJC012);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Details not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            clsHelper.ResponceMsgWithFileName objexport = new clsHelper.ResponceMsgWithFileName();
            try
            {
                string strFileName = string.Empty;

                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    List<SP_CJC_GET_PROJECT_RATE_MATRIX_Result> lst = db.SP_CJC_GET_PROJECT_RATE_MATRIX(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      ProjectActivity = uc.ProjectActivityDesc.ToString(),
                                      UOM = uc.UOMDesc.ToString(),
                                      Rate = uc.Rate.ToString(),
                                      ProductType = uc.DiaDesc.ToString(),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

    }
}